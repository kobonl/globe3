inherited BusinessUnitDM: TBusinessUnitDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnNewRecord = TableMasterNewRecord
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableMasterBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterBONUS_FACTOR: TFloatField
      DefaultExpression = '0'
      FieldName = 'BONUS_FACTOR'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterNORM_ILL_VS_DIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterNORM_ILL_VS_INDIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterNORM_ILL_VS_TOTAL_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnNewRecord = TableMasterNewRecord
    OnFilterRecord = TableDetailFilterRecord
    TableName = 'BUSINESSUNIT'
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailBONUS_FACTOR: TFloatField
      DefaultExpression = '0'
      FieldName = 'BONUS_FACTOR'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableDetailNORM_ILL_VS_DIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailNORM_ILL_VS_INDIRECT_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableDetailNORM_ILL_VS_TOTAL_HOURS: TFloatField
      DefaultExpression = '0'
      FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = TablePlantFilterRecord
    TableName = 'PLANT'
    Left = 96
    Top = 184
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 216
    Top = 184
  end
end
