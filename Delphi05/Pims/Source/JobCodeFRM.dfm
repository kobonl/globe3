inherited JobCodeF: TJobCodeF
  Left = 270
  Top = 135
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Job codes'
  ClientHeight = 473
  ClientWidth = 695
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 695
    Height = 15
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 11
      Width = 693
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 693
      Height = 10
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'WorkSpot'
        end>
      Visible = False
      ShowBands = True
      object dxMasterGridColumn12: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 147
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        DisableEditor = True
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridLookupColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTLU'
      end
      object dxMasterGridColumn4: TdxDBGridLookupColumn
        Caption = 'Hour type'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxMasterGridColumn8: TdxDBGridDateColumn
        Caption = 'Date inative'
        DisableEditor = True
        MinWidth = 16
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
        DateValidation = True
      end
      object dxMasterGridColumn11: TdxDBGridCheckColumn
        Caption = 'Use job codes'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USE_JOBCODE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn5: TdxDBGridCheckColumn
        Caption = 'Automatic data colection'
        DisableEditor = True
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_DATACOL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn6: TdxDBGridCheckColumn
        Caption = 'Counter value'
        DisableEditor = True
        Width = 76
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_VALUE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn7: TdxDBGridCheckColumn
        Caption = 'Enter scan counter '
        DisableEditor = True
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn9: TdxDBGridCheckColumn
        Caption = 'Measure productivity'
        DisableEditor = True
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MEASURE_PRODUCTIVITY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn10: TdxDBGridCheckColumn
        Caption = 'Productive hour'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRODUCTIVE_HOUR_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 230
    Width = 695
    Height = 243
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 693
      Height = 241
      Align = alClient
      Caption = 'Job codes'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 67
        Width = 43
        Height = 13
        Caption = 'Job code'
      end
      object Label2: TLabel
        Left = 8
        Top = 93
        Width = 62
        Height = 13
        Caption = 'Business unit'
      end
      object Label6: TLabel
        Left = 8
        Top = 118
        Width = 71
        Height = 13
        Caption = 'Interface code'
      end
      object Label7: TLabel
        Left = 8
        Top = 146
        Width = 63
        Height = 13
        Caption = 'Date inactive'
      end
      object Label8: TLabel
        Left = 8
        Top = 19
        Width = 50
        Height = 13
        Caption = 'Plant code'
      end
      object Label9: TLabel
        Left = 8
        Top = 43
        Width = 75
        Height = 13
        Caption = 'Workspot  code'
      end
      object Label11: TLabel
        Left = 208
        Top = 93
        Width = 59
        Height = 13
        Caption = 'Sample Time'
      end
      object Label12: TLabel
        Left = 328
        Top = 93
        Width = 25
        Height = 13
        Caption = 'mins.'
      end
      object DBEditJobCodes: TDBEdit
        Tag = 1
        Left = 86
        Top = 66
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'JOB_CODE'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 4
      end
      object DBEditDescription: TDBEdit
        Tag = 1
        Left = 150
        Top = 66
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 5
      end
      object DBEditInterfaceCode: TDBEdit
        Left = 86
        Top = 117
        Width = 113
        Height = 19
        Ctl3D = False
        DataField = 'INTERFACE_CODE'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 7
        OnKeyUp = DBEditInterfaceCodeKeyUp
      end
      object dxDBDateEditInactiveDate: TdxDBDateEdit
        Left = 86
        Top = 143
        Width = 113
        Style.BorderStyle = xbs3D
        TabOrder = 9
        DataField = 'DATE_INACTIVE'
        DataSource = WorkSpotDM.DataSourceJobCode
      end
      object DBRadioGroupQty: TDBRadioGroup
        Left = 376
        Top = 8
        Width = 73
        Height = 97
        Caption = 'Quantity'
        DataField = 'QUANT_PIECE_YN'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        Items.Strings = (
          'Pieces'
          'Weights')
        TabOrder = 15
        Values.Strings = (
          'Y'
          'N')
      end
      object DBEditPlant: TDBEdit
        Tag = 1
        Left = 86
        Top = 15
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = WorkSpotDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditWK: TDBEdit
        Tag = 1
        Left = 86
        Top = 41
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'WORKSPOT_CODE'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
      end
      object DBEditPlantDesc: TDBEdit
        Tag = 1
        Left = 150
        Top = 15
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = WorkSpotDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditWKDesc: TDBEdit
        Tag = 1
        Left = 150
        Top = 41
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 3
      end
      object GroupBox1: TGroupBox
        Left = 456
        Top = 9
        Width = 225
        Height = 96
        Caption = 'Levels per hour'
        TabOrder = 16
        object Label3: TLabel
          Left = 9
          Top = 25
          Width = 104
          Height = 13
          Caption = 'Norm production level'
        end
        object Label4: TLabel
          Left = 9
          Top = 48
          Width = 54
          Height = 13
          Caption = 'Bonus level'
        end
        object Label5: TLabel
          Left = 9
          Top = 71
          Width = 102
          Height = 13
          Caption = 'Norm machine output'
        end
        object Label10: TLabel
          Left = 168
          Top = 6
          Width = 54
          Height = 13
          Alignment = taRightJustify
          Caption = 'Minimum %'
        end
        object dxDBMaskEdit1: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 20
          Width = 41
          Style.BorderStyle = xbsSingle
          TabOrder = 0
          DataField = 'NORM_PROD_LEVEL'
          DataSource = WorkSpotDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEdit2: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 44
          Width = 41
          Style.BorderStyle = xbsSingle
          TabOrder = 2
          DataField = 'BONUS_LEVEL'
          DataSource = WorkSpotDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEdit3: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 68
          Width = 41
          Style.BorderStyle = xbsSingle
          TabOrder = 3
          DataField = 'NORM_OUTPUT_LEVEL'
          DataSource = WorkSpotDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEditProdLevelMinPerc: TdxDBMaskEdit
          Left = 176
          Top = 20
          Width = 41
          Style.BorderStyle = xbsSingle
          TabOrder = 1
          DataField = 'PRODLEVELMINPERC'
          DataSource = WorkSpotDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEditMachOutputMinPerc: TdxDBMaskEdit
          Left = 176
          Top = 68
          Width = 41
          Style.BorderStyle = xbsSingle
          TabOrder = 4
          DataField = 'MACHOUTPUTMINPERC'
          DataSource = WorkSpotDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
      end
      object DBCheckBoxIgnoreInterface: TDBCheckBox
        Tag = 1
        Left = 204
        Top = 117
        Width = 173
        Height = 17
        Caption = 'Ignore other interface codes'
        Ctl3D = False
        DataField = 'IGNOREINTERFACECODE_YN'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 8
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxIgnoreInterfaceClick
      end
      object ButtonShowForm: TButton
        Left = 204
        Top = 140
        Width = 153
        Height = 25
        Caption = 'Show ignored codes'
        TabOrder = 10
        OnClick = ButtonShowFormClick
      end
      object DBCheckBox1: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 166
        Width = 267
        Height = 17
        Caption = 'Show job code at scanning'
        Ctl3D = False
        DataField = 'SHOW_AT_SCANNING_YN'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 11
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxIgnoreInterfaceClick
      end
      object GroupBoxCompareJob: TGroupBox
        Left = 376
        Top = 105
        Width = 305
        Height = 81
        Caption = 'Compare job'
        TabOrder = 17
        object Label13: TLabel
          Left = 14
          Top = 55
          Width = 91
          Height = 13
          Caption = 'Maximum deviation'
        end
        object DBCheckBoxCompJob: TDBCheckBox
          Tag = 1
          Left = 14
          Top = 16
          Width = 107
          Height = 17
          Caption = 'Comparison job'
          Ctl3D = False
          DataField = 'COMPARATION_JOB_YN'
          DataSource = WorkSpotDM.DataSourceJobCode
          ParentCtl3D = False
          TabOrder = 0
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxCompJobClick
        end
        object DBEditPercentage: TDBEdit
          Left = 110
          Top = 52
          Width = 107
          Height = 19
          Ctl3D = False
          DataField = 'MAXIMUM_DEVIATION'
          DataSource = WorkSpotDM.DataSourceJobCode
          ParentCtl3D = False
          TabOrder = 2
          OnKeyUp = DBEditInterfaceCodeKeyUp
        end
        object ButtonCompareJob: TButton
          Left = 112
          Top = 13
          Width = 105
          Height = 25
          Caption = 'Show compare jobs'
          TabOrder = 1
          OnClick = ButtonCompareJobClick
        end
        object DBRadioGroupRejectYN: TDBRadioGroup
          Left = 224
          Top = 8
          Width = 73
          Height = 66
          DataField = 'COMPARISON_REJECT_YN'
          DataSource = WorkSpotDM.DataSourceJobCode
          Enabled = False
          Items.Strings = (
            'In/out'
            'Rejects')
          TabOrder = 3
          Values.Strings = (
            'N'
            'Y')
        end
      end
      object DBCheckBox2: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 184
        Width = 267
        Height = 17
        Caption = 'Show job code at production screen'
        Ctl3D = False
        DataField = 'SHOW_AT_PRODUCTIONSCREEN_YN'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 12
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxIgnoreInterfaceClick
      end
      object DBLookupComboBoxBusinessUnit: TDBLookupComboBox
        Tag = 1
        Left = 86
        Top = 92
        Width = 113
        Height = 19
        Ctl3D = False
        DataField = 'BUSINESSUNIT_CODE'
        DataSource = WorkSpotDM.DataSourceJobCode
        DropDownWidth = 243
        KeyField = 'BUSINESSUNIT_CODE'
        ListField = 'DESCRIPTION;BUSINESSUNIT_CODE'
        ListSource = WorkSpotDM.DataSourceBULU
        ParentCtl3D = False
        TabOrder = 6
      end
      object DBCheckBox3: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 203
        Width = 267
        Height = 17
        Caption = 'Ignore quantities in reports'
        Ctl3D = False
        DataField = 'IGNOREQUANTITIESINREPORTS_YN'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 13
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxIgnoreInterfaceClick
      end
      object GroupBoxLinkJob: TGroupBox
        Left = 376
        Top = 186
        Width = 306
        Height = 44
        Caption = 'Link job'
        TabOrder = 18
        object ButtonLinkJob: TButton
          Left = 120
          Top = 11
          Width = 177
          Height = 25
          Caption = 'Show linked job'
          TabOrder = 1
          OnClick = ButtonLinkJobClick
        end
        object CheckBoxLinkJob: TCheckBox
          Left = 14
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Link job'
          TabOrder = 0
          OnClick = CheckBoxLinkJobClick
        end
      end
      object DBCheckBoxDefaultJob: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 221
        Width = 267
        Height = 17
        Caption = 'Default job'
        Ctl3D = False
        DataField = 'DEFAULT_YN'
        DataSource = WorkSpotDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 14
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxIgnoreInterfaceClick
      end
      object dxDBMaskEdit4: TdxDBMaskEdit
        Left = 280
        Top = 92
        Width = 41
        Style.BorderStyle = xbsSingle
        TabOrder = 19
        DataField = 'SAMPLE_TIME_MINS'
        DataSource = WorkSpotDM.DataSourceJobCode
        IgnoreMaskBlank = False
        MaxLength = 6
        StoredValues = 2
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 41
    Width = 695
    Height = 189
    inherited spltDetail: TSplitter
      Top = 185
      Width = 693
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 693
      Height = 184
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Job codes'
        end>
      KeyField = 'JOB_CODE'
      DataSource = WorkSpotDM.DataSourceJobCode
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        Width = 45
        BandIndex = 0
        RowIndex = 0
        FieldName = 'JOB_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 131
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridLookupColumn
        Caption = 'Business unit'
        Width = 105
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BULU'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Bonus level'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_LEVEL'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Caption = 'Norm production level'
        Width = 113
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_PROD_LEVEL'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Caption = 'Norm machine output'
        Width = 109
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_OUTPUT_LEVEL'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Caption = 'Interface code'
        Width = 81
        BandIndex = 0
        RowIndex = 0
        FieldName = 'INTERFACE_CODE'
      end
      object dxDetailGridColumn8: TdxDBGridCheckColumn
        Caption = 'Ignore other interface codes'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IGNOREINTERFACECODE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn9: TdxDBGridDateColumn
        Caption = 'Date inactive'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
      end
      object dxDetailGridColumnDefaultYN: TdxDBGridCheckColumn
        Caption = 'Default'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEFAULT_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnSampleTimeMins: TdxDBGridColumn
        Caption = 'Sample time'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SAMPLE_TIME_MINS'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 744
  end
  inherited dsrcActive: TDataSource
    Left = 8
    Top = 80
  end
end
