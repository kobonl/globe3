(*
  Changes:
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
*)
unit DepartmentFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, DBCtrls, StdCtrls, Mask,
  dxEditor, dxEdLib, dxDBELib, Buttons, dxExEdtr, dxDBEdtr;

type
  TDepartmentF = class(TGridBaseF)
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnCity: TdxDBGridColumn;
    dxMasterGridColumnPhone: TdxDBGridColumn;
    dxMasterGridColumnState: TdxDBGridColumn;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnBusiness: TdxDBGridLookupColumn;
    dxDetailGridColumnDirectHour: TdxDBGridCheckColumn;
    dxDetailGridColumnPlanWorkspot: TdxDBGridCheckColumn;
    LabelCode: TLabel;
    LabelBusinessUnit: TLabel;
    DBEditCode: TdxDBEdit;
    DBEditDescription: TdxDBEdit;
    LabelRemarks: TLabel;
    DBEditRemarks: TdxDBEdit;
    DBCheckBoxDirectHours: TDBCheckBox;
    DBCheckBoxWorkSpot: TDBCheckBox;
    dxDetailGridColumnRemarks: TdxDBGridColumn;
    dxDBLookupEdit1: TdxDBLookupEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }

  end;
function DepartmentF: TDepartmentF;

implementation

uses
  DepartmentDMT, SystemDMT;
{$R *.DFM}
var
  DepartmentF_HND: TDepartmentF;

function DepartmentF: TDepartmentF;
begin
  if (DepartmentF_HND = nil) then
  begin
    DepartmentF_HND := TDepartmentF.Create(Application);
  end;
  Result := DepartmentF_HND;
end;

procedure TDepartmentF.FormCreate(Sender: TObject);
begin
  DepartmentDM := CreateFormDM(TDepartmentDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := DepartmentDM.DataSourceDetail;
    dxMasterGrid.DataSource := DepartmentDM.DataSourceMaster;
  end;
  inherited;
end;

procedure TDepartmentF.FormDestroy(Sender: TObject);
begin
  inherited;
  DepartmentF_HND:=NIL;
end;

procedure TDepartmentF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

procedure TDepartmentF.FormShow(Sender: TObject);
begin
  inherited;

  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      DepartmentDM.TableMaster.FieldByName('PLANT_CODE').AsString
  else
    DepartmentDM.TableMaster.FindKey([SystemDM.ASaveTimeRecScanning.Plant]);
end;

procedure TDepartmentF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  SystemDM.ASaveTimeRecScanning.Plant :=
    DepartmentDM.TableMaster.FieldByName('PLANT_CODE').AsString;
end;

end.
