inherited ErrorLogF: TErrorLogF
  Width = 671
  Height = 406
  Caption = 'Error Log'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 655
    Height = 167
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 162
      Width = 653
      Height = 4
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 39
      Width = 653
      Height = 123
      KeyField = 'COMPUTER_NAME'
      DataSource = ErrorLogDM.DataSourceMaster
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Workstation'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPUTER_NAME'
      end
    end
    object pnlMasterSelection: TPanel
      Left = 1
      Top = 1
      Width = 653
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 184
        Top = 8
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Priority'
      end
      object CmbPriority: TComboBox
        Left = 56
        Top = 6
        Width = 121
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'CmbPriority'
        Items.Strings = (
          'All'
          '1 - Message'
          '2 - Warning'
          '3 - Error'
          '4 - Stack Trace'
          '5 - Debug')
      end
      object btnRefresh: TButton
        Left = 424
        Top = 4
        Width = 75
        Height = 25
        Caption = 'Refresh'
        TabOrder = 1
        OnClick = btnRefreshClick
      end
      object DateEditFrom: TDateTimePicker
        Left = 232
        Top = 6
        Width = 185
        Height = 21
        CalAlignment = dtaLeft
        Date = 41824.4050508681
        Time = 41824.4050508681
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 2
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 280
    Width = 655
    Height = 87
    TabOrder = 3
    object GBoxMessage: TGroupBox
      Left = 1
      Top = 1
      Width = 653
      Height = 85
      Align = alClient
      Caption = 'Message'
      TabOrder = 0
      object dxDBMemo1: TdxDBMemo
        Left = 2
        Top = 15
        Width = 649
        Align = alClient
        TabOrder = 0
        DataField = 'LOGMESSAGE'
        DataSource = ErrorLogDM.DataSourceDetail
        Height = 68
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 193
    Width = 655
    Height = 87
    inherited spltDetail: TSplitter
      Top = 80
      Width = 653
      Height = 6
    end
    inherited dxDetailGrid: TdxDBGrid
      Width = 653
      Height = 79
      KeyField = 'ABSLOG_ID'
      DataSource = ErrorLogDM.DataSourceDetail
      object dxDetailGridColumnTimestamp: TdxDBGridColumn
        Caption = 'Timestamp'
        Width = 41
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSLOG_TIMESTAMP'
      end
      object dxDetailGridColumnSource: TdxDBGridColumn
        Caption = 'Source'
        Width = 73
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LOGSOURCE'
      end
      object dxDetailGridColumnUser: TdxDBGridColumn
        Caption = 'User'
        Width = 69
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SYSTEMUSER'
      end
      object dxDetailGridColumnPriority: TdxDBGridColumn
        Caption = 'Priority'
        Width = 48
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRIORITY'
      end
      object dxDetailGridColumnCalcMessage: TdxDBGridColumn
        Caption = 'Message'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CALCMESSAGE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
  end
end
