(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportAbsenceSheduleDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportAbsenceSheduleDM = class(TReportBaseDM)
    QueryAbsence: TQuery;
    DataSourceAbsence: TDataSource;
    TableEarnedWTR: TTable;
    TableAbsenceType: TTable;
    TableEmpl: TTable;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryAbsenceFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportAbsenceSheduleDM: TReportAbsenceSheduleDM;

implementation

{$R *.DFM}

uses
  SystemDMT;

procedure TReportAbsenceSheduleDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryAbsence);
end;

procedure TReportAbsenceSheduleDM.QueryAbsenceFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
