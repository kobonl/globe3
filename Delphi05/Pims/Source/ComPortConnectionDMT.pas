unit ComPortConnectionDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TComPortConnectionDM = class(TGridBaseDM)
    TableSerialDevice: TTable;
    DataSourceSerialDevice: TDataSource;
    TableSerialDeviceSERIALDEVICE_CODE: TStringField;
    TableSerialDeviceNAME: TStringField;
    TableSerialDeviceDEVICETYPE: TStringField;
    TableSerialDeviceBAUDRATE: TIntegerField;
    TableSerialDevicePARITY: TStringField;
    TableSerialDeviceDATABITS: TIntegerField;
    TableSerialDeviceSTOPBITS: TIntegerField;
    TableSerialDeviceINITSTRING: TStringField;
    TableSerialDeviceINITCHAR1: TIntegerField;
    TableSerialDeviceINITCHAR2: TIntegerField;
    TableSerialDeviceINITCHAR3: TIntegerField;
    TableSerialDeviceVERIFYSTRING: TStringField;
    TableSerialDeviceVERIFYPOSITION: TIntegerField;
    TableSerialDeviceENDCHARACTER1: TIntegerField;
    TableSerialDeviceENDCHARACTER2: TIntegerField;
    TableSerialDeviceCREATIONDATE: TDateTimeField;
    TableSerialDeviceMUTATIONDATE: TDateTimeField;
    TableSerialDeviceMUTATOR: TStringField;
    TableDetailCOMPUTER_NAME: TStringField;
    TableDetailCOMPORT: TIntegerField;
    TableDetailSERIALDEVICE_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterCOMPUTER_NAME: TStringField;
    TableMasterLICENSEVERSION: TIntegerField;
    TableDetailSERIALDEVICE_DESCRIPTIONLU: TStringField;
    TableSerialDeviceVALUEPOSITION: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ComPortConnectionDM: TComPortConnectionDM;

implementation

{$R *.DFM}

uses
  SystemDMT;

procedure TComPortConnectionDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableDetail.MasterSource := DataSourceMaster;
  TableDetail.MasterFields := 'COMPUTER_NAME';
  TableDetail.IndexFieldNames := 'COMPUTER_NAME;COMPORT';
end;

procedure TComPortConnectionDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetailCOMPORT.Value := 1;
end;

end.
