inherited ErrorLogDM: TErrorLogDM
  OldCreateOrder = True
  Left = 534
  Top = 298
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    TableName = 'WORKSTATION'
    Left = 36
    Top = 16
    object TableMasterCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 128
    end
  end
  inherited TableDetail: TTable
    Left = 36
    Top = 68
  end
  inherited DataSourceMaster: TDataSource
    Left = 160
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    DataSet = qryABSLOG
    Left = 160
    Top = 68
  end
  object qryABSLOG: TQuery
    OnCalcFields = qryABSLOGCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.ABSLOG_ID,'
      '  A.COMPUTER_NAME,'
      '  A.ABSLOG_TIMESTAMP,'
      '  A.LOGSOURCE,'
      '  A.PRIORITY,'
      '  A.SYSTEMUSER,'
      '  A.LOGMESSAGE'
      'FROM'
      '  ABSLOG A'
      'WHERE'
      '  A.COMPUTER_NAME = :COMPUTER_NAME AND'
      '  (A.PRIORITY = :PRIORITY OR :PRIORITY <= 0) AND'
      '  A.ABSLOG_TIMESTAMP >= :DATEFROM AND'
      '  A.ABSLOG_TIMESTAMP < :DATETO'
      'ORDER BY'
      '  A.PRIORITY,'
      '  A.ABSLOG_ID'
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 40
    Top = 136
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PRIORITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PRIORITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
    object qryABSLOGABSLOG_ID: TIntegerField
      FieldName = 'ABSLOG_ID'
      Origin = 'PIMS.ABSLOG.ABSLOG_ID'
    end
    object qryABSLOGCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Origin = 'PIMS.ABSLOG.COMPUTER_NAME'
      Size = 128
    end
    object qryABSLOGABSLOG_TIMESTAMP: TDateTimeField
      FieldName = 'ABSLOG_TIMESTAMP'
      Origin = 'PIMS.ABSLOG.ABSLOG_TIMESTAMP'
    end
    object qryABSLOGLOGSOURCE: TStringField
      FieldName = 'LOGSOURCE'
      Origin = 'PIMS.ABSLOG.LOGSOURCE'
      Size = 120
    end
    object qryABSLOGPRIORITY: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PRIORITY'
      Origin = 'PIMS.ABSLOG.PRIORITY'
    end
    object qryABSLOGSYSTEMUSER: TStringField
      FieldName = 'SYSTEMUSER'
      Origin = 'PIMS.ABSLOG.SYSTEMUSER'
      Size = 128
    end
    object qryABSLOGLOGMESSAGE: TMemoField
      FieldName = 'LOGMESSAGE'
      Origin = 'PIMS.ABSLOG.LOGMESSAGE'
      BlobType = ftMemo
      Size = 1020
    end
    object qryABSLOGCALCMESSAGE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALCMESSAGE'
      Size = 80
      Calculated = True
    end
  end
end
