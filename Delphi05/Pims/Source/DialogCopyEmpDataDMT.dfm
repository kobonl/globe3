object DialogCopyEmpDataDM: TDialogCopyEmpDataDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 408
  Top = 222
  Height = 430
  Width = 901
  object qryCopyEmpContract: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEECONTRACT'
      '('
      '  EMPLOYEE_NUMBER,'
      '  STARTDATE,'
      '  CREATIONDATE,'
      '  ENDDATE,'
      '  HOURLY_WAGE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  CONTRACT_TYPE,'
      '  HOLIDAY_HOUR_PER_YEAR,'
      '  CONTRACT_HOUR_PER_WEEK,'
      '  CONTRACT_DAY_PER_WEEK,'
      '  GUARANTEED_DAYS,'
      '  EXPORT_CODE,'
      '  SENIORITY_HOURS,'
      '  WORKER_YN,'
      '  WORKSCHEDULE_ID'
      ')'
      'SELECT'
      '  :EMPLOYEE_NUMBER,'
      '  :STARTDATE,'
      '  SYSDATE,'
      '  TO_DATE('#39'31-12-2099'#39', '#39'dd-mm-yyyy'#39'),'
      '  HOURLY_WAGE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  CONTRACT_TYPE,'
      '  HOLIDAY_HOUR_PER_YEAR,'
      '  CONTRACT_HOUR_PER_WEEK,'
      '  CONTRACT_DAY_PER_WEEK,'
      '  GUARANTEED_DAYS,'
      '  EXPORT_CODE,'
      '  SENIORITY_HOURS,'
      '  WORKER_YN,'
      '  WORKSCHEDULE_ID'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE'
      '  EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  STARTDATE IN (SELECT MAX(STARTDATE) FROM EMPLOYEECONTRACT WHER' +
        'E EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER) AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEECONTRACT WHERE' +
        ' EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)')
    Left = 280
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyWSPerEmp: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WORKSPOTSPEREMPLOYEE'
      '('
      '  EMPLOYEE_NUMBER,'
      '  PLANT_CODE,'
      '  EMPLOYEE_LEVEL,'
      '  DEPARTMENT_CODE,'
      '  CREATIONDATE,'
      '  WORKSPOT_CODE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  STARTDATE_LEVEL'
      ')'
      'SELECT '
      '  :EMPLOYEE_NUMBER,'
      '  PLANT_CODE,'
      '  EMPLOYEE_LEVEL,'
      '  DEPARTMENT_CODE,'
      '  SYSDATE,'
      '  WORKSPOT_CODE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  :STARTDATE'
      'FROM '
      '  WORKSPOTSPEREMPLOYEE'
      'WHERE '
      '  EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM WORKSPOTSPEREMPLOYEE W' +
        'HERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)   ')
    Left = 384
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyStandardAvail: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO STANDARDAVAILABILITY'
      '('
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  :EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM STANDARDAVAILABILITY'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM STANDARDAVAILABILITY WHE' +
        'RE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)'
      ' '
      ' ')
    Left = 488
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyShiftSchedule: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTSCHEDULE'
      '('
      '  EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT '
      '  :EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM SHIFTSCHEDULE'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'SHIFT_SCHEDULE_DATE >= :DATEFROM AND SHIFT_SCHEDULE_DATE <= :DAT' +
        'ETO AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM SHIFTSCHEDULE WHERE EMPL' +
        'OYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  SHIFT_SCHEDULE_DATE >= :DATEFROM AND SHIFT_SCHEDULE_DATE <= :D' +
        'ATETO)')
    Left = 600
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpAvail: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEAVAILABILITY'
      '('
      '  PLANT_CODE,'
      '  EMPLOYEEAVAILABILITY_DATE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  SAVE_AVAILABLE_TIMEBLOCK_1,'
      '  SAVE_AVAILABLE_TIMEBLOCK_2,'
      '  SAVE_AVAILABLE_TIMEBLOCK_3,'
      '  SAVE_AVAILABLE_TIMEBLOCK_4,'
      '  SAVE_AVAILABLE_TIMEBLOCK_5,'
      '  SAVE_AVAILABLE_TIMEBLOCK_6,'
      '  SAVE_AVAILABLE_TIMEBLOCK_7,'
      '  SAVE_AVAILABLE_TIMEBLOCK_8,'
      '  SAVE_AVAILABLE_TIMEBLOCK_9,'
      '  SAVE_AVAILABLE_TIMEBLOCK_10,'
      '  EXPORTED_YN'
      ')'
      'SELECT'
      '  EA.PLANT_CODE,'
      '  EA.EMPLOYEEAVAILABILITY_DATE,'
      '  EA.SHIFT_NUMBER,'
      '  :EMPLOYEE_NUMBER,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_1 NOT IN ('#39'*'#39','#39'-'#39') THEN '
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_1 '
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_1 '
      '  END AVAILABLE_TIMEBLOCK_1,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_2 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_2 '
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_2 '
      '  END AVAILABLE_TIMEBLOCK_2,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_3 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_3'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_3'
      '  END AVAILABLE_TIMEBLOCK_3,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_4 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_4'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_4'
      '  END AVAILABLE_TIMEBLOCK_4,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_5 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_5'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_5 '
      '  END AVAILABLE_TIMEBLOCK_5,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_6 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_6'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_6'
      '  END AVAILABLE_TIMEBLOCK_6,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_7 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_7'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_7 '
      '  END AVAILABLE_TIMEBLOCK_7,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_8 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_8'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_8 '
      '  END AVAILABLE_TIMEBLOCK_8,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_9 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_9'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_9 '
      '  END AVAILABLE_TIMEBLOCK_9,'
      '  CASE '
      '    WHEN EA.AVAILABLE_TIMEBLOCK_10 NOT IN ('#39'*'#39','#39'-'#39') THEN'
      '      (SELECT SA.AVAILABLE_TIMEBLOCK_10'
      '       FROM STANDARDAVAILABILITY SA '
      '       WHERE SA.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '       SA.SHIFT_NUMBER = EA.SHIFT_NUMBER AND'
      
        '       SA.DAY_OF_WEEK = PIMSDAYOFWEEK(EA.EMPLOYEEAVAILABILITY_DA' +
        'TE))'
      '    ELSE EA.AVAILABLE_TIMEBLOCK_10'
      '  END AVAILABLE_TIMEBLOCK_10,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_1,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_2,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_3,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_4,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_5,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_6,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_7,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_8,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_9,'
      '  EA.SAVE_AVAILABLE_TIMEBLOCK_10,'
      '  '#39'N'#39
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE EA.EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND EA.EMPLOYEEAVAIL' +
        'ABILITY_DATE <= :DATETO'
      '  AND'
      
        '    NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEEAVAILABILITY' +
        ' WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '      EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND EMPLOYEEAVAILAB' +
        'ILITY_DATE <= :DATETO)'
      '')
    Left = 72
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpPlan: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEPLANNING'
      '('
      '  PLANT_CODE,'
      '  EMPLOYEEPLANNING_DATE,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5,'
      '  SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7,'
      '  SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9,'
      '  SCHEDULED_TIMEBLOCK_10,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  EMPLOYEEPLANNING_DATE,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  :EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5,'
      '  SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7,'
      '  SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9,'
      '  SCHEDULED_TIMEBLOCK_10,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM EMPLOYEEPLANNING'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'EMPLOYEEPLANNING_DATE >= :DATEFROM AND EMPLOYEEPLANNING_DATE <= ' +
        ':DATETO AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEEPLANNING WHERE E' +
        'MPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  EMPLOYEEPLANNING_DATE >= :DATEFROM AND EMPLOYEEPLANNING_DATE <' +
        '= :DATETO)'
      ' '
      ' ')
    Left = 280
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryCopyThis: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 72
    Top = 16
  end
  object qryEmpContWorkSched: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  WORKSCHEDULE_ID'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 384
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEE'
      'SET PLANT_CODE = :PLANT_CODE,'
      '  DEPARTMENT_CODE = :DEPARTMENT_CODE,'
      '  CREATIONDATE = SYSDATE,'
      '  MUTATIONDATE = SYSDATE,'
      '  MUTATOR = :MUTATOR,'
      '  LANGUAGE_CODE = :LANGUAGE_CODE,'
      '  TEAM_CODE = :TEAM_CODE,'
      '  CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE,'
      '  IS_SCANNING_YN = :IS_SCANNING_YN,'
      '  CUT_OF_TIME_YN = :CUT_OF_TIME_YN,'
      '  CALC_BONUS_YN = :CALC_BONUS_YN,'
      '  FIRSTAID_YN = '#39'N'#39','
      '  GRADE = :GRADE,'
      '  SHIFT_NUMBER = :SHIFT_NUMBER,'
      '  BOOK_PROD_HRS_YN = :BOOK_PROD_HRS_YN,'
      '  FAKE_EMPLOYEE_YN = '#39'N'#39
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 176
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'LANGUAGE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IS_SCANNING_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CUT_OF_TIME_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CALC_BONUS_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'GRADE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BOOK_PROD_HRS_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  DEPARTMENT_CODE,'
      '  LANGUAGE_CODE,'
      '  TEAM_CODE,'
      '  CONTRACTGROUP_CODE,'
      '  IS_SCANNING_YN,'
      '  CUT_OF_TIME_YN,'
      '  CALC_BONUS_YN,'
      '  GRADE,'
      '  SHIFT_NUMBER,'
      '  BOOK_PROD_HRS_YN'
      'FROM EMPLOYEE'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER  ')
    Left = 72
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpStdPlan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO STANDARDEMPLOYEEPLANNING'
      '('
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5,'
      '  SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7,'
      '  SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9,'
      '  SCHEDULED_TIMEBLOCK_10,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  :EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5,'
      '  SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7,'
      '  SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9,'
      '  SCHEDULED_TIMEBLOCK_10,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM STANDARDEMPLOYEEPLANNING'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM STANDARDEMPLOYEEPLANNING' +
        ' WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)'
      ' ')
    Left = 176
    Top = 152
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyTBPEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO TIMEBLOCKPEREMPLOYEE'
      '('
      '  PLANT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  SHIFT_NUMBER,'
      '  CREATIONDATE,'
      '  TIMEBLOCK_NUMBER,'
      '  MUTATIONDATE,'
      '  DESCRIPTION,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  MUTATOR,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  :EMPLOYEE_NUMBER,'
      '  SHIFT_NUMBER,'
      '  SYSDATE,'
      '  TIMEBLOCK_NUMBER,'
      '  SYSDATE,'
      '  DESCRIPTION,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  :MUTATOR,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  TIMEBLOCKPEREMPLOYEE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM TIMEBLOCKPEREMPLOYEE W' +
        'HERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)'
      ''
      ''
      ' '
      ' ')
    Left = 72
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
