(*
  MRA:14-JUL-2011 RV089.1./RV094.10. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:18-JUL-2011. RV095.2. SO-20011797
  - Moved Workspot to DialogReportBase-form.
*)
unit DialogMonthlyEmployeeEfficiencyFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls, dxEdLib;

type
  TDialogMonthlyEmployeeEfficiencyF = class(TDialogReportBaseF)
    Label10: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label16: TLabel;
    Label15: TLabel;
    Label22: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditMonth: TdxSpinEdit;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FMonth: Integer;
    FYear: Integer;
    FPlantFrom: String;
    FDeptFrom: String;
    FWorkspotFrom: String;
    FPlantTo: String;
    FWorkspotTo: String;
    FDeptTo: String;
    procedure FillDept;
{    procedure FillWorkSpot; }
    procedure CreateEfficiencyRecords;
  public
    { Public declarations }
    property Year: Integer read FYear write FYear;
    property Month: Integer read FMonth write FMonth;
    property PlantFrom: String read FPlantFrom write FPlantFrom;
    property PlantTo: String read FPlantTo write FPlantTo;
    property DeptFrom: String read FDeptFrom write FDeptFrom;
    property DeptTo: String read FDeptTo write FDeptTo;
    property WorkspotFrom: String read FWorkspotFrom write FWorkspotFrom;
    property WorkspotTo: String read FWorkspotTo write FWorkspotTo;
  end;

var
  DialogMonthlyEmployeeEfficiencyF: TDialogMonthlyEmployeeEfficiencyF;

// RV089.1.
function DialogMonthlyEmployeeEfficiencyForm: TDialogMonthlyEmployeeEfficiencyF;

implementation

{$R *.DFM}

uses
  ReportMonthlyEmployeeEfficiencyQRPT, ListProcsFRM, UPimsConst,
  DialogMonthlyEmployeeEfficiencyDMT,
  ReportProductionDetailDMT, // This is used for procedures to calc Emp.Eff.
  SystemDMT,
  DialogMonthlyGroupEfficiencyDMT; // THis is used to calc. Group.Eff.

// RV089.1.
var
  DialogMonthlyEmployeeEfficiencyF_HND: TDialogMonthlyEmployeeEfficiencyF;

// RV089.1.
function DialogMonthlyEmployeeEfficiencyForm: TDialogMonthlyEmployeeEfficiencyF;
begin
  if (DialogMonthlyEmployeeEfficiencyF_HND = nil) then
  begin
    DialogMonthlyEmployeeEfficiencyF_HND := TDialogMonthlyEmployeeEfficiencyF.Create(Application);
    with DialogMonthlyEmployeeEfficiencyF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogMonthlyEmployeeEfficiencyF_HND;
end;

// RV089.1.
procedure TDialogMonthlyEmployeeEfficiencyF.FormDestroy(Sender: TObject);
begin
  inherited;
  DialogMonthlyEmployeeEfficiencyDM.Free;
  DialogMonthlyGroupEfficiencyDM.Free;
//  ReportProductionDetailDM.Free;
  if (DialogMonthlyEmployeeEfficiencyF_HND <> nil) then
  begin
    DialogMonthlyEmployeeEfficiencyF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogMonthlyEmployeeEfficiencyF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

// MR:29-01-2007 Order 550438 Employee Efficiency.

procedure TDialogMonthlyEmployeeEfficiencyF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogMonthlyEmployeeEfficiencyDM :=
    TDialogMonthlyEmployeeEfficiencyDM.Create(nil);
//  ReportProductionDetailDM := TReportProductionDetailDM.Create(nil);
  ReportProductionDetailDM.Init; // 20015220 + 20015221
  DialogMonthlyGroupEfficiencyDM :=
    TDialogMonthlyGroupEfficiencyDM.Create(nil);
  ReportMonthlyEmployeeEfficiencyQR :=
    CreateReportQR(TReportMonthlyEmployeeEfficiencyQR);
    
  // MR:13-03-2007 Order 550443 Use extra filter in selects.
  ReportProductionDetailDM.GroupEmployeeEfficiency := True;
  DialogMonthlyGroupEfficiencyDM.GroupEfficiency := True;
end;

procedure TDialogMonthlyEmployeeEfficiencyF.FormShow(Sender: TObject);
var
  AYear, AMonth, ADay: Word;
begin
  InitDialog(True, False, False, False, False, True, False);
  inherited;
  FillDept;
{  FillWorkSpot; }
  DecodeDate(Now, AYear, AMonth, ADay);
  dxSpinEditYear.IntValue := AYear;
  dxSpinEditMonth.IntValue := AMonth;
end;

procedure TDialogMonthlyEmployeeEfficiencyF.FillDept;
begin
{
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'DEPARTMENT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptTo, GetStrValue(CmbPlusPlantFrom.Value),
      'DEPARTMENT_CODE', False);
    ComboBoxPlusDeptFrom.Visible := True;
    ComboBoxPlusDeptTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusDeptFrom.Visible := False;
    ComboBoxPlusDeptTo.Visible := False;
  end;
}  
end;
{
procedure TDialogMonthlyEmployeeEfficiencyF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
procedure TDialogMonthlyEmployeeEfficiencyF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue; }
end;

procedure TDialogMonthlyEmployeeEfficiencyF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue; }
end;

procedure TDialogMonthlyEmployeeEfficiencyF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillDept;
{  FillWorkspot; }
end;

procedure TDialogMonthlyEmployeeEfficiencyF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillDept;
{  FillWorkspot; }
end;

procedure TDialogMonthlyEmployeeEfficiencyF.CreateEfficiencyRecords;
begin
  try
    Screen.Cursor := crHourGlass;
    DialogMonthlyGroupEfficiencyDM.DetermineGroupEfficiency(Year, Month,
      PlantFrom, PlantTo, DeptFrom, DeptTo, WorkspotFrom, WorkspotTo);
    DialogMonthlyEmployeeEfficiencyDM.DetermineEmployeeEfficiency(Year, Month,
      PlantFrom, PlantTo, DeptFrom, DeptTo, WorkspotFrom, WorkspotTo,
      ProgressBar1);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDialogMonthlyEmployeeEfficiencyF.btnOkClick(Sender: TObject);
begin
  inherited;
  // MR:23-01-2007 Department-selection is not used.
  Year := Round(dxSpinEditYear.Value);
  Month := Round(dxSpinEditMonth.Value);
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);
  DeptFrom := GetStrValue(CmbPlusDepartmentFrom.Value);
  DeptTo := GetStrValue(CmbPlusDepartmentTo.Value);
  WorkspotFrom := GetStrValue(CmbPlusWorkspotFrom.Value);
  WorkspotTo := GetStrValue(CmbPlusWorkspotTo.Value);
  // First select and store all values (for MonthlyGroupEfficiency).
  try
    SystemDM.Pims.StartTransaction;
    CreateEfficiencyRecords;
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    on E: EDBEngineError do
    begin
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      DisplayMessage(GetErrorMessage(E, PIMS_POSTING), mtError, [mbOK]);
    end;
  end;
  // Now show report (only contents of MonthlyEmployeeEfficiency).
  if ReportMonthlyEmployeeEfficiencyQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    Round(dxSpinEditYear.Value),
    Round(dxSpinEditMonth.Value))
  then
    ReportMonthlyEmployeeEfficiencyQR.ProcessRecords;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogMonthlyEmployeeEfficiencyF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogMonthlyEmployeeEfficiencyF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
