(*
  Changes:
    MRA:19-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    - Changed so it is inherited from TDialogReportBaseF.
  MRA:4-FEB-2010. RV053.2.
  - When this dialog is started from EmployeeRegistrations, is must
    only show 1 employee and 1 department.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
*)
unit DialogCalculatedEarnedWTRFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables,
  dxLayout, dxExGrEd, dxExELib, DialogReportBaseFRM;

type
  TDialogCalculateEarnedWTRF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    TableEarned: TTable;
    TableAbsenceTot: TTable;
    TableEmplContract: TTable;
    QuerySel: TQuery;
    QueryEmpl: TQuery;
    TableAbsenceTotEMPLOYEE_NUMBER: TIntegerField;
    TableAbsenceTotABSENCE_YEAR: TIntegerField;
    TableAbsenceTotLAST_YEAR_HOLIDAY_MINUTE: TFloatField;
    TableAbsenceTotNORM_HOLIDAY_MINUTE: TFloatField;
    TableAbsenceTotUSED_HOLIDAY_MINUTE: TFloatField;
    TableAbsenceTotLAST_YEAR_TFT_MINUTE: TFloatField;
    TableAbsenceTotEARNED_TFT_MINUTE: TFloatField;
    TableAbsenceTotUSED_TFT_MINUTE: TFloatField;
    TableAbsenceTotLAST_YEAR_WTR_MINUTE: TFloatField;
    TableAbsenceTotEARNED_WTR_MINUTE: TFloatField;
    TableAbsenceTotCREATIONDATE: TDateTimeField;
    TableAbsenceTotUSED_WTR_MINUTE: TFloatField;
    TableAbsenceTotMUTATIONDATE: TDateTimeField;
    TableAbsenceTotMUTATOR: TStringField;
    TableAbsenceTotILLNESS_MINUTE: TFloatField;
    TableEarnedEARNED_WTR_YEAR: TIntegerField;
    TableEarnedEARNED_WTR_WEEK: TIntegerField;
    TableEarnedEMPLOYEE_NUMBER: TIntegerField;
    TableEarnedCONTRACT_MINUTES: TIntegerField;
    TableEarnedEARNED_WTR_MINUTES: TIntegerField;
    TableEarnedCREATIONDATE: TDateTimeField;
    TableEarnedMUTATIONDATE: TDateTimeField;
    TableEarnedMUTATOR: TStringField;
    Label14: TLabel;
    Label15: TLabel;
    Label25: TLabel;
    Label16: TLabel;
    Label27: TLabel;
    Label22: TLabel;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure TableAbsenceTotNewRecord(DataSet: TDataSet);
    procedure TableEarnedNewRecord(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    FMyWeek: Integer;
    FMyYear: Integer;
    FMyDepartment: String;
    FMyPlant: String;
    FMyEmployee: String;
    { Private declarations }
  public
    { Public declarations }
    FDateMin, FDateMax: TDateTime;
    property MyPlant: String read FMyPlant write FMyPlant;
    property MyDepartment: String read FMyDepartment write FMyDepartment;
    property MyEmployee: String read FMyEmployee write FMyEmployee;
    property MyYear: Integer read FMyYear write FMyYear;
    property MyWeek: Integer read FMyWeek write FMyWeek;
  end;

var
  DialogCalculateEarnedWTRF: TDialogCalculateEarnedWTRF;

// RV089.1.
function DialogCalculateEarnedWTRForm: TDialogCalculateEarnedWTRF;

implementation

uses ListProcsFRM, SystemDMT, UPimsConst, UPimsMessageRes;

{$R *.DFM}

// RV089.1.
var
  DialogCalculateEarnedWTRF_HND: TDialogCalculateEarnedWTRF;

// RV089.1.
function DialogCalculateEarnedWTRForm: TDialogCalculateEarnedWTRF;
begin
  if (DialogCalculateEarnedWTRF_HND = nil) then
  begin
    DialogCalculateEarnedWTRF_HND := TDialogCalculateEarnedWTRF.Create(Application);
    with DialogCalculateEarnedWTRF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogCalculateEarnedWTRF_HND;
end;

// RV089.1.
procedure TDialogCalculateEarnedWTRF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogCalculateEarnedWTRF_HND <> nil) then
  begin
    DialogCalculateEarnedWTRF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogCalculateEarnedWTRF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
//  TableContr.Close;
  TableEarned.Close;
  TableAbsenceTot.Close;
  TableEmplContract.Close;
  Action := caFree;
end;

procedure TDialogCalculateEarnedWTRF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, False, True, False, False, False);
  // RV053.2.
  if MyPlant <> '' then
    SearchPlant := MyPlant;
  inherited;
  Update;
  Screen.Cursor := crHourGlass;

  CmbPlusPlantFromCloseUp(Sender);

  // MR:06-01-2004 Get year through weeknumber
//  DecodeDate(Now, Year, Month, Day);
  ListProcsF.WeekUitDat(Now, Year, Week);

  dxSpinEditYear.Value := Year;
  dxSpinEditWeek.Value := ListProcsF.WeekOfYear(Now);
  Update;
  Screen.Cursor := crDefault;
  // MR:09-12-2003
  // RV053.2.
  if MyDepartment <> '' then
    SearchDepartment := MyDepartment;
  // RV053.2.
  if MyEmployee <> '' then
    SearchEmployee := MyEmployee;
  if (MyYear <> 0) and (MyWeek <> 0) then
  begin
    dxSpinEditYear.Value := MyYear;
    dxSpinEditWeek.Value := MyWeek;
  end;
end;

procedure TDialogCalculateEarnedWTRF.btnOkClick(Sender: TObject);
var
  PlantFrom, PlantTo, DeptFrom, DeptTo, SelectStr: String;
  EmplFrom, EmplTo, Empl, Year, Week: Integer;
  OldEarnedMin, EarnedMin, WorkMin, AbsenceMin, ContractMin: Integer;
  ProcessedRecCount: Integer;
  procedure UpdateValues(TempTable: TTable);
  begin
    TempTable.FieldByName('CREATIONDATE').Value := Now;
    TempTable.FieldByName('MUTATIONDATE').Value := Now;
    TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
  end;
  procedure Contract_Time(Empl: Integer; var ContractMin: Integer);
  var
    ContractHourPerWeek: Double;
  begin
    ContractMin := 0;
    TableEmplContract.First;
    TableEmplContract.FindNearest([Empl]);
    while not TableEmplContract.Eof and
     (TableEmplContract.FieldByNAME('EMPLOYEE_NUMBER').AsInteger = Empl) do
    begin
      if (TableEmplContract.FieldByNAME('STARTDATE').AsDateTime <= FDateMax) and
        (TableEmplContract.FieldByNAME('ENDDATE').AsDateTime >= FDateMax) then
      begin
        ContractHourPerWeek :=
          TableEmplContract.FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat;
      // MR:22-11-2004 CONTRACT_HOUR_PER_WEEK is a double!
//      ContractMin := Int(ContractMin) * 60;
        ContractMin := Round(ContractHourPerWeek * 60);
     // Exit;
      end;
      TableEmplContract.Next;
    end;
  end;
  procedure SetQuery(SelectStr: String; Empl: Integer);
  begin
    QuerySel.Active := False;
    QuerySel.UniDirectional := False;
    QuerySel.SQL.Clear;
    QuerySel.SQL.Add(UpperCase(SelectStr));
    QuerySel.ParamByName('FSTARTDATE').Value := FDateMin;
    QuerySel.ParamByName('FENDDATE').Value := FDateMax;
    QuerySel.ParamByName('EMPLOYEE_NUMBER').Value := Empl;
    if not QuerySel.Prepared then
      QuerySel.Prepare;
    QuerySel.Active := True;
  end;
  procedure Worked_Time(Empl: Integer; var WorkMin: Integer);
  var
    SelectStr: String;
  begin
    SelectStr :=
      'SELECT ' +
      '  S.EMPLOYEE_NUMBER, SUM(S.SALARY_MINUTE) AS SUMMIN  ' +
      'FROM ' +
      '  SALARYHOURPEREMPLOYEE S ' +
      'WHERE ' +
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
      '  AND S.SALARY_DATE >= :FSTARTDATE AND S.SALARY_DATE <= :FENDDATE AND ' +
      '  S.HOURTYPE_NUMBER = 1 ' +
      'GROUP BY ' +
      '  S.EMPLOYEE_NUMBER ';
    SetQuery(SelectStr, Empl);
    WorkMin := 0;
    if (QuerySel.RecordCount = 1) then
      WorkMin := Round(QuerySel.FieldByName('SUMMIN').AsFloat);
  end;
  procedure Absence_Time(Empl: Integer; var AbsenceMin: Integer);
  var
    SelectStr: String;
  begin
    SelectStr :=
      'SELECT ' +
      '  A.EMPLOYEE_NUMBER, SUM(A.ABSENCE_MINUTE) AS SUMMIN ' +
      'FROM ' +
      '  ABSENCEHOURPEREMPLOYEE A ' +
      'WHERE ' +
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE AND ' +
      '  A.ABSENCEHOUR_DATE <= :FENDDATE ' +
      'GROUP BY ' +
      '  A.EMPLOYEE_NUMBER ';
    SetQuery(SelectStr, Empl);
    AbsenceMin := 0;
    if (QuerySel.RecordCount = 1) then
      AbsenceMin := Round(QuerySel.FieldByName('SUMMIN').AsFloat);
  end;
begin
  inherited;
  Screen.Cursor := crHourGlass;
  ProcessedRecCount := 0;
  Year := Round(dxSpinEditYear.Value);
  Week := Round(dxSpinEditWeek.Value);
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);
  if PlantFrom = PlantTo then
  begin
    DeptFrom := GetStrValue(CmbPlusDepartmentFrom.Value);
    DeptTo := GetStrValue(CmbPlusDepartmentTo.Value);
  end
  else
  begin
    DeptFrom := '1';
    DeptTo := 'zzzzzzzz';
  end;
  EmplFrom := 0;
  EmplTo := 0;
  if PlantFrom = PlantTo then
  begin
    if dxDBExtLookupEditEmplFrom.Text <> '' then
      EmplFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
    if dxDBExtLookupEditEmplTo.Text <> '' then
      EmplTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
  end
  else
  begin
    EmplFrom := 1;
    EmplTo := 999999999;
  end;
  FDateMin := ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
    Round(dxSpinEditWeek.Value), 1);
  FDateMax := ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
    Round(dxSpinEditWeek.Value), 7);

  SelectStr :=
    'SELECT ' +
    '  E.EMPLOYEE_NUMBER ' +
    'FROM ' +
    '  EMPLOYEE E, CONTRACTGROUP C ';
  //CAR 550274
{  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
     SelectStr := SelectStr + ', TEAMPERUSER T '; }
   // RV050.8.
   SelectStr := SelectStr +
    'WHERE ' +
    ' ( ' +
    '  (:USER_NAME = ''*'') OR ' +
    '  (E.TEAM_CODE IN ' +
    '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
    ') AND ' +
    '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND '+
    '  C.WORK_TIME_REDUCTION_YN =  ''' +  CHECKEDVALUE + '''';
  if (PlantFrom <> PlantTo) then
    SelectStr := SelectStr + ' AND E.PLANT_CODE >= ''' +
      DoubleQuote(PlantFrom) + '''' +
    ' AND E.PLANT_CODE <= ''' + DoubleQuote(PlantTo) + ''''
  else
    SelectStr := SelectStr +
      ' AND E.PLANT_CODE = ''' + DoubleQuote(PlantFrom) + '''' +
      ' AND E.DEPARTMENT_CODE >= ''' + DoubleQuote(DeptFrom) + '''' +
      ' AND E.DEPARTMENT_CODE <= ''' + DoubleQuote(DeptTo) + '''' +
      ' AND E.EMPLOYEE_NUMBER >= ' + IntToStr(EmplFrom) +
      ' AND E.EMPLOYEE_NUMBER <= ' + IntToStr(EmplTo);
  //CAR 550274
{  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
     SelectStr := SelectStr + 'AND T.USER_NAME = :USER_NAME AND '+
       ' T.TEAM_CODE = E.TEAM_CODE '; }
  QueryEmpl.Active := False;
  QueryEmpl.UniDirectional := False;
  QueryEmpl.SQL.Clear;
  QueryEmpl.SQL.Add(UpperCase(SelectStr));
  //CAR 550274
{  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.LoginUser; }
  // RV050.8.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmpl.ParamByName('USER_NAME').AsString := '*';
  if not QueryEmpl.Prepared then
    QueryEmpl.Prepare;
  QueryEmpl.Active := True;
  while not QueryEmpl.Eof do
  begin
    Empl := QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    Contract_Time(Empl, ContractMin);
    Worked_Time(Empl, WorkMin);
    Absence_Time(Empl, AbsenceMin);
    EarnedMin := WorkMin + AbsenceMin - ContractMin;
    OldEarnedMin := 0;
    // First update 'TableEarned'
    if not TableEarned.FindKey([Year, Week, Empl]) then
    begin
      TableEarned.Insert;
      TableEarned.FieldByName('EMPLOYEE_NUMBER').Value := Empl;
      TableEarned.FieldByName('EARNED_WTR_YEAR').Value :=
        dxSpinEditYear.Value;
      TableEarned.FieldByName('EARNED_WTR_WEEK').Value :=
        dxSpinEditWeek.Value;
      TableEarned.FieldByName('EARNED_WTR_MINUTES').Value := EarnedMin;
      TableEarned.FieldByName('CONTRACT_MINUTES').Value := ContractMin;
      TableEarned.Post;
      inc(ProcessedRecCount);
    end
    else
    begin
      TableEarned.Edit;
      if TableEarned.FieldByName('EARNED_WTR_MINUTES').AsString <> '' then
        OldEarnedMin := TableEarned.FieldByName('EARNED_WTR_MINUTES').Value
      else
        OldEarnedMin := 0;
      TableEarned.FieldByName('EARNED_WTR_MINUTES').Value := EarnedMin;
      TableEarned.FieldByName('CONTRACT_MINUTES').Value := ContractMin;
      TableEarned.FieldByName('MUTATIONDATE').Value := Now;
      TableEarned.FieldByName('MUTATOR').Value :=
        SystemDM.CurrentProgramUser;
      TableEarned.Post;
      inc(ProcessedRecCount);
    end;
    // Now update TotalAbsence-table
    if not TableAbsenceTot.FindKey([Empl, Year]) then
    begin
      TableAbsenceTot.Insert;
      TableAbsenceTot.FieldByName('EARNED_WTR_MINUTE').Value :=
        EarnedMin - OldEarnedMin;
      TableAbsenceTot.FieldByName('EMPLOYEE_NUMBER').Value := Empl;
      TableAbsenceTot.FieldByName('ABSENCE_YEAR').Value := Year;
      TableAbsenceTot.Post;
      inc(ProcessedRecCount);
    end
    else
    begin
      TableAbsenceTot.Edit;
      // MR:8-12-2004
      // If this field is a null-value then initialise it with 0!
      if TableAbsenceTot.FieldByName('EARNED_WTR_MINUTE').AsString = '' then
        TableAbsenceTot.FieldByName('EARNED_WTR_MINUTE').Value := 0;
      TableAbsenceTot.FieldByName('EARNED_WTR_MINUTE').Value :=
        TableAbsenceTot.FieldByName('EARNED_WTR_MINUTE').Value +
          EarnedMin - OldEarnedMin;
      TableAbsenceTot.FieldByName('MUTATIONDATE').Value := Now;
      TableAbsenceTot.FieldByName('MUTATOR').Value :=
        SystemDM.CurrentProgramUser;
      TableAbsenceTot.Post;
      inc(ProcessedRecCount);
    end;
    QueryEmpl.Next;
  end;{while}
  Screen.Cursor := crDefault;
  if ProcessedRecCount = 0 then
    DisplayMessage(SSProcessFinished + #13 + #13 +
      SPimsNotFound, mtInformation, [mbOK])
  else
    DisplayMessage(SSProcessFinished + #13 + #13 +
      SPimsNrOfRecsProcessed + IntToStr(ProcessedRecCount),
      mtInformation, [mbOK]);
  // RV089.1.
  if DialogCalculateEarnedWTRF_HND = nil then
    Close;
end;

procedure TDialogCalculateEarnedWTRF.FormCreate(Sender: TObject);
begin
  inherited;
  TableEarned.Open;
  TableAbsenceTot.Open;
  TableEmplContract.Open;
  MyPlant := '';
  MyDepartment := '';
  MyEmployee := '';
  MyYear := 0;
  MyWeek := 0;
end;

// MR:05-12-2003
procedure TDialogCalculateEarnedWTRF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

procedure TDialogCalculateEarnedWTRF.TableAbsenceTotNewRecord(
  DataSet: TDataSet);
begin
  inherited;
  TableAbsenceTotLAST_YEAR_HOLIDAY_MINUTE.Value := 0;
  TableAbsenceTotNORM_HOLIDAY_MINUTE.Value := 0;
  TableAbsenceTotUSED_HOLIDAY_MINUTE.Value := 0;
  TableAbsenceTotLAST_YEAR_TFT_MINUTE.Value := 0;
  TableAbsenceTotEARNED_TFT_MINUTE.Value := 0;
  TableAbsenceTotUSED_TFT_MINUTE.Value := 0;
  TableAbsenceTotLAST_YEAR_WTR_MINUTE.Value := 0;
  TableAbsenceTotEARNED_WTR_MINUTE.Value := 0;
  TableAbsenceTotUSED_WTR_MINUTE.Value := 0;
  TableAbsenceTotILLNESS_MINUTE.Value := 0;
  SystemDM.DefaultNewRecord(DataSet);
end;

procedure TDialogCalculateEarnedWTRF.TableEarnedNewRecord(
  DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogCalculateEarnedWTRF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogCalculateEarnedWTRF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
