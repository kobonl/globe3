inherited MistakePerEmplDM: TMistakePerEmplDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableDetail: TTable
    Top = 92
  end
  inherited DataSourceDetail: TDataSource
    DataSet = QueryDetail
    Top = 92
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 384
    Top = 16
  end
  object QueryDetail: TQuery
    ObjectView = True
    AfterScroll = QueryDetailAfterScroll
    OnCalcFields = QueryDetailCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  MPE.NUMBER_OF_MISTAKE '
      'FROM '
      '  EMPLOYEE E, MISTAKEPEREMPLOYEE MPE'
      'WHERE '
      ' E.EMPLOYEE_NUMBER =  MPE.EMPLOYEE_NUMBER AND'
      ' MPE.MISTAKE_DATE = :FDATE '
      ' ')
    Left = 96
    Top = 160
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
    object QueryDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryDetailNUMBER_OF_MISTAKE: TIntegerField
      FieldName = 'NUMBER_OF_MISTAKE'
    end
    object QueryDetailWEEKMISTAKE: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'WEEKMISTAKE'
      Calculated = True
    end
  end
  object TableTeam: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'TEAM'
    Left = 384
    Top = 72
  end
  object QueryPlantTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT  '
      '  DPT.PLANT_CODE,'
      '  P.DESCRIPTION'
      'FROM'
      '  DEPARTMENTPERTEAM DPT,'
      '  PLANT P'
      'WHERE'
      '  DPT.TEAM_CODE = :TEAM_CODE AND'
      '  DPT.PLANT_CODE = P.PLANT_CODE')
    Left = 384
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourcePlantTeam: TDataSource
    DataSet = QueryPlantTeam
    Left = 472
    Top = 136
  end
  object DataSourceTeam: TDataSource
    DataSet = TableTeam
    Left = 464
    Top = 72
  end
  object TableMPE: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'MISTAKEPEREMPLOYEE'
    Left = 96
    Top = 224
  end
end
