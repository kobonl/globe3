inherited ReportAvailableUsedTimeDM: TReportAvailableUsedTimeDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object DataSourceEmpl: TDataSource
    DataSet = QueryEmpl
    Left = 128
    Top = 8
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE, P.DESCRIPTION AS PDESCRIPTION,'
      '  E.TEAM_CODE, T.DESCRIPTION AS TDESCRIPTION,'
      '  E.DEPARTMENT_CODE, D.DESCRIPTION AS DDESCRIPTION,'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.STARTDATE, E.ENDDATE, A.USED_HOLIDAY_MINUTE,'
      '  A.LAST_YEAR_HOLIDAY_MINUTE,'
      '  A.NORM_HOLIDAY_MINUTE, A.LAST_YEAR_WTR_MINUTE,'
      '  A.EARNED_WTR_MINUTE,'
      '  A.USED_WTR_MINUTE, A.LAST_YEAR_TFT_MINUTE,'
      '  A.EARNED_TFT_MINUTE, A.USED_TFT_MINUTE'
      'FROM'
      '  EMPLOYEE E LEFT JOIN ABSENCETOTAL A ON'
      '    E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER'
      '  INNER JOIN DEPARTMENT D ON'
      '    E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  INNER JOIN TEAM T ON'
      '    E.TEAM_CODE = T.TEAM_CODE'
      '  INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  E.STARTDATE >= :FDATE'
      ''
      ' '
      ' '
      ' ')
    Left = 32
    Top = 8
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
    object QueryEmplPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'EMPLOYEE.PLANT_CODE'
      Size = 6
    end
    object QueryEmplEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryEmplDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryEmplSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object QueryEmplDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryEmplENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object QueryEmplUSED_HOLIDAY_MINUTE: TFloatField
      FieldName = 'USED_HOLIDAY_MINUTE'
    end
    object QueryEmplLAST_YEAR_HOLIDAY_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_HOLIDAY_MINUTE'
    end
    object QueryEmplNORM_HOLIDAY_MINUTE: TFloatField
      FieldName = 'NORM_HOLIDAY_MINUTE'
    end
    object QueryEmplLAST_YEAR_WTR_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_WTR_MINUTE'
    end
    object QueryEmplEARNED_WTR_MINUTE: TFloatField
      FieldName = 'EARNED_WTR_MINUTE'
    end
    object QueryEmplUSED_WTR_MINUTE: TFloatField
      FieldName = 'USED_WTR_MINUTE'
    end
    object QueryEmplLAST_YEAR_TFT_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_TFT_MINUTE'
    end
    object QueryEmplEARNED_TFT_MINUTE: TFloatField
      FieldName = 'EARNED_TFT_MINUTE'
    end
    object QueryEmplUSED_TFT_MINUTE: TFloatField
      FieldName = 'USED_TFT_MINUTE'
    end
    object QueryEmplPDESCRIPTION: TStringField
      FieldName = 'PDESCRIPTION'
      Size = 120
    end
    object QueryEmplTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 24
    end
    object QueryEmplTDESCRIPTION: TStringField
      FieldName = 'TDESCRIPTION'
      Size = 120
    end
    object QueryEmplDDESCRIPTION: TStringField
      FieldName = 'DDESCRIPTION'
      Size = 120
    end
  end
  object QueryEmpAvail: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DISTINCT E.EMPLOYEEAVAILABILITY_DATE,'
      '  E.EMPLOYEE_NUMBER, E.PLANT_CODE, E.SHIFT_NUMBER,'
      '  E.AVAILABLE_TIMEBLOCK_1, E.AVAILABLE_TIMEBLOCK_2,'
      '  E.AVAILABLE_TIMEBLOCK_3, E.AVAILABLE_TIMEBLOCK_4,'
      '  E.AVAILABLE_TIMEBLOCK_5, E.AVAILABLE_TIMEBLOCK_6,'
      '  E.AVAILABLE_TIMEBLOCK_7, E.AVAILABLE_TIMEBLOCK_8,'
      '  E.AVAILABLE_TIMEBLOCK_9, E.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY E, ABSENCEREASON A'
      'WHERE'
      '  E.EMPLOYEEAVAILABILITY_DATE >= :FDATE  AND'
      '  A.ABSENCETYPE_CODE IN ('#39'T'#39', '#39'H'#39', '#39'W'#39') AND'
      '  ( (E.AVAILABLE_TIMEBLOCK_1 = A.ABSENCEREASON_CODE) OR'
      '    (E.AVAILABLE_TIMEBLOCK_2 = A.ABSENCEREASON_CODE)  OR'
      '    (E.AVAILABLE_TIMEBLOCK_3 = A.ABSENCEREASON_CODE) OR'
      '    (E.AVAILABLE_TIMEBLOCK_4 = A.ABSENCEREASON_CODE)  OR'
      '    (E.AVAILABLE_TIMEBLOCK_5 = A.ABSENCEREASON_CODE) OR'
      '    (E.AVAILABLE_TIMEBLOCK_6 = A.ABSENCEREASON_CODE)  OR'
      '    (E.AVAILABLE_TIMEBLOCK_7 = A.ABSENCEREASON_CODE) OR'
      '    (E.AVAILABLE_TIMEBLOCK_8 = A.ABSENCEREASON_CODE)  OR'
      '    (E.AVAILABLE_TIMEBLOCK_9 = A.ABSENCEREASON_CODE) OR'
      '    (E.AVAILABLE_TIMEBLOCK_10 = A.ABSENCEREASON_CODE) )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' ')
    Left = 32
    Top = 64
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
    object TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryEmpAvailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryEmpAvailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object QueryEmpAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField
      FieldName = 'EMPLOYEEAVAILABILITY_DATE'
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Size = 4
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Size = 4
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Size = 4
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Size = 4
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Size = 4
    end
    object QueryEmpAvailAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Size = 4
    end
  end
  object cdsAbsenceReason: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byAbsenceReasonCode'
        Fields = 'ABSENCEREASON_CODE'
      end>
    IndexName = 'byAbsenceReasonCode'
    Params = <>
    ProviderName = 'dspAbsenceReason'
    StoreDefs = True
    Left = 40
    Top = 144
  end
  object dspAbsenceReason: TDataSetProvider
    DataSet = qryAbsenceReason
    Constraints = True
    Left = 160
    Top = 144
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCETYPE_CODE,'
      '  ABSENCEREASON_CODE'
      'FROM'
      '  ABSENCEREASON')
    Left = 272
    Top = 144
  end
end
