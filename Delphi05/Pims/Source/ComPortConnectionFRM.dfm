inherited ComPortConnectionF: TComPortConnectionF
  Left = 267
  Top = 173
  Height = 515
  Caption = 'Com Port Connection'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Height = 147
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 143
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 142
      Bands = <
        item
          Caption = 'Workstation'
        end>
      KeyField = 'COMPUTER_NAME'
      DataSource = ComPortConnectionDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'COMPUTER NAME'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPUTER_NAME'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 337
    Height = 140
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 138
      Align = alClient
      Caption = 'Com Port Connection'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 71
        Width = 44
        Height = 13
        Caption = 'Com port'
      end
      object Label6: TLabel
        Left = 8
        Top = 20
        Width = 27
        Height = 13
        Caption = 'Name'
      end
      object Label3: TLabel
        Left = 8
        Top = 45
        Width = 61
        Height = 13
        Caption = 'Serial Device'
      end
      object DBEditComputerName: TDBEdit
        Left = 83
        Top = 16
        Width = 231
        Height = 19
        Ctl3D = False
        DataField = 'COMPUTER_NAME'
        DataSource = ComPortConnectionDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object dxDBSpinEditCOMPORT: TdxDBSpinEdit
        Tag = 1
        Left = 83
        Top = 65
        Width = 73
        TabOrder = 2
        DataSource = ComPortConnectionDM.DataSourceDetail
        MaxValue = 255
        MinValue = 1
        StoredValues = 48
      end
      object DBLookupComboBoxSERIALDEVICE: TDBLookupComboBox
        Tag = 1
        Left = 83
        Top = 40
        Width = 231
        Height = 21
        DataField = 'SERIALDEVICE_CODE'
        DataSource = ComPortConnectionDM.DataSourceDetail
        DropDownWidth = 250
        KeyField = 'SERIALDEVICE_CODE'
        ListField = 'NAME;SERIALDEVICE_CODE'
        ListSource = ComPortConnectionDM.DataSourceSerialDevice
        TabOrder = 1
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 173
    Height = 164
    Align = alBottom
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 160
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 159
      Bands = <
        item
          Caption = 'Com Port Connection'
        end>
      KeyField = 'COMPORT'
      DataSource = ComPortConnectionDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnSERIALDEVICE: TdxDBGridColumn
        Caption = 'SERIALDEVICE'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SERIALDEVICE_DESCRIPTIONLU'
      end
      object dxDetailGridColumnCOMPORT: TdxDBGridSpinColumn
        Caption = 'COM PORT'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPORT'
        MinValue = 1
        MaxValue = 255
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
