inherited StaffPlanningEMPDM: TStaffPlanningEMPDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 383
  Top = 165
  Height = 611
  Width = 817
  inherited TableMaster: TTable
    Left = 32
    Top = 16
  end
  inherited TableDetail: TTable
    Left = 28
    Top = 60
    object TableDetailABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
      Required = True
    end
    object TableDetailABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableDetailABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailPAYED_YN: TStringField
      FieldName = 'PAYED_YN'
      Required = True
      Size = 1
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 112
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Left = 112
    Top = 68
  end
  inherited TableExport: TTable
    Left = 200
    Top = 16
  end
  inherited DataSourceExport: TDataSource
    Left = 272
    Top = 16
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    UpdateMode = upWhereChanged
    Left = 112
    Top = 176
  end
  object DataSourcePIVOT: TDataSource
    DataSet = TablePivot
    Left = 112
    Top = 112
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select * from plant')
    Left = 376
    Top = 288
  end
  object QuerySelect: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 200
    Top = 184
  end
  object QueryWorkTmp: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    UpdateMode = upWhereChanged
    Left = 32
    Top = 176
  end
  object QuerySHS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.EMPLOYEE_NUMBER, S.SHIFT_NUMBER ,'
      '  S.AVAILABLE_TIMEBLOCK_1, S.AVAILABLE_TIMEBLOCK_2,'
      '  S.AVAILABLE_TIMEBLOCK_3, S.AVAILABLE_TIMEBLOCK_4,'
      '  S.AVAILABLE_TIMEBLOCK_5, S.AVAILABLE_TIMEBLOCK_6,'
      '  S.AVAILABLE_TIMEBLOCK_7, S.AVAILABLE_TIMEBLOCK_8,'
      '  S.AVAILABLE_TIMEBLOCK_9, S.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  SHIFTSCHEDULE H, STANDARDAVAILABILITY S'
      'WHERE  '
      '  H.SHIFT_SCHEDULE_DATE = :DATESHS AND'
      '  H.SHIFT_NUMBER = 0 AND '
      '  S.EMPLOYEE_NUMBER = H.EMPLOYEE_NUMBER  AND'
      '  S.PLANT_CODE = :PLANT_CODE AND'
      '  S.SHIFT_NUMBER = :FSHIFT AND'
      '  S.DAY_OF_WEEK = :S_DAY'
      'ORDER BY '
      '  S.EMPLOYEE_NUMBER'
      ' ')
    Left = 376
    Top = 240
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATESHS'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'FSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'S_DAY'
        ParamType = ptUnknown
      end>
  end
  object QuerySTA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT AND'
      '  DAY_OF_WEEK = :DAY_WEEK AND'
      '  (( AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' ) OR'
      '   ( AVAILABLE_TIMEBLOCK_10 ='#39'*'#39' ))'
      'ORDER BY'
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER'
      ' ')
    Left = 312
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_WEEK'
        ParamType = ptUnknown
      end>
  end
  object QueryEMA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER,  SHIFT_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  (SHIFT_NUMBER = :SHIFT OR SHIFT_NUMBER = 0)   AND'
      '  EMPLOYEEAVAILABILITY_DATE = :DATEEMA AND'
      '  ((AVAILABLE_TIMEBLOCK_1 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_1 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_2 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_2 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_3 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_3 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_4 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_4 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_5 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_5 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_6 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_6 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_7 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_7 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_8 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_8 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_9 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_9 <> '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_10 IS NOT NULL) OR'
      '   (AVAILABLE_TIMEBLOCK_10 <> '#39'-'#39')'
      '  )'
      'ORDER BY '
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER'
      ' '
      ' ')
    Left = 376
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEEMA'
        ParamType = ptUnknown
      end>
  end
  object QueryEMP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DISTINCT EP.EMPLOYEE_NUMBER, EP.PLANT_CODE, EP.WORKSPOT_CODE,'
      '  EP.DEPARTMENT_CODE, EP.SHIFT_NUMBER,'
      '  EP.SCHEDULED_TIMEBLOCK_1, EP.SCHEDULED_TIMEBLOCK_2,'
      '  EP.SCHEDULED_TIMEBLOCK_3, EP.SCHEDULED_TIMEBLOCK_4,'
      '  EP.SCHEDULED_TIMEBLOCK_5, EP.SCHEDULED_TIMEBLOCK_6,'
      '  EP.SCHEDULED_TIMEBLOCK_7, EP.SCHEDULED_TIMEBLOCK_8,'
      '  EP.SCHEDULED_TIMEBLOCK_9, EP.SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING EP INNER JOIN EMPLOYEE E ON'
      '    EP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN DEPARTMENTPERTEAM DT ON'
      '    EP.PLANT_CODE = DT.PLANT_CODE AND'
      '    EP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND'
      '    ('
      '      (:ALL_TEAMS = 1)'
      '      OR'
      '      (DT.TEAM_CODE >= :TEAMFROM AND DT.TEAM_CODE <= :TEAMTO)'
      '    )'
      'WHERE'
      '  (E.STARTDATE <= :FDATE) AND'
      '  ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      '  AND'
      '  (EP.PLANT_CODE = :PLANT_CODE OR :ALL_PLANTS = 1) AND'
      '  EP.EMPLOYEEPLANNING_DATE = :DATEEMA  AND'
      '  (EP.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  EP.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39','#39'B'#39','#39'C'#39'))'
      'ORDER BY'
      '  EP.EMPLOYEE_NUMBER, EP.WORKSPOT_CODE,'
      '  EP.DEPARTMENT_CODE, EP.SHIFT_NUMBER'
      ''
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 304
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ALL_TEAMS'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ALL_PLANTS'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEEMA'
        ParamType = ptUnknown
      end>
  end
  object QuerySTDEMP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DISTINCT SP.EMPLOYEE_NUMBER, SP.PLANT_CODE, SP.WORKSPOT_CODE,'
      '  SP.DEPARTMENT_CODE, SP.SHIFT_NUMBER,'
      '  SP.SCHEDULED_TIMEBLOCK_1, SP.SCHEDULED_TIMEBLOCK_2,'
      '  SP.SCHEDULED_TIMEBLOCK_3, SP.SCHEDULED_TIMEBLOCK_4,'
      '  SP.SCHEDULED_TIMEBLOCK_5, SP.SCHEDULED_TIMEBLOCK_6,'
      '  SP.SCHEDULED_TIMEBLOCK_7, SP.SCHEDULED_TIMEBLOCK_8,'
      '  SP.SCHEDULED_TIMEBLOCK_9, SP.SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  STANDARDEMPLOYEEPLANNING SP INNER JOIN EMPLOYEE E ON'
      '    SP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN DEPARTMENTPERTEAM DT ON'
      '    SP.PLANT_CODE = DT.PLANT_CODE AND'
      '    SP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND'
      '    ('
      '      (:ALL_TEAMS = 1)'
      '      OR'
      '      (DT.TEAM_CODE >= :TEAMFROM AND DT.TEAM_CODE <= :TEAMTO)'
      '    )'
      'WHERE'
      '  (E.STARTDATE <= :FDATE) AND'
      '  ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      '  AND'
      '  (SP.PLANT_CODE = :PLANT_CODE OR :ALL_PLANTS = 1) AND'
      '  SP.DAY_OF_WEEK = :DAY_WEEK   AND'
      '  (SP.SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SP.SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39','#39'B'#39','#39'C'#39'))'
      'ORDER BY'
      '  SP.EMPLOYEE_NUMBER,'
      '  SP.WORKSPOT_CODE, SP.DEPARTMENT_CODE, SP.SHIFT_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 376
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ALL_TEAMS'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ALL_PLANTS'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_WEEK'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdateEMP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEPLANNING'
      'SET SCHEDULED_TIMEBLOCK_1 = :TB1,'
      'SCHEDULED_TIMEBLOCK_2 = :TB2,'
      'SCHEDULED_TIMEBLOCK_3 = :TB3,'
      'SCHEDULED_TIMEBLOCK_4 = :TB4,'
      'SCHEDULED_TIMEBLOCK_5 = :TB5,'
      'SCHEDULED_TIMEBLOCK_6 = :TB6,'
      'SCHEDULED_TIMEBLOCK_7 = :TB7,'
      'SCHEDULED_TIMEBLOCK_8 = :TB8,'
      'SCHEDULED_TIMEBLOCK_9 = :TB9,'
      'SCHEDULED_TIMEBLOCK_10 = :TB10'
      'WHERE DEPARTMENT_CODE = :DEPT  AND'
      'WORKSPOT_CODE = :WK AND SHIFT_NUMBER = :SHIFT AND'
      'PLANT_CODE = :PLANT AND EMPLOYEEPLANNING_DATE = :DATEEMA AND'
      'EMPLOYEE_NUMBER = :EMPL'
      ' ')
    Left = 40
    Top = 336
    ParamData = <
      item
        DataType = ftString
        Name = 'TB1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEEMA'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPL'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdateSTDEMP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE STANDARDEMPLOYEEPLANNING SET'
      'SCHEDULED_TIMEBLOCK_1 = :TB1, SCHEDULED_TIMEBLOCK_2 = :TB2,'
      'SCHEDULED_TIMEBLOCK_3 = :TB3, SCHEDULED_TIMEBLOCK_4 = :TB4,'
      'SCHEDULED_TIMEBLOCK_5 = :TB5, SCHEDULED_TIMEBLOCK_6 = :TB6,'
      'SCHEDULED_TIMEBLOCK_7 = :TB7, SCHEDULED_TIMEBLOCK_8 = :TB8,'
      'SCHEDULED_TIMEBLOCK_9 = :TB9, SCHEDULED_TIMEBLOCK_10 = :TB10'
      'WHERE PLANT_CODE = :PLANT AND SHIFT_NUMBER = :SHIFT AND'
      'DAY_OF_WEEK = :DAY_WEEK  AND EMPLOYEE_NUMBER = :EMPL AND'
      'DEPARTMENT_CODE = :DEPT AND WORKSPOT_CODE = :WK'
      ' ')
    Left = 160
    Top = 336
    ParamData = <
      item
        DataType = ftString
        Name = 'TB1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TB10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WK'
        ParamType = ptUnknown
      end>
  end
  object StoredProcFillEPL: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_FILLEPL'
    Left = 152
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'SHIFT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WORKSPOT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_4'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_5'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_6'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_7'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_8'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_9'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_10'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEEPL'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATENOW'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end>
  end
  object StoredProcSTDEPL: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_FILLSTDEPL'
    Left = 40
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'SHIFT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WORKSPOT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_4'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_5'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_6'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_7'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_8'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_9'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_10'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'DAYEPL'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATENOW'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end>
  end
  object ClientDataSetWKPerEmpl: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderWKPerEmpl'
    Left = 488
    Top = 424
  end
  object DataSetProviderWKPerEmpl: TDataSetProvider
    Constraints = True
    Left = 624
    Top = 416
  end
  object ClientDataSetAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsRsn'
    Left = 480
    Top = 16
  end
  object DataSetProviderAbsRsn: TDataSetProvider
    DataSet = QueryAbs
    Constraints = True
    Left = 608
    Top = 16
  end
  object QueryAbs: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ABSENCEREASON_CODE, DESCRIPTION '
      'FROM '
      '  ABSENCEREASON '
      'ORDER BY '
      '  DESCRIPTION')
    Left = 376
    Top = 16
  end
  object QueryTBPerEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER, '
      '  TIMEBLOCK_NUMBER '
      'FROM '
      '  TIMEBLOCKPEREMPLOYEE'
      'ORDER BY  '
      '  EMPLOYEE_NUMBER, PLANT_CODE, '
      '  SHIFT_NUMBER, TIMEBLOCK_NUMBER ')
    Left = 376
    Top = 72
  end
  object ClientDataSetTBPerEmpl: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderTBPerEmpl'
    Left = 480
    Top = 72
  end
  object DataSetProviderTBPerEmpl: TDataSetProvider
    DataSet = QueryTBPerEmpl
    Constraints = True
    Left = 608
    Top = 72
  end
  object QueryTBPerDept: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DEPARTMENT_CODE, '
      '  COUNT(*) AS RECNO '
      'FROM '
      '  TIMEBLOCKPERDEPARTMENT'
      'GROUP BY  '
      '  PLANT_CODE, SHIFT_NUMBER, DEPARTMENT_CODE'
      'ORDER BY'
      '  PLANT_CODE, SHIFT_NUMBER,  DEPARTMENT_CODE')
    Left = 376
    Top = 128
  end
  object ClientDataSetTBPerDept: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderTBPerDept'
    Left = 480
    Top = 128
  end
  object DataSetProviderTBPerDept: TDataSetProvider
    DataSet = QueryTBPerDept
    Constraints = True
    Left = 608
    Top = 128
  end
  object ClientDataSetEMA: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderEMA'
    Left = 480
    Top = 184
  end
  object DataSetProviderEMA: TDataSetProvider
    DataSet = QueryEMA
    Constraints = True
    Left = 608
    Top = 184
  end
  object ClientDataSetSHS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderSHS'
    Left = 480
    Top = 240
  end
  object DataSetProviderSHS: TDataSetProvider
    DataSet = QuerySHS
    Constraints = True
    Left = 608
    Top = 240
  end
  object ClientDataSetEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpl'
    Left = 480
    Top = 288
  end
  object DataSetProviderEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 608
    Top = 288
  end
  object ClientDataSetEMPLN: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderEMPLN'
    Left = 488
    Top = 352
  end
  object DataSetProviderEMPLN: TDataSetProvider
    DataSet = QueryEMP
    Constraints = True
    Left = 616
    Top = 352
  end
  object TablePivot: TTable
    CachedUpdates = True
    AfterScroll = TablePivotAfterScroll
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'PIVOTEMPSTAFFPLANNING'
    UpdateMode = upWhereChanged
    Left = 32
    Top = 112
  end
  object QueryUpdateDB: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    ParamCheck = False
    UpdateMode = upWhereChanged
    Left = 208
    Top = 112
  end
  object QueryCountColumn: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  COUNT(*) AS COUNTCOL '
      'FROM  '
      '  SYS.ALL_TAB_COLUMNS T'
      'WHERE '
      '  T.TABLE_NAME = '#39'PIVOTEMPSTAFFPLANNING'#39)
    Left = 208
    Top = 64
  end
  object StoredProcInsertEMP: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_INSERTPIVOTEMP'
    Left = 304
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'PIVOTCOMPUTERNAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'EMP_PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CURRENTDATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'ONLYEMPAVAIL'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'TYPEPLANNING'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'SHIFT'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'DAY_WEEK'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATESELECTION'
        ParamType = ptInput
      end>
  end
end
