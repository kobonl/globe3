(*
  MRA:12-JAN-2010. RV050.8. 889955.
  - Added Plant-level for teams.
  - Restrict plants using teamsperuser.
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
*)
unit TeamFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, dxDBTLCl, dxGrClms,
  dxEditor, dxExEdtr, dxDBEdtr, dxDBELib;

type
  TTeamF = class(TGridBaseF)
    Label1: TLabel;
    Label3: TLabel;
    DBEditDescription: TDBEdit;
    DBEditCode: TDBEdit;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnEmployee: TdxDBGridLookupColumn;
    dxDBLookupEdit1: TdxDBLookupEdit;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnCity: TdxDBGridColumn;
    dxMasterGridColumnState: TdxDBGridColumn;
    dxMasterGridColumnPhone: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function TeamF: TTeamF;

implementation

{$R *.DFM}
uses TeamDMT, SystemDMT;
var
  TeamF_HND: TTeamF;

function TeamF: TTeamF;
begin
  if (TeamF_HND = nil) then
  begin
    TeamF_HND := TTeamF.Create(Application);
  end;
  Result := TeamF_HND;
end;

procedure TTeamF.FormDestroy(Sender: TObject);
begin
  inherited;
  TeamF_HND := Nil;
end;

procedure TTeamF.FormCreate(Sender: TObject);
begin
  TeamDM := CreateFormDM(TTeamDM);
  if (dxDetailGrid.DataSource = nil) or (dxMasterGrid.DataSource = nil) then
  begin
    dxMasterGrid.DataSource := TeamDM.DataSourceMaster;
    dxDetailGrid.DataSource := TeamDM.DataSourceDetail;
  end;
  inherited;
end;

procedure TTeamF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

procedure TTeamF.FormShow(Sender: TObject);
begin
  inherited;
  // RV050.8. Do following, so DetailGrid is shown OK,
  // otherwise the vertical scrollbar is not shown completely.
  pnlDetail.Align := alNone;
  pnlDetail.Align := alBottom;

  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      TeamDM.TableMaster.FieldByName('PLANT_CODE').AsString
  else
    TeamDM.TableMaster.FindKey([SystemDM.ASaveTimeRecScanning.Plant]);
end;

procedure TTeamF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  SystemDM.ASaveTimeRecScanning.Plant :=
    TeamDM.TableMaster.FieldByName('PLANT_CODE').AsString;
end;

end.
