inherited ShiftScheduleDM: TShiftScheduleDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 229
  Top = 156
  Height = 509
  Width = 729
  inherited TableMaster: TTable
    Left = 48
    Top = 16
  end
  inherited TableDetail: TTable
    Left = 48
    Top = 72
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDAY_OF_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
    end
    object TableDetailSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableDetailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 144
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    DataSet = QueryDetail
    Left = 144
    Top = 76
  end
  inherited TableExport: TTable
    Left = 436
    Top = 372
  end
  inherited DataSourceExport: TDataSource
    Left = 536
    Top = 372
  end
  object QueryDetail: TQuery
    ObjectView = True
    AfterScroll = QueryDetailAfterScroll
    OnCalcFields = QueryDetailCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  * '
      'FROM '
      '  SHS_QUERY (:TEAM_CODE,'
      '  :PLANT_CODE, :USER_NAME, :DATEMIN, :DATEMAX)')
    Left = 248
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
    object QueryDetailD11: TStringField
      DisplayWidth = 1
      FieldName = 'D11'
      Size = 1
    end
    object QueryDetailD12: TStringField
      FieldName = 'D12'
      Size = 1
    end
    object QueryDetailD13: TStringField
      FieldName = 'D13'
      Size = 1
    end
    object QueryDetailD14: TStringField
      FieldName = 'D14'
      Size = 1
    end
    object QueryDetailD15: TStringField
      DisplayWidth = 1
      FieldName = 'D15'
      Size = 1
    end
    object QueryDetailD16: TStringField
      DisplayWidth = 1
      FieldName = 'D16'
      Size = 1
    end
    object QueryDetailD17: TStringField
      DisplayWidth = 1
      FieldName = 'D17'
      Size = 1
    end
    object QueryDetailD21: TStringField
      DisplayWidth = 1
      FieldName = 'D21'
      Size = 1
    end
    object QueryDetailD22: TStringField
      FieldName = 'D22'
      Size = 1
    end
    object QueryDetailD23: TStringField
      FieldName = 'D23'
      Size = 1
    end
    object QueryDetailD24: TStringField
      FieldName = 'D24'
      Size = 1
    end
    object QueryDetailD25: TStringField
      FieldName = 'D25'
      Size = 1
    end
    object QueryDetailD26: TStringField
      FieldName = 'D26'
      Size = 1
    end
    object QueryDetailD27: TStringField
      FieldName = 'D27'
      Size = 1
    end
    object QueryDetailD31: TStringField
      DisplayWidth = 1
      FieldName = 'D31'
      Size = 1
    end
    object QueryDetailD32: TStringField
      FieldName = 'D32'
      Size = 1
    end
    object QueryDetailD33: TStringField
      FieldName = 'D33'
      Size = 1
    end
    object QueryDetailD34: TStringField
      FieldName = 'D34'
      Size = 1
    end
    object QueryDetailD35: TStringField
      FieldName = 'D35'
      Size = 1
    end
    object QueryDetailD36: TStringField
      FieldName = 'D36'
      Size = 1
    end
    object QueryDetailD37: TStringField
      FieldName = 'D37'
      Size = 1
    end
    object QueryDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryDetailPLANT: TStringField
      FieldName = 'PLANT'
      Size = 6
    end
    object QueryDetailTEAM: TStringField
      FieldName = 'TEAM'
      Size = 6
    end
    object QueryDetailFLOAT_EMP: TFloatField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object QueryDetailSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object QueryDetailENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object QueryDetailDEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Size = 6
    end
    object QueryDetailP11: TStringField
      FieldName = 'P11'
      Size = 6
    end
    object QueryDetailP12: TStringField
      FieldName = 'P12'
      Size = 6
    end
    object QueryDetailP13: TStringField
      FieldName = 'P13'
      Size = 6
    end
    object QueryDetailP14: TStringField
      FieldName = 'P14'
      Size = 6
    end
    object QueryDetailP15: TStringField
      FieldName = 'P15'
      Size = 6
    end
    object QueryDetailP16: TStringField
      FieldName = 'P16'
      Size = 6
    end
    object QueryDetailP17: TStringField
      FieldName = 'P17'
      Size = 6
    end
    object QueryDetailP21: TStringField
      FieldName = 'P21'
      Size = 6
    end
    object QueryDetailP22: TStringField
      FieldName = 'P22'
      Size = 6
    end
    object QueryDetailP23: TStringField
      FieldName = 'P23'
      Size = 6
    end
    object QueryDetailP24: TStringField
      FieldName = 'P24'
      Size = 6
    end
    object QueryDetailP25: TStringField
      FieldName = 'P25'
      Size = 6
    end
    object QueryDetailP26: TStringField
      FieldName = 'P26'
      Size = 6
    end
    object QueryDetailP27: TStringField
      FieldName = 'P27'
      Size = 6
    end
    object QueryDetailP31: TStringField
      FieldName = 'P31'
      Size = 6
    end
    object QueryDetailP32: TStringField
      FieldName = 'P32'
      Size = 6
    end
    object QueryDetailP33: TStringField
      FieldName = 'P33'
      Size = 6
    end
    object QueryDetailP34: TStringField
      FieldName = 'P34'
      Size = 6
    end
    object QueryDetailP35: TStringField
      FieldName = 'P35'
      Size = 6
    end
    object QueryDetailP36: TStringField
      FieldName = 'P36'
      Size = 6
    end
    object QueryDetailP37: TStringField
      FieldName = 'P37'
      Size = 6
    end
  end
  object StoredProcSHS: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'SHS_QUERY'
    Left = 48
    Top = 312
  end
  object ClientDataSetShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 144
    Top = 192
  end
  object DataSetProviderShift: TDataSetProvider
    DataSet = QueryShift
    Constraints = True
    Left = 248
    Top = 192
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  SHIFT_NUMBER, PLANT_CODE , DESCRIPTION, '
      '  STARTTIME1, ENDTIME1, STARTTIME2, ENDTIME2, '
      '  STARTTIME3, ENDTIME3, STARTTIME4, ENDTIME4, '
      '  STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6,'
      '  STARTTIME7, ENDTIME7 '
      'FROM '
      '  SHIFT '
      'ORDER BY '
      '  PLANT_CODE, SHIFT_NUMBER')
    Left = 48
    Top = 192
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  TEAM_CODE, DESCRIPTION'
      'FROM '
      '  TEAM '
      'ORDER BY '
      '  TEAM_CODE')
    Left = 144
    Top = 256
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 48
    Top = 136
  end
  object QueryCheckIsPlanned: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2, '
      '  SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4, '
      '  SHIFT_NUMBER  '
      'FROM '
      '  EMPLOYEEPLANNING '
      'WHERE '
      '  PLANT_CODE=:PCODE AND EMPLOYEEPLANNING_DATE=:PDATE'
      '  AND EMPLOYEE_NUMBER=:EMPNO AND'
      ' ( SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR '
      '  SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39','#39'C'#39'))')
    Left = 368
    Top = 192
    ParamData = <
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object QueryDeletePlanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEPLANNING '
      'WHERE PLANT_CODE=:PCODE AND EMPLOYEEPLANNING_DATE=:PDATE'
      'AND EMPLOYEE_NUMBER=:EMPNO')
    Left = 368
    Top = 248
    ParamData = <
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object QueryPlantTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      '  DPT.PLANT_CODE,'
      '  P.DESCRIPTION'
      'FROM'
      '  DEPARTMENTPERTEAM DPT,'
      '  PLANT P'
      'WHERE'
      '  DPT.TEAM_CODE = :TEAM_CODE AND'
      '  DPT.PLANT_CODE = P.PLANT_CODE'
      ' '
      ' ')
    Left = 48
    Top = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantFilterRecord
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DESCRIPTION '
      'FROM '
      '  PLANT '
      'ORDER BY '
      '  PLANT_CODE')
    Left = 244
    Top = 256
  end
  object DataSourceEmpl: TDataSource
    DataSet = QueryEmpl
    Left = 144
    Top = 136
  end
  object QueryEmplTo: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 248
    Top = 136
  end
  object DataSourceEmplTo: TDataSource
    DataSet = QueryEmplTo
    Left = 368
    Top = 136
  end
  object qryEmplAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) AS RECCOUNT'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      '   ')
    Left = 408
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qrySHSFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER'
      'FROM'
      '  SHIFTSCHEDULE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 512
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qrySHSDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SHIFTSCHEDULE'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 512
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qrySHSInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTSCHEDULE'
      '('
      '  EMPLOYEE_NUMBER, SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE, SHIFT_NUMBER, CREATIONDATE,'
      '  MUTATIONDATE, MUTATOR'
      ')'
      'VALUES'
      '('
      '  :EMPLOYEE_NUMBER, :SHIFT_SCHEDULE_DATE,'
      '  :PLANT_CODE, :SHIFT_NUMBER, :CREATIONDATE,'
      '  :MUTATIONDATE, :MUTATOR'
      ')'
      '')
    Left = 512
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qrySHSUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE SHIFTSCHEDULE'
      'SET PLANT_CODE = :PLANT_CODE,'
      'SHIFT_NUMBER = :SHIFT_NUMBER,'
      'MUTATIONDATE = :MUTATIONDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 512
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEmplAvailDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEAVAILABILITY'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      '   '
      ' ')
    Left = 408
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
