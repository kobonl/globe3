(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:28-JAN-2011 RV085.19. SC-50015401. SR-20011412. CVL (tailor made).
  - Added 'qryExceptHRsDef' to get total hours based on exceptional hour
    definition of an employee, linked by contract group, for hourtype=1.
  MRA:23-FEB-2018 GLOB3-86
  - Change for hours per employee report
  - For salary hours:
    - Higlight a line when hourtype is linked to a workspot.
  MRA:28-JAN-2019 GLOB3-205
  - Time Card Report
*)
unit ReportHoursPerEmplDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, Provider, DBClient, SystemDMT;

type
  TReportHoursPerEmplDM = class(TReportBaseDM)
    QueryProductionMinute: TQuery;
    QueryAbsenceMinute: TQuery;
    QueryAbsenceMinuteEMPLOYEE_NUMBER: TIntegerField;
    QueryAbsenceMinuteSUMABS_MIN: TFloatField;
    QueryDirProduction: TQuery;
    QueryProductive: TQuery;
    QueryNonProductive: TQuery;
    QueryIndProduction: TQuery;
    DataSourceProdMinute: TDataSource;
    DataSourceEmpl: TDataSource;
    QueryDirSalary: TQuery;
    QueryIndSalary: TQuery;
    QueryEmpl: TQuery;
    TableTeam: TTable;
    QueryCheckTeam: TQuery;
    QueryAbsenceMinuteHourtype: TQuery;
    QueryEmplContract: TQuery;
    ClientDataSetHT: TClientDataSet;
    DataSetProviderHT: TDataSetProvider;
    QueryHT: TQuery;
    ClientDataSetHrsPerWeek: TClientDataSet;
    ClientDataSetHrsPerWeekWEEK_NR: TIntegerField;
    ClientDataSetHrsPerWeekDAY_NR: TIntegerField;
    ClientDataSetHrsPerWeekDAY_MINUTE: TIntegerField;
    ClientDataSetHrsPerHT: TClientDataSet;
    IntegerField1: TIntegerField;
    ClientDataSetHrsPerHTHOUR_TYPE: TIntegerField;
    ClientDataSetHrsPerHTHOUR_TYPE_DESCRIPTION: TStringField;
    ClientDataSetHrsPerHTMINUTE_DAY_1: TIntegerField;
    ClientDataSetHrsPerHTMINUTE_DAY_2: TIntegerField;
    ClientDataSetHrsPerHTMINUTE_DAY_3: TIntegerField;
    ClientDataSetHrsPerHTMINUTEDAY_4: TIntegerField;
    ClientDataSetHrsPerHTMINUTE_DAY_5: TIntegerField;
    ClientDataSetHrsPerHTMINUTE_DAY_6: TIntegerField;
    ClientDataSetHrsPerHTMINUTE_DAY_7: TIntegerField;
    ClientDataSetHrsPerHTWORKED_HOURS: TBooleanField;
    ClientDataSetHrsPerPlantWeek: TClientDataSet;
    ClientDataSetHrsPerPlantWeekPLANT_CODE: TStringField;
    ClientDataSetHrsPerPlantWeekDESCRIPTION: TStringField;
    ClientDataSetHrsPerPlantWeekWEEK_NR: TIntegerField;
    ClientDataSetHrsPerPlant: TClientDataSet;
    ClientDataSetHrsPerPlantPLANT_CODE: TStringField;
    ClientDataSetHrsPerPlantDAY_NR: TIntegerField;
    ClientDataSetHrsPerPlantMINUTE_DAY: TIntegerField;
    ClientDataSetHrsPerPlantDESCRIPTION: TStringField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_1: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_2: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_3: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_4: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_5: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_6: TIntegerField;
    ClientDataSetHrsPerPlantWeekMINUTE_DAY_7: TIntegerField;
    WorkQuery: TQuery;
    qryExceptHrDef: TQuery;
    ClientDataSetHrsPerHTISWORKSPOTHOURTYPE: TStringField;
    qryExtraPaymentsWeek: TQuery;
    qryPaymentExportCode: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryCheckTeamFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateHourTypePerCountry(AEmplNo: Integer);
    function DetermineExceptionalHourDefMin(AEmployeeNumber: Integer): Integer;
    function PaymentExportDescription(APaymentExportCode: String): String;
  end;

var
  ReportHoursPerEmplDM: TReportHoursPerEmplDM;

implementation

{$R *.DFM}

procedure TReportHoursPerEmplDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //550292
  ClientDataSetHrsPerWeek.CreateDataSet;
  ClientDataSetHrsPerHT.CreateDataSet;
  ClientDataSetHrsPerPlantWeek.CreateDataSet;
  ClientDataSetHrsPerPlant.CreateDataSet;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
  SystemDM.PlantTeamFilterEnable(QueryCheckTeam);
  qryPaymentExportCode.Open; // GLOB3-205
end;

procedure TReportHoursPerEmplDM.QueryCheckTeamFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.qryPlantDeptTeam.Locate('TEAM_CODE',
    DataSet.FieldByName('TEAM_CODE').AsString, []);
end;

procedure TReportHoursPerEmplDM.QueryEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TReportHoursPerEmplDM.UpdateHourTypePerCountry(AEmplNo: Integer);
var
  CountryId: Integer;
begin
  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(AEmplNo), 0
  );
  QueryHT.Close;
  QueryHT.Sql.Text :=
    ' select h.*, NVL(' +
    ' (select HC.DESCRIPTION from  hourtypepercountry hc where hc.hourtype_number = h.hourtype_number and hc.country_id =' + IntToStr(CountryId)
    + '), H.DESCRIPTION) HDESCRIPTION from hourtype h ';
  QueryHT.Open;
end;

// RV085.19. Determine total minutes based on Exceptional Hour Definition,
//           from Employee linked by Contract Group. Only take hours
//           for hourtype = 1.
function TReportHoursPerEmplDM.DetermineExceptionalHourDefMin(
  AEmployeeNumber: Integer): Integer;
begin
  with qryExceptHrDef do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    Open;
    Result := FieldByName('EH_MINUTE').AsInteger;
    Close;
  end;
end;

procedure TReportHoursPerEmplDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  qryPaymentExportCode.Close; // GLOB3-205
end;

// GLOB3-205
function TReportHoursPerEmplDM.PaymentExportDescription(
  APaymentExportCode: String): String;
begin
  Result := '';
  if not qryPaymentExportCode.IsEmpty then
    if qryPaymentExportCode.Locate('PAYMENT_EXPORT_CODE', APaymentExportCode, []) then
      Result := qryPaymentExportCode.FieldByName('DESCRIPTION').AsString;
end; // PaymentExportDescription

end.
