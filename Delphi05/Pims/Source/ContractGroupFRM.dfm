inherited ContractGroupF: TContractGroupF
  Left = 266
  Top = 117
  Width = 675
  Height = 559
  Caption = 'Contract groups'
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 659
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 657
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 657
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 332
    Width = 659
    Height = 188
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 657
      Height = 186
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'General'
        object pnlDetail1: TPanel
          Left = 0
          Top = 0
          Width = 280
          Height = 158
          Align = alLeft
          BevelOuter = bvNone
          Caption = 'pnlDetail1'
          TabOrder = 0
          object GroupBoxContractGroup: TGroupBox
            Left = 0
            Top = 0
            Width = 280
            Height = 158
            Align = alClient
            Caption = 'Contract groups'
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 15
              Width = 25
              Height = 13
              Caption = 'Code'
            end
            object Label3: TLabel
              Left = 8
              Top = 40
              Width = 53
              Height = 13
              Caption = 'Description'
            end
            object DBEditContractGroup: TDBEdit
              Tag = 1
              Left = 80
              Top = 14
              Width = 54
              Height = 19
              CharCase = ecUpperCase
              Ctl3D = False
              DataField = 'CONTRACTGROUP_CODE'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 0
            end
            object DBEditDesc: TDBEdit
              Tag = 1
              Left = 80
              Top = 38
              Width = 185
              Height = 19
              Ctl3D = False
              DataField = 'DESCRIPTION'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 1
            end
            object GroupBox4: TGroupBox
              Left = 2
              Top = 89
              Width = 276
              Height = 67
              Align = alBottom
              Caption = 'Guaranteed'
              TabOrder = 2
              object Label5: TLabel
                Left = 8
                Top = 18
                Width = 28
                Height = 13
                Caption = 'Hours'
              end
              object Label6: TLabel
                Left = 144
                Top = 18
                Width = 37
                Height = 13
                Caption = 'Minutes'
              end
              object LabelHourType: TLabel
                Left = 8
                Top = 40
                Width = 48
                Height = 13
                Caption = 'Hour type'
              end
              object dxSpinEditHour: TdxSpinEdit
                Left = 72
                Top = 16
                Width = 67
                TabOrder = 0
                OnKeyPress = dxSpinEditMinKeyPress
                OnMouseUp = dxSpinEditMinMouseUp
                MaxValue = 9999
                StoredValues = 16
              end
              object dxSpinEditMin: TdxSpinEdit
                Left = 198
                Top = 16
                Width = 67
                TabOrder = 1
                OnKeyPress = dxSpinEditMinKeyPress
                OnMouseUp = dxSpinEditMinMouseUp
                MaxValue = 59
                StoredValues = 16
              end
              object dxDBLookupEditHT: TdxDBLookupEdit
                Left = 72
                Top = 40
                Width = 193
                Style.BorderStyle = xbsSingle
                TabOrder = 2
                DataField = 'HOURTYPELU'
                DataSource = ContractGroupDM.DataSourceDetail
                DropDownRows = 5
                DropDownWidth = 250
                ClearKey = 46
                ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
              end
            end
          end
        end
        object pnlDetail2: TPanel
          Left = 280
          Top = 0
          Width = 369
          Height = 158
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBoxOvertime: TGroupBox
            Left = 0
            Top = 0
            Width = 369
            Height = 158
            Align = alClient
            TabOrder = 0
            object DBCheckBoxTimeForTime: TDBCheckBox
              Left = 8
              Top = 10
              Width = 223
              Height = 17
              Caption = 'Time for time'
              Ctl3D = False
              DataField = 'TIME_FOR_TIME_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 0
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
              OnClick = DBCheckBoxTimeForTimeClick
            end
            object DBCheckBoxBonusInMoney: TDBCheckBox
              Left = 8
              Top = 28
              Width = 224
              Height = 17
              Caption = 'Bonus in money'
              Ctl3D = False
              DataField = 'BONUS_IN_MONEY_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 1
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
              OnClick = DBCheckBoxBonusInMoneyClick
            end
            object DBCheckBoxWorkTimeReduction: TDBCheckBox
              Left = 8
              Top = 46
              Width = 225
              Height = 17
              Caption = 'Work time reduction'
              Ctl3D = False
              DataField = 'WORK_TIME_REDUCTION_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 2
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBoxExceptionalBeforeOvertime: TDBCheckBox
              Left = 8
              Top = 63
              Width = 201
              Height = 17
              Caption = 'Exceptional before overtime'
              DataField = 'EXCEPTIONAL_BEFORE_OVERTIME'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 3
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBoxBankHolidayOverrulesAvail: TDBCheckBox
              Left = 8
              Top = 81
              Width = 217
              Height = 17
              Caption = 'Bank holiday overrules availability'
              DataField = 'BANKHOLIDAY_OVERRULES_AVAIL_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 4
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object gBoxPaidIllness: TGroupBox
              Left = 2
              Top = 96
              Width = 365
              Height = 60
              Align = alBottom
              Caption = 'Paid Illness'
              TabOrder = 5
              object Label10: TLabel
                Left = 8
                Top = 16
                Width = 86
                Height = 13
                Caption = 'Paid Illness period'
              end
              object Label11: TLabel
                Left = 8
                Top = 36
                Width = 112
                Height = 13
                Caption = 'Unpaid illn. abs. reason'
              end
              object DBEditPaidIllnessPeriod: TDBEdit
                Left = 136
                Top = 12
                Width = 129
                Height = 19
                Ctl3D = False
                DataField = 'PAID_ILLNESS_PERIOD'
                DataSource = ContractGroupDM.DataSourceDetail
                ParentCtl3D = False
                TabOrder = 0
              end
              object dxDBLookupEditUnpaidIllnessAbsenceReason: TdxDBLookupEdit
                Left = 136
                Top = 34
                Width = 129
                Style.BorderStyle = xbsSingle
                TabOrder = 1
                DataField = 'UNPAID_ILLN_ABSENCEREASONLU'
                DataSource = ContractGroupDM.DataSourceDetail
                DropDownRows = 3
                DropDownWidth = 250
                ClearKey = 46
                ListFieldName = 'DESCRIPTION;ABSENCEREASON_CODE'
              end
            end
            object dxDBCheckEditExportWorkedDays: TdxDBCheckEdit
              Left = 229
              Top = 28
              Width = 144
              TabOrder = 6
              Caption = 'Export Worked Days'
              DataField = 'EXPORT_WORKED_DAYS_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object dxDBCheckEditExportNormalHrs: TdxDBCheckEdit
              Left = 229
              Top = 10
              Width = 144
              TabOrder = 7
              Caption = 'Export Normal Hours'
              DataField = 'EXPORT_NORMAL_HRS_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Additional'
        ImageIndex = 1
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 337
          Height = 158
          Align = alLeft
          TabOrder = 0
          object gboxPeriod: TGroupBox
            Left = 8
            Top = 48
            Width = 321
            Height = 65
            Caption = 'Period'
            TabOrder = 3
            object Label2: TLabel
              Left = 8
              Top = 18
              Width = 100
              Height = 13
              Caption = 'Period starts in week'
            end
            object Label4: TLabel
              Left = 8
              Top = 40
              Width = 76
              Height = 13
              Caption = 'Weeks in period'
            end
            object DBEditPeriodWeeks: TDBEdit
              Left = 192
              Top = 15
              Width = 121
              Height = 19
              Ctl3D = False
              DataField = 'PERIOD_STARTS_IN_WEEK'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 0
            end
            object DBEditWeeks: TDBEdit
              Left = 192
              Top = 39
              Width = 121
              Height = 19
              Ctl3D = False
              DataField = 'WEEKS_IN_PERIOD'
              DataSource = ContractGroupDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 1
            end
          end
          object DBRadioGroupOvertime: TDBRadioGroup
            Left = 8
            Top = 7
            Width = 321
            Height = 40
            Caption = 'Overtime'
            Columns = 4
            DataField = 'OVERTIME_PER_DAY_WEEK_PERIOD'
            DataSource = ContractGroupDM.DataSourceDetail
            Items.Strings = (
              'Day'
              'Week'
              'Month'
              'Period')
            TabOrder = 0
            Values.Strings = (
              '1'
              '2'
              '4'
              '3')
            OnChange = DBRadioGroupOvertimeChange
            OnClick = DBRadioGroupOvertimeClick
          end
          object gboxWorkingDays: TGroupBox
            Left = 8
            Top = 48
            Width = 321
            Height = 40
            Caption = 'Working Days'
            TabOrder = 1
            object DBCheckBox1: TDBCheckBox
              Left = 8
              Top = 16
              Width = 41
              Height = 17
              Caption = 'MO'
              DataField = 'WORKDAY_MO_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 0
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox2: TDBCheckBox
              Left = 51
              Top = 16
              Width = 41
              Height = 17
              Caption = 'TU'
              DataField = 'WORKDAY_TU_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 1
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox3: TDBCheckBox
              Left = 93
              Top = 16
              Width = 41
              Height = 17
              Caption = 'WE'
              DataField = 'WORKDAY_WE_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 2
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox4: TDBCheckBox
              Left = 136
              Top = 16
              Width = 41
              Height = 17
              Caption = 'TH'
              DataField = 'WORKDAY_TH_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 3
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox5: TDBCheckBox
              Left = 179
              Top = 16
              Width = 41
              Height = 17
              Caption = 'FR'
              DataField = 'WORKDAY_FR_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 4
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox6: TDBCheckBox
              Left = 221
              Top = 16
              Width = 41
              Height = 17
              Caption = 'SA'
              DataField = 'WORKDAY_SA_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 5
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
            object DBCheckBox7: TDBCheckBox
              Left = 264
              Top = 16
              Width = 41
              Height = 17
              Caption = 'SU'
              DataField = 'WORKDAY_SU_YN'
              DataSource = ContractGroupDM.DataSourceDetail
              TabOrder = 6
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
          end
          object gboxNormalHrsPerDay: TGroupBox
            Left = 8
            Top = 88
            Width = 321
            Height = 46
            Caption = 'Normal hours per day'
            TabOrder = 2
            object Label12: TLabel
              Left = 8
              Top = 20
              Width = 31
              Height = 13
              Caption = 'Hours '
            end
            object Label13: TLabel
              Left = 176
              Top = 20
              Width = 37
              Height = 13
              Caption = 'Minutes'
            end
            object dxSpinEditHourNHrsPDay: TdxSpinEdit
              Left = 104
              Top = 17
              Width = 67
              TabOrder = 0
              OnKeyPress = dxSpinEditMinKeyPress
              OnMouseUp = dxSpinEditMinMouseUp
              MaxValue = 24
              StoredValues = 16
            end
            object dxSpinEditMinNHrsPDay: TdxSpinEdit
              Left = 246
              Top = 17
              Width = 67
              TabOrder = 1
              OnKeyPress = dxSpinEditMinKeyPress
              OnMouseUp = dxSpinEditMinMouseUp
              MaxValue = 59
              StoredValues = 16
            end
          end
        end
        object Panel2: TPanel
          Left = 337
          Top = 0
          Width = 312
          Height = 158
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 145
            Height = 158
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBoxPayroll: TGroupBox
              Left = 0
              Top = 0
              Width = 145
              Height = 96
              Align = alTop
              Caption = 'Salary hours per day'
              TabOrder = 0
              object Label7: TLabel
                Left = 8
                Top = 71
                Width = 10
                Height = 13
                Caption = 'to'
              end
              object Label8: TLabel
                Left = 86
                Top = 71
                Width = 37
                Height = 13
                Caption = 'minutes'
              end
              object DBRadioGroupPayroll: TDBRadioGroup
                Left = 8
                Top = 12
                Width = 113
                Height = 53
                DataField = 'ROUND_TRUNC_SALARY_HOURS'
                DataSource = ContractGroupDM.DataSourceDetail
                Items.Strings = (
                  'Round'
                  'Truncate')
                TabOrder = 0
                Values.Strings = (
                  '1'
                  '0')
              end
              object dxDBSpinEditMinuteRound: TdxDBSpinEdit
                Tag = 1
                Left = 24
                Top = 69
                Width = 57
                TabOrder = 1
                DataField = 'ROUND_MINUTE'
                DataSource = ContractGroupDM.DataSourceDetail
                OnChange = dxDBSpinEditMinuteRoundChange
                StoredValues = 48
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 96
              Width = 145
              Height = 62
              Align = alClient
              TabOrder = 1
              object Label14: TLabel
                Left = 8
                Top = 7
                Width = 101
                Height = 13
                Caption = 'Hourtype worked on '
              end
              object Label15: TLabel
                Left = 8
                Top = 20
                Width = 64
                Height = 13
                Caption = 'bank holiday:'
              end
              object dxDBLookupEditHTBankHol: TdxDBLookupEdit
                Left = 6
                Top = 35
                Width = 134
                Style.BorderStyle = xbsSingle
                TabOrder = 0
                DataField = 'HOURTYPEBANKHOLLU'
                DataSource = ContractGroupDM.DataSourceDetail
                DropDownRows = 5
                DropDownWidth = 250
                ClearKey = 46
                ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
              end
            end
          end
          object Panel3: TPanel
            Left = 145
            Top = 0
            Width = 167
            Height = 158
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 167
              Height = 158
              Align = alClient
              TabOrder = 0
              object Label9: TLabel
                Left = 9
                Top = 12
                Width = 61
                Height = 13
                Caption = 'Max. Salary:'
              end
              object lblRaisePerc: TLabel
                Left = 8
                Top = 76
                Width = 88
                Height = 13
                Caption = 'Raise Percentage:'
              end
              object LblMaxSatCredMinute: TLabel
                Left = 8
                Top = 114
                Width = 129
                Height = 13
                Caption = 'Max. hrs. Saturday Credit:'
              end
              object LblHrsMin: TLabel
                Left = 112
                Top = 134
                Width = 49
                Height = 13
                Caption = '(Hrs/Mins)'
              end
              object dxDBMaskEdit1: TdxDBMaskEdit
                Left = 6
                Top = 27
                Width = 115
                Style.BorderStyle = xbsSingle
                TabOrder = 0
                DataField = 'MAXIMUM_SALARY'
                DataSource = ContractGroupDM.DataSourceDetail
                IgnoreMaskBlank = False
                MaxLength = 6
                StoredValues = 2
              end
              object dxDBCheckEditExportSRHours: TdxDBCheckEdit
                Left = 5
                Top = 51
                Width = 121
                TabOrder = 1
                Caption = 'Export Hours'
                DataField = 'EXPORT_SR_HOURS_YN'
                DataSource = ContractGroupDM.DataSourceDetail
                ValueChecked = 'Y'
                ValueUnchecked = 'N'
              end
              object dxDBMaskEditRaisePerc: TdxDBMaskEdit
                Left = 6
                Top = 91
                Width = 115
                Style.BorderStyle = xbsSingle
                TabOrder = 2
                DataField = 'RAISE_PERCENTAGE'
                DataSource = ContractGroupDM.DataSourceDetail
                IgnoreMaskBlank = False
                MaxLength = 0
                StoredValues = 6
              end
              object dxSpinEditHMaxSC: TdxSpinEdit
                Left = 6
                Top = 131
                Width = 52
                TabOrder = 3
                OnKeyPress = dxSpinEditMinKeyPress
                OnMouseUp = dxSpinEditMinMouseUp
                MaxValue = 9999
                StoredValues = 16
              end
              object dxSpinEditMMaxSC: TdxSpinEdit
                Left = 67
                Top = 131
                Width = 40
                TabOrder = 4
                OnKeyPress = dxSpinEditMinKeyPress
                OnMouseUp = dxSpinEditMinMouseUp
                MaxValue = 59
                StoredValues = 16
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'PTO'
        ImageIndex = 2
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 313
          Height = 158
          Align = alLeft
          TabOrder = 0
          object GroupBox7: TGroupBox
            Left = 2
            Top = 15
            Width = 309
            Height = 42
            Align = alTop
            Caption = 'Max. PTO transferred'
            TabOrder = 0
            object Label16: TLabel
              Left = 8
              Top = 19
              Width = 28
              Height = 13
              Caption = 'Hours'
            end
            object Label17: TLabel
              Left = 152
              Top = 19
              Width = 37
              Height = 13
              Caption = 'Minutes'
            end
            object dxSpinEditMaxPTOHours: TdxSpinEdit
              Left = 80
              Top = 15
              Width = 67
              TabOrder = 0
              OnKeyPress = dxSpinEditMinKeyPress
              OnMouseUp = dxSpinEditMinMouseUp
              MaxValue = 9999
              StoredValues = 16
            end
            object dxSpinEditMaxPTOMin: TdxSpinEdit
              Left = 230
              Top = 15
              Width = 67
              TabOrder = 1
              OnKeyPress = dxSpinEditMinKeyPress
              OnMouseUp = dxSpinEditMinMouseUp
              MaxValue = 59
              StoredValues = 16
            end
          end
        end
        object GroupBox6: TGroupBox
          Left = 313
          Top = 0
          Width = 336
          Height = 158
          Align = alClient
          TabOrder = 1
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 659
    Height = 177
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 173
      Width = 657
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 657
      Height = 172
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Contract groups'
          Width = 705
        end>
      KeyField = 'CONTRACTGROUP_CODE'
      DataSource = ContractGroupDM.DataSourceDetail
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        CharCase = ecUpperCase
        Width = 42
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACTGROUP_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 193
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn7: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 77
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxDetailGridColumn3: TdxDBGridCheckColumn
        Caption = 'Time for time'
        MinWidth = 20
        Width = 86
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIME_FOR_TIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn4: TdxDBGridCheckColumn
        Caption = 'Work time reduction'
        Width = 115
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORK_TIME_REDUCTION_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn5: TdxDBGridCheckColumn
        Caption = 'Bonus in money'
        MinWidth = 20
        Width = 91
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_IN_MONEY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Alignment = taRightJustify
        Caption = 'Guaranteed hours'
        Width = 99
        BandIndex = 0
        RowIndex = 0
        FieldName = 'GUARANTEEDHRS'
      end
      object dxDetailGridColumn9: TdxDBGridColumn
        Caption = 'Period starts in week'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PERIOD_STARTS_IN_WEEK'
      end
      object dxDetailGridColumn10: TdxDBGridColumn
        Caption = 'Weeks in period'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WEEKS_IN_PERIOD'
      end
      object dxDetailGridColumn11: TdxDBGridColumn
        Caption = 'Overtime'
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OVERTIMETYPE'
      end
      object dxDetailGridColumn12: TdxDBGridCheckColumn
        Caption = 'Exceptional before overtime'
        ReadOnly = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXCEPTIONAL_BEFORE_OVERTIME'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn13: TdxDBGridColumn
        Caption = 'Round/Trunc salary hours'
        DisableEditor = True
        Visible = False
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ROUND_TRUNCATE_HRS'
      end
      object dxDetailGridColumn14: TdxDBGridColumn
        Caption = 'Round/Trunc minutes'
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ROUND_MINUTE'
      end
      object dxDetailGridColumn15: TdxDBGridColumn
        Caption = 'Max. Salary'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MAXIMUM_SALARY'
      end
      object dxDetailGridColumn16: TdxDBGridCheckColumn
        Caption = 'Bank holiday overrules availability'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BANKHOLIDAY_OVERRULES_AVAIL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn17: TdxDBGridLookupColumn
        Caption = 'Hour type w. on bank hol.'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPEBANKHOLLU'
      end
      object dxDetailGridColumn18: TdxDBGridColumn
        Caption = 'Max PTO tranferred'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MAX_PTO_MINUTE_CALC'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
end
