inherited DialogReportAbsenceSheduleF: TDialogReportAbsenceSheduleF
  Left = 339
  Top = 156
  Caption = 'Report work time reduction, holiday, illness '
  ClientHeight = 343
  ClientWidth = 595
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 595
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 299
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 595
    Height = 222
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 40
    end
    inherited LblEmployee: TLabel
      Top = 40
    end
    inherited LblToPlant: TLabel
      Top = 41
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    object Label2: TLabel [6]
      Left = 8
      Top = 90
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [7]
      Left = 40
      Top = 90
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [8]
      Left = 195
      Top = 90
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 44
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 44
    end
    object Label5: TLabel [11]
      Left = 128
      Top = 70
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [12]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [13]
      Left = 8
      Top = 70
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPeriod: TLabel [14]
      Left = 296
      Top = 90
      Width = 40
      Height = 13
      Caption = 'lblPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 261
    end
    inherited LblDepartment: TLabel
      Top = 261
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 263
    end
    inherited LblStarDepartmentTo: TLabel
      Top = 263
    end
    inherited LblToDepartment: TLabel
      Top = 263
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 91
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 92
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 135
      TabOrder = 24
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 23
    end
    inherited CheckBoxAllTeams: TCheckBox
      TabOrder = 22
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 260
      ColCount = 128
      TabOrder = 26
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 260
      ColCount = 129
      TabOrder = 27
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Top = 263
      TabOrder = 28
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 40
      TabOrder = 2
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 40
      TabOrder = 3
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 135
      TabOrder = 11
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 12
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 144
      TabOrder = 14
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 145
      TabOrder = 15
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 145
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 146
      TabOrder = 16
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 152
      TabOrder = 17
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 151
      TabOrder = 18
    end
    object GroupBox1: TGroupBox
      Left = 296
      Top = 120
      Width = 217
      Height = 97
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      object CheckBoxShowTotal: TCheckBox
        Left = 5
        Top = 20
        Width = 209
        Height = 17
        Caption = 'Show total hours per absence reasons'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object CheckBoxShowSelection: TCheckBox
        Left = 5
        Top = 45
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 1
      end
      object CheckBoxExport: TCheckBox
        Left = 5
        Top = 70
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 2
      end
    end
    object RadioGroupEmployee: TRadioGroup
      Left = 152
      Top = 120
      Width = 137
      Height = 97
      Caption = 'Status employee'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Active'
        'Inactive'
        'All')
      ParentFont = False
      TabOrder = 8
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 120
      Top = 64
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 120
      Top = 88
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 120
      Width = 137
      Height = 97
      Caption = 'Absence types'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      object CheckBoxWTR: TCheckBox
        Left = 8
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Work time reduction'
        TabOrder = 0
      end
      object CheckBoxIll: TCheckBox
        Left = 8
        Top = 48
        Width = 65
        Height = 17
        Caption = 'Illness'
        TabOrder = 1
      end
      object CheckBoxHol: TCheckBox
        Left = 8
        Top = 72
        Width = 73
        Height = 17
        Caption = 'Holiday'
        TabOrder = 2
      end
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 216
      Top = 88
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
  end
  inherited stbarBase: TStatusBar
    Top = 283
    Width = 595
  end
  inherited pnlBottom: TPanel
    Top = 302
    Width = 595
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 88
  end
  inherited QueryEmplFrom: TQuery
    Left = 192
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 296
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 576
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4E040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072E4469616C6F675265706F7274416273656E636553686564
        756C65462E44617461536F75726365456D706C46726F6D104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E
        06064E756D62657206536F727465640704637355700557696474680241094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060F
        454D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D
        6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274
        206E616D6505576964746802540942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060A53484F52545F4E414D4500000F546478
        444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06
        044E616D6505576964746803B4000942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060B4445534352495054494F4E00000F54
        6478444247726964436F6C756D6E0D436F6C756D6E4164647265737307436170
        74696F6E06074164647265737305576964746802450942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D6506074144445245535300
        000F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F6465
        0743617074696F6E060F4465706172746D656E7420636F646505576964746802
        580942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D65060F4445504152544D454E545F434F444500000F54647844424772696443
        6F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D2063
        6F64650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        15040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072C4469616C6F675265706F72
        74416273656E636553686564756C65462E44617461536F75726365456D706C54
        6F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F7669
        6E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F7669
        6E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A69
        6E6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D
        6564676F43616E44656C6574650D6564676F43616E496E73657274116564676F
        43616E4E617669676174696F6E116564676F436F6E6669726D44656C65746512
        6564676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61
        726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C
        0743617074696F6E06064E756D62657206536F72746564070463735570055769
        64746802310942616E64496E646578020008526F77496E646578020009466965
        6C644E616D65060F454D504C4F5945455F4E554D42455200000F546478444247
        726964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F
        6E060A53686F7274206E616D65055769647468024E0942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D65060A53484F52545F4E41
        4D4500000F546478444247726964436F6C756D6E11436F6C756D6E4465736372
        697074696F6E0743617074696F6E06044E616D6505576964746803CC00094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060B
        4445534352495054494F4E00000F546478444247726964436F6C756D6E0D436F
        6C756D6E416464726573730743617074696F6E06074164647265737305576964
        746802650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D6506074144445245535300000F546478444247726964436F6C756D6E
        0E436F6C756D6E44657074436F64650743617074696F6E060F4465706172746D
        656E7420636F64650942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060F4445504152544D454E545F434F444500000F546478
        444247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06
        095465616D20636F64650942616E64496E646578020008526F77496E64657802
        00094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 460
  end
end
