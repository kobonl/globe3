// MR:10-07-2003
// Important note:
// This Datamodule has 3 extra tables for lookup-fields in the grid.
// The lookup-queries that were already made didn't give correct results
// in the grid. Therefore it was decided to add these 3 extra tables.
// This is necessary for tables that have more than 1 key to connect to
// the Master/Detail-source, otherwise the DESCRIPTION-field is not shown
// correct in the Grid!
// In this case this was needed for:
// - Workspot - Keys PLANT_CODE, WORKSPOT_CODE
// - Jobcode - Keys PLANT_CODE, WORKSPOT_CODE, JOB_CODE
// - Shift - Keys PLANT_CODE, SHIFT_NUMBER
// The problems can be seen when you have a situation like this:
// - Workspot:
//     115_1, 115_2, 115_3, 115_4 (WORKSPOT_CODE)
// - Jobs for this Workspot:
//     115_1, 115, Cleaning 1 (WORKSPOT_CODE, JOB_CODE, DESCRIPTION)
//     115_2, 115, Cleaning 2
//     115_3, 115, Cleaning 3
//     115_4, 115, Cleaning 4
// In this example the descriptions can be wrong of the workspots + jobs,
// if these extra tables are not used.
(*
  Changes:
  MR:23-1-2004 If employee has 'Is_scanning_yn' = 'N' then salary
               should not be recalculated.
  MR:05-02-2004 Closing of Not-Complete-Scans should be possible by filtering
                on a date, so more than 1 record can be closed at a time.
                Order 550300.
  MR:23-03-2004 Some changes for Order 550300. Instead of 'QueryWorkspotLU',
                'TableWorkspot' is used.
  MR:25-10-2005 Sometimes the 'TableWorkspot' pointed to a wrong workspot
                (first workspot instead of the selected one).
                To prevent this, it is looked up again.
  MRA:18-SEP-2009 RV033.4.
    - Add call to Recalculate TFT hours (RecalcEarnedTFTHours).
  MRA:27-NOV-2009 RV045.1.
    - Addition of RecalcEarnedTFTHours. Call this always when a scan is
      changed to be sure the TFT-hours in balance are correct.
  MRA:14-MAY-2010. RV063.1. 889997
  - When a complete scan is entered or updated, then
    look for non-manual PQ-record of the same period, plant, workspot
    and job equals 'NOSCAN' (or same job as scan-job). If found then
    change these PQ-records to the scan-job and scan-shift.
  SO:09-06-2010 RV065.8.
    - when the scan is longer than 12h is splitted in two scans
  SO:27-08-2010 RV067.11. 550500
  - time components replacement
  MRA:30-SEP-2010 RV070.1. User rights.
  - Use user-rights here, to filter on teams-per-user.
  MRA:6-OCT-2010 RV071.6. 550497
  - Added call to: RecalcEarnedSWWHours
  MRA:18-OCT-2010 RV072.2. Bugfixing.
  - Not Complete:
    - When an end-time is entered then
      it can happen the end-date stays empty (null).
      When saving, this will give an error-message.
      To solve this, assign the datetime-in date
      to datetime-out on saving.
  MRA:20-OCT-2010 RV073.1. 50015255
  - When entering times, do not use defaults, but only 00:00.
  - A value entered higher then 23:59 must be trapped.
  MRA:21-OCT-2010 RV073.3.
  - Time Rec Scanning-dialog shows always all
    workspots. It does not filter on teams-per-user
    (departments linked to workspots).
  MRA:2-NOV-2010 RV075.11.
  - When in tab-page scans and trying to
    change the workspot of an existing open scan
    it gives an error-message: Cannot change a closed
    scan back to open.
  MRA:3-NOV-2010 RV076.5. Bugfix.
  - Time rec scanning
    - Not Complete-tab:
      - When inserting a new open scan it gives an error
        about wrong entered period.
        Reason: It is setting autom. the end-date during before-post.
      - It must not do this automatically!
  MRA:4-NOV-2010 RV076.7. Bugfix.
  - Not Complete-tab:
    - Inserting new scans must not be allowed!
      Or it can give multiple open scans!
  - Scans-tab:
    - It appears to be possible to insert multiple open
      scans! This must be prevented!
      The check (qryTRS_BICheck1) has been disabled: to
      be investigated.
  MRA:8-NOV-2010. RV077.3. Bugfix. RFL-only change.
  - A check is made if an employee is a non-scanner,
    if so, then give a warning when trying to make a
    mutation. Otherwise it can lead to wrong hours,
    or overwrite hours made because of 'prod. hrs.
    based on standard planning.'.
  MRA:1-DEC-2010. RV082.4.
  - Do not allow changes at all for employees of type
    'Autom. scans based on standard planning'.
  MRA:3-DEC-2010. RV082.8.
  - Tailor made non-scanners
    - Time Rec Scanning-dialog
      - Because of restrictions:
        - When first in Scans-tab a 'auto. scans'-employee
          is selected and then you go to Open-scans-tab, it
          also gives the restriction for an open-scan for an
          employee that is not of type 'auto. scans'.
  MRA:12-MAY-2011. RV092.15. Bugfix. 550518
  - Since RV073.3 it filters on
    workspots based on teams-per-user linked to the department-team
    for the workspots.
    This makes it difficult to assign a workspot when
    using 'plan-in-other-plants'.
  - SOLUTION: It should only use this filter when Plan-in-other-plants
              is not used.
  - Also: Filter on plants with same conditions as for workspots.
  MRA:1-NOV-2011 RV100.1.
  - Added Contractgroup.
  MRA:25-JUL-2012 20013472. Change.
  - There is no option to sort in the grid in
    Not-completed-scans.
  - Change: Add sort-button and sort-options.
  - NOTE: The above gave problems when clicking this sort-button.
          It gave errors about fields like 'PLANT_CODE;WORKSPOT_CODE'
          and 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'. To prevent this
          now calculated fields are used + TQuery's instead of TTable's and
          lookup-fields.
  MRA:1-OCT-2012 20013489 Overnight-Shift-System
  - Addition of field ShiftDate to scans to make it possible to
    keep track of shifts.
  - Handle situation when from an existing scan the shift is changed. When
    this was an overnight-shift and changed to non-overnight-shift, it means
    hours must be booked on a different date.
  - Filter on ShiftDate for 'ScanPerEmployee'-ScanFilter.
  MRA:25-MAR-2013 20013035 Do not show inactive employees
  - Added STARTDATE and ENDDATE to QueryFilterEmployee
  - Added IsEmployeeActive-function that determines if an employee is
    active or not.
  MRA:22-MAY-2013 TD-22538
  - When using teams-per-user it gave an error about QueryFilterEmployee,
    field STARTDATE not found.
  - Solution: Use fixed queries for QueryFilterEmployee and QueryEmployeeLU
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:6-NOV-2013 Bugfix.
  - Call to: UpdatePQBasedOnScan (RV063.1)
    - This gives an exception! Disable it for now.
  MRA:13-NOV-2013 TD-23386
  - Hourtype on shift should not result in double hours.
    - For PopulateIDCard: Also determine hourtype on shift:
      - TableDetail: SHIFT_HOURTYPE_NUMBER
  - Related to this:
    - Also get 3 extra fields:
      - WORKSPOT_HOURTYPE_NUMBER
      - IGNORE_OVERTIME_YN (based on hourtype-of-workspot)
      - DEPARTMENT_CODE (of workspot)
  MRA:14-FEB-2014 20011800
  - Final Run System
  - Store PlantCode in DataFilter for later use.
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - cdsFilterEmployee, Filtered is set to True
  MRA:6-MAY-2015 20014450.50
  - Real Time Efficiency
  - After a scan has been changed call stored procedure
    RECALC_EFFICIENCY.
  MRA:15-SEP=2015 PIM-87 Related to this order
  - Sometimes it did not commit the transaction. When a scan was deleted, then
    the hours were still there!
  MRA:6-MAY-2016 PIM-151 Related to this issue
  - Also do a recalc-efficiency when the scan is NOT closed.
  MRA:3-NOV-2017 PIM-323
  - In Early/Late-Tab, do not check on an existing
    scan during entry/change of a record, so always allow a change.
  - In Early/Late-Tab: Show line in red when there was a first scan found for
    employee that is later than the Early-date.
*)

unit TimeRecScanningDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, BDE, TimeRecScanningFRM, UGlobalFunctions,
  DBClient, Provider, UScannedIDCard, CalculateTotalHoursDMT, GlobalDMT;

type
  TOldRecValue = record
    DateTime_IN, DateTime_Out: TdateTime;
    Employee_Number,Shift_Number: integer;
    Plant_Code,WorkSpot_Code,Job_Code,IDCard_IN,IDCard_Out: string;
    Processed: Boolean;
    ShiftDate: TDateTime; // 20013489
  end;

  TScanFilter = (NotComplete, ScanPerEmployee, EarlyLate);

  TScanAction = (AddScan,DeleteScan,CompleteScan);

  TDataFilter = record
    ScanFilter: TScanFilter;
    ScanAction: TScanAction;
    ShiftNumber, EmployeeNumber: integer;
    IsScanning: Boolean; // MR:23-1-2004
    StartDate, EndDate: TdateTime;
    Active: Boolean;
    NotCompleteFilterActive: Boolean; // MR:05-02-2004
    BookProdHrs: Boolean; // RV077.3.
    PlantCode: String; // 20011800
    ShowOnlyActive: Boolean; // 20013035
  end;

  TTimeRecScanningDM = class(TGridBaseDM)
    DataSourcePlantLU: TDataSource;
    TableMasterREQUEST_DATE: TDateTimeField;
    TableMasterEMPLOYEE_NUMBER: TIntegerField;
    TableMasterREQUESTED_EARLY_TIME: TDateTimeField;
    TableMasterREQUESTED_LATE_TIME: TDateTimeField;
    TableDetailDATETIME_IN: TDateTimeField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailWORKSPOT_CODE: TStringField;
    TableDetailJOB_CODE: TStringField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPROCESSED_YN: TStringField;
    TableDetailIDCARD_IN: TStringField;
    TableDetailIDCARD_OUT: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    QueryWork: TQuery;
    QueryDelete: TQuery;
    TableDetailDATETIME_OUT: TDateTimeField;
    DataSourceWorkSpotLU: TDataSource;
    DataSourceJobLU: TDataSource;
    TableDetailPLANTCAL: TStringField;
    TableDetailEMPLOYEECAL: TStringField;
    DataSourceEmployeeLU: TDataSource;
    DataSourceShiftLU: TDataSource;
    TableMasterEMPLOYEELU: TStringField;
    DataSourceFilterEmployee: TDataSource;
    DataSourceFilterShift: TDataSource;
    QueryFilterEmployee: TQuery;
    dspFilterEmployee: TDataSetProvider;
    cdsFilterEmployee: TClientDataSet;
    QueryFilterShift: TQuery;
    dspFilterShift: TDataSetProvider;
    cdsFilterShift: TClientDataSet;
    QueryFilterEmployeeEMPLOYEE_NUMBER: TIntegerField;
    QueryFilterEmployeeSHORT_NAME: TStringField;
    QueryFilterEmployeeDESCRIPTION: TStringField;
    QueryFilterShiftPLANT_CODE: TStringField;
    QueryFilterShiftSHIFT_NUMBER: TIntegerField;
    QueryFilterShiftDESCRIPTION: TStringField;
    QueryFilterEmployeePLANTLU: TStringField;
    QueryFilterShiftPLANTLU: TStringField;
    QueryFilterEmployeePLANT_CODE: TStringField;
    QueryEmployeeLU: TQuery;
    QueryWorkspotLU: TQuery;
    QueryJobLU: TQuery;
    QueryShiftLU: TQuery;
    QueryPlantLU: TQuery;
    QueryFilterEmployeeIS_SCANNING_YN: TStringField;
    TableWorkspot: TTable;
    TableWorkspotPLANT_CODE: TStringField;
    TableWorkspotDESCRIPTION: TStringField;
    TableWorkspotWORKSPOT_CODE: TStringField;
    TableWorkspotCREATIONDATE: TDateTimeField;
    TableWorkspotDEPARTMENT_CODE: TStringField;
    TableWorkspotMUTATIONDATE: TDateTimeField;
    TableWorkspotMUTATOR: TStringField;
    TableWorkspotUSE_JOBCODE_YN: TStringField;
    TableWorkspotHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotQUANT_PIECE_YN: TStringField;
    TableWorkspotAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotCOUNTER_VALUE_YN: TStringField;
    TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotDATE_INACTIVE: TDateTimeField;
    DataSourceWorkspot: TDataSource;
    qryTRS_BICheck1: TQuery;
    qryTRS_BICheck2: TQuery;
    qryTRS_BUCheck1: TQuery;
    qryTRS_BUCheck2: TQuery;
    qryPQ: TQuery;
    qryUpdatePQ: TQuery;
    QueryInsertScanning: TQuery;
    TableDetailEMP_PLANT_CODE: TStringField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailTEAM_CODE: TStringField;
    QueryFilterEmployeeBOOK_PROD_HRS_YN: TStringField;
    qryTRSSetEnddate: TQuery;
    qryWorkspotLU: TQuery;
    TableDetailWORKSPOTCAL: TStringField;
    qryJobLU: TQuery;
    TableDetailJOBCAL: TStringField;
    qryShiftLU: TQuery;
    TableDetailSHIFTCAL: TStringField;
    TableDetailSHIFT_DATE: TDateTimeField;
    qryTRSLastRec: TQuery;
    QueryFilterEmployeeSTARTDATE: TDateTimeField;
    QueryFilterEmployeeENDDATE: TDateTimeField;
    TableDetailSHIFT_HOURTYPE_NUMBER: TIntegerField;
    TableDetailWORKSPOT_HOURTYPE_NUMBER: TIntegerField;
    TableDetailIGNORE_FOR_OVERTIME_YN: TStringField;
    TableDetailWORKSPOT_DEPARTMENT_CODE: TStringField;
    StoredProcRecalcEfficiency: TStoredProc;
    StoredProcMoveEfficiency: TStoredProc;
    qryEarlyLateScanCheck: TQuery;
    procedure TableMasterNewRecord(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailWORKSPOT_CODEChange(Sender: TField);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure TableDetailDATETIME_INChange(Sender: TField);
    procedure TableDetailSHIFT_NUMBERChange(Sender: TField);
    procedure TableMasterREQUEST_DATEChange(Sender: TField);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableFilterEmployeeAfterScroll(DataSet: TDataSet);
    procedure TableFilterShiftAfterScroll(DataSet: TDataSet);
    procedure TableDetailBeforeOpen(DataSet: TDataSet);
    procedure TablePlantAfterScroll(DataSet: TDataSet);
    procedure QueryWorkSpotAfterScroll(DataSet: TDataSet);
    procedure DefaultSaveCurrentRecord(DataSet: TDataSet);
    procedure TableDetailJOB_CODEChange(Sender: TField);
    procedure TableMasterBeforePost(DataSet: TDataSet);
    procedure TableMasterBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailPLANT_CODEChange(Sender: TField);
    procedure TableDetailBeforeInsert(DataSet: TDataSet);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableWorkspotFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
    procedure QueryPlantLUFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure cdsFilterEmployeeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailAfterDelete(DataSet: TDataSet);

  private
    { Private declarations }
    //RV065.8.
    FEnableValidation: Boolean;
    FFoundMoreThan12hScan: Boolean;
    FScanFilter: TDataFilter;
    FNewEnteredDateOut: TDateTime;
    procedure SetFilter(Value: TDataFilter);
{    function CheckScanRecOnDay(ADate: TDateTime;
      EmployeeCode: integer): boolean; }
    function CheckBeforeInsert(ADataSet: TDataSet): Boolean;
    function CheckBeforeUpdate(ADataSet: TDataSet): Boolean;
    function CheckScanOverlap(ADataSet: TDataSet): Boolean;
    procedure CheckWorkspot(DataSet: TDataSet);
    procedure MoveEfficiency(ADataSet: TDataSet);
    procedure RecalcEfficiency(ADataSet: TDataSet);
  public
    { Public declarations }
    OldRecVal : TOldRecValue;
    TimeScanAction: TScanAction;
    procedure DefaultAfterScroll(DataSet: TDataSet);
    //CAR 550274 - User rights for teams
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ProcessCompletedScan(Action: TScanAction);
//    procedure FindShiftWorkSpots;
//    procedure FindJobCodes;
    procedure IfUniqueSetShift;
    procedure IfUniqueSetJob;
    // This function check if exist record for
    procedure SetJobCodeField(WorkspotCode: string);
    procedure UpdatePQBasedOnScan; // RV063.1.
    function CheckIfUseJobCode(Plant, Workspot, JobCode: String): Boolean;
    procedure AddRemainingScanPeriod;
    function IsNoScannerWarning: Boolean;
    function IsEmployeeActive(ADate: TDateTime): Boolean;
    procedure ShowOnlyActiveSwitch; // 20013035
    function EarlyLateScanCheck(AEarlyDate: TDateTime; AEmployeeNumber: Integer): Boolean;
    property ScanFilter: TDataFilter read FScanFilter write SetFilter;
    property EnableValidation: Boolean read FEnableValidation write FEnableValidation;
    property NewEnteredDateOut: TDateTime read FNewEnteredDateOut write FNewEnteredDateOut;
  end;

var
  TimeRecScanningDM: TTimeRecScanningDM;
  NewIDCard: TScannedIDCard; //RV065.8.
  EmployeeIDCard: TScannedIDCard;
  CurrentRecord: TOldRecValue;

implementation

uses
  UPimsConst, SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TTimeRecScanningDM.TableMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // MR:13-02-2003 Be sure only date-part is put in field!
  DataSet.FieldByName('REQUEST_DATE').AsDateTime := Trunc(ScanFilter.StartDate); // Trunc(Now); // PIM-323
end;

procedure TTimeRecScanningDM.TableMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
// PIM-323
(*
  if CheckScanRecOnDay(DataSet.FieldByName('REQUEST_DATE').AsDateTime,
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger) then
  begin
    DisplayMessage(SPimsScanExists, mtInformation, [mbOk]);
    DataSet.Cancel;
    SysUtils.Abort;
  end;
*)
  SystemDM.DefaultBeforePost(DataSet);
end;

procedure TTimeRecScanningDM.TableMasterBeforeDelete(DataSet: TDataSet);
begin
  inherited;
// PIM-323
(*
  if CheckScanRecOnDay(DataSet.FieldByName('REQUEST_DATE').AsDateTime,
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger) then
  begin
    DisplayMessage(SpimsScanExists, mtInformation, [mbOk]);
    DataSet.Cancel;
    SysUtils.Abort;
  end;
*)
  SystemDM.DefaultBeforeDelete(DataSet);
end;

// Triggers on a change of 'request_date'-field
procedure TTimeRecScanningDM.TableMasterREQUEST_DATEChange(Sender: TField);
var
  AFilter: TDataFilter;
begin
  inherited;
  if TField(Sender).AsDateTime <= NullDate then
    Exit;
  AFilter := ScanFilter;
  AFilter.StartDate := TField(Sender).AsDateTime;
  ScanFilter := AFilter;
end;

procedure TTimeRecScanningDM.TableDetailNewRecord(DataSet: TDataSet);
var
  ADate: TDateTime;
  CurrentRecDateTime: TDateTime;
  LastRecDateTime: TDateTime;
//  hour,min,sec,msec: word;
  // 20013489
  procedure DetermineLastRecDateTime;
  begin
    try
      LastRecDateTime := Nulldate;
      with qryTRSLastRec do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
        ParamByName('SHIFT_DATE').AsDateTime := ScanFilter.StartDate;
        Open;
        if not Eof then
          LastRecDateTime := FieldByName('DATETIME_OUT').AsDateTime;
        Close;
      end;
    except
      LastRecDateTime := Nulldate;
    end;
  end;
begin
  inherited;
  NewEnteredDateOut := NullDate; // RV076.5.
  DefaultNewrecord(DataSet);
  if ScanFilter.ScanFilter = ScanPerEmployee then
  begin
    // MR:13-01-2005 Order 550366. Get the time from the current record,
    // use this for new record.
    CurrentRecDateTime := OldRecVal.DateTime_Out;
    // RV073.1. 50015255
    // Do not set this to Now, but leave it to 0.
{    if CurrentRecDateTime = 0 then
      CurrentRecDateTime := Now; }
    DataSet.FieldByName('EMPLOYEE_NUMBER').Value :=
      cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
    // 20013489
    if SystemDM.UseShiftDateSystem then // 20014327
      DetermineLastRecDateTime
    else
      LastRecDateTime := NullDate;
    if LastRecDateTime <> NullDate then
      ADate := LastRecDateTime
    else
      ADate := ScanFilter.StartDate;
    ReplaceTime(ADate, RoundTime(CurrentRecDateTime, 1));
    DataSet.FieldByName('DATETIME_IN').Value := ADate;
    DataSet.FieldByName('DATETIME_OUT').Value := ADate;
    DataSet.FieldByName('PLANT_CODE').AsString :=
      cdsFilterEmployee.FieldByName('PLANT_CODE').Value;
 //CAR - SHIFT DEFAULT VALUE
    if cdsFilterShift.RecordCount = 1 then
     DataSet.FieldByName('SHIFT_NUMBER').Value:=
      cdsFilterShift.FieldByName('SHIFT_NUMBER').Value;
  {!!!  if not TableFilterShift.IsEmpty then
      DataSet.FieldByName('SHIFT_NUMBER').Value :=
        TableFilterShift.FieldByName('SHIFT_NUMBER').Value;}
    DataSet.FieldByName('PROCESSED_YN').AsString := UNCHECKEDVALUE;
  end
  else
  begin
    DataSet.FieldByName('DATETIME_IN').AsDateTime := RoundTime(Now,1);
    if QueryPlantLU.RecordCount = 1  then
      DataSet.FieldByNAme('PLANT_CODE').AsString :=
        QueryPlantLU.FieldByName('PLANT_CODE').Value;
    if TableWorkSpot.RecordCount = 1 then
      DataSet.FieldByName('WORKSPOT_CODE').Value :=
      	TableWorkSpot.FieldByName('WORKSPOT_CODE').Value;
    if QueryJobLU.RecordCount = 1 then
      DataSet.FieldByName('JOB_CODE').Value :=
      	QueryJobLU.FieldByName('JOB_CODE').Value;
    if QueryShiftLU.RecordCount = 1 then
      DataSet.FieldByName('SHIFT_NUMBER').Value:=
        QueryShiftLU.FieldByName('SHIFT_NUMBER').Value;
  end;
end;

procedure TTimeRecScanningDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  // RV077.3.
  if not TimeRecScanningDM.IsNoScannerWarning then
  begin
    SysUtils.Abort;
    Exit;
  end;
  NewEnteredDateOut := 0; // RV076.5.
  // MR:09-07-2003 Save it here.
  DefaultSaveCurrentRecord(DataSet);
  SystemDM.DefaultBeforeDelete(DataSet);
  if (DataSet.FieldByName('PROCESSED_YN').AsString = CHECKEDVALUE) and
    (SystemDM.ChangeScan=UNCHECKEDVALUE) then //The completed time rec cannot
  begin
    DisplayMessage(SPimsNoCompletedScans, mtinformation, [mbOk]);
    SysUtils.Abort;
    Exit;
  end;
  try
    // MR:25-10-2005
    CheckWorkspot(DataSet);
    // Fill Record with scan that must be deleted
    EmptyIDCard(EmployeeIDCard);
    GlobalDM.PopulateIDCard(EmployeeIDCard, DataSet);

    SystemDM.Pims.StartTransaction;
    ProcessCompletedScan(DeleteScan);
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except on E:EDBEngineError do
    SystemDM.Pims.Rollback;
  end;
end;

procedure TTimeRecScanningDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  // if part of filter changed
  inherited;
  if EmployeeIDcard.DateOut > 0 then
  begin
    try
      SystemDM.Pims.StartTransaction;
      ProcessCompletedScan(TimeScanAction);
      // 20014450.50
      RecalcEfficiency(DataSet);
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
    except
      on E: EDBENgineError do
      begin
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
        DataSet.Delete;
      end;
    end;
  end
  else
  begin
    // PIM-151 Related to this issue: Also do this when the scan is NOT closed.
    try
      // 20014450.50
      RecalcEfficiency(DataSet);
    except
      on E: EDBENgineError do
        WErrorLog(E.Message);
    end;
  end;
  //RV065.8.
  if FFoundMoreThan12hScan then
  begin
    FFoundMoreThan12hScan := False;
    AddRemainingScanPeriod();

    EmptyIDCard(EmployeeIDcard);
    CopyIDCard(EmployeeIDcard, NewIDCard);
    if EmployeeIDcard.DateOut > 0 then
    begin
      try
        SystemDM.Pims.StartTransaction;
        ProcessCompletedScan(AddScan);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      except
        on E: EDBENgineError do
        begin
          if SystemDM.Pims.InTransaction then
            SystemDM.Pims.Rollback;
          DataSet.Delete;
        end;
      end;
    end;
    DataSet.Refresh;
  end;

  DataSet.First;
  // MR:14-10-2004 Locate + datefield can give errors,
  // depending how the Windows-date-format been set.
  // Use 'ADateFmt'-routines to solve this.
  SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
  DataSet.Locate('DATETIME_IN;EMPLOYEE_NUMBER',
    VarArrayOf([
      FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, CurrentRecord.Datetime_In),
      CurrentRecord.Employee_Number]), [loCaseInsensitive]);
  SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
end;

procedure TTimeRecScanningDM.TableDetailBeforePost(DataSet: TDataSet);
var
  StartDate, EndDate: TDateTime;
begin
  // RV076.5. Use 'NewEnteredDateOut' to keep track of the new-dateout.
  if (ScanFilter.ScanFilter = NotComplete) or
    (ScanFilter.ScanFilter = ScanPerEmployee) then
  begin
    if Trunc(NewEnteredDateOut) = 0 then
      DataSet.FieldByName('DATETIME_OUT').Value := null;
  end;

  // MR:09-07-2003 Save it here!
  //car 18-9-2003 the old values are already changed with the last one
 //!! DefaultSaveCurrentRecord(DataSet);
  // User cannot set datetime-out to '0'.
  // MR:07-10-2003 Do this only if 'scanfilter=scanperemployee'.
  if (ScanFilter.ScanFilter = ScanPerEmployee) then
  begin
    // MR:08-10-2003 Is State not 'dsInsert'?
    // Inserting of a open scanning is allowed.
    if DataSet.State <> dsInsert  then
    begin
      // MR:08-10-2003
      // If old datetime-out is not 0 and new datetime-out is 0 then
      // user is trying to open a closed-scan.
      // This is not allowed.
      if
        (OldRecVal.DateTime_Out <> 0) and // RV075.11. This was 'remarked'.
        (DataSet.FieldByName('DATETIME_OUT').AsDateTime = 0) then
      begin
        DisplayMessage(SPimsCompletedToNotScans,mtInformation,[mbOk]);
        SysUtils.Abort;
        Exit;
      end;
    end;
  end;

  // RV072.2.
  // RV076.5. Use 'NewEnteredDateOut' to keep track of the new-dateout.
  if (ScanFilter.ScanFilter = NotComplete) or
    (ScanFilter.ScanFilter = ScanPerEmployee) then
  begin
    // When NewEnteredDateOut <> 0 do this:
    // Is date-part of datetime-out empty?
    // Then assign date-part of datetime-in to it, but keep the time-part.
    if Trunc(NewEnteredDateOut) <> 0 then
      if (Dataset.FieldByName('DATETIME_OUT').IsNull) or
        (Trunc(DataSet.FieldByName('DATETIME_OUT').AsDateTime) = 0) then
          DataSet.FieldByName('DATETIME_OUT').AsDateTime :=
            Trunc(NewEnteredDateOut) +
              Frac(DataSet.FieldByName('DATETIME_OUT').AsDateTime);
  end;

  //RV065.8.
  if (not Dataset.FieldByName('DATETIME_IN').IsNull) and
    (not Dataset.FieldByName('DATETIME_OUT').IsNull) and
    (Dataset.FieldByName('DATETIME_OUT').AsDateTime -
    Dataset.FieldByName('DATETIME_IN').AsDateTime > 1)
  then
  begin
    DisplayMessage(SPimsScanLongerThan24h, mtInformation,[mbOk]);
    SysUtils.Abort;
    Exit;
  end;
  // MR:29-11-2002
  if (not Dataset.FieldByName('DATETIME_IN').IsNull) and
    (not Dataset.FieldByName('DATETIME_OUT').IsNull) and
    (Dataset.FieldByName('DATETIME_OUT').AsDateTime -
    Dataset.FieldByName('DATETIME_IN').AsDateTime > 0.5) then
  begin
    //RV065.8. set the long scan found flag and commented the error message
    FFoundMoreThan12hScan := True;
    //DisplayMessage(SPimsScanTooBig,mtInformation,[mbOk]);
    //SysUtils.Abort;
    //Exit;
  end; // MR:29-11-2002

  // Pims -> Oracle
  if not CheckScanOverlap(DataSet) then
  begin
    DisplayMessage(SPimsScanOverlap, mtInformation, [mbOK]);
    SysUtils.Abort;
    Exit;
  end;
  if DataSet.State = dsInsert then
  begin
    if not CheckBeforeInsert(DataSet) then
    begin
      DisplayMessage(SPimsInvalidScan, mtInformation, [mbOK]);
      SysUtils.Abort;
      Exit;
    end
  end
  else
  begin
    if not CheckBeforeUpdate(DataSet) then
    begin
      DisplayMessage(SPimsInvalidScan, mtInformation, [mbOK]);
      SysUtils.Abort;
      Exit;
    end;
  end;

  if DataSet.FieldByName('DATETIME_OUT').IsNull then
    DataSet.FieldByName('PROCESSED_YN').AsString := UNCHECKEDVALUE
  else
    DataSet.FieldByName('PROCESSED_YN').AsString := CHECKEDVALUE;
  DefaultBeforePost(DataSet);
  if DataSet.State = dsEdit  then
    TimeScanAction := CompleteScan
  else
    TimeScanAction := AddScan;

  // MR:25-10-2005
  CheckWorkspot(DataSet);

  // populate New Employee ID Card.
  EmptyIDCard(EmployeeIDCard);
  GlobalDM.PopulateIDCard(EmployeeIDCard, DataSet);
  // TD-23386 This is done in PopulateIDCard
{
  // 27-01-2003 Get DepartmentCode from Workspot!
  EmployeeIDCard.DepartmentCode :=
    TableWorkspot.FieldByName('DEPARTMENT_CODE').AsString;
}
  //RV065.8. BEGIN
  if FFoundMoreThan12hScan then
  begin
    EmployeeIdCard.DateIn := Dataset.FieldByName('DATETIME_IN').AsDateTime;
    EmployeeIdCard.DateOut := Dataset.FieldByName('DATETIME_IN').AsDateTime + 0.5;
    EmptyIDCard(NewIDCard);
    GlobalDM.PopulateIDCard(NewIDCard, DataSet);
    // TD-23386 This is done in PopulateIDCard
{
    NewIDCard.DepartmentCode :=
      TableWorkspot.FieldByName('DEPARTMENT_CODE').AsString;
}
    NewIdCard.DateIn  := Dataset.FieldByName('DATETIME_IN').AsDateTime + 0.5;
    NewIdCard.DateOut := Dataset.FieldByName('DATETIME_OUT').AsDateTime;

    Dataset.FieldByName('DATETIME_OUT').AsDateTime :=
      Dataset.FieldByName('DATETIME_IN').AsDateTime + 0.5;
  end;
  //RV065.8. END

  CurrentRecord.DateTime_In :=
    DataSet.FieldByName('DATETIME_IN').AsDateTime;
  CurrentRecord.Employee_Number :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  inherited;
  //RV067.11.
  if FEnableValidation and (not CheckIfUseJobCode(
    DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString,
    DataSet.FieldByName('JOB_CODE').AsString)) then
  begin
    DisplayMessage(SPimsUseJobCode, mtInformation, [mbOk]);
    SysUtils.Abort;
    Exit; // 20014450.50
  end;

  // 20013489
  if SystemDM.UseShiftDateSystem then // 20014327
  begin
    AProdMinClass.GetShiftDay(EmployeeIDCard, StartDate, EndDate);
    EmployeeIDCard.ShiftDate := Trunc(StartDate);
    DataSet.FieldByName('SHIFT_DATE').AsDateTime := EmployeeIDCard.ShiftDate;
  end
  else
    DataSet.FieldByName('SHIFT_DATE').AsDateTime :=
      Trunc(DataSet.FieldByName('DATETIME_IN').AsDateTime);
  NewEnteredDateOut := 0; // RV076.5.
  MoveEfficiency(DataSet); // 20014550.50
end; // TableDetailBeforePost

procedure TTimeRecScanningDM.TableDetailBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  // 20012858.130.4. These must be opened here: Calculate fields.
  qryWorkspotLU.Open;
  qryJobLU.Open;
  qryShiftLU.Open;
//  if not TablePlantLU.Active then
//    TablePlantLU.Active := True;
//  if not TableWorkSpotLU.Active then
//    TableWorkSpotLU.Active := True;
//  if not TableJobLU.Active then
//    TableJobLU.Active := True;
//  FindShiftWorkSpots;
//  FindJobCodes;
end;

// Triggers on a field-change.
procedure TTimeRecScanningDM.TableDetailDATETIME_INChange(Sender: TField);
var
  AFilter: TDataFilter;
begin
  inherited;
  AFilter := ScanFilter;
  if (TField(Sender).AsDateTime <= NullDate) or
    ((TField(Sender).AsDateTime >= AFilter.StartDate) and
    (TField(Sender).AsDateTime <= AFilter.EndDate)) then
    Exit;
  // MR:31-3-2005 Use a procedure to set the date.
//  TimeRecScanningF_HND.dxDateEditScan.Date := TField(Sender).AsDateTime;
  // 20013489 Do not do this. Or it will changed the selectiion-date, when
  //          DateIN is changed.
  if not SystemDM.UseShiftDateSystem then // 20014327
    TimeRecScanningF_HND.AssignDateEditScan(TField(Sender).AsDateTime);
end;

// Triggers on a field-change.
procedure TTimeRecScanningDM.TableDetailSHIFT_NUMBERChange(Sender: TField);
var
  AFilter: TDataFilter;
begin
  inherited;
  if TField(Sender).AsInteger <> ScanFilter.ShiftNumber then
  begin
    AFilter := ScanFilter;
    AFilter.ShiftNumber := TField(Sender).AsInteger;
{!!!    TableFilterShift.Locate('SHIFT_NUMBER',AFilter.ShiftNumber,
    	[loCaseInsensitive]);}
    ScanFilter := AFilter;
  end;
end;

procedure TTimeRecScanningDM.TableDetailPLANT_CODEChange(Sender: TField);
begin
  inherited;
  DataSourceDetail.DataSet.FieldByName('WORKSPOT_CODE').Clear;
end;

// Triggers on a field-change.
// If workspot is changed with combobox, clear the job-field
// To avoid there is a jobcode that does not belong to a workspot.
procedure TTimeRecScanningDM.TableDetailWORKSPOT_CODEChange(
  Sender: TField);
begin
  inherited;
  DataSourceDetail.DataSet.FieldByName('JOB_CODE').Clear;
  SetJobCodeField(TField(Sender).AsString);
end;

procedure TTimeRecScanningDM.TableDetailJOB_CODEChange(Sender: TField);
begin
  inherited;
  if TField(Sender).IsNull then
    TField(Sender).AsString := DUMMYSTR;
end;

procedure TTimeRecScanningDM.ProcessCompletedScan(Action: TScanAction);
var
  StartDate, EndDate, OldStartDate, OldEndDate : TDateTime;
  OldEmployeeIDCard: TScannedIDCard;
  OldBookingDate, NewBookingDate: TDateTime;
  ProdOldDateIn, ProdOldDateOut: TDateTime;
  ProdNewDateIn, ProdNewDateOut: TDateTime;
  ProdDate1, ProdDate2, ProdDate3, ProdDate4: TDateTime;
  OldDateIn, OldDateOut, NewDateIn, NewDateOut: TDateTime;
  FromDate: TDateTime;
  ActionOld: TScanAction;
begin
  // This variable was not initialized.
  if SystemDM.UseShiftDateSystem then // 20014327
    OldBookingDate := OldRecVal.ShiftDate // 20013489
  else
    OldBookingDate := NullDate;
  // If it was a completed scan then
  // Unprocess the Old scan values
  EmptyIDCard(OldEmployeeIDCard);
  if (Action <> AddScan) and (OldRecVal.DateTime_Out > NullDate) then
  begin
    OldEmployeeIDCard.EmployeeCode := OldRecVal.Employee_Number;
    OldEmployeeIDCard.ShiftNumber := OldRecVal.Shift_Number;
    OldEmployeeIDCard.IDCard := OldRecVal.IDCard_IN;
    OldEmployeeIDCard.WorkSpotCode := OldRecVal.WorkSpot_Code;
    OldEmployeeIDCard.JobCode := OldRecVal.Job_Code;
    OldEmployeeIDCard.PlantCode := OldRecVal.Plant_Code;
    OldEmployeeIDCard.DateIn := OldRecVal.DateTime_IN;
    OldEmployeeIDCard.DateOut := OldRecVal.DateTime_Out;
    OldEmployeeIDCard.ShiftDate := OldRecVal.ShiftDate; // 20013489
    QueryDelete.Active;
    QueryDelete.SQL.Clear;
    // RV100. Added Contractgroup.
    QueryDelete.SQL.Add(
      'SELECT ' + NL +
      '  E.DEPARTMENT_CODE, P.INSCAN_MARGIN_EARLY, ' + NL +
      '  P.INSCAN_MARGIN_LATE, P.OUTSCAN_MARGIN_EARLY, ' + NL +
      '  P.OUTSCAN_MARGIN_LATE, E.CUT_OF_TIME_YN, ' + NL +
      '  E.CONTRACTGROUP_CODE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, PLANT P ' + NL +
      'WHERE ' + NL +
      '  E.EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  E.PLANT_CODE = P.PLANT_CODE ');
    QueryDelete.Prepare;
    QueryDelete.ParamByName('EMPNO').AsInteger :=
      OldEmployeeIDCard.EmployeeCode;
    QueryDelete.Active := True;
    if not QueryDelete.IsEmpty then
    begin
      // Take DepartmentCode from 'Workspot'-table!
      OldEmployeeIDCard.DepartmentCode :=
        TableWorkspot.FieldByName('DEPARTMENT_CODE').AsString;
      OldEmployeeIDCard.InscanEarly :=
        QueryDelete.FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
      OldEmployeeIDCard.InscanLate :=
        QueryDelete.FieldByName('INSCAN_MARGIN_LATE').AsInteger;
      OldEmployeeIDCard.OutScanEarly :=
        QueryDelete.FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
      OldEmployeeIDCard.OutScanLate :=
        QueryDelete.FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
      OldEmployeeIDCard.CutOfTime :=
        QueryDelete.FieldByName('CUT_OF_TIME_YN').AsString;
      // RV100.1. Added Contractgroup.
      OldEmployeeIDCard.ContractgroupCode :=
        QueryDelete.FieldByName('CONTRACTGROUP_CODE').AsString;
    end;
    QueryDelete.Active := False;
    // Compute Old OverTimePeriod
    // MR: 25-09-2003
    AProdMinClass.GetShiftDay(OldEmployeeIDCard, OldStartDate, OldEndDate);
    OldBookingDate := Trunc(OldStartDate);
    // Over Time Period
    GlobalDM.ComputeOvertimePeriod(OldStartDate, OldEndDate,
      OldEmployeeIDCard);
  end;
  // booking date
  // MR: 25-09-2003
  AProdMinClass.GetShiftDay(EmployeeIDCard, StartDate, EndDate);
  NewBookingDate :=  Trunc(StartDate);
  // Over Time Period
  GlobalDM.ComputeOvertimePeriod(StartDate, EndDate, EmployeeIDCard);

  // MR:05-09-2003
  // Determine dates for productionhour-recalculation
  ProdOldDateIn := NullDate;
  ProdOldDateOut := NullDate;
  ProdNewDateIn := NullDate;
  ProdNewDateOut := NullDate;

  // MR:15-09-2003
  OldDateIn := Trunc(OldEmployeeIDCard.DateIn);
  OldDateOut := Trunc(OldEmployeeIDCard.DateOut);
  NewDateIn := Trunc(EmployeeIDCard.DateIn);
  NewDateOut := Trunc(EmployeeIDCard.DateOut);

{$IFDEF DEBUG}
  WDebugLog('- OldBookingDate=' + DateToStr(OldBookingDate) +
    ' NewBookingDate=' + DateToStr(NewBookingDate)
    ); 
  WDebugLog('- OldOvertimePeriod=' + DateTimeToStr(OldStartDate) +
    ' - ' + DateTimeToStr(OldEndDate) +
    ' NewOvertimePeriod=' + DateTimeToStr(StartDate) +
    ' - ' + DateTimeToStr(EndDate)
    );
  WDebugLog('- OldDateIn=' + DateTimeToStr(OldEmployeeIDCard.DateIn) +
    ' OldDateOut=' + DateTimeToStr(OldEmployeeIDCard.DateOut) +
    ' NewDateIn=' + DateTimeToStr(EmployeeIDCard.DateIn) +
    ' NewDateOut=' + DateTimeToStr(EmployeeIDCard.DateOut)
    );
{$ENDIF}

  // Determine if Date-out is '00:00:00'
  if (Trunc(OldEmployeeIDCard.DateOut) <> 0) and
    (Frac(OldEmployeeIDCard.DateOut) = 0) then
    OldDateOut := OldDateOut - 1;
  // Determine if Date-out is '00:00:00'
  if (Trunc(EmployeeIDCard.DateOut) <> 0) and
    (Frac(EmployeeIDCard.DateOut) = 0) then
    NewDateOut := NewDateOut - 1;
  // if new scan is complete
  if (EmployeeIDCard.DateOut > NullDate) and (Action <> DeleteScan) then
  begin
    ProdNewDateOut := NewDateOut;
    if NewDateIn <> NewDateOut then
      ProdNewDateIn := NewDateIn;
  end;
  // if old scan is complete
  if OldEmployeeIDCard.DateOut > NullDate then
  begin
    // if new scan is not complete
    if (EmployeeIDCard.DateOut = NullDate) or (Action = DeleteScan) then
    begin
      ProdOldDateOut := OldDateOut;
      if OldDateIn <> OldDateOut then
        ProdOldDateIn := NewDateIn;
    end
    else
    begin
      if (OldDateOut <> NewDateIn) and
        (OldDateOut <> NewDateOut) then
        ProdOldDateOut := OldDateOut;
      if (OldDateIn <> OldDateOut) and
        (OldDateIn <> NewDateIn) and
        (OldDateIn <> NewDateOut) then
        ProdOldDateIn := OldDateIn;
    end;
  end;
  // MR:14-10-2003 If open scan is deleted then production of date-in should
  // be recalculated.
  if (EmployeeIDCard.DateOut = NullDate) and
    (Action = DeleteScan) then
    ProdNewDateOut := NewDateIn;
  // If old scan is complete and new scan not complete or
  // different overtime period, reprocess the old scan.
  // Also check if one of the productiondates must be reprocessed.
  // 20013489. Overnight-shift-system
  // When the ShiftNumber is changed of the scan, then
  // the old scan must first be deleted!
  // This must be done when the new shift is an overnight-shift,
  // which means it can store the record on the previous day, and it
  // will not look anymore to the old-record that was stored in the current day.
  if
    (OldEmployeeIDCard.DateOut > NullDate)
    and
    (
      (EmployeeIDCard.DateOut = NullDate)
      or
      (
        (
          SystemDM.UseShiftDateSystem and // 20014327
           (OldEmployeeIDCard.ShiftNumber <> EmployeeIDCard.ShiftNumber)
        )
        or
        (
          not SystemDM.UseShiftDateSystem
        )
      )
      or
      (Action = DeleteScan)
      or
      (OldStartDate <> StartDate)
    ) then
  begin
    // 20013489. Temporary set the Action to DeleteScan, to ensure
    // the old record is deleted.
    ActionOld := Action;
    if SystemDM.UseShiftDateSystem then // 20014327
      if (OldEmployeeIDCard.ShiftNumber <> EmployeeIDCard.ShiftNumber) then
        Action := DeleteScan;
    ProdDate1 := NullDate;
    ProdDate2 := NullDate;
    ProdDate3 := NullDate;
    ProdDate4 := NullDate;
    if (ProdNewDateOut <> NullDate) and
      (ProdNewDateOut >= Trunc(OldStartDate)) and
      (ProdNewDateOut <= Trunc(OldEndDate)) and
      (
        (ProdNewDateIn = NullDate) or
        ( (ProdNewDateIn >= Trunc(OldStartDate)) and
          (ProdNewDateIn <= Trunc(OldEndDate)) )
      ) then
    begin
      ProdDate1 := ProdNewDateOut;
      ProdNewDateOut := NullDate;
    end;
    if (ProdDate1 <> NullDate) and
      (ProdNewDateIn <> NullDate) and
      (ProdNewDateIn >= Trunc(OldStartDate)) and
      (ProdNewDateIn <= Trunc(OldEndDate)) then
    begin
      ProdDate2 := ProdNewDateIn;
      ProdNewDateIn := NullDate;
    end;
    if (ProdOldDateOut <> NullDate) and
      (ProdOldDateOut >= Trunc(OldStartDate)) and
      (ProdOldDateOut <= Trunc(OldEndDate)) and
      (
        (ProdOldDateIn = NullDate) or
        ( (ProdOldDateIn >= Trunc(OldStartDate)) and
          (ProdOldDateIn <= Trunc(OldEndDate)) )
      ) then
    begin
      ProdDate3 := ProdOldDateOut;
      ProdOldDateOut := NullDate;
    end;
    if (ProdDate3 <> NullDate) and
      (ProdOldDateIn <> NullDate) and
      (ProdOldDateIn >= Trunc(OldStartDate)) and
      (ProdOldDateIn <= Trunc(OldEndDate)) then
    begin
      ProdDate4 := ProdOldDateIn;
      ProdOldDateIn := NullDate;
    end;
{$IFDEF DEBUG}
  WDebugLog('TimeRecScanning (1)');
{$ENDIF}
    // 20013489 Argument 'ProcessSalary' is depending on 'IsScanning', but
    //          why would you add/change something for a non-scanner?
    GlobalDM.UnprocessProcessScans(QueryWork,
      OldEmployeeIDcard,
      OldBookingDate, ProdDate1, ProdDate2, ProdDate3, ProdDate4,
      // 20013489 Set always to True here instead of using IsScanning
      True,
//      FScanFilter.IsScanning, // MR:23-1-2004
      (Action = DeleteScan),
      False);
    Action := ActionOld;
  end;
  FromDate := NewBookingDate;
  if (OldEmployeeIDCard.DateOut > NullDate) and
    (EmployeeIDCard.DateOut > NullDate) and
    (OldStartDate = StartDate) and
    (OldEndDate = EndDate) and
    (Action <> DeleteScan) then
  begin
    if OldBookingDate < NewBookingDate then
      FromDate := OldBookingDate;
  end;
  if (EmployeeIDCard.DateOut = NullDate) and
    (Action = DeleteScan) then
    FromDate := Trunc(EmployeeIDCard.DateIn);
  // If new scan is complete then reprocess new scan.
  // Also check if one of the productiondates must be reprocessed.
  // MR:14-10-2003 Also: If open scan is deleted then
  // date-in should be recalculated.
  if ((EmployeeIDCard.DateOut > NullDate) and (Action <> DeleteScan)) or
    ((EmployeeIDCard.DateOut = NullDate) and (Action = DeleteScan)) then
  begin
    ProdDate1 := NullDate;
    ProdDate2 := NullDate;
    ProdDate3 := NullDate;
    ProdDate4 := NullDate;
    if (ProdNewDateOut <> NullDate) and
      (ProdNewDateOut >= Trunc(StartDate)) and
      (ProdNewDateOut <= Trunc(EndDate)) and
      (
        (ProdNewDateIn = NullDate) or
        ( (ProdNewDateIn >= Trunc(StartDate)) and
          (ProdNewDateIn <= Trunc(EndDate)) )
      ) then
    begin
      ProdDate1 := ProdNewDateOut;
      ProdNewDateOut := NullDate;
    end;
    if (ProdDate1 <> NullDate) and
      (ProdNewDateIn <> NullDate) and
      (ProdNewDateIn >= Trunc(StartDate)) and
      (ProdNewDateIn <= Trunc(EndDate)) then
    begin
      ProdDate2 := ProdNewDateIn;
      ProdNewDateIn := NullDate;
    end;
    if (ProdOldDateOut <> NullDate) and
      (ProdOldDateOut >= Trunc(StartDate)) and
      (ProdOldDateOut <= Trunc(EndDate)) and
      (
        (ProdOldDateIn = NullDate) or
        ( (ProdOldDateIn >= Trunc(StartDate)) and
          (ProdOldDateIn <= Trunc(EndDate)) )
      ) then
    begin
      ProdDate3 := ProdOldDateOut;
      ProdOldDateOut := NullDate;
    end;
    if (ProdDate3 <> NullDate) and
      (ProdOldDateIn <> NullDate) and
      (ProdOldDateIn >= Trunc(StartDate)) and
      (ProdOldDateIn <= Trunc(EndDate)) then
    begin
      ProdDate4 := ProdOldDateIn;
      ProdOldDateIn := NullDate;
    end;
    if SystemDM.UseShiftDateSystem then // 20014327
    begin
      // 20013489
      // Handle change of shiftnumber from old record.
      // First the hours were deleted, but now they must be created.
      if (OldEmployeeIDCard.ShiftNumber <> EmployeeIDCard.ShiftNumber) then
      begin
        // Assign one of the ProdDates, to force the creation of the PHE-record.
        if ProdDate1 = NullDate then
          ProdDate1 := Trunc(EmployeeIDCard.DateIn)
        else
          if ProdDate2 = NullDate then
            ProdDate2 := Trunc(EmployeeIDCard.DateIn)
          else
            if ProdDate3 = NullDate then
              ProdDate3 := Trunc(EmployeeIDCard.DateIn)
            else
              if ProdDate4 = NullDate then
                ProdDate4 := Trunc(EmployeeIDCard.DateIn);
        // Handle when scan goes through the night.
        if Trunc(EmployeeIDCard.DateIn) <> Trunc(EmployeeIDCard.DateOut) then
        begin
          if ProdDate1 = NullDate then
            ProdDate1 := Trunc(EmployeeIDCard.DateOut)
          else
            if ProdDate2 = NullDate then
              ProdDate2 := Trunc(EmployeeIDCard.DateOut)
            else
              if ProdDate3 = NullDate then
                ProdDate3 := Trunc(EmployeeIDCard.DateOut)
              else
                if ProdDate4 = NullDate then
                  ProdDate4 := Trunc(EmployeeIDCard.DateOut);
        end; // if
      end; // if
    end; // if SystemDM.UseShiftDateSystem
{$IFDEF DEBUG}
  WDebugLog('TimeRecScanning (2)');
{$ENDIF}
    // 20013489 Argument 'ProcessSalary' is depending on 'IsScanning', but
    //          why would you add/change changings for a non-scanner?
    GlobalDM.UnprocessProcessScans(QueryWork, EmployeeIDcard,
      FromDate, ProdDate1, ProdDate2, ProdDate3, ProdDate4,
      // 20013489 Set always to True here instead of using IsScanning
      True,
      // FScanFilter.IsScanning, // MR:23-1-2004
      (Action = DeleteScan),
      False);
  end;
  // 20013489 Overnight-Shift-System
  // Do not do this anymore! (TimeRecScanning(3))
  // The hours are already processed for production/salary on the
  // correct booking date with checks for overnight-scans.
  // When this is also done, then it gives double production hours!
(*
{$IFDEF DEBUG}
  WDebugLog('- ProdOldDateIn=' + DateTimeToStr(ProdOldDateIn) +
    ' ProdOldDateOut=' + DateTimeToStr(ProdOldDateOut) +
    ' ProdNewDateIn=' + DateTimeToStr(ProdNewDateIn) +
    ' ProdNewDateOut=' + DateTimeToStr(ProdNewDateOut)
    );
{$ENDIF}
*)
  if not SystemDM.UseShiftDateSystem then // 20014327
  begin
    // If any of the dates on which production hours have to be processed are
    // not done yet, do them here.
    if (ProdOldDateIn <> NullDate) or
      (ProdOldDateOut <> NullDate) or
      (ProdNewDateIn <> NullDate) or
      (ProdNewDateOut <> NullDate) then
    begin
{$IFDEF DEBUG}
  WDebugLog('TimeRecScanning (3)');
{$ENDIF}
      GlobalDM.UnprocessProcessScans(QueryWork, EmployeeIDcard,
        NullDate, ProdOldDateIn, ProdOldDateOut, ProdNewDateIn, ProdNewDateOut,
        False,
        (Action = DeleteScan),
        False);
    end; // if
  end; // if not SystemDM.UseShiftDateSystem

  // RV033.4. Recalculate Earned TFT Hours.
  // Due to a bug (RV033.5) the AbsenceTotal was not updated correct.
  // The following call is not needed because of this error.
(*  GlobalDM.RecalcEarnedTFTHours(EmployeeIDCard.EmployeeCode,
    EmployeeIDCard.DateIn, EmployeeIDCard.DateOut); *)
  // RV045.1. Always do this!
  GlobalDM.RecalcEarnedTFTHours(EmployeeIDCard.EmployeeCode,
    EmployeeIDCard.DateIn, EmployeeIDCard.DateOut);
  // RV071.6. Also do this!
  GlobalDM.RecalcEarnedSWWHours(EmployeeIDCard.EmployeeCode,
    EmployeeIDCard.DateIn, EmployeeIDCard.DateOut);

  // RV063.1.
  // MRA:6-NOV-2013 This gives an exception! Disable it for now.
//  try
//    UpdatePQBasedOnScan;
//  except
//  end;

  // MR:05-09-2003
(*
  if OldEmployeeIDCard.DateOut > NullDate then // it was a completed scan
    if (Action <> DeleteScan) and (StartDate=OldStartDate) and
      (EndDate = OldEndDate) and (EmployeeIDCard.DateOut > NullDate) and
      (NewBookingDay = OldBookingDay) then
      // if the same overtime period do not recalculate the period will be done
      // at new scan if completed - improving the process speed.
      UnprocessProcessScans(QueryWork, OldEmployeeIDcard.DateIn,
        OldEmployeeIDcard, False, False)
    else
      UnprocessProcessScans(QueryWork, OldEmployeeIDcard.DateIn,
        OldEmployeeIDcard, True, False);

  // Processing the new scan values including the recalculation for new
  if (Action <> DeleteScan) and (EmployeeIDCard.DateOut > NullDate) then
    UnprocessProcessScans(QueryWork, OldEmployeeIDcard.DateIn, EmployeeIDCard,
      True,True);
*)
end;
(*
// Check for closed Scan
function TTimeRecScanningDM.CheckScanRecOnDay(ADate: TDateTime;
  EmployeeCode: Integer): boolean;
var
  SDateTime, EDateTime: TDateTime;
begin
  Result := False;
  SDateTime := ADate;
  GetStartEndDay(SDateTime, EDateTime);
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    '  EMPLOYEE_NUMBER ' + NL +
    'FROM ' + NL +
    '  TIMEREGSCANNING ' + NL +
    'WHERE ' + NL +
    '  (DATETIME_IN >= :START AND ' + NL +
    '  DATETIME_IN <= :END AND ' + NL +
    '  DATETIME_OUT IS NOT NULL) AND ' + NL +
    '  EMPLOYEE_NUMBER = :EMPNO ');
  QueryWork.ParamByName('START').AsDateTime := SDateTime;
  QueryWork.ParamByName('END').AsDateTime := EDateTime;
  QueryWork.ParamByName('EMPNO').AsInteger := EmployeeCode;
  QueryWork.Open;
  if not QueryWork.IsEmpty then
    Result := True;
end;
*)
procedure TTimeRecScanningDM.SetFilter(Value: TDataFilter);
begin
  ReplaceTime(Value.StartDate, EncodeTime(0,0,0,0));
  FScanFilter := Value;
  GetStartEndDay(FScanFilter.StartDate, FScanFilter.EndDate);
  // in case of insert
  if not FScanFilter.Active then
    Exit;
  DataSourceDetail.Enabled := False;
  DataSourceMaster.Enabled := False;
  case ScanFilter.ScanFilter of
  NotComplete:
    if not (DataSourceDetail.DataSet.State in [dsEdit, dsInsert]) then
    begin
      // MR:05-02-2004 filter also on date for Not-Complete.
      if ScanFilter.NotCompleteFilterActive then
      begin
        SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
        DataSourceDetail.DataSet.Filter := Format('PROCESSED_YN = ''%s''',
          [UNCHECKEDVALUE]) +
          ' AND ' +
          '(DATETIME_IN >= ' +
          SystemDM.ADateFmt.Quote +
          FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.StartDate) +
          SystemDM.ADateFmt.Quote + ')' +
          ' AND ' +
          '(DATETIME_IN <= ' +
          SystemDM.ADateFmt.Quote +
          FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.EndDate) +
          SystemDM.ADateFmt.Quote + ')';
        SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
      end
      else
        DataSourceDetail.DataSet.Filter := Format('PROCESSED_YN = ''%s''',
          [UNCHECKEDVALUE]);
      DataSourceDetail.DataSet.Refresh;
      DataSourceDetail.DataSet.First;
    end;
  ScanPerEmployee:
    if not (DataSourceDetail.DataSet.State in [dsEdit, dsInsert]) then
    begin
      // 20013489 Filter on SHIFT_DATE instead of on DATETIME_IN.
      //          Note: Only compare Startdate equals ShiftDate.
      SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
      if SystemDM.UseShiftDateSystem then // 20014327
      begin
        DataSourceDetail.DataSet.Filter := Format('EMPLOYEE_NUMBER = %d',
          [ScanFilter.EmployeeNumber]) + ' AND ' +
          '(SHIFT_DATE = ' +
          SystemDM.ADateFmt.Quote +
          FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.StartDate) +
          SystemDM.ADateFmt.Quote + ')';
      end
      else
      begin
        DataSourceDetail.DataSet.Filter := Format('EMPLOYEE_NUMBER = %d',
          [ScanFilter.EmployeeNumber]) + ' AND ' +
          '(DATETIME_IN >= ' +
          SystemDM.ADateFmt.Quote +
          FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.StartDate) +
          SystemDM.ADateFmt.Quote + ')' +
          ' AND ' +
          '(DATETIME_IN <= ' +
          SystemDM.ADateFmt.Quote +
          FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.EndDate) +
          SystemDM.ADateFmt.Quote + ')';
      end; // if
      SystemDM.ADateFmt.SwitchDateTimeFmtWindows;

//      DataSourceDetail.DataSet.Refresh;
//      DataSourceDetail.DataSet.First;
    end;
  EarlyLate:
    if not (TableMaster.State in [dsEdit, dsInsert]) then
    begin
      SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
      TableMaster.Filter :=
        '(REQUEST_DATE >= ' +
        SystemDM.ADateFmt.Quote +
        FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.StartDate) +
        SystemDM.ADateFmt.Quote + ')' +
        ' AND ' +
        '(REQUEST_DATE <= ' +
        SystemDM.ADateFmt.Quote +
        FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, ScanFilter.EndDate) +
        SystemDM.ADateFmt.Quote + ')';
      SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
      TableMaster.Refresh;
    end;
  end;
  DataSourceDetail.Enabled := True;
  DataSourceMaster.Enabled := True;
end;

procedure TTimeRecScanningDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV070.1.
  SystemDM.PlantTeamFilterEnable(TableDetail);
  // RV073.3.
  // RV092.15. Only filter workspots when plan-in-other-plants is not used.
  if SystemDM.EmployeesAcrossPlantsYN = UNCHECKEDVALUE then
  begin
    // RV092.15. Also filter on plants.
    SystemDM.PlantTeamFilterEnable(QueryPlantLU);
    SystemDM.PlantTeamFilterEnable(TableWorkspot);
  end;
  //
  FEnableValidation := True;
  // set default values for filter
  FScanFilter.Active := False;
  FScanFilter.StartDate := Now;
  FScanFilter.ScanFilter := NotComplete;
  FScanFilter.ShiftNumber := 0;
  FScanFilter.EmployeeNumber := 0;
  FScanFilter.IsScanning := False; // MR:23-1-2004
  FScanFilter.ScanAction := AddScan;
  FScanFilter.BookProdHrs := False; // RV077.3.
  FScanFilter.PlantCode := '';  // 20011800
  // TD-22538 Use a fixed query in query-components with USER_NAME-parameter!
  //          OLD CODE:
(*
  //CAR 550274 - team selection
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryFilterEmployee.Close;
    QueryFilterEmployee.SQL.Clear;
    //Car 3.3.2004 - bug corrected
    QueryFilterEmployee.SQL.Add(
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME,' + NL +
      '  E.DESCRIPTION, E.PLANT_CODE, P.DESCRIPTION AS PLANTLU, ' + NL +
      '  E.IS_SCANNING_YN, E.BOOK_PROD_HRS_YN ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, PLANT P, TEAMPERUSER T ' + NL +
      'WHERE ' + NL +
      '  E.PLANT_CODE = P.PLANT_CODE AND ' + NL +
      '  T.USER_NAME = :USER_NAME AND ' + NL +
      '  E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'ORDER BY ' + NL +
      '  E.EMPLOYEE_NUMBER');
    QueryFilterEmployee.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser;
    QueryFilterEmployee.Prepare;
    QueryFilterEmployee.Open;
    QueryEmployeeLU.Close;
    QueryEmployeeLU.SQL.Clear;
    QueryEmployeeLU.SQL.Add(
      'SELECT ' +
      '  E.EMPLOYEE_NUMBER, DESCRIPTION, ' +
      '  E.PLANT_CODE EMP_PLANT_CODE, ' +
      '  E.TEAM_CODE, E.DEPARTMENT_CODE ' +
      'FROM ' +
      '  EMPLOYEE E, TEAMPERUSER T ' +
      'WHERE ' +
      '  T.USER_NAME = :USER_NAME AND ' +
      '  E.TEAM_CODE = T.TEAM_CODE ' +
      'ORDER BY ' +
      '  E.EMPLOYEE_NUMBER');
    QueryEmployeeLU.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryEmployeeLU.Prepare;
    QueryEmployeeLU.Open;
  end;
*)
  // TD-22538 Use a fixed query in query-components with USER_NAME-parameter!
  //          NEW CODE:
  QueryFilterEmployee.Close;
  QueryFilterEmployee.ParamByName('USER_NAME').AsString :=
    SystemDM.UserTeamLoginUser;
  QueryFilterEmployee.Prepare;
  QueryFilterEmployee.Open;
  QueryEmployeeLU.Close;
  QueryEmployeeLU.ParamByName('USER_NAME').AsString :=
    SystemDM.UserTeamLoginUser;
  QueryEmployeeLU.Prepare;
  QueryEmployeeLU.Open;
end;

function TTimeRecScanningDM.CheckIfUseJobCode(Plant, Workspot,
  JobCode: String): Boolean;
begin
  Result := False;
  // MR:03-02-2005 Search again! Otherwise it can look at a wrong
  // workspot.
  if TableWorkspot.FindKey([Plant, Workspot]) then
  begin
    if TableWorkspot.FieldByName('USE_JOBCODE_YN').AsString =
      CHECKEDVALUE then
    begin
      if (JobCode <> '0') and (JobCode <> '') then
        Result := True;
    end
    else
      Result := True;
  end;
end;

procedure TTimeRecScanningDM.TableFilterEmployeeAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  FScanFilter.EmployeeNumber :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FScanFilter.PlantCode :=
    DataSet.FieldByName('PLANT_CODE').AsString; // 20011800
end;

procedure TTimeRecScanningDM.TableFilterShiftAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  FScanFilter.ShiftNumber := DataSet.FieldByName('SHIFT_NUMBER').AsInteger;
end;

(*
procedure TTimeRecScanningDM.FindJobCodes;
begin
  if QueryWorkSpot.Active and (not QueryWorkSpot.IsEmpty) then
  begin
    QueryJob.Active := False;
    QueryJob.ParamByName('PCODE').AsString :=
      QueryWorkSpot.FieldByName('PLANT_CODE').AsString;
    QueryJob.ParamByName('WCODE').AsString :=
      QueryWorkSpot.FieldByName('WORKSPOT_CODE').AsString;
    QueryJob.Active := True;
  end;
end;
*)
(*
procedure TTimeRecScanningDM.FindShiftWorkSpots;
begin
  if TablePlant.active and (not TablePlant.IsEmpty) then
  begin
    QueryWorkSpot.Active := False;
    QueryWorkSpot.ParamByName('PCODE').AsString :=
    	TablePlant.FieldByName('PLANT_CODE').AsString;
    QueryWorkSpot.Active := True;
    QueryShift.Active := False;
    QueryShift.ParamByName('PCODE').AsString :=
    	TablePlant.FieldByName('PLANT_CODE').AsString;
    QueryShift.Active := True;
  end;
end;
*)

procedure TTimeRecScanningDM.TablePlantAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  FindShiftWorkSpots;
end;

procedure TTimeRecScanningDM.QueryWorkSpotAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  FindJobCodes;
end;

// If there is just 1 shift for the given plant, assign this shift
procedure TTimeRecScanningDM.IfUniqueSetShift;
begin
  if (QueryShiftLU.RecordCount = 1) and
    (DataSourceDetail.Dataset.State in [dsEdit, dsInsert]) then
    DataSourceDetail.Dataset.FieldByName('SHIFT_NUMBER').Value :=
      QueryShiftLU.FieldByName('SHIFT_NUMBER').Value;
end;

// If there is just 1 job for the given workspot, assign this job.
procedure TTimeRecScanningDM.IfUniqueSetJob;
begin
  if (QueryJobLU.RecordCount = 1) and
    (DataSourceDetail.Dataset.State in [dsEdit, dsInsert]) then
    DataSourceDetail.Dataset.FieldByName('JOB_CODE').Value :=
      QueryJobLU.FieldByName('JOB_CODE').Value;
end;

procedure TTimeRecScanningDM.DefaultSaveCurrentRecord(DataSet: TDataSet);
begin
  NewEnteredDateOut := 0; // RV076.5.
  OldRecVal.DateTime_IN := DataSet.FieldByName('DATETIME_IN').AsDateTime;
  OldRecVal.DateTime_Out := DataSet.FieldByName('DATETIME_OUT').AsDateTime;
  OldRecVal.Employee_Number := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  OldRecVal.Plant_Code := DataSet.FieldByName('PLANT_CODE').AsString;
  OldRecVal.WorkSpot_Code := DataSet.FieldByName('WORKSPOT_CODE').AsString;
  OldRecVal.Job_Code := DataSet.FieldByName('JOB_CODE').AsString;
  OldRecVal.Shift_Number := DataSet.FieldByName('SHIFT_NUMBER').AsInteger;
  OldRecVal.Processed := DataSet.FieldByName('PROCESSED_YN').AsBoolean;
  OldRecVal.IDCard_IN := DataSet.FieldByName('IDCARD_IN').AsString;
  OldRecVal.IDCard_Out := DataSet.FieldByName('IDCARD_OUT').AsString;
  OldRecVal.ShiftDate := DataSet.FieldByName('SHIFT_DATE').AsDateTime; // 20013489
end;

procedure TTimeRecScanningDM.SetJobCodeField(WorkspotCode: string);
begin
//  if not QueryWorkspotLU.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString,
//  WorkspotCode]) then
//  begin
//    TableDetail.FieldByName('JOB_CODE').Clear;
//    Exit;
//  end;
  if TableWorkspot.FieldByName('USE_JOBCODE_YN').AsString = UNCHECKEDVALUE then
    TableDetail.FieldByName('JOB_CODE').AsString := DUMMYSTR
end;

(*
procedure TTimeRecScanningDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  // MR:09-07-2003 Don't do this here...
//  DefaultSaveCurrentRecord(DataSet);
end;
*)

procedure TTimeRecScanningDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  // Close the queries
  QueryEmployeeLU.Close;
  QueryPlantLU.Close;
  // MR:04-06-2004 We use 'TableWorkspot' instead of this query.
//  QueryWorkspotLU.Close;
  QueryJobLU.Close;
  QueryShiftLU.Close;
  // Close the TClientDataSets
  cdsFilterEmployee.Close;
  cdsFilterShift.Close;
  //CAR 550274 - team selection
  QueryFilterEmployee.Close;
  QueryEmployeeLU.Close;

  QueryFilterEmployee.Unprepare;
  QueryEmployeeLU.Unprepare;
end;

//car 18-8-2003 - save the old values only for tab 2 - set afterscroll message
// of TableDetail
procedure TTimeRecScanningDM.DefaultAfterScroll(DataSet: TDataSet);
begin
  inherited;
  // MR:31-3-2005
  // Instead of 'TimeRecScanningF' we use the variable
  // named 'TimeRecScanningF_HND'
//  if TimeRecScanningF_HND.PageControl2.ActivePageIndex = 1 then
  // Also, we can use 'scanfilter' to see what pagecontrol is active.
  // 20014450.50 Do NOT do this when in edit/insert mode!
  if not (DataSet.State in [dsEdit, dsInsert]) then
    if ScanFilter.ScanFilter = ScanPerEmployee then
      DefaultSaveCurrentRecord(DataSet);
end;

procedure TTimeRecScanningDM.FilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := cdsFilterEmployee.FindKey([DataSet.
    FieldByName('EMPLOYEE_NUMBER').AsInteger]);
end;

procedure TTimeRecScanningDM.TableDetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  // MR:23-03-2004 Reset the filter, so it is set to
  //              'all not completed records'. This to prevent
  //              an error about 'record/key deleted'.
  // MR:31-3-2005 Use function/procedure to set the checkbox.
  if ScanFilter.ScanFilter = NotComplete then
    if not TimeRecScanningF_HND.CBoxAllNotCompleteResult then
      TimeRecScanningF_HND.AssignCBoxAllNotComplete(True);
//    if not TimeRecScanningF_HND.cBoxAllNotComplete.Checked then
//      TimeRecScanningF_HND.cBoxAllNotComplete.Checked := True;
end;

// Pims -> Oracle
function TTimeRecScanningDM.CheckBeforeInsert(ADataSet: TDataSet): Boolean;
begin
  if ADataSet.FieldByName('DATETIME_OUT').AsDateTime = 0 then
  begin
    qryTRS_BICheck1.Close;
    qryTRS_BICheck1.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryTRS_BICheck1.ParamByName('DATETIME_IN').AsDateTime :=
      ADataSet.FieldByName('DATETIME_IN').AsDateTime;

    // RV076.7.
    // Also look for open scans for the last 12 hours!
    // To prevent inserting multiple open scans!
//    qryTRS_BICheck1.ParamByName('DATEFROM').AsDateTime :=
//      ADataSet.FieldByName('DATETIME_IN').AsDateTime - 0.5;

    qryTRS_BICheck1.Open;
    Result := not (qryTRS_BICheck1.FieldByName('RECCOUNT').AsFloat > 0);
    qryTRS_BICheck1.Close;

// RV076.7.
// This does not work correct: To be investigated.
{
SELECT
  COUNT(*) RECCOUNT
FROM
  TIMEREGSCANNING T
WHERE
  (T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND
  (
    (T.DATETIME_OUT IS NOT NULL) AND
    (T.DATETIME_IN < :DATETIME_IN) AND
    (T.DATETIME_OUT > :DATETIME_IN)
  )
  OR
  (
    (T.DATETIME_OUT IS NULL) AND
    (T.DATETIME_IN >= :DATEFROM)
  )
}
  end
  else
  begin
    qryTRS_BICheck2.Close;
    qryTRS_BICheck2.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryTRS_BICheck2.ParamByName('DATETIME_IN').AsDateTime :=
      ADataSet.FieldByName('DATETIME_IN').AsDateTime;
    qryTRS_BICheck2.ParamByName('DATETIME_OUT').AsDateTime :=
      ADataSet.FieldByName('DATETIME_OUT').AsDateTime;
    qryTRS_BICheck2.Open;
    Result := not (qryTRS_BICheck2.FieldByName('RECCOUNT').AsFloat > 0);
    qryTRS_BICheck2.Close;
  end;
end;

// Pims -> Oracle
function TTimeRecScanningDM.CheckBeforeUpdate(ADataSet: TDataSet): Boolean;
begin
  if ADataSet.FieldByName('DATETIME_OUT').AsDateTime = 0 then
  begin
    qryTRS_BUCheck1.Close;
    qryTRS_BUCheck1.ParamByName('OLD_EMPLOYEE_NUMBER').AsInteger :=
      OldRecVal.Employee_Number;
    qryTRS_BUCheck1.ParamByName('OLD_DATETIME_IN').AsDateTime :=
      OldRecVal.DateTime_IN;
    qryTRS_BUCheck1.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
      ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryTRS_BUCheck1.ParamByName('NEW_DATETIME_IN').AsDateTime :=
      ADataSet.FieldByName('DATETIME_IN').AsDateTime;
    qryTRS_BUCheck1.Open;
    Result := not (qryTRS_BUCheck1.FieldByName('RECCOUNT').AsFloat > 0);
    qryTRS_BUCheck1.Close;
  end
  else
  begin
    qryTRS_BUCheck2.Close;
    qryTRS_BUCheck2.ParamByName('OLD_EMPLOYEE_NUMBER').AsInteger :=
      OldRecVal.Employee_Number;
    qryTRS_BUCheck2.ParamByName('OLD_DATETIME_IN').AsDateTime :=
      OldRecVal.DateTime_IN;
    qryTRS_BUCheck2.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
      ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryTRS_BUCheck2.ParamByName('NEW_DATETIME_IN').AsDateTime :=
      ADataSet.FieldByName('DATETIME_IN').AsDateTime;
    qryTRS_BUCheck2.ParamByName('NEW_DATETIME_OUT').AsDateTime :=
      ADataSet.FieldByName('DATETIME_OUT').AsDateTime;
    qryTRS_BUCheck2.Open;
    Result := not (qryTRS_BUCheck2.FieldByName('RECCOUNT').AsFloat > 0);
    qryTRS_BUCheck2.Close;
  end;
end;

function TTimeRecScanningDM.CheckScanOverlap(ADataSet: TDataSet): Boolean;
begin
  Result := not ((ADataSet.FieldByName('DATETIME_IN').AsDateTime >=
    ADataSet.FieldByName('DATETIME_OUT').AsDateTime) and
    (ADataSet.FieldByName('DATETIME_OUT').AsDateTime <> 0));
end;

// MR:25-10-2005 TableWorkspot is not always 'looking' at the correct
// workspot, therefore we search it here again.
procedure TTimeRecScanningDM.CheckWorkspot(DataSet: TDataSet);
begin
  if not ((TableWorkspot.FieldByName('PLANT_CODE').AsString =
    DataSet.FieldByName('PLANT_CODE').AsString) and
    (TableWorkspot.FieldByName('WORKSPOT_CODE').AsString =
    DataSet.FieldByName('WORKSPOT_CODE').AsString)) then
  begin
    TableWorkspot.FindKey([
      DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('WORKSPOT_CODE').AsString
      ]);
  end;
end;
//RV065.8.
//when a scan longer than 12h was found is splitted in two scans
//- a 12h scan
//- the rest of the scan which is entered using this function
procedure TTimeRecScanningDM.AddRemainingScanPeriod;
var
  StartDate, EndDate: TDateTime;
begin
  try
    // 20013489
    if SystemDM.UseShiftDateSystem then // 20014327
    begin
      AProdMinClass.GetShiftDay(NewIDCard, StartDate, EndDate);
      NewIDCard.ShiftDate := Trunc(StartDate);
    end
    else
      NewIDCard.ShiftDate := Trunc(NewIDCard.DateIn);
    with QueryInsertScanning do
    begin
      Close;
      Prepare;
      ParamByName('DATETIME_IN').AsDateTime := NewIDCard.DateIn;
      ParamByName('DATETIME_OUT').AsDateTime   := NewIDCard.DateOut;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := NewIDCard.EmployeeCode;
      ParamByName('PLANT_CODE').AsString    := NewIDCard.PlantCode;
      ParamByName('WORKSPOT_CODE').AsString := NewIDCard.WorkSpotCode;
      ParamByName('JOB_CODE').AsString      := NewIDCard.JobCode;
      ParamByName('SHIFT_NUMBER').AsInteger := NewIDCard.ShiftNumber;
      ParamByName('PROCESSED_YN').AsString  := 'Y';
      ParamByName('IDCARD_IN').AsString     := NewIDCard.IDCard;
      ParamByName('IDCARD_OUT').AsString    := NewIDCard.IDCard;
      ParamByName('MUTATOR').AsString       := SystemDM.CurrentProgramUser;
      // 20013489
      ParamByName('SHIFT_DATE').AsDateTime  := NewIDCard.ShiftDate;
      ExecSQL;
    end;
  except
    on E: Exception do
      DisplayMessage(E.Message, mtInformation, [mbOk])
  end;
end;

// RV063.1. Update PQ-records based on scans.
procedure TTimeRecScanningDM.UpdatePQBasedOnScan;
  procedure UpdatePQ;
  begin
    with qryUpdatePQ do
    begin
      try
        // Search params
        ParamByName('START_DATE').AsDateTime :=
          qryPQ.FieldByName('START_DATE').AsDateTime;
        ParamByName('PLANT_CODE').AsString := EmployeeIDCard.PlantCode;
        ParamByName('WORKSPOT_CODE').AsString := EmployeeIDCard.WorkSpotCode;
        ParamByName('JOB_CODE').AsString :=
          qryPQ.FieldByName('JOB_CODE').AsString;
        ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
        // Update params
        ParamByName('TRS_JOB_CODE').AsString := EmployeeIDCard.JobCode;
        ParamByName('SHIFT_NUMBER').AsInteger := EmployeeIDCard.ShiftNumber;
        ParamByName('MUTATIONDATE').AsDateTime := Now;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
      except
        // Ignore error. Can be a duplicate value.
      end;
    end;
  end;
begin
  with qryPQ do
  begin
    ParamByName('DATEFROM').AsDateTime := EmployeeIDCard.DateIn;
    ParamByName('DATETO').AsDateTime := EmployeeIDCard.DateOut;
    ParamByName('PLANT_CODE').AsString := EmployeeIDCard.PlantCode;
    ParamByName('WORKSPOT_CODE').AsString := EmployeeIDCard.WorkSpotCode;
    ParamByName('NO_SCAN_JOB_CODE').AsString := NO_SCAN;
//RV067.11.    ParamByName('SHIFT_NUMBER').AsInteger := EmployeeIDCard.ShiftNumber;
    ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
    Open;
    while not Eof do
    begin
      UpdatePQ;
      Next;
    end;
  end;
end;

// RV070.1.
procedure TTimeRecScanningDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantDeptTeamFilter(DataSet,
    'EMP_PLANT_CODE', 'DEPARTMENT_CODE');
end;

// RV073.3.
procedure TTimeRecScanningDM.TableWorkspotFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV092.15. Only filter workspots when plan-in-other-plants is not used.
  if SystemDM.EmployeesAcrossPlantsYN = UNCHECKEDVALUE then
    Accept := SystemDM.PlantDeptTeamFilter(DataSet,
      'PLANT_CODE', 'DEPARTMENT_CODE')
  else
    Accept := True;
end;

procedure TTimeRecScanningDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  // RV077.3.
  if not IsNoScannerWarning then
  begin
    SysUtils.Abort;
    Exit;
  end;
  inherited;
  DefaultSaveCurrentRecord(DataSet); // 20014450.50  
  // RV076.5.
  NewEnteredDateOut := DataSet.FieldByName('DATETIME_OUT').AsDateTime;
end;

// RV077.3.
// RV082.4. Employee is of type: Autom. scans based on standard planning.
//          Do not allow changes at all!
function TTimeRecScanningDM.IsNoScannerWarning: Boolean;
var
  ADataFilter: TDataFilter;
begin
  Result := True;
  if SystemDM.IsRFL then
  begin
    ADataFilter := TimeRecScanningDM.ScanFilter;
    // RV082.8. Only do this for Scans-tab, not for OpenScans-tab!
    //          There can be no open scans for an autom. scans-employee.
    if ADataFilter.ScanFilter = ScanPerEmployee then
    begin
      if (ADataFilter.IsScanning = False) and
        (ADataFilter.BookProdHrs = True) then
      begin
        // RV082.4. Do not allow changes at all.
        DisplayMessage(SPimsIsNoScannerWarning, mtInformation, [mbOk]);
        Result := False;
//     if (DisplayMessage(SPimsIsNoScannerWarning,
//        mtConfirmation, [mbYes, mbNo]) = mrYes) then
//        Result := True
//      else
//        Result := False;
      end;
    end;
  end;
end;

// RV092.15. Addition of filtering on plant.
procedure TTimeRecScanningDM.QueryPlantLUFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV092.15. Only filter workspots when plan-in-other-plants is not used.
  if SystemDM.EmployeesAcrossPlantsYN = UNCHECKEDVALUE then
    Accept := SystemDM.PlantTeamFilter(DataSet)
  else
    Accept := True;
end;

// 20013472.
procedure TTimeRecScanningDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if DataSet.Active and qryWorkspotLU.Active and
    qryJobLU.Active and qryShiftLU.Active then
  begin
    TableDetailSHIFT_HOURTYPE_NUMBER.AsString := ''; // TD-23386
    TableDetailIGNORE_FOR_OVERTIME_YN.Value := ''; // TD-23386
    if qryShiftLU.Locate('PLANT_CODE;SHIFT_NUMBER',
      VarArrayOf([DataSet.FieldByName('PLANT_CODE').AsString,
        DataSet.FieldByName('SHIFT_NUMBER').AsInteger]), []) then
    begin
      TableDetailSHIFTCAL.Value :=
        qryShiftLU.FieldByName('DESCRIPTION').AsString;
      // TD-23386
      if qryShiftLU.FieldByName('HOURTYPE_NUMBER').AsString <> '' then
      begin
        TableDetailSHIFT_HOURTYPE_NUMBER.Value :=
          qryShiftLU.FieldByName('HOURTYPE_NUMBER').AsInteger;
        TableDetailIGNORE_FOR_OVERTIME_YN.Value :=
          qryShiftLU.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
      end;
    end
    else
      TableDetailSHIFTCAL.Value := '';
    // Calculate workspot / job instead of using 'lookup'.
    // TD-23386 Also get some extra fields
    TableDetailWORKSPOT_HOURTYPE_NUMBER.AsString := '';
    TableDetailWORKSPOT_DEPARTMENT_CODE.Value := '';
    if qryWorkspotLU.Locate('PLANT_CODE;WORKSPOT_CODE',
      VarArrayOf([DataSet.FieldByName('PLANT_CODE').AsString,
        DataSet.FieldByName('WORKSPOT_CODE').AsString]), []) then
    begin
      TableDetailWORKSPOTCAL.Value :=
        qryWorkspotLU.FieldByName('DESCRIPTION').AsString;
      // // TD-23386
      if qryWorkspotLU.FieldByName('HOURTYPE_NUMBER').AsString <> '' then
      begin
        TableDetailWORKSPOT_HOURTYPE_NUMBER.Value :=
          qryWorkspotLU.FieldByName('HOURTYPE_NUMBER').AsInteger;
        TableDetailIGNORE_FOR_OVERTIME_YN.Value :=
          qryWorkspotLU.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString;
      end;
      // TD-23386
      TableDetailWORKSPOT_DEPARTMENT_CODE.Value :=
        qryWorkspotLU.FieldByName('DEPARTMENT_CODE').AsString;
    end
    else
      TableDetailWORKSPOTCAL.Value := '';
    if qryJobLU.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
      VarArrayOf([DataSet.FieldByName('PLANT_CODE').AsString,
        DataSet.FieldByName('WORKSPOT_CODE').AsString,
        DataSet.FieldByName('JOB_CODE').AsString]), []) then
      TableDetailJOBCAL.Value :=
        qryJobLU.FieldByName('DESCRIPTION').AsString
    else
      TableDetailJOBCAL.Value := '';
  end;
end;

// 20013035
function TTimeRecScanningDM.IsEmployeeActive(ADate: TDateTime): Boolean;
begin
  if cdsFilterEmployee.FieldByName('ENDDATE').AsString <> '' then
  begin
    if (ADate >= cdsFilterEmployee.FieldByName('STARTDATE').AsDateTime) and
      (ADate <= cdsFilterEmployee.FieldByName('ENDDATE').AsDateTime) then
      Result := True
    else
      Result := False;
  end
  else
  begin
    if (ADate >= cdsFilterEmployee.FieldByName('STARTDATE').AsDateTime) then
      Result := True
    else
      Result := False;
  end;
end;

// 20013035
procedure TTimeRecScanningDM.ShowOnlyActiveSwitch;
begin
  cdsFilterEmployee.Refresh;
end;

// 20013035
procedure TTimeRecScanningDM.cdsFilterEmployeeFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  if ScanFilter.ShowOnlyActive then
    Accept := IsEmployeeActive(ScanFilter.StartDate)
  else
    Accept := True;
end;

// 20014450.50
procedure TTimeRecScanningDM.RecalcEfficiency(ADataSet: TDataSet);
begin
  // 20014450.50 Recalculate efficiency here
  try
    // First recalculate with old values when these fields are changed:
    if not (
       (OldRecVal.Plant_Code = ADataSet.FieldByName('PLANT_CODE').AsString) and
       (OldRecVal.Shift_Number = ADataSet.FieldByName('SHIFT_NUMBER').AsInteger) and
       (OldRecVal.ShiftDate = ADataSet.FieldByName('SHIFT_DATE').AsDateTime) and
       (OldRecVal.WorkSpot_Code = ADataSet.FieldByName('WORKSPOT_CODE').AsString) and
       (OldRecVal.Job_Code = ADataSet.FieldByName('JOB_CODE').AsString)
       ) then
    begin
      with StoredProcRecalcEfficiency do
      begin
        ParamByName('FP_PLANT_CODE').AsString := OldRecVal.Plant_Code;
        ParamByName('FP_SHIFT_NUMBER').AsInteger := OldRecVal.Shift_Number;
        ParamByName('FP_SHIFT_DATE').AsDateTime := OldRecVal.ShiftDate;
        ParamByName('FP_WORKSPOT_CODE').AsString := OldRecVal.WorkSpot_Code;
        ParamByName('FP_JOB_CODE').AsString := OldRecVal.Job_Code;
        ExecProc;
      end;
    end;
    // Recalculate with new values
    with StoredProcRecalcEfficiency do
    begin
      ParamByName('FP_PLANT_CODE').AsString := ADataSet.FieldByName('PLANT_CODE').AsString;
      ParamByName('FP_SHIFT_NUMBER').AsInteger := ADataSet.FieldByName('SHIFT_NUMBER').AsInteger;
      ParamByName('FP_SHIFT_DATE').AsDateTime := ADataSet.FieldByName('SHIFT_DATE').AsDateTime;
      ParamByName('FP_WORKSPOT_CODE').AsString := ADataSet.FieldByName('WORKSPOT_CODE').AsString;
      ParamByName('FP_JOB_CODE').AsString :=  ADataSet.FieldByName('JOB_CODE').AsString;
      ExecProc;
    end;
  except
    on E: EDBENgineError do
    begin
      WErrorLog('Recalc Efficiency: ' + E.Message);
    end;
  end;
end; // RecalcEfficiency

// 20014450.50
procedure TTimeRecScanningDM.TableDetailAfterDelete(DataSet: TDataSet);
begin
  inherited;
  RecalcEfficiency(DataSet);
end; // TableDetailAfterDelete

// 20014450.50 Call this before updeting the scan and when
// workspot, shift and/or job has changed.
procedure TTimeRecScanningDM.MoveEfficiency(ADataSet: TDataSet);
begin
  try
    if not (
       (OldRecVal.Shift_Number = ADataSet.FieldByName('SHIFT_NUMBER').AsInteger) and
       (OldRecVal.WorkSpot_Code = ADataSet.FieldByName('WORKSPOT_CODE').AsString) and
       (OldRecVal.Job_Code = ADataSet.FieldByName('JOB_CODE').AsString)
       ) then
    begin
      with StoredProcMoveEfficiency do
      begin
        ParamByName('FP_EMPLOYEE').AsInteger := ADataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('FP_DATETIME_IN').AsDateTime := ADataSet.FieldByName('DATETIME_IN').AsDateTime;
        ParamByName('FP_NEW_SHIFT_NR').AsInteger := ADataSet.FieldByName('SHIFT_NUMBER').AsInteger;
        ParamByName('FP_NEW_WORKSPOT').AsString := ADataSet.FieldByName('WORKSPOT_CODE').AsString;
        ParamByName('FP_NEW_JOBCODE').AsString := ADataSet.FieldByName('JOB_CODE').AsString;
        ExecProc;
      end;
    end;
  except
    on E: EDBENgineError do
    begin
      WErrorLog('Move Efficiency: ' + E.Message);
    end;
  end;
end;

// PIM-323
// Return True when there was a first scan found for employee where
// DateIn > EarlyDate.
function TTimeRecScanningDM.EarlyLateScanCheck(AEarlyDate: TDateTime;
  AEmployeeNumber: Integer): Boolean;
var
  SDateTime, EDateTime: TDateTime;
begin
  Result := False;
  SDateTime := Trunc(AEarlyDate);
  GetStartEndDay(SDateTime, EDateTime);
  with qryEarlyLateScanCheck do
  begin
    Close;
    ParamByName('STARTDATE').AsDateTime := SDateTime;
    ParamByName('ENDDATE').AsDateTime := EDateTime;
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    Open;
    if not Eof then
      if FieldByName('DATETIME_IN').AsDateTime > AEarlyDate then
        Result := True;
    Close;
  end;
end; // EarlyLateScanCheck

end.


