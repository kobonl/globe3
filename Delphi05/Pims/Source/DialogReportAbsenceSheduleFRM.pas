(*
  Changes:
    MRA:07-AUG-2008 RV008.
      Added 'showperiod'. This shows extra info about the period based on
      selected year and weeks.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
*)

unit DialogReportAbsenceSheduleFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib;

type
  TDialogReportAbsenceSheduleF = class(TDialogReportBaseF)
    Label2: TLabel;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    CheckBoxShowTotal: TCheckBox;
    Label4: TLabel;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroupEmployee: TRadioGroup;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeekFrom: TdxSpinEdit;
    GroupBox2: TGroupBox;
    CheckBoxWTR: TCheckBox;
    CheckBoxIll: TCheckBox;
    CheckBoxHol: TCheckBox;
    dxSpinEditWeekTo: TdxSpinEdit;
    CheckBoxExport: TCheckBox;
    lblPeriod: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  DialogReportAbsenceSheduleF: TDialogReportAbsenceSheduleF;

// RV089.1.
function DialogReportAbsenceSheduleForm: TDialogReportAbsenceSheduleF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  SystemDMT, ReportAbsenceSheduleDMT, ReportAbsenceSheduleQRPT, ListProcsFRM,
  UPimsMessageRes;

// RV089.1.
var
  DialogReportAbsenceSheduleF_HND: TDialogReportAbsenceSheduleF;

// RV089.1.
function DialogReportAbsenceSheduleForm: TDialogReportAbsenceSheduleF;
begin
  if (DialogReportAbsenceSheduleF_HND = nil) then
  begin
    DialogReportAbsenceSheduleF_HND := TDialogReportAbsenceSheduleF.Create(Application);
    with DialogReportAbsenceSheduleF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportAbsenceSheduleF_HND;
end;

// RV089.1.
procedure TDialogReportAbsenceSheduleF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportAbsenceSheduleF_HND <> nil) then
  begin
    DialogReportAbsenceSheduleF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportAbsenceSheduleF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportAbsenceSheduleF.btnOkClick(Sender: TObject);
begin
  inherited;
  if dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value then
    begin
      DisplayMessage(SPimsWeekStartEnd, mtInformation, [mbYes]);
      Exit;
    end;
   if (dxSpinEditWeekTo.Value - dxSpinEditWeekFrom.Value > 12) then
    begin
      DisplayMessage(SPimsMaxNumberOfWeeks, mtInformation, [mbYes]);
      Exit;
    end;
   if (not CheckBoxWTR.Checked) and (not CheckBoxIll.Checked) and
    (not CheckBoxHol.Checked) then
    begin
      DisplayMessage(SPimsReportWTR_HOL_ILL_Checks, mtInformation, [mbYes]);
      Exit;
    end;
  if ReportAbsenceSheduleQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value),
      Round(dxSpinEditWeekTo.Value),
      RadioGroupEmployee.ItemIndex,
      CheckBoxShowTotal.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxWTR.Checked,
      CheckBoxIll.Checked,
      CheckBoxHol.Checked,
      CheckBoxExport.Checked)
  then
    ReportAbsenceSheduleQR.ProcessRecords;
end;

procedure TDialogReportAbsenceSheduleF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, False, False, True, False, False, False);
  inherited;

  ListProcsF.WeekUitDat(Now, Year, Week);

  dxSpinEditYear.Value := Year;
  dxSpinEditWeekFrom.Value := Week;
  dxSpinEditWeekTo.Value := Week + 12;
  CheckBoxShowSelection.Checked := True;
  CheckBoxShowTotal.Checked := True;
  CheckBoxWTR.Checked := True;
  CheckBoxIll.Checked := False;
  CheckBoxHol.Checked := False;
  RadioGroupEmployee.ItemIndex := 0;
  ChangeDate(Sender);
end;

procedure TDialogReportAbsenceSheduleF.FormCreate(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;

  ListProcsF.WeekUitDat(Now, Year, Week);
  dxSpinEditYear.Value := Year;
  CheckBoxShowTotal.Checked := True;
// CREATE data module of report
  ReportAbsenceSheduleDM := CreateReportDM(TReportAbsenceSheduleDM);
  ReportAbsenceSheduleQR := CreateReportQR(TReportAbsenceSheduleQR);
end;

// MR:05-12-2003
procedure TDialogReportAbsenceSheduleF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
  ShowPeriod;
end;

procedure TDialogReportAbsenceSheduleF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekTo.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportAbsenceSheduleF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportAbsenceSheduleF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
