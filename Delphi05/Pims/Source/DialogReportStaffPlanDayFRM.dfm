inherited DialogReportStaffPlanDayF: TDialogReportStaffPlanDayF
  Caption = 'Report staff planning per day'
  ClientHeight = 457
  ClientWidth = 582
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 582
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 465
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 582
    Height = 338
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 40
      Top = 387
    end
    inherited LblEmployee: TLabel
      Left = 80
      Top = 387
      Width = 22
      Caption = 'Shift'
    end
    inherited LblToPlant: TLabel
      Top = 16
    end
    inherited LblToEmployee: TLabel
      Left = 331
      Top = 377
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 136
      Top = 388
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 376
      Top = 388
    end
    object Label5: TLabel [8]
      Left = 120
      Top = 422
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label10: TLabel [9]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [10]
      Left = 40
      Top = 43
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [11]
      Left = 8
      Top = 422
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [12]
      Left = 40
      Top = 422
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [13]
      Left = 315
      Top = 423
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [14]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [15]
      Left = 120
      Top = 441
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [16]
      Left = 336
      Top = 441
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [17]
      Left = 336
      Top = 422
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [18]
      Left = 8
      Top = 440
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [19]
      Left = 40
      Top = 440
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [20]
      Left = 315
      Top = 442
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel [21]
      Left = 8
      Top = 150
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [22]
      Left = 315
      Top = 151
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [23]
      Left = 120
      Top = 121
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [24]
      Left = 336
      Top = 121
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [25]
      Left = 8
      Top = 124
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [26]
      Left = 40
      Top = 124
      Width = 22
      Height = 13
      Caption = 'Shift'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [27]
      Left = 315
      Top = 122
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelDeptFrom: TLabel [28]
      Left = 120
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object LabelDeptTo: TLabel [29]
      Left = 336
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 120
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
    end
    inherited LblToTeam: TLabel
      Top = 70
    end
    inherited LblStarTeamFrom: TLabel
      Left = 120
    end
    inherited LblTeam: TLabel
      Top = 68
    end
    inherited LblFromTeam: TLabel
      Top = 68
    end
    inherited LblFromWorkspot: TLabel
      Top = 96
    end
    inherited LblWorkspot: TLabel
      Top = 96
    end
    inherited LblStarWorkspotFrom: TLabel
      Left = 120
      Top = 98
    end
    inherited LblToWorkspot: TLabel
      Top = 96
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 98
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 127
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 128
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 67
      ColCount = 133
      TabOrder = 5
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 67
      ColCount = 134
      TabOrder = 6
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 521
      Top = 70
      TabOrder = 7
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 133
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      ColCount = 134
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 136
      Top = 370
      TabOrder = 17
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 366
      Top = 370
      TabOrder = 29
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 139
      TabOrder = 30
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 140
      TabOrder = 31
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 34
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 24
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 26
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 35
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 157
      TabOrder = 32
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 158
      TabOrder = 22
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 94
      ColCount = 158
      TabOrder = 8
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 94
      ColCount = 159
      TabOrder = 9
    end
    inherited EditWorkspots: TEdit
      TabOrder = 33
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 159
      TabOrder = 14
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 158
      TabOrder = 16
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 19
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 21
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 18
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 20
    end
    inherited DatePickerToBase: TDateTimePicker
      TabOrder = 36
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 192
      Width = 505
      Height = 137
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 15
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 248
        Top = 15
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 4
      end
      object CheckBoxPageShift: TCheckBox
        Left = 248
        Top = 40
        Width = 169
        Height = 17
        Caption = 'New page per shift'
        TabOrder = 5
      end
      object CheckBoxPageWK: TCheckBox
        Left = 248
        Top = 62
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 6
      end
      object CheckBoxShowNPlannedWorkspot: TCheckBox
        Left = 8
        Top = 40
        Width = 217
        Height = 17
        Caption = 'Show n-planned workspots'
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CheckBoxShowNPlannedWorkspotClick
      end
      object CheckBoxSortEmp: TCheckBox
        Left = 8
        Top = 88
        Width = 217
        Height = 17
        Caption = 'Sort on employee'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CheckBoxSortEmpClick
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 112
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 3
      end
      object CheckBoxShowStartEndTimes: TCheckBox
        Left = 8
        Top = 63
        Width = 225
        Height = 17
        Caption = 'Show start/end times'
        TabOrder = 7
      end
      object RGrpSortOnEmployee: TRadioGroup
        Left = 247
        Top = 88
        Width = 250
        Height = 41
        Caption = 'Sort on employee'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Number'
          'Name')
        TabOrder = 8
      end
    end
    object ComboBoxPlusShiftFrom: TComboBoxPlus
      Left = 120
      Top = 122
      Width = 180
      Height = 19
      ColCount = 130
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftFromCloseUp
    end
    object ComboBoxPlusShiftTo: TComboBoxPlus
      Left = 334
      Top = 121
      Width = 180
      Height = 19
      ColCount = 130
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 136
      Top = 440
      Width = 180
      Height = 19
      ColCount = 138
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 23
      TitleColor = clBtnFace
      Visible = False
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 350
      Top = 439
      Width = 180
      Height = 19
      ColCount = 139
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 25
      TitleColor = clBtnFace
      Visible = False
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 149
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 12
      OnChange = DateFromChange
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 148
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
      Visible = False
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 520
      Top = 420
      Width = 41
      Height = 17
      Caption = 'All '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 28
      Visible = False
    end
  end
  inherited stbarBase: TStatusBar
    Top = 438
    Width = 582
  end
  inherited pnlBottom: TPanel
    Top = 399
    Width = 582
    Height = 39
    inherited btnOk: TBitBtn
      Left = 176
    end
    inherited btnCancel: TBitBtn
      Left = 290
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 272
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 304
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        44040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507244469616C6F675265706F727442617365462E4461746153
        6F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A650B0E
        6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67106564
        676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E
        6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F43
        616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F4361
        6E496E73657274116564676F43616E4E617669676174696F6E116564676F436F
        6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264731065
        64676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D
        6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265720653
        6F7274656407046373557005576964746802410942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E
        554D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E5368
        6F72744E616D650743617074696F6E060A53686F7274206E616D650557696474
        6802540942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060A53484F52545F4E414D4500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557696474
        6803B4000942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741646472
        65737305576964746802450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506074144445245535300000F5464784442477269
        64436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F
        4465706172746D656E7420636F646505576964746802580942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F444550415254
        4D454E545F434F444500000F546478444247726964436F6C756D6E0A436F6C75
        6D6E5465616D0743617074696F6E06095465616D20636F64650942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507224469616C6F675265706F72
        7442617365462E44617461536F75726365456D706C546F104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802310942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D65055769647468024E0942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07436170
        74696F6E06044E616D6505576964746803CC000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E06074164647265737305576964746802650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        5245535300000F546478444247726964436F6C756D6E0E436F6C756D6E446570
        74436F64650743617074696F6E060F4465706172746D656E7420636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 532
  end
  inherited QueryEmplTo: TQuery
    Left = 504
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_NUMBER,'
      '  S.PLANT_CODE,'
      '  S.DESCRIPTION'
      'FROM '
      '  SHIFT S'
      'WHERE'
      '  S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  S.SHIFT_NUMBER')
    Left = 128
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
