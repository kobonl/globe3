(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:28-SEP-2010. RV069.1. 550494.
    - User Rights. Departments are shown that do not belong
      to user based on teams-per-user.
    - Cause: It filtered only on Plant, but it must filter on
      Plant + Department.
*)
unit BreakPerDeptDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TBreakPerDeptDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableMasterPLANTLU: TStringField;
    TableDept: TTable;
    DataSourceDept: TDataSource;
    TableDeptDEPARTMENT_CODE: TStringField;
    TableDeptPLANT_CODE: TStringField;
    TableDeptDESCRIPTION: TStringField;
    TableDeptBUSINESSUNIT_CODE: TStringField;
    TableDeptCREATIONDATE: TDateTimeField;
    TableDeptDIRECT_HOUR_YN: TStringField;
    TableDeptMUTATIONDATE: TDateTimeField;
    TableDeptMUTATOR: TStringField;
    TableDeptPLAN_ON_WORKSPOT_YN: TStringField;
    TableDeptREMARK: TStringField;
    TableDeptPLANTLU: TStringField;
    DataSourcePlant: TDataSource;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailBREAK_NUMBER: TIntegerField;
    TableDetailPAYED_YN: TStringField;
    TableBRPerDept: TTable;
    TableBRPerDeptPLANT_CODE: TStringField;
    TableBRPerDeptDEPARTMENT_CODE: TStringField;
    TableBRPerDeptSHIFT_NUMBER: TIntegerField;
    TableBRPerDeptBREAK_NUMBER: TIntegerField;
    TableBRPerDeptDESCRIPTION: TStringField;
    TableBRPerDeptPAYED_YN: TStringField;
    TableBRPerDeptCREATIONDATE: TDateTimeField;
    TableBRPerDeptMUTATIONDATE: TDateTimeField;
    TableBRPerDeptMUTATOR: TStringField;
    TableBRPerDeptSTARTTIME1: TDateTimeField;
    TableBRPerDeptENDTIME1: TDateTimeField;
    TableBRPerDeptSTARTTIME2: TDateTimeField;
    TableBRPerDeptENDTIME2: TDateTimeField;
    TableBRPerDeptSTARTTIME3: TDateTimeField;
    TableBRPerDeptENDTIME3: TDateTimeField;
    TableBRPerDeptSTARTTIME4: TDateTimeField;
    TableBRPerDeptENDTIME4: TDateTimeField;
    TableBRPerDeptSTARTTIME5: TDateTimeField;
    TableBRPerDeptENDTIME5: TDateTimeField;
    TableBRPerDeptSTARTTIME6: TDateTimeField;
    TableBRPerDeptENDTIME6: TDateTimeField;
    TableBRPerDeptSTARTTIME7: TDateTimeField;
    TableBRPerDeptENDTIME7: TDateTimeField;
    TableDetailPLANTLU: TStringField;
    TableExportDEPTLU: TStringField;
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailDESCRIPTIONValidate(Sender: TField);
    procedure TableDeptAfterScroll(DataSet: TDataSet);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDeptFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetDepartment;
  end;

var
  BreakPerDeptDM: TBreakPerDeptDM;

implementation

uses SystemDMT, UPimsMessageRes, UPimsConst, BreakPerDeptFRM;

{$R *.DFM}

procedure TBreakPerDeptDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('BREAK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('BREAK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;
  if (CheckBlocks(BreakPerDeptDM.Handle_Grid, TableMaster, True) <> 0 ) then
  begin
    DisplayMessage(SPIMSTimeIntervalError, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerDeptDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  TableDetail.FieldByName('DEPARTMENT_CODE').AsString :=
    TableDept.FieldByName('DEPARTMENT_CODE').AsString;
  TableDetail.FieldByName('PLANT_CODE').AsString :=
    TableMaster.FieldByName('PLANT_CODE').AsString;
  TableDetail.FieldByName('SHIFT_NUMBER').AsInteger :=
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger;
  TableDetail.FieldByName('PAYED_YN').AsString := UNCHECKEDVALUE;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('BREAK_NUMBER').AsInteger := Index + 1;

  if TableBRPerDept.FindKey([TableMaster.FieldByName('PLANT_CODE').AsString,
    TableDept.FieldByName('DEPARTMENT_CODE').AsString,
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
  for Index := 1 to 7 do
  begin
    DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
      TableBRPerDept.FieldByName('ENDTIME' + IntToStr(Index)).Value;
    DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
      TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
  end
  else
    InitializeTimeValues(TableDetail, TableMaster);
  TableDetailPAYED_YN.Value := UNCHECKEDVALUE;
end;

procedure TBreakPerDeptDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailBREAK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerDeptDM.SetDepartment;
var
  SubStr, Dept: String;
begin
 // if Dept = '' then
  Dept := TableDept.FieldByName('DEPARTMENT_CODE').AsString;
  if Dept = '' then  
    Exit;

  if not TableMaster.Active then
    TableMaster.Active := True;

  if TableMaster.RecordCount = 0 then
    Exit;
  SubStr :=
    'DEPARTMENT_CODE = ' + ''''+ DoubleQuote(Dept) + '''' +
    ' AND PLANT_CODE = ' + '''' +
    DoubleQuote(TableMaster.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND SHIFT_NUMBER = ' +
    TableMaster.FieldByName('SHIFT_NUMBER').AsString;
  TableDetail.Close;
  TableDetail.Filter := SubStr;
  TableDetail.Open;
end;
procedure TBreakPerDeptDM.TableDetailDESCRIPTIONValidate(Sender: TField);
begin
  inherited;
  SystemDM.DefaultNotEmptyValidate(Sender);
end;

procedure TBreakPerDeptDM.TableDeptAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetDepartment;
end;

procedure TBreakPerDeptDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetDepartment;
end;

procedure TBreakPerDeptDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableDept);
end;

// RV050.8.
procedure TBreakPerDeptDM.TableDeptFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV069.1. Filter not only on Plant!
//  Accept := SystemDM.PlantTeamFilter(DataSet);
  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

end.
