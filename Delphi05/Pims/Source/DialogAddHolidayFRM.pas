(*
  MRA:15-JUL-2015 PIM-53
  - Add Holiday functionality
  MRA:29-SEP-2015 PIM-53
  - Check on valid absence reason when this is entered manually
  MRA:22-MAR-2019 GLOB3-275
  - Add Holiday/Absence and message about no working days selected
  - It should not check on working days (Monday to Friday), because it is
    possible employees are available on Saturday and/or Sunday.
*)
unit DialogAddHolidayFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, SystemDMT, DialogAddHolidayDMT, dxCntner, dxEditor, dxExEdtr, dxDBEdtr,
  dxDBELib, UPimsConst, jpeg;

type
  TDialogAddHolidayF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    LblFromDateTime: TLabel;
    LblDateTime: TLabel;
    DatePickerFrom: TDateTimePicker;
    LblToDateTime: TLabel;
    DatePickerTo: TDateTimePicker;
    Label1: TLabel;
    dxDBLUEditShift: TdxDBLookupEdit;
    Label2: TLabel;
    edtEmployee: TEdit;
    Label3: TLabel;
    ComboBoxAbsRsn: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DatePickerFromCloseUp(Sender: TObject);
    procedure DatePickerToCloseUp(Sender: TObject);
    procedure ComboBoxAbsRsnExit(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FEmployeeNumber: Integer;
    FDateFrom: TDateTime;
    FDateTo: TDateTime;
    FShiftNumber: Integer;
    FAbsenceReasonCode: String;
    FCountryID: Integer;
    FPlantCode: String;
    FDayCount: Integer;
    FAbsRsnOK: Boolean;
    procedure UpdateAbsenceReasons;
    function AbsenceReasonCheck(AValue: String): Boolean;
  public
    { Public declarations }
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
    property DateFrom: TDateTime read FDateFrom write FDateFrom;
    property DateTo: TDateTime read FDateTo write FDateTo;
    property PlantCode: String read FPlantCode write FPlantCode;
    property ShiftNumber: Integer read FShiftNumber write FShiftNumber;
    property AbsenceReasonCode: String read FAbsenceReasonCode write FAbsenceReasonCode;
    property CountryID: Integer read FCountryID write FCountryID;
    property DayCount: Integer read FDayCount write FDayCount;
    property AbsRsnOK: Boolean read FAbsRsnOK write FAbsRsnOK;
  end;

var
  DialogAddHolidayF: TDialogAddHolidayF;

implementation

uses UPimsMessageRes;

{$R *.DFM}

procedure TDialogAddHolidayF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogAddHolidayDM := TDialogAddHolidayDM.Create(nil);

  DatePickerFrom.DateTime := Date;
  DatePickerTo.DateTime := DatePickerFrom.DateTime;
end;

procedure TDialogAddHolidayF.FormDestroy(Sender: TObject);
begin
  inherited;
  DialogAddHolidayDM.Free;
end;

procedure TDialogAddHolidayF.FormShow(Sender: TObject);
begin
  inherited;
  DayCount := 0;
  DialogAddHolidayDM.EmployeeNumber := EmployeeNumber;
  DialogAddHolidayDM.Init;
  edtEmployee.Text :=
    DialogAddHolidayDM.tblEmployeeEMPLOYEE_NUMBER.AsString + ' | ' +
    DialogAddHolidayDM.tblEmployeeDESCRIPTION.AsString;
  CountryID := DialogAddHolidayDM.tblEmployeeCOUNTRY_ID.AsInteger;
  PlantCode := DialogAddHolidayDM.tblEmployeePLANT_CODE.AsString;
  UpdateAbsenceReasons;
end;

procedure TDialogAddHolidayF.btnOkClick(Sender: TObject);
//var
//  DateLoop: TDateTime;
  function MyShiftNumber(AValue: String): Integer;
  var
    I: Integer;
  begin
    Result := 1;
    try
      I := Pos('|', AValue);
      if I > 0 then
        Result := StrToInt(Trim(Copy(AValue, 1, I-1)));
    except
      Result := 1;
    end;
  end; // DetermineShiftNumber
begin
  inherited;
  AbsRsnOK := False;
  DateFrom := DatePickerFrom.Date;
  DateTo := DatePickerTo.Date;
  ShiftNumber := 1;
  if DialogAddHolidayDM.qryShift.Locate('SHIFT_NUMBER', MyShiftNumber(dxDBLUEditShift.Text), []) then
    ShiftNumber := DialogAddHolidayDM.qryShift.FieldByName('SHIFT_NUMBER').AsInteger;
  AbsenceReasonCode := '';
  if ComboBoxAbsRsn.Text = '' then
  begin
    DisplayMessage(SPimsEmptyAbsReason, mtInformation, [mbOk]);
  end
  else
  begin
    AbsenceReasonCode := Copy(ComboBoxAbsRsn.Text, 0, 1);
    AbsRsnOK := AbsenceReasonCheck(AbsenceReasonCode);
    if AbsRsnOK then
    begin
      // GLOB3-275 Do not check for working-days
(*
      // Check if work-days were selected
      DateLoop := DateFrom;
      DayCount := 0;
      while DateLoop <= DateTo do
      begin
        if not (DayOfWeek(DateLoop) in [1,7]) then
          DayCount := DayCount + 1;
        DateLoop := DateLoop + 1;
      end;
      if DayCount = 0 then
      begin
        DisplayMessage(SPimsNoWorkingDays, mtInformation, [mbOk]);
      end
      else
      begin
        if DisplayMessage(
          Format(SPimsAddHoliday, [DayCount]),
            mtConfirmation, [mbYes, mbNo]) <> mrYes then
        begin
          DayCount := 0;
          ModalResult := mrCancel;
        end;
      end;
*)
    end;
  end;
end; // btnOkClick

// Here the combo-box must be filled with absence reasons.
procedure TDialogAddHolidayF.UpdateAbsenceReasons;
var
  SaveSql: String;
begin
  with DialogAddHolidayDM do
  begin
    SaveSql := qryAbsReason.Sql.Text;
    SystemDM.UpdateAbsenceReasonPerCountry(
      CountryID,
      qryAbsReason
    );

    ComboBoxAbsRsn.Items.Clear;

    qryAbsReason.Open;
    qryAbsReason.First;
    while not qryAbsReason.Eof do
    begin
       ComboBoxAbsRsn.Items.Add(qryAbsReason.FieldByName('ABSENCEREASON_CODE').
         AsString + ' | ' + qryAbsReason.FieldByName('DESCRIPTION').AsString);
       qryAbsReason.Next;
    end;

    qryAbsReason.Sql.Text := SaveSql;

    // Also fill cdsAbsRsn for check-function
    SaveSql := qryAbsRsn.Sql.Text;
    SystemDM.UpdateAbsenceReasonPerCountry(
     CountryId,
     qryAbsRsn
    );
    cdsAbsRsn.Close;
    cdsAbsRsn.Open;
    cdsAbsRsn.First;
    qryAbsRsn.Close;
    qryAbsRsn.Sql.Text := SaveSql;
  end;
end; // UpdateAbsenceReasons

procedure TDialogAddHolidayF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // GLOB3-275
  if (ModalResult = mrOK) and ((AbsenceReasonCode = '') {or (DayCount = 0)} or
    (not AbsRsnOK)) then
    Action := caNone;
end;

// Ensure the form stays on top of the calling form.
procedure TDialogAddHolidayF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;
  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TDialogAddHolidayF.DatePickerFromCloseUp(Sender: TObject);
begin
  inherited;
  if DatePickerFrom.DateTime > DatePickerTo.DateTime then
    DatePickerTo.DateTime := DatePickerFrom.DateTime;
end;

procedure TDialogAddHolidayF.DatePickerToCloseUp(Sender: TObject);
begin
  inherited;
  if DatePickerFrom.DateTime > DatePickerTo.DateTime then
    DatePickerTo.DateTime := DatePickerFrom.DateTime;
end;

procedure TDialogAddHolidayF.ComboBoxAbsRsnExit(Sender: TObject);
begin
  inherited;
  AbsRsnOK := AbsenceReasonCheck((Sender as TComboBox).Text);
end; // ComboBoxAbsRsnExit

function TDialogAddHolidayF.AbsenceReasonCheck(AValue: String): Boolean;
  function FindString(s: string): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    i := 0;
    try
      while (i < ComboBoxAbsRsn.Items.Count) and
        (ComboBoxAbsRsn.Items[i][1] <> s[1]) do
        inc(i);
      Result := i;
      ComboBoxAbsRsn.ItemIndex := i;
    except
    end;
  end;
begin
  Result := True;
  // Check if entered absence reason is valid here
  if (AValue = '') then
  begin
    DisplayMessage(SPimsFieldRequired, mtInformation, [mbOk]);
    Result := False;
  end
  else
  begin
    if (not DialogAddHolidayDM.cdsAbsRsn.Locate('ABSENCEREASON_CODE',
      AValue[1], [])) then
    begin
      DisplayMessage(SPimsAddHolidayEmplAvail, mtInformation, [mbOk]);
      Result := False;
    end
    else
      FindString(AValue[1]);
  end;
end; // AbsenceReasonCheck

end.
