(*
  MRA:25-JAN-2019 GLOB3-204
  - Personal Time Off USA
*)
unit PTODefFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, SystemDMT, PTODefDMT, dxEditor, dxExEdtr, dxEdLib,
  dxDBELib, StdCtrls;

type
  TPTODefF = class(TGridBaseF)
    dxMasterGridCONTRACTGROUP_CODE: TdxDBGridMaskColumn;
    dxMasterGridDESCRIPTION: TdxDBGridMaskColumn;
    dxMasterGridMAX_PTO_MINUTE: TdxDBGridMaskColumn;
    dxDetailGridLINE_NUMBER: TdxDBGridMaskColumn;
    dxDetailGridFROMEMPYEARS: TdxDBGridMaskColumn;
    dxDetailGridTILLEMPYEARS: TdxDBGridMaskColumn;
    dxDetailGridPTOMINUTE: TdxDBGridMaskColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dxDBSpinEditLine: TdxDBSpinEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    dxDBSpinEditFromYears: TdxDBSpinEdit;
    Label3: TLabel;
    dxDBSpinEditTillYears: TdxDBSpinEdit;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    dxSpinEditHour: TdxSpinEdit;
    dxSpinEditMin: TdxSpinEdit;
    Label8: TLabel;
    dxSpinEditHourOld: TdxSpinEdit;
    dxSpinEditMinOld: TdxSpinEdit;
    CopyAct: TAction;
    PasteAct: TAction;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxSpinEditHourChange(Sender: TObject);
    procedure dxSpinEditMinChange(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure CopyActExecute(Sender: TObject);
    procedure PasteActExecute(Sender: TObject);
  private
    { Private declarations }
    FInitForm: Boolean;
    FPasteContractGroupCode: String;
    FCopyContractGroupCode: String;
    procedure SetTimeFields;
    function ComputeTime: Integer;
    procedure StartEditMode(Sender: TObject);
  public
    { Public declarations }
    property InitForm: Boolean read FInitForm write FInitForm; // RV082.5.
    property CopyContractGroupCode: String read FCopyContractGroupCode write FCopyContractGroupCode;
    property PasteContractGroupCode: String read FPasteContractGroupCode write FPasteContractGroupCode; 
  end;

function PTODefF: TPTODefF;

implementation

uses
  UPimsMessageRes;

{$R *.DFM}

var
  PTODefF_HND: TPTODefF;

function PTODefF: TPTODefF;
begin
  if (PTODefF_HND = nil) then
  begin
    PTODefF_HND := TPTODefF.Create(Application);
  end;
  Result := PTODefF_HND;
end;


procedure TPTODefF.FormDestroy(Sender: TObject);
begin
  inherited;
  PTODefF_HND := Nil;
end;

procedure TPTODefF.FormCreate(Sender: TObject);
begin
  InitForm := True;
  PTODefDM := CreateFormDM(TPTODefDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := PTODefDM.DataSourceDetail;
    dxMasterGrid.DataSource := PTODefDM.DataSourceMaster;
  end;
  inherited;
  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste;
end;

procedure TPTODefF.FormShow(Sender: TObject);
begin
  inherited;
  if SystemDM.ASaveTimeRecScanning.ContractGroupCode <> '' then
  begin
    try
      PTODefDM.TableMaster.Locate('CONTRACTGROUP_CODE',
        SystemDM.ASaveTimeRecScanning.ContractGroupCode, []);
    except
      // Ignore error
    end;
  end;
  SetTimeFields;
  InitForm := False;
end;

procedure TPTODefF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  try
    if not PTODefDM.TableMaster.IsEmpty then
      SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
        PTODefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end;
end;

procedure TPTODefF.SetTimeFields;
begin
  with PTODefDM do
  begin
    // First store old value
    dxSpinEditHourOld.Value :=
      TableDetail.FieldByName('PTOMINUTE').asInteger div 60;
    dxSpinEditMinOld.Value :=
      TableDetail.FieldByName('PTOMINUTE').asInteger mod 60;
    // Then actual value
    dxSpinEditHour.Value :=
      TableDetail.FieldByName('PTOMINUTE').asInteger div 60;
    dxSpinEditMin.Value :=
      TableDetail.FieldByName('PTOMINUTE').asInteger mod 60;
  end;
end;

procedure TPTODefF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  dxDBSpinEditLine.SetFocus;
end;

function TPTODefF.ComputeTime: Integer;
begin
  Result := Round(dxSpinEditHour.Value * 60 + dxSpinEditMin.Value);
end;

procedure TPTODefF.StartEditMode(Sender: TObject);
begin
  if not InitForm then
    if not GridFormDM.TableDetail.IsEmpty then
      if (Sender is TdxSpinEdit) then
        GridFormDM.TableDetail.Edit;
end;

procedure TPTODefF.dxSpinEditHourChange(Sender: TObject);
begin
  inherited;
  if Assigned(PTODefDM) then
  begin
    PTODefDM.PTOHour := ComputeTime;
    if dxSpinEditHour.IntValue <> dxSpinEditHourOld.IntValue then
      StartEditMode(Sender);
  end;
end;

procedure TPTODefF.dxSpinEditMinChange(Sender: TObject);
begin
  inherited;
  if Assigned(PTODefDM) then
  begin
    PTODefDM.PTOHour := ComputeTime;
    if dxSpinEditMin.IntValue <> dxSpinEditMinOld.IntValue then
      StartEditMode(Sender);
  end;
end;

procedure TPTODefF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
end;

procedure TPTODefF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
  PasteContractGroupCode :=
    PTODefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  dxBarButtonPaste.Enabled := (CopyContractGroupCode <> '') and
    (CopyContractGroupCode <> PasteContractGroupCode);
  if dxBarButtonPaste.Enabled then
    dxBarButtonPaste.Hint :=
      Format(SPimsPasteOvertimeDef,
        [FCopyContractGroupCode])
  else
    dxBarButtonPaste.Hint := SPimsPaste;
end;

procedure TPTODefF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  dxSpinEditHour.Value :=
    dxSpinEditHourOld.Value;
  dxSpinEditMin.Value :=
    dxSpinEditMinOld.Value;
  PTODefDM.TableDetail.Cancel;
end;

procedure TPTODefF.dxGridEnter(Sender: TObject);
begin
  inherited;
  if ActiveGrid = dxMastergrid then
  begin
    dxBarButtonCopy.Visible := ivAlways;
    dxBarButtonPaste.Visible := ivAlways;
  end
  else
  begin
    dxBarButtonCopy.Visible := ivNever;
    dxBarButtonPaste.Visible := ivNever;
  end;
end;

procedure TPTODefF.CopyActExecute(Sender: TObject);
begin
  inherited;
  CopyContractGroupCode := '';

  if (not PTODefDM.TableMaster.Active) or
    PTODefDM.TableMaster.IsEmpty then
    Exit;

  CopyContractGroupCode :=
    PTODefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste;
end;

procedure TPTODefF.PasteActExecute(Sender: TObject);
var
  PTODefDefined: Boolean;
begin
  inherited;
  PasteContractGroupCode := '';

  if (not PTODefDM.TableMaster.Active) or
    PTODefDM.TableMaster.IsEmpty then
    Exit;

  PasteContractGroupCode :=
    PTODefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  PTODefDefined :=
    PTODefDM.PTODefFor(PasteContractGroupCode);
  if PTODefDefined then
    if DisplayMessage(SPimsOvertimeDefFound, 
        mtConfirmation, [mbYes, mbNo]) = mrNo
    then
      Exit;

  PTODefDM.CopyPTODef(CopyContractGroupCode,
    PasteContractGroupCode, PTODefDefined);
end;

end.
