inherited DepartmentF: TDepartmentF
  Left = 250
  Top = 158
  Height = 443
  Caption = 'Departments'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
        end>
      KeyField = 'PLANT_CODE'
      ShowBands = True
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 67
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 238
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnCity: TdxDBGridColumn
        Caption = 'City'
        Width = 164
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxMasterGridColumnPhone: TdxDBGridColumn
        Caption = 'Phone'
        Width = 126
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
      object dxMasterGridColumnState: TdxDBGridColumn
        Caption = 'State'
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        Width = 124
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'STATE'
        DisableFilter = True
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 301
    Height = 104
    object LabelCode: TLabel
      Left = 20
      Top = 12
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object LabelBusinessUnit: TLabel
      Left = 20
      Top = 46
      Width = 62
      Height = 13
      Caption = 'Business unit'
    end
    object LabelRemarks: TLabel
      Left = 20
      Top = 77
      Width = 41
      Height = 13
      Caption = 'Remarks'
    end
    object DBEditCode: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 8
      Width = 105
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'DEPARTMENT_CODE'
      DataSource = DepartmentDM.DataSourceDetail
    end
    object DBEditDescription: TdxDBEdit
      Tag = 1
      Left = 208
      Top = 8
      Width = 241
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'DESCRIPTION'
      DataSource = DepartmentDM.DataSourceDetail
    end
    object DBEditRemarks: TdxDBEdit
      Left = 96
      Top = 69
      Width = 353
      Style.BorderStyle = xbsSingle
      TabOrder = 3
      DataField = 'REMARK'
      DataSource = DepartmentDM.DataSourceDetail
    end
    object DBCheckBoxDirectHours: TDBCheckBox
      Tag = 1
      Left = 500
      Top = 12
      Width = 97
      Height = 17
      Caption = 'Direct hours'
      Ctl3D = False
      DataField = 'DIRECT_HOUR_YN'
      DataSource = DepartmentDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 4
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBCheckBoxWorkSpot: TDBCheckBox
      Tag = 1
      Left = 500
      Top = 40
      Width = 109
      Height = 17
      Caption = 'Plan on workspot'
      Ctl3D = False
      DataField = 'PLAN_ON_WORKSPOT_YN'
      DataSource = DepartmentDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 5
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object dxDBLookupEdit1: TdxDBLookupEdit
      Left = 96
      Top = 40
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'BUSINESSLU'
      DataSource = DepartmentDM.DataSourceDetail
      DropDownRows = 3
      ListFieldName = 'DESCRIPTION;BUSINESSUNIT_CODE'
      CanDeleteText = True
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 146
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 142
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 141
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Departments'
        end>
      KeyField = 'DEPARTMENT_CODE'
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENT_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 186
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnBusiness: TdxDBGridLookupColumn
        Caption = 'Business unit'
        Width = 149
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BUSINESSLU'
        ListFieldName = 'NAME'
      end
      object dxDetailGridColumnDirectHour: TdxDBGridCheckColumn
        Caption = 'Direct hours'
        Width = 75
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DIRECT_HOUR_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnPlanWorkspot: TdxDBGridCheckColumn
        Caption = 'Plan on workspot'
        Width = 90
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLAN_ON_WORKSPOT_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnRemarks: TdxDBGridColumn
        Caption = 'Remarks'
        Width = 167
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REMARK'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Left = 36
    Top = 124
  end
end
