(*
  MRA:15-MAY-2013 SO-20014137
  - New report employee illness.
    - Shows who is ill based on today's date.
*)
unit ReportEmployeeIllnessDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportEmployeeIllnessDM = class(TReportBaseDM)
    qryEmpIllness: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportEmployeeIllnessDM: TReportEmployeeIllnessDM;

implementation

{$R *.DFM}

uses
  SystemDMT;


end.
