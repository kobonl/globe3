inherited AbsTypeF: TAbsTypeF
  Width = 650
  Height = 409
  Caption = 'Absence types'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 634
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 632
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 632
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 279
    Width = 634
    Height = 92
    object LabelDescription: TLabel
      Left = 36
      Top = 18
      Width = 79
      Height = 13
      Caption = 'Type description'
    end
    object LabelExportCode: TLabel
      Left = 36
      Top = 64
      Width = 58
      Height = 13
      Caption = 'Export code'
    end
    object DBEditDescription: TdxDBEdit
      Tag = 1
      Left = 128
      Top = 16
      Width = 273
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'DESCRIPTION'
      DataSource = AbsTypeDM.DataSourceDetail
    end
    object DBEditExportCode: TDBEdit
      Left = 128
      Top = 60
      Width = 109
      Height = 19
      Ctl3D = False
      DataField = 'EXPORT_CODE'
      DataSource = AbsTypeDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 1
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 634
    Height = 124
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 120
      Width = 632
    end
    inherited dxDetailGrid: TdxDBGrid
      Width = 632
      Height = 119
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Absence types'
        end>
      KeyField = 'ABSENCETYPE_CODE'
      DataSource = AbsTypeDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 141
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCETYPE_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 326
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnExportCode: TdxDBGridColumn
        Caption = 'Export code'
        Width = 146
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
    end
  end
end
