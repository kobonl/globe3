(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:28-SEP-2010. RV069.1. 550494.
    - User Rights. Departments are shown that do not belong
      to user based on teams-per-user.
    - Cause: It filtered only on Plant, but it must filter on
      Plant + Department.
    MRA:15-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
*)
unit TimeBlockPerDeptDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTimeBlockPerDeptDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailTIMEBLOCK_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableMasterPLANTLU: TStringField;
    TableDept: TTable;
    DataSourceDept: TDataSource;
    TableDeptDEPARTMENT_CODE: TStringField;
    TableDeptPLANT_CODE: TStringField;
    TableDeptDESCRIPTION: TStringField;
    TableDeptBUSINESSUNIT_CODE: TStringField;
    TableDeptCREATIONDATE: TDateTimeField;
    TableDeptDIRECT_HOUR_YN: TStringField;
    TableDeptMUTATIONDATE: TDateTimeField;
    TableDeptMUTATOR: TStringField;
    TableDeptPLAN_ON_WORKSPOT_YN: TStringField;
    TableDeptREMARK: TStringField;
    TableDeptPLANTLU: TStringField;
    DataSourcePlant: TDataSource;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableTempTBPerDept: TTable;
    TableDetailPLANTLU: TStringField;
    TableExportDEPTLU: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDeptAfterScroll(DataSet: TDataSet);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure TableDeptFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetDepartment;
  end;

var
  TimeBlockPerDeptDM: TTimeBlockPerDeptDM;

implementation

uses
  SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TTimeBlockPerDeptDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableDept);
  // RV069.1.
  TablePlant.Active := True;
  TableTempTBPerDept.Active := True;
  TableDept.Active := True;
end;

procedure TTimeBlockPerDeptDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;

  if (CheckBlocks(TimeBlockPerDeptDM.Handle_Grid, TableMaster, False) <> 0 ) then
  begin
    DisplayMessage(SPIMSTimeIntervalError, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TTimeBlockPerDeptDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  TableDetail.FieldByName('DEPARTMENT_CODE').AsString :=
    TableDept.FieldByName('DEPARTMENT_CODE').AsString;
  TableDetail.FieldByName('PLANT_CODE').AsString :=
    TableMaster.FieldByName('PLANT_CODE').AsString;
  TableDetail.FieldByName('SHIFT_NUMBER').AsInteger :=
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger;

  if not CheckNrOfTimeblocks(TableDetail.RecordCount) then // GLOB3-60
    Abort;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger := Index + 1;
// InitializeTimeValues(TableDetail, TableMaster);
  if TableTempTBPerDept.FindKey([TableMaster.FieldByName('PLANT_CODE').AsString,
    TableDept.FieldByName('DEPARTMENT_CODE').AsString,
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
  for Index := 1 to 7 do
  begin
    DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
      TableTempTBPerDept.FieldByName('ENDTIME' + IntToStr(Index)).Value;
    DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
      TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
  end
  else
    InitializeTimeValues(TableDetail, TableMaster);
end;

procedure TTimeBlockPerDeptDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailTIMEBLOCK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

procedure TTimeBlockPerDeptDM.SetDepartment;
var
  SubStr, Dept: String;
begin
 if not TableMaster.Active then
   Exit;
//    TableMaster.Active := True;
//  if Dept = '' then
  Dept := TableDept.FieldByName('DEPARTMENT_CODE').AsString;
  if Dept = '' then
    Exit;
  if (TableMaster.RecordCount = 0) then
    Exit;
  SubStr :=
    'DEPARTMENT_CODE = ' + ''''+ DoubleQuote(Dept) + '''' +
    ' AND PLANT_CODE = ' + '''' +
    DoubleQuote(TableMaster.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND SHIFT_NUMBER = ' +
    TableMaster.FieldByName('SHIFT_NUMBER').AsString;
  TableDetail.Close;
  TableDetail.Filter := SubStr;
  TableDetail.Open;
end;

procedure TTimeBlockPerDeptDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  TablePlant.Active := False;
  TableDept.Active := False;
  TableTempTBPerDept.Active := False;
end;

procedure TTimeBlockPerDeptDM.TableDeptAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetDepartment;
end;

procedure TTimeBlockPerDeptDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetDepartment;
end;

procedure TTimeBlockPerDeptDM.TableDeptFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV069.1. Filter not only on Plant!
//  Accept := SystemDM.PlantTeamFilter(DataSet);
  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

end.
