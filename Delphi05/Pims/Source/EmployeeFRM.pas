(*
  Changes:
    MR:25-02-2005 Order 550386 Shift_Number added to Employee-Table.
    MRA:22-FEB-2010. RV054.9. 889956. UpdateScript: 48_1.
    - Make field SHIFT_NUMBER a mandatory field.
    SO: 07-JUN-2010 REV065.3 added the Free Text field
    SO: 15-JUN-2010 RV066.1. 550491
    - tailor made non-scanners
    MRA:27-AUG-2010 RV067.MRA.14. Small change.
    - Changed the width of the drop-down-box for departments: made bigger.
    MRA:1-DEC-2010 RV082.5.
    - Store ContractgroupCode for selected employee in memory,
      to use in Contractgroup-related dialogs, so it can
      be looked up when showing these dialogs.
    MRA:7-DEC-2010 RV082.14. Bugfix.
    - When called from Employee Registrations, it shows
      the Tailor-made field for RFL! Even if this is disabled.
    MRA:21-JAN-2011 RV085.11. Bugfix. SC-50016464.
    - When Contract Group is set as 'not-editable' using
      Admin Tool, then the Tab 'Overige' cannot be opened,
      only the first tab is visible.
    - Solution for dialogs with tab-pages:
      - Only the components on the tab-pages must be
        set as non-editable. But the tab-pages themselves
        not.
    MRA:27-JAN-2011 RV085.12. Small change.
    - Enlarge the drop-down-component for teams.
    - It showed 3 lines, this is enlarged to 7 lines.
    JVL:14-APR-2011. RV089. SO-550532
    - Memorize selected plant
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDBPicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:13-JUN-2016 VEI001-7
    - Addition of EMPLOYEE_ID that is only used for HYBRID=1.
    - It is filled automatically (sequence).
    - It can be shown in the grid as readonly-field to check it's value.
    MRA:3-OCT-2016 PIM-227
    - Make change of employee number optional
    MRA:4-FEB-2019 GLOB3-209
    - Employee Information Page, add 3 extra fields.
    MRA:29-APR-2019 GLOB3-290
    - Handling information about gender
    MRA:25-JAN-2021 GLOB3-411 Add External Reference to Employee-dialog
*)

unit EmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, dxEditor, dxExEdtr, dxEdLib,
  dxDBELib, Mask, ComCtrls, dxDBTLCl, dxGrClms, DBPicker, dxDBEdtr,
  dxExGrEd, dxExELib, SystemDMT;

type
  TEmployeeF = class(TGridBaseF)
    PageControlEmpl: TPageControl;
    TabSheetGeneral: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label11: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    DBEditEmployeeNumber: TDBEdit;
    DBEditName: TDBEdit;
    dbrgRetired: TDBRadioGroup;
    DBEditDescription: TDBEdit;
    GroupBox2: TGroupBox;
    TabSheetAddress: TTabSheet;
    GroupBox3: TGroupBox;
    Label23: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    LabelEmail: TLabel;
    Label6: TLabel;
    DBEditAddress: TDBEdit;
    DBEditZipcode: TDBEdit;
    DBEditCity: TDBEdit;
    DBEditEMail: TDBEdit;
    DBEditState: TDBEdit;
    DBEditPhone: TDBEdit;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label12: TLabel;
    Label24: TLabel;
    DBEditSocialNumber: TDBEdit;
    DBEdit10: TDBEdit;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridLookupColumn;
    dxDetailGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridLookupColumn;
    dxDetailGridColumn7: TdxDBGridLookupColumn;
    dxDetailGridColumn8: TdxDBGridColumn;
    Label10: TLabel;
    dxDetailGridColumn9: TdxDBGridColumn;
    dxDBDateEdit3: TdxDBDateEdit;
    dxDBLookupEdit1: TdxDBLookupEdit;
    dxDBLookupEdit2: TdxDBLookupEdit;
    dxDBLookupEditTeam: TdxDBLookupEdit;
    dxDetailGridColumn11: TdxDBGridLookupColumn;
    dxDetailGridColumn12: TdxDBGridDateColumn;
    dxDetailGridColumn13: TdxDBGridDateColumn;
    dxDetailGridColumn14: TdxDBGridCheckColumn;
    dxDetailGridColumn15: TdxDBGridCheckColumn;
    dxDetailGridColumn16: TdxDBGridCheckColumn;
    dxDetailGridColumn17: TdxDBGridDateColumn;
    dxDetailGridColumn18: TdxDBGridCheckColumn;
    dxDetailGridColumn19: TdxDBGridColumn;
    dxDetailGridColumn20: TdxDBGridColumn;
    dxDetailGridColumn21: TdxDBGridColumn;
    dxDetailGridColumn22: TdxDBGridColumn;
    dxDetailGridColumn23: TdxDBGridColumn;
    dxDetailGridColumn24: TdxDBGridDateColumn;
    dxDetailGridColumn25: TdxDBGridColumn;
    dxDetailGridColumn10: TdxDBGridColumn;
    Label13: TLabel;
    Label5: TLabel;
    Label20: TLabel;
    DBPicker1: TDBPicker;
    dxDBLookupEditDept: TdxDBLookupEdit;
    dxDBLookupEditPlant: TdxDBLookupEdit;
    dxDBDateEdit2: TdxDBDateEdit;
    DBCheckBoxFirstAid: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    LabelGrade: TLabel;
    DBEditGrade: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label16: TLabel;
    dxDBDateEdit1: TdxDBDateEdit;
    DBCheckBoxCalcBonusYN: TDBCheckBox;
    Label14: TLabel;
    dxDetailGridColumn26: TdxDBGridColumn;
    dxDBLookupEditShift: TdxDBLookupEdit;
    TabSheetFreeText: TTabSheet;
    GroupBox5: TGroupBox;
    dxDBMemo1: TdxDBMemo;
    DBCheckBoxBookProdHrs: TDBCheckBox;
    dxDetailGridColumn27: TdxDBGridColumn;
    Label15: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dxDetailGridColumn28: TdxDBGridColumn;
    dxDetailGridColumn29: TdxDBGridColumn;
    dxDetailGridColumn30: TdxDBGridColumn;
    dxDetailGridColumn31: TdxDBGridColumn;
    LabelExtRef: TLabel;
    DBEditExtRef: TDBEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxDBLookupEditPlantSelectionChange(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure dxDBLookupEditPlantCloseUp(Sender: TObject;
      var Value: Variant; var Accept: Boolean);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBLookupEditShiftKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxDBLookupEditDeptEnter(Sender: TObject);
    procedure dxDBLookupEditShiftEnter(Sender: TObject);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure DBCheckBoxBookProdHrsClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure DBEditEmployeeNumberEnter(Sender: TObject);
  private
    { Private declarations }
    //!!
    function IsRFL: Boolean;
    procedure EmpNrEnable(AEnable: Boolean);
  public
    { Public declarations }
  end;

function EmployeeF: TEmployeeF;

var
  EmployeeF_HND: TEmployeeF;

implementation

{$R *.DFM}

uses
  EmployeeDMT, UPimsConst;

function EmployeeF: TEmployeeF;
begin
  if (EmployeeF_HND = nil) then
    EmployeeF_HND := TEmployeeF.Create(Application);
  Result := EmployeeF_HND;
end;

procedure TEmployeeF.FormCreate(Sender: TObject);
begin
  EmployeeDM := CreateFormDM(TEmployeeDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := EmployeeDM.DataSourceDetail;
// RV082.14. Do this here! Or it will still show when called from Emp. Regs.
  //CAR: 550299
  LabelExtRef.Visible := False;
  DBEditExtRef.Visible := False;
  DBCheckBoxBookProdHrs.Visible := IsRFL;
  if SystemDM.GetGradeYN then
  begin
    LabelGrade.Visible := True;
    DBEditGrade.Visible := True;
  end
  else
  begin
    LabelGrade.Visible := False;
    DBEditGrade.Visible := False;
    if not DBCheckBoxBookProdHrs.Visible then
    begin
      LabelExtRef.Visible := True;
      DBEditExtRef.Visible := True;
    end;
  end;
  inherited;
  PageControlEmpl.ActivePageIndex := 0;
end;

procedure TEmployeeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  if PageControlEmpl.ActivePageIndex = 0 then
    DBEditEmployeeNumber.SetFocus
  else
    DBEditAddress.SetFocus
end;

procedure TEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  EmployeeF_HND := Nil;
end;

procedure TEmployeeF.FormShow(Sender: TObject);
begin
  inherited;
  dxDetailGrid.SetFocus;
  //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
    EmployeeDM.TableDetail.FindKey([SystemDM.ASaveTimeRecScanning.Employee]);
// RV082.14. Moved to FormCreate. This does not work correct when called from
//           Employee Regs!
{
   //CAR: 550299
  if not SystemDM.GetGradeYN then
  begin
    LabelGrade.Visible := False;
    DBEditGrade.Visible := False;
  end;
  DBCheckBoxBookProdHrs.Visible := IsRFL;
}
  // RV085.11. Disable only some parts of pnlDetail, so the
  //           tab-pages can still be clicked and seen.
  if not pnlDetail.Enabled then
  begin
    pnlDetail.Enabled := True;
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
    GroupBox3.Enabled := False;
    GroupBox4.Enabled := False;
    GroupBox5.Enabled := False;
  end;
end;

procedure TEmployeeF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  EmployeeDM.TableDetail.Cancel;
end;

procedure TEmployeeF.dxDBLookupEditPlantSelectionChange(Sender: TObject);
begin
  inherited;
  if dsrcActive.State in [dsInsert, dsEdit] then
  begin
    dxDBLookupEditDept.Text := '';
    dxDBLookupEditShift.Text := '';
  end;
end;

procedure TEmployeeF.dxBarButtonExportGridClick(Sender: TObject);
begin
//CAR 27.02.2003
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;
end;

procedure TEmployeeF.dxDBLookupEditPlantCloseUp(Sender: TObject;
  var Value: Variant; var Accept: Boolean);
begin
  inherited;
  if dsrcActive.State in [dsInsert, dsEdit] then
  begin
    with EmployeeDM do
    begin
      TableDetail.FieldByName('DEPTDESC').AsString := '';
      QueryDept.Close;
      QueryDept.Open;
      TableDetail.FieldByName('SHIFTDESC').AsString := '';
      QueryShift.Close;
      QueryShift.Open;
    end;
  end;
  dxDBLookupEditDept.SetFocus;
end;

procedure TEmployeeF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  EmployeeDM.TableDetail.Post;
end;

procedure TEmployeeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  // RV082.5 Do this during node-change, not here, or it can be emptied.
{  try
    //CAR 17-10-2003 : 550262
    SystemDM.ASaveTimeRecScanning.Employee :=
      EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    // RV082.5.
    SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
      EmployeeDM.TableDetail.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end; }
end;

procedure TEmployeeF.dxDBLookupEditShiftKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_DELETE then
  begin
    EmployeeDM.TableDetail.Edit;
    EmployeeDM.TableDetailSHIFT_NUMBER.AsString := '';
  end;
end;

procedure TEmployeeF.dxDBLookupEditDeptEnter(Sender: TObject);
begin
  inherited;
  if dsrcActive.State in [dsInsert, dsEdit] then
  begin
    with EmployeeDM do
    begin
      TableDetail.FieldByName('DEPTDESC').AsString := '';
      QueryDept.Close;
      QueryDept.Open;
    end;
  end;
end;

procedure TEmployeeF.dxDBLookupEditShiftEnter(Sender: TObject);
begin
  inherited;
  if dsrcActive.State in [dsInsert, dsEdit] then
  begin
    with EmployeeDM do
    begin
      TableDetail.FieldByName('SHIFTDESC').AsString := '';
      QueryShift.Close;
      QueryShift.Open;
    end;
  end;
end;


//RV066.1.
procedure TEmployeeF.DBCheckBox1Click(Sender: TObject);
begin
  inherited;
  if IsRFL then
    DBCheckBoxBookProdHrs.Enabled := not DBCheckBox1.Checked;
end;

procedure TEmployeeF.DBCheckBoxBookProdHrsClick(Sender: TObject);
begin
  inherited;
  if IsRFL then
    DBCheckBox1.Enabled := not DBCheckBoxBookProdHrs.Checked;
end;

procedure TEmployeeF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  try
    SystemDM.ASaveTimeRecScanning.Employee :=
      EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    // RV082.5.
    SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
      EmployeeDM.TableDetail.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end;
  if IsRFL then
  begin
    DBCheckBoxBookProdHrs.Enabled :=
      EmployeeDM.TableDetail.FieldByName('IS_SCANNING_YN').AsString = 'N';
    DBCheckBox1.Enabled :=
      EmployeeDM.TableDetail.FieldByName('BOOK_PROD_HRS_YN').AsString = 'N';
  end;
end;

function TEmployeeF.IsRFL: Boolean;
begin
  Result := SystemDM.GetCode = 'RFL';
end;

// PIM-227
procedure TEmployeeF.EmpNrEnable(AEnable: Boolean);
begin
  if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
  begin
    if (EmployeeDM.TableDetail.State in [dsInsert]) then
      AEnable := True;
    if AEnable then
    begin
      DBEditEmployeeNumber.Color := clRequired;
      DBEditEmployeeNumber.ReadOnly := False;
    end
    else
    begin
      DBEditEmployeeNumber.Color := clDisabled;
      DBEditEmployeeNumber.ReadOnly := True;
    end;
  end;
end;

// PIM-227
procedure TEmployeeF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  G: TCustomdxTreeListControl;
begin
  inherited;
  G := Sender as TCustomdxTreeListControl;
  if (G is TdxDBGrid) and ANode.HasChildren then
    Exit;
  if ASelected then
  begin
    if AColumn = dxDetailGridColumn1 then
    begin
      EmpNrEnable(SystemDM.UpdateEmpNr);
    end;
  end;
end;

// PIM-227
procedure TEmployeeF.DBEditEmployeeNumberEnter(Sender: TObject);
begin
  inherited;
  EmpNrEnable(SystemDM.UpdateEmpNr);
end;

end.
