inherited TimeBlockPerDeptF: TTimeBlockPerDeptF
  Left = 198
  Top = 167
  Width = 788
  Caption = 'Timeblocks per department'
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 772
    Height = 295
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 172
      Width = 770
      Height = 2
    end
    object SplitterTopMaster: TSplitter [1]
      Left = 1
      Top = 169
      Width = 770
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 100
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 174
      Width = 770
      Height = 120
      Bands = <
        item
          Caption = 'Shifts'
          Width = 247
        end
        item
          Caption = 'Monday'
          Width = 73
        end
        item
          Caption = 'Tuesday'
          Width = 76
        end
        item
          Caption = 'Wednesday'
          Width = 72
        end
        item
          Caption = 'Thursday'
          Width = 72
        end
        item
          Caption = 'Friday'
          Width = 72
        end
        item
          Caption = 'Sunday'
          Width = 72
        end
        item
          Caption = 'Saturday'
          Width = 76
        end>
      DefaultLayout = False
      KeyField = 'SHIFT_NUMBER'
      Align = alBottom
      ShowBands = True
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        DisableEditor = True
        Width = 74
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHIFT_NUMBER'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 173
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 38
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 38
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
    object dxDBGridDept: TdxDBGrid
      Left = 1
      Top = 1
      Width = 770
      Height = 168
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Departments'
        end>
      DefaultLayout = False
      HeaderPanelRowCount = 1
      KeyField = 'DEPARTMENT_CODE'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 1
      OnClick = dxDBGridDeptClick
      OnEnter = dxGridEnter
      DataSource = TimeBlockPerDeptDM.DataSourceDept
      HideSelectionTextColor = clWindowText
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
      OnChangeNode = dxDBGridDeptChangeNode
      OnCustomDrawBand = dxGridCustomDrawBand
      OnCustomDrawCell = dxGridCustomDrawCell
      OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
      OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
      object dxDBGridDeptColumn4: TdxDBGridLookupColumn
        Caption = 'Plant code'
        DisableEditor = True
        Width = 83
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxDBGridDeptColumn5: TdxDBGridColumn
        Caption = 'Plant name'
        DisableEditor = True
        Width = 186
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDBGridColumn1: TdxDBGridColumn
        Caption = 'Dept code'
        DisableEditor = True
        Width = 62
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENT_CODE'
      end
      object dxDBGridColumn2: TdxDBGridColumn
        Caption = 'Dept description'
        DisableEditor = True
        Width = 249
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDBGridColumn3: TdxDBGridColumn
        Caption = 'Business unit'
        DisableEditor = True
        Width = 180
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BUSINESSUNIT_CODE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 441
    Width = 772
    Height = 119
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 770
      Height = 117
      Align = alClient
      Caption = 'Times per Day'
      TabOrder = 0
      object Label4: TLabel
        Left = 16
        Top = 63
        Width = 24
        Height = 13
        Caption = 'Start'
      end
      object Label5: TLabel
        Left = 16
        Top = 90
        Width = 18
        Height = 13
        Caption = 'End'
      end
      object LabelMO: TLabel
        Left = 83
        Top = 40
        Width = 38
        Height = 13
        Caption = 'Monday'
      end
      object LabelTU: TLabel
        Left = 157
        Top = 40
        Width = 41
        Height = 13
        Caption = 'Tuesday'
      end
      object LabelWE: TLabel
        Left = 226
        Top = 40
        Width = 57
        Height = 13
        Caption = 'Wednesday'
      end
      object LabelTH: TLabel
        Left = 300
        Top = 40
        Width = 45
        Height = 13
        Caption = 'Thursday'
      end
      object LabelFR: TLabel
        Left = 378
        Top = 40
        Width = 30
        Height = 13
        Caption = 'Friday'
      end
      object LabelSA: TLabel
        Left = 448
        Top = 40
        Width = 44
        Height = 13
        Caption = 'Saterday'
      end
      object LabelSU: TLabel
        Left = 520
        Top = 40
        Width = 36
        Height = 13
        Caption = 'Sunday'
      end
      object Label1: TLabel
        Left = 16
        Top = 19
        Width = 54
        Height = 13
        Caption = 'Timeblocks '
      end
      object dxDBTimeEditST1: TdxDBTimeEdit
        Left = 80
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'STARTTIME1'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET1: TdxDBTimeEdit
        Left = 80
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'ENDTIME1'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST2: TdxDBTimeEdit
        Left = 152
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'STARTTIME2'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET2: TdxDBTimeEdit
        Left = 152
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 5
        DataField = 'ENDTIME2'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST3: TdxDBTimeEdit
        Left = 224
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 6
        DataField = 'STARTTIME3'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET3: TdxDBTimeEdit
        Left = 224
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 7
        DataField = 'ENDTIME3'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST4: TdxDBTimeEdit
        Left = 304
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 8
        DataField = 'STARTTIME4'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET4: TdxDBTimeEdit
        Left = 304
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 9
        DataField = 'ENDTIME4'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST5: TdxDBTimeEdit
        Left = 376
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 10
        DataField = 'STARTTIME5'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET5: TdxDBTimeEdit
        Left = 376
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 11
        DataField = 'ENDTIME5'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST6: TdxDBTimeEdit
        Left = 448
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 12
        DataField = 'STARTTIME6'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET6: TdxDBTimeEdit
        Left = 448
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 13
        DataField = 'ENDTIME6'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST7: TdxDBTimeEdit
        Left = 520
        Top = 60
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 14
        DataField = 'STARTTIME7'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET7: TdxDBTimeEdit
        Left = 520
        Top = 85
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 15
        DataField = 'ENDTIME7'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object DBEditTimeBlock: TDBEdit
        Tag = 1
        Left = 80
        Top = 16
        Width = 65
        Height = 19
        Ctl3D = False
        DataField = 'TIMEBLOCK_NUMBER'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditDesc: TDBEdit
        Tag = 1
        Left = 160
        Top = 16
        Width = 153
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = TimeBlockPerDeptDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 1
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 321
    Width = 772
    Height = 120
    Align = alBottom
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 116
      Width = 770
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Top = 0
      Width = 770
      Height = 116
      Bands = <
        item
          Caption = 'Timeblocks per department'
          Width = 259
        end
        item
          Caption = 'Monday'
          Width = 66
        end
        item
          Caption = 'Tuesday'
          Width = 80
        end
        item
          Caption = 'Wednesday'
          Width = 70
        end
        item
          Caption = 'Thursday'
          Width = 75
        end
        item
          Caption = 'Friday'
          Width = 67
        end
        item
          Caption = 'Saturday'
          Width = 69
        end
        item
          Caption = 'Sunday'
          Width = 74
        end>
      DefaultLayout = False
      KeyField = 'TIMEBLOCK_NUMBER'
      Align = alBottom
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        Width = 67
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMEBLOCK_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        Width = 192
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Top = 96
  end
end
