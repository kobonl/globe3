(*
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)
unit ExtraPaymentFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Dblup1a, Mask, DBCtrls, dxEditor, dxExEdtr,
  dxEdLib, ComCtrls, dxDBTLCl, dxGrClms, SystemDMT;

type
  TExtraPaymentF = class(TGridBaseF)
    pnlTeamPlantShift: TPanel;
    GroupBox1: TGroupBox;
    DateFrom: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dxDetailGridColumn4: TdxDBGridColumn;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    dxDetailGridColumn3: TdxDBGridLookupColumn;
    DBLookupCmbBoxEmployee: TDBLookupComboBox;
    dxDetailGridColumn2: TdxDBGridLookupColumn;
    DBLookupComboBox1: TDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EnabledNavButtons(Active: Boolean);
    procedure RefreshGrid;
  end;

function ExtraPaymentF: TExtraPaymentF;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UPimsConst, UPimsMessageRes,  ExtraPaymentDMT;

var
  ExtraPaymentF_HDN: TExtraPaymentF;

function ExtraPaymentF: TExtraPaymentF;
begin
  if ExtraPaymentF_HDN = nil then
    ExtraPaymentF_HDN := TExtraPaymentF.Create(Application);
  Result := ExtraPaymentF_HDN;
end;

procedure TExtraPaymentF.FormCreate(Sender: TObject);
begin
  ExtraPaymentDM := CreateFormDM(TExtraPaymentDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := ExtraPaymentDM.DataSourceDetail;
  inherited;
end;

procedure TExtraPaymentF.FormDestroy(Sender: TObject);
begin
  inherited;
  ExtraPaymentF_HDN := nil;
end;

procedure TExtraPaymentF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TExtraPaymentF.FormShow(Sender: TObject);
begin
  inherited;
  DateFrom.Date := Now;
  RefreshGrid;
  dxDetailGrid.SetFocus;
end;


procedure TExtraPaymentF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
end;

procedure TExtraPaymentF.RefreshGrid;
begin
  ExtraPaymentDM.FDate := GetDate(DateFrom.Date);
  ExtraPaymentDM.TableDetail.Refresh;
end;

procedure TExtraPaymentF.DateFromChange(Sender: TObject);
begin
  inherited;
  RefreshGrid;
end;

end.
