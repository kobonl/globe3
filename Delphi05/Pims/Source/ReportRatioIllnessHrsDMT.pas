(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportRatioIllnessHrsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, Provider, DBClient;

type
  TReportRatioIllnessHrsDM = class(TReportBaseDM)
    QueryProdMin: TQuery;
    QueryAbsence: TQuery;
    dspEmpl: TDataSetProvider;
    cdsEmpl: TClientDataSet;
    QueryEmpl: TQuery;
    cdsEmplContract: TClientDataSet;
    dspEmplContract: TDataSetProvider;
    qryEmplContract: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProdMinFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportRatioIllnessHrsDM: TReportRatioIllnessHrsDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportRatioIllnessHrsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryProdMin);
end;

procedure TReportRatioIllnessHrsDM.QueryProdMinFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
