inherited DialogAddHolidayF: TDialogAddHolidayF
  Caption = 'Add Holiday / Absence'
  ClientHeight = 255
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 1
  end
  inherited pnlInsertBase: TPanel
    Height = 134
    Font.Color = clBlack
    Font.Height = -11
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 637
      Height = 134
      Align = alClient
      Caption = 'Add Holiday / Absence'
      TabOrder = 0
      object LblFromDateTime: TLabel
        Left = 8
        Top = 52
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblDateTime: TLabel
        Left = 40
        Top = 52
        Width = 23
        Height = 13
        Caption = 'Date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblToDateTime: TLabel
        Left = 379
        Top = 52
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 8
        Top = 78
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object Label2: TLabel
        Left = 8
        Top = 25
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object Label3: TLabel
        Left = 8
        Top = 104
        Width = 80
        Height = 13
        Caption = 'Absence Reason'
      end
      object DatePickerFrom: TDateTimePicker
        Left = 184
        Top = 48
        Width = 113
        Height = 21
        CalAlignment = dtaLeft
        Date = 41824.4050508681
        Time = 41824.4050508681
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 0
        OnCloseUp = DatePickerFromCloseUp
      end
      object DatePickerTo: TDateTimePicker
        Left = 406
        Top = 48
        Width = 113
        Height = 21
        CalAlignment = dtaLeft
        Date = 41824.4050508681
        Time = 41824.4050508681
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 1
        OnCloseUp = DatePickerToCloseUp
      end
      object dxDBLUEditShift: TdxDBLookupEdit
        Tag = 1
        Left = 184
        Top = 72
        Width = 337
        TabOrder = 2
        DataField = 'SHIFTLU'
        DataSource = DialogAddHolidayDM.dsrcEmployee
        DropDownWidth = 337
        ListFieldName = 'DESCRIPTION'
      end
      object edtEmployee: TEdit
        Left = 184
        Top = 23
        Width = 333
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 3
      end
      object ComboBoxAbsRsn: TComboBox
        Tag = 1
        Left = 184
        Top = 96
        Width = 337
        Height = 21
        DropDownCount = 6
        ItemHeight = 13
        TabOrder = 4
        OnExit = ComboBoxAbsRsnExit
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 236
  end
  inherited pnlBottom: TPanel
    Top = 195
    inherited btnOk: TBitBtn
      OnClick = btnOkClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
