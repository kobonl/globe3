inherited HoursPerEmployeeF: THoursPerEmployeeF
  Left = 282
  Top = 119
  Width = 694
  Height = 591
  Caption = 'Hours per Employee'
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 678
    Height = 75
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 73
      Width = 676
      Height = 1
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 124
      Height = 1
      Align = alNone
    end
    object GroupBoxEmployee: TGroupBox
      Left = 1
      Top = 1
      Width = 676
      Height = 68
      Align = alTop
      Caption = 'Employee / Period'
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 20
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object Label2: TLabel
        Left = 16
        Top = 44
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object Label3: TLabel
        Left = 232
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Week'
      end
      object LabelStartWeekDate: TLabel
        Left = 364
        Top = 44
        Width = 27
        Height = 13
        Caption = 'From '
      end
      object LabelEndWeekDate: TLabel
        Left = 460
        Top = 44
        Width = 12
        Height = 13
        Caption = 'To'
      end
      object LabelEmplDesc: TLabel
        Left = 176
        Top = 20
        Width = 70
        Height = 13
        Caption = 'LabelEmplDesc'
      end
      object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
        Left = 72
        Top = 16
        Width = 97
        TabOrder = 0
        OnClick = dxDBExtLookupEditEmployeeClick
        OnEnter = dxDBExtLookupEditEmployeeEnter
        AutoSize = False
        DataField = 'EMPLOYEE_NUMBER'
        DataSource = HoursPerEmployeeDM.DataSourceMaster
        PopupHeight = 200
        PopupWidth = 500
        OnCloseUp = dxDBExtLookupEditEmployeeCloseUp
        DBGridLayout = dxDBGridLayoutList1Item1
        Height = 21
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 72
        Top = 40
        Width = 73
        TabOrder = 1
        OnChange = DefaultChangeDate
        MaxValue = 2099
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
      object dxSpinEditWeek: TdxSpinEdit
        Left = 272
        Top = 40
        Width = 73
        TabOrder = 2
        OnChange = DefaultChangeDate
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object BitBtnFirst: TBitBtn
        Left = 279
        Top = 9
        Width = 27
        Height = 30
        TabOrder = 3
        OnClick = BitBtnFirstClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000000000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACAC2B2B2BAC
          ACACACACACACACACACACACACACACACACACACACAC2BCDCDACACACACACAC00ACAC
          ACACACACACACACACACACACAC2BCDCDACACACACAC0000ACACACACACACACACACAC
          ACACACAC2BCDCDACACACAC00C700ACACACACACACACACACACACACACAC2BCDCDAC
          ACAC00C7C700ACACACACACACACACACACACACACAC2BCDCDACAC00C7CDC7000000
          000000000000ACACACACACAC2BCDCDAC00C7CDCDC7C7C7C7C7C7C7C7C700ACAC
          ACACACAC2BCDCDC1D6D5CDCDCDCDCDCDCDCDCDCDC700ACACACACACAC2BCDCDAC
          C1D6D5CDD5D6D5D6D5D5D6D5C700ACACACACACAC2BCDCDACACC1D6D5D500C1C1
          C1C1C1C1C100ACACACACACAC2BCDCDACACACC1D6D500ACACACACACACACACACAC
          ACACACAC2BCDCDACACACACC1D600ACACACACACACACACACACACACACAC2BCDCDAC
          ACACACACC100ACACACACACACACACACACACACACAC2BCDCDACACACACACAC00ACAC
          ACACACACACACACACACACACAC2BCDCDACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object BitBtnPrev: TBitBtn
        Left = 306
        Top = 9
        Width = 27
        Height = 30
        TabOrder = 4
        OnClick = BitBtnPrevClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000000000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC00ACAC
          ACACACACACACACACACACACACACACACACACACACAC0000ACACACACACACACACACAC
          ACACACACACACACACACACAC00C700ACACACACACACACACACACACACACACACACACAC
          ACAC00C7C700ACACACACACACACACACACACACACACACACACACAC00C7CDC7000000
          000000000000ACACACACACACACACACAC00C7CDCDC7C7C7C7C7C7C7C7C700ACAC
          ACACACACACACACC1D6D5CDCDCDCDCDCDCDCDCDCDC700ACACACACACACACACACAC
          C1D6D5CDD5D6D5D6D5D5D6D5C700ACACACACACACACACACACACC1D6D5D500C1C1
          C1C1C1C1C100ACACACACACACACACACACACACC1D6D500ACACACACACACACACACAC
          ACACACACACACACACACACACC1D600ACACACACACACACACACACACACACACACACACAC
          ACACACACC100ACACACACACACACACACACACACACACACACACACACACACACAC00ACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object BitBtnNext: TBitBtn
        Left = 333
        Top = 9
        Width = 27
        Height = 30
        TabOrder = 5
        OnClick = BitBtnNextClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000000000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC00
          ACACACACACACACACACACACACACACACACACACACACACACAC0000ACACACACACACAC
          ACACACACACACACACACACACACACACAC00C100ACACACACACACACACACACACACACAC
          ACACACACACACAC00C7C100ACACACACACACACACACACACAC000000000000000000
          C7CDC100ACACACACACACACACACACACC1C7C7C7C7C7C7C7C7C7CDCDC100ACACAC
          ACACACACACACACC1D6CDCDCDCDCDCDCDCDCDCDCDC100ACACACACACACACACACC1
          D6D6D6D6D6D6D6D6D6CDD5CD00ACACACACACACACACACACC1C1C1C1C1C1C1C1C1
          D6D5CD00ACACACACACACACACACACACACACACACACACACACC1D6CD00ACACACACAC
          ACACACACACACACACACACACACACACACC1CD00ACACACACACACACACACACACACACAC
          ACACACACACACACC100ACACACACACACACACACACACACACACACACACACACACACACC1
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object BitBtnLast: TBitBtn
        Left = 360
        Top = 9
        Width = 27
        Height = 30
        TabOrder = 6
        OnClick = BitBtnLastClick
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          0800000000004002000000000000000000000001000000000000000000000034
          190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
          9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
          FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
          CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
          FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
          870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
          7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
          C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
          FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
          570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
          790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
          B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
          FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
          FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
          A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
          C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
          990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
          FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
          BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
          D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
          CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
          FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
          FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
          B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
          FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
          FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
          F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000ACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACAC2B2B2BACACACACACACACACACACACACACAC00ACAC
          ACACACACCDCD2BACACACACACACACACACACACACACAC0000ACACACACACCDCD2BAC
          ACACACACACACACACACACACACAC00C100ACACACACCDCD2BACACACACACACACACAC
          ACACACACAC00C7C100ACACACCDCD2BACACACACACAC000000000000000000C7CD
          C100ACACCDCD2BACACACACACACC1C7C7C7C7C7C7C7C7C7CDCDC100ACCDCD2BAC
          ACACACACACC1D6CDCDCDCDCDCDCDCDCDCDCDC100CDCD2BACACACACACACC1D6D6
          D6D6D6D6D6D6D6CDD5CD00ACCDCD2BACACACACACACC1C1C1C1C1C1C1C1C1D6D5
          CD00ACACCDCD2BACACACACACACACACACACACACACACC1D6CD00ACACACCDCD2BAC
          ACACACACACACACACACACACACACC1CD00ACACACACCDCD2BACACACACACACACACAC
          ACACACACACC100ACACACACACCDCD2BACACACACACACACACACACACACACACC1ACAC
          ACACACACCDCD2BACACACACACACACACACACACACACACACACACACACACACCDCD2BAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
          ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
      end
      object cBoxShowOnlyActive: TCheckBox
        Left = 392
        Top = 16
        Width = 185
        Height = 17
        Caption = 'Show Only Active'
        Checked = True
        State = cbChecked
        TabOrder = 7
        OnClick = cBoxShowOnlyActiveClick
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 440
    Width = 678
    Height = 112
    TabOrder = 3
    object GroupBoxAbsenceHour: TGroupBox
      Left = 1
      Top = 1
      Width = 676
      Height = 110
      Align = alClient
      Caption = 'Absence Hours Details'
      TabOrder = 0
      object Label40: TLabel
        Left = 16
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay1'
      end
      object Label41: TLabel
        Left = 109
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay2'
      end
      object Label42: TLabel
        Left = 201
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay3'
      end
      object Label43: TLabel
        Left = 294
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay4'
      end
      object Label44: TLabel
        Left = 387
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay5'
      end
      object Label45: TLabel
        Left = 479
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay6'
      end
      object Label46: TLabel
        Left = 572
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay7'
      end
      object Label47: TLabel
        Left = 16
        Top = 20
        Width = 77
        Height = 13
        Caption = 'Absence reason'
      end
      object DBLookupComboBoxAbsenceReason: TDBLookupComboBox
        Tag = 1
        Left = 112
        Top = 16
        Width = 265
        Height = 21
        DataField = 'ABSENCEREASONLU'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        KeyField = 'ABSENCEREASON_CODE'
        ListField = 'ABSENCEREASON_CODE;DESCRIPTION'
        ListSource = HoursPerEmployeeDM.DataSourceAbsenceReason
        TabOrder = 0
      end
      object dxDBTimeEditAbsence1: TdxDBTimeEdit
        Left = 12
        Top = 56
        Width = 73
        TabOrder = 1
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY1'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence2: TdxDBTimeEdit
        Left = 105
        Top = 56
        Width = 73
        TabOrder = 2
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY2'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence3: TdxDBTimeEdit
        Left = 197
        Top = 56
        Width = 73
        TabOrder = 3
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY3'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence4: TdxDBTimeEdit
        Left = 290
        Top = 56
        Width = 73
        TabOrder = 4
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY4'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence5: TdxDBTimeEdit
        Left = 383
        Top = 56
        Width = 73
        TabOrder = 5
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY5'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence6: TdxDBTimeEdit
        Left = 475
        Top = 56
        Width = 73
        TabOrder = 6
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY6'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditAbsence7: TdxDBTimeEdit
        Left = 568
        Top = 56
        Width = 73
        TabOrder = 7
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY7'
        DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
    end
    object GroupBoxSalaryHour: TGroupBox
      Left = 1
      Top = 1
      Width = 676
      Height = 110
      Align = alClient
      Caption = 'Salary Hours Details'
      TabOrder = 1
      object Label32: TLabel
        Left = 16
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay1'
      end
      object Label33: TLabel
        Left = 109
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay2'
      end
      object Label34: TLabel
        Left = 201
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay3'
      end
      object Label35: TLabel
        Left = 294
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay4'
      end
      object Label36: TLabel
        Left = 387
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay5'
      end
      object Label37: TLabel
        Left = 479
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay6'
      end
      object Label38: TLabel
        Left = 572
        Top = 40
        Width = 48
        Height = 13
        Caption = 'LongDay7'
      end
      object Label39: TLabel
        Left = 16
        Top = 20
        Width = 80
        Height = 13
        Caption = 'Salary hour type'
      end
      object DBLookupComboBoxDetailSalary: TDBLookupComboBox
        Tag = 1
        Left = 155
        Top = 16
        Width = 301
        Height = 21
        DataField = 'HOURTYPELU'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        KeyField = 'HOURTYPE_NUMBER'
        ListField = 'HOURTYPE_NUMBER;HDESCRIPTION'
        ListSource = HoursPerEmployeeDM.DataSourceHourtype
        TabOrder = 1
      end
      object dxDBTimeEditSalary1: TdxDBTimeEdit
        Left = 12
        Top = 56
        Width = 73
        TabOrder = 2
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY1'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary2: TdxDBTimeEdit
        Left = 105
        Top = 56
        Width = 73
        TabOrder = 3
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY2'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary3: TdxDBTimeEdit
        Left = 197
        Top = 56
        Width = 73
        TabOrder = 4
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY3'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary4: TdxDBTimeEdit
        Left = 290
        Top = 56
        Width = 73
        TabOrder = 5
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY4'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary5: TdxDBTimeEdit
        Left = 383
        Top = 56
        Width = 73
        TabOrder = 6
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY5'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary6: TdxDBTimeEdit
        Left = 475
        Top = 56
        Width = 73
        TabOrder = 7
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY6'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditSalary7: TdxDBTimeEdit
        Left = 568
        Top = 56
        Width = 73
        TabOrder = 8
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY7'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object edtHourType: TdxDBEdit
        Tag = 1
        Left = 104
        Top = 16
        Width = 49
        TabOrder = 0
        OnExit = edtHourTypeExit
        DataField = 'HOURTYPE_NUMBER'
        DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
      end
    end
    object GroupBoxProductionHour: TGroupBox
      Left = 1
      Top = 1
      Width = 676
      Height = 110
      Align = alClient
      Caption = 'Production Hours Details '
      TabOrder = 2
      object Label20: TLabel
        Left = 16
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay1'
      end
      object Label21: TLabel
        Left = 109
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay2'
      end
      object Label22: TLabel
        Left = 201
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay3'
      end
      object Label23: TLabel
        Left = 294
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay4'
      end
      object Label24: TLabel
        Left = 387
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay5'
      end
      object Label25: TLabel
        Left = 479
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay6'
      end
      object Label26: TLabel
        Left = 572
        Top = 65
        Width = 48
        Height = 13
        Caption = 'LongDay7'
      end
      object Label29: TLabel
        Left = 12
        Top = 44
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object Label30: TLabel
        Left = 366
        Top = 20
        Width = 46
        Height = 13
        Caption = 'Workspot'
      end
      object Label31: TLabel
        Left = 366
        Top = 44
        Width = 17
        Height = 13
        Caption = 'Job'
      end
      object Label50: TLabel
        Left = 12
        Top = 20
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object lblWorkspotDescription: TLabel
        Left = 512
        Top = 20
        Width = 109
        Height = 13
        Caption = 'lblWorkspotDescription'
      end
      object DBLookupComboBoxProdShift: TDBLookupComboBox
        Tag = 1
        Left = 56
        Top = 40
        Width = 217
        Height = 21
        DataField = 'SHIFT_NUMBER'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        DropDownRows = 5
        DropDownWidth = 240
        KeyField = 'SHIFT_NUMBER'
        ListField = 'DESCRIPTION;SHIFT_NUMBER'
        ListSource = HoursPerEmployeeDM.DataSourceShift
        TabOrder = 1
      end
      object DBLookupComboBoxProdWorkspot: TDBLookupComboBox
        Tag = 1
        Left = 424
        Top = 16
        Width = 81
        Height = 21
        DataField = 'WORKSPOT_CODE'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        DropDownRows = 6
        DropDownWidth = 240
        KeyField = 'WORKSPOT_CODE'
        ListField = 'DESCRIPTION;WORKSPOT_CODE'
        ListSource = HoursPerEmployeeDM.DataSourceWorkspotLU
        TabOrder = 11
        Visible = False
      end
      object DBLookupComboBoxPlant: TDBLookupComboBox
        Tag = 1
        Left = 56
        Top = 16
        Width = 217
        Height = 21
        DataField = 'PLANTLU'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        DropDownRows = 6
        DropDownWidth = 240
        KeyField = 'PLANT_CODE'
        ListField = 'DESCRIPTION;PLANT_CODE'
        ListSource = HoursPerEmployeeDM.DataSourcePlant
        TabOrder = 0
      end
      object DBLookupComboBoxProdJob: TDBLookupComboBox
        Left = 424
        Top = 40
        Width = 217
        Height = 21
        DataField = 'JOB_CODE'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        DropDownRows = 5
        DropDownWidth = 240
        KeyField = 'JOB_CODE'
        ListField = 'DESCRIPTION;JOB_CODE'
        ListSource = HoursPerEmployeeDM.DataSourceJob
        TabOrder = 3
      end
      object dxDBTimeEditProdDay1: TdxDBTimeEdit
        Left = 12
        Top = 80
        Width = 73
        TabOrder = 4
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY1'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay2: TdxDBTimeEdit
        Left = 105
        Top = 80
        Width = 73
        TabOrder = 5
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY2'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay3: TdxDBTimeEdit
        Left = 197
        Top = 80
        Width = 73
        TabOrder = 6
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY3'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay4: TdxDBTimeEdit
        Left = 290
        Top = 80
        Width = 73
        TabOrder = 7
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY4'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay5: TdxDBTimeEdit
        Left = 383
        Top = 80
        Width = 73
        TabOrder = 8
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY5'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay6: TdxDBTimeEdit
        Left = 475
        Top = 80
        Width = 73
        TabOrder = 9
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY6'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditProdDay7: TdxDBTimeEdit
        Left = 568
        Top = 80
        Width = 73
        TabOrder = 10
        OnDblClick = DefaultTimeEditDblClick
        DataField = 'DAY7'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        OnChange = DefaultProdTimeEditChange
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit
        Tag = 1
        Left = 424
        Top = 16
        Width = 81
        Style.BorderStyle = xbs3D
        TabOrder = 2
        OnEnter = dxDBExtLookupEditWorkspotEnter
        OnExit = dxDBExtLookupEditWorkspotExit
        OnKeyPress = dxDBExtLookupEditWorkspotKeyPress
        DataField = 'WORKSPOT_CODE'
        DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
        ImmediateDropDown = True
        PopupHeight = 100
        PopupWidth = 241
        OnPopup = dxDBExtLookupEditWorkspotPopup
        DBGridLayout = dxDBGridLayoutListWorkspotItem
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 101
    Width = 678
    Height = 339
    inherited spltDetail: TSplitter
      Top = 337
      Width = 676
      Height = 1
    end
    inherited dxDetailGrid: TdxDBGrid
      Top = 160
      Height = 2
      Align = alNone
      TabOrder = 1
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 676
      Height = 336
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = DefaultSetFilterData
      OnChanging = PageControl1Changing
      object TabSheet1: TTabSheet
        Caption = 'Production Hours'
        object dxDBGridProductionHour: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 0
          Width = 668
          Height = 173
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Production Hours'
              Width = 616
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'PLANT_CODE'
          SummaryGroups = <
            item
              DefaultGroup = False
              SummaryItems = <>
              Name = 'dxDBGridProductionHourSummaryGroup2'
            end>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          DataSource = HoursPerEmployeeDM.DataSourceHREPRODUCTION
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssVertical
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          object dxDBGridProductionHourColumnPlant: TdxDBGridLookupColumn
            Caption = 'Plant'
            DisableEditor = True
            Width = 96
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PLANTLU'
            ListFieldName = 'PLANT_CODE;DESCRIPTION'
          end
          object dxDBGridProductionHourColumnWorkspot: TdxDBGridLookupColumn
            Caption = 'Workspot'
            DisableEditor = True
            Width = 86
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOTLU'
            ListFieldName = 'WORKSPOT_CODE;DESCRIPTION'
          end
          object dxDBGridProductionHourColumnShift: TdxDBGridLookupColumn
            Caption = 'Shift'
            DisableEditor = True
            Width = 96
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFTLU'
            ListFieldName = 'SHIFT_NUMBER;DESCRIPTION'
          end
          object dxDBGridProductionHourColumnJob: TdxDBGridColumn
            Caption = 'Job'
            DisableEditor = True
            Width = 66
            BandIndex = 0
            RowIndex = 0
            FieldName = 'JOBLU'
          end
          object dxDBGridProductionHourColumnDay1: TdxDBGridColumn
            Caption = 'Day1'
            Width = 51
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY1'
          end
          object dxDBGridProductionHourColumnDay2: TdxDBGridColumn
            Caption = 'Day2'
            Width = 46
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridProductionHourColumnDay3: TdxDBGridColumn
            Caption = 'Day3'
            Width = 40
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridProductionHourColumnDay4: TdxDBGridColumn
            Caption = 'Day4'
            Width = 44
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridProductionHourColumnDay5: TdxDBGridColumn
            Caption = 'Day5'
            Width = 41
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridProductionHourColumnDay6: TdxDBGridColumn
            Caption = 'Day6'
            Width = 41
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridProductionHourColumnDay7: TdxDBGridColumn
            Caption = 'Day7'
            Width = 39
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridProductionHourColumnTotal: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            DisableEditor = True
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
        object dxDBGridAbsenceHour: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 173
          Width = 668
          Height = 98
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Absence Hours'
              Width = 665
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alBottom
          TabOrder = 1
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssVertical
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          object dxDBGridAbsenceHourColumnAbsenceReason: TdxDBGridLookupColumn
            Caption = 'Absence Reason'
            DisableEditor = True
            Width = 342
            BandIndex = 0
            RowIndex = 0
            FieldName = 'ABSENCEREASONLU'
            ListFieldName = 'ABSENCEREASON_CODE;DESCRIPTION'
          end
          object dxDBGridAbsenceHourColumnDay1: TdxDBGridColumn
            Caption = 'Day1'
            Width = 40
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY1'
          end
          object dxDBGridAbsenceHourColumnDay2: TdxDBGridColumn
            Caption = 'Day2'
            Width = 45
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridAbsenceHourColumnDay3: TdxDBGridColumn
            Caption = 'Day3'
            Width = 48
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridAbsenceHourColumnDay4: TdxDBGridColumn
            Caption = 'Day4'
            Width = 42
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridAbsenceHourColumnDay5: TdxDBGridColumn
            Caption = 'Day5'
            Width = 43
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridAbsenceHourColumnDay6: TdxDBGridColumn
            Caption = 'Day6'
            Width = 45
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridAbsenceHourColumnDay7: TdxDBGridColumn
            Caption = 'Day7'
            Width = 40
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridAbsenceHourColumnTotal: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            DisableEditor = True
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
        object dxDBGridPTotal: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 271
          Width = 668
          Height = 37
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Total Hours'
              MinWidth = 5
              Width = 342
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day1'
              MinWidth = 5
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day2'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day3'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day4'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day5'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day6'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day7'
              MinWidth = 10
              Width = 40
            end
            item
              Alignment = taLeftJustify
              Caption = 'Total'
              MinWidth = 10
              Width = 40
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alBottom
          TabOrder = 2
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssNone
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxDBGridPTotalCustomDrawColumnHeader
          object dxDBGridLookupColumn1: TdxDBGridLookupColumn
            Tag = 2
            DisableEditor = True
            Width = 342
            BandIndex = 0
            RowIndex = 0
          end
          object dxDBGridColumn1: TdxDBGridColumn
            Caption = 'Day1'
            Width = 40
            BandIndex = 1
            RowIndex = 0
          end
          object dxDBGridColumn2: TdxDBGridColumn
            Caption = 'Day2'
            Width = 45
            BandIndex = 2
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridColumn3: TdxDBGridColumn
            Caption = 'Day3'
            Width = 48
            BandIndex = 3
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridColumn4: TdxDBGridColumn
            Caption = 'Day4'
            Width = 42
            BandIndex = 4
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridColumn5: TdxDBGridColumn
            Caption = 'Day5'
            Width = 43
            BandIndex = 5
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridColumn6: TdxDBGridColumn
            Caption = 'Day6'
            Width = 45
            BandIndex = 6
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridColumn7: TdxDBGridColumn
            Caption = 'Day7'
            Width = 40
            BandIndex = 7
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridColumn8: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            DisableEditor = True
            BandIndex = 8
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Salary Hours'
        ImageIndex = 1
        object dxDBGridSalaryHour: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 0
          Width = 668
          Height = 173
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Salary Hours'
              Width = 654
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'HOURTYPE_NUMBER'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          DataSource = HoursPerEmployeeDM.DataSourceHRESALARY
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoCanAppend, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoResetColumnFocus, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssVertical
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          object dxDBGridSalaryHourColumnHourType: TdxDBGridLookupColumn
            Caption = 'Hour Type'
            ReadOnly = True
            Width = 260
            BandIndex = 0
            RowIndex = 0
            FieldName = 'HOURTYPESHOW'
            ListFieldName = 'HOURTYPE_NUMBER;DESCRIPTION'
          end
          object dxDBGridSalaryHourColumnDay1: TdxDBGridColumn
            Caption = 'Day1'
            Width = 61
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY1'
          end
          object dxDBGridSalaryHourColumnDay2: TdxDBGridColumn
            Caption = 'Day2'
            Width = 63
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridSalaryHourColumnDay3: TdxDBGridColumn
            Caption = 'Day3'
            Width = 59
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridSalaryHourColumnDay4: TdxDBGridColumn
            Caption = 'Day4'
            Width = 54
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridSalaryHourColumnDay5: TdxDBGridColumn
            Caption = 'Day5'
            Width = 48
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridSalaryHourColumnDay6: TdxDBGridColumn
            Caption = 'Day6'
            Width = 51
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridSalaryHourColumnDay7: TdxDBGridColumn
            Caption = 'Day7'
            Width = 49
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridSalaryHourColumnTotal: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            ReadOnly = True
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
        object dxDBGridAbsenceHourS: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 173
          Width = 668
          Height = 98
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Absence Hours'
              Width = 649
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alBottom
          TabOrder = 1
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          DataSource = HoursPerEmployeeDM.DataSourceHREABSENCE
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoCanAppend, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssVertical
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          object dxDBGridAbsenceHourSColumnReason: TdxDBGridLookupColumn
            Caption = 'Absence Reason'
            Width = 260
            BandIndex = 0
            RowIndex = 0
            FieldName = 'ABSENCEREASONLU'
            ListFieldName = 'ABSENCETYPE_CODE'
          end
          object dxDBGridAbsenceHourSColumnDay1: TdxDBGridColumn
            Caption = 'Day1'
            Width = 60
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY1'
          end
          object dxDBGridAbsenceHourSColumnDay2: TdxDBGridColumn
            Caption = 'Day2'
            Width = 66
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridAbsenceHourSColumnDay3: TdxDBGridColumn
            Caption = 'day3'
            Width = 57
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridAbsenceHourSColumnDay4: TdxDBGridColumn
            Caption = 'Day4'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridAbsenceHourSColumnDay5: TdxDBGridColumn
            Caption = 'Day5'
            Width = 51
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridAbsenceHourSColumnDay6: TdxDBGridColumn
            Caption = 'Day6'
            Width = 48
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridAbsenceHourSColumnDay7: TdxDBGridColumn
            Caption = 'Day7'
            Width = 51
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridAbsenceHourSColumnTotal: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
        object dxDBGridSTotal: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 271
          Width = 668
          Height = 37
          Bands = <
            item
              Alignment = taLeftJustify
              Caption = 'Total Hours'
              Width = 649
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day1'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day2'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day3'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day4'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day5'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'DAY6'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Day7'
              Width = 60
            end
            item
              Alignment = taLeftJustify
              Caption = 'Total'
              Width = 60
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alBottom
          TabOrder = 2
          OnClick = DefaultGridClick
          OnEnter = DefaultGridEnter
          HideSelectionTextColor = clWindowText
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsCustomize = []
          OptionsDB = [edgoCanAppend, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ScrollBars = ssNone
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnChangeNode = GridChangeRecord
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxDBGridDefaultCustomDrawCell
          OnCustomDrawColumnHeader = dxDBGridPTotalCustomDrawColumnHeader
          object dxDBGridLookupColumn2: TdxDBGridLookupColumn
            Width = 260
            BandIndex = 0
            RowIndex = 0
          end
          object dxDBGridColumn9: TdxDBGridColumn
            Caption = 'Day1'
            Width = 60
            BandIndex = 1
            RowIndex = 0
            FieldName = 'DAY1'
          end
          object dxDBGridColumn10: TdxDBGridColumn
            Caption = 'Day2'
            Width = 66
            BandIndex = 2
            RowIndex = 0
            FieldName = 'DAY2'
          end
          object dxDBGridColumn11: TdxDBGridColumn
            Caption = 'day3'
            Width = 57
            BandIndex = 3
            RowIndex = 0
            FieldName = 'DAY3'
          end
          object dxDBGridColumn12: TdxDBGridColumn
            Caption = 'Day4'
            Width = 53
            BandIndex = 4
            RowIndex = 0
            FieldName = 'DAY4'
          end
          object dxDBGridColumn13: TdxDBGridColumn
            Caption = 'Day5'
            Width = 51
            BandIndex = 5
            RowIndex = 0
            FieldName = 'DAY5'
          end
          object dxDBGridColumn14: TdxDBGridColumn
            Caption = 'Day6'
            Width = 48
            BandIndex = 6
            RowIndex = 0
            FieldName = 'DAY6'
          end
          object dxDBGridColumn15: TdxDBGridColumn
            Caption = 'Day7'
            Width = 51
            BandIndex = 7
            RowIndex = 0
            FieldName = 'DAY7'
          end
          object dxDBGridColumn16: TdxDBGridColumn
            Alignment = taLeftJustify
            Caption = 'Total'
            BandIndex = 8
            RowIndex = 0
            FieldName = 'TOTAL'
            OnGetText = DefaultFormatTotalCol
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Balances'
        ImageIndex = 2
        object Employee: TGroupBox
          Left = 0
          Top = 0
          Width = 668
          Height = 41
          Align = alTop
          Caption = 'Employee'
          TabOrder = 0
          object Label4: TLabel
            Left = 20
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Start Date'
          end
          object Label48: TLabel
            Left = 244
            Top = 16
            Width = 102
            Height = 13
            Alignment = taRightJustify
            Caption = 'Max. salary balance  '
          end
          object EditStartDate: TEdit
            Left = 116
            Top = 12
            Width = 77
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '01/01/2002'
          end
          object MaskEditMaxSalaryBalance: TMaskEdit
            Left = 352
            Top = 13
            Width = 55
            Height = 21
            EditMask = '#999:99;;0'
            MaxLength = 7
            TabOrder = 1
            Text = '    :  '
            OnKeyDown = MaskEditKeyChange
            OnKeyUp = MaskEditKeyChange
          end
          object MaskEditMaxSalaryBalanceBackup: TMaskEdit
            Left = 416
            Top = 12
            Width = 17
            Height = 21
            EditMask = '#999:99;;0'
            MaxLength = 7
            TabOrder = 2
            Text = '    :  '
            Visible = False
          end
        end
        object GroupBoxTravelTime: TGroupBox
          Left = 0
          Top = 135
          Width = 668
          Height = 49
          Align = alTop
          Caption = 'Travel Time'
          TabOrder = 1
          object Label49: TLabel
            Left = 16
            Top = 20
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label66: TLabel
            Left = 200
            Top = 20
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object Label70: TLabel
            Left = 320
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label71: TLabel
            Left = 430
            Top = 20
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label76: TLabel
            Left = 550
            Top = 20
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditTravelTimeRemaining: TEdit
            Left = 116
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditTravelTimeEarned: TEdit
            Left = 238
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditTravelTimeUsed: TEdit
            Left = 350
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditTravelTimePlanned: TEdit
            Left = 472
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditTravelTimeAvailable: TEdit
            Left = 594
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 90
          Width = 668
          Height = 45
          Align = alTop
          Caption = 'Illness'
          TabOrder = 2
          object Label15: TLabel
            Left = 44
            Top = 20
            Width = 66
            Height = 13
            Caption = 'Selected year'
          end
          object Label16: TLabel
            Left = 318
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Total'
          end
          object EditIllness: TEdit
            Left = 116
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditIllnessUntil: TEdit
            Left = 350
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
        end
        object GroupBox3: TGroupBox
          Left = 0
          Top = 41
          Width = 668
          Height = 49
          Align = alTop
          Caption = 'Worktime Reduction'
          TabOrder = 3
          object Label8: TLabel
            Left = 16
            Top = 20
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label9: TLabel
            Left = 200
            Top = 20
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object Label12: TLabel
            Left = 320
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label13: TLabel
            Left = 430
            Top = 20
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label14: TLabel
            Left = 550
            Top = 20
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditWorkRemaining: TEdit
            Left = 116
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditWorkEarned: TEdit
            Left = 238
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditWorkUsed: TEdit
            Left = 350
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditWorkPlanned: TEdit
            Left = 472
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditWorkAvailable: TEdit
            Left = 594
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Holiday Counters'
        ImageIndex = 3
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Holiday'
          TabOrder = 0
          object LabelHolidayRemaining: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label7: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object Label10: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label11: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label5: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditHolidayRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditHolidayPlanned: TEdit
            Left = 472
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object MaskEditHolidayNorm: TMaskEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 4
            Text = '   :  '
            OnKeyDown = MaskEditKeyChange
            OnKeyUp = MaskEditKeyChange
          end
          object MaskEditHolidayNormBackup: TMaskEdit
            Left = 296
            Top = 11
            Width = 17
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 5
            Text = '   :  '
            Visible = False
          end
        end
        object GroupBox5: TGroupBox
          Left = 0
          Top = 39
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Time for Time'
          TabOrder = 1
          object Label17: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label19: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label27: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label28: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label61: TLabel
            Left = 200
            Top = 15
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object EditTimeRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditTimeUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditTimePlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditTimeAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditTimeEarned: TEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBoxHldPrevYear: TGroupBox
          Left = 0
          Top = 78
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Holiday Previous Year'
          TabOrder = 3
          object Label55: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label57: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label58: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label59: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditHldPrevYearRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditHldPrevYearUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditHldPrevYearPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditHldPrevYearAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
        end
        object GroupBoxSeniorityHld: TGroupBox
          Left = 0
          Top = 117
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Seniority Holiday'
          TabOrder = 2
          object Label52: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label53: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label54: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label51: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object EditSeniorityHldUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditSeniorityHldPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditSeniorityHldAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object MaskEditSeniorityHldNorm: TMaskEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 3
            Text = '   :  '
            OnKeyDown = MaskEditKeyChange
            OnKeyUp = MaskEditKeyChange
          end
          object MaskEditSeniorityHldNormBackup: TMaskEdit
            Left = 296
            Top = 11
            Width = 17
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 4
            Text = '   :  '
            Visible = False
          end
        end
        object GroupBoxAddBnkHoliday: TGroupBox
          Left = 0
          Top = 156
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Additional Bank Holiday'
          TabOrder = 5
          object Label65: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label67: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label68: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label69: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditAddBnkHolidayRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditAddBnkHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditAddBnkHolidayPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditAddBnkHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
        end
        object GroupBoxSatCredit: TGroupBox
          Left = 0
          Top = 195
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Saturday Credit'
          TabOrder = 7
          object Label75: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
            Visible = False
          end
          object Label77: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label78: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label79: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label18: TLabel
            Left = 200
            Top = 15
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object EditSatCreditRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
            Visible = False
          end
          object EditSatCreditUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditSatCreditPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditSatCreditAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditSatCreditEarned: TEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBoxRsvBnkHoliday: TGroupBox
          Left = 0
          Top = 234
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Bank Holiday to Reserve'
          TabOrder = 6
          object Label72: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label73: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label74: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label56: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object EditRsvBnkHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditRsvBnkHolidayPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditRsvBnkHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object MaskEditRsvBnkHolidayNormBackup: TMaskEdit
            Left = 296
            Top = 11
            Width = 17
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 4
            Text = '   :  '
            Visible = False
          end
          object EditNormBankHldResMinManYN: TEdit
            Left = 240
            Top = 11
            Width = 33
            Height = 21
            TabOrder = 5
            Text = 'EditNormBankHldResMinManYN'
            Visible = False
          end
          object MaskEditRsvBnkHolidayNorm: TMaskEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 3
            Text = '   :  '
            OnKeyDown = MaskEditKeyChange
            OnKeyUp = MaskEditKeyChange
          end
        end
        object GroupBoxShorterWeek: TGroupBox
          Left = 0
          Top = 273
          Width = 668
          Height = 39
          Align = alTop
          Caption = 'Shorter Working Week'
          TabOrder = 4
          object Label60: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label62: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label63: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label64: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label6: TLabel
            Left = 200
            Top = 15
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object EditShorterWeekRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditShorterWeekUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditShorterWeekPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditShorterWeekAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object MaskEditShorterWeekEarned: TMaskEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 4
            Text = '   :  '
            OnKeyDown = MaskEditKeyChange
            OnKeyUp = MaskEditKeyChange
          end
          object MaskEditShorterWeekEarnedBackup: TMaskEdit
            Left = 296
            Top = 10
            Width = 16
            Height = 21
            EditMask = '999:99;;0'
            MaxLength = 6
            TabOrder = 5
            Text = '   :  '
            Visible = False
          end
        end
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 608
    Top = 40
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonSort: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 640
    Top = 40
  end
  inherited StandardMenuActionList: TActionList
    Left = 576
    Top = 40
  end
  inherited dsrcActive: TDataSource
    Left = 656
    Top = 64
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 536
    Top = 39
    object dxDBGridLayoutList1Item1: TdxDBGridLayout
      Data = {
        BF030000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F757263650723486F757273506572456D706C6F796565444D2E44617461
        536F757263654D6173746572104F7074696F6E73437573746F6D697A650B0E65
        64676F42616E644D6F76696E670E6564676F42616E6453697A696E6710656467
        6F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E65
        64676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F4361
        6E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43616E
        496E73657274116564676F43616E4E617669676174696F6E116564676F436F6E
        6669726D44656C657465126564676F4C6F6164416C6C5265636F726473106564
        676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D6E
        0C436F6C756D6E4E756D6265720743617074696F6E06064E756D626572094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060F
        454D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D
        6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274
        204E616D6505576964746802480942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060A53484F52545F4E414D4500000F546478
        444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06
        044E616D6505576964746803F4000942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060B4445534352495054494F4E00000F54
        6478444247726964436F6C756D6E0B436F6C756D6E506C616E74074361707469
        6F6E0605506C616E74055769647468027C0942616E64496E646578020008526F
        77496E6465780200094669656C644E616D650607504C414E544C5500000F5464
        78444247726964436F6C756D6E0B436F6C756D6E53686966740743617074696F
        6E060553686966740756697369626C65080942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060C53484946545F4E554D424552
        000000}
    end
  end
  object dxDBGridLayoutListWorkspot: TdxDBGridLayoutList
    Left = 496
    Top = 43
    object dxDBGridLayoutListWorkspotItem: TdxDBGridLayout
      Data = {
        21020000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0608576F726B73706F7400000D44656661756C744C61
        796F7574091348656164657250616E656C526F77436F756E740201084B657946
        69656C64060D574F524B53504F545F434F44450D53756D6D61727947726F7570
        730E001053756D6D617279536570617261746F7206022C200A44617461536F75
        7263650725486F757273506572456D706C6F796565444D2E44617461536F7572
        6365576F726B73706F74094F7074696F6E7344420B106564676F43616E63656C
        4F6E457869740D6564676F43616E44656C6574650D6564676F43616E496E7365
        7274116564676F43616E4E617669676174696F6E116564676F436F6E6669726D
        44656C657465126564676F4C6F6164416C6C5265636F726473106564676F5573
        65426F6F6B6D61726B730000135464784442477269644D61736B436F6C756D6E
        0D574F524B53504F545F434F44450743617074696F6E0604436F646505576964
        746802450942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060D574F524B53504F545F434F4445000013546478444247726964
        4D61736B436F6C756D6E0B4445534352495054494F4E0743617074696F6E060B
        4465736372697074696F6E0557696474680399000942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060B444553435249505449
        4F4E000000}
    end
  end
end
