inherited RealTimeEffMonitorF: TRealTimeEffMonitorF
  Height = 527
  Caption = 'Real Time Efficiency Monitor'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 200
    Height = 53
    Align = alNone
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 49
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 48
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 323
    TabOrder = 3
    object gBoxEmployeeEffPerShift: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 163
      Align = alClient
      Caption = 'Details'
      TabOrder = 2
      object Label25: TLabel
        Left = 8
        Top = 19
        Width = 48
        Height = 13
        Caption = 'Shift Date'
      end
      object Label26: TLabel
        Left = 8
        Top = 42
        Width = 62
        Height = 13
        Caption = 'Shift Number'
      end
      object Label27: TLabel
        Left = 8
        Top = 65
        Width = 52
        Height = 13
        Caption = 'Plant Code'
      end
      object Label28: TLabel
        Left = 8
        Top = 89
        Width = 74
        Height = 13
        Caption = 'Workspot Code'
      end
      object Label29: TLabel
        Left = 8
        Top = 112
        Width = 45
        Height = 13
        Caption = 'Job Code'
      end
      object Label32: TLabel
        Left = 328
        Top = 20
        Width = 120
        Height = 13
        Caption = 'Employee seconds actual'
      end
      object Label33: TLabel
        Left = 328
        Top = 44
        Width = 115
        Height = 13
        Caption = 'Employee seconds norm'
      end
      object Label34: TLabel
        Left = 328
        Top = 68
        Width = 89
        Height = 13
        Caption = 'Employee quantity'
      end
      object Label35: TLabel
        Left = 328
        Top = 91
        Width = 53
        Height = 13
        Caption = 'Norm Level'
      end
      object Label30: TLabel
        Left = 8
        Top = 136
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object dxDBEdit17: TdxDBEdit
        Left = 120
        Top = 109
        Width = 161
        TabOrder = 0
        DataField = 'JOB_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit18: TdxDBEdit
        Left = 120
        Top = 85
        Width = 161
        TabOrder = 1
        DataField = 'WORKSPOT_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit19: TdxDBEdit
        Left = 120
        Top = 62
        Width = 161
        TabOrder = 2
        DataField = 'PLANT_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit20: TdxDBEdit
        Left = 120
        Top = 38
        Width = 161
        TabOrder = 3
        DataField = 'SHIFT_NUMBER'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBDateEdit7: TdxDBDateEdit
        Left = 120
        Top = 15
        Width = 161
        TabOrder = 4
        DataField = 'SHIFT_DATE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit21: TdxDBEdit
        Left = 464
        Top = 87
        Width = 161
        TabOrder = 5
        DataField = 'NORMLEVEL'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit22: TdxDBEdit
        Left = 464
        Top = 63
        Width = 161
        TabOrder = 6
        DataField = 'EMPLOYEEQUANTITY'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit23: TdxDBEdit
        Left = 464
        Top = 39
        Width = 161
        TabOrder = 7
        DataField = 'EMPLOYEESECONDSNORM'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit24: TdxDBEdit
        Left = 464
        Top = 15
        Width = 161
        TabOrder = 8
        DataField = 'EMPLOYEESECONDSACTUAL'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
      object dxDBEdit25: TdxDBEdit
        Left = 120
        Top = 133
        Width = 161
        TabOrder = 9
        DataField = 'EMPLOYEE_NUMBER'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
      end
    end
    object gBoxEmployeeEffPerMinute: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 163
      Align = alClient
      Caption = 'Details'
      TabOrder = 1
      object Label14: TLabel
        Left = 8
        Top = 19
        Width = 48
        Height = 13
        Caption = 'Shift Date'
      end
      object Label15: TLabel
        Left = 8
        Top = 42
        Width = 62
        Height = 13
        Caption = 'Shift Number'
      end
      object Label16: TLabel
        Left = 8
        Top = 65
        Width = 52
        Height = 13
        Caption = 'Plant Code'
      end
      object Label17: TLabel
        Left = 8
        Top = 89
        Width = 74
        Height = 13
        Caption = 'Workspot Code'
      end
      object Label18: TLabel
        Left = 8
        Top = 112
        Width = 45
        Height = 13
        Caption = 'Job Code'
      end
      object Label19: TLabel
        Left = 328
        Top = 19
        Width = 85
        Height = 13
        Caption = 'Interval Starttime'
      end
      object Label20: TLabel
        Left = 328
        Top = 43
        Width = 79
        Height = 13
        Caption = 'Interval Endtime'
      end
      object Label21: TLabel
        Left = 328
        Top = 67
        Width = 120
        Height = 13
        Caption = 'Employee seconds actual'
      end
      object Label22: TLabel
        Left = 328
        Top = 91
        Width = 115
        Height = 13
        Caption = 'Employee seconds norm'
      end
      object Label23: TLabel
        Left = 328
        Top = 115
        Width = 89
        Height = 13
        Caption = 'Employee quantity'
      end
      object Label24: TLabel
        Left = 328
        Top = 138
        Width = 53
        Height = 13
        Caption = 'Norm Level'
      end
      object Label31: TLabel
        Left = 8
        Top = 136
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object dxDBDateEdit4: TdxDBDateEdit
        Left = 120
        Top = 15
        Width = 161
        TabOrder = 0
        DataField = 'SHIFT_DATE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit5: TdxDBEdit
        Left = 120
        Top = 38
        Width = 161
        TabOrder = 1
        DataField = 'SHIFT_NUMBER'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit6: TdxDBEdit
        Left = 120
        Top = 62
        Width = 161
        TabOrder = 2
        DataField = 'PLANT_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit11: TdxDBEdit
        Left = 120
        Top = 85
        Width = 161
        TabOrder = 3
        DataField = 'WORKSPOT_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit12: TdxDBEdit
        Left = 120
        Top = 109
        Width = 161
        TabOrder = 4
        DataField = 'JOB_CODE'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit13: TdxDBEdit
        Left = 464
        Top = 134
        Width = 161
        TabOrder = 5
        DataField = 'NORMLEVEL'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit14: TdxDBEdit
        Left = 464
        Top = 110
        Width = 161
        TabOrder = 6
        DataField = 'EMPLOYEEQUANTITY'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit15: TdxDBEdit
        Left = 464
        Top = 86
        Width = 161
        TabOrder = 7
        DataField = 'EMPLOYEESECONDSNORM'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit16: TdxDBEdit
        Left = 464
        Top = 62
        Width = 161
        TabOrder = 8
        DataField = 'EMPLOYEESECONDSACTUAL'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBDateEdit5: TdxDBDateEdit
        Left = 464
        Top = 39
        Width = 161
        TabOrder = 9
        DataField = 'INTERVALENDTIME'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBDateEdit6: TdxDBDateEdit
        Left = 464
        Top = 15
        Width = 161
        TabOrder = 10
        DataField = 'INTERVALSTARTTIME'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
      object dxDBEdit26: TdxDBEdit
        Left = 120
        Top = 133
        Width = 161
        TabOrder = 11
        DataField = 'EMPLOYEE_NUMBER'
        DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
      end
    end
    object gBoxWorkspotEffPerMinute: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 163
      Align = alClient
      Caption = 'Detals'
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 19
        Width = 48
        Height = 13
        Caption = 'Shift Date'
      end
      object Label4: TLabel
        Left = 8
        Top = 42
        Width = 62
        Height = 13
        Caption = 'Shift Number'
      end
      object Label5: TLabel
        Left = 8
        Top = 65
        Width = 52
        Height = 13
        Caption = 'Plant Code'
      end
      object Label6: TLabel
        Left = 8
        Top = 89
        Width = 74
        Height = 13
        Caption = 'Workspot Code'
      end
      object Label7: TLabel
        Left = 8
        Top = 112
        Width = 45
        Height = 13
        Caption = 'Job Code'
      end
      object Label8: TLabel
        Left = 328
        Top = 19
        Width = 85
        Height = 13
        Caption = 'Interval Starttime'
      end
      object Label9: TLabel
        Left = 328
        Top = 43
        Width = 79
        Height = 13
        Caption = 'Interval Endtime'
      end
      object Label10: TLabel
        Left = 328
        Top = 67
        Width = 120
        Height = 13
        Caption = 'Workspot seconds actual'
      end
      object Label11: TLabel
        Left = 328
        Top = 91
        Width = 115
        Height = 13
        Caption = 'Workspot seconds norm'
      end
      object Label12: TLabel
        Left = 328
        Top = 115
        Width = 89
        Height = 13
        Caption = 'Workspot quantity'
      end
      object Label13: TLabel
        Left = 328
        Top = 138
        Width = 53
        Height = 13
        Caption = 'Norm Level'
      end
      object dxDBDateEdit1: TdxDBDateEdit
        Left = 120
        Top = 15
        Width = 161
        TabOrder = 0
        DataField = 'SHIFT_DATE'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit1: TdxDBEdit
        Left = 120
        Top = 38
        Width = 161
        TabOrder = 1
        DataField = 'SHIFT_NUMBER'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit2: TdxDBEdit
        Left = 120
        Top = 62
        Width = 161
        TabOrder = 2
        DataField = 'PLANT_CODE'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit3: TdxDBEdit
        Left = 120
        Top = 85
        Width = 161
        TabOrder = 3
        DataField = 'WORKSPOT_CODE'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit4: TdxDBEdit
        Left = 120
        Top = 109
        Width = 161
        TabOrder = 4
        DataField = 'JOB_CODE'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit7: TdxDBEdit
        Left = 464
        Top = 62
        Width = 161
        TabOrder = 5
        DataField = 'WORKSPOTSECONDSACTUAL'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit8: TdxDBEdit
        Left = 464
        Top = 86
        Width = 161
        TabOrder = 6
        DataField = 'WORKSPOTSECONDSNORM'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit9: TdxDBEdit
        Left = 464
        Top = 110
        Width = 161
        TabOrder = 7
        DataField = 'WORKSPOTQUANTITY'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBEdit10: TdxDBEdit
        Left = 464
        Top = 134
        Width = 161
        TabOrder = 8
        DataField = 'NORMLEVEL'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBDateEdit2: TdxDBDateEdit
        Left = 464
        Top = 15
        Width = 161
        TabOrder = 9
        DataField = 'INTERVALSTARTTIME'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
      object dxDBDateEdit3: TdxDBDateEdit
        Left = 464
        Top = 39
        Width = 161
        TabOrder = 10
        DataField = 'INTERVALENDTIME'
        DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 260
    Height = 53
    Align = alNone
    Visible = False
    inherited spltDetail: TSplitter
      Top = 49
    end
    inherited dxDetailGrid: TdxDBGrid
      Height = 48
    end
  end
  object pnlMasterPlant: TPanel [4]
    Left = 0
    Top = 26
    Width = 642
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object grpBoxPlantDate: TGroupBox
      Left = 0
      Top = 0
      Width = 642
      Height = 50
      Align = alClient
      Caption = 'Plant / Date'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 19
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label2: TLabel
        Left = 344
        Top = 19
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object dxDBExtLookupEditPlant: TdxDBExtLookupEdit
        Left = 36
        Top = 16
        Width = 241
        TabOrder = 0
        DataField = 'DESCRIPTION'
        DataSource = RealTimeEffMonitorDM.DataSourceMaster
        OnCloseUp = dxDBExtLookupEditPlantCloseUp
        DBGridLayout = dxDBGridLayoutList1Item1
      end
      object BtnRefresh: TBitBtn
        Left = 569
        Top = 12
        Width = 64
        Height = 25
        Caption = '&Refresh '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = BtnRefreshClick
      end
      object DateEditFrom: TDateTimePicker
        Left = 372
        Top = 16
        Width = 193
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 2
        OnChange = DateEditFromChange
      end
    end
  end
  object pnlPages: TPanel [5]
    Left = 0
    Top = 76
    Width = 642
    Height = 247
    Align = alClient
    TabOrder = 8
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 640
      Height = 245
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Workspot Eff Per Minute'
        object dxDBGridWorkspotEffPerMin: TdxDBGrid
          Left = 0
          Top = 0
          Width = 632
          Height = 217
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'WORKSPOTEFFPERMINUTE_ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = RealTimeEffMonitorDM.dsWorkspotEffPerMin
          OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          object dxDBGridWorkspotEffPerMinSHIFT_DATE: TdxDBGridDateColumn
            Caption = 'Shift Date'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_DATE'
          end
          object dxDBGridWorkspotEffPerMinSHIFT_NUMBER: TdxDBGridMaskColumn
            Caption = 'Shift Number'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_NUMBER'
          end
          object dxDBGridWorkspotEffPerMinPLANT_CODE: TdxDBGridMaskColumn
            Caption = 'Plant Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PLANT_CODE'
          end
          object dxDBGridWorkspotEffPerMinWORKSPOT_CODE: TdxDBGridMaskColumn
            Caption = 'Workspot Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_CODE'
          end
          object dxDBGridWorkspotEffPerMinJOB_CODE: TdxDBGridMaskColumn
            Caption = 'Job Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'JOB_CODE'
          end
          object dxDBGridWorkspotEffPerMinINTERVALSTARTTIME: TdxDBGridDateColumn
            Caption = 'Interval Starttime'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'INTERVALSTARTTIME'
          end
          object dxDBGridWorkspotEffPerMinINTERVALENDTIME: TdxDBGridDateColumn
            Caption = 'Interval Endtime'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'INTERVALENDTIME'
          end
          object dxDBGridWorkspotEffPerMinWORKSPOTSECONDSACTUAL: TdxDBGridMaskColumn
            Caption = 'Workspot Seconds Actual'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOTSECONDSACTUAL'
          end
          object dxDBGridWorkspotEffPerMinWORKSPOTSECONDSNORM: TdxDBGridMaskColumn
            Caption = 'Workspot Seconds Norm'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOTSECONDSNORM'
          end
          object dxDBGridWorkspotEffPerMinWORKSPOTQUANTITY: TdxDBGridMaskColumn
            Caption = 'Workspot Quantity'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOTQUANTITY'
          end
          object dxDBGridWorkspotEffPerMinNORMLEVEL: TdxDBGridMaskColumn
            Caption = 'Norm Level'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'NORMLEVEL'
          end
          object dxDBGridWorkspotEffPerMinSTART_TIMESTAMP: TdxDBGridDateColumn
            Caption = 'Start Timestamp'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'START_TIMESTAMP'
          end
          object dxDBGridWorkspotEffPerMinEND_TIMESTAMP: TdxDBGridDateColumn
            Caption = 'End Timestamp'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'END_TIMESTAMP'
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Employee Eff Per Minute'
        ImageIndex = 1
        object dxDBGridEmployeeEffPerMin: TdxDBGrid
          Left = 0
          Top = 0
          Width = 632
          Height = 217
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'EMPLOYEEEFFPERMINUTE_ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerMin
          OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          object dxDBGridEmployeeEffPerMinSHIFT_DATE: TdxDBGridDateColumn
            Caption = 'Shift Date'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_DATE'
          end
          object dxDBGridEmployeeEffPerMinSHIFT_NUMBER: TdxDBGridMaskColumn
            Caption = 'Shift Number'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_NUMBER'
          end
          object dxDBGridEmployeeEffPerMinPLANT_CODE: TdxDBGridMaskColumn
            Caption = 'Plant Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PLANT_CODE'
          end
          object dxDBGridEmployeeEffPerMinWORKSPOT_CODE: TdxDBGridMaskColumn
            Caption = 'Workspot Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_CODE'
          end
          object dxDBGridEmployeeEffPerMinJOB_CODE: TdxDBGridMaskColumn
            Caption = 'Job Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'JOB_CODE'
          end
          object dxDBGridEmployeeEffPerMinColumnEMPLOYEE_NUMBER: TdxDBGridColumn
            Caption = 'Employee Number'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEE_NUMBER'
          end
          object dxDBGridEmployeeEffPerMinINTERVALSTARTTIME: TdxDBGridDateColumn
            Caption = 'Interval Starttime'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'INTERVALSTARTTIME'
          end
          object dxDBGridEmployeeEffPerMinINTERVALENDTIME: TdxDBGridDateColumn
            Caption = 'Interval Endtime'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'INTERVALENDTIME'
          end
          object dxDBGridEmployeeEffPerMinEMPLOYEESECONDSACTUAL: TdxDBGridMaskColumn
            Caption = 'Employee Seconds Actual'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEESECONDSACTUAL'
          end
          object dxDBGridEmployeeEffPerMinEMPLOYEESECONDSNORM: TdxDBGridMaskColumn
            Caption = 'Employee Seconds Norm'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEESECONDSNORM'
          end
          object dxDBGridEmployeeEffPerMinEMPLOYEEQUANTITY: TdxDBGridMaskColumn
            Caption = 'Employee Quantity'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEEQUANTITY'
          end
          object dxDBGridEmployeeEffPerMinNORMLEVEL: TdxDBGridMaskColumn
            Caption = 'Norm Level'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'NORMLEVEL'
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Employee Eff Per Shift'
        ImageIndex = 2
        object dxDBGridEmployeeEffPerShift: TdxDBGrid
          Left = 0
          Top = 0
          Width = 632
          Height = 217
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'EMPLOYEEEFFPERSHIFT_ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = RealTimeEffMonitorDM.dsEmployeeEffPerShift
          OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          object dxDBGridEmployeeEffPerShiftSHIFT_DATE: TdxDBGridDateColumn
            Caption = 'Shift Date'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_DATE'
          end
          object dxDBGridEmployeeEffPerShiftSHIFT_NUMBER: TdxDBGridMaskColumn
            Caption = 'Shift Number'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'SHIFT_NUMBER'
          end
          object dxDBGridEmployeeEffPerShiftPLANT_CODE: TdxDBGridMaskColumn
            Caption = 'Plant Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PLANT_CODE'
          end
          object dxDBGridEmployeeEffPerShiftWORKSPOT_CODE: TdxDBGridMaskColumn
            Caption = 'Workspot Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_CODE'
          end
          object dxDBGridEmployeeEffPerShiftJOB_CODE: TdxDBGridMaskColumn
            Caption = 'Job Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'JOB_CODE'
          end
          object dxDBGridEmployeeEffPerShiftEMPLOYEE_NUMBER: TdxDBGridMaskColumn
            Caption = 'Employee Number'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEE_NUMBER'
          end
          object dxDBGridEmployeeEffPerShiftEMPLOYEESECONDSACTUAL: TdxDBGridMaskColumn
            Caption = 'Employee Seconds Actual'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEESECONDSACTUAL'
          end
          object dxDBGridEmployeeEffPerShiftEMPLOYEESECONDSNORM: TdxDBGridMaskColumn
            Caption = 'Employee Seconds Norm'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEESECONDSNORM'
          end
          object dxDBGridEmployeeEffPerShiftEMPLOYEEQUANTITY: TdxDBGridMaskColumn
            Caption = 'Employee Quantity'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'EMPLOYEEQUANTITY'
          end
          object dxDBGridEmployeeEffPerShiftNORMLEVEL: TdxDBGridMaskColumn
            Caption = 'Norm Level'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'NORMLEVEL'
          end
        end
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 489
    Top = 65
    object dxDBGridLayoutList1Item1: TdxDBGridLayout
      Data = {
        5F010000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0605506C616E7400000D44656661756C744C61796F75
        74091348656164657250616E656C526F77436F756E740201084B65794669656C
        64060A504C414E545F434F44450D53756D6D61727947726F7570730E00105375
        6D6D617279536570617261746F7206022C200A44617461536F75726365072552
        65616C54696D654566664D6F6E69746F72444D2E44617461536F757263654D61
        73746572000F546478444247726964436F6C756D6E07436F6C756D6E31074361
        7074696F6E0604434F44450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A504C414E545F434F444500000F5464784442
        47726964436F6C756D6E07436F6C756D6E320942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060B4445534352495054494F4E
        000000}
    end
  end
end
