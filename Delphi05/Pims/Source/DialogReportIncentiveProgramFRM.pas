(*
  MRA:21-OCT-2013 20014715
  - New Report Incentive Program (WMU)
*)
unit DialogReportIncentiveProgramFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls, dxEdLib, SystemDMT;

type
  TDialogReportIncentiveProgramF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    LblMonth: TLabel;
    LblYear: TLabel;
    dxSpinEditMonth: TdxSpinEdit;
    dxSpinEditYear: TdxSpinEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportIncentiveProgramF: TDialogReportIncentiveProgramF;

function DialogReportIncentiveProgramForm: TDialogReportIncentiveProgramF;

implementation

{$R *.DFM}

uses
  ReportIncentiveProgramDMT, ReportIncentiveProgramQRPT,
  ListProcsFRM, UPimsMessageRes;

var
  DialogReportIncentiveProgramF_HND: TDialogReportIncentiveProgramF;

function DialogReportIncentiveProgramForm: TDialogReportIncentiveProgramF;
begin
  if (DialogReportIncentiveProgramF_HND = nil) then
  begin
    DialogReportIncentiveProgramF_HND := TDialogReportIncentiveProgramF.Create(Application);
    with DialogReportIncentiveProgramF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportIncentiveProgramF_HND;
end;

procedure TDialogReportIncentiveProgramF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportIncentiveProgramF_HND <> nil) then
  begin
    DialogReportIncentiveProgramF_HND := nil;
  end;
end;

procedure TDialogReportIncentiveProgramF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportIncentiveProgramF.FormShow(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  InitDialog(True, False, False, True, False, False, False);
  inherited;
  DecodeDate(Now, Year, Month, Day);
  dxSpinEditMonth.IntValue := Month;
  dxSpinEditYear.IntValue := Year;
end;

procedure TDialogReportIncentiveProgramF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportIncentiveProgramF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportIncentiveProgramF.btnOkClick(Sender: TObject);
var
  DateFrom, DateTo: TDateTime;
begin
  inherited;
  DateFrom := EncodeDate(dxSpinEditYear.IntValue, dxSpinEditMonth.IntValue, 1);
  DateTo := EncodeDate(dxSpinEditYear.IntValue, dxSpinEditMonth.IntValue,
    ListProcsF.LastMounthDay(dxSpinEditYear.IntValue,
    dxSpinEditMonth.IntValue));
  if ReportIncentiveProgramQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Trunc(DateFrom),
      Trunc(DateTo),
      dxSpinEditMonth.IntValue,
      dxSpinEditYear.IntValue,
      CBoxShowSelection.Checked,
      CheckBoxExport.Checked)
  then
    ReportIncentiveProgramQR.ProcessRecords;
end;

procedure TDialogReportIncentiveProgramF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportIncentiveProgramDM := CreateReportDM(TReportIncentiveProgramDM);
  ReportIncentiveProgramQR := CreateReportQR(TReportIncentiveProgramQR);
end;

end.
