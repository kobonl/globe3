(*
  Changes
  MR: 15-04-2004 A 'ClientDataSet' (cdsEmpl) is used for first select-statement,
                 to improve speed when using Pims stand-alone. It appeared to
                 be very slow stand-alone, client-server was not slow.
  MR:08-10-2004 Use datefrom-dateto instead of year, weekfrom and weekto.
                Order 550344.
  MRA:08-08-2008 REV008.
    - Added Order By (Oracle 10).
    - Use Employee's plant during select-statement, to prevent absence is
      not shown that is booked on another plant.
  MRA:29-APR-2011 RV092.1. RV092.1. SC-50015974.
  - This report gives wrong values for certain Indirect/Direct hours.
  - Reason: Determination of Direct/Indirect was wrong.
  MRA:5-JUN-2012 20656. Todo.
  - Total percentage on plant-level of 'Ratio Illness total hours indirect'
    is different from details on employee-level.
    Reason: It based this total per plant and grand total on
           'illness indirect' and 'total hours indirect' but total per
           employee is based on 'illness indirect' and 'total hours indirect'
           plus 'total hours direct'.
           To solve this the plant/grand total for calculation of this
           percentageis stored in two different variables.
*)

unit ReportRatioIllnessHrsQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FDepartmentFrom, FDepartmentTo: String;
    FShowSelection: Boolean;
    FExportToFile: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      DepartmentFrom, DepartmentTo: String;
      ShowSelection, ExportToFile: Boolean);
  end;

  TReportRatioIllnessHrsQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLabel5: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRGroupHDPlant: TQRGroup;
    ChildBandPlant: TQRChildBand;
    QRLabel21: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel22: TQRLabel;
    QRGroupHDEmpl: TQRGroup;
    QRBandFooterEmpl: TQRBand;
    QRDBTextEmpl: TQRDBText;
    QRDBTextEmplDesc: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRLabel4: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBandSummary: TQRBand;
    QRLabel28: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabelIllInd: TQRLabel;
    QRLabelIllDir: TQRLabel;
    QRLabelTotInd: TQRLabel;
    QRLabelTotDir: TQRLabel;
    QRLabelTot: TQRLabel;
    QRLabelContrInd: TQRLabel;
    QRLabelContrDir: TQRLabel;
    QRLabelRInd: TQRLabel;
    QRLabelRDir: TQRLabel;
    QRLabelRContrInd: TQRLabel;
    QRLabelRContrDir: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel25: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabelIllIndP: TQRLabel;
    QRLabelIllDirP: TQRLabel;
    QRLabelTotIndP: TQRLabel;
    QRLabelTotDirP: TQRLabel;
    QRLabelTotP: TQRLabel;
    QRLabelContrIndP: TQRLabel;
    QRLabelContrDirP: TQRLabel;
    QRLabelRIndP: TQRLabel;
    QRLabelRDirP: TQRLabel;
    QRLabelRContrIndP: TQRLabel;
    QRLabelRContrDirP: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabelIllIndT: TQRLabel;
    QRLabelIllDirT: TQRLabel;
    QRLabelTotIndT: TQRLabel;
    QRLabelTotDirT: TQRLabel;
    QRLabelTotT: TQRLabel;
    QRLabelContrIndT: TQRLabel;
    QRLabelContrDirT: TQRLabel;
    QRLabelRIndT: TQRLabel;
    QRLabelRDirT: TQRLabel;
    QRLabelRContrIndT: TQRLabel;
    QRLabelRContrDirT: TQRLabel;
    QRLabelRIllHrsDir: TQRLabel;
    QRLabelRIllHrsContr: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel27: TQRLabel;
    QRLabelFromDepartment: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDepartmentFrom: TQRLabel;
    QRLabelToDepartment: TQRLabel;
    QRLabelDepartmentTo: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextEmplDescPrint(sender: TObject; var Value: String);
    procedure FormDestroy(Sender: TObject);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
    FWholeWeeks: Boolean;
    SelectionIllness: String;
    SelectionProduction: String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FListABS_Ill, FListABS_Tot, FListProdMin: TStringList;
    FMinDate, FMaxDate: TDateTime;
    FDirIll_Hrs, FIndIll_Hrs, FDirTot_Hrs, FIndTot_Hrs,
    FDirIllP_Hrs, FIndIllP_Hrs, FDirTotP_Hrs, FIndTotP_Hrs: Integer;
    FDirIllT_Hrs, FIndIllT_Hrs, FDirTotT_Hrs, FIndTotT_Hrs: Integer;
    FIndContractT_Hrs, FDirContractT_Hrs, FIndContractP_Hrs, FIndContract_Hrs,
    FDirContractP_Hrs, FDirContract_Hrs: Double;
    FTotIndDirPlant_Hrs, FTotIndDirGrandTotal_Hrs: Integer;
    function QRSendReportParameters(const PlantFrom, PlantTo, DepartmentFrom,
      DepartmentTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowSelection, ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    function GetAbsenceIll_Minutes(Absence_Reason_Code: String;
      QuerySel: TQuery): Boolean;
    function GetProdMin(QuerySel: TQuery): Boolean;
    function SetQueryParameters(SelectStr: String;
      QuerySel: TQuery; SetIntervDate, GetRecordCount: Boolean): Boolean;
    procedure GetContractMinPerEmployee;
    procedure FillLabelsPerEmpl;
    procedure FillLabelsPerPlant;
    function DecodeHrs(Hrs: Integer): String;
    function DecodeHrsDouble(Hrs: Double): String;
  end;

var
  ReportRatioIllnessHrsQR: TReportRatioIllnessHrsQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ListProcsFRM, ReportRatioIllnessHrsDMT, UPimsConst,
  UGlobalFunctions;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  DepartmentFrom, DepartmentTo: String;
  ShowSelection, ExportToFile: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
end;

function TReportRatioIllnessHrsQR.QRSendReportParameters(const PlantFrom,
  PlantTo, DepartmentFrom, DepartmentTo, EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowSelection, ExportTofile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      DepartmentFrom, DepartmentTo, ShowSelection,
      ExportToFile);
  end;
  // MR:15-04-2004 ClientDataSet is used here, instead of QueryEmpl.
//  SetDataSetQueryReport(ReportRatioIllnessHrsDM.qryEmpl);
  if qrptBase.DataSet = nil then
    qrptBase.DataSet := ReportRatioIllnessHrsDM.cdsEmpl;
end;

function TReportRatioIllnessHrsQR.SetQueryParameters(SelectStr: String;
  QuerySel: TQuery; SetIntervDate, GetRecordCount: Boolean): Boolean;
begin
  QuerySel.Active := False;
  QuerySel.UniDirectional := False;
  QuerySel.SQL.Clear;
  QuerySel.SQL.Add(UpperCase(SelectStr));
// QuerySel.SQL.SaveToFile('c:\temp\report_ratio_illess_main.sql');
  if SetIntervDate then
  begin
    QuerySel.ParamByName('FSTARTDATE').Value := FMinDate;
    QuerySel.ParamByName('FENDDATE').Value :=  FMaxDate;
  end;
  if not QuerySel.Prepared then
    QuerySel.Prepare;
  QuerySel.Active := True;
  Result := True;
  if GetRecordCount then
    if (QuerySel.RecordCount = 0) then
      Result := False
    else
      Result := True;
end;

function TReportRatioIllnessHrsQR.GetAbsenceIll_Minutes(
  Absence_Reason_Code: String; QuerySel: TQuery): Boolean;
var
  SelectStr, WhereSelectStr: String;
begin
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
    WhereSelectStr :=
      ' E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + ' AND ' +
      ' E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + ' AND '
  else
    WhereSelectStr :=
      ' E.PLANT_CODE = ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + ' AND ' +
      ' E.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDepartmentFrom) + '''' + ' AND ' +
      ' E.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDepartmentTo) + '''' + ' AND ' +
      ' E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + ' AND ' +
      ' E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + ' AND ';

  SelectStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE, A.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS ABSMIN ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON R ON ' + NL +
    '    A.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    D.PLANT_CODE = E.PLANT_CODE AND ' + NL +
    '    D.DEPARTMENT_CODE = E.DEPARTMENT_CODE ' + NL +
    'WHERE ' + NL +
    WhereSelectStr + NL +
    '  A.ABSENCEHOUR_DATE >= :FSTARTDATE AND ' + NL +
    '  A.ABSENCEHOUR_DATE <= :FENDDATE ' + NL;
  if Absence_Reason_Code <> '' then
    SelectStr :=
      SelectStr + ' AND R.ABSENCETYPE_CODE IN ' + Absence_Reason_Code;
  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + NL +
    '  A.PLANT_CODE, A.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN ' + NL;
  SelectionIllness := SelectStr;
  SelectStr := SelectStr + ' ' +
    'ORDER BY ' + NL +
    '  A.PLANT_CODE, A.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN ';
  Result := SetQueryParameters(SelectStr, QuerySel, True, False);
// QuerySel.SQL.SaveToFile('c:\temp\report_ratio_illness1.sql');
  if Absence_Reason_Code <> '' then
  begin
    FListABS_Ill.Clear;
    QuerySel.First;
    while not QuerySel.Eof do
    begin
      // RV092.1. Added DIRECT_HOUR_YN in key.
      FListABS_Ill.Add(CHR_SEP + QuerySel.FieldByName('PLANT_CODE').AsString +
        CHR_SEP + QuerySel.FieldByName('EMPLOYEE_NUMBER').AsString +
        CHR_SEP + QuerySel.FieldByName('DIRECT_HOUR_YN').AsString);
      FListABS_Ill.Add(QuerySel.FieldByName('DIRECT_HOUR_YN').AsString +
        CHR_SEP + QuerySel.FieldByName('ABSMIN').AsString);
      QuerySel.Next
    end;
  end
  else
  begin
    FListABS_Tot.Clear;
    QuerySel.First;
    while not QuerySel.Eof do
    begin
      // RV092.1. Added DIRECT_HOUR_YN in key.
      FListABS_Tot.Add(CHR_SEP + QuerySel.FieldByName('PLANT_CODE').AsString +
        CHR_SEP + QuerySel.FieldByName('EMPLOYEE_NUMBER').AsString +
        CHR_SEP + QuerySel.FieldByName('DIRECT_HOUR_YN').AsString);
      FListABS_Tot.Add(QuerySel.FieldByName('DIRECT_HOUR_YN').AsString +
        CHR_SEP + QuerySel.FieldByName('ABSMIN').AsString);
      QuerySel.Next;
    end;
  end;
end;

function TReportRatioIllnessHrsQR.GetProdMin(QuerySel: TQuery): Boolean;
var
  SelectStr, WhereSelectStr: String;
begin
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
    WhereSelectStr :=
      '  E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + ''' AND ' +
      '  E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''' AND '
  else
    WhereSelectStr :=
      '  E.PLANT_CODE = ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + ' AND ' +
      '  E.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDepartmentFrom) + ''' AND ' +
      '  E.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDepartmentTo) + ''' AND ' +
      '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + ' AND  ' +
      '  P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + ' AND  ';

  SelectStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, P.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN, ' + NL +
    '  SUM(P.PRODUCTION_MINUTE) AS PRODMIN ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    D.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    D.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL +
    'WHERE ' + NL +
    WhereSelectStr + NL +
    '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE <= :FENDDATE ' + NL +
    'GROUP BY ' + NL +
    '  P.PLANT_CODE, P.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN ' + NL;
  SelectionProduction := SelectStr;
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  P.PLANT_CODE, P.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN ';
  Result := SetQueryParameters(SelectStr, QuerySel, True, False);
// QuerySel.SQL.SaveToFile('c:\temp\report_ratio_illness2.sql');
  FListProdMin.Clear;
  QuerySel.First;
  while not QuerySel.Eof do
  begin
    // RV092.1. Added DIRECT_HOUR_YN in key.
    FListProdMin.Add(CHR_SEP + QuerySel.FieldByName('PLANT_CODE').AsString +
      CHR_SEP + QuerySel.FieldByNAME('EMPLOYEE_NUMBER').AsString +
      CHR_SEP + QuerySel.FieldByName('DIRECT_HOUR_YN').AsString);
    FListProdMin.Add(QuerySel.FieldByName('DIRECT_HOUR_YN').AsString +
      CHR_SEP + QuerySel.FieldByName('PRODMIN').AsString);
    QuerySel.Next;
  end;
end;

function TReportRatioIllnessHrsQR.ExistsRecords: Boolean;
var
  SelectStr{, WhereSelectStr}: String;
  WeekDayMin, WeekDayMax: Word;
begin
  Screen.Cursor := crHourGlass;

  with ReportRatioIllnessHrsDM do
  begin
    FMinDate := QRParameters.FDateFrom;
    FMaxDate := QRParameters.FDateTo;
    // MR:15-12-2004 Check for whole weeks.
    // Because FDateFrom and FDateTo can span more than 1 week,
    // look if the first and last day start/end with a whole week.
    WeekDayMin := ListProcsF.PimsDayOfWeek(QRParameters.FDateFrom);
    WeekDayMax := ListProcsF.PimsDayOfWeek(QRParameters.FDateTo);
    FWholeWeeks := ((WeekDayMin = 1) and (WeekDayMax = 7));

(*
    if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
      WhereSelectStr :=
        '  E.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + ' AND ' +
        '  E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + ' '
    else
      WhereSelectStr :=
        '  E.PLANT_CODE = ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + ' AND ' +
        '  E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + ' AND ' +
        '  E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + ' AND ' +
        '  E.DEPARTMENT_CODE >= ''' +
        DoubleQuote(QRParameters.FDepartmentFrom) + '''' + ' AND ' +
        '  E.DEPARTMENT_CODE <= ''' +
        DoubleQuote(QRParameters.FDepartmentTo) + '''' + ' ';

    SelectStr :=
      'SELECT ' + NL +
      '  E.PLANT_CODE, P.DESCRIPTION, ' + NL +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
      '  D.DIRECT_HOUR_YN ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, PLANT P, DEPARTMENT D ' + NL +
      'WHERE ' + NL +
      '  P.PLANT_CODE = E.PLANT_CODE AND ' + NL +
      '  D.PLANT_CODE = E.PLANT_CODE AND ' + NL +
      '  D.DEPARTMENT_CODE = E.DEPARTMENT_CODE AND ' + NL +
      WhereSelectStr + NL +
      'ORDER BY ' + NL +
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER ' + NL;
*)

    GetAbsenceIll_Minutes('('''+ ILLNESS + ''')', QueryAbsence);
    GetAbsenceIll_Minutes('', QueryAbsence);
    GetProdMin(QueryProdMin);

    // MRA:08-08-2008 RV008.
    // Following combines the 2 alrady made selections (illness + production)
    // to get the correct plants... out of it.
    // Because this can be independent of the plant of the employee, if
    // he had worked in other plants or had illness in other plants.
    SelectStr :=
      'SELECT ' + NL +
      '  B.PLANT_CODE, PL.DESCRIPTION, ' + NL +
      '  B.EMPLOYEE_NUMBER, EMP.DESCRIPTION, B.DIRECT_HOUR_YN ' + NL +
      'FROM ' + NL +
      ' ( ' + NL +
      SelectionIllness +
      ' UNION ' +
      SelectionProduction + ' ' +
      'ORDER BY ' + NL +
      '  1, 2, 3 ' + NL +
      ' ) B ' + NL +
      'INNER JOIN PLANT PL ON ' + NL +
      '  B.PLANT_CODE = PL.PLANT_CODE ' + NL +
      'INNER JOIN EMPLOYEE EMP ON ' + NL +
      '  B.EMPLOYEE_NUMBER = EMP.EMPLOYEE_NUMBER ' + NL +
      'GROUP BY ' + NL +
      '  B.PLANT_CODE, PL.DESCRIPTION, ' + NL +
      '  B.EMPLOYEE_NUMBER, EMP.DESCRIPTION, B.DIRECT_HOUR_YN ' + NL +
      'ORDER BY ' + NL +
      '  1, 3 ';

//    Result := SetQueryParameters(SelectStr, QueryEmpl, False, True);
    Result := SetQueryParameters(SelectStr, QueryEmpl, True, True);

// QueryEmpl.SQL.SaveToFile('c:\temp\report_ratio_illness_main.sql');

    // MR:15-04-2004 ClientDataSet is used here, instead of QueryEmpl.
    if Result then
      cdsEmpl.Refresh;

    if not Result then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    qryEmplContract.Close;
    qryEmplContract.Open;

    FDirIll_Hrs := 0;
    FIndIll_Hrs := 0;
    FDirTot_Hrs := 0;
    FIndTot_Hrs := 0;
    FDirContract_Hrs := 0;
    FIndContract_Hrs := 0;
  {check if report is empty}
    Screen.Cursor := crDefault;
  end;
end;

procedure TReportRatioIllnessHrsQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelDepartmentFrom.Caption := '*';
  QRLabelDepartmentTo.Caption := '*';
  if  (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    QRLabelDepartmentFrom.Caption := QRParameters.FDepartmentFrom;
    QRLabelDepartmentTo.Caption := QRParameters.FDepartmentTo;
    QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  end
  else
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end;
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);
end;

procedure TReportRatioIllnessHrsQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportRatioIllnessHrsQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  FListABS_Ill := TStringList.Create;
  FListABS_Tot := TStringList.Create;
  FListProdMin := TStringList.Create;
  // MR:22-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportRatioIllnessHrsQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel19.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel16.Caption + ' ' + QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabelFromDepartment.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDepartmentFrom.Caption + ' ' +
        QRLabelToDepartment.Caption + ' ' + QRLabelDepartmentTo.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLblEmployeeFrom.Caption + ' ' +
        QRLabel27.Caption + ' ' + QRLblEmployeeTo.Caption);
      ExportClass.AddText(QRLabel1.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLblDateFrom.Caption + ' ' +
        QRLabel5.Caption + ' ' + QRLblDateTo.Caption);
    end;
  end;
  FDirIll_Hrs := 0;
  FIndIll_Hrs := 0;
  FDirTot_Hrs := 0;
  FIndTot_Hrs := 0;
  FDirContract_Hrs := 0;
  FIndContract_Hrs := 0;
  FIndIllT_Hrs := 0;
  FDirIllT_Hrs := 0;
  FIndTotT_Hrs := 0;
  FDirTotT_Hrs := 0;
  FIndContractT_Hrs := 0;
  FDirContractT_Hrs := 0;
  FTotIndDirGrandTotal_Hrs := 0;
end;

procedure TReportRatioIllnessHrsQR.GetContractMinPerEmployee;
var
  Empl, WeekNr: Integer;
  StartYear, StartWeek, EndYear, EndWeek: Word;
  LastDateOnEndWeek, FirstDateOnStartWeek: TDateTime;
  YearFrom, YearTo, WeekFrom, WeekTo: Word;
  procedure CalculateAmountsContract(Amount: Double);
  begin
    if (ReportRatioIllnessHrsDM.cdsEmpl.
        FieldByName('DIRECT_HOUR_YN').AsString = 'Y') then
      FDirContract_Hrs := Amount + FDirContract_Hrs
    else
      FIndContract_Hrs := Amount + FIndContract_Hrs;
  end;
begin
  // MR:16-12-2004 If datefrom-dateto are not whole weeks,
  // then don't determine contract-minutes.
  if not FWholeWeeks then
    Exit;
  with ReportRatioIllnessHrsDM do
  begin
    Empl := cdsEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    ListProcsF.WeekUitDat(
      QRParameters.FDateFrom,
      YearFrom, WeekFrom);
    ListProcsF.WeekUitDat(
      QRParameters.FDateTo,
      YearTo, WeekTo);
    cdsEmplContract.Filtered := False;
    cdsEmplContract.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(Empl);
    cdsEmplContract.Filtered := True;
//    TableContractEmpl.FindNearest([Empl, FMinDate]);
    while not cdsEmplContract.Eof do
    begin
//CAR 12.12.2002
      if (cdsEmplContract.FieldByName('STARTDATE').AsDateTime > FMaxDate) then
        Exit;
// END CAR
      ListProcsF.WeekUitDat(
        cdsEmplContract.FieldByName('STARTDATE').AsDateTime,
        StartYear, StartWeek);
      ListProcsF.WeekUitDat(
        cdsEmplContract.FieldByName('ENDDATE').AsDateTime,
        EndYear, EndWeek);

      if (StartYear < YearFrom) or
        ((StartYear = YearFrom) and
         (StartWeek < WeekFrom)) then
      begin
        StartWeek := WeekFrom;
        StartYear := YearFrom;
      end;
      if (EndYear < YearFrom) or
         ((EndYear = YearFrom) and
          (EndWeek < WeekFrom)) then
// not valid contract
      else
      begin
        if (EndYear > YearTo) or
          ((EndYear = YearTo) and
           (EndWeek > WeekTo)) then
        begin
          EndYear := YearTo;
          EndWeek := WeekTo
        end;
        LastDateOnEndWeek :=
          ListProcsF.DateFromWeek(EndYear, EndWeek, 7);
        FirstDateOnStartWeek :=
          ListProcsF.DateFromWeek(StartYear, StartWeek, 1);
        if (StartYear = EndYear) then
          WeekNr := (EndWeek - StartWeek)
        else
          WeekNr :=
            (ListProcsF.WeeksInYear(StartYear) - StartWeek + 1 + EndWeek);
        if (cdsEmplContract.FieldByName('STARTDATE').AsDateTime >
            FirstDateOnStartWeek) then
          WeekNr := WeekNr - 1;
        if (cdsEmplContract.FieldByName('ENDDATE').AsDateTime >=
            LastDateOnEndWeek) then
          WeekNr := WeekNr + 1;
        // MR:22-11-2004 COUNTRACT_HOUR_PER_WEEK is a Double!
        CalculateAmountsContract(
          cdsEmplContract.FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat *
          WeekNr);
      end;
      cdsEmplContract.Next;
    end;
  end;
end;

procedure TReportRatioIllnessHrsQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  IndexList, p: Integer;
  StrTmp, DirectYN : String;
  ABSMin: Integer;
  CurrentKey: String;
begin
  inherited;

  // RV092.1. Use a different key to search for values:
  //          added 'DIRECT_HOUR_YN'.
  CurrentKey := CHR_SEP +
    ReportRatioIllnessHrsDM.cdsEmpl.FieldByName('PLANT_CODE').AsString +
    CHR_SEP +
    ReportRatioIllnessHrsDM.cdsEmpl.FieldByName('EMPLOYEE_NUMBER').AsString +
    CHR_SEP +
    ReportRatioIllnessHrsDM.cdsEmpl.FieldByName('DIRECT_HOUR_YN').AsString;

  IndexList := FListABS_Ill.IndexOf(CurrentKey);
  // CAR 9-7-2003
  if (IndexList >= 0) and ((IndexList + 1) <= (FListABS_Ill.Count -1))  then
  begin
    StrTmp := FListABS_Ill.Strings[IndexList + 1];
    p := Pos(CHR_SEP, StrTmp);
    DirectYN := Copy(StrTmp, 0, p-1);
    ABSMin := StrToInt(Copy(StrTmp, p + 1, Length(strtmp) + 1));

    if (DirectYN = 'Y') then
      FDirIll_Hrs := FDirIll_Hrs + ABSMin;
    if (DirectYN = 'N') then
      FIndIll_Hrs := FIndIll_Hrs + ABSMin;
  end;
  IndexList := FListABS_Tot.IndexOf(CurrentKey);
// CAR 9-7-2003 ADD EXTRA CHECK FOR INDEXLIST
  if (IndexList >= 0 )  and ((IndexList + 1) <= (FListABS_Tot.Count -1)) then
  begin
    StrTmp := FListABS_Tot.Strings[IndexList + 1];
    p := Pos(CHR_SEP, StrTmp);
    DirectYN := Copy(StrTmp, 0, p-1);
    ABSMin := StrToInt(Copy(StrTmp, p + 1, Length(strtmp) + 1));

    if (DirectYN = 'Y') then
      FDirTot_Hrs := FDirTot_Hrs + ABSMin;
    if (DirectYN = 'N') then
      FIndTot_Hrs := FIndTot_Hrs + ABSMin;
  end;

  IndexList := FListProdMin.IndexOf(CurrentKey);
  if (IndexList >= 0) and ((IndexList + 1) <= (FListProdMin.Count -1)) then
  begin
    StrTmp := FListProdMin.Strings[IndexList + 1];
    p := Pos(CHR_SEP, StrTmp);
    DirectYN := Copy(StrTmp, 0, p-1);
    ABSMin := StrToInt(Copy(StrTmp, p + 1, Length(strtmp) + 1));

    if (DirectYN = 'Y') then
      FDirTot_Hrs := FDirTot_Hrs + ABSMin;
    if (DirectYN = 'N') then
      FIndTot_Hrs := FIndTot_Hrs + ABSMin;
  end;
end;

procedure TReportRatioIllnessHrsQR.FillLabelsPerEmpl;
var
  FloatVar: Double;
begin
  QRLabelIllInd.Caption := DecodeHrsMin(FIndIll_Hrs);
  QRLabelIllDir.Caption := DecodeHrsMin(FDirIll_Hrs);
  QRLabelTotDir.Caption := DecodeHrsMin(FDirTot_Hrs);
  QRLabelTotInd.Caption := DecodeHrsMin(FIndTot_Hrs);
  QRLabelTot.Caption :=  DecodeHrsMin(FDirTot_Hrs + FIndTot_Hrs);
  QRLabelContrInd.Caption := DecodeHrsDouble(FIndContract_Hrs);
  QRLabelContrDir.Caption := DecodeHrsDouble(FDirContract_Hrs);
  if (FDirTot_Hrs + FIndTot_Hrs) <> 0 then
  begin
    FloatVar := FIndIll_Hrs * 100/(FDirTot_Hrs + FIndTot_Hrs);
    QRLabelRInd.Caption := Format('%3.1f', [FloatVar]);
    FloatVar := FDirIll_Hrs * 100/(FDirTot_Hrs + FIndTot_Hrs);
    QRLabelRDir.Caption := Format('%3.1f', [FloatVar]);
    // 20656.
    if FIndIll_Hrs <> 0 then
    begin
      FTotIndDirPlant_Hrs := FTotIndDirPlant_Hrs + (FDirTot_Hrs + FIndTot_Hrs);
      FTotIndDirGrandTotal_Hrs := FTotIndDirGrandTotal_Hrs +
        (FDirTot_Hrs + FIndTot_Hrs);
    end;
  end
  else
  begin
    QRLabelRInd.Caption := '0.0';
    QRLabelRDir.Caption := '0.0';
  end;
  QRLabelRInd.Caption  := QRLabelRInd.Caption  + '%';
  QRLabelRDir.Caption := QRLabelRDir.Caption + '%';

  if FDirContract_Hrs <> 0 then
  begin
    FloatVar := FDirIll_Hrs * 100/(FDirContract_Hrs*60);
    QRLabelRContrDir.Caption := Format('%3.1f', [FloatVar]) + '%';
  end
  else
    QRLabelRContrDir.Caption := '0.0%';

  if FIndContract_Hrs <> 0 then
  begin
    FloatVar := FIndIll_Hrs * 100/(FIndContract_Hrs*60);
    QRLabelRContrInd.Caption := Format('%3.1f', [FloatVar]) + '%'
  end
  else
    QRLabelRContrInd.Caption := '0.0%';
end;

function TReportRatioIllnessHrsQR.DecodeHrs(Hrs: Integer): String;
begin
  Result := '';
  if Hrs < 10 then
    Result := '0' + IntToStr(Hrs) + ':00'
  else
    Result := IntToStr(Hrs) + ':00';
end;

function TReportRatioIllnessHrsQR.DecodeHrsDouble(Hrs: Double): String;
begin
  Result := FillZero(Trunc(Hrs)) + ':' +
    FillZero(Round(60 * (Hrs - Trunc(Hrs))));
end;

procedure TReportRatioIllnessHrsQR.FillLabelsPerPlant;
var
  FloatVar: Double;
begin

  QRLabelIllIndP.Caption := DecodeHrsMin(FIndIllP_Hrs);
  QRLabelIllDirP.Caption := DecodeHrsMin(FDirIllP_Hrs);
  QRLabelTotDirP.Caption := DecodeHrsMin(FDirTotP_Hrs);
  QRLabelTotIndP.Caption := DecodeHrsMin(FIndTotP_Hrs);
  QRLabelTotP.Caption := DecodeHrsMin(FDirTotP_Hrs + FIndTotP_Hrs);
// only hours contains
  QRLabelContrIndP.Caption := DecodeHrsDouble(FIndContractP_Hrs);
  QRLabelContrDirP.Caption := DecodeHrsDouble(FDirContractP_Hrs);
  // 20656.
  if (FTotIndDirPlant_Hrs <> 0) then
  begin
    FloatVar := FIndIllP_Hrs * 100 / (FTotIndDirPlant_Hrs);
    QRLabelRIndP.Caption := Format('%3.1f', [FloatVar]) + '%'
  end
  else
    QRLabelRIndP.Caption := '0.0%';
{
  if (FIndTotP_Hrs) <> 0 then
  begin
    FloatVar := FIndIllP_Hrs * 100/(FIndTotP_Hrs);
    QRLabelRIndP.Caption := Format('%3.1f', [FloatVar]) + '%'
  end
  else
    QRLabelRIndP.Caption := '0.0%';
}
  if (FDirTotP_Hrs ) <> 0 then
  begin
    FloatVar := FDirIllP_Hrs * 100/FDirTotP_Hrs;
    QRLabelRDirP.Caption := Format('%3.1f', [FloatVar])  + '%'
  end
  else
    QRLabelRDirP.Caption := '0.0%';

  if FDirContractP_Hrs <> 0 then
  begin
    FloatVar := FDirIllP_Hrs * 100/(FDirContractP_Hrs*60);
    QRLabelRContrDirP.Caption := Format('%3.1f', [FloatVar]) + '%'
  end
  else
    QRLabelRContrDirP.Caption := '0.0%';

  if FIndContractP_Hrs <> 0 then
  begin
    FloatVar := FIndIllP_Hrs * 100/(FIndContractP_Hrs*60);
    QRLabelRContrIndP.Caption :=  Format('%3.1f', [FloatVar]) + '%';
  end
  else
    QRLabelRContrIndP.Caption := '0.0%';
end;

procedure TReportRatioIllnessHrsQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  FDirIll_Hrs := 0;
  FIndIll_Hrs := 0;
  FDirTot_Hrs := 0;
  FIndTot_Hrs := 0;
  FDirContract_Hrs := 0;
  FIndContract_Hrs := 0;
end;

procedure TReportRatioIllnessHrsQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  GetContractMinPerEmployee;
  FillLabelsPerEmpl;
  FDirIllP_Hrs := FDirIllP_Hrs +  FDirIll_Hrs;
  FIndIllP_Hrs := FIndIllP_Hrs +  FIndIll_Hrs;
  FDirTotP_Hrs := FDirTotP_Hrs + FDirTot_Hrs;
  FIndTotP_Hrs := FIndTotP_Hrs + FIndTot_Hrs;
  FDirContractP_Hrs := FDirContractP_Hrs + FDirContract_Hrs;
  FIndContractP_Hrs := FIndContractP_Hrs + FIndContract_Hrs;
end;

procedure TReportRatioIllnessHrsQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  FillLabelsPerPlant;
  FDirIllT_Hrs := FDirIllT_Hrs +  FDirIllP_Hrs;
  FIndIllT_Hrs := FIndIllT_Hrs +  FIndIllP_Hrs;
  FDirTotT_Hrs := FDirTotT_Hrs + FDirTotP_Hrs;
  FIndTotT_Hrs := FIndTotT_Hrs + FIndTotP_Hrs;
  FDirContractT_Hrs := FDirContractT_Hrs + FDirContractP_Hrs;
  FIndContractT_Hrs := FIndContractT_Hrs + FIndContractP_Hrs;
end;

procedure TReportRatioIllnessHrsQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  FloatVar: Double;
begin
  inherited;

  QRLabelIllIndT.Caption := DecodeHrsMin(FIndIllT_Hrs);
  QRLabelIllDirT.Caption := DecodeHrsMin(FDirIllT_Hrs);
  QRLabelTotDirT.Caption := DecodeHrsMin(FDirTotT_Hrs);
  QRLabelTotIndT.Caption := DecodeHrsMin(FIndTotT_Hrs);
  QRLabelTotT.Caption := DecodeHrsMin(FDirTotT_Hrs + FIndTotT_Hrs);
  QRLabelContrIndT.Caption := DecodeHrsDouble(FIndContractT_Hrs);
  QRLabelContrDirT.Caption := DecodeHrsDouble(FDirContractT_Hrs);

  // 20656.
  if (FTotIndDirGrandTotal_Hrs) <> 0 then
  begin
    FloatVar := FIndIllT_Hrs * 100 / FTotIndDirGrandTotal_Hrs;
    QRLabelRIndT.Caption := Format('%3.1f', [FloatVar])  + '%'
  end
  else
    QRLabelRIndT.Caption := '0.0%';
{
  if (FIndTotT_Hrs) <> 0 then
  begin
    FloatVar := FIndIllT_Hrs * 100/FIndTotT_Hrs;
    QRLabelRIndT.Caption := Format('%3.1f', [FloatVar])  + '%'
  end
  else
    QRLabelRIndT.Caption := '0.0%';
}
  if (FDirTotT_Hrs) <> 0 then
  begin
    FloatVar := FDirIllT_Hrs * 100/FDirTotT_Hrs;
    QRLabelRDirT.Caption := Format('%3.1f', [FloatVar]);
  end
  else
    QRLabelRDirT.Caption := '0.0';
  QRLabelRDirT.Caption := QRLabelRDirT.Caption + '%';
  if FDirContractT_Hrs <> 0 then
  begin
    FloatVar := FDirIllT_Hrs * 100/(FDirContractT_Hrs*60);
    QRLabelRContrDirT.Caption := Format('%3.1f', [FloatVar]);
  end
  else
    QRLabelRContrDirT.Caption := '0.0';
  QRLabelRContrDirT.Caption  := QRLabelRContrDirT.Caption  + '%';
  if FIndContractT_Hrs <> 0 then
  begin
    FloatVar := FIndIllT_Hrs * 100/(FIndContractT_Hrs*60);
    QRLabelRContrIndT.Caption := Format('%3.1f', [FloatVar]) + '%'
  end
  else
    QRLabelRContrIndT.Caption := '0.0%';
  if (FDirTotT_Hrs + FIndTotT_Hrs) <> 0 then
  begin
    FloatVar := (FIndIllT_Hrs + FDirIllT_Hrs) * 100/
      (FIndTotT_Hrs + FDirTotT_Hrs);
    QRLabelRIllHrsDir.Caption := Format('%3.1f', [FloatVar]) + '%';
  end
  else
    QRLabelRIllHrsDir.Caption := '0.0%';
  if (FDirContractT_Hrs + FIndContractT_Hrs) <> 0 then
  begin
    FloatVar := (FIndIllT_Hrs + FDirIllT_Hrs) * 100/
      ((FDirContractT_Hrs + FIndContractT_Hrs) * 60);
    QRLabelRIllHrsContr.Caption := Format('%3.1f', [FloatVar]) + '%';
  end
  else
    QRLabelRIllHrsContr.Caption := '0.0%';
end;

procedure TReportRatioIllnessHrsQR.QRDBText4Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value,0,10);
end;

procedure TReportRatioIllnessHrsQR.QRGroupHDPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  FDirIllP_Hrs := 0;
  FIndIllP_Hrs := 0;
  FDirTotP_Hrs := 0;
  FIndTotP_Hrs := 0;
  FDirContractP_Hrs := 0;
  FIndContractP_Hrs := 0;
  FTotIndDirPlant_Hrs := 0; // 20656.
end;

procedure TReportRatioIllnessHrsQR.QRDBTextEmplDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value, 0, 15);
end;

procedure TReportRatioIllnessHrsQR.FormDestroy(Sender: TObject);
begin
  inherited;
  FListABS_Ill.Free;
  FListABS_Tot.Free;
  FListProdMin.Free;
end;

procedure TReportRatioIllnessHrsQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportRatioIllnessHrsDM do
    begin
      // Plant header
      ExportClass.AddText(QRLabel3.Caption + ' ' +
        cdsEmpl.FieldByName('PLANT_CODE').AsString + ' ' +
        cdsEmpl.FieldByName('DESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportRatioIllnessHrsQR.ChildBandPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel24.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel9.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel30.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel15.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
    ExportClass.AddText(
      QRLabel21.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel6.Caption + ExportClass.Sep +
      QRLabel7.Caption + ExportClass.Sep +
      QRLabel10.Caption + ExportClass.Sep +
      QRLabel12.Caption + ExportClass.Sep +
      QRLabel14.Caption + ExportClass.Sep +
      QRLabel8.Caption + ExportClass.Sep +
      QRLabel29.Caption + ExportClass.Sep +
      QRLabel17.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel22.Caption + ExportClass.Sep +
      ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel25.Caption + ExportClass.Sep +
      QRLabel31.Caption + ExportClass.Sep +
      QRLabel23.Caption + ExportClass.Sep +
      QRLabel26.Caption + ExportClass.Sep);
  end;
end;

procedure TReportRatioIllnessHrsQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Detail
    with ReportRatioIllnessHrsDM do
    begin
      ExportClass.AddText(
        cdsEmpl.FieldByName('EMPLOYEE_NUMBER').AsString + ExportClass.Sep +
        cdsEmpl.FieldByName('DESCRIPTION_1').AsString + ExportClass.Sep +
        QRLabelIllInd.Caption + ExportClass.Sep +
        QRLabelIllDir.Caption + ExportClass.Sep +
        QRLabelTotInd.Caption + ExportClass.Sep +
        QRLabelTotDir.Caption + ExportClass.Sep +
        QRLabelTot.Caption + ExportClass.Sep +
        QRLabelContrInd.Caption + ExportClass.Sep +
        QRLabelContrDir.Caption + ExportClass.Sep +
        QRLabelRInd.Caption + ExportClass.Sep +
        QRLabelRDir.Caption + ExportClass.Sep +
        QRLabelRContrInd.Caption + ExportClass.Sep +
        QRLabelRContrDir.Caption + ExportClass.Sep);
    end;
  end;
end;

procedure TReportRatioIllnessHrsQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Total Plant
    with ReportRatioIllnessHrsDM do
    begin
      ExportClass.AddText(
        QRLabel4.Caption + ExportClass.Sep +
        cdsEmpl.FieldByName('PLANT_CODE').AsString + ' ' +
          cdsEmpl.FieldByName('DESCRIPTION').AsString + ExportClass.Sep +
        QRLabelIllIndP.Caption + ExportClass.Sep +
        QRLabelIllDirP.Caption + ExportClass.Sep +
        QRLabelTotIndP.Caption + ExportClass.Sep +
        QRLabelTotDirP.Caption + ExportClass.Sep +
        QRLabelTotP.Caption + ExportClass.Sep +
        QRLabelContrIndP.Caption + ExportClass.Sep +
        QRLabelContrDirP.Caption + ExportClass.Sep +
        QRLabelRIndP.Caption + ExportClass.Sep +
        QRLabelRDirP.Caption + ExportClass.Sep +
        QRLabelRContrIndP.Caption + ExportClass.Sep +
        QRLabelRContrDirP.Caption + ExportClass.Sep);
    end;
  end;
end;

procedure TReportRatioIllnessHrsQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Summary
    ExportClass.AddText(
      QRLabel28.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabelIllIndT.Caption + ExportClass.Sep +
      QRLabelIllDirT.Caption + ExportClass.Sep +
      QRLabelTotIndT.Caption + ExportClass.Sep +
      QRLabelTotDirT.Caption + ExportClass.Sep +
      QRLabelTotT.Caption + ExportClass.Sep +
      QRLabelContrIndT.Caption + ExportClass.Sep +
      QRLabelContrDirT.Caption + ExportClass.Sep +
      QRLabelRIndT.Caption + ExportClass.Sep +
      QRLabelRDirT.Caption + ExportClass.Sep +
      QRLabelRContrIndT.Caption + ExportClass.Sep +
      QRLabelRContrDirT.Caption + ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabelRIllHrsDir.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabelRIllHrsContr.Caption + ExportClass.Sep);
  end;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

end.
