unit AbsTypeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TAbsTypeDM = class(TGridBaseDM)
    TableDetailABSENCETYPE_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailEXPORT_CODE: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AbsTypeDM: TAbsTypeDM;

implementation

uses SystemDMT;

{$R *.DFM}

end.
