(*
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
*)
unit DialogReportEmployeePaylistFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxLayout, dxExGrEd, dxExELib, jpeg;

type
  TDialogReportEmployeePaylistF = class(TDialogReportBaseF)
    Label7: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    Label2: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label3: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    Label4: TLabel;
    dxSpinEditWeek: TdxSpinEdit;
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    CheckBoxShowHourtypes: TCheckBox;
    GroupBoxQualityIncentive: TGroupBox;
    RadioGroupLinearSteps: TRadioGroup;
    LabelStepSize: TLabel;
    dxSpinEditStepSize: TdxSpinEdit;
    QueryBU: TQuery;
    LabelDatePeriod: TLabel;
    procedure RadioGroupLinearStepsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure FillBusiness;
    //car 29.1.2004 not necessary it is declare in class
//    procedure FillEmpl;
  public
    { Public declarations }
  end;

var
  DialogReportEmployeePaylistF: TDialogReportEmployeePaylistF;

// RV089.1.
function DialogReportEmployeePaylistForm: TDialogReportEmployeePaylistF;

implementation

{$R *.DFM}

uses
  SystemDMT, ListProcsFRM, ReportEmployeePaylistDMT,
  ReportEmployeePaylistQRPT, UPimsMessageRes;

// RV089.1.
var
  DialogReportEmployeePaylistF_HND: TDialogReportEmployeePaylistF;

// RV089.1.
function DialogReportEmployeePaylistForm: TDialogReportEmployeePaylistF;
begin
  if (DialogReportEmployeePaylistF_HND = nil) then
  begin
    DialogReportEmployeePaylistF_HND := TDialogReportEmployeePaylistF.Create(Application);
    with DialogReportEmployeePaylistF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportEmployeePaylistF_HND;
end;

// RV089.1.
procedure TDialogReportEmployeePaylistF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportEmployeePaylistF_HND <> nil) then
  begin
    DialogReportEmployeePaylistF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportEmployeePaylistF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportEmployeePaylistF.RadioGroupLinearStepsClick(
  Sender: TObject);
var
  Steps: Boolean;
begin
  inherited;
  Steps := False;
  case RadioGroupLinearSteps.ItemIndex of
  0: Steps := False;
  1: Steps := True;
  end;
  LabelStepSize.Enabled := Steps;
  dxSpinEditStepSize.Enabled := Steps;
end;

procedure TDialogReportEmployeePaylistF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

{procedure TDialogReportEmployeePaylistF.FillEmpl;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    dxDBExtLookupEditEmplFrom.Visible := True;
    dxDBExtLookupEditEmplTo.Visible := True;
(*
    ListProcsF.FillComboBoxMasterPlant(
      qryEmployeeByPlant, CmbPlusEmployeeFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'EMPLOYEE_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(
      qryEmployeeByPlant, CmbPlusEmployeeTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'EMPLOYEE_NUMBER', False);
    CmbPlusEmployeeFrom.Visible := True;
    CmbPlusEmployeeTo.Visible := True;
*)
  end
  else
  begin
    dxDBExtLookupEditEmplFrom.Visible := False;
    dxDBExtLookupEditEmplTo.Visible := False;
(*
    CmbPlusEmployeeFrom.Visible := False;
    CmbPlusEmployeeTo.Visible := False;
*)
  end;
end;    }

procedure TDialogReportEmployeePaylistF.FormShow(Sender: TObject);
//var
//  Year, Week: Word;
begin
  InitDialog(True, False, False, True, False, False, False);
  UseDate := True;
  inherited;
  ComboBoxPlusBusinessFrom.ShowSpeedButton := False;
  ComboBoxPlusBusinessFrom.ShowSpeedButton := True;
  ComboBoxPlusBusinessTo.ShowSpeedButton := False;
  ComboBoxPlusBusinessTo.ShowSpeedButton := True;
  FillBusiness;
  DatePickerFromBase.DateTime := Date;
  DatePickerToBase.DateTime := Date;
//  ListProcsF.WeekUitDat(Now, Year, Week);
//  dxSpinEditYear.Value := Year;
//  dxSpinEditWeek.Value := Week;
//  dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(Year);
//  dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(Year);
//  RadioGroupLinearStepsClick(Sender);
end;

procedure TDialogReportEmployeePaylistF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
    LabelDatePeriod.Caption :=
      DateToStr(ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
        dxSpinEditWeek.IntValue, 1)) + ' - ' +
      DateToStr(
        ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
          dxSpinEditWeek.IntValue, 7));
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

procedure TDialogReportEmployeePaylistF.btnOkClick(Sender: TObject);
begin
  inherited;
  if DatePickerFromBase.DateTime > DatePickerToBase.DateTime then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbYes]);
    Exit;
  end;
  if ReportEmployeePaylistQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      IntToStr(GetIntValue( dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue( dxDBExtLookupEditEmplTo.Text)),
//      IntToStr(GetIntValue(CmbPlusEmployeeFrom.Value)),
//      IntToStr(GetIntValue(CmbPlusEmployeeTo.Value)),
//      Round(dxSpinEditYear.Value),
//      Round(dxSpinEditWeek.Value),
      Round(dxSpinEditStepSize.Value),
      DatePickerFromBase.DateTime,
      DatePickerToBase.DateTime,
      RadioGroupLinearSteps.ItemIndex = 1, // Show Steps?
      CheckBoxShowHourtypes.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxExport.Checked)
  then
    ReportEmployeePaylistQR.ProcessRecords;
end;

procedure TDialogReportEmployeePaylistF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportEmployeePaylistDM := CreateReportDM(TReportEmployeePaylistDM);
  ReportEmployeePaylistQR := CreateReportQR(TReportEmployeePaylistQR);
  LabelDatePeriod.Caption := '';
end;

procedure TDialogReportEmployeePaylistF.btnCancelClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TDialogReportEmployeePaylistF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
    ComboBoxPlusBusinessTo.DisplayValue :=
      ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportEmployeePaylistF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessFrom.DisplayValue :=
      ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportEmployeePaylistF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillBusiness;
end;

procedure TDialogReportEmployeePaylistF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillBusiness;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportEmployeePaylistF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportEmployeePaylistF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
