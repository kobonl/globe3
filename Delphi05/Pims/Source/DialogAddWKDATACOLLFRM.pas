unit DialogAddWKDATACOLLFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables;

type
  TDialogAddWKDATACOLLF = class(TDialogSelectionF)
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    ComboBoxPlusFromDept: TComboBoxPlus;
    Label14: TLabel;
    ComboBoxPlusToDept: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    ComboBoxPlusFromWK: TComboBoxPlus;
    Label15: TLabel;
    ComboBoxPlusToWK: TComboBoxPlus;
    Label2: TLabel;
    DateFrom: TDateTimePicker;
    Label4: TLabel;
    TimeFrom: TdxTimeEdit;
    Label8: TLabel;
    DateTo: TDateTimePicker;
    Label6: TLabel;
    TimeTo: TdxTimeEdit;
    StoredProcADDWK: TStoredProc;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure ComboBoxPlusFromDeptCloseUp(Sender: TObject);
    procedure ComboBoxPlusToDeptCloseUp(Sender: TObject);
    procedure ComboBoxPlusFromWKCloseUp(Sender: TObject);
    procedure ComboBoxPlusToWKCloseUp(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    FPlant: String;
    FDate: TDateTime;
  end;

var
  DialogAddWKDATACOLLF: TDialogAddWKDATACOLLF;

implementation

uses ListProcsFRM, DataCollectionEntryDMT, SystemDMT, UPimsMessageRes,
  UPimsConst;

{$R *.DFM}

procedure TDialogAddWKDATACOLLF.FormShow(Sender: TObject);
begin
  inherited;
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryDeptPlant, ComboBoxPlusFromDept,
    FPlant, '', True, 'PLANT_CODE', '', 'DEPARTMENT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryDeptPlant, ComboBoxPlusToDept,
    FPlant, '', False, 'PLANT_CODE', '', 'DEPARTMENT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryWKPlant, ComboBoxPlusFromWK,
    FPlant, '', True, 'PLANT_CODE', '', 'WORKSPOT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryWKPlant, ComboBoxPlusToWK,
    FPlant, '', False, 'PLANT_CODE', '', 'WORKSPOT_CODE', 'DESCRIPTION');
  DateFrom.Date := FDate;
  DateTo.Date := FDate;
end;

procedure TDialogAddWKDATACOLLF.btnOkClick(Sender: TObject);
var
  SDate, EDate: TDateTime;
begin
  inherited;
  SDate := GetDate(DateFrom.Date) + TimeFrom.Time;
  EDate := GetDate(DateTo.Date) + TimeTo.Time ;
  if (SDate >= EDate) then
  begin
    DisplayMessage( SPimsStartEndDateTimeEQ, mtInformation, [mbOk]);
    Exit;
  end;
  if (TimeFrom.Time  = 0) or (TimeTo.Time = 0) then
  begin
    if DisplayMessage(SPimsStartEndTimeNull,mtInformation,[mbYes,mbNo]) = mrNo then
      exit;
  end;
  StoredProcADDWK.Prepare;
  StoredProcADDWK.ParamByName('PLANT_CODE').asString := FPlant;
  StoredProcADDWK.ParamByName('DEPT_FROM').asString :=
    GetStrValue(ComboBoxPlusFromDept.DisplayValue);
  StoredProcADDWK.ParamByName('DEPT_TO').asString :=
    GetStrValue(ComboBoxPlusToDept.DisplayValue);
  StoredProcADDWK.ParamByName('WK_FROM').asString :=
    GetStrValue(ComboBoxPlusFromWK.DisplayValue);
  StoredProcADDWK.ParamByName('WK_TO').asString :=
    GetStrValue(ComboBoxPlusToWK.DisplayValue);
  StoredProcADDWK.ParamByName('SDATE').asDateTime := SDate;
  StoredProcADDWK.ParamByName('EDATE').asDateTime := EDate;
  StoredProcADDWK.ParamByName('CREATIONDATE').asdateTime := Now;
  StoredProcADDWK.ParamByName('MUTATIONDATE').asdateTime := Now;
  StoredProcADDWK.ParamByName('MUTATOR').asString :=
  	SystemDM.CurrentProgramUser;
  //Car 550279
  // Pims -> Oracle, use '*' instead of empty string.
  StoredProcADDWK.ParamByName('USER_NAME').asString := '*';
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    StoredProcADDWK.ParamByName('USER_NAME').asString :=
      SystemDM.UserTeamLoginUser;
  StoredProcADDWK.ExecProc;

  DisplayMessage(SADDWKDateCollection , mtInformation, [mbOk]);
  DialogAddWKDATACOLLF.Close;
end;

procedure TDialogAddWKDATACOLLF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
  begin
    DateTo.Date := DateFrom.Date;
    TimeTo.Time := TimeFrom.Time;
  end;

end;

procedure TDialogAddWKDATACOLLF.DateToChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
  begin
    DateFrom.Date := DateTo.Date;
    TimeFrom.Time := TimeTo.Time;
  end;
end;

procedure TDialogAddWKDATACOLLF.ComboBoxPlusFromDeptCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromDept.DisplayValue <> '') and
     (ComboBoxPlusToDept.DisplayValue <> '') then
    if (ComboBoxPlusFromDept.Value > ComboBoxPlusToDept.Value) then
      ComboBoxPlusToDept.DisplayValue := ComboBoxPlusFromDept.DisplayValue;
end;

procedure TDialogAddWKDATACOLLF.ComboBoxPlusToDeptCloseUp(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromDept.DisplayValue <> '') and
     (ComboBoxPlusToDept.DisplayValue <> '') then
    if (ComboBoxPlusFromDept.Value >  ComboBoxPlusToDept.Value) then
      ComboBoxPlusFromDept.DisplayValue := ComboBoxPlusToDept.DisplayValue;
end;

procedure TDialogAddWKDATACOLLF.ComboBoxPlusFromWKCloseUp(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromWK.DisplayValue <> '') and
     (ComboBoxPlusToWK.DisplayValue <> '') then
    if (ComboBoxPlusFromWK.Value > ComboBoxPlusToWK.Value) then
      ComboBoxPlusToWK.DisplayValue := ComboBoxPlusFromWK.DisplayValue;
end;

procedure TDialogAddWKDATACOLLF.ComboBoxPlusToWKCloseUp(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromWK.DisplayValue <> '') and
     (ComboBoxPlusToWK.DisplayValue <> '') then
    if (ComboBoxPlusFromWK.Value > ComboBoxPlusToWK.Value) then
      ComboBoxPlusFromWK.DisplayValue := ComboBoxPlusToWK.DisplayValue;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogAddWKDATACOLLF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
