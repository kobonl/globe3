inherited TimeRecScanningF: TTimeRecScanningF
  Tag = 1
  Left = 311
  Top = 162
  Width = 673
  Height = 472
  Caption = 'Time recording scannings'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 657
    Height = 79
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 76
      Width = 655
      Height = 2
    end
    object PageControl2: TPageControl [1]
      Left = 1
      Top = 1
      Width = 655
      Height = 76
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 0
      OnChange = PageControl2Change
      object TabSheet2: TTabSheet
        Caption = 'Not Completed Scans'
        object GroupBox7: TGroupBox
          Left = 0
          Top = 0
          Width = 647
          Height = 48
          Align = alClient
          TabOrder = 0
          object Label10: TLabel
            Left = 12
            Top = 16
            Width = 267
            Height = 18
            Caption = 'Not completed time recording scans'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object lblDateNotComplete: TLabel
            Left = 340
            Top = 20
            Width = 23
            Height = 13
            Caption = 'Date'
            Enabled = False
          end
          object cBoxAllNotComplete: TCheckBox
            Left = 500
            Top = 18
            Width = 46
            Height = 17
            Caption = 'All'
            Checked = True
            State = cbChecked
            TabOrder = 0
            OnClick = cBoxAllNotCompleteClick
          end
          object btnCloseNotComplete: TButton
            Left = 552
            Top = 14
            Width = 96
            Height = 25
            Caption = 'Close Scans'
            Enabled = False
            TabOrder = 1
            OnClick = btnCloseNotCompleteClick
          end
          object DateEditNotComplete: TDateTimePicker
            Left = 375
            Top = 16
            Width = 121
            Height = 21
            CalAlignment = dtaLeft
            Date = 0.544151967595099
            Time = 0.544151967595099
            DateFormat = dfShort
            DateMode = dmComboBox
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBtnShadow
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkDate
            ParseInput = False
            ParentFont = False
            TabOrder = 2
            OnChange = ChangeFilter
            OnEnter = DateEditNotCompleteEnter
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Scans per Employee'
        ImageIndex = 1
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 647
          Height = 48
          Align = alClient
          Caption = 'Employee / date / shift'
          TabOrder = 0
          object Label7: TLabel
            Left = 4
            Top = 20
            Width = 46
            Height = 13
            Caption = 'Employee'
          end
          object Label6: TLabel
            Left = 289
            Top = 20
            Width = 48
            Height = 13
            Alignment = taRightJustify
            Caption = 'Shift Date'
          end
          object LabelEmpDesc: TLabel
            Left = 124
            Top = 20
            Width = 137
            Height = 13
            AutoSize = False
            Caption = 'LabelEmpDesc'
          end
          object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
            Left = 52
            Top = 16
            Width = 69
            TabOrder = 0
            DataField = 'EMPLOYEE_NUMBER'
            DataSource = TimeRecScanningDM.DataSourceFilterEmployee
            ReadOnly = True
            PopupWidth = 500
            OnCloseUp = ChangeTablesFilter
            DBGridLayout = dxDBGridLayoutListEmployeeItemEmployee
            StoredValues = 64
          end
          object DateEditScan: TDateTimePicker
            Left = 343
            Top = 16
            Width = 121
            Height = 21
            CalAlignment = dtaLeft
            Date = 0.544151967595099
            Time = 0.544151967595099
            DateFormat = dfShort
            DateMode = dmComboBox
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBtnShadow
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkDate
            ParseInput = False
            ParentFont = False
            TabOrder = 1
            OnChange = ChangeFilter
            OnEnter = DateEditScanEnter
          end
          object cBoxShowOnlyActive: TCheckBox
            Left = 472
            Top = 20
            Width = 161
            Height = 17
            Caption = 'Show Only Active'
            Checked = True
            State = cbChecked
            TabOrder = 2
            OnClick = cBoxShowOnlyActiveClick
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Early / Late'
        ImageIndex = 2
        object GroupBox6: TGroupBox
          Left = 0
          Top = 0
          Width = 647
          Height = 48
          Align = alClient
          Caption = 'Date Selection '
          TabOrder = 0
          object Label9: TLabel
            Left = 44
            Top = 20
            Width = 91
            Height = 13
            Caption = 'Show request date'
          end
          object DateEditEarlyLate: TDateTimePicker
            Left = 167
            Top = 16
            Width = 121
            Height = 21
            CalAlignment = dtaLeft
            Date = 0.544151967595099
            Time = 0.544151967595099
            DateFormat = dfShort
            DateMode = dmComboBox
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBtnShadow
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkDate
            ParseInput = False
            ParentFont = False
            TabOrder = 0
            OnChange = ChangeFilter
            OnEnter = DateEditEarlyLateEnter
          end
        end
      end
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Top = 77
      Width = 655
      Height = 1
      KeyField = 'MUTATIONDATE'
      TabOrder = 1
      OptionsBehavior = [edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OnCustomDrawCell = dxMasterGridCustomDrawCell
      object dxMasterGridColumnRequestDate: TdxDBGridDateColumn
        Caption = 'Date'
        Width = 120
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REQUEST_DATE'
      end
      object dxMasterGridColumnEarlyTime: TdxDBGridTimeColumn
        Caption = 'Requested early time'
        Width = 142
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REQUESTED_EARLY_TIME'
      end
      object dxMasterGridColumnLateTime: TdxDBGridTimeColumn
        Caption = 'Requested late time'
        Width = 120
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REQUESTED_LATE_TIME'
      end
      object dxMasterGridColumnEmployee: TdxDBGridLookupColumn
        Caption = 'Employee'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEELU'
        ListFieldName = 'EMPLOYEE_NUMBER;DESCRIPTION'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 268
    Width = 657
    TabOrder = 3
    object GroupBoxEarlyLate: TGroupBox
      Left = 177
      Top = 1
      Width = 368
      Height = 163
      TabOrder = 1
      object GroupBoxEarlyLateDate: TGroupBox
        Left = 2
        Top = 15
        Width = 335
        Height = 146
        Align = alLeft
        Caption = 'Date / Employee'
        TabOrder = 0
        object Label15: TLabel
          Left = 20
          Top = 36
          Width = 23
          Height = 13
          Caption = 'Date'
        end
        object Label16: TLabel
          Left = 21
          Top = 76
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object dxDBDateEditEarlyLateDate: TdxDBDateEdit
          Tag = 1
          Left = 80
          Top = 32
          Width = 121
          TabOrder = 0
          OnEnter = dxDBDateEditEarlyLateDateEnter
          DataField = 'REQUEST_DATE'
          DataSource = TimeRecScanningDM.DataSourceMaster
          DateOnError = deToday
        end
        object DBLookupComboBox1: TDBLookupComboBox
          Tag = 1
          Left = 80
          Top = 72
          Width = 241
          Height = 21
          DataField = 'EMPLOYEE_NUMBER'
          DataSource = TimeRecScanningDM.DataSourceMaster
          DropDownRows = 6
          DropDownWidth = 310
          KeyField = 'EMPLOYEE_NUMBER'
          ListField = 'DESCRIPTION;EMPLOYEE_NUMBER'
          ListSource = TimeRecScanningDM.DataSourceEmployeeLU
          TabOrder = 1
        end
      end
      object GroupBoxEarlyLateRequest: TGroupBox
        Left = 337
        Top = 15
        Width = 29
        Height = 146
        Align = alClient
        Caption = 'Requested times'
        TabOrder = 1
        object Label17: TLabel
          Left = 48
          Top = 40
          Width = 24
          Height = 13
          Caption = 'Early'
        end
        object Label18: TLabel
          Left = 48
          Top = 84
          Width = 21
          Height = 13
          Caption = 'Late'
        end
        object dxDBTimeEditEarlyLateEary: TdxDBTimeEdit
          Left = 120
          Top = 36
          Width = 121
          TabOrder = 0
          DataField = 'REQUESTED_EARLY_TIME'
          DataSource = TimeRecScanningDM.DataSourceMaster
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
        object dxDBTimeEditEarlyLateLate: TdxDBTimeEdit
          Left = 120
          Top = 80
          Width = 121
          TabOrder = 1
          DataField = 'REQUESTED_LATE_TIME'
          DataSource = TimeRecScanningDM.DataSourceMaster
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
      end
    end
    object GroupBoxTimeRecordin: TGroupBox
      Left = 1
      Top = 1
      Width = 655
      Height = 163
      Align = alClient
      Caption = 'Time recording scan'
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 6
        Top = 13
        Width = 335
        Height = 52
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object dxDBLookupEditEmployee: TDBLookupComboBox
          Tag = 1
          Left = 56
          Top = 16
          Width = 265
          Height = 21
          DataField = 'EMPLOYEE_NUMBER'
          DataSource = TimeRecScanningDM.DataSourceDetail
          DropDownWidth = 310
          KeyField = 'EMPLOYEE_NUMBER'
          ListField = 'DESCRIPTION;EMPLOYEE_NUMBER'
          ListSource = TimeRecScanningDM.DataSourceEmployeeLU
          TabOrder = 0
        end
      end
      object GroupBox3: TGroupBox
        Left = 6
        Top = 72
        Width = 335
        Height = 83
        Caption = 'Date / time'
        TabOrder = 1
        object Label2: TLabel
          Left = 16
          Top = 20
          Width = 37
          Height = 13
          Caption = 'Date IN'
        end
        object Label4: TLabel
          Left = 8
          Top = 51
          Width = 47
          Height = 13
          Caption = 'Date OUT'
        end
        object dxTimeEditIn: TdxDBTimeEdit
          Tag = 1
          Left = 208
          Top = 16
          Width = 121
          TabOrder = 1
          DataField = 'DATETIME_IN'
          DataSource = TimeRecScanningDM.DataSourceDetail
          StoredValues = 4
        end
        object dxDateEditIn: TdxDBDateEdit
          Tag = 1
          Left = 72
          Top = 16
          Width = 121
          Enabled = False
          TabOrder = 0
          DataField = 'DATETIME_IN'
          DataSource = TimeRecScanningDM.DataSourceDetail
          UseEditMask = True
          OnDateChange = dxDateEditInDateChange
          StoredValues = 4
        end
        object dxDateEditOut: TdxDBDateEdit
          Left = 72
          Top = 48
          Width = 121
          TabOrder = 2
          OnEnter = dxDateEditOutEnter
          DataField = 'DATETIME_OUT'
          DataSource = TimeRecScanningDM.DataSourceDetail
          UseEditMask = True
          OnDateChange = dxDateEditOutDateChange
          StoredValues = 4
        end
        object dxTimeEditOut: TdxDBTimeEdit
          Left = 208
          Top = 48
          Width = 121
          TabOrder = 3
          OnEnter = dxTimeEditOutEnter
          DataField = 'DATETIME_OUT'
          DataSource = TimeRecScanningDM.DataSourceDetail
          OnChange = dxTimeEditOutChange
          StoredValues = 4
        end
      end
      object GroupBox4: TGroupBox
        Left = 345
        Top = 15
        Width = 308
        Height = 146
        Align = alRight
        TabOrder = 2
        object Label11: TLabel
          Left = 16
          Top = 20
          Width = 24
          Height = 13
          Caption = 'Plant'
        end
        object Label12: TLabel
          Left = 16
          Top = 48
          Width = 46
          Height = 13
          Caption = 'Workspot'
        end
        object Label13: TLabel
          Left = 16
          Top = 76
          Width = 42
          Height = 13
          Caption = 'JobCode'
        end
        object Label14: TLabel
          Left = 16
          Top = 104
          Width = 22
          Height = 13
          Caption = 'Shift'
        end
        object lblWorkspotDescription: TLabel
          Left = 152
          Top = 48
          Width = 109
          Height = 13
          Caption = 'lblWorkspotDescription'
        end
        object DBLookupComboBoxPlant: TDBLookupComboBox
          Tag = 1
          Left = 68
          Top = 16
          Width = 221
          Height = 21
          DataField = 'PLANT_CODE'
          DataSource = TimeRecScanningDM.DataSourceDetail
          DropDownWidth = 240
          KeyField = 'PLANT_CODE'
          ListField = 'DESCRIPTION;PLANT_CODE'
          ListSource = TimeRecScanningDM.DataSourcePlantLU
          TabOrder = 0
          OnCloseUp = DBLookupComboBoxPlantCloseUp
        end
        object DBLookupComboBoxJobCode: TDBLookupComboBox
          Left = 68
          Top = 75
          Width = 221
          Height = 21
          DataField = 'JOB_CODE'
          DataSource = TimeRecScanningDM.DataSourceDetail
          DropDownRows = 5
          DropDownWidth = 240
          KeyField = 'JOB_CODE'
          ListField = 'DESCRIPTION;JOB_CODE'
          ListSource = TimeRecScanningDM.DataSourceJobLU
          TabOrder = 2
          OnEnter = DBLookupComboBoxJobCodeEnter
        end
        object DBLookupComboBoxShift: TDBLookupComboBox
          Tag = 1
          Left = 68
          Top = 104
          Width = 221
          Height = 21
          DataField = 'SHIFT_NUMBER'
          DataSource = TimeRecScanningDM.DataSourceDetail
          DropDownRows = 3
          DropDownWidth = 240
          KeyField = 'SHIFT_NUMBER'
          ListField = 'DESCRIPTION;SHIFT_NUMBER'
          ListSource = TimeRecScanningDM.DataSourceShiftLU
          TabOrder = 3
        end
        object dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit
          Tag = 1
          Left = 68
          Top = 45
          Width = 78
          Style.BorderStyle = xbs3D
          TabOrder = 1
          OnEnter = dxDBExtLookupEditWorkspotEnter
          OnExit = dxDBExtLookupEditWorkspotExit
          OnKeyPress = dxDBExtLookupEditWorkspotKeyPress
          DataField = 'WORKSPOT_CODE'
          DataSource = TimeRecScanningDM.DataSourceDetail
          ImmediateDropDown = True
          PopupHeight = 120
          PopupWidth = 248
          OnCloseUp = dxDBExtLookupEditWorkspotCloseUp
          OnPopup = dxDBExtLookupEditWorkspotPopup
          DBGridLayout = dxDBGridLayoutListWorkspotItem
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 105
    Width = 657
    Height = 163
    inherited spltDetail: TSplitter
      Top = 162
      Width = 655
      Height = 0
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 655
      Height = 161
      KeyField = 'EMPLOYEE_NUMBER'
      OnClick = dxDetailGridClick
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDblClick, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      object dxDetailGridColumnDateIN: TdxDBGridDateColumn
        Caption = 'Date / Time in'
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATETIME_IN'
        UseEditMask = True
      end
      object dxDetailGridColumnDateOut: TdxDBGridDateColumn
        Caption = 'Date / Time out'
        ReadOnly = True
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATETIME_OUT'
        UseEditMask = True
      end
      object dxDetailGridColumnEmployee: TdxDBGridColumn
        Caption = 'Employee'
        Width = 52
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEECAL'
      end
      object dxDetailGridColumnPlant: TdxDBGridColumn
        Caption = 'Plant'
        Width = 104
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTCAL'
      end
      object dxDetailGridColumnWorkspot: TdxDBGridColumn
        Caption = 'Workspot'
        Width = 129
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOTCAL'
      end
      object dxDetailGridColumnJob: TdxDBGridColumn
        Caption = 'Job'
        Width = 91
        BandIndex = 0
        RowIndex = 0
        FieldName = 'JOBCAL'
      end
      object dxDetailGridColumnShift: TdxDBGridLookupColumn
        Caption = 'Shift'
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHIFTCAL'
      end
      object dxDetailGridColumnMutationDate: TdxDBGridColumn
        Caption = 'Timestamp'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MUTATIONDATE'
      end
      object dxDetailGridColumnMutator: TdxDBGridColumn
        Caption = 'User'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MUTATOR'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 628
    Top = 12
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonEditMode: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButtonShowGroup: TdxBarButton
      Enabled = False
      Visible = ivNever
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 660
    Top = 12
  end
  inherited StandardMenuActionList: TActionList
    Left = 596
    Top = 12
  end
  inherited dsrcActive: TDataSource
    Left = 4
    Top = 140
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 533
    Top = 11
    object dxDBGridLayoutListEmployeeItemEmployee: TdxDBGridLayout
      Data = {
        1B040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656500000D44656661756C744C61796F7574091348
        656164657250616E656C526F77436F756E740201084B65794669656C64060F45
        4D504C4F5945455F4E554D4245521153686F7753756D6D617279466F6F746572
        090D53756D6D61727947726F7570730E001053756D6D61727953657061726174
        6F7206022C200E506172656E7453686F7748696E74080A44617461536F757263
        65072A54696D655265635363616E6E696E67444D2E44617461536F7572636546
        696C746572456D706C6F7965650F4D6178526F774C696E65436F756E7402010F
        4F7074696F6E734265686176696F720B0C6564676F4175746F536F72740B6564
        676F45646974696E67136564676F456E74657253686F77456469746F72136564
        676F496D6D656469617465456469746F720E6564676F5461625468726F756768
        0F6564676F566572745468726F75676800094F7074696F6E7344420B10656467
        6F43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F
        43616E496E73657274116564676F43616E4E617669676174696F6E116564676F
        436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F726473
        106564676F557365426F6F6B6D61726B73000B4F7074696F6E73566965770B13
        6564676F42616E644865616465725769647468106564676F496E766572745365
        6C6563740D6564676F5573654269746D6170000F53686F775072657669657747
        726964080D53686F77526F77466F6F746572091253696D706C65437573746F6D
        697A65426F7809000F546478444247726964436F6C756D6E0C436F6C756D6E4E
        756D6265720743617074696F6E06064E756D6265720557696474680239094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060F
        454D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D
        6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274
        204E616D6505576964746802570942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060A53484F52545F4E414D4500000F546478
        444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06
        044E616D6505576964746803DE000942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060B4445534352495054494F4E00000F54
        6478444247726964436F6C756D6E0B436F6C756D6E506C616E74074361707469
        6F6E0605506C616E7405576964746803B8000942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D650607504C414E544C55000000}
    end
  end
  object dxDBGridLayoutListShift: TdxDBGridLayoutList
    Left = 565
    Top = 11
    object dxDBGridLayoutListShiftItem1: TdxDBGridLayout
      Data = {
        08020000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656500000D44656661756C744C61796F7574091348
        656164657250616E656C526F77436F756E740201084B65794669656C64061753
        484946545F4E554D4245523B504C414E545F434F44450D53756D6D6172794772
        6F7570730E001053756D6D617279536570617261746F7206022C200A44617461
        536F75726365072754696D655265635363616E6E696E67444D2E44617461536F
        7572636546696C7465725368696674000F546478444247726964436F6C756D6E
        0C436F6C756D6E4E756D6265720743617074696F6E06064E756D626572094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060C
        53484946545F4E554D42455200000F546478444247726964436F6C756D6E0A43
        6F6C756D6E4E616D650743617074696F6E06044E616D6506536F727465640704
        637355700942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0B436F6C756D6E506C616E740743617074696F6E0605506C616E7409
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        0607504C414E544C55000000}
    end
  end
  object dxDBGridLayoutListWorkspot: TdxDBGridLayoutList
    Left = 496
    Top = 11
    object dxDBGridLayoutListWorkspotItem: TdxDBGridLayout
      Data = {
        3D020000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0608576F726B73706F7400000D44656661756C744C61
        796F7574091348656164657250616E656C526F77436F756E740201084B657946
        69656C64060D574F524B53504F545F434F44450D53756D6D61727947726F7570
        730E001053756D6D617279536570617261746F7206022C200A44617461536F75
        726365072454696D655265635363616E6E696E67444D2E44617461536F757263
        65576F726B73706F74094F7074696F6E7344420B106564676F43616E63656C4F
        6E457869740D6564676F43616E44656C6574650D6564676F43616E496E736572
        74116564676F43616E4E617669676174696F6E116564676F436F6E6669726D44
        656C657465126564676F4C6F6164416C6C5265636F726473106564676F557365
        426F6F6B6D61726B730010507265766965774669656C644E616D65060B444553
        4352495054494F4E00135464784442477269644D61736B436F6C756D6E0D574F
        524B53504F545F434F44450743617074696F6E0604436F646505576964746802
        690942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D65060D574F524B53504F545F434F44450000135464784442477269644D6173
        6B436F6C756D6E0B4445534352495054494F4E0743617074696F6E060B446573
        6372697074696F6E055769647468027C0942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060B4445534352495054494F4E0000
        00}
    end
  end
end
