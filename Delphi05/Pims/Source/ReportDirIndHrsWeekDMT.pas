(*
  Changes:
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Field WEEKNUMBER gave error like:
        QueryDirIndHrs:
          Type mismatch for field WEEKNUMBER, expecting float, actual integer.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'. (QueryDirIndHrs).
      Also field-component for QueryDirIndHrs for this field must be 'float'.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)

unit ReportDirIndHrsWeekDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, DBClient, Provider;

type
  TReportDirIndHrsWeekDM = class(TReportBaseDM)
    QueryDirIndHrs: TQuery;
    DataSourceDirIndHrs: TDataSource;
    QueryDirIndHrsPRODHOUREMPLOYEE_DATE: TDateTimeField;
    QueryDirIndHrsDIRECT_HOUR_YN: TStringField;
    QueryDirIndHrsPLANT_CODE: TStringField;
    QueryDirIndHrsWORKSPOT_CODE: TStringField;
    QueryDirIndHrsSUMMIN: TFloatField;
    QueryBUPerWK: TQuery;
    dspQueryBUWK: TDataSetProvider;
    cdsQueryBUWK: TClientDataSet;
    QueryDirIndHrsBUCODE: TStringField;
    QueryDirIndHrsLISTCODE: TFloatField;
    QueryDirIndHrsWEEKNUMBER: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryDirIndHrsFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportDirIndHrsWeekDM: TReportDirIndHrsWeekDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportDirIndHrsWeekDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryDirIndHrs);
end;

procedure TReportDirIndHrsWeekDM.QueryDirIndHrsFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
