(*
  Changes:
    MRA:6-OCT-2009 RV037.
      - Add Close-button at right-bottom of menu-dialog.
    MRA:13-JAN-2010. RV050.9. 889966.
    - Allow user to enter a user-name as parameter.
    MRA:17-FEB-2010. RV054.5. 889946.
    - Show/keep the last selected option.
    - When a user has made a selection, show this in the menu, it
      should not show a different option when you move the mouse
      over it.
    MRA:11-FEB-2011. RV086.3. SC-50016862.
    - Fixed: Check for not-nil, when re-opening forms.
    MRA:14-FEB-2011. RV086.3. SC-50016862.
    - Sometimes a dialog is not shown on-top anymore, for example,
      when the desktop is shown and clicking the taskbar-icon to
      show the application again.
      Example: When 'Dialog Export Payroll' is started,
               this can become invisible, because
               it is behind the 'sidemenu'-dialog. For the user it looks
               like the application is not responding anymore.
   - How to solve this?
   MRA:28-MAY-2013 20014289 New Look Pims
   - Addition of extra Exit-option. This replaces the extra close-button.
   - Some pictures/colors are changed.
   MRA:25-JUN-2015 20014450.50
   - Real Time Eff.
*)

unit HomeFRM;
(*******************************************************************************
Unit     : FHome  TfrmHome = (TfrmPims)
Function : Application main form.
--------------------------------------------------------------------------------
Inheritance tree:
TForm
frmPims
  Changed design properties :
  + Font.Name := Tahoma (MicroSoft Outlook Style)
  + Position  := poScreenCenter

  Added controls:
  -
  Events:
  + FormCreate : Update the progressbar on the splashscreen

  Methods:
  -
TfrmHome
  Changed design properties :
  + Heigth := 385
  + Width  := 485
  + Position  := 0, 0

  Added controls:
  4 buttons
   //CAR : user rights changes
   add panels to align on vertical with equally spaces if one label is
   set not visible

  Events:
  -

  Methods:
  -

*******************************************************************************)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls,
  Db, DBTables, Dblup1a, dxCntner, dxTL, dxDBCtrl, dxDBGrid,
  Dblup1b, OleCtrls, SHDocVw, PimsFRM, Buttons;

type
  THomeF = class(TPimsF)
    PanelTop: TPanel;
    PanelUnitMaintenance: TPanel;
    ImageHomeTop: TImage;
    labelDefinitions: TLabel;
    labelTimeRecording: TLabel;
    labelStaffPlanning: TLabel;
    labelProductivity: TLabel;
    labelPayments: TLabel;
    labelReporting: TLabel;
    btnDefinitions: TButton;
    btnTimeRecording: TButton;
    btnStaffPlanning: TButton;
    btnProductivity: TButton;
    btnPayment: TButton;
    btnReporting: TButton;
    PanelBottom: TPanel;
    labelExit: TLabel;
    ImageBottom: TImage;
    ImageMain: TImage;
    PanelLeft: TPanel;
    ImageLLeft: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure labelDefinitionsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure imgHomeMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure labelDefinitionsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure labelDefinitionsMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnDefinitionsClick(Sender: TObject);
    procedure btnReportingClick(Sender: TObject);
    procedure btnStaffPlanningClick(Sender: TObject);
    procedure btnTimeRecordingClick(Sender: TObject);
    procedure btnProductivityClick(Sender: TObject);
    procedure btnPaymentClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure labelExitClick(Sender: TObject);
  protected
    procedure WMTimeChange(var Message: TMessage); message WM_TIMECHANGE;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  HomeF: THomeF;

implementation

uses
  SystemDMT,
  SideMenuUnitMaintenanceFRM, UMenuOptions,
  UPimsConst, UGlobalFunctions;

{$R *.DFM}

procedure THomeF.WMTimeChange(var Message: TMessage);
begin
  PimsTimeInitialised := False;
end;

procedure THomeF.FormCreate(Sender: TObject);
begin
  inherited;
  // PIM-250
  labelDefinitions.Font.Color := clDarkRed;
  labelTimeRecording.Font.Color := clDarkRed;
  labelStaffPlanning.Font.Color := clDarkRed;
  labelProductivity.Font.Color := clDarkRed;
  labelPayments.Font.Color := clDarkRed;
  labelReporting.Font.Color := clDarkRed;
  labelExit.Font.Color := clDarkRed;
  
  Caption := SystemDM.DatabaseServerName + ' - Globe' + ' - ' +
  SystemDM.CurrentComputerName;
  Left := 0;
  Top  := 0;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_ADMIN) then
    labelDefinitions.Visible := False;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_TIMERECORDING) then
    labelTimeRecording.Visible := False;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_STAFFPLANNING) then
    labelStaffPlanning.Visible := False;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_PRODUCTIVITY) then
    labelProductivity.Visible := False;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_PAYMENT) then
    labelPayments.Visible := False;
  if not SystemDM.CheckVisibleMenuOption(MENU_OPTION_REPORT) then
    labelReporting.Visible := False;
end;

procedure THomeF.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    85, 117 { U }:
      if labelDefinitions.Enabled then btnDefinitionsClick(btnDefinitions);
    82, 114 { R }:
      if labelReporting.Enabled then btnReportingClick(btnReporting);
    // for the French version: &Reports is translated with &Listings
    76 { L }:
      if labelReporting.Enabled then btnReportingClick(btnReporting);
    80, 112 :  {P}
       if labelPayments.Enabled then btnPaymentClick(btnPayment);
    84, 116 :  {T}
       if labelTimerecording.Enabled then btnTimeRecordingClick(btnTimeRecording);
    67, 99 :  {C}
       if labelProductivity.Enabled then btnProductivityClick(btnProductivity);
    83, 115 :  {S}
       if labelStaffPlanning.Enabled then btnStaffPlanningClick(btnStaffPlanning);
    69, 101 : {E}
      labelExitClick(labelExit);
   end; { case }
end;

procedure THomeF.labelDefinitionsMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
  procedure SetDisableLabel(FLabel: TLabel; FTag: Integer);
  begin
    if FLabel.Tag <> FTag then
    begin
      FLabel.Font.Color := clDarkRed; // PIMS-250 // clWhite;
      FLabel.Font.Style := FLabel.Font.Style - [fsUnderline];
    end;
  end;
begin
  inherited;
  if not Active then // RV054.5.
    Exit;
  SetDisableLabel(labelDefinitions, (Sender as TLabel).Tag);
  SetDisableLabel(labelTimeRecording, (Sender as TLabel).Tag);
  SetDisableLabel(labelStaffPlanning, (Sender as TLabel).Tag);
  SetDisableLabel(labelProductivity, (Sender as TLabel).Tag);
  SetDisableLabel(labelPayments, (Sender as TLabel).Tag);
  SetDisableLabel(labelReporting, (Sender as TLabel).Tag);
  SetDisableLabel(labelExit, (Sender as TLabel).Tag);
  if Sender is TLabel then
    if (Sender as TLabel).Tag > 0 then
    begin
      (Sender as TLabel).Font.Style :=
         (Sender as TLabel).Font.Style + [fsUnderline];
      (Sender as TLabel).Font.Color := clBlack; //PIM-250 // clYellow;
    end;
end;

procedure THomeF.imgHomeMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
  procedure SetDisableLabel(FLabel: TLabel);
  begin
    FLabel.Font.Color := clDarkRed; // PIM-250 // clWhite;
    FLabel.Font.Style := FLabel.Font.Style - [fsUnderline];
  end;
begin
  inherited;
  if not Active then // RV054.5.
    Exit;
  SetDisableLabel(labelDefinitions);
  SetDisableLabel(labelTimeRecording);
  SetDisableLabel(labelStaffPlanning);
  SetDisableLabel(labelProductivity);
  SetDisableLabel(labelPayments);
  SetDisableLabel(labelReporting);
  SetDisableLabel(labelExit);
end;

procedure THomeF.labelDefinitionsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TLabel).Font.Style :=
     (Sender as TLabel).Font.Style + [fsUnderline];
  (Sender as TLabel).Font.Color := clDarkRed; // PIM-250 // clPimsDarkBlue; // clLime; // 20014450.50
end;

procedure THomeF.labelDefinitionsMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TLabel).Font.Style :=
     (Sender as TLabel).Font.Style + [fsUnderline];
  (Sender as TLabel).Font.Color := clDarkRed; // PIMS-250 clWhite;
end;

procedure THomeF.btnDefinitionsClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowAdminPlant;
end;

procedure THomeF.btnReportingClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowReportMenu;
end;

procedure THomeF.btnStaffPlanningClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowPlanning;
end;

procedure THomeF.btnTimeRecordingClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowTimeRecording;
end;

procedure THomeF.btnProductivityClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowProductivity;
end;

procedure THomeF.btnPaymentClick(Sender: TObject);
begin
  inherited;
  SideMenuUnitMaintenanceF.ShowPayment;
end;

procedure THomeF.FormShow(Sender: TObject);
begin
  inherited;
  // 20014450.50
  // set color of image to panels
  PanelUnitMaintenance.Color := clPimsBlue;
  PanelLeft.Color := clWhite; //PIM-250 // clPimsBlue;
  PanelBottom.Color := clWhite; // PIMS-250 // clPimsBlue;
end;

procedure THomeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if SideMenuUnitMaintenanceF <> Nil then
    SideMenuUnitMaintenanceF.Close;
end;

procedure THomeF.labelExitClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
