inherited DialogReportAvailableUsedTimeF: TDialogReportAvailableUsedTimeF
  Left = 287
  Top = 202
  Caption = 'Report available, used and planned free time'
  ClientHeight = 392
  ClientWidth = 595
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 595
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 478
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 595
    Height = 271
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    inherited LblFromEmployee: TLabel
      Top = 124
    end
    inherited LblEmployee: TLabel
      Top = 124
    end
    inherited LblToEmployee: TLabel
      Top = 126
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 126
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 126
    end
    object Label7: TLabel [8]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [9]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [10]
      Left = 315
      Top = 42
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [11]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [12]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [13]
      Left = 307
      Top = 278
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [14]
      Left = 8
      Top = 313
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [15]
      Left = 40
      Top = 313
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [16]
      Left = 315
      Top = 313
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [17]
      Left = 128
      Top = 316
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [18]
      Left = 344
      Top = 316
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [19]
      Left = 344
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [20]
      Left = 344
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel [21]
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [22]
      Left = 128
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited LblFromDepartment: TLabel
      Top = 69
    end
    inherited LblDepartment: TLabel
      Top = 69
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 71
    end
    inherited LblStarDepartmentTo: TLabel
      Top = 71
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblStarTeamTo: TLabel
      Top = 100
    end
    inherited LblToTeam: TLabel
      Top = 99
    end
    inherited LblStarTeamFrom: TLabel
      Top = 100
    end
    inherited LblTeam: TLabel
      Top = 97
    end
    inherited LblFromTeam: TLabel
      Top = 97
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 95
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 96
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 96
      ColCount = 131
      TabOrder = 7
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 96
      ColCount = 132
      TabOrder = 8
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 520
      Top = 99
      TabOrder = 9
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 68
      ColCount = 131
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 68
      ColCount = 132
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 520
      Top = 71
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 122
      TabOrder = 10
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 122
      TabOrder = 11
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 135
      TabOrder = 12
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 29
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 33
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 32
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 146
      TabOrder = 21
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 147
      TabOrder = 22
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 147
      TabOrder = 23
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 148
      TabOrder = 24
    end
    inherited EditWorkspots: TEdit
      TabOrder = 30
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 154
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 153
      TabOrder = 31
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object GroupBox1: TGroupBox
      Left = 129
      Top = 152
      Width = 158
      Height = 113
      Caption = 'Selections'
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 14
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Show selections'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 88
        Width = 129
        Height = 17
        Caption = 'Export'
        TabOrder = 2
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 8
        Top = 56
        Width = 137
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 1
      end
    end
    object RadioGroupStatusEmpl: TRadioGroup
      Left = 8
      Top = 152
      Width = 121
      Height = 113
      Caption = 'Status Employee'
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Active'
        'Inactive'
        'All')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 130
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 131
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 520
      Top = 313
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object GroupBox2: TGroupBox
      Left = 287
      Top = 152
      Width = 170
      Height = 113
      Caption = 'Show'
      TabOrder = 34
      object CheckBoxHoliday: TCheckBox
        Left = 8
        Top = 24
        Width = 137
        Height = 17
        Caption = 'Holiday'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBoxWTR: TCheckBox
        Left = 8
        Top = 56
        Width = 145
        Height = 17
        Caption = 'Worktime reduction'
        TabOrder = 1
      end
      object CheckBoxTimeForTime: TCheckBox
        Left = 8
        Top = 88
        Width = 145
        Height = 17
        Caption = 'Time for time'
        TabOrder = 2
      end
    end
    object GroupBox3: TGroupBox
      Left = 458
      Top = 152
      Width = 133
      Height = 113
      Caption = 'Sort on'
      TabOrder = 35
      object CheckBoxSortOnDepartment: TCheckBox
        Left = 8
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Department'
        TabOrder = 0
      end
      object CheckBoxSortOnTeam: TCheckBox
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Team'
        TabOrder = 1
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 332
    Width = 595
  end
  inherited pnlBottom: TPanel
    Top = 351
    Width = 595
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 568
    Top = 64
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 48
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        51040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507314469616C6F675265706F7274417661696C61626C655573
        656454696D65462E44617461536F75726365456D706C46726F6D104F7074696F
        6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F
        42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F
        436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074
        696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E
        44656C6574650D6564676F43616E496E73657274116564676F43616E4E617669
        676174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F61
        64416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54
        6478444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074
        696F6E06064E756D62657206536F727465640704637355700557696474680241
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F454D504C4F5945455F4E554D42455200000F546478444247726964436F
        6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A5368
        6F7274206E616D6505576964746802540942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F
        546478444247726964436F6C756D6E0A436F6C756D6E4E616D65074361707469
        6F6E06044E616D6505576964746803B4000942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060B4445534352495054494F4E00
        000F546478444247726964436F6C756D6E0D436F6C756D6E4164647265737307
        43617074696F6E06074164647265737305576964746802450942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D6506074144445245
        535300000F546478444247726964436F6C756D6E0E436F6C756D6E4465707443
        6F64650743617074696F6E060F4465706172746D656E7420636F646505576964
        746802580942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F4445504152544D454E545F434F444500000F54647844424772
        6964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609546561
        6D20636F64650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        18040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072F4469616C6F675265706F72
        74417661696C61626C655573656454696D65462E44617461536F75726365456D
        706C546F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D
        6F76696E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D
        6F76696E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53
        697A696E6700094F7074696F6E7344420B106564676F43616E63656C4F6E4578
        69740D6564676F43616E44656C6574650D6564676F43616E496E736572741165
        64676F43616E4E617669676174696F6E116564676F436F6E6669726D44656C65
        7465126564676F4C6F6164416C6C5265636F726473106564676F557365426F6F
        6B6D61726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E45
        6D706C0743617074696F6E06064E756D62657206536F72746564070463735570
        05576964746802310942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060F454D504C4F5945455F4E554D42455200000F546478
        444247726964436F6C756D6E0F436F6C756D6E53686F72744E616D6507436170
        74696F6E060A53686F7274206E616D65055769647468024E0942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060A53484F5254
        5F4E414D4500000F546478444247726964436F6C756D6E11436F6C756D6E4465
        736372697074696F6E0743617074696F6E06044E616D6505576964746803CC00
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060B4445534352495054494F4E00000F546478444247726964436F6C756D6E
        0D436F6C756D6E416464726573730743617074696F6E06074164647265737305
        576964746802650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D6506074144445245535300000F546478444247726964436F6C
        756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F44657061
        72746D656E7420636F64650942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060F4445504152544D454E545F434F444500000F
        546478444247726964436F6C756D6E0A436F6C756D6E5465616D074361707469
        6F6E06095465616D20636F64650942616E64496E646578020008526F77496E64
        65780200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited qryPivotWorkspot: TQuery
    Left = 568
    Top = 29
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 8
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTypePerCountryExist: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.COUNTRY_ID'
      'FROM ABSENCETYPEPERCOUNTRY T'
      'WHERE T.COUNTRY_ID = :COUNTRY_ID'
      '')
    Left = 560
    Top = 104
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
end
