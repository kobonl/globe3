inherited DialogWorkspotPerEmployeeDM: TDialogWorkspotPerEmployeeDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 192
  Top = 157
  Height = 552
  Width = 805
  inherited TableMaster: TTable
    Left = 28
    Top = 16
  end
  inherited TableDetail: TTable
    Left = 28
    Top = 68
  end
  inherited DataSourceMaster: TDataSource
    Left = 120
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Left = 120
    Top = 68
  end
  inherited TableExport: TTable
    Left = 460
    Top = 300
  end
  inherited DataSourceExport: TDataSource
    Left = 592
    Top = 300
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 152
    Top = 296
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  T.TEAM_CODE, T.DESCRIPTION'
      'FROM '
      '  TEAM T'
      'ORDER BY '
      '  T.TEAM_CODE')
    Left = 348
    Top = 16
  end
  object ClientDataSetTeam: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTeam'
    Left = 456
    Top = 16
  end
  object DataSetProviderTeam: TDataSetProvider
    DataSet = QueryTeam
    Constraints = True
    Left = 592
    Top = 16
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE, P.DESCRIPTION '
      'FROM '
      '  PLANT P'
      'ORDER BY '
      '  P.PLANT_CODE')
    Left = 348
    Top = 64
  end
  object ClientDataSetPlant: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderPlant'
    Left = 456
    Top = 64
  end
  object DataSetProviderPlant: TDataSetProvider
    DataSet = QueryPlant
    Constraints = True
    Left = 592
    Top = 64
  end
  object TablePivot: TTable
    CachedUpdates = True
    AfterScroll = TablePivotAfterScroll
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'PIVOTWKPEREMP'
    UpdateMode = upWhereChanged
    Left = 24
    Top = 144
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 352
    Top = 136
  end
  object ClientDataSetEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpl'
    Left = 456
    Top = 136
  end
  object DataSetProviderEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 592
    Top = 136
  end
  object StoredProcInsertEMP: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'WORKSPOTPEREMPLOYEE_INSPIVOT'
    Left = 40
    Top = 296
  end
  object ClientDataSetWKPerEmpl: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderWKPerEmpl'
    Left = 456
    Top = 192
  end
  object DataSetProviderWKPerEmpl: TDataSetProvider
    Constraints = True
    Left = 592
    Top = 192
  end
  object ClientDataSetWK: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderWK'
    Left = 456
    Top = 248
  end
  object DataSetProviderWK: TDataSetProvider
    Constraints = True
    Left = 592
    Top = 248
  end
  object DataSourcePIVOT: TDataSource
    DataSet = TablePivot
    Left = 120
    Top = 144
  end
  object ClientDataSetSave: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetSaveIdx'
        Fields = 'EMPLOYEE_NUMBER;WORKSPOT_CODE;DEPARTMENT_CODE'
      end>
    IndexFieldNames = 'EMPLOYEE_NUMBER;WORKSPOT_CODE;DEPARTMENT_CODE'
    Params = <>
    ProviderName = 'DataSetProviderSave'
    StoreDefs = True
    Left = 456
    Top = 368
    object ClientDataSetSaveEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object ClientDataSetSaveWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object ClientDataSetSaveDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
  end
  object DataSetProviderSave: TDataSetProvider
    Constraints = True
    Left = 592
    Top = 360
  end
  object QueryCountColumn: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  COUNT(*)  AS COUNTCOL '
      'FROM  '
      '  RDB$RELATION_FIELDS REL'
      'WHERE '
      '  RDB$RELATION_NAME = '#39'PIVOTWKPEREMP'#39)
    Left = 232
    Top = 64
  end
  object QueryUpdateDB: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    ParamCheck = False
    UpdateMode = upWhereChanged
    Left = 232
    Top = 128
  end
end
