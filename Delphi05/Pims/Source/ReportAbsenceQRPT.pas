(*
  Changes
    MR:14-07-2004
      Changes for Order 550321, from date to date instead of
      from week-to-week.
    MRA:07-AUG-2008 RV008.
      - Link employee to plant instead of absence during select, to
        prevent absence is not shown if it has a different (employee-)plant.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:17-DEC-2010 RV083.3. SR-890041
    - Report Absence Hours
      - The report dialog always shows all absence
        reasons for selection. Instead of that it must
        show absence reasons per country. This should be
        based on plant-from and plant-to that is selected.
     - The report itself: When 1 plant (from-to the same) was choosen,
       it must look if there are absence-reasons-per-country (ARC).
       If this is the case, then it must just ARC-table to link.
       Otherwise it must use the absence-reasons to link.
    MRA:7-DEC-2018 PIM-414
    - Report absence hours and show per week issue
    - When using 'Report absence hours' and show per week, it should show
      per week and per absence reason what hours were booked. But when in one
      week 2 (or more) different absence-reasons are used
      (like: MO=S, TU=B, WE=S, TH=B, FR=S), then (in this example) it shows
      3 lines for S and 2 lines for B, but it should show 1 line for S
      and 1 line for B.
    - Use WEEK-table to know about what week it is via the select-statement,
      via FillWeekTable (part of ReportBaseDMT) this table is filled.
    - Added extra groups in report: Week and Reason. This is used when
      showing reasons per week.
*)

unit ReportAbsenceQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, Db;

type
  TQRParameters = class
  private
    { FYear, FWeekFrom, FWeekTo: Integer }
    FDateFrom, FDateTo: TDateTime;
    FSort: Integer;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo,
    FAbsenceTypeFrom, FAbsenceTypeTo, FAbsenceFrom, FAbsenceTo,
    FTeamFrom, FTeamTo: String;
    FShowDept, FShowTeam, FShowEmpl, FShowSelection, FPagePlant, FPageDept,
    FPageTeam, FPageEmpl, FAllAbsReason, FAllTeam, FTeamEMP,
    FSuppressZeroes, FExportToFile: Boolean;
  public
    procedure SetValues({ Year, WeekFrom, WeekTo: Integer; }
      DateFrom, DateTo: TDateTime;
      Sort: Integer;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo,
      AbsenceTypeFrom, AbsenceTypeTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo: String;
      AllAbsReason, AllTeam, ShowDept, ShowTeam, ShowEmpl,
      ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP,
      SuppressZeroes, ExportToFile: Boolean);
  end;

  TAmountsPerDay = Array[1..7] of Integer;
  TAmountsPrintPerDay = Array[1..7] of Boolean;

  TReportAbsenceQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDDept: TQRGroup;
    QRGroupHDTeam: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGroupHDWeekNumber: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelAbsReasonFrom: TQRLabel;
    QRLabelAbsReasonTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDeptDesc: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterDept: TQRBand;
    ChildBandPlantWeekAbs: TQRChildBand;
    QRLabelWeekPlant: TQRLabel;
    QRLabelAbsReasonPlant: TQRLabel;
    QRLabelMO: TQRLabel;
    QRLabelTU: TQRLabel;
    QRLabelWE: TQRLabel;
    QRLabelTH: TQRLabel;
    QRLabelFR: TQRLabel;
    QRLabelSA: TQRLabel;
    QRLabelSU: TQRLabel;
    QRLabel5: TQRLabel;
    ChildBandDeptWeekAbs: TQRChildBand;
    QRLabelWeekDept: TQRLabel;
    QRLabelAbsReasonDept: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    ChildBandTeamWeekAbs: TQRChildBand;
    QRLabelWeekTeam: TQRLabel;
    QRLabelAbsReasonTeam: TQRLabel;
    QRBandFooterEmpl: TQRBand;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextTeamDesc: TQRDBText;
    QRLabelDetail: TQRLabel;
    QRLabelDetailDesc: TQRLabel;
    QRLabelAmountMO: TQRLabel;
    QRLabelAmountTU: TQRLabel;
    QRLabelAmountWE: TQRLabel;
    QRLabelAmountTH: TQRLabel;
    QRLabelAmountFR: TQRLabel;
    QRLabelAmountSA: TQRLabel;
    QRLabelAmountSU: TQRLabel;
    QRLabel36: TQRLabel;
    ChildBandTotalDetail: TQRChildBand;
    QRLabelTotalDesc: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabelTotalDetail: TQRLabel;
    QRLabelTotal: TQRLabel;
    QRLabelTotalEmpl: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabelTotalDept: TQRLabel;
    QRLabelTotalPlant: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBTextEmplDesc: TQRDBText;
    QRBandFooterTeam: TQRBand;
    ChildBandEmplWeek: TQRChildBand;
    QRLabelWeekEmpl: TQRLabel;
    QRLabelAbsReasonEmpl: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel76: TQRLabel;
    QRBandSummary: TQRBand;
    QRLabel4: TQRLabel;
    QRLabelAbsenceType: TQRLabel;
    QRLabelAbsenceTypeFrom: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabelAbsenceTypeTo: TQRLabel;
    QRLabelFromWeek: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRLabelDayFrom: TQRLabel;
    QRLabelFromDay: TQRLabel;
    QRLabelToWeek: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabelToDay: TQRLabel;
    QRLabelDayTo: TQRLabel;
    QRDBText2: TQRDBText;
    QRBandFooterWeekNumber: TQRBand;
    QRLabelTotalWeekNumber: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabelWeekTotal: TQRLabel;
    QRGroupHDAbsenceReasonCode: TQRGroup;
    QRBandFooterAbsenceReasonCode: TQRBand;
    QRLblWeekNr: TQRLabel;
    QRLblAbsenceReason: TQRLabel;
    QRLblAbsRsnMO: TQRLabel;
    QRLblAbsRsnTU: TQRLabel;
    QRLblAbsRsnWE: TQRLabel;
    QRLblAbsRsnTH: TQRLabel;
    QRLblAbsRsnFR: TQRLabel;
    QRLblAbsRsnSA: TQRLabel;
    QRLblAbsRsnSU: TQRLabel;
    QRLblAbsRsnTotal: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandPlantWeekAbsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  
    procedure ChildBandTotalDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotalEmplPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalPlantPrint(sender: TObject; var Value: String);
    procedure QRDBTextDeptDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextTeamDescPrint(sender: TObject; var Value: String);

    procedure QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRBandFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel86Print(sender: TObject; var Value: String);
    procedure QRDBTextEmplDescPrint(sender: TObject; var Value: String);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelAmountPrint(sender: TObject; var Value: String);
    procedure QRBandFooterWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterAbsenceReasonCodeBeforePrint(
      Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRGroupHDAbsenceReasonCodeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterAbsenceReasonCodeAfterPrint(
      Sender: TQRCustomBand; BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
    FirstTimeDetail: Boolean;
   protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate,
    FMaxDate: TDateTime;
    PrintTotalDetailBand: Boolean;
    AbsencePerEmpl,
    AbsencePerReason, TotalAbsencePerReason, TotalOnDetail,
    TotalOnDays, TotalEmpl, TotalTeam, TotalDept, TotalPlant:TAmountsPerDay;
    PrintTotalOnDays: TAmountsPrintPerDay;
    FReason, FReasonDesc, FPlant, FDept, FTeam: String;
    FEmpl: Integer;
    FWeek: Word;
    FEmplDesc, FDeptDesc, FTeamDesc, FPlantDesc: String; // MR:31-12-2002
    FTotalText: String; // MR:31-12-2002
    function CheckFixValues: Boolean;
    procedure InitializeAmountsPerDaysOfWeek(var AmountsPerDaysOfWeek: TAmountsPerDay);
    procedure FillAmountsPerDaysOfWeek(IndexMin: Integer;
      AmountsPerDaysOfWeek: TAmountsPerDay; PrintDecide: Boolean);
    procedure FillDescDaysPerWeek(IndexMin: Integer);
    procedure PrintDetailWeek(ActualWeek: Integer; Reason, ReasonDesc: String);
    procedure PrintDetailWeekReason(ActualWeek, Day: Integer; Reason, ReasonDesc: String);
    procedure PrintTotalDetailReason;
    procedure PrintTotalDetailWeek;

    procedure PrintReasonWeek;
    function PrintWeekReason: Boolean;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo,
      AbsenceTypeFrom, AbsenceTypeTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
      { const Year, WeekFrom, WeekTo: Integer; }
      const DateFrom, DateTo: TDateTime;
      const Sort: Integer;
      const ShowAllAbsReason, ShowAllTeam, ShowDept, ShowTeam, ShowEmpl,
        ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl,
        TeamEMP: Boolean;
        SuppressZeroes,
        ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportAbsenceQR: TReportAbsenceQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAbsenceDMT, UGlobalFunctions, ListProcsFRM,
  UPimsMessageRes, UPimsConst;

// MR:12-12-2002
procedure ExportDetail(
  TotalText, Code, Description, Week, D1, D2, D3, D4, D5, D6, D7, Total: String);
begin
  ExportClass.AddText(
    TotalText + ExportClass.Sep +
    Code + ExportClass.Sep +
    Description + ExportClass.Sep +
    Week + ExportClass.Sep +
    D1 + ExportClass.Sep +
    D2 + ExportClass.Sep +
    D3 + ExportClass.Sep +
    D4 + ExportClass.Sep +
    D5 + ExportClass.Sep +
    D6 + ExportClass.Sep +
    D7 + ExportClass.Sep +
    Total
    );
end;

procedure TQRParameters.SetValues( {Year, WeekFrom, WeekTo: Integer; }
  DateFrom, DateTo: TDateTime;
  Sort: Integer;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceTypeFrom, AbsenceTypeTo,
  AbsenceFrom, AbsenceTo, TeamFrom, TeamTo: String;
  AllAbsReason, AllTeam, ShowDept, ShowTeam, ShowEmpl,
  ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP: Boolean;
  SuppressZeroes, ExportToFile: Boolean);
begin
//  FYear := Year;
//  FWeekFrom := WeekFrom;
//  FWeekTo := WeekTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FSort := Sort;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FAbsenceTypeFrom := AbsenceTypeFrom;
  FAbsenceTypeTo := AbsenceTypeTo;
  FAbsenceFrom := AbsenceFrom;
  FAbsenceTo := AbsenceTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FAllAbsReason := AllAbsReason;
  FAllTeam := AllTeam;
  FShowDept := ShowDept;
  FShowTeam := ShowTeam;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageDept := PageDept;
  FPageTeam := PageTeam;
  FPageEmpl := PageEmpl;
  FTeamEMP := TeamEMP;
  FSuppressZeroes := SuppressZeroes;
  FExportToFile := ExportToFile;
end;

function TReportAbsenceQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceTypeFrom, AbsenceTypeTo,
  AbsenceFrom, AbsenceTo,
  TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
  { const Year, WeekFrom, WeekTo: Integer; }
  const DateFrom, DateTo: TDateTime;
  const Sort: Integer; const ShowAllAbsReason, ShowAllTeam, ShowDept, ShowTeam,
  ShowEmpl, ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl,
  TeamEMP: Boolean;
  SuppressZeroes, ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues( { Year, WeekFrom, WeekTo }
      DateFrom, DateTo, Sort,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceTypeFrom,
      AbsenceTypeTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo, ShowAllAbsReason, ShowAllTeam,ShowDept, ShowTeam, ShowEmpl,
      ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP,
      SuppressZeroes, ExportToFile);
  end;
  SetDataSetQueryReport(ReportAbsenceDM.QueryAbsence);
end;

function TReportAbsenceQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  CountryId, CountryCnt: Integer; // RV083.3.
  AbsenceReasonCountryJoin: String; // RV083.3.
begin
  Screen.Cursor := crHourGlass;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FDeptFrom := '1';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FBusinessFrom := '1';
    QRParameters.FBusinessTo := 'zzzzzz';
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999';
  end;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;
//  FMinDate :=
//    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekFrom, 1);
//  FMaxDate :=
//    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekTo, 7);
  SelectStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE, P.DESCRIPTION AS PDESC, ' + NL;
  if QRParameters.FShowDept then
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, ' + NL;
  if QRParameters.FShowTeam then
    SelectStr := SelectStr +
      '  E.TEAM_CODE, T.DESCRIPTION AS TDESC, ' + NL;
  if QRParameters.FShowEmpl then
    SelectStr := SelectStr +
      '  A.EMPLOYEE_NUMBER, E.DESCRIPTION AS EDESC, ' + NL ;
  if QRParameters.FSort = 0 then
    SelectStr := SelectStr +
      '  A.ABSENCEREASON_ID, R.ABSENCEREASON_CODE, ' + NL +
      '  R.DESCRIPTION AS ABSDESC, ' + ' A.ABSENCEHOUR_DATE, ' + NL
  else
    SelectStr := SelectStr +
      '  WK.WEEKNUMBER, A.ABSENCEHOUR_DATE, A.ABSENCEREASON_ID, ' + NL +
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION AS ABSDESC, ' + NL;

  SelectStr := SelectStr +
    '  SUM(A.ABSENCE_MINUTE) AS SUMMIN ' + NL;

  // RV083.3.
  AbsenceReasonCountryJoin := '';
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    CountryCnt := 0;
    CountryId := SystemDM.GetDBValue(
      ' SELECT NVL(P.COUNTRY_ID, 0) ' +
      ' FROM PLANT P ' +
      ' WHERE ' +
      '   P.PLANT_CODE =  ' + '''' +
      QRBaseParameters.FPlantFrom + '''', 0);
    if CountryId <> 0 then
      CountryCnt :=
        SystemDM.GetDBValue('SELECT COUNT(*) ' +
          ' FROM ABSENCEREASONPERCOUNTRY WHERE COUNTRY_ID = ' +
          IntToStr(CountryId), 0);
    if (CountryId <> 0) and (CountryCnt <> 0) then
      AbsenceReasonCountryJoin :=
        '  INNER JOIN ABSENCEREASONPERCOUNTRY R2 ON ' + NL +
        '     R.ABSENCEREASON_ID = R2.ABSENCEREASON_ID AND ' + NL +
        '     R2.COUNTRY_ID = ' + IntToStr(CountryId);
  end;

  SelectStr := SelectStr + ' ' +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON ' + NL +
    '    A.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL +
    AbsenceReasonCountryJoin + // RV083.3.
    '  INNER JOIN PLANT P ON ' + NL +
    '    A.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    A.ABSENCEHOUR_DATE >= WK.DATEFROM AND ' + NL +
    '    A.ABSENCEHOUR_DATE <= WK.DATETO ' + NL;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) or
   (QRParameters.FShowDept) or (QRParameters.FShowTeam) or
   (not QRParameters.FAllTeam) then
  begin
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENT D ON ' + NL +
      '    E.PLANT_CODE = D.PLANT_CODE AND ' + NL +
      '    E.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
      '  INNER JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ';
  end;

  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
    '  AND A.ABSENCEHOUR_DATE <= :FENDDATE ' + NL +
    '  AND E.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND E.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND R.ABSENCETYPE_CODE >= ''' + DoubleQuote(QRParameters.FAbsenceTypeFrom) + '''' + NL +
    '  AND R.ABSENCETYPE_CODE <= ''' + DoubleQuote(QRParameters.FAbsenceTypeTo) + '''' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    SelectStr := SelectStr +
      '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
      '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
      '  AND D.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND D.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND D.BUSINESSUNIT_CODE >= ''' + DoubleQuote(QRParameters.FBusinessFrom) + ''''+ NL +
      '  AND D.BUSINESSUNIT_CODE <= ''' + DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
  end;

  if not QRParameters.FAllTeam then
  begin
    SelectStr := SelectStr +
      '  AND ( (E.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
      '  AND E.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + ''')' + NL;
    if QRParameters.FTeamEMP then
      SelectStr := SelectStr +
        '  OR  (E.TEAM_CODE IS NULL))' + NL
    else
      SelectStr := SelectStr +
        ')' + NL;
  end;
  if not QRParameters.FAllAbsReason  then
    SelectStr := SelectStr +
      ' AND R.ABSENCEREASON_CODE >= ''' + DoubleQuote(QRParameters.FAbsenceFrom) + '''' + NL +
      ' AND R.ABSENCEREASON_CODE <= ''' + DoubleQuote(QRParameters.FAbsenceTo) + '''' + NL;

  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + NL +
    '  A.PLANT_CODE, P.DESCRIPTION ' + NL;
  if (QRParameters.FShowDept) then
    SelectStr := SelectStr +
      '  , D.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
  if (QRParameters.FShowTeam) then
    SelectStr := SelectStr +
      '  , E.TEAM_CODE, T.DESCRIPTION ' + NL;
  if (QRParameters.FShowEmpl) then
    SelectStr := SelectStr +
      '  , A.EMPLOYEE_NUMBER, E.DESCRIPTION ' + NL;
  if QRParameters.FSort = 0 then
    SelectStr := SelectStr +
      '  , A.ABSENCEREASON_ID, R.ABSENCEREASON_CODE, ' + NL +
      '  R.DESCRIPTION, A.ABSENCEHOUR_DATE ' + NL
  else
    SelectStr := SelectStr +
      '  , WK.WEEKNUMBER, A.ABSENCEHOUR_DATE, A.ABSENCEREASON_ID, ' + NL +
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION ' + NL;
  // MR:02-03-2005 Added 'Order by', otherwise the report gives wrong result.
  SelectStr := SelectStr + ' ' +
    'ORDER BY ' + NL +
    '  A.PLANT_CODE, ' + NL;
  // Pims -> Oracle, add order by fields or ordering goes wrong
  if (QRParameters.FShowDept) then
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, ' + NL;
  if (QRParameters.FShowTeam) then
    SelectStr := SelectStr +
      '  E.TEAM_CODE, ' + NL;
  if (QRParameters.FShowEmpl) then
    SelectStr := SelectStr +
      '  A.EMPLOYEE_NUMBER, ' + NL;
  if QRParameters.FSort = 0 then
    SelectStr := SelectStr +
      '  R.ABSENCEREASON_CODE, A.ABSENCEHOUR_DATE '
  else
    SelectStr := SelectStr +
      '  WK.WEEKNUMBER, R.ABSENCEREASON_CODE, A.ABSENCEHOUR_DATE ';
  with ReportAbsenceDM do
  begin
    QueryAbsence.Active := False;
    ReportAbsenceDM.FillWeekTable(FMinDate, FMaxDate); // PIM-414
//  CAR 10-7-2003 remove Unprepare
   // QueryAbsence.UnPrepare;
    QueryAbsence.UniDirectional := False;
    QueryAbsence.SQL.Clear;
    // MR:02-03-2005 Don't convert to uppercase, because 'AbsenceReason_Code'
    // can be in small- or upper-case!
//    QueryAbsence.SQL.Add(UpperCase(SelectStr));
    QueryAbsence.SQL.Add(SelectStr);
// QueryAbsence.SQL.SaveToFile('c:\temp\reportabsence.sql');
    QueryAbsence.ParamByName('FSTARTDATE').Value := FMinDate;
    QueryAbsence.ParamByName('FENDDATE').Value := FMaxDate;
    if not QueryAbsence.Prepared then
      QueryAbsence.Prepare;
    QueryAbsence.Active := True;
    Result := not QueryAbsence.IsEmpty;
  end;
  FirstTimeDetail := True;
  //CAR : Update progress indicator
  if Result then
  begin
    ProgressIndicatorPos := 0;
    Report_RecordCount := ReportAbsenceDM.QueryAbsence.RecordCount;
  end;
  {check if report is empty}
  Screen.Cursor := crDefault;

end;

procedure TReportAbsenceQR.ConfigReport;
var
  Year, Week, Day: Word;
  procedure FillHeader(CaptionWeek, CaptionAbsence: String);
  begin
    QRLabelWeekPlant.Caption := CaptionWeek;
    QRLabelWeekDept.Caption :=  CaptionWeek;
    QRLabelWeekEmpl.Caption := CaptionWeek;
    QRLabelWeekTeam.Caption := CaptionWeek;
    QRLabelAbsReasonPlant.Caption := CaptionAbsence;
    QRLabelAbsReasonDept.Caption := CaptionAbsence;
    QRLabelAbsReasonEmpl.Caption := CaptionAbsence;
    QRLabelAbsReasonTeam.Caption := CaptionAbsence;
  end;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes; // RV054.4.
  // MR:12-12-2002
  ExportClass.ExportDone := False;
  FTotalText := QRLabelTotalDesc.Caption;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelAbsenceTypeFrom.Caption := QRParameters.FAbsenceTypeFrom;
  QRLabelAbsenceTypeTo.Caption := QRParameters.FAbsenceTypeTo;
  if QRParameters.FAllAbsReason then
  begin
    QRLabelAbsReasonFrom.Caption := '*';
    QRLabelAbsReasonTo.Caption := '*';
  end
  else
  begin
    QRLabelAbsReasonFrom.Caption := QRParameters.FAbsenceFrom;
    QRLabelAbsReasonTo.Caption := QRParameters.FAbsenceTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  ListProcsF.WeekUitDat(QRParameters.FDateFrom, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  QRLabelWeekFrom.Caption := IntToStr(Week);
  QRLabelDayFrom.Caption := IntToStr(Day);
  ListProcsF.WeekUitDat(QRParameters.FDateTo, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  QRLabelWeekTo.Caption := IntToStr(Week);
  QRLabelDayTo.Caption := IntToStr(Day) + ')';

//  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
//  QRLabelWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
//  QRLabelWeekTo.Caption := IntToStr(QRParameters.FWeekTo);

  ChildBandPlantWeekAbs.Enabled:= True;
  QRGroupHDDept.Enabled := False;
  ChildBandDeptWeekAbs.Enabled := False;
  QRGroupHDTeam.Enabled := False;
  ChildBandTeamWeekAbs.Enabled := False;
  QRBandFooterTeam.Enabled := False;
  QRGroupHDEmpl.Enabled := False;
  ChildBandEmplWeek.Enabled := False;
  QRBandFooterEmpl.Enabled := False;
  QRBandFooterDept.Enabled := False;
  if QRParameters.FShowDept then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDDept.Enabled := True;
    ChildBandDeptWeekAbs.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FShowTeam then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDTeam.Enabled := True;
    ChildBandTeamWeekAbs.Enabled := True;
    QRBandFooterTeam.Enabled := True;
  end;
  if QRParameters.FShowEmpl then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDEmpl.Enabled := True;
    ChildBandEmplWeek.Enabled := True;
    QRBandFooterEmpl.Enabled := True;
  end;
  if (QRParameters.FShowEmpl) and (QRParameters.FShowDept) then
  begin
    ChildBandDeptWeekAbs.Enabled := False;
  end;
  if (QRParameters.FShowEmpl) and (QRParameters.FShowTeam) then
  begin
    ChildBandTeamWeekAbs.Enabled := False;
  end;
  FillHeader('', '');
  if (QRParameters.FSort = 0) then
    FillHeader(SHeaderAbsRsn, SHeaderWeekAbsRsn)
  else
    FillHeader(SHeaderWeekAbsRsn, SHeaderAbsRsn);
end;

procedure TReportAbsenceQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportAbsenceQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:12-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportAbsenceQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  MyAbsenceReason, MyWeek: String;
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  FEmpl := 0;
  FPlant := '';
  FDept := '';
  FTeam := '';

  // MR:12-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(
          QRLabel11.Caption
          );
        // From Plant PlantFrom to PlantTo
        ExportClass.AddText(
          QRLabel13.Caption + ' ' +
          QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' +
          QRLabel49.Caption + ' ' +
          QRLabelPlantTo.Caption
          );
        // From BusinessUnit BUFrom to BUTo
        ExportClass.AddText(
          QRLabel25.Caption + ' ' +
          QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' +
          QRLabel16.Caption + ' ' +
          QRLabelBusinessTo.Caption
          );
        // From Department DepartmentFrom to DepartmentTo
        ExportClass.AddText(
          QRLabel47.Caption + ' ' +
          QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' +
          QRLabel50.Caption + ' ' +
          QRLabelDeptTo.Caption
          );
        // From Team TeamFrom to TeamTo
        ExportClass.AddText(
          QRLabel87.Caption + ' ' +
          QRLabel88.Caption + ' ' +
          QRLabelTeamFrom.Caption + ' ' +
          QRLabel90.Caption + ' ' +
          QRLabelTeamTo.Caption
          );
        // From Employee EmployeeFrom to EmployeeTo
        ExportClass.AddText(
          QRLabel18.Caption + ' ' +
          QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' +
          QRLabel22.Caption + ' ' +
          QRLabelEmployeeTo.Caption
          );
        // From AbsenceType AbsenceTypeFrom to AbsenceTypeTo
        ExportClass.AddText(
          QRLabel4.Caption + ' ' +
          QRLabelAbsenceType.Caption + ' ' +
          QRLabelAbsenceTypeFrom.Caption + ' ' +
          QRLabel91.Caption + ' ' +
          QRLabelAbsenceTypeTo.Caption
          );
        // From AbsenceReason AbsReasonFrom to AbsReasonTo
        ExportClass.AddText(
          QRLabel51.Caption + ' ' +
          QRLabelAbsenceRsn.Caption + ' ' +
          QRLabelAbsReasonFrom.Caption + ' ' +
          QRLabel53.Caption + ' ' +
          QRLabelAbsReasonTo.Caption
          );
        // Year Year
//        ExportClass.AddText(
//          QRLabel2.Caption + ' ' + QRLabelYear.Caption
//          );
        // From Date to Date
        ExportClass.AddText(
          QRLabelFromDate.Caption + ' ' +
          QRLabelDate.Caption + ' ' +
          QRLabelDateFrom.Caption + ' ' +
          QRLabelToDate.Caption + ' ' +
          QRLabelDateTo.Caption
          );
        // (from week day to week day)
        ExportClass.AddText(
          QRLabelFromWeek.Caption + ' ' +
          QRLabelWeekFrom.Caption + ' ' +
          QRLabelFromDay.Caption + ' ' +
          QRLabelDayFrom.Caption + ' ' +
          QRLabelToWeek.Caption + ' ' +
          QRLabelWeekTo.Caption + ' ' +
          QRLabelToDay.Caption + ' ' +
          QRLabelDayTo.Caption
          );
      end;
      if (QRParameters.FSort = 0) then
      begin
        MyAbsenceReason := SHeaderAbsRsn;
        MyWeek := SHeaderWeekAbsRsn;
      end
      else
      begin
        MyAbsenceReason := SHeaderWeekAbsRsn;
        MyWeek := SHeaderAbsRsn;
      end;
      // Show Column-names
      if (QRParameters.FSort = 0) then
        ExportDetail(
          '',
          MyAbsenceReason,
          '',
          MyWeek,
          SystemDM.GetDayWCode(1),
          SystemDM.GetDayWCode(2),
          SystemDM.GetDayWCode(3),
          SystemDM.GetDayWCode(4),
          SystemDM.GetDayWCode(5),
          SystemDM.GetDayWCode(6),
          SystemDM.GetDayWCode(7),
          QRLabel5.Caption
          )
      else
        ExportDetail(
          '',
          MyAbsenceReason,
          MyWeek,
          '',
          SystemDM.GetDayWCode(1),
          SystemDM.GetDayWCode(2),
          SystemDM.GetDayWCode(3),
          SystemDM.GetDayWCode(4),
          SystemDM.GetDayWCode(5),
          SystemDM.GetDayWCode(6),
          SystemDM.GetDayWCode(7),
          QRLabel5.Caption
          );
    end;
end;

procedure TReportAbsenceQR.PrintDetailWeek(ActualWeek: Integer;
  Reason, ReasonDesc: String);
var
  Counter, TotalDetail: Integer;
begin
  QRLabelDetail.Caption := '';
  if FirstTimeDetail then
  begin
    QRLabelDetail.Caption := Reason + ' ' + ReasonDesc;
    FirstTimeDetail := False;
  end;
  QRLabelDetailDesc.Caption := IntToStr(ActualWeek);
  FillAmountsPerDaysOfWeek(1, TotalOnDays, True);

  TotalDetail := 0;
  for Counter := 1 to 7 do
  begin
    TotalOnDetail[Counter] := TotalOnDetail[Counter] + TotalOnDays[Counter];
    TotalDetail := TotalDetail + TotalOnDays[Counter];
  end;
  QRLabelTotalDetail.Caption := DecodeHrsMin(TotalDetail);

  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      '',
      Reason,
      ReasonDesc,
      QRLabelDetailDesc.Caption, // Week
      IntToStr(TotalOnDays[1]),
      IntToStr(TotalOnDays[2]),
      IntToStr(TotalOnDays[3]),
      IntToStr(TotalOnDays[4]),
      IntToStr(TotalOnDays[5]),
      IntToStr(TotalOnDays[6]),
      IntToStr(TotalOnDays[7]),
      IntToStr(TotalDetail)
      );
end;

procedure TReportAbsenceQR.PrintTotalDetailReason;
var
  Counter, TotalDetail: Integer;
begin
  QRLabelTotalDesc.Caption := FTotalText +  FReason + ' ' + FReasonDesc;
  FillAmountsPerDaysOfWeek(40, TotalOnDetail, False);
  TotalDetail := 0;
  for Counter := 1 to 7 do
    TotalDetail := TotalDetail + TotalOnDetail[Counter];
  QRLabelTotal.Caption := DecodeHrsMin(TotalDetail);

  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      FTotalText,
      FReason,
      FReasonDesc,
      '',
      IntToStr(TotalOnDetail[1]),
      IntToStr(TotalOnDetail[2]),
      IntToStr(TotalOnDetail[3]),
      IntToStr(TotalOnDetail[4]),
      IntToStr(TotalOnDetail[5]),
      IntToStr(TotalOnDetail[6]),
      IntToStr(TotalOnDetail[7]),
      IntToStr(TotalDetail)
      );
end;

// PIM-414 Week -> Reason
procedure TReportAbsenceQR.PrintDetailWeekReason(ActualWeek, Day: Integer;
  Reason, ReasonDesc: String);
var
  Counter, TotalDetail: Integer;
begin
  QRLblWeekNr.Caption := '';
  if FirstTimeDetail then
  begin
    QRLblWeekNr.Caption := IntToStr(ActualWeek);
//    FirstTimeDetail := False;
  end;
  QRLblAbsenceReason.Caption := Reason + ' ' + ReasonDesc;

  FillAmountsPerDaysOfWeek(1, TotalOnDays, True);

  TotalOnDetail[Day] := TotalOnDetail[Day] + TotalOnDays[Day];

  TotalDetail := 0;
  for Counter := 1 to 7 do
  begin
//    TotalOnDetail[Counter] := TotalOnDetail[Counter] + TotalOnDays[Counter];
    TotalDetail := TotalDetail + TotalOnDays[Counter];
  end;
  QRLblAbsRsnTotal.Caption := DecodeHrsMin(TotalDetail);
end; // PrintDetailWeekReason

procedure TReportAbsenceQR.PrintTotalDetailWeek;
var
  Counter, TotalDetail: Integer;
begin
//  QRLabelTotalDesc.Caption := SHdTotalWeek +  ' ' + IntToStr(FWeek);
  QRLabelTotalWeekNumber.Caption := SHdTotalWeek +  ' ' +
    ReportAbsenceDM.QueryAbsence.FieldByName('WEEKNUMBER').AsString;
  FillAmountsPerDaysOfWeek(40, TotalOnDetail, False);
  TotalDetail := 0;
  for Counter := 1 to 7 do
    TotalDetail := TotalDetail + TotalOnDetail[Counter];
  QRLabelTotal.Caption := DecodeHrsMin(TotalDetail);
  QRLabelWeekTotal.Caption := DecodeHrsMin(TotalDetail);

  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      SHdTotalWeek,
      IntToStr(FWeek),
      '',
      '',
      IntToStr(TotalOnDetail[1]),
      IntToStr(TotalOnDetail[2]),
      IntToStr(TotalOnDetail[3]),
      IntToStr(TotalOnDetail[4]),
      IntToStr(TotalOnDetail[5]),
      IntToStr(TotalOnDetail[6]),
      IntToStr(TotalOnDetail[7]),
      IntToStr(TotalDetail)
      );
end;

function TReportAbsenceQR.CheckFixValues: Boolean;
begin
  Result := True;
  if (QRParameters.FShowDept) then
    Result := Result and (FDept =
      ReportAbsenceDM.QueryAbsence.FieldByName('DEPARTMENT_CODE').AsString);
  if (QRParameters.FShowTeam) then
     Result := Result and (FTeam =
      ReportAbsenceDM.QueryAbsence.FieldByName('TEAM_CODE').AsString);
  if (QRParameters.FShowEmpl) then
    Result := Result and (FEmpl =
      ReportAbsenceDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger);
end;

procedure TReportAbsenceQR.PrintReasonWeek;
var
  Count, Year, ActualWeek, Week: Word;
  AbsDate: TDateTime;
begin
  with  ReportAbsenceDM do
  begin
    PrintTotalDetailBand := False;
    for Count := 1 to 7 do
    begin
      TotalOnDays[Count] := 0;
      PrintTotalOnDays[Count] := False;
    end;
    FReason := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
    FReasonDesc := QueryAbsence.FieldByName('ABSDESC').AsString;
    AbsDate := QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
    ListProcsF.WeekUitDat(AbsDate, Year, ActualWeek);
    Count := ListProcsF.DayWStartOnFromDate(AbsDate);
    TotalOnDays[Count] := Round(QueryAbsence.FieldByName('SUMMIN').AsFloat);
    QueryAbsence.Next;
    // CAR : Update progress indicator
    ProgressIndicatorPos := ProgressIndicatorPos +1;
    while not (QueryAbsence.Eof) and  CheckFixValues and
     (QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString = FReason) do
    begin
      AbsDate := QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
      ListProcsF.WeekUitDat(AbsDate, Year, Week);
      if (Week = ActualWeek) then
      begin
        Count := ListProcsF.DayWStartOnFromDate(AbsDate);
        TotalOnDays[Count] := Round(QueryAbsence.FieldByName('SUMMIN').AsFloat);
        QueryAbsence.Next;
        // CAR : Update progress indicator
        ProgressIndicatorPos := ProgressIndicatorPos + 1;
      end
      else
      begin
// print one week
        for Count := 1 to 7 do
        begin
          AbsDate := ListProcsF.DateFromWeek(Year, ActualWeek, Count);
          PrintTotalOnDays[Count] := ((AbsDate >= QRParameters.FDateFrom) and
            (AbsDate <= QRParameters.FDateTo));
        end;
        PrintDetailWeek(ActualWeek, FReason, FReasonDesc);
        if not QueryAbsence.Eof then
          QueryAbsence.Prior;
        Exit;
      end;
    end;{while}
 // print one reason total
    for Count := 1 to 7 do
    begin
      AbsDate := ListProcsF.DateFromWeek(Year, ActualWeek, Count);
      PrintTotalOnDays[Count] := ((AbsDate >= QRParameters.FDateFrom) and
        (AbsDate <= QRParameters.FDateTo));
    end;
    PrintDetailWeek(ActualWeek, FReason, FReasonDesc);
    PrintTotalDetailBand := True;
    if not QueryAbsence.Eof then
      QueryAbsence.Prior;
  end;
end; // PrintReasonWeek

// PIM-414
function TReportAbsenceQR.PrintWeekReason: Boolean;
var
  Count, Year: Word;
  AbsDate: TDateTime;
begin
  with  ReportAbsenceDM do
  begin
    PrintTotalDetailBand := False;
    FReason := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
    FReasonDesc := QueryAbsence.FieldByName('ABSDESC').AsString;
    AbsDate := QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
    ListProcsF.WeekUitDat(AbsDate, Year, FWeek);
    Count := ListProcsF.DayWStartOnFromDate(AbsDate);
    TotalOnDays[Count] := Round(QueryAbsence.FieldByName('SUMMIN').AsFloat);
    PrintTotalOnDays[Count] := True;
    PrintDetailWeekReason(FWeek, Count, FReason, FReasonDesc);
    Result := False;
  end; // with
end; // PrintWeekReason

procedure TReportAbsenceQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FSort = 0 then
    PrintReasonWeek
  else
    PrintBand := PrintWeekReason;
  // CAR : Update progress indicator
  UpdateProgressIndicator;
end;

procedure TReportAbsenceQR.InitializeAmountsPerDaysOfWeek(var AmountsPerDaysOfWeek:
  TAmountsPerDay);
var
  Counter: Integer;
begin
  for Counter := 1 to 7 do
    AmountsPerDaysOfWeek[Counter] := 0;
end;

procedure TReportAbsenceQR.FillAmountsPerDaysOfWeek(IndexMin: Integer;
  AmountsPerDaysOfWeek: TAmountsPerDay; PrintDecide: Boolean);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
  PrintItem: Boolean;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
      begin
        PrintItem := True;
        if PrintDecide then
          PrintItem := PrintTotalOnDays[Index - IndexMin + 1];
        if PrintItem then
          (TempComponent as TQRLabel).Caption :=
            DecodeHrsMin(AmountsPerDaysOfWeek[Index - IndexMin + 1])
        else
          (TempComponent as TQRLabel).Caption := ' '
      end;
    end;
  end;
end;

procedure TReportAbsenceQR.FillDescDaysPerWeek(IndexMin: Integer);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
      begin
        (TempComponent as TQRLabel).Caption :=
          SystemDM.GetDayWCode(Index - IndexMin + 1);
      end;
    end;
  end;
end;

procedure TReportAbsenceQR.ChildBandPlantWeekAbsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //tag 10-16
  FillDescDaysPerWeek(10);
end;

procedure TReportAbsenceQR.ChildBandTotalDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Counter: Integer;
begin
  inherited;
  if PrintTotalDetailBand then
  begin
    if QRParameters.FSort = 0 then
      PrintTotalDetailReason
    else
      PrintTotalDetailWeek;
    for Counter := 1 to 7 do
    begin
      TotalEmpl[Counter] := TotalEmpl[Counter] + TotalOnDetail[Counter];
      TotalDept[Counter] := TotalDept[Counter] + TotalOnDetail[Counter];
      TotalPlant[Counter] := TotalPlant[Counter] + TotalOnDetail[Counter];
      TotalTeam[Counter] := TotalTeam[Counter] + TotalOnDetail[Counter];
    end;
    for Counter := 1 to 7 do
    begin
      TotalOnDetail[Counter] := 0;
      TotalOnDays[Counter] := 0;
      PrintTotalOnDays[Counter] := False;
    end;
    FirstTimeDetail := True;
  end
  else
    PrintBand := False;
end;

procedure TReportAbsenceQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(50, TotalEmpl, False);
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportDetail(
      QRLabel34.Caption,
      IntToStr(FEmpl),
      FEmplDesc,
      '',
      IntToStr(TotalEmpl[1]),
      IntToStr(TotalEmpl[2]),
      IntToStr(TotalEmpl[3]),
      IntToStr(TotalEmpl[4]),
      IntToStr(TotalEmpl[5]),
      IntToStr(TotalEmpl[6]),
      IntToStr(TotalEmpl[7]),
      IntToStr(TotalEmpl[1] +
        TotalEmpl[2] + TotalEmpl[3] +
        TotalEmpl[4] + TotalEmpl[5] +
        TotalEmpl[6] + TotalEmpl[7])
      );
  end;
end;

procedure TReportAbsenceQR.QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdEmpl.ForceNewPage := QRParameters.FPageEmpl;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalEmpl);
  FirstTimeDetail := True;
  FEmpl := ReportAbsenceDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin

    FEmplDesc := ReportAbsenceDM.QueryAbsence.FieldByName('EDESC').AsString;
    ExportClass.AddText(
      QRLabel3.Caption + ExportClass.Sep +
      IntToStr(FEmpl) + ExportClass.Sep + FEmplDesc
      );
  end;
end;

procedure TReportAbsenceQR.QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalDept);
  FirstTimeDetail := True;
  FDept := ReportAbsenceDM.QueryAbsence.FieldByName('DEPARTMENT_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FDeptDesc := ReportAbsenceDM.QueryAbsence.FieldByName('DDESC').AsString;
    ExportClass.AddText(
      QRLabel55.Caption + ExportClass.Sep +
      FDept + ExportClass.Sep + FDeptDesc
      );
  end;
end;

procedure TReportAbsenceQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalPlant);
  FirstTimeDetail := True;
  FPlant := ReportAbsenceDM.QueryAbsence.FieldByName('PLANT_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin

    FPlantDesc := ReportAbsenceDM.QueryAbsence.FieldByName('PDESC').AsString;
    ExportClass.AddText(
      QRLabel54.Caption + ExportClass.Sep +
      FPlant + ExportClass.Sep + FPlantDesc
      );
  end;
end;

procedure TReportAbsenceQR.QRBandFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(60, TotalDept, False);
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportDetail(
      QRLabel21.Caption,
      FDept,
      FDeptDesc,
      '',
      IntToStr(TotalDept[1]),
      IntToStr(TotalDept[2]),
      IntToStr(TotalDept[3]),
      IntToStr(TotalDept[4]),
      IntToStr(TotalDept[5]),
      IntToStr(TotalDept[6]),
      IntToStr(TotalDept[7]),
      IntToStr(TotalDept[1] +
        TotalDept[2] + TotalDept[3] +
        TotalDept[4] + TotalDept[5] +
        TotalDept[6] + TotalDept[7])
      );
  end;
end;

procedure TReportAbsenceQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(70, TotalPlant, False);
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel23.Caption,
      FPlant,
      FPlantDesc,
      '',
      IntToStr(TotalPlant[1]),
      IntToStr(TotalPlant[2]),
      IntToStr(TotalPlant[3]),
      IntToStr(TotalPlant[4]),
      IntToStr(TotalPlant[5]),
      IntToStr(TotalPlant[6]),
      IntToStr(TotalPlant[7]),
      IntToStr(TotalPlant[1] +
        TotalPlant[2] + TotalPlant[3] +
        TotalPlant[4] + TotalPlant[5] +
        TotalPlant[6] + TotalPlant[7])
      );
end;

procedure TReportAbsenceQR.QRLabelTotalEmplPrint(sender: TObject;
  var Value: String);
var
  Count, Total_Empl: Integer;
begin
  inherited;
  Total_Empl := 0;
  for Count := 1 to 7 do
    Total_Empl := Total_Empl + TotalEmpl[Count];
  Value := DecodeHrsMin(Total_Empl);
end;

procedure TReportAbsenceQR.QRLabelTotalDeptPrint(sender: TObject;
  var Value: String);
var
  Count, Total_Dept: Integer;
begin
  inherited;
  Total_Dept := 0;
  for Count := 1 to 7 do
    Total_Dept := Total_Dept + TotalDept[Count];
  Value := DecodeHrsMin(Total_Dept);
end;

procedure TReportAbsenceQR.QRLabelTotalPlantPrint(sender: TObject;
  var Value: String);
var
  Count, Total_Plant: Integer;
begin
  inherited;
  Total_Plant := 0;
  for Count := 1 to 7 do
    Total_Plant := Total_Plant + TotalPlant[Count];
  Value := DecodeHrsMin(Total_Plant);
end;

procedure TReportAbsenceQR.QRDBTextDeptDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportAbsenceDM.QueryAbsence.FieldByName('DDESC').AsString,0, 25);
end;

procedure TReportAbsenceQR.QRDBTextTeamDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportAbsenceDM.QueryAbsence.FieldByName('TDESC').AsString, 0 , 25);
end;

procedure TReportAbsenceQR.QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdTeam.ForceNewPage := QRParameters.FPageTeam;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalTeam);
  FirstTimeDetail := True;
  FTeam := ReportAbsenceDM.QueryAbsence.FieldByName('TEAM_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FTeamDesc := ReportAbsenceDM.QueryAbsence.FieldByName('TDESC').AsString;
    ExportClass.AddText(
      QRLabel35.Caption + ExportClass.Sep +
      FTeam + ExportClass.Sep + FTeamDesc
      );
  end;
end;

procedure TReportAbsenceQR.QRBandFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(90, TotalTeam, False);
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportDetail(
      QRLabel78.Caption,
      FTeam,
      FTeamDesc,
      '',
      IntToStr(TotalTeam[1]),
      IntToStr(TotalTeam[2]),
      IntToStr(TotalTeam[3]),
      IntToStr(TotalTeam[4]),
      IntToStr(TotalTeam[5]),
      IntToStr(TotalTeam[6]),
      IntToStr(TotalTeam[7]),
      IntToStr(TotalTeam[1] +
        TotalTeam[2] + TotalTeam[3] +
        TotalTeam[4] + TotalTeam[5] +
        TotalTeam[6] + TotalTeam[7])
      );
  end;
end;

procedure TReportAbsenceQR.QRLabel86Print(sender: TObject;
  var Value: String);
var
  Count, Total_Team: Integer;
begin
  inherited;
  Total_Team := 0;
  for Count := 1 to 7 do
    Total_Team := Total_Team + TotalTeam[Count];
  Value := DecodeHrsMin(Total_Team);
end;

procedure TReportAbsenceQR.QRDBTextEmplDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportAbsenceDM.QueryAbsence.FieldByName('EDESC').AsString, 0, 25);
end;

procedure TReportAbsenceQR.QRBandSummaryAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:12-12-2002
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
  // MR: Put original text back!
  QRLabelTotalDesc.Caption := FTotalText;
end;

procedure TReportAbsenceQR.QRLabelAmountPrint(sender: TObject;
  var Value: String);
begin
  inherited;
//  if Value
end;

// PIM-414
procedure TReportAbsenceQR.QRBandFooterWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Counter: Integer;  
begin
  inherited;
  PrintBand := QRParameters.FSort = 1;
  if PrintBand then
  begin
    PrintTotalDetailWeek;
    for Counter := 1 to 7 do
    begin
      TotalEmpl[Counter] := TotalEmpl[Counter] + TotalOnDetail[Counter];
      TotalDept[Counter] := TotalDept[Counter] + TotalOnDetail[Counter];
      TotalPlant[Counter] := TotalPlant[Counter] + TotalOnDetail[Counter];
      TotalTeam[Counter] := TotalTeam[Counter] + TotalOnDetail[Counter];
    end;
    for Counter := 1 to 7 do
    begin
      TotalOnDetail[Counter] := 0;
      TotalOnDays[Counter] := 0;
      PrintTotalOnDays[Counter] := False;
    end;
  end;
end;

// PIM-414
procedure TReportAbsenceQR.QRGroupHDWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
begin
  inherited;
  PrintBand := False;
  if QRParameters.FSort = 1 then
  begin
    FirstTimeDetail := True;
  end;
  for Count := 1 to 7 do
  begin
    TotalOnDays[Count] := 0;
    PrintTotalOnDays[Count] := False;
  end;
end;

// PIM-414
procedure TReportAbsenceQR.QRBandFooterAbsenceReasonCodeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FSort = 1;
  if QRParameters.FSort = 1 then
    FirstTimeDetail := False;
end;

// PIM-414
procedure TReportAbsenceQR.QRGroupHDAbsenceReasonCodeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
begin
  inherited;
  PrintBand := False;
  with ReportAbsenceDM do
  begin
    QRLblAbsenceReason.Caption :=
      QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString + ' ' +
      QueryAbsence.FieldByName('ABSDESC').AsString;
  end;
  for Count := 1 to 7 do
  begin
    TotalOnDays[Count] := 0;
    PrintTotalOnDays[Count] := False;
  end;
end;

// PIM-414
procedure TReportAbsenceQR.QRBandFooterAbsenceReasonCodeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Counter, TotalDetail: Integer;
begin
  inherited;
  TotalDetail := 0;
  for Counter := 1 to 7 do
  begin
    TotalDetail := TotalDetail + TotalOnDays[Counter];
  end;

  // MR:12-12-2002
  with ReportAbsenceDM do
  begin
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        '',
        QRLblWeekNr.Caption, // Week
        QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString,  // Code
        QueryAbsence.FieldByName('ABSDESC').AsString,  // Description
        IntToStr(TotalOnDays[1]),
        IntToStr(TotalOnDays[2]),
        IntToStr(TotalOnDays[3]),
        IntToStr(TotalOnDays[4]),
        IntToStr(TotalOnDays[5]),
        IntToStr(TotalOnDays[6]),
        IntToStr(TotalOnDays[7]),
        IntToStr(TotalDetail)
        );
  end;
end;

end.
