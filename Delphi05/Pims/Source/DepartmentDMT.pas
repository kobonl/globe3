(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:10-FEB-2011 RV086.1. SC-50014392.
    - Plan in other plants, related issues:
      - When teams-per-users-filtering is used, you still see
        plants/teams/department/workspots not belonging to the
        teams-per-user-selection.
    MRA:27-FEB-2015 TD-26114
    - Cannot change name in Pims
    - TableDetail: Changed 'UpdateMode' to 'upWhereKeyOnly'
*)
unit DepartmentDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TDepartmentDM = class(TGridBaseDM)
    TableBusiness: TTable;
    DataSourceBusiness: TDataSource;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableMasterFAX: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterINSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterINSCAN_MARGIN_LATE: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterOUTSCAN_MARGIN_LATE: TIntegerField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailBUSINESSUNIT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailDIRECT_HOUR_YN: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailPLAN_ON_WORKSPOT_YN: TStringField;
    TableDetailREMARK: TStringField;
    TableDetailBUSINESSLU: TStringField;
    TableBusinessBUSINESSUNIT_CODE: TStringField;
    TableBusinessDESCRIPTION: TStringField;
    TableBusinessPLANT_CODE: TStringField;
    TableBusinessCREATIONDATE: TDateTimeField;
    TableBusinessBONUS_FACTOR: TFloatField;
    TableBusinessNORM_ILL_VS_DIRECT_HOURS: TFloatField;
    TableBusinessMUTATIONDATE: TDateTimeField;
    TableBusinessMUTATOR: TStringField;
    TableBusinessNORM_ILL_VS_INDIRECT_HOURS: TFloatField;
    TableBusinessNORM_ILL_VS_TOTAL_HOURS: TFloatField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableBusinessFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DepartmentDM: TDepartmentDM;

implementation

uses
  SystemDMT, UPimsConst;

{$R *.DFM}

procedure TDepartmentDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not TableBusiness.IsEmpty then
  begin
    TableBusiness.First;
    TableDetail.FieldByName('BUSINESSUNIT_CODE').Value :=
      TableBusiness.FieldByName('BUSINESSUNIT_CODE').Value;
  end;
end;

procedure TDepartmentDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableMaster);
  // RV086.1.
  SystemDM.PlantTeamFilterEnable(TableDetail);
  SystemDM.PlantTeamFilterEnable(TableBusiness);
end;

// RV050.8.
procedure TDepartmentDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// RV050.8.
procedure TDepartmentDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.1.
  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

procedure TDepartmentDM.TableBusinessFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.1.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
