inherited BusinessUnitF: TBusinessUnitF
  Left = 240
  Top = 129
  Width = 677
  Height = 502
  Caption = 'Business unit'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 661
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 659
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 659
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 299
    Width = 661
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 344
      Height = 163
      Align = alLeft
      Caption = 'Business unit'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 25
        Height = 13
        Caption = 'Code'
      end
      object Label3: TLabel
        Left = 8
        Top = 55
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object Label2: TLabel
        Left = 8
        Top = 89
        Width = 27
        Height = 13
        Caption = 'Plant '
      end
      object Label4: TLabel
        Left = 8
        Top = 124
        Width = 61
        Height = 13
        Caption = 'Bonus factor'
      end
      object DBEditBusinessUnit: TDBEdit
        Tag = 1
        Left = 83
        Top = 20
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'BUSINESSUNIT_CODE'
        DataSource = BusinessUnitDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditName: TDBEdit
        Tag = 1
        Left = 83
        Top = 53
        Width = 230
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = BusinessUnitDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditBonusFactor: TDBEdit
        Left = 83
        Top = 119
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'BONUS_FACTOR'
        DataSource = BusinessUnitDM.DataSourceDetail
        MaxLength = 6
        ParentCtl3D = False
        TabOrder = 3
      end
      object dxDBLookupEdit1: TdxDBLookupEdit
        Tag = 1
        Left = 83
        Top = 86
        Width = 230
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'PLANTLU'
        DataSource = BusinessUnitDM.DataSourceDetail
        DropDownRows = 4
        ListFieldName = 'DESCRIPTION;PLANT_CODE'
      end
    end
    object GroupBox3: TGroupBox
      Left = 345
      Top = 1
      Width = 315
      Height = 163
      Align = alClient
      Caption = 'Norm illness'
      TabOrder = 1
      object Label8: TLabel
        Left = 16
        Top = 24
        Width = 58
        Height = 13
        Caption = 'Direct hours'
      end
      object Label9: TLabel
        Left = 16
        Top = 54
        Width = 67
        Height = 13
        Caption = 'Indirect hours'
      end
      object Label10: TLabel
        Left = 16
        Top = 85
        Width = 54
        Height = 13
        Caption = 'Total hours'
      end
      object Label14: TLabel
        Left = 152
        Top = 26
        Width = 11
        Height = 13
        Caption = '%'
      end
      object Label5: TLabel
        Left = 152
        Top = 56
        Width = 11
        Height = 13
        Caption = '%'
      end
      object Label6: TLabel
        Left = 152
        Top = 82
        Width = 11
        Height = 13
        Caption = '%'
      end
      object DBEditDirH: TDBEdit
        Left = 91
        Top = 23
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'NORM_ILL_VS_DIRECT_HOURS'
        DataSource = BusinessUnitDM.DataSourceDetail
        MaxLength = 6
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditIndH: TDBEdit
        Left = 91
        Top = 51
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'NORM_ILL_VS_INDIRECT_HOURS'
        DataSource = BusinessUnitDM.DataSourceDetail
        MaxLength = 6
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditTotH: TDBEdit
        Left = 91
        Top = 79
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'NORM_ILL_VS_TOTAL_HOURS'
        DataSource = BusinessUnitDM.DataSourceDetail
        MaxLength = 6
        ParentCtl3D = False
        TabOrder = 2
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 661
    Height = 144
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 140
      Width = 659
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 659
      Height = 139
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Business unit'
        end>
      KeyField = 'BUSINESSUNIT_CODE'
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        Width = 69
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BUSINESSUNIT_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridLookupColumn
        Caption = 'Plant'
        Width = 398
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
        ListFieldName = 'DESCRIPTION;PLANT_CODE'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Bonus factor'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        Width = 60
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_FACTOR'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Direct hours'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Indirect hours'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Total hours'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Top = 112
  end
end
