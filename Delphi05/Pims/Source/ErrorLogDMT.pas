(*
  MRA:17-NOV-2014 20014826
  - Log in DB
*)
unit ErrorLogDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TDataFilter = record
    ComputerName: String;
    StartDate, EndDate: TDateTime;
    Priority: Integer;
  end;

type
  TErrorLogDM = class(TGridBaseDM)
    TableMasterCOMPUTER_NAME: TStringField;
    qryABSLOG: TQuery;
    qryABSLOGABSLOG_ID: TIntegerField;
    qryABSLOGCOMPUTER_NAME: TStringField;
    qryABSLOGABSLOG_TIMESTAMP: TDateTimeField;
    qryABSLOGLOGSOURCE: TStringField;
    qryABSLOGPRIORITY: TIntegerField;
    qryABSLOGSYSTEMUSER: TStringField;
    qryABSLOGLOGMESSAGE: TMemoField;
    qryABSLOGCALCMESSAGE: TStringField;
    procedure qryABSLOGCalcFields(DataSet: TDataSet);
  private
    FScanFilter: TDataFilter;
    procedure SetFilter(const Value: TDataFilter);
    { Private declarations }
  public
    { Public declarations }
    property ScanFilter: TDataFilter read FScanFilter write SetFilter;
  end;

var
  ErrorLogDM: TErrorLogDM;

implementation

{$R *.DFM}

{ TErrorLogDM }

procedure TErrorLogDM.SetFilter(const Value: TDataFilter);
begin
  FScanFilter := Value;
  with qryABSLOG do
  begin
    Close;
    ParamByName('COMPUTER_NAME').AsString := FScanFilter.ComputerName;
    ParamByName('PRIORITY').AsInteger := FScanFilter.Priority;
    ParamByName('DATEFROM').AsDateTime := FScanFilter.StartDate;
    ParamByName('DATETO').AsDateTime := FScanFilter.EndDate;
    Open;
  end;
end;

procedure TErrorLogDM.qryABSLOGCalcFields(DataSet: TDataSet);
begin
  inherited;
  try
    qryABSLOGCALCMESSAGE.Value := Copy(qryABSLOGLOGMESSAGE.Value, 1, 255);
  except
    qryABSLOGCALCMESSAGE.Value := '';
  end;
end;

end.
