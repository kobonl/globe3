(*
  SO:08-JUL-2010 RV067.4. 550497
  MRA:24-AUG-2010. RV067.MRA.8. 550497.
  - Changed hardcoded strings to UPimsMessageRes.
  MRA:19-FEB-2018 Glob3-72.
  - Add Export_code to AbsenceReason
*)

unit AbsReasonPerCountryFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, dxEditor, dxEdLib, dxDBELib,
  dxDBTLCl, dxGrClms, dxExEdtr, dxDBEdtr;

type
  TAbsReasonPerCountryF = class(TGridBaseF)
    Label1: TLabel;
    Label3: TLabel;
    DBEditCode: TdxDBEdit;
    DBCheckBoxPayed: TDBCheckBox;
    DBEditDescription: TdxDBEdit;
    Label4: TLabel;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnAbsenceType: TdxDBGridLookupColumn;
    dxDetailGridColumnHourType: TdxDBGridLookupColumn;
    dxDetailGridColumnPayed: TdxDBGridCheckColumn;
    DBCheckBoxOverruleWithIllness: TDBCheckBox;
    dxDetailGridColumnOverruleWithIllness: TdxDBGridCheckColumn;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnExportType: TdxDBGridColumn;
    DBEditAbsenceType: TdxDBEdit;
    Label2: TLabel;
    DBEditHourType: TdxDBEdit;
    lblExportCode: TLabel;
    dxDBEditExportCode: TdxDBEdit;
    dxDetailGridColumnExportCode: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
    procedure dxDetailGridEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure SelectRecords;
    { Private declarations }
  public
    { Public declarations }
  end;

function AbsReasonPerCountryF: TAbsReasonPerCountryF;

implementation

uses AbsReasonPerCountryDMT, MultipleSelectFRM, UPimsConst,
  UPimsMessageRes, SystemDMT;

{$R *.DFM}

var
  AbsReasonPerCountryF_HND: TAbsReasonPerCountryF;

// MR:09-02-2007 Order 550441.
// Delete has been disabled, to prevent problems with foreign keys.
  
function AbsReasonPerCountryF: TAbsReasonPerCountryF;
begin
  if (AbsReasonPerCountryF_HND = nil) then
  begin
    AbsReasonPerCountryF_HND := TAbsReasonPerCountryF.Create(Application);
  end;
  Result := AbsReasonPerCountryF_HND;
end;

procedure TAbsReasonPerCountryF.FormDestroy(Sender: TObject);
begin
  inherited;
  AbsReasonPerCountryF_HND := nil;
end;

procedure TAbsReasonPerCountryF.FormCreate(Sender: TObject);
begin
  AbsReasonPerCountryDM := CreateFormDM(TAbsReasonPerCountryDM);
  if (dxMasterGrid.DataSource = Nil) then
    dxMasterGrid.DataSource := AbsReasonPerCountryDM.DataSourceMaster;
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := AbsReasonPerCountryDM.DataSourceDetail;
  inherited;
  dxBarButtonEditMode.Visible := ivNever;
  // GLOB3-72
  lblExportCode.Visible := SystemDM.IsNTS;
  dxDBEditExportCode.Visible := SystemDM.IsNTS;
  dxDetailGridColumnExportCode.Visible := SystemDM.IsNTS;
  dxDetailGridColumnExportCode.DisableCustomizing := not SystemDM.IsNTS;
end;

procedure TAbsReasonPerCountryF.SelectRecords;
var
  frm: TDialogMultipleSelectF;
  CountryId: String;
begin
  frm := TDialogMultipleSelectF.Create(Application);
  try
    CountryId := AbsReasonPerCountryDM.TableMaster.FieldByName('COUNTRY_ID').AsString;
    frm.Init(SPimsAbsenceReasons,
      'select code || '' | '' || description from country t where t.country_id = ' + CountryId,
      //Available,
      'select t.absencereason_id id, t.absencereason_code code, t.description from absencereason t ' +
      'where t.absencereason_id not in (select ac.absencereason_id from absencereasonpercountry ac where ac.country_id =  ' + CountryId + ') ' +
      'order by t.absencereason_code',
      //Selected,
      'select t.absencereason_id id, t.absencereason_code code, t.description from absencereason t ' +
      'where t.absencereason_id in (select ac.absencereason_id from absencereasonpercountry ac where ac.country_id =  ' + CountryId + ') ' +
      'order by t.absencereason_code',
      //Insert
      'insert into absencereasonpercountry(country_id, creationdate, mutationdate, mutator, absencereason_id) values ('
      + CountryId + ', sysdate, sysdate, ''' + SystemDM.CurrentProgramUser + ''', %s)',
      //Delete
      'delete from absencereasonpercountry ac where ac.country_id = ' + CountryId +
      ' and absencereason_id = %s',
      //Exists
      'select NVL(count(*), 0) from absencereasonpercountry ac where ac.country_id = ' + CountryId +
      ' and absencereason_id = %s'
    );
    if frm.ShowModal = mrOk then
      AbsReasonPerCountryDM.TableDetail.Refresh;
  finally
    frm.Free();
  end;
end;

procedure TAbsReasonPerCountryF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TAbsReasonPerCountryF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TAbsReasonPerCountryF.dxDetailGridEnter(Sender: TObject);
begin
  inherited dxGridEnter(Sender);
  dxBarButtonEditMode.Visible := ivNever;
end;

procedure TAbsReasonPerCountryF.FormShow(Sender: TObject);
begin
  inherited;
  dxMasterGrid.SetFocus;
end;

end.
