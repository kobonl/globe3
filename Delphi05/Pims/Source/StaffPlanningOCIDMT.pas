(*
   MRA:4-DEC-2009. RV047.3.
   - Planning gives wrong counts at bottom because no check is done on
     the Plant. Only Workspot + Department are used to see if employees
     are planned. This goes wrong when there are workspots + departments
     with the same codes as the plant that was choosen.
  MRA:11-NOV-2010 RV079.3. Optimise.
  - Use 'LogChanges := False' for all Clientdatasets to improve performance.
  MRA:9-MAR-2018 GLOB3-84
  - Dialog Staff Planning and Totals
  MRA:26-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - When totals (D1 and D2) should not be shown (when only 1 level must be
    shown), do not leave them out of the data read because it uses this to
    handle colors and check for over-planning.
*)
unit StaffPlanningOCIDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, UPimsConst, StaffPlanningUnit, Provider,
  DBClient;


type

  TStaffPlanningOCIDM = class(TGridBaseDM)
    QueryWork: TQuery;
    DataSourceOCI: TDataSource;
    StoredProcInsertOCI: TStoredProc;
    StoredProcUpdateOCI: TStoredProc;
    StoredProcUpdateOCPL: TStoredProc;
    QuerySTOC: TQuery;
    ClientDataSetSTOC: TClientDataSet;
    DataSetProviderSTOC: TDataSetProvider;
    TableOCI: TTable;
    qryOCIPlanning: TQuery;
    QueryPlant: TQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }

    FTBOCIA, FTBOCIB, FTBOCIC, FTBPlannedA, FTBPLANNEDB,
    FTBPLANNEDC: TTBColor;
    FShowLevel: Boolean;
    FShowLevelC: Boolean;
    FStdOccA: Boolean;
    FStdOccB: Boolean;
    FStdOccC: Boolean;
    procedure FillTableOCI;
    procedure GetTBOCI(WK, Dept: String);
    procedure FillOCP_STO(Plant : String; DateEMA: TDateTime;
      Shift, Day_Planning: Integer);
    procedure InsertIntoOCIPivot;
    procedure PlantSetFilter;
 //   procedure OpenQuerySTOC;
  public
    { Public declarations }
// occupation indicator
    procedure OpenQuerySTOC(Plant: String; Shift, Day: Integer);
    procedure CountPlannedEmpl(WK, DEPT: String);
    procedure SetFieldsOCI(WK, Level: String;  var X, PrevA, NewD2,
      NewX: Integer);
    function GetPlannedEMP(WKCode,  Level: String): Integer;
    procedure SaveOCP;
    procedure UpdateLineOCI(ColumnStr, ValStr:String;
      PlannedEMP: Integer);
    procedure UpdateOCI;
    procedure DataCreate;
    procedure PlantGetSTDOCCFlags(APlant: String);
    function LevelABCCount: Integer;
    property ShowLevel: Boolean read FShowLevel write FShowLevel;
    property ShowLevelC: Boolean read FShowLevelC write FShowLevelC;
    property StdOccA: Boolean read FStdOccA write FStdOccA;
    property StdOccB: Boolean read FStdOccB write FStdOccB;
    property StdOccC: Boolean read FStdOccC write FStdOccC;
  end;

var
  StaffPlanningOCIDM: TStaffPlanningOCIDM;

implementation

{$R *.DFM}
uses SystemDMT, UPimsMessageRes, ListProcsFRM, StaffPlanningEMPDMT,
  StaffPlanningFRM;

 //CAR 28-7-2003- changes for performance
procedure TStaffPlanningOCIDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FListOCIPlanned.Free;

  QuerySTOC.Close;
  QuerySTOC.UnPrepare;
end;

procedure TStaffPlanningOCIDM.InsertIntoOCIPivot;
begin
   //CAR : 5-6-2004
  StoredProcInsertOCI.ParamByName('PIVOTCOMPUTERNAME').asString :=
    SystemDM.CurrentCOMPUTERName;
  StoredProcInsertOCI.Prepare;
  StoredProcInsertOCI.ExecProc;
end;

procedure TStaffPlanningOCIDM.OpenQuerySTOC(
  Plant: String; Shift, Day: Integer);
begin
  ClientDataSetSTOC.Close;
  QuerySTOC.ParamByName('PLANT_CODE').AsString := Plant;
  QuerySTOC.ParamByName('SHIFT_NUMBER').AsInteger := Shift;
  QuerySTOC.ParamByName('DAY_OF_WEEK').AsInteger := Day;
  ClientDataSetSTOC.Open;
  try
    ClientDataSetSTOC.LogChanges := False; // RV079.3.
  except
  end;
end;

procedure TStaffPlanningOCIDM.DataCreate;

begin
  inherited;
  PlantSetFilter;  
  InsertIntoOCIPivot;
//  OpenQuerySTOC;
  FillTableOCI;
end;

procedure TStaffPlanningOCIDM.GetTBOCI(WK, Dept: String);
var
  i: Integer;
begin
  for i:= 1 to MAX_TBS do
  begin
    FTBOCIA[i]:= 0;
    FTBOCIB[i]:= 0;
    FTBOCIC[i]:= 0;
  end;
  if ClientDataSetSTOC.FindKey([Dept, WK]) then
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      if Self.StdOccA then
        FTBOCIA[i] := ClientDataSetSTOC.FieldByName('OCC_A_TIMEBLOCK_' +
          IntToStr(i)).AsInteger;
      if Self.StdOccB then
        FTBOCIB[i] :=  ClientDataSetSTOC.FieldByName('OCC_B_TIMEBLOCK_' +
          IntToStr(i)).AsInteger;
      if Self.StdOccC then
        FTBOCIC[i] :=  ClientDataSetSTOC.FieldByName('OCC_C_TIMEBLOCK_' +
          IntToStr(i)).AsInteger;
    end;
end; // GetTBOCI

procedure TStaffPlanningOCIDM.CountPlannedEmpl(WK, DEPT: String);
var
  i: Integer;
  LevelStr, SCHTB: String;
  Empl : Integer;
begin
  for i:= 1 to MAX_TBS do
  begin
    FTBPlannedA[i]:= 0;
    FTBPlannedB[i]:= 0;
    FTBPlannedC[i]:= 0;
  end;
  // RV047.3. Add the plant!
  StaffPlanningEMPDM.ClientDataSetEMPLN.Filtered := False;
  StaffPlanningEMPDM.ClientDataSetEMPLN.Filter :=
    'SHIFT_NUMBER = ' + IntToStr(FShift) +
    ' AND PLANT_CODE = ' +  '''' + FPlant + '''' +
    ' AND WORKSPOT_CODE = ' + '''' + WK + '''' +
    ' AND DEPARTMENT_CODE = ' + '''' + Dept + '''';
   StaffPlanningEMPDM.ClientDataSetEMPLN.Filtered := True;
  StaffPlanningEMPDM.ClientDataSetEMPLN.First;
  while not StaffPlanningEMPDM.ClientDataSetEMPLN.Eof do
  begin
    Empl :=
      StaffPlanningEMPDM.ClientDataSetEMPLN.
        FieldByName('EMPLOYEE_NUMBER').AsInteger;
    LevelStr := 'C';
    if StaffPlanningEMPDM.ClientDataSetWKPerEmpl.
      FindKey([Empl, FPlant, DEPT, WK]) then
        LevelStr :=
          StaffPlanningEMPDM.ClientDataSetWKPerEmpl.
            FieldByName('EMPLOYEE_LEVEL').AsString;
    for i := 1 to SystemDM.MaxTimeblocks do
    begin
      SCHTB := StaffPlanningEMPDM.ClientDataSetEMPLN.
        FieldByName('SCHEDULED_TIMEBLOCK_' +
          IntToStr(i)).AsString;
      if (SchTB = 'A') or (SchTB = 'B') or (SchTB = 'C') then
        case LevelStr[1] of
          'A': Inc(FTBPlannedA[i]);
          'B': Inc(FTBPlannedB[i]);
          'C': Inc(FTBPlannedC[i]);
        end;
    end;
    StaffPlanningEMPDM.ClientDataSetEMPLN.Next;
  end;
end; // CountPlannedEmpl

procedure TStaffPlanningOCIDM.FillTableOCI;
var
  j, RecCount, X: Integer;
  UpdateStr, WK, Dept: string;
  UpdateA1Str, UpdateB1Str, UpdateC1Str: string;
  UpdateA2Str, UpdateB2Str, UpdateC2Str: String;
  UpdateA3Str, UpdateB3Str: String;
  TmpA1StrWK, TmpB1StrWK, TmpC1StrWK, TmpA2StrWK,
  TmpB2StrWK, TmpC2StrWK, TmpA3StrWK, TmpB3StrWK, TmpC3StrWK: String;
  LevelFilter: String;
  FilterABC: String;
begin
  FListOCIPlanned.Clear;
  UpdateStr := 'UPDATE ' +  TMPSTAFFPLANNINGOCI + ' SET ';

  UpdateA1Str := ''; UpdateA2Str := ''; UpdateA3Str := '';
  UpdateB1Str := ''; UpdateB2Str := ''; UpdateB3Str := '';
  UpdateC1Str := ''; UpdateC2Str := '';

  for RecCount := 0 to FWKCount do
  begin
    GetWKDeptCode(FWKList.Strings[RecCount],RecCount, WK, DEPT);
    GetTBOCI(WK, Dept);

    TmpA1StrWK := '';
    TmpB1StrWK := '';
    TmpC1StrWK := '';
    for j := 1 to SystemDM.MaxTimeblocks do
    begin
      if Self.StdOccA then
        TmpA1StrWK := TmpA1StrWK + FillLeadSpace(IntToStr(FTBOCIA[j]), 6);
      if Self.StdOccB then
        TmpB1StrWK := TmpB1StrWK + FillLeadSpace(IntToStr(FTBOCIB[j]), 6);
      if Self.StdOccC then
        TmpC1StrWK := TmpC1StrWK + FillLeadSpace(IntToStr(FTBOCIC[j]), 6);
    end;//FOR J 1..4
    if (Trim(TmpA1StrWK) <> '') then
      UpdateA1Str := UpdateA1Str +  'WK' + IntToStr(RecCount)  +
        ' = ''' + TmpA1StrWK + ''', ';
    if (Trim(TmpB1StrWK) <> '') then
      UpdateB1Str := UpdateB1Str +  'WK' + IntToStr(RecCount)   +
         ' = ''' + TmpB1StrWK + ''', ';
    if (Trim(TmpC1StrWK) <> '') then
      UpdateC1Str := UpdateC1Str +  'WK' + IntToStr(RecCount)   +
          ' = ''' + TmpC1StrWK + ''', ';
    CountPlannedEmpl(WK, Dept);
    TmpA2StrWK := '';
    TmpB2StrWK := '';
    TmpC2StrWK := '';
    TmpA3StrWK := '';
    TmpB3StrWK := '';
    TmpC3StrWK := '';
    for j:= 1 to SystemDM.MaxTimeblocks do
    begin
      FListOCIPlanned.Add('WK'+ IntToStr(RecCount) + IntToStr(j) + 'A');
      TmpA2StrWK := TmpA2StrWK + FillLeadSpace(IntToStr(FTBPlannedA[j]), 6);
      FListOCIPlanned.Add(IntToStr(FTBPlannedA[j]));

      FListOCIPlanned.Add('WK'+ IntToStr(RecCount) + IntToStr(j)  + 'B');
      TmpB2StrWK := TmpB2StrWK + FillLeadSpace(IntToStr(FTBPlannedB[j]), 6);
      FListOCIPlanned.Add(IntToStr(FTBPlannedB[j]));

      FListOCIPlanned.Add('WK'+ IntToStr(RecCount) + IntToStr(j) + 'C');
      TmpC2StrWK := TmpC2StrWK + FillLeadSpace(IntToStr(FTBPlannedC[j]), 6);
      FListOCIPlanned.Add(IntToStr(FTBPlannedC[j]));
      FListOCIPlanned.Add('WK'+ IntToStr(RecCount) + IntToStr(j) + 'D');

      X := 0;
      if Self.StdOccA then
        X := X + FTBOCIA[j];
      if Self.StdOccB then
        X := X + FTBOCIB[j];
      if Self.StdOccC then
        X := X + FTBOCIC[j];
      TmpA3StrWK := TmpA3StrWK + FillLeadSpace(IntToStr(X),6);

      X := 0;
      if Self.StdOccA then
        X := X + FTBPlannedA[j];
      if Self.StdOccB then
        X := X + FTBPlannedB[j];
      if Self.StdOccC then
        X := X + FTBPlannedC[j];
      TmpB3StrWK := TmpB3StrWK + FillLeadSpace(IntToStr(X),6);
      FListOCIPlanned.Add(IntToStr(X));
    end;
    if (Trim(TmpA2StrWK) <> '') then
      UpdateA2Str := UpdateA2Str + 'WK'+ IntToStr(RecCount) +
        ' = ''' +  TmpA2StrWK + ''', ';
    if (Trim(TmpB2StrWK) <> '') then
      UpdateB2Str := UpdateB2Str + 'WK'+ IntToStr(RecCount) +
        ' = ''' +  TmpB2StrWK + ''', ';
    if (Trim(TmpC2StrWK) <> '') then
      UpdateC2Str := UpdateC2Str + 'WK'+ IntToStr(RecCount) +
        ' = ''' +  TmpC2StrWK + ''', ';
    UpdateA3Str := UpdateA3Str + 'WK'+ IntToStr(RecCount) +
      ' = ''' +  TmpA3StrWK + ''', ';
    UpdateB3Str := UpdateB3Str + 'WK'+ IntToStr(RecCount) +
      ' = ''' + TmpB3StrWK + ''', ';
  end;

  StaffPlanningEMPDM.ClientDataSetEMPLN.Filter := '';
  //CAR 6-5-2004
  //Total line    - occupation & planning
  if UpdateA3Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateA3Str,
      0, Length(UpdateA3Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'D1' + '''');
  if UpdateB3Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateB3Str,
      0, Length(UpdateB3Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'D2' + '''');
  if (not ShowLevel) then
  begin
    TableOCI.Filter := '(PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentCOMPUTERName +
      '''' + ' ) AND (OCI_LEVEL >= ''D1'')';
      TableOCI.Refresh;
      Exit;
  end;

  //first line  - level A  occupation
  if UpdateA1Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateA1Str,
      0, Length(UpdateA1Str) -2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'A1' + '''');
 //  level B occupation
  if UpdateB1Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateB1Str,
      0, Length(UpdateB1Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'B1' + '''');
  //  level C occupation
  if UpdateC1Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateC1Str,
      0, Length(UpdateC1Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
       SystemDM.CurrentComputerName + '''' +
       ' AND OCI_LEVEL = ''' + 'C1' + '''');

  //   level A planned
  if UpdateA2Str <> ''  then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateA2Str,
      0, Length(UpdateA2Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'A2' + '''');
  //   level B planned
  if UpdateB2Str  <> ''  then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateB2Str,
      0, Length(UpdateB2Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'B2' + '''');
  //   level C planned
  if UpdateC2Str <> '' then
    ExecuteSql(QueryWork, UpdateStr + Copy(UpdateC2Str,
      0, Length(UpdateC2Str)-2) +
      ' WHERE PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + '''' +
      ' AND OCI_LEVEL = ''' + 'C2' + '''');
  if Self.StdOccA and Self.StdOccB and Self.StdOccC then
  begin
    TableOCI.Filter := 'PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentCOMPUTERName + '''';
    TableOCI.Refresh;
  end
  else
  begin
    LevelFilter := '';
    FilterABC := '';
    // If there are 2 levels to show or more, then also show totals, otherwise not
//    if Self.LevelABCCount > 1 then
      FilterABC := '(OCI_LEVEL >= ''D1'')'; // Totals
    if Self.StdOccA or Self.StdOccB or Self.StdOccC then
    begin
      if Self.StdOccA then
      begin
        if FilterABC <> '' then
          FilterABC := FilterABC + ' OR ';
        FilterABC := FilterABC + ' (OCI_LEVEL = ''A1'' OR OCI_LEVEL = ''A2'')';
      end;
      if Self.StdOccB then
      begin
        if FilterABC <> '' then
          FilterABC := FilterABC + ' OR ';
        FilterABC := FilterABC + ' (OCI_LEVEL = ''B1'' OR OCI_LEVEL = ''B2'')';
      end;
      if Self.StdOccC then
      begin
        if FilterABC <> '' then
          FilterABC := FilterABC + ' OR ';
        FilterABC := FilterABC + ' (OCI_LEVEL = ''C1'' OR OCI_LEVEL = ''C2'')';
      end;
    end;
    LevelFilter := ' AND ' + '(' + FilterABC + ')';
    TableOCI.Filter := '(PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentCOMPUTERName + '''' + ' ) ' +
      LevelFilter;
    TableOCI.Refresh;
  end; // if
end; // FillTableOCI

procedure TStaffPlanningOCIDM.FillOCP_STO(Plant: String;
  DateEMA: TDateTime; Shift, Day_Planning: Integer);
var
  Day: Integer;
begin
  if Day_Planning <> 0 then
    Exit;
  Day := ListProcsF.DayWStartOnFromDate(DateEMA);
  StoredProcUpdateOCPL.ParamByName('PLANT').AsString := Plant;
  StoredProcUpdateOCPL.ParamByName('DATESELECTION').AsDateTime := DateEMA;
  StoredProcUpdateOCPL.ParamByName('DAYSELECTION').AsInteger := Day;
  StoredProcUpdateOCPL.ParamByName('SHIFT').AsInteger := SHIFT;
  StoredProcUpdateOCPL.ParamByName('FDATE').AsDateTime := FCurrentDate;
  StoredProcUpdateOCPL.ParamByName('MUT').AsString :=
    SystemDM.CurrentProgramUser;
  StoredProcUpdateOCPL.ExecProc;
end; // FillOCP_STO

function TStaffPlanningOCIDM.GetPlannedEMP(WKCode,  Level: String): Integer;
var
  Index: Integer;
begin
  Result := 0;
  index := FListOCIPLANNED.IndexOf(WKCODE + LEVEL);
  if index >= 0 then
    Result := StrToInt(FListOCIPlanned.Strings[Index + 1]);
end; // GetPlannedEMP

procedure TStaffPlanningOCIDM.SetFieldsOCI(WK, Level: String; var X, PrevA,
  NewD2, NewX: Integer);
var
  PrevX, PrevD2 : Integer;
  CurrD1, CurrD2: Integer;
  i, IndexCol: Integer;
  NewStr, OldStr, ValStr: String;
  function WKField: String;
  begin
    Result := Copy(WK, 1, Length(WK)-1); // GLOB3-60: Last pos. is column
  end; // WKField
  function WKIndexColumn: Integer;
  begin
    Result := Hex2Int{StrToInt}(Copy(WK, Length(WK) , 1)); // GLOB3-60 Last pos. is column and in Hex
  end; // WKIndexColumn
begin
//Car 6-5-2004
// GLOB3-60 WK: Last 2 positions is now column, instead of last 1 position!
  if ShowLevel then
  begin
    TableOCI.FindKey([LEVEL + '2']);
    IndexCol := WKIndexColumn;
    ValStr := Trim(Copy(TableOCI.
      FieldByName(WKField).AsString, 6 * IndexCol - 5, 6));
    if (ValStr <> '') then
      PrevX := StrToInt(ValStr)
    else
      PrevX := 0;

    NewX := PrevX + X;
    if NewX < 0 then
      NewX := 0;
    if (NewX <> PrevX) then
    begin
      TableOCI.Edit;
      NewStr := FillLeadSpace(IntToStr(NewX), 6);
      OldStr :=  TableOCI.FieldByName(WKField).AsString;
      for i := 1 to 6 do
        OldStr[6 * IndexCol - 6 + i ] := NewStr[i];
      TableOCI.FieldByName(WKField).AsString := OldStr;
      TableOCI.Post;
    end;
  end;
  // MR:03-05-2004 Not needed anymore
//  NewC := X - (2 * PrevA);
//  if (NewC < 0) then
//    NewC := 0;
  // GLOB3-60 Show totals or not:
  // If there are 2 levels to show or more, then also show totals, otherwise not
//  if Self.LevelABCCount > 1 then
  begin
    IndexCol := WKIndexColumn;
    TableOCI.FindKey(['D2']);
    ValStr := Trim(Copy(TableOCI.FieldByName(WKField).AsString,
      6*IndexCol - 5, 6));
    if (ValStr <> '') then
      PrevD2 :=  StrToInt(ValStr)
    else
      PrevD2 := 0;
   //  CAR 6-4-2004
    NewD2 := PrevD2 + X;
    if (PrevD2 <> NewD2) then
    begin
      TableOCI.Edit;
      NewStr := FillLeadSpace(IntToStr(NewD2), 6);
      OldStr :=  TableOCI.FieldByName(WKField).AsString;
      for i := 1 to 6 do
        OldStr[6 * IndexCol - 6 + i ] := NewStr[i];
      TableOCI.FieldByName(WKField).AsString := OldStr;

      TableOCI.Post;
    end;
    // MR:03-05-2004 Only give message if planned-total is bigger
    // than 200 procent of occupation-total
    TableOCI.FindKey(['D1']); // Occupation-total
    ValStr := Trim(Copy(TableOCI.
      FieldByName(WKField).AsString, 6*IndexCol - 5, 6));
    if (ValStr <> '') then
      CurrD1 := StrToInt(ValStr)
    else
      CurrD1 := 0;
    TableOCI.FindKey(['D2']); // Planned-total
    ValStr := Trim(Copy(TableOCI.
      FieldByName(WKField).AsString, 6*IndexCol - 5, 6));
    if (ValStr <> '') then
      CurrD2 := StrToInt(ValStr)
    else
      CurrD2 := 0;
//  Bug solved for Ctrl pressed -
    if (StaffPlanningF.FCtrl) then
    begin
     if (not FOverPlanning and
       (StaffPlanningF.FColumnName = WKField))then
       FOverPlanning := (CurrD2 > (2 * CurrD1));
    end
    else
       FOverPlanning := (CurrD2 > (2 * CurrD1));
  end;
end; // SetFieldsOCI

procedure TStaffPlanningOCIDM.UpdateLineOCI(ColumnStr, ValStr: String;
  PlannedEMP: Integer);
var
 PrevA, Index, NewD2, NewValueLevel: Integer;
 IndexColumn: Integer;
 Level, ColumnStr1 : String;
 // GLOB3-60
 function NewColumnStr: String;
 begin
   Result := ColumnStr1 + IntToStr(IndexColumn);
 end;
begin
  if PlannedEMP = 0 then Exit;
  Level := GetLevel(StrToInt(ValStr));
  if Level = '' then
    Exit;
  TableOCI.FindKey([LEVEL + '1']);

  // GLOB3-60 IndexColumn is now stored as Hex, so it is still 1 long, because
  //          it can now be 10 (A as hex) as max.
  IndexColumn := Hex2Int{StrToInt}(Copy(ColumnStr, Length(ColumnStr),1));
  ColumnStr1 := Copy(ColumnStr, 1, Length(ColumnStr) -1);

  if Trim(Copy(TableOCI.FieldByName(ColumnStr1).AsString,
    6 * IndexColumn - 5, 6)) <> '' then
    PrevA := StrToInt(Trim(Copy(TableOCI.FieldByName(ColumnStr1).AsString,
      6 * IndexColumn - 5, 6)))
  else
    PrevA := 0;

  SetFieldsOCI(ColumnStr, Level, PlannedEMP, PrevA, NewD2, NewValueLevel);

  Index := FListOCIPlanned.IndexOf(NewColumnStr + level);
  if Index >= 0 then
  begin
    FListOCIPlanned.Delete(Index + 1);
    FListOCIPlanned.Insert(Index + 1, IntToStr(NewValueLevel));
  end;
  //Car 3-4-2004
  Index := FListOCIPlanned.IndexOf(NewColumnStr + 'D');
  if Index >= 0 then
  begin
    FListOCIPlanned.Delete(Index + 1);
    FListOCIPlanned.Insert(Index + 1, IntToStr(NewD2));
  end;
end; // UpdateLineOCI

procedure TStaffPlanningOCIDM.UpdateOCI;
var
  IndexList, PlannedEMP, PosOCI, PosSpaceOCI: Integer;
  TmpStr, ValColumn, ColumnStr: String;
begin
  for IndexList := 0 to FListOCIUpdate.Count -1 do
  begin
    PlannedEMP := 0;
    TmpStr := FListOCIUpdate.Strings[IndexList];
    PosSpaceOCI := Pos(' ', TmpStr);
    if PosSpaceOCI > 0 then
    begin
      ColumnStr := Copy(TmpStr, 0 , PosSpaceOCI - 1);
      PosOCI := Pos('+', TmpStr);
      if PosOCI > 0 then
      begin
        ValColumn := Copy(TmpStr, PosSpaceOCI + 1, PosOCI - PosSpaceOCI -1);
        PlannedEmp := StrToInt(Copy(TmpStr, PosOCI + 1, 1))
      end
      else
      begin
        PosOCI := Pos('-', TmpStr);
        if PosOCI > 0 then
        begin
          ValColumn := Copy(TmpStr, PosSpaceOCI + 1, PosOCI - PosSpaceOCI -1);
          PlannedEMP := (-1) * StrToInt(Copy(TmpStr, PosOCI + 1, 1));
        end;
      end;
      if PlannedEMP <> 0 then
        UpdateLineOCI(ColumnStr , ValColumn, PlannedEMP);
    end;
  end;{for list}
end; // UpdateOCI

procedure TStaffPlanningOCIDM.SaveOCP;
begin
  if FSelection <> 0 then
    exit;
  FillOCP_STO(FPlant, FDate, FShift, FSelection);
end; // SaveOCP

procedure TStaffPlanningOCIDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := StaffPlanningOCIDM.DataModuleDestroy;
  ShowLevel := True;
  ShowLevelC := True;
  FListOCIPlanned := TStringList.Create;
  QuerySTOC.Prepare;

  if TableOCI.TableName = '' then
    TableOCI.TableName := 'PIVOTOCISTAFFPLANNING';
  TableOCI.Filtered := True;
  if (not ShowLevel) then
  begin
    TableOCI.Filter := '(PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentCOMPUTERName + '''' + ' ) AND (OCI_LEVEL >= ''D1'')';
  end
  else
    TableOCI.Filter := 'PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentCOMPUTERName + '''';
  TableOCI.Open;
end; // DataModuleCreate

procedure TStaffPlanningOCIDM.PlantGetSTDOCCFlags(APlant: String);
begin
  with QueryPlant do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlant;
    Open;
    if not Eof then
    begin
      Self.StdOccA := FieldByName('STDOCC_A_YN').AsString = 'Y';
      Self.StdOccB := FieldByName('STDOCC_B_YN').AsString = 'Y';
      Self.StdOccC := FieldByName('STDOCC_C_YN').AsString = 'Y';
    end;
    Close;
  end;
end; // PlantGetSTDOCCFlags

procedure TStaffPlanningOCIDM.PlantSetFilter;
begin
  PlantGetSTDOCCFlags(FPlant);
end; // PlantSetFilter

function TStaffPlanningOCIDM.LevelABCCount: Integer;
begin
  Result := 0;
  if Self.StdOccA then
    Result := Result + 1;
  if Self.StdOccB then
    Result := Result + 1;
  if Self.StdOccC then
    Result := Result + 1;
end;

end.
