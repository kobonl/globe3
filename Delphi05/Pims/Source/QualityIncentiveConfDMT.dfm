inherited QualityIncentiveConfDM: TQualityIncentiveConfDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    BeforePost = TableMasterBeforePost
    AfterPost = TableMasterAfterPost
    AfterDelete = TableMasterAfterDelete
    OnCalcFields = TableMasterCalcFields
    TableName = 'QUALITYINCENTIVECONF'
    object TableMasterMISTAKE: TIntegerField
      FieldName = 'MISTAKE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterBONUS_PER_HOUR: TFloatField
      FieldName = 'BONUS_PER_HOUR'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object TableMasterMISTAKECAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'MISTAKECAL'
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    BeforeDelete = nil
    MasterSource = nil
    TableName = 'QUALITYINCENTIVECONF'
  end
end
