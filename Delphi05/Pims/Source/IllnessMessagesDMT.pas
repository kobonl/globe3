(*
    SO:08-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
    MRA:1-NOV-2010. RV075.9.
    - When an illness message is added and there is already
      an illness message that overlaps this, it gave
      a wrong messsage: Start date is bigger than end date.
    - Solved by changing the error-message.
    - Also: Do not cancel the dataset on error.
    MRA:11-JAN-2011. RV084.3. SR-980041
    - Added order by for query 'absence reasons'.
    MRA:13-JAN-2011 RV084.4. Bugfix.
    - When adding illness message from tab-page 'Illness messages per employee',
      then there are 2 places where you can enter the employee!
      - The second place (in detail-panel) must be non-editable.
    - Removed call to IllnessMessageF-component in 'AfterDelete'.
    MRA:20-JAN-2011 RV085.8. Bugfix.
    - Filtering on teams-per-user is sometimes wrong.
      - It still shows all absence reasons when a new
        illness message is added. It must only show
        absence reasons for the country of the plant linked
        to the employee that was selected.
    MRA:18-FEB-2014 20011800
    - Final Run System
    - Prevent it is going to update in EmployeeAvailability-table, based
      on last-export-date.
    MRA:29-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
*)
unit IllnessMessagesDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, DBClient, Provider;

type
  TActiveTab = (OutStanding, PerEmployee);
  TIllnessFilter = Record
    StartDate: TDateTime;
    Employee_Number: Integer;
    ActiveTab: TActiveTab;
  end;
  TIllnessMessagesDM = class(TGridBaseDM)
    DataSourceAbsenceReason: TDataSource;
    TableDetailILLNESSMESSAGE_STARTDATE: TDateTimeField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailILLNESSMESSAGE_ENDDATE: TDateTimeField;
    TableDetailPROCESSED_YN: TStringField;
    TableDetailABSENCEREASON_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailABSENCEREASONLU: TStringField;
    TableDetailEMPLOYEELU: TStringField;
    QueryWork: TQuery;
    dspFilterEmployee: TDataSetProvider;
    cdsFilterEmployee: TClientDataSet;
    QueryEmployee: TQuery;
    dspMaster: TDataSetProvider;
    cdsMaster: TClientDataSet;
    TableDetailLINE_NUMBER: TIntegerField;
    qryMaxLine: TQuery;
    QueryUpdate: TQuery;
    qryIMCheck1BI: TQuery;
    qryIMCheck2BI: TQuery;
    qryIMCheck3BI: TQuery;
    qryIMCheck1BU: TQuery;
    qryIMCheck2BU: TQuery;
    qryIMCheck3BU: TQuery;
    qryAbsenceReason: TQuery;
    TableAbsenceReason: TTable;
    TableAbsenceReasonABSENCEREASON_ID: TIntegerField;
    TableAbsenceReasonABSENCETYPE_CODE: TStringField;
    TableAbsenceReasonABSENCEREASON_CODE: TStringField;
    TableAbsenceReasonDESCRIPTION: TStringField;
    TableAbsenceReasonCREATIONDATE: TDateTimeField;
    TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField;
    TableAbsenceReasonMUTATIONDATE: TDateTimeField;
    TableAbsenceReasonMUTATOR: TStringField;
    TableAbsenceReasonPAYED_YN: TStringField;
    TableAbsenceReasonOVERRULE_WITH_ILLNESS_YN: TStringField;
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailILLNESSMESSAGE_ENDDATEChange(Sender: TField);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure TableDetailAfterCancel(DataSet: TDataSet);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
    procedure TableDetailAfterDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FIllnessFilter: TIllnessFilter;
    procedure SetIllnessFilter(Value: TIllnessFilter);
    function IllnessMessageDatesCheckBI(
      DataSet: TDataSet): Boolean;
    function IllnessMessageDatesCheckBU(
      DataSet: TDataSet): Boolean;
  public
    { Public declarations }
    FBeforePost, FInsertRecord : Boolean;
    Old_Employee : Integer;
    Old_StartDate, Old_EndDate: TDateTime;
    Old_Absence: String;
    property IllnessFilter: TIllnessFilter read FIllnessFilter
    	write SetIllnessFilter;
    function UpdateEmployeeAvailability(TimeBlock,
      IllnessCode {NewCode, OldCode}: String; EmployeeNumber: Integer;
      StartDate,EndDate: TDateTime;
      InsertAction: Boolean): Integer;
    procedure DeleteEmployeePlanning(EmployeeNumber: integer;
      StartDate,EndDate: TDateTime);
     procedure DefaultAfterScroll(DataSet: TDataSet);
     procedure FillAbsenceReasons(AEmplNo: Integer);
  end;

var
  IllnessMessagesDM: TIllnessMessagesDM;

implementation

uses SystemDMT, UPimsConst, UGlobalFunctions, UPimsMessageRes;

{$R *.DFM}

procedure TIllnessMessagesDM.SetIllnessFilter(Value: TIllnessFilter);
begin
  DataSourceDetail.Enabled := False;
  if Value.Employee_Number = 0 then
  begin
    if cdsMaster.Active and (not cdsMaster.IsEmpty) then
      Value.Employee_Number :=
      //550286  - replace queryMaster with cdsMaster
      //remove old message QueryMasterAfterScroll(DataSet: TDataSet)
      //- not necessary
  	   cdsMaster.FieldByName('EMPLOYEE_NUMBER').asInteger
  end;
  FIllnessFilter := Value;
  if TableDetail.Active then
    TableDetail.Refresh;
  DataSourceDetail.Enabled := True;
end;

procedure TIllnessMessagesDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;

  case IllnessFilter.ActiveTab of
    OutStanding:
    begin
      Accept := ((DataSet.FieldByName('PROCESSED_YN').asString = UNCHECKEDVALUE) or
        (DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').asDateTime >=
        IllnessFilter.StartDate));
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        if Accept then
          Accept := cdsFilterEmployee.FindKey([DataSet.
            FieldByName('EMPLOYEE_NUMBER').AsInteger]);
    end;
    PerEmployee:
      Accept := ((DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger=
        IllnessFilter.Employee_Number)and
        	(DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').asDateTime >=
        	IllnessFilter.StartDate));
  end;
  if (FBeforePost) and (not Accept) then
  begin
    Accept := True;
    FBeforePost := False;
 end;
end;

procedure TIllnessMessagesDM.TableDetailNewRecord(DataSet: TDataSet);
var
  DateTmp: TDateTime;
  function DetermineMaxLine: Integer;
  begin
    Result := 0;
    qryMaxLine.Close;
    qryMaxLine.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      cdsMaster.fieldByName('EMPLOYEE_NUMBER').asInteger;
    qryMaxLine.Open;
    if not qryMaxLine.IsEmpty then
      Result := qryMaxLine.FieldByName('MAXLINE').AsInteger;
    qryMaxLine.Close;
    Result := Result + 1;
  end;
begin
  inherited;
  FillAbsenceReasons(
    cdsMaster.fieldByName('EMPLOYEE_NUMBER').asInteger); // RV085.8.
  DateTmp := Now;
  ReplaceTime(DateTmp, 0);
  DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').asDateTime := DateTmp;
  DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger :=
    cdsMaster.fieldByName('EMPLOYEE_NUMBER').asInteger;
  DataSet.FieldByName('PROCESSED_YN').asString := UNCHECKEDVALUE;
  DataSet.FieldByName('LINE_NUMBER').AsInteger := DetermineMaxLine;
  DefaultNewRecord(DataSet);

  FInsertRecord := True;
end;

procedure TIllnessMessagesDM.TableDetailILLNESSMESSAGE_ENDDATEChange(
  Sender: TField);
begin
  inherited;
  if Sender.AsDateTime <= NullDate then
  begin
    TableDetail.Edit;
    TableDetail.FieldByName('PROCESSED_YN').asString := UNCHECKEDVALUE;
  end
  else
  begin
    TableDetail.Edit;
    TableDetail.FieldByName('PROCESSED_YN').asString := CHECKEDVALUE;
  end;
end;

procedure TIllnessMessagesDM.TableDetailBeforeDelete(DataSet: TDataSet);
var
  TimeBlockNo: integer;
  Status: Integer; // 20011800
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  Status := 0;
  for TimeBlockNo := 1 to SystemDM.MaxTimeblocks do
    Status := Status +
      UpdateEmployeeAvailability(Format('AVAILABLE_TIMEBLOCK_%d',[TimeBlockNo]),
  //      DEFAULTAVAILABILITYCODE,
        DataSet.FieldByName('ABSENCEREASON_CODE').asString,
        DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger,
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').asDateTime,
        DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').asDateTime,
        False);
  if Status > 0 then
    DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]); // 20011800
end;

procedure TIllnessMessagesDM.DeleteEmployeePlanning(EmployeeNumber: integer;
  StartDate,EndDate: TDateTime);
begin
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'DELETE FROM EMPLOYEEPLANNING ' +
    'WHERE EMPLOYEEPLANNING_DATE >= :STDATE AND ' +
    'EMPLOYEE_NUMBER = :EMPNO '
    );
  if EndDate > NullDate Then
    QueryWork.SQL.Add('AND EMPLOYEEPLANNING_DATE <= :ENDATE');
  QueryWork.Prepare;
  QueryWork.ParamByName('STDATE').asDateTime :=StartDate;
  QueryWork.ParamByName('EMPNO').asInteger := EmployeeNumber;
  if EndDate > NullDate Then
    QueryWork.ParamByName('ENDATE').asDateTime := EndDate;
  QueryWork.ExecSql;
end;

function TIllnessMessagesDM.UpdateEmployeeAvailability(TimeBlock,
  IllnessCode {NewCode, OldCode}: String; EmployeeNumber: Integer;
  StartDate, EndDate: TDateTime;
  InsertAction: Boolean): Integer;
var
  SelectStr: String;
  function UpdateEARecord(PlantCode: String;
    EmployeeAvailabilityDate: TDateTime;
    ShiftNumber, EmployeeNumber: Integer;
    SaveTimeBlock: String;
    TimeBlock, IllnessCode: String;
    InsertAction: Boolean): Boolean;
  var
    SelectStr: String;
    FinalRunExportDate: TDateTime;
  begin
    Result := True;
    // 20011800
    if SystemDM.UseFinalRun then
    begin
      FinalRunExportDate := SystemDM.FinalRunExportDateByPlant(PlantCode);
      if FinalRunExportDate <> NullDate then
        if EmployeeAvailabilityDate <= FinalRunExportDate then
          Result := False
    end;
    if Result then
    begin
      QueryUpdate.Close;
      QueryUpdate.SQL.Clear;
      // Save original timeblock in Save_timeblock
      // After that, the timeblock is replaced with 'IllnessCode'.
      if InsertAction then
        SelectStr :=
          'UPDATE EMPLOYEEAVAILABILITY ' +
          'SET SAVE_' + TimeBlock + ' = ' + TimeBlock + ', ' +
          TimeBlock + '= :ILLNESSCODE, ' +
          'MUTATIONDATE = :MUTATIONDATE, MUTATOR = :MUTATOR ' +
          'WHERE PLANT_CODE = :PLANT_CODE AND ' +
          'EMPLOYEEAVAILABILITY_DATE = :EADATE AND ' +
          'SHIFT_NUMBER = :SHIFT_NUMBER AND ' +
          'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      else
      // Restore orginal timeblock from Save_timeblock.
        SelectStr :=
          'UPDATE EMPLOYEEAVAILABILITY ' +
          'SET ' + Timeblock + '= :SAVETIMEBLOCK, ' +
          'MUTATIONDATE = :MUTATIONDATE, MUTATOR = :MUTATOR ' +
          'WHERE PLANT_CODE = :PLANT_CODE AND ' +
          'EMPLOYEEAVAILABILITY_DATE = :EADATE AND ' +
          'SHIFT_NUMBER = :SHIFT_NUMBER AND ' +
          'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER';
      QueryUpdate.SQL.Add(SelectStr);
      if InsertAction then
        QueryUpdate.ParamByName('ILLNESSCODE').AsString := IllnessCode
      else
      begin
        if SaveTimeBlock = '' then
          SaveTimeBlock := DEFAULTAVAILABILITYCODE;
        QueryUpdate.ParamByName('SAVETIMEBLOCK').AsString := SaveTimeBlock;
      end;
      QueryUpdate.ParamByName('MUTATIONDATE').AsDateTime := Now;
      QueryUpdate.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      QueryUpdate.ParamByName('PLANT_CODE').AsString := PlantCode;
      QueryUpdate.ParamByName('EADATE').AsDateTime := EmployeeAvailabilityDate;
      QueryUpdate.ParamByName('SHIFT_NUMBER').AsInteger := ShiftNumber;
      QueryUpdate.ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
      QueryUpdate.ExecSQL;
    end; // if GoOn
  end; // UpdateEARecord
begin
  Result := 0;
  QueryWork.Close;
  QueryWork.SQL.Clear;
  SelectStr :=
    'SELECT ' + NL +
    '  EA.PLANT_CODE, EA.EMPLOYEEAVAILABILITY_DATE, ' + NL +
    '  EA.SHIFT_NUMBER, EA.EMPLOYEE_NUMBER, ' + NL +
    '  EA.SAVE_' + TimeBlock + ', ' +
    TimeBlock + ', ' +
    '  AR.OVERRULE_WITH_ILLNESS_YN ' + NL +
    'FROM ' + NL +
    '  EMPLOYEEAVAILABILITY EA LEFT JOIN ABSENCEREASON AR ON ' + NL +
    '    EA.' + TimeBlock + ' = AR.ABSENCEREASON_CODE ' + NL +
    'WHERE ' + NL +
    '  EA.EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
    '  EA.EMPLOYEEAVAILABILITY_DATE >= :STDATE ' + NL;
  if EndDate > NullDate then
    SelectStr := SelectStr +
      '  AND EA.EMPLOYEEAVAILABILITY_DATE <= :ENDATE ';
  QueryWork.SQL.Add(SelectStr);
  QueryWork.ParamByName('EMPNO').asInteger := EmployeeNumber;
  QueryWork.ParamByName('STDATE').asDateTime := StartDate;
  if EndDate > NullDate then
    QueryWork.ParamByName('ENDATE').asDateTime := EndDate;
  QueryWork.Open;
  if not QueryWork.IsEmpty then
  begin
    QueryWork.First;
    while not QueryWork.Eof do
    begin
      if (
           InsertAction and
           (QueryWork.
            FieldByName(TimeBlock).AsString = DEFAULTAVAILABILITYCODE) or
           (QueryWork.
            FieldByName('OVERRULE_WITH_ILLNESS_YN').AsString = CHECKEDVALUE)
          )
        or
         (
           (not InsertAction) and
           (QueryWork.FieldByName(TimeBlock).AsString = IllnessCode)
         ) then
      begin
        if not UpdateEARecord(QueryWork.FieldByName('PLANT_CODE').AsString,
          QueryWork.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime,
          QueryWork.FieldByName('SHIFT_NUMBER').AsInteger,
          QueryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          QueryWork.FieldByName('SAVE_' + TimeBlock).AsString,
          TimeBlock, IllnessCode,
          InsertAction) then
          Result := Result + 1;
      end;
      QueryWork.Next;
    end;
  end;
end; // UpdateEmployeeAvailability

// MR:26-07-2005 Instead of trigger, this function is used.
// IllnessMessage Dates Check BI (Before Insert)
function TIllnessMessagesDM.IllnessMessageDatesCheckBI(
  DataSet: TDataSet): Boolean;
var
  NewIMStartDate, NewIMEndDate: TDateTime;
  CurrIMStartDate, CurrIMEndDate: TDateTime;
begin
  Result := True;
  // First check if new-record's dates are correct.
  if (DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').Value <> Null) then
    if (DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime >
      DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime) and
      (DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime <> 0) then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbOK]);
      Result := False;
      Exit;
    end;
  // Check if there is already an 'open' record,
  // where enddate is null.
  qryIMCheck1BI.Close;
  qryIMCheck1BI.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  qryIMCheck1BI.Open;
  if qryIMCheck1BI.FieldByName('RECCOUNT').AsInteger >= 1 then
  begin
    DisplayMessage(SPimsEXCheckIllMsgClose, mtInformation, [mbOK]);
    Result := False;
  end;
  qryIMCheck1BI.Close;
  if Result then
  begin
    if DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').Value <> Null then
    begin
      // Check if start/enddate do not interfere, both are not null.
      NewIMStartDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
      NewIMEndDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
      qryIMCheck2BI.Close;
      qryIMCheck2BI.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
        DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      qryIMCheck2BI.Open;
      if not qryIMCheck2BI.IsEmpty then
      begin
        qryIMCheck2BI.First;
        while (not qryIMCheck2BI.Eof) and Result do
        begin
          CurrIMStartDate :=
            qryIMCheck2BI.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
          CurrIMEndDate :=
            qryIMCheck2BI.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
          if (((NewIMStartDate <= CurrIMStartDate) and
            (NewIMEndDate >= CurrIMStartDate)) or
            ((NewIMStartDate >= CurrIMStartDate) and
            (NewIMStartDate <= CurrIMEndDate))) then
          begin
            DisplayMessage(SPimsEXCheckIllMsgStartDate, mtInformation, [mbOK]);
            Result := False;
          end;
          qryIMCheck2BI.Next;
        end;
      end;
      qryIMCheck2BI.Close;
    end
    else
    begin
      // Check if start/enddate do not interfere, enddate is null.
      NewIMStartDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
      qryIMCheck3BI.Close;
      qryIMCheck3BI.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
        DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      qryIMCheck3BI.Open;
      if not qryIMCheck3BI.IsEmpty then
      begin
        qryIMCheck3BI.First;
        while (not qryIMCheck3BI.Eof) and Result do
        begin
          CurrIMEndDate :=
            qryIMCheck3BI.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
          if (NewIMStartDate <= CurrIMEndDate) then
          begin
            // RV075.9. Message was wrong!
            DisplayMessage(SPimsStartEndDateSmaller,
              {SPimsStartEndDate,} mtInformation, [mbOK]);
            Result := False;
          end;
          qryIMCheck3BI.Next;
        end;
      end;
      qryIMCheck3BI.Close;
    end;
  end;
end;

// MR:26-07-2005 Instead of trigger, this function is used.
// IllnessMessage Dates Check BU (Before Update)
function TIllnessMessagesDM.IllnessMessageDatesCheckBU(
  DataSet: TDataSet): Boolean;
var
  NewIMStartDate, NewIMEndDate: TDateTime;
  CurrIMStartDate, CurrIMEndDate: TDateTime;
begin
  Result := True;
  // NOTE: The parameter for the 'startdate' should be
  // the 'old_startdate', because the changed 'startdate' is not in
  // the database yet during 'Before Update'.
  // First check if new-record's dates are correct.
  if (DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').Value <> Null) then
    if (DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime >
      DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime) and
      (DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime <> 0) then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbOK]);
      Result := False;
      Exit;
    end;
  // Check if there is already an 'open' record,
  // where enddate is null.
  if DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').Value = Null then
  begin
    qryIMCheck1BU.Close;
    qryIMCheck1BU.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
      DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryIMCheck1BU.ParamByName('OLD_ILLNESSMESSAGE_STARTDATE').AsDateTime :=
      Old_StartDate;
    qryIMCheck1BU.Open;
    if qryIMCheck1BU.FieldByName('RECCOUNT').AsInteger >= 1 then
    begin
      DisplayMessage(SPimsEXCheckIllMsgClose, mtInformation, [mbOK]);
      Result := False;
    end;
    qryIMCheck1BU.Close;
  end;
  if Result then
  begin
    if DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').Value <> Null then
    begin
      // Check if start/enddate do not interfere, both are not null.
      NewIMStartDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
      NewIMEndDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
      qryIMCheck2BU.Close;
      qryIMCheck2BU.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
        DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      qryIMCheck2BU.ParamByName('OLD_ILLNESSMESSAGE_STARTDATE').AsDateTime :=
        Old_StartDate;
      qryIMCheck2BU.Open;
      if not qryIMCheck2BU.IsEmpty then
      begin
        qryIMCheck2BU.First;
        while (not qryIMCheck2BU.Eof) and Result do
        begin
          CurrIMStartDate :=
            qryIMCheck2BU.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
          CurrIMEndDate :=
            qryIMCheck2BU.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
          if (((NewIMStartDate <= CurrIMStartDate) and
            (NewIMEndDate >= CurrIMStartDate)) or
            ((NewIMStartDate >= CurrIMStartDate) and
            (NewIMStartDate <= CurrIMEndDate))) then
          begin
            DisplayMessage(SPimsEXCheckIllMsgStartDate, mtInformation, [mbOK]);
            Result := False;
          end;
          qryIMCheck2BU.Next;
        end;
      end;
      qryIMCheck2BU.Close;
    end
    else
    begin
      // Check if start/enddate do not interfere, enddate is null.
      NewIMStartDate :=
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
      qryIMCheck3BU.Close;
      qryIMCheck3BU.ParamByName('NEW_EMPLOYEE_NUMBER').AsInteger :=
        DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      qryIMCheck3BU.ParamByName('OLD_ILLNESSMESSAGE_STARTDATE').AsDateTime :=
        Old_StartDate;
      qryIMCheck3BU.Open;
      if not qryIMCheck3BU.IsEmpty then
      begin
        qryIMCheck3BU.First;
        while (not qryIMCheck3BU.Eof) and Result do
        begin
          CurrIMEndDate :=
            qryIMCheck3BU.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
          if (NewIMStartDate <= CurrIMEndDate) then
          begin
            // RV075.9. Message was wrong!
            DisplayMessage(SPimsStartEndDateSmaller, {SPimsStartEndDate,}
              mtInformation, [mbOK]);
            Result := False;
          end;
          qryIMCheck3BU.Next;
        end;
      end;
      qryIMCheck3BU.Close;
    end;
  end;
end;

procedure TIllnessMessagesDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  // Record is inserted
  if DataSet.State = dsInsert then
  begin
    if not IllnessMessageDatesCheckBI(DataSet) then
    begin
      // RV075.9. Do not cancel the dataset.
//      DataSet.Cancel;
      SysUtils.Abort;
    end;
  end
  else
    // Record is updated
    if not IllnessMessageDatesCheckBU(DataSet) then
    begin
      // RV075.9. Do not cancel the dataset.
//      DataSet.Cancel;
      SysUtils.Abort;
    end;
  FBeforePost := True;
end;

procedure TIllnessMessagesDM.TableDetailAfterPost(DataSet: TDataSet);
var
  TimeBlockNo: integer;
  Status: Integer;
begin
  inherited;
  Status := 0;
  FBeforePost := False;
  TableDetail.Refresh;
  if not FInsertRecord then
  begin
    for TimeBlockNo := 1 to SystemDM.MaxTimeblocks do
      Status := Status +
        UpdateEmployeeAvailability(Format('AVAILABLE_TIMEBLOCK_%d',[TimeBlockNo]),
          {DEFAULTAVAILABILITYCODE,} Old_Absence, Old_Employee, OLD_StartDate,
          Old_EndDate, False);
  end;
  FInsertRecord := False;
  for TimeBlockNo := 1 to SystemDM.MaxTimeblocks do
    Status := Status +
      UpdateEmployeeAvailability(Format('AVAILABLE_TIMEBLOCK_%d',[TimeBlockNo]),
        DataSet.FieldByName('ABSENCEREASON_CODE').asString,
        {DEFAULTAVAILABILITYCODE,}
        DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger,
        DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').asDateTime,
        DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').asDateTime,
        True);
  DeleteEmployeePlanning(DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger,
    DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').asDateTime,
    DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').asDateTime);
  if Status > 0 then
    DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]); // 20011800
end;

procedure TIllnessMessagesDM.DefaultAfterScroll(DataSet: TDataSet);
begin
  Old_Employee := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Old_StartDate := DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
  Old_EndDate := DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
  Old_Absence := DataSet.FieldByName('ABSENCEREASON_CODE').AsString;
end;

procedure TIllnessMessagesDM.TableDetailAfterCancel(DataSet: TDataSet);
begin
  inherited;
  FInsertRecord := False;
end;

procedure TIllnessMessagesDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  FillAbsenceReasons(
    cdsMaster.fieldByName('EMPLOYEE_NUMBER').asInteger); // RV085.8.
  Old_Employee := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Old_StartDate := DataSet.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
  Old_EndDate := DataSet.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
  Old_Absence := DataSet.FieldByName('ABSENCEREASON_CODE').AsString;
end;

procedure TIllnessMessagesDM.TableDetailAfterDelete(DataSet: TDataSet);
begin
  inherited;
  // RV084.4. Not needed here.
  //  IllnessMessagesF.DBLookupComboBoxEmployee.SetFocus;
end;

procedure TIllnessMessagesDM.DataModuleCreate(Sender: TObject);
//CAR 550274 - team selection
var
  SelectStr: String;
begin
  inherited;
  QueryEmployee.SQL.Clear;
  //550286 - remove queryMaster
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    SelectStr :=
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME, E.DESCRIPTION, ' + NL +
      '  E.ADDRESS ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, TEAMPERUSER T ' + NL +
      'WHERE ' + NL +
      '  T.USER_NAME = :USER_NAME AND ' + NL +
      '  E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'ORDER BY ' + NL +
      '  E.EMPLOYEE_NUMBER';
    QueryEmployee.Sql.Add(SelectStr);
    QueryEmployee.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end
  else
  begin
    SelectStr := 
      'SELECT ' + NL +
      '  EMPLOYEE_NUMBER, SHORT_NAME, DESCRIPTION, ADDRESS ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE ' + NL +
      'ORDER BY ' + NL +
      '  EMPLOYEE_NUMBER';
    QueryEmployee.Sql.Add(SelectStr);
  end;
  qryAbsenceReason.Open;
  QueryEmployee.Prepare;
  cdsFilterEmployee.Open;
  cdsMaster.Open;
end;

procedure TIllnessMessagesDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //CAR 550274
  QueryEmployee.Close;
  QueryEmployee.UnPrepare;
end;
//RV067.4.
procedure TIllnessMessagesDM.FillAbsenceReasons(AEmplNo: Integer);
var
  CountryId: Integer;
begin
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) FROM ' +
  ' EMPLOYEE E, PLANT P ' +
  ' WHERE ' +
  '   E.PLANT_CODE = P.PLANT_CODE AND ' +
  '   E.EMPLOYEE_NUMBER = ' +
  IntToStr(AEmplNo), 0
  );

  // RV084.3.
  IllnessMessagesDM.qryAbsenceReason.Close;
  IllnessMessagesDM.qryAbsenceReason.SQL.Text :=
    'SELECT * FROM ABSENCEREASON ORDER BY DESCRIPTION';
  IllnessMessagesDM.qryAbsenceReason.Open;
  if CountryId <> 0 then
    SystemDM.UpdateAbsenceReasonPerCountry(
      CountryId, IllnessMessagesDM.qryAbsenceReason
    );
end;

end.
