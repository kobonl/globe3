inherited DialogCopyEmpDataF: TDialogCopyEmpDataF
  Left = 702
  Top = 285
  Height = 415
  Caption = 'Copy additional data from other employee'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 316
    inherited btnOk: TBitBtn
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      OnClick = btnCancelClick
    end
  end
  inherited stbarBase: TStatusBar
    Top = 357
  end
  object gboxEmployee: TGroupBox
    Left = 0
    Top = 0
    Width = 504
    Height = 57
    Align = alTop
    Caption = 'Copy additional data from other employee'
    TabOrder = 2
    object Label1: TLabel
      Left = 10
      Top = 24
      Width = 46
      Height = 13
      Caption = 'Employee'
    end
    object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
      Left = 96
      Top = 22
      Width = 384
      TabOrder = 0
      DataField = 'VALUEDISPLAY'
      DataSource = dsEmployee
      OnCloseUp = dxDBExtLookupEditEmployeeCloseUp
      DBGridLayout = dxDBGridLayoutListEmployee
    end
  end
  object gboxPeriod: TGroupBox
    Left = 0
    Top = 57
    Width = 504
    Height = 56
    Align = alTop
    Caption = 'Period'
    TabOrder = 3
    object Label6: TLabel
      Left = 10
      Top = 24
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 376
      Top = 24
      Width = 9
      Height = 13
      Caption = 'to'
    end
    object Label2: TLabel
      Left = 226
      Top = 24
      Width = 54
      Height = 13
      Caption = 'From Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dxSpinEditYearFrom: TdxSpinEdit
      Left = 96
      Top = 20
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 304
      Top = 20
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 416
      Top = 20
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
  end
  object gboxMessages: TGroupBox
    Left = 0
    Top = 113
    Width = 504
    Height = 203
    Align = alClient
    Caption = 'Messages'
    TabOrder = 4
    object mmMessages: TMemo
      Left = 2
      Top = 15
      Width = 500
      Height = 186
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object qryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER,'
      '  E.EMPLOYEE_NUMBER ||  '#39' | '#39'  || E.DESCRIPTION AS VALUEDISPLAY,'
      
        '  E.DESCRIPTION, E.SHORT_NAME, E.DEPARTMENT_CODE, E.TEAM_CODE, E' +
        '.ADDRESS'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  E.PLANT_CODE = :PLANT_CODE AND'
      '  E.EMPLOYEE_NUMBER <> :EMPLOYEE_NUMBER AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      '  AND'
      '  (E.STARTDATE <= trunc(sysdate)) AND'
      '  ((E.ENDDATE >= trunc(sysdate)) OR (E.ENDDATE IS NULL))'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' ')
    Left = 56
    Top = 40
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object qryEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 24
    end
    object qryEmployeeEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object qryEmployeeVALUEDISPLAY: TStringField
      FieldName = 'VALUEDISPLAY'
      Size = 201
    end
    object qryEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 160
    end
    object qryEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 24
    end
    object qryEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 24
    end
    object qryEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 24
    end
    object qryEmployeeADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 160
    end
  end
  object dsEmployee: TDataSource
    DataSet = qryEmployee
    Left = 104
    Top = 40
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 368
    Top = 8
    object dxDBGridLayoutListEmployee: TdxDBGridLayout
      Data = {
        BE030000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365071D4469616C6F67436F707945
        6D7044617461462E6473456D706C6F79656500135464784442477269644D6173
        6B436F6C756D6E0F454D504C4F5945455F4E554D4245520743617074696F6E06
        064E756D6265720942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F454D504C4F5945455F4E554D42455200001354647844
        42477269644D61736B436F6C756D6E0C56414C5545444953504C415907566973
        69626C65080942616E64496E646578020008526F77496E646578020009466965
        6C644E616D65060C56414C5545444953504C4159000013546478444247726964
        4D61736B436F6C756D6E0B4445534352495054494F4E0743617074696F6E0604
        4E616D650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E0000135464784442477269644D61
        736B436F6C756D6E0A53484F52545F4E414D450743617074696F6E060A53686F
        7274206E616D650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060A53484F52545F4E414D45000013546478444247726964
        4D61736B436F6C756D6E0A504C414E545F434F44450743617074696F6E060A50
        6C616E7420636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060A504C414E545F434F444500001354647844424772
        69644D61736B436F6C756D6E0F4445504152544D454E545F434F444507436170
        74696F6E060F4465706172746D656E7420636F64650942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D65060F4445504152544D45
        4E545F434F44450000135464784442477269644D61736B436F6C756D6E095445
        414D5F434F44450743617074696F6E06095465616D20636F64650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650609544541
        4D5F434F44450000135464784442477269644D61736B436F6C756D6E07414444
        524553530743617074696F6E0607416464726573730942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D6506074144445245535300
        0000}
    end
  end
end
