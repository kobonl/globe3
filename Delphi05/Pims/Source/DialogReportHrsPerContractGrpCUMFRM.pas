(*
  Changes:
    MRA:24-MAY-2011. RV093.1 SO-20011749.
    - New report hours per contract group cumulative
      - Checkbox for 'New Page on Workspot' made invisible
      - Checbox for 'New Page on Jobcode' made invisible
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase-form.
    - CheckBoxAllEmployees moved to DialogReportBase-form.
    MRA:23-JAN-2012 RV104.3. 20011749. Addition.
    - It should also have a selection on contract group.
*)

unit DialogReportHrsPerContractGrpCUMFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportHrsPerContractGrpCUMF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    GroupBoxShow: TGroupBox;
    CheckBoxBU: TCheckBox;
    CheckBoxEmpl: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    CheckBoxWK: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    QueryBU: TQuery;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBoxJobcode: TCheckBox;
    CheckBoxPageJob: TCheckBox;
    Label16: TLabel;
    Label13: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    Label14: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    QueryJobCode: TQuery;
    DataSourceWorkSpot: TDataSource;
    CheckBoxDept: TCheckBox;
    CheckBoxPageBU: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxExport: TCheckBox;
    LblStarContractGroupFrom: TLabel;
    ComboBoxPlusContractGroupFrom: TComboBoxPlus;
    LblToContractGroup: TLabel;
    LblStarContractGroupTo: TLabel;
    ComboBoxPlusContractGroupTo: TComboBoxPlus;
    LblContractGroup: TLabel;
    LblFromContractGroup: TLabel;
    QueryContractGroup: TQuery;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
{    procedure FillWorkSpot; }
    procedure FillJobcode;
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxWKClick(Sender: TObject);
    procedure CheckBoxJobcodeClick(Sender: TObject);
    procedure SetNewPageWK;
    procedure SetNewPageJobCode;
    procedure CheckBoxBUClick(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CheckBoxAllEmployeesClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbPlusWorkspotFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotToCloseUp(Sender: TObject);
    procedure ComboBoxPlusContractGroupFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusContractGroupToCloseUp(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure FillContractGroup;
    procedure AllEmployeeAction;
    procedure EnableIncludeNotConnectedEmp;
  public
    { Public declarations }

  end;

var
  DialogReportHrsPerContractGrpCUMF: TDialogReportHrsPerContractGrpCUMF;

// RV089.1.
function DialogReportHrsPerContractGrpCUMForm: TDialogReportHrsPerContractGrpCUMF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportHrsPerContractGrpCUMDMT, ReportHrsPerContractGrpCUMQRPT, ListProcsFRM;

var
  DialogReportHrsPerContractGrpCUMF_HND: TDialogReportHrsPerContractGrpCUMF;

// RV089.1.
function DialogReportHrsPerContractGrpCUMForm: TDialogReportHrsPerContractGrpCUMF;
begin
  if (DialogReportHrsPerContractGrpCUMF_HND = nil) then
  begin
    DialogReportHrsPerContractGrpCUMF_HND := TDialogReportHrsPerContractGrpCUMF.Create(Application);
    with DialogReportHrsPerContractGrpCUMF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportHrsPerContractGrpCUMF_HND;
end;

// RV089.1.
procedure TDialogReportHrsPerContractGrpCUMF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportHrsPerContractGrpCUMF_HND <> nil) then
  begin
    DialogReportHrsPerContractGrpCUMF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportHrsPerContractGrpCUMF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportHrsPerContractGrpCUMF.btnOkClick(Sender: TObject);
begin
  inherited;

  if ReportHrsPerContractGrpCUMQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      GetStrValue(CmbPlusJobFrom.Value),
      GetStrValue(CmbPlusJobTo.Value),
      GetStrValue(ComboBoxPlusContractGroupFrom.Value),
      GetStrValue(ComboBoxPlusContractGroupTo.Value),
       //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      DateFrom.Date,
      DateTo.Date,
      CheckBoxBU.Checked, CheckBoxDept.Checked, CheckBoxWK.Checked,
      CheckBoxJobcode.Checked, CheckBoxEmpl.Checked,
      CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
      CheckBoxPageBU.Checked, CheckBoxPageDept.Checked,
      CheckBoxPageWK.Checked, CheckBoxPageJob.Checked,
      CheckBoxExport.Checked,
      CheckBoxAllEmployees.Checked,
      CheckBoxIncludeNotConnectedEmp.Checked)
  then
    ReportHrsPerContractGrpCUMQR.ProcessRecords;
end;

procedure TDialogReportHrsPerContractGrpCUMF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;
{
procedure TDialogReportHrsPerContractGrpCUMF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom,  GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}

procedure TDialogReportHrsPerContractGrpCUMF.FillJobCode;
begin
  // Do not show job-selection.
  if not CheckBoxJobCode.Visible then
  begin
    Label12.Visible := False;
    Label13.Visible := False;
//    ComboBoxPlusJobCodeFrom.Visible := False;
//    ComboBoxPlusJobCodeTo.Visible := False;
    Label14.Visible := False;
    Label24.Visible := False;
    Label25.Visible := False;
    Exit;
  end;
{
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
     (CmbPlusWorkspotFrom.Value <> '') and
     (CmbPlusWorkspotFrom.Value = CmbPlusWorkspotTo.Value) then
  begin
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeFrom,
       GetStrValue(CmbPlusPlantFrom.Value),
       GetStrValue(CmbPlusWorkspotFrom.Value), True);
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeTo,
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value), False);
    ComboBoxPlusJobCodeFrom.Visible := True;
    ComboBoxPlusJobCodeTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusJobCodeFrom.Visible := False;
    ComboBoxPlusJobCodeTo.Visible := False;
  end;
}  
end;

procedure TDialogReportHrsPerContractGrpCUMF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, False, True, False, True, True);
  CheckBoxAllEmployeesShow := True;
  inherited;
  CmbPlusPlantFromCloseUp(Sender);
//  FillBusiness;
//  FillWorkSpot;
  FillJobCode;

  // RV104.3.
  FillContractGroup;

  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageBU.Checked := False;
  CheckBoxPageDept.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxPageJob.Checked := False;

  CheckBoxPageBU.Enabled := False;
  CheckBoxPageDept.Enabled := False;
  CheckBoxPageWK.Enabled := False;
  CheckBoxPageJob.Enabled := False;

  CheckBoxBU.Checked := False;
  CheckBoxDept.Checked := True;

  CheckBoxWK.Checked := False;

  CheckBoxJobCode.Checked := False;

  CheckBoxEmpl.Checked := False;
end;

procedure TDialogReportHrsPerContractGrpCUMF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportHrsPerContractGrpCUMDM := CreateReportDM(TReportHrsPerContractGrpCUMDM);
  ReportHrsPerContractGrpCUMQR := CreateReportQR(TReportHrsPerContractGrpCUMQR);
end;

procedure TDialogReportHrsPerContractGrpCUMF.CheckBoxDeptClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
//  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.SetNewPageWK;
begin
  CheckBoxPageDept.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageDept.Checked := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxWK.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageBU.Checked := not CheckBoxWK.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CheckBoxWKClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageWK.Enabled := CheckBoxWK.Checked;
//  CheckBoxPageWK.Checked := CheckBoxWK.Checked;
{  CheckBoxJobCode.Enabled := CheckBoxWK.Checked; }
//  CheckBoxJobCode.Checked := CheckBoxWK.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.SetNewPageJobCode;
begin
  CheckBoxPageDept.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageDept.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Checked := not CheckBoxJobcode.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CheckBoxJobcodeClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageJob.Enabled := CheckBoxJobcode.Checked;
//  CheckBoxPageJob.Checked := CheckBoxJobcode.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CheckBoxBUClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageBU.Enabled := CheckBoxBU.Checked;
//  CheckBoxPageBU.Checked := CheckBoxBU.Checked;
end;

procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessTo.DisplayValue :=
          ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue;
  FillJobCode; }
end;

procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue;
  FillJobCode; }
end;

procedure TDialogReportHrsPerContractGrpCUMF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
  FillJobCode;
  AllEmployeeAction;
  //!!
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
  FillJobCode;
  AllEmployeeAction;
  //!!
  EnableIncludeNotConnectedEmp;
end;

// RV050.4.
procedure TDialogReportHrsPerContractGrpCUMF.AllEmployeeAction;
begin
  CheckBoxAllEmployeesClick(nil);
end;

// RV050.4.
procedure TDialogReportHrsPerContractGrpCUMF.CheckBoxAllEmployeesClick(
  Sender: TObject);
begin
  inherited;
  if CheckBoxAllEmployees.Checked then
  begin
    dxDBExtLookupEditEmplFrom.Visible := False;
    dxDBExtLookupEditEmplTo.Visible := False;
  end
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      dxDBExtLookupEditEmplFrom.Visible := True;
      dxDBExtLookupEditEmplTo.Visible := True;
    end
    else
    begin
      CheckBoxAllEmployees.Checked := True;
    end;
  end;

  //!!
  EnableIncludeNotConnectedEmp;
end;

//!!
procedure TDialogReportHrsPerContractGrpCUMF.EnableIncludeNotConnectedEmp;
begin
  CheckBoxIncludeNotConnectedEmp.Enabled :=
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
    (not CheckBoxAllEmployees.Checked);
  if not CheckBoxIncludeNotConnectedEmp.Enabled then
  begin
    CheckBoxIncludeNotConnectedEmp.Checked := False;
    CheckBoxIncludeNotConnectedEmpClick(Self);
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportHrsPerContractGrpCUMF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportHrsPerContractGrpCUMF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CmbPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillJobCode;
end;

procedure TDialogReportHrsPerContractGrpCUMF.CmbPlusWorkspotToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillJobCode;
end;

// RV104.3.
procedure TDialogReportHrsPerContractGrpCUMF.FillContractGroup;
begin
  ListProcsF.FillComboBox(QueryContractGroup,
    ComboBoxPlusContractGroupFrom, '', '', True, '', '',
    'CONTRACTGROUP_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(QueryContractGroup,
    ComboBoxPlusContractGroupTo, '', '', False, '', '',
    'CONTRACTGROUP_CODE', 'DESCRIPTION');
end;

// RV104.3.
procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusContractGroupFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusContractGroupFrom.DisplayValue <> '') and
     (ComboBoxPlusContractGroupTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusContractGroupFrom.Value) >
      GetStrValue(ComboBoxPlusContractGroupTo.Value) then
        ComboBoxPlusContractGroupTo.DisplayValue :=
          ComboBoxPlusContractGroupFrom.DisplayValue;
end;

// RV104.3.
procedure TDialogReportHrsPerContractGrpCUMF.ComboBoxPlusContractGroupToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusContractGroupFrom.DisplayValue <> '') and
     (ComboBoxPlusContractGroupTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusContractGroupFrom.Value) >
      GetStrValue(ComboBoxPlusContractGroupTo.Value) then
        ComboBoxPlusContractGroupFrom.DisplayValue :=
          ComboBoxPlusContractGroupTo.DisplayValue;
end;

end.
