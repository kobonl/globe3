(*
  MRA:16-NOV-2010
  - Report Memo Class. This handles Memo-components in a report.
  MRA:18-NOV-2010.
  - Do not use this anymore! It can give double lines when printing to real
    printer of PDF.
*)
unit UReportMemoClass;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, FileCtrl, QRPDFFilt, QRExport, QRXMLSFilt,
  QRWebFilt;

const
  HEIGHT_MEMO = 14; // MRA:28-JUN-2010 Changed from 15 to 14. RV066.
  MAX_CHILD_BAND = 10;
  MAX_MEMO_LINES = 3;
  MAX_LINES_ON_PAGE = 35;

type
  TChildBandMemoArray = Array[0.. MAX_CHILD_BAND-1] of TQRChildBand;

// RV080.1.
type
  TMemoClass = class
  private
  protected
    FChildBand: TChildBandMemoArray;// array of child bands created
    FMAX_MEMO_LINES, FMax_Line_Memo_Page: Integer;
    FMemoBand: TQRCustomBand;
    procedure GetMaxCountMemo(ParentBand: TQRCustomBand;
      var MaxCountLines, MaxTopMemo: Integer);
    procedure FillMemoChildBand(IndexLines: Integer;
      ParentBand, ChildBand: TQRCustomBand);
    procedure SetHeightMemoFields(ParentBand: TQRCustomBand;
       MaxCountLines, MaxTopMemo: Integer);
    function GetMemo(NameMemo: String; BandParent: TQRCustomBand): TQRMemo;
  public
    constructor Create(Memo_Lines, Max_Lines_Memo_Page: Integer;
      BandMemo: TQRCustomBand);
    procedure Free;
    procedure Init;
    procedure SetHeightMemoBand(PrintBand: Boolean; ParentBand: TQRCustomBand);
    procedure SetMemoParam(Memo_Lines, Max_Lines_Memo_Page: Integer;
      BandMemo: TQRCustomBand);
  end;

implementation

uses
  UGlobalFunctions;

{ TMemoClass }

constructor TMemoClass.Create(Memo_Lines, Max_Lines_Memo_Page: Integer;
  BandMemo: TQRCustomBand);
var
  Counter, IndexBand : Integer;
  ParentBand: TQRCustomBand;
  TmpMemo, NewTmpMemo: TQRMemo;
begin
  inherited Create;

  FMemoBand := BandMemo;   // set Band who contains memo fields
  // set Number of lines into a memo field on a child band or bandmemo
  FMAX_MEMO_LINES := Memo_Lines;
  //set number of lines into a memo which had to fit in one page
  FMax_Line_Memo_Page := Max_Lines_Memo_Page;

  // check if this mechanism is user further - if exists a band with memo fields
  if FMemoBand = nil then
    Exit;

//Create childband with memo fields on the same position as leftmemo
// maximal number of child bands is MAX_CHILD_BAND
  ParentBand := FMemoBand;
  for IndexBand := 0 to MAX_CHILD_BAND-1 do
  begin
    ParentBand.HasChild := True;
    FChildBand[IndexBand] := ParentBand.ChildBand;
    FChildBand[IndexBand].Height := HEIGHT_MEMO;
    FChildBand[IndexBand].Name := FMemoBand.Name + 'ChildBand' + IntToStr(IndexBand);
    ParentBand := FChildBand[IndexBand];
    FChildBand[IndexBand].Font.Size := 8;
    FChildBand[IndexBand].Left := FMemoBand.Left;
  end;
  // for each memo field of parent band
  //create a memo field in each childband created before
  for Counter := 0 to FMemoBand.ControlCount -1 do
    if (FMemoBand.Controls[Counter] is TQRMemo) then
    begin
      TmpMemo := TQRMemo(FMemoBand.Controls[Counter]);
      for IndexBand := 0 to MAX_CHILD_BAND-1 do
      begin
        NewTmpMemo := TQRMemo(FChildBand[IndexBand].AddPrintable(TQRMemo));
        NewTmpMemo.Left := TmpMemo.Left;
        NewTmpMemo.Font.Size := 8;
        NewTmpMemo.Top := -1;
        NewTmpMemo.Tag := -1;  // to remove any space between child bands
        NewTmpMemo.Height := HEIGHT_MEMO;
        NewTmpMemo.AutoSize := True;
        NewTmpMemo.AutoStretch := False;
        NewTmpMemo.Lines.Clear;
        NewTmpMemo.Width := TmpMemo.Width;
        NewTmpMemo.Name := 'Memo' + IntToStr(IndexBand) + '_' +  TmpMemo.Name;
        NewTmpMemo.Alignment := TmpMemo.Alignment;
        NewTmpMemo.Parent := FChildBand[IndexBand];
        NewTmpMemo.AlignToBand := TmpMemo.AlignToBand;
        NewTmpMemo.Transparent := TmpMemo.Transparent;
      end;
    end;
end; // Create;

procedure TMemoClass.Init;
(* var
  Counter, IndexBand : Integer;
  ParentBand: TQRCustomBand;
  TmpMemo, NewTmpMemo: TQRMemo; *)
begin
{$IFDEF DEBUG}
  WLog('TMemoClass.Init');
{$ENDIF}
(*
  if FMemoBand <> nil then
  begin
    ParentBand := FMemoBand;
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
    begin
      ParentBand.HasChild := True;
      FChildBand[IndexBand] := ParentBand.ChildBand;
      FChildBand[IndexBand].Height := HEIGHT_MEMO;
//      FChildBand[IndexBand].Name := FMemoBand.Name + 'ChildBand' + IntToStr(IndexBand);
      ParentBand := FChildBand[IndexBand];
      FChildBand[IndexBand].Font.Size := 8;
      FChildBand[IndexBand].Left := FMemoBand.Left;
    end;
    for Counter := 0 to FMemoBand.ControlCount -1 do
      if (FMemoBand.Controls[Counter] is TQRMemo) then
      begin
        TmpMemo := TQRMemo(FMemoBand.Controls[Counter]);
        for IndexBand := 0 to MAX_CHILD_BAND-1 do
        begin
          NewTmpMemo := TQRMemo(FChildBand[IndexBand].AddPrintable(TQRMemo));
          NewTmpMemo.Left := TmpMemo.Left;
          NewTmpMemo.Font.Size := 8;
          NewTmpMemo.Top := -1;
          NewTmpMemo.Tag := -1;  // to remove any space between child bands
          NewTmpMemo.Height := HEIGHT_MEMO;
          NewTmpMemo.AutoSize := True;
          NewTmpMemo.AutoStretch := False;
          NewTmpMemo.Lines.Clear;
          NewTmpMemo.Width := TmpMemo.Width;
//          NewTmpMemo.Name := 'Memo' + IntToStr(IndexBand) + '_' +  TmpMemo.Name;
          NewTmpMemo.Alignment := TmpMemo.Alignment;
          NewTmpMemo.Parent := FChildBand[IndexBand];
          NewTmpMemo.AlignToBand := TmpMemo.AlignToBand;
          NewTmpMemo.Transparent := TmpMemo.Transparent;
        end;
      end;
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
{    for IndexBand := MAX_CHILD_BAND-1 downto 0 do
    begin
      FChildBand[IndexBand].HasChild := False;
    end; }
  end;
*)
end; // Init

procedure TMemoClass.Free;
begin
  inherited Free;
end; // Free;

procedure TMemoClass.FillMemoChildBand(IndexLines: Integer; ParentBand,
  ChildBand: TQRCustomBand);
var
  PosName, Counter, CountLines: Integer;
  TmpChildMemo, TmpMemo: TQRMemo;
begin
{$IFDEF DEBUG}
  WLog('- ChildBand.ControlCount=' + IntToStr(ChildBand.ControlCount)
    );
{$ENDIF}
  for Counter := 0 to ChildBand.ControlCount -1 do
    if (ChildBand.Controls[Counter] is TQRMemo) then
    begin
      TmpChildMemo := TQRMemo(ChildBand.Controls[Counter]);
      PosName := Pos('_', TmpChildMemo.Name);
{$IFDEF DEBUG}
  WLog('- TmpChildMemo.Name=' + TmpChildMemo.Name);
{$ENDIF}
      TmpMemo := Nil;
      if PosName > 0 then
      // get the name of memo corresponding to parent line
        TmpMemo := GetMemo(Copy(TmpChildMemo.Name, PosName + 1, 100), ParentBand);
      if TmpMemo <> Nil then
      begin
        TmpChildMemo.Lines.Clear;
        TmpChildMemo.Left := TmpMemo.Left;
        TmpChildMemo.Width := TmpMemo.Width;
        TmpChildMemo.AutoSize := TmpMemo.AutoSize;
        for CountLines := 0 to (FMAX_MEMO_LINES - 1) do
          if(TmpMemo.Lines.Count >  IndexLines + CountLines) then
          begin
{$IFDEF DEBUG}
  WLog('- FillMemoChildBand. Add. [' + IntToStr(TmpMemo.Lines.Count) + ']'+
    '[' + IntToStr(IndexLines + CountLines) + ']');
{$ENDIF}
            TmpChildMemo.Lines.Add(TmpMemo.Lines.Strings[IndexLines + CountLines]);
          end;
       end;
    end;
end; // FillMemoChildBand

procedure TMemoClass.GetMaxCountMemo(ParentBand: TQRCustomBand;
  var MaxCountLines, MaxTopMemo: Integer);
var
  Counter: Integer;
begin
  inherited;
  MaxCountLines := 0;
  MaxTopMemo := 0;
  for Counter := 0 to ParentBand.ControlCount -1 do
    if (ParentBand.Controls[Counter] is TQRMemo) then
    begin
      (ParentBand.Controls[Counter] as TQRMemo).AutoStretch := False;
      (ParentBand.Controls[Counter] as TQRMemo).AutoSize := True;
      (ParentBand.Controls[Counter] as TQRMemo).Height := HEIGHT_MEMO;
       MaxCountLines := Max(MaxCountLines,
         (ParentBand.Controls[Counter] as TQRMemo).Lines.Count);
       MaxTopMemo :=
         Max(MaxTopMemo,(ParentBand.Controls[Counter] as TQRMemo).Top);
    end;
{$IFDEF DEBUG}
  WLog('GetMaxCountMemo.' +
    ' ParentBand.ControlCount=' + IntToStr(ParentBand.ControlCount) +
    ' MaxCountLines=' + IntToStr(MaxCountLines) +
    ' MaxTopMemo=' + IntToStr(MaxTopMemo)
    );
{$ENDIF}
end; // GetMaxCountMemo

function TMemoClass.GetMemo(NameMemo: String;
  BandParent: TQRCustomBand): TQRMemo;
var
  Counter: Integer;
begin
  Result := Nil;
  if NameMemo = '' then
    Exit;
  for Counter := 0 to BandParent.ControlCount -1 do
    if (BandParent.Controls[Counter] is TQRPrintable) and
      (BandParent.Controls[Counter] is TQRMemo) then
      if TQRMemo(BandParent.Controls[Counter]).Name = NameMemo then
      begin
        Result := TQRMemo(BandParent.Controls[Counter]);
        Exit;
      end;
end; // GetMemo

procedure TMemoClass.SetHeightMemoBand(PrintBand: Boolean;
  ParentBand: TQRCustomBand);
var
  Save_FMAX_MEMO_LINES, MaxTopMemo, IndexLines, Lines,
  IndexBand, MaxMemoLines: Integer;
begin
{$IFDEF DEBUG}
  WLog('SetHeightMemoBand');
{$ENDIF}
  if not PrintBand then
  begin
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
    Exit;
  end;
  //get maximal lines per memo fields of ParentBand
  GetMaxCountMemo(ParentBand, Lines, MaxTopMemo);
  //if Lines is less than the number printed with parent band then delete all child band
  // there is no need to use them
  if (Lines < FMAX_MEMO_LINES)  then
  begin
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
    SetHeightMemoFields(ParentBand, Lines, MaxTopMemo);
{$IFDEF DEBUG}
  WLog('-> Lines=' + IntToStr(Lines) +
    ' FMAX_MEMO_LINES=' + IntToStr(FMAX_MEMO_LINES) +
    ' MaxTopMemo=' + IntToStr(MaxTopMemo)
    );
{$ENDIF}
    Exit;
  end;

  // check if maximal lines of memo fields is bigger then
  // the covered length by all child band runtime build
  Save_FMAX_MEMO_LINES := 0;
  if Lines > (FMAX_MEMO_LINES + 1) * MAX_CHILD_BAND then
  begin
    // if yes then set the number of lines of one memo with the
    // maximal number allowed to print lines into one page
    // - this is a parameter set in a report on create message
    Save_FMAX_MEMO_LINES := FMAX_MEMO_LINES;
    FMAX_MEMO_LINES := MAX_LINES_ON_PAGE;
  end;
  // set height of fix band of report - ParentBand
  SetHeightMemoFields(ParentBand, FMAX_MEMO_LINES, MaxTopMemo);
  // determine number of child band used further
  if (Lines mod FMAX_MEMO_LINES) <> 0 then
  begin
    Lines := (Lines div FMAX_MEMO_LINES);
    Lines := Lines + 1;
  end
  else
    Lines := (Lines div FMAX_MEMO_LINES);
  Lines := Lines - 1; // number of active band childs - one line was used for parent

   // fill memo of active child bands with Max_memo_lines
  IndexLines := FMAX_MEMO_LINES;
  for IndexBand := 0 to MAX_CHILD_BAND-1 do
  begin
    if IndexBand < Lines then
    begin
      FillMemoChildBand(IndexLines,  ParentBand, FChildBand[IndexBand]);
      GetMaxCountMemo(FChildBand[IndexBand], MaxMemoLines, MaxTopMemo) ;
      SetHeightMemoFields(FChildBand[IndexBand], MaxMemoLines, -2); // MRA:Why is last parameter -2 ?
      IndexLines := IndexLines + FMAX_MEMO_LINES;
    end
    else
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
  end;

// restore number of lines of one memo
  if Save_FMAX_MEMO_LINES <> 0 then
    FMAX_MEMO_LINES := Save_FMAX_MEMO_LINES;
end; // SetHeightMemoBand

procedure TMemoClass.SetHeightMemoFields(ParentBand: TQRCustomBand;
  MaxCountLines, MaxTopMemo: Integer);
var
  TempComponent: TComponent;
  Counter: Integer;
begin
{$IFDEF DEBUG}
  WLog('SetHeightMemoFields');
  WLog('-> ParentBand.ControlCount=' + IntToStr(ParentBand.ControlCount)
    );
{$ENDIF}

  for Counter := 0 to ParentBand.ControlCount -1 do
    if (ParentBand.Controls[Counter] is TQRMemo) then
  //set height for memo of band
    begin
      TempComponent := TQRMemo(ParentBand.Controls[Counter]);
      (TempComponent as TQRMemo).Height := HEIGHT_MEMO * MaxCountLines;
{$IFDEF DEBUG}
  WLog('-> TQRMemo.Height=' + IntToStr((TempComponent as TQRMemo).Height)
    );
{$ENDIF}
    end;
  // set height of band
  ParentBand.Height :=  MaxTopMemo + HEIGHT_MEMO * MaxCountLines;

{$IFDEF DEBUG}
  WLog('-> ParentBand.Height=' + IntToStr(ParentBand.Height) +
    ' MaxTopMemo=' + IntToStr(MaxTopMemo) +
    ' MaxCountLines=' + IntToStr(MaxCountLines)
    );
{$ENDIF}
end; // SetHeightMemoFields

procedure TMemoClass.SetMemoParam(Memo_Lines, Max_Lines_Memo_Page: Integer;
  BandMemo: TQRCustomBand);
begin
  // Done during create of TMemoClass.
(*
  FMemoBand := BandMemo;   // set Band who contains memo fields
  // set Number of lines into a memo field on a child band or bandmemo
  FMAX_MEMO_LINES := Memo_Lines;
  //set number of lines into a memo which had to fit in one page
  FMax_Line_Memo_Page := Max_Lines_Memo_Page;
*)
end; // SetMemoParam

end.
