unit DialogSettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, DBCtrls;

type
  TDialogSettingsF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    CheckBoxShowLevel: TCheckBox;
    CheckBoxShowLevelC: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FShowLevel: Boolean;
    FShowLevelC: Boolean;
    { Private declarations }
  public
    { Public declarations }
    property ShowLevel: Boolean read FShowLevel write FShowLevel;
    property ShowLevelC: Boolean read FShowLevelC write FShowLevelC;
  end;

var
  DialogSettingsF: TDialogSettingsF;

implementation

uses SystemDMT;

{$R *.DFM}

{ TDialogSettingsF }

procedure TDialogSettingsF.btnOkClick(Sender: TObject);
begin
  inherited;
  ShowLevel := CheckBoxShowLevel.Checked;
  ShowLevelC := CheckBoxShowLevelC.Checked;
end;

procedure TDialogSettingsF.FormShow(Sender: TObject);
begin
  inherited;
  CheckBoxShowLevel.Checked := ShowLevel;
  CheckBoxShowLevelC.Checked := ShowLevelC;
end;

end.
