inherited ShiftScheduleF: TShiftScheduleF
  Left = 371
  Top = 177
  Width = 670
  Height = 505
  Caption = 'Shift schedule'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 126
    Width = 654
    Height = 11
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 7
      Width = 652
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Width = 652
      Height = 6
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 345
    Width = 654
    Height = 121
    TabOrder = 3
    object Label7: TLabel
      Left = 8
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Employee'
    end
    object GroupBox2: TGroupBox
      Left = 168
      Top = 12
      Width = 161
      Height = 69
      Caption = 'GroupBox'
      TabOrder = 0
      OnExit = pnlDetailExit
      object LabelW11: TLabel
        Tag = 11
        Left = 2
        Top = 16
        Width = 8
        Height = 13
        Caption = 'M'
      end
      object LabelW12: TLabel
        Tag = 12
        Left = 25
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW13: TLabel
        Tag = 13
        Left = 47
        Top = 16
        Width = 10
        Height = 13
        Caption = 'W'
      end
      object LabelW14: TLabel
        Tag = 14
        Left = 70
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW15: TLabel
        Tag = 15
        Left = 93
        Top = 16
        Width = 6
        Height = 13
        Caption = 'F'
      end
      object LabelW16: TLabel
        Tag = 16
        Left = 115
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object LabelW17: TLabel
        Tag = 17
        Left = 138
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object EditD11: TEdit
        Tag = 11
        Left = 2
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 0
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD12: TEdit
        Tag = 12
        Left = 25
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 1
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD13: TEdit
        Tag = 13
        Left = 47
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 2
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD14: TEdit
        Tag = 14
        Left = 70
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 3
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD15: TEdit
        Tag = 15
        Left = 93
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 4
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD16: TEdit
        Tag = 16
        Left = 115
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 5
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD17: TEdit
        Tag = 17
        Left = 138
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 6
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
    end
    object DBEdit31: TDBEdit
      Left = 64
      Top = 16
      Width = 81
      Height = 21
      DataField = 'EMPLOYEE_NUMBER'
      DataSource = ShiftScheduleDM.DataSourceDetail
      Enabled = False
      TabOrder = 4
    end
    object ButtonCopyFromEmp: TButton
      Left = 8
      Top = 40
      Width = 137
      Height = 25
      Caption = 'Copy from other employee'
      TabOrder = 3
      OnClick = ButtonCopyFromEmpClick
    end
    object GroupBox3: TGroupBox
      Left = 344
      Top = 12
      Width = 153
      Height = 69
      Caption = 'GroupBox'
      TabOrder = 1
      object LabelW21: TLabel
        Tag = 21
        Left = 2
        Top = 16
        Width = 8
        Height = 13
        Caption = 'M'
      end
      object LabelW22: TLabel
        Tag = 22
        Left = 23
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW23: TLabel
        Tag = 23
        Left = 45
        Top = 16
        Width = 10
        Height = 13
        Caption = 'W'
      end
      object LabelW24: TLabel
        Tag = 24
        Left = 66
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW25: TLabel
        Tag = 25
        Left = 87
        Top = 16
        Width = 6
        Height = 13
        Caption = 'F'
      end
      object LabelW26: TLabel
        Tag = 26
        Left = 109
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object LabelW27: TLabel
        Tag = 27
        Left = 130
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object EditD21: TEdit
        Tag = 21
        Left = 2
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 0
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD22: TEdit
        Tag = 22
        Left = 23
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 1
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD23: TEdit
        Tag = 23
        Left = 45
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 2
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD24: TEdit
        Tag = 24
        Left = 66
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 3
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD25: TEdit
        Tag = 25
        Left = 87
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 4
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD26: TEdit
        Tag = 26
        Left = 109
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 5
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD27: TEdit
        Tag = 27
        Left = 130
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 6
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
    end
    object GroupBox4: TGroupBox
      Left = 504
      Top = 12
      Width = 153
      Height = 69
      Caption = 'GroupBox'
      TabOrder = 2
      object LabelW31: TLabel
        Tag = 31
        Left = 2
        Top = 16
        Width = 8
        Height = 13
        Caption = 'M'
      end
      object LabelW32: TLabel
        Tag = 32
        Left = 23
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW33: TLabel
        Tag = 33
        Left = 45
        Top = 16
        Width = 10
        Height = 13
        Caption = 'W'
      end
      object LabelW34: TLabel
        Tag = 34
        Left = 66
        Top = 16
        Width = 6
        Height = 13
        Caption = 'T'
      end
      object LabelW35: TLabel
        Tag = 35
        Left = 87
        Top = 16
        Width = 6
        Height = 13
        Caption = 'F'
      end
      object LabelW36: TLabel
        Tag = 36
        Left = 109
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object LabelW37: TLabel
        Tag = 37
        Left = 130
        Top = 16
        Width = 6
        Height = 13
        Caption = 'S'
      end
      object EditD31: TEdit
        Tag = 31
        Left = 2
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 0
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD32: TEdit
        Tag = 32
        Left = 23
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 1
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD33: TEdit
        Tag = 33
        Left = 45
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 2
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD34: TEdit
        Tag = 34
        Left = 66
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 3
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD35: TEdit
        Tag = 35
        Left = 87
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 4
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD36: TEdit
        Tag = 36
        Left = 109
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 5
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
      object EditD37: TEdit
        Tag = 37
        Left = 130
        Top = 36
        Width = 20
        Height = 21
        MaxLength = 2
        PopupMenu = PopupMenuShift
        TabOrder = 6
        OnContextPopup = EditD11ContextPopup
        OnExit = EditD11Exit
        OnKeyDown = EditKeyDown
        OnKeyPress = EditD11KeyPress
        OnMouseDown = EditD11MouseDown
      end
    end
    object ButtonCopy: TButton
      Left = 8
      Top = 68
      Width = 137
      Height = 25
      Caption = 'Copy from other weeks'
      TabOrder = 5
      OnClick = ButtonCopyClick
    end
    object EditP11: TEdit
      Tag = 11
      Left = 172
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 6
      Text = 'EditP11'
      Visible = False
    end
    object EditP12: TEdit
      Tag = 12
      Left = 195
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 7
      Text = 'EditP12'
      Visible = False
    end
    object EditP13: TEdit
      Tag = 13
      Left = 217
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 8
      Text = 'EditP13'
      Visible = False
    end
    object EditP14: TEdit
      Tag = 14
      Left = 240
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 9
      Text = 'EditP14'
      Visible = False
    end
    object EditP15: TEdit
      Tag = 15
      Left = 263
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 10
      Text = 'EditP15'
      Visible = False
    end
    object EditP16: TEdit
      Tag = 16
      Left = 285
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 11
      Text = 'EditP16'
      Visible = False
    end
    object EditP17: TEdit
      Tag = 17
      Left = 308
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 12
      Text = 'EditP17'
      Visible = False
    end
    object EditP21: TEdit
      Tag = 21
      Left = 346
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 13
      Text = 'EditP21'
      Visible = False
    end
    object EditP22: TEdit
      Tag = 22
      Left = 367
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 14
      Text = 'EditP22'
      Visible = False
    end
    object EditP23: TEdit
      Tag = 23
      Left = 389
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 15
      Text = 'EditP23'
      Visible = False
    end
    object EditP24: TEdit
      Tag = 24
      Left = 410
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 16
      Text = 'EditP24'
      Visible = False
    end
    object EditP25: TEdit
      Tag = 25
      Left = 431
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 17
      Text = 'EditP25'
      Visible = False
    end
    object EditP26: TEdit
      Tag = 26
      Left = 453
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 18
      Text = 'EditP26'
      Visible = False
    end
    object EditP27: TEdit
      Tag = 27
      Left = 474
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 19
      Text = 'EditP27'
      Visible = False
    end
    object EditP31: TEdit
      Tag = 31
      Left = 504
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 20
      Text = 'EditP31'
      Visible = False
    end
    object EditP32: TEdit
      Tag = 32
      Left = 527
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 21
      Text = 'EditP32'
      Visible = False
    end
    object EditP33: TEdit
      Tag = 33
      Left = 549
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 22
      Text = 'EditP33'
      Visible = False
    end
    object EditP34: TEdit
      Tag = 34
      Left = 572
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 23
      Text = 'EditP34'
      Visible = False
    end
    object EditP35: TEdit
      Tag = 35
      Left = 595
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 24
      Text = 'EditP35'
      Visible = False
    end
    object EditP36: TEdit
      Tag = 36
      Left = 617
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 25
      Text = 'EditP36'
      Visible = False
    end
    object EditP37: TEdit
      Tag = 37
      Left = 640
      Top = 88
      Width = 20
      Height = 21
      TabOrder = 26
      Text = 'EditP37'
      Visible = False
    end
    object btnConfirm: TBitBtn
      Left = 8
      Top = 95
      Width = 65
      Height = 25
      Caption = 'Confirm'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 27
      OnClick = btnConfirmClick
    end
    object btnUndo: TBitBtn
      Left = 80
      Top = 95
      Width = 65
      Height = 25
      Caption = 'Undo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 28
      OnClick = btnUndoClick
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 137
    Width = 654
    Height = 208
    inherited spltDetail: TSplitter
      Top = 204
      Width = 652
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 652
      Height = 203
      Bands = <
        item
          Caption = 'Employee'
          Width = 291
        end
        item
          Caption = 'Week'
          Width = 154
        end
        item
          Caption = 'WEEK'
          Width = 162
        end
        item
          Caption = 'WEEK'
          Width = 192
        end>
      DefaultLayout = False
      KeyField = 'EMPLOYEE_NUMBER'
      OnClick = dxDetailGridClick
      OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Nr'
        DisableEditor = True
        Width = 58
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 189
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridColumn
        Tag = 11
        Caption = 'M'
        Width = 21
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D11'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Tag = 12
        Caption = 'T'
        Width = 21
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D12'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Tag = 13
        Caption = 'W'
        Width = 21
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D13'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Tag = 14
        Caption = 'T'
        Width = 21
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D14'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Tag = 15
        Caption = 'F'
        Width = 21
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D15'
      end
      object dxDetailGridColumn8: TdxDBGridColumn
        Tag = 16
        Caption = 'S'
        Width = 22
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D16'
      end
      object dxDetailGridColumn9: TdxDBGridColumn
        Tag = 17
        Caption = 'S'
        Width = 27
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D17'
      end
      object dxDetailGridColumn10: TdxDBGridColumn
        Tag = 21
        Caption = 'M'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D21'
      end
      object dxDetailGridColumn11: TdxDBGridColumn
        Tag = 22
        Caption = 'T'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D22'
      end
      object dxDetailGridColumn12: TdxDBGridColumn
        Tag = 23
        Caption = 'W'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D23'
      end
      object dxDetailGridColumn13: TdxDBGridColumn
        Tag = 24
        Caption = 'T'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D24'
      end
      object dxDetailGridColumn14: TdxDBGridColumn
        Tag = 25
        Caption = 'F'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D25'
      end
      object dxDetailGridColumn15: TdxDBGridColumn
        Tag = 26
        Caption = 'S'
        Width = 22
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D26'
      end
      object dxDetailGridColumn16: TdxDBGridColumn
        Tag = 27
        Caption = 'S'
        Width = 30
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D27'
      end
      object dxDetailGridColumn17: TdxDBGridColumn
        Tag = 31
        Caption = 'M'
        Width = 24
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D31'
      end
      object dxDetailGridColumn18: TdxDBGridColumn
        Tag = 32
        Caption = 'T'
        Width = 27
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D32'
      end
      object dxDetailGridColumn19: TdxDBGridColumn
        Tag = 33
        Caption = 'W'
        Width = 27
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D33'
      end
      object dxDetailGridColumn20: TdxDBGridColumn
        Tag = 34
        Caption = 'T'
        Width = 24
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D34'
      end
      object dxDetailGridColumn21: TdxDBGridColumn
        Tag = 35
        Caption = 'F'
        Width = 24
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D35'
      end
      object dxDetailGridColumn22: TdxDBGridColumn
        Tag = 36
        Caption = 'S'
        Width = 24
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D36'
      end
      object dxDetailGridColumn23: TdxDBGridColumn
        Tag = 37
        Caption = 'S'
        Width = 41
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D37'
      end
      object dxDetailGridColumnSTARTDATE: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'STARTDATE'
        DisableFilter = True
      end
      object dxDetailGridColumnENDDATE: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'ENDDATE'
        DisableFilter = True
      end
      object dxDetailGridColumn26: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P11'
        DisableFilter = True
      end
      object dxDetailGridColumn27: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P12'
        DisableFilter = True
      end
      object dxDetailGridColumn28: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P13'
        DisableFilter = True
      end
      object dxDetailGridColumn29: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P14'
        DisableFilter = True
      end
      object dxDetailGridColumn30: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P15'
        DisableFilter = True
      end
      object dxDetailGridColumn31: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P16'
        DisableFilter = True
      end
      object dxDetailGridColumn32: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P17'
        DisableFilter = True
      end
      object dxDetailGridColumn33: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P21'
        DisableFilter = True
      end
      object dxDetailGridColumn34: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P22'
        DisableFilter = True
      end
      object dxDetailGridColumn35: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P23'
        DisableFilter = True
      end
      object dxDetailGridColumn36: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P24'
        DisableFilter = True
      end
      object dxDetailGridColumn37: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P25'
        DisableFilter = True
      end
      object dxDetailGridColumn38: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P26'
        DisableFilter = True
      end
      object dxDetailGridColumn39: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P27'
        DisableFilter = True
      end
      object dxDetailGridColumn40: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P31'
        DisableFilter = True
      end
      object dxDetailGridColumn41: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P32'
        DisableFilter = True
      end
      object dxDetailGridColumn42: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P33'
        DisableFilter = True
      end
      object dxDetailGridColumn43: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P34'
        DisableFilter = True
      end
      object dxDetailGridColumn44: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P35'
        DisableFilter = True
      end
      object dxDetailGridColumn45: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P36'
        DisableFilter = True
      end
      object dxDetailGridColumn46: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P37'
        DisableFilter = True
      end
    end
  end
  object pnlTeamPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 654
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 654
      Height = 100
      Align = alClient
      Caption = 'Team/Plant/Shift'
      TabOrder = 0
      object Label1: TLabel
        Left = 36
        Top = 19
        Width = 26
        Height = 13
        Caption = 'Team'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label3: TLabel
        Left = 8
        Top = 71
        Width = 22
        Height = 13
        Caption = 'Year'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 147
        Top = 71
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 179
        Top = 71
        Width = 27
        Height = 13
        Caption = 'Week'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 8
        Top = 19
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 280
        Top = 17
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPeriod: TLabel
        Left = 282
        Top = 72
        Width = 40
        Height = 13
        Caption = 'lblPeriod'
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 74
        Top = 42
        Width = 201
        Height = 19
        ColCount = 149
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        ParentCtl3D = False
        TabOrder = 3
        TitleColor = clBtnFace
        OnChange = cmbPlusPlantChange
      end
      object CheckBoxAllTeam: TCheckBox
        Left = 506
        Top = 18
        Width = 73
        Height = 17
        Caption = 'All teams'
        TabOrder = 2
        OnClick = CheckBoxAllTeamClick
      end
      object CheckBoxAllPlant: TCheckBox
        Left = 282
        Top = 44
        Width = 73
        Height = 17
        Caption = 'All plants'
        TabOrder = 4
        OnClick = CheckBoxAllPlantClick
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 74
        Top = 68
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnChange = dxSpinEditYearChange
        MaxValue = 2099
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
      object dxSpinEditWeekFrom: TdxSpinEdit
        Left = 211
        Top = 69
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnChange = dxSpinEditWeekFromChange
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object BitBtnRefresh: TBitBtn
        Left = 556
        Top = 66
        Width = 93
        Height = 25
        Caption = '&Refresh '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = BitBtnRefreshClick
      end
      object CheckBoxPlanInOtherPlants: TCheckBox
        Left = 506
        Top = 42
        Width = 140
        Height = 17
        Caption = 'Plan in other plants'
        TabOrder = 7
        OnClick = CheckBoxPlanInOtherPlantsClick
      end
      object cmbPlusTeamFrom: TComboBoxPlus
        Left = 74
        Top = 16
        Width = 201
        Height = 19
        ColCount = 245
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamFromCloseUp
      end
      object cmbPlusTeamTo: TComboBoxPlus
        Left = 298
        Top = 16
        Width = 201
        Height = 19
        ColCount = 245
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamToCloseUp
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 696
    Top = 80
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    DataSource = nil
    Left = 736
    Top = 80
  end
  inherited StandardMenuActionList: TActionList
    Left = 656
    Top = 40
  end
  inherited dsrcActive: TDataSource
    Left = 696
    Top = 40
  end
  object PopupMenuShift: TPopupMenu
    AutoHotkeys = maManual
    Left = 656
    Top = 75
  end
end
