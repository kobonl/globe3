inherited DialogReportHrsPerDayF: TDialogReportHrsPerDayF
  Left = 419
  Top = 126
  Caption = 'Report hours per day'
  ClientHeight = 525
  ClientWidth = 586
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 586
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 290
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 586
    Height = 404
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 152
    end
    inherited LblEmployee: TLabel
      Top = 152
    end
    inherited LblToPlant: TLabel
      Top = 153
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 156
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 156
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 342
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 41
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [15]
      Left = 8
      Top = 437
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [16]
      Left = 40
      Top = 437
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [17]
      Left = 315
      Top = 437
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [18]
      Left = 8
      Top = 98
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel [19]
      Left = 40
      Top = 98
      Width = 77
      Height = 13
      Caption = 'Absence reason'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel [20]
      Left = 315
      Top = 70
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [21]
      Left = 315
      Top = 97
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel [22]
      Left = 128
      Top = 153
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [23]
      Left = 336
      Top = 153
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [24]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [25]
      Left = 128
      Top = 96
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel [26]
      Left = 336
      Top = 96
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label24: TLabel [27]
      Left = 128
      Top = 438
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [28]
      Left = 336
      Top = 438
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label26: TLabel [29]
      Left = 8
      Top = 204
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label27: TLabel [30]
      Left = 40
      Top = 204
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label28: TLabel [31]
      Left = 315
      Top = 203
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 68
    end
    inherited LblDepartment: TLabel
      Top = 68
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 132
    end
    inherited LblToTeam: TLabel
      Top = 128
    end
    inherited LblStarTeamFrom: TLabel
      Top = 131
    end
    inherited LblTeam: TLabel
      Top = 126
    end
    inherited LblFromTeam: TLabel
      Top = 126
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 124
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 125
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 125
      ColCount = 139
      TabOrder = 10
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 125
      ColCount = 140
      TabOrder = 11
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 127
      TabOrder = 12
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 68
      ColCount = 140
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 68
      ColCount = 141
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      Top = 69
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 152
      TabOrder = 13
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 152
      TabOrder = 14
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 138
      TabOrder = 30
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 139
      TabOrder = 28
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 29
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 176
      TabOrder = 38
      Visible = True
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 521
      Top = 154
      TabOrder = 15
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 31
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 149
      TabOrder = 32
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 150
      TabOrder = 25
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 150
      TabOrder = 26
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 151
      TabOrder = 27
    end
    inherited EditWorkspots: TEdit
      TabOrder = 16
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 154
      TabOrder = 18
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 153
      TabOrder = 17
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 35
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 37
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 34
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 36
    end
    object GroupBoxSelection: TGroupBox
      Left = 336
      Top = 237
      Width = 185
      Height = 164
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 24
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 36
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 8
        Top = 56
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 2
        OnClick = CheckBoxPageDeptClick
      end
      object CheckBoxPageTeam: TCheckBox
        Left = 8
        Top = 76
        Width = 169
        Height = 17
        Caption = 'New page per team'
        TabOrder = 3
        OnClick = CheckBoxPageTeamClick
      end
      object CheckBoxPageEmpl: TCheckBox
        Left = 8
        Top = 96
        Width = 161
        Height = 17
        Caption = 'New page per employee'
        TabOrder = 4
        OnClick = CheckBoxPageEmplClick
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 136
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 6
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 8
        Top = 116
        Width = 169
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 5
      end
    end
    object RadioGroupSort: TRadioGroup
      Left = 128
      Top = 237
      Width = 97
      Height = 164
      Caption = 'Sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Reason'
        'Week')
      ParentFont = False
      TabOrder = 22
    end
    object GroupBoxShow: TGroupBox
      Left = 8
      Top = 237
      Width = 113
      Height = 164
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 16
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 0
        OnClick = CheckBoxDeptClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 136
        Width = 73
        Height = 17
        Caption = 'Employee'
        TabOrder = 2
        OnClick = CheckBoxEmplClick
      end
      object CheckBoxTeam: TCheckBox
        Left = 8
        Top = 76
        Width = 73
        Height = 17
        Caption = 'Team'
        TabOrder = 1
        OnClick = CheckBoxTeamClick
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 125
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 126
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusAbsReasonFrom: TComboBoxPlus
      Left = 120
      Top = 97
      Width = 180
      Height = 19
      ColCount = 126
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsReasonFromCloseUp
    end
    object ComboBoxPlusAbsReasonTo: TComboBoxPlus
      Left = 334
      Top = 97
      Width = 180
      Height = 19
      ColCount = 127
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsReasonToCloseUp
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 521
      Top = 437
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 33
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object CheckBoxAllAbsReason: TCheckBox
      Left = 521
      Top = 95
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      OnClick = CheckBoxAllAbsReasonClick
    end
    object RadioGroupTypeHrs: TRadioGroup
      Left = 232
      Top = 237
      Width = 97
      Height = 164
      Caption = 'Show  hours of'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Production'
        'Salary')
      ParentFont = False
      TabOrder = 23
      OnClick = RadioGroupTypeHrsClick
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 200
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 19
      OnCloseUp = DateFromCloseUp
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 200
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 20
      OnCloseUp = DateToCloseUp
    end
  end
  inherited stbarBase: TStatusBar
    Top = 465
    Width = 586
  end
  inherited pnlBottom: TPanel
    Top = 484
    Width = 586
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 16
  end
  inherited QueryEmplFrom: TQuery
    Left = 192
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 264
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        37040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507294469616C6F675265706F7274487273506572446179462E
        44617461536F75726365456D706C46726F6D104F7074696F6E73437573746F6D
        697A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A69
        6E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E5369
        7A696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B10
        6564676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D65
        64676F43616E496E73657274116564676F43616E4E617669676174696F6E1165
        64676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F
        726473106564676F557365426F6F6B6D61726B7300000F546478444247726964
        436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D
        62657206536F7274656407046373557005576964746802410942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060F454D504C4F
        5945455F4E554D42455200000F546478444247726964436F6C756D6E0F436F6C
        756D6E53686F72744E616D650743617074696F6E060A53686F7274204E616D65
        05576964746802480942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060A53484F52545F4E414D4500000F5464784442477269
        64436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D65
        05576964746803F4000942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060B4445534352495054494F4E00000F546478444247
        726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E06
        0741646472657373055769647468022C0942616E64496E646578020008526F77
        496E6465780200094669656C644E616D6506074144445245535300000F546478
        444247726964436F6C756D6E07436F6C756D6E350743617074696F6E060F4465
        706172746D656E7420636F64650942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060F4445504152544D454E545F434F444500
        000F546478444247726964436F6C756D6E07436F6C756D6E360743617074696F
        6E06095465616D20636F64650942616E64496E646578020008526F77496E6465
        780200094669656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        06040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507274469616C6F675265706F72
        74487273506572446179462E44617461536F75726365456D706C546F104F7074
        696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564
        676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564
        676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F
        7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43
        616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E61
        7669676174696F6E116564676F436F6E6669726D44656C657465126564676F4C
        6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B730000
        0F546478444247726964436F6C756D6E0A436F6C756D6E456D706C0743617074
        696F6E06064E756D62657206536F727465640704637355700557696474680231
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F454D504C4F5945455F4E554D42455200000F546478444247726964436F
        6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A5368
        6F7274206E616D65055769647468024E0942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F
        546478444247726964436F6C756D6E11436F6C756D6E4465736372697074696F
        6E0743617074696F6E06044E616D6505576964746803CC000942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060B4445534352
        495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E41
        6464726573730743617074696F6E060741646472657373055769647468026509
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        06074144445245535300000F546478444247726964436F6C756D6E07436F6C75
        6D6E350743617074696F6E060F4465706172746D656E7420636F64650942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F44
        45504152544D454E545F434F444500000F546478444247726964436F6C756D6E
        07436F6C756D6E360743617074696F6E06095465616D20636F64650942616E64
        496E646578020008526F77496E6465780200094669656C644E616D6506095445
        414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 452
  end
  inherited QueryEmplTo: TQuery
    Left = 392
  end
  object TableAbsReason: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCEREASON_CODE'
    TableName = 'ABSENCEREASON'
    Left = 496
    Top = 23
    object TableAbsReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_ID,'
      '  ABSENCEREASON_CODE,'
      '  DESCRIPTION,'
      '  ABSENCETYPE_CODE'
      'FROM'
      '  ABSENCEREASON'
      'ORDER BY'
      '  ABSENCEREASON_CODE'
      '')
    Left = 296
    Top = 24
  end
end
