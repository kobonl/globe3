object WorkScheduleProcessDM: TWorkScheduleProcessDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 387
  Top = 165
  Height = 603
  Width = 820
  object qryWorkScheduleDetails: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.STARTDATE, WL.*'
      'FROM'
      '  WORKSCHEDULE W INNER JOIN WORKSCHEDULELINE WL ON'
      '    W.WORKSCHEDULE_ID = WL.WORKSCHEDULE_ID'
      '  INNER JOIN EMPLOYEECONTRACT EC ON'
      '    WL.WORKSCHEDULE_ID = EC.WORKSCHEDULE_ID'
      'WHERE'
      '  EC.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  WL.WORKSCHEDULE_ID, WL.WORKSCHEDULELINE_ID'
      ' ')
    Left = 48
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyThis: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 392
    Top = 8
  end
  object qryStandardAvailability: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  T.PLANT_CODE, T.DAY_OF_WEEK, T.SHIFT_NUMBER, T.EMPLOYEE_NUMBER' +
        ','
      '  T.AVAILABLE_TIMEBLOCK_1, T.AVAILABLE_TIMEBLOCK_2,'
      '  T.AVAILABLE_TIMEBLOCK_3, T.AVAILABLE_TIMEBLOCK_4,'
      '  T.AVAILABLE_TIMEBLOCK_5, T.AVAILABLE_TIMEBLOCK_6,'
      '  T.AVAILABLE_TIMEBLOCK_7, T.AVAILABLE_TIMEBLOCK_8,'
      '  T.AVAILABLE_TIMEBLOCK_9, T.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY T INNER JOIN EMPLOYEE E ON'
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.PLANT_CODE = E.PLANT_CODE'
      'ORDER BY'
      '  T.SHIFT_NUMBER, T.DAY_OF_WEEK'
      ' '
      ' '
      ' '
      ' ')
    Left = 48
    Top = 72
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryShiftSchedule: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.SHIFT_SCHEDULE_DATE, T.PLANT_CODE, T.SHIFT_NUMBER'
      'FROM '
      '  SHIFTSCHEDULE T INNER JOIN EMPLOYEE E ON'
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE '
      '  T.PLANT_CODE = E.PLANT_CODE AND'
      
        '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND T.SHIFT_SCHEDULE_DATE' +
        ' >= :SHIFT_SCHEDULE_DATE'
      'ORDER BY '
      '  T.SHIFT_SCHEDULE_DATE'
      ' '
      ' ')
    Left = 48
    Top = 128
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeeAvailability: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE, T.EMPLOYEEAVAILABILITY_DATE, T.SHIFT_NUMBER,'
      '  T.AVAILABLE_TIMEBLOCK_1, T.AVAILABLE_TIMEBLOCK_2,'
      '  T.AVAILABLE_TIMEBLOCK_3, T.AVAILABLE_TIMEBLOCK_4,'
      '  T.AVAILABLE_TIMEBLOCK_5, T.AVAILABLE_TIMEBLOCK_6,'
      '  T.AVAILABLE_TIMEBLOCK_7, T.AVAILABLE_TIMEBLOCK_8,'
      '  T.AVAILABLE_TIMEBLOCK_9, T.AVAILABLE_TIMEBLOCK_10'
      'FROM EMPLOYEEAVAILABILITY T INNER JOIN EMPLOYEE E ON'
      '  T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  T.PLANT_CODE = E.PLANT_CODE AND'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.EMPLOYEEAVAILABILITY_DATE >= :EMPLOYEEAVAILABILITY_DATE'
      'ORDER BY'
      '  T.EMPLOYEEAVAILABILITY_DATE'
      ' '
      ' ')
    Left = 48
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeePlanning: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE, T.EMPLOYEEPLANNING_DATE,'
      '  T.SHIFT_NUMBER, T.DEPARTMENT_CODE, T.WORKSPOT_CODE,'
      '  T.EMPLOYEE_NUMBER'
      'FROM '
      '  EMPLOYEEPLANNING T INNER JOIN EMPLOYEE E ON'
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE '
      ' T.PLANT_CODE = E.PLANT_CODE AND'
      ' T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      ' T.EMPLOYEEPLANNING_DATE >= :EMPLOYEEPLANNING_DATE'
      ' '
      ' ')
    Left = 48
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpInfo: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT E.PLANT_CODE, EC.STARTDATE, EC.ENDDATE '
      'FROM EMPLOYEECONTRACT EC INNER JOIN EMPLOYEE E ON'
      '  EC.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE EC.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  TRUNC(SYSDATE) BETWEEN EC.STARTDATE AND EC.ENDDATE '
      ' ')
    Left = 48
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEMAInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEAVAILABILITY'
      '(PLANT_CODE, EMPLOYEEAVAILABILITY_DATE, SHIFT_NUMBER,'
      'EMPLOYEE_NUMBER, AVAILABLE_TIMEBLOCK_1,'
      'AVAILABLE_TIMEBLOCK_2, AVAILABLE_TIMEBLOCK_3,'
      'AVAILABLE_TIMEBLOCK_4, AVAILABLE_TIMEBLOCK_5,'
      'AVAILABLE_TIMEBLOCK_6, AVAILABLE_TIMEBLOCK_7,'
      'AVAILABLE_TIMEBLOCK_8, AVAILABLE_TIMEBLOCK_9,'
      'AVAILABLE_TIMEBLOCK_10, CREATIONDATE, MUTATIONDATE, MUTATOR)'
      'VALUES(:PLANT_CODE, :EMPLOYEEAVAILABILITY_DATE, :SHIFT_NUMBER,'
      ':EMPLOYEE_NUMBER, :AVAILABLE_TIMEBLOCK_1,'
      ':AVAILABLE_TIMEBLOCK_2, :AVAILABLE_TIMEBLOCK_3,'
      ':AVAILABLE_TIMEBLOCK_4, :AVAILABLE_TIMEBLOCK_5,'
      ':AVAILABLE_TIMEBLOCK_6, :AVAILABLE_TIMEBLOCK_7,'
      ':AVAILABLE_TIMEBLOCK_8, :AVAILABLE_TIMEBLOCK_9,'
      ':AVAILABLE_TIMEBLOCK_10, SYSDATE, SYSDATE, :MUTATOR)'
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 289
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryEMAUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET'
      '  AVAILABLE_TIMEBLOCK_1 = :AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2 = :AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3 = :AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4 = :AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5 = :AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6 = :AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7 = :AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8 = :AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9 = :AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10 = :AVAILABLE_TIMEBLOCK_10,'
      '  MUTATIONDATE = SYSDATE,'
      '  MUTATOR = :MUTATOR'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER'
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 289
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qrySHSMergeXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'MERGE INTO SHIFTSCHEDULE S'
      'USING DUAL'
      'ON (S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE)'
      'WHEN NOT MATCHED THEN'
      '  INSERT VALUES ('
      '    :EMPLOYEE_NUMBER,'
      '    :SHIFT_SCHEDULE_DATE,'
      '    :PLANT_CODE,'
      '    :SHIFT_NUMBER,'
      '    SYSDATE,'
      '    SYSDATE,'
      '    :MUTATOR'
      '     )'
      'WHEN MATCHED THEN'
      '  UPDATE SET'
      '    S.SHIFT_NUMBER = :SHIFT_NUMBER,'
      '    S.PLANT_CODE = :PLANT_CODE,'
      '    S.MUTATIONDATE = SYSDATE,'
      '    S.MUTATOR = :MUTATOR'
      '    '
      '')
    Left = 525
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryEMPDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEPLANNING'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEPLANNING_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '')
    Left = 288
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qrySHSInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTSCHEDULE'
      '('
      '  EMPLOYEE_NUMBER, SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE, SHIFT_NUMBER, CREATIONDATE,'
      '  MUTATIONDATE, MUTATOR'
      ')'
      'VALUES'
      '('
      '  :EMPLOYEE_NUMBER, :SHIFT_SCHEDULE_DATE,'
      '  :PLANT_CODE, :SHIFT_NUMBER, SYSDATE,'
      '  SYSDATE, :MUTATOR'
      ')'
      ''
      ' ')
    Left = 288
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qrySHSUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE SHIFTSCHEDULE'
      'SET PLANT_CODE = :PLANT_CODE,'
      'SHIFT_NUMBER = :SHIFT_NUMBER,'
      'MUTATIONDATE = SYSDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE'
      ' ')
    Left = 288
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEMADelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEAVAILABILITY'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      'EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_NUMBER <> :SHIFT_NUMBER'
      ' '
      ' ')
    Left = 288
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryIllnessMessage: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT T.ILLNESSMESSAGE_STARTDATE, T.ILLNESSMESSAGE_ENDDATE, T.P' +
        'ROCESSED_YN, T.ABSENCEREASON_CODE'
      'FROM ILLNESSMESSAGE T '
      
        'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND T.ILLNESSMESSAGE_' +
        'STARTDATE >= :ILLNESSMESSAGE_STARTDATE'
      'ORDER BY T.ILLNESSMESSAGE_STARTDATE, T.LINE_NUMBER'
      ' ')
    Left = 48
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ILLNESSMESSAGE_STARTDATE'
        ParamType = ptUnknown
      end>
  end
end
