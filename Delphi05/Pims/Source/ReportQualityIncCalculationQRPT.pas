unit ReportQualityIncCalculationQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, Db, Grids, DBGrids, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FPlantFrom, FPlantTo :String;
    FYear, FWeek: Integer;
    FShowSelection, FShowPerDay, FCalculateWithOvertimeBonus: Boolean;
  public
    procedure SetValues(PlantFrom, PlantTo: String; Year, Week: Integer;
      ShowSelection, ShowPerDay: Boolean;
      CalculateWithOvertimeBonus: Boolean);
  end;

  TReportQualityIncCalculationQR = class(TReportBaseF)
    QRBandDetails: TQRBand;
    QRBandHeaderColumns: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBTextEmployeeName: TQRDBText;
    QRDBTextEmployeeNumber: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRBandShowSelection: TQRBand;
    QRLabelPlantFrom: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelYear: TQRLabel;
    QRLabelWeek: TQRLabel;
    QRLabelValPlantFrom: TQRLabel;
    QRLabelValPlantTo: TQRLabel;
    QRLabelValYear: TQRLabel;
    QRLabelValWeek: TQRLabel;
    QRLabelEmployeeTotal: TQRLabel;
    QRLabelTotalTotal: TQRLabel;
    QRLabelBonusTotal: TQRLabel;
    QRLabelENOTotal: TQRLabel;
    QRLabelHRSTotal: TQRLabel;
    QRLabelMistTotal: TQRLabel;
    QRLabelDolarTotal: TQRLabel;
    QRDBTextEmployeeTotal: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    procedure DefaultPrintHrsField(sender: TObject; var Value: String);
    procedure DefaultPrintMistField(sender: TObject; var Value: String);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    StartDate, EndDate: TDateTime;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    function QRSendReportParameters(const PlantFrom, PlantTo: String;
      const Year, Week: Integer; ShowSelection, ShowPerDay: Boolean;
       CalculateWithOvertimeBonus: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure ShowSelection(ShowHide: Boolean);
    procedure ShowPerDayTotal(DayTotal: Boolean);
    procedure FreeMemory; override;
  end;

var
  ReportQualityIncCalculationQR: TReportQualityIncCalculationQR;

implementation

uses ReportQualityIncCalculationDMT, ListProcsFRM, UGlobalFunctions, UPimsConst,
  SystemDMT, UPimsMessageRes;
{$R *.DFM}

{ TReportQualityIncCalculationQR }

procedure TReportQualityIncCalculationQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  QRPTBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  if QRParameters.FShowSelection then
  begin
    QRLabelValPlantFrom.Caption := QRParameters.FPlantFrom;
    QRLabelValPlantTo.Caption := QRParameters.FPlantTo;
    QRLabelValYear.Caption := IntToStr(QRParameters.FYear);
    QRLabelValWeek.Caption := IntToStr(QRParameters.FWeek);
  end;
  ShowSelection(QRParameters.FShowSelection);
end;

function TReportQualityIncCalculationQR.ExistsRecords: Boolean;
var
	DBOk: boolean;
begin
  Screen.Cursor := crSQLWait;

{  DBOk := }ReportQualityIncCalculationDM.DeleteOldQualityRows(QRParameters.
  	FPlantFrom,QRParameters.FPlantTo,QRParameters.FYear,QRParameters.FWeek);
  ReportQualityIncCalculationDM.PopulateReportQuery(QRParameters.FShowPerDay,
  	QRParameters.FPlantFrom,QRParameters.FPlantTo,StartDate);
  if QRParameters.FShowPerDay then
  begin
    qrptBase.DataSet := ReportQualityIncCalculationDM.QueryReportDSDay;
    DBOk := ReportQualityIncCalculationDM.SaveBonus(
    	ReportQualityIncCalculationDM.QueryReportDSDay,QRParameters.FYear,
      	QRParameters.FWeek);
  end
  else
  begin
    qrptBase.DataSet := ReportQualityIncCalculationDM.QueryReportDSTotal;
    DBOk := ReportQualityIncCalculationDM.SaveBonus(
    	ReportQualityIncCalculationDM.QueryReportDSTotal,QRParameters.FYear,
      	QRParameters.FWeek);
  end;
  if not DBOk then
    DisplayMessage(SPimsDBError,mtError,[mbOk]);
  Result := DBOk and (not qrptBase.DataSet.IsEmpty);
  Screen.Cursor := crDefault;
end;

procedure TReportQualityIncCalculationQR.FreeMemory;
begin
  FreeAndNil(FQRParameters);
  inherited;
end;

function TReportQualityIncCalculationQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String; const Year, Week: Integer; ShowSelection,
  	ShowPerDay: Boolean; CalculateWithOvertimeBonus: Boolean): Boolean;
var
  Cmp: Tcomponent;
  day, index: integer;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '','');
  {check if parameters are valid}
  {if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin

  end;}
  Result := True;
  FQRParameters.SetValues(PlantFrom, PlantTo, Year, Week, ShowSelection,
    ShowPerDay, CalculateWithOvertimeBonus);
  StartDate := ListProcsF.DateFromWeek(Year, Week, 1);
  EndDate := StartDate + 6;

  // set label description
  for index := 0 to ComponentCount -1 do
  begin
  	Cmp := Components[index];
    if Cmp is TQRLabel then
    begin
    	for day := 1 to 7 do
        if TQRLabel(Cmp).Caption = Format('Day%d',[day]) then
          TQRLabel(Cmp).Caption := ' ' + SystemDM.GetDayWCode(day);
    end;
  end;
  ShowPerDayTotal(ShowPerDay);
end;

procedure TReportQualityIncCalculationQR.DefaultPrintHrsField(
  sender: TObject; var Value: String);
begin
  try
  Value := IntMin2StringTime(StrToInt(Value),True)+' ';
  except
  	Value := NullTime;
  end;
end;

procedure TQRParameters.SetValues(PlantFrom, PlantTo: String; Year,
	Week: Integer; ShowSelection, ShowPerDay: Boolean;
        CalculateWithOvertimeBonus: Boolean);
begin
  FPlantFrom := PlantFrom;
  FPlantTo := PlantTo;
  FYear := Year;
  FWeek := Week;
  FShowSelection := ShowSelection;
  FShowPerDay := ShowPerDay;
  FCalculateWithOvertimeBonus := CalculateWithOvertimeBonus;
  ReportQualityIncCalculationDM.CalculateWithOvertimeBonus :=
    CalculateWithOvertimeBonus;
end;

procedure TReportQualityIncCalculationQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
end;

procedure TReportQualityIncCalculationQR.ShowSelection(ShowHide: Boolean);
begin
  QRBandShowSelection.Enabled := ShowHide;
end;

procedure TReportQualityIncCalculationQR.ShowPerDayTotal(DayTotal: Boolean);
var
	index: integer;
  cmp: TComponent;
begin
  //Column Header
	for index := 0 to ComponentCount -1 do
  begin
  	cmp := Components[index];
    if cmp is TQRLabel then
    begin
      if TQRLabel(cmp).Tag = 20 then TQRLabel(cmp).Enabled := DayTotal;
      if TQRLabel(cmp).Tag = 10 then TQRLabel(cmp).Enabled := not DayTotal;
    end;
    if cmp is TQRDBText then
    begin
      if TQRDBText(cmp).Tag = 20 then TQRDBText(cmp).Enabled := DayTotal;
      if TQRDBText(cmp).Tag = 10 then TQRDBText(cmp).Enabled := not DayTotal;
    end;
  end;
end;

procedure TReportQualityIncCalculationQR.DefaultPrintMistField(
  sender: TObject; var Value: String);
begin
  Value := Value + ' ';
end;

end.
