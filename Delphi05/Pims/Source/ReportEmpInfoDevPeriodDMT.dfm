inherited ReportEmpInfoDevPeriodDM: TReportEmpInfoDevPeriodDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object qryEmpInfo: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DAYDATE'
      'FROM'
      '  PIVOTDAY'
      'WHERE'
      '  DAYDATE >= :DATEFROM AND'
      '  DAYDATE <= :DATETO'
      '  '
      ''
      ' ')
    Left = 32
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryPivotDayMerge: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'insert into PIVOTDAY'
      'select :DAYDATE from dual'
      'where not exists (select DAYDATE from PIVOTDAY'
      '                  where DAYDATE = :DAYDATE'
      '                 )')
    Left = 40
    Top = 120
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DAYDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DAYDATE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpHeader: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select'
      '  e.employee_number, e.description edescription,'
      '  e.plant_code, p.description pdescription,'
      '  bu.businessunit_code, bu.description budescription,'
      '  id.idcard_number,'
      '  e.contractgroup_code, cg.description cgdescription,'
      '  e.team_code, t.description tdescription,'
      '  e.department_code, d.description ddescription,'
      '  e.startdate, e.enddate, e.is_scanning_yn,'
      
        '  e.cut_of_time_yn, e.calc_bonus_yn, e.firstaid_yn, e.firstaid_e' +
        'xp_date,'
      '  p.country_id,'
      '  p.inscan_margin_early, p.inscan_margin_late,'
      '  p.outscan_margin_early, p.outscan_margin_late'
      'from'
      '  employee e inner join plant p on'
      '    e.plant_code = p.plant_code'
      '  left join department d on'
      '    e.department_code = d.department_code and'
      '    e.plant_code = d.plant_code'
      '  left join businessunit bu on'
      '    bu.plant_code = e.plant_code and'
      '    d.businessunit_code = bu.businessunit_code'
      '  left join idcard id on'
      '    e.employee_number = id.employee_number'
      '  left join contractgroup cg on'
      '    e.contractgroup_code = cg.contractgroup_code'
      '  left join team t on'
      '    e.team_code = t.team_code and'
      '    e.plant_code = t.plant_code'
      'where'
      '  e.employee_number = :employee_number'
      ''
      ' ')
    Left = 40
    Top = 176
    ParamData = <
      item
        DataType = ftInteger
        Name = 'employee_number'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAbsence: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AR.ABSENCEREASON_CODE,'
      '  AR.DESCRIPTION,'
      '  MAX(('
      '    SELECT COUNT(*) FROM ABSENCEHOURPEREMPLOYEE A2 '
      '    WHERE A2.ABSENCEHOUR_DATE >= :DATEFROM AND '
      '    A2.ABSENCEHOUR_DATE <= :DATETO AND '
      '    A2.ABSENCEREASON_ID = A.ABSENCEREASON_ID AND'
      '    A2.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)) ABSDAYS,'
      '  SUM(A.ABSENCE_MINUTE) ABSMIN'
      'FROM '
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON AR ON'
      '    A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID'
      'WHERE '
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  A.ABSENCEHOUR_DATE >= :DATEFROM AND A.ABSENCEHOUR_DATE <= :DAT' +
        'ETO'
      'GROUP BY '
      '  AR.ABSENCEREASON_CODE, AR.DESCRIPTION'
      'ORDER BY '
      '  1, 2'
      ' '
      ' ')
    Left = 40
    Top = 232
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryEmpBalance: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT A.LAST_YEAR, A.NORM, A.USED, A.EARNED, A.ABSTYPE,'
      
        '  (SELECT MIN(NVL(ATC.COUNTER_DESCRIPTION, ATC.DESCRIPTION)) FRO' +
        'M ABSENCETYPEPERCOUNTRY ATC WHERE ATC.COUNTRY_ID = :COUNTRY_ID A' +
        'ND ATC.ABSENCETYPE_CODE = A.ABSTYPE AND ATC.ACTIVE_YN = '#39'Y'#39' AND ' +
        'ATC.COUNTER_ACTIVE_YN = '#39'Y'#39') ABSDESCRIPTION1,'
      
        '  (SELECT MIN(AT2.DESCRIPTION) FROM ABSENCETYPE AT2 WHERE AT2.AB' +
        'SENCETYPE_CODE = A.ABSTYPE) ABSDESCRIPTION2'
      'FROM'
      '('
      
        'SELECT NVL(AT.LAST_YEAR_HOLIDAY_MINUTE,0) LAST_YEAR, NVL(AT.NORM' +
        '_HOLIDAY_MINUTE,0) NORM, NVL(AT.USED_HOLIDAY_MINUTE,0) USED, 0 E' +
        'ARNED, '#39'H'#39' ABSTYPE'
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_TFT_MINUTE,0), 0, NVL(AT.USED_TFT_MINUTE' +
        ',0), NVL(AT.EARNED_TFT_MINUTE,0), '#39'T'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_WTR_MINUTE,0), 0, NVL(AT.USED_WTR_MINUTE' +
        ',0), NVL(AT.EARNED_WTR_MINUTE,0), '#39'W'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_HLD_PREV_YEAR_MINUTE,0), 0, NVL(AT.USED_' +
        'HLD_PREV_YEAR_MINUTE,0), 0, '#39'R'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT 0, NVL(AT.NORM_SENIORITY_HOLIDAY_MINUTE,0), NVL(AT.USED_S' +
        'ENIORITY_HOLIDAY_MINUTE,0), 0, '#39'E'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_ADD_BANK_HLD_MINUTE,0), 0, NVL(AT.USED_A' +
        'DD_BANK_HLD_MINUTE,0), 0, '#39'F'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_SAT_CREDIT_MINUTE,0), 0, NVL(AT.USED_SAT' +
        '_CREDIT_MINUTE,0), 0, '#39'S'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_SHORTWWEEK_MINUTE,0), 0, NVL(AT.USED_SHO' +
        'RTWWEEK_MINUTE,0), NVL(AT.EARNED_SHORTWWEEK_MINUTE,0), '#39'D'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      'UNION'
      
        'SELECT NVL(AT.LAST_YEAR_TRAVELTIME_MINUTE,0), 0, NVL(AT.USED_TRA' +
        'VELTIME_MINUTE,0), NVL(AT.EARNED_TRAVELTIME_MINUTE,0), '#39'N'#39
      'FROM ABSENCETOTAL AT'
      
        'WHERE AT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND AT.ABSENCE_YEAR ' +
        '= :YEAR'
      ') A'
      
        'WHERE NOT (A.LAST_YEAR=0 AND A.NORM=0 AND A.USED=0 AND A.EARNED=' +
        '0)'
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 40
    Top = 280
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryEmpShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE,'
      '  A.SHIFT_NUMBER,'
      '  (SELECT S.DESCRIPTION FROM SHIFT S'
      '   WHERE S.PLANT_CODE = A.PLANT_CODE AND'
      '   S.SHIFT_NUMBER = A.SHIFT_NUMBER) DESCRIPTION,'
      '  COUNT(*) DAYS,'
      '  SUM(A.SALMIN) SALMIN'
      'FROM'
      '('
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  PD.DAYDATE,'
      '  SUM(PIMSDAYOFWEEK(PD.DAYDATE)) DAYOFWEEK,'
      '  MIN(('
      '    SELECT MIN(T.PLANT_CODE)'
      '    FROM PRODHOURPEREMPLOYEE T'
      '    WHERE T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '    T.PRODHOUREMPLOYEE_DATE = PD.DAYDATE)) PLANT_CODE,'
      '  MIN(('
      '    SELECT MIN(T.SHIFT_NUMBER)'
      '    FROM PRODHOURPEREMPLOYEE T'
      '    WHERE T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '    T.PRODHOUREMPLOYEE_DATE = PD.DAYDATE)) SHIFT_NUMBER,'
      '  SUM(('
      '    SELECT SUM(S.SALARY_MINUTE)'
      '    FROM SALARYHOURPEREMPLOYEE S'
      '    WHERE S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '    S.SALARY_DATE = PD.DAYDATE)) SALMIN'
      'FROM'
      'EMPLOYEE E,'
      'PIVOTDAY PD'
      'WHERE PD.DAYDATE >= TO_DATE(:DATEFROM)'
      '  AND PD.DAYDATE <= TO_DATE(:DATETO)'
      '  AND E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'GROUP BY E.EMPLOYEE_NUMBER, PD.DAYDATE'
      'ORDER BY 1, 2  '
      ') A'
      'WHERE A.SALMIN IS NOT NULL'
      'GROUP BY A.PLANT_CODE, A.SHIFT_NUMBER'
      'ORDER BY A.PLANT_CODE, A.SHIFT_NUMBER'
      ''
      ' '
      ' '
      ' ')
    Left = 40
    Top = 336
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryContractInfo: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CONTRACTGROUP_CODE,'
      '  OVERTIME_PER_DAY_WEEK_PERIOD,'
      '  PERIOD_STARTS_IN_WEEK,'
      '  WEEKS_IN_PERIOD,'
      '  WORKDAY_MO_YN,'
      '  WORKDAY_TU_YN,'
      '  WORKDAY_WE_YN,'
      '  WORKDAY_TH_YN,'
      '  WORKDAY_FR_YN,'
      '  WORKDAY_SA_YN,'
      '  WORKDAY_SU_YN,'
      '  NORMALHOURSPERDAY,'
      '  ROUND_TRUNC_SALARY_HOURS,'
      '  ROUND_MINUTE'
      'FROM CONTRACTGROUP'
      'WHERE CONTRACTGROUP_CODE ='
      
        '  (SELECT CONTRACTGROUP_CODE FROM EMPLOYEE WHERE EMPLOYEE_NUMBER' +
        ' = :EMPLOYEE_NUMBER)'
      ' ')
    Left = 136
    Top = 120
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryOvertimeDef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT OD.LINE_NUMBER, OD.HOURTYPE_NUMBER, HT.DESCRIPTION, OD.ST' +
        'ARTTIME, OD.ENDTIME'
      'FROM OVERTIMEDEFINITION OD INNER JOIN HOURTYPE HT ON'
      '  OD.HOURTYPE_NUMBER = HT.HOURTYPE_NUMBER'
      'WHERE OD.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE'
      'ORDER BY OD.LINE_NUMBER'
      ' ')
    Left = 136
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryExceptHourDef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT ED.DAY_OF_WEEK, ED.STARTTIME, ED.ENDTIME, ED.HOURTYPE_NUM' +
        'BER, HT.DESCRIPTION, '
      'ED.OVERTIME_HOURTYPE_NUMBER, '
      
        '(SELECT HT2.DESCRIPTION FROM HOURTYPE HT2 WHERE HT2.HOURTYPE_NUM' +
        'BER = ED.OVERTIME_HOURTYPE_NUMBER) OVERTIME_HOURTYPE_DESCRIPTION'
      'FROM EXCEPTIONALHOURDEF ED INNER JOIN HOURTYPE HT ON'
      '  ED.HOURTYPE_NUMBER = HT.HOURTYPE_NUMBER'
      'WHERE ED.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE'
      'ORDER BY ED.DAY_OF_WEEK')
    Left = 136
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryPlantShiftDept: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DISTINCT T.PLANT_CODE, T.SHIFT_NUMBER, W.DEPARTMENT_CODE'
      'FROM'
      '  TIMEREGSCANNING T INNER JOIN WORKSPOT W ON'
      '    T.PLANT_CODE = W.PLANT_CODE AND'
      '    T.WORKSPOT_CODE = W.WORKSPOT_CODE'
      'WHERE'
      
        '  T.PROCESSED_YN = '#39'Y'#39' AND T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
        'AND'
      '  T.SHIFT_DATE >= :DATEFROM AND T.SHIFT_DATE <= :DATETO'
      '')
    Left = 136
    Top = 336
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
