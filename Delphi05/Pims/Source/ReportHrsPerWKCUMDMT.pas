(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportHrsPerWKCUMDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, DBClient, Provider;

type
  TReportHrsPerWKCUMDM = class(TReportBaseDM)
    QueryProdHour: TQuery;
    DataSourceProd: TDataSource;
    QueryBUPerWK: TQuery;
    dspBUPerWK: TDataSetProvider;
    cdsBUPerWK: TClientDataSet;
    QueryEmpl: TQuery;
    dspEmpl: TDataSetProvider;
    cdsEmpl: TClientDataSet;
    QueryWSWorkingDays: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProdHourFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportHrsPerWKCUMDM: TReportHrsPerWKCUMDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportHrsPerWKCUMDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryProdHour);
end;

procedure TReportHrsPerWKCUMDM.QueryProdHourFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
