(*
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machines-dialog.
  MRA:7-JUN-2011. RV093.5.
  - Filter on plant for teams-per-user.
  MRA:11-JUN-2014 SO-20014450
  - Addition of field Department.
*)
unit MachineDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TMachineDM = class(TGridBaseDM)
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailMACHINE_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableJobCode: TTable;
    TableJobCodePLANT_CODE: TStringField;
    TableJobCodeMACHINE_CODE: TStringField;
    TableJobCodeJOB_CODE: TStringField;
    TableJobCodeBUSINESSUNIT_CODE: TStringField;
    TableJobCodeDESCRIPTION: TStringField;
    TableJobCodeNORM_PROD_LEVEL: TFloatField;
    TableJobCodeBONUS_LEVEL: TFloatField;
    TableJobCodeNORM_OUTPUT_LEVEL: TFloatField;
    TableJobCodeDATE_INACTIVE: TDateTimeField;
    TableJobCodeSHOW_AT_SCANNING_YN: TStringField;
    TableJobCodeSHOW_AT_PRODUCTIONSCREEN_YN: TStringField;
    TableJobCodeCREATIONDATE: TDateTimeField;
    TableJobCodeMUTATIONDATE: TDateTimeField;
    TableJobCodeMUTATOR: TStringField;
    DataSourceJobCode: TDataSource;
    QueryBU: TQuery;
    TableJobCodeBULU: TStringField;
    DataSourceBULU: TDataSource;
    QueryWorkspot: TQuery;
    DataSourceWorkspot: TDataSource;
    QueryInsertJobCode: TQuery;
    QueryWorkspotsPerMachine: TQuery;
    QueryCheckJobCode: TQuery;
    QueryUpdateJobcode: TQuery;
    QueryDeleteJobcode: TQuery;
    QueryMachineJobs: TQuery;
    QueryCheckJobInUse: TQuery;
    TableDetailSHORT_NAME: TStringField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDepartment: TTable;
    TableDetailDEPARTMENTLU: TStringField;
    DataSourceDepartment: TDataSource;
    procedure TableJobCodeNewRecord(DataSet: TDataSet);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableJobCodeAfterPost(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableJobCodeBeforeDelete(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
    procedure TableJobCodeBeforeEdit(DataSet: TDataSet);
    procedure TableJobCodeBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FMachineCode: String;
    FJobCode: String;
    FPlantCode: String;
  public
    { Public declarations }
    procedure AddUpdateJobForWorkspot(ADataSet: TDataSet);
    procedure DeleteJobForWorkspot(ADataSet: TDataSet);
    function CheckWorkspotsForMachine: Boolean;
    function CheckJobsForMachine(ADataSet: TDataSet): Boolean;
    property PlantCode: String read FPlantCode write FPlantCode;
    property MachineCode: String read FMachineCode write FMachineCode;
    property JobCode: String read FJobCode write FJobCode;
  end;

var
  MachineDM: TMachineDM;

implementation

uses
  SystemDMT, UPimsConst, UPimsMessageRes;

{$R *.DFM}

procedure TMachineDM.TableJobCodeNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableJobCode.FieldByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString := 'Y';
  TableJobCode.FieldByName('SHOW_AT_SCANNING_YN').AsString := 'Y';
  if not QueryBU.IsEmpty then
  begin
    QueryBU.First;
    TableJobCode.FieldByName('BUSINESSUNIT_CODE').Value :=
      QueryBU.FieldByName('BUSINESSUNIT_CODE').Value;
  end;
end;

procedure TMachineDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TMachineDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  PlantCode := '';
  MachineCode := '';
  JobCode := '';
  // RV093.5.
  SystemDM.PlantTeamFilterEnable(TableMaster);
  QueryWorkspot.Open;
end;

procedure TMachineDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  QueryWorkspot.Close;
end;

// Add or update job for one or more workspot linked to a machine.
procedure TMachineDM.AddUpdateJobForWorkspot(ADataSet: TDataSet);
  procedure AddUpdateJob(AWorkspotCode: String);
  begin
    with QueryCheckJobCode do
    begin
      ParamByName('PLANT_CODE').AsString :=
        ADataSet.FieldByName('PLANT_CODE').AsString;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString :=
        ADataSet.FieldByName('JOB_CODE').AsString;
      Open;
    end;
    if QueryCheckJobCode.Eof then
    begin
      // Insert job
      try
        with QueryInsertJobCode do
        begin
          ParamByName('PLANT_CODE').AsString :=
            ADataSet.FieldByName('PLANT_CODE').AsString;
          ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
          ParamByName('JOB_CODE').AsString :=
            ADataSet.FieldByName('JOB_CODE').AsString;
          ParamByName('BUSINESSUNIT_CODE').AsString :=
            ADataSet.FieldByName('BUSINESSUNIT_CODE').AsString;
          ParamByName('DESCRIPTION').AsString :=
            ADataSet.FieldByName('DESCRIPTION').AsString;
          ParamByName('NORM_PROD_LEVEL').AsFloat :=
            ADataSet.FieldByName('NORM_PROD_LEVEL').AsFloat;
          ParamByName('BONUS_LEVEL').AsFloat :=
            ADataSet.FieldByName('BONUS_LEVEL').AsFloat;
          ParamByName('NORM_OUTPUT_LEVEL').AsFloat :=
            ADataSet.FieldByName('NORM_OUTPUT_LEVEL').AsFloat;
          ParamByName('INTERFACE_CODE').AsString := '';
          if ADataSet.FieldByName('DATE_INACTIVE').AsDateTime = 0 then
            ParamByName('DATE_INACTIVE').AsString := ''
          else
            ParamByName('DATE_INACTIVE').AsDateTime :=
              ADataSet.FieldByName('DATE_INACTIVE').AsDateTime;
          ParamByName('CREATIONDATE').AsDateTime := Now;
          ParamByName('MUTATIONDATE').AsDateTime := Now;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ParamByName('IGNOREINTERFACECODE_YN').AsString := 'N';
          ParamByName('SHOW_AT_SCANNING_YN').AsString :=
            ADataSet.FieldByName('SHOW_AT_SCANNING_YN').AsString;
          ParamByName('COMPARATION_JOB_YN').AsString := 'N';
          ParamByName('MAXIMUM_DEVIATION').AsFloat := 0;
          ParamByName('COMPARISON_REJECT_YN').AsString := 'N';
          ParamByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString :=
            ADataSet.FieldByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString;
          ExecSQL;
        end;
      except
      end;
    end
    else
    begin
      // Update job
      try
        with QueryUpdateJobCode do
        begin
          ParamByName('PLANT_CODE').AsString :=
            ADataSet.FieldByName('PLANT_CODE').AsString;
          ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
          ParamByName('JOB_CODE').AsString :=
            ADataSet.FieldByName('JOB_CODE').AsString;
          ParamByName('BUSINESSUNIT_CODE').AsString :=
            ADataSet.FieldByName('BUSINESSUNIT_CODE').AsString;
          ParamByName('DESCRIPTION').AsString :=
            ADataSet.FieldByName('DESCRIPTION').AsString;
          ParamByName('NORM_PROD_LEVEL').AsFloat :=
            ADataSet.FieldByName('NORM_PROD_LEVEL').AsFloat;
          ParamByName('BONUS_LEVEL').AsFloat :=
            ADataSet.FieldByName('BONUS_LEVEL').AsFloat;
          ParamByName('NORM_OUTPUT_LEVEL').AsFloat :=
            ADataSet.FieldByName('NORM_OUTPUT_LEVEL').AsFloat;
          if ADataSet.FieldByName('DATE_INACTIVE').AsDateTime = 0 then
            ParamByName('DATE_INACTIVE').AsString := ''
          else
            ParamByName('DATE_INACTIVE').AsDateTime :=
              ADataSet.FieldByName('DATE_INACTIVE').AsDateTime;
          ParamByName('MUTATIONDATE').AsDateTime := Now;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ParamByName('SHOW_AT_SCANNING_YN').AsString :=
            ADataSet.FieldByName('SHOW_AT_SCANNING_YN').AsString;
          ParamByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString :=
            ADataSet.FieldByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString;
          ExecSQL;
        end;
      except
      end;
    end;
  end;
begin
  // For all workspots linked to the machine,
  // add or update the job.
  with QueryWorkspotsPerMachine do
  begin
    ParamByName('PLANT_CODE').AsString :=
      ADataSet.FieldByName('PLANT_CODE').AsString;
    ParamByName('MACHINE_CODE').AsString :=
      ADataSet.FieldByName('MACHINE_CODE').AsString;
    Open;
    while not Eof do
    begin
      AddUpdateJob(
        QueryWorkspotsPerMachine.FieldByName('WORKSPOT_CODE').AsString);
      Next;
    end;
    Close;
  end;
end;

// Delete job for one or more workspots linked to a machine.
procedure TMachineDM.DeleteJobForWorkspot(ADataSet: TDataSet);
  procedure DeleteJob(AWorkspotCode: String);
  begin
    try
      with QueryDeleteJobCode do
      begin
        ParamByName('PLANT_CODE').AsString :=
          ADataSet.FieldByName('PLANT_CODE').AsString;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        ParamByName('JOB_CODE').AsString :=
          ADataSet.FieldByName('JOB_CODE').AsString;
        ExecSQL;
      end;
    except
    end;
  end;
begin
  // For all workspots linked to the machine,
  // delete the job.
  with QueryWorkspotsPerMachine do
  begin
    ParamByName('PLANT_CODE').AsString :=
      ADataSet.FieldByName('PLANT_CODE').AsString;
    ParamByName('MACHINE_CODE').AsString :=
      ADataSet.FieldByName('MACHINE_CODE').AsString;
    Open;
    while not Eof do
    begin
      DeleteJob(
        QueryWorkspotsPerMachine.FieldByName('WORKSPOT_CODE').AsString);
      Next;
    end;
    Close;
  end;
end;

procedure TMachineDM.TableJobCodeAfterPost(DataSet: TDataSet);
begin
  inherited;
  AddUpdateJobForWorkspot(DataSet);
end;

function TMachineDM.CheckWorkspotsForMachine: Boolean;
begin
  Result := not QueryWorkspot.Eof;
end;

function TMachineDM.CheckJobsForMachine(ADataSet: TDataSet): Boolean;
begin
  Result := False;
  try
    with QueryMachineJobs do
    begin
      ParamByName('PLANT_CODE').AsString :=
        ADataSet.FieldByName('PLANT_CODE').AsString;
      ParamByName('MACHINE_CODE').AsString :=
        ADataSet.FieldByName('MACHINE_CODE').AsString;
      Open;
      if not Eof then
        Result := True;
      Close;
    end;
  except
  end;
end;

procedure TMachineDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  // When workspots are linked to the machine, do not allow deletion
  if CheckWorkspotsForMachine then
  begin
    DisplayMessage(SPimsDeleteMachineWorkspotsNotAllowed, mtConfirmation, [mbOK]);
    SysUtils.Abort;
  end
  else
  begin
    // When jobs exist for the machine, do not allow deletion
    if CheckJobsForMachine(DataSet) then
    begin
      DisplayMessage(SPimsDeleteMachineJobsNotAllowed, mtConfirmation, [mbOK]);
      SysUtils.Abort;
    end
    else
      if DisplayMessage(SPimsConfirmDelete, mtConfirmation, [mbYes, mbNo])
        <> mrYes then
        SysUtils.Abort;
  end;
end;

procedure TMachineDM.TableJobCodeBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  if DisplayMessage(SPimsConfirmDelete, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
    SysUtils.Abort
  else
    DeleteJobForWorkspot(DataSet);
end;

procedure TMachineDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  // When workspots are linked to the machine, do not allow changing
  // the Machine Code.
  if MachineCode <> DataSet.FieldByName('MACHINE_CODE').AsString then
  begin
    if CheckWorkspotsForMachine then
    begin
      DisplayMessage(SPimsUpdateMachineCodeNotAllowed, mtConfirmation, [mbOK]);
      SysUtils.Abort;
    end
    else
      SystemDM.DefaultBeforePost(DataSet);
  end
  else
    SystemDM.DefaultBeforePost(DataSet);
end;

procedure TMachineDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  // Store current machine code for comparison later
  MachineCode := DataSet.FieldByName('MACHINE_CODE').AsString;
end;

procedure TMachineDM.TableJobCodeBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  // Store the primary key of the machinejobcode for comparison later
  PlantCode := DataSet.FieldByName('PLANT_CODE').AsString;
  MachineCode := DataSet.FieldByName('MACHINE_CODE').AsString;
  JobCode := DataSet.FieldByName('JOB_CODE').AsString;
end;

procedure TMachineDM.TableJobCodeBeforePost(DataSet: TDataSet);
begin
  inherited;
  // When machine-job is in use, do not allow to change the
  // code of the job.
  with QueryCheckJobInUse do
  begin
    ParamByName('PLANT_CODE').AsString := PlantCode;
    ParamByName('MACHINE_CODE').AsString := MachineCode;
    ParamByName('JOB_CODE').AsString := JobCode;
    Open;
    if not Eof then
    begin
      if JobCode <> DataSet.FieldByName('JOB_CODE').AsString then
      begin
        DisplayMessage(SPimsUpdateMachineJobCodeNotAllowed, mtConfirmation, [mbOK]);
        SysUtils.Abort;
      end
      else
        SystemDM.DefaultBeforePost(DataSet);
    end
    else
      SystemDM.DefaultBeforePost(DataSet);
    Close;
  end;
end;

end.
