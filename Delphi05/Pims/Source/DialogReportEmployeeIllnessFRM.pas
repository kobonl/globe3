(*
  MRA:15-MAY-2013 SO-20014137
  - New report employee illness.
    - Shows who is ill based on today's date.
*)
unit DialogReportEmployeeIllnessFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls, SystemDMT, dxEdLib;

type
  TDialogReportEmployeeIllnessF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    CheckBoxShowOnlyActualIllness: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    CheckBoxPageBU: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }
  end;

var
  DialogReportEmployeeIllnessF: TDialogReportEmployeeIllnessF;

function DialogReportEmployeeIllnessForm: TDialogReportEmployeeIllnessF;

implementation

{$R *.DFM}

uses
  ReportEmployeeIllnessDMT, ReportEmployeeIllnessQRPT,
  ListProcsFRM, UPimsMessageRes;

var
  DialogReportEmployeeIllnessF_HND: TDialogReportEmployeeIllnessF;

function DialogReportEmployeeIllnessForm: TDialogReportEmployeeIllnessF;
begin
  if (DialogReportEmployeeIllnessF_HND = nil) then
  begin
    DialogReportEmployeeIllnessF_HND := TDialogReportEmployeeIllnessF.Create(Application);
    with DialogReportEmployeeIllnessF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportEmployeeIllnessF_HND;
end;


procedure TDialogReportEmployeeIllnessF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportEmployeeIllnessF_HND <> nil) then
  begin
    DialogReportEmployeeIllnessF_HND := nil;
  end;
end;

procedure TDialogReportEmployeeIllnessF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportEmployeeIllnessF.FormShow(Sender: TObject);
begin
  InitDialog(True, False, False, True, False, False, False);
  inherited;

  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
end;

procedure TDialogReportEmployeeIllnessF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportEmployeeIllnessF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportEmployeeIllnessF.FillAll;
begin
  inherited;
//
end;

procedure TDialogReportEmployeeIllnessF.btnOkClick(Sender: TObject);
begin
  inherited;
  if Trunc(DateFrom.Date) > Trunc(DateTo.Date) then
  begin
    DisplayMessage(SDateFromTo, mtInformation, [mbOk]);
    Exit;
  end;
  if ReportEmployeeIllnessQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Trunc(DateFrom.Date),
      Trunc(DateTo.Date),
      CheckBoxShowSelection.Checked,
      CheckBoxExport.Checked,
      CheckBoxShowOnlyActualIllness.Checked)
  then
    ReportEmployeeIllnessQR.ProcessRecords;
end;

procedure TDialogReportEmployeeIllnessF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportEmployeeIllnessDM := CreateReportDM(TReportEmployeeIllnessDM);
  ReportEmployeeIllnessQR := CreateReportQR(TReportEmployeeIllnessQR);
end;

end.
