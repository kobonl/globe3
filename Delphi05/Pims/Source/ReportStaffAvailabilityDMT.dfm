inherited ReportStaffAvailabilityDM: TReportStaffAvailabilityDM
  OldCreateOrder = True
  Left = 244
  Top = 193
  Height = 485
  Width = 741
  inherited qryEmplMinMax: TQuery
    Left = 312
  end
  inherited qryBUMinMax: TQuery
    Left = 296
  end
  inherited qryWeekDelete: TQuery
    Left = 448
  end
  inherited qryWeekInsert: TQuery
    Left = 456
  end
  object QueryEMA: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEMAFilterRecord
    SQL.Strings = (
      'SELECT '
      '  EMA.EMPLOYEEAVAILABILITY_DATE '
      'FROM '
      '  EMPLOYEEAVAILABILITY EMA '
      'WHERE'
      '  EMA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART AND'
      '  EMA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND ')
    Left = 64
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'FDATESTART'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FDATEEND'
        ParamType = ptUnknown
      end>
  end
  object QueryEMP: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.EMPLOYEEPLANNING_DATE, P.EMPLOYEE_NUMBER,'
      '  P.SHIFT_NUMBER,P.PLANT_CODE,'
      '  P.DEPARTMENT_CODE , D.DIRECT_HOUR_YN ,'
      '  P.SCHEDULED_TIMEBLOCK_1, P.SCHEDULED_TIMEBLOCK_2,'
      '  P.SCHEDULED_TIMEBLOCK_3, P.SCHEDULED_TIMEBLOCK_4,'
      '  P.SCHEDULED_TIMEBLOCK_5, P.SCHEDULED_TIMEBLOCK_6,'
      '  P.SCHEDULED_TIMEBLOCK_7, P.SCHEDULED_TIMEBLOCK_8,'
      '  P.SCHEDULED_TIMEBLOCK_9, P.SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING P , DEPARTMENT D'
      'WHERE'
      '  P.EMPLOYEEPLANNING_DATE >= :FDATEMIN AND'
      '  P.EMPLOYEEPLANNING_DATE<= :FDATEMAX AND'
      '  P.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  P.PLANT_CODE = D.PLANT_CODE AND'
      '  ( (P.SCHEDULED_TIMEBLOCK_1 IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_2  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_3  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_4  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_5  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_6  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_7  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_8  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_9  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) OR'
      '    (P.SCHEDULED_TIMEBLOCK_10  IN ( '#39'A'#39' , '#39'B'#39', '#39'C'#39')) )'
      'ORDER BY'
      '  P.EMPLOYEEPLANNING_DATE, P.EMPLOYEE_NUMBER,'
      '  P.SHIFT_NUMBER,P.PLANT_CODE'
      ' ')
    Left = 64
    Top = 80
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATEMAX'
        ParamType = ptUnknown
      end>
    object QueryEMPDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryEMPDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_1: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_1'
      Size = 1
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_2: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_2'
      Size = 1
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_3: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_3'
      Size = 1
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_4: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_4'
      Size = 1
    end
    object QueryEMPEMPLOYEEPLANNING_DATE: TDateTimeField
      FieldName = 'EMPLOYEEPLANNING_DATE'
    end
    object QueryEMPEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryEMPSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object QueryEMPPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_5: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_5'
      Size = 4
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_6: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_6'
      Size = 4
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_7: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_7'
      Size = 4
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_8: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_8'
      Size = 4
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_9: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_9'
      Size = 4
    end
    object QueryEMPSCHEDULED_TIMEBLOCK_10: TStringField
      FieldName = 'SCHEDULED_TIMEBLOCK_10'
      Size = 4
    end
  end
  object ClientDataSetSHIFT: TClientDataSet
    Aggregates = <>
    IndexName = 'DEFAULT_ORDER'
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 392
    Top = 40
  end
  object DataSetProviderShift: TDataSetProvider
    DataSet = QueryShift
    Constraints = True
    Left = 520
    Top = 40
  end
  object QueryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION '
      'FROM '
      '  SHIFT'
      'ORDER BY '
      '  PLANT_CODE, SHIFT_NUMBER')
    Left = 256
    Top = 48
  end
  object QuerySHS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, SHIFT_SCHEDULE_DATE  '
      'FROM '
      '  SHIFTSCHEDULE '
      'WHERE  '
      '  SHIFT_SCHEDULE_DATE >= :FDATEMIN AND '
      '  SHIFT_SCHEDULE_DATE <= :FDATEMAX AND '
      '  SHIFT_NUMBER <> -1 '
      'ORDER BY   EMPLOYEE_NUMBER, SHIFT_SCHEDULE_DATE')
    UpdateMode = upWhereKeyOnly
    Left = 192
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'FDATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FDATEMAX'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetSHS: TClientDataSet
    Aggregates = <>
    IndexName = 'DEFAULT_ORDER'
    Params = <>
    Left = 400
    Top = 112
  end
  object DataSetProviderSHS: TDataSetProvider
    DataSet = QuerySHS
    Constraints = True
    Left = 528
    Top = 112
  end
  object QueryTBPerEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER,'
      '  TIMEBLOCK_NUMBER '
      'FROM '
      '  TIMEBLOCKPEREMPLOYEE '
      'ORDER BY '
      '  EMPLOYEE_NUMBER, PLANT_CODE, SHIFT_NUMBER,'
      '  TIMEBLOCK_NUMBER')
    Left = 200
    Top = 192
  end
  object ClientDataSetTBPerEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTBPerEmpl'
    Left = 400
    Top = 176
  end
  object DataSetProviderTBPerEmpl: TDataSetProvider
    DataSet = QueryTBPerEmpl
    Constraints = True
    Left = 536
    Top = 176
  end
  object QueryABS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ABSENCEREASON_CODE'
      'FROM '
      '  ABSENCEREASON'
      'ORDER BY '
      '  ABSENCEREASON_CODE')
    Left = 288
    Top = 264
  end
  object ClientDataSetAbs: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbs'
    Left = 408
    Top = 256
  end
  object DataSetProviderAbs: TDataSetProvider
    DataSet = QueryABS
    Constraints = True
    Left = 536
    Top = 265
  end
  object TableDept: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'DEPARTMENT'
    Left = 64
    Top = 184
  end
end
