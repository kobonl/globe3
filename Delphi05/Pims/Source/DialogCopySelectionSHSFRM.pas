(*
  Changes:
    MRA: 14-SEP-2009 Order 550470 - Prevent copying to 2099.
      Do not allow to select year 2099 for copying: Give a message about
      'not allowed' and do not continue, when OK-button is pressed.
    MRA: 3-DEC-2009 RV047.1. CLF91109.
      - Allow copying within 2099, from a week-range to another week-range.
      - Abort loop when answer is NoToAll during copy.
    MRA: 11-JAN-2010. RV049.4. Bugfix.
      - Shift Schedule:
        - During copying, answer NoToAll means copying must not be done
          for existing records, but the rest must be copied.
    MRA:21-APR-2011. RV090.1.
    - Add option to Confirm/Undo a copy-action.
    MRA:25-FEB-2014 20011800
    - Final Run System
    - Prevent it is copying data for dates <= last-export-date.
*)

unit DialogCopySelectionSHSFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables, dxExGrEd, dxExELib,
  dxLayout;

type
  TDialogCopySelectionSHSF = class(TDialogSelectionF)
    GroupBoxCopyTo: TGroupBox;
    GroupBoxCopyFrom: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    dxSpinEditWeekFrom: TdxSpinEdit;
    Label4: TLabel;
    dxSpinEditWeekTo: TdxSpinEdit;
    dxSpinEditStartWeek: TdxSpinEdit;
    TableCopy: TTable;
    dxSpinEditYearFrom: TdxSpinEdit;
    Label6: TLabel;
    dxSpinEditYearTo: TdxSpinEdit;
    Label9: TLabel;
    dxSpinEditStartYear: TdxSpinEdit;
    dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit;
    dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout;
    dxDBGridLayoutListEmployeeTo: TdxDBGridLayout;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure dxDBExtLookupEditEmplFromCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure dxDBExtLookupEditEmplToCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure FillEmpl;
  public
    { Public declarations }
    FStartWeek: Integer;
    FYear: Word;
    FCopy : Boolean;
    function CopyEmployee: Integer;
  end;

var
  DialogCopySelectionSHSF: TDialogCopySelectionSHSF;

implementation

{$R *.DFM}
uses ListProcsFRM, SystemDMT, UPimsMessageRes, ShiftScheduleDMT, UPimsConst;

procedure TDialogCopySelectionSHSF.FillEmpl;
begin
  ShiftScheduleDM.QueryEmpl.First;
  ShiftScheduleDM.QueryEmplTo.Last;
end;

procedure TDialogCopySelectionSHSF.FormShow(Sender: TObject);
var
  CurrentYear, CurrentMonth, CurrentDay: Word;
begin
  inherited;
  DecodeDate(Now, CurrentYear, CurrentMonth, CurrentDay);
  dxSpinEditWeekFrom.Value := FStartWeek;
  dxSpinEditWeekTo.Value := FStartWeek;
  dxSpinEditStartWeek.Value := FStartWeek + 1;
  dxSpinEditYearFrom.Value := FYear;
  dxSpinEditYearTo.Value := FYear;
  dxSpinEditStartYear.Value := FYear;
  // MR:10-12-2004
  // If From-year is bigger than currentyear, then
  // assign it to currentyear.
  if FYear > CurrentYear then
  begin
    dxSpinEditStartYear.Value := CurrentYear;
    dxSpinEditStartWeek.Value := FStartWeek;
  end;
  FillEmpl;
end;

procedure TDialogCopySelectionSHSF.FormCreate(Sender: TObject);
begin
  inherited;
  // MR:20-1-2006 Do this later, optimising.
//  TableCopy.Active := True;
end;

procedure TDialogCopySelectionSHSF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TableCopy.Close;
end;

procedure TDialogCopySelectionSHSF.btnOkClick(Sender: TObject);
var
  FromDateMin, FromDateMax: TDateTime;
  ToDateMin, ToDateMax: TDateTime;
  WeekCount, MaxWeeks, ToWeekMax: Integer;
  SkipCount: Integer; // 20011800
begin
  inherited;
  if ((dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value) and
   (dxSpinEditYearFrom.Value = dxSpinEditYearTo.Value)) or
   (dxSpinEditYearFrom.Value > dxSpinEditYearTo.Value) then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbOk]);
    Exit;
  end;

  // MRA:14-SEP-2009 Order 550470.
  // MRA:3-DEC-2009 RV047.1. CLF91109.
  // Copying is not allowed to PIMS_TEMPLATE_YEAR, unless the
  // YearFrom and YearTo are both also PIMS_TEMPLATE_YEAR.
  if (dxSpinEditStartYear.Value = PIMS_TEMPLATE_YEAR) and
    not
    (
      (dxSpinEditYearFrom.Value = PIMS_TEMPLATE_YEAR) and
      (dxSpinEditYearTo.Value = PIMS_TEMPLATE_YEAR)
    ) then
  begin
    DisplayMessage(SPimsTemplateYearCopyNotAllowed, mtInformation, [mbOK]);
    Exit;
  end;

  // MR:10-12-2004 Allow 'from'-date that's later than 'to'-date,
  // from example: fromdate='1-1-2099' and todate='1-1-2004'.

  // Check if dates will cross each other during copy.
  FromDateMin := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekFrom.Value), 1);
  FromDateMax := ListProcsF.DateFromWeek(Round(dxSpinEditYearTo.Value),
    Round(dxSpinEditWeekTo.Value), 7);
  WeekCount := Round(dxSpinEditWeekTo.Value) - Round(dxSpinEditWeekFrom.Value);
  MaxWeeks := ListProcsF.WeeksInYear(Round(dxSpinEditStartYear.Value));
  ToWeekMax := Round(dxSpinEditStartWeek.Value) + WeekCount;
  if ToWeekMax > MaxWeeks then
    ToWeekMax := MaxWeeks;
  ToDateMin :=  ListProcsF.DateFromWeek(Round(dxSpinEditStartYear.Value),
    Round(dxSpinEditStartWeek.Value), 1);
  ToDateMax :=  ListProcsF.DateFromWeek(Round(dxSpinEditStartYear.Value),
    ToWeekMax, 1);
  if (ToDateMin >= FromDateMin) and (ToDateMin <= FromDateMax) or
    (ToDateMax >= FromDateMin) and (ToDateMax <= FromDateMax) then
  begin
    DisplayMessage(SSHSWeekDatesCross, mtInformation, [mbOk]);
    Exit;
  end;
  // MR:10-12-2004 No check for 'to'-date that will before 'from'-date.
(*
  if ((dxSpinEditStartWeek.Value < dxSpinEditWeekTo.Value) and
      (dxSpinEditStartYear.Value = dxSpinEditYearTo.Value)) or
    (dxSpinEditStartYear.Value < dxSpinEditYearTo.Value) then
  begin
    DisplayMessage(SSHSWeekCopy, mtInformation, [mbOk]);
    Exit;
  end;
*)
  // RV090.1. Start a transaction, to make it possible to commit/rollback it
  //          later.
  SystemDM.Pims.StartTransaction;
  SkipCount := CopyEmployee;
  // 20011800
  if SystemDM.UseFinalRun and (SkipCount > 0) then
    DisplayMessage(SPimsFinalRunShiftSchedule, mtInformation, [mbOK])
  else
    DisplayMessage(SCopyFinished, mtInformation, [mbOk]);
  DialogCopySelectionSHSF.Close;
  FCopy := True;
end;

function TDialogCopySelectionSHSF.CopyEmployee: Integer;
var
  DateMin, DateMax, DateCopySHS, DateSHS: TDateTime;
  MyResult, Empl: Integer;
  StartDate, EndDate: TDateTime;
  GoOn: Boolean; // 20011800
  FinalRunExportDate: TDateTime; // 20011800
  SkipCount: Integer; // 2001800
begin
  MyResult := 0;
  SkipCount := 0;
  TableCopy.Open; // MR:20-1-2006 Optimising
  DateMin := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekFrom.Value), 1);
  DateMax := ListProcsF.DateFromWeek(Round(dxSpinEditYearTo.Value),
    Round(dxSpinEditWeekTo.Value), 7);
  with ShiftScheduleDM do
  begin
  // select all employees
    QueryDetail.First;
    while (not QueryDetail.Eof) and
     (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <
      GetIntValue(dxDBExtLookupEditEmplFrom.Text)) do
        QueryDetail.Next;
    while (not QueryDetail.Eof) and
     (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <=
      GetIntValue(dxDBExtLookupEditEmplTo.Text)) do
    begin
// select records from SHS Table
      DateSHS := DateMin;
      Empl := QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      StartDate := QueryDetail.FieldByName('STARTDATE').AsDateTime;
      EndDate := QueryDetail.FieldByName('ENDDATE').AsDateTime;
      DateCopySHS :=  ListProcsF.DateFromWeek(Round(dxSpinEditStartYear.Value),
        Round(dxSpinEditStartWeek.Value), 1);
      while DateSHS <= DateMax do
      begin
        // MR:23-07-2004 Order 550323
        // Don't copy to days the employee is not-active
        if (DateCopySHS >= StartDate) and
          ((DateCopySHS <= EndDate) or (EndDate = 0)) then
        begin
          // 20011800
          GoOn := True;
          // 20011800 Final Run System
          if SystemDM.UseFinalRun then
          begin
            FinalRunExportDate :=
              SystemDM.FinalRunExportDateByEmployee(Empl);
            if FinalRunExportDate <> NullDate then
              if DateCopySHS <= FinalRunExportDate then
              begin
                inc(SkipCount);
                GoOn := False;
              end;
          end;
          if GoOn then
            CopyFunction({TableSHS, }TableCopy, Empl, Empl, DateSHS,
              DateCopySHS, MyResult);
        end;
        // RV047.1 Answer was NoToAll, no reason to go on!
        // RV049.4. Bugfix. Do not break!
//        if Result = NoToAll then
//          Break;
        DateSHS := DateSHS + 1;
        DateCopySHS := DateCopySHS + 1;
      end; // while DateSHS <= DateMax do
      // RV049.4. Bugfix. Do not break!
//      if Result = NoToAll then
//        Break;
      QueryDetail.Next;
    end;{while QueryDetail}
  end;{with}
  Result := SkipCount;
end;

procedure TDialogCopySelectionSHSF.btnCancelClick(Sender: TObject);
begin
  inherited;
  FCopy := False;
end;

// MR:04-12-2003 Set max of weeks and correct the component that handles
//               the week-selection
procedure TDialogCopySelectionSHSF.ChangeDate(
  Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearFrom.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;

  try
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearTo.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;

  try
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditStartYear.IntValue);
    dxSpinEditStartWeek.MaxValue := MaxWeeks;
    if dxSpinEditStartWeek.Value > MaxWeeks then
      dxSpinEditStartWeek.Value := MaxWeeks;
  except
    dxSpinEditStartWeek.Value := 1;
  end;
end;

procedure TDialogCopySelectionSHSF.dxDBExtLookupEditEmplFromCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  if (dxDBExtLookupEditEmplFrom.Text <> '') and
     (dxDBExtLookupEditEmplTo.Text <> '') then
  begin
    if GetIntValue(dxDBExtLookupEditEmplFrom.Text) >
       GetIntValue(dxDBExtLookupEditEmplTo.Text) then
      ShiftScheduleDM.QueryEmplTo.Locate('EMPLOYEE_NUMBER',
        GetIntValue(dxDBExtLookupEditEmplFrom.Text), [] );
  end;
end;

procedure TDialogCopySelectionSHSF.dxDBExtLookupEditEmplToCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  if (dxDBExtLookupEditEmplFrom.Text <> '') and
     (dxDBExtLookupEditEmplTo.Text <> '') then
  begin
    if GetIntValue(dxDBExtLookupEditEmplFrom.Text) >
      GetIntValue(dxDBExtLookupEditEmplTo.Text) then
        ShiftScheduleDM.QueryEmpl.Locate('EMPLOYEE_NUMBER',
          GetIntValue(dxDBExtLookupEditEmplTo.Text), [] );
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogCopySelectionSHSF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
