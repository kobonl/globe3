inherited TypeHourPerCountryDM: TTypeHourPerCountryDM
  OldCreateOrder = True
  Left = 306
  Top = 118
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    DatabaseName = 'Pims'
    TableName = 'COUNTRY'
    object TableMasterCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableMasterEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Size = 6
    end
    object TableMasterCODE: TStringField
      FieldName = 'CODE'
      Required = True
      Size = 3
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
  end
  inherited TableDetail: TTable
    IndexName = 'XPKHOURTYPEPERCOUNTRY'
    MasterFields = 'COUNTRY_ID'
    TableName = 'HOURTYPEPERCOUNTRY'
    Left = 84
    Top = 172
    object TableDetailCOUNTRY_ID: TIntegerField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
      Required = True
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailBONUS_PERCENTAGE: TFloatField
      FieldKind = fkLookup
      FieldName = 'BONUS_PERCENTAGE'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'BONUS_PERCENTAGE'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailMINIMUM_WAGE: TWordField
      FieldKind = fkLookup
      FieldName = 'MINIMUM_WAGE'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'MINIMUM_WAGE'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailOVERTIME_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'OVERTIME_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'OVERTIME_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailCOUNT_DAY_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'COUNT_DAY_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'COUNT_DAY_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailIGNORE_FOR_OVERTIME_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'IGNORE_FOR_OVERTIME_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailWAGE_BONUS_ONLY_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'WAGE_BONUS_ONLY_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'WAGE_BONUS_ONLY_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailTIME_FOR_TIME_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'TIME_FOR_TIME_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'TIME_FOR_TIME_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailBONUS_IN_MONEY_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'BONUS_IN_MONEY_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'BONUS_IN_MONEY_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailADV_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'ADV_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'ADV_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailSATURDAY_CREDIT_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'SATURDAY_CREDIT_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'SATURDAY_CREDIT_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableDetailTRAVELTIME_YN: TStringField
      FieldKind = fkLookup
      FieldName = 'TRAVELTIME_YN'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'TRAVELTIME_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 1
      Lookup = True
    end
    object TableDetailEXPORT_OVERTIME_YN: TStringField
      FieldName = 'EXPORT_OVERTIME_YN'
      Size = 1
    end
    object TableDetailEXPORT_PAYROLL_YN: TStringField
      FieldName = 'EXPORT_PAYROLL_YN'
      Size = 1
    end
    object TableDetailEXPORT_OVERTIME_YN_HT: TStringField
      FieldKind = fkLookup
      FieldName = 'EXPORT_OVERTIME_YN_HT'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'EXPORT_OVERTIME_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 1
      Lookup = True
    end
    object TableDetailEXPORT_PAYROLL_YN_HT: TStringField
      FieldKind = fkLookup
      FieldName = 'EXPORT_PAYROLL_YN_HT'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'EXPORT_PAYROLL_YN'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 1
      Lookup = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Top = 172
  end
  object QueryDetail_: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'select * from '
      'hourtype h, hourtypepercountry hc'
      'where '
      '  h.hourtype_number = hc.hourtype_number and'
      '  hc.country_id = :country_id'
      '')
    Left = 88
    Top = 240
    ParamData = <
      item
        DataType = ftFloat
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
    object QueryDetail_HOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
      Origin = 'HOURTYPE.HOURTYPE_NUMBER'
    end
    object QueryDetail_DESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'HOURTYPE.DESCRIPTION'
      Size = 30
    end
    object QueryDetail_CREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Origin = 'HOURTYPE.CREATIONDATE'
    end
    object QueryDetail_BONUS_PERCENTAGE: TFloatField
      FieldName = 'BONUS_PERCENTAGE'
      Origin = 'HOURTYPE.BONUS_PERCENTAGE'
    end
    object QueryDetail_OVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Origin = 'HOURTYPE.OVERTIME_YN'
      Size = 1
    end
    object QueryDetail_MUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Origin = 'HOURTYPE.MUTATIONDATE'
    end
    object QueryDetail_MUTATOR: TStringField
      FieldName = 'MUTATOR'
      Origin = 'HOURTYPE.MUTATOR'
    end
    object QueryDetail_COUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Origin = 'HOURTYPE.COUNT_DAY_YN'
      Size = 1
    end
    object QueryDetail_EXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Origin = 'HOURTYPE.EXPORT_CODE'
      Size = 7
    end
    object QueryDetail_IGNORE_FOR_OVERTIME_YN: TStringField
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Origin = 'HOURTYPE.IGNORE_FOR_OVERTIME_YN'
      Size = 1
    end
    object QueryDetail_MINIMUM_WAGE: TFloatField
      FieldName = 'MINIMUM_WAGE'
      Origin = 'HOURTYPE.MINIMUM_WAGE'
    end
    object QueryDetail_WAGE_BONUS_ONLY_YN: TStringField
      FieldName = 'WAGE_BONUS_ONLY_YN'
      Origin = 'HOURTYPE.WAGE_BONUS_ONLY_YN'
      Size = 1
    end
    object QueryDetail_EXPORTVL_NUMBER: TIntegerField
      FieldName = 'EXPORTVL_NUMBER'
      Origin = 'HOURTYPE.EXPORTVL_NUMBER'
    end
    object QueryDetail_TIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Origin = 'HOURTYPE.TIME_FOR_TIME_YN'
      Size = 1
    end
    object QueryDetail_BONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Origin = 'HOURTYPE.BONUS_IN_MONEY_YN'
      Size = 1
    end
    object QueryDetail_ADV_YN: TStringField
      FieldName = 'ADV_YN'
      Origin = 'HOURTYPE.ADV_YN'
      Size = 1
    end
    object QueryDetail_SATURDAY_CREDIT_YN: TStringField
      FieldName = 'SATURDAY_CREDIT_YN'
      Origin = 'HOURTYPE.SATURDAY_CREDIT_YN'
      Size = 1
    end
    object QueryDetail_COUNTRY_ID: TIntegerField
      FieldName = 'COUNTRY_ID'
      Origin = 'HOURTYPE.SATURDAY_CREDIT_YN'
    end
    object QueryDetail_HOURTYPE_NUMBER_1: TIntegerField
      FieldName = 'HOURTYPE_NUMBER_1'
      Origin = 'HOURTYPE.SATURDAY_CREDIT_YN'
    end
  end
  object TableHourType: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 316
    Top = 172
    object TableHourTypeHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
      Required = True
    end
    object TableHourTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourTypeBONUS_PERCENTAGE: TFloatField
      FieldName = 'BONUS_PERCENTAGE'
      Required = True
    end
    object TableHourTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
    object TableHourTypeMINIMUM_WAGE: TFloatField
      FieldName = 'MINIMUM_WAGE'
    end
    object TableHourTypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeIGNORE_FOR_OVERTIME_YN: TStringField
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeWAGE_BONUS_ONLY_YN: TStringField
      FieldName = 'WAGE_BONUS_ONLY_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeADV_YN: TStringField
      FieldName = 'ADV_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeSATURDAY_CREDIT_YN: TStringField
      FieldName = 'SATURDAY_CREDIT_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeTRAVELTIME_YN: TStringField
      FieldName = 'TRAVELTIME_YN'
      Required = True
      Size = 1
    end
    object TableHourTypeEXPORT_OVERTIME_YN: TStringField
      FieldName = 'EXPORT_OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeEXPORT_PAYROLL_YN: TStringField
      FieldName = 'EXPORT_PAYROLL_YN'
      Size = 1
    end
  end
end
