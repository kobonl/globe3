(*
  MRA:17-NOV-2014 20014826
  - Log in DB
*)
unit ErrorLogFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, ComCtrls, SystemDMT, StdCtrls, dxEditor, dxExEdtr,
  dxDBEdtr, dxDBELib, dxDBTLCl, dxGrClms, dxEdLib, absDateTimePicker;

type
  TErrorLogF = class(TGridBaseF)
    pnlMasterSelection: TPanel;
    Label3: TLabel;
    Label2: TLabel;
    CmbPriority: TComboBox;
    btnRefresh: TButton;
    dxMasterGridColumn1: TdxDBGridColumn;
    dxDetailGridColumnTimestamp: TdxDBGridColumn;
    dxDetailGridColumnSource: TdxDBGridColumn;
    dxDetailGridColumnUser: TdxDBGridColumn;
    dxDetailGridColumnPriority: TdxDBGridColumn;
    GBoxMessage: TGroupBox;
    dxDBMemo1: TdxDBMemo;
    dxDetailGridColumnCalcMessage: TdxDBGridColumn;
    DateEditFrom: TDateTimePicker;
    procedure FormDestroy(Sender: TObject);
    procedure btnRefreshClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure SetDefaultValues;
    procedure ChangeFilter;
  public
    { Public declarations }
  end;

function ErrorLogF: TErrorLogF;

implementation

{$R *.DFM}

uses
  ErrorLogDMT;

var
  ErrorLogF_HND: TErrorLogF;

function ErrorLogF: TErrorLogF;
begin
  if (ErrorLogF_HND = nil) then
  begin
    ErrorLogF_HND := TErrorLogF.Create(Application);
    ErrorLogDM :=
      ErrorLogF_HND.CreateFormDM(TErrorLogDM);
    ErrorLogF_HND.dxMasterGrid.DataSource := ErrorLogDM.DataSourceMaster;
    ErrorLogF_HND.dxDetailGrid.DataSource := ErrorLogDM.DataSourceDetail;
    ErrorLogF_HND.SetDefaultValues;
  end;
  Result := ErrorLogF_HND;
end;

procedure TErrorLogF.FormDestroy(Sender: TObject);
begin
  inherited;
  ErrorLogF_HND := nil;
end;

procedure TErrorLogF.btnRefreshClick(Sender: TObject);
begin
  inherited;
  ChangeFilter;
end;

procedure TErrorLogF.SetDefaultValues;
begin
  SetNotEditableNow;
  DateEditFrom.DateTime := Date;
  CmbPriority.ItemIndex := 0;
  ChangeFilter;
end;

procedure TErrorLogF.ChangeFilter;
var
  ADataFilter: TDataFilter;
begin
  ADataFilter.ComputerName := ErrorLogDM.TableMasterCOMPUTER_NAME.AsString;
  ADataFilter.StartDate := Trunc(DateEditFrom.DateTime);
  ADataFilter.EndDate := Trunc(DateEditFrom.DateTime + 1);
  if CmbPriority.ItemIndex <> -1 then
    ADataFilter.Priority := CmbPriority.ItemIndex
  else
    ADataFilter.Priority := 1;
  ErrorLogDM.ScanFilter := ADataFilter;
end;

procedure TErrorLogF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

end.
