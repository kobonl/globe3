(*
  MRA:25-SEP-2009 RV035.1.
    - Added 2 absence type for export payroll ADP;
      'M' = Maternity leave (zwangerschapsverlof)
      'A' = Lay days (wachtdagen)
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict information by user (team-per-user).
  SO:04-AUG-2010 RV067.5. 550497
    Export Attentia
  MRA:3-SEP-2010. RV067.MRA.28 Bugfix.
  - New counters made for export attentia are not initialized.
  - Some title-fields were not initialized.
  - Use ChildBand to show extra fields for Attentia
  MRA:13-OCT-2010 RV071.14. 550497 Bugfixing/Changes.
  - Not all hours export exported for Attentia.
    Reason: Problem with HourTypes-Per-Country.
  MRA:1-JUN-2012. SO-20013169 Travel time
  - Addition for travel time (day counter) with
    absence type code = 'N'.
*)

unit ReportExportPayrollQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FYear, FSort, FPeriod, FWeekFrom, FWeekTo, FMonth: Integer;
    FInitDate, FFinDate: TDateTime;
  public
    procedure SetValues(Year, Sort, Period, WeekFrom, WeekTo, Month: Integer); overload;
    procedure SetValues(InitDate, FinDate: TDateTime); overload;
  end;

  TReportExportPayrollQR = class(TReportBaseF)
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabelHrs: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLabel16: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelWeek: TQRLabel;
    QRLabelToWeek: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabelFromWeek: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRGroupHDEmpl: TQRGroup;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabelPeriod: TQRLabel;
    QRLabelPeriodNr: TQRLabel;
    QueryEmpl: TQuery;
    QRMemoHourType: TQRMemo;
    QRMemoHourDesc: TQRMemo;
    QRMemoHrs: TQRMemo;
    QRBandFooterEmpl: TQRBand;
    QRLabel5: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabelWKDays: TQRLabel;
    QRLabelIllDays: TQRLabel;
    QRLabelHolDays: TQRLabel;
    QRLabelPaidDays: TQRLabel;
    QRLabelUnpaidDays: TQRLabel;
    QRLabelWKRDays: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabelLabour: TQRLabel;
    QRLabel3: TQRLabel;
    QRMemoPayment: TQRMemo;
    QRLabel15: TQRLabel;
    QRLabelBank: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabelTFT: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabelMat: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabelLay: TQRLabel;
    QRLabelInitDate: TQRLabel;
    QRLabelFinDate: TQRLabel;
    QRChildBandFooterEmpl: TQRChildBand;
    QRLabel21: TQRLabel;
    QRLabelHolPrev: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabelHolSen: TQRLabel;
    QRLabelHolAddBank: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabelSatCred: TQRLabel;
    QRLabelHolBnkRsv: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabelShorterWeek: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabelTravel: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandHDBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelFromWeekPrint(sender: TObject; var Value: String);
    procedure QRLabelToWeekPrint(sender: TObject; var Value: String);
    procedure QRLabelWeekToPrint(sender: TObject; var Value: String);
    procedure QRLabelWeekFromPrint(sender: TObject; var Value: String);
    procedure QRLabelWeekPrint(sender: TObject; var Value: String);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure QRChildBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    FAttentia: Boolean;
    procedure EnableForAttentia;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FEmpl, FContrEmpl, FIndexEmpl: Integer;
    function QRSendReportParameters(const PlantFrom, PlantTo, EmployeeFrom,
      EmployeeTo: String;
      const Year, Sort, Period, WeekFrom, WeekTo, Month: Integer): Boolean; overload;
    //RV067.5.
    function QRSendReportParameters(const PlantFrom,
      PlantTo, EmployeeFrom, EmployeeTo: String;
      const InitDate, FinDate: TDateTime): Boolean; overload;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure PrintHourType;
    procedure PrintTotalDays;
  end;

var
  ReportExportPayrollQR: TReportExportPayrollQR;

implementation

{$R *.DFM}

uses
  SystemDMT,
  UPimsMessageRes,
  UPimsConst,
  DialogExportPayrollFRM, DialogExportPayrollDMT;

procedure TQRParameters.SetValues(Year, Sort, Period, WeekFrom,
  WeekTo, Month: Integer);
begin
  FYear := Year;
  FSort := Sort;
  FPeriod := Period;
  FWeekFrom := WeekFrom;
  FWeekTo := WeekTo;
  FMonth := Month;
end;

function TReportExportPayrollQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo: String;
  const Year, Sort, Period, WeekFrom, WeekTo, Month: Integer): Boolean;
begin
  FAttentia := False;
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, Sort, Period, WeekFrom, WeekTo, Month);
  end;
  SetDataSetQueryReport(QueryEmpl);
  EnableForAttentia;
end;

procedure TReportExportPayrollQR.EnableForAttentia;
begin
  // RV067.MRA.28
  QRChildBandFooterEmpl.Enabled := FAttentia;
{
  if FAttentia then
    QRBandFooterEmpl.Height := 330
  else
    QRBandFooterEmpl.Height := 234;
}

  //RV067.5.
  QRLabelPeriodNr .Visible := not FAttentia;
  QRLabelFromWeek .Visible := not FAttentia;
  QRLabelWeek     .Visible := not FAttentia;
  QRLabelWeekFrom .Visible := not FAttentia;
  QRLabelToWeek   .Visible := not FAttentia;
  QRLabelWeekTo   .Visible := not FAttentia;

  QRLabelInitDate    .Visible := FAttentia;
  QRLabelFinDate     .Visible := FAttentia;
  QRLabelHolPrev     .Visible := FAttentia;
  QRLabelHolSen      .Visible := FAttentia;
  QRLabelHolAddBank  .Visible := FAttentia;
  QRLabelSatCred     .Visible := FAttentia;
  QRLabelHolBnkRsv   .Visible := FAttentia;
  QRLabelShorterWeek .Visible := FAttentia;

  QRLabel21 .Visible := FAttentia;
  QRLabel25 .Visible := FAttentia;
  QRLabel27 .Visible := FAttentia;
  QRLabel29 .Visible := FAttentia;
  QRLabel31 .Visible := FAttentia;
  QRLabel33 .Visible := FAttentia;
end;

function TReportExportPayrollQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo: String;
  const InitDate, FinDate: TDateTime): Boolean;
begin
  FAttentia := True;
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(InitDate, FinDate);
  end;
  SetDataSetQueryReport(QueryEmpl);
  EnableForAttentia;
end;

function TReportExportPayrollQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;

{initialize variables}
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999';
  end;
  QueryEmpl.Active := False;
  QueryEmpl.UniDirectional := False;
  QueryEmpl.SQL.Clear;
  // RV050.8. Added PLANT_CODE for filtering.
  SelectStr :=
    'SELECT ' +
    '  PLANT_CODE, EMPLOYEE_NUMBER, DESCRIPTION ' +
    'FROM ' +
    '  EMPLOYEE ' +
    'WHERE ' +
    '  PLANT_CODE  >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom)+ '''' +
    '  AND PLANT_CODE <= '''+ DoubleQuote(QRBaseParameters.FPlantTo) + '''' +
    '  AND EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom +
    '  AND EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo +
    'ORDER BY ' +
    '  EMPLOYEE_NUMBER ';
  QueryEmpl.SQL.Add(UpperCase(SelectStr));
    if not QueryEmpl.Prepared then
      QueryEmpl.Prepare;
 //  DialogExportPayrollDM.QueryEmpl.Active := True;
    QueryEmpl.Open;
    {open all linked datasets}
    Result := True;
    if (QueryEmpl.RecordCount = 0) or
     ((DialogExportPayrollDM.FEmplSort.Count = 0)  and
      (DialogExportPayrollDM.FEmpQuaranteedHrs.Count = 0) and
      (DialogExportPayrollDM.FEmpHourSick.Count = 0) and
      (DialogExportPayrollDM.FEmpExtraPayment.Count = 0)) then
      Result := False;
    {check if report is empty}

  Screen.Cursor := crDefault;
end;

procedure TReportExportPayrollQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end;
end;

procedure TReportExportPayrollQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportExportPayrollQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  if qrptBase.DataSet = Nil then
    qrptBase.DataSet := QueryEmpl;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
end;

procedure TReportExportPayrollQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  if FAttentia then
  begin
    QRLabelInitDate.Caption := FormatDateTime('yyyy-mm-dd', QRParameters.FInitDate);
    QRLabelFinDate.Caption  := FormatDateTime('yyyy-mm-dd', QRParameters.FFinDate);
    QRLabelPeriodNr.Caption := '';
    QRLabelWeek.Caption := '';
  end
  else
  begin
    // RV067.MRA.28
    QRLabelInitDate.Caption := '';
    QRLabelFinDate.Caption  := '';
    if QRParameters.FSort = 0 then
    begin
      QRLabelPeriodNr.Caption := IntToStr(QRParameters.FPeriod);
      QRLabelPeriod.Caption := SPeriodReport;
      QRLabelFromWeek.Visible := True;
      QRLabelWeek.Caption := SHeaderWeekAbsRsn;
      QRLabelWeekFrom.Caption := SRepExpPayroll;

      QRLabelWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
      QRLabelWeekTo.Caption := IntToStr(QRParameters.FWeekTo);
    end
    else
    begin
      QRLabelPeriodNr.Caption := IntToStr(QRParameters.FMonth);
      QRLabelPeriod.Caption := SMonthReport;
      QRLabelFromWeek.Caption := '';
      QRLabelWeek.Caption := '';
      QRLabelWeekFrom.Caption := '';
      QRLabelToWeek.Caption := '';
      QRLabelWeekTo.Caption := '';
    end;
  end;
end;

procedure TReportExportPayrollQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FEmpl := QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FIndexEmpl := DialogExportPayrollDM.FEmplSort.IndexOf(IntToStr(FEmpl));
  FContrEmpl := DialogExportPayrollDM.FEmpQuaranteedHrs.IndexOf(IntToStr(FEmpl));
  PrintBand := (FIndexEmpl >= 0) or (FContrEmpl >= 0) or
    (DialogExportPayrollDM.FEmpHourSick.IndexOf(IntToStr(FEmpl)) >= 0) or
    (DialogExportPayrollDM.FEmpExtraPayment.IndexOf(IntToStr(FEmpl)) >= 0);
  if PrintBand then
    PrintHourType;
end;

procedure TReportExportPayrollQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  PrintBand := (FIndexEmpl >= 0) or (FContrEmpl >= 0)or
    (DialogExportPayrollDM.FEmpHourSick.IndexOf(IntToStr(FEmpl)) >= 0) or
    (DialogExportPayrollDM.FEmpExtraPayment.IndexOf(IntToStr(FEmpl)) >= 0);
  if PrintBand then
    PrintTotalDays;
end;

procedure TReportExportPayrollQR.PrintHourType;
var
  HourType, Min, IndexHourType, p, IndexEmpl, IndexOfWage, IndexPayment: Integer;
  AmountExport: Real;
  DescTypeHour, StrTmp, ExportCode,  Wage_Bonus_YN: String;
  ExportCodeDummy: String;
//  CountryID: Integer;
  // RV071.14. CountryID
  function MyCountryID(AEmpl: Integer): Integer;
  begin
    Result := -1;
     if DialogExportPayrollDM.ClientDataSetEmpl.FindKey([AEmpl]) then
       Result := DialogExportPayrollDM.ClientDataSetEmpl.
           FieldByName('COUNTRY_ID').AsInteger;
  end;
begin
  inherited;
  QRMemoHourType.Lines.Clear;
  QRMemoHourDesc.Lines.Clear;
  QRMemoHrs.Lines.Clear;
  QRMemoPayment.Lines.Clear;
  for IndexHourType := 0 to (DialogExportPayrollDM.FEmplHourType.Count -1) do
  begin
    if (IndexHourType mod 2) = 0 then
      if DialogExportPayrollF.IsEmpl(DialogExportPayrollDM.FEmplHourType,
        IndexHourType, FEmpl, HourType,
        Min, Wage_Bonus_YN, ExportCodeDummy) then
      begin
        // RV071.14. CountryID
        if DialogExportPayrollDM.HourTypeFind(HourType, MyCountryID(FEmpl))
          {DialogExportPayrollDM.ClientDataSetHourType.FindKey([HourType])} then
        begin
          DescTypeHour := FillSpaces(DialogExportPayrollDM.ClientDataSetHourType.
              FieldByName('DESCRIPTION').AsString, 30);
          QRMemoHourType.Lines.Add(IntToStr(HourType));
          QRMemoHourDesc.Lines.Add(DescTypeHour);

          IndexOfWage := DialogExportPayrollDM.FEmpHourTypeWage.IndexOf(
            IntToStr(FEmpl) + CHR_SEP + IntToStr(HourType) + CHR_SEP +
             Wage_Bonus_YN);
//CAR 9-7-2003 add an extra check
          if (IndexOfWage >= 0) and
            (IndexOfWage <= (DialogExportPayrollDM.FEmpHourTypeWage.Count -2)) then
             QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)) +
               ' / '+ DialogExportPayrollDM.FEmpHourTypeWage.Strings[IndexOfWage+1])
          else
            QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)));
        end;
      end;
  end;
  IndexEmpl := DialogExportPayrollDM.FEmpQuaranteedHrs.IndexOf(IntToStr(FEmpl));
  //CAR 9-7-2003 add an extra check
  if (IndexEmpl >= 0) and
    (IndexEmpl <= DialogExportPayrollDM.FEmpQuaranteedHrs.Count -2) then
  begin
    p := Pos(' ', DialogExportPayrollDM.FEmpQuaranteedHrs.Strings[IndexEmpl + 1]);
    HourType := StrToInt(Copy(DialogExportPayrollDM.FEmpQuaranteedHrs.Strings[IndexEmpl+ 1],
      0, p - 1));
    Min :=  StrToInt(Copy(DialogExportPayrollDM.FEmpQuaranteedHrs.Strings[IndexEmpl + 1],
      p + 1, 40));
    if DialogExportPayrollDM.HourTypeFind(HourType, MyCountryID(FEmpl))
      {DialogExportPayrollDM.ClientDataSetHourType.FindKey([HourType])} then
    begin
      DescTypeHour := FillSpaces(DialogExportPayrollDM.ClientDataSetHourType.
          FieldByName('DESCRIPTION').AsString, 30);
      QRMemoHourType.Lines.Add(IntToStr(HourType));
      QRMemoHourDesc.Lines.Add(DescTypeHour);
      IndexOfWage := DialogExportPayrollDM.FEmpHourTypeWage.IndexOf(
        IntToStr(FEmpl) + CHR_SEP + IntToStr(HourType) + CHR_SEP +
        DialogExportPayrollDM.ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString);
//CAR 9-7-2003 add extra check
      if (IndexOfWage >=0) and
        (IndexOfWage <= DialogExportPayrollDM.FEmpHourTypeWage.Count -2) then
         QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)) +
           ' / '+ DialogExportPayrollDM.FEmpHourTypeWage.Strings[IndexOfWage+1])
      else
        QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)));
    end;
  end;
  IndexEmpl := DialogExportPayrollDM.FEmpHourSick.IndexOf(IntToStr(FEmpl));
//CAR 9-7-2003
  if (IndexEmpl >= 0) and
    (IndexEmpl <= DialogExportPayrollDM.FEmpHourSick.Count - 2) then
  begin
    p := Pos(CHR_SEP, DialogExportPayrollDM.FEmpHourSick.Strings[IndexEmpl + 1]);
    HourType := StrToInt(Copy(DialogExportPayrollDM.FEmpHourSick.Strings[IndexEmpl+ 1],
      0, p - 1));
    Min :=  StrToInt(Copy(DialogExportPayrollDM.FEmpHourSick.Strings[IndexEmpl + 1],
      p + 1, 40));
    if DialogExportPayrollDM.HourTypeFind(HourType, MyCountryID(FEmpl))
      {DialogExportPayrollDM.ClientDataSetHourType.FindKey([HourType])} then
    begin
      DescTypeHour := FillSpaces(DialogExportPayrollDM.ClientDataSetHourType.
          FieldByName('DESCRIPTION').AsString, 30);
      QRMemoHourType.Lines.Add(IntToStr(HourType));
      QRMemoHourDesc.Lines.Add(DescTypeHour);
      IndexOfWage := DialogExportPayrollDM.FEmpHourTypeWage.IndexOf(
        IntToStr(FEmpl) + CHR_SEP + IntToStr(HourType) + CHR_SEP +
        DialogExportPayrollDM.ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString);
//CAR 9-7-2003 add extra check
      if (IndexOfWage >=0) and
        (IndexOfWage <= DialogExportPayrollDM.FEmpHourTypeWage.Count -2) then
         QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)) +
           ' / '+ DialogExportPayrollDM.FEmpHourTypeWage.Strings[IndexOfWage+1])
      else
        QRMemoHrs.Lines.Add(DecodeHrsMin(DialogExportPayrollF.Round5(Min)));
    end;
  end;
// extra payment
//CAR 9-7-2003 add extra check
  IndexPayment:= DialogExportPayrollDM.FEmpExtraPayment.IndexOf(IntToStr(FEmpl));
  if  (IndexPayment >= 0) and
    (IndexPayment <= DialogExportPayrollDM.FEmpExtraPayment.Count - 2) then
  begin
    StrTmp := DialogExportPayrollDM.FEmpExtraPayment.Strings[IndexPayment + 1];
    IndexPayment := IndexPayment + 1;
    while (pos(Chr_SEP, StrTmp) > 0) do
    begin
      ExportCode := Copy(StrTmp, 0, Pos(CHR_SEP, StrTmp) -1) + '      ';
      ExportCode := Copy (ExportCode, 0, 6);
      AmountExport := StrToFloat(Copy(StrTmp, Pos(CHR_SEP,StrTmp) + 1 , 40)) * 100;
      QRMemoPayment.Lines.Add(ExportCode + ' ' + FloatToStr(AmountExport));
      IndexPayment := IndexPayment + 1;
      if IndexPayment >= DialogExportPayrollDM.FEmpExtraPayment.Count then
        Break;
      StrTmp := DialogExportPayrollDM.FEmpExtraPayment.Strings[IndexPayment];
    end;
  end;
end;

procedure TReportExportPayrollQR.PrintTotalDays;
var
 WKDays, IllDays, HolDays, PaidDays, UnpaidDays, WTRDays,
 LabourDays, BankDays, TFTDays, MatDays, LayDays, TravelDays,
 HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
 ShorterWeekDays: Integer;
var
  ExportCodeDummy: String;
begin
  inherited;
  DialogExportPayrollDM.DetermineDays(FEmpl, WKDays, IllDays, HolDays, PaidDays,
    UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays, TravelDays,
    HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
    ShorterWeekDays, ExportCodeDummy);
  QRLabelWKDays.Caption :=  IntToStr(WKDays);
  QRLabelIllDays.Caption :=  IntToStr(IllDays);
  QRLabelHolDays.Caption := IntToStr(HolDays);
  QRLabelPaidDays.Caption := IntToStr(PaidDays);
  QRLabelUnpaidDays.Caption := IntToStr(UnpaidDays);
  QRLabelWKRDays.Caption := IntToStr(WTRDays);
  QRLabelLabour.Caption := IntToStr(LabourDays);
  QRLabelBank.Caption := IntToStr(BankDays);
  QRLabelTFT.Caption := IntToStr(TFTDays);
  QRLabelMat.Caption := IntToStr(MatDays);
  QRLabelLay.Caption := IntToStr(LayDays);
  QRLabelTravel.Caption := IntToStr(TravelDays); // SO-20013169
  //RV067.5.
  if FAttentia then
  begin
    QRLabelHolPrev.Caption := IntToStr(HolPrevDays);
    QRLabelHolSen.Caption := IntToStr(HolSenDays);
    QRLabelHolAddBank.Caption := IntToStr(HolAddBankDays);
    QRLabelSatCred.Caption := IntToStr(SatCredDays);
    QRLabelHolBnkRsv.Caption := IntToStr(HolBnkRsvDays);
    QRLabelShorterWeek.Caption := IntToStr(ShorterWeekDays);
  end
  else
  begin
    // RV067.MRA.28 Initialise here!
    QRLabelHolPrev.Caption := '0';
    QRLabelHolSen.Caption := '0';
    QRLabelHolAddBank.Caption := '0';
    QRLabelSatCred.Caption := '0';
    QRLabelHolBnkRsv.Caption := '0';
    QRLabelShorterWeek.Caption := '0';
  end;
end;

procedure TReportExportPayrollQR.QRBandHDBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if DialogExportPayrollDM.TableEP.FieldByName('EXPORT_WAGE_YN').AsString = CHECKEDVALUE then
    QRLabelHrs.Caption := 'Hours/ Wage'
  else
    QRLabelHrs.Caption := 'Hours';
end;

procedure TReportExportPayrollQR.QRLabelFromWeekPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if FAttentia then
  begin
    Value := '';
    Exit;
  end;
  
  if QRParameters.FWeekFrom = QRParameters.FWeekTo then
    Value := SPimsWeek
  else
    Value := SRepExpPayroll;
end;

procedure TReportExportPayrollQR.QRLabelToWeekPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FWeekFrom = QRParameters.FWeekTo then
    Value := '';
end;

procedure TReportExportPayrollQR.QRLabelWeekToPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FWeekFrom = QRParameters.FWeekTo then
    Value := '';
end;

procedure TReportExportPayrollQR.QRLabelWeekFromPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FWeekFrom = QRParameters.FWeekTo then
    Value := '';
end;

procedure TReportExportPayrollQR.QRLabelWeekPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if (not FAttentia) and (QRParameters.FWeekFrom = QRParameters.FWeekTo) then
    Value := IntToStr(QRParameters.FWeekFrom);
end;

procedure TReportExportPayrollQR.QueryEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TQRParameters.SetValues(InitDate, FinDate: TDateTime);
begin
  FInitDate := InitDate;
  FFinDate := FinDate;
end;

procedure TReportExportPayrollQR.QRChildBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FAttentia;
  if PrintBand then
    PrintBand := (FIndexEmpl >= 0) or (FContrEmpl >= 0)or
      (DialogExportPayrollDM.FEmpHourSick.IndexOf(IntToStr(FEmpl)) >= 0) or
      (DialogExportPayrollDM.FEmpExtraPayment.IndexOf(IntToStr(FEmpl)) >= 0);
end;

end.


