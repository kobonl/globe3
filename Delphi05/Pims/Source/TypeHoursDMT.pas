unit TypeHoursDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTypeHoursDM = class(TGridBaseDM)
    TableMasterHOURTYPE_NUMBER: TIntegerField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterBONUS_PERCENTAGE: TIntegerField;
    TableMasterOVERTIME_YN: TStringField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterCOUNT_DAY_YN: TStringField;
    TableMasterEXPORT_CODE: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TypeHoursDM: TTypeHoursDM;

implementation

{$R *.DFM}

end.
