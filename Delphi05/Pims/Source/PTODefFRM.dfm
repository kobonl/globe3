inherited PTODefF: TPTODefF
  Caption = 'PTO Definitions'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Contract Groups'
        end>
      DefaultLayout = False
      KeyField = 'CONTRACTGROUP_CODE'
      DataSource = PTODefDM.DataSourceMaster
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridCONTRACTGROUP_CODE: TdxDBGridMaskColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACTGROUP_CODE'
      end
      object dxMasterGridDESCRIPTION: TdxDBGridMaskColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridMAX_PTO_MINUTE: TdxDBGridMaskColumn
        Caption = 'Max PTO Transferred'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MAX_PTO_MINUTE_CALC'
      end
    end
  end
  inherited pnlDetail: TPanel
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 163
      Align = alClient
      Caption = 'PTO Definition'
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 25
        Width = 38
        Height = 13
        Caption = 'Line no.'
      end
      object dxDBSpinEditLine: TdxDBSpinEdit
        Tag = 1
        Left = 67
        Top = 21
        Width = 54
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'LINE_NUMBER'
        DataSource = PTODefDM.DataSourceDetail
      end
      object GroupBox2: TGroupBox
        Left = 8
        Top = 48
        Width = 289
        Height = 49
        Caption = 'Employment years'
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 21
          Width = 24
          Height = 13
          Caption = 'From'
        end
        object Label3: TLabel
          Left = 160
          Top = 21
          Width = 12
          Height = 13
          Caption = 'Till'
        end
        object dxDBSpinEditFromYears: TdxDBSpinEdit
          Tag = 1
          Left = 83
          Top = 18
          Width = 62
          Style.BorderStyle = xbsSingle
          TabOrder = 0
          DataField = 'FROMEMPYEARS'
          DataSource = PTODefDM.DataSourceDetail
        end
        object dxDBSpinEditTillYears: TdxDBSpinEdit
          Tag = 1
          Left = 219
          Top = 18
          Width = 54
          Style.BorderStyle = xbsSingle
          TabOrder = 1
          DataField = 'TILLEMPYEARS'
          DataSource = PTODefDM.DataSourceDetail
        end
      end
      object GroupBox3: TGroupBox
        Left = 304
        Top = 48
        Width = 329
        Height = 49
        Caption = 'Hours'
        TabOrder = 2
        object Label6: TLabel
          Left = 10
          Top = 20
          Width = 28
          Height = 13
          Caption = 'Hours'
        end
        object Label8: TLabel
          Left = 138
          Top = 18
          Width = 37
          Height = 13
          Caption = 'Minutes'
        end
        object dxSpinEditHour: TdxSpinEdit
          Tag = 1
          Left = 56
          Top = 16
          Width = 72
          TabOrder = 0
          OnChange = dxSpinEditHourChange
          MaxValue = 9999
          StoredValues = 16
        end
        object dxSpinEditMin: TdxSpinEdit
          Tag = 1
          Left = 200
          Top = 16
          Width = 49
          TabOrder = 1
          OnChange = dxSpinEditMinChange
          MaxValue = 59
          StoredValues = 16
        end
      end
      object dxSpinEditHourOld: TdxSpinEdit
        Left = 591
        Top = 56
        Width = 41
        TabOrder = 3
        Visible = False
      end
      object dxSpinEditMinOld: TdxSpinEdit
        Left = 591
        Top = 80
        Width = 41
        TabOrder = 4
        Visible = False
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'PTO Definitions'
        end>
      KeyField = 'LINE_NUMBER'
      DataSource = PTODefDM.DataSourceDetail
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridLINE_NUMBER: TdxDBGridMaskColumn
        Caption = 'Line number'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LINE_NUMBER'
      end
      object dxDetailGridFROMEMPYEARS: TdxDBGridMaskColumn
        Caption = 'From'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FROMEMPYEARS'
      end
      object dxDetailGridTILLEMPYEARS: TdxDBGridMaskColumn
        Caption = 'Till'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TILLEMPYEARS'
      end
      object dxDetailGridPTOMINUTE: TdxDBGridMaskColumn
        Caption = 'Hours'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PTOHOURMINCALC'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
    inherited dxBarButtonCopy: TdxBarButton
      Action = CopyAct
    end
    inherited dxBarButtonPaste: TdxBarButton
      Action = PasteAct
    end
  end
  inherited StandardMenuActionList: TActionList
    object CopyAct: TAction
      Caption = 'Copy'
      OnExecute = CopyActExecute
    end
    object PasteAct: TAction
      Caption = 'Paste'
      OnExecute = PasteActExecute
    end
  end
end
