inherited DialogAddHolidayDM: TDialogAddHolidayDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 676
  Width = 741
  object dsrcEmployee: TDataSource
    DataSet = tblEmployee
    Left = 144
    Top = 24
  end
  object qryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = dsrcEmployee
    SQL.Strings = (
      'SELECT'
      '  SHIFT_NUMBER, '
      '  SHIFT_NUMBER || '#39' | '#39' || DESCRIPTION DESCRIPTION'
      'FROM'
      '  SHIFT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  SHIFT_NUMBER')
    Left = 48
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object tblEmployee: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'EMPLOYEE'
    Left = 48
    Top = 24
    object tblEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
      Required = True
    end
    object tblEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object tblEmployeeSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object tblEmployeeSHIFTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFTLU'
      LookupDataSet = qryShift
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      Size = 30
      Lookup = True
    end
    object tblEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 160
    end
    object tblEmployeeCOUNTRY_ID: TIntegerField
      FieldKind = fkLookup
      FieldName = 'COUNTRY_ID'
      LookupDataSet = qryPlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'COUNTRY_ID'
      KeyFields = 'PLANT_CODE'
      Lookup = True
    end
  end
  object qryAbsReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select * from absencereason'
      ' ')
    Left = 48
    Top = 162
  end
  object qryPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = dsrcEmployee
    SQL.Strings = (
      'SELECT PLANT_CODE, COUNTRY_ID'
      'FROM PLANT'
      'WHERE PLANT_CODE = :PLANT_CODE')
    Left = 112
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryAbsRsn: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      
        '  ABSENCEREASON_ID, ABSENCEREASON_CODE, DESCRIPTION, ABSENCETYPE' +
        '_CODE'
      'FROM '
      '  ABSENCEREASON '
      'ORDER BY '
      '  DESCRIPTION')
    Left = 48
    Top = 224
  end
  object dspAbsRsn: TDataSetProvider
    DataSet = qryAbsRsn
    Constraints = True
    Left = 120
    Top = 224
  end
  object cdsAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspAbsRsn'
    Left = 192
    Top = 224
  end
end
