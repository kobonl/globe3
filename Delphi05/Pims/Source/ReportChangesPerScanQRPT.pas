(*
  MRA:12-MAR-2013 New report Changes per Scan
  - This report shows information based on a table that stores logging-info
    during changes made to scans.
  MRA:3-APR-2013 20014037.60 Rework
  - Add option to filter on Pims User or Workstation or None (compared with
    mutator-field).
*)
unit ReportChangesPerScanQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FSortOn: Integer;
    FShowSelection,
    FExportToFile: Boolean;
    FPimsUserFrom, FPimsUserTo: String;
    FWorkstationFrom, FWorkstationTo: String;
    FFilterOn: Integer;
  public
    procedure SetValues(
      DateFrom, DateTo: TDateTime;
      SortOn: Integer;
      ShowSelection,
      ExportToFile: Boolean;
      PimsUserFrom, PimsUserTo: String;
      WorkstationFrom, WorkstationTo: String;
      FilterOn: Integer
      );
  end;

  TReportChangesPerScanQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel2: TQRLabel;
    QRBandSummary: TQRBand;
    QRBandTitleEmpl: TQRBand;
    QRShape5: TQRShape;
    QRLabelAction: TQRLabel;
    QRLabelDateIn: TQRLabel;
    QRLabelDateOut: TQRLabel;
    QRLabelEmployee: TQRLabel;
    QRLabelWorkspot: TQRLabel;
    QRLabelProcessed: TQRLabel;
    QRLabelShift: TQRLabel;
    QRShape6: TQRShape;
    QRLabelPlant: TQRLabel;
    QRLabelAction2: TQRLabel;
    QRLabelMutator: TQRLabel;
    QRLabelTimestamp: TQRLabel;
    QRLabelJob: TQRLabel;
    QRLabelIDCardIn: TQRLabel;
    QRLabelIDCardOut: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRDBTextAction: TQRDBText;
    QRDBTextActionTimestamp: TQRDBText;
    QRDBTextEmployee: TQRDBText;
    QRDBTextDateIn: TQRDBText;
    QRDBTextDateOut: TQRDBText;
    QRDBTextProcessed: TQRDBText;
    QRDBTextWorkspot: TQRDBText;
    QRDBTextJob: TQRDBText;
    QRDBTextShift: TQRDBText;
    QRDBTextIDCardIn: TQRDBText;
    QRDBTextIDCardOut: TQRDBText;
    QRDBTextMutator: TQRDBText;
    QRDBTextPlant: TQRDBText;
    QRLabelShiftDate: TQRLabel;
    QRLabelExported: TQRLabel;
    QRDBTextShiftDate: TQRDBText;
    QRDBTextExported: TQRDBText;
    QRLblFromFilterOn: TQRLabel;
    QRLblFilterOn: TQRLabel;
    QRLblFilterOnFrom: TQRLabel;
    QRLblToFilterOn: TQRLabel;
    QRLblFilterOnTo: TQRLabel;
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBTextActionTimestampPrint(sender: TObject;
      var Value: String);
    procedure QRDBTextDateInPrint(sender: TObject; var Value: String);
    procedure QRDBTextDateOutPrint(sender: TObject; var Value: String);
    procedure QRDBTextShiftDatePrint(sender: TObject; var Value: String);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const DateFrom, DateTo: TDateTime;
      const SortOn: Integer;
      const ShowSelection, ExportToFile: Boolean;
      const PimsUserFrom, PimsUserTo: String;
      const WorkstationFrom, WorkstationTo: String;
      const FilterOn: Integer
      ): Boolean;
    procedure FreeMemory; override;
    property QRParameters: TQRParameters read FQRParameters;
  end;

var
  ReportChangesPerScanQR: TReportChangesPerScanQR;

implementation

uses
  SystemDMT, UGlobalFunctions, UPimsMessageRes, UPimsConst,
  ReportChangesPerScanDMT;

{$R *.DFM}

procedure ExportDetail(
  Action, ActionTimeStamp, EmployeeNumber, DateIn, DateOut,
  Processed, Plant, Workspot, Job, Shift, IdCardIn, IdCardOut, ShiftDate,
  Exported, Mutator: String);
begin
  ExportClass.AddText(
    Action + ExportClass.Sep +
    ActionTimeStamp + ExportClass.Sep +
    EmployeeNumber + ExportClass.Sep +
    DateIn + ExportClass.Sep +
    DateOut + ExportClass.Sep +
    Processed + ExportClass.Sep +
    Plant + ExportClass.Sep +
    Workspot + ExportClass.Sep +
    Job + ExportClass.Sep +
    Shift + ExportClass.Sep +
    IDCardIn + ExportClass.Sep +
    IDCardOut + ExportClass.Sep +
    ShiftDate + ExportClass.Sep +
    Exported + ExportClass.Sep +
    Mutator
    );
end; // ExportDetail

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  SortOn: Integer;
  ShowSelection,
  ExportToFile: Boolean;
  PimsUserFrom, PimsUserTo: String;
  WorkstationFrom, WorkstationTo: String;
  FilterOn: Integer
  );
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FSortOn := SortOn;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  // 20014037.60
  FPimsUserFrom := PimsUserFrom;
  FPimsUserTo := PimsUserTo;
  FWorkstationFrom := WorkstationFrom;
  FWorkstationTo := WorkstationTo;
  FFilterOn := FilterOn;
end; // SetValues

function TReportChangesPerScanQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const DateFrom, DateTo: TDateTime;
  const SortOn: Integer;
  const ShowSelection, ExportToFile: Boolean;
  const PimsUserFrom, PimsUserTo: String;
  const WorkstationFrom, WorkstationTo: String;
  const FilterOn: Integer
  ): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '1', '1');
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      DateFrom, DateTo,
      SortOn,
      ShowSelection,
      ExportToFile,
      PimsUserFrom, PimsUserTo,
      WorkstationFrom, WorkstationTo,
      FilterOn
      );
  end;
  SetDataSetQueryReport(ReportChangesPerScanDM.QueryChangesPerScan);
end; // QRSendReportParameters

function TReportChangesPerScanQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;
  SelectStr :=
    'SELECT ' + NL +
    '  ABSLOG_ACTION, ' + NL +
    '  ABSLOG_TIMESTAMP, ' + NL +
    '  EMPLOYEE_NUMBER, ' + NL +
    '  DATETIME_IN, ' + NL +
    '  DATETIME_OUT, ' + NL +
    '  PLANT_CODE, ' + NL +
    '  WORKSPOT_CODE, ' + NL +
    '  JOB_CODE, ' + NL +
    '  SHIFT_NUMBER, ' + NL +
    '  PROCESSED_YN, ' + NL +
    '  IDCARD_IN, ' + NL +
    '  IDCARD_OUT, ' + NL +
    '  MUTATOR, ' + NL +
    '  SHIFT_DATE, ' + NL +
    '  EXPORTED_YN ' + NL +
    'FROM ' + NL +
    '  ABSLOGTRS T ' + NL +
    'WHERE ' + NL;
  SelectStr := SelectStr +
    '  T.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND T.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND T.ABSLOG_TIMESTAMP >= :DATEFROM ' + NL +
    '  AND T.ABSLOG_TIMESTAMP < :DATETO ' + NL;
  // 20014037.60 Filter on PimsUser, Workstation or None.
  case QRParameters.FFilterOn of
  0: // Pims User
    begin
      SelectStr := SelectStr +
        ' AND T.MUTATOR IN ' + NL +
        '   (SELECT A.USER_NAME ' + NL +
        '    FROM ' + NL +
        '    ( ' + NL +
        '      SELECT ' + NL +
        '        P.USER_NAME ' + NL +
        '      FROM ' + NL + 
        '        PIMSUSER P ' + NL +
        '      UNION ' + NL +
        '        SELECT ' + NL +
        '          ''SYSDBA'' ' + NL +
        '        FROM DUAL ' + NL +
        '    ) A ' + NL +
        '    WHERE A.USER_NAME >= ' + '''' +
        DoubleQuote(QRParameters.FPimsUserFrom) + '''' + NL +
        '    AND A.USER_NAME <= ' + '''' +
        DoubleQuote(QRParameters.FPimsUserTo) + '''' + NL +
        '    ) ';
    end;
  1: // Workstation
    begin
      SelectStr := SelectStr +
        ' AND T.MUTATOR IN ' + NL +
        '    (SELECT W.COMPUTER_NAME FROM WORKSTATION W ' + NL +
        '     WHERE W.COMPUTER_NAME >= ' + '''' +
        DoubleQuote(QRParameters.FWorkstationFrom) + '''' + NL +
        '     AND W.COMPUTER_NAME <= ' + '''' +
        DoubleQuote(QRParameters.FWorkstationTo) + '''' + NL +
        '    ) ';
    end;
  2: // None (no filtering)
  end;
  SelectStr := SelectStr +
    'ORDER BY ' + NL;
  if QRParameters.FSortOn = 0 then // Date
    SelectStr := SelectStr +
    '  T.ABSLOG_TIMESTAMP '
  else
    SelectStr := SelectStr +
    '  T.EMPLOYEE_NUMBER, T.ABSLOG_TIMESTAMP ';

  with ReportChangesPerScanDM do
  begin
    QueryChangesPerScan.Active := False;
    QueryChangesPerScan.UniDirectional := False;
    QueryChangesPerScan.SQL.Clear;
    QueryChangesPerScan.SQL.Add(SelectStr);

    QueryChangesPerScan.
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
    QueryChangesPerScan.
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;

    if not QueryChangesPerScan.Prepared then
       QueryChangesPerScan.Prepare;
    QueryChangesPerScan.Active := True;

    Result := not QueryChangesPerScan.IsEmpty;
  end;
  Screen.Cursor := crDefault;
end; // ExistsRecords

procedure TReportChangesPerScanQR.ConfigReport;
begin
//  inherited; // Do not inherite, or it gives an 'abstract error' !
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;

  // 20014037.60
  case QRParameters.FFilterOn of
  0: // Pims User
    begin
      QRLblFromFilterOn.Caption := QRLabel2.Caption; // From
      QRLblFilterOn.Caption := SPimsPimsUser;
      QRLblFilterOnFrom.Caption := QRParameters.FPimsUserFrom;
      QRLblToFilterOn.Caption := QRLabel49.Caption; // to
      QRLblFilterOnTo.Caption := QRParameters.FPimsUserTo;
    end;
  1: // Workstation
    begin
      QRLblFromFilterOn.Caption := QRLabel2.Caption; // From
      QRLblFilterOn.Caption := SPimsWorkstation;
      QRLblFilterOnFrom.Caption := QRParameters.FWorkstationFrom;
      QRLblToFilterOn.Caption := QRLabel49.Caption; // to
      QRLblFilterOnTo.Caption := QRParameters.FWorkstationTo;
    end;
  2: // None
    begin
      QRLblFromFilterOn.Caption := '';
      QRLblFilterOn.Caption := '';
      QRLblFilterOnFrom.Caption := '';
      QRLblFilterOnTo.Caption := '';
      QRLblToFilterOn.Caption := '';
    end;
  end;

  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo - 1);
end; // ConfigReport

procedure TReportChangesPerScanQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end; // FreeMemory

procedure TReportChangesPerScanQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption;
end; // FormCreate

procedure TReportChangesPerScanQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Show selections
    if QRParameters.FShowSelection then
    begin
      // Selections
      ExportClass.AddText(QRLabel11.Caption);
      // From Plant PlantCode to PlantCode
      ExportClass.AddText(QRLabel2.Caption + ' ' +
        QRLabel19.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' + QRLabelPlantTo.Caption);
      // 20014037.60
      // From [Pims User | Workstation] ... to ...
      ExportClass.AddText(QRLblFromFilterOn.Caption + ' ' +
        QRLblFilterOn.Caption + ' ' + QRLblFilterOnFrom.Caption + ' ' +
        QRLblToFilterOn.Caption + ' ' + QRLblFilterOnTo.Caption);
      // From Scandate date to date
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' +
        QRLabelDate.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabelToDate.Caption + ' ' + QRLabelDateTo.Caption);
    end;

    // Show columns
    ExportDetail(
      QRLabelAction.Caption,                                   // Action
      QRLabelAction2.Caption + ' ' + QRLabelTimestamp.Caption, // Action Timestamp
      QRLabelEmployee.Caption,                                 // Employee
      QRLabelDateIn.Caption,                                   // Date In
      QRLabelDateOut.Caption,                                  // Date Out
      QRLabelProcessed.Caption,                                // Processed
      QRLabelPlant.Caption,                                    // Plant
      QRLabelWorkspot.Caption,                                 // Workspot
      QRLabelJob.Caption,                                      // Job
      QRLabelShift.Caption,                                    // Shift
      QRLabelIDCardIn.Caption,                                 // IDcard In
      QRLabelIDCardOut.Caption,                                // Idcard Out
      QRLabelShiftDate.Caption,                                // Shift Date
      QRLabelExported.Caption,                                 // Exported
      QRLabelMutator.Caption                                   // Mutator
      );
  end;
end; // QRBandTitleBeforePrint

procedure TReportChangesPerScanQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportChangesPerScanQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportChangesPerScanQR.QRBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  ShiftDate: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    if QRDBTextShiftDate.DataSet.FieldByName('SHIFT_DATE').AsDateTime = 0 then
      ShiftDate := ''
    else
      ShiftDate := FormatDateTime('dd-mm-yyyy', QRDBTextShiftDate.
        DataSet.FieldByName('SHIFT_DATE').AsDateTime);
    ExportDetail(
      QRDBTextAction.DataSet.FieldByName('ABSLOG_ACTION').AsString,
      FormatDateTime('dd-mm-yyyy hh:nn', QRDBTextActionTimestamp.
        DataSet.FieldByName('ABSLOG_TIMESTAMP').AsDateTime),
      QRDBTextEmployee.DataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
      FormatDateTime('dd-mm-yyyy hh:nn', QRDBTextDateIn.DataSet.
        FieldByName('DATETIME_IN').AsDateTime),
      FormatDateTime('dd-mm-yyyy hh:nn', QRDBTextDateOut.DataSet.
        FieldByName('DATETIME_OUT').AsDateTime),
      QRDBTextProcessed.DataSet.FieldByName('PROCESSED_YN').AsString,
      QRDBTextPlant.DataSet.FieldByName('PLANT_CODE').AsString,
      QRDBTextWorkspot.DataSet.FieldByName('WORKSPOT_CODE').AsString,
      QRDBTextJob.DataSet.FieldByName('JOB_CODE').AsString,
      QRDBTextShift.DataSet.FieldByName('SHIFT_NUMBER').AsString,
      QRDBTextIDCardIn.DataSet.FieldByName('IDCARD_IN').AsString,
      QRDBTextIDCardOut.DataSet.FieldByName('IDCARD_OUT').AsString,
      ShiftDate,
      QRDBTextExported.DataSet.FieldByName('EXPORTED_YN').AsString,
      QRDBTextMutator.DataSet.FieldByName('MUTATOR').AsString
    );
  end;
end; // QRBandDetailEmployeeAfterPrint

procedure TReportChangesPerScanQR.QRDBTextActionTimestampPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  Value := FormatDateTime('dd-mm-yyyy hh:nn', ReportChangesPerScanDM.
    QueryChangesPerScan.FieldByName('ABSLOG_TIMESTAMP').AsDateTime);
end;

procedure TReportChangesPerScanQR.QRDBTextDateInPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FormatDateTime('dd-mm-yyyy hh:nn', ReportChangesPerScanDM.
    QueryChangesPerScan.FieldByName('DATETIME_IN').AsDateTime);
end;

procedure TReportChangesPerScanQR.QRDBTextDateOutPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FormatDateTime('dd-mm-yyyy hh:nn', ReportChangesPerScanDM.
    QueryChangesPerScan.FieldByName('DATETIME_OUT').AsDateTime);
end;

procedure TReportChangesPerScanQR.QRDBTextShiftDatePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if ReportChangesPerScanDM.
    QueryChangesPerScan.FieldByName('SHIFT_DATE').AsDateTime = 0 then
    Value := ''
  else
    Value := FormatDateTime('dd-mm-yyyy', ReportChangesPerScanDM.
      QueryChangesPerScan.FieldByName('SHIFT_DATE').AsDateTime);
end;

end.
