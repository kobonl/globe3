(*
  Changes:
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Fields WEEKNUMBER, EMPLOYEE_NUMBER, ABSENCEREASON_ID gave an error
      like:
        QueryAbsence:
          Type mismatch for field WEEKNUMBER, expecting float, actual integer.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'. (QueryAbsence).
      Also field-components for QueryAbsence for these 3 fields must be 'float'.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)

unit ReportHrsPerDayDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, ReportBaseDMT, DBTables, Db, Provider, DBClient;

type
  TReportHrsPerDayDM = class(TReportBaseDM)
    QueryAbsence: TQuery;
    DataSourceAbsence: TDataSource;
    QueryReportHrs: TQuery;
    QueryBUPerWK: TQuery;
    dspQueryBUWK: TDataSetProvider;
    cdsQueryBUWK: TClientDataSet;
    QueryAbsencePLANT_CODE: TStringField;
    QueryAbsencePDESC: TStringField;
    QueryAbsenceTEAM_CODE: TStringField;
    QueryAbsenceTDESC: TStringField;
    QueryAbsenceDEPARTMENT_CODE: TStringField;
    QueryAbsenceDDESC: TStringField;
    QueryAbsenceEDESC: TStringField;
    QueryAbsenceABSENCEREASON_CODE: TStringField;
    QueryAbsenceABSDESC: TStringField;
    QueryAbsenceREPORT_DATE: TDateTimeField;
    QueryAbsenceSUMMIN: TFloatField;
    QueryAbsenceBUCODE: TStringField;
    QueryAbsenceWORKSPOT_CODE: TStringField;
    QueryAbsenceLISTCODE: TFloatField;
    QueryAbsenceABSENCEREASON_ID: TFloatField;
    QueryAbsenceWEEKNUMBER: TFloatField;
    QueryAbsenceEMPLOYEE_NUMBER: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryAbsenceFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportHrsPerDayDM: TReportHrsPerDayDM;

implementation

{$R *.DFM}

procedure TReportHrsPerDayDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryAbsence);
end;

procedure TReportHrsPerDayDM.QueryAbsenceFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
