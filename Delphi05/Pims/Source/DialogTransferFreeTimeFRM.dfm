inherited DialogTransferFreeTimeF: TDialogTransferFreeTimeF
  Left = 292
  Top = 169
  Caption = 'Transfer free time to next year'
  ClientHeight = 372
  ClientWidth = 585
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 585
    inherited imgOrbit: TImage
      Left = 468
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 585
    Height = 251
    inherited LblFromEmployee: TLabel
      Top = 68
    end
    inherited LblEmployee: TLabel
      Top = 68
    end
    inherited LblToEmployee: TLabel
      Top = 68
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 68
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 351
      Top = 68
    end
    object Label14: TLabel [8]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [9]
      Left = 40
      Top = 43
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [10]
      Left = 128
      Top = 47
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [11]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label27: TLabel [12]
      Left = 351
      Top = 47
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [13]
      Left = 8
      Top = 93
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel [14]
      Left = 40
      Top = 93
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label23: TLabel [15]
      Left = 315
      Top = 93
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 351
    end
    inherited LblStarTeamTo: TLabel
      Left = 352
      Top = 332
      Visible = False
    end
    inherited LblToTeam: TLabel
      Top = 332
      Visible = False
    end
    inherited LblStarTeamFrom: TLabel
      Top = 332
      Visible = False
    end
    inherited LblTeam: TLabel
      Top = 330
      Visible = False
    end
    inherited LblFromTeam: TLabel
      Top = 330
      Visible = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 157
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 158
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 329
      ColCount = 154
      TabOrder = 22
      Visible = False
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 329
      ColCount = 155
      TabOrder = 11
      Visible = False
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 332
      TabOrder = 32
      Visible = False
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 153
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 154
    end
    inherited CheckBoxAllDepartments: TCheckBox
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 5
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 6
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 156
      TabOrder = 12
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 157
      TabOrder = 20
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 153
      TabOrder = 21
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 154
      TabOrder = 29
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 154
      TabOrder = 30
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 155
      TabOrder = 31
    end
    inherited EditWorkspots: TEdit
      TabOrder = 13
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 160
      TabOrder = 15
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 159
      TabOrder = 14
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 17
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 19
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 16
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 18
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 152
      Width = 625
      Height = 57
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      Visible = False
    end
    object GroupBoxTimeForTime: TGroupBox
      Left = 0
      Top = 136
      Width = 585
      Height = 115
      Align = alBottom
      Caption = 'Settings'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 41
        Height = 13
        Caption = 'Transfer'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object CheckBoxHoliday: TCheckBox
        Left = 88
        Top = 19
        Width = 129
        Height = 17
        Caption = 'Holiday'
        TabOrder = 0
      end
      object CheckBoxWTR: TCheckBox
        Left = 88
        Top = 43
        Width = 129
        Height = 17
        Caption = 'Work time reduction'
        TabOrder = 1
      end
      object CheckBoxTimeForTime: TCheckBox
        Left = 88
        Top = 67
        Width = 129
        Height = 17
        Caption = 'Time for time'
        TabOrder = 2
      end
      object CheckBoxNormHoliday: TCheckBox
        Left = 232
        Top = 19
        Width = 137
        Height = 17
        Caption = 'Norm Holiday'
        TabOrder = 3
        OnClick = CheckBoxNormHolidayClick
      end
      object CheckBoxAddBankHoliday: TCheckBox
        Left = 376
        Top = 19
        Width = 185
        Height = 17
        Caption = 'Additional Bank Holiday'
        TabOrder = 6
      end
      object CheckBoxSaturdayCredit: TCheckBox
        Left = 216
        Top = 150
        Width = 129
        Height = 17
        Caption = 'Saturday Credit'
        TabOrder = 9
        Visible = False
      end
      object CheckBoxRsvBankHoliday: TCheckBox
        Left = 376
        Top = 43
        Width = 185
        Height = 17
        Caption = 'Bank Holiday to Reserve'
        TabOrder = 7
      end
      object CheckBoxShorterWeek: TCheckBox
        Left = 376
        Top = 67
        Width = 177
        Height = 17
        Caption = 'Shorter Working Week'
        TabOrder = 8
      end
      object CheckBoxPrevYearHoliday: TCheckBox
        Left = 376
        Top = 92
        Width = 193
        Height = 17
        Caption = 'Prev Year Holiday'
        TabOrder = 10
        Visible = False
      end
      object CheckBoxSeniorityHoliday: TCheckBox
        Left = 232
        Top = 43
        Width = 137
        Height = 17
        Caption = 'Seniority Holiday'
        TabOrder = 4
      end
      object CheckBoxTravelTime: TCheckBox
        Left = 232
        Top = 67
        Width = 137
        Height = 17
        Caption = 'Travel Time'
        TabOrder = 5
      end
      object CheckBoxCalculatePTO: TCheckBox
        Left = 88
        Top = 91
        Width = 129
        Height = 17
        Caption = 'Calculate PTO'
        TabOrder = 11
        OnClick = CheckBoxCalculatePTOClick
      end
    end
    object dxSpinEditYearFrom: TdxSpinEdit
      Left = 120
      Top = 91
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditYearTo: TdxSpinEdit
      Left = 342
      Top = 91
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
  end
  inherited stbarBase: TStatusBar
    Top = 353
    Width = 585
  end
  inherited pnlBottom: TPanel
    Top = 312
    Width = 585
    inherited btnOk: TBitBtn
      Left = 184
    end
    inherited btnCancel: TBitBtn
      Left = 298
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 640
    Top = 160
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 648
    Top = 64
  end
  inherited StandardMenuActionList: TActionList
    Left = 640
    Top = 112
  end
  inherited tblPlant: TTable
    Left = 64
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 520
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4A040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072A4469616C6F675472616E736665724672656554696D6546
        2E44617461536F75726365456D706C46726F6D104F7074696F6E73437573746F
        6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A
        696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53
        697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B
        106564676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D
        6564676F43616E496E73657274116564676F43616E4E617669676174696F6E11
        6564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C526563
        6F726473106564676F557365426F6F6B6D61726B7300000F5464784442477269
        64436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E75
        6D62657206536F7274656407046373557005576964746802410942616E64496E
        646578020008526F77496E6465780200094669656C644E616D65060F454D504C
        4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F436F
        6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E616D
        6505576964746802540942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060A53484F52545F4E414D4500000F54647844424772
        6964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D
        6505576964746803B4000942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060B4445534352495054494F4E00000F5464784442
        47726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E
        06074164647265737305576964746802450942616E64496E646578020008526F
        77496E6465780200094669656C644E616D6506074144445245535300000F5464
        78444247726964436F6C756D6E0E436F6C756D6E44657074436F646507436170
        74696F6E060F4465706172746D656E7420636F64650557696474680258094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060F
        4445504152544D454E545F434F444500000F546478444247726964436F6C756D
        6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F646509
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        06095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        11040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507284469616C6F675472616E73
        6665724672656554696D65462E44617461536F75726365456D706C546F104F70
        74696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E65
        64676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E671065
        64676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E670009
        4F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F
        43616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E
        617669676174696F6E116564676F436F6E6669726D44656C657465126564676F
        4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300
        000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C07436170
        74696F6E06064E756D62657206536F7274656407046373557005576964746802
        310942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D65060F454D504C4F5945455F4E554D42455200000F54647844424772696443
        6F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53
        686F7274206E616D65055769647468024E0942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060A53484F52545F4E414D450000
        0F546478444247726964436F6C756D6E11436F6C756D6E446573637269707469
        6F6E0743617074696F6E06044E616D6505576964746803CC000942616E64496E
        646578020008526F77496E6465780200094669656C644E616D65060B44455343
        52495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E
        416464726573730743617074696F6E0607416464726573730557696474680265
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506074144445245535300000F546478444247726964436F6C756D6E0E436F6C
        756D6E44657074436F64650743617074696F6E060F4465706172746D656E7420
        636F64650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F4445504152544D454E545F434F444500000F54647844424772
        6964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609546561
        6D20636F64650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D6506095445414D5F434F4445000000}
    end
  end
  object TableAbsTotTarget: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'ABSENCETOTAL'
    Left = 448
    Top = 23
  end
  object TableEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = TableEmplFilterRecord
    TableName = 'EMPLOYEE'
    Left = 552
    Top = 23
  end
  object QueryEmployeeContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER,'
      '  STARTDATE,'
      '  HOLIDAY_HOUR_PER_YEAR'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE'
      '  (EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPLOYEETO) AND'
      '  ('
      '    ((STARTDATE <= :DATEFROM) AND'
      '    (ENDDATE > :DATEFROM)) OR'
      '   ((STARTDATE > :DATEFROM) AND'
      '    (ENDDATE < :DATETO)) OR'
      '   ((ENDDATE >= :DATETO) AND'
      '    (STARTDATE < :DATETO))'
      '  )'
      'ORDER BY'
      '  EMPLOYEE_NUMBER,'
      '  STARTDATE'
      '  ')
    Left = 544
    Top = 204
    ParamData = <
      item
        DataType = ftString
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotal: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, T.ABSENCE_YEAR,'
      '  T.LAST_YEAR_HOLIDAY_MINUTE,'
      '  T.NORM_HOLIDAY_MINUTE, T.USED_HOLIDAY_MINUTE,'
      
        '  T.LAST_YEAR_TFT_MINUTE, T.EARNED_TFT_MINUTE, T.USED_TFT_MINUTE' +
        ','
      
        '  T.LAST_YEAR_WTR_MINUTE, T.EARNED_WTR_MINUTE, T.USED_WTR_MINUTE' +
        ', T.ILLNESS_MINUTE,'
      
        '  T.LAST_YEAR_HLD_PREV_YEAR_MINUTE, T.USED_HLD_PREV_YEAR_MINUTE,' +
        ' T.NORM_SENIORITY_HOLIDAY_MINUTE,'
      
        '  T.USED_SENIORITY_HOLIDAY_MINUTE, T.LAST_YEAR_ADD_BANK_HLD_MINU' +
        'TE, T.USED_ADD_BANK_HLD_MINUTE,'
      
        '  T.LAST_YEAR_SAT_CREDIT_MINUTE, T.USED_SAT_CREDIT_MINUTE, T.NOR' +
        'M_BANK_HLD_RESERVE_MINUTE,'
      
        '  T.USED_BANK_HLD_RESERVE_MINUTE, T.LAST_YEAR_SHORTWWEEK_MINUTE,' +
        ' T.USED_SHORTWWEEK_MINUTE,'
      '  T.EARNED_SHORTWWEEK_MINUTE, T.NORM_BANK_HLD_RES_MIN_MAN_YN,'
      
        '  T.LAST_YEAR_TRAVELTIME_MINUTE, T.EARNED_TRAVELTIME_MINUTE, T.U' +
        'SED_TRAVELTIME_MINUTE'
      'FROM'
      '  EMPLOYEE E LEFT OUTER JOIN ABSENCETOTAL T ON'
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '    T.ABSENCE_YEAR = :ABSENCE_YEAR AND'
      
        '    T.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND T.EMPLOYEE_NUMBER <= ' +
        ':EMPLOYEETO'
      'ORDER BY E.EMPLOYEE_NUMBER'
      ' '
      ' '
      ' ')
    Left = 544
    Top = 244
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTypePerCountryExist: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.COUNTRY_ID'
      'FROM ABSENCETYPEPERCOUNTRY T'
      'WHERE T.COUNTRY_ID = :COUNTRY_ID'
      '')
    Left = 496
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
  object qryCountryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  P.COUNTRY_ID, C.CODE'
      'FROM '
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    P.PLANT_CODE = E.PLANT_CODE '
      '  INNER JOIN COUNTRY C ON'
      '    P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE '
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 496
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryMaxPTOMinute: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT NVL(C.MAX_PTO_MINUTE, 0) MAX_PTO_MINUTE'
      'FROM CONTRACTGROUP C INNER JOIN EMPLOYEE E ON'
      '  C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE'
      'WHERE E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 496
    Top = 276
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPTODef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT PD.LINE_NUMBER, PD.FROMEMPYEARS, PD.TILLEMPYEARS, PD.PTOM' +
        'INUTE, E.STARTDATE'
      'FROM PTODEFINITION PD INNER JOIN CONTRACTGROUP C ON'
      '    PD.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      '  INNER JOIN EMPLOYEE E ON'
      '    C.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE'
      'WHERE E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY PD.LINE_NUMBER')
    Left = 456
    Top = 284
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
