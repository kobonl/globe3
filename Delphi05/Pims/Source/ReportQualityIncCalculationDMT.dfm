inherited ReportQualityIncCalculationDM: TReportQualityIncCalculationDM
  OldCreateOrder = True
  Left = 239
  Top = 191
  Height = 479
  Width = 621
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  A.EMPLOYEE_NUMBER, A.DESCRIPTION, '
      '  (SELECT '
      '     COUNT(B.SALARY_MINUTE) '
      '   FROM '
      '     SALARYHOURPEREMPLOYEE B'
      '   WHERE '
      '     (B.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER) AND'
      '     (B.SALARY_DATE = :DAY1)) AS HDAY1,'
      '     (SELECT '
      '        B.NUMBER_OF_MISTAKE'
      '      FROM '
      '        MISTAKEPEREMPLOYEE B'
      '      WHERE '
      '        (B.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER) AND'
      '        (B.MISTAKE_DATE = :DAY1)) AS MDAY1 '
      'FROM '
      '  EMPLOYEE A'
      'WHERE '
      '  (A.PLANT_CODE >= :PLANTFROM) AND'
      '  (A.PLANT_CODE <= :PLANTTO)')
    Left = 68
    Top = 40
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DAY1'
        ParamType = ptInput
        Value = 0d
      end
      item
        DataType = ftDateTime
        Name = 'DAY1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptInput
        Value = ''
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptInput
        Value = ''
      end>
  end
  object QueryReportDSDay: TQuery
    OnCalcFields = QueryReportDSDayCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 176
    Top = 40
    object QueryReportDSDayEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryReportDSDayDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
    end
    object QueryReportDSDayHDAY1: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY1'
      Calculated = True
    end
    object QueryReportDSDayMDAY1: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY1'
      Calculated = True
    end
    object QueryReportDSDayHDAY2: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY2'
      Calculated = True
    end
    object QueryReportDSDayMDAY2: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY2'
      Calculated = True
    end
    object QueryReportDSDayHDAY3: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY3'
      Calculated = True
    end
    object QueryReportDSDayMDAY3: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY3'
      Calculated = True
    end
    object QueryReportDSDayHDAY4: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY4'
      Calculated = True
    end
    object QueryReportDSDayMDAY4: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY4'
      Calculated = True
    end
    object QueryReportDSDayHDAY5: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY5'
      Calculated = True
    end
    object QueryReportDSDayMDAY5: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY5'
      Calculated = True
    end
    object QueryReportDSDayHDAY6: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY6'
      Calculated = True
    end
    object QueryReportDSDayMDAY6: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MDAY6'
      Calculated = True
    end
    object QueryReportDSDayMDAY7: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MDAY7'
      Calculated = True
    end
    object QueryReportDSDayHDAY7: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HDAY7'
      Calculated = True
    end
    object QueryReportDSDayHTOTAL: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HTOTAL'
      Calculated = True
    end
    object QueryReportDSDayMTOTAL: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MTOTAL'
      Calculated = True
    end
    object QueryReportDSDayBONUS: TFloatField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'BONUS'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object QueryReportDSTotal: TQuery
    OnCalcFields = QueryReportDSTotalCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 176
    Top = 104
    object IntegerField1: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object StringField1: TStringField
      FieldName = 'DESCRIPTION'
    end
    object IntegerField16: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'HTOTAL'
      Calculated = True
    end
    object IntegerField17: TIntegerField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'MTOTAL'
      Calculated = True
    end
    object QueryReportDSTotalBONUS: TFloatField
      DefaultExpression = '0'
      FieldKind = fkCalculated
      FieldName = 'BONUS'
      DisplayFormat = '0.00'
      Calculated = True
    end
  end
  object QueryBonusPerHour: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MISTAKE, BONUS_PER_HOUR '
      'FROM '
      '  QUALITYINCENTIVECONF'
      'ORDER BY '
      '  MISTAKE')
    Left = 308
    Top = 40
    object QueryBonusPerHourMISTAKE: TIntegerField
      FieldName = 'MISTAKE'
    end
    object QueryBonusPerHourBONUS_PER_HOUR: TCurrencyField
      FieldName = 'BONUS_PER_HOUR'
    end
  end
  object QueryIns: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 68
    Top = 104
  end
  object TableOverTime: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'OVERTIMEDEFINITION'
    Left = 72
    Top = 168
  end
  object TableHT: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 264
    Top = 168
  end
end
