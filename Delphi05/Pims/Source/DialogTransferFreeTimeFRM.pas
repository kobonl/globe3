(*
  MRA:27-JAN-2009 RV021.
    Team selection correction for QueryEmplFrom + QueryEmplTo,
    to prevent multiple the same records in combobox.
  MRA:19-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  - Changed so it is inherited from TDialogReportBaseF.
  - Restrict employees to teams-per-user: only employees must be used
    that are within plants defined for teams-per-user.
  SO:08-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
    - new holiday counters
  MRA:7-SEP-2010 RV067.MRA.30 550497
  - Special checkboxes for counters did not show.
  - Fields were not taken from absencetotal-source-record.
  MRA:5-OCT-2010 RV071.5. 550497
  - The ADV-fields had to be renamed, because they became too long:
    - USED_SHORTER_WWEEK_MINUTE -> USED_SHORTWWEEK_MINUTE
    - LAST_YEAR_SHORTER_WWEEK_MINUTE -> LAST_YEAR_SHORTWWEEK_MINUTE
  MRA:7-OCT-2010 RV071.8. 550497
  - Holiday Previous Year-checkbox:
    - The process should be transfer 'holiday' in NL
      transferred to the 'holiday' counter and in BEL
      to the 'holiday previous year' counter.
  - Checkbox Prev Year Holiday must be removed (made invisible).
  - Test if something was 'checked' is wrong, it only checks
    on 3 checkbox, but there are more checkboxes to check.
  - Two extra fields for ABSENCETOTAL must be initialized.
  - Take Seniority norm only from empl. contract.
  - Add norm of bank holiday to reserve to 'add. bank holiday'.
  - Norm Bank Holiday to reserve: Should be empty (zero), because it is
    filled with 'bank holiday'-dialog.
  - Shorter Working Week (ADV): Add also earned minutes!
  MRA:11-OCT-2010 RV071.11. 550497
  - Saturday Credit handling must be removed.
  MRA:11-OCT-2010 RV071.13 550497 Bugfixing.
  - Prev. hol. was stored wrong.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:16-MAY-2012 20013183
  - Add checkbox for 'travel time'. When this is checked it
    must transfer the time that is booked on travel time in balance
    to next year.
  MRA:16-OCT-2017 PIM-313
  - Transfer free time to next year does not work correct anymore.
  - Bugfix: Because TableEmpl.FindKey always gave False, it did not work correct
    anymore. Use Locate instead!
  MRA:28-JAN-2019 GLOB3-204
  - Personal Time Off USA
  MRA:11-MAR-2019 GLOB3-204
  - Personal Time Off USA - Update
  MRA:12-APR-2019 GLOB3-204
  - Personal Time Off USA - Rework
*)
unit DialogTransferFreeTimeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables,
  dxExGrEd, dxExELib, dxLayout, DialogReportBaseFRM, jpeg;
                                                                            
type
  TDialogTransferFreeTimeF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    GroupBoxTimeForTime: TGroupBox;
    Label1: TLabel;
    CheckBoxHoliday: TCheckBox;
    CheckBoxWTR: TCheckBox;
    CheckBoxTimeForTime: TCheckBox;
    TableAbsTotTarget: TTable;
    TableEmpl: TTable;
    CheckBoxNormHoliday: TCheckBox;
    QueryEmployeeContract: TQuery;
    qryAbsenceTotal: TQuery;
    Label14: TLabel;
    Label15: TLabel;
    Label25: TLabel;
    Label16: TLabel;
    Label27: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dxSpinEditYearFrom: TdxSpinEdit;
    Label23: TLabel;
    dxSpinEditYearTo: TdxSpinEdit;
    CheckBoxSeniorityHoliday: TCheckBox;
    CheckBoxAddBankHoliday: TCheckBox;
    CheckBoxSaturdayCredit: TCheckBox;
    CheckBoxRsvBankHoliday: TCheckBox;
    CheckBoxShorterWeek: TCheckBox;
    qryAbsenceTypePerCountryExist: TQuery;
    qryCountryEmployee: TQuery;
    CheckBoxPrevYearHoliday: TCheckBox;
    CheckBoxTravelTime: TCheckBox;
    CheckBoxCalculatePTO: TCheckBox;
    qryMaxPTOMinute: TQuery;
    qryPTODef: TQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TableEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CheckBoxNormHolidayClick(Sender: TObject);
    procedure CheckBoxCalculatePTOClick(Sender: TObject);
  private
    { Private declarations }
    CheckBoxHolidayCaption, CheckBoxWTRCaption,
    CheckBoxTimeForTimeCaption: String;
    procedure SetCheckBoxesVisible(AVisible: Boolean);
    function GetCounterActivated(AAbsenceType: Char;
      var ACounterDescription: String): Boolean;
    function GetNormSeniority(AEmplNo: Integer): Double;
    function AbsenceTypePerCountryExist: Boolean;
  public
    { Public declarations }
  end;

var
  DialogTransferFreeTimeF: TDialogTransferFreeTimeF;

// RV089.1.
function DialogTransferFreeTimeForm: TDialogTransferFreeTimeF;

implementation

uses ListProcsFRM, SystemDMT, UPimsMessageRes, UPimsConst;

{$R *.DFM}

// RV089.1.
var
  DialogTransferFreeTimeF_HND: TDialogTransferFreeTimeF;

// RV089.1.
function DialogTransferFreeTimeForm: TDialogTransferFreeTimeF;
begin
  if (DialogTransferFreeTimeF_HND = nil) then
  begin
    DialogTransferFreeTimeF_HND := TDialogTransferFreeTimeF.Create(Application);
    with DialogTransferFreeTimeF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogTransferFreeTimeF_HND;
end;

// RV089.1.
procedure TDialogTransferFreeTimeF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogTransferFreeTimeF_HND <> nil) then
  begin
    DialogTransferFreeTimeF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogTransferFreeTimeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
 // QryEmployeeByPlant.Close;
//  TableAbsTotSource.Close;
  TableAbsTotTarget.Close;
  TableEmpl.Close;
  Action := caFree;
end;

procedure TDialogTransferFreeTimeF.FormShow(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  InitDialog(True, True, False, True, False, False, False);
  inherited;
  Update;
  Screen.Cursor := crHourGlass;
  DecodeDate(Now(), Year, Month, Day);
  dxSpinEditYearFrom.Value := Year;
  dxSpinEditYearTo.Value := Year + 1;
  CheckBoxHoliday.Checked := False;
  CheckBoxWTR.Checked := False;
  CheckBoxTimeForTime.Checked := False;
  Update;
  Screen.Cursor := crDefault;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableEmpl);
  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  );
end;

procedure TDialogTransferFreeTimeF.btnOkClick(Sender: TObject);
var
  PlantFrom, PlantTo, DeptFrom, DeptTo: String;
  EmplFrom, EmplTo, Empl{, Year}: Integer;
  HolidayHourPerYear: Double;  // MR:03-01-2003 + MR:06-01-2005 Integer->Double
  YearFrom, YearTo: Integer; // MR:22-4-2005 Order 550396
  MaxPTOMinute: Integer; // GLOB3-204
  CalculatePTOMinute: Integer; // GLOB3-204
  //RV067.4.
  function NVL(AFieldName: String; DefaultValue: Variant): Variant;
  begin
    // RV067.MRA.30 This is not the source-table!
//    Result := TableAbsTotTarget.FieldByName(AFieldName).Value;
    Result := qryAbsenceTotal.FieldByName(AFieldName).Value;
    if Result = NULL then
      Result := DefaultValue;
  end;
  function ActiveEmployee(Empl: Integer): Boolean;
  begin
    Result := False;
    if TableEmpl.Locate('EMPLOYEE_NUMBER', Empl, []) then // Use Locate instead of FindKey or it always fails!
    begin
      if (PlantFrom <> PlantTo) then
        Result :=
          (TableEmpl.FieldByName('PLANT_CODE').AsString >= PlantFrom) and
           (TableEmpl.FieldByName('PLANT_CODE').AsString <= PlantTo)
      else
        Result :=
          (TableEmpl.FieldByName('PLANT_CODE').AsString = PlantFrom) and
          (TableEmpl.FieldByName('DEPARTMENT_CODE').AsString >= DeptFrom) and
          (TableEmpl.FieldByName('DEPARTMENT_CODE').AsString <= DeptTo) and
          (TableEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger >= EmplFrom) and
          (TableEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger <= EmplTo);
    end;
  end;
  // GLOB3-204
  function MaxPTOTransfer: Integer;
  begin
    Result := 0;
    with qryMaxPTOMinute do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
      Open;
      if not Eof then
        Result := FieldByName('MAX_PTO_MINUTE').AsInteger;
      Close;
    end;
  end; // MaxPTOTransfer
  // GLOB3-204
  function CalculatePTO: Integer;
  var
    EYears: Double;
    LastPTOMinute: Integer;
    ReferenceDate: TDateTime;
    procedure DetermineReferenceDate;
    begin
      ReferenceDate := EncodeDate(YearTo, 12, 31);
    end;
    function EmploymentYears(AStartDate: TDateTime): Double;
    begin
      // Calculate number of year + days based on from current date.
      Result := (ReferenceDate - AStartDate) / 365.0;
    end;
  begin
    Result := -1;
    LastPTOMinute := 0;
    DetermineReferenceDate;
    with qryPTODef do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
      Open;
      while (not Eof) and (Result = -1) do
      begin
        EYears := EmploymentYears(FieldByName('STARTDATE').AsDateTime);
        if EYears < 1.0 then
        begin
          Result := 0;
          Exit;
        end;
        // Does it fall in anniversary year?
        if (Trunc(EYears) = FieldByName('FROMEMPYEARS').AsInteger) then
        begin
          Result := LastPTOMinute +
              Round(
                (FieldByName('PTOMINUTE').AsInteger - LastPTOMinute) *
                  Frac(EYears) );
        end
        else
          if (EYears >= FieldByName('FROMEMPYEARS').AsInteger) and
            (EYears <= FieldByName('TILLEMPYEARS').AsInteger) then
            Result := Round(FieldByName('PTOMINUTE').AsInteger);
        LastPTOMinute := FieldByName('PTOMINUTE').AsInteger;
        Next;
      end;
    end;
  end; // CalculatePTO
  procedure FillValues(const NewRecord: Boolean;
    const NormHolidayMinute: Integer);
  var
    Last_Hol_Min, Earned_Hol_Min, Norm_Hol_Min, Used_Hol_Min: Real;
    CountryId: Integer;
//    CountryCode: String;
    PrevYearCounterActive: Boolean;
  begin
    CountryId := SystemDM.GetDBValue(
      'SELECT P.COUNTRY_ID FROM EMPLOYEE E, ' +
      '  PLANT P WHERE P.PLANT_CODE = E.PLANT_CODE ' +
      '  AND E.EMPLOYEE_NUMBER = ' + IntToStr(NVL('EMPLOYEE_NUMBER', ''))
      , 0
      );
    PrevYearCounterActive :=
      SystemDM.GetCounterActivated(CountryId, HOLIDAYPREVIOUSYEARAVAILABILITY);
    if NewRecord then
      TableAbsTotTarget.FieldByName('CREATIONDATE').Value := Now;
    TableAbsTotTarget.FieldByName('MUTATIONDATE').Value := Now;
    TableAbsTotTarget.FieldByName('MUTATOR').Value :=
      SystemDM.CurrentProgramUser;
    // RV071.8.
(*
    // Previous year holiday
    if CheckBoxPrevYearHoliday.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_HLD_PREV_YEAR_MINUTE', 0);
      if PrevYearCounterActive then
        Last_Hol_Min := NVL('LAST_YEAR_HOLIDAY_MINUTE', 0);
      Used_Hol_Min := NVL('USED_HLD_PREV_YEAR_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_HLD_PREV_YEAR_MINUTE').Value :=
        Last_Hol_Min - Used_Hol_Min;
    end;
*)
    // Holiday
    // Holiday Previous Year: Depends on counter-active.
    if CheckBoxHoliday.Checked then
    begin
      MaxPTOMinute := MaxPTOTransfer; // GLOB3-204
      if PrevYearCounterActive then
      begin
        // RV071.13. 550497 Bugfixing.
        // Store Values from Holiday in Last-year-hld-prev-year.
        Norm_Hol_Min := NVL('NORM_HOLIDAY_MINUTE', 0);
        Used_Hol_Min := NVL('USED_HOLIDAY_MINUTE', 0);
        TableAbsTotTarget.FieldByName('LAST_YEAR_HLD_PREV_YEAR_MINUTE').Value :=
          Norm_Hol_Min - Used_Hol_Min;
      end
      else
      begin
        Last_Hol_Min := NVL('LAST_YEAR_HOLIDAY_MINUTE', 0);
        Norm_Hol_Min := NVL('NORM_HOLIDAY_MINUTE', 0);
        Used_Hol_Min := NVL('USED_HOLIDAY_MINUTE', 0);
        TableAbsTotTarget.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value :=
          Last_Hol_Min + Norm_Hol_Min - Used_Hol_Min;
        // GLOB3-204
        if MaxPTOMinute > 0 then
        begin
          if (Last_Hol_Min + Norm_Hol_Min - Used_Hol_Min) > MaxPTOMinute then
            TableAbsTotTarget.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value :=
              MaxPTOMinute;
        end;
      end;
    end;
    // Work time reduction
    if CheckBoxWTR.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_WTR_MINUTE', 0);
      Earned_Hol_Min := NVL('EARNED_WTR_MINUTE', 0);
      Used_Hol_Min := NVL('USED_WTR_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_WTR_MINUTE').Value :=
        Last_Hol_Min + Earned_Hol_Min - Used_Hol_Min;
    end;
    // Time for Time
    if CheckBoxTimeForTime.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_TFT_MINUTE', 0);
      Earned_Hol_Min := NVL('EARNED_TFT_MINUTE', 0);
      Used_Hol_Min := NVL('USED_TFT_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_TFT_MINUTE').Value :=
        Last_Hol_Min + Earned_Hol_Min - Used_Hol_Min;
    end;
    // Seniority holiday
    if CheckBoxSeniorityHoliday.Checked then
    begin
      // RV071.8. Take norm only from empl. contract!
      Norm_Hol_Min := GetNormSeniority(Empl);
//      Used_Hol_Min := NVL('USED_SENIORITY_HOLIDAY_MINUTE', 0);
      TableAbsTotTarget.FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').Value :=
        Norm_Hol_Min;
    end;
    // Add. bank holiday
    if CheckBoxAddBankHoliday.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_ADD_BANK_HLD_MINUTE', 0);
      Used_Hol_Min := NVL('USED_ADD_BANK_HLD_MINUTE', 0);
      // RV071.8. Add norm of 'bank holiday to reserve'.
      Norm_Hol_Min := NVL('NORM_BANK_HLD_RESERVE_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_ADD_BANK_HLD_MINUTE').Value :=
        Last_Hol_Min + Norm_Hol_Min - Used_Hol_Min;
    end;
    // RV071.11. 550497 Sat Credit.
(*
    // Saturday credit
    if CheckBoxSaturdayCredit.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_SAT_CREDIT_MINUTE', 0);
      Used_Hol_Min := NVL('USED_SAT_CREDIT_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_SAT_CREDIT_MINUTE').Value :=
        Last_Hol_Min - Used_Hol_Min;
    end;
*)    
    // Bank holiday to reserve
    if CheckBoxRsvBankHoliday.Checked then
    begin
      // RV071.8. Norm Bank Holiday to reserve:
      //          Should be empty (zero), because it is filled with
      //         'bank holiday'-dialog.
//      Norm_Hol_Min := NVL('NORM_BANK_HLD_RESERVE_MINUTE', 0);
//      Used_Hol_Min := NVL('USED_BANK_HLD_RESERVE_MINUTE', 0);
      Norm_Hol_Min := 0;
      Used_Hol_Min := 0;
      TableAbsTotTarget.FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').Value :=
        Norm_Hol_Min - Used_Hol_Min;
    end;
    // Shorter Week
    if CheckBoxShorterWeek.Checked then
    begin
      // RV071.5. Fields are renamed!
      Last_Hol_Min := NVL('LAST_YEAR_SHORTWWEEK_MINUTE', 0);
      // RV076.8. Add also earned minutes!
      Earned_Hol_Min := NVL('EARNED_SHORTWWEEK_MINUTE', 0);
      Used_Hol_Min := NVL('USED_SHORTWWEEK_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_SHORTWWEEK_MINUTE').Value :=
        Last_Hol_Min + Earned_Hol_Min - Used_Hol_Min;
    end;

    if CheckBoxNormHoliday.Checked then
    begin
      TableAbsTotTarget.FieldByName('NORM_HOLIDAY_MINUTE').Value :=
        NormHolidayMinute;
    end;

    // GLOB3-204
    if CheckBoxCalculatePTO.Checked then
    begin
      CalculatePTOMinute := CalculatePTO;
      if CalculatePTOMinute >= 0 then
        TableAbsTotTarget.FieldByName('NORM_HOLIDAY_MINUTE').Value :=
          CalculatePTOMinute;
    end;

    // 20013183
    // Travel Time
    if CheckBoxTravelTime.Checked then
    begin
      Last_Hol_Min := NVL('LAST_YEAR_TRAVELTIME_MINUTE', 0);
      Earned_Hol_Min := NVL('EARNED_TRAVELTIME_MINUTE', 0);
      Used_Hol_Min := NVL('USED_TRAVELTIME_MINUTE', 0);
      TableAbsTotTarget.FieldByName('LAST_YEAR_TRAVELTIME_MINUTE').Value :=
        Last_Hol_Min + Earned_Hol_Min - Used_Hol_Min;
    end;
  end;
  procedure InitializeValues;
  begin
    TableAbsTotTarget.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('NORM_HOLIDAY_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_HOLIDAY_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('LAST_YEAR_TFT_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('EARNED_TFT_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_TFT_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('LAST_YEAR_WTR_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('EARNED_WTR_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_WTR_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('ILLNESS_MINUTE').Value := 0;
    //RV067.4.
    TableAbsTotTarget.FieldByName('LAST_YEAR_HLD_PREV_YEAR_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_HLD_PREV_YEAR_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').Value := 0;

    TableAbsTotTarget.FieldByName('USED_SENIORITY_HOLIDAY_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('LAST_YEAR_ADD_BANK_HLD_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_ADD_BANK_HLD_MINUTE').Value := 0;
    // RV071.11. 550497 Sat Credit.
//    TableAbsTotTarget.FieldByName('LAST_YEAR_SAT_CREDIT_MINUTE').Value := 0;
//    TableAbsTotTarget.FieldByName('USED_SAT_CREDIT_MINUTE').Value := 0;
//    TableAbsTotTarget.FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_BANK_HLD_RESERVE_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('LAST_YEAR_SHORTWWEEK_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_SHORTWWEEK_MINUTE').Value := 0;
    // RV071.8. 550497 Two extra fields for ABSENCETOTAL must be initialized.
    TableAbsTotTarget.FieldByName('NORM_BANK_HLD_RES_MIN_MAN_YN').Value := 'N';
    TableAbsTotTarget.FieldByName('EARNED_SHORTWWEEK_MINUTE').Value := 0;
    // 20013183 Travel Time
    TableAbsTotTarget.FieldByName('LAST_YEAR_TRAVELTIME_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('EARNED_TRAVELTIME_MINUTE').Value := 0;
    TableAbsTotTarget.FieldByName('USED_TRAVELTIME_MINUTE').Value := 0;
  end;
begin
  inherited;
  if (dxSpinEditYearFrom.Value = dxSpinEditYearTo.Value) then
  begin
    DisplayMessage(SPimsDifferentYears, mtInformation, [mbOk]);
    Exit;
  end;
  // MR:22-04-2005 Order 550396
  YearFrom := Round(dxSpinEditYearFrom.Value);
  YearTo := Round(dxSpinEditYearTo.Value);
  // RV071.11. 550497 Sat Credit.
  // 20013183 Travel Time
  if (
    (not CheckBoxHoliday.Checked) and (not CheckBoxWTR.Checked) and
    (not CheckBoxTimeForTime.Checked) and (not CheckBoxNormHoliday.Checked) and
    (not CheckBoxSeniorityHoliday.Checked) {and (not CheckBoxSaturdayCredit.Checked)} and
    (not CheckBoxAddBankHoliday.Checked) and (not CheckBoxRsvBankHoliday.Checked) and
    (not CheckBoxShorterWeek.Checked) and
    (not CheckBoxTravelTime.Checked) and
    (not CheckBoxCalculatePTO.Checked)
    ) then
  begin
    DisplayMessage(SPimsCheckBox, mtInformation, [mbOk]);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);
  DeptFrom := GetStrValue(CmbPlusDepartmentFrom.Value);
  DeptTo := GetStrValue(CmbPlusDepartmentTo.Value);
  if dxDBExtLookupEditEmplFrom.Text <> '' then
    EmplFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
//     GetIntValue(ComboBoxPlusEmplFrom.Value);
  if dxDBExtLookupEditEmplTo.Text <> '' then
    EmplTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
//    GetIntValue(ComboBoxPlusEmplTo.Value);

  // MR:22-04-2005
  if PlantFrom <> PlantTo then
  begin
    // MR:13-06-2005 Both were 'EmplFrom'!
    EmplFrom := 1;
    EmplTo := 99999999;
  end;

  // MR:13-06-2005 Get Norm-Holiday from 'EmployeeContract'.
  // Open Query that is used later.
  // MR:13-03-2007 Order 550444. Get norm-holiday from employee-contract
  //               active in target-year, instead active in source-year.
  if CheckBoxNormHoliday.Checked then
  begin
    QueryEmployeeContract.Close;
    QueryEmployeeContract.ParamByName('DATEFROM').AsDateTime :=
      EncodeDate(YearTo, 1, 1);
    QueryEmployeeContract.ParamByName('DATETO').AsDateTime :=
      EncodeDate(YearTo, 12, 31);
    QueryEmployeeContract.ParamByName('EMPLOYEEFROM').AsInteger := EmplFrom;
    QueryEmployeeContract.ParamByName('EMPLOYEETO').AsInteger := EmplTo;
    QueryEmployeeContract.Open;
  end;
  // MR:03-01-2007 Order 550435. Use a Query to get the Absencetotal-records.
  // This ensures there will also be a result even if Absencetotal-table is
  // empty. For example, if the normholiday should be filled.
{
  TableAbsTotSource.Filtered := False;
  TableAbsTotSource.Filter :=
    'ABSENCE_YEAR = ' + IntToStr(YearFrom) + ' AND ' +
    'EMPLOYEE_NUMBER >= ' + IntToStr(EmplFrom) + ' AND ' +
    'EMPLOYEE_NUMBER <= ' + IntToStr(EmplTo);
  TableAbsTotSource.Filtered := True;
}
  qryAbsenceTotal.Close;
  qryAbsenceTotal.ParamByName('ABSENCE_YEAR').AsInteger := YearFrom;
  qryAbsenceTotal.ParamByName('EMPLOYEEFROM').AsInteger := EmplFrom;
  qryAbsenceTotal.ParamByName('EMPLOYEETO').AsInteger := EmplTo;
  qryAbsenceTotal.Open;

  qryAbsenceTotal.First;
  while not qryAbsenceTotal.Eof do
  begin
    Empl := qryAbsenceTotal.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if ActiveEmployee(Empl) then
    begin
      // MR:13-06-2005 Get Norm-Holiday from 'EmployeeContract'
      // NOTE: If an employee has contract-changes during
      // ONE year, it can result in more than 1 EmployeeContract-record...
      // Here always the first one of the year will be taken.
      HolidayHourPerYear := 0;
      if CheckBoxNormHoliday.Checked then
      begin
        QueryEmployeeContract.Filtered := False;
        QueryEmployeeContract.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(Empl);
        QueryEmployeeContract.Filtered := True;
        if not QueryEmployeeContract.IsEmpty then
          if (QueryEmployeeContract.
            FieldByName('HOLIDAY_HOUR_PER_YEAR').Value <> Null) then
              HolidayHourPerYear :=
                QueryEmployeeContract.
                  FieldByName('HOLIDAY_HOUR_PER_YEAR').Value;
      end;
      if TableAbsTotTarget.FindKey([Empl, YearTo]) then
      begin
        TableAbsTotTarget.Edit;
        FillValues(False, Round(HolidayHourPerYear * 60));
        TableAbsTotTarget.Post;
      end
      else
      begin
        TableAbsTotTarget.Insert;
        TableAbsTotTarget.FieldByName('EMPLOYEE_NUMBER').Value := Empl;
        TableAbsTotTarget.FieldByName('ABSENCE_YEAR').Value :=
          YearTo;
        InitializeValues;
        FillValues(True, Round(HolidayHourPerYear * 60));
        TableAbsTotTarget.Post;
      end;
    end; {active employee}
    qryAbsenceTotal.Next;
  end; {while}

  qryAbsenceTotal.Close;
  Screen.Cursor := crDefault;
  DisplayMessage(SPimsProcessFinish, mtInformation, [mbOk]);
end;

procedure TDialogTransferFreeTimeF.FormCreate(Sender: TObject);
begin
  inherited;
  TableEmpl.Open;
//  TableAbsTotSource.Open;
  TableAbsTotTarget.Open;
  CheckBoxHolidayCaption := CheckBoxHoliday.Caption;
  CheckBoxWTRCaption := CheckBoxWTR.Caption;
  CheckBoxTimeForTimeCaption := CheckBoxTimeForTime.Caption;
end;

procedure TDialogTransferFreeTimeF.TableEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

function TDialogTransferFreeTimeF.GetCounterActivated(
  AAbsenceType: Char; var ACounterDescription: String): Boolean;
var
  CountryId: Integer;
  StrValue: String;
begin
  Result := False;
  if Trim(CmbPlusPlantFrom.Value) = '' then Exit;

  CountryId := SystemDM.GetDBValue(
    Format(
    'select p.country_id from plant p where plant_code = %s',
    [GetStrValue(CmbPlusPlantFrom.Value)]
    ), 0
  );

  if CountryId = 0 then Exit;

  Result := SystemDM.GetCounterActivated(CountryId, AAbsenceType);

  if Result then
  begin
    StrValue := SystemDM.GetDBValue(
      Format(
      'select ac.counter_description from absencetypepercountry ac ' +
      'where country_id = %d and absencetype_code = ''%s''',
      [CountryId, AAbsenceType]
      ), '*'
    );
    if StrValue <> '*' then
      ACounterDescription := StrValue;
  end;
end;

procedure TDialogTransferFreeTimeF.SetCheckBoxesVisible(AVisible: Boolean);
  procedure SetVisible(ACk: TCheckbox; AAbsenceType: Char);
  var
    Desc: String;
    ACheckBoxVisible: Boolean;
  begin
    if AVisible then
    begin
      ACheckBoxVisible := AVisible and GetCounterActivated(AAbsenceType, Desc);
      if ACheckBoxVisible then
      begin
        ACk.Visible := True;
        ACk.Caption := Desc;
      end
      else
      begin
        ACk.Visible := False;
        ACk.Checked := False;
      end;
    end
    else
    begin
      ACk.Visible := False;
      ACk.Checked := False;
    end;
  end;
begin
  if AbsenceTypePerCountryExist then
  begin
    // RV067.MRA.29
    SetVisible(CheckBoxHoliday, HOLIDAYAVAILABLETB);
    SetVisible(CheckBoxWTR, WORKTIMEREDUCTIONAVAILABILITY);
    SetVisible(CheckBoxTimeForTime, TIMEFORTIMEAVAILABILITY);
    // New counters.
    SetVisible(CheckBoxPrevYearHoliday, HOLIDAYPREVIOUSYEARAVAILABILITY);
    SetVisible(CheckBoxSeniorityHoliday, SENIORITYHOLIDAYAVAILABILITY);
    SetVisible(CheckBoxAddBankHoliday, ADDBANKHOLIDAYAVAILABILITY);
    // RV071.11. 550497 Sat Credit.
//    SetVisible(CheckBoxSaturdayCredit, SATCREDITAVAILABILITY);
    SetVisible(CheckBoxRsvBankHoliday, BANKHOLIDAYRESERVEAVAILABILITY);
    SetVisible(CheckBoxShorterWeek, SHORTERWORKWEEKAVAILABILITY);
    // 20013183
    SetVisible(CheckBoxTravelTime, TRAVELTIMEAVAILABILITY);
  end
  else
  begin
    // RV067.MRA.29
    // These must be visible when there are NO 'per-country'-definitions.
    CheckBoxHoliday.Caption := CheckBoxHolidayCaption;
    CheckBoxHoliday.Visible := True;
    CheckBoxWTR.Caption := CheckBoxWTRCaption;
    CheckBoxWTR.Visible := True;
    CheckBoxTimeForTime.Caption := CheckBoxTimeForTimeCaption;
    CheckBoxTimeForTime.Visible := True;
    // New counters. These are not default visible.
    CheckBoxPrevYearHoliday.Visible := False;
    CheckBoxPrevYearHoliday.Checked := False;
    CheckBoxSeniorityHoliday.Visible := False;
    CheckBoxSeniorityHoliday.Checked := False;
    CheckBoxAddBankHoliday.Visible := False;
    CheckBoxAddBankHoliday.Checked := False;
    // RV071.11. 550497 Sat Credit.
//    CheckBoxSaturdayCredit.Visible := False;
//    CheckBoxSaturdayCredit.Checked := False;
    CheckBoxRsvBankHoliday.Visible := False;
    CheckBoxRsvBankHoliday.Checked := False;
    CheckBoxShorterWeek.Visible := False;
    CheckBoxShorterWeek.Checked := False;
    // 20013183
    CheckBoxTravelTime.Visible := False;
    CheckBoxTravelTime.Checked := False;
  end;
end;

procedure TDialogTransferFreeTimeF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  );
end;

procedure TDialogTransferFreeTimeF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  );
end;

function TDialogTransferFreeTimeF.GetNormSeniority(AEmplNo: Integer): Double;
var
  CountryId: Integer;
  CounterActivated: Boolean;
  SeniorityHours: Double;
begin
  Result := 0;
  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(AEmplNo), 0);

(*
This means 'seniority holiday norm' that exists in Employee-contract must be taken to fill the norm for the ABSENCETOTAL-table for this counter.
*)
//When absence type 'E' is active for the country of the employees' plant
  CounterActivated :=
    SystemDM.GetCounterActivated(CountryId, SENIORITYHOLIDAYAVAILABILITY);
  if not CounterActivated then Exit;

//When 'seniority holiday' is set (not 0) in Employee-contract, then the 'norm'-field for this type must be calculated.
  SeniorityHours := SystemDM.GetDBValue(
  ' select ec.seniority_hours from employeecontract ec ' +
  ' where ' +
  '   ec.employee_number =  ' + IntToStr(AEmplNo) +
  '   and ec.startdate <= sysdate and sysdate <= ec.enddate ', 0);

//Fill with EMPLOYEECONTRACT.SENIORITY_HOUR
  if SeniorityHours <> 0 then
    Result := SeniorityHours * 60;
end;

// RV067.MRA.29 Are there any absencetype-per-country defined?
function TDialogTransferFreeTimeF.AbsenceTypePerCountryExist: Boolean;
var
  CountryId: Integer;
begin
  Result := False;
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CountryId := SystemDM.GetDBValue(
      ' select NVL(p.country_id, 0) from ' +
      ' plant p ' +
      ' where ' +
      '   p.plant_code = ' + '''' + GetStrValue(CmbPlusPlantFrom.Value) + '''',
      0
    );
    if CountryID <> 0 then
    begin
      with qryAbsenceTypePerCountryExist do
      begin
        Close;
        ParamByName('COUNTRY_ID').AsInteger :=
          CountryID;
        Open;
        Result := not Eof;
        Close;
      end;
    end;
  end;
end;

procedure TDialogTransferFreeTimeF.CheckBoxNormHolidayClick(
  Sender: TObject);
begin
  inherited;
  // GLOB3-204
  if CheckBoxNormHoliday.Checked then
  begin
    CheckBoxCalculatePTO.Checked := False;
    CheckBoxCalculatePTO.Enabled := False;
  end
  else
    CheckBoxCalculatePTO.Enabled := True;

end;

procedure TDialogTransferFreeTimeF.CheckBoxCalculatePTOClick(
  Sender: TObject);
begin
  inherited;
  // GLOB3-204
  if CheckBoxCalculatePTO.Checked then
  begin
    CheckBoxNormHoliday.Checked := False;
    CheckBoxNormHoliday.Enabled := False;
  end
  else
    CheckBoxNormHoliday.Enabled := True;
end;

end.
