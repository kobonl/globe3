inherited DialogReportRatioIllnessHrsF: TDialogReportRatioIllnessHrsF
  Caption = 'Report ratio illness hours'
  ClientHeight = 317
  ClientWidth = 576
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 576
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 280
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 576
    Height = 196
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 64
    end
    inherited LblEmployee: TLabel
      Top = 64
    end
    inherited LblToPlant: TLabel
      Top = 65
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 70
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [15]
      Left = 8
      Top = 89
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [16]
      Left = 40
      Top = 89
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [17]
      Left = 315
      Top = 91
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 90
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 91
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 126
      TabOrder = 23
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 127
      TabOrder = 10
    end
    inherited CheckBoxAllTeams: TCheckBox
      TabOrder = 11
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 126
      TabOrder = 2
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      ColCount = 127
      TabOrder = 3
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 65
      TabOrder = 5
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 65
      TabOrder = 6
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 133
      TabOrder = 13
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 134
      TabOrder = 14
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 15
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 22
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 12
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 18
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 145
      TabOrder = 16
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 146
      TabOrder = 17
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 146
      TabOrder = 19
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 147
      TabOrder = 20
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 152
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 151
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 120
      Width = 505
      Height = 73
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 45
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 1
      end
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 90
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 7
      OnCloseUp = DateFromCloseUp
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 90
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 8
      OnCloseUp = DateToCloseUp
    end
  end
  inherited stbarBase: TStatusBar
    Top = 257
    Width = 576
  end
  inherited pnlBottom: TPanel
    Top = 276
    Width = 576
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 160
  end
  inherited QueryEmplFrom: TQuery
    Left = 56
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 224
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 552
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4F040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072F4469616C6F675265706F7274526174696F496C6C6E6573
        73487273462E44617461536F75726365456D706C46726F6D104F7074696F6E73
        437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F4261
        6E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F
        6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F
        6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E4465
        6C6574650D6564676F43616E496E73657274116564676F43616E4E6176696761
        74696F6E116564676F436F6E6669726D44656C657465126564676F4C6F616441
        6C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F546478
        444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F
        6E06064E756D62657206536F7274656407046373557005576964746802410942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C75
        6D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F72
        74206E616D6505576964746802540942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060A53484F52545F4E414D4500000F5464
        78444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E
        06044E616D6505576964746803B4000942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060B4445534352495054494F4E00000F
        546478444247726964436F6C756D6E0D436F6C756D6E41646472657373074361
        7074696F6E06074164647265737305576964746802450942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060741444452455353
        00000F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F64
        650743617074696F6E060F4465706172746D656E7420636F6465055769647468
        02580942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F4445504152544D454E545F434F444500000F546478444247726964
        436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20
        636F64650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        16040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072D4469616C6F675265706F72
        74526174696F496C6C6E657373487273462E44617461536F75726365456D706C
        546F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76
        696E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76
        696E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A
        696E6700094F7074696F6E7344420B106564676F43616E63656C4F6E45786974
        0D6564676F43616E44656C6574650D6564676F43616E496E7365727411656467
        6F43616E4E617669676174696F6E116564676F436F6E6669726D44656C657465
        126564676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D
        61726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D70
        6C0743617074696F6E06064E756D62657206536F727465640704637355700557
        6964746802310942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060F454D504C4F5945455F4E554D42455200000F5464784442
        47726964436F6C756D6E0F436F6C756D6E53686F72744E616D65074361707469
        6F6E060A53686F7274206E616D65055769647468024E0942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060A53484F52545F4E
        414D4500000F546478444247726964436F6C756D6E11436F6C756D6E44657363
        72697074696F6E0743617074696F6E06044E616D6505576964746803CC000942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0B4445534352495054494F4E00000F546478444247726964436F6C756D6E0D43
        6F6C756D6E416464726573730743617074696F6E060741646472657373055769
        64746802650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506074144445245535300000F546478444247726964436F6C756D
        6E0E436F6C756D6E44657074436F64650743617074696F6E060F446570617274
        6D656E7420636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060F4445504152544D454E545F434F444500000F5464
        78444247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E
        06095465616D20636F64650942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 420
  end
  inherited QueryEmplTo: TQuery
    Left = 344
  end
end
