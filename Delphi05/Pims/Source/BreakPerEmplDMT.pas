(*
  MRA:25-MAR-2013 TD-22311
  - In second grid: Show plant-shift-combination, to make
    it possible to also show breaks-per-employee for
    employee where the plant was changed.
  - Also filter on teams-per-user
  - Added TablePlant for looking up plant description for TableMaster.
*)
unit BreakPerEmplDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TBreakPerEmplDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TableMasterPLANTLU: TStringField;
    DataSourceEmpl: TDataSource;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailBREAK_NUMBER: TIntegerField;
    TableDetailPAYED_YN: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableTempBreakPerEmpl: TTable;
    TableDetailPLANTLU: TStringField;
    TableExportEMPLU: TStringField;
    QueryEmpl: TQuery;
    QueryEmplFLOAT_EMP: TFloatField;
    QueryEmplEMPLOYEE_NUMBER: TIntegerField;
    QueryEmplDESCRIPTION: TStringField;
    QueryEmplSHORT_NAME: TStringField;
    QueryEmplDEPARTMENT_CODE: TStringField;
    QueryEmplADDRESS: TStringField;
    QueryEmplPLANT_CODE: TStringField;
    QueryEmplPLANTLU: TStringField;
    QueryEmplSHIFT_NUMBER: TIntegerField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEmplCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetEmployee;
  end;

var
  BreakPerEmplDM: TBreakPerEmplDM;

implementation

uses SystemDMT, UPimsMessageRes, UPimsConst, BreakPerEmplFRM;

{$R *.DFM}

procedure TBreakPerEmplDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('BREAK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('BREAK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;
  if (CheckBlocks(Handle_Grid, TableMaster, True) <> 0 ) then
  begin
    DisplayMessage(SPIMSTimeIntervalError, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerEmplDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  SetNullValues(TableDetail);
  TableDetail.FieldByName('EMPLOYEE_NUMBER').AsString :=
    QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString;
  TableDetail.FieldByName('PLANT_CODE').AsString :=
    TableMaster.FieldByName('PLANT_CODE').AsString;
  TableDetail.FieldByName('SHIFT_NUMBER').AsInteger :=
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('BREAK_NUMBER').AsInteger := Index + 1;
  TableDetail.FieldByName('PAYED_YN').AsString := UNCHECKEDVALUE;

  if TableTempBreakPerEmpl.FindKey([QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    TableMaster.FieldByName('PLANT_CODE').AsString,
      TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
    for Index := 1 to 7 do
    begin
      DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
        TableTempBreakPerEmpl.FieldByName('ENDTIME' + IntToStr(Index)).Value;
      DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
        TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
    end
    else
      InitializeTimeValues(TableDetail, TableMaster);
end;

procedure TBreakPerEmplDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailBREAK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerEmplDM.SetEmployee;
var
  SubStr: String;
begin
//car 550279
  SubStr :=
    'EMPLOYEE_NUMBER = ' +  QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' AND PLANT_CODE = ' + '''' +
    DoubleQuote(TableMaster.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND SHIFT_NUMBER = ' +
  TableMaster.FieldByName('SHIFT_NUMBER').AsString;
  if TableDetail.Filter = SubStr then
    Exit;
  TableDetail.Filter := SubStr;
end;

procedure TBreakPerEmplDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetEmployee;
end;

procedure TBreakPerEmplDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //CAR: 550279
  // TD-22311 Shift added.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryEmpl.Sql.Clear;
    QueryEmpl.Sql.Add(
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME, ' + NL +
      '  E.DESCRIPTION, E.PLANT_CODE, P.DESCRIPTION AS PLANTLU, ' + NL +
      '  E.DEPARTMENT_CODE, P.ADDRESS, ' + NL +
      '  E.SHIFT_NUMBER ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, PLANT P, TEAMPERUSER T ' + NL +
      'WHERE ' + NL +
      '  E.PLANT_CODE = P.PLANT_CODE AND T.USER_NAME = :USER_NAME AND ' + NL +
      '  E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'ORDER BY ' + NL +
      '  E.EMPLOYEE_NUMBER '
      );
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryEmpl.Prepare;  
  end;
  QueryEmpl.Open;
  // TD-22311 Also filter on team
  SystemDM.PlantTeamFilterEnable(Tablemaster);
end;

procedure TBreakPerEmplDM.QueryEmplCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('FLOAT_EMP').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TBreakPerEmplDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
 //car 550279
  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
end;

// TD-22311 Also filter on teams-per-user
procedure TBreakPerEmplDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
