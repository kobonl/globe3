unit EmployeeRegsBaseFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SideMenuBaseFRM, ImgList, ActnList, dxBarDBNav, dxBar, ComCtrls, ExtCtrls;

type
  TEmployeeRegsBaseF = class(TSideMenuBaseF)
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure NilInsertForm;
  end;

var
  EmployeeRegsBaseF: TEmployeeRegsBaseF;

implementation

{$R *.DFM}

{ TEmployeeRegsFormBaseF }

// RV089.1. Ensure the form stays on top of the calling form.
procedure TEmployeeRegsBaseF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TEmployeeRegsBaseF.NilInsertForm;
begin
  inherited;
end;

end.
