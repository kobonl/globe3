(*
  MRA:20-AUG-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Changed color for LightGreen to Blue
*)
unit StaffPlanningLegendFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PimsFRM, ExtCtrls, StdCtrls, StaffPlanningUnit;

type
  TStaffPlanningLegendF = class(TPimsF)
    GroupBoxBtn: TGroupBox;
    GroupBoxOci: TGroupBox;
    ButtonToBePln: TButton;
    Label1: TLabel;
    Label2: TLabel;
    PanelPln: TPanel;
    PanelPlnS: TPanel;
    Label3: TLabel;
    PanelPlnWK: TPanel;
    Label4: TLabel;
    PanelPlnNotAV: TPanel;
    Label5: TLabel;
    PanelLevelA: TPanel;
    PanelLevelB: TPanel;
    PanelLevelC: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    PanelOCIPln: TPanel;
    PanelOCIOver: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Button2: TButton;
    Label11: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StaffPlanningLegendF: TStaffPlanningLegendF;

implementation

{$R *.DFM}

procedure TStaffPlanningLegendF.FormShow(Sender: TObject);
begin
  inherited;
  PanelPln.Color := Green;
//  PanelPlnS.Color := LightGreen;
  PanelPlnWK.Color := Normal;
  PanelPlnNotAV.Color := DarkGrey;;
  PanelLevelC.Color := LightRed;
  PanelLevelA.Color := DarkRed;
  PanelLevelB.Color := Red;
  PanelOCIPln.Color := Green;
  PanelOCIOver.Color := Yellow;
end;

end.
