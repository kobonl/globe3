inherited EmployeeDM: TEmployeeDM
  OldCreateOrder = True
  Left = 256
  Top = 105
  Height = 743
  Width = 833
  inherited TableMaster: TTable
    Left = 64
    Top = 16
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    BeforeDelete = TableDetailBeforeDelete
    AfterScroll = TableDetailAfterScroll
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    IndexName = 'XPKEMPLOYEE'
    TableName = 'EMPLOYEE'
    UpdateMode = upWhereKeyOnly
    Left = 64
    Top = 80
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 40
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableDetailZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableDetailCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableDetailSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableDetailLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      OnValidate = DefaultNotEmptyValidate
      Size = 3
    end
    object TableDetailPHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableDetailEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableDetailDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableDetailSEX: TStringField
      FieldName = 'SEX'
      Size = 1
    end
    object TableDetailSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableDetailTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableDetailENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableDetailCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableDetailREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableDetailCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableDetailFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableDetailFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object TableDetailCONTRACTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'CONTRACTLU'
      LookupDataSet = TableContractGroup
      LookupKeyFields = 'CONTRACTGROUP_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CONTRACTGROUP_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailLANGUAGELU: TStringField
      FieldKind = fkLookup
      FieldName = 'LANGUAGELU'
      LookupDataSet = TableLanguage
      LookupKeyFields = 'LANGUAGE_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'LANGUAGE_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailTEAMLU: TStringField
      FieldKind = fkLookup
      FieldName = 'TEAMLU'
      LookupDataSet = QueryTeam
      LookupKeyFields = 'TEAM_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'TEAM_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailSTARTDATE: TDateTimeField
      DisplayWidth = 10
      FieldName = 'STARTDATE'
    end
    object TableDetailFIRSTAIDEXP_DATE_CAL: TDateField
      FieldKind = fkCalculated
      FieldName = 'FIRSTAIDEXP_DATE_CAL'
      Calculated = True
    end
    object TableDetailEMPCALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailGRADE: TIntegerField
      FieldName = 'GRADE'
    end
    object TableDetailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object TableDetailSHIFTDESC: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFTDESC'
      LookupDataSet = TableShift
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      Size = 30
      Lookup = True
    end
    object TableDetailSHIFTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFTLU'
      LookupDataSet = QueryShift
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      Size = 30
      Lookup = True
    end
    object TableDetailDEPTDESC: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPTDESC'
      LookupDataSet = TableDept
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailDEPTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPTLU'
      LookupDataSet = QueryDept
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailFREETEXT: TMemoField
      FieldName = 'FREETEXT'
      BlobType = ftMemo
      Size = 1600
    end
    object TableDetailBOOK_PROD_HRS_YN: TStringField
      FieldName = 'BOOK_PROD_HRS_YN'
      Size = 1
    end
    object TableDetailFAKE_EMPLOYEE_YN: TStringField
      FieldName = 'FAKE_EMPLOYEE_YN'
      Size = 4
    end
    object TableDetailEMPLOYEE_ID: TFloatField
      FieldName = 'EMPLOYEE_ID'
    end
    object TableDetailETHNICITY: TStringField
      FieldName = 'ETHNICITY'
      Size = 120
    end
    object TableDetailJOBTITLE: TStringField
      FieldName = 'JOBTITLE'
      Size = 120
    end
    object TableDetailMARITALSTATE: TStringField
      FieldName = 'MARITALSTATE'
      Size = 120
    end
    object TableDetailEXT_REF: TStringField
      FieldName = 'EXT_REF'
      Size = 80
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 184
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Left = 184
    Top = 84
  end
  inherited TableExport: TTable
    Left = 444
    Top = 340
  end
  inherited DataSourceExport: TDataSource
    Left = 544
    Top = 340
  end
  object TableContractGroup: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'CONTRACTGROUP'
    Left = 64
    Top = 136
    object TableContractGroupCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableContractGroupDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableContractGroupCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableContractGroupTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Size = 1
    end
    object TableContractGroupBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Size = 1
    end
    object TableContractGroupMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableContractGroupOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableContractGroupMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableContractGroupPERIOD_STARTS_IN_WEEK: TIntegerField
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableContractGroupWEEKS_IN_PERIOD: TIntegerField
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableContractGroupWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Size = 1
    end
  end
  object DataSourceContractGroup: TDataSource
    DataSet = TableContractGroup
    Left = 184
    Top = 136
  end
  object TableLanguage: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'LANGUAGE'
    Left = 64
    Top = 216
    object TableLanguageLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Required = True
      Size = 3
    end
    object TableLanguageDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
    end
    object TableLanguageCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableLanguageMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableLanguageMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object DataSourceLanguage: TDataSource
    DataSet = TableLanguage
    Left = 168
    Top = 216
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = TablePlantFilterRecord
    TableName = 'PLANT'
    Left = 64
    Top = 272
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 168
    Top = 272
  end
  object DataSourceTeam: TDataSource
    DataSet = QueryTeam
    Left = 384
    Top = 80
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT EMPLOYEE_NUMBER FROM EMPLOYEE'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE')
    Left = 352
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end>
  end
  object StoredProcDelete: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'EMPLOYEE_DELETECASCADE'
    Left = 456
    Top = 272
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptInput
      end>
  end
  object QueryCheckDeptTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      ' SELECT PLANT_CODE, TEAM_CODE, DEPARTMENT_CODE '
      ' FROM DEPARTMENTPERTEAM  '
      ' WHERE '
      ' PLANT_CODE = :PLANT_CODE AND'
      '  TEAM_CODE = :TEAM_CODE AND'
      '  DEPARTMENT_CODE = :DEPARTMENT_CODE')
    Left = 504
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableTeamUser: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'TEAMPERUSER'
    Left = 296
    Top = 136
  end
  object dspFilterTeamUser: TDataSetProvider
    DataSet = TableTeamUser
    Constraints = True
    Left = 392
    Top = 136
  end
  object cdsFilterTeamUser: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SHORT_NAME'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PLANTLU'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PLANT_CODE'
        DataType = ftString
        Size = 6
      end>
    IndexDefs = <
      item
        Name = 'byTeam'
        Fields = 'TEAM_CODE'
      end>
    IndexName = 'byTeam'
    Params = <>
    ProviderName = 'dspFilterTeamUser'
    StoreDefs = True
    Left = 504
    Top = 136
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE, T.TEAM_CODE, T.DESCRIPTION'
      'FROM'
      '  TEAM T'
      'WHERE'
      '  (:USER_NAME = '#39'*'#39')'
      '  OR'
      
        '  (T.TEAM_CODE IN (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE' +
        ' TU.USER_NAME = :USER_NAME))'
      'ORDER BY'
      '  T.TEAM_CODE'
      ' ')
    Left = 296
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object QueryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT'
      '  SHIFT_NUMBER, DESCRIPTION'
      'FROM'
      '  SHIFT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  SHIFT_NUMBER')
    Left = 168
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableShift: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;SHIFT_NUMBER'
    TableName = 'SHIFT'
    Left = 56
    Top = 400
    object TableShiftSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object TableShiftPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableShiftDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object TableDept: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = TableDeptFilterRecord
    IndexFieldNames = 'PLANT_CODE;DEPARTMENT_CODE'
    TableName = 'DEPARTMENT'
    Left = 56
    Top = 352
    object TableDeptDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object QueryDept: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT'
      '  DEPARTMENT_CODE, DESCRIPTION'
      'FROM'
      '  DEPARTMENT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY'
      '  DEPARTMENT_CODE'
      ' ')
    Left = 168
    Top = 352
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdateIlnessMessage: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ILLNESSMESSAGE IL SET'
      '  IL.ILLNESSMESSAGE_ENDDATE = :ENDDATE'
      'WHERE'
      '  (EMPLOYEE_NUMBER = :EMPLNO) AND'
      '  (IL.ILLNESSMESSAGE_ENDDATE IS NULL)'
      '')
    Left = 352
    Top = 400
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'endDate'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'emplNo'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdateShiftSchedule: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SHIFTSCHEDULE S'
      'WHERE'
      '  (S.SHIFT_SCHEDULE_DATE > :ENDDATE) AND'
      '  (S.EMPLOYEE_NUMBER = :EMPLNO)'
      '')
    Left = 488
    Top = 400
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'endDate'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'emplNo'
        ParamType = ptUnknown
      end>
  end
  object qryDeleteStandAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM STANDARDAVAILABILITY T'
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
      ' ')
    Left = 56
    Top = 456
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryDeleteEmpAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEAVAILABILITY T'
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'T.EMPLOYEEAVAILABILITY_DATE > :EMPLOYEEAVAILABILITY_DATE'
      ''
      ''
      ' ')
    Left = 168
    Top = 456
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryDeleteStandEmpPlan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM STANDARDEMPLOYEEPLANNING T'
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ''
      ' '
      ' ')
    Left = 280
    Top = 456
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryEndDateEmplContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEECONTRACT T'
      '   SET T.ENDDATE = :ENDDATE'
      ' WHERE T.EMPLOYEE_NUMBER = :EMPLOYEENUMBER AND '
      '       T.ENDDATE = :NODATE')
    Left = 304
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEENUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NODATE'
        ParamType = ptUnknown
      end>
  end
  object StoredProcUpdateEmpl: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'EMPLOYEE_UPDATECASCADE'
    Left = 344
    Top = 264
    ParamData = <
      item
        DataType = ftFloat
        Name = 'OLD_EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'NEW_EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SHORT_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DESCRIPTION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ZIPCODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CITY'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'STATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'LANGUAGE_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PHONE_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'EMAIL_ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATE_OF_BIRTH'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SEX'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SOCIAL_SECURITY_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'IS_SCANNING_YN'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CUT_OF_TIME_YN'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'REMARK'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CALC_BONUS_YN'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FIRSTAID_YN'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'FIRSTAID_EXP_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'GRADE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'SHIFT_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FREETEXT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'BOOK_PROD_HRS_YN'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SPECIAL_CHR'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETMP'
        ParamType = ptInput
      end>
  end
  object qryDeleteEmpPlan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEPLANNING T'
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'T.EMPLOYEEPLANNING_DATE > :EMPLOYEEPLANNING_DATE'
      ''
      ''
      ' '
      ' ')
    Left = 400
    Top = 456
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpContract: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEECONTRACT'
      '('
      '  EMPLOYEE_NUMBER,'
      '  STARTDATE,'
      '  CREATIONDATE,'
      '  ENDDATE,'
      '  HOURLY_WAGE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  CONTRACT_TYPE,'
      '  HOLIDAY_HOUR_PER_YEAR,'
      '  CONTRACT_HOUR_PER_WEEK,'
      '  CONTRACT_DAY_PER_WEEK,'
      '  GUARANTEED_DAYS,'
      '  EXPORT_CODE,'
      '  SENIORITY_HOURS,'
      '  WORKER_YN,'
      '  WORKSCHEDULE_ID'
      ')'
      'SELECT'
      '  :EMPLOYEE_NUMBER,'
      '  :STARTDATE,'
      '  SYSDATE,'
      '  TO_DATE('#39'31-12-2099'#39', '#39'dd-mm-yyyy'#39'),'
      '  HOURLY_WAGE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  CONTRACT_TYPE,'
      '  HOLIDAY_HOUR_PER_YEAR,'
      '  CONTRACT_HOUR_PER_WEEK,'
      '  CONTRACT_DAY_PER_WEEK,'
      '  GUARANTEED_DAYS,'
      '  EXPORT_CODE,'
      '  SENIORITY_HOURS,'
      '  WORKER_YN,'
      '  WORKSCHEDULE_ID'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE'
      '  EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  STARTDATE IN (SELECT MAX(STARTDATE) FROM EMPLOYEECONTRACT WHER' +
        'E EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER) AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEECONTRACT WHERE' +
        ' EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)')
    Left = 56
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyWSPerEmp: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WORKSPOTSPEREMPLOYEE'
      '('
      '  EMPLOYEE_NUMBER,'
      '  PLANT_CODE,'
      '  EMPLOYEE_LEVEL,'
      '  DEPARTMENT_CODE,'
      '  CREATIONDATE,'
      '  WORKSPOT_CODE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  STARTDATE_LEVEL'
      ')'
      'SELECT '
      '  :EMPLOYEE_NUMBER,'
      '  PLANT_CODE,'
      '  EMPLOYEE_LEVEL,'
      '  DEPARTMENT_CODE,'
      '  SYSDATE,'
      '  WORKSPOT_CODE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  :STARTDATE'
      'FROM '
      '  WORKSPOTSPEREMPLOYEE'
      'WHERE '
      '  EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM WORKSPOTSPEREMPLOYEE W' +
        'HERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)   ')
    Left = 160
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyStandardAvail: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO STANDARDAVAILABILITY'
      '('
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT '
      '  PLANT_CODE,'
      '  DAY_OF_WEEK,'
      '  SHIFT_NUMBER,'
      '  :EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM STANDARDAVAILABILITY'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM STANDARDAVAILABILITY WHE' +
        'RE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER)  '
      ' ')
    Left = 264
    Top = 512
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCopyShiftSchedule: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTSCHEDULE'
      '('
      '  EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT '
      '  :EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM SHIFTSCHEDULE'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'SHIFT_SCHEDULE_DATE >= :DATEFROM AND SHIFT_SCHEDULE_DATE <= :DAT' +
        'ETO AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM SHIFTSCHEDULE WHERE EMPL' +
        'OYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  SHIFT_SCHEDULE_DATE >= :DATEFROM AND SHIFT_SCHEDULE_DATE <= :D' +
        'ATETO)')
    Left = 376
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpAvail: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEAVAILABILITY'
      '('
      '  PLANT_CODE,'
      '  EMPLOYEEAVAILABILITY_DATE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR,'
      '  SAVE_AVAILABLE_TIMEBLOCK_1,'
      '  SAVE_AVAILABLE_TIMEBLOCK_2,'
      '  SAVE_AVAILABLE_TIMEBLOCK_3,'
      '  SAVE_AVAILABLE_TIMEBLOCK_4,'
      '  EXPORTED_YN'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  EMPLOYEEAVAILABILITY_DATE,'
      '  SHIFT_NUMBER,'
      '  :EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR,'
      '  SAVE_AVAILABLE_TIMEBLOCK_1,'
      '  SAVE_AVAILABLE_TIMEBLOCK_2,'
      '  SAVE_AVAILABLE_TIMEBLOCK_3,'
      '  SAVE_AVAILABLE_TIMEBLOCK_4,'
      '  '#39'N'#39
      'FROM EMPLOYEEAVAILABILITY'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        '  EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND EMPLOYEEAVAILABILIT' +
        'Y_DATE <= :DATETO AND'
      
        '  NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEEAVAILABILITY W' +
        'HERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '    EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND EMPLOYEEAVAILABIL' +
        'ITY_DATE <= :DATETO)'
      '    '
      '')
    Left = 472
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryCopyEmpPlan: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEPLANNING'
      '('
      '  PLANT_CODE,'
      '  EMPLOYEEPLANNING_DATE,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'SELECT'
      '  PLANT_CODE,'
      '  EMPLOYEEPLANNING_DATE,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  :EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      'FROM EMPLOYEEPLANNING'
      'WHERE EMPLOYEE_NUMBER = :OTHEREMPLOYEE_NUMBER AND'
      
        'EMPLOYEEPLANNING_DATE >= :DATEFROM AND EMPLOYEEPLANNING_DATE <= ' +
        ':DATETO AND'
      
        'NOT EXISTS (SELECT EMPLOYEE_NUMBER FROM EMPLOYEEPLANNING WHERE E' +
        'MPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  EMPLOYEEPLANNING_DATE >= :DATEFROM AND EMPLOYEEPLANNING_DATE <' +
        '= :DATETO)'
      ' ')
    Left = 560
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OTHEREMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryDeptPerTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlant
    SQL.Strings = (
      'SELECT'
      '   PLANT_CODE, TEAM_CODE, DEPARTMENT_CODE'
      'FROM'
      '   DEPARTMENTPERTEAM'
      'WHERE'
      '   PLANT_CODE = :PLANT_CODE AND'
      '   TEAM_CODE = :TEAM_CODE'
      'ORDER BY'
      '  DEPARTMENT_CODE')
    Left = 400
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
end
