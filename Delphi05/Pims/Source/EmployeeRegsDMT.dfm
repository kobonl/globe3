inherited EmployeeRegsDM: TEmployeeRegsDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 599
  Width = 742
  object qryFilterEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = qryFilterEmployeeFilterRecord
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.CUT_OF_TIME_YN,'
      '  E.PLANT_CODE,'
      '  E.PLANT_CODE FILTER_PLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.DEPARTMENT_CODE FILTER_DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.STARTDATE,'
      '  E.ENDDATE,'
      '  E.CONTRACTGROUP_CODE,'
      '  P.DESCRIPTION AS PLANTLU,'
      '  '#39'1'#39' AS EMP_PLANT_IN_TEAM'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'UNION'
      '  SELECT'
      '    E.EMPLOYEE_NUMBER,'
      '    E.SHORT_NAME,'
      '    E.DESCRIPTION,'
      '    E.CUT_OF_TIME_YN,'
      '    E.PLANT_CODE,'
      '    E.PLANT_CODE,'
      '    E.DEPARTMENT_CODE,'
      '    E.DEPARTMENT_CODE,'
      '    E.TEAM_CODE,'
      '    E.STARTDATE,'
      '    E.ENDDATE,'
      '    E.CONTRACTGROUP_CODE,'
      
        '    (SELECT P2.DESCRIPTION FROM PLANT P2 WHERE P2.PLANT_CODE = E' +
        '.PLANT_CODE),'
      '    '#39'1'#39
      '  FROM PRODHOURPEREMPLOYEE PE INNER JOIN EMPLOYEE E ON'
      '    PE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN PLANT P ON'
      '    PE.PLANT_CODE = P.PLANT_CODE'
      '  WHERE'
      '    P.PLANT_CODE IN'
      '      ('
      '      SELECT'
      '        E2.PLANT_CODE'
      '      FROM'
      '        EMPLOYEE E2'
      '      WHERE'
      '        ('
      '          (:USER_NAME = '#39'*'#39') OR'
      '          (E2.TEAM_CODE IN'
      
        '            (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_' +
        'NAME = :USER_NAME))'
      '       )'
      '    )'
      '    AND PE.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE'
      'ORDER BY '
      '  EMPLOYEE_NUMBER  '
      ''
      ''
      ' '
      ' '
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end>
    object qryFilterEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object qryFilterEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object qryFilterEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object qryFilterEmployeePLANTLU: TStringField
      FieldName = 'PLANTLU'
      Size = 30
    end
    object qryFilterEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryFilterEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object qryFilterEmployeeCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object qryFilterEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object qryFilterEmployeeENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object qryFilterEmployeeEMP_PLANT_IN_TEAM: TStringField
      FieldName = 'EMP_PLANT_IN_TEAM'
      FixedChar = True
      Size = 1
    end
    object qryFilterEmployeeFILTER_PLANT_CODE: TStringField
      FieldName = 'FILTER_PLANT_CODE'
      Size = 6
    end
    object qryFilterEmployeeFILTER_DEPARTMENT_CODE: TStringField
      FieldName = 'FILTER_DEPARTMENT_CODE'
      Size = 6
    end
    object qryFilterEmployeeCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Size = 6
    end
    object qryFilterEmployeeSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
  end
  object dspFilterEmployee: TDataSetProvider
    DataSet = qryFilterEmployee
    Constraints = True
    Left = 152
    Top = 8
  end
  object cdsFilterEmployee: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SHORT_NAME'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PLANTLU'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PLANT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DEPARTMENT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'CUT_OF_TIME_YN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TEAM_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'ENDDATE'
        DataType = ftDateTime
      end
      item
        Name = 'EMP_PLANT_IN_TEAM'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspFilterEmployee'
    StoreDefs = True
    OnFilterRecord = cdsFilterEmployeeFilterRecord
    Left = 256
    Top = 8
  end
  object DataSourceFilterEmployee: TDataSource
    DataSet = cdsFilterEmployee
    Left = 376
    Top = 8
  end
  object qryPlanning: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  W.DESCRIPTION AS WDESCRIPTION,'
      '  E.SHIFT_NUMBER,'
      '  E.WORKSPOT_CODE,'
      '  E.SCHEDULED_TIMEBLOCK_1,'
      '  E.SCHEDULED_TIMEBLOCK_2,'
      '  E.SCHEDULED_TIMEBLOCK_3,'
      '  E.SCHEDULED_TIMEBLOCK_4,'
      '  E.SCHEDULED_TIMEBLOCK_5,'
      '  E.SCHEDULED_TIMEBLOCK_6,'
      '  E.SCHEDULED_TIMEBLOCK_7,'
      '  E.SCHEDULED_TIMEBLOCK_8,'
      '  E.SCHEDULED_TIMEBLOCK_9,'
      '  E.SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING E, WORKSPOT W'
      'WHERE '
      '  E.PLANT_CODE = W.PLANT_CODE AND'
      '  E.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  E.EMPLOYEEPLANNING_DATE = :EMPLOYEEPLANNING_DATE AND'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 144
    Top = 384
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAvailable: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE '
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 48
    Top = 440
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_CODE, DESCRIPTION'
      'FROM'
      '  ABSENCEREASON'
      'ORDER BY'
      '  ABSENCEREASON_CODE')
    Left = 48
    Top = 296
  end
  object qryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION'
      'FROM'
      '  SHIFT'
      'ORDER BY'
      '  PLANT_CODE, SHIFT_NUMBER')
    Left = 48
    Top = 56
  end
  object qrySalaryHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.HOURTYPE_NUMBER,'
      '  H.DESCRIPTION AS HDESCRIPTION, '
      '  S.SALARY_MINUTE'
      'FROM '
      '  SALARYHOURPEREMPLOYEE S, '
      '  HOURTYPE H'
      'WHERE '
      '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  S.SALARY_DATE = :SALARY_DATE AND '
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  S.HOURTYPE_NUMBER')
    Left = 48
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  AE.ABSENCE_MINUTE, '
      '  A.ABSENCEREASON_CODE, '
      '  A.DESCRIPTION AS ADESCRIPTION'
      'FROM '
      '  ABSENCEHOURPEREMPLOYEE AE, '
      '  ABSENCEREASON A'
      'WHERE '
      '  AE.ABSENCEREASON_ID = A.ABSENCEREASON_ID AND'
      '  ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE AND '
      '  AE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 240
    Top = 384
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryScans: TQuery
    OnCalcFields = qryScansCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.DATETIME_IN,'
      '  T.DATETIME_OUT,'
      '  T.PLANT_CODE,'
      '  P.DESCRIPTION AS PDESCRIPTION,'
      '  T.WORKSPOT_CODE,'
      '  W.DESCRIPTION AS WDESCRIPTION,'
      '  T.JOB_CODE,'
      '  W.DEPARTMENT_CODE,'
      '  T.SHIFT_NUMBER,'
      '  (SELECT S.DESCRIPTION FROM SHIFT S'
      
        '   WHERE S.PLANT_CODE = T.PLANT_CODE AND S.SHIFT_NUMBER = T.SHIF' +
        'T_NUMBER)'
      '   AS SDESCRIPTION'
      'FROM'
      '  TIMEREGSCANNING T,'
      '  PLANT P, WORKSPOT W'
      'WHERE'
      '  T.PLANT_CODE = P.PLANT_CODE AND'
      '  T.PLANT_CODE = W.PLANT_CODE AND'
      '  T.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  ('
      '    (:ONSHIFT = 1 AND'
      '    T.SHIFT_DATE >= :DATEFROM AND'
      '    T.SHIFT_DATE < :DATETO)'
      '    OR'
      '    (:ONSHIFT = 0 AND'
      '    T.DATETIME_IN >= :DATEFROM AND'
      '    T.DATETIME_IN < :DATETO)'
      '  )'
      '  AND'
      '  T.DATETIME_OUT IS NOT NULL AND'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  T.DATETIME_IN'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 144
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
    object qryScansDATETIME_IN: TDateTimeField
      FieldName = 'DATETIME_IN'
    end
    object qryScansDATETIME_OUT: TDateTimeField
      FieldName = 'DATETIME_OUT'
    end
    object qryScansPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryScansPDESCRIPTION: TStringField
      FieldName = 'PDESCRIPTION'
      Size = 30
    end
    object qryScansWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object qryScansWDESCRIPTION: TStringField
      FieldName = 'WDESCRIPTION'
      Size = 30
    end
    object qryScansJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object qryScansSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object qryScansSDESCRIPTION: TStringField
      FieldName = 'SDESCRIPTION'
      Size = 30
    end
    object qryScansCALCJDESCRIPTION: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALCJDESCRIPTION'
      Calculated = True
    end
    object qryScansDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
  end
  object qryJob: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE,'
      '  DESCRIPTION'
      'FROM'
      '  JOBCODE'
      'ORDER BY'
      '  PLANT_CODE,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE'
      '  ')
    Left = 48
    Top = 104
  end
  object qryPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, DESCRIPTION'
      'FROM'
      '  PLANT'
      'ORDER BY'
      '  PLANT_CODE')
    Left = 48
    Top = 152
  end
  object qryDepartment: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, DEPARTMENT_CODE, DESCRIPTION'
      'FROM'
      '  DEPARTMENT'
      'ORDER BY'
      '  PLANT_CODE,  DEPARTMENT_CODE')
    Left = 48
    Top = 200
  end
  object qryTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  TEAM_CODE, DESCRIPTION'
      'FROM'
      '  TEAM'
      'ORDER BY'
      '  TEAM_CODE')
    Left = 48
    Top = 248
  end
  object dspShift: TDataSetProvider
    DataSet = qryShift
    Constraints = True
    Left = 152
    Top = 56
  end
  object cdsShift: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'PLANT_CODE;SHIFT_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspShift'
    StoreDefs = True
    Left = 256
    Top = 56
  end
  object dspJob: TDataSetProvider
    DataSet = qryJob
    Constraints = True
    Left = 152
    Top = 104
  end
  object cdsJob: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byJob'
        Fields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
      end>
    IndexName = 'byJob'
    Params = <>
    ProviderName = 'dspJob'
    StoreDefs = True
    Left = 256
    Top = 104
  end
  object dspPlant: TDataSetProvider
    DataSet = qryPlant
    Constraints = True
    Left = 152
    Top = 152
  end
  object cdsPlant: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byCode'
        Fields = 'PLANT_CODE'
      end>
    IndexName = 'byCode'
    Params = <>
    ProviderName = 'dspPlant'
    StoreDefs = True
    Left = 256
    Top = 152
  end
  object dspDepartment: TDataSetProvider
    DataSet = qryDepartment
    Constraints = True
    Left = 152
    Top = 200
  end
  object cdsDepartment: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byCode'
        Fields = 'PLANT_CODE;DEPARTMENT_CODE'
      end>
    IndexName = 'byCode'
    Params = <>
    ProviderName = 'dspDepartment'
    StoreDefs = True
    Left = 256
    Top = 200
  end
  object dspTeam: TDataSetProvider
    DataSet = qryTeam
    Constraints = True
    Left = 152
    Top = 248
  end
  object cdsTeam: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'TEAM_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 30
      end>
    IndexDefs = <
      item
        Name = 'byCode'
        Fields = 'TEAM_CODE'
      end>
    IndexName = 'byCode'
    Params = <>
    ProviderName = 'dspTeam'
    StoreDefs = True
    Left = 256
    Top = 248
  end
  object dspAbsenceReason: TDataSetProvider
    DataSet = qryAbsenceReason
    Constraints = True
    Left = 152
    Top = 296
  end
  object cdsAbsenceReason: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byCode'
        Fields = 'ABSENCEREASON_CODE'
      end>
    IndexName = 'byCode'
    Params = <>
    ProviderName = 'dspAbsenceReason'
    StoreDefs = True
    Left = 256
    Top = 296
  end
  object qryEmployeeContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT EC.CONTRACT_HOUR_PER_WEEK'
      'FROM'
      '  EMPLOYEECONTRACT EC'
      'WHERE'
      '  (EC.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND'
      '  (STARTDATE <= :START_DATE) AND '
      '  (:END_DATE <= ENDDATE) ')
    Left = 240
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'END_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryProdHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  W.DESCRIPTION || DECODE(NVL(J.DESCRIPTION, '#39'*'#39'), '#39'*'#39', '#39#39', '#39'/'#39')' +
        ' || NVL(J.DESCRIPTION, '#39' '#39') DESCR,'
      '  P.PRODUCTION_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLOYEE P'
      'INNER JOIN WORKSPOT W ON '
      '  P.PLANT_CODE = W.PLANT_CODE AND'
      '  P.WORKSPOT_CODE = W.WORKSPOT_CODE'
      'LEFT JOIN JOBCODE J ON'
      '  P.PLANT_CODE = J.PLANT_CODE AND'
      '  P.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  P.JOB_CODE = J.JOB_CODE'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE = :PROD_DATE AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  DESCR')
    Left = 344
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PROD_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryFilterEmployeeOLD: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.CUT_OF_TIME_YN,'
      '  E.PLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.ENDDATE,'
      '  P.DESCRIPTION AS PLANTLU'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      '  '
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 464
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object StringField1: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object StringField3: TStringField
      FieldName = 'PLANTLU'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object StringField5: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object StringField6: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object StringField7: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'ENDDATE'
    end
  end
  object qryShiftSchedule: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.PLANT_CODE, S.SHIFT_NUMBER'
      'FROM'
      '  SHIFTSCHEDULE S'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE '
      ''
      ' ')
    Left = 344
    Top = 440
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
end
