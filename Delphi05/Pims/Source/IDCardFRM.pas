(*
  MRA:3-DEC-2009 RV047.2.
  - Filter employees on user by team-per-user.
  MRA:8-NOV-2010 RV077.4.
  - Prevent editing record without first setting to edit-mode.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDBPicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)

unit IDCardFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib,
  StdCtrls, Mask, dxDBTLCl, dxGrClms, ComCtrls, DBPicker, dxDBEdtr,
  dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TIDCardF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label11: TLabel;
    DBEditCardNumber: TDBEdit;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridLookupColumn;
    dxDetailGridColumn4: TdxDBGridDateColumn;
    DBPicker1: TDBPicker;
    dxDBDateEdit1: TdxDBDateEdit;
    dxDetailGridColumn5: TdxDBGridDateColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    dxDBLUEmployee: TdxDBExtLookupEdit;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1ItemEmployee: TdxDBGridLayout;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure GroupBox1Enter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBLUEmployeeEnter(Sender: TObject);
    procedure dxDBLUEmployeeExit(Sender: TObject);
    procedure dxDBLUEmployeeKeyPress(Sender: TObject; var Key: Char);
    procedure dxDBLUEmployeePopup(Sender: TObject; const EditText: String);
    procedure dxBarButtonEditModeClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
  private
    { Private declarations }
    procedure EnableEditFields(AEnable: Boolean);
  public
    { Public declarations }
  end;

function IDCardF: TIDCardF;

implementation

{$R *.DFM}
uses
  IDCardDMT;

var
  IDCardF_HND: TIDCardF;

function IDCardF: TIDCardF;
begin
  if (IDCardF_HND = nil) then
    IDCardF_HND := TIDCardF.Create(Application);
  Result := IDCardF_HND;
end;

procedure TIDCardF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  EnableEditFields(True);
  inherited;
  DBEditCardNumber.SetFocus;
end;

procedure TIDCardF.GroupBox1Enter(Sender: TObject);
begin
  inherited;
  DBEditCardNumber.SetFocus;
end;

procedure TIDCardF.FormCreate(Sender: TObject);
begin
  IDCardDM := CreateFormDM(TIDCardDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := IDCardDM.DataSourceDetail;
  EnableEditFields(False);
  inherited;
end;

procedure TIDCardF.FormDestroy(Sender: TObject);
begin
  inherited;
  IDCardF_HND := Nil;
end;

procedure TIDCardF.dxBarButtonExportGridClick(Sender: TObject);
begin
//CAR 27.02.2003
//  FloatEmpl := 'EMPLOYEENRLU';
  // MR:12-07-2004: Order 550329
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;
end;

procedure TIDCardF.FormShow(Sender: TObject);
begin
  inherited;
 //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      IDCardDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
  begin
    IDCardDM.QueryIdCardEmpl.Close;
    IDCardDM.QueryIdCardEmpl.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      SystemDM.ASaveTimeRecScanning.Employee;
    IDCardDM.QueryIdCardEmpl.Open;
    if (IDCardDM.QueryIdCardEmpl.FieldByName('IDCARD_NUMBER').AsString <> '') then
      IDCardDM.TableDetail.
        FindKey([IDCardDM.QueryIdCardEmpl.FieldByName('IDCARD_NUMBER').AsString]);
  end;
end;

procedure TIDCardF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
//CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    IDCardDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TIDCardF.dxDBLUEmployeeEnter(Sender: TObject);
begin
  inherited;
  // MR:12-07-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBLUEmployee.DroppedDown := True;
end;

procedure TIDCardF.dxDBLUEmployeeExit(Sender: TObject);
begin
  inherited;
  // MR:12-07-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure TIDCardF.dxDBLUEmployeeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBLUEmployee.DroppedDown := True;
end;

procedure TIDCardF.dxDBLUEmployeePopup(Sender: TObject;
  const EditText: String);
begin
  inherited;
  with IDCardDM do
  begin
    // RV047.2.
//    TableEmployee.FindKey([
//      TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger]);
    QueryEmployee.Locate('EMPLOYEE_NUMBER',
      TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger, []);
  end;
end;

// RV077.4.
procedure TIDCardF.dxBarButtonEditModeClick(Sender: TObject);
begin
  inherited;
  if dxBarButtonEditMode.Down then
    IDCardDM.DataSourceDetail.AutoEdit := True
  else
    IDCardDM.DataSourceDetail.AutoEdit := False;
  EnableEditFields(IDCardDM.DataSourceDetail.AutoEdit);
end;

// RV077.4.
procedure TIDCardF.EnableEditFields(AEnable: Boolean);
begin
  DBEditCardNumber.Enabled := AEnable;
  dxDBLUEmployee.Enabled := AEnable;
  DBPicker1.Enabled := AEnable;
  dxDBDateEdit1.Enabled := AEnable;
end;

// RV077.4.
procedure TIDCardF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  try
    (ActiveGrid as TdxDBGrid).DataSource.DataSet.Post;
  finally
    EnableEditFields(False);
  end;
end;

// RV077.4.
procedure TIDCardF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  try
    (ActiveGrid as TdxDBGrid).DataSource.DataSet.Cancel;
  finally
    EnableEditFields(False);
  end;
end;

end.
