(*
  MRA:19-APR-2016 ABS-27052
  - Default printer handling
  - It is possible the found default printer is from a previous session
    when using citrix. To solve that, check if the found default printer
    is available in the found printers. If not the ask the user for a printer,
    or when there is only 1 printer then just make that the default one.
  MRA:31-MAY-2016 ABS-27052 Rework
  - Default printer handling
  - Do not do a second check when the default printer was already found.
*)
unit DialogSelectPrinterFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Printers, SystemDMT;

type
  TDialogSelectPrinterF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    ListBox1: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FDefaultPrinterFound: Boolean;
//    function CheckDefaultPrinter(ADefaultPrinter: String): Boolean;
    function SearchPrinters: Boolean;
  public
    { Public declarations }
    property DefaultPrinterFound: Boolean read FDefaultPrinterFound write FDefaultPrinterFound;
  end;

var
  DialogSelectPrinterF: TDialogSelectPrinterF;

implementation

uses
  UPimsMessageRes;

{$R *.DFM}

function GetDefaultPrinter1: string;
var
  ResStr: array[0..255] of Char;
  IPos: Integer;
begin
  Result := '';
  try
    GetProfileString('Windows', 'device', '', ResStr, 255);
    Result := StrPas(ResStr);
    if Result <> '' then
    begin
      IPos := Pos(',', Result);
      if IPos > 0 then
        Result := Copy(Result, 1, IPos-1);
    end;
  except
    Result := '';
  end;
end;

{
function GetDefaultPrinter2: String;
begin
  Result := '';
  if Printer.Printers.Count > 0 then
  begin
    Printer.PrinterIndex := -1;
    try
      Result := Printer.Printers.Strings[Printer.PrinterIndex];
    except
      Result := '';
    end;
  end;
end;
}

procedure SetDefaultPrinter1(NewDefPrinter: string);
var
  ResStr: array[0..255] of Char;
begin
  Screen.Cursor := crHourglass;
  try
    StrPCopy(ResStr, NewdefPrinter);
    WriteProfileString('windows', 'device', ResStr);
    StrCopy(ResStr, 'windows');
    SendMessage(HWND_BROADCAST, WM_WININICHANGE, 0, Longint(@ResStr));
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
function TDialogSelectPrinterF.CheckDefaultPrinter(
  ADefaultPrinter: String): Boolean;
var
  MyPrinter: TPrinter;
  i: Integer;
begin
  Result := False;
  MyPrinter := TPrinter.Create;
  try
    for i := 0 to MyPrinter.Printers.Count - 1 do
    begin
      if LowerCase(ADefaultPrinter) = LowerCase(MyPrinter.Printers[i]) then
      begin
        Result := True;
        Break;
      end;
    end;
  finally
    MyPrinter.Free;
  end;
end; // CheckDefaultPrinter
}

function TDialogSelectPrinterF.SearchPrinters: Boolean;
var
  MyPrinter: TPrinter;
begin
  Result := False;
  MyPrinter := TPrinter.Create;
  try
    if MyPrinter.Printers.Count > 0 then
      Result := True;
  finally
    MyPrinter.Free;
  end;
end; // SearchPrinters

procedure TDialogSelectPrinterF.FormCreate(Sender: TObject);
var
  MyPrinter: TPrinter;
  i: Integer;
  MyDefaultPrinter: String;
begin
  inherited;
  Caption := SPimsSelectPrinter;
  GroupBox1.Caption := SPimsSelectDefaultPrinter;
  SearchPrinters;
  MyDefaultPrinter := GetDefaultPrinter1;
  DefaultPrinterFound := MyDefaultPrinter <> '';
//  if DefaultPrinterFound then
//    DefaultPrinterFound := CheckDefaultPrinter(MyDefaultPrinter);
//  DefaultPrinterFound := False;
  if not DefaultPrinterFound then
  begin
    MyPrinter := TPrinter.Create;
    try
      ListBox1.Items.Clear;
      if MyPrinter.Printers.Count > 0 then
      begin
        for i := 0 to MyPrinter.Printers.Count-1 do
          ListBox1.Items.Add(MyPrinter.Printers[i]);
        ListBox1.ItemIndex := 0;
      end;
      if not DefaultPrinterFound then
        if MyPrinter.Printers.Count = 1 then
        begin
          SetDefaultPrinter1(ListBox1.Items.Strings[0]);
          DefaultPrinterFound := True;
        end;
    finally
      MyPrinter.Free;
    end;
  end;
end;

procedure TDialogSelectPrinterF.FormShow(Sender: TObject);
begin
  inherited;
  if ListBox1.Items.Count = 0 then
  begin
    DisplayMessage(SPimsNoPrinterFound, mtError, [mbOK]);
    Close;
  end;
end;

procedure TDialogSelectPrinterF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ListBox1.ItemIndex <> -1 then
  begin
    SetDefaultPrinter1(ListBox1.Items.Strings[ListBox1.ItemIndex]);
    ModalResult := mrOK;
  end
  else
  begin
    DisplayMessage(SPimsPleaseSelectPrinter, mtInformation, [mbOK]);
    ModalResult := mrNone;
  end;
end;

procedure TDialogSelectPrinterF.btnCancelClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TDialogSelectPrinterF.CreateParams(var params: TCreateParams);
begin
  inherited;
  try
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  except
  end;
end;

end.
