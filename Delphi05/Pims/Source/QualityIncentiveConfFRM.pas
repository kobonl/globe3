unit QualityIncentiveConfFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, DBTables;

type
  TQualityIncentiveConfF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    GroupBox1: TGroupBox;
    LabelNrMistake: TLabel;
    LabelBonus: TLabel;
    DBEditMistake: TDBEdit;
    DBEditBonus: TDBEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function QualityIncentiveConfF: TQualityIncentiveConfF;

implementation

{$R *.DFM}

uses
  SystemDMT, QualityIncentiveConfDMT, UPimsMessageRes;

var
  QualityIncentiveConfF_HND: TQualityIncentiveConfF;

function QualityIncentiveConfF: TQualityIncentiveConfF;
begin
  if (QualityIncentiveConfF_HND = nil) then
    QualityIncentiveConfF_HND := TQualityIncentiveConfF.Create(Application);
  Result := QualityIncentiveConfF_HND;
end;

procedure TQualityIncentiveConfF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditMistake.SetFocus;
end;

procedure TQualityIncentiveConfF.FormDestroy(Sender: TObject);
begin
  inherited;
  QualityIncentiveConfF_HND := nil;
end;

procedure TQualityIncentiveConfF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  if DBEditMistake.Visible then
    DBEditMistake.SetFocus;
end;

procedure TQualityIncentiveConfF.FormCreate(Sender: TObject);
begin
  QualityIncentiveConfDM := CreateFormDM(TQualityIncentiveConfDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := QualityIncentiveConfDM.DataSourceMaster;
  inherited;
end;

procedure TQualityIncentiveConfF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TQualityIncentiveConfF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  if (QualityIncentiveConfDM.TableMaster.FieldByName('MISTAKE').AsInteger =
    999999) then
  begin
    DBEditMistake.Visible := False;
    LabelNrMistake.Visible := False;
    dxBarBDBNavDelete.Enabled := False;
    LabelBonus.Visible := False;
    DBEditBonus.Visible := False;
  end
  else
  begin
    dxBarBDBNavDelete.Enabled := True;
    DBEditMistake.Visible := True;
    LabelNrMistake.Visible := True;
    LabelBonus.Visible := True;
    DBEditBonus.Visible := True;
  end;
end;

procedure TQualityIncentiveConfF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  inherited;
  if (QualityIncentiveConfDM.TableMaster.FieldByName('MISTAKE').AsInteger =
    999999) then
  begin
    DisplayMessage(SNotDeleteRecord, mtInformation, [mbOk]);
  end
  else
    QualityIncentiveConfDM.TableMaster.Delete;
end;

end.
