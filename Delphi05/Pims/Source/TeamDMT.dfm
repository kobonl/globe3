inherited TeamDM: TTeamDM
  OldCreateOrder = True
  Left = 267
  Top = 179
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    OnFilterRecord = TableMasterFilterRecord
    TableName = 'PLANT'
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
  end
  inherited TableDetail: TTable
    OnNewRecord = TableDetailNewRecord
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    TableName = 'TEAM'
    Top = 132
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailRESP_EMPLOYEE_NUMBER: TIntegerField
      FieldName = 'RESP_EMPLOYEE_NUMBER'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupDataSet = TableEmployee
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'RESP_EMPLOYEE_NUMBER'
      LookupCache = True
      Lookup = True
    end
  end
  object TableEmployee: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'EMPLOYEE'
    Left = 92
    Top = 200
    object TableEmployeeEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableEmployeeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableEmployeeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableEmployeeADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableEmployeeZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableEmployeeCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableEmployeeSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableEmployeeLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableEmployeePHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableEmployeeEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableEmployeeDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableEmployeeSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableEmployeeSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableEmployeeENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableEmployeeCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableEmployeeCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableEmployeeREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableEmployeeCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
  end
end
