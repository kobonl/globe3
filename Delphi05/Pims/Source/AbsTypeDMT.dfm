inherited AbsTypeDM: TAbsTypeDM
  OldCreateOrder = True
  inherited TableMaster: TTable
    Top = 56
  end
  inherited TableDetail: TTable
    TableName = 'ABSENCETYPE'
    object TableDetailABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 1
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
end
