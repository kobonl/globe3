(*
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:16-OCT-2017 PIM-311
  - Bugfix for target-year.
*)
unit DialogProcessWorkScheduleFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, DialogProcessWorkScheduleDMT, Spin, Db, DBTables,
  WorkScheduleProcessDMT, SystemDMT, jpeg;

type
  TDialogProcessWorkScheduleF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    lblYear: TLabel;
    Label2: TLabel;
    lblFrom: TLabel;
    SpinEditFromWeek: TSpinEdit;
    Label1: TLabel;
    Label3: TLabel;
    SpinEditToWeek: TSpinEdit;
    GroupBox2: TGroupBox;
    Memo1: TMemo;
    RadioGrpPlanningMode: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FYear: Integer;
    FFromWeek: Integer;
    FToWeek: Integer;
    FqryEmployee: TQuery;
    FMyEmployee: Integer;
  public
    { Public declarations }
    property Year: Integer read FYear write FYear;
    property FromWeek: Integer read FFromWeek write FFromWeek;
    property ToWeek: Integer read FToWeek write FToWeek;
    property qryEmployee: TQuery read FqryEmployee write FqryEmployee;
    property MyEmployee: Integer read FMyEmployee write FMyEmployee;
  end;

var
  DialogProcessWorkScheduleF: TDialogProcessWorkScheduleF;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UPimsMessageRes;

procedure TDialogProcessWorkScheduleF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogProcessWorkScheduleDM := TDialogProcessWorkScheduleDM.Create(nil);
  WorkScheduleProcessDM := TWorkScheduleProcessDM.Create(nil);
  MyEmployee := -1;
end;

procedure TDialogProcessWorkScheduleF.FormShow(Sender: TObject);
var
  MaxWeek: Integer;
begin
  inherited;
  Label2.Caption := IntToStr(Year);
  MaxWeek := ListProcsF.WeeksInYear(Year);
  SpinEditFromWeek.MaxValue := MaxWeek;
  SpinEditFromWeek.MinValue := 1;
  SpinEditFromWeek.Value := FromWeek;
  SpinEditToWeek.MaxValue := MaxWeek;
  SpinEditToWeek.MinValue := 1;
  SpinEditToWeek.Value := ToWeek;
  Memo1.Lines.Clear;
  WorkScheduleProcessDM.Memo1 := Memo1;
  WorkScheduleProcessDM.qryEmployee := qryEmployee;
end;

procedure TDialogProcessWorkScheduleF.FormDestroy(Sender: TObject);
begin
  inherited;
  DialogProcessWorkScheduleDM.Free;
  WorkScheduleProcessDM.Free;
end;

procedure TDialogProcessWorkScheduleF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;
  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TDialogProcessWorkScheduleF.btnOkClick(Sender: TObject);
begin
  inherited;
  try
    with WorkScheduleProcessDM do
    begin
      try
        WorkScheduleProcessDM.Year := Self.Year; // PIM-311 Add Self before it, or it will take the wrong property!
        WorkScheduleProcessDM.FromWeek := SpinEditFromWeek.Value;
        WorkScheduleProcessDM.ToWeek := SpinEditToWeek.Value;
        WorkScheduleProcessDM.PlanningMode := RadioGrpPlanningMode.ItemIndex;
        if MyEmployee <> -1 then
          ProcessWorkScheduleEmployee(MyEmployee)
        else
          ProcessWorkSchedule;
      except
      end;
    end;
  finally
    Memo1.Lines.Add(SPimsReady);
    DisplayMessage(SPimsReady, mtInformation, [mbOK]);
  end;
end;

end.
