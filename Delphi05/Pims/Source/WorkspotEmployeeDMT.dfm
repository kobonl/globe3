inherited WorkspotEmployeeDM: TWorkspotEmployeeDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnCalcFields = TableMasterCalcFields
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    TableName = 'EMPLOYEE'
    Top = 16
    object TableMasterEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableMasterSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableMasterDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableMasterLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableMasterPHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableMasterEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableMasterDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableMasterSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableMasterSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableMasterTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableMasterENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableMasterIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableMasterCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableMasterREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableMasterCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableMasterDEPARTMENTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPARTMENTLU'
      LookupDataSet = TableDepartment
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableMasterEMP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    DatabaseName = 'Pims'
    IndexFieldNames = 'EMPLOYEE_NUMBER;PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE'
    MasterFields = 'EMPLOYEE_NUMBER'
    TableName = 'WORKSPOTSPEREMPLOYEE'
    Top = 68
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailSTARTDATE_LEVEL: TDateTimeField
      FieldName = 'STARTDATE_LEVEL'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailWORKSPOTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOTLU'
      LookupDataSet = TableWorkspot
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableDetailEMPLOYEE_LEVEL: TStringField
      FieldName = 'EMPLOYEE_LEVEL'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 1
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDEPTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPTLU'
      LookupDataSet = TableDeptLU
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailWKDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'WKDESC'
      Size = 30
      Calculated = True
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailDEPTDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'DEPTDESC'
      Size = 30
      Calculated = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 208
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Top = 68
  end
  inherited TableExport: TTable
    Left = 188
  end
  inherited DataSourceExport: TDataSource
    Left = 288
    Top = 316
  end
  object TablePlant: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 92
    Top = 124
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableWorkspot: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = TableWorkspotFilterRecord
    IndexFieldNames = 'PLANT_CODE;DEPARTMENT_CODE'
    MasterFields = 'PLANT_CODE;DEPARTMENT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'WORKSPOT'
    Left = 96
    Top = 184
    object TableWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 216
    Top = 128
  end
  object DataSourceWorkSpot: TDataSource
    DataSet = TableWorkspot
    Left = 212
    Top = 188
  end
  object TableDept: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;DEPARTMENT_CODE'
    TableName = 'DEPARTMENT'
    Left = 96
    Top = 304
    object TableDeptDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDeptBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDeptCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDeptDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDeptMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDeptMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDeptPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDeptREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
  object DataSourceDept: TDataSource
    DataSet = TableDept
    Left = 216
    Top = 240
  end
  object Tablewk: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 360
    Top = 240
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField4: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField5: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField6: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField7: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object StringField8: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object StringField9: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object StringField11: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object StringField12: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TablewkAUTOMATIC_RESCAN_YN: TStringField
      FieldName = 'AUTOMATIC_RESCAN_YN'
      Size = 1
    end
    object TablewkSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
  end
  object TableDepartment: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceMaster
    TableName = 'DEPARTMENT'
    Left = 312
    Top = 16
    object TableDepartmentDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDepartmentBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDepartmentCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDepartmentDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDepartmentMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDepartmentMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDepartmentPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDepartmentREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
  object TableDeptLU: TTable
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'DEPARTMENT'
    Left = 96
    Top = 240
    object StringField13: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object StringField14: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField15: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField16: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField17: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField18: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField19: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object StringField20: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
end
