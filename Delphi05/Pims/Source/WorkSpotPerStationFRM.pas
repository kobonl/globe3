(*
  Changes:
  MRA:27-JAN-2011 RV085.13. Small change.
  - Enlarge the drop-down-components for plants
    and workspots. They should show more lines.
  MRA:18-APR-2011 RV089.5.
  - Small changed: Added confirmation.
*)
unit WorkSpotPerStationFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, DBCtrls, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, StdCtrls, dxExGrEd, dxExELib, dxDBEdtr, dxLayout;

type
  TWorkSpotPerStationF = class(TGridBaseF)
    dxMasterGridColumnComputer: TdxDBGridColumn;
    dxMasterGridColumnLicense: TdxDBGridColumn;
    dxDetailGridColumnSequence: TdxDBGridSpinColumn;
    LabelSequence: TLabel;
    dxDBSpinEditSequence: TdxDBSpinEdit;
    Label1: TLabel;
    LabelWorkspot: TLabel;
    dxDetailGridColumnWorkspotDescr: TdxDBGridColumn;
    dxDetailGridColumnPlant: TdxDBGridLookupColumn;
    dxDetailGridColumnWorkspotCode: TdxDBGridColumn;
    dxDBLookupEditPlant: TdxDBLookupEdit;
    ButtonCopy: TButton;
    ButtonAdd: TButton;
    dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit;
    dxDBGridLayoutListWorkspot: TdxDBGridLayoutList;
    dxDBGridLayoutListWorkspotItem: TdxDBGridLayout;
    lblWorkspotDescription: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxDBLookupEditPlantSelectionChange(Sender: TObject);
    procedure ButtonCopyClick(Sender: TObject);
    procedure ButtonAddClick(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotEnter(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotExit(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotPopup(Sender: TObject;
      const EditText: String);
    procedure FormShow(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotKeyPress(Sender: TObject;
      var Key: Char);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilterWK(Plant: String);
 
  end;

  function WorkSpotPerStationF: TWorkSpotPerStationF;

implementation
{$R *.DFM}
uses
  WorkSpotPerStationDMT,
  UPimsConst, SYSTEMDMT,
  DialogAddWKWorkstationFRM, UPimsMessageRes;

var
  WorkSpotPerStationF_HND: TWorkSpotPerStationF;

function WorkSpotPerStationF: TWorkSpotPerStationF;
begin
  if (WorkSpotPerStationF_HND = nil) then
  begin
    WorkSpotPerStationF_HND := TWorkSpotPerStationF.Create(Application);
  end;
  Result := WorkSpotPerStationF_HND;
end;

procedure TWorkSpotPerStationF.FormCreate(Sender: TObject);
begin
  WorkSpotPerStationDM := CreateFormDM(TWorkSpotPerStationDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := WorkSpotPerStationDM.DataSourceDetail;
    dxMasterGrid.DataSource := WorkSpotPerStationDM.DataSourceMaster;
  end;
  lblWorkspotDescription.Caption := '';
  inherited;
end;

procedure TWorkSpotPerStationF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  dxDBSpinEditSequence.SetFocus;
end;

procedure TWorkSpotPerStationF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  if (dxDBExtLookupEditWorkspot.Text = '') then
  begin
    dxDBExtLookupEditWorkspot.Text :=
      WorkSpotPerStationDM.TableDetail.FieldByName('WORKSPOTCAL').AsString;
  end;
end;

procedure TWorkSpotPerStationF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  if (dxDBExtLookupEditWorkspot.Text = '') then
  begin
    dxDBExtLookupEditWorkspot.Text :=
      WorkSpotPerStationDM.TableDetail.FieldByName('WORKSPOTCAL').AsString;
  end;
end;

procedure TWorkSpotPerStationF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkSpotPerStationF_HND := nil
end;

procedure TWorkSpotPerStationF.SetFilterWK(Plant: String);
var
  StrFilter: String;
begin
  WorkSpotPerStationDM.TableTmpPlant.FindKey([Plant]);
  Plant := WorkSpotPerStationDM.TableTmpPlant.FieldByName('PLANT_CODE').AsString;
  WorkSpotPerStationDM.TableWorkspot.Filtered := True;
  StrFilter := '';
  if Plant <> '' then
    StrFilter := 'PLANT_CODE = ''' + DoubleQuote(Plant) + ''' AND ' ;
  WorkSpotPerStationDM.TableWorkspot.Filter := StrFilter +
     'WORKSPOT_CODE <> ''' + DummyStr + '''';
end;

procedure TWorkSpotPerStationF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  WorkSpotPerStationDM.TableDetail.Cancel;
  if (dxDBExtLookupEditWorkspot.Text = '') then
  begin
    dxDBExtLookupEditWorkspot.Text :=
      WorkSpotPerStationDM.TableDetail.FieldByName('WORKSPOTCAL').AsString;
  end;
end;

procedure TWorkSpotPerStationF.dxDBLookupEditPlantSelectionChange(
  Sender: TObject);
begin
  inherited;
  dxDBExtLookupEditWorkspot.Text := '';
end;

procedure TWorkSpotPerStationF.ButtonCopyClick(Sender: TObject);
begin
  inherited;
  try
    DialogAddWKWorkstationF := TDialogAddWKWorkstationF.Create(Self);
    DialogAddWKWorkstationF.LabelName.Caption := WorkSpotPerStationDM.TableMaster.
      FieldByName('COMPUTER_NAME').asString;
    if DialogAddWKWorkstationF.ShowModal = mrOk then
    begin
      WorkSpotPerStationDM.StoredProcWork.Active := False;
      WorkSpotPerStationDM.StoredProcWork.StoredProcName := 'WKPERWS_COPYFROM';
      WorkSpotPerStationDM.StoredProcWork.Prepare;
      WorkSpotPerStationDM.StoredProcWork.ParamByName('WSFROM').AsString :=
        WorkSpotPerStationDM.TableWorkStation.FieldByName('COMPUTER_NAME').
          AsString;
      WorkSpotPerStationDM.StoredProcWork.ParamByName('WSTO').AsString :=
        WorkSpotPerStationDM.TableMaster.FieldByName('COMPUTER_NAME').
          AsString;
      WorkSpotPerStationDM.StoredProcWork.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      WorkSpotPerStationDM.StoredProcWork.ParamByName('CURRENTDATE').
        AsDateTime := Now();
      WorkSpotPerStationDM.StoredProcWork.ExecProc;
      WorkSpotPerStationDM.TableDetail.Active := False;
  	   WorkSpotPerStationDM.TableDetail.Active := True;
    end;
  finally
    DialogAddWKWorkstationF.Free;
  end;
  WorkSpotPerStationDM.TableDetail.Active := False;
  WorkSpotPerStationDM.TableDetail.Active := True;
end;

procedure TWorkSpotPerStationF.ButtonAddClick(Sender: TObject);
begin
  inherited;
  // RV089.5. Ask for confirmation first.
  if DisplayMessage(SPimsAddAllWorkspots, mtConfirmation, [mbYes, mbNo]) =
    mrYes then
  begin
    WorkSpotPerStationDM.StoredProcWork.Active := False;
    WorkSpotPerStationDM.StoredProcWork.StoredProcName := 'WKPERWS_ADDALLWK';
    WorkSpotPerStationDM.StoredProcWork.Prepare;
    WorkSpotPerStationDM.StoredProcWork.ParamByName('WSTO').AsString :=
      WorkSpotPerStationDM.TableMaster.FieldByName('COMPUTER_NAME').
        AsString;
    WorkSpotPerStationDM.StoredProcWork.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    WorkSpotPerStationDM.StoredProcWork.ParamByName('CURRENTDATE').AsDateTime :=
    	 Now();
    WorkSpotPerStationDM.StoredProcWork.ExecProc;
    WorkSpotPerStationDM.TableDetail.Active := False;
    WorkSpotPerStationDM.TableDetail.Active := True;
  end;
end;

procedure TWorkSpotPerStationF.dxDBExtLookupEditWorkspotEnter(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure TWorkSpotPerStationF.dxDBExtLookupEditWorkspotExit(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure TWorkSpotPerStationF.dxDBExtLookupEditWorkspotPopup(
  Sender: TObject; const EditText: String);
begin
  inherited;
  with WorkspotPerStationDM do
  begin
    if not TableWorkspot.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('WORKSPOT_CODE').AsString]) then
      TableWorkspot.Locate('PLANT_CODE',
        TableDetail.FieldByName('PLANT_CODE').AsString, []);
  end;
end;

procedure TWorkSpotPerStationF.dxDBExtLookupEditWorkspotKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure TWorkSpotPerStationF.FormShow(Sender: TObject);
begin
  inherited;
  if pnlDetail.Visible then
  begin
    pnlDetail.Visible := False;
    pnlDetail.Visible := True;
  end;
end;

procedure TWorkSpotPerStationF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if ASelected then
    if AColumn = dxDetailGridColumnWorkspotDescr then
      lblWorkspotDescription.Caption := AText;
end;

end.
