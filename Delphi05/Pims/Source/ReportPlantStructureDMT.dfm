inherited ReportPlantStructureDM: TReportPlantStructureDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryPlantStructure: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantStructureFilterRecord
    Left = 72
    Top = 24
  end
  object ClientDataSetDataCol: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderDataCol'
    Left = 72
    Top = 232
  end
  object DataSetProviderDataCol: TDataSetProvider
    DataSet = QueryDataCol
    Constraints = True
    Left = 72
    Top = 152
  end
  object QueryDataCol: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COMPUTER_NAME,'
      '  COMPORT,  '
      '  ADDRESS, '
      '  COUNTER,'
      '  INTERFACE_CODE'#9
      'FROM '
      '  DATACOLCONNECTION'
      'ORDER BY'
      '  INTERFACE_CODE')
    Left = 72
    Top = 88
  end
  object QueryCompareJob: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.PLANT_CODE,'
      '  C.WORKSPOT_CODE,'
      '  C.JOB_CODE,'
      '  C.COMPARE_WORKSPOT_CODE,'
      '  W.DESCRIPTION AS WDESC,'
      '  C.COMPARE_JOB_CODE,'
      '  J.DESCRIPTION AS JDESC'
      'FROM '
      '  COMPAREJOB C,'
      '  WORKSPOT W,'
      '  JOBCODE J'
      'WHERE'
      '  W.PLANT_CODE = C.PLANT_CODE AND'
      '  W.WORKSPOT_CODE = C.COMPARE_WORKSPOT_CODE AND'
      '  J.PLANT_CODE = C.PLANT_CODE AND'
      '  J.WORKSPOT_CODE =  C.COMPARE_WORKSPOT_CODE AND'
      '  J.JOB_CODE = C.COMPARE_JOB_CODE'
      'ORDER BY'
      '  C.PLANT_CODE,'
      '  C.WORKSPOT_CODE,'
      '  C.JOB_CODE')
    Left = 240
    Top = 88
  end
  object DataSetProviderCompareJob: TDataSetProvider
    DataSet = QueryCompareJob
    Constraints = True
    Left = 240
    Top = 152
  end
  object ClientDataSetCompareJob: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderCompareJob'
    Left = 240
    Top = 232
  end
end
