inherited DialogReportStaffPlanningF: TDialogReportStaffPlanningF
  Left = 256
  Top = 112
  Caption = 'Report staff planning'
  ClientHeight = 485
  ClientWidth = 567
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 567
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 450
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 567
    Height = 366
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 40
      Top = 387
      Visible = False
    end
    inherited LblEmployee: TLabel
      Top = 411
      Visible = False
    end
    inherited LblToPlant: TLabel
      Top = 124
    end
    inherited LblToEmployee: TLabel
      Left = 323
      Top = 369
      Visible = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 144
      Top = 380
      Visible = False
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 328
      Top = 428
      Visible = False
    end
    object Label5: TLabel [8]
      Left = 120
      Top = 350
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label10: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [11]
      Left = 40
      Top = 43
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [12]
      Left = 8
      Top = 350
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [13]
      Left = 40
      Top = 350
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [14]
      Left = 315
      Top = 351
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [15]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel [16]
      Left = 120
      Top = 449
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [17]
      Left = 336
      Top = 449
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [18]
      Left = 336
      Top = 350
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [19]
      Left = 8
      Top = 448
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [20]
      Left = 40
      Top = 448
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [21]
      Left = 315
      Top = 450
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [22]
      Left = 8
      Top = 150
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [23]
      Left = 40
      Top = 150
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [24]
      Left = 315
      Top = 15
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [25]
      Left = 120
      Top = 121
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [26]
      Left = 336
      Top = 121
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [27]
      Left = 120
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label8: TLabel [28]
      Left = 336
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label9: TLabel [29]
      Left = 8
      Top = 120
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label22: TLabel [30]
      Left = 40
      Top = 120
      Width = 22
      Height = 13
      Caption = 'Shift'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label23: TLabel [31]
      Left = 315
      Top = 150
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 120
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
    end
    inherited LblToTeam: TLabel
      Top = 71
    end
    inherited LblStarTeamFrom: TLabel
      Left = 120
    end
    inherited LblTeam: TLabel
      Top = 69
    end
    inherited LblFromTeam: TLabel
      Top = 69
    end
    inherited LblFromWorkspot: TLabel
      Top = 96
    end
    inherited LblWorkspot: TLabel
      Top = 96
    end
    inherited LblStarWorkspotFrom: TLabel
      Left = 120
      Top = 98
    end
    inherited LblToWorkspot: TLabel
      Top = 96
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 98
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 131
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 132
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 68
      ColCount = 142
      TabOrder = 5
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 68
      ColCount = 143
      TabOrder = 6
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 520
      Top = 71
      TabOrder = 7
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 142
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      ColCount = 143
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 520
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 128
      Top = 402
      TabOrder = 35
      Visible = False
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 394
      TabOrder = 36
      Visible = False
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 148
      TabOrder = 29
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 149
      TabOrder = 31
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 23
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 30
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 24
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 25
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 157
      TabOrder = 32
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 158
      TabOrder = 34
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 94
      ColCount = 158
      TabOrder = 8
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 94
      ColCount = 159
      TabOrder = 9
    end
    inherited EditWorkspots: TEdit
      TabOrder = 33
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 177
      Width = 505
      Height = 184
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 15
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxOnlyPlanDev: TCheckBox
        Left = 8
        Top = 39
        Width = 161
        Height = 17
        Caption = 'Show only plan deviations'
        TabOrder = 1
      end
      object CheckBoxShowTB: TCheckBox
        Left = 8
        Top = 87
        Width = 137
        Height = 17
        Caption = 'Show per timeblock'
        TabOrder = 2
        OnClick = CheckBoxShowTBClick
      end
      object CheckBoxPaidBreaks: TCheckBox
        Left = 8
        Top = 111
        Width = 121
        Height = 17
        Caption = 'Include paid breaks'
        TabOrder = 3
      end
      object CheckBoxShowTotals: TCheckBox
        Left = 8
        Top = 159
        Width = 97
        Height = 17
        Caption = 'Show Totals'
        TabOrder = 4
      end
      object CheckBoxShowNotPlannedEmployees: TCheckBox
        Left = 248
        Top = 15
        Width = 233
        Height = 17
        Caption = 'Show not planned employees'
        TabOrder = 5
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 248
        Top = 39
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 6
      end
      object CheckBoxPageShift: TCheckBox
        Left = 248
        Top = 64
        Width = 169
        Height = 17
        Caption = 'New page per shift'
        TabOrder = 7
      end
      object CheckBoxPageWK: TCheckBox
        Left = 248
        Top = 86
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 8
      end
      object CheckBoxExport: TCheckBox
        Left = 248
        Top = 111
        Width = 177
        Height = 17
        Caption = 'Export'
        TabOrder = 9
      end
      object CheckBoxShowStartEndTimes: TCheckBox
        Left = 8
        Top = 63
        Width = 225
        Height = 17
        Caption = 'Show start/end times'
        TabOrder = 10
      end
      object CheckBoxSortOnEmployee: TCheckBox
        Left = 8
        Top = 135
        Width = 225
        Height = 17
        Caption = 'Show per employee'
        TabOrder = 11
      end
      object RGrpSortOnEmployee: TRadioGroup
        Left = 247
        Top = 133
        Width = 250
        Height = 41
        Caption = 'Sort on employee'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Number'
          'Name')
        TabOrder = 12
      end
    end
    object ComboBoxPlusShiftFrom: TComboBoxPlus
      Left = 120
      Top = 122
      Width = 180
      Height = 19
      ColCount = 134
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftFromCloseUp
    end
    object ComboBoxPlusShiftTo: TComboBoxPlus
      Left = 334
      Top = 121
      Width = 180
      Height = 19
      ColCount = 134
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 112
      Top = 448
      Width = 180
      Height = 19
      ColCount = 142
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 21
      TitleColor = clBtnFace
      Visible = False
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 334
      Top = 447
      Width = 180
      Height = 19
      ColCount = 143
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 22
      TitleColor = clBtnFace
      Visible = False
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 149
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 12
      OnChange = DateFromChange
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 148
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 520
      Top = 348
      Width = 41
      Height = 17
      Caption = 'All '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 26
      Visible = False
    end
  end
  inherited stbarBase: TStatusBar
    Top = 466
    Width = 567
  end
  inherited pnlBottom: TPanel
    Top = 427
    Width = 567
    Height = 39
    inherited btnOk: TBitBtn
      Left = 176
    end
    inherited btnCancel: TBitBtn
      Left = 290
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 160
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 264
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 544
    Top = 32
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4D040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072D4469616C6F675265706F72745374616666506C616E6E69
        6E67462E44617461536F75726365456D706C46726F6D104F7074696F6E734375
        73746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E64
        53697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C75
        6D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E73
        44420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C65
        74650D6564676F43616E496E73657274116564676F43616E4E61766967617469
        6F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C
        5265636F726473106564676F557365426F6F6B6D61726B7300000F5464784442
        47726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06
        064E756D62657206536F7274656407046373557005576964746802410942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F45
        4D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E
        0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F727420
        6E616D6505576964746802540942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060A53484F52545F4E414D4500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E0604
        4E616D6505576964746803B4000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060B4445534352495054494F4E00000F5464
        78444247726964436F6C756D6E0D436F6C756D6E416464726573730743617074
        696F6E06074164647265737305576964746802450942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D650607414444524553530000
        0F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F646507
        43617074696F6E060F4465706172746D656E7420636F64650557696474680258
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F4445504152544D454E545F434F444500000F546478444247726964436F
        6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F
        64650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        14040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072B4469616C6F675265706F72
        745374616666506C616E6E696E67462E44617461536F75726365456D706C546F
        104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E
        670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E
        67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E
        6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D65
        64676F43616E44656C6574650D6564676F43616E496E73657274116564676F43
        616E4E617669676174696F6E116564676F436F6E6669726D44656C6574651265
        64676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D6172
        6B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C07
        43617074696F6E06064E756D62657206536F7274656407046373557005576964
        746802310942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F454D504C4F5945455F4E554D42455200000F54647844424772
        6964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E
        060A53686F7274206E616D65055769647468024E0942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D
        4500000F546478444247726964436F6C756D6E11436F6C756D6E446573637269
        7074696F6E0743617074696F6E06044E616D6505576964746803CC000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060B44
        45534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C
        756D6E416464726573730743617074696F6E0607416464726573730557696474
        6802650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506074144445245535300000F546478444247726964436F6C756D6E0E
        436F6C756D6E44657074436F64650743617074696F6E060F4465706172746D65
        6E7420636F64650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F4445504152544D454E545F434F444500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609
        5465616D20636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 460
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_NUMBER,'
      '  S.PLANT_CODE,'
      '  S.DESCRIPTION'
      'FROM '
      '  SHIFT S'
      'WHERE'
      '  S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  S.SHIFT_NUMBER')
    Left = 200
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryWorkSpot: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      ' W.WORKSPOT_CODE, W.DESCRIPTION, W.PLANT_CODE'
      'FROM '
      ' WORKSPOT W'
      'LEFT JOIN DEPARTMENT D ON'
      ' W.PLANT_CODE = D.PLANT_CODE AND'
      ' W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      'LEFT JOIN DEPARTMENTPERTEAM T ON'
      ' D.PLANT_CODE = T.PLANT_CODE AND'
      ' D.DEPARTMENT_CODE = T.DEPARTMENT_CODE '
      'WHERE'
      ' W.PLANT_CODE  = :PLANT_CODE AND'
      ' ('
      '   (:USER_NAME = '#39'*'#39') OR'
      '   (T.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME)'
      '   )'
      ' )'
      'ORDER BY'
      ' W.WORKSPOT_CODE')
    Left = 344
    Top = 23
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryWorkSpotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryWorkSpotDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object QueryWorkSpotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
  end
end
