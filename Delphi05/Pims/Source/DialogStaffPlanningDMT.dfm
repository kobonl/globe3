inherited DialogStaffPlanningDM: TDialogStaffPlanningDM
  OldCreateOrder = True
  Left = 188
  Top = 107
  Height = 519
  Width = 796
  inherited TableMaster: TTable
    Left = 52
    Top = 8
  end
  inherited TableDetail: TTable
    Left = 52
    Top = 60
  end
  inherited DataSourceMaster: TDataSource
    Left = 144
    Top = 8
  end
  inherited DataSourceDetail: TDataSource
    Left = 144
    Top = 60
  end
  inherited TableExport: TTable
    Left = 388
    Top = 212
  end
  inherited DataSourceExport: TDataSource
    Top = 212
  end
  object QueryShiftPlant: TQuery
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.PLANT_CODE, S.SHIFT_NUMBER, S.DESCRIPTION'
      'FROM  '
      '  SHIFT S'
      'WHERE'
      '  S.SHIFT_NUMBER <> 0 '
      'ORDER BY '
      '  S.PLANT_CODE, S.SHIFT_NUMBER')
    Left = 384
    Top = 112
  end
  object QueryWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 144
    Top = 240
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 56
    Top = 240
  end
  object StoredProcReadPastDay: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_READINTODAYPLN'
    Left = 56
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATENOW'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATESOURCEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATESOURCETO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end>
  end
  object StoredProcReadSTDPLNXXX: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_SELECTSTDEMPPLN'
    Left = 560
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATENOW'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DAYSTART'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DAYEND'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DAYTMP'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_1'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_2'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_3'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_4'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'RECCOUNT'
        ParamType = ptOutput
      end>
  end
  object StoredProcSELECTEMAXXX: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_SELECTEMA'
    Left = 560
    Top = 312
  end
  object QueryFillEMA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 144
    Top = 128
  end
  object QueryRDP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  COUNT(*) AS RECCOUNT '
      'FROM '
      '  READINTODAYPLANNING R'
      'WHERE '
      '  R.PLANT_CODE >= :PLANTFROM AND '
      '  R.PLANT_CODE <= :PLANTTO AND '
      '  R.TEAM_CODE >= :TEAMFROM AND '
      '  R.TEAM_CODE <= :TEAMTO AND '
      '  R.READ_DATE >= :DATEFROM AND '
      '  R.READ_DATE <= :DATETO')
    Left = 144
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object StoredProcWriteInRDP: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_WRITEINRDP'
    Left = 184
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptInput
      end>
  end
  object QueryEPL: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING'
      'WHERE'
      '  (EMPLOYEEPLANNING_DATE = :DATEEMA) AND'
      '   (SHIFT_NUMBER = :SHIFT_NUMBER) AND'
      '  (PLANT_CODE = :PLANT_CODE) AND'
      '   (EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND'
      '    ((SCHEDULED_TIMEBLOCK_1 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_2 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_3 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_4 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_5 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_6 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_7 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_8 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_9 <> '#39'N'#39') OR'
      '    (SCHEDULED_TIMEBLOCK_10 <> '#39'N'#39'))')
    Left = 56
    Top = 184
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEEMA'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object StoredProcFillEPL: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_FILLEPL'
    Left = 56
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'SHIFT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WORKSPOT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_1'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_2'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_3'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_4'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_5'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_6'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_7'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_8'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_9'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'SCHEDULED_TIMEBLOCK_10'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEEPL'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATENOW'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end>
  end
  object QueryWKDesc: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DESCRIPTION '
      'FROM '
      '  WORKSPOT '
      'WHERE '
      '  PLANT_CODE = :FPLANT AND'
      '  WORKSPOT_CODE = :WK')
    Left = 56
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'FPLANT'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WK'
        ParamType = ptUnknown
      end>
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  TEAM_CODE, DESCRIPTION'
      'FROM '
      '  TEAM    '
      'ORDER BY '
      '  TEAM_CODE')
    Left = 380
    Top = 16
  end
  object ClientDataSetTeam: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderTeam'
    Left = 488
    Top = 16
  end
  object DataSetProviderTeam: TDataSetProvider
    DataSet = QueryTeam
    Constraints = True
    Left = 600
    Top = 16
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantFilterRecord
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DESCRIPTION '
      'FROM '
      '  PLANT '
      'ORDER BY '
      '  PLANT_CODE')
    Left = 380
    Top = 64
  end
  object ClientDataSetPlant: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderPlant'
    Left = 488
    Top = 64
    object ClientDataSetPlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object ClientDataSetPlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object DataSetProviderPlant: TDataSetProvider
    DataSet = QueryPlant
    Constraints = True
    Left = 600
    Top = 64
  end
  object ClientDataSetShift: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 488
    Top = 112
  end
  object DataSetProviderShift: TDataSetProvider
    DataSet = QueryShiftPlant
    Constraints = True
    Left = 600
    Top = 112
  end
  object StoredProcDELETEEPLNSTD: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_DELETEEPLNSTD'
    Left = 184
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptInput
      end>
  end
  object QueryPlantTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DISTINCT DPT.PLANT_CODE,'
      '  P.DESCRIPTION'
      'FROM'
      '  DEPARTMENTPERTEAM DPT,'
      '  PLANT P'
      'WHERE'
      '  DPT.TEAM_CODE = :TEAM_CODE AND'
      '  DPT.PLANT_CODE = P.PLANT_CODE')
    Left = 384
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
end
