(*
  MRA: Order 550441
  This dialog gives a database-error if a record is changed:
  - User selects a reoord.
  - User select another hour-type for this record.
  - Error-message 'Record/key deleted' (8708) is shown.
  This has to with the sorting of the table, it is sorted by
  ABSENCEREASON_CODE. Also because the contents is a mix of
  capital and small letters, example: 'B', 'a', 'U', 'k'.
  It can be solved by setting a property:
  - TableDetail.IndexFieldNames := 'ABSENCEREASON_ID'
  But then, of course, the grid is not sorted correctly.
  This latter can be solved by rearranging the contents (if needed),
  before the Pims-application is started, or before this Form is started.
  MRA:19-FEB-2018 Glob3-72.
  - Add Export_code to Absence Reason.
*)

unit AbsReasonDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TAbsReasonDM = class(TGridBaseDM)
    TableAbsenceType: TTable;
    TableHourType: TTable;
    DataSourceAbsenceType: TDataSource;
    DataSourceHourType: TDataSource;
    TableAbsenceTypeABSENCETYPE_CODE: TStringField;
    TableAbsenceTypeDESCRIPTION: TStringField;
    TableAbsenceTypeCREATIONDATE: TDateTimeField;
    TableAbsenceTypeEXPORT_CODE: TStringField;
    TableAbsenceTypeMUTATIONDATE: TDateTimeField;
    TableAbsenceTypeMUTATOR: TStringField;
    TableHourTypeHOURTYPE_NUMBER: TIntegerField;
    TableHourTypeDESCRIPTION: TStringField;
    TableHourTypeCREATIONDATE: TDateTimeField;
    TableHourTypeOVERTIME_YN: TStringField;
    TableHourTypeMUTATIONDATE: TDateTimeField;
    TableHourTypeMUTATOR: TStringField;
    TableHourTypeCOUNT_DAY_YN: TStringField;
    TableHourTypeEXPORT_CODE: TStringField;
    TableHourTypeBONUS_PERCENTAGE: TFloatField;
    QueryDetail: TQuery;
    QueryDetailABSENCEREASON_ID: TIntegerField;
    QueryDetailABSENCETYPE_CODE: TStringField;
    QueryDetailABSENCEREASON_CODE: TStringField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailHOURTYPE_NUMBER: TIntegerField;
    QueryDetailPAYED_YN: TStringField;
    QueryDetailOVERRULE_WITH_ILLNESS_YN: TStringField;
    QueryDetailCREATIONDATE: TDateTimeField;
    QueryDetailMUTATIONDATE: TDateTimeField;
    QueryDetailMUTATOR: TStringField;
    QueryDetailHOURTYPELU: TStringField;
    QueryDetailABSENCETYPELU: TStringField;
    QueryDetailCORRECT_WORK_HRS_YN: TStringField;
    QueryDetailEXPORT_CODE: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryDetailNewRecord(DataSet: TDataSet);
    procedure QueryDetailAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    procedure AbsenceReasonRefresh(DataSet: TDataSet);
  public
    { Public declarations }
  end;

var
  AbsReasonDM: TAbsReasonDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TAbsReasonDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryDetail.Open;
end;

procedure TAbsReasonDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  try
    if (QueryDetail.State in [dsEdit, dsInsert]) then
      QueryDetail.Post;
  finally
    QueryDetail.Close;
  end;
end;

// MR:09-02-2007 Order 550441.
// Use TQuery instead of TTable, for solving
// problem with sorting related with small/capitals in
// field ABSENCEREASON_CODE.
procedure TAbsReasonDM.QueryDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not TableAbsenceType.IsEmpty then
  begin
    TableAbsenceType.First;
    DataSet.FieldByName('ABSENCETYPE_CODE').AsString :=
      TableAbsenceType.FieldByName('ABSENCETYPE_CODE').AsString;
  end;
  if not TableHourType.IsEmpty then
  begin
    TableHourType.First;
    DataSet.FieldByName('HOURTYPE_NUMBER').AsInteger :=
      TableHourType.FieldByName('HOURTYPE_NUMBER').AsInteger;
  end;
  // Pims -> Oracle, the stored proc. is not used anymore.
  // MR:01-03-2005 Generate a new (unique) ID for AbsenceReason-table.
//  StoredProcAbsenceReasonGenID.Prepare;
//  StoredProcAbsenceReasonGenID.ExecProc;
//  StoredProcAbsenceReasonGenID.GetResults;
//  TableDetailABSENCEREASON_ID.Value :=
//    StoredProcAbsenceReasonGenID.ParamByName('ABSENCEREASON_ID').AsInteger;
  DataSet.FieldByName('PAYED_YN').AsString := 'N';
  DataSet.FieldByName('OVERRULE_WITH_ILLNESS_YN').AsString := 'N';
end;

// MR:09-02-2007 Refresh the Detail-Dataset each time a 'post'
// is done. This prevents problem:
// - If a record is added, another disappears in the grid.
//   Only when you re-open the form, this record is shown again.
//   Seems to be a problem with BDE that tries to sort the records,
//   in combination with small/capital letters in a key-field.
procedure TAbsReasonDM.AbsenceReasonRefresh(DataSet: TDataSet);
var
  MyKey: String;
begin
  with DataSet do
  begin
    if not (State in [dsEdit, dsInsert]) then
    begin
      MyKey := FieldByName('ABSENCEREASON_CODE').AsString;
      Close;
      Open;
      Locate('ABSENCEREASON_CODE', MyKey, []);
    end;
  end;
end;

procedure TAbsReasonDM.QueryDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  AbsenceReasonRefresh(DataSet);
end;


end.
