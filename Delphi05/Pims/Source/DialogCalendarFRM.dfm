object DialogCalendarF: TDialogCalendarF
  Left = 429
  Top = 297
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  ClientHeight = 156
  ClientWidth = 188
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object MonthCalendarGrid: TMonthCalendar
    Left = -5
    Top = 0
    Width = 197
    Height = 161
    Date = 37957
    TabOrder = 0
    OnClick = MonthCalendarGridClick
    OnGetMonthInfo = MonthCalendarGridGetMonthInfo
  end
end
