inherited EmployeeAvailabilityF: TEmployeeAvailabilityF
  Tag = 1
  Left = 458
  Top = 188
  Width = 737
  Height = 594
  Caption = 'Employee availability'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 721
    Height = 54
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 300
      Width = 696
      Height = 0
      Cursor = crHSplit
      Align = alNone
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 53
      Width = 719
      Height = 0
      DefaultLayout = False
      Align = alBottom
      Visible = False
      ShowBands = True
    end
    object GroupBoxEmployee: TGroupBox
      Left = 1
      Top = 1
      Width = 719
      Height = 50
      Align = alTop
      Caption = 'Employee / Year'
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 21
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object Label2: TLabel
        Left = 392
        Top = 22
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object LabelEmplDesc: TLabel
        Left = 176
        Top = 22
        Width = 70
        Height = 13
        Caption = 'LabelEmplDesc'
      end
      object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
        Left = 72
        Top = 18
        Width = 97
        TabOrder = 0
        OnEnter = dxDBExtLookupEditEmployeeEnter
        DataField = 'EMPLOYEE_NUMBER'
        DataSource = EmployeeAvailabilityDM.DataSourceMaster
        HideEditCursor = True
        PopupWidth = 550
        OnCloseUp = dxDBExtLookupEditEmployeeCloseUp
        DBGridLayout = dxDBGridLayoutList1Item1
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 432
        Top = 18
        Width = 73
        TabOrder = 1
        OnChange = dxSpinEditYearChange
        MaxValue = 2099
        MinValue = 1950
        Value = 2000
        StoredValues = 48
      end
      object CheckBoxPlanInOtherPlants: TCheckBox
        Left = 510
        Top = 20
        Width = 132
        Height = 17
        Caption = 'Plan in other plants'
        TabOrder = 2
        OnClick = CheckBoxPlanInOtherPlantsClick
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 419
    Width = 721
    Height = 136
    TabOrder = 3
    object GroupBoxDetail: TGroupBox
      Left = 1
      Top = 1
      Width = 719
      Height = 100
      Align = alTop
      Caption = 'Availability'
      TabOrder = 0
      object dxDBEditP15: TdxDBEdit
        Left = 472
        Top = 8
        Width = 22
        TabOrder = 0
        Visible = False
        DataField = 'PLANT1'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP25: TdxDBEdit
        Left = 496
        Top = 8
        Width = 22
        TabOrder = 1
        Visible = False
        DataField = 'PLANT2'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP35: TdxDBEdit
        Left = 520
        Top = 8
        Width = 22
        TabOrder = 2
        Visible = False
        DataField = 'PLANT3'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP45: TdxDBEdit
        Left = 544
        Top = 8
        Width = 22
        TabOrder = 3
        Visible = False
        DataField = 'PLANT4'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP55: TdxDBEdit
        Left = 568
        Top = 8
        Width = 22
        TabOrder = 4
        Visible = False
        DataField = 'PLANT5'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP65: TdxDBEdit
        Left = 592
        Top = 8
        Width = 22
        TabOrder = 5
        Visible = False
        DataField = 'PLANT6'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object dxDBEditP75: TdxDBEdit
        Left = 616
        Top = 8
        Width = 22
        TabOrder = 6
        Visible = False
        DataField = 'PLANT7'
        DataSource = EmployeeAvailabilityDM.DataSourceDetail
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 715
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 7
        object lblPeriod: TLabel
          Left = 139
          Top = 5
          Width = 40
          Height = 13
          Caption = 'lblPeriod'
        end
        object LabelWeek: TLabel
          Left = 8
          Top = 3
          Width = 27
          Height = 13
          Caption = 'Week'
        end
        object GroupBox6: TGroupBox
          Left = 339
          Top = -24
          Width = 90
          Height = 41
          Caption = 'Week'
          TabOrder = 0
          Visible = False
        end
        object dxDBSpinEditWeek: TdxDBSpinEdit
          Left = 64
          Top = 1
          Width = 65
          TabOrder = 1
          DataSource = EmployeeAvailabilityDM.DataSourceDetail
          OnChange = dxDBSpinEditWeekChange
          MaxValue = 53
          MinValue = 1
          StoredValues = 48
        end
      end
      object ScrollBox1: TScrollBox
        Left = 2
        Top = 38
        Width = 715
        Height = 60
        Align = alClient
        BorderStyle = bsNone
        TabOrder = 8
        object GroupBoxDay1: TGroupBox
          Tag = 1
          Left = 0
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 0
          object dxDBEdit11: TdxDBEdit
            Tag = 11
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY11'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit12: TdxDBEdit
            Tag = 12
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY12'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit13: TdxDBEdit
            Tag = 13
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY13'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit14: TdxDBEdit
            Tag = 14
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY14'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit111: TdxDBEdit
            Tag = 15
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY111'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit15: TdxDBEdit
            Tag = 11
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY15'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit16: TdxDBEdit
            Tag = 12
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY16'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit17: TdxDBEdit
            Tag = 13
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY17'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit18: TdxDBEdit
            Tag = 14
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY18'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit19: TdxDBEdit
            Tag = 13
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY19'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit110: TdxDBEdit
            Tag = 14
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY110'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay2: TGroupBox
          Tag = 1
          Left = 188
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 1
          object dxDBEdit21: TdxDBEdit
            Tag = 21
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY21'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit22: TdxDBEdit
            Tag = 22
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY22'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit23: TdxDBEdit
            Tag = 23
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY23'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit24: TdxDBEdit
            Tag = 24
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY24'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit211: TdxDBEdit
            Tag = 25
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY211'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit25: TdxDBEdit
            Tag = 21
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY25'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit26: TdxDBEdit
            Tag = 22
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY26'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit27: TdxDBEdit
            Tag = 23
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY27'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit28: TdxDBEdit
            Tag = 24
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY28'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit29: TdxDBEdit
            Tag = 23
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY29'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit210: TdxDBEdit
            Tag = 24
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY210'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay3: TGroupBox
          Tag = 1
          Left = 376
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 2
          object dxDBEdit31: TdxDBEdit
            Tag = 31
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY31'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit32: TdxDBEdit
            Tag = 32
            Left = 41
            Top = 17
            Width = 16
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY32'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit33: TdxDBEdit
            Tag = 33
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY33'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit34: TdxDBEdit
            Tag = 34
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY34'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit311: TdxDBEdit
            Tag = 35
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY311'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit35: TdxDBEdit
            Tag = 31
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY35'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit36: TdxDBEdit
            Tag = 32
            Left = 105
            Top = 17
            Width = 16
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY36'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit37: TdxDBEdit
            Tag = 33
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY37'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit38: TdxDBEdit
            Tag = 34
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY38'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit39: TdxDBEdit
            Tag = 33
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY39'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit310: TdxDBEdit
            Tag = 34
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY310'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay4: TGroupBox
          Tag = 1
          Left = 564
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 3
          object dxDBEdit41: TdxDBEdit
            Tag = 41
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY41'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit42: TdxDBEdit
            Tag = 42
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY42'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit43: TdxDBEdit
            Tag = 43
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY43'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit44: TdxDBEdit
            Tag = 44
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY44'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit411: TdxDBEdit
            Tag = 45
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY411'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit45: TdxDBEdit
            Tag = 41
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY45'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit46: TdxDBEdit
            Tag = 42
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY46'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit47: TdxDBEdit
            Tag = 43
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY47'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit48: TdxDBEdit
            Tag = 44
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY48'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit49: TdxDBEdit
            Tag = 41
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY49'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit410: TdxDBEdit
            Tag = 42
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY410'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay5: TGroupBox
          Tag = 1
          Left = 752
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 4
          object dxDBEdit51: TdxDBEdit
            Tag = 51
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY51'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit52: TdxDBEdit
            Tag = 52
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY52'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit53: TdxDBEdit
            Tag = 53
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY53'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit54: TdxDBEdit
            Tag = 54
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY54'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit511: TdxDBEdit
            Tag = 55
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY511'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit55: TdxDBEdit
            Tag = 51
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY55'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit56: TdxDBEdit
            Tag = 52
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY56'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit57: TdxDBEdit
            Tag = 53
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY57'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit58: TdxDBEdit
            Tag = 54
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY58'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit59: TdxDBEdit
            Tag = 51
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY59'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit510: TdxDBEdit
            Tag = 52
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY510'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay6: TGroupBox
          Tag = 1
          Left = 940
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 5
          object dxDBEdit61: TdxDBEdit
            Tag = 61
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY61'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit62: TdxDBEdit
            Tag = 62
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY62'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit63: TdxDBEdit
            Tag = 63
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY63'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit64: TdxDBEdit
            Tag = 64
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY64'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit611: TdxDBEdit
            Tag = 65
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY611'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit65: TdxDBEdit
            Tag = 61
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY65'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit66: TdxDBEdit
            Tag = 62
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY66'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit67: TdxDBEdit
            Tag = 63
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY67'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit68: TdxDBEdit
            Tag = 64
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY68'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit69: TdxDBEdit
            Tag = 61
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY69'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit610: TdxDBEdit
            Tag = 62
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY610'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
        object GroupBoxDay7: TGroupBox
          Tag = 1
          Left = 1128
          Top = 0
          Width = 188
          Height = 43
          Align = alLeft
          Caption = 'Monday'
          TabOrder = 6
          object dxDBEdit71: TdxDBEdit
            Tag = 71
            Left = 25
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 1
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY71'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit72: TdxDBEdit
            Tag = 72
            Left = 41
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 2
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY72'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit73: TdxDBEdit
            Tag = 73
            Left = 57
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 3
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY73'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit74: TdxDBEdit
            Tag = 74
            Left = 73
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 4
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY74'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit711: TdxDBEdit
            Tag = 75
            Left = 3
            Top = 17
            Width = 20
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnKeyDown = dxDBEdit111KeyDown
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = dxDBEdit111MouseDown
            OnContextPopup = dxDBEdit111ContextPopup
            DataField = 'DAY711'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 2
            OnChange = dxDBEdit111Change
            StoredValues = 2
          end
          object dxDBEdit75: TdxDBEdit
            Tag = 71
            Left = 89
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 5
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY75'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit76: TdxDBEdit
            Tag = 72
            Left = 105
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 6
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY76'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit77: TdxDBEdit
            Tag = 73
            Left = 121
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 7
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY77'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit78: TdxDBEdit
            Tag = 74
            Left = 137
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 8
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY78'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit79: TdxDBEdit
            Tag = 71
            Left = 153
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 9
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY79'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
          object dxDBEdit710: TdxDBEdit
            Tag = 72
            Left = 169
            Top = 17
            Width = 15
            PopupMenu = PopupMenuAbsRsn
            TabOrder = 10
            OnExit = Edit1Exit
            OnKeyDown = dxDBEdit11KeyDown
            OnMouseDown = dxDBEdit14MouseDown
            OnContextPopup = dxDBEdit11ContextPopup
            DataField = 'DAY710'
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            MaxLength = 1
            OnSelectionChange = dxDBEdit11SelectionChange
            StoredValues = 2
          end
        end
      end
    end
    object ButtonRestoreStd: TButton
      Left = 4
      Top = 107
      Width = 90
      Height = 25
      Caption = 'Restore standard'
      TabOrder = 1
      OnClick = ButtonRestoreStdClick
    end
    object ButtonStaffAvailability: TButton
      Left = 99
      Top = 107
      Width = 90
      Height = 25
      Caption = 'Staff availability'
      TabOrder = 2
      OnClick = ButtonStaffAvailabilityClick
    end
    object ButtonCopyFromOtherWeek: TButton
      Left = 194
      Top = 107
      Width = 134
      Height = 25
      Caption = 'Copy from other week'
      TabOrder = 3
      OnClick = ButtonCopyFromOtherWeekClick
    end
    object ButtonAddHoliday: TButton
      Left = 333
      Top = 107
      Width = 140
      Height = 25
      Caption = 'Add Holiday / Absence'
      TabOrder = 4
      OnClick = ButtonAddHolidayClick
    end
    object ButtonWorkSchedule: TButton
      Left = 477
      Top = 107
      Width = 140
      Height = 25
      Caption = 'Process workschedule'
      TabOrder = 5
      OnClick = ButtonWorkScheduleClick
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 80
    Width = 721
    Height = 339
    Alignment = taLeftJustify
    inherited spltDetail: TSplitter
      Top = -1
      Width = 680
      Height = 0
      Cursor = crHSplit
      Align = alNone
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Top = 131
      Width = 680
      Height = 0
      DefaultLayout = False
      SummaryGroups = <
        item
          DefaultGroup = True
          SummaryItems = <
            item
              SummaryField = 'PRODUCTION_MINUTE'
              SummaryFormat = '0.##'
              SummaryType = cstSum
            end>
          Name = 'dxDetailGridSummaryGroupMinute'
        end>
      Align = alNone
      TabOrder = 1
      Visible = False
      DblClkExpanding = False
      RowFooterColor = clBlack
      ShowBands = True
      ShowRowFooter = True
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 719
      Height = 337
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Availability'
        object PanelGrid: TPanel
          Left = 0
          Top = 0
          Width = 711
          Height = 309
          Align = alClient
          Caption = 'PanelGrid'
          TabOrder = 0
          object dxDBGridAvailability: TdxDBGrid
            Tag = 1
            Left = 1
            Top = 1
            Width = 709
            Height = 307
            Bands = <
              item
                Alignment = taLeftJustify
                Width = 52
              end
              item
                Caption = 'Monday'
                Width = 112
              end
              item
                Caption = 'Tuesday'
                Width = 109
              end
              item
                Caption = 'Wednesday'
                Width = 111
              end
              item
                Caption = 'Thursday'
                Width = 107
              end
              item
                Caption = 'Friday'
                Width = 108
              end
              item
                Caption = 'Saturday'
                Width = 113
              end
              item
                Caption = 'Sunday'
                Width = 117
              end>
            DefaultLayout = False
            HeaderPanelRowCount = 1
            KeyField = 'WEEK_NR'
            SummaryGroups = <>
            SummarySeparator = ', '
            Align = alClient
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            PopupMenu = PopupMenuCopyPaste
            TabOrder = 0
            OnEnter = dxDBGridAvailabilityEnter
            OnExit = dxDBGridAvailabilityExit
            BandFont.Charset = DEFAULT_CHARSET
            BandFont.Color = clWindowText
            BandFont.Height = -11
            BandFont.Name = 'Tahoma'
            BandFont.Style = []
            DataSource = EmployeeAvailabilityDM.DataSourceDetail
            HeaderFont.Charset = DEFAULT_CHARSET
            HeaderFont.Color = clWindowText
            HeaderFont.Height = -11
            HeaderFont.Name = 'Tahoma'
            HeaderFont.Style = []
            HideSelectionTextColor = clWindowText
            HighlightTextColor = clWindow
            OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
            OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
            OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
            PreviewFont.Charset = DEFAULT_CHARSET
            PreviewFont.Color = clBlue
            PreviewFont.Height = -11
            PreviewFont.Name = 'Tahoma'
            PreviewFont.Style = []
            ShowBands = True
            OnBackgroundDrawEvent = dxDBGridAvailabilityBackgroundDrawEvent
            OnCustomDrawBand = dxDBGridAvailabilityCustomDrawBand
            OnCustomDrawCell = dxDBGridAvailabilityCustomDrawCell
            OnCustomDrawColumnHeader = dxDBGridAvailabilityCustomDrawColumnHeader
            object dxDBGridAvailabilityColumnWeek: TdxDBGridColumn
              Caption = 'Week'
              Width = 52
              BandIndex = 0
              RowIndex = 0
              FieldName = 'WEEK_NR'
            end
            object dxDBGridAvailabilityColumnD111: TdxDBGridColumn
              Caption = 'S'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY111'
            end
            object dxDBGridAvailabilityColumnD11: TdxDBGridColumn
              Caption = '1'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY11'
            end
            object dxDBGridAvailabilityColumnD12: TdxDBGridColumn
              Caption = '2'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY12'
            end
            object dxDBGridAvailabilityColumnD13: TdxDBGridColumn
              Caption = '3'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY13'
            end
            object dxDBGridAvailabilityColumnD14: TdxDBGridColumn
              Caption = '4'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY14'
            end
            object dxDBGridAvailabilityColumnD15: TdxDBGridColumn
              Caption = '5'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY15'
            end
            object dxDBGridAvailabilityColumnD16: TdxDBGridColumn
              Caption = '6'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY16'
            end
            object dxDBGridAvailabilityColumnD17: TdxDBGridColumn
              Caption = '7'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY17'
            end
            object dxDBGridAvailabilityColumnD18: TdxDBGridColumn
              Caption = '8'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY18'
            end
            object dxDBGridAvailabilityColumnD19: TdxDBGridColumn
              Caption = '9'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY19'
            end
            object dxDBGridAvailabilityColumnD110: TdxDBGridColumn
              Caption = '10'
              Width = 22
              BandIndex = 1
              RowIndex = 0
              FieldName = 'DAY110'
            end
            object dxDBGridAvailabilityColumnD211: TdxDBGridColumn
              Caption = 'S'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY211'
            end
            object dxDBGridAvailabilityColumnD21: TdxDBGridColumn
              Caption = '1'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY21'
            end
            object dxDBGridAvailabilityColumnD22: TdxDBGridColumn
              Caption = '2'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY22'
            end
            object dxDBGridAvailabilityColumnD23: TdxDBGridColumn
              Caption = '3'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY23'
            end
            object dxDBGridAvailabilityColumnD24: TdxDBGridColumn
              Caption = '4'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY24'
            end
            object dxDBGridAvailabilityColumnD25: TdxDBGridColumn
              Caption = '5'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY25'
            end
            object dxDBGridAvailabilityColumnD26: TdxDBGridColumn
              Caption = '6'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY26'
            end
            object dxDBGridAvailabilityColumnD27: TdxDBGridColumn
              Caption = '7'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY27'
            end
            object dxDBGridAvailabilityColumnD28: TdxDBGridColumn
              Caption = '8'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY28'
            end
            object dxDBGridAvailabilityColumnD29: TdxDBGridColumn
              Caption = '9'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY29'
            end
            object dxDBGridAvailabilityColumnD210: TdxDBGridColumn
              Caption = '10'
              Width = 22
              BandIndex = 2
              RowIndex = 0
              FieldName = 'DAY210'
            end
            object dxDBGridAvailabilityColumnD311: TdxDBGridColumn
              Caption = 'S'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY311'
            end
            object dxDBGridAvailabilityColumnD31: TdxDBGridColumn
              Caption = '1'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY31'
            end
            object dxDBGridAvailabilityColumnD32: TdxDBGridColumn
              Caption = '2'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY32'
            end
            object dxDBGridAvailabilityColumnD33: TdxDBGridColumn
              Caption = '3'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY33'
            end
            object dxDBGridAvailabilityColumnD34: TdxDBGridColumn
              Caption = '4'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY34'
            end
            object dxDBGridAvailabilityColumnD35: TdxDBGridColumn
              Caption = '5'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY35'
            end
            object dxDBGridAvailabilityColumnD36: TdxDBGridColumn
              Caption = '6'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY36'
            end
            object dxDBGridAvailabilityColumnD37: TdxDBGridColumn
              Caption = '7'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY37'
            end
            object dxDBGridAvailabilityColumnD38: TdxDBGridColumn
              Caption = '8'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY38'
            end
            object dxDBGridAvailabilityColumnD39: TdxDBGridColumn
              Caption = '9'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY39'
            end
            object dxDBGridAvailabilityColumnD310: TdxDBGridColumn
              Caption = '10'
              Width = 22
              BandIndex = 3
              RowIndex = 0
              FieldName = 'DAY310'
            end
            object dxDBGridAvailabilityColumnD411: TdxDBGridColumn
              Caption = 'S'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY411'
            end
            object dxDBGridAvailabilityColumnD41: TdxDBGridColumn
              Caption = '1'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY41'
            end
            object dxDBGridAvailabilityColumnD42: TdxDBGridColumn
              Caption = '2'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY42'
            end
            object dxDBGridAvailabilityColumnD43: TdxDBGridColumn
              Caption = '3'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY43'
            end
            object dxDBGridAvailabilityColumnD44: TdxDBGridColumn
              Caption = '4'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY44'
            end
            object dxDBGridAvailabilityColumnD45: TdxDBGridColumn
              Caption = '5'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY45'
            end
            object dxDBGridAvailabilityColumnD46: TdxDBGridColumn
              Caption = '6'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY46'
            end
            object dxDBGridAvailabilityColumnD47: TdxDBGridColumn
              Caption = '7'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY47'
            end
            object dxDBGridAvailabilityColumnD48: TdxDBGridColumn
              Caption = '8'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY48'
            end
            object dxDBGridAvailabilityColumnD49: TdxDBGridColumn
              Caption = '9'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY49'
            end
            object dxDBGridAvailabilityColumnD410: TdxDBGridColumn
              Caption = '10'
              Width = 21
              BandIndex = 4
              RowIndex = 0
              FieldName = 'DAY410'
            end
            object dxDBGridAvailabilityColumnD511: TdxDBGridColumn
              Caption = 'S'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY511'
            end
            object dxDBGridAvailabilityColumnD51: TdxDBGridColumn
              Caption = '1'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY51'
            end
            object dxDBGridAvailabilityColumnD52: TdxDBGridColumn
              Caption = '2'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY52'
            end
            object dxDBGridAvailabilityColumnD53: TdxDBGridColumn
              Caption = '3'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY53'
            end
            object dxDBGridAvailabilityColumnD54: TdxDBGridColumn
              Caption = '4'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY54'
            end
            object dxDBGridAvailabilityColumnD55: TdxDBGridColumn
              Caption = '5'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY55'
            end
            object dxDBGridAvailabilityColumnD56: TdxDBGridColumn
              Caption = '6'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY56'
            end
            object dxDBGridAvailabilityColumnD57: TdxDBGridColumn
              Caption = '7'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY57'
            end
            object dxDBGridAvailabilityColumnD58: TdxDBGridColumn
              Caption = '8'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY58'
            end
            object dxDBGridAvailabilityColumnD59: TdxDBGridColumn
              Caption = '9'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY59'
            end
            object dxDBGridAvailabilityColumnD510: TdxDBGridColumn
              Caption = '10'
              Width = 22
              BandIndex = 5
              RowIndex = 0
              FieldName = 'DAY510'
            end
            object dxDBGridAvailabilityColumnD611: TdxDBGridColumn
              Caption = 'S'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY611'
            end
            object dxDBGridAvailabilityColumnD61: TdxDBGridColumn
              Caption = '1'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY61'
            end
            object dxDBGridAvailabilityColumnD62: TdxDBGridColumn
              Caption = '2'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY62'
            end
            object dxDBGridAvailabilityColumnD63: TdxDBGridColumn
              Caption = '3'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY63'
            end
            object dxDBGridAvailabilityColumnD64: TdxDBGridColumn
              Caption = '4'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY64'
            end
            object dxDBGridAvailabilityColumnD65: TdxDBGridColumn
              Caption = '5'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY65'
            end
            object dxDBGridAvailabilityColumnD66: TdxDBGridColumn
              Caption = '6'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY66'
            end
            object dxDBGridAvailabilityColumnD67: TdxDBGridColumn
              Caption = '7'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY67'
            end
            object dxDBGridAvailabilityColumnD68: TdxDBGridColumn
              Caption = '8'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY68'
            end
            object dxDBGridAvailabilityColumnD69: TdxDBGridColumn
              Caption = '9'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY69'
            end
            object dxDBGridAvailabilityColumnD610: TdxDBGridColumn
              Caption = '10'
              Width = 23
              BandIndex = 6
              RowIndex = 0
              FieldName = 'DAY610'
            end
            object dxDBGridAvailabilityColumnD711: TdxDBGridColumn
              Caption = 'S'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY711'
            end
            object dxDBGridAvailabilityColumnD71: TdxDBGridColumn
              Caption = '1'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY71'
            end
            object dxDBGridAvailabilityColumnD72: TdxDBGridColumn
              Caption = '2'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY72'
            end
            object dxDBGridAvailabilityColumnD73: TdxDBGridColumn
              Caption = '3'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY73'
            end
            object dxDBGridAvailabilityColumnD74: TdxDBGridColumn
              Caption = '4'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY74'
            end
            object dxDBGridAvailabilityColumnD75: TdxDBGridColumn
              Caption = '5'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY75'
            end
            object dxDBGridAvailabilityColumnD76: TdxDBGridColumn
              Caption = '6'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY76'
            end
            object dxDBGridAvailabilityColumnD77: TdxDBGridColumn
              Caption = '7'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY77'
            end
            object dxDBGridAvailabilityColumnD78: TdxDBGridColumn
              Caption = '8'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY78'
            end
            object dxDBGridAvailabilityColumnD79: TdxDBGridColumn
              Caption = '9'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY79'
            end
            object dxDBGridAvailabilityColumnD710: TdxDBGridColumn
              Caption = '10'
              Width = 23
              BandIndex = 7
              RowIndex = 0
              FieldName = 'DAY710'
            end
            object dxDBGridAvailabilityColumnP1: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT1'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP2: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT2'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP3: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT3'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP4: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT4'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP5: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT5'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP6: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT6'
              DisableFilter = True
            end
            object dxDBGridAvailabilityColumnP7: TdxDBGridColumn
              DisableCaption = True
              DisableCustomizing = True
              DisableDragging = True
              DisableEditor = True
              Visible = False
              BandIndex = 0
              RowIndex = 0
              DisableGrouping = True
              FieldName = 'PLANT7'
              DisableFilter = True
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Balances'
        ImageIndex = 2
        object GroupBox3: TGroupBox
          Left = 0
          Top = 41
          Width = 657
          Height = 45
          Align = alTop
          Caption = 'Worktime Reduction'
          TabOrder = 0
          object Label8: TLabel
            Left = 16
            Top = 20
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label9: TLabel
            Left = 184
            Top = 20
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object Label12: TLabel
            Left = 314
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label13: TLabel
            Left = 416
            Top = 20
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label14: TLabel
            Left = 528
            Top = 20
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditWorkRemaining: TEdit
            Left = 116
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditWorkEarned: TEdit
            Left = 232
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditWorkUsed: TEdit
            Left = 344
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditWorkPlanned: TEdit
            Left = 460
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditWorkAvailable: TEdit
            Left = 580
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBox4: TGroupBox
          Left = 0
          Top = 86
          Width = 657
          Height = 42
          Align = alTop
          Caption = 'Illness'
          TabOrder = 1
          object Label3: TLabel
            Left = 20
            Top = 17
            Width = 66
            Height = 13
            Caption = 'Selected year'
          end
          object Label16: TLabel
            Left = 314
            Top = 17
            Width = 24
            Height = 13
            Caption = 'Total'
          end
          object EditIllness: TEdit
            Left = 116
            Top = 13
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditIllnessUntil: TEdit
            Left = 344
            Top = 13
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
        end
        object Employee: TGroupBox
          Left = 0
          Top = 0
          Width = 657
          Height = 41
          Align = alTop
          Caption = 'Employee'
          TabOrder = 2
          object Label4: TLabel
            Left = 20
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Start Date'
          end
          object EditStartDate: TEdit
            Left = 116
            Top = 12
            Width = 77
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '01/01/2002'
          end
        end
        object GroupBoxTravelTime: TGroupBox
          Left = 0
          Top = 128
          Width = 657
          Height = 49
          Align = alTop
          Caption = 'Travel Time'
          TabOrder = 3
          object Label49: TLabel
            Left = 16
            Top = 20
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label66: TLabel
            Left = 200
            Top = 20
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object Label70: TLabel
            Left = 320
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label71: TLabel
            Left = 430
            Top = 20
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label76: TLabel
            Left = 550
            Top = 20
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditTravelTimeRemaining: TEdit
            Left = 116
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditTravelTimeEarned: TEdit
            Left = 238
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditTravelTimeUsed: TEdit
            Left = 350
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditTravelTimePlanned: TEdit
            Left = 472
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditTravelTimeAvailable: TEdit
            Left = 594
            Top = 16
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Holiday Counters'
        ImageIndex = 2
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 657
          Height = 38
          Align = alTop
          Caption = 'Holiday'
          TabOrder = 0
          object Label6: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label7: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object Label10: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label11: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label5: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditHolidayRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditHolidayPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditHolidayNorm: TEdit
            Left = 242
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBox5: TGroupBox
          Left = 0
          Top = 38
          Width = 657
          Height = 42
          Align = alTop
          Caption = 'Time for Time'
          TabOrder = 1
          object Label17: TLabel
            Left = 16
            Top = 16
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label18: TLabel
            Left = 192
            Top = 16
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object Label19: TLabel
            Left = 320
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label27: TLabel
            Left = 430
            Top = 16
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label28: TLabel
            Left = 550
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditTimeRemaining: TEdit
            Left = 116
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditTimeEarned: TEdit
            Left = 240
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditTimeUsed: TEdit
            Left = 350
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditTimePlanned: TEdit
            Left = 474
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditTimeAvailable: TEdit
            Left = 594
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBoxHldPrevYear: TGroupBox
          Left = 0
          Top = 80
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Holiday Previous Year'
          TabOrder = 2
          object Label55: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label57: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label58: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label59: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditHldPrevYearRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditHldPrevYearUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditHldPrevYearPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditHldPrevYearAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
        end
        object GroupBoxSeniorityHld: TGroupBox
          Left = 0
          Top = 119
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Seniority Holiday'
          TabOrder = 3
          object Label52: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label53: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label54: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label51: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object EditSeniorityHldUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditSeniorityHldPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditSeniorityHldAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditSeniorityHldNorm: TEdit
            Left = 240
            Top = 12
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
        end
        object GroupBoxAddBnkHoliday: TGroupBox
          Left = 0
          Top = 158
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Additional Bank Holiday'
          TabOrder = 4
          object Label65: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label67: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label68: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label69: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object EditAddBnkHolidayRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditAddBnkHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditAddBnkHolidayPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditAddBnkHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
        end
        object GroupBoxSatCredit: TGroupBox
          Left = 0
          Top = 197
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Saturday Credit'
          TabOrder = 5
          object Label75: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
            Visible = False
          end
          object Label77: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label78: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label79: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label23: TLabel
            Left = 200
            Top = 15
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object EditSatCreditRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
            Visible = False
          end
          object EditSatCreditUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditSatCreditPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditSatCreditAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditSatCreditEarned: TEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBoxRsvBnkHoliday: TGroupBox
          Left = 0
          Top = 236
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Bank Holiday to Reserve'
          TabOrder = 6
          object Label72: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label73: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label74: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label56: TLabel
            Left = 208
            Top = 15
            Width = 25
            Height = 13
            Caption = 'Norm'
          end
          object EditRsvBnkHolidayUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditRsvBnkHolidayPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditRsvBnkHolidayAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditNormBankHldResMinManYN: TEdit
            Left = 240
            Top = 11
            Width = 33
            Height = 21
            TabOrder = 3
            Text = 'EditNormBankHldResMinManYN'
            Visible = False
          end
          object EditRsvBnkHolidayNorm: TEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
        object GroupBoxShorterWeek: TGroupBox
          Left = 0
          Top = 275
          Width = 657
          Height = 39
          Align = alTop
          Caption = 'Shorter Working Week'
          TabOrder = 7
          object Label60: TLabel
            Left = 16
            Top = 15
            Width = 94
            Height = 13
            Caption = 'Remaining last year'
          end
          object Label62: TLabel
            Left = 320
            Top = 15
            Width = 24
            Height = 13
            Caption = 'Used'
          end
          object Label63: TLabel
            Left = 430
            Top = 15
            Width = 38
            Height = 13
            Caption = 'Planned'
          end
          object Label64: TLabel
            Left = 550
            Top = 15
            Width = 43
            Height = 13
            Caption = 'Available'
          end
          object Label24: TLabel
            Left = 200
            Top = 15
            Width = 34
            Height = 13
            Caption = 'Earned'
          end
          object EditShorterWeekRemaining: TEdit
            Left = 116
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 0
            Text = '00:00'
          end
          object EditShorterWeekUsed: TEdit
            Left = 350
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 1
            Text = '00:00'
          end
          object EditShorterWeekPlanned: TEdit
            Left = 474
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 2
            Text = '00:00'
          end
          object EditShorterWeekAvailable: TEdit
            Left = 594
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 3
            Text = '00:00'
          end
          object EditShorterWeekEarned: TEdit
            Left = 240
            Top = 11
            Width = 55
            Height = 21
            Enabled = False
            TabOrder = 4
            Text = '00:00'
          end
        end
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = False
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 580
    Top = 0
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonEditMode: TdxBarButton
      Enabled = False
    end
    inherited dxBarButtonRecordDetails: TdxBarButton
      Enabled = False
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 492
    Top = 0
  end
  inherited StandardMenuActionList: TActionList
    Left = 548
    Top = 0
    object Action1: TAction
      Category = 'File'
      Caption = 'Action1'
    end
  end
  inherited dsrcActive: TDataSource
    Left = 296
    Top = 32
  end
  object PopupMenuCopyPaste: TPopupMenu
    AutoPopup = False
    Left = 272
    Top = 68
    object CopyRecord: TMenuItem
      Caption = 'Copy'
      OnClick = CopyRecordClick
    end
    object PasteRecord: TMenuItem
      Caption = 'Paste'
      OnClick = PasteRecordClick
    end
  end
  object PopupMenuAbsRsn: TPopupMenu
    OnPopup = PopupMenuAbsRsnPopup
    Left = 304
    Top = 68
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 521
    Top = 3
    object dxDBGridLayoutList1Item1: TdxDBGridLayout
      Data = {
        60040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656500000D44656661756C744C61796F7574091348
        656164657250616E656C526F77436F756E740201084B65794669656C64060F45
        4D504C4F5945455F4E554D4245521153686F7753756D6D617279466F6F746572
        090D53756D6D61727947726F7570730E001053756D6D61727953657061726174
        6F7206022C200E506172656E7453686F7748696E74080A44617461536F757263
        650727456D706C6F796565417661696C6162696C697479444D2E44617461536F
        757263654D61737465720F4D6178526F774C696E65436F756E7402010F4F7074
        696F6E734265686176696F720B0C6564676F4175746F536F72740B6564676F45
        646974696E67136564676F456E74657253686F77456469746F72136564676F49
        6D6D656469617465456469746F720E6564676F5461625468726F7567680F6564
        676F566572745468726F75676800094F7074696F6E7344420B106564676F4361
        6E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43616E
        496E73657274116564676F43616E4E617669676174696F6E116564676F436F6E
        6669726D44656C657465126564676F4C6F6164416C6C5265636F726473106564
        676F557365426F6F6B6D61726B73000B4F7074696F6E73566965770B13656467
        6F42616E644865616465725769647468106564676F496E7665727453656C6563
        740D6564676F5573654269746D6170000F53686F775072657669657747726964
        080D53686F77526F77466F6F746572091253696D706C65437573746F6D697A65
        426F7809000F546478444247726964436F6C756D6E0C436F6C756D6E4E756D62
        65720743617074696F6E06064E756D626572055769647468023D0942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F454D50
        4C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0743
        6F6C756D6E320743617074696F6E060A53686F7274204E616D65055769647468
        02540942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060A53484F52545F4E414D4500000F546478444247726964436F6C756D
        6E07436F6C756D6E330743617074696F6E06044E616D6505576964746803CA00
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060B4445534352495054494F4E00000F546478444247726964436F6C756D6E
        07436F6C756D6E340743617074696F6E0605506C616E74055769647468027309
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        0605504445534300000F546478444247726964436F6C756D6E07436F6C756D6E
        370743617074696F6E06055465616D2005576964746803E7000942616E64496E
        646578020008526F77496E6465780200094669656C644E616D65060554444553
        43000000}
    end
  end
  object PopupMenuShift: TPopupMenu
    AutoHotkeys = maManual
    OnPopup = PopupMenuAbsRsnPopup
    Left = 265
    Top = 35
  end
end
