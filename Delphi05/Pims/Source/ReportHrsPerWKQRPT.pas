(*
  Changes:
    MR:11-10-2004 Order 550345 Use Date-From-To instead of Year-1-week
      selection.
    MR:07-10-2005 Partly rewritten, so the combination USE_JOBCODE_YN = 'N'
      and 'BUSINESSUNITPERWORKSPOT with PERCENTAGE AGAINST USE_JOBCODE_YN = 'Y'
      will give a correct result.
      Also using a calculated field 'WEEKNUMBER' there is no need for internal
      loops to assemble all dates for a given weeknumber.
    MR:13-10-2005 Order 550411
      Select should give results for 3 situations:
      1. USE_JOBCODE_YN = 'Y' AND JOB_CODE <> '0'
      2. USE_JOBCODE_YN = 'N' (for any jobcode; '0' or not '0')
      3. USE_JOBCODE_YN = 'Y' AND JOB_CODE = '0'
      This can happen if user switches these settings in Pims on Workspot-level.
      The hours from PRODHOURPEREMPLOYEE should always be shown, regardless
      of these settings.
    MR:06-01-2006 Order 550419
      Show totals on department added (without showing workspots).
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Fields WEEKNUMBER, EMPLOYEE_NUMBER gave an error
      like:
        QueryProdHour:
          Type mismatch for field WEEKNUMBER, expecting float, actual integer.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'. (QueryProdHour).
      Also field-components for QueryProdHour for these fields must be 'float'.
    MR:4-NOV-2009 RV040.
      - Problem with business units per workspot and percentage.
        The percentage must be calculated in the select-statement!
        Or it will give wrong result when 'show business unit' is unchecked.
        Example with a workspot with 2 workspots per b.u.:
        - Formula: percentage / 100 * summin = minutes
        - 34 / 100 * 480 = 2:43 (163,2 -> rounded 163 minutes)
        - 66 / 100 * 480 = 5:17 (316,8 -> rounded 317 minutes)
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
    MRA:28-JAN-2010. RV050.4.1.
    - When 'AllEmployees' is False, then not only select on Employee-From-To,
      but ALSO on the plant from the employee! Because then only 1 plant is
      selected.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    SO: 15-JUN-2010 RV065.10. 550480
    - PIMS Hours per employee other plant
*)

unit ReportHrsPerWKQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DB, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo: String; FShowBU, FShowDept, FShowWK, FShowJob, FShowEmpl,
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK, FPageJob: Boolean;
    //RV065.10.
    FExportToFile, FAllEmployees, FSuppressZeroes, FIncludeNotConnectedEmp: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
      ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp: Boolean);
  end;

  TAmountsPerDay = Array[1..7] of Double; // RV040. Integer -> Double.
  TAmountsPrintPerDay = Array[1..7] of Boolean;

  TReportHrsPerWKQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDJob: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGroupWeekNumber: TQRGroup;
    QRBandWeekNumberFooter: TQRBand;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelJobCodeFrom: TQRLabel;
    QRLabelJobCodeTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDescBU: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRBandFooterWK: TQRBand;
    QRLabelTotalWK: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabelTotalBU: TQRLabel;
    QRLabelTotalPlant: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBandFooterDEPT: TQRBand;
    QRLabelDeptTot: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRBandFooterJOB: TQRBand;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRBandFooterEmpl: TQRBand;
    QRLabel106: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabelEmplTot: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRLabelTotJob: TQRLabel;
    QRDBTextJobCode: TQRDBText;
    QRDBTextJobDesc: TQRDBText;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabelJobTot: TQRLabel;
    QRLabelTotWK: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRShape1: TQRShape;
    QRBand1: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRBandSummary: TQRBand;
    QRLabel114: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText18: TQRDBText;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRLabelFromWeek: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRLabelFromDay: TQRLabel;
    QRLabelDayFrom: TQRLabel;
    QRLabelToWeek: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabelToDay: TQRLabel;
    QRLabelDayTo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabelDetailTot: TQRLabel;
    QRLabelWeek: TQRLabel;
    ChildBandUnknownBusinessUnit: TQRChildBand;
    QRLabelWeekUN: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabelDetailTotUN: TQRLabel;
    QRLabel28: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotalWKPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalBUPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalPlantPrint(sender: TObject; var Value: String);
    procedure QRDBTextDescBUPrint(sender: TObject; var Value: String);
    procedure QRDBTextWKDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelDeptTotPrint(sender: TObject; var Value: String);
    procedure QRDBTextJobDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotEmplPrint(sender: TObject; var Value: String);
    procedure QRDBTextEmplDescPrint(sender: TObject; var Value: String);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDescDeptPrint(sender: TObject; var Value: String);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJOBAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelJobTotPrint(sender: TObject; var Value: String);
    procedure ChildBandWKDummyBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextJobCodePrint(sender: TObject; var Value: String);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandWeekNumberFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandUnknownBusinessUnitBeforePrint(
      Sender: TQRCustomBand; var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    FOneWeek: Boolean;
    AllPlants, AllBusinessUnits: Boolean;
    function DetermineSUMMIN: Double; // RV040. Integer -> Double
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FPlant, FBU, FDept, FJob, FWorkspot: String;
    FEmpl: Integer;
    LevelPlant, LevelBU, LevelDept,
    LevelWK, LevelJob, LevelEmpl: Boolean;
    LastLevel: Char;
    FMinDate, FMaxDate: TDateTime;
    TotalEmpl,TotalBU, TotalWK, TotalJob, TotalDept,
    TotalPlant: TAmountsPerDay;
    TotalOnDays, TotalOnDaysUnknown: TAmountsPerDay;
    PrintTotalOnDays: TAmountsPrintPerDay;
    procedure SetLevelReport;
    procedure InitializeAmountsPerDaysOfWeek(var
      AmountsPerDaysOfWeek: TAmountsPerDay);
    procedure FillAmountsPerDaysOfWeek(AmountsPerDaysOfWeek: TAmountsPerDay;
      PrintDecide: Boolean);
    procedure FillDescDaysPerWeek;
    function Summarize(AmountsPerDaysOfWeek: TAmountsPerDay): Integer;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob: Boolean;
        ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
(*    function ExecuteForDummy(Plant, WK, BU:String): Integer; *)
    property OneWeek: Boolean read FOneWeek write FOneWeek;
  end;

var
  ReportHrsPerWKQR: TReportHrsPerWKQR;
  // MR:06-12-2002
  MyAmountsPerDaysOfWeek: TAmountsPerDay;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportHrsPerWKDMT, UGlobalFunctions, ListProcsFRM, UPimsConst, Math,
  UPimsMessageRes;

// MR:06-12-2002
procedure ExportDetail(PlantCode, BusinessUnitCode, DepartmentCode,
  WorkspotCode, JobCode, EmployeeNumber,
  V1, V2, V3, V4, V5, V6, V7, V8: String);
begin
  ExportClass.AddText(
    PlantCode + ExportClass.Sep +
    BusinessUnitCode + ExportClass.Sep +
    DepartmentCode + ExportClass.Sep +
    WorkspotCode + ExportClass.Sep +
    JobCode + ExportClass.Sep +
    EmployeeNumber + ExportClass.Sep +
    V1 + ExportClass.Sep +
    V2 + ExportClass.Sep +
    V3 + ExportClass.Sep +
    V4 + ExportClass.Sep +
    V5 + ExportClass.Sep +
    V6 + ExportClass.Sep +
    V7 + ExportClass.Sep +
    V8)
end;
//RV065.10.
procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo: String;
  ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
  ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
  ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FWKFrom := WKFrom;
  FWKTo := WKTo;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FShowBU := ShowBU;
  FShowDept := ShowDept;
  FShowWK := ShowWK;
  FShowJob := ShowJob;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FPageJob := PageJob;
  FExportToFile := ExportToFile;
  FAllEmployees := AllEmployees;
  FSuppressZeroes := SuppressZeroes;
  FIncludeNotConnectedEmp := IncludeNotConnectedEmp;
end;
//RV065.10.
function TReportHrsPerWKQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
  PageBU, PageDept, PageWK, PageJob: Boolean;
  ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
      ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp);
  end;
  SetDataSetQueryReport(ReportHrsPerWKDM.QueryProdHour);
end;

function TReportHrsPerWKQR.ExistsRecords: Boolean;
var
  SelectStr,  SelectStr1, SelectStr2, SelectStr3, TempStr: String;
  YearFrom, WeekFrom, YearTo, WeekTo: Word;
  SelectEmployeeStr: String;
//  OrderStr: String;
begin
  Screen.Cursor := crHourGlass;
  ReportHrsPerWKDM.QueryProdHour.Close;
  SetLevelReport;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;

  // MR:11-11-2005
  AllPlants := ReportHrsPerWKDM.DetermineAllPlants(
    QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllBusinessUnits := ReportHrsPerWKDM.DetermineAllBusinessUnits(
    QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
  ReportHrsPerWKDM.FillWeekTable(FMinDate, FMaxDate);

  // MR:11-01-2005 If selections spans only 1 week, don't show totals.
  // on levels 'workspot', 'employee' and 'job'.
  ListProcsF.WeekUitDat(QRParameters.FDateFrom, YearFrom, WeekFrom);
  ListProcsF.WeekUitDat(QRParameters.FDateTo, YearTo, WeekTo);
  OneWeek := (YearFrom = YearTo) and (WeekFrom = WeekTo);
  TempStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, PL.DESCRIPTION AS PDESC ' + NL;
  SelectStr1 := TempStr;
  SelectStr2 := TempStr;
  SelectStr3 := TempStr;
  if QRParameters.FShowBU then
  begin
//
    TempStr :=
      ', J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', BW.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
    SelectStr2 :=  SelectStr2 + TempStr;
    TempStr :=
      ', J.BUSINESSUNIT_CODE, CAST ('''' AS VARCHAR(30)) AS BDESC ' + NL; // Dummy!
    SelectStr3 :=  SelectStr3 + TempStr;
//
    // MR:07-10-2005 Set fields to fkData if needed.
    ReportHrsPerWKDM.QueryProdHourBUSINESSUNIT_CODE.FieldKind := fkData;
    ReportHrsPerWKDM.QueryProdHourBDESC.FieldKind := fkData;
  end
  else
  begin
    // MR:07-10-2005 Set fields to fkCalculated, otherwise an error will
    // be shown about missing fields, because they are persistent fields
    // defined in 'queryprodhour'-component.
    ReportHrsPerWKDM.QueryProdHourBUSINESSUNIT_CODE.FieldKind := fkCalculated;
    ReportHrsPerWKDM.QueryProdHourBDESC.FieldKind := fkCalculated;
  end;
  if QRParameters.FShowDept then
  begin
    TempStr :=
      ', W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    ReportHrsPerWKDM.QueryProdHourDEPARTMENT_CODE.FieldKind := fkData;
    ReportHrsPerWKDM.QueryProdHourDDESC.FieldKind := fkData;
  end
  else
  begin
    ReportHrsPerWKDM.QueryProdHourDEPARTMENT_CODE.FieldKind := fkCalculated;
    ReportHrsPerWKDM.QueryProdHourDDESC.FieldKind := fkCalculated;
  end;
  TempStr :=
    ', P.WORKSPOT_CODE, W.DESCRIPTION AS WDESC ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if QRParameters.FShowJob then
  begin
//
    TempStr :=
      ', P.JOB_CODE, J.DESCRIPTION AS JDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ',  P.JOB_CODE, CAST ('''' AS VARCHAR(30)) as JDESC' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
//
    ReportHrsPerWKDM.QueryProdHourJOB_CODE.FieldKind := fkData;
    ReportHrsPerWKDM.QueryProdHourJDESC.FieldKind := fkData;
  end
  else
  begin
    ReportHrsPerWKDM.QueryProdHourJOB_CODE.FieldKind := fkCalculated;
    ReportHrsPerWKDM.QueryProdHourJDESC.FieldKind := fkCalculated;
  end;
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      ',  CAST(P.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER ' ;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    ReportHrsPerWKDM.QueryProdHourEMPLOYEE_NUMBER.FieldKind := fkData;
  end
  else
    ReportHrsPerWKDM.QueryProdHourEMPLOYEE_NUMBER.FieldKind := fkCalculated;
  TempStr :=
    ', CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER ' + NL +
    ', P.PRODHOUREMPLOYEE_DATE ' + NL;
//    ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(1) AS LISTCODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    ', SUM(BW.PERCENTAGE / 100 * P.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(BW.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(2) AS LISTCODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL + // DUMMY!
    ', MAX(3) AS LISTCODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    ' FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN PLANT PL ON ' + NL +
    '    P.PLANT_CODE = PL.PLANT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  // RV050.4.1. Select also on employee-plant!
  if not QRParameters.FAllEmployees then
  begin
    TempStr :=
      ' INNER JOIN EMPLOYEE E ON ' + NL +
      '   E.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if not AllPlants then
  begin
    TempStr :=
      '    AND P.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '    AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    '    AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
    '    AND P.PRODHOUREMPLOYEE_DATE <= :FENDDATE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    P.PRODHOUREMPLOYEE_DATE >= WK.DATEFROM AND ' + NL +
    '    P.PRODHOUREMPLOYEE_DATE <= WK.DATETO ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (P.JOB_CODE <> ''' + DUMMYSTR + ''') ' + NL +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND P.JOB_CODE = J.JOB_CODE ' + NL +
    '  INNER JOIN BUSINESSUNIT B ON ' + NL +
    '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''N'' ' + NL +
    '  INNER JOIN BUSINESSUNITPERWORKSPOT BW ON ' + NL +
    '    P.PLANT_CODE = BW.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = BW.WORKSPOT_CODE ' + NL +
    '  INNER JOIN BUSINESSUNIT B ON ' + NL +
    '    BW.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (P.JOB_CODE = ''' + DUMMYSTR + ''')' + NL +
    '  LEFT JOIN JOBCODE J ON ' + NL + // Left Join -> Jobs do not exist!
    '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND P.JOB_CODE = J.JOB_CODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    W.PLANT_CODE = D.PLANT_CODE ' + NL +
    '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
//
    TempStr :=
      ' WHERE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    if not AllBusinessUnits then
    begin
      TempStr :=
        '  J.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND J.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
    end;
    if not AllBusinessUnits then
    begin
      TempStr :=
        '  BW.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND BW.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
      SelectStr2 := SelectStr2 + TempStr;
    end;
    // SelectStr3 -> No Jobs/BU!
    if not AllBusinessUnits then
    begin
      TempStr := ' AND ';
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      // SelectStr3 -> No Jobs/BU!
    end;
//
    // RV050.4.
    if QRParameters.FAllEmployees then
      SelectEmployeeStr := ''
    else
    begin
    // RV050.4.1. Select also on employee-plant!
      SelectEmployeeStr :=
        '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
        '  AND ( '
        //RV065.10.
        + '(E.PLANT_CODE >= ''' + NL +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '  AND E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
        if QRParameters.FIncludeNotConnectedEmp then
        begin
          SelectEmployeeStr := SelectEmployeeStr + ' OR ' + NL +
          '(P.PLANT_CODE >= ''' + NL +
          DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
          '  AND P.PLANT_CODE <= ''' +
          DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
        end;
        SelectEmployeeStr := SelectEmployeeStr + '  )' + NL +
(*
        '  AND E.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '  AND E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
*)
        '  AND ';
    end; 
    TempStr := SelectEmployeeStr +
      '  W.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND W.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND P.WORKSPOT_CODE >= ''' +
      DoubleQuote(QRParameters.FWKFrom) + '''' + NL +
      '  AND P.WORKSPOT_CODE <= ''' +
      DoubleQuote(QRParameters.FWKTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;

    if (QRParameters.FWKFrom = QRParameters.FWKTo)then
    begin
      if (QRParameters.FJobFrom <> '') and
        (QRParameters.FJobTo <> '') then
      begin
        TempStr :=
          '  AND P.JOB_CODE >= ''' + DoubleQuote(QRParameters.FJobFrom) +
          '''' + ' AND P.JOB_CODE <= ''' +
          DoubleQuote(QRParameters.FJobTo) + '''' + NL;
        SelectStr1 := SelectStr1 + TempStr;
      end;
      // For other selects: No Job available (JOB_CODE = '0' or
      // USE_JOBCODE is set to 'N')
    end;
  end;
  TempStr :=
    ' GROUP BY P.PLANT_CODE, PL.DESCRIPTION ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if QRParameters.FShowBU then
  begin
    TempStr :=
      '  , J.BUSINESSUNIT_CODE, B.DESCRIPTION  ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      '  , BW.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      '  , J.BUSINESSUNIT_CODE ' + NL; // Oracle->Leave out J.DESCRIPTION
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowDept then
  begin
    TempStr :=
      '  , W.DEPARTMENT_CODE, D.DESCRIPTION ' ;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    '  , P.WORKSPOT_CODE, W.DESCRIPTION ';
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if QRParameters.FShowJob then
  begin
    TempStr :=
      '  , P.JOB_CODE, J.DESCRIPTION ';
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      '  , P.JOB_CODE '; // Oracle->Leave out J.DESCRIPTION
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      '  , P.EMPLOYEE_NUMBER   ' ;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    '  , WK.WEEKNUMBER ' + NL +
    '  , P.PRODHOUREMPLOYEE_DATE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  SelectStr := SelectStr1 + NL +
    '  UNION ' + SelectStr2 + NL +
    '  UNION ' + SelectStr3 + NL;
  with ReportHrsPerWKDM.QueryProdHour do
  begin
    Active := False;
    Unidirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
// SQL.SaveToFile('c:\temp\rephrsperwk.sql');
    ParamByName('FSTARTDATE').Value := FMinDate;
    ParamByName('FENDDATE').Value := FMaxDate;
    if not Prepared then
      Prepare;
    Active := True;
    Result := not IsEmpty;
    if Result then
    begin
      ReportHrsPerWKDM.cdsQueryBUWK.Close;
      ReportHrsPerWKDM.cdsQueryBUWK.Open;
      ReportHrsPerWKDM.cdsEmpl.Close;
      ReportHrsPerWKDM.cdsEmpl.Open;
    end;
  end;

  Screen.Cursor := crDefault;
end;
(*
function TReportHrsPerWKQR.ExecuteForDummy(Plant, WK, BU: String): Integer;
begin
  Result := 0;
  if BU = '' then
  begin
    if ReportHrsPerWKDM.ClientDataSetBUWK.Locate('PLANT_CODE;WORKSPOT_CODE',
      VarArrayOf([Plant, WK ]), []) then
      Result :=
        ReportHrsPerWKDM.ClientDataSetBUWK.FieldByName('SUMMIN').AsInteger;
  end
  else
    if ReportHrsPerWKDM.ClientDataSetBUWK.Locate(
      'PLANT_CODE;WORKSPOT_CODE;BUSINESSUNIT_CODE',
       VarArrayOf([Plant, WK, BU ]), []) then
       Result :=
         ReportHrsPerWKDM.ClientDataSetBUWK.FieldByName('SUMMIN').AsInteger;
end;
*)

// RV040. Percentage is calculated in query! This gives a double-value.
function TReportHrsPerWKQR.DetermineSUMMIN: Double;
begin
  Result := ReportHrsPerWKDM.QueryProdHour.FieldByName('SUMMIN').AsFloat;
(*
  with ReportHrsPerWKDM do
  begin
    // Only do this for the second select of the UNIONS.
    // Only this select is of correct type (join to BUSINESSUNITPERWORKSPOT).
    if QueryProdHour.FieldByName('LISTCODE').AsInteger = 2 then
      if cdsQueryBUWK.Locate('PLANT_CODE;WORKSPOT_CODE;BUSINESSUNIT_CODE',
        VarArrayOf([QueryProdHour.FieldByName('PLANT_CODE').AsString,
          QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
          QueryProdHour.FieldByName('BUCODE').AsString]), []) then
        Result := Round(
          cdsQueryBUWK.FieldByName('SUMPERC').AsFloat / 100 *
            QueryProdHour.FieldByName('SUMMIN').AsInteger);
  end;
*)
end;

procedure TReportHrsPerWKQR.SetLevelReport;
begin
  LevelPlant := True;
  LevelBU := False;
  LevelDept := False;
  LevelWK := False;
  LevelJob := False;
  LevelEmpl := False;
  LastLevel := 'X';
 if QRParameters.FShowBU then
  begin
    LevelBU := True;
    LastLevel := 'B';
  end;
  if QRParameters.FShowDept then
  begin
    LevelDept := True;
    LastLevel := 'D';
  end;
  if QRParameters.FShowWK then
  begin
    LevelWK := True;
    LastLevel := 'W';
  end;
  if QRParameters.FShowJob then
  begin
    LevelJob := True;
    LastLevel := 'J';
  end;
  if QRParameters.FShowEmpl then
  begin
    LevelEmpl := True;
    LastLevel := 'E';
  end;
end;

procedure TReportHrsPerWKQR.ConfigReport;
var
  Year, Week, Day: Word;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes; // RV054.4.

  // MR:06-12-2002
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := QRParameters.FWKFrom;
  QRLabelWKTo.Caption := QRParameters.FWKTo;
  QRLabelJobCodeFrom.Caption := QRParameters.FJobFrom;
  QRLabelJobCodeTo.Caption := QRParameters.FJobTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);

  ListProcsF.WeekUitDat(QRParameters.FDateFrom, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  QRLabelWeekFrom.Caption := IntToStr(Week);
  QRLabelDayFrom.Caption := IntToStr(Day);
  ListProcsF.WeekUitDat(QRParameters.FDateTo, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  QRLabelWeekTo.Caption := IntToStr(Week);
  QRLabelDayTo.Caption := IntToStr(Day) + ')';

  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
  if (QRParameters.FWKFrom <> QRParameters.FWKTo) then
  begin
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
  end;
  // RV050.4.
  if QRParameters.FAllEmployees then
  begin
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;

  QRGroupHDBU.Enabled := QRParameters.FShowBU;
  QRBandFooterBU.Enabled := QRParameters.FShowBU;
  QRGroupHDDept.Enabled := QRParameters.FShowDept and QRParameters.FShowWK;
  QRBandFooterDept.Enabled := QRParameters.FShowDept;
  QRGroupHDWK.Enabled := QRParameters.FShowWK;
  QRBandFooterWK.Enabled := QRParameters.FShowWK;
  QRGroupHDJob.Enabled := QRParameters.FShowJob;
  QRBandFooterJob.Enabled := QRParameters.FShowJob;
  QRGroupHDEmpl.Enabled := QRParameters.FShowEmpl;
  QRBandFooterEmpl.Enabled := QRParameters.FShowEmpl;

(*
  if QRParameters.FShowBU then
  begin
    if LastLevel <> 'B' then
      QRGroupHDBU.Enabled := True;
    QRBandFooterBU.Enabled := True;
  end;
  if QRParameters.FShowDept then
  begin
    if LastLevel <> 'D' then
      QRGroupHDDept.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
//  if QRParameters.FShowWK then
//  begin
//    if LastLevel <> 'W' then
    QRGroupHDWK.Enabled := True;
    QRBandFooterWK.Enabled := True;
//  end;
  if QRParameters.FShowJob then
  begin
    if LastLevel <> 'J' then
      QRGroupHDJob.Enabled := True;
    QRBandFooterJob.Enabled := True;
  end;
  if QRParameters.FShowEmpl then
    QRBandFooterEmpl.Enabled := True;
*)
end;

procedure TReportHrsPerWKQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportHrsPerWKQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:06-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  // MR:22-10-2004 Always show workspot! Set it here.
  // MR:06-01-2006 Not used here, can now be set by dialog.
//  FShowWK := True;
end;

procedure TReportHrsPerWKQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  InitializeAmountsPerDaysOfWeek(TotalEmpl);
  InitializeAmountsPerDaysOfWeek(TotalWK);
  InitializeAmountsPerDaysOfWeek(TotalJob);
  InitializeAmountsPerDaysOfWeek(TotalDept);
  InitializeAmountsPerDaysOfWeek(TotalBU);
  InitializeAmountsPerDaysOfWeek(TotalPlant);
  // MR:06-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      // Show Selections
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(QRLabel11.Caption);
        // From Plant to Plant
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelPlantTo.Caption
          );
        // From BU to BU
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelBusinessTo.Caption
          );
        // From Dept to Dept
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelDeptTo.Caption
          );
        // From Jobcode to Jobcode
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelAbsenceRsn.Caption + ' ' +
          QRLabelJobCodeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelJobCodeTo.Caption
          );
        // From Employee to Employee
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelEmployeeTo.Caption
          );
        // From Date to Date
        ExportClass.AddText(
          QRLabelFromDate.Caption + ' ' + { From }
          QRLabelDate.Caption + ' ' + { Date }
          QRLabelDateFrom.Caption + ' ' + QRLabel114.Caption + { To }
          QRLabelDateTo.Caption
          );
      end;
      // Show Details
      ExportDetail(
        QRLabel54.Caption,
        QRLabel55.Caption,
        QRLabel35.Caption,
        QRLabel3.Caption,
        QRLabel6.Caption,
        QRLabel106.Caption,
        QRLabel5.Caption, QRLabel7.Caption, QRLabel8.Caption,
        QRLabel9.Caption, QRLabel10.Caption, QRLabel12.Caption,
        QRLabel14.Caption, QRLabel15.Caption);
    end;
end;

procedure TReportHrsPerWKQR.InitializeAmountsPerDaysOfWeek(
  var AmountsPerDaysOfWeek: TAmountsPerDay);
var
  Counter: Integer;
begin
  for Counter := 1 to 7 do
    AmountsPerDaysOfWeek[Counter] := 0;
end;

procedure TReportHrsPerWKQR.FillAmountsPerDaysOfWeek(
  AmountsPerDaysOfWeek: TAmountsPerDay; PrintDecide: Boolean);
var
  PrintItem: Boolean;
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 1) and (Index <= 7) then
      begin
        PrintItem := True;
        if PrintDecide then
          PrintItem := PrintTotalOnDays[Index];
        if PrintItem then
          (TempComponent as TQRLabel).Caption :=
            DecodeHrsMin(Round(AmountsPerDaysOfWeek[Index]))
        else
          (TempComponent as TQRLabel).Caption := ' '
      end;
    end;
  end;
  // MR:06-12-2002
  for Index := 1 to 7 do
    MyAmountsPerDaysOfWeek[Index] := AmountsPerDaysOfWeek[Index];
end;

procedure TReportHrsPerWKQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 10) and (Index <= 16) then
      begin
        SystemDM.ClientDataSetDay.FindKey([Index - 10 + 1]);
        (TempComponent as TQRLabel).Caption :=
           SystemDM.GetDayWCode(Index - 10 + 1);
      end;
    end;
  end;
end;

procedure TReportHrsPerWKQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
// MR:06-12-2002
var
  JobCode, EmplCode, DeptCode, BUCode: String;
begin
  inherited;
  if (not QRParameters.FShowJob) and
    (not QRParameters.FShowEmpl) and OneWeek then
    PrintBand := False;
//  if LastLevel = 'W'  then
//    QRLabelTotWK.Caption := SRepHrsCUMWK
//  else
//    QRLabelTotWK.Caption := SRepHrsTotalCUM;
  FillAmountsPerDaysOfWeek(TotalWK, False);
  // MR:06-12-2002
  if QRParameters.FShowJob then
    JobCode := ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString
  else
    JobCode := '';
  if QRParameters.FShowEmpl then
    EmplCode :=
      ReportHrsPerWKDM.QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsString
  else
    EmplCode := '';
  if QRParameters.FShowDept then
    DeptCode :=
      ReportHrsPerWKDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString
  else
    DeptCode := '';
  if QRParameters.FShowBU then
    BUCode :=
      ReportHrsPerWKDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString
  else
    BUCode := '';
  // MR:16-02-2005 Order 550377 Don't export the totals
(*
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      ReportHrsPerWKDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
      BUCode,
      DeptCode,
      ReportHrsPerWKDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
      JobCode,
      EmplCode,
      IntToStr(MyAmountsPerDaysOfWeek[1]),
      IntToStr(MyAmountsPerDaysOfWeek[2]), IntToStr(MyAmountsPerDaysOfWeek[3]),
      IntToStr(MyAmountsPerDaysOfWeek[4]), IntToStr(MyAmountsPerDaysOfWeek[5]),
      IntToStr(MyAmountsPerDaysOfWeek[6]), IntToStr(MyAmountsPerDaysOfWeek[7]),
      IntToStr(MyAmountsPerDaysOfWeek[1] +
      MyAmountsPerDaysOfWeek[2] + MyAmountsPerDaysOfWeek[3] +
      MyAmountsPerDaysOfWeek[4] + MyAmountsPerDaysOfWeek[5] +
      MyAmountsPerDaysOfWeek[6] + MyAmountsPerDaysOfWeek[7])
      );
*)
end;

procedure TReportHrsPerWKQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FWorkspot :=
    ReportHrsPerWKDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString;
//  if LastLevel = 'W' then
//    PrintBand := False;
  QRGroupHdWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
end;

procedure TReportHrsPerWKQR.QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FBU :=
    ReportHrsPerWKDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString;
//  if LastLevel = 'B' then
//    PrintBand := False;
  QRGroupHdBU.ForceNewPage := QRParameters.FPageBU;
  inherited;
end;

procedure TReportHrsPerWKQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FPlant := ReportHrsPerWKDM.QueryProdHour.FieldByName('PLANT_CODE').AsString;
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalPlant);
  FillDescDaysPerWeek;
end;

procedure TReportHrsPerWKQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if ( LastLevel = 'B') then
    QRLabelTotBU.Caption := SRepHrsCUMWKBU
  else
    QRLabelTotBU.Caption := SRepHrsTotalCUMBU;
  FillAmountsPerDaysOfWeek(TotalBU, False);
end;

procedure TReportHrsPerWKQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if (LastLevel = 'P') then
    QRLabelTotPlant.Caption := SRepHrsCUMWKPlant
  else
    QRLabelTotPlant.Caption := SRepHrsTotalCUMPlant;
  FillAmountsPerDaysOfWeek(TotalPlant, False);
end;

// RV040. Changed, so it totalizes with doubles, but returns a rounded integer.
function TReportHrsPerWKQR.Summarize(AmountsPerDaysOfWeek:
  TAmountsPerDay): Integer;
var
  Count: Integer;
  Total: Double;
begin
  inherited;
  Total := 0;
  for Count := 1 to 7 do
    Total := Total + Round(AmountsPerDaysOfWeek[Count]);
  Result := Round(Total);
end;

procedure TReportHrsPerWKQR.QRLabelTotalWKPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalWK));
end;

procedure TReportHrsPerWKQR.QRLabelTotalBUPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalBU));
end;

procedure TReportHrsPerWKQR.QRLabelTotalPlantPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalPlant));
end;

procedure TReportHrsPerWKQR.QRDBTextDescBUPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value :=
    Copy(ReportHrsPerWKDM.QueryProdHour.FieldByName('BDESC').AsString,0,15);
end;

procedure TReportHrsPerWKQR.QRDBTextWKDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
//  Value := Copy(ReportHrsPerWKDM.
//    QueryProdHour.FieldByName('WDESC').AsString,0 , 15);
end;

procedure TReportHrsPerWKQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FDept :=
    ReportHrsPerWKDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString;
//  if LastLevel = 'D' then
//    PrintBand := False;
  QRGroupHdDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
end;

procedure TReportHrsPerWKQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if (LASTLevel = 'D') then
    QRLabelTotDept.Caption := SRepHrsCUMWKDept
  else
    QRLabelTotDept.Caption := SRepHrsTotalCUMDept;
  FillAmountsPerDaysOfWeek(TotalDept, False);
end;

procedure TReportHrsPerWKQR.QRLabelDeptTotPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalDept));
end;

procedure TReportHrsPerWKQR.QRDBTextJobDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString =
    DummyStr then
    Value := ''
  else
    Value :=
      Copy(ReportHrsPerWKDM.QueryProdHour.FieldByName('JDESC').AsString,0, 15);
end;

procedure TReportHrsPerWKQR.QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  FJob := ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString;
//  if LastLevel = 'J' then
//    PrintBand := False;
(*
  if ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString =
    DummyStr then
    PrintBand := False;
*)
  if (ReportHrsPerWKDM.
    QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
    (ReportHrsPerWKDM.
    QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
  begin
    PrintBand := False;
    if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
      (not AllBusinessUnits)) then
      PrintBand := True;
  end;
  QRGroupHDJob.ForceNewPage := QRParameters.FPageJob;
  inherited;
end;

procedure TReportHrsPerWKQR.QRBandFooterJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if (not QRParameters.FShowEmpl) and OneWeek then
    PrintBand := False;
(*
  if ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString =
    DummyStr then
    PrintBand := False;
*)
  if PrintBand then
  begin
    if (ReportHrsPerWKDM.
      QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
      (ReportHrsPerWKDM.
      QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
    begin
      PrintBand := False;
      if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
        (not AllBusinessUnits)) then
        PrintBand := True;
    end;
  end;
//  if LastLevel = 'J' then
//    QRLabelTotJob.Caption := 'Job code'
//  else
//    QRLabelTotJob.Caption := 'Total job code';
  FillAmountsPerDaysOfWeek(TotalJob, False);
end;

procedure TReportHrsPerWKQR.QRLabelTotEmplPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalEmpl));
end;

procedure TReportHrsPerWKQR.QRDBTextEmplDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  if ReportHrsPerWKDM.cdsEmpl.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([ReportHrsPerWKDM.QueryProdHour.
    FieldByName('EMPLOYEE_NUMBER').AsInteger]), []) then
      Value := Copy(ReportHrsPerWKDM.cdsEmpl.
        FieldByName('DESCRIPTION').AsString, 0,25);
end;

procedure TReportHrsPerWKQR.QRGroupWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
begin
  inherited;
  PrintBand := False;
  for Count := 1 to 7 do
  begin
    TotalOnDays[Count] := 0;
    TotalOnDaysUnknown[Count] := 0;
    PrintTotalOnDays[Count] := False;
  end;
end;

procedure TReportHrsPerWKQR.QRBandDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Count: Integer;
  ActualDate: TDateTime;
  procedure AddAmounts(Amount: Double; Index: Integer;
    BU, Dept, WK, Job, Empl: Boolean);
  begin
    if BU then
      TotalBU[index] := TotalBU[index] + Amount;
    if Dept then
      TotalDept[index]:= TotalDept[index] + Amount;
    if WK then
      TotalWK[index] := TotalWK[index] + Amount;
    if Job then
      TotalJob[index] := TotalJob[index] + Amount;
    if Empl then
      TotalEmpl[index] := TotalEmpl[index] + Amount;
  end;
  function CheckFixValues: Boolean;
  begin
    Result := True;
    with ReportHrsPerWKDM do
    begin
      if (QRParameters.FShowBU) then
        Result := Result and (FBU =
          QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString);
      if (QRParameters.FShowDept) then
        Result := Result and (FDept =
          QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString);
      if (QRParameters.FShowJob) then
         Result := Result and (FJob =
          QueryProdHour.FieldByName('JOB_CODE').AsString);
      if (QRParameters.FShowEmpl) then
        Result := Result and (FEmpl =
          QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger);
    end;
  end;
  procedure SummarizeAll;
  var
    Index: Integer;
    SumMin: Double;
    Plant, WK, BU: String;
  begin
    Index := ListProcsF.DayWStartOnFromDate(
      ReportHrsPerWKDM.QueryProdHour.
        FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime);

    Plant := ReportHrsPerWKDM.QueryProdHour.FieldByName('PLANT_CODE').AsString;
    WK := ReportHrsPerWKDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString;
    BU := '';
    if QRParameters.FShowBU then
      BU :=
        ReportHrsPerWKDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString;
    // MR:04-10-2005 Make calculation with percentage.
    SumMin := DetermineSUMMIN;
    case LastLevel of
      'B': AddAmounts(SumMin, Index, True, False, False, False, False);
      'D': AddAmounts(SumMin, Index, True, True, False, False, False);
      'W': AddAmounts(SumMin, Index, True, True, True, False, False);
      'J': AddAmounts(SumMin, Index, True, True, True, True, False);
      'E': AddAmounts(SumMin, Index, True, True, True, True, True);
     end;
    TotalPlant[Index] :=  TotalPlant[Index] + SumMin;
  end;
begin
  inherited;
  PrintBand := False;
  // MR:10-01-2005 Only print a total if there was more than 1 week
  // printed.

  // Show day-values for this week
  with ReportHrsPerWKDM do
  begin
    ActualDate :=
      QueryProdHour.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
    Count := ListProcsF.DayWStartOnFromDate(ActualDate);
    // MR:11-11-2005 If LISTCODE = 3 (UNKNOWN BUSINESS UNIT)
    // and if ONE PLANT and NOT ALL BUSINESSUNITS selected,
    // save the result in 'unknown'.
    if (QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) and
      ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
       (not AllBusinessUnits)) then
      TotalOnDaysUnknown[Count] := DetermineSUMMIN
    else
      TotalOnDays[Count] := DetermineSUMMIN;
    // This determines if a value should be printed in the report,
    // or left blank.
    if not PrintTotalOnDays[Count] then
      PrintTotalOnDays[Count] := ((ActualDate >= QRParameters.FDateFrom) and
        (ActualDate <= QRParameters.FDateTo));
    SummarizeAll;
  end;
(*
    QueryProdHour.Next;
    ProgressIndicatorPos := ProgressIndicatorPos + 1;
    while not (QueryProdHour.Eof) and CheckFixValues and
     (QueryProdHour.FieldByName('WORKSPOT_CODE').AsString = ActualWorkspot) do
    begin
      ActualDate :=
        QueryProdHour.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
      ListProcsF.WeekUitDat(ActualDate, Year, Week);
      if ((ActualYear = Year) and (Week = ActualWeek)) then
      begin
        Count := ListProcsF.DayWStartOnFromDate(ActualDate);
        TotalOnDays[Count] :=
          DetermineSUMMIN(QueryProdHour.FieldByName('SUMMIN').AsInteger);
        SummarizeAll;
        QueryProdHour.Next;
        ProgressIndicatorPos := ProgressIndicatorPos + 1;
      end
      else
        Break;
    end;{while}
*)
(*
    // print one week
    for Count := 1 to 7 do
    begin
      // MR:10-01-2005 Use 'ActualYear' instead of 'Year' to prevent
      // problem during year-intervals.
      ActualDate := ListProcsF.DateFromWeek(ActualYear, ActualWeek, Count);
      PrintTotalOnDays[Count] := ((ActualDate >= QRParameters.FDateFrom) and
        (ActualDate <= QRParameters.FDateTo));
    end;
*)
(*
    PrintDetailWeek(ActualWeek);
    if not QueryProdHour.Eof then
      QueryProdHour.Prior;
  end;
*)
end;

procedure TReportHrsPerWKQR.QRBandWeekNumberFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalDetail: Integer;
begin
  inherited;
  // print one week
  TotalDetail := Summarize(TotalOnDays);
  PrintBand := QRParameters.FShowWK;
  if PrintBand then
    PrintBand := (TotalDetail <> 0);
  if PrintBand then
  begin
    QRLabelWeek.Caption :=
      IntToStr(
        ReportHrsPerWKDM.QueryProdHour.FieldByName('WEEKNUMBER').AsInteger);
    FillAmountsPerDaysOfWeek(TotalOnDays, True);
    QRLabelDetailTot.Caption := DecodeHrsMin(TotalDetail);

    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(FPlant, FBU, FDept, FWorkspot, FJob, IntToStr(FEmpl),
        IntToStr(Round(TotalOnDays[1])),
        IntToStr(Round(TotalOnDays[2])),
        IntToStr(Round(TotalOnDays[3])),
        IntToStr(Round(TotalOnDays[4])),
        IntToStr(Round(TotalOnDays[5])),
        IntToStr(Round(TotalOnDays[6])),
        IntToStr(Round(TotalOnDays[7])),
        IntToStr(TotalDetail));
  end;
end;

// MR:11-11-2005 Show Unknown Business Unit for this week.
procedure TReportHrsPerWKQR.ChildBandUnknownBusinessUnitBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalDetail: Integer;
begin
  inherited;
  // print one week if there is 'unknown business unit'.
  TotalDetail := Summarize(TotalOnDaysUnknown);
  PrintBand := (TotalDetail <> 0);
  if PrintBand then
  begin
    QRLabelWeekUN.Caption := IntToStr(
      ReportHrsPerWKDM.QueryProdHour.FieldByName('WEEKNUMBER').AsInteger);
    FillAmountsPerDaysOfWeek(TotalOnDaysUnknown, True);
    QRLabelDetailTotUN.Caption := DecodeHrsMin(TotalDetail);
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(FPlant, FBU, FDept, FWorkspot, FJob, IntToStr(FEmpl),
        IntToStr(Round(TotalOnDaysUnknown[1])),
        IntToStr(Round(TotalOnDaysUnknown[2])),
        IntToStr(Round(TotalOnDaysUnknown[3])),
        IntToStr(Round(TotalOnDaysUnknown[4])),
        IntToStr(Round(TotalOnDaysUnknown[5])),
        IntToStr(Round(TotalOnDaysUnknown[6])),
        IntToStr(Round(TotalOnDaysUnknown[7])),
        IntToStr(TotalDetail));
  end;
end;

procedure TReportHrsPerWKQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if OneWeek then
    PrintBand := False;
//  if LevelJob then
//    if ReportHrsPerWKDM.QueryProdHour.FieldByName('JOB_CODE').AsString =
//      DummyStr then
//      PrintBand := False;
  FillAmountsPerDaysOfWeek(TotalEmpl, False);
end;

procedure TReportHrsPerWKQR.QRDBTextDescDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value :=
    Copy(ReportHrsPerWKDM.QueryProdHour.FieldByName('DDESC').AsString,0,15);
end;

procedure TReportHrsPerWKQR.QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalBU);
end;

procedure TReportHrsPerWKQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalDept);
end;

procedure TReportHrsPerWKQR.QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalWK);
end;

procedure TReportHrsPerWKQR.QRBandFooterJOBAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalJob);
end;

procedure TReportHrsPerWKQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalEmpl);
end;

procedure TReportHrsPerWKQR.QRLabelJobTotPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalJob));
end;

procedure TReportHrsPerWKQR.ChildBandWKDummyBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportHrsPerWKQR.QRDBTextJobCodePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if Value = DummyStr then
    Value := '';
end;

procedure TReportHrsPerWKQR.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  FillDescDaysPerWeek;
end;

procedure TReportHrsPerWKQR.QRBandSummaryAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  qrptBase.QRPrinter.Progress := 100; // MR:10-01-2005 Force to 100 %
  // MR:06-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHrsPerWKQR.QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  FEmpl :=
    ReportHrsPerWKDM.QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

end.
