inherited HoursPerEmployeeDM: THoursPerEmployeeDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 254
  Top = 111
  Height = 704
  Width = 927
  inherited TableMaster: TTable
    Left = 53
    Top = 12
    object TableMasterEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableMasterSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableMasterDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableMasterLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableMasterPHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableMasterEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableMasterDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableMasterSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableMasterSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableMasterTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableMasterENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableMasterIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableMasterCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableMasterREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableMasterCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Lookup = True
    end
  end
  inherited TableDetail: TTable
    DatabaseName = 'Pims'
    Filtered = True
    MasterSource = nil
    TableName = 'HOURTYPE'
    Left = 56
    Top = 76
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailBONUS_PERCENTAGE: TFloatField
      FieldName = 'BONUS_PERCENTAGE'
    end
    object TableDetailOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
  end
  inherited DataSourceMaster: TDataSource
    DataSet = cdsFilterEmployee
    Left = 148
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Left = 148
    Top = 76
  end
  inherited TableExport: TTable
    Left = 324
    Top = 564
  end
  inherited DataSourceExport: TDataSource
    Left = 416
    Top = 564
  end
  object TablePlant: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceMaster
    TableName = 'PLANT'
    Left = 328
    Top = 20
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 428
    Top = 20
  end
  object DataSourceShift: TDataSource
    DataSet = QueryShiftLU
    Left = 428
    Top = 76
  end
  object DataSourceWorkspotLU: TDataSource
    DataSet = QueryWorkspotLU
    Left = 428
    Top = 128
  end
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      '')
    Left = 540
    Top = 72
  end
  object DataSourceJob: TDataSource
    DataSet = QueryJobLU
    Left = 428
    Top = 185
  end
  object TableAbsenceType: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCEREASON_CODE'
    TableName = 'ABSENCEREASON'
    Left = 540
    Top = 260
    object TableAbsenceTypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceTypeHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
  end
  object DataSourceHREPRODUCTION: TDataSource
    DataSet = cdsHREPRODUCTION
    Left = 204
    Top = 164
  end
  object DataSourceHRESALARY: TDataSource
    DataSet = cdsHRESALARY
    Left = 204
    Top = 216
  end
  object DataSourceHREABSENCE: TDataSource
    DataSet = cdsHREABSENCE
    Left = 204
    Top = 268
  end
  object QueryDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 540
    Top = 16
  end
  object QueryCheckIfExist: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 536
    Top = 128
  end
  object TableWK: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    TableName = 'WORKSPOT'
    Left = 561
    Top = 444
    object StringField6: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWKPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWKDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWKWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
  end
  object TableTmpJob: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'JOBCODE'
    Left = 563
    Top = 388
    object TableTmpJobDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableTmpJobPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableTmpJobWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableTmpJobJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 536
    Top = 184
  end
  object cdsHRESALARY: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'HOURTYPE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'MANUAL_YN'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'DAY1'
        DataType = ftDateTime
      end
      item
        Name = 'DAY2'
        DataType = ftDateTime
      end
      item
        Name = 'DAY3'
        DataType = ftDateTime
      end
      item
        Name = 'DAY4'
        DataType = ftDateTime
      end
      item
        Name = 'DAY5'
        DataType = ftDateTime
      end
      item
        Name = 'DAY6'
        DataType = ftDateTime
      end
      item
        Name = 'DAY7'
        DataType = ftDateTime
      end
      item
        Name = 'TOTAL'
        DataType = ftInteger
      end
      item
        Name = 'CREATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'MUTATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'MUTATOR'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY1'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY3'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY4'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY5'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY6'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SIGNDAY7'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'FROMMANUALPROD_YN'
        DataType = ftString
        Size = 20
      end>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'HOURTYPE_NUMBER;MANUAL_YN'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDeleteRecord
    AfterDelete = DefaultAfterDelete
    AfterScroll = DefaultAfterScroll
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = cdsHRESALARYNewRecord
    OnPostError = DefaultPostError
    Left = 64
    Top = 216
    object cdsHRESALARYHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object cdsHRESALARYMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
    end
    object cdsHRESALARYDAY1: TDateTimeField
      FieldName = 'DAY1'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY2: TDateTimeField
      FieldName = 'DAY2'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY3: TDateTimeField
      FieldName = 'DAY3'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY4: TDateTimeField
      FieldName = 'DAY4'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY5: TDateTimeField
      FieldName = 'DAY5'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY6: TDateTimeField
      FieldName = 'DAY6'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYDAY7: TDateTimeField
      FieldName = 'DAY7'
      DisplayFormat = 'hh:mm'
    end
    object cdsHRESALARYTOTAL: TIntegerField
      FieldName = 'TOTAL'
    end
    object cdsHRESALARYCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object cdsHRESALARYMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object cdsHRESALARYMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object cdsHRESALARYSIGNDAY1: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY1'
    end
    object cdsHRESALARYSIGNDAY2: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY2'
    end
    object cdsHRESALARYSIGNDAY3: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY3'
    end
    object cdsHRESALARYSIGNDAY4: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY4'
    end
    object cdsHRESALARYSIGNDAY5: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY5'
    end
    object cdsHRESALARYSIGNDAY6: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY6'
    end
    object cdsHRESALARYSIGNDAY7: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY7'
    end
    object cdsHRESALARYHOURTYPELU: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = QueryHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'HDESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 30
      Lookup = True
    end
    object cdsHRESALARYFROMMANUALPROD_YN: TStringField
      FieldName = 'FROMMANUALPROD_YN'
    end
    object cdsHRESALARYHOURTYPESHOW: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'HOURTYPESHOW'
      LookupDataSet = QueryHourTypeLU
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'HDESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 30
      Lookup = True
    end
  end
  object cdsHREABSENCE: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'ABSENCEREASON_CODE;MANUAL_YN'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDeleteRecord
    AfterDelete = DefaultAfterDelete
    AfterScroll = DefaultAfterScroll
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = cdsHREABSENCENewRecord
    OnPostError = DefaultPostError
    Left = 64
    Top = 272
    object cdsHREABSENCEABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
    end
    object cdsHREABSENCEMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
    end
    object cdsHREABSENCEDAY1: TDateTimeField
      FieldName = 'DAY1'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY2: TDateTimeField
      FieldName = 'DAY2'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY3: TDateTimeField
      FieldName = 'DAY3'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY4: TDateTimeField
      FieldName = 'DAY4'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY5: TDateTimeField
      FieldName = 'DAY5'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY6: TDateTimeField
      FieldName = 'DAY6'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCEDAY7: TDateTimeField
      FieldName = 'DAY7'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREABSENCETOTAL: TIntegerField
      DefaultExpression = '0'
      FieldName = 'TOTAL'
    end
    object cdsHREABSENCECREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object cdsHREABSENCEMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object cdsHREABSENCEMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object cdsHREABSENCESIGNDAY1: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY1'
    end
    object cdsHREABSENCESIGNDAY2: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY2'
    end
    object cdsHREABSENCESIGNDAY3: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY3'
    end
    object cdsHREABSENCESIGNDAY4: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY4'
    end
    object cdsHREABSENCESIGNDAY5: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY5'
    end
    object cdsHREABSENCESIGNDAY6: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY6'
    end
    object cdsHREABSENCESIGNDAY7: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY7'
    end
    object cdsHREABSENCEABSENCEREASONLU: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'ABSENCEREASONLU'
      LookupDataSet = QueryAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCEREASON_CODE'
      Size = 30
      Lookup = True
    end
  end
  object cdsHREPRODUCTION: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'PLANT_CODE;SHIFT_NUMBER;WORKSPOT_CODE;JOB_CODE;MANUAL_YN'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    BeforeEdit = cdsHREPRODUCTIONBeforeEdit
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDeleteRecord
    AfterDelete = DefaultAfterDelete
    AfterScroll = DefaultAfterScroll
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = cdsHREPRODUCTIONNewRecord
    OnPostError = DefaultPostError
    Left = 64
    Top = 160
    object cdsHREPRODUCTIONPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      KeyFields = 'PLANT_CODE'
      OnChange = cdsHREPRODUCTIONPLANT_CODEChange
      Size = 6
    end
    object cdsHREPRODUCTIONSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsHREPRODUCTIONWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      OnChange = cdsHREPRODUCTIONWORKSPOT_CODEChange
      Size = 6
    end
    object cdsHREPRODUCTIONJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object cdsHREPRODUCTIONDAY1: TDateTimeField
      FieldName = 'DAY1'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY2: TDateTimeField
      FieldName = 'DAY2'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY3: TDateTimeField
      FieldName = 'DAY3'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY4: TDateTimeField
      FieldName = 'DAY4'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY5: TDateTimeField
      FieldName = 'DAY5'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY6: TDateTimeField
      FieldName = 'DAY6'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONDAY7: TDateTimeField
      FieldName = 'DAY7'
      DisplayFormat = 'hh:mm'
    end
    object cdsHREPRODUCTIONTOTAL: TIntegerField
      FieldName = 'TOTAL'
    end
    object cdsHREPRODUCTIONMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
    end
    object cdsHREPRODUCTIONCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object cdsHREPRODUCTIONMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object cdsHREPRODUCTIONMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object cdsHREPRODUCTIONSIGNDAY1: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY1'
    end
    object cdsHREPRODUCTIONSIGNDAY2: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY2'
    end
    object cdsHREPRODUCTIONSIGNDAY3: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY3'
    end
    object cdsHREPRODUCTIONSIGNDAY4: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY4'
    end
    object cdsHREPRODUCTIONSIGNDAY5: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY5'
    end
    object cdsHREPRODUCTIONSIGNDAY6: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY6'
    end
    object cdsHREPRODUCTIONSIGNDAY7: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'SIGNDAY7'
    end
    object cdsHREPRODUCTIONPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlantLU
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Lookup = True
    end
    object cdsHREPRODUCTIONSHIFTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFTLU'
      LookupDataSet = TableShift
      LookupKeyFields = 'PLANT_CODE;SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;SHIFT_NUMBER'
      Lookup = True
    end
    object cdsHREPRODUCTIONWORKSPOTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOTLU'
      LookupDataSet = TableWorkspotLU
      LookupKeyFields = 'PLANT_CODE;WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;WORKSPOT_CODE'
      Lookup = True
    end
    object cdsHREPRODUCTIONJOBLU: TStringField
      FieldKind = fkLookup
      FieldName = 'JOBLU'
      LookupDataSet = TableJob
      LookupKeyFields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
      Lookup = True
    end
  end
  object DataSourceAbsenceReason: TDataSource
    DataSet = QueryAbsenceReason
    Left = 424
    Top = 368
  end
  object DataSourceHourtype: TDataSource
    DataSet = QueryHourType
    Left = 424
    Top = 240
  end
  object TableShift: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'SHIFT'
    Left = 64
    Top = 384
  end
  object TableJob: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'JOBCODE'
    Left = 64
    Top = 432
  end
  object QueryWorkspotLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlantLU
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, DESCRIPTION,'
      '  USE_JOBCODE_YN, DEPARTMENT_CODE'
      'FROM '
      '  WORKSPOT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  WORKSPOT_CODE')
    Left = 328
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkspotLU
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, DESCRIPTION'
      'FROM '
      '  JOBCODE'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE'
      'ORDER BY '
      '  JOB_CODE')
    Left = 328
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryShiftLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlant
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION'
      'FROM '
      '  SHIFT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  SHIFT_NUMBER')
    Left = 328
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryEmployee: TQuery
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME,'
      '  E.DESCRIPTION, E.PLANT_CODE,'
      '  P.DESCRIPTION AS PLANTLU, E.STARTDATE, E.ENDDATE,'
      '  E.CUT_OF_TIME_YN, E.DEPARTMENT_CODE,'
      '  E.IS_SCANNING_YN, E.BOOK_PROD_HRS_YN,'
      '  E.CONTRACTGROUP_CODE,'
      '  NVL(E.SHIFT_NUMBER,'
      
        '    NVL((SELECT MIN(S.SHIFT_NUMBER) FROM SHIFT S WHERE S.PLANT_C' +
        'ODE = E.PLANT_CODE),'
      
        '      (SELECT MIN(S.SHIFT_NUMBER) FROM SHIFT S))) AS SHIFT_NUMBE' +
        'R'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 240
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object TableWorkspot: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = TableWorkspotFilterRecord
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceHREPRODUCTION
    TableName = 'WORKSPOT'
    Left = 64
    Top = 488
    object TableWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object DataSourceWorkspot: TDataSource
    DataSet = TableWorkspot
    Left = 156
    Top = 488
  end
  object TableWorkspotLU: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 64
    Top = 336
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField4: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField5: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField7: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField8: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object StringField9: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object StringField11: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object StringField12: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object StringField13: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object QueryMaxSalaryBalance: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '   EMPLOYEE_NUMBER, MAX_SALARY_BALANCE_MINUTES'
      'FROM'
      '  MAXSALARYBALANCE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 328
    Top = 416
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryMaxSalaryBalanceUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE MAXSALARYBALANCE'
      'SET MAX_SALARY_BALANCE_MINUTES = :MAX_SALARY_BALANCE_MINUTES,'
      'MUTATIONDATE = :MUTATIONDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 328
    Top = 464
    ParamData = <
      item
        DataType = ftInteger
        Name = 'MAX_SALARY_BALANCE_MINUTES'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object TablePlantLU: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 152
    Top = 336
    object TablePlantLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object DataSourcePlantLU: TDataSource
    DataSet = TablePlantLU
    Left = 240
    Top = 336
  end
  object QueryMaxSalaryBalanceInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO MAXSALARYBALANCE'
      '(EMPLOYEE_NUMBER, MAX_SALARY_BALANCE_MINUTES,'
      'CREATIONDATE, MUTATIONDATE, MUTATOR)'
      'VALUES(:EMPLOYEE_NUMBER, :MAX_SALARY_BALANCE_MINUTES, '
      ':CREATIONDATE, :MUTATIONDATE, :MUTATOR)'
      ' ')
    Left = 328
    Top = 512
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MAX_SALARY_BALANCE_MINUTES'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryWorkspotJobByEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      
        '  NVL(E.SHIFT_NUMBER, NVL((SELECT MIN(S.SHIFT_NUMBER) FROM SHIFT' +
        ' S), 1)) SHIFT_NUMBER,'
      '  NVL((SELECT MIN(W.WORKSPOT_CODE) FROM WORKSPOT W'
      
        '       WHERE W.PLANT_CODE = E.PLANT_CODE AND W.DEPARTMENT_CODE =' +
        ' E.DEPARTMENT_CODE),'
      
        '      (SELECT MIN(W2.WORKSPOT_CODE) FROM WORKSPOT W2 WHERE W2.PL' +
        'ANT_CODE = E.PLANT_CODE)) WORKSPOT_CODE,'
      
        '  NVL((SELECT MIN(J.JOB_CODE) FROM JOBCODE J WHERE J.PLANT_CODE ' +
        '= E.PLANT_CODE AND J.WORKSPOT_CODE = '
      '      (NVL((SELECT MIN(W.WORKSPOT_CODE) FROM WORKSPOT W '
      
        '           WHERE W.PLANT_CODE = E.PLANT_CODE AND W.DEPARTMENT_CO' +
        'DE = E.DEPARTMENT_CODE),'
      
        '           (SELECT MIN(W2.WORKSPOT_CODE) FROM WORKSPOT W2 WHERE ' +
        'W2.PLANT_CODE = E.PLANT_CODE)))),0) JOB_CODE'
      'FROM '
      '  EMPLOYEE E'
      'WHERE'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ' ')
    Left = 560
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryHourType: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  H.*, H.DESCRIPTION HDESCRIPTION'
      'FROM '
      '  HOURTYPE H'
      'ORDER BY '
      '  H.HOURTYPE_NUMBER')
    Left = 328
    Top = 240
  end
  object QueryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  *'
      'FROM '
      '  ABSENCEREASON')
    Left = 328
    Top = 352
  end
  object QueryHourTypeLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  H.*, H.DESCRIPTION HDESCRIPTION'
      'FROM '
      '  HOURTYPE H'
      'ORDER BY '
      '  H.HOURTYPE_NUMBER')
    Left = 328
    Top = 296
  end
  object DataSourceHourTypeLU: TDataSource
    DataSet = QueryHourTypeLU
    Left = 424
    Top = 296
  end
  object qryAbsenceTypePerCountryExist: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.COUNTRY_ID'
      'FROM ABSENCETYPEPERCOUNTRY T'
      'WHERE T.COUNTRY_ID = :COUNTRY_ID'
      '')
    Left = 560
    Top = 552
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
  object qryHourtypeCheck: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT H.SATURDAY_CREDIT_YN, H.TRAVELTIME_YN'
      'FROM HOURTYPE H'
      'WHERE H.HOURTYPE_NUMBER = :HOURTYPE_NUMBER'
      ''
      ' '
      ' ')
    Left = 64
    Top = 552
    ParamData = <
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotalSatCred: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT NVL(A.EARNED_SAT_CREDIT_MINUTE,0) EARNED_SAT_CREDIT_MINUT' +
        'E'
      'FROM ABSENCETOTAL A'
      'WHERE A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'A.ABSENCE_YEAR = :ABSENCE_YEAR'
      '')
    Left = 176
    Top = 552
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryPHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT SUM(T.PRODUCTION_MINUTE) PRODMIN'
      'FROM PRODHOURPEREMPLOYEE T '
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
      'AND T.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE'
      'AND T.MANUAL_YN = :MANUAL_YN'
      ''
      ' ')
    Left = 660
    Top = 392
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryPHEByEmpDate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE, P.SHIFT_NUMBER, P.WORKSPOT_CODE, P.JOB_CODE'
      'FROM'
      '  PRODHOURPEREMPLOYEE P'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE'
      ''
      ' ')
    Left = 672
    Top = 496
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPHEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT SUM(T.PRODUCTION_MINUTE) PRODMIN'
      'FROM PRODHOURPEREMPLPERTYPE T '
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
      'AND T.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE'
      'AND T.MANUAL_YN = :MANUAL_YN'
      ''
      ' '
      ' ')
    Left = 660
    Top = 344
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryPHESHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT PHE.PRODHOUREMPLOYEE_DATE, PHE.PLANT_CODE, PHE.SHIFT_NUMB' +
        'ER, '
      
        'PHE.EMPLOYEE_NUMBER, PHE.WORKSPOT_CODE, PHE.JOB_CODE, PHE.PRODUC' +
        'TION_MINUTE,'
      'PHE.MANUAL_YN, PHE.PAYED_BREAK_MINUTE, '
      
        '(SELECT NVL(MIN(SHE.HOURTYPE_NUMBER), 1) FROM SALARYHOURPEREMPLO' +
        'YEE SHE '
      ' WHERE SHE.SALARY_DATE = PHE.PRODHOUREMPLOYEE_DATE AND '
      ' SHE.EMPLOYEE_NUMBER = PHE.EMPLOYEE_NUMBER AND'
      ' SHE.MANUAL_YN = PHE.MANUAL_YN) HOURTYPE_NUMBER'
      'FROM PRODHOURPEREMPLOYEE PHE '
      'WHERE PHE.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND'
      'PHE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'PHE.MANUAL_YN = :MANUAL_YN'
      ''
      ' '
      ' ')
    Left = 660
    Top = 288
    ParamData = <
      item
        DataType = ftDate
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object cdsFilterEmployee: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SHORT_NAME'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PLANTLU'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PLANT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DEPARTMENT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'CUT_OF_TIME_YN'
        DataType = ftString
        Size = 1
      end
      item
        Name = 'TEAM_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'ENDDATE'
        DataType = ftDateTime
      end
      item
        Name = 'EMP_PLANT_IN_TEAM'
        Attributes = [faFixed]
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspFilterEmployee'
    StoreDefs = True
    OnFilterRecord = cdsFilterEmployeeFilterRecord
    Left = 240
    Top = 112
  end
  object dspFilterEmployee: TDataSetProvider
    DataSet = QueryEmployee
    Constraints = True
    Left = 240
    Top = 64
  end
end
