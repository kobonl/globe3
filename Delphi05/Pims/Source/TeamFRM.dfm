inherited TeamF: TTeamF
  Left = 253
  Top = 120
  Width = 661
  Height = 476
  Caption = 'Teams'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 645
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 643
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 643
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
        end>
      KeyField = 'PLANT_CODE'
      DataSource = TeamDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnCity: TdxDBGridColumn
        Caption = 'City'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxMasterGridColumnState: TdxDBGridColumn
        Caption = 'State'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STATE'
      end
      object dxMasterGridColumnPhone: TdxDBGridColumn
        Caption = 'Phone'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 317
    Width = 645
    Height = 121
    object Label1: TLabel
      Left = 48
      Top = 16
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object Label3: TLabel
      Left = 48
      Top = 48
      Width = 57
      Height = 13
      Caption = 'Responsible'
    end
    object DBEditDescription: TDBEdit
      Tag = 1
      Left = 211
      Top = 11
      Width = 230
      Height = 19
      Ctl3D = False
      DataField = 'DESCRIPTION'
      DataSource = TeamDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 1
    end
    object DBEditCode: TDBEdit
      Tag = 1
      Left = 123
      Top = 11
      Width = 78
      Height = 19
      Ctl3D = False
      DataField = 'TEAM_CODE'
      DataSource = TeamDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 0
    end
    object dxDBLookupEdit1: TdxDBLookupEdit
      Left = 123
      Top = 40
      Width = 318
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'EMPLOYEELU'
      DataSource = TeamDM.DataSourceDetail
      DropDownRows = 6
      ListFieldName = 'DESCRIPTION;EMPLOYEE_NUMBER'
      CanDeleteText = True
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 645
    Height = 162
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 156
      Width = 643
      Height = 5
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 643
      Height = 155
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Teams'
        end>
      KeyField = 'TEAM_CODE'
      DataSource = TeamDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 163
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TEAM_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 324
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnEmployee: TdxDBGridLookupColumn
        Caption = 'Employee'
        Width = 232
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEELU'
        ListFieldName = 'DESCRIPTION;EMPLOYEE_NUMBER'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dsrcActive: TDataSource
    Top = 108
  end
end
