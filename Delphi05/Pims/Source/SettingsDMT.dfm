inherited SettingsDM: TSettingsDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 650
  Width = 837
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    OnNewRecord = nil
    TableName = 'WORKSTATION'
    Top = 132
    object TableDetailCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 128
    end
    object TableDetailLICENSEVERSION: TIntegerField
      FieldName = 'LICENSEVERSION'
      Required = True
    end
    object TableDetailTIMERECORDING_YN: TStringField
      FieldName = 'TIMERECORDING_YN'
      Size = 1
    end
    object TableDetailSHOW_HOUR_YN: TStringField
      FieldName = 'SHOW_HOUR_YN'
      Required = True
      Size = 1
    end
    object TableDetailHIDE_ID_YN: TStringField
      FieldName = 'HIDE_ID_YN'
      Size = 4
    end
    object TableDetailCARDREADER_DELAY: TFloatField
      FieldName = 'CARDREADER_DELAY'
    end
    object TableDetailMANREGS_MULTJOBS_YN: TStringField
      FieldName = 'MANREGS_MULTJOBS_YN'
      Size = 4
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 24
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = qryPlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailSHOW_ONLY_DEFJOB_YN: TStringField
      FieldName = 'SHOW_ONLY_DEFJOB_YN'
      Size = 4
    end
  end
  object StoredProcCleanup_PQ: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_PQ'
    Left = 96
    Top = 192
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_ABSLOGAHE: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_ABSLOGAHE'
    Left = 264
    Top = 192
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_ABSLOGPHE: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_ABSLOGPHE'
    Left = 264
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_ABSLOGPHEPT: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_ABSLOGPHEPT'
    Left = 272
    Top = 304
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_ABSLOGSHE: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_ABSLOGSHE'
    Left = 272
    Top = 360
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_PHE: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_PHE'
    Left = 96
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_ABSLOGTRS: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_ABSLOGTRS'
    Left = 272
    Top = 416
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_PHEPT: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_PHEPT'
    Left = 96
    Top = 304
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_SHE: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_SHE'
    Left = 96
    Top = 360
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object StoredProcCleanup_TRS: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'CLEANUP_TRS'
    Left = 96
    Top = 416
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptInput
      end>
  end
  object qryPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT PLANT_CODE, DESCRIPTION'
      'FROM PLANT')
    Left = 400
    Top = 128
  end
end
