inherited TypeOfHoursDM: TTypeOfHoursDM
  inherited TableMaster: TTable
    TableName = 'HOURTYPE'
    object TableMasterHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterBONUS_PERCENTAGE: TIntegerField
      FieldName = 'BONUS_PERCENTAGE'
    end
    object TableMasterOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableMasterEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
  end
end
