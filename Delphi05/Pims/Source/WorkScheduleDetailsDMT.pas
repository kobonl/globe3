(*
  MRA:3-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:13-JUL-2018 GLOB3-60
  - Related to this order.
  - Changes made to show list of shifts in correct numerical sorting-order.
*)
unit WorkScheduleDetailsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, SystemDMT, DBClient, Provider;

type
  TWorkScheduleDetailsDM = class(TGridBaseDM)
    TableMasterCODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterREPEATS: TFloatField;
    TableMasterSTARTDATE: TDateTimeField;
    TableDetailWORKSCHEDULE_ID: TFloatField;
    TableDetailWORKSCHEDULELINE_ID: TFloatField;
    TableDetailDAY1_ABSRSN_CODE: TStringField;
    TableDetailDAY1_SHIFT_NUMBER: TFloatField;
    TableDetailDAY1_TYPE: TFloatField;
    TableDetailDAY1_REPEATS: TFloatField;
    TableDetailDAY2_ABSRSN_CODE: TStringField;
    TableDetailDAY2_SHIFT_NUMBER: TFloatField;
    TableDetailDAY2_TYPE: TFloatField;
    TableDetailDAY2_REPEATS: TFloatField;
    TableDetailDAY3_ABSRSN_CODE: TStringField;
    TableDetailDAY3_SHIFT_NUMBER: TFloatField;
    TableDetailDAY3_TYPE: TFloatField;
    TableDetailDAY3_REPEATS: TFloatField;
    TableDetailDAY4_ABSRSN_CODE: TStringField;
    TableDetailDAY4_SHIFT_NUMBER: TFloatField;
    TableDetailDAY4_TYPE: TFloatField;
    TableDetailDAY4_REPEATS: TFloatField;
    TableDetailDAY5_ABSRSN_CODE: TStringField;
    TableDetailDAY5_SHIFT_NUMBER: TFloatField;
    TableDetailDAY5_TYPE: TFloatField;
    TableDetailDAY5_REPEATS: TFloatField;
    TableDetailDAY6_ABSRSN_CODE: TStringField;
    TableDetailDAY6_SHIFT_NUMBER: TFloatField;
    TableDetailDAY6_TYPE: TFloatField;
    TableDetailDAY6_REPEATS: TFloatField;
    TableDetailDAY7_ABSRSN_CODE: TStringField;
    TableDetailDAY7_SHIFT_NUMBER: TFloatField;
    TableDetailDAY7_TYPE: TFloatField;
    TableDetailDAY7_REPEATS: TFloatField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterCALCREFERENCEWEEK: TStringField;
    TableMasterWORKSCHEDULE_ID: TFloatField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    qryDetails: TQuery;
    TableDetailMYROW: TIntegerField;
    qryAbsRsn: TQuery;
    qryShift: TQuery;
    dspAbsRsn: TDataSetProvider;
    cdsAbsRsn: TClientDataSet;
    dspShift: TDataSetProvider;
    cdsShift: TClientDataSet;
    TableDetailDAY1_SHIFT: TStringField;
    TableDetailDAY2_SHIFT: TStringField;
    TableDetailDAY3_SHIFT: TStringField;
    TableDetailDAY4_SHIFT: TStringField;
    TableDetailDAY5_SHIFT: TStringField;
    TableDetailDAY6_SHIFT: TStringField;
    TableDetailDAY7_SHIFT: TStringField;
    procedure TableMasterCalcFields(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WorkScheduleDetailsDM: TWorkScheduleDetailsDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM;

procedure TWorkScheduleDetailsDM.TableMasterCalcFields(DataSet: TDataSet);
var
  Year, Week: Word;
begin
  inherited;
  try
    ListProcsF.WeekUitDat(TableMasterSTARTDATE.Value, Year, Week);
    TableMasterCALCREFERENCEWEEK.Value := IntToStr(Week) + ';' + IntToStr(Year);
  except
    TableMasterCALCREFERENCEWEEK.Value := '';
  end;
end;

procedure TWorkScheduleDetailsDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  try
    if qryDetails.Locate('WORKSCHEDULELINE_ID',
      TableDetailWORKSCHEDULELINE_ID.AsInteger, []) then
      TableDetailMYROW.AsInteger := qryDetails.FieldByName('ROWNUM').AsInteger;
    if not (DataSet.State in [dsEdit]) then
    begin
      if TableDetailDAY1_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY1_SHIFT.Value := '-'
      else
        TableDetailDAY1_SHIFT.Value := TableDetailDAY1_SHIFT_NUMBER.AsString;
      if TableDetailDAY2_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY2_SHIFT.Value := '-'
      else
        TableDetailDAY2_SHIFT.Value := TableDetailDAY2_SHIFT_NUMBER.AsString;
      if TableDetailDAY3_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY3_SHIFT.Value := '-'
      else
        TableDetailDAY3_SHIFT.Value := TableDetailDAY3_SHIFT_NUMBER.AsString;
      if TableDetailDAY4_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY4_SHIFT.Value := '-'
      else
        TableDetailDAY4_SHIFT.Value := TableDetailDAY4_SHIFT_NUMBER.AsString;
      if TableDetailDAY5_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY5_SHIFT.Value := '-'
      else
        TableDetailDAY5_SHIFT.Value := TableDetailDAY5_SHIFT_NUMBER.AsString;
      if TableDetailDAY6_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY6_SHIFT.Value := '-'
      else
        TableDetailDAY6_SHIFT.Value := TableDetailDAY6_SHIFT_NUMBER.AsString;
      if TableDetailDAY7_SHIFT_NUMBER.AsString = '-1' then
        TableDetailDAY7_SHIFT.Value := '-'
      else
        TableDetailDAY7_SHIFT.Value := TableDetailDAY7_SHIFT_NUMBER.AsString;
    end;
  except
  end;
end;

procedure TWorkScheduleDetailsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  qryDetails.Open;
  qryAbsRsn.Open;
  cdsAbsRsn.Open;
  qryShift.Open;
  cdsShift.Open;
end;

procedure TWorkScheduleDetailsDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  cdsShift.Close;
  qryShift.Close;
  cdsAbsRsn.Close;
  qryAbsRsn.Close;
  qryDetails.Close;
end;

end.
