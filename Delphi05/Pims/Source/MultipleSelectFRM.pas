(*
  SO:08-JUL-2010 RV067.4. 550497
  MRA:24-AUG-2010. RV067.MRA.8. 550497.
  MRA:31-OCT-2012 20013551
  - Extra actions added to keep track of overlapping shifts, when
    entering workspot-shifts.
  MRA:8-NOV-2012 20013551 - Rework
  - For check overlapping shifts: Use 4 parameters to check for overlap, this
    means there can a maximum of 4 non-overlapping shifts.
  MRA:24-JUN-2015 20014450.50
  - Real Time Eff.
  - Replaced (un)selectAll/One-TBitBtn by TButton
*)
unit MultipleSelectFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxTL, dxCntner, Db, DBTables, UPimsConst, jpeg;

{type
  TMyAction = function(AField: String): Boolean of object; }

type
  TDialogMultipleSelectF = class(TDialogBaseF)
    dxTLAvailable: TdxTreeList;
    dxTLAvailableCode: TdxTreeListColumn;
    dxTLAvailableDescription: TdxTreeListColumn;
    dxTLSelected: TdxTreeList;
    dxTLSelectedCode: TdxTreeListColumn;
    dxTLSelectedDescription: TdxTreeListColumn;
    lblAvailable: TLabel;
    lblSelected: TLabel;
    lblCountry: TLabel;
    edCountry: TEdit;
    btnSelectAll: TButton;
    btnSelectOne: TButton;
    btnUnselectAll: TButton;
    btnUnselectOne: TButton;
    lblTitle: TLabel;
    dxTLAvailableId: TdxTreeListColumn;
    dxTLSelectedId: TdxTreeListColumn;
    Query: TQuery;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnSelectOneClick(Sender: TObject);
    procedure btnUnselectOneClick(Sender: TObject);
    procedure btnSelectAllClick(Sender: TObject);
    procedure btnUnselectAllClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    FSqlInsert: String;
    FSqlDelete: String;
    FSqlExists: String;
    procedure AddNode(AList: TdxTreeList; AId, ACode, AName: String);
    procedure FillList(AList: TdxTreeList; ASql: String;
      ANode1: TdxTreeListNode=nil; ANode2: TdxTreeListNode=nil;
      ANode3: TdxTreeListNode=nil; ANode4: TdxTreeListNode=nil);
    procedure UpdateDatabase;
    procedure InsertRecord(ANode: TdxTreeListNode);
    procedure MoveNode(ASourceList, ADestList: TdxTreeList;
      ANode: TdxTreeListNode);
    procedure DeleteRecord(ANode: TdxTreeListNode);
    function ExistsRecord(ANode: TdxTreeListNode): Boolean;
    procedure LoadAlternativeAvailableList(AAll: Boolean);
    { Private declarations }
  public
    { Public declarations }
{    MyAction: TMyAction; }
    MySqlAvailable: String;
    MySqlAvailable2: String;
    MySqlAll: String;
    procedure Init(ACaptionSuffix, ASqlCountry, ASqlAvailable, ASqlSelected,
      ASqlInsert, ASqlDelete, ASqlExists: String;
      ASqlAvailable2: String='';
      ASqlAll: String='');
  end;

var
  DialogMultipleSelectF: TDialogMultipleSelectF;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TDialogMultipleSelectF.FormShow(Sender: TObject);
begin
  inherited;
  dxBarManBase.Bars.Items[0].Visible := False;
end;

procedure TDialogMultipleSelectF.AddNode(AList: TdxTreeList;
  AId, ACode, AName: String);
var
  Node: TdxTreeListNode;
begin
  Node := AList.Add;
  Node.Values[0] := ACode;
  Node.Values[1] := AName;
  Node.Values[2] := AId;
end;

procedure TDialogMultipleSelectF.Init(ACaptionSuffix, ASqlCountry,
  ASqlAvailable, ASqlSelected, ASqlInsert, ASqlDelete, ASqlExists: String;
  ASqlAvailable2: String='';
  ASqlAll: String='');
begin
  lblTitle.Caption := lblTitle.Caption + ' ' + ACaptionSuffix;
  lblAvailable.Caption := lblAvailable.Caption + ' ' + ACaptionSuffix;
  lblSelected.Caption := lblSelected.Caption + ' ' + ACaptionSuffix;

  edCountry.Text := SystemDM.GetDBValue(ASqlCountry, 'not found');

  FillList(dxTLSelected, ASqlSelected);

  // 20013551
  MySqlAvailable := ASqlAvailable;
  MySqlAvailable2 := ASqlAvailable2;
  MySqlAll := ASqlAll;
  if MySqlAvailable2 <> '' then
    LoadAlternativeAvailableList(True)
  else
    FillList(dxTLAvailable, ASqlAvailable);

  FSqlInsert := ASqlInsert;
  FSqlDelete := ASqlDelete;
  FSqlExists := ASqlExists;
end;

procedure TDialogMultipleSelectF.FillList(AList: TdxTreeList; ASql: String;
  ANode1: TdxTreeListNode=nil; ANode2: TdxTreeListNode=nil;
  ANode3: TdxTreeListNode=nil; ANode4: TdxTreeListNode=nil);
var
  Field: TField;
begin
  Query.Active := False;
  // Look for 1, 2, 3 or 4 shifts for overlap-check.
  if Assigned(ANode1) then
  begin
    if not Assigned(ANode2) then
      Query.Sql.Text := Format(ASql,
        [ANode1.Values[0], ANode1.Values[0],
        ANode1.Values[0], ANode1.Values[0],
        ANode1.Values[0], ANode1.Values[0],
        ANode1.Values[0], ANode1.Values[0]])
    else
      if not Assigned(ANode3) then
        Query.Sql.Text := Format(ASql,
          [ANode1.Values[0], ANode1.Values[0],
          ANode2.Values[0], ANode2.Values[0],
          ANode2.Values[0], ANode2.Values[0],
          ANode2.Values[0], ANode2.Values[0]
          ])
      else
        if not Assigned(ANode4) then
        Query.Sql.Text := Format(ASql,
          [ANode1.Values[0], ANode1.Values[0],
          ANode2.Values[0], ANode2.Values[0],
          ANode3.Values[0], ANode3.Values[0],
          ANode3.Values[0], ANode3.Values[0]
          ])
        else
        Query.Sql.Text := Format(ASql,
          [ANode1.Values[0], ANode1.Values[0],
          ANode2.Values[0], ANode2.Values[0],
          ANode3.Values[0], ANode3.Values[0],
          ANode4.Values[0], ANode4.Values[0]
          ]);
  end
  else
    Query.Sql.Text := ASql;
  Query.Prepare;
  Query.Open;

  AList.ClearNodes;

  Field := Query.FindField('id');
  while not Query.Eof do
  begin
    if Assigned(field) then
      AddNode(AList,
        Query.FieldByName('Id').AsString,
        Query.FieldByName('Code').AsString,
        Query.FieldByName('Description').AsString
      )
    else
    AddNode(AList,
      '',
      Query.FieldByName('Code').AsString,
      Query.FieldByName('Description').AsString
    );
    Query.Next;
  end;
  Query.Close;
end;

procedure TDialogMultipleSelectF.btnOkClick(Sender: TObject);
begin
  inherited;
  UpdateDatabase;
end;

procedure TDialogMultipleSelectF.MoveNode(ASourceList, ADestList: TdxTreeList;
  ANode: TdxTreeListNode);
var
  Node: TdxTreeListNode;
  i: Integer;
begin
  Node := ADestList.Add;
  for i := 0 to ANode.CountValues - 1 do
    Node.Values[i] := ANode.Values[i];
  ANode.Free;
end;

// 20013551
procedure TDialogMultipleSelectF.LoadAlternativeAvailableList(AAll: Boolean);
var
  ANode2, ANode3, ANode4: TdxTreeListNode;
begin
  // This can load a different available-list when needed (for overlapping shifts).
  if MySqlAvailable2 <> '' then
  begin
    if dxTLSelected.Count = 1 then // load a different available-list
      FillList(dxTLAvailable, MySqlAvailable2, dxTLSelected.LastNode)
    else
      if dxTLSelected.Count = 2 then // load a different available-list
      begin
        ANode2 := dxTLSelected.Items[0];
        FillList(dxTLAvailable, MySqlAvailable2, dxTLSelected.LastNode, ANode2);
      end
      else
        if dxTLSelected.Count = 3 then // load a different available-list
        begin
          ANode2 := dxTLSelected.Items[0];
          ANode3 := dxTLSelected.Items[1];
          FillList(dxTLAvailable, MySqlAvailable2, dxTLSelected.LastNode,
            ANode2, ANode3);
        end
        else
          if dxTLSelected.Count = 4 then // load a different available-list
          begin
            ANode2 := dxTLSelected.Items[0];
            ANode3 := dxTLSelected.Items[1];
            ANode4 := dxTLSelected.Items[2];
            FillList(dxTLAvailable, MySqlAvailable2, dxTLSelected.LastNode,
              ANode2, ANode3, ANode4);
          end
          else
            if dxTLSelected.Count = 0 then // load all for available-list
            begin
              if (MySqlAll <> '') and AAll then
                FillList(dxTLAvailable, MySqlAll)
              else
                FillList(dxTLAvailable, MySqlAvailable);
        end
        else
          if AAll then
            dxTLAvailable.ClearNodes;
  end;
end; // LoadAlternativeAvailableList

procedure TDialogMultipleSelectF.btnSelectOneClick(Sender: TObject);
begin
  inherited;
  if not Assigned(dxTLAvailable.FocusedNode) then Exit;

  MoveNode(dxTLAvailable, dxTLSelected, dxTLAvailable.FocusedNode);
  // 20013551
  LoadAlternativeAvailableList(False);
end;

procedure TDialogMultipleSelectF.btnUnselectOneClick(Sender: TObject);
begin
  inherited;
  if not Assigned(dxTLSelected.FocusedNode) then Exit;

  MoveNode(dxTLSelected, dxTLAvailable, dxTLSelected.FocusedNode);
  // 20013551
  LoadAlternativeAvailableList(False);
end;

procedure TDialogMultipleSelectF.btnSelectAllClick(Sender: TObject);
var
  Node: TdxTreeListNode;
  i: Integer;
begin
  inherited;
  for i := dxTLAvailable.Count - 1 downto 0 do
  begin
    Node := dxTLAvailable.Items[i];
    MoveNode(dxTLAvailable, dxTLSelected, Node);
  end;
end;

procedure TDialogMultipleSelectF.btnUnselectAllClick(Sender: TObject);
var
  Node: TdxTreeListNode;
  i: Integer;
begin
  inherited;
  for i := dxTLSelected.Count - 1 downto 0 do
  begin
    Node := dxTLSelected.Items[i];
    MoveNode(dxTLSelected, dxTLAvailable, Node);
  end;
  // 20013551
  LoadAlternativeAvailableList(True);
end;

procedure TDialogMultipleSelectF.DeleteRecord(ANode: TdxTreeListNode);
begin
  Query.Active := False;
  Query.Sql.Text := Format(FSqlDelete, [ANode.Values[0], ANode.Values[1]]);
  if ANode.Values[2] <> '' then
    Query.Sql.Text := Format(FSqlDelete, [ANode.Values[2], ANode.Values[1]]);
  Query.ExecSQL;
end;

procedure TDialogMultipleSelectF.InsertRecord(ANode: TdxTreeListNode);
begin
  Query.Active := False;
  Query.Sql.Text := Format(FSqlInsert, [ANode.Values[0], ANode.Values[1]]);
  if ANode.Values[2] <> '' then
    Query.Sql.Text := Format(FSqlInsert, [ANode.Values[2], ANode.Values[1]]);
  Query.ExecSQL;
end;

function TDialogMultipleSelectF.ExistsRecord(ANode: TdxTreeListNode): Boolean;
begin
  Query.Active := False;
  Query.Sql.Text := Format(FSqlExists, [ANode.Values[0], ANode.Values[1]]);
  if ANode.Values[2] <> '' then
    Query.Sql.Text := Format(FSqlExists, [ANode.Values[2], ANode.Values[1]]);
  Query.Open;
  Result := (not Query.Eof) and (Query.Fields[0].AsInteger > 0);
end;

procedure TDialogMultipleSelectF.UpdateDatabase;
var
  i: Integer;
  Node: TdxTreeListNode;
begin
  for i := 0 to dxTLAvailable.Count - 1 do
  begin
    Node := dxTLAvailable.Items[i];
    DeleteRecord(Node);
  end;
  for i := 0 to dxTLSelected.Count - 1 do
  begin
    Node := dxTLSelected.Items[i];
    if not ExistsRecord(Node) then
      InsertRecord(Node);
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogMultipleSelectF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TDialogMultipleSelectF.FormCreate(Sender: TObject);
begin
  inherited;
  // 20014450.50
  dxTLAvailable.HighlightColor := clPimsDarkBlue;
  dxTLAvailable.HideSelectionColor := clPimsDarkBlue;

  dxTLSelected.HighlightColor := clPimsDarkBlue;
  dxTLSelected.HideSelectionColor := clPimsDarkBlue;
end;

end.
