(*
  Changes:
    MRA:5-JAN-2010 RV050.1. 889953.
      - Addition of field COUNTRY_ID (refers to COUNTRY-table).
        This can not be left empty.
      Stored procedure 'UpdateEMA':
      - Added filtering on TEAMPERUSER
      - Added change of MUTATATIONDATE + MUTATOR
    SO:08-JUL-2010 RV067.4. 550497
    - filter absence reasons
    - calculate NORM_BANK_HLD_RESERVE_MINUTE field
    MRA:4-OCT-2010 RV071.3. 550497
    - When the flag 'Norm Bank Holiday Reserve Minute Manual YN' is
      set to 'Y', then this norm must not be recalculated during
      Update Counters-action, because the user has changed it manually.
    - Use UPimsMessageRes-string for update-counter-message.
    - Added try-except for update-counter-action.
    - Show message when process to availability is ready.
    MRA:11-OCT-2010 RV071.12. 550497
    - Update Counters
      - If employee has standard staff avail. linked
        to absence type 'J' (Time Credit), then
        bank holiday must be skipped on that same day
        of the week.
    MRA:19-JAN-2011 RV085.6. Small change.
    - Bank Holidays
      - The button 'Update Counters' must only be visible when the selected
        Country = Belgium (Code = 'BEL').
    - Added property named CountryID to keep track of current Country-ID.
    - Added filter on teams-per-user for qryCountry:
      - Show only the countries based on teams-per-user for plants that
        are within this selection.
    MRA:2-DEC-2011. RV103.1. SO-20011799. Worked hours during bank holiday.
    - Addition of hourtype for 'worked hours during bank holiday'.
    - Can be NULL.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:18-FEB-2014 20011800
    - Final Run System
    - Database Procedure BANKHOL_UPDATEEMA has been changed!
    MRA:25-JUN-2015 20014450.50
    - Real Time Eff.
    MRA:29-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning.
    - Database Procedure BANKHOL_UPDATEEMA has been changed!
*)

unit DialogBankHolidayFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Dblup1a, Dblup1b,
  dxDBELib, Grids, Calendar, Db, DBTables, dxTL, dxDBCtrl, dxDBGrid,
  DBCtrls, dxDBEdtr, SystemDMT, UPimsConst, jpeg;

type
  TDialogBankHolidayF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    GroupBoxDetailBank: TGroupBox;
    DateBank: TDateTimePicker;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    TableBK: TTable;
    DataSourceBK: TDataSource;
    TableAbsReason_: TTable;
    TableBKBANK_HOLIDAY_DATE: TDateTimeField;
    TableBKDESCRIPTION: TStringField;
    TableBKABSENCEREASON_CODE: TStringField;
    TableBKCREATIONDATE: TDateTimeField;
    TableBKMUTATIONDATE: TDateTimeField;
    TableBKMUTATOR: TStringField;
    TableBKABSENCELU: TStringField;
    DataSourceAbs: TDataSource;
    EditDescBK: TEdit;
    ComboBoxAbsRsn: TComboBox;
    QueryProcess: TQuery;
    QueryBK: TQuery;
    DataSourceQryBK: TDataSource;
    StringGrid1: TStringGrid;
    Label5: TLabel;
    StringGrid2: TStringGrid;
    Label6: TLabel;
    StringGrid3: TStringGrid;
    Label7: TLabel;
    StringGrid4: TStringGrid;
    Label8: TLabel;
    StringGrid5: TStringGrid;
    Label9: TLabel;
    StringGrid6: TStringGrid;
    Label10: TLabel;
    StringGrid7: TStringGrid;
    Label11: TLabel;
    StringGrid8: TStringGrid;
    Label12: TLabel;
    Label13: TLabel;
    StringGrid9: TStringGrid;
    Label14: TLabel;
    StringGrid10: TStringGrid;
    Label15: TLabel;
    StringGrid11: TStringGrid;
    Label16: TLabel;
    StringGrid12: TStringGrid;
    StoredProcUpdateEMA: TStoredProc;
    btnSelectedDateOK: TBitBtn;
    qryCountry: TQuery;
    TableBKCOUNTRY_ID: TFloatField;
    LabelCountry: TLabel;
    ComboBoxCountry: TComboBox;
    QueryAbsReason: TQuery;
    btnUpdateCounterBankHolidays: TBitBtn;
    QueryEmployeesByCountry: TQuery;
    qryStandAvail: TQuery;
    qryStandAvailXXX: TQuery;
    Label17: TLabel;
    ComboBoxHTWOBH: TComboBox;
    qryHourtypeWOBH: TQuery;
    TableBKHOURTYPE_WRK_ON_BANKHOL: TIntegerField;
    procedure dxSpinEditYearChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure StringGrid1Click(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure TableBKDESCRIPTIONValidate(Sender: TField);
    procedure TableBKABSENCEREASON_CODEValidate(Sender: TField);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
    procedure EditDescBKChange(Sender: TObject);
    procedure TableBKBeforeDelete(DataSet: TDataSet);
    procedure ComboBoxAbsRsnChange(Sender: TObject);
    procedure TableBKAfterPost(DataSet: TDataSet);
    procedure TableBKAfterDelete(DataSet: TDataSet);
    procedure btnOkClick(Sender: TObject);
    procedure TableBKBeforePost(DataSet: TDataSet);
    procedure TableBKDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure TableBKEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure TableBKPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DateBankChange(Sender: TObject);
    procedure DateBankClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure StringGridDrawCell(
      Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btnSelectedDateOKClick(Sender: TObject);
    procedure ComboBoxCountryChange(Sender: TObject);
    procedure btnUpdateCounterBankHolidaysClick(Sender: TObject);
    procedure ComboBoxHTWOBHChange(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FCountryID: Integer; // RV085.6.
    FForm_Edit_YN: String; // 20011800
    procedure ProcessEmployeeAvailability(ADateFrom, ADateTo: TDateTime);
    procedure ComboBoxCountryPopulate;
    function SelectBankHoliday(ADateFrom, ADateTo: TDateTime):
      Boolean;
    function DetermineCountryCode: String;
    function DetermineCountryID: Integer;
    function TableBKFind(ADateBK: TDateTime): Boolean;
    procedure UpdateAbsenceReasons;
    procedure UpdateHourtypesWOBH;
  public
    { Public declarations }
//    FListBankHOL: TStringList;
    FShowForm: Boolean;
    FFocusGrid: TStringGrid;
    FLastSelectedGrid: TStringGrid;
    FArrayUpdateMonths: Array[1..12] of Boolean;
    function GetLastDayOfMonth(Year, Month: Integer): Integer;
    procedure FillCalendars(TmpStringGrid: TStringGrid; Month: Integer);
    procedure ShowMonths(Year: Word);
    procedure SetNoSelectionGrid(TmpGrid: TStringGrid);
    procedure SetDetailBankHoliday(TmpGrid: TStringGrid);
    procedure SetFieldsBank(DateCurrent: TDateTime);
    procedure ShowOneMonths(Month: Integer);
    procedure SetFocusCell(Month, Day: Integer);
    function ProcessEmplAvail(ADateBank: TDateTime;
      AOldAbsenceReason, ANewAbsenceReason: String): Integer;
    property CountryID: Integer read FCountryID write FCountryID; // RV085.6.
    property Form_Edit_YN: String read FForm_Edit_YN write FForm_Edit_YN; // 20011800
  end;

var
  DialogBankHolidayF: TDialogBankHolidayF;

implementation

uses
  ListProcsFRM, UPimsMessageRes, UGlobalFunctions;

{$R *.DFM}

function TDialogBankHolidayF.SelectBankHoliday(ADateFrom, ADateTo: TDateTime):
  Boolean;
begin
  QueryBK.Close;
  QueryBK.ParamByName('FSTARTDATE').Value := ADateFrom;
  QueryBK.ParamByName('FENDDATE').Value := ADateTo;
  QueryBK.ParamByName('FCOUNTRY_ID').Value := CountryID;
  if not QueryBK.Prepared then
    QueryBK.Prepare;
  QueryBK.Open;
  Result := not QueryBK.IsEmpty;
end;

procedure TDialogBankHolidayF.ShowMonths(Year: Word);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 1 to 12 do
    FArrayUpdateMonths[Counter] := False;

  SelectBankHoliday(EncodeDate(Year, 1, 1), EncodeDate(Year, 12, 31));

  QueryBK.First;

  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TStringGrid) then
    begin
      Index := (TempComponent as TStringGrid).Tag;
      FillCalendars((TempComponent as TStringGrid), Index);
      SetNoSelectionGrid((TempComponent as TStringGrid));
    end;
  end;
end;

procedure TDialogBankHolidayF.ShowOneMonths(Month : Integer);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  FArrayUpdateMonths[Month] := False;
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TStringGrid) then
    begin
      Index := (TempComponent as TStringGrid).Tag;
      if (Index = Month)  then
      begin
        FillCalendars((TempComponent as TStringGrid), Index);
        SetNoSelectionGrid((TempComponent as TStringGrid));
      end;
    end;
  end;
end;

procedure TDialogBankHolidayF.dxSpinEditYearChange(Sender: TObject);
var
  DateCurrent: TDateTime;
begin
  inherited;

  if (not FShowForm) then
    Exit;
  if (Round(dxSpinEditYear.Value) > 1900) and
    (Round(dxSpinEditYear.Value) < 2099) then
  begin
    DateCurrent := EncodeDate(Round(dxSpinEditYear.Value), 1, 1);
    SetFieldsBank(DateCurrent);
    ShowMonths(Round(dxSpinEditYear.Value));
    SetFocusCell(1, 1);
  end;
end;

function TDialogBankHolidayF.GetLastDayOfMonth(Year, Month: Integer): Integer;
begin
  case Month of
    1, 3, 5, 7, 8, 10, 12: Result := 31;
    2: if (Year mod 4)= 0 then
         Result := 29
       else
         Result := 28;
    else
      Result := 30;
  end;
end;

procedure TDialogBankHolidayF.FillCalendars(TmpStringGrid: TStringGrid;
  Month: Integer);
var
  I, J, K, FirstDayOfMonth, LastDatOfMonth : Integer;
  DateBK: TDateTime;
  DayStr: String;
begin
  Screen.Cursor := crHourGlass;
  if FArrayUpdateMonths[Month] then
    Exit;
  FArrayUpdateMonths[Month] := True;
  for i := 0 to 6 do
    for j:= 0 to 6 do
      TmpStringGrid.Cells[J,I] := '';
  with TmpStringGrid do
  begin
    for i:= 0 to 6 do
    begin
      DayStr := SystemDM.GetDayWCode(i + 1);
      if DayStr <> '' then
        cells[i,0] := SystemDM.GetDayWCode(i + 1)[1]{+
        Lowercase(SystemDM.GetDayWCode(i + 1)[2])};
    end;
    FirstDayOfMonth :=
      ListProcsF.DayWStartOnFromDate(Encodedate(Round(dxSpinEditYear.Value),
      Month, 1));
    LastDatOfMonth := GetLastDayOfMonth(Round(dxSpinEditYear.Value), Month);
    K:= 0;
    for J:= (FirstDayOfMonth ) - 1 to 6 do
    begin
      K:= K + 1;
      DateBK := Encodedate(Round(dxSpinEditYear.Value), Month, K);
      // RV050.1.
//      if TableBK.FindKey([DateBK]) then
      if TableBKFind(DateBK) then
        Cells[J,1] := Cells[J,1] + IntToStr(K) + '*'
      else
        Cells[J,1] := Cells[j,1] + IntToStr(K);
    end;
    for I:= 2 to 6 do
      for J := 0 to 6 do
      begin
        K := K + 1;
        if K <= LastDatOfMonth then
        begin
          DateBK := Encodedate(Round(dxSpinEditYear.Value), Month, K);
          // RV050.1.
//          if TableBK.FindKey([DateBK]) then
          if TableBKFind(DateBK) then
            Cells[J,I] :=  Cells[J,I] + IntToStr(K) + '*'
          else
            Cells[J,I] :=  Cells[J,I] + IntToStr(K);
         end;
      end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TDialogBankHolidayF.SetNoSelectionGrid(TmpGrid: TStringGrid);
var
  myRect: TGridRect;
begin
  myRect.Left := -1;
  myRect.Top := -1;
  myRect.Right := -1;
  myRect.Bottom := -1;
  TmpGrid.Selection := MyRect;
end;

procedure TDialogBankHolidayF.SetFocusCell(Month, Day: Integer);
var
  TempComponent: TComponent;
  Counter, Index, Col, Row: Integer;
 // TmpGrid: TStringGrid;
  GR : TGridRect;
  procedure GetRowCol(TmpGrid: TStringGrid;
    Day: Integer; var Row, Col: Integer);
  var
    i,j: Integer;
  begin
    for i := 0 to 6 do
      for j:= 0 to 6 do
        if (TmpGrid.Cells[J,I] = IntToStr(Day)) or
           (TmpGrid.Cells[j,i] = IntToStr(Day) + '*')  then
         begin
           Row := j; Col := i;
           Exit;
         end;
  end;
begin

  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TStringGrid) then
    begin
      Index := (TempComponent as TStringGrid).Tag;
      if (Index = Month)  then
      begin
        FFocusGrid := (TempComponent as TStringGrid);
        GetRowCol(FFocusGrid, Day, Col, Row);
        GR.Left := Col;
        GR.Top  := Row;
        GR.Right := Col;
        GR.Bottom := Row;
        FFocusGrid.Selection := GR;
        Exit;
      end;
    end;
  end;
end;

procedure TDialogBankHolidayF.FormShow(Sender: TObject);
var
  Year, Month, Day: Word;
//  CanSelect: Boolean;
//  TmpGrid: TDrawGrid;
begin
  inherited;
  DecodeDate(Now, Year, Month, Day);
  dxSpinEditYear.Value := Year;
  FShowForm := True;
  ShowMonths(Year);
  SetFieldsBank(Now);
  SetFocusCell(Month, Day);
  // 20011800 Related to this order
  if (Form_Edit_YN = ITEM_VISIBIL) or not (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
  begin
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    GroupBoxDetailBank.Enabled := False;
    DateBank.Enabled := False;
    btnOK.Enabled := False;
    btnSelectedDateOK.Enabled := False;
    btnUpdateCounterBankHolidays.Enabled := False;
  end;
end;

procedure TDialogBankHolidayF.FormCreate(Sender: TObject);
begin
  inherited;
  // 20014450.50
  btnSelectedDateOK.Font.Color := clWhite;
  btnUpdateCounterBankHolidays.Font.Color := clWhite;

  TableBK.Active := True;
  pnlInsertBase.Caption := '';

  // RV050.1. Fill a combobox with all possible countries.
  ComboBoxCountryPopulate;
  UpdateAbsenceReasons;
  // RV103.1.
  UpdateHourtypesWOBH;

  // RV085.6. After ComboBoxCountry is populated, initialise CountryID.
  CountryID := DetermineCountryID; // RV085.6.

  // RV085.6.
  btnUpdateCounterBankHolidays.Visible := False;
  ComboBoxCountryChange(Sender);

  FShowForm := False;
end;

procedure TDialogBankHolidayF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TableBK.Active := False;
  qryCountry.Close;
  QueryAbsReason.Active := False;

end;

procedure TDialogBankHolidayF.SetFieldsBank(DateCurrent: TDateTime);
begin
  ReplaceTime(DateCurrent, 0);
  DateBank.Date  := DateCurrent;
  // RV050.1.
//  if TableBK.FindKey([DateCurrent]) then
  if TableBKFind(DateCurrent) then
  begin
    EditDescBK.Text := TableBK.FieldByName('DESCRIPTION').AsString;
    // MR:02-01-2003 Use Locate because of key-change for ABSENCEREASON-table
//    if QueryAbsReason.FindKey([TableBK.FieldByName('ABSENCEREASON_CODE').AsString]) then
//      ComboBoxAbsRsn.Text := TableBK.FieldByName('ABSENCEREASON_CODE').AsString +
//        ' ' + QueryAbsReason.FieldByName('DESCRIPTION').AsString;
    QueryAbsReason.Open;
    if QueryAbsReason.Locate('ABSENCEREASON_CODE',
      TableBK.FieldByName('ABSENCEREASON_CODE').AsString, []) then
      ComboBoxAbsRsn.Text := TableBK.FieldByName('ABSENCEREASON_CODE').AsString +
        ' ' + QueryAbsReason.FieldByName('DESCRIPTION').AsString;

    // RV103.1.
    qryHourtypeWOBH.Open;
    ComboBoxHTWOBH.Text := '';
    if (TableBK.FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsString <> '') then
      if qryHourtypeWOBH.Locate('HOURTYPE_NUMBER',
        TableBK.FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsInteger, []) then
        ComboBoxHTWOBH.Text :=
          TableBK.FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsString +
          ' ' + qryHourtypeWOBH.FieldByName('DESCRIPTION').AsString;

    dxBarBDBNavDelete.Enabled := True;
    ComboBoxAbsRsn.Enabled := False;
  end
  else
  begin
    EditDescBK.Text := '';
    ComboBoxAbsRsn.Text := '';
    ComboBoxHTWOBH.Text := ''; // RV103.1.
    ComboBoxAbsRsn.Enabled := True;
    dxBarBDBNavDelete.Enabled := False;
  end;
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
end;

procedure TDialogBankHolidayF.SetDetailBankHoliday(TmpGrid: TStringGrid);
var
  Day, Year, Month: Integer;
  DateCurrent: TDateTime;
  ValStr: String;
begin
  if (FLastSelectedGrid <> Nil) and (FLastSelectedGrid <> TmpGrid) then
    SetNoSelectionGrid(FLastSelectedGrid);
  FLastSelectedGrid := TmpGrid;
  Year := Round(dxSpinEditYear.Value);
  if (TmpGrid.Col < 0) or (TmpGrid.Row < 0) then
    Exit;
  ValStr := TmpGrid.Cells[TmpGrid.Col, TmpGrid.Row];
  if (ValStr = '') then
    Exit;
  if (Copy(ValStr,length(ValStr), 1) = '*') then
    ValStr := Copy(ValStr, 0, length(ValStr) - 1);

  Day := StrToInt(ValStr);
  Month := TmpGrid.Tag;
  if ((Day >= 1) and (Day <= GetLastDayOfMonth(Year, Month))) then
  begin
    DateCurrent := EncodeDate(Year, Month, Day);
    SetFieldsBank(DateCurrent);
  end;
end;

procedure TDialogBankHolidayF.StringGrid1Click(Sender: TObject);
begin
  inherited;
  if (Sender is TStringGrid) then
  begin
    SetDetailBankHoliday((Sender as TStringGrid));
    if (Sender as TStringGrid) <> FFocusGrid then
      SetNoSelectionGrid(FFocusGrid);
  end;
end;

procedure TDialogBankHolidayF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  EditDescBK.Text := '';
  ComboBoxAbsRsn.Text := '';
  ComboBoxHTWOBH.Text := ''; // RV103.1.
  EditDescBK.SetFocus;
  TableBK.Insert;
end;

procedure TDialogBankHolidayF.dxBarBDBNavPostClick(Sender: TObject);
var
  Year, Month, Day: Word;
  DateTmp: TDateTime;
  // RV103.1. Copy till first found space.
  function MyCopy(AValue: String): String;
  var
    IPos: Integer;
  begin
    Result := AValue;
    if Result <> '' then
    begin
      IPos := Pos(' ', AValue);
      if IPos > 0 then
        Result := Copy(AValue, 1, IPos - 1);
    end;
  end;
begin
  inherited;
  DateTmp := Trunc(DateBank.Date);
  // RV050.1.
//  if TableBK.FindKey([DateTmp]) then
  if TableBKFind(DateTmp) then
  begin
    if (EditDescBK.Text = '') and (ComboBoxAbsRsn.Text = '') then
    begin
      TableBK.Delete;
      Exit;
    end;
    TableBK.Edit;
  end
  else
     TableBK.Insert;
  if  EditDescBK.Text = '' then
  begin
    DisplayMessage(SPimsEmptyDescription, mtInformation, [mbOk]);
    Exit;
  end;
  if ComboBoxAbsRsn.Text = '' then
  begin
    DisplayMessage(SPimsEmptyAbsReason, mtInformation, [mbOk]);
    Exit;
  end;

  // MR:02-01-2003 Use Locate because of key-change for ABSENCEREASON-table
//  if not QueryAbsReason.FindKey([Copy(ComboBoxAbsRsn.Text, 0, 1)]) then
  QueryAbsReason.Open;
  if not QueryAbsReason.Locate('ABSENCEREASON_CODE', copy(ComboBoxAbsRsn.Text, 0, 1), []) then
  begin
    DisplayMessage(SPimsInvalidAbsReason, mtInformation, [mbOk]);
    Exit;
  end;
  DateTmp := Trunc(DateBank.Date);
  TableBK.FieldByName('BANK_HOLIDAY_DATE').Value := DateTmp;
  // RV050.1.
  TableBK.FieldByName('COUNTRY_ID').Value := CountryID;
  TableBK.FieldByName('DESCRIPTION').Value := EditDescBK.Text;
  TableBK.FieldByName('ABSENCEREASON_CODE').Value :=
    Copy(ComboBoxAbsRsn.Text, 0, 1);
  // RV103.1.
  TableBK.FieldByName('HOURTYPE_WRK_ON_BANKHOL').AsString :=
    MyCopy(ComboBoxHTWOBH.Text);
  TableBK.FieldByName('CREATIONDATE').Value := Now;
  TableBK.FieldByName('MUTATIONDATE').Value := Now;
  TableBK.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
  TableBK.Post;
  DecodeDate(DateBank.Date, Year, Month, Day);
  SetFocusCell(Month, Day);
  ComboBoxAbsRsn.Enabled := False;
end;

procedure TDialogBankHolidayF.TableBKDESCRIPTIONValidate(Sender: TField);
begin
  inherited;
  SystemDM.DefaultNotEmptyValidate(Sender);
end;

procedure TDialogBankHolidayF.TableBKABSENCEREASON_CODEValidate(
  Sender: TField);
begin
  inherited;
  SystemDM.DefaultNotEmptyValidate(Sender);
end;

procedure TDialogBankHolidayF.dxBarBDBNavDeleteClick(Sender: TObject);
var
  Year, Month, Day: Word;
  Date_Bank: TDateTime;
  OldAbsenceReason: String;
begin
  inherited;
  OldAbsenceReason := TableBK.FieldByName('ABSENCEREASON_CODE').AsString;
// car 12.12.2002
  // RV050.1.
//  TableBK.FindKey([GetDate(DateBank.Date)]);
  Date_Bank := Trunc(DateBank.Date);
  TableBKFind(Date_Bank);
// end car
  Date_Bank := TableBK.FieldByName('BANK_HOLIDAY_DATE').AsDateTime;
  TableBK.Delete;
  if ProcessEmplAvail(Date_Bank, OldAbsenceReason , '*') <> 0 then
    if SystemDM.UseFinalRun then
      DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]); // 20011800
  EditDescBK.Text := '';
  ComboBoxAbsRsn.Text := '';
  ComboBoxHTWOBH.Text := ''; // RV103.1.
  ComboBoxAbsRsn.Enabled := True;
  DecodeDate(DateBank.Date, Year, Month, Day);
  SetFocusCell(Month, Day);
  dxBarBDBNavDelete.Enabled := False;
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
end;

procedure TDialogBankHolidayF.EditDescBKChange(Sender: TObject);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;
  dxBarBDBNavCancel.Enabled := True;
end;

procedure TDialogBankHolidayF.TableBKBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforeDelete(DataSet);
end;

procedure TDialogBankHolidayF.ComboBoxAbsRsnChange(Sender: TObject);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;
  dxBarBDBNavCancel.Enabled := True;
end;

procedure TDialogBankHolidayF.TableBKAfterPost(DataSet: TDataSet);
var
  Year, Month, Day: Word;
begin
  inherited;
  DecodeDate(DateBank.Date, Year, Month, Day);
  if (Year <> Round(dxSpinEditYear.Value)) then
  begin
    ShowMonths(Year);
    dxSpinEditYear.Value := Year;
  end
  else
    ShowOneMonths(Month);
end;

procedure TDialogBankHolidayF.TableBKAfterDelete(DataSet: TDataSet);
var
  Year, Month, Day: Word;
begin
  inherited;
  DecodeDate(DateBank.Date, Year, Month, Day);
  ShowOneMonths(Month);
end;

function TDialogBankHolidayF.ProcessEmplAvail(ADateBank: TDateTime;
  AOldAbsenceReason, ANewAbsenceReason: String): Integer;
begin
  Result := 0; // 20011800
  // RV050.1. Added parameter countryid + currentuser
  StoredProcUpdateEMA.Prepare;
  StoredProcUpdateEMA.ParamByName('DATE_BANK').AsDateTime := ADateBank;
  StoredProcUpdateEMA.ParamByName('COUNTRYID').AsInteger := CountryID;
  StoredProcUpdateEMA.ParamByName('CURRENTUSER').AsString :=
    SystemDM.CurrentProgramUser;
  StoredProcUpdateEMA.ParamByName('USERNAME').AsString := SystemDM.UserTeamLoginUser;
  StoredProcUpdateEMA.ParamByName('NEW_ABSENCE').AsString := ANewAbsenceReason;
  StoredProcUpdateEMA.ParamByName('OLD_ABSENCE').AsString := AOldAbsenceReason;
  StoredProcUpdateEMA.Prepare;
  StoredProcUpdateEMA.ExecProc;
  // 20011800 Get status that indicates if availability could not be changed because of final-run.
  try
    if SystemDM.UseFinalRun then
      Result := StoredProcUpdateEMA.ParamByName('STATUS').AsInteger;
  except
  end;
end;

// MR:16-06-2005 Order 550403. Changed, so the 'datefrom' and 'dateto' can be
// used to indicate over what period the action must be processed.
procedure TDialogBankHolidayF.ProcessEmployeeAvailability(
  ADateFrom, ADateTo: TDateTime);
var
  AbsenceRsn: String;
  Date_Bank: TDateTime;
  OK: Boolean;
  Status: Integer; // 20011800
begin
  SelectBankHoliday(ADateFrom, ADateTo);

  OK := False;
  Status := 0; // 20011800
  // RV071.3.
  Screen.Cursor := crHourGlass;
  try
    try
      QueryBK.First;
      while not QueryBK.Eof do
      begin
        AbsenceRsn := QueryBK.FieldByName('ABSENCEREASON_CODE').AsString;
        Date_Bank := QueryBK.FieldByName('BANK_HOLIDAY_DATE').AsDateTime;
        Status := Status + ProcessEmplAvail(Date_Bank, '*',  AbsenceRsn); // 20011800
        QueryBK.Next;
      end;
      if SystemDM.UseFinalRun then
        if Status <> 0 then
          DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]); // 20011800
      OK := True;
    except // RV071.3.
      on E:Exception do
        DisplayMessage(E.Message, mtInformation, [mbOk]);
    end;
  finally
    Screen.Cursor := crDefault;
    if OK then
      DisplayMessage(SPimsProcessToAvailIsReady, mtInformation, [mbOk]);
  end;
end;

// MR:16-06-2005 Order 550403. Process ALL bank-holiday-days for the WHOLE YEAR
procedure TDialogBankHolidayF.btnOkClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
     DisplayMessage(SPimsSaveBefore, mtInformation, [mbOk]);
     Exit;
  end;
  if DisplayMessage(SPimsConfirmProcessEmployee, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
    Abort;
  ProcessEmployeeAvailability(
    EncodeDate(Round(dxSpinEditYear.Value), 1, 1),
    EncodeDate(Round(dxSpinEditYear.Value), 12, 31)
    );
end;

procedure TDialogBankHolidayF.TableBKBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
end;

procedure TDialogBankHolidayF.TableBKDeleteError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  SystemDM.DefaultDeleteError(DataSet, E, Action);
end;

procedure TDialogBankHolidayF.TableBKEditError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  SystemDM.DefaultEditError(DataSet, E, Action);
end;

procedure TDialogBankHolidayF.TableBKPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  inherited;
  SystemDM.DefaultPostError(DataSet, E, Action);
end;

procedure TDialogBankHolidayF.DateBankChange(Sender: TObject);
begin
  inherited;
  SetFieldsBank(DateBank.Date);
  dxBarBDBNavPost.Enabled := True;
  dxBarBDBNavCancel.Enabled := True;
end;

procedure TDialogBankHolidayF.DateBankClick(Sender: TObject);
begin
  inherited;
  dxBarBDBNavCancel.Enabled := True;
  dxBarBDBNavPost.Enabled := True;
end;

procedure TDialogBankHolidayF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  TableBK.Cancel;
  SetFieldsBank(DateBank.Date);
end;

procedure TDialogBankHolidayF.StringGridDrawCell(
  Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  TextStr : array[0..255] of Char ;
begin
  (Sender as TStringGrid).Canvas.Font.Color := clBlack;
  (Sender as TStringGrid).Color := clSilver; //$00E8F9A6;
  StrPCopy(TextStr, (Sender as TStringGrid).Cells[ACol, ARow]) ;

  if Pos('*', (Sender as TStringGrid).Cells[ACol, ARow]) > 0 then
    (Sender as TStringGrid).Canvas.Brush.Color := clSecondLine //$00B9D17E
  else
    (Sender as TStringGrid).Canvas.Brush.Color  := clSilver; //$00DFF3BC;

  (Sender as TStringGrid).Canvas.FillRect(Rect);
  DrawText((Sender as TStringGrid).Canvas.Handle,
    TextStr, StrLen(TextStr), Rect, 0);
end;

// MR:16-06-2005 Order 550403. For processing only selected date, not whole year.
procedure TDialogBankHolidayF.btnSelectedDateOKClick(Sender: TObject);
var
  HolidayDescription: String;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
     DisplayMessage(SPimsSaveBefore, mtInformation, [mbOk]);
     Exit;
  end;
  // Check if this is a bankholiday
  if not SelectBankHoliday(Trunc(DateBank.Date), Trunc(DateBank.Date)) then
  begin
     DisplayMessage(SPimsNoBankHoliday, mtInformation, [mbOk]);
     Exit;
  end;
  if EditDescBK.Text = '' then
    HolidayDescription := '???'
  else
    HolidayDescription := EditDescBK.Text;
  if DisplayMessage(Format(SPimsConfirmProcessEmployeeSelectedDate,
    [DateToStr(DateBank.Date) + ' (' + HolidayDescription + ')']),
    mtConfirmation, [mbYes, mbNo]) <> mrYes then
    Abort;
  ProcessEmployeeAvailability(
    Trunc(DateBank.Date),
    Trunc(DateBank.Date)
    );
end;

// RV050.1. Populate the ComboBox with all possible countries.
procedure TDialogBankHolidayF.ComboBoxCountryPopulate;
begin
  qryCountry.Close;
  qryCountry.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser; // RV085.6.
  qryCountry.Open;
  while not qryCountry.Eof do
  begin
    ComboBoxCountry.Items.Add(qryCountry.FieldByName('CODE').AsString + ' ' +
      qryCountry.FieldByName('DESCRIPTION').AsString);
    qryCountry.Next;
  end;
  ComboBoxCountry.ItemIndex := 0;
end;

// RV085.6.
function TDialogBankHolidayF.DetermineCountryCode: String;
var
  IPos: Integer;
begin
  Result := '';
  IPos := Pos(' ', ComboBoxCountry.Text);
  if IPos > 0 then
    Result := Copy(ComboBoxCountry.Text, 1, IPos - 1);
end;

// RV050.1. Determine the country_id from country-table.
function TDialogBankHolidayF.DetermineCountryID: Integer;
var
  ResultStr: String;
begin
  Result := 0;
  ResultStr := DetermineCountryCode;
  if ResultStr <> '' then
    if qryCountry.Locate('CODE', ResultStr, []) then
      Result := qryCountry.FieldByName('COUNTRY_ID').AsInteger;
end;

// RV050.1.
procedure TDialogBankHolidayF.ComboBoxCountryChange(Sender: TObject);
begin
  inherited;
  // RV085.6.
  CountryID := DetermineCountryID;
  btnUpdateCounterBankHolidays.Visible :=
    (DetermineCountryCode = COUNTRYCODE_BELGIUM);
  dxSpinEditYearChange(Sender);
  UpdateAbsenceReasons;
  // RV103.1.
  UpdateHourtypesWOBH;
end;

// RV050.1.
function TDialogBankHolidayF.TableBKFind(ADateBK: TDateTime): Boolean;
begin
  Result := TableBK.Locate('BANK_HOLIDAY_DATE;COUNTRY_ID',
    VarArrayOf([ADateBK, CountryID]),
    []);
end;

//RV067.4.
procedure TDialogBankHolidayF.UpdateAbsenceReasons;
var
  SaveSql: String;
begin
  SaveSql := QueryAbsReason.Sql.Text;
  SystemDM.UpdateAbsenceReasonPerCountry(
    CountryID,
    QueryAbsReason
  );

  ComboBoxAbsRsn.Items.Clear;

  QueryAbsReason.Open;
  QueryAbsReason.First;
  while not QueryAbsReason.Eof do
  begin
     ComboBoxAbsRsn.Items.Add(QueryAbsReason.FieldByName('ABSENCEREASON_CODE').
       AsString + ' ' + QueryAbsReason.FieldByName('DESCRIPTION').AsString);
     QueryAbsReason.Next;
  end;

  QueryAbsReason.Sql.Text := SaveSql;
end;

procedure TDialogBankHolidayF.btnUpdateCounterBankHolidaysClick(
  Sender: TObject);
var
//  EmployeesByCountrySql: String;
  EmployeeNumber, CountBankHolidays: Integer;
  Minutes: Double;
  OK: Boolean;
  // RV071.12. 550497
  // Check if employee has a standard availability linked to
  // Absence type code = TIMECREDIT, if so, then decrease the
  // Bank holiday that falls on that same day of the week.
  function StandAvailAction: Integer;
  var
    WeekDay: Integer;
  begin
    Result := CountBankHolidays;
    // RV071.12 Look if employee is NOT available (-) in Stand. Avail.
    //          If so, then decrease the holiday.
    with qryStandAvail do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
//      ParamByName('ABSENCETYPE_CODE').AsString := TIMECREDIT;
      Open;
      while not Eof do
      begin
        QueryBk.First;
        while not QueryBk.Eof do
        begin
          WeekDay :=
            ListProcsF.DayWStartOnFromDate(
              QueryBk.
                FieldByName('BANK_HOLIDAY_DATE').AsDateTime);
          if FieldByName('DAY_OF_WEEK').AsInteger = WeekDay then
            Result := Result - 1;
          QueryBk.Next;
        end;
        Next;
      end;
    end;
    if Result < 0 then
      Result := 0;
  end;
begin
  OK := False;
  Screen.Cursor := crHourGlass;
  try
    try
  //-	Reset counter to 75 for all employee of the selected country

      // RV071.12. Select-statement put in TQuery-variable.
      //            Added Team-Per-User-filtering.
{
      EmployeesByCountrySql :=
        'SELECT ' + NL +
        '  E.EMPLOYEE_NUMBER, P.COUNTRY_ID ' + NL +
        '  FROM EMPLOYEE E, PLANT P ' + NL +
        '  WHERE E.PLANT_CODE = P.PLANT_CODE ' +
        '    AND P.COUNTRY_ID = ' + IntToStr(DetermineCountryId) + ' ' + NL +
        '    AND ' + NL +
        '    ( ' +
        '      (:USER_NAME = ''*'') OR ' +
        '      (E.TEAM_CODE IN ' +
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME))' +
        '    ) ' +
        'ORDER BY ' + NL +
        '  E.EMPLOYEE_NUMBER ';
}
      QueryEmployeesByCountry.Close;
//      QueryEmployeesByCountry.Sql.Text := EmployeesByCountrySql;
      QueryEmployeesByCountry.ParamByName('COUNTRY_ID').AsInteger := CountryID;
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        QueryEmployeesByCountry.ParamByName('USER_NAME').AsString :=
          SystemDM.UserTeamLoginUser
      else
        QueryEmployeesByCountry.ParamByName('USER_NAME').AsString := '*';
      QueryEmployeesByCountry.Open;

      // RV071.12. This must be done first!
      SelectBankHoliday(EncodeDate(Round(dxSpinEditYear.Value), 1, 1),
        EncodeDate(Round(dxSpinEditYear.Value), 12, 31));

      CountBankHolidays := QueryBk.RecordCount;
      // RV071.11. Calculate this later.
//      Minutes := 75 * 60 - 7.5 * 60 * CountBankHolidays;
    //for each employee of the selected country
    //-	Decrease the counter with 7.5 for each bank holiday in the calendar for all employees of the selected country.
    //set NORM_BANK_HLD_RESERVE_MINUTE = 75 - 7.5 * CountBankHolidays
      while not QueryEmployeesByCountry.Eof do
      begin
        EmployeeNumber :=
          QueryEmployeesByCountry.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        // RV071.11.
        Minutes := 75 * 60 - 7.5 * 60 * StandAvailAction;
        UpdateAbsenceTotalMode(QueryEmployeesByCountry,
          EmployeeNumber, Round(dxSpinEditYear.Value), Minutes, 'X', False);
        QueryEmployeesByCountry.Next;
      end;
      QueryEmployeesByCountry.Close;
      // RV071.3.
      OK := True;
    except // RV071.3.
      on E:Exception do
        DisplayMessage(E.Message, mtInformation, [mbOk]);
    end;
  finally
    Screen.Cursor := crDefault;
    if OK then
      DisplayMessage(SPimsCountersHaveBeenUpdated, mtInformation, [mbOk]);
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogBankHolidayF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

// RV103.1.
procedure TDialogBankHolidayF.UpdateHourtypesWOBH;
var
  SaveSql: String;
begin
  with qryHourtypeWOBH do
  begin
    SaveSql := Sql.Text;
    SystemDM.UpdateHourTypePerCountry(
      CountryID,
      qryHourtypeWOBH
    );

    ComboBoxHTWOBH.Items.Clear;

    Open;
    First;
    while not Eof do
    begin
       ComboBoxHTWOBH.Items.Add(FieldByName('HOURTYPE_NUMBER').
         AsString + ' ' + FieldByName('DESCRIPTION').AsString);
       Next;
    end;

    Sql.Text := SaveSql;
  end;
end;

// RV103.1.
procedure TDialogBankHolidayF.ComboBoxHTWOBHChange(Sender: TObject);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;
  dxBarBDBNavCancel.Enabled := True;
end;

end.
