(*
  MRA:25-JAN-2019 GLOB3-204
  - Personal Time Off USA
*)
unit PTODefDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TPTODefDM = class(TGridBaseDM)
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterTIME_FOR_TIME_YN: TStringField;
    TableMasterBONUS_IN_MONEY_YN: TStringField;
    TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableMasterWEEKS_IN_PERIOD: TIntegerField;
    TableMasterWORK_TIME_REDUCTION_YN: TStringField;
    TableMasterHOURTYPE_NUMBER: TIntegerField;
    TableMasterMAX_PTO_MINUTE: TIntegerField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    TableDetailLINE_NUMBER: TIntegerField;
    TableDetailFROMEMPYEARS: TIntegerField;
    TableDetailTILLEMPYEARS: TIntegerField;
    TableDetailPTOMINUTE: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailPTOHOURMINCALC: TStringField;
    TableMasterMAX_PTO_MINUTE_CALC: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure TableMasterCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FPTOHour: Integer;
    FContractGroupCodeOriginal: String;
    FContractGroupLineOriginal: String;
  public
    { Public declarations }
    function PTODefFor(AContractGroupCode: String): Boolean;
    procedure CopyPTODef(ASrcContractGroupCode,
      ADestContractGroupCode: String; APTODefDefined: Boolean);
    property PTOHour: Integer read FPTOHour write FPTOHour;
    property ContractGroupCodeOriginal: String read FContractGroupCodeOriginal write FContractGroupCodeOriginal;
    property ContractGroupLineOriginal: String read FContractGroupLineOriginal write FContractGroupLineOriginal;
  end;

var
  PTODefDM: TPTODefDM;

implementation

uses SystemDMT, UPimsMessageRes;

{$R *.DFM}

{ TPTODefDM }

function TPTODefDM.PTODefFor(AContractGroupCode: String): Boolean;
var
  HoursFound: Integer;
begin
  HoursFound := SystemDM.GetDBValue(
    Format('SELECT COUNT(*) FROM PTODEFINITION O ' +
           ' WHERE O.CONTRACTGROUP_CODE = ''%s''', [AContractGroupCode]),
           0);
  Result := HoursFound <> 0;
end;

procedure TPTODefDM.CopyPTODef(ASrcContractGroupCode,
  ADestContractGroupCode: String; APTODefDefined: Boolean);
begin
  if APTODefDefined then
    SystemDM.ExecSql(
      Format('DELETE FROM PTODEFINITION O ' +
             ' WHERE O.CONTRACTGROUP_CODE = ''%s''', [ADestContractGroupCode])
             );
  SystemDM.ExecSql(
    Format(
      'INSERT INTO PTODEFINITION ' +
      '  (CONTRACTGROUP_CODE, LINE_NUMBER, ' +
      '  FROMEMPYEARS, TILLEMPYEARS, PTOMINUTE, CREATIONDATE, MUTATIONDATE, ' +
      '  MUTATOR) ' +
      'SELECT ''%s'', O.LINE_NUMBER, ' +
      '  O.FROMEMPYEARS, O.TILLEMPYEARS, O.PTOMINUTE, SYSDATE, SYSDATE, ' +
      '  ''%s'' ' +
      'FROM PTODEFINITION O ' +
      'WHERE O.CONTRACTGROUP_CODE = ''%s'' ',
    [ADestContractGroupCode, SystemDM.CurrentProgramUser , ASrcContractGroupCode]
    )
  );
  TableDetail.Refresh;
end;

procedure TPTODefDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  ContractGroupCodeOriginal := '';
  ContractGroupLineOriginal := '';
end;

procedure TPTODefDM.DefaultNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetail.FieldByName('LINE_NUMBER').AsInteger := 0;
  TableDetail.FieldByName('FROMEMPYEARS').AsInteger:= 0;
  TableDetail.FieldByName('TILLEMPYEARS').AsInteger:= 0;
  TableDetail.FieldByName('PTOMINUTE').AsInteger:= 0;
end;

procedure TPTODefDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if TableDetail.FieldByName('PTOMINUTE').AsString <> '' then
    TableDetail.FieldByName('PTOHOURMINCALC').AsString :=
      Format('%.2d:%.2d',[(TableDetail.FieldByName('PTOMINUTE').AsInteger div 60),
      (TableDetail.FieldByName('PTOMINUTE').AsInteger mod 60)]);
end;

procedure TPTODefDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  TableDetail.FieldByName('PTOMINUTE').AsInteger := PTOHour;
end;

procedure TPTODefDM.TableMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  // GLOB3-204
  if TableMaster.FieldByName('MAX_PTO_MINUTE').AsString <> '' then
    TableMaster.FieldByName('MAX_PTO_MINUTE_CALC').AsString :=
      Format('%.2d:%.2d',[(TableMaster.FieldByName('MAX_PTO_MINUTE').AsInteger div 60),
      (TableMaster.FieldByName('MAX_PTO_MINUTE').AsInteger mod 60)]);

end;

end.
