inherited DataCollectionEntryF: TDataCollectionEntryF
  Left = 479
  Top = 130
  Width = 671
  Height = 574
  Caption = 'Data Collection Entry'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 220
    Height = 50
    Align = alNone
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 46
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 45
    end
  end
  inherited pnlDetail: TPanel
    Top = 418
    Width = 655
    Height = 117
    TabOrder = 3
    OnClick = pnlDetailClick
    OnDblClick = pnlDetailDblClick
    object GrpBoxAutoDetails: TGroupBox
      Left = 1
      Top = 1
      Width = 653
      Height = 115
      Align = alClient
      Caption = 'Details'
      TabOrder = 0
      object GroupBox4: TGroupBox
        Left = 2
        Top = 15
        Width = 255
        Height = 98
        Align = alLeft
        Caption = 'Workspot / Jobcode'
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 20
          Width = 46
          Height = 13
          Caption = 'Workspot'
        end
        object Label8: TLabel
          Left = 8
          Top = 44
          Width = 40
          Height = 13
          Caption = 'Jobcode'
        end
        object lblWorkspotDescriptionAuto: TLabel
          Left = 169
          Top = 20
          Width = 132
          Height = 13
          Caption = 'lblWorkspotDescriptionAuto'
        end
        object Label14: TLabel
          Left = 8
          Top = 68
          Width = 22
          Height = 13
          Caption = 'Shift'
        end
        object DBLookupCmbBoxJobcodeAuto: TDBLookupComboBox
          Tag = 1
          Left = 80
          Top = 40
          Width = 167
          Height = 21
          DataField = 'JOB_CODE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          DropDownRows = 4
          DropDownWidth = 250
          KeyField = 'JOB_CODE'
          ListField = 'JOB_CODE;DESCRIPTION'
          ListSource = DataCollectionEntryDM.DataSourceJobcodeAuto
          TabOrder = 1
        end
        object dxDBExtLookupEditWorkspotAuto: TdxDBExtLookupEdit
          Tag = 1
          Left = 80
          Top = 16
          Width = 81
          Style.BorderStyle = xbs3D
          TabOrder = 0
          OnEnter = dxDBExtLookupEditWorkspotAutoEnter
          OnExit = dxDBExtLookupEditWorkspotAutoExit
          OnKeyPress = dxDBExtLookupEditWorkspotAutoKeyPress
          DataField = 'WORKSPOT_CODE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          ImmediateDropDown = True
          PopupHeight = 100
          PopupWidth = 271
          OnPopup = dxDBExtLookupEditWorkspotAutoPopup
          DBGridLayout = dxDBGridLayoutListWorkspotAuto
        end
        object DBLookupComboBox1: TDBLookupComboBox
          Tag = 1
          Left = 80
          Top = 64
          Width = 166
          Height = 21
          DataField = 'SHIFT_NUMBER'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          DropDownRows = 4
          DropDownWidth = 250
          KeyField = 'SHIFT_NUMBER'
          ListField = 'SHIFT_NUMBER;DESCRIPTION'
          ListSource = DataCollectionEntryDM.DataSourceShiftAuto
          TabOrder = 2
        end
      end
      object GroupBox5: TGroupBox
        Left = 257
        Top = 15
        Width = 311
        Height = 98
        Align = alClient
        Caption = 'Date / Time'
        TabOrder = 1
        object Label9: TLabel
          Left = 8
          Top = 19
          Width = 82
          Height = 13
          Caption = 'Start Date / Time'
        end
        object Label10: TLabel
          Left = 8
          Top = 43
          Width = 76
          Height = 13
          Caption = 'End Date / Time'
        end
        object Label12: TLabel
          Left = 8
          Top = 66
          Width = 48
          Height = 13
          Caption = 'Shift Date'
        end
        object dxDBDateEditStartDateAuto: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 16
          Width = 120
          TabOrder = 0
          DataField = 'START_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          OnChange = dxDBDateEditStartDateAutoChange
          DateButtons = [btnToday]
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBDateEditEndDateAuto: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 40
          Width = 120
          TabOrder = 1
          DataField = 'END_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          OnChange = dxDBDateEditEndDateAutoChange
          DateButtons = [btnToday]
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBDateEditShiftDateAuto: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 63
          Width = 120
          TabOrder = 2
          DataField = 'SHIFT_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          DateButtons = [btnToday]
          SaveTime = False
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBTimeEditStartTimeAuto: TdxDBTimeEdit
          Tag = 1
          Left = 218
          Top = 16
          Width = 90
          TabOrder = 3
          DataField = 'START_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
        object dxDBTimeEditEndTimeAuto: TdxDBTimeEdit
          Tag = 1
          Left = 218
          Top = 40
          Width = 90
          TabOrder = 4
          DataField = 'END_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
      end
      object GroupBox6: TGroupBox
        Left = 568
        Top = 15
        Width = 83
        Height = 98
        Align = alRight
        Caption = 'Quantity'
        TabOrder = 2
        object DBEditQuantityAuto: TDBEdit
          Tag = 1
          Left = 8
          Top = 16
          Width = 65
          Height = 21
          DataField = 'QUANTITY'
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          TabOrder = 0
        end
      end
    end
    object GrpBoxManualDetails: TGroupBox
      Left = 1
      Top = 1
      Width = 653
      Height = 115
      Align = alClient
      Caption = 'Details'
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 2
        Top = 15
        Width = 255
        Height = 98
        Align = alLeft
        Caption = 'Workspot / Jobcode'
        TabOrder = 0
        object Label3: TLabel
          Left = 8
          Top = 20
          Width = 46
          Height = 13
          Caption = 'Workspot'
        end
        object Label4: TLabel
          Left = 8
          Top = 44
          Width = 40
          Height = 13
          Caption = 'Jobcode'
        end
        object lblWorkspotDescriptionManual: TLabel
          Left = 168
          Top = 20
          Width = 143
          Height = 13
          Caption = 'lblWorkspotDescriptionManual'
        end
        object Label13: TLabel
          Left = 8
          Top = 68
          Width = 22
          Height = 13
          Caption = 'Shift'
        end
        object DBLookupCmbBoxJobcodeManual: TDBLookupComboBox
          Tag = 1
          Left = 80
          Top = 40
          Width = 166
          Height = 21
          DataField = 'JOB_CODE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          DropDownRows = 4
          DropDownWidth = 250
          KeyField = 'JOB_CODE'
          ListField = 'JOB_CODE;DESCRIPTION'
          ListSource = DataCollectionEntryDM.DataSourceJobcodeManual
          TabOrder = 1
        end
        object dxDBExtLookupEditWorkspotManual: TdxDBExtLookupEdit
          Tag = 1
          Left = 80
          Top = 16
          Width = 81
          Style.BorderStyle = xbs3D
          TabOrder = 0
          OnEnter = dxDBExtLookupEditWorkspotManualEnter
          OnExit = dxDBExtLookupEditWorkspotManualExit
          OnKeyPress = dxDBExtLookupEditWorkspotManualKeyPress
          DataField = 'WORKSPOT_CODE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          ImmediateDropDown = True
          PopupHeight = 100
          PopupWidth = 271
          OnPopup = dxDBExtLookupEditWorkspotManualPopup
          DBGridLayout = dxDBGridLayoutListWorkspotManual
        end
        object DBLookupCmbBoxShiftMan: TDBLookupComboBox
          Tag = 1
          Left = 80
          Top = 64
          Width = 166
          Height = 21
          DataField = 'SHIFT_NUMBER'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          DropDownRows = 4
          DropDownWidth = 250
          KeyField = 'SHIFT_NUMBER'
          ListField = 'SHIFT_NUMBER;DESCRIPTION'
          ListSource = DataCollectionEntryDM.DataSourceShiftManual
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 257
        Top = 15
        Width = 312
        Height = 98
        Align = alClient
        Caption = 'Date / Time'
        TabOrder = 1
        object Label5: TLabel
          Left = 8
          Top = 19
          Width = 82
          Height = 13
          Caption = 'Start Date / Time'
        end
        object Label7: TLabel
          Left = 8
          Top = 43
          Width = 76
          Height = 13
          Caption = 'End Date / Time'
        end
        object Label11: TLabel
          Left = 8
          Top = 67
          Width = 48
          Height = 13
          Caption = 'Shift Date'
        end
        object dxDBDateEditStartDateManual: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 16
          Width = 120
          TabOrder = 0
          DataField = 'START_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          DateButtons = [btnToday]
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBDateEditEndDateManual: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 40
          Width = 120
          TabOrder = 1
          DataField = 'END_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          DateButtons = [btnToday]
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBDateEditShiftDateManual: TdxDBDateEdit
          Tag = 1
          Left = 92
          Top = 63
          Width = 120
          TabOrder = 2
          DataField = 'SHIFT_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          DateButtons = [btnToday]
          SaveTime = False
          UseEditMask = True
          StoredValues = 4
        end
        object dxDBTimeEditStartTimeManual: TdxDBTimeEdit
          Tag = 1
          Left = 218
          Top = 16
          Width = 90
          TabOrder = 3
          DataField = 'START_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
        object dxDBTimeEditEndTimeManual: TdxDBTimeEdit
          Tag = 1
          Left = 218
          Top = 40
          Width = 90
          TabOrder = 4
          DataField = 'END_DATE'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          TimeEditFormat = tfHourMin
          StoredValues = 4
        end
      end
      object GroupBox3: TGroupBox
        Left = 569
        Top = 15
        Width = 82
        Height = 98
        Align = alRight
        Caption = 'Quantity'
        TabOrder = 2
        object DBEditQuantityManual: TDBEdit
          Tag = 1
          Left = 8
          Top = 16
          Width = 65
          Height = 21
          DataField = 'QUANTITY'
          DataSource = DataCollectionEntryDM.dsQryPQManual
          TabOrder = 0
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 270
    Height = 50
    Align = alNone
    Visible = False
    inherited spltDetail: TSplitter
      Top = 46
    end
    inherited dxDetailGrid: TdxDBGrid
      Height = 45
    end
  end
  object pnlMasterSelection: TPanel [4]
    Left = 0
    Top = 26
    Width = 655
    Height = 80
    Align = alTop
    TabOrder = 7
    object grpBoxPlantDate: TGroupBox
      Left = 1
      Top = 1
      Width = 653
      Height = 78
      Align = alClient
      Caption = 'Plant / Date'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 19
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label2: TLabel
        Left = 280
        Top = 19
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object dxDBExtLookupEditPlant: TdxDBExtLookupEdit
        Left = 36
        Top = 16
        Width = 241
        TabOrder = 0
        DataField = 'DESCRIPTION'
        DataSource = DataCollectionEntryDM.DataSourceMaster
        OnChange = dxDBExtLookupEditPlantChange
        OnSelectionChange = dxDBExtLookupEditPlantSelectionChange
        OnCloseUp = dxDBExtLookupEditPlantCloseUp
        DBGridLayout = dxDBGridLayoutList1Item1
      end
      object BtnAccept: TBitBtn
        Left = 505
        Top = 12
        Width = 64
        Height = 25
        Caption = '&Refresh '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = btnAcceptClick
      end
      object ButtonAddWK: TButton
        Left = 572
        Top = 12
        Width = 81
        Height = 25
        Caption = 'Add workspots'
        TabOrder = 2
        OnClick = ButtonAddWKClick
      end
      object RadioGroupShiftDate: TRadioGroup
        Left = 308
        Top = 37
        Width = 191
        Height = 35
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Shift Date'
          'Date')
        TabOrder = 3
        OnClick = RadioGroupShiftDateClick
      end
      object DateEditFrom: TDateTimePicker
        Left = 308
        Top = 16
        Width = 193
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 4
        OnChange = DateEditFromChange
      end
    end
  end
  object pnlPages: TPanel [5]
    Left = 0
    Top = 106
    Width = 655
    Height = 312
    Align = alClient
    TabOrder = 8
    object PageControl1: TPageControl
      Tag = 1
      Left = 1
      Top = 1
      Width = 653
      Height = 310
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Manual Datacollection'
        object dxDBGridProductionQuantityManual: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 0
          Width = 645
          Height = 282
          Bands = <
            item
              Caption = 'Workspot'
            end
            item
              Caption = 'Jobcode'
            end
            item
              Caption = 'Date / Time'
            end
            item
              Caption = 'Quantity'
            end
            item
              Caption = 'Shift'
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'MYROWID_CAL'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          OnDblClick = dxDetailGridDblClick
          OnEnter = dxGridEnter
          DataSource = DataCollectionEntryDM.dsQryPQManual
          HideSelectionTextColor = clWindowText
          OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxGridCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          object dxDBGridProductionQuantityManualColumnWORKSPOT_CODE: TdxDBGridLookupColumn
            Caption = 'Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_CODELU'
            DropDownWidth = 300
            ListFieldName = 'WORKSPOT_CODE;DESCRIPTION'
          end
          object dxDBGridProductionQuantityManualColumnJOB_CODE: TdxDBGridLookupColumn
            Caption = 'Code'
            BandIndex = 1
            RowIndex = 0
            FieldName = 'JOBCODELU'
            DropDownWidth = 300
            ListFieldName = 'JOB_CODE;DESCRIPTION'
          end
          object dxDBGridProductionQuantityManualColumnWORKSPOT_DESCRIPTION: TdxDBGridLookupColumn
            Caption = 'Description'
            DisableEditor = True
            ReadOnly = True
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_DESCRIPTIONLU'
            DropDownWidth = 300
            ListFieldName = 'DESCRIPTION;WORKSPOT_CODE'
          end
          object dxDBGridProductionQuantityManualColumnSTARTTIME: TdxDBGridDateColumn
            Caption = 'Start'
            BandIndex = 2
            RowIndex = 0
            FieldName = 'START_DATE'
          end
          object dxDBGridProductionQuantityManualColumnJOB_DESCRIPTION: TdxDBGridLookupColumn
            Caption = 'Description'
            DisableEditor = True
            ReadOnly = True
            BandIndex = 1
            RowIndex = 0
            FieldName = 'JOBDESC_CAL'
            DropDownWidth = 300
            ListFieldName = 'DESCRIPTION;JOB_CODE'
          end
          object dxDBGridProductionQuantityManualColumnENDTIME: TdxDBGridDateColumn
            Caption = 'End'
            BandIndex = 2
            RowIndex = 0
            FieldName = 'END_DATE'
          end
          object dxDBGridProductionQuantityManualColumnQUANTITY: TdxDBGridColumn
            Caption = 'Quantity'
            BandIndex = 3
            RowIndex = 0
            FieldName = 'QUANTITY'
          end
          object dxDBGridProductionQuantityManualColumnPQPLANT_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'PLANT_CODE'
          end
          object dxDBGridProductionQuantityManualColumnPQWORKSPOT_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'WORKSPOT_CODE'
          end
          object dxDBGridProductionQuantityManualColumnPQJOB_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'JOB_CODE'
          end
          object dxDBGridProductionQuantityManualColumnSHIFT_NUMBER: TdxDBGridColumn
            Caption = 'Number'
            BandIndex = 4
            RowIndex = 0
            FieldName = 'SHIFT_NUMBER'
          end
          object dxDBGridProductionQuantityManualColumnSHIFTDESCLU: TdxDBGridColumn
            Caption = 'Description'
            BandIndex = 4
            RowIndex = 0
            FieldName = 'SHIFT_DESC_LU'
          end
          object dxDBGridProductionQuantityManualColumnSHIFT_DATE: TdxDBGridDateColumn
            Caption = 'Shift Date'
            BandIndex = 4
            RowIndex = 0
            FieldName = 'SHIFT_DATE'
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Automatic Datacollection'
        ImageIndex = 1
        object dxDBGridProductionQuantityAuto: TdxDBGrid
          Tag = 1
          Left = 0
          Top = 0
          Width = 645
          Height = 282
          Bands = <
            item
              Caption = 'Workspot'
            end
            item
              Caption = 'Jobcode'
            end
            item
              Caption = 'Date / time'
            end
            item
              Caption = 'Quantity'
            end
            item
              Caption = 'Automatic collection'
            end
            item
              Caption = 'Shift'
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 1
          KeyField = 'MYROWID_CAL'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          OnDblClick = dxDetailGridDblClick
          OnEnter = dxGridEnter
          DataSource = DataCollectionEntryDM.dsQryPQAuto
          HideSelectionTextColor = clWindowText
          OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDblClick, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
          OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
          ShowBands = True
          OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
          OnCustomDrawBand = dxGridCustomDrawBand
          OnCustomDrawCell = dxGridCustomDrawCell
          OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
          OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
          object dxDBGridProductionQuantityAutoColumnWORKSPOT_CODE: TdxDBGridLookupColumn
            Caption = 'Code'
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_CODELU'
            DropDownWidth = 300
            ListFieldName = 'WORKSPOT_CODE;DESCRIPTION'
          end
          object dxDBGridProductionQuantityAutoColumnJOB_CODE: TdxDBGridLookupColumn
            Caption = 'Code'
            BandIndex = 1
            RowIndex = 0
            FieldName = 'JOBCODELU'
            DropDownWidth = 300
            ListFieldName = 'JOB_CODE;DESCRIPTION'
          end
          object dxDBGridProductionQuantityAutoColumnWORKSPOT_DESCRIPTION: TdxDBGridLookupColumn
            Caption = 'Description'
            DisableEditor = True
            ReadOnly = True
            BandIndex = 0
            RowIndex = 0
            FieldName = 'WORKSPOT_DESCRIPTIONLU'
            DropDownWidth = 300
            ListFieldName = 'DESCRIPTION;WORKSPOT_CODE'
          end
          object dxDBGridProductionQuantityAutoColumnSTARTDATE: TdxDBGridDateColumn
            Caption = 'Start'
            BandIndex = 2
            RowIndex = 0
            FieldName = 'START_DATE'
          end
          object dxDBGridProductionQuantityAutoColumnJOB_DESCRIPTION: TdxDBGridLookupColumn
            Caption = 'Description'
            DisableEditor = True
            ReadOnly = True
            BandIndex = 1
            RowIndex = 0
            FieldName = 'JOBDESC_CAL'
            DropDownWidth = 300
            ListFieldName = 'DESCRIPTION;JOB_CODE'
          end
          object dxDBGridProductionQuantityAutoColumnENDDATE: TdxDBGridDateColumn
            Caption = 'End'
            BandIndex = 2
            RowIndex = 0
            FieldName = 'END_DATE'
          end
          object dxDBGridProductionQuantityAutoColumnQUANTITY: TdxDBGridColumn
            Caption = 'Quantity'
            BandIndex = 3
            RowIndex = 0
            FieldName = 'QUANTITY'
          end
          object dxDBGridProductionQuantityAutoColumnAUTO_QUANTITY: TdxDBGridColumn
            Caption = 'Quantity'
            ReadOnly = True
            BandIndex = 4
            RowIndex = 0
            FieldName = 'AUTOMATIC_QUANTITY'
          end
          object dxDBGridProductionQuantityAutoColumnCOUNTER_VALUE: TdxDBGridColumn
            Caption = 'Counter Value'
            ReadOnly = True
            BandIndex = 4
            RowIndex = 0
            FieldName = 'COUNTER_VALUE'
          end
          object dxDBGridProductionQuantityAutoColumnPQPLANT_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'PLANT_CODE'
          end
          object dxDBGridProductionQuantityAutoColumnPQJOB_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'JOB_CODE'
          end
          object dxDBGridProductionQuantityAutoColumnPQWORKSPOT_CODE: TdxDBGridColumn
            DisableCustomizing = True
            Visible = False
            BandIndex = 0
            RowIndex = 0
            DisableGrouping = True
            FieldName = 'WORKSPOT_CODE'
          end
          object dxDBGridProductionQuantityAutoColumnSHIFT_NUMBER: TdxDBGridColumn
            Caption = 'Number'
            BandIndex = 5
            RowIndex = 0
            FieldName = 'SHIFT_NUMBER'
          end
          object dxDBGridProductionQuantityAutoColumnSHIFTDESCLU: TdxDBGridColumn
            Caption = 'Description'
            BandIndex = 5
            RowIndex = 0
            FieldName = 'SHIFT_DESC_LU'
          end
          object dxDBGridProductionQuantityAutoColumnSHIFT_DATE: TdxDBGridDateColumn
            Caption = 'Shift Date'
            BandIndex = 5
            RowIndex = 0
            FieldName = 'SHIFT_DATE'
          end
        end
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 440
    Top = 96
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonRecordDetails: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 504
    Top = 96
  end
  inherited StandardMenuActionList: TActionList
    Left = 636
    Top = 4
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 473
    Top = 97
    object dxDBGridLayoutList1Item1: TdxDBGridLayout
      Data = {
        4B010000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0605506C616E7400000D44656661756C744C61796F75
        74091348656164657250616E656C526F77436F756E7402010D53756D6D617279
        47726F7570730E001053756D6D617279536570617261746F7206022C200A4461
        7461536F75726365072644617461436F6C6C656374696F6E456E747279444D2E
        44617461536F757263654D6173746572000F546478444247726964436F6C756D
        6E07436F6C756D6E310743617074696F6E0604434F44450942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060A504C414E545F
        434F444500000F546478444247726964436F6C756D6E07436F6C756D6E320942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0B4445534352495054494F4E000000}
    end
  end
  object dxDBGridLayoutListWorkspot: TdxDBGridLayoutList
    Left = 432
    Top = 59
    object dxDBGridLayoutListWorkspotManual: TdxDBGridLayout
      Data = {
        2A020000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0608576F726B73706F7400000D44656661756C744C61
        796F7574091348656164657250616E656C526F77436F756E740201084B657946
        69656C64060D574F524B53504F545F434F44450D53756D6D61727947726F7570
        730E001053756D6D617279536570617261746F7206022C200A44617461536F75
        726365072E44617461436F6C6C656374696F6E456E747279444D2E4461746153
        6F75726365576F726B73706F744D616E75616C094F7074696F6E7344420B1065
        64676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564
        676F43616E496E73657274116564676F43616E4E617669676174696F6E116564
        676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72
        6473106564676F557365426F6F6B6D61726B730000135464784442477269644D
        61736B436F6C756D6E0D574F524B53504F545F434F44450743617074696F6E06
        04436F646505576964746802450942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060D574F524B53504F545F434F4445000013
        5464784442477269644D61736B436F6C756D6E0B4445534352495054494F4E07
        43617074696F6E060B4465736372697074696F6E05576964746803B800094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060B
        4445534352495054494F4E000000}
    end
    object dxDBGridLayoutListWorkspotAuto: TdxDBGridLayout
      Data = {
        27020000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0608576F726B73706F7400000D44656661756C744C61
        796F7574091348656164657250616E656C526F77436F756E740201084B657946
        69656C64060D574F524B53504F545F434F44450D53756D6D61727947726F7570
        730E001053756D6D617279536570617261746F7206022C200A44617461536F75
        726365072C44617461436F6C6C656374696F6E456E747279444D2E4461746153
        6F75726365576F726B73706F744175746F094F7074696F6E7344420B10656467
        6F43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F
        43616E496E73657274116564676F43616E4E617669676174696F6E116564676F
        436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F726473
        106564676F557365426F6F6B6D61726B730000135464784442477269644D6173
        6B436F6C756D6E0D574F524B53504F545F434F44450743617074696F6E060443
        6F646505576964746802280942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060D574F524B53504F545F434F44450000135464
        784442477269644D61736B436F6C756D6E0B4445534352495054494F4E074361
        7074696F6E060B4465736372697074696F6E055769647468027E0942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060B444553
        4352495054494F4E000000}
    end
  end
end
