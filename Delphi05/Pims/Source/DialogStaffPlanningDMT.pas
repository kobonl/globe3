(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:18-JAN-2013. 20013910.
    - Plan on departments gives problems.
    MRA:26-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
*)
unit DialogStaffPlanningDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, StaffPlanningUnit, Provider, DBClient;

type

  TDialogStaffPlanningDM = class(TGridBaseDM)
    QueryShiftPlant: TQuery;
    QueryWK: TQuery;
    QueryWork: TQuery;
    StoredProcReadPastDay: TStoredProc;
    StoredProcReadSTDPLNXXX: TStoredProc;
    StoredProcSELECTEMAXXX: TStoredProc;
    QueryFillEMA: TQuery;
    QueryRDP: TQuery;
    StoredProcWriteInRDP: TStoredProc;
    QueryEPL: TQuery;
    StoredProcFillEPL: TStoredProc;
    QueryWKDesc: TQuery;
    QueryTeam: TQuery;
    ClientDataSetTeam: TClientDataSet;
    DataSetProviderTeam: TDataSetProvider;
    QueryPlant: TQuery;
    ClientDataSetPlant: TClientDataSet;
    DataSetProviderPlant: TDataSetProvider;
    ClientDataSetShift: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    StoredProcDELETEEPLNSTD: TStoredProc;
    QueryPlantTeam: TQuery;
    ClientDataSetPlantPLANT_CODE: TStringField;
    ClientDataSetPlantDESCRIPTION: TStringField;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    FListSPL: TStringList;
    procedure FillWK(AllTeam: Boolean;
     TeamFrom, TeamTo, Plant: String; SortWK, SelectionType: Integer;
     DateSelection: TDateTime; Shift, Day: Integer);
    procedure ProcessReadSTDPLNIntoDayPLN(PlantFrom, PlantTo, TeamFrom, TeamTo: String;
      AllTeam: Boolean; DateFrom, DateTo: TDateTime);
    procedure WriteIntoRDP(PlantFrom,PlantTo, TeamFrom, TeamTo: String;
      AllTeam: Boolean; DateFrom, DateTo: TDateTime);
    function CheckTableRDP(PlantFrom, PlantTo, TeamFrom, TeamTo: String;
     AllTeam: Boolean; DateFrom, DateTo: TDateTime): Boolean;

    procedure ProcessReadPastIntoDayPLN(PlantFrom, PlantTo, TeamFrom, TeamTo: String;
     AllTeam: Boolean; DateFrom, DateTo, DateSourceFrom, DateSourceTo: TDateTime);
    procedure ProcessDayPlanning(Plant: String;
       Shift, Empl, Day: Integer; DateEMA: TDateTime; TB: TTBLevel);
    procedure FillListEMAProcess(PlantFrom, PlantTo, TeamFrom, TeamTo: String;
      AllTeam: Boolean; DateSourceFrom, DateSourceTo: TDateTime;
      DayStart, DayEnd, DayTmp: Integer);
    procedure FillAbsDesc;
  end;

var
  DialogStaffPlanningDM: TDialogStaffPlanningDM;

implementation

{$R *.DFM}

uses
  UPimsConst, SystemDMT, ListProcsFRM, StaffPlanningEMPDMT,
  StaffPlanningOCIDMT;

procedure TDialogStaffPlanningDM.FillWK(AllTeam: Boolean;
  TeamFrom, TeamTo, Plant: String;
  SortWK, SelectionType: Integer; DateSelection: TDateTime;
  Shift, Day: Integer);
var
  SelectStr, WKCode, WKDesc, DeptCode: String;
  OccWK, i : Integer;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  DISTINCT W.WORKSPOT_CODE AS CODE1, ' + NL +
    '  CAST(W.SHORT_NAME AS VARCHAR(30)) AS DESCRIPTION, ' + NL +
    '  W.DESCRIPTION AS LONGDESC, D.DEPARTMENT_CODE AS CODE2,  ' + NL +
    '  D.PLAN_ON_WORKSPOT_YN ' + NL +
    'FROM ' + NL +
    '  WORKSPOT W, DEPARTMENT D ' + NL;
  if (not AllTeam) and (TeamFrom <> '') and (TeamTo <> '') then
  begin
     SelectStr := SelectStr + ' , DEPARTMENTPERTEAM T ' + NL;
     //CAR 550274 - team selection
     if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
     begin
       SelectStr := SelectStr + ' ,TEAMPERUSER U ' + NL +
         ' WHERE T.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' + NL;
     end
     else
       SelectStr := SelectStr + ' WHERE ' + NL;
     SelectStr := SelectStr +
       ' T.TEAM_CODE >= ''' + DoubleQuote(TeamFrom) + '''' + NL +
       ' AND T.TEAM_CODE <= ''' + DoubleQuote(TeamTo)  + '''' + NL +
       ' AND T.PLANT_CODE = ''' + DoubleQuote(Plant) + '''' + NL +
       ' AND T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ' + NL;
  end
  else
  begin
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', DEPARTMENTPERTEAM T , TEAMPERUSER U ' + NL +
        ' WHERE T.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' + NL +
        ' W.DEPARTMENT_CODE = T.DEPARTMENT_CODE AND ' + NL +
        ' W.PLANT_CODE = T.PLANT_CODE AND ' + NL
    else
      SelectStr := SelectStr + ' WHERE ' + NL;
  end;
  SelectStr :=
    SelectStr + ' D.PLANT_CODE = ''' + DoubleQuote(Plant) + '''' + NL +
      ' AND W.PLANT_CODE = ''' + DoubleQuote(Plant) + '''' + NL +
      ' AND D.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL +
      ' AND D.PLAN_ON_WORKSPOT_YN = ''Y''' + NL;
  if SelectionType = 0 then
    SelectStr := SelectStr + ' AND ( (W.DATE_INACTIVE IS NULL) ' + NL +
      ' OR ((W.DATE_INACTIVE IS NOT NULL) AND ' + NL +
      ' (W.DATE_INACTIVE >= :FDATE)) )' + NL;
//department table
  SelectStr := SelectStr + ' UNION  ' + NL +
    'SELECT ' + NL +
    '  DISTINCT D.DEPARTMENT_CODE AS CODE1, ' + NL +
    '  D.DESCRIPTION AS DESCRIPTION, D.DESCRIPTION AS LONGDESC, ' + NL +
    '  D.DEPARTMENT_CODE AS CODE2, ' + NL +
  //Bug 16-1-2004
    '  D.PLAN_ON_WORKSPOT_YN ' + NL +
    'FROM ' + NL +
    '  DEPARTMENT D' + NL;
  if (not AllTeam) and (TeamFrom <> '') and (TeamTo <> '') then
  begin
    SelectStr := SelectStr + ' , DEPARTMENTPERTEAM T ' + NL;
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', TEAMPERUSER U ' + NL +
        ' WHERE T.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' + NL
    else
      SelectStr := SelectStr + ' WHERE ' + NL;
    SelectStr := SelectStr +
      ' T.TEAM_CODE >= ''' + DoubleQuote(TeamFrom) + '''' + NL +
      ' AND T.TEAM_CODE <= ''' + DoubleQuote(TeamTo) + '''' + NL +
      ' AND T.PLANT_CODE = ''' + DoubleQuote(Plant) + '''' + NL +
      ' AND T.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND ' + NL;
  end
  else
  begin
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', DEPARTMENTPERTEAM T , TEAMPERUSER U ' + NL +
        ' WHERE T.TEAM_CODE = U.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' + NL +
        ' T.DEPARTMENT_CODE = T.DEPARTMENT_CODE AND ' + NL +
        ' T.PLANT_CODE = T.PLANT_CODE AND ' + NL
    else
      SelectStr := SelectStr + ' WHERE ' + NL;
  end;
  SelectStr :=
    SelectStr + ' D.PLANT_CODE =  ''' + DoubleQuote(Plant) + '''' + NL +
    ' AND D.PLAN_ON_WORKSPOT_YN = ''N''' + NL;
  if SortWK = 0 then
    SelectStr := SelectStr + ' ORDER BY 1' + NL
  else
    SelectStr := SelectStr + ' ORDER BY 2' + NL;
  QueryWK.Close;
  QueryWK.SQL.Clear;
  QueryWK.SQL.Add(UpperCase(SelectStr));
// QueryWK.SQL.SaveToFile('c:\temp\dsp.txt');
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryWK.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  if SelectionType = 0 then
    QueryWK.ParamByName('FDATE').AsDateTime := DateSelection;
  QueryWK.Open;

  FWKList.Clear;
  FDeptList.Clear;

  FWKDescList.Clear;
  if QueryWK.RecordCount = 0 then
    Exit;
// Fill lists FWKList; WKDescList; DeptList
  QueryWK.First;

  if SelectionType = 0 then
   Day := ListProcsF.DayWStartOnFromDate(DateSelection);
  StaffPlanningOCIDM.OpenQuerySTOC(Plant, Shift, Day);
  while True do
  begin
    WKCode := QueryWK.FieldByNAME('CODE1').AsString;
    DeptCode := QueryWK.FieldByNAME('CODE2').AsString;
    if QueryWK.FieldByName('PLAN_ON_WORKSPOT_YN').AsString = 'N' then
    begin
      DeptCode := WKCode;
      WKCode := DummyStr;
    end;
    OccWK := -1;
    if (FShowWKWithOcc) then
    begin
      OccWK := 0;
      if StaffPlanningOCIDM.ClientDataSetSTOC.FindKey([DeptCode, WKCode]) then
      begin
        for i:= 1 to SystemDM.MaxTimeblocks do
        begin
          OccWK := OccWK + StaffPlanningOCIDM.ClientDataSetSTOC.
            FieldByName('OCC_A_TIMEBLOCK_' + IntToStr(i)).AsInteger +
            StaffPlanningOCIDM.ClientDataSetSTOC.FieldByName('OCC_B_TIMEBLOCK_' +
              IntToStr(i)).AsInteger +
            StaffPlanningOCIDM.ClientDataSetSTOC.FieldByName('OCC_C_TIMEBLOCK_' +
              IntToStr(i)).AsInteger;
        end;
      end;
    end;
    WKDesc := QueryWK.FieldByNAME('DESCRIPTION').AsString;
    if (OccWK <> 0) then
    begin
      FWKDescList.Add(WKDesc + CHR_SEP + QueryWK.FieldByNAME('LONGDESC').AsString);
      FWKList.Add(WKCode);
      FDeptList.Add(DeptCode);
    end;
    QueryWK.Next;
    if QueryWK.Eof then
      Exit;
  end;
end; // FillWK

procedure TDialogStaffPlanningDM.FillAbsDesc;
begin
  if FABSList.Count > 0 then
    Exit;
  with StaffPlanningEMPDM do
  begin
    ClientDataSetAbsRsn.First;
    while not ClientDataSetAbsRsn.Eof do
    begin
      if FABSLIST.IndexOf(ClientDataSetAbsRsn.FieldByName('ABSENCEREASON_CODE').AsString) < 0 then
        FABSList.Add(ClientDataSetAbsRsn.FieldByName('ABSENCEREASON_CODE').AsString)
      else
      // when the letter is small - already exists save with a '0' at the end
        FABSList.Add(ClientDataSetAbsRsn.FieldByName('ABSENCEREASON_CODE').AsString + '0');
// fill desc in absdesclist
      FABSDescList.Add(ClientDataSetAbsRsn.FieldByName('DESCRIPTION').AsString);
      ClientDataSetAbsRsn.Next;
    end;
  end;
end; // FillAbsDesc

procedure TDialogStaffPlanningDM.ProcessReadPastIntoDayPLN(PlantFrom, PlantTo,
  TeamFrom, TeamTo: String; AllTeam: Boolean;
  DateFrom, DateTo, DateSourceFrom, DateSourceTo: TDateTime);
var
  DateNow: TDateTime;
begin
  DateNow := Now;
  ReplaceTime(DateNow, EncodeTime(0,0,0,0));
  ReplaceTime(DateFrom, EncodeTime(0,0,0,0));
  ReplaceTime(DateTo, EncodeTime(0,0,0,0));
  ReplaceTime(DateSourceFrom, EncodeTime(0,0,0,0));
  ReplaceTime(DateSourceTo, EncodeTime(0,0,0,0));
  if AllTeam then
  begin
    TeamFrom := '*';
    TeamTo := '*';
  end;
  StoredProcReadPastDay.ParamByName('PLANTFROM').AsString := PlantFrom;
  StoredProcReadPastDay.ParamByName('PLANTTO').AsString := PlantTo;
  StoredProcReadPastDay.ParamByName('TEAMFROM').AsString := TeamFrom;
  StoredProcReadPastDay.ParamByName('TEAMTO').AsString := TeamTo;
  StoredProcReadPastDay.ParamByName('DATEFROM').AsDateTime := DATEFrom;
  StoredProcReadPastDay.ParamByName('DATETO').AsDateTime := DATETo;
  StoredProcReadPastDay.ParamByName('DATENOW').AsDateTime := DateNow;
  StoredProcReadPastDay.ParamByName('DATESOURCEFROM').AsDateTime := DateSourceFrom;
  StoredProcReadPastDay.ParamByName('DATESOURCETO').AsDateTime := DateSourceTo;
  StoredProcReadPastDay.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
  // car 550274
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    StoredProcReadPastDay.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser
  else
    StoredProcReadPastDay.ParamByName('USER_NAME').AsString := '*';
  StoredProcReadPastDay.Prepare;
  StoredProcReadPastDay.ExecProc;
// process Epl
end; // ProcessReadPastIntoDayPLN

procedure TDialogStaffPlanningDM.ProcessReadSTDPLNIntoDayPLN(PlantFrom,
  PlantTo, TeamFrom, TeamTo: String; AllTeam: Boolean; DateFrom, DateTo: TDateTime);
var
  DayStart, DayEnd, DaySPL, EmplSPL, ShiftSPL, DayTmp, i: Integer;
  PlantSPL, DeptSPL, WKSPL: String;
  TB: TTBLevel;
  DateNow: TDateTime;
begin
  FListSPL.Clear;
  DateNow := Now;
  DayStart := ListProcsF.DayWStartOnFromDate(DateFrom);
  DayEnd := ListProcsF.DayWStartOnFromDate(DateTo);
  if ListProcsF.DateDifferenceDays(DateFrom, DateTo) >= 7 then
  begin
    DayStart := 1;
    DayEnd := 7;
    DayTmp := 0;
  end
  else
  begin
    if DayStart <= DayEnd then
      DayTmp := 0
    else
    begin
      DayTmp := DayEnd;
      DayEnd := 7;
    end;
  end;
  ReplaceTime(DateNow, EncodeTime(0,0,0,0));
  ReplaceTime(DATEFrom, EncodeTime(0,0,0,0));
  ReplaceTime(DATETo, EncodeTime(0,0,0,0));
  if AllTeam then
  begin
    TeamFrom := '*';
    TeamTo := '*';
  end;
  QueryWork.Active := False;
  QueryWork.UniDirectional := False;
  QueryWork.SQL.Clear;
  // Pims -> Oracle
  // FIRST CALL PROC SINCE THIS WAS DONE IN ONCE IN INTERBASE
  with StoredProcDELETEEPLNSTD do
  begin
    ParamByName('PLANTFROM').AsString   := PlantFrom;
    ParamByName('PLANTTO').AsString     := PlantTo;
    ParamByName('TEAMFROM').AsString    := TEAMFrom;
    ParamByName('TEAMTO').AsString      := TEAMTo;
    ParamByName('DATEFROM').AsDateTime  := DATEFrom;
    ParamByName('DATETO').AsDateTime    := DATETo;
    if SystemDM.UserTeamSelectionYN  = CHECKEDVALUE then
      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      ParamByName('USER_NAME').AsString := '*';
    if not Prepared then
      Prepare;
    ExecProc;
  end;
  // 20013910 Use other query for Plan On Departments
  //          Do not INNER JOIN with workspots or it will find nothing
  //          when using Plan On Departments.
  //          Because then workspot code is '0'.
  QueryWork.SQL.Add(
    'SELECT' + NL +
    '  S.PLANT_CODE, S.EMPLOYEE_NUMBER,' + NL +
    '  S.SHIFT_NUMBER, S.DAY_OF_WEEK,' + NL +
    '  S.DEPARTMENT_CODE, S.WORKSPOT_CODE,' + NL +
    '  S.SCHEDULED_TIMEBLOCK_1, S.SCHEDULED_TIMEBLOCK_2,' + NL +
    '  S.SCHEDULED_TIMEBLOCK_3, S.SCHEDULED_TIMEBLOCK_4,' + NL +
    '  S.SCHEDULED_TIMEBLOCK_5, S.SCHEDULED_TIMEBLOCK_6,' + NL +
    '  S.SCHEDULED_TIMEBLOCK_7, S.SCHEDULED_TIMEBLOCK_8,' + NL +
    '  S.SCHEDULED_TIMEBLOCK_9, S.SCHEDULED_TIMEBLOCK_10' + NL +
    '  FROM ' + NL +
    '    STANDARDEMPLOYEEPLANNING S INNER JOIN DEPARTMENT D ON ' + NL +
    '      S.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '      S.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
    '    LEFT JOIN WORKSPOT W ON ' + NL +
    '      S.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '      S.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '    LEFT JOIN DEPARTMENTPERTEAM T ON ' + NL +
    '      S.PLANT_CODE = T.PLANT_CODE AND ' + NL +
    '      S.DEPARTMENT_CODE = T.DEPARTMENT_CODE ' + NL +
    '    INNER JOIN EMPLOYEE E ON ' + NL +
    '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE' + NL +
    '  S.PLANT_CODE >= :PLANTFROM AND' + NL +
    '  S.PLANT_CODE <= :PLANTTO AND' + NL +
    '  (' + NL +
    '    (' + NL +
    '      (S.DAY_OF_WEEK >= :DAYSTART) AND' + NL +
    '      (S.DAY_OF_WEEK <= :DAYEND)' + NL +
    '    ) OR' + NL +
    '    (' + NL +
    '      (S.DAY_OF_WEEK >= 1) AND' + NL +
    '      (S.DAY_OF_WEEK <= :DAYTMP) AND' + NL +
    '      (:DAYTMP <> 0)' + NL +
    '    )' + NL +
    '  ) AND' + NL +
    '  ((D.PLAN_ON_WORKSPOT_YN = ''N'')  OR ((D.PLAN_ON_WORKSPOT_YN   = ''Y'') AND' + NL +
    '  ((W.DATE_INACTIVE IS NULL) OR (W.DATE_INACTIVE > :DATENOW)))) AND' + NL +
    '  ((E.TEAM_CODE >= :TEAMFROM) OR' + NL +
    '  (:TEAMFROM = ''*'')) AND' + NL +
    '  (' + NL +
    '    E.TEAM_CODE <= :TEAMTO OR' + NL +
    '    :TEAMTO = ''*''' + NL +
    '  ) AND' + NL +
    '  (' + NL +
    '    T.TEAM_CODE >= :TEAMFROM OR' + NL +
    '    :TEAMFROM = ''*''' + NL +
    '  ) AND' + NL +
    '  (' + NL +
    '    T.TEAM_CODE <= :TEAMTO OR' + NL +
    '    :TEAMTO = ''*''' + NL +
    '  ) AND' + NL +
    '  (' + NL +
    '    (:USER_NAME = ''*'') OR' + NL +
    '    (' + NL +
    '      (:USER_NAME <> ''*'') AND' + NL +
    '      (E.TEAM_CODE IN' + NL +
    '        (' + NL +
    '        SELECT U.TEAM_CODE' + NL +
    '        FROM TEAMPERUSER U' + NL +
    '        WHERE' + NL +
    '           E.TEAM_CODE = U.TEAM_CODE AND' + NL +
    '           U.USER_NAME = :USER_NAME' + NL +
    '        )' + NL +
    '      ) AND' + NL +
    '      (T.TEAM_CODE IN' + NL +
    '        (' + NL +
    '        SELECT U.TEAM_CODE' + NL +
    '        FROM TEAMPERUSER U' + NL +
    '        WHERE' + NL +
    '          T.TEAM_CODE = U.TEAM_CODE AND' + NL +
    '          U.USER_NAME = :USER_NAME)' + NL +
    '        )' + NL +
    '      )' + NL +
    '   )' + NL +
    'ORDER BY S.PLANT_CODE, S.SHIFT_NUMBER, S.EMPLOYEE_NUMBER, S.DAY_OF_WEEK');
  QueryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
  QueryWork.ParamByName('PLANTTO').AsString := PlantTo;
  QueryWork.ParamByName('TEAMFROM').AsString := TEAMFrom;
  QueryWork.ParamByName('TEAMTO').AsString := TEAMTo;
  QueryWork.ParamByName('DATENOW').AsDateTime := DateNow;
  QueryWork.ParamByName('DAYSTART').AsInteger := DayStart;
  QueryWork.ParamByName('DAYEND').AsInteger := DayEnd;
  QueryWork.ParamByName('DAYTMP').AsInteger := DayTmp;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryWork.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryWork.ParamByName('USER_NAME').AsString := '*';

  if not QueryWork.Prepared then
     QueryWork.Prepare;
// QueryWork.SQL.SaveToFile('c:\temp\ReadSTDPLNIntoDayPLN.txt');
  QueryWork.Active := True;
  QueryWork.First;
  while not QueryWork.Eof do
  begin
    DaySPL := QueryWork.FieldByName('DAY_OF_WEEK').AsInteger;
    PlantSPL := QueryWork.FieldByNAME('PLANT_CODE').AsString;
    DeptSPL := QueryWork.FieldByNAME('DEPARTMENT_CODE').AsString;
    WKSPL := QueryWork.FieldByNAME('WORKSPOT_CODE').AsString;
    EmplSPL := QueryWork.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
    ShiftSPL := QueryWork.FieldByNAME('SHIFT_NUMBER').AsInteger;
    for i := 1 to MAX_TBS do
      TB[i] := ' ';
    for i := 1 to SystemDM.MaxTimeblocks do
      TB[i] := QueryWork.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
    FListSPL.Add(PlantSPL + CHR_SEP + IntToStr(ShiftSPL) + CHR_SEP +
     IntToStr(EmplSPL) + CHR_SEP + IntToStr(DaySPL) + CHR_SEP +
     DeptSPL + CHR_SEP +  WKSPL + CHR_SEP +
     TB[1] + CHR_SEP + TB[2] + CHR_SEP +
     TB[3] + CHR_SEP + TB[4] + CHR_SEP +
     TB[5] + CHR_SEP + TB[6] + CHR_SEP +
     TB[7] + CHR_SEP + TB[8] + CHR_SEP +
     TB[9] + CHR_SEP + TB[10]+ CHR_SEP);
    QueryWork.Next;
  end;
  if not QueryWork.IsEmpty then
    FillListEMAProcess(PlantFrom, PlantTo, TeamFrom, TeamTo, AllTeam,
      DateFrom, DateTo, DayStart, DayEnd, DayTmp);
end; // ProcessReadSTDPLNIntoDayPLN

procedure TDialogStaffPlanningDM.FillListEMAProcess(PlantFrom, PlantTo,
  TeamFrom, TeamTo: String; AllTeam: Boolean;
  DateSourceFrom, DateSourceTo: TDateTime;
  DayStart, DayEnd, DayTmp: Integer);
var
  i, DayEma: Integer;
  EmplEMA, ShiftEMA: Integer;
  DateEMA: TDateTime;
  PlantEma: String;
  TB: TTBLevel;
  { (TB[1] <> 'N') or (TB[2] <> 'N') or (TB[3] <> 'N') or (TB[4] <> 'N') }
  function TBCheck: Boolean;
  var
    Index: Integer;
  begin
    Result := False;
    Index := 1;
    while (not Result) and (Index <= SystemDM.MaxTimeblocks) do
    begin
      if (TB[Index] <> 'N') then
        Result := True;
      inc(Index);
    end;
  end; // TBCheck
begin
  if AllTeam then
  begin
    TeamFrom := '*'; TeamTo := '*';
  end;
  ReplaceTime(DateSourceFrom, EncodeTime(0,0,0,0));
  ReplaceTime(DateSourceTo, EncodeTime(0,0,0,0));
  QueryFillEMA.Active := False;
  QueryFillEMA.UniDirectional := False;
  QueryFillEMA.SQL.Clear;
  QueryFillEMA.SQL.Add(
    'SELECT' + NL +
    '  A.PLANT_CODE, A.SHIFT_NUMBER, A.EMPLOYEE_NUMBER,' + NL +
    '  A.EMPLOYEEAVAILABILITY_DATE,' + NL +
    '  A.AVAILABLE_TIMEBLOCK_1, A.AVAILABLE_TIMEBLOCK_2,' + NL +
    '  A.AVAILABLE_TIMEBLOCK_3, A.AVAILABLE_TIMEBLOCK_4,' + NL +
    '  A.AVAILABLE_TIMEBLOCK_5, A.AVAILABLE_TIMEBLOCK_6,' + NL +
    '  A.AVAILABLE_TIMEBLOCK_7, A.AVAILABLE_TIMEBLOCK_8,' + NL +
    '  A.AVAILABLE_TIMEBLOCK_9, A.AVAILABLE_TIMEBLOCK_10' + NL +
    'FROM EMPLOYEEAVAILABILITY A, EMPLOYEE E' + NL +
    'WHERE' + NL +
    '  A.PLANT_CODE >= :PLANTFROM AND' + NL +
    '  A.PLANT_CODE <= :PLANTTO AND' + NL +
    '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND' + NL +
    '  (E.TEAM_CODE >= :TEAMFROM OR :TEAMFROM = ''*'') AND' + NL +
    '  (E.TEAM_CODE <= :TEAMTO OR :TEAMTO = ''*'') AND' + NL +
    '  (E.STARTDATE <= A.EMPLOYEEAVAILABILITY_DATE AND' + NL +
    '  (E.ENDDATE >= A.EMPLOYEEAVAILABILITY_DATE OR E.ENDDATE IS NULL)) AND' + NL +
    '  (' + NL +
    '    :USER_NAME = ''*'' OR' + NL +
    '    (' + NL +
    '      :USER_NAME <> ''*'' AND' + NL +
    '      (E.TEAM_CODE IN' + NL +
    '        (' + NL +
    '        SELECT U.TEAM_CODE' + NL +
    '        FROM TEAMPERUSER U' + NL +
    '        WHERE U.USER_NAME = :USER_NAME AND E.TEAM_CODE = U.TEAM_CODE' + NL +
    '        )' + NL +
    '      )' + NL +
    '    )' + NL +
    '  ) AND' + NL +
    '  A.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND' + NL +
    '  A.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND' + NL +
    '  (' + NL +
    '    A.AVAILABLE_TIMEBLOCK_1 = ''*'' OR A.AVAILABLE_TIMEBLOCK_2 = ''*'' OR' + NL +
    '    A.AVAILABLE_TIMEBLOCK_3 = ''*'' OR A.AVAILABLE_TIMEBLOCK_4 = ''*'' OR' + NL +
    '    A.AVAILABLE_TIMEBLOCK_5 = ''*'' OR A.AVAILABLE_TIMEBLOCK_6 = ''*'' OR' + NL +
    '    A.AVAILABLE_TIMEBLOCK_7 = ''*'' OR A.AVAILABLE_TIMEBLOCK_8 = ''*'' OR' + NL +
    '    A.AVAILABLE_TIMEBLOCK_9 = ''*'' OR A.AVAILABLE_TIMEBLOCK_10 = ''*''' + NL +
    '  )' + NL +
    'ORDER BY' + NL +
    '  A.PLANT_CODE, A.SHIFT_NUMBER, A.EMPLOYEE_NUMBER');

  QueryFillEMA.ParamByName('PLANTFROM').AsString := PlantFrom;
  QueryFillEMA.ParamByName('PLANTTO').AsString := PlantTo;
  QueryFillEMA.ParamByName('TEAMFROM').AsString := TeamFrom;
  QueryFillEMA.ParamByName('TEAMTO').AsString := TeamTo;
  QueryFillEMA.ParamByName('DATEFROM').AsDateTime := DateSourceFrom;
  QueryFillEMA.ParamByName('DATETO').AsDateTime := DateSourceTo;

//CAR 550274 - team selection
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryFillEMA.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryFillEMA.ParamByName('USER_NAME').AsString := '*';

  if not QueryFillEMA.Prepared then
     QueryFillEMA.Prepare;
  QueryFillEMA.Active := True;
  QueryFillEMA.First;
  while not QueryFillEMA.Eof do
  begin
    DateEMA := QueryFillEMA.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
    DayEMA := ListProcsF.DayWStartOnFromDate(DateEMA);
    PlantEMA := QueryFillEMA.FieldByNAME('PLANT_CODE').AsString;
    EmplEMA := QueryFillEMA.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
    ShiftEMA := QueryFillEMA.FieldByNAME('SHIFT_NUMBER').AsInteger;
    for i:= 1 to MAX_TBS do
      TB[I] := ' ';
    for i := 1 to SystemDM.MaxTimeblocks do
      if QueryFillEMA.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(i)).AsString <> '*' then
        TB[I] := 'N';

    if ((DayEMA >= DayStart) and (DayEma <= DayEnd)) or
      ((DAYEMA >=1) and (DayEma <= DayTmp) and (DayTmp <> 0)) then
    begin
// scan EPL => SET TB
      if TBCheck then
      begin
        QueryEPL.Close;
        QueryEPL.ParamByName('DATEEMA').AsDateTime := DateEMA;
        QueryEPL.ParamByName('EMPLOYEE_NUMBER').AsInteger := EmplEMA;
        QueryEPL.ParamByName('SHIFT_NUMBER').AsInteger := ShiftEMA;
        QueryEPL.ParamByName('PLANT_CODE').AsString := PlantEMA;
        IF NOT QueryEPL.Prepared then
          QueryEPL.Prepare;
        QueryEPL.OPEN;
        QueryEPL.First;
        while not QueryEPL.eof do
        begin
          for i := 1 to SystemDM.MaxTimeblocks do
           if (QueryEPL.FieldByName('SCHEDULED_TIMEBLOCK_' +
             IntToStr(i)).AsString <> 'N')  then
             TB[i] := 'N';
          QueryEPL.Next;
        end;
      end;
      if TBCheck then
        ProcessDayPlanning(PlantEMA, ShiftEMA, EmplEMA, DayEMA, DateEMA, TB);
    end;
    QueryFillEMA.Next;
  end;
end; // FillListEMAProcess

procedure TDialogStaffPlanningDM.ProcessDayPlanning(Plant: String;
   Shift, Empl, Day: Integer; DateEMA: TDateTime; TB: TTBLevel);
var
  i, j, PosStr: Integer;
  StrTmp, DeptSPL, WKSPL: String;
  Found: Boolean;
  TBTmp: TTBLevel;
  function TBCheck: Boolean;
  var
    Index: Integer;
  begin
    Result := False;
    Index := 1;
    while (not Result) and (Index <= SystemDM.MaxTimeblocks) do
    begin
      if (TBTmp[Index] <> 'N') then
        Result := True;
      inc(Index);
    end;
  end; // TBCheck
begin
  Found := False;
  for i := 0 to FListSPL.Count - 1 do
  begin
    StrTmp := Plant + CHR_SEP + IntToStr(Shift) + CHR_SEP +
    IntToStr(Empl) + CHR_SEP + IntToStr(Day);
    for j := 1 to MAX_TBS do
     TBTmp[j] := TB[j];
    PosStr := Pos(StrTmp, FListSPL.Strings[i]);
    if Found and (Pos(STrTmp, FListSPL.Strings[i]) = 0 ) then
      exit;
    if PosStr = 1 then
    begin
      Found := True;
      StrTmp := Copy(FListSPL.Strings[i], Length(StrTmp) + 2,
        length(FListSPL.Strings[i]) + 1);
      Posstr := Pos(CHR_SEP, StrTmp);
      DEPTSPL := Copy(StrTmp, 0, PosStr - 1);
      StrTmp := Copy(StrTmp, PosStr + 1, length(StrTmp ) + 1);
      PosStr := Pos(CHR_SEP, StrTmp);
      WKSPL := Copy(StrTmp, 0 ,PosStr -1);
      for j := 1 to SystemDM.MaxTimeblocks do
      begin
        StrTmp := Copy(StrTmp, PosStr + 1, length(StrTmp ) + 1);
        PosStr := Pos(CHR_SEP, StrTmp);
        if TBTmp[J] <> 'N' then
          TBTmp[j] := Copy(StrTmp, 0, PosStr - 1);
      end;
      if TBCheck then
      begin
        StoredProcFillEPL.ParamByName('PLANT').AsString := Plant;
        StoredProcFillEPL.ParamByName('WORKSPOT').AsString := WKSPL;
        StoredProcFillEPL.ParamByName('DEPARTMENT').AsString := DEPTSPL;
        StoredProcFillEPL.ParamByName('EMPLOYEE').AsInteger := Empl;
        StoredProcFillEPL.ParamByName('SHIFT').AsInteger := Shift;
        StoredProcFillEPL.ParamByName('DATEEPL').AsDateTime := GetDate(DATEEMA);
        StoredProcFillEPL.ParamByName('DATENOW').AsDateTime := Now;
        StoredProcFillEPL.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        for j := 1 to MAX_TBS do
          StoredProcFillEPL.ParamByName('SCHEDULED_TIMEBLOCK_'+IntToStr(j)).AsString := TBTmp[j];
        StoredProcFillEPL.Prepare;
        StoredProcFillEPL.ExecProc;
      end;
    end;//if
  end;//for
end; // ProcessDayPlanning

function TDialogStaffPlanningDM.CheckTableRDP(PlantFrom, PlantTo,
  TeamFrom, TeamTo: String; AllTeam: Boolean;
  DateFrom, DateTo: TDateTime): Boolean;
begin
  if AllTeam then
  begin
    TeamFrom := '';
    TeamTo := 'zzzzzz';
  end;
  QueryRDP.Close;
  QueryRDP.ParamBYName('PLANTFROM').AsSTRING := PLANTFROM;
  QueryRDP.ParamBYName('PLANTTO').AsSTRING := PLANTTO;
  QueryRDP.ParamBYName('TEAMFROM').AsSTRING := TEAMFROM;
  QueryRDP.ParamBYName('TEAMTO').AsSTRING := TEAMTO;
  QueryRDP.ParamBYName('DATEFROM').AsDateTime := GetDate(DateFrom);
  QueryRDP.ParamBYName('DATETO').AsDateTime := GetDate(DateTo);
 // QueryRDP.Prepare;
  QueryRDP.Open;
  Result := (QueryRDP.FieldBYname('RECCOUNT').AsInteger > 0);
end; // CheckTableRDP

procedure TDialogStaffPlanningDM.WriteIntoRDP(PlantFrom, PlantTo,
  TeamFrom, TeamTo: String; AllTeam: Boolean;
  DateFrom, DateTo: TDateTime);
//var
//  DateTmp: TDateTime;
//  Plant: String;
begin
  if AllTeam then
  begin
    TeamFrom := 'A';TeamTo := 'zzzzzz';
  end;
  ReplaceTime(DateFrom, EncodeTime(0,0,0,0));
  ReplaceTime(DateTo, EncodeTime(0,0,0,0));
  StoredProcWriteInRDP.ParamByName('PLANTFROM').AsString := PlantFrom;
  StoredProcWriteInRDP.ParamByName('PLANTTO').AsString := PlantTo;
  StoredProcWriteInRDP.ParamByName('TEAMFROM').AsString := TeamFrom;
  StoredProcWriteInRDP.ParamByName('TEAMTO').AsString := TeamTo;
  StoredProcWriteInRDP.ParamByName('DATEFROM').AsDateTime := DATEFrom;
  StoredProcWriteInRDP.ParamByName('DATETO').AsDateTime := DATETo;
  //CAR 550274 - team selection
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    StoredProcWriteInRDP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    StoredProcWriteInRDP.ParamByName('USER_NAME').AsString := '*';

  StoredProcWriteInRDP.Prepare;
  StoredProcWriteInRDP.ExecProc;
end; // WriteIntoRDP

procedure TDialogStaffPlanningDM.DataModuleDestroy(Sender: TObject);
var
  SelectStr: String;
begin
  inherited;

  FListSPL.Free;
  SelectStr := 'DELETE FROM PIVOTEMPSTAFFPLANNING WHERE ' +
    ' PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentComputerName + '''';
  ExecuteSql(QueryWork, SelectStr);
  SelectStr := 'DELETE FROM PIVOTOCISTAFFPLANNING WHERE ' +
    ' PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentComputerName + '''';
  ExecuteSql(QueryWork, SelectStr);
  ClientDataSetTeam.Close;
  ClientDataSetPlant.Close;
  ClientDataSetShift.Close;
  //CAR 550274 - Team selection
  QueryRDP.Close;
  QueryRDP.UnPrepare;
  if StoredProcDELETEEPLNSTD.Prepared then
    StoredProcDELETEEPLNSTD.UnPrepare;
end; // DataModuleDestroy

procedure TDialogStaffPlanningDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
//  ActiveTables(Self, True);

//  ClientDataSetPlant.Open;
  ClientDataSetShift.Open;
  FListSPL :=  TStringList.Create;
  //CAR 550274
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryTeam.Sql.Clear;
    QueryTeam.Sql.Add(
      'SELECT ' + NL +
      '  T.TEAM_CODE, T.DESCRIPTION ' + NL +
      'FROM ' + NL +
      '  TEAM T, TEAMPERUSER U ' + NL +
      'WHERE ' + NL +
      '  U.USER_NAME = :USER_NAME AND ' + NL +
      '  T.TEAM_CODE = U.TEAM_CODE ' + NL +
      'ORDER BY ' + NL +
      '  T.TEAM_CODE');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;

    QueryRDP.Sql.Clear;
    QueryRDP.Sql.Add(
      'SELECT ' + NL +
      '  COUNT(*) AS RECCOUNT ' + NL +
      'FROM ' + NL +
      '  READINTODAYPLANNING R ' + NL +
      'WHERE ' + NL +
      '  R.PLANT_CODE >= :PLANTFROM AND R.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  R.TEAM_CODE >= :TEAMFROM AND R.TEAM_CODE <=:TEAMTO AND ' + NL +
      '  R.READ_DATE >= :DATEFROM AND R.READ_DATE <= :DATETO AND ' + NL +
      '  (R.TEAM_CODE IN ' + NL +
      '  (SELECT ' + NL +
      '    U.TEAM_CODE ' + NL +
      '  FROM ' + NL +
      '    TEAMPERUSER U ' + NL +
      '  WHERE ' + NL +
      '    U.USER_NAME =:USER_NAME AND ' + NL +
      '    R.TEAM_CODE = U.TEAM_CODE)) ');
    QueryRDP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end;
  QueryRDP.Prepare;
  ClientDataSetTeam.Open;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlant);
  QueryPlant.Open;
  // RV050.8. When ClientDataSetPlant is used, it gives an error-message,
  //          about QueryPlant.Plant_code not found.
//  ClientDataSetPlant.Open;
end; // DataModuleCreate

// RV050.8.
procedure TDialogStaffPlanningDM.QueryPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end; // QueryPlantFilterRecord

end.

