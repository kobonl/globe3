inherited IgnoreInterfaceCodeF: TIgnoreInterfaceCodeF
  Left = 262
  Top = 145
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Ignore interface code'
  ClientHeight = 444
  ClientWidth = 698
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 698
    Height = 15
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 11
      Width = 696
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 696
      Height = 10
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'WorkSpot'
        end>
      Visible = False
      ShowBands = True
      object dxMasterGridColumn12: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 147
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        DisableEditor = True
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridLookupColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTLU'
      end
      object dxMasterGridColumn4: TdxDBGridLookupColumn
        Caption = 'Hour type'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxMasterGridColumn8: TdxDBGridDateColumn
        Caption = 'Date inative'
        DisableEditor = True
        MinWidth = 16
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
        DateValidation = True
      end
      object dxMasterGridColumn11: TdxDBGridCheckColumn
        Caption = 'Use job codes'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USE_JOBCODE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn5: TdxDBGridCheckColumn
        Caption = 'Automatic data colection'
        DisableEditor = True
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_DATACOL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn6: TdxDBGridCheckColumn
        Caption = 'Counter value'
        DisableEditor = True
        Width = 76
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_VALUE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn7: TdxDBGridCheckColumn
        Caption = 'Enter scan counter '
        DisableEditor = True
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn9: TdxDBGridCheckColumn
        Caption = 'Measure productivity'
        DisableEditor = True
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MEASURE_PRODUCTIVITY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn10: TdxDBGridCheckColumn
        Caption = 'Productive hour'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRODUCTIVE_HOUR_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 322
    Width = 698
    Height = 122
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 696
      Height = 120
      Align = alClient
      Caption = 'Interface codes'
      TabOrder = 0
      object Label6: TLabel
        Left = 8
        Top = 37
        Width = 123
        Height = 13
        Caption = 'Choose an interface code'
      end
      object ComboBoxInterface: TComboBox
        Left = 144
        Top = 32
        Width = 529
        Height = 21
        ItemHeight = 13
        TabOrder = 0
        Text = 'ComboBoxInterface'
        OnClick = ComboBoxInterfaceClick
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 41
    Width = 698
    Height = 281
    inherited spltDetail: TSplitter
      Top = 277
      Width = 696
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 696
      Height = 276
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Ignore interface codes'
          Width = 678
        end>
      KeyField = 'JOB_CODE'
      OnClick = dxDetailGridClick
      DataSource = WorkSpotDM.DataSourceIgnoreInterface
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Interface '
        DisableEditor = True
        Width = 156
        BandIndex = 0
        RowIndex = 0
        FieldName = 'INTERFACE_CODE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 744
  end
  inherited dsrcActive: TDataSource
    DataSet = WorkSpotDM.TableIgnoreInterface
    Left = 8
    Top = 80
  end
end
