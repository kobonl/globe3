(*
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:19-JUL-2011 RV095.1.
  - Workspot moved to DialogReportBase-form.
*)
unit DialogReportPlantStructure;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, jpeg;

type
  TComboBoxPlusReport = class(TComboBoxPlus);

  TDialogReportPlantStructureF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    GroupBoxShow: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    CheckBoxWKDetail: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    QueryBU: TQuery;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    CheckBoxIgnoreCodes: TCheckBox;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    CheckBoxCompareJobs: TCheckBox;
    CheckBoxJobDetail: TCheckBox;
    CheckBoxDeptPage: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
{    procedure FillWorkSpot; }
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportPlantStructureF: TDialogReportPlantStructureF;

// RV089.1.
function DialogReportPlantStructureForm: TDialogReportPlantStructureF;

implementation

{$R *.DFM}

uses
  SystemDMT, ReportPlantStructureDMT, ReportPlantStructure, ListProcsFRM;

// RV089.1.
var
  DialogReportPlantStructureF_HND: TDialogReportPlantStructureF;

// RV089.1.
function DialogReportPlantStructureForm: TDialogReportPlantStructureF;
begin
  if (DialogReportPlantStructureF_HND = nil) then
  begin
    DialogReportPlantStructureF_HND := TDialogReportPlantStructureF.Create(Application);
    with DialogReportPlantStructureF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportPlantStructureF_HND;
end;

// RV089.1.
procedure TDialogReportPlantStructureF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportPlantStructureF_HND <> nil) then
  begin
//    ReportPlantStructureDM.Free;
//    ReportPlantStructureQR.Free;
    DialogReportPlantStructureF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportPlantStructureF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportPlantStructureF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportPlantStructureQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    CheckBoxWKDetail.Checked,
    CheckBoxJobDetail.Checked,
    CheckBoxIgnoreCodes.Checked,
    CheckBoxCompareJobs.Checked,
    CheckBoxShowSelection.Checked,
    CheckBoxPagePlant.Checked,
    CheckBoxDeptPage.Checked,
    CheckBoxPageWK.Checked) then
    ReportPlantStructureQR.ProcessRecords;
end;
{
procedure TDialogReportPlantStructureF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
procedure TDialogReportPlantStructureF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, False, False, False, True, False);
  inherited;

{  FillWorkSpot; }

  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxWKDetail.Checked := False;
  CheckBoxJobDetail.Checked := False;
  CheckBoxIgnoreCodes.Checked := False;
  CheckBoxCompareJobs.Checked := False;
end;

procedure TDialogReportPlantStructureF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportPlantStructureDM := CreateReportDM(TReportPlantStructureDM);
  ReportPlantStructureQR := CreateReportQR(TReportPlantStructureQR);
end;

procedure TDialogReportPlantStructureF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
      ComboBoxPlusWorkSpotFrom.DisplayValue := ComboBoxPlusWorkSpotTo.DisplayValue;
}
end;

procedure TDialogReportPlantStructureF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
     ComboBoxPlusWorkSpotTo.DisplayValue := ComboBoxPlusWorkSpotFrom.DisplayValue;
}
end;

procedure TDialogReportPlantStructureF.FillAll;
begin
  inherited;
{  FillWorkSpot; }
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportPlantStructureF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportPlantStructureF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
