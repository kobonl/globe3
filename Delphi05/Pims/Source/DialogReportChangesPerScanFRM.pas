(*
  MRA:12-MAR-2013 20014037 New report Changes per Scan
  - This report shows information based on a table that stores logging-info
    during changes made to scans.
  MRA:3-APR-2013 20014037.60 Rework
  - Add option to filter on Pims User or Workstation or None (compared with
    mutator-field).
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogReportChangesPerScanFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls, SystemDMT, dxEdLib;

type
  TDialogReportChangesPerScanF = class(TDialogReportBaseF)
    Label2: TLabel;
    Label3: TLabel;
    DateFrom: TDateTimePicker;
    Label4: TLabel;
    DateTo: TDateTimePicker;
    LblFromPimsUser: TLabel;
    LblPimsUser: TLabel;
    CmbPlusPimsUserFrom: TComboBoxPlus;
    LblToPimsUser: TLabel;
    CmbPlusPimsUserTo: TComboBoxPlus;
    LblFromWorkstation: TLabel;
    LblWorkstation: TLabel;
    CmbPlusWorkstationFrom: TComboBoxPlus;
    LblToWorkstation: TLabel;
    CmbPlusWorkstationTo: TComboBoxPlus;
    qryPimsUser: TQuery;
    qryWorkstation: TQuery;
    pnlSelections: TPanel;
    GroupBox1: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    RadioGroupSortOn: TRadioGroup;
    RadioGroupFilterOn: TRadioGroup;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure RadioGroupFilterOnClick(Sender: TObject);
    procedure CmbPlusWorkstationFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkstationToCloseUp(Sender: TObject);
    procedure CmbPlusPimsUserFromCloseUp(Sender: TObject);
    procedure CmbPlusPimsUserToCloseUp(Sender: TObject);
  private
    { Private declarations }
    procedure UserVisible(AVisible: Boolean);
    procedure WorkstationVisible(AVisible: Boolean);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }
  end;

var
  DialogReportChangesPerScanF: TDialogReportChangesPerScanF;

function DialogReportChangesPerScanForm: TDialogReportChangesPerScanF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes, ReportChangesPerScanDMT, ReportChangesPerScanQRPT,
  ListProcsFRM;

var
  DialogReportChangesPerScanF_HND: TDialogReportChangesPerScanF;

function DialogReportChangesPerScanForm: TDialogReportChangesPerScanF;
begin
  if (DialogReportChangesPerScanF_HND = nil) then
  begin
    DialogReportChangesPerScanF_HND := TDialogReportChangesPerScanF.Create(Application);
    with DialogReportChangesPerScanF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportChangesPerScanF_HND;
end;

procedure TDialogReportChangesPerScanF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportChangesPerScanF_HND <> nil) then
  begin
    DialogReportChangesPerScanF_HND := nil;
  end;
end;

procedure TDialogReportChangesPerScanF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportChangesPerScanF.FormShow(Sender: TObject);
var
  DateNow: TDateTime;
begin
  InitDialog(True, False, False, False, False, False, False);
  inherited;
  DateNow := Now();
  DateFrom.DateTime := DateNow;
  DateTo.DateTime := DateNow;
  RadioGroupFilterOnClick(Sender); // 20014037.60
end;

procedure TDialogReportChangesPerScanF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportChangesPerScanDM := CreateReportDM(TReportChangesPerScanDM);
  ReportChangesPerScanQR := CreateReportQR(TReportChangesPerScanQR);
  // 20014037.60
  if SystemDM.IsBCH then
    RadioGroupFilterOn.ItemIndex := 0 // Pims User
  else
    RadioGroupFilterOn.ItemIndex := 1; // Workstation
  ListProcsF.FillComboBoxMaster(qryPimsUser, 'USER_NAME', True, CmbPlusPimsUserFrom);
  ListProcsF.FillComboBoxMaster(qryPimsUser, 'USER_NAME', False, CmbPlusPimsUserTo);
  ListProcsF.FillComboBoxMaster(qryWorkstation, 'COMPUTER_NAME', True,
    CmbPlusWorkstationFrom);
  ListProcsF.FillComboBoxMaster(qryWorkstation, 'COMPUTER_NAME', False,
    CmbPlusWorkstationTo);
end;

procedure TDialogReportChangesPerScanF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportChangesPerScanF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportChangesPerScanF.FillAll;
begin
  inherited;
//
end;

procedure TDialogReportChangesPerScanF.btnOkClick(Sender: TObject);
var
  TmpDateFrom, TmpDateTo: TDateTime;
begin
  inherited;
  if DateFrom.DateTime > DateTo.DateTime then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbYes]);
    Exit;
  end;
  TmpDateFrom := GetDate(DateFrom.Date);
  TmpDateTo := GetDate(DateTo.Date) + 1;
  if ReportChangesPerScanQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      TmpDateFrom,
      TmpDateTo,
      RadioGroupSortOn.ItemIndex,
      CheckBoxShowSelection.Checked,
      CheckBoxExport.Checked,
      GetStrValue(CmbPlusPimsUserFrom.Value),
      GetStrValue(CmbPlusPimsUserTo.Value),
      GetStrValue(CmbPlusWorkstationFrom.Value),
      GetStrValue(CmbPlusWorkstationTo.Value),
      RadioGroupFilterOn.ItemIndex
      )
  then
    ReportChangesPerScanQR.ProcessRecords;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.UserVisible(AVisible: Boolean);
begin
  LblFromPimsUser.Visible := AVisible;
  LblPimsUser.Visible := AVisible;
  CmbPlusPimsUserFrom.Visible := AVisible;
  LblToPimsUser.Visible := AVisible;
  CmbPlusPimsUserTo.Visible := AVisible;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.WorkstationVisible(
  AVisible: Boolean);
begin
  LblFromWorkstation.Visible := AVisible;
  LblWorkstation.Visible := AVisible;
  CmbPlusWorkstationFrom.Visible := AVisible;
  LblToWorkstation.Visible := AVisible;
  CmbPlusWorkstationTo.Visible := AVisible;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.RadioGroupFilterOnClick(
  Sender: TObject);
begin
  inherited;
  case RadioGroupFilterOn.ItemIndex of
  0: // Pims User
    begin
      WorkstationVisible(False);
      UserVisible(True);
    end;
  1: // Workstation
    begin
      UserVisible(False);
      WorkstationVisible(True);
    end;
  2: // None
    begin
      UserVisible(False);
      WorkstationVisible(False);
    end;
  end;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.CmbPlusWorkstationFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (CmbPlusWorkstationFrom.DisplayValue <> '') and
     (CmbPlusWorkstationTo.DisplayValue <> '') then
  begin
    if CmbPlusWorkstationFrom.Value > CmbPlusWorkstationTo.Value then
      CmbPlusWorkstationTo.DisplayValue :=
        CmbPlusWorkstationFrom.DisplayValue;
  end;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.CmbPlusWorkstationToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (CmbPlusWorkstationFrom.DisplayValue <> '') and
     (CmbPlusWorkstationTo.DisplayValue <> '') then
  begin
    if CmbPlusWorkstationFrom.Value >  CmbPlusWorkstationTo.Value then
      CmbPlusWorkstationFrom.DisplayValue :=
        CmbPlusWorkstationTo.DisplayValue;
  end;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.CmbPlusPimsUserFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (CmbPlusPimsUserFrom.DisplayValue <> '') and
     (CmbPlusPimsUserTo.DisplayValue <> '') then
  begin
    if CmbPlusPimsUserFrom.Value > CmbPlusPimsUserTo.Value then
      CmbPlusPimsUserTo.DisplayValue :=
        CmbPlusPimsUserFrom.DisplayValue;
  end;
end;

// 20014037.60
procedure TDialogReportChangesPerScanF.CmbPlusPimsUserToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (CmbPlusPimsUserFrom.DisplayValue <> '') and
     (CmbPlusPimsUserTo.DisplayValue <> '') then
  begin
    if CmbPlusPimsUserFrom.Value >  CmbPlusPimsUserTo.Value then
      CmbPlusPimsUserFrom.DisplayValue :=
        CmbPlusPimsUserTo.DisplayValue;
  end;
end;

end.
