inherited WorkSpotPerStationDM: TWorkSpotPerStationDM
  OldCreateOrder = True
  Left = 210
  Top = 167
  Height = 509
  Width = 754
  inherited TableMaster: TTable
    TableName = 'WORKSTATION'
    Left = 96
    object TableMasterCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 32
    end
    object TableMasterLICENSEVERSION: TIntegerField
      FieldName = 'LICENSEVERSION'
    end
  end
  inherited TableDetail: TTable
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'COMPUTER_NAME;SEQUENCE_NUMBER'
    MasterFields = 'COMPUTER_NAME'
    TableName = 'WORKSPOTPERWORKSTATION'
    Left = 96
    Top = 121
    object TableDetailCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 32
    end
    object TableDetailSEQUENCE_NUMBER: TIntegerField
      DefaultExpression = '0'
      FieldName = 'SEQUENCE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableDetailWORKSPOTCAL: TStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'WORKSPOTCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailWORKSPOTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOTLU'
      LookupDataSet = TableWorkspotLU
      LookupKeyFields = 'PLANT_CODE;WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;WORKSPOT_CODE'
      Lookup = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Top = 117
  end
  inherited TableExport: TTable
    Left = 420
    Top = 332
  end
  inherited DataSourceExport: TDataSource
    Left = 336
    Top = 332
  end
  object TablePlant: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 96
    Top = 324
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableWorkspot: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'WORKSPOT'
    Left = 96
    Top = 192
    object TableWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 204
    Top = 256
  end
  object DataSourceWorkspot: TDataSource
    DataSet = TableWorkspot
    Left = 204
    Top = 192
  end
  object TableWorkspotDsc: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 336
    Top = 48
    object TableWorkspotDscPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDscDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotDscWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDscCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotDscDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDscMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotDscMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotDscUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotDscHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotDscMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotDscPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotDscQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotDscAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotDscCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotDscENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotDscDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object TableTmpPlant: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'DESCRIPTION'
    TableName = 'PLANT'
    Left = 96
    Top = 260
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object StringField6: TStringField
      FieldName = 'STATE'
    end
    object StringField7: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object IntegerField1: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object IntegerField2: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField9: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object IntegerField4: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object StoredProcWork: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 340
    Top = 112
  end
  object TableWorkStation: TTable
    BeforePost = DefaultBeforeDelete
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'WORKSTATION'
    Left = 344
    Top = 200
    object TableWorkStationCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      ReadOnly = True
      Required = True
      Size = 32
    end
    object TableWorkStationLICENSEVERSION: TIntegerField
      FieldName = 'LICENSEVERSION'
      ReadOnly = True
    end
    object TableWorkStationTIMERECORDING_YN: TStringField
      FieldName = 'TIMERECORDING_YN'
      ReadOnly = True
      Size = 1
    end
  end
  object DataSourceWorkStation: TDataSource
    DataSet = SystemDM.TableWorkStation
    Left = 464
    Top = 200
  end
  object TableWorkspotLU: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 96
    Top = 376
    object TableWorkspotLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotLUWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotLUCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotLUDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotLUMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotLUMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotLUUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotLUHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotLUMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotLUPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotLUQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotLUAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotLUCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotLUENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotLUDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableWorkspotLUAUTOMATIC_RESCAN_YN: TStringField
      FieldName = 'AUTOMATIC_RESCAN_YN'
      Size = 1
    end
    object TableWorkspotLUSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
    end
    object TableWorkspotLUGRADE: TIntegerField
      FieldName = 'GRADE'
    end
  end
  object qryWSPerWSCount: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT NVL(MAX(W.SEQUENCE_NUMBER), 0) WSCOUNT'
      'FROM WORKSPOTPERWORKSTATION W'
      'WHERE W.COMPUTER_NAME = :COMPUTER_NAME'
      ' '
      ' ')
    Left = 224
    Top = 368
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTER_NAME'
        ParamType = ptUnknown
      end>
  end
end
