(*
  Changes:
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Workspot-from-to was showing a wrong value. Cause was a missing
      'order by workspot_code' in query 'QueryWK'.
    MRA:12-MAR-2008 Order 550463, RV005.
      - Add plant-from-to selection to report.
      - Disabled (made non-visible) CheckBoxAllDate.
    SO: 05-JUL-2010 RV067.3. 550494
      PIMS User rights related issues
    MRA:27-JAN-2011 RV085.18. SC-50016412.
    - This must be changed, so it can show scans made in other
      plants. This will be done with an additional checkbox
      named 'Include scans made in other plants'.
      When this checkbox is checked, it will not filter on
      plant+workspots but only on plant+employees. It will
      also show the plantcode-info for each scan.
      When this checkbox is not checked, it works like before.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:18-JUL-2011 RV095.1. SO-2011798.
    - Addition of Department-selection.
    - Addition of second plant-selection:
      - When All-plants (for first plant-selection) is CHECKED,
        it makes second plant-selection VISIBLE. This is then
        used to make it possible to select departments and workspots
        per plant.
      - When All-plants (for first plant-selection) is UNCHECKED,
        then the second plant-selection is made INVISIBLE, and
        all is related to the plants of this first plant-selection.
    - Workspot-selection moved to DialogReportBaseFRM.
    MRA:3-OCT-2012 20013489 Overnight-Shift-System.
    - Changed text 'Date' to 'Shift Date', because output is now based
      on shifts. This means all scans for 1 shift can be shown, even
      when they continue next day.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:10-OCT-2013
    - Make use of Overnight-Shift-System optional.
    MRA:1-SEP-2014 TD-23699
    - Show optionally Shift, Dept and Job in report.
    MRA:5-JAN-2015 20015995
    - Add checkbox to show only SANA-employees.
    - Custom-made, only usable for VOS
*)
unit DialogReportCheckListScanFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxLayout, dxCntner, dxEditor,
  dxExEdtr, dxExGrEd, dxExELib, SystemDMT, dxEdLib;

type
  TDialogReportCheckListScanF = class(TDialogReportBaseF)
    Label1: TLabel;
    ComboBoxPlusWorkSpotFrom: TComboBoxPlus;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    Label2: TLabel;
    Label3: TLabel;
    DateFrom: TDateTimePicker;
    DateTo: TDateTimePicker;
    GroupBox1: TGroupBox;
    CheckBoxShowTotal: TCheckBox;
    Label4: TLabel;
    CheckBoxAllDate: TCheckBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroupProcessedYN: TRadioGroup;
    QueryWK: TQuery;
    QueryEmpl: TQuery;
    CheckBoxExport: TCheckBox;
    CheckBoxShowAvail: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    CheckBoxIncludeScansOtherPlants: TCheckBox;
    BevelPlantDeptWS: TBevel;
    CheckBoxShowDetails: TCheckBox;
    CheckBoxOnlySANAEmps: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxAllDateClick(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure ComboBoxPlusWorkSpotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure CheckBoxIncludeScansOtherPlantsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBoxAllPlantsClick(Sender: TObject);
  private
{    procedure FillWorkspots; }
    { Private declarations }
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }
    procedure DateVisible;
  end;

var
  DialogReportCheckListScanF: TDialogReportCheckListScanF;

// RV089.1.
function DialogReportCheckListScanForm: TDialogReportCheckListScanF;

implementation

{$R *.DFM}

uses
  ReportCheckListScanQRPT, ReportCheckListScanDMT, ListProcsFRM,
  UPimsMessageRes;

// RV089.1.
var
  DialogReportCheckListScanF_HND: TDialogReportCheckListScanF;

// RV089.1.
function DialogReportCheckListScanForm: TDialogReportCheckListScanF;
begin
  if (DialogReportCheckListScanF_HND = nil) then
  begin
    DialogReportCheckListScanF_HND := TDialogReportCheckListScanF.Create(Application);
    with DialogReportCheckListScanF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportCheckListScanF_HND;
end;

// RV089.1.
procedure TDialogReportCheckListScanF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportCheckListScanF_HND <> nil) then
  begin
    DialogReportCheckListScanF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportCheckListScanF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportCheckListScanF.btnOkClick(Sender: TObject);
var
  TmpDateFrom, TmpDateTo: TDateTime;
begin
  inherited;
  if not CheckBoxAllDate.Checked then
    if DateFrom.DateTime > DateTo.DateTime then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbYes]);
      Exit;
    end;
    TmpDateFrom := GetDate(DateFrom.Date);
    TmpDateTo := GetDate(DateTo.Date) + 1;

  if ReportCheckListScanQR.QRSendReportParameters(
      MyPlantFrom, // RV095.1.
      MyPlantTo, // RV095.1.
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkSpotFrom.Value), // RV095.1.
      GetStrValue(CmbPlusWorkSpotTo.Value), // RV095.1.
      TmpDateFrom,
      TmpDateTo,
      CheckBoxAllDate.Checked,
      CheckBoxShowTotal.Checked,
      CheckBoxShowSelection.Checked,
      RadioGroupProcessedYN.ItemIndex,
      CheckBoxExport.Checked,
      CheckBoxShowAvail.Checked,
      CheckBoxIncludeScansOtherPlants.Checked, // RV085.18.
      CheckBoxAllDepartments.Checked,
      CheckBoxAllPlants.Checked, // RV095.1.
      CheckBoxAllEmployees.Checked,
      CheckBoxShowDetails.Checked,  // TD-23699
      CheckBoxOnlySANAEmps.Checked) // 20015995
  then
    ReportCheckListScanQR.ProcessRecords;
end;

{
procedure TDialogReportCheckListScanF.FillWorkspots;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    QueryWK.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWK, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWK, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantTo.Value),
      'WORKSPOT_CODE', False);

    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}

procedure TDialogReportCheckListScanF.FormShow(Sender: TObject);
var
  DateNow: TDateTime;
begin
  InitDialog(True, True, False, True, False, True, False);
  // RV095.1.
  UsePlant2 := True;
  CheckBoxAllPlantsShow := True;
//  CheckBoxAllEmployeesShow := True;
  inherited;
{  ComboBoxPlusWorkSpotFrom.ShowSpeedButton := False;
  ComboBoxPlusWorkSpotFrom.ShowSpeedButton := True;
  ComboBoxPlusWorkSpotTo.ShowSpeedButton := False;
  ComboBoxPlusWorkSpotTo.ShowSpeedButton := True; }

  //RV067.3.
//  FillWorkspots;

  DateVisible;
  DateNow := Now();
  DateFrom.DateTime := DateNow;
  DateTo.DateTime :=DateNow;
  CheckBoxAllDate.Checked := False;
  CheckBoxShowSelection.Checked := True;
  CheckBoxShowTotal.Checked := True;
  RadioGroupProcessedYN.ItemIndex := 0;
  // RV095.1.
  CheckBoxAllPlantsClick(Sender);
end;

procedure TDialogReportCheckListScanF.FormCreate(Sender: TObject);
begin
  inherited;
//  CheckBoxAllDate.Checked := True;
  ReportCheckListScanDM := CreateReportDM(TReportCheckListScanDM);
  ReportCheckListScanQR := CreateReportQR(TReportCheckListScanQR);
  CheckBoxIncludeScansOtherPlantsClick(Sender); // RV085.18.
  if not SystemDM.UseShiftDateSystem then // 20014328
    Label3.Caption := SDate; // Date instead of Shift Date

  // 20015995
  CheckBoxOnlySANAEmps.Visible := SystemDM.IsVOS;
  if SystemDM.IsVOS then
    CheckBoxOnlySANAEmps.Caption :=  SPimsOnlySANAEmps;
end;

procedure TDialogReportCheckListScanF.DateVisible;
begin
  if CheckBoxAllDate.Checked then
  begin
    DateFrom.Visible := False;
    DateTo.Visible := False;
  end
  else
  begin
    DateFrom.Visible := True;
    DateTo.Visible := True;
  end;
end;

procedure TDialogReportCheckListScanF.CheckBoxAllDateClick(Sender: TObject);
begin
  inherited;
  DateVisible;
  CheckBoxShowAvail.Checked := False;
  CheckBoxShowAvail.Enabled := not CheckBoxAllDate.Checked;
end;

procedure TDialogReportCheckListScanF.DateToChange(Sender: TObject);
begin
  inherited;
  if (getdate(DateTo.Date) - getdate(DateFrom.Date)) = 0 then
  begin
    CheckBoxShowAvail.Checked := True;
    CheckBoxShowAvail.Enabled := True;
  end
  else
  begin
    CheckBoxShowAvail.Checked := False;
    CheckBoxShowAvail.Enabled := False;
  end;
end;

procedure TDialogReportCheckListScanF.ComboBoxPlusWorkSpotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{
  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
  begin
    if ComboBoxPlusWorkSpotFrom.Value > ComboBoxPlusWorkSpotTo.Value then
      ComboBoxPlusWorkSpotTo.DisplayValue :=
        ComboBoxPlusWorkSpotFrom.DisplayValue;
  end;
}
end;

procedure TDialogReportCheckListScanF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{
  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
  begin
    if ComboBoxPlusWorkSpotFrom.Value >  ComboBoxPlusWorkSpotTo.Value then
      ComboBoxPlusWorkSpotFrom.DisplayValue :=
        ComboBoxPlusWorkSpotTo.DisplayValue;
  end;
}
end;

procedure TDialogReportCheckListScanF.FillAll;
begin
  inherited;
{  FillWorkspots; }
  CheckBoxIncludeScansOtherPlantsClick(nil); // RV085.18.
end;

// RV085.18.
procedure TDialogReportCheckListScanF.CheckBoxIncludeScansOtherPlantsClick(
  Sender: TObject);
  procedure WorkspotSelectionVisible(AVisible: Boolean);
  begin
    // RV095.1. Use 'MyPlant'-function: Plant can come from 2 plant-selections!
    if (MyPlantFrom <> '') and
       (MyPlantFrom = MyPlantTo) then
    begin
      WorkspotVisible(AVisible);
{      ComboBoxPlusWorkspotFrom.Visible := AVisible;
      ComboBoxPlusWorkspotTo.Visible := AVisible; }
    end;
  end;
begin
  inherited;
  WorkspotSelectionVisible(not CheckBoxIncludeScansOtherPlants.Checked);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportCheckListScanF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportCheckListScanF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// RV095.1.
procedure TDialogReportCheckListScanF.CheckBoxAllPlantsClick(
  Sender: TObject);
begin
  inherited;
  BevelPlantDeptWS.Visible := CheckBoxAllPlants.Checked;
end;

end.
