(*
  Changes:
    MRA: 08-JUL-2009 RV032.
      - Prevent opening of tables here. This takes too much time
        during opening of report-dialog.
*)

unit ReportTeamIncentiveDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, CalculateTotalHoursDMT;

type
  TReportTeamIncentiveDM = class(TReportBaseDM)
    QueryProdHour: TQuery;
    DataSourceProd: TDataSource;
    TableProdQuant: TTable;
    QueryWork: TQuery;
    TableTRS: TTable;
    QueryPQ: TQuery;
    QueryTRS: TQuery;
    AQuery: TQuery;
    QueryTRSCNT: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProdHourFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportTeamIncentiveDM: TReportTeamIncentiveDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportTeamIncentiveDM.DataModuleCreate(Sender: TObject);
begin
//  inherited;
  // RV067.2.
  SystemDM.PlantTeamFilterEnable(QueryProdHour);
end;

procedure TReportTeamIncentiveDM.QueryProdHourFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV067.2.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
