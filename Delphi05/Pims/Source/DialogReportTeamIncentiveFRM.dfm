inherited DialogReportTeamIncentiveF: TDialogReportTeamIncentiveF
  Left = 425
  Top = 134
  Caption = 'Report team incentive'
  ClientHeight = 539
  ClientWidth = 641
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 641
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 345
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 641
    Height = 418
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 169
    end
    inherited LblEmployee: TLabel
      Top = 169
    end
    inherited LblToPlant: TLabel
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Top = 169
    end
    object Label3: TLabel [6]
      Left = 296
      Top = 658
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 120
      Top = 173
      Visible = False
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 173
    end
    object Label5: TLabel [9]
      Left = 120
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [10]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [11]
      Left = 8
      Top = 226
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [12]
      Left = 8
      Top = 42
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [13]
      Left = 40
      Top = 42
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [14]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [15]
      Left = 8
      Top = 67
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [16]
      Left = 40
      Top = 67
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [17]
      Left = 8
      Top = 473
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [18]
      Left = 8
      Top = 333
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [19]
      Left = 315
      Top = 68
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [20]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [21]
      Left = 120
      Top = 173
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel [22]
      Left = 336
      Top = 336
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [23]
      Left = 144
      Top = 472
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [24]
      Left = 352
      Top = 472
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [25]
      Left = 40
      Top = 333
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [26]
      Left = 40
      Top = 470
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [27]
      Left = 315
      Top = 333
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [28]
      Left = 299
      Top = 480
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [29]
      Left = 32
      Top = 602
      Width = 24
      Height = 13
      Caption = 'Days'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel [30]
      Left = 291
      Top = 610
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label26: TLabel [31]
      Left = 8
      Top = 424
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label27: TLabel [32]
      Left = 40
      Top = 424
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label28: TLabel [33]
      Left = 307
      Top = 451
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [34]
      Left = 120
      Top = 43
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [35]
      Left = 336
      Top = 43
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label29: TLabel [36]
      Left = 120
      Top = 427
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label30: TLabel [37]
      Left = 336
      Top = 427
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object LabelDatePeriod: TLabel [38]
      Left = 408
      Top = 228
      Width = 78
      Height = 13
      Caption = 'LabelDatePeriod'
      Visible = False
    end
    object Label31: TLabel [39]
      Left = 8
      Top = 195
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label32: TLabel [40]
      Left = 40
      Top = 195
      Width = 22
      Height = 13
      Caption = 'Shift'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label33: TLabel [41]
      Left = 315
      Top = 195
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label34: TLabel [42]
      Left = 120
      Top = 196
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label35: TLabel [43]
      Left = 336
      Top = 196
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label36: TLabel [44]
      Left = 24
      Top = 570
      Width = 54
      Height = 13
      Caption = 'From Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label37: TLabel [45]
      Left = 222
      Top = 570
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lblPeriod: TLabel [46]
      Left = 320
      Top = 612
      Width = 40
      Height = 13
      Caption = 'lblPeriod'
      Visible = False
    end
    object Label38: TLabel [47]
      Left = 8
      Top = 224
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label39: TLabel [48]
      Left = 40
      Top = 224
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label40: TLabel [49]
      Left = 315
      Top = 224
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 66
    end
    inherited LblDepartment: TLabel
      Top = 66
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 120
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 68
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 148
    end
    inherited LblToTeam: TLabel
      Top = 146
    end
    inherited LblStarTeamFrom: TLabel
      Left = 120
      Top = 148
    end
    inherited LblTeam: TLabel
      Top = 144
    end
    inherited LblFromTeam: TLabel
      Top = 144
    end
    inherited LblToShift: TLabel
      Left = 323
      Top = 473
      Visible = False
    end
    inherited LblFromPlant2: TLabel
      Left = 16
      Top = 503
    end
    inherited LblPlant2: TLabel
      Left = 48
      Top = 503
    end
    inherited LblToPlant2: TLabel
      Left = 323
      Top = 503
    end
    inherited LblFromWorkspot: TLabel
      Top = 93
    end
    inherited LblWorkspot: TLabel
      Top = 93
    end
    inherited LblStarWorkspotFrom: TLabel
      Left = 120
      Top = 95
    end
    inherited LblToWorkspot: TLabel
      Top = 94
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 95
    end
    inherited LblWorkspots: TLabel
      Top = 93
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 520
      Top = 88
    end
    inherited LblStarJobTo: TLabel
      Top = 118
    end
    inherited LblToJob: TLabel
      Top = 118
    end
    inherited LblStarJobFrom: TLabel
      Top = 118
    end
    inherited LblJob: TLabel
      Top = 118
    end
    inherited LblFromJob: TLabel
      Top = 118
    end
    inherited LblFromDateTime: TLabel
      Top = 224
    end
    inherited LblDateTime: TLabel
      Top = 224
    end
    inherited LblToDateTime: TLabel
      Top = 224
    end
    object DateFrom: TDateTimePicker [87]
      Left = 120
      Top = 220
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 19
    end
    object DateTo: TDateTimePicker [88]
      Left = 334
      Top = 220
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 20
    end
    object dxSpinEditWeek: TdxSpinEdit [89]
      Left = 336
      Top = 656
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 28
      Visible = False
      OnChange = dxSpinEditWeekChange
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditYear: TdxSpinEdit [90]
      Left = 120
      Top = 640
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 27
      Visible = False
      OnChange = dxSpinEditYearChange
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 149
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 150
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 143
      ColCount = 150
      TabOrder = 12
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 143
      ColCount = 151
      TabOrder = 13
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 146
      TabOrder = 14
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 66
      ColCount = 150
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 66
      ColCount = 151
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 68
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 168
      TabOrder = 15
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 168
      TabOrder = 16
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Top = 116
      ColCount = 157
      TabOrder = 39
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Left = 334
      Top = 116
      ColCount = 158
      TabOrder = 37
    end
    inherited CheckBoxAllShifts: TCheckBox
      Left = 514
      Top = 563
      TabOrder = 38
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 26
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 514
      Top = 522
      TabOrder = 40
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 514
      Top = 459
      TabOrder = 34
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Left = 128
      Top = 501
      ColCount = 165
      TabOrder = 35
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 350
      Top = 501
      ColCount = 166
      TabOrder = 36
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus [109]
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 148
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus [110]
      Left = 334
      Top = 41
      Width = 180
      Height = 19
      ColCount = 149
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object dxSpinEditDayFrom: TdxSpinEdit [111]
      Left = 120
      Top = 608
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 29
      Visible = False
      OnChange = dxSpinEditDayFromChange
      MaxValue = 7
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditDayTo: TdxSpinEdit [112]
      Left = 350
      Top = 568
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 30
      Visible = False
      OnChange = dxSpinEditDayToChange
      MaxValue = 7
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object CheckBoxAllTeam: TCheckBox [113]
      Left = 518
      Top = 423
      Width = 43
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 25
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object ComboBoxPlusShiftFrom: TComboBoxPlus [114]
      Left = 120
      Top = 194
      Width = 180
      Height = 19
      ColCount = 158
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 17
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftFromCloseUp
    end
    object ComboBoxPlusShiftTo: TComboBoxPlus [115]
      Left = 334
      Top = 194
      Width = 180
      Height = 19
      ColCount = 158
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 18
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftToCloseUp
    end
    object dxSpinEditWeekFrom: TdxSpinEdit [116]
      Left = 136
      Top = 568
      Width = 73
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 31
      Visible = False
      OnChange = dxSpinEditWeekFromChange
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditWeekTo: TdxSpinEdit [117]
      Left = 246
      Top = 568
      Width = 73
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 32
      Visible = False
      OnChange = dxSpinEditWeekToChange
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object Panel1: TPanel [118]
      Left = 0
      Top = 253
      Width = 641
      Height = 169
      BevelOuter = bvNone
      TabOrder = 33
      object RadioGroupSort: TRadioGroup
        Left = 8
        Top = 6
        Width = 105
        Height = 67
        Caption = 'Sort on'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Number'
          'Name')
        ParentFont = False
        TabOrder = 0
      end
      object RadioGroupBUDept: TRadioGroup
        Left = 8
        Top = 78
        Width = 105
        Height = 67
        Caption = 'Group by'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Items.Strings = (
          'Business unit'
          'Department')
        ParentFont = False
        TabOrder = 1
      end
      object GroupBoxSelection: TGroupBox
        Left = 120
        Top = 6
        Width = 393
        Height = 139
        Caption = 'Selections'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object CheckBoxShowSelection: TCheckBox
          Left = 12
          Top = 94
          Width = 129
          Height = 17
          Caption = 'Show selections'
          TabOrder = 4
        end
        object CheckBoxPagePlant: TCheckBox
          Left = 12
          Top = 114
          Width = 137
          Height = 17
          Caption = 'New page per plant'
          TabOrder = 5
          OnClick = CheckBoxPagePlantClick
        end
        object CheckBoxPageEmpl: TCheckBox
          Left = 208
          Top = 14
          Width = 145
          Height = 17
          Caption = 'New page per employee'
          TabOrder = 6
          OnClick = CheckBoxPageEmplClick
        end
        object CheckBoxDetailRep: TCheckBox
          Left = 12
          Top = 54
          Width = 97
          Height = 17
          Caption = 'Detailed report'
          TabOrder = 2
          OnClick = CheckBoxDetailRepClick
        end
        object CheckBoxOnlyJob: TCheckBox
          Left = 12
          Top = 14
          Width = 121
          Height = 17
          Caption = 'Show only job '
          TabOrder = 0
        end
        object CheckBoxIncentive: TCheckBox
          Left = 12
          Top = 34
          Width = 121
          Height = 17
          Caption = 'Show incentive'
          TabOrder = 1
        end
        object CheckBoxExport: TCheckBox
          Left = 12
          Top = 74
          Width = 125
          Height = 17
          Caption = 'Export'
          TabOrder = 3
        end
        object RadioGroupIncludeDowntime: TRadioGroup
          Left = 208
          Top = 35
          Width = 178
          Height = 99
          Caption = 'Include down-time quantities'
          ItemIndex = 0
          Items.Strings = (
            'No'
            'Yes'
            'Only show down-time')
          TabOrder = 7
        end
      end
      object ProgressBar: TProgressBar
        Left = 8
        Top = 152
        Width = 505
        Height = 8
        Min = 0
        Max = 100
        TabOrder = 3
      end
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 91
      ColCount = 166
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 91
      ColCount = 167
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      Top = 91
      Width = 393
      TabOrder = 9
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 116
      ColCount = 168
      TabOrder = 11
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 116
      ColCount = 167
      TabOrder = 10
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 220
      TabOrder = 22
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Left = 450
      Top = 220
      TabOrder = 24
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 220
      TabOrder = 21
    end
    inherited DatePickerTo: TDateTimePicker
      Left = 334
      Top = 220
      TabOrder = 23
    end
  end
  inherited stbarBase: TStatusBar
    Top = 479
    Width = 641
  end
  inherited pnlBottom: TPanel
    Top = 498
    Width = 641
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 184
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 216
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 584
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4D040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072D4469616C6F675265706F72745465616D496E63656E7469
        7665462E44617461536F75726365456D706C46726F6D104F7074696F6E734375
        73746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E64
        53697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C75
        6D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E73
        44420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C65
        74650D6564676F43616E496E73657274116564676F43616E4E61766967617469
        6F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C
        5265636F726473106564676F557365426F6F6B6D61726B7300000F5464784442
        47726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06
        064E756D62657206536F7274656407046373557005576964746802410942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F45
        4D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E
        0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F727420
        6E616D6505576964746802540942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060A53484F52545F4E414D4500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E0604
        4E616D6505576964746803B4000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060B4445534352495054494F4E00000F5464
        78444247726964436F6C756D6E0D436F6C756D6E416464726573730743617074
        696F6E06074164647265737305576964746802450942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D650607414444524553530000
        0F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F646507
        43617074696F6E060F4465706172746D656E7420636F64650557696474680258
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F4445504152544D454E545F434F444500000F546478444247726964436F
        6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F
        64650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        24040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072B4469616C6F675265706F72
        745465616D496E63656E74697665462E44617461536F75726365456D706C546F
        104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E
        670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E
        67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E
        6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D65
        64676F43616E44656C6574650D6564676F43616E496E73657274116564676F43
        616E4E617669676174696F6E116564676F436F6E6669726D44656C6574651265
        64676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D6172
        6B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C07
        43617074696F6E06064E756D62657206536F7274656407046373557005576964
        746802370942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F454D504C4F5945455F4E554D42455200000F54647844424772
        6964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E
        060A53686F7274206E616D65055769647468024E0942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D
        4500000F546478444247726964436F6C756D6E11436F6C756D6E446573637269
        7074696F6E0743617074696F6E06044E616D6505576964746803CC000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060B44
        45534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C
        756D6E416464726573730743617074696F6E0607416464726573730557696474
        6802650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506074144445245535300000F546478444247726964436F6C756D6E0E
        436F6C756D6E44657074436F64650743617074696F6E060F4465706172746D65
        6E7420636F646505576964746802580942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060F4445504152544D454E545F434F44
        4500000F546478444247726964436F6C756D6E0A436F6C756D6E5465616D0743
        617074696F6E06095465616D20636F6465055769647468023C0942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 436
  end
  inherited QueryEmplTo: TQuery
    Left = 408
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkSpot
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 296
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    DataSet = qryWorkspot
    Left = 520
    Top = 23
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_NUMBER,'
      '  S.PLANT_CODE,'
      '  S.DESCRIPTION'
      'FROM '
      '  SHIFT S'
      'WHERE'
      '  S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  S.SHIFT_NUMBER')
    Left = 600
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 80
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
