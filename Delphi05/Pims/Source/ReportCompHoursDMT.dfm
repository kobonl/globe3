inherited ReportCompHoursDM: TReportCompHoursDM
  OldCreateOrder = True
  Left = 191
  Top = 140
  Height = 571
  Width = 753
  object QueryAbsMinute_Ill: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, '
      '  SUM(A.ABSENCE_MINUTE) AS ABS_MIN'
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A, '
      '  ABSENCEREASON R '
      'WHERE  '
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  E.EMPLOYEE_NUMBER  =  A.EMPLOYEE_NUMBER  AND'
      '  ABSENCEREASON_CODE = '#39'I'#39
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 200
    Top = 8
    object IntegerField1: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAbsMinute_IllABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object QueryAbsMinute_Hol: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  E.EMPLOYEE_NUMBER, '
      '  SUM(A.ABSENCE_MINUTE) AS ABS_MIN'
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A,'
      '  ABSENCEREASON R'
      'WHERE  '
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  E.EMPLOYEE_NUMBER  =  A.EMPLOYEE_NUMBER AND'
      '  ABSENCEREASON_CODE = '#39'H'#39' '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 200
    Top = 60
    object QueryAbsMinute_HolEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAbsMinute_HolABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object QueryAbsMinute_Paid: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  E.EMPLOYEE_NUMBER, '
      '  SUM(A.ABSENCE_MINUTE) AS ABS_MIN'
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A,'
      '  ABSENCEREASON R'
      'WHERE  '
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  E.EMPLOYEE_NUMBER  =  A.EMPLOYEE_NUMBER  AND'
      '  ABSENCEREASON_CODE = '#39'P'#39' '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 200
    Top = 112
    object IntegerField3: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAbsMinute_PaidABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object QueryAbsMinute_Unpaid: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  E.EMPLOYEE_NUMBER,  '
      '  SUM(A.ABSENCE_MINUTE) AS ABS_MIN'
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A,'
      '  ABSENCEREASON R'
      'WHERE  '
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  E.EMPLOYEE_NUMBER  =  A.EMPLOYEE_NUMBER  AND'
      '  ABSENCEREASON_CODE = '#39'U'#39' '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 200
    Top = 164
    object IntegerField2: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAbsMinute_UnpaidABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object QuerySalaryMinute: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.EMPLOYEE_NUMBER, SUM(A.SALARY_MINUTE) AS ABS_MIN'
      'FROM'
      '  SALARYHOURPEREMPLOYEE A'
      'WHERE'
      '  A.SALARY_DATE >= :FSTARTDATE AND'
      '  A.SALARY_DATE <= :FENDDATE'
      'GROUP BY'
      '  A.EMPLOYEE_NUMBER'
      'ORDER BY'
      '  A.EMPLOYEE_NUMBER'
      ''
      ' ')
    Left = 200
    Top = 216
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
    object QuerySalaryMinuteEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QuerySalaryMinuteABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT'
      '  E.TEAM_CODE,'
      '  T.DESCRIPTION TDESCRIPTION,'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.STARTDATE, E.ENDDATE'
      'FROM'
      '  EMPLOYEE E LEFT JOIN TEAM T ON'
      '    E.TEAM_CODE = T.TEAM_CODE'
      'ORDER BY'
      '  E.TEAM_CODE, E.EMPLOYEE_NUMBER'
      ' '
      ' ')
    Left = 56
    Top = 8
  end
  object TableCtrEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'STARTDATE'
        DataType = ftDateTime
      end
      item
        Name = 'CREATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'ENDDATE'
        DataType = ftDateTime
      end
      item
        Name = 'HOURLY_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'MUTATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'MUTATOR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONTRACT_TYPE'
        DataType = ftInteger
      end
      item
        Name = 'CONTRACT_HOUR_PER_WEEK'
        DataType = ftFloat
      end
      item
        Name = 'CONTRACT_DAY_PER_WEEK'
        DataType = ftInteger
      end
      item
        Name = 'GUARANTEED_DAYS'
        DataType = ftInteger
      end
      item
        Name = 'EXPORT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'HOLIDAY_HOUR_PER_YEAR'
        DataType = ftFloat
      end>
    IndexDefs = <
      item
        Name = 'XPKEMPLOYEECONTRACT'
        Fields = 'EMPLOYEE_NUMBER;STARTDATE'
        Options = [ixUnique]
      end
      item
        Name = 'RDB$PRIMARY16'
        Fields = 'EMPLOYEE_NUMBER;STARTDATE'
        Options = [ixPrimary, ixUnique]
      end
      item
        Name = 'RDB$FOREIGN63'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    StoreDefs = True
    TableName = 'EMPLOYEECONTRACT'
    Left = 200
    Top = 268
  end
  object qryEmployeePlanning: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  EMPLOYEEPLANNING_DATE,'
      '  SHIFT_NUMBER,'
      '  DEPARTMENT_CODE,'
      '  WORKSPOT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1,'
      '  SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3,'
      '  SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5,'
      '  SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7,'
      '  SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9,'
      '  SCHEDULED_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEPLANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER >= :EMPFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPTO AND'
      '  EMPLOYEEPLANNING_DATE >= :DATEFROM AND'
      '  EMPLOYEEPLANNING_DATE <= :DATETO AND'
      '  (SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39') OR'
      '  SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39', '#39'B'#39', '#39'C'#39'))'
      'ORDER BY'
      '  EMPLOYEE_NUMBER,'
      '  EMPLOYEEPLANNING_DATE'
      ' '
      ' ')
    Left = 200
    Top = 372
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeeAvailable: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EA.PLANT_CODE,'
      '  EA.EMPLOYEEAVAILABILITY_DATE,'
      '  EA.SHIFT_NUMBER,'
      '  EA.EMPLOYEE_NUMBER,'
      '  E.DEPARTMENT_CODE,'
      '  EA.AVAILABLE_TIMEBLOCK_1,'
      '  EA.AVAILABLE_TIMEBLOCK_2,'
      '  EA.AVAILABLE_TIMEBLOCK_3,'
      '  EA.AVAILABLE_TIMEBLOCK_4,'
      '  EA.AVAILABLE_TIMEBLOCK_5,'
      '  EA.AVAILABLE_TIMEBLOCK_6,'
      '  EA.AVAILABLE_TIMEBLOCK_7,'
      '  EA.AVAILABLE_TIMEBLOCK_8,'
      '  EA.AVAILABLE_TIMEBLOCK_9,'
      '  EA.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE E ON'
      '    EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  (EA.EMPLOYEE_NUMBER >= :EMPFROM AND'
      '  EA.EMPLOYEE_NUMBER <= :EMPTO) AND'
      '  (EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO) AND'
      '  (EA.AVAILABLE_TIMEBLOCK_1 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_2 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_3 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_4 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_5 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_6 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_7 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_8 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_9 IN ('#39'*'#39') OR'
      '  EA.AVAILABLE_TIMEBLOCK_10 IN ('#39'*'#39'))'
      'ORDER BY'
      '  EA.EMPLOYEE_NUMBER,'
      '  EA.EMPLOYEEAVAILABILITY_DATE'
      ''
      ' ')
    Left = 200
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'EMPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EMPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetAbsIll: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsIll'
    Left = 352
    Top = 8
    object ClientDataSetAbsIllEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object ClientDataSetAbsIllABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object DataSetProviderAbsIll: TDataSetProvider
    DataSet = QueryAbsMinute_Ill
    Constraints = True
    Left = 496
    Top = 8
  end
  object ClientDataSetAbsHol: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsHol'
    Left = 352
    Top = 60
    object ClientDataSetAbsHolEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object ClientDataSetAbsHolABS_MIN: TFloatField
      FieldName = 'ABS_MIN'
    end
  end
  object DataSetProviderAbsHol: TDataSetProvider
    DataSet = QueryAbsMinute_Hol
    Constraints = True
    Left = 496
    Top = 56
  end
  object ClientDataSetAbsPaid: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsPaid'
    Left = 352
    Top = 112
  end
  object DataSetProviderAbsPaid: TDataSetProvider
    DataSet = QueryAbsMinute_Paid
    Constraints = True
    Left = 496
    Top = 104
  end
  object DataSetProviderAbsUnpaid: TDataSetProvider
    DataSet = QueryAbsMinute_Unpaid
    Constraints = True
    Left = 496
    Top = 160
  end
  object ClientDataSetAbsUnpaid: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsUnpaid'
    Left = 352
    Top = 164
  end
  object ClientDataSetSal: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderSal'
    Left = 352
    Top = 216
  end
  object DataSetProviderSal: TDataSetProvider
    DataSet = QuerySalaryMinute
    Constraints = True
    Left = 496
    Top = 208
  end
  object ClientDataSetEmplContr: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'STARTDATE'
        DataType = ftDateTime
      end
      item
        Name = 'CREATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'ENDDATE'
        DataType = ftDateTime
      end
      item
        Name = 'HOURLY_WAGE'
        DataType = ftFloat
      end
      item
        Name = 'MUTATIONDATE'
        DataType = ftDateTime
      end
      item
        Name = 'MUTATOR'
        Attributes = [faRequired]
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CONTRACT_TYPE'
        DataType = ftInteger
      end
      item
        Name = 'HOLIDAY_HOUR_PER_YEAR'
        DataType = ftFloat
      end
      item
        Name = 'CONTRACT_HOUR_PER_WEEK'
        DataType = ftFloat
      end
      item
        Name = 'CONTRACT_DAY_PER_WEEK'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'DEFAULT_ORDER'
        Fields = 'EMPLOYEE_NUMBER;STARTDATE'
        Options = [ixUnique]
      end
      item
        Name = 'CHANGEINDEX'
      end>
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    Params = <>
    ProviderName = 'DataSetProviderEmplContr'
    StoreDefs = True
    Left = 352
    Top = 268
  end
  object DataSetProviderEmplContr: TDataSetProvider
    DataSet = TableCtrEmpl
    Constraints = True
    Left = 496
    Top = 256
  end
  object ClientDataSetEmpAv: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpAv'
    Left = 352
    Top = 320
  end
  object DataSetProviderEmpAv: TDataSetProvider
    DataSet = qryEmployeeAvailable
    Constraints = True
    Left = 496
    Top = 304
  end
  object ClientDataSetEmpPln: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpPln'
    Left = 352
    Top = 372
  end
  object DataSetProviderEmpPln: TDataSetProvider
    DataSet = qryEmployeePlanning
    Constraints = True
    Left = 496
    Top = 360
  end
  object QueryRequest: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER '
      'FROM '
      '  REQUESTEARLYLATE  '
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 200
    Top = 424
  end
  object ClientDataSetEmpReq: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpRequest'
    Left = 352
    Top = 424
  end
  object DataSetProviderEmpRequest: TDataSetProvider
    DataSet = QueryRequest
    Constraints = True
    Left = 496
    Top = 424
  end
end
