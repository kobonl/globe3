inherited StaffPlanningLegendF: TStaffPlanningLegendF
  Left = 391
  Top = 165
  Width = 209
  Height = 397
  BorderIcons = [biSystemMenu]
  Caption = 'Legend'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBoxBtn: TGroupBox
    Left = 0
    Top = 0
    Width = 193
    Height = 169
    Align = alTop
    Caption = 'Buttons'
    Color = clSilver
    ParentColor = False
    TabOrder = 0
    object Label1: TLabel
      Left = 48
      Top = 22
      Width = 68
      Height = 13
      Caption = 'To be planned'
    end
    object Label2: TLabel
      Left = 48
      Top = 52
      Width = 38
      Height = 13
      Caption = 'Planned'
    end
    object Label3: TLabel
      Left = 48
      Top = 82
      Width = 133
      Height = 13
      Caption = 'Planned (special timeblocks)'
    end
    object Label4: TLabel
      Left = 48
      Top = 112
      Width = 141
      Height = 13
      Caption = 'Planned on another workspot'
    end
    object Label5: TLabel
      Left = 48
      Top = 142
      Width = 62
      Height = 13
      Caption = 'Not available'
    end
    object ButtonToBePln: TButton
      Left = 8
      Top = 16
      Width = 25
      Height = 25
      TabOrder = 0
    end
    object PanelPln: TPanel
      Left = 8
      Top = 46
      Width = 25
      Height = 25
      BorderStyle = bsSingle
      Color = clGreen
      TabOrder = 1
    end
    object PanelPlnS: TPanel
      Left = 8
      Top = 76
      Width = 25
      Height = 25
      BorderStyle = bsSingle
      Color = clHighlight
      TabOrder = 2
    end
    object PanelPlnWK: TPanel
      Left = 8
      Top = 106
      Width = 25
      Height = 25
      BorderStyle = bsSingle
      Color = clSilver
      TabOrder = 3
    end
    object PanelPlnNotAV: TPanel
      Left = 8
      Top = 136
      Width = 25
      Height = 25
      BorderStyle = bsSingle
      Color = clGray
      TabOrder = 4
    end
  end
  object GroupBoxOci: TGroupBox
    Left = 0
    Top = 169
    Width = 193
    Height = 189
    Align = alClient
    Caption = 'Occupation'
    TabOrder = 1
    object Label6: TLabel
      Left = 48
      Top = 17
      Width = 93
      Height = 13
      Caption = 'Not planned level A'
    end
    object Label7: TLabel
      Left = 48
      Top = 42
      Width = 92
      Height = 13
      Caption = 'Not planned level B'
    end
    object Label8: TLabel
      Left = 48
      Top = 69
      Width = 93
      Height = 13
      Caption = 'Not planned level C'
    end
    object Label9: TLabel
      Left = 48
      Top = 93
      Width = 38
      Height = 13
      Caption = 'Planned'
    end
    object Label10: TLabel
      Left = 48
      Top = 118
      Width = 62
      Height = 13
      Caption = 'Overplanned'
    end
    object Label11: TLabel
      Left = 48
      Top = 144
      Width = 122
      Height = 13
      Caption = 'Planned more than 200%'
    end
    object PanelLevelA: TPanel
      Left = 8
      Top = 18
      Width = 33
      Height = 15
      Color = clPurple
      TabOrder = 0
    end
    object PanelLevelB: TPanel
      Left = 8
      Top = 42
      Width = 33
      Height = 15
      Color = clFuchsia
      TabOrder = 1
    end
    object PanelLevelC: TPanel
      Left = 8
      Top = 69
      Width = 33
      Height = 15
      Color = clFuchsia
      TabOrder = 2
    end
    object PanelOCIPln: TPanel
      Left = 8
      Top = 92
      Width = 33
      Height = 15
      Color = clGreen
      TabOrder = 3
    end
    object PanelOCIOver: TPanel
      Left = 8
      Top = 117
      Width = 33
      Height = 15
      Color = clYellow
      TabOrder = 4
    end
    object Button2: TButton
      Left = 8
      Top = 140
      Width = 25
      Height = 25
      Caption = '2'
      TabOrder = 5
    end
  end
end
