(*
  MRA:27-NOV-2009 RV045.1.
  - Check for TFT-settings on Hourtype-level.
    If this is found, then disable these settings on
    ContractGroup-level.
  MRA:2-DEC-2009 RV046.01. Order 550476.
  - Add field 'Raise Percentage' in Contract group.
    This is only stored in database, but not used anywhere else.
  MRA:5-MAR-2010. RV055.2.
  - Remove mask for Raise_Percentage-field.
  MRA:8-OCT-2010. RV071.10. 550497 Additions for Saturday Credit.
  - Addition of field 'Max. hrs. Saturday Credit'.
  MRA:1-DEC-2010. RV082.5.
  - Store ContractgroupCode for selected employee in memory,
    to use in Contractgroup-related dialogs, so it can
    be looked up when showing these dialogs.
  MRA:6-DEC-2010. RV082.11.
  - Also store contractgroupcode when closing form.
  MRA:21-JAN-2011 RV085.11. Bugfix. SC-50016464.
  - When Contract Group is set as 'not-editable' using
    Admin Tool, then the Tab 'Overige' cannot be opened,
    only the first tab is visible.
  - Solution for dialogs with tab-pages:
    - Only the components on the tab-pages must be
      set as non-editable. But the tab-pages themselves
      not.
  MRA:27-JAN-2011 RV085.16. Small change. SC-50016904.
  - Only show 'Max hrs. Saturday Credit' for
    export-payroll-type Attentia.
  MRA:24-MAR-2011. RV088.1. SO-20011510.20 Export payroll AFAS.
  - Addition of checkbox 'Export normal hours' to contractgroup.
    Only visible for export payroll AFAS.
    Default: 'N'.
  MRA:11-NOV-2011. RV101.1. 20012331. Export AFAS.
  - Addition of checkbox 'Export worked days'.
  - Should only be visible for 'Export AFAS'.
  - Moved 'export normal hours'-checkbox to different place, to
    put it near 'export worked days'-checkbox.
  MRA:2-DEC-2011. RV103.1. SO-20011799. Worked hours during bank holiday.
  - Addition of hourtype for 'worked hours during bank holiday'.
  - Can be NULL.
  MRA:14-JAN-2019 GLOB3-204
  - Personal Time Off USA
*)

unit ContractGroupFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, Mask, dxDBTLCl, dxGrClms,
  dxEditor, dxExEdtr, dxDBEdtr, dxDBELib, ComCtrls, DBPicker, dxEdLib;

type
  TContractGroupF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridCheckColumn;
    dxDetailGridColumn4: TdxDBGridCheckColumn;
    dxDetailGridColumn5: TdxDBGridCheckColumn;
    dxDetailGridColumn7: TdxDBGridLookupColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn9: TdxDBGridColumn;
    dxDetailGridColumn10: TdxDBGridColumn;
    dxDetailGridColumn11: TdxDBGridColumn;
    dxDetailGridColumn12: TdxDBGridCheckColumn;
    dxDetailGridColumn14: TdxDBGridColumn;
    dxDetailGridColumn13: TdxDBGridColumn;
    dxDetailGridColumn15: TdxDBGridColumn;
    pnlDetail1: TPanel;
    GroupBoxContractGroup: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    DBEditContractGroup: TDBEdit;
    DBEditDesc: TDBEdit;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    LabelHourType: TLabel;
    dxSpinEditHour: TdxSpinEdit;
    dxSpinEditMin: TdxSpinEdit;
    dxDBLookupEditHT: TdxDBLookupEdit;
    dxDetailGridColumn16: TdxDBGridCheckColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    DBRadioGroupOvertime: TDBRadioGroup;
    pnlDetail2: TPanel;
    GroupBoxOvertime: TGroupBox;
    DBCheckBoxTimeForTime: TDBCheckBox;
    DBCheckBoxBonusInMoney: TDBCheckBox;
    DBCheckBoxWorkTimeReduction: TDBCheckBox;
    DBCheckBoxExceptionalBeforeOvertime: TDBCheckBox;
    DBCheckBoxBankHolidayOverrulesAvail: TDBCheckBox;
    gboxWorkingDays: TGroupBox;
    gBoxPaidIllness: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    DBEditPaidIllnessPeriod: TDBEdit;
    dxDBLookupEditUnpaidIllnessAbsenceReason: TdxDBLookupEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    gboxNormalHrsPerDay: TGroupBox;
    Label12: TLabel;
    gboxPeriod: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    DBEditPeriodWeeks: TDBEdit;
    DBEditWeeks: TDBEdit;
    Panel2: TPanel;
    dxSpinEditHourNHrsPDay: TdxSpinEdit;
    dxSpinEditMinNHrsPDay: TdxSpinEdit;
    Label13: TLabel;
    dxDBCheckEditExportWorkedDays: TdxDBCheckEdit;
    dxDBCheckEditExportNormalHrs: TdxDBCheckEdit;
    Panel1: TPanel;
    GroupBoxPayroll: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    DBRadioGroupPayroll: TDBRadioGroup;
    dxDBSpinEditMinuteRound: TdxDBSpinEdit;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    lblRaisePerc: TLabel;
    LblMaxSatCredMinute: TLabel;
    LblHrsMin: TLabel;
    dxDBMaskEdit1: TdxDBMaskEdit;
    dxDBCheckEditExportSRHours: TdxDBCheckEdit;
    dxDBMaskEditRaisePerc: TdxDBMaskEdit;
    dxSpinEditHMaxSC: TdxSpinEdit;
    dxSpinEditMMaxSC: TdxSpinEdit;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    dxDBLookupEditHTBankHol: TdxDBLookupEdit;
    dxDetailGridColumn17: TdxDBGridLookupColumn;
    TabSheet3: TTabSheet;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    dxSpinEditMaxPTOHours: TdxSpinEdit;
    dxSpinEditMaxPTOMin: TdxSpinEdit;
    dxDetailGridColumn18: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBRadioGroupOvertimeClick(Sender: TObject);
    procedure DBRadioGroupOvertimeChange(Sender: TObject);
    procedure DBCheckBoxTimeForTimeClick(Sender: TObject);
    procedure DBCheckBoxBonusInMoneyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxSpinEditMinKeyPress(Sender: TObject; var Key: Char);
    procedure dxSpinEditMinMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure dxDBSpinEditMinuteRoundChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure EnableSave(Status: Boolean);
    procedure SetTimeFields;
  end;

function ContractGroupF: TContractGroupF;

implementation

{$R *.DFM}

uses
  SystemDMT, ContractGroupDMT, UPimsMessageRes;

var
  ContractGroupF_HND: TContractGroupF;

function ContractGroupF: TContractGroupF;
begin
  if (ContractGroupF_HND = nil) then
    ContractGroupF_HND := TContractGroupF.Create(Application);
  Result := ContractGroupF_HND;
end;

procedure TContractGroupF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePage := TabSheet1;
  DBEditContractGroup.SetFocus;
end;

procedure TContractGroupF.FormCreate(Sender: TObject);
begin
  ContractGroupDM := CreateFormDM(TContractGroupDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := ContractGroupDM.DataSourceDetail;
  // RV085.16. Only show this part for Attentia.
  LblMaxSatCredMinute.Visible := SystemDM.IsAttent;
  dxSpineditHMaxSC.Visible := LblMaxSatCredMinute.Visible;
  dxSpinEditMMaxSC.Visible := LblMaxSatCredMinute.Visible;
  LblHrsMin.Visible := LblMaxSatCredMinute.Visible;
  inherited;
  // RV046.01. Order 550476.
  // Content can be: 99,9 or 9,9 or 0
  // RV055.2.
//  dxDBMaskEditRaisePerc.EditMask := '09' + DecimalSeparator + '9;0';
end;

procedure TContractGroupF.FormDestroy(Sender: TObject);
begin
  inherited;
  ContractGroupF_HND := nil;
end;

procedure TContractGroupF.DBRadioGroupOvertimeClick(Sender: TObject);
begin
  inherited;
  if (DBRadioGroupOvertime.ItemIndex <> -1) then
  begin
    case DBRadioGroupOvertime.ItemIndex of
    0, 1: // day, week
      begin
        gboxPeriod.Visible := False;
        gboxWorkingDays.Visible := False;
        gboxNormalHrsPerDay.Visible := False;
      end;
    3: // period
      begin
        gboxPeriod.Visible := True;
        gboxWorkingDays.Visible := False;
        gboxNormalHrsPerDay.Visible := False;
      end;
    2: // month
      begin
        gboxPeriod.Visible := False;
        gboxWorkingDays.Visible := True;
        gboxNormalHrsPerDay.Visible := True;
      end;
    end;
  end;
end;

procedure TContractGroupF.DBRadioGroupOvertimeChange(Sender: TObject);
begin
  inherited;
  DBRadioGroupOvertimeClick(Sender);
end;

procedure TContractGroupF.DBCheckBoxTimeForTimeClick(Sender: TObject);
begin
  inherited;
  DBCheckBoxBonusInMoney.Enabled := DBCheckBoxTimeForTime.Checked;
  if  (DBCheckBoxTimeForTime.Checked = False) then
    DBCheckBoxBonusInMoney.Checked := False;
  if  ContractGroupDM <> nil then
  begin
    ContractGroupDM.FTFT_YN := DBCheckBoxTimeForTime.Checked;
  end;
end;

procedure TContractGroupF.DBCheckBoxBonusInMoneyClick(Sender: TObject);
begin
  inherited;
  if ContractGroupDM <> nil then
  begin
    ContractGroupDM.FBonus_YN := DBCheckBoxBonusInMoney.Checked;
  end;  
end;

procedure TContractGroupF.FormShow(Sender: TObject);
begin
  inherited;
   //Car: 550287 set here instead in Object inspector
  //- otherwise get an error when form is created - because table is not opened
  dxDBSpinEditMinuteRound.MinValue := 1;
  dxDBSpinEditMinuteRound.MaxValue := 59;

  // For VOSS, enable a component
  dxDBCheckEditExportSRHours.Visible := False;
  dxDBCheckEditExportNormalHrs.Visible := False;
  if SystemDM.GetExportPayroll then
  begin
    dxDBCheckEditExportSRHours.Visible := (SystemDM.TableExportPayroll.
      FieldByName('EXPORT_TYPE').AsString = 'SR');
    // RV088.1. Export AFAS.
    dxDBCheckEditExportNormalHrs.Visible :=
      (SystemDM.TableExportPayroll.Locate('EXPORT_TYPE', 'AFAS', []));
    // RV101.1. Export AFAS. Visible when 'export normal hrs' is also visible.
    dxDBCheckEditExportWorkedDays.Visible :=
      dxDBCheckEditExportNormalHrs.Visible;
  end;

  if (ContractGroupDM.TableMaster.FieldByName('TIME_FOR_TIME_YN').AsString = 'N') then
  begin
    DBCheckBoxBonusInMoney.Checked := False;
    DBCheckBoxBonusInMoney.Enabled := False;
  end
  else
    DBCheckBoxBonusInMoney.Enabled := True;
  SetTimeFields;
  EnableSave(False);
  PageControl1.ActivePage := TabSheet1;
  // RV045.1.
  ContractGroupDM.HourTypeTFT := ContractGroupDM.CheckHourTypeTFT;
  if ContractGroupDM.HourTypeTFT then
  begin
    DBCheckBoxTimeForTime.Enabled := False;
    DBCheckBoxBonusInMoney.Enabled := False;
//    dxDetailGridColumn3.Visible := False;
//    dxDetailGridColumn5.Visible := False;
    dxDetailGridColumn3.ReadOnly := True;
    dxDetailGridColumn5.ReadOnly := True;
  end;
  // RV082.5.
  if SystemDM.ASaveTimeRecScanning.ContractGroupCode <> '' then
  begin
    try
      ContractGroupDM.TableDetail.Locate('CONTRACTGROUP_CODE',
        SystemDM.ASaveTimeRecScanning.ContractGroupCode, []);
    except
      // Ignore error
    end;
  end;
  // RV085.11. Disable only some parts of pnlDetail, so the
  //           tab-pages can still be clicked and seen.
  if not pnlDetail.Enabled then
  begin
    pnlDetail.Enabled := True;
    pnlDetail1.Enabled := False;
    pnlDetail2.Enabled := False;
    GroupBox1.Enabled := False;
    Panel2.Enabled := False;
  end;
end;

procedure TContractGroupF.SetTimeFields;
begin
  with ContractGroupDM do
  begin
    dxSpinEditHour.Value :=
      TableDetail.FieldByName('GUARANTEED_HOURS').AsInteger div 60;
    dxSpinEditMin.Value :=
      TableDetail.FieldByName('GUARANTEED_HOURS').AsInteger mod 60;
    dxSpinEditHourNHrsPDay.Value :=
      TableDetail.FieldByName('NORMALHOURSPERDAY').AsInteger div 60;
    dxSpinEditMinNHrsPDay.Value :=
      TableDetail.FieldByName('NORMALHOURSPERDAY').AsInteger mod 60;
    // RV071.10. Max. Hrs. Saturday Credit
    dxSpinEditHMaxSC.Value :=
      TableDetail.FieldByName('MAX_SAT_CREDIT_MINUTE').AsInteger div 60;
    dxSpinEditMMaxSC.Value :=
      TableDetail.FieldByName('MAX_SAT_CREDIT_MINUTE').AsInteger mod 60;
    // GLOB3-204
    dxSpinEditMaxPTOHours.Value :=
      TableDetail.FieldByName('MAX_PTO_MINUTE').AsInteger div 60;
    dxSpinEditMaxPTOMin.Value :=
      TableDetail.FieldByName('MAX_PTO_MINUTE').AsInteger mod 60;
  end;
end;

procedure TContractGroupF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
end;

procedure TContractGroupF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  if dxSpinEditMin.Value > 59 then
  begin
    DisplayMessage(SValidMinutes, mtInformation, [mbOk]);
    Exit;
  end;
  if dxSpinEditHourNHrsPDay.Value >= 24 then
  begin
    dxSpinEditHourNHrsPDay.Value := 24;
    dxSpinEditMinNHrsPDay.Value := 0;
  end;
  if (dxSpinEditHour.Value <> 0 ) or (dxSpinEditMin.Value <> 0 ) then
    if (dxDBLookupEditHT.Text = '') then
    begin
      DisplayMessage(SQuaranteedHourType, mtInformation, [mbOk]);
      Exit;
    end;
  // RV071.10. Max. Hrs. Saturday Credit
  if (dxSpinEditMMaxSC.Value > 59) then
    dxSpinEditMMaxSC.Value := 59;
  // GLOB3-204
  if (dxSpinEditMaxPTOMin.Value > 59) then
    dxSpinEditMaxPTOMin.Value := 59;

  ContractGroupDM.TableDetail.Edit;
  try
    ContractGroupDM.GuaranteedHours := Round(dxSpinEditHour.Value * 60 +
      dxSpinEditMin.Value);
    ContractGroupDM.NormalHoursPerDay := Round(dxSpinEditHourNHrsPDay.Value * 60 +
      dxSpinEditMinNHrsPDay.Value);
    // RV071.10. Max. Hrs. Saturday Credit
//    ContractGroupDM.TableDetail.FieldByName('MAX_SAT_CREDIT_MINUTE').AsInteger :=
//      Round(dxSpinEditHMaxSC.Value * 60 + dxSpinEditMMaxSC.Value);
    ContractGroupDM.MaxSatCreditMinute :=
      Round(dxSpinEditHMaxSC.Value * 60 + dxSpinEditMMaxSC.Value);
    // GLOB3-204
    ContractGroupDM.MaxPTOMinute :=
      Round(dxSpinEditMaxPTOHours.Value * 60 + dxSpinEditMaxPTOMin.Value);
  except
    // Ignore error.
  end;
  ContractGroupDM.TableDetail.Post;
end;

procedure TContractGroupF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  ContractGroupDM.TableDetail.Cancel;
  EnableSave(False);
  SetTimeFields;
end;

procedure TContractGroupF.EnableSave(Status: Boolean);
begin
  dxBarBDBNavPost.Enabled:= Status;
  dxBarBDBNavCancel.Enabled:= Status;
end;

procedure TContractGroupF.dxSpinEditMinKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  EnableSave(True);
end;

procedure TContractGroupF.dxSpinEditMinMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  EnableSave(True);
end;

procedure TContractGroupF.FormResize(Sender: TObject);
begin
  inherited;
  //CAR 550287 - increase width of PayrollgroupBox when it's resize
  GroupBoxPayroll.Width := (ContractGroupF.ClientWidth -
    GroupBoxContractGroup.Width) div 3;
end;

procedure TContractGroupF.dxDBSpinEditMinuteRoundChange(Sender: TObject);
begin
  inherited;
  //550287
  // the value is: >= 1
  if (dxDBSpinEditMinuteRound.Text = '0') or
    (dxDBSpinEditMinuteRound.Text = '') then
     dxDBSpinEditMinuteRound.Value := 1; 
end;

procedure TContractGroupF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
   if dxBarBDBNavPost.Enabled then
     if (DisplayMessage(SPimsSaveChanges, mtConfirmation,
       [mbYes, mbNo])= mrYes) then
       dxBarBDBNavPostClick(nil)
     else
       dxBarBDBNavCancelClick(nil);

  // RV082.11.
  try
    if not ContractGroupDM.TableDetail.IsEmpty then
      SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
        ContractGroupDM.TableDetail.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end;
end;

procedure TContractGroupF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if ASelected or AFocused then
  begin
    SetTimeFields;
  end;
end;

end.
