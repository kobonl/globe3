(*
  MRA:24-JUL-2015 PIM-50
  - new report to show comments for down-jobs
*)
unit ReportJobCommentsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, SystemDMT;

type
  TReportJobCommentsDM = class(TReportBaseDM)
    qryJobComments: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportJobCommentsDM: TReportJobCommentsDM;

implementation

{$R *.DFM}

end.
