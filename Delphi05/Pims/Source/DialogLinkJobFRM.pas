(*
  MRA:20-NOV-2013 20013196
  - Multiple counters for workspot
    - Addition of link job-dialog.
*)
unit DialogLinkJobFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, DBCtrls, WorkspotDMT, DB;

type
  TDialogLinkJobF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DBLookupComboBoxWorkspot: TDBLookupComboBox;
    DBLookupComboBoxJob: TDBLookupComboBox;
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBLookupComboBoxJobEnter(Sender: TObject);
    procedure DBLookupComboBoxWorkspotCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FJobCode: String;
    FWorkspotCode: String;
    FPlantCode: String;
    { Private declarations }
  public
    { Public declarations }
    property PlantCode: String read FPlantCode write FPlantCode;
    property WorkspotCode: String read FWorkspotCode write FWorkspotCode;
    property JobCode: String read FJobCode write FJobCode;
  end;

var
  DialogLinkJobF: TDialogLinkJobF;

implementation

{$R *.DFM}

procedure TDialogLinkJobF.FormCreate(Sender: TObject);
begin
  inherited;
  // Be sure this dialog stays on top of any sub-dialogs.
  FormStyle := fsStayOnTop;
end;

procedure TDialogLinkJobF.FormShow(Sender: TObject);
begin
  inherited;
  // Point tables to correct values based on incoming plant/workspot/job
  // Refresh this first!
  WorkspotDM.TableLinkJob.Close;
  WorkspotDM.TableLinkJob.Open;
  WorkspotDM.TableWorkspot.Locate('PLANT_CODE;WORKSPOT_CODE',
    VarArrayOf([PlantCode, WorkspotCode]), []);
  WorkspotDM.TableJob.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
    VarArrayOf([PlantCode, WorkspotCode, JobCode]), []);
  if not WorkspotDM.TableLinkJob.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
    VarArrayOf([PlantCode, WorkspotCode, JobCode]), []) then
    WorkspotDM.TableLinkJob.Insert;
end;

procedure TDialogLinkJobF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  btnCancelClick(Sender);
end;

procedure TDialogLinkJobF.btnOkClick(Sender: TObject);
begin
  inherited;
  if WorkspotDM.TableLinkJob.State in [dsEdit, dsInsert] then
  begin
    WorkspotDM.TableLinkJob.FieldByName('PLANT_CODE').AsString := PlantCode;
    WorkspotDM.TableLinkJob.FieldByName('WORKSPOT_CODE').AsString := WorkspotCode;
    WorkspotDM.TableLinkJob.FieldByName('JOB_CODE').AsString := JobCode;
    WorkspotDM.TableLinkJob.Post;
  end;
end;

procedure TDialogLinkJobF.btnCancelClick(Sender: TObject);
begin
  inherited;
  if WorkspotDM.TableLinkJob.State in [dsEdit, dsInsert] then
    WorkspotDM.TableLinkJob.Cancel;
end;

procedure TDialogLinkJobF.DBLookupComboBoxJobEnter(Sender: TObject);
begin
  inherited;
  // Be sure it points to jobs belonging to the selected (link-) workspot!
  // If this is not done, then when a user re-opens the dialog and selects
  // a different job, then it just shows all jobs!
  try
    if WorkspotDM.TableLinkJob.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
      VarArrayOf([PlantCode, WorkspotCode, JobCode]), []) then
      WorkspotDM.TableWorkspot.Locate('PLANT_CODE;WORKSPOT_CODE',
        VarArrayOf([PlantCode,
          WorkspotDM.TableLinkJob.FieldByName('LINK_WORKSPOT_CODE').AsString]),
          []);
  except
  end;
end;

// When workspot has changed, fill link-job with first found job belonging to
// this workspot!
procedure TDialogLinkJobF.DBLookupComboBoxWorkspotCloseUp(Sender: TObject);
begin
  inherited;
  try
    WorkspotDM.TableJob.Locate('PLANT_CODE;WORKSPOT_CODE',
      VarArrayOf([PlantCode,
        WorkspotDM.TableLinkJob.FieldByName('LINK_WORKSPOT_CODE').AsString]),
        []);
  except
  end;
  // Empty the value to ensure the user selects a job that belongs to the
  // workspot. This prevents a job is still shown that belonged to a different
  // workspot.
  DBLookupComboBoxJob.KeyValue := '';
end;

end.
