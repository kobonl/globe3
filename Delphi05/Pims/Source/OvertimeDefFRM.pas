(*
  MRA:1-DEC-2010. RV082.5.
  - Store ContractgroupCode for selected employee in memory,
    to use in Contractgroup-related dialogs, so it can
    be looked up when showing these dialogs.
  - Because of this change, it appears the form is in 'edit'-mode,
    when the contractgroup was changed during FormShow. To prevent
    this a InitForm-boolean is used.
  MRA:27-APR-2011. RV091.1. SO-20011677.
  - Copy/Paste option for Overtime definitions.
  MRA:27-APR-2011. RV091.2. Bug fix.
  - Prevent the question about 'save changes', when
    nothing was changed.
  MRA:12-FEB-2018 GLOB3-81
  - Change for overtime hours (only used for NTS)
  - Add from-to-time as extra restriction for day-period
  - Add day-of-week as extra setting.
  MRA:22-FEB-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
*)
unit OvertimeDefFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib,
  StdCtrls, dxDBTLCl, dxGrClms, Mask, dxDBEdtr;

type
  TOvertimeDefF = class(TGridBaseF)
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnWeekNo: TdxDBGridColumn;
    dxDetailGridColumnLine: TdxDBGridColumn;
    dxDetailGridColumnHourType: TdxDBGridLookupColumn;
    Label3: TLabel;
    dxMasterGridColumnWeeksPeriod: TdxDBGridColumn;
    GroupBoxOverTime: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    dxDBSpinEditLine: TdxDBSpinEdit;
    GroupBoxMore: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    GroupBoxuntil: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    dxSpinEditStartHour: TdxSpinEdit;
    dxSpinEditStartMin: TdxSpinEdit;
    dxSpinEditEndHour: TdxSpinEdit;
    dxSpinEditEndMin: TdxSpinEdit;
    dxDetailGridColumnStartTime: TdxDBGridColumn;
    dxDetailGridColumnEndTime: TdxDBGridColumn;
    dxDBLookupEdit1: TdxDBLookupEdit;
    dxSpinEditStartHourOld: TdxSpinEdit;
    dxSpinEditStartMinOld: TdxSpinEdit;
    dxSpinEditEndHourOld: TdxSpinEdit;
    dxSpinEditEndMinOld: TdxSpinEdit;
    dxMasterGridColumnCalcOvertimetype: TdxDBGridColumn;
    CopyAct: TAction;
    PasteAct: TAction;
    dxDetailGridColumnFromTime: TdxDBGridColumn;
    dxDetailGridColumnToTime: TdxDBGridColumn;
    dxDetailGridColumnDayOfWeek: TdxDBGridColumn;
    lblOnlyForHrsBetween: TLabel;
    LblWeekDay: TLabel;
    dxDBTimeEditFromTime: TdxDBTimeEdit;
    lblAnd: TLabel;
    dxDBTimeEditToTime: TdxDBTimeEdit;
    dxDBImgEdtDayOfWeek: TdxDBImageEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxSpinEditStartHourExit(Sender: TObject);
    procedure dxSpinEditStartMinExit(Sender: TObject);
    procedure dxSpinEditEndHourExit(Sender: TObject);
    procedure dxSpinEditEndMinExit(Sender: TObject);
    procedure dxSpinEditStartHourChange(Sender: TObject);
    procedure dxSpinEditStartMinChange(Sender: TObject);
    procedure dxSpinEditEndHourChange(Sender: TObject);
    procedure dxSpinEditEndMinChange(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxGridEnter(Sender: TObject);
    procedure CopyActExecute(Sender: TObject);
    procedure PasteActExecute(Sender: TObject);
  private
    { Private declarations }
    FInitForm: Boolean;
    FCopyContractGroupCode: String;
    FPasteContractGroupCode: String;
    procedure StartEditMode(Sender: TObject);
    procedure InitNTS;
  public
    { Public declarations }
    function ComputeStartTime: Integer;
    function ComputeEndTime: Integer;
    procedure SetTimeFields;
    procedure ShowFromToTime;
    property InitForm: Boolean read FInitForm write FInitForm; // RV082.5.
  end;

function OvertimeDefF: TOvertimeDefF;

implementation

uses
  OvertimeDefDMT, SystemDMT, UPimsMessageRes;

{$R *.DFM}

var
  OvertimeDefF_HND: TOvertimeDefF;

function OvertimeDefF: TOvertimeDefF;
begin
  if (OvertimeDefF_HND = nil) then
  begin
    OvertimeDefF_HND := TOvertimeDefF.Create(Application);
  end;
  Result := OvertimeDefF_HND;
end;

procedure TOvertimeDefF.FormDestroy(Sender: TObject);
begin
  inherited;
  OvertimeDefF_HND := Nil;
end;

procedure TOvertimeDefF.FormCreate(Sender: TObject);
begin
  InitForm := True; // RV082.5.
  OvertimeDefDM := CreateFormDM(TOvertimeDefDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := OvertimeDefDM.DataSourceDetail;
    dxMasterGrid.DataSource := OvertimeDefDM.DataSourceMaster;
  end;
  inherited;
  // RV091.1.
  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste;
  // GLOB3-81
  InitNTS;
end;

procedure TOvertimeDefF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  dxDBSpinEditLine.SetFocus;
end;

function TOvertimeDefF.ComputeStartTime: Integer;
begin
  Result := Round(dxSpinEditStartHour.Value * 60 + dxSpinEditStartMin.Value);
end;

function TOvertimeDefF.ComputeEndTime: Integer;
begin
  Result := Round(dxSpinEditEndHour.Value * 60 + dxSpinEditEndMin.Value);
end;

procedure TOvertimeDefF.dxSpinEditStartHourExit(Sender: TObject);
begin
  inherited;
(*
  if dxSpinEditStartHour.Modified then
  begin
    GridFormDM.TableDetail.Edit;
    GridFormDM.TableDetail.FieldByName('STARTTIME').asInteger:=
      ComputeStartTime;
  end;
*)
end;

procedure TOvertimeDefF.dxSpinEditStartMinExit(Sender: TObject);
begin
  inherited;
(*
  if dxSpinEditStartMin.Modified then
  begin
    GridFormDM.TableDetail.Edit;
    GridFormDM.TableDetail.FieldByName('STARTTIME').asInteger:=
      ComputeStartTime;
  end;
*)
end;

procedure TOvertimeDefF.dxSpinEditEndHourExit(Sender: TObject);
begin
  inherited;
(*
  if dxSpinEditEndHour.Modified then
  begin
    GridFormDM.TableDetail.Edit;
    GridFormDM.TableDetail.FieldByName('ENDTIME').asInteger:=
      ComputeEndTime;
  end;
*)
end;

procedure TOvertimeDefF.dxSpinEditEndMinExit(Sender: TObject);
begin
  inherited;
(*
  if dxSpinEditEndMin.Modified then
  begin
    GridFormDM.TableDetail.Edit;
    GridFormDM.TableDetail.FieldByName('ENDTIME').asInteger:=
      ComputeEndTime;
  end;
*)
end;

procedure TOverTimeDefF.StartEditMode(Sender: TObject);
begin
  if not InitForm then // RV082.5.
    if not GridFormDM.TableDetail.IsEmpty then
      if (Sender is TdxSpinEdit) then
        GridFormDM.TableDetail.Edit;
//  dxDetailGrid.SetFocus;
//  if pnlDetail.Visible then
//    pnlDetail.SetFocus;
end;

procedure TOvertimeDefF.dxSpinEditStartHourChange(Sender: TObject);
begin
  inherited;
  if OvertimeDefDM <> Nil then
  begin
    OvertimeDefDM.FStartTime := ComputeStartTime;
    if dxSpinEditStartHour.IntValue <> dxSpinEditStartHourOld.IntValue then
    // NOTE: Modified does NOT work if you change the contents by
    // using the up/down buttons of this component!
//      if dxSpinEditStartHour.Modified then
      StartEditMode(Sender);
  end;
end;

procedure TOvertimeDefF.dxSpinEditStartMinChange(Sender: TObject);
begin
  inherited;
  if OvertimeDefDM <> Nil then
  begin
    OvertimeDefDM.FStartTime := ComputeStartTime;
    if dxSpinEditStartMin.IntValue <> dxSpinEditStartMinOld.IntValue then
      StartEditMode(Sender);
  end;
end;

procedure TOvertimeDefF.dxSpinEditEndHourChange(Sender: TObject);
begin
  inherited;
  if OvertimeDefDM <> Nil then
  begin
    OvertimeDefDM.FEndTime := ComputeEndTime;
    if dxSpinEditEndHour.IntValue <> dxSpinEditEndHourOld.IntValue then
      StartEditMode(Sender);
  end;
end;

procedure TOvertimeDefF.dxSpinEditEndMinChange(Sender: TObject);
begin
  inherited;
  if OvertimeDefDM <> Nil then
  begin
    OvertimeDefDM.FEndTime := ComputeEndTime;
    if dxSpinEditEndMin.IntValue <> dxSpinEditEndMinOld.IntValue then
      StartEditMode(Sender);
  end;
end;

procedure TOvertimeDefF.SetTimeFields;
begin
  with OverTimeDefDM DO
  begin
    // MRA: RV091.2 First store old values, then new values,
    //              to prevent the 'onChange'-event.
    // MR: First store old values.
    dxSpinEditStartHourOld.Value :=
      TableDetail.FieldByName('STARTTIME').asInteger div 60;
    dxSpinEditStartMinOld.Value :=
      TableDetail.FieldByName('STARTTIME').asInteger mod 60;
    dxSpinEditEndHourOld.Value :=
      TableDetail.FieldByName('ENDTIME').asInteger div 60;
    dxSpinEditEndMinOld.Value :=
      TableDetail.FieldByName('ENDTIME').asInteger mod 60;
    // MR: Then store actual values.
    dxSpinEditStartHour.Value :=
      TableDetail.FieldByName('STARTTIME').asInteger div 60;
    dxSpinEditStartMin.Value :=
      TableDetail.FieldByName('STARTTIME').asInteger mod 60;
    dxSpinEditEndHour.Value :=
      TableDetail.FieldByName('ENDTIME').asInteger div 60;
    dxSpinEditEndMin.Value :=
      TableDetail.FieldByName('ENDTIME').asInteger mod 60;
  end;
end;

procedure TOvertimeDefF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
  ShowFromToTime;  
end;

procedure TOvertimeDefF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
  ShowFromToTime;  
  // RV091.1.
  FPasteContractGroupCode :=
    OvertimeDefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  dxBarButtonPaste.Enabled := (FCopyContractGroupCode <> '') and
    (FCopyContractGroupCode <> FPasteContractGroupCode);
  if dxBarButtonPaste.Enabled then
    dxBarButtonPaste.Hint :=
      Format(SPimsPasteOvertimeDef,
        [FCopyContractGroupCode])
  else
    dxBarButtonPaste.Hint := SPimsPaste;
end;

procedure TOvertimeDefF.FormShow(Sender: TObject);
begin
  inherited;
  // RV082.5.
  if SystemDM.ASaveTimeRecScanning.ContractGroupCode <> '' then
  begin
    try
      OvertimeDefDM.TableMaster.Locate('CONTRACTGROUP_CODE',
        SystemDM.ASaveTimeRecScanning.ContractGroupCode, []);
    except
      // Ignore error
    end;
  end;
  SetTimeFields;
  ShowFromToTime;  
  InitForm := False;
end;

procedure TOvertimeDefF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // RV082.5.
  try
    if not OvertimeDefDM.TableMaster.IsEmpty then
      SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
        OvertimeDefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end;
end;

procedure TOvertimeDefF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  // Retrieve old values from 'old'-components.
  dxSpinEditStartHour.Value :=
    dxSpinEditStartHourOld.Value;
  dxSpinEditStartMin.Value :=
    dxSpinEditStartMinOld.Value;
  dxSpinEditEndHour.Value :=
    dxSpinEditEndHourOld.Value;
  dxSpinEditEndMin.Value :=
    dxSpinEditEndMinOld.Value;
  // Because these components have a OnChange-event
  // that set the TableDetail to 'Edit', we cancel this.
  OverTimeDefDM.TableDetail.Cancel;
end;

// RV091.1.
procedure TOvertimeDefF.dxGridEnter(Sender: TObject);
begin
  inherited;
  if ActiveGrid = dxMastergrid then
  begin
    dxBarButtonCopy.Visible := ivAlways;
    dxBarButtonPaste.Visible := ivAlways;
  end
  else
  begin
    dxBarButtonCopy.Visible := ivNever;
    dxBarButtonPaste.Visible := ivNever;
  end;
  ShowFromToTime;  
end;

// RV091.1.
procedure TOvertimeDefF.CopyActExecute(Sender: TObject);
begin
  inherited;
  FCopyContractGroupCode := '';

  if (not OvertimeDefDM.TableMaster.Active) or
    OvertimeDefDM.TableMaster.IsEmpty then
    Exit;

  FCopyContractGroupCode :=
    OvertimeDefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste; // RV067.MRA.34.
end;

// RV091.1.
procedure TOvertimeDefF.PasteActExecute(Sender: TObject);
var
  OvertimeDefDefined: Boolean;
begin
  inherited;
  FPasteContractGroupCode := '';

  if (not OvertimeDefDM.TableMaster.Active) or
    OvertimeDefDM.TableMaster.IsEmpty then
    Exit;

  FPasteContractGroupCode :=
    OvertimeDefDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  OvertimeDefDefined :=
    OvertimeDefDM.OvertimeDefFor(FPasteContractGroupCode);
  if OvertimeDefDefined then
    if DisplayMessage(SPimsOvertimeDefFound, // RV067.MRA.34
        mtConfirmation, [mbYes, mbNo]) = mrNo
    then
      Exit;

  OvertimeDefDM.CopyOvertimeDef(FCopyContractGroupCode,
    FPasteContractGroupCode, OvertimeDefDefined);
end;

// GLOB3.81
procedure TOvertimeDefF.ShowFromToTime;
var
  Show: Boolean;
begin
  if not SystemDM.OvertimePerDay then // GLOB3-223
    Exit;
  if (not OvertimeDefDM.TableMaster.Active) or
    OvertimeDefDM.TableMaster.IsEmpty then
    Exit;
  Show := OvertimeDefDM.TableMaster.
    FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsString = '1';
  lblOnlyForHrsBetween.Visible := Show;
  dxDBTimeEditFromTime.Visible := Show;
  dxDBTimeEditToTime.Visible := Show;
  lblAnd.Visible := Show;
  LblWeekDay.Visible := Show;
  dxDBImgEdtDayOfWeek.Visible := Show;
end;

// GLOB3-81
procedure TOvertimeDefF.InitNTS;
var
  I: Integer;
begin
  // GLOB3-223
  lblOnlyForHrsBetween.Visible := SystemDM.OvertimePerDay;
  dxDBTimeEditFromTime.Visible := SystemDM.OvertimePerDay;
  dxDBTimeEditToTime.Visible := SystemDM.OvertimePerDay;
  lblAnd.Visible := SystemDM.OvertimePerDay;
  LblWeekDay.Visible := SystemDM.OvertimePerDay;
  dxDBImgEdtDayOfWeek.Visible := SystemDM.OvertimePerDay;
  dxDetailGridColumnFromTime.Visible := SystemDM.OvertimePerDay;
  dxDetailGridColumnFromTime.DisableCustomizing := not SystemDM.OvertimePerDay;
  dxDetailGridColumnToTime.Visible := SystemDM.OvertimePerDay;
  dxDetailGridColumnToTime.DisableCustomizing := not SystemDM.OvertimePerDay;
  dxDetailGridColumnDayOfWeek.Visible := SystemDM.OvertimePerDay;
  dxDetailGridColumnDayOfWeek.DisableCustomizing := not SystemDM.OvertimePerDay;
  if SystemDM.OvertimePerDay then
  begin
    dxDBImgEdtDayOfWeek.Values.Clear;
    dxDBImgEdtDayOfWeek.Descriptions.Clear;
    for I := 1 to 7 do
    begin
      dxDBImgEdtDayOfWeek.Values.Add(IntToStr(I));
      dxDBImgEdtDayOfWeek.Descriptions.Add(SystemDM.GetDayWDescription(I));
    end;
  end;
end; // InitNTS

end.
