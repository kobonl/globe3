inherited ReportChangesPerScanDM: TReportChangesPerScanDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryChangesPerScan: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.ABSLOG_ACTION,'
      '  T.ABSLOG_TIMESTAMP,'
      '  T.DATETIME_IN,'
      '  T.EMPLOYEE_NUMBER,'
      '  T.PLANT_CODE,'
      '  T.WORKSPOT_CODE,'
      '  T.JOB_CODE,'
      '  T.SHIFT_NUMBER,'
      '  T.DATETIME_OUT,'
      '  T.PROCESSED_YN,'
      '  T.IDCARD_IN,'
      '  T.IDCARD_OUT,'
      '  T.CREATIONDATE,'
      '  T.MUTATIONDATE,'
      '  T.MUTATOR,'
      '  T.SHIFT_DATE,'
      '  T.EXPORTED_YN'
      'FROM'
      '  ABSLOGTRS T'
      'ORDER BY'
      '  T.ABSLOG_TIMESTAMP'
      ' ')
    Left = 72
    Top = 144
  end
end
