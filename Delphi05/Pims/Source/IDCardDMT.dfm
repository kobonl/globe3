inherited IDCardDM: TIDCardDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 250
  Top = 191
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnNewRecord = TableMasterNewRecord
    Top = 40
    object TableMasterIDCARD_NUMBER: TStringField
      FieldName = 'IDCARD_NUMBER'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 15
    end
    object TableMasterEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableMasterEMPLOYEESHORTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEESHORTLU'
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'SHORT_NAME'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Size = 6
      Lookup = True
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    BeforeDelete = TableDetailBeforeDelete
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableMasterNewRecord
    TableName = 'IDCARD'
    Top = 100
    object TableDetailIDCARD_NUMBER: TStringField
      FieldName = 'IDCARD_NUMBER'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 15
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupDataSet = ClientDataSetEmpl
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailEMPLOYEESHORTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEESHORTLU'
      LookupDataSet = ClientDataSetEmpl
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'SHORT_NAME'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Size = 6
      Lookup = True
    end
    object TableDetailEMPCALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 208
    Top = 40
  end
  inherited DataSourceDetail: TDataSource
    AutoEdit = False
    Left = 216
    Top = 100
  end
  inherited TableExport: TTable
    Left = 396
  end
  object QueryIdCardEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  IDCARD_NUMBER '
      'FROM '
      '  IDCARD '
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
      'ORDER BY '
      '  IDCARD_NUMBER')
    Left = 328
    Top = 40
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, E.SHORT_NAME'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' ')
    Left = 96
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpl'
    Left = 208
    Top = 160
  end
  object DataSetProviderEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 328
    Top = 160
  end
  object DataSourceEmployee: TDataSource
    DataSet = QueryEmployee
    Left = 208
    Top = 256
  end
  object QueryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  P.DESCRIPTION AS PLANT_DESCRIPTION'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 96
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
end
