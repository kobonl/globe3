unit ExtraPaymentDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TExtraPaymentDM = class(TGridBaseDM)
    QueryDetail: TQuery;
    QueryDetailPAYMENT_DATE: TDateTimeField;
    QueryDetailEMPLOYEE_NUMBER: TIntegerField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailPAYMENT_EXPORT_CODE: TStringField;
    QueryDetailPAYMENT_AMOUNT: TFloatField;
    TableEmployee: TTable;
    DataSourceEmpl: TDataSource;
    TablePaymentExpCode: TTable;
    DataSourcePaymentExpCode: TDataSource;
    TableDetailPAYMENT_DATE: TDateTimeField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailPAYMENT_EXPORT_CODE: TStringField;
    TableDetailPAYMENT_AMOUNT: TFloatField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailEMPLU: TStringField;
    TableDetailPAYMENTEXPCODELU: TStringField;
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FDate: TDateTime;
    procedure FillQueryDetail(DateEmpl: TDateTime);
  end;

var
  ExtraPaymentDM: TExtraPaymentDM;

implementation

uses MistakePerEmplFRM, ListProcsFRM, SystemDMT;

{$R *.DFM}
procedure TExtraPaymentDM.FillQueryDetail(DateEmpl: TDateTime);
begin
  if ( DateEmpl = 0) then
    Exit;
  QueryDetail.Active := False;
  QueryDetail.UniDirectional := False;
  QueryDetail.ParamByName('FDATE').Value :=GetDate(DateEmpl);
  if not QueryDetail.Prepared then
     QueryDetail.Prepare;
  QueryDetail.Active := True;
end;

procedure TExtraPaymentDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept :=
   (Int(TableDetail.FieldByNAME('PAYMENT_DATE').AsDateTime) = Int(FDate));

end;

procedure TExtraPaymentDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetail.FieldByName('PAYMENT_DATE').AsDateTime := FDate;
end;

end.
