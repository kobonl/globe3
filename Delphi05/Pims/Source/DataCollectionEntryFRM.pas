(*
  MRA:30-OCT-2012 Overnight-Shift-System
  - Filter on SHIFT_DATE
  - Add field SHIFT_DATE for entry
  - Make choice for selection on Shiftdate or on (normal) Date.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDBPicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  - One component named dxDateEditFrom is replaced by a component of type
    TDateTimePicker.
  - Clear-button is removed from 'Start Date/Time' and 'End Date/Time' fields.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:19-MAY-2014 20015349
  - Addition of field Shift in form + grid.
  MRA:19-MAY-2014 TD-24048
  - Details section does not match the selected item when Data Collection
    Entry dialog is sorted different than default
  - This is solved by using a calculated field that defines the primary key.
    This calculated field must then be used as KEYFIELD in grid.
  MRA:11-JUL-2014 TD-25181
  - Refresh-button:
    - Always refresh when clicked. Keep the text in black.
  - Also changed the time-components to other ones (without seconds-part)
  MRA:11-APR-2016 PIM-96
  - Also refresh:
    - When dialog is opened
    - When date is changed
    - When plant is changed
  MRA:11-APR-2016 PIM-94
  - Also show shift-date in grid for manual and automatic.
*)
unit DataCollectionEntryFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, dxEditor, dxExEdtr, dxExGrEd, dxExELib,
  dxEdLib, ComCtrls, dxDBTLCl, dxGrClms, DBCtrls, dxDBELib, Mask, dxLayout,
  DbTables, Buttons, DBPicker, SystemDMT;

type
  TActiveGrid = (ProductionQuantityManual,ProductionQuantityAuto);
  TDataCollectionEntryF = class(TGridBaseF)
    pnlMasterSelection: TPanel;
    pnlPages: TPanel;
    grpBoxPlantDate: TGroupBox;
    dxDBExtLookupEditPlant: TdxDBExtLookupEdit;
    Label1: TLabel;
    Label2: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dxDBGridProductionQuantityManual: TdxDBGrid;
    dxDBGridProductionQuantityManualColumnSTARTTIME: TdxDBGridDateColumn;
    dxDBGridProductionQuantityManualColumnENDTIME: TdxDBGridDateColumn;
    dxDBGridProductionQuantityManualColumnQUANTITY: TdxDBGridColumn;
    dxDBGridProductionQuantityManualColumnWORKSPOT_CODE: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityManualColumnWORKSPOT_DESCRIPTION: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityManualColumnJOB_DESCRIPTION: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityManualColumnJOB_CODE: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityAuto: TdxDBGrid;
    dxDBGridProductionQuantityAutoColumnSTARTDATE: TdxDBGridDateColumn;
    dxDBGridProductionQuantityAutoColumnENDDATE: TdxDBGridDateColumn;
    dxDBGridProductionQuantityAutoColumnQUANTITY: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnAUTO_QUANTITY: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnCOUNTER_VALUE: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnWORKSPOT_CODE: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityAutoColumnWORKSPOT_DESCRIPTION: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityAutoColumnJOB_DESCRIPTION: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityAutoColumnJOB_CODE: TdxDBGridLookupColumn;
    dxDBGridProductionQuantityAutoColumnPQPLANT_CODE: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnPQJOB_CODE: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnPQWORKSPOT_CODE: TdxDBGridColumn;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    GrpBoxAutoDetails: TGroupBox;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    Label8: TLabel;
    DBLookupCmbBoxJobcodeAuto: TDBLookupComboBox;
    GroupBox5: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    dxDBDateEditStartDateAuto: TdxDBDateEdit;
    dxDBDateEditEndDateAuto: TdxDBDateEdit;
    GroupBox6: TGroupBox;
    DBEditQuantityAuto: TDBEdit;
    dxDBGridProductionQuantityManualColumnPQPLANT_CODE: TdxDBGridColumn;
    dxDBGridProductionQuantityManualColumnPQWORKSPOT_CODE: TdxDBGridColumn;
    dxDBGridProductionQuantityManualColumnPQJOB_CODE: TdxDBGridColumn;
    BtnAccept: TBitBtn;
    ButtonAddWK: TButton;
    GrpBoxManualDetails: TGroupBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    DBLookupCmbBoxJobcodeManual: TDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    dxDBDateEditStartDateManual: TdxDBDateEdit;
    dxDBDateEditEndDateManual: TdxDBDateEdit;
    GroupBox3: TGroupBox;
    DBEditQuantityManual: TDBEdit;
    dxDBExtLookupEditWorkspotManual: TdxDBExtLookupEdit;
    lblWorkspotDescriptionManual: TLabel;
    dxDBGridLayoutListWorkspot: TdxDBGridLayoutList;
    dxDBGridLayoutListWorkspotManual: TdxDBGridLayout;
    dxDBGridLayoutListWorkspotAuto: TdxDBGridLayout;
    dxDBExtLookupEditWorkspotAuto: TdxDBExtLookupEdit;
    lblWorkspotDescriptionAuto: TLabel;
    Label11: TLabel;
    dxDBDateEditShiftDateManual: TdxDBDateEdit;
    Label12: TLabel;
    dxDBDateEditShiftDateAuto: TdxDBDateEdit;
    RadioGroupShiftDate: TRadioGroup;
    DateEditFrom: TDateTimePicker;
    Label13: TLabel;
    DBLookupCmbBoxShiftMan: TDBLookupComboBox;
    dxDBGridProductionQuantityManualColumnSHIFT_NUMBER: TdxDBGridColumn;
    dxDBGridProductionQuantityManualColumnSHIFTDESCLU: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnSHIFT_NUMBER: TdxDBGridColumn;
    dxDBGridProductionQuantityAutoColumnSHIFTDESCLU: TdxDBGridColumn;
    Label14: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    dxDBTimeEditStartTimeManual: TdxDBTimeEdit;
    dxDBTimeEditEndTimeManual: TdxDBTimeEdit;
    dxDBTimeEditStartTimeAuto: TdxDBTimeEdit;
    dxDBTimeEditEndTimeAuto: TdxDBTimeEdit;
    dxDBGridProductionQuantityManualColumnSHIFT_DATE: TdxDBGridDateColumn;
    dxDBGridProductionQuantityAutoColumnSHIFT_DATE: TdxDBGridDateColumn;
    procedure FormDestroy(Sender: TObject);
//    procedure dxDateEditFromChange(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxBarButtonNavigationClick(Sender: TObject);
    procedure dxBarButtonRecordDetailsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pnlDetailExit(Sender: TObject);
    procedure btnAcceptClick(Sender: TObject);
    procedure dxDBExtLookupEditPlantChange(Sender: TObject);
    procedure dxDBExtLookupEditPlantSelectionChange(Sender: TObject);
    procedure ButtonAddWKClick(Sender: TObject);
    procedure dxDBDateEditStartDateAutoChange(Sender: TObject);
    procedure dxDBDateEditEndDateAutoChange(Sender: TObject);
    procedure SetTimes(Sender: TObject);
    procedure pnlDetailClick(Sender: TObject);
    procedure pnlDetailDblClick(Sender: TObject);
    procedure dxBarButtonEditModeClick(Sender: TObject);
    procedure dxDBGridProductionQuantityAutoEnter(Sender: TObject);
    procedure dxDBGridProductionQuantityAutoDblClick(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotManualPopup(Sender: TObject;
      const EditText: String);
    procedure dxDBExtLookupEditWorkspotAutoPopup(Sender: TObject;
      const EditText: String);
    procedure dxDBExtLookupEditWorkspotManualEnter(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotManualExit(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotManualKeyPress(Sender: TObject;
      var Key: Char);
    procedure dxDBExtLookupEditWorkspotAutoEnter(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotAutoExit(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotAutoKeyPress(Sender: TObject;
      var Key: Char);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure RadioGroupShiftDateClick(Sender: TObject);
    procedure DateEditFromChange(Sender: TObject);
    procedure dxDBExtLookupEditPlantCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
  private
    { Private declarations }
    ActiveFormGrid: TActiveGrid;
    procedure SetDefaultValues;
    procedure AskForSave;
    procedure ChangeGrid;
    procedure ChangeFilter;
  public
    { Public declarations }
 
//    procedure SetRefreshStatus(OnOff: Boolean); // TD-25181
    procedure RefreshGrid(OnOff: Boolean);
    procedure ResetTimes;
  end;

function DataCollectionEntryF: TDataCollectionEntryF;

implementation

{$R *.DFM}

uses
  DataCollectionEntryDMT,
  DialogAddWKDATACOLLFRM,
  UGlobalFunctions,
  UPimsConst,
  UPimsMessageRes;

var
  DataCollectionEntryF_HND: TDataCollectionEntryF;

function DataCollectionEntryF: TDataCollectionEntryF;
begin
  if (DataCollectionEntryF_HND = nil) then
  begin
    DataCollectionEntryF_HND := TDataCollectionEntryF.Create(Application);
    DataCollectionEntryDM :=
      DataCollectionEntryF_HND.CreateFormDM(TDataCollectionEntryDM);
    DataCollectionEntryF_HND.SetDefaultValues;
  end;
  Result := DataCollectionEntryF_HND;
end;

procedure TDataCollectionEntryF.FormDestroy(Sender: TObject);
begin
  inherited;
//Car 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    DataCollectionEntryDM.qryPQManual.Filtered := False;
    DataCollectionEntryDM.qryPQManual.OnFilterRecord := Nil;
    DataCollectionEntryDM.qryPQAuto.Filtered := False;
    DataCollectionEntryDM.qryPQAuto.OnFilterRecord := Nil;
  end;
  DataCollectionEntryF_HND := nil;
end;

procedure TDataCollectionEntryF.SetDefaultValues;
begin
  PageControl1.ActivePageIndex := 0;
  ActiveFormGrid := ProductionQuantityManual;
  {dxDateEditFrom.Date} DateEditFrom.DateTime := Date;
  ChangeFilter;
  ChangeGrid;
end;

procedure TDataCollectionEntryF.AskForSave;
begin
  // Ask for save changes...
  if dsrcActive.State in [dsInsert, dsEdit] then
  begin
    if DisplayMessage(SPimsSaveChanges,
      mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      case ActiveFormGrid of
      ProductionQuantityManual:
        DataCollectionEntryDM.qryPQManual.Post;
      ProductionQuantityAuto:
        DataCollectionEntryDM.qryPQAuto.Post;
      end;
    end
    else
    begin
      case ActiveFormGrid of
      ProductionQuantityManual:
        DataCollectionEntryDM.qryPQManual.Cancel;
      ProductionQuantityAuto:
        DataCollectionEntryDM.qryPQAuto.Cancel;
      end;
    end;
  end;
end;

procedure TDataCollectionEntryF.ChangeGrid;
begin
  case ActiveFormGrid of
  ProductionQuantityManual:
  begin
    ActiveGrid := dxDBGridProductionQuantityManual;
    dsrcActive.DataSet :=
      DataCollectionEntryDM.qryPQManual;
    GrpBoxAutoDetails.Visible := False;
    GrpBoxManualDetails.Visible := True;
    dxBarBDBNavDelete.visible := ivAlways;
  end;
  ProductionQuantityAuto:
  begin
    ActiveGrid := dxDBGridProductionQuantityAuto;
    dsrcActive.DataSet :=
      DataCollectionEntryDM.qryPQAuto;
    GrpBoxAutoDetails.Visible := True;
    GrpBoxManualDetails.Visible := False;
    dxBarBDBNavDelete.visible := ivNever;
  end;
  end;
{  if btnAccept.Font.Color = clRed then // TD-25181
  begin
    dxBarBDBNavDelete.Enabled := False;
    dxBarBDBNavInsert.Enabled := False;
  end; }
end;

procedure TDataCollectionEntryF.ChangeFilter;
var
  ADataFilter: TDataFilter;
begin
  ADataFilter.StartDate := {dxDateEditFrom.Date} Trunc(DateEditFrom.DateTime) {+ Frac(EncodeTime(0, 0, 0, 0))};
  ADataFilter.EndDate := {dxDateEditFrom.Date} Trunc(DateEditFrom.DateTime) + Frac(EncodeTime(23, 59, 59, 0));
  DataCollectionEntryDM.ScanFilter := ADataFilter;
end;

// TD-25181 Do not use this anymore
{
procedure TDataCollectionEntryF.SetRefreshStatus(OnOff: Boolean);
begin
  ResetTimes;
  if OnOff then
  begin
    BtnAccept.Font.Color := clWindowText;
    RefreshGrid(True);
  end
  else
  begin
    BtnAccept.Font.Color := clRed;
    RefreshGrid(False);
  end;
end;
}
{
procedure TDataCollectionEntryF.dxDateEditFromChange(Sender: TObject);
begin
  inherited;
  case PageControl1.ActivePageIndex of
    0: ActiveFormGrid := ProductionQuantityManual;
    1: ActiveFormGrid := ProductionQuantityAuto;
  end;
  ChangeFilter;
  SetRefreshStatus(False);
end;
}
procedure TDataCollectionEntryF.RefreshGrid(OnOff: Boolean);
begin
// TD-25181
{
  if not OnOff then
  begin
    // Don't close the queries!
    // Otherwise you get an error when you do the following action:
    // 1. Select something
    // 2. Edit something (edit-mode is on)
    // 3. Save the changes
    // 4. Select another plant in plant-selection-box
    // 5. Error 'Cannot perform this operation on a closed dataset'
//    DataCollectionEntryDM.qryPQManual.Close;
//    DataCollectionEntryDM.qryPQAuto.Close;
    Exit;
  end;
}
  ChangeFilter;
  case ActiveFormGrid of
    ProductionQuantityManual:
    begin
      with DataCollectionEntryDM do
      begin
        //Car 550279
        ClientDataSetWKManual.Close;
        QueryWKManual.ParamByName('PLANT_CODE').AsString :=
          TableMaster.FieldByName('PLANT_CODE').AsString;
        ClientDataSetWKManual.Open;
       //
        qryPQManual.Close;
        qryPQManual.ParamByName('PLANT_CODE').AsString :=
          TableMaster.FieldByName('PLANT_CODE').AsString;
        qryPQManual.ParamByName('DATEFROM').AsDateTime :=
          DataCollectionEntryDM.ScanFilter.StartDate;
        // 20013489 Filter on SHIFT_DATE and < the enddate instead of <=
        qryPQManual.ParamByName('DATETO').AsDateTime :=
          Trunc(DataCollectionEntryDM.ScanFilter.EndDate) + 1;
        // 20013489 Select on SHIFT_DATE or on START_DATE (0 or 1)
        qryPQManual.ParamByName('ONSHIFT').AsInteger :=
          RadioGroupShiftDate.ItemIndex;
        qryPQManual.Open;
      end;
    end;
    ProductionQuantityAuto:
    begin
      with DataCollectionEntryDM do
      begin
        //Car 550279 -
        ClientDataSetWKAuto.Close;
        QueryWKAuto.ParamByName('PLANT_CODE').AsString :=
          TableMaster.FieldByName('PLANT_CODE').AsString;
        ClientDataSetWKAuto.Open;
       //
        qryPQAuto.Close;
        qryPQAuto.ParamByName('PLANT_CODE').AsString :=
          TableMaster.FieldByName('PLANT_CODE').AsString;
        qryPQAuto.ParamByName('DATEFROM').AsDateTime :=
          {dxDateEditFrom.Date} Trunc(DateEditFrom.DateTime) {+ Frac(EncodeTime(0, 0, 0, 0))};
        // 20013489 Filter on SHIFT_DATE and < the enddate instead of <=
        qryPQAuto.ParamByName('DATETO').AsDateTime :=
          Trunc({dxDateEditFrom.Date} DateEditFrom.DateTime) + 1;
          //dxDateEditFrom.Date + Frac(EncodeTime(23, 59, 59, 59));
        // 20013489 Select on SHIFT_DATE or on START_DATE (0 or 1)
        qryPQAuto.ParamByName('ONSHIFT').AsInteger :=
          RadioGroupShiftDate.ItemIndex;
        qryPQAuto.Open;
      end;
    end;
  end;
end;

procedure TDataCollectionEntryF.pnlDetailEnter(Sender: TObject);
begin
// Do not inherit!
//  inherited;
  if pnlDetail.Visible then
  begin
    if GrpBoxAutoDetails.Visible then
      dxDBExtLookupEditWorkspotAuto.SetFocus;
//      DBLookupCmbBoxWorkspotAuto.SetFocus;
    if GrpBoxManualDetails.Visible then
      dxDBExtLookupEditWorkspotManual.SetFocus;
//      DBLookupCmbBoxWorkspotManual.SetFocus;
  end;
end;

procedure TDataCollectionEntryF.dxBarBDBNavInsertClick(Sender: TObject);
begin
// Do not inherit!
//  inherited;
  pnlDetail.Visible := True;

  // WARNING: First do 'pnlDetailEnter(Sender)'
  // then do the Insert for the given table.
  // If you do it the other way round, the insert will fail, because
  // the pnlDetailEnter triggers a the workspot-filter-setting
  // and re-loading of the records.
  pnlDetailEnter(Sender);
  case ActiveFormGrid of
  ProductionQuantityManual:
    DataCollectionEntryDM.qryPQManual.Insert;
  ProductionQuantityAuto:
    DataCollectionEntryDM.qryPQAuto.Insert;
  end;
end;

procedure TDataCollectionEntryF.PageControl1Change(Sender: TObject);
begin
  inherited;
  AskForSave;
  // When the Tab changes or has changed
  case PageControl1.ActivePageIndex of
  0: begin
       ActiveFormGrid := ProductionQuantityManual;
       ButtonAddWK.Visible := True;
     end;
  1: begin
        ActiveFormGrid := ProductionQuantityAuto;
        ButtonAddWK.Visible := False;
     end;
  end;
  ChangeFilter;
  ChangeGrid;
  if not (dsrcActive.State in [dsInsert, dsEdit])  then
    //if BtnAccept.Font.Color = ClRed then // TD-25181
      RefreshGrid(False);
    //else
      //RefreshGrid(True);
  //car 15-9-2003
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    SetNotEditable;
    ButtonAddWK.Enabled := False;
  end;
end;

procedure TDataCollectionEntryF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDataCollectionEntryF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  case ActiveFormGrid of
  ProductionQuantityManual:
    DataCollectionEntryDM.RestoreError(
      DataCollectionEntryDM.qryPQManual);
  ProductionQuantityAuto:
    DataCollectionEntryDM.RestoreError(
      DataCollectionEntryDM.qryPQAuto);
  end;
end;

procedure TDataCollectionEntryF.dxBarButtonNavigationClick(Sender: TObject);
var
  AQuery: TQuery;
begin
  AQuery := DataCollectionEntryDM.qryPQManual;
  case ActiveFormGrid of
    ProductionQuantityManual:
      AQuery := DataCollectionEntryDM.qryPQManual;
    ProductionQuantityAuto:
      AQuery := DataCollectionEntryDM.qryPQAuto;
  end;
  //550296
  if AQuery.Active and (not AQuery.IsEmpty) then
  begin
    if Sender = dxBarButtonFirst then
      AQuery.First;
    if Sender = dxBarButtonPrior then
      AQuery.Prior;
    if Sender = dxBarButtonNext then
      AQuery.Next;
    if Sender = dxBarButtonLast then
      AQuery.Last;
  end;
end;

procedure TDataCollectionEntryF.dxBarButtonRecordDetailsClick(
  Sender: TObject);
var
  AQuery: TQuery;
begin
  AQuery := DataCollectionEntryDM.qryPQManual;
  case ActiveFormGrid of
    ProductionQuantityManual:
      AQuery := DataCollectionEntryDM.qryPQManual;
    ProductionQuantityAuto:
      AQuery := DataCollectionEntryDM.qryPQAuto;
  end;
  if AQuery.Active and (not AQuery.IsEmpty ) and
    dxBarButtonRecordDetails.Down then
    pnlDetail.Visible := True
  else
  begin
    pnlDetail.Visible := False;
    dxBarButtonRecordDetails.Down := False;
  end;
end;

procedure TDataCollectionEntryF.FormShow(Sender: TObject);
begin
  if (dxDBGridProductionQuantityManual.DataSource = nil) then
    dxDBGridProductionQuantityManual.DataSource :=
      DataCollectionEntryDM.dsQryPQManual;
  if (dxDBGridProductionQuantityAuto.DataSource = nil) then
    dxDBGridProductionQuantityAuto.DataSource :=
      DataCollectionEntryDM.dsQryPQAuto;
  inherited;
  dxBarButtonEditMode.Down := False;
//  btnAccept.Font.Color := clRed; // TD-25181
  //
  if Form_Edit_YN = ITEM_VISIBIL then
    ButtonAddWK.Enabled := False;
//  dxBarButtonEditMode.Visible := ivNever;
  RefreshGrid(False); // PIM-98
end;

procedure TDataCollectionEntryF.pnlDetailExit(Sender: TObject);
begin
// Do not inherit!
//  inherited;
  // do nothing
end;

procedure TDataCollectionEntryF.btnAcceptClick(Sender: TObject);
begin
  inherited;
//  if (BtnAccept.Font.Color = clRed) then // TD-25181
//    SetRefreshStatus(True); // TD-25181
  RefreshGrid(False);
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditPlantChange(
  Sender: TObject);
begin
  inherited;
//  SetRefreshStatus(False); // TD-25181
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditPlantSelectionChange(
  Sender: TObject);
begin
  inherited;
//  SetRefreshStatus(False); // TD-25181
end;

procedure TDataCollectionEntryF.ButtonAddWKClick(Sender: TObject);
begin
  inherited;
  DialogAddWKDATACOLLF := TDialogAddWKDATACOLLF.Create(Application);
  DialogAddWKDATACOLLF.FPlant :=
    DataCollectionEntryDM.TableMaster.FieldByName('PLANT_CODE').AsString;
  DialogAddWKDATACOLLF.FDate := {dxDateEditFrom.Date} DateEditFrom.DateTime;
  try
    DialogAddWKDATACOLLF.ShowModal;
//    if (BtnAccept.Font.Color <> clRed) then // TD-25181
//      SetRefreshStatus(True);
  finally
    DialogAddWKDATACOLLF.Free;
  end;
end;

procedure TDataCollectionEntryF.dxDBDateEditStartDateAutoChange(
  Sender: TObject);
begin
  inherited;
//  DBPickerStartTimeAuto.Time := TdxDBDateEdit(Sender).Date;
  dxDBTimeEditStartTimeAuto.Time := TdxDBDateEdit(Sender).Date; // TD-25181
end;

procedure TDataCollectionEntryF.dxDBDateEditEndDateAutoChange(
  Sender: TObject);
begin
  inherited;
//  DBPickerEndTimeAuto.Time := TdxDBDateEdit(Sender).Date;
  dxDBTimeEditEndTimeAuto.Time := TdxDBDateEdit(Sender).Date; // TD-25181
end;

procedure TDataCollectionEntryF.SetTimes(Sender: TObject);
var
  ADate: Tdatetime;
  hour,min,sec,msec: word;
begin
  Adate := TDBPicker(Sender).Time;
  DecodeTime(ADate,hour,min,sec,msec);
  ReplaceTime(ADate,EncodeTime(hour,min,0,0));
  TDBPicker(Sender).Time := Adate;
end;

procedure TDataCollectionEntryF.ResetTimes;
begin
//  DBPickerStartTimeAuto.Time := 0;
//  DBPickerEndTimeAuto.Time := 0;
  dxDBTimeEditStartTimeAuto.Time := 0; // TD-25181
  dxDBTimeEditEndTimeAuto.Time := 0; // TD-25181
//  DBPickerStartTimeManual.Time := 0;
  dxDBTimeEditStartTimeManual.Time := 0; // TD-25181
  dxDBTimeEditEndTimeManual.Time := 0; // TD-25181
//  DBPickerStartTimeManual.Time := 0;
end;

procedure TDataCollectionEntryF.pnlDetailClick(Sender: TObject);
begin
// Do not inherit!
//  inherited;
// Do nothing!
end;

procedure TDataCollectionEntryF.pnlDetailDblClick(Sender: TObject);
begin
// Do not inherit!
//  inherited;
// Do nothing!
end;

procedure TDataCollectionEntryF.dxBarButtonEditModeClick(Sender: TObject);
var
  AQuery: TQuery;
begin
  // If nothing is shown in grid, don't allow edit mode!
  AQuery := DataCollectionEntryDM.qryPQManual;
  case ActiveFormGrid of
    ProductionQuantityManual:
      AQuery := DataCollectionEntryDM.qryPQManual;
    ProductionQuantityAuto:
      AQuery := DataCollectionEntryDM.qryPQAuto;
  end;
  if not AQuery.Active then
  begin
    dxBarButtonEditMode.Down := False;
    Exit;
  end
  else
    if AQuery.IsEmpty then
    begin
      dxBarButtonEditMode.Down := False;
      Exit;
    end;
  inherited;
end;

procedure TDataCollectionEntryF.dxDBGridProductionQuantityAutoEnter(
  Sender: TObject);
begin
//550296
  if not dxBarButtonEditMode.Down then
    inherited;
end;

procedure TDataCollectionEntryF.dxDBGridProductionQuantityAutoDblClick(
  Sender: TObject);
begin
//550296
  if not dxBarButtonEditMode.Down then
    inherited;
end;

procedure TDataCollectionEntryF.dxGridEnter(Sender: TObject);
begin
//550296
  if PageControl1.ActivePageIndex = 0 then
    ActiveGrid := dxDBGridProductionQuantityManual
  else
    ActiveGrid := dxDBGridProductionQuantityAuto;
  if not dxBarButtonEditMode.Down then
    inherited;
end;

procedure TDataCollectionEntryF.FormCreate(Sender: TObject);
begin
  inherited;
  lblWorkspotDescriptionManual.Caption := '';
  lblWorkspotDescriptionAuto.Caption := '';
  // 20014289
  dxDBGridProductionQuantityManual.BandFont.Color := clWhite;
  dxDBGridProductionQuantityManual.BandFont.Style := [fsBold];
  dxDBGridProductionQuantityAuto.BandFont.Color := clWhite;
  dxDBGridProductionQuantityAuto.BandFont.Style := [fsBold];
  if not SystemDM.UseShiftDateSystem then
  begin
    RadioGroupShiftDate.ItemIndex := 1; // Date
    RadioGroupShiftDate.Visible := False;
    grpBoxPlantDate.Height := grpBoxPlantDate.Height div 2;
    Label11.Visible := False; // Shift Date
    dxDBDateEditShiftDateAuto.Visible := False; // Shift Date entry
    Label12.Visible := False; // Shift Date
    dxDBDateEditShiftDateManual.Visible := False; // Shift Date entry
  end;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotManualPopup(
  Sender: TObject; const EditText: String);
begin
  inherited;
  with DataCollectionEntryDM do
  begin
    ClientDataSetWKManual.Locate('WORKSPOT_CODE',
      qryPQManual.FieldByName('WORKSPOT_CODE').AsString, []);
  end;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotAutoPopup(
  Sender: TObject; const EditText: String);
begin
  inherited;
  with DataCollectionEntryDM do
  begin
    ClientDataSetWKAuto.Locate('WORKSPOT_CODE',
      qryPQAuto.FieldByName('WORKSPOT_CODE').AsString, []);
  end;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotManualEnter(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBExtLookupEditWorkspotManual.DroppedDown := True;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotManualExit(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotManualKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBExtLookupEditWorkspotManual.DroppedDown := True;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotAutoEnter(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBExtLookupEditWorkspotAuto.DroppedDown := True;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotAutoExit(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditWorkspotAutoKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBExtLookupEditWorkspotAuto.DroppedDown := True;
end;

procedure TDataCollectionEntryF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if pnlDetail.Visible then
  begin
    if GrpBoxAutoDetails.Visible then
    begin
      if ASelected then
        if AColumn =
          dxDBGridProductionQuantityAutoColumnWORKSPOT_DESCRIPTION then
          lblWorkspotDescriptionAuto.Caption := AText;
    end;
    if GrpBoxManualDetails.Visible then
    begin
      if ASelected then
        if AColumn =
          dxDBGridProductionQuantityManualColumnWORKSPOT_DESCRIPTION then
          lblWorkspotDescriptionManual.Caption := AText;
    end;
  end;
end;

// 20013489
procedure TDataCollectionEntryF.RadioGroupShiftDateClick(Sender: TObject);
begin
  inherited;
//  SetRefreshStatus(False); // TD-25181
end;

procedure TDataCollectionEntryF.DateEditFromChange(Sender: TObject);
begin
  inherited;
  case PageControl1.ActivePageIndex of
    0: ActiveFormGrid := ProductionQuantityManual;
    1: ActiveFormGrid := ProductionQuantityAuto;
  end;
//  ChangeFilter; // PIM-96
  RefreshGrid(False); // PIM-96
//  SetRefreshStatus(False); // TD-25181
end;

procedure TDataCollectionEntryF.dxDBExtLookupEditPlantCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  RefreshGrid(False); // PIM-96
end;

end.
