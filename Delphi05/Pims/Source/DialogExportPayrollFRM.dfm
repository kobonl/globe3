inherited DialogExportPayrollF: TDialogExportPayrollF
  Left = 257
  Top = 106
  Caption = 'Export to payroll '
  ClientHeight = 485
  ClientWidth = 624
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 624
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 507
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 624
    Height = 371
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Left = 40
      Top = 42
    end
    inherited LblPlant: TLabel
      Left = 72
      Top = 42
    end
    inherited LblFromEmployee: TLabel
      Left = 40
      Top = 90
    end
    inherited LblEmployee: TLabel
      Left = 72
      Top = 90
    end
    inherited LblToPlant: TLabel
      Left = 339
      Top = 41
    end
    inherited LblToEmployee: TLabel
      Left = 339
      Top = 90
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 155
      Top = 91
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 365
      Top = 91
    end
    inherited LblFromDepartment: TLabel
      Left = 40
      Top = 140
    end
    inherited LblDepartment: TLabel
      Left = 72
      Top = 140
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 155
      Top = 142
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 365
      Top = 142
    end
    inherited LblToDepartment: TLabel
      Left = 339
      Top = 142
    end
    inherited LblStarTeamTo: TLabel
      Left = 352
      Top = 452
    end
    inherited LblToTeam: TLabel
      Top = 452
    end
    inherited LblStarTeamFrom: TLabel
      Top = 452
    end
    inherited LblTeam: TLabel
      Top = 450
    end
    inherited LblFromTeam: TLabel
      Top = 450
    end
    inherited LblFromShift: TLabel
      Top = 492
    end
    inherited LblShift: TLabel
      Top = 492
    end
    inherited LblStartShiftFrom: TLabel
      Top = 494
    end
    inherited LblToShift: TLabel
      Top = 494
    end
    inherited LblStarShiftTo: TLabel
      Top = 494
    end
    object LblFromBU: TLabel [23]
      Left = 40
      Top = 66
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblBU: TLabel [24]
      Left = 72
      Top = 66
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblToBU: TLabel [25]
      Left = 339
      Top = 66
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarBUFrom: TLabel [26]
      Left = 155
      Top = 66
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarBUTo: TLabel [27]
      Left = 365
      Top = 66
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblFromContractGroup: TLabel [28]
      Left = 40
      Top = 167
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblContractGroup: TLabel [29]
      Left = 72
      Top = 167
      Width = 70
      Height = 13
      Caption = 'Contractgroup'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToContractGroup: TLabel [30]
      Left = 339
      Top = 165
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarContractGroupFrom: TLabel [31]
      Left = 155
      Top = 165
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarContractGroupTo: TLabel [32]
      Left = 365
      Top = 165
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblStarPlantFrom: TLabel
      Left = 155
      Top = 43
    end
    inherited LblStarPlantTo: TLabel
      Left = 365
      Top = 43
    end
    inherited LblFromPlant2: TLabel
      Left = 40
      Top = 116
    end
    inherited LblPlant2: TLabel
      Left = 72
      Top = 116
    end
    inherited LblStarPlant2From: TLabel
      Left = 155
      Top = 118
    end
    inherited LblToPlant2: TLabel
      Left = 339
      Top = 116
    end
    inherited LblStarPlant2To: TLabel
      Left = 365
      Top = 118
    end
    inherited LblFromWorkspot: TLabel
      Top = 552
    end
    inherited LblWorkspot: TLabel
      Top = 552
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 554
    end
    inherited LblToWorkspot: TLabel
      Top = 552
    end
    inherited LblStarWorkspotTo: TLabel
      Top = 554
    end
    object BevelPlantDept: TBevel [45]
      Left = 37
      Top = 109
      Width = 505
      Height = 52
      Style = bsRaised
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      Left = 152
      Top = 39
      ColCount = 377
      TabOrder = 1
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 358
      Top = 39
      ColCount = 378
      TabOrder = 2
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 449
      ColCount = 266
      TabOrder = 42
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 449
      ColCount = 267
      TabOrder = 33
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 452
      TabOrder = 41
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Left = 152
      Top = 139
      ColCount = 261
      TabOrder = 12
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 358
      Top = 139
      ColCount = 262
      TabOrder = 13
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 546
      Top = 140
      Width = 76
      TabOrder = 14
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 152
      Top = 89
      TabOrder = 7
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 358
      Top = 89
      TabOrder = 8
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Top = 491
      ColCount = 266
      TabOrder = 32
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Top = 491
      ColCount = 267
      TabOrder = 30
    end
    inherited CheckBoxAllShifts: TCheckBox
      Top = 491
      TabOrder = 31
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 528
      TabOrder = 29
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 546
      TabOrder = 9
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 546
      Top = 43
      TabOrder = 3
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Left = 152
      Top = 114
      ColCount = 230
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 358
      Top = 114
      ColCount = 231
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 550
      ColCount = 231
      TabOrder = 15
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Top = 550
      ColCount = 232
      TabOrder = 16
    end
    inherited EditWorkspots: TEdit
      TabOrder = 35
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 175
      TabOrder = 36
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 174
      TabOrder = 37
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 39
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 38
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 40
    end
    inherited DatePickerFromBase: TDateTimePicker
      TabOrder = 43
    end
    object RadioGroupSort: TRadioGroup
      Left = 208
      Top = 190
      Width = 329
      Height = 41
      Caption = 'Select'
      Columns = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Period'
        'Month')
      ParentFont = False
      TabOrder = 22
      OnClick = RadioGroupSortClick
    end
    object GroupBoxMonth: TGroupBox
      Left = 40
      Top = 235
      Width = 497
      Height = 57
      Caption = 'Date selection'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 25
      object Label10: TLabel
        Left = 40
        Top = 20
        Width = 30
        Height = 13
        Caption = 'Month'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object dxSpinEditMonth: TdxSpinEdit
        Left = 88
        Top = 20
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        MaxValue = 12
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
    end
    object GroupBoxPeriod: TGroupBox
      Left = 40
      Top = 232
      Width = 497
      Height = 80
      Caption = 'Date selection'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 26
      object LabelFrom: TLabel
        Left = 8
        Top = 50
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelWeek: TLabel
        Left = 40
        Top = 50
        Width = 25
        Height = 13
        Caption = 'week'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelTo: TLabel
        Left = 179
        Top = 50
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 8
        Top = 20
        Width = 69
        Height = 13
        Caption = 'Period number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelWeekNr: TLabel
        Left = 163
        Top = 20
        Width = 69
        Height = 13
        Caption = 'Week  number'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPeriod: TLabel
        Left = 289
        Top = 50
        Width = 40
        Height = 13
        Caption = 'lblPeriod'
      end
      object lblWeekPeriod: TLabel
        Left = 312
        Top = 20
        Width = 67
        Height = 13
        Caption = 'lblWeekPeriod'
      end
      object dxSpinEditPeriodNr: TdxSpinEdit
        Left = 88
        Top = 20
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        MaxValue = 99
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object dxSpinEditWeekMin: TdxSpinEdit
        Left = 88
        Top = 50
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnChange = ChangeDate
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object dxSpinEditWeekMax: TdxSpinEdit
        Left = 216
        Top = 50
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = ChangeDate
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object dxSpinEditWKNumber: TdxSpinEdit
        Left = 240
        Top = 20
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnChange = ChangeDate
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
    end
    object GroupBoxPayDate: TGroupBox
      Left = 40
      Top = 313
      Width = 497
      Height = 50
      Caption = 'Pay Date'
      TabOrder = 27
      Visible = False
      object Label1: TLabel
        Left = 8
        Top = 23
        Width = 44
        Height = 13
        Caption = 'Pay Date'
      end
      object DTPickerPayDate: TDateTimePicker
        Left = 88
        Top = 19
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 0
      end
    end
    object GroupBoxATTENT: TGroupBox
      Left = 40
      Top = 232
      Width = 497
      Height = 80
      TabOrder = 34
      object Label2: TLabel
        Left = 8
        Top = 23
        Width = 68
        Height = 13
        Caption = 'Date between'
      end
      object DateTimePickerMin: TDateTimePicker
        Left = 88
        Top = 19
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 0
      end
      object DateTimePickerMax: TDateTimePicker
        Left = 200
        Top = 19
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 1
      end
      object CheckBoxExportHours: TCheckBox
        Left = 88
        Top = 55
        Width = 105
        Height = 17
        Caption = 'Export Hours'
        TabOrder = 2
      end
      object CheckBoxExportCounters: TCheckBox
        Left = 200
        Top = 55
        Width = 185
        Height = 17
        Caption = 'Export Counters'
        TabOrder = 3
      end
    end
    object rgpAfasSelect: TRadioGroup
      Left = 40
      Top = -2
      Width = 497
      Height = 36
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'AFAS'
        'Extern Personeel')
      TabOrder = 0
      OnClick = rgpAfasSelectClick
    end
    object GroupBoxYear: TGroupBox
      Left = 40
      Top = 190
      Width = 161
      Height = 41
      TabOrder = 21
      object Label22: TLabel
        Left = 10
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 86
        Top = 12
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = ChangeDate
        MaxValue = 2950
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 152
      Top = 64
      Width = 180
      Height = 19
      ColCount = 229
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 358
      Top = 64
      Width = 180
      Height = 19
      ColCount = 230
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusContractGroupFrom: TComboBoxPlus
      Left = 152
      Top = 164
      Width = 180
      Height = 19
      ColCount = 230
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 17
      TitleColor = clBtnFace
    end
    object ComboBoxPlusContractGroupTo: TComboBoxPlus
      Left = 358
      Top = 164
      Width = 180
      Height = 19
      ColCount = 231
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 18
      TitleColor = clBtnFace
    end
    object CheckBoxAllBusinessUnits: TCheckBox
      Left = 546
      Top = 64
      Width = 76
      Height = 17
      Caption = 'All'
      TabOrder = 6
      OnClick = CheckBoxAllBusinessUnitsClick
    end
    object CheckBoxAllContractGroups: TCheckBox
      Left = 546
      Top = 163
      Width = 76
      Height = 17
      Caption = 'All'
      TabOrder = 19
      OnClick = CheckBoxAllContractGroupsClick
    end
    object GroupBoxEasyLabor: TGroupBox
      Left = 40
      Top = 233
      Width = 497
      Height = 80
      Caption = 'Selections'
      TabOrder = 23
      object Label3: TLabel
        Left = 8
        Top = 23
        Width = 38
        Height = 13
        Caption = 'Till Date'
      end
      object DTPickerTillDate: TDateTimePicker
        Left = 88
        Top = 18
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 0
      end
      object CheckBoxIncludeOpenScans: TCheckBox
        Left = 198
        Top = 19
        Width = 225
        Height = 17
        Caption = 'Include employee(s) with open scan(s)'
        TabOrder = 1
      end
      object CheckBoxExportSwipes: TCheckBox
        Left = 198
        Top = 48
        Width = 97
        Height = 17
        Caption = 'Export Swipes'
        TabOrder = 2
      end
      object CheckBoxExportSchedule: TCheckBox
        Left = 320
        Top = 48
        Width = 97
        Height = 17
        Caption = 'Export Schedule'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
    end
    object GroupBoxFinalRun: TGroupBox
      Left = 40
      Top = 313
      Width = 497
      Height = 50
      Caption = 'Final Run'
      TabOrder = 24
      Visible = False
      object LblLastExportedTill: TLabel
        Left = 8
        Top = 20
        Width = 80
        Height = 13
        Caption = 'Last exported till'
      end
      object LblExportType: TLabel
        Left = 248
        Top = 19
        Width = 59
        Height = 13
        Caption = 'Export Type'
      end
      object EditFinalRunExportDate: TEdit
        Left = 117
        Top = 17
        Width = 121
        Height = 19
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object EditExportType: TEdit
        Left = 360
        Top = 16
        Width = 121
        Height = 19
        Ctl3D = False
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
      end
    end
    object GroupBoxDateSelection: TGroupBox
      Left = 40
      Top = 232
      Width = 497
      Height = 81
      Caption = 'Date Selection'
      TabOrder = 44
      Visible = False
      object Label4: TLabel
        Left = 8
        Top = 24
        Width = 24
        Height = 13
        Caption = 'From'
      end
      object Label5: TLabel
        Left = 48
        Top = 24
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object Label6: TLabel
        Left = 216
        Top = 24
        Width = 10
        Height = 13
        Caption = 'to'
      end
      object DateFromDate: TDateTimePicker
        Left = 88
        Top = 21
        Width = 121
        Height = 21
        CalAlignment = dtaLeft
        Date = 42373.4753661227
        Time = 42373.4753661227
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 0
        OnCloseUp = DateFromDateCloseUp
      end
      object DateToDate: TDateTimePicker
        Left = 240
        Top = 21
        Width = 121
        Height = 21
        CalAlignment = dtaLeft
        Date = 42373.4753661227
        Time = 42373.4753661227
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 1
        OnCloseUp = DateToDateCloseUp
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 466
    Width = 624
  end
  inherited pnlBottom: TPanel
    Top = 432
    Width = 624
    Height = 34
    inherited btnOk: TBitBtn
      Left = 192
      Top = 6
    end
    inherited btnCancel: TBitBtn
      Top = 6
    end
    object btnTestRun: TBitBtn
      Left = 64
      Top = 6
      Width = 105
      Height = 25
      Caption = '&Test Run'
      Default = True
      TabOrder = 2
      OnClick = btnTestRunClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000012170000121700000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFBBD8BCAFD1B0FEFFFEFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBBD8BCC0
        C0C0FEFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFAED0AF00A80000B50085BB87FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAED0AFC0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFE4EFE505920B12DC1510E413029A06E5F0E6FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4EFE5C0C0C0C0C0C0C0
        C0C0C0C0C0E5F0E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFEFFFE37953B18B81F18C41F16C91C10BF1460AB64FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFEC0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9EC89F168E1E2BA83428AB3124B02C22BA2A0C9213D7E8D7FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0D7E8D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        E2EFE2117F17359C3F329A3C13841B25942D2FA13928A1312F9034FCFEFCFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2EFE2C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0FCFEFCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFDFB
        2C8B313B99453B9A4613801ABDDABE4D9D512C9436359C401D8C2683BA85FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFBFDFBC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF74B277
        6EB1775EA8671B8221D0E5D1FFFFFFEFF7EF1A82203B9A453E9D4813801AC4DE
        C5FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0D0E5D1FFFFFFEF
        F7EFC0C0C0C0C0C0C0C0C0C0C0C0C4DEC5FFFFFFFFFFFFFFFFFF9AC79C58A660
        6DB1765CA35EEDF5EDFFFFFFFFFFFFFFFFFFE1EFE21D83234DA2574FA3591680
        1DE0EEE1FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0EDF5EDFFFFFFFFFFFFFF
        FFFFE1EFE2C0C0C0C0C0C0C0C0C0C0C0C0E0EEE1FFFFFFFFFFFF509D5377B379
        CFE4CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F2E737903C64AB6D82BB
        8B2B8930F0F7F0FFFFFFC0C0C0C0C0C0CFE4CFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFE7F2E7C0C0C0C0C0C0C0C0C0C0C0C0F0F7F0FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF876B3795AA4
        619ACBA353A158F7FBF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFF8FCF8C0C0C0C0C0C0C0C0C0C0C0C0F7FBF7FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCDE3
        CD5CA660469B4D358F3AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFCDE3CDC0C0C0C0C0C0C0C0C0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFDFEFDD0E5D1A7CEA8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFEFDD0E5D1C0C0C0FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 552
    Top = 24
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 584
    Top = 24
  end
  inherited StandardMenuActionList: TActionList
    Left = 512
    Top = 24
  end
  inherited tblPlant: TTable
    Left = 48
  end
  inherited QueryEmplFrom: TQuery
    OnFilterRecord = QueryEmplFromFilterRecord
    Left = 112
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 176
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 576
    Top = 120
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        47040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507274469616C6F674578706F7274506179726F6C6C462E4461
        7461536F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A
        650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67
        106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A69
        6E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564
        676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D656467
        6F43616E496E73657274116564676F43616E4E617669676174696F6E11656467
        6F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264
        73106564676F557365426F6F6B6D61726B7300000F546478444247726964436F
        6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265
        7206536F7274656407046373557005576964746802410942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060F454D504C4F5945
        455F4E554D42455200000F546478444247726964436F6C756D6E0F436F6C756D
        6E53686F72744E616D650743617074696F6E060A53686F7274206E616D650557
        6964746802540942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060A53484F52545F4E414D4500000F54647844424772696443
        6F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557
        6964746803B4000942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060B4445534352495054494F4E00000F5464784442477269
        64436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741
        64647265737305576964746802450942616E64496E646578020008526F77496E
        6465780200094669656C644E616D6506074144445245535300000F5464784442
        47726964436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F
        6E060F4465706172746D656E7420636F646505576964746802580942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F444550
        4152544D454E545F434F444500000F546478444247726964436F6C756D6E0A43
        6F6C756D6E5465616D0743617074696F6E06095465616D20636F64650942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060954
        45414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        1E040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507254469616C6F674578706F72
        74506179726F6C6C462E44617461536F75726365456D706C546F104F7074696F
        6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F
        42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F
        436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074
        696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E
        44656C6574650D6564676F43616E496E73657274116564676F43616E4E617669
        676174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F61
        64416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54
        6478444247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F
        6E06064E756D62657206536F7274656407046373557005576964746802350942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C75
        6D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F72
        74206E616D65055769647468024E0942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060A53484F52545F4E414D4500000F5464
        78444247726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07
        43617074696F6E06044E616D6505576964746803CC000942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060B44455343524950
        54494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464
        726573730743617074696F6E0607416464726573730557696474680265094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D650607
        4144445245535300000F546478444247726964436F6C756D6E0E436F6C756D6E
        44657074436F64650743617074696F6E060F4465706172746D656E7420636F64
        6505576964746802580942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060F4445504152544D454E545F434F444500000F5464
        78444247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E
        06095465616D20636F6465055769647468023C0942616E64496E646578020008
        526F77496E6465780200094669656C644E616D6506095445414D5F434F444500
        0000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 388
  end
  inherited QueryEmplTo: TQuery
    OnFilterRecord = QueryEmplToFilterRecord
    Left = 296
  end
  inherited qryDept: TQuery
    Left = 80
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 264
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryContractGroup: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION'
      'FROM'
      '  CONTRACTGROUP CG'
      'ORDER BY'
      '  CG.CONTRACTGROUP_CODE'
      ' ')
    Left = 232
    Top = 24
  end
end
