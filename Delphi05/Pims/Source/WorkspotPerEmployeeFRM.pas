unit WorkspotPerEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PimsFRM, DBCGrids, StdCtrls, ExtCtrls, Grids, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, dxDBTLCl, dxGrClms, ImgList, DB, Menus, StaffPlanningUnit,
  ComCtrls;
type

  TWorkspotPerEmployeeF = class(TPimsF)
    Panel2: TPanel;
    dxDBGridPlanning: TdxDBGrid;
    PopupMenuLevel: TPopupMenu;
    MainMenu: TMainMenu;
    File1: TMenuItem;
    Save1: TMenuItem;
    ExitMenu: TMenuItem;
    imgBandTexture: TImage;
    Panel1: TPanel;
    ScrollBarGrid: TScrollBar;
    PopupLevelItemB: TMenuItem;
    PopupLevelItemA: TMenuItem;
    PopupItemLevelC: TMenuItem;
    ImgListBase: TImageList;
    PopupMenuItemDate: TMenuItem;
    PopupItemLevelEmpty: TMenuItem;
    procedure dxDBGridPlanningCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure dxDBGridPlanningEditing(Sender: TObject;
      Node: TdxTreeListNode; var Allow: Boolean);
    procedure dxDBGridPlanningClick(Sender: TObject);
    procedure dxDBGridPlanningMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PopupMenuLevelPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SaveClick(Sender: TObject);
    procedure ExitMenuClick(Sender: TObject);
    procedure ScrollBarGridScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure dxDBGridPlanningCustomDrawBand(Sender: TObject;
      ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxDBGridPlanningMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure PopupMenuItemDateClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PopupItemLevelEmptyClick(Sender: TObject);
    procedure PopupLevelItemAClick(Sender: TObject);
    procedure PopupLevelItemBClick(Sender: TObject);
    procedure PopupItemLevelCClick(Sender: TObject);
//    procedure MonthCalendarGridClick(Sender: TObject);
//    procedure MonthCalendarGridGetMonthInfo(Sender: TObject;
 //     Month: Cardinal; var MonthBoldInfo: Cardinal);

  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FPosX, FPosY: Integer;

    FEdit_Workspot_Planning: String;

    FFocusedEmpl: Integer;
    // number of columns display depending on screen resolution
    FScreenShowColumn: Integer;
    // width of employee description - depending by the screen resolution 
    FDescriptionWidth: Integer;
    FEditing: Boolean;
    procedure BuildGridPlanning;
    procedure ScrollGrid(ScrollPos: Integer);
    procedure CreateFixColumn(IndexCol, IndexBand: Integer; dxGridTMP: TdxDBGrid);
    function GetStartDate(ColumnName: String; var StartDate: TDateTime): String;

  public
    { Public declarations }
    FFocusedColumn: String;
    property Edit_Workspot_Planning: String read FEdit_Workspot_Planning
      write FEdit_Workspot_Planning;
    procedure EditWorkspotPerEmployee(SetLevel, SetDate: Boolean;
      ColumnName, LevelStr: String; DateStart: TDateTime);
  end;

var
  WorkspotPerEmployeeF: TWorkspotPerEmployeeF;

implementation

{$R *.DFM}
uses SystemDMT, UPimsMessageRes,
    UPimsConst, DialogWorkspotPerEmployeeDMT, DialogCalendarFRM;

procedure TWorkspotPerEmployeeF.CreateFixColumn(IndexCol, IndexBand: Integer;
  dxGridTMP: TdxDBGrid);
begin
  dxGridTMP.Columns[IndexCol] :=  dxGridTMP.CreateColumn(TdxDBGridColumn);
  dxGridTMP.Columns[IndexCol].Caption := '';
  dxGridTMP.Columns[IndexCol].BandIndex := IndexBand;
  dxGridTMP.Columns[IndexCol].MinWidth := 2;
  dxGridTMP.Columns[IndexCol].Width := 2;
  dxGridTMP.Columns[IndexCol].Sizing := False;
  dxGridTMP.Columns[IndexCol].DisableDragging := True;
  dxGridTMP.Columns[IndexCol].Tag := 1;
end;

procedure TWorkspotPerEmployeeF.BuildGridPlanning;
var
  J, RecCount: Integer;
procedure CreateColumnOfBand(IndexCol, IndexBand, Width: Integer;
  CaptionStr, FieldByName: String);
begin
  dxDBGridPlanning.Columns[IndexCol] :=
    dxDBGridPlanning.CreateColumn(TdxDBGridColumn);

  dxDBGridPlanning.Columns[IndexCol].Caption := CaptionStr;
  dxDBGridPlanning.Columns[IndexCol].BandIndex := IndexBand;
  dxDBGridPlanning.Columns[IndexCol].Width := Width;
  dxDBGridPlanning.Columns[IndexCol].Sizing := False;
  if IndexBand > 0 then
    dxDBGridPlanning.Columns[IndexCol].Alignment := taCenter;
  dxDBGridPlanning.Columns[IndexCol].DisableDragging := True;
  if IndexBand = 0 then
    dxDBGridPlanning.Columns[IndexCol].FieldName := FieldByName
  else
  begin
    FieldByName := FieldByName + '1_BTN';
    dxDBGridPlanning.Columns[IndexCol].FieldName := FieldByName;
  end;
end;
begin
  CreateColumnOfBand( 0, 0, 80, SStaffPlnEmployee, 'EMPLOYEE_NUMBER');
  CreateColumnOfBand( 1, 0, FDescriptionWidth, SStaffPlnName, 'DESCRIPTION');

  j:= 2;
  RecCount := 0;
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
  while (Not DialogWorkspotPerEmployeeDM.ClientDataSetWK.Eof) and
    (RecCount <= FShowColumn - 1) do
  begin
    CreateBand(dxDBGridPlanning, RecCount + 1, 81,
      DialogWorkspotPerEmployeeDM.GetWkCode +
      CHR_SEP + Chr(Ord(#13)) +
      Copy(DialogWorkspotPerEmployeeDM.ClientDataSetWK.
        FieldByName('DESCRIPTION').AsString, 0, 20));

    CreateColumnOfBand(J, RecCount + 1, 80, '',
        'WK' + IntToStr(RecCount));
    if RecCount < FShowColumn then
    begin
      CreateFixColumn(j + 1, RecCount, dxDBGridPlanning);
      j := j + 1;
    end;
    j := j + 1;
    Inc(RecCount);
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.Next;

  end;
  dxDBGridPlanning.Bands[0].Fixed := bfLeft;
end;

procedure TWorkspotPerEmployeeF.ScrollGrid(ScrollPos: Integer);
var
  i, j1, RecCount, MaxIndexWK, LastIndexWK: Integer; 
begin
  Screen.Cursor := crHourGlass;
  LastIndexWK :=  ScrollPos - 1;

  MaxIndexWK := LastIndexWK + FShowColumn - 1;

  if MaxIndexWK >= FWkCount then
  begin
    MaxIndexWK := FWKCount - 1;
    LastIndexWK := MaxIndexWK - FShowColumn + 1;
  end;
//
  dxDBGridPlanning.Items[0].Focused := True;
  i := 1;
  j1:= 2;
  dxDBGridPlanning.DataSource := Nil;
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
  //Car 30.3.2004 - bug
//  DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(LastIndexWK - 1);
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(LastIndexWK);
  for RecCount := LastIndexWK to MaxIndexWK do
  begin
    dxDBGridPlanning.Bands[i].Caption :=
      DialogWorkspotPerEmployeeDM.GetWkCode +
//      DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('WKCODE').AsString +
      ' ' + Chr(Ord(#13)) +
      Copy(DialogWorkspotPerEmployeeDM.ClientDataSetWK.
        FieldByName('DESCRIPTION').AsString, 0 , 20);

    dxDBGridPlanning.Columns[J1].FieldName := 'WK' +  IntToStr(RecCount) + '1_BTN';
    j1 := j1 + 2;
    Inc(i);
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.Next;
    if DialogWorkspotPerEmployeeDM.ClientDataSetWK.Eof then
      Break;
  end;
  dxDBGridPlanning.DataSource :=  DialogWorkspotPerEmployeeDM.DataSourcePIVOT;
  Screen.Cursor := crDefault;
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if AColumn.TAG = 1 THEN
    AColor := ClBlack
  else
    if ANode.Index mod 2 <> 0 then
      AColor := clScanRouteP1{Normal }
    else
      AColor := clInactiveBorder;{clGray;}
 // end;
end;

procedure TWorkspotPerEmployeeF.FormCreate(Sender: TObject);
begin
  inherited;
  with Screen do
  begin
    //CAR 550279 - depends on the resolution
    // 81 is the length of a band for a workspot
    FScreenShowColumn :=((Width - 34 - 260 ) div  81);
    // width of scroll of grid planning is almost 34;
    // 260 is minimum width of bands[0] (employee number - 80 + description - 180)
    // 80 is width of employee number
    FDescriptionWidth := Width - 34 - (FScreenShowColumn * 81) - 80  ;
  end;

  FSaveChanges := False;

  dxDBGridPlanning.DataSource := Nil;

  FShowColumn := Min(FScreenShowColumn, FWKCount + 1);
  BuildGridPlanning;

  dxDBGridPlanning.DataSource :=  DialogWorkspotPerEmployeeDM.DataSourcePIVOT;
 
  if (FWKCount + 1 - FShowColumn - 1) < 0 then
  begin
    ScrollBarGrid.Visible := False;
    Exit;
  end
  else
    ScrollBarGrid.Visible := True;
  ScrollBarGrid.Min := 1;
  ScrollBarGrid.Max := FWKCount ;//+1
  ScrollBarGrid.LargeChange := FShowColumn;
  ScrollBarGrid.SmallChange := 1;
  ScrollBarGrid.PageSize := FShowColumn;

  DialogCalendarF.Visible := False;
  dxDBGridPlanning.Hint := '';
end;

function TWorkspotPerEmployeeF.GetStartDate(ColumnName: String;
  var StartDate: TDateTime): String;
var
  Empl, RecCountWK: Integer;
begin
  Result := '';
  StartDate := FCurrentDate;
  if ColumnName = '' then
    Exit;
  if (Pos('EMPLOYEE_NUMBER', ColumnName) > 0) or
    (Pos('DESCRIPTION', ColumnName)> 0) then
    Exit;

  if not WKEmpl_GetFieldsPivot(ColumnName, DialogWorkspotPerEmployeeDM.TablePivot,
    Empl, RecCountWK) then
    Exit;
  if (DialogWorkspotPerEmployeeDM.TablePivot.FieldByName(ColumnName).AsString <> '') then
  begin
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(RecCountWK);
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Filter := '';
    if DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.FindKey([Empl, FPlant,
      DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('DEPTCODE').AsString,
      DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('WKCODE').AsString]) then
    begin
      Result := DateToStr(Trunc(DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('STARTDATE_LEVEL').AsDateTime));
      StartDate := Trunc(DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('STARTDATE_LEVEL').AsDateTime);
    end;
  end;
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningEditing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
var
  ColumnName: String;
  StartDate: TDateTime;
begin
  inherited;
  Allow := False;
 
  if DXdbGridPlanning.FocusedField <> nil then
    ColumnName := DXdbGridPlanning.FocusedField.FieldName;
  TDXDBGrid(Sender).Hint := GetStartDate(ColumnName, StartDate);
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningClick(Sender: TObject);
begin
  inherited;
  if Edit_Workspot_Planning <> ITEM_VISIBIL_EDIT then
    Exit;
  //DateTimePicker_StartDate.Hide;
  DialogCalendarF.Hide;
  if DXdbGridPlanning.FocusedField = nil then
    Exit;
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ColumnName: String;
  StartDate: TDateTime;
begin
  inherited;

  if Edit_Workspot_Planning <> ITEM_VISIBIL_EDIT then
    Exit;
  if (DXdbGridPlanning.GetColumnAT(X,Y) = Nil) or (Y <= 50) or
     (DXdbGridPlanning.FocusedField = nil) then
  begin
    DXdbGridPlanning.Hint := '';
    DXdbGridPlanning.PopupMenu := Nil;
    Exit;
  end
  else
  begin
    if DXdbGridPlanning.FocusedField <> nil then
    begin
      ColumnName := DXdbGridPlanning.FocusedField.FieldName;
      if (Pos('WK', ColumnName) = 0) then
        DXdbGridPlanning.PopupMenu := Nil
      else
        DXdbGridPlanning.PopupMenu := PopupMenuLevel;
      DXdbGridPlanning.Hint := GetStartDate(ColumnName, StartDate);
    end;
  end;
end;

procedure TWorkspotPerEmployeeF.PopupMenuLevelPopup(Sender: TObject);
var
  StartDate: TDateTime;
begin
  dxDBGridPlanning.PopupMenu := PopupMenuLevel;
  if DXdbGridPlanning.FocusedField <> Nil then
    PopupMenuItemDate.Caption :=
      GetStartDate(DXdbGridPlanning.FocusedField.FieldName, StartDate)
  else
  begin
    dxDBGridPlanning.PopupMenu := Nil;
    Exit;
  end;

  if PopupMenuItemDate.Caption = '' then
  begin
    StartDate := FCurrentDate;
    PopupMenuItemDate.Caption := DateToStr(StartDate);
    PopupMenuItemDate.Enabled:= False;
  end
  else
    PopupMenuItemDate.Enabled:= True;

  DialogCalendarF.MonthCalendarGrid.Date := StartDate;
  DialogCalendarF.Visible := False;

  inherited;
end;

procedure TWorkspotPerEmployeeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if FEditing then
    if DisplayMessage(SSaveChanges, mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      DialogWorkspotPerEmployeeDM.SaveChanges;
      FEditing := False;
    end;
  inherited;
  DialogCalendarF.Hide;
end;

procedure TWorkspotPerEmployeeF.SaveClick(Sender: TObject);
begin
  inherited;
  DialogCalendarF.Visible := False;
  if not FEditing then
    Exit;

  if DisplayMessage(SSaveChanges, mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    DialogWorkspotPerEmployeeDM.SaveChanges;
    FEditing := False;
  end;
end;

procedure TWorkspotPerEmployeeF.ExitMenuClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TWorkspotPerEmployeeF.ScrollBarGridScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  if (Scrollcode = scEndScroll) then
  begin
    if ScrollPos > (ScrollBarGrid.Max - ScrollBarGrid.PageSize + 1) then
      ScrollPos := (ScrollBarGrid.Max - ScrollBarGrid.PageSize + 1);

    if ScrollPos < 0 then
      ScrollPos := 0;

    ScrollGrid(ScrollPos);
  end;
  dxDBGridPlanning.SetFocus;
  dxDBGridPlanning.Hint := '';
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningCustomDrawBand(Sender: TObject;
  ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
 if ABand.Index = 0 then
  begin
   ACanvas.Font.Size := 12;
   ACanvas.Font.Style := [fsBold];
  end;
end;

procedure TWorkspotPerEmployeeF.dxDBGridPlanningMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  p, IndexWK: Integer;
  WK, StrTmp, LongDesc: String;

  G: TdxDBGrid;
  HitTest: TdxTreeListHitTest;
  CurCol: TdxDBTreeListColumn;
  BandCurrent: TdxTreeListBand;
  StartDate: TDateTime;
begin
  inherited;
  // show hint of caption band - workspots description
  G := Sender as TdxDBGrid;
  G.Hint := '';
  G.ShowHint := True;
  //save x, y position
  FPosX := x;
  FPosY := Y;
  
  CurCol := G.GetColumnAt(X, Y);
  if CurCol <> Nil then
    G.Hint := GetStartDate(CurCol.FieldName, StartDate);

  try
    HitTest := G.GetHitTestInfoAt(X, Y);
    BandCurrent :=  G.GetBandAt(X, Y);

    if (HitTest = htBand) and (BandCurrent <> Nil) then
    begin
      StrTmp := BandCurrent.Caption;
      if StrTmp = '' then
        Exit;
      p := pos(#13, StrTmp);
      if p = 0 then
      begin
        G.ShowHint := False;
        Exit;
      end;
      WK := Copy(StrTmp, 0 , p - 2);
      IndexWK := FWKList.IndexOf(WK);
      LongDesc := Copy(StrTmp, p + 1, Length(StrTmp) +1);
      if (IndexWK >= 0) then
        LongDesc := GetLongDescWK(FWKDescList.Strings[IndexWK]);
      G.Hint := LongDesc;
    end;
  except
      // do nothing
  end;
end;

procedure TWorkspotPerEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkspotPerEmployeeF := Nil;
end;

procedure TWorkspotPerEmployeeF.PopupMenuItemDateClick(Sender: TObject);
var
  LeftY, TopX,
  WidthColumns_Empl,
  WidthColumn_Wk,
  Height_Col,
  Height_Header,
  Width_Calendar,
  Height_Calendar: Integer;
begin
  inherited;
  Height_Header := 50;
  Height_Col := 17;
  WidthColumns_Empl := dxDBGridPlanning.Columns[0].Width +
    dxDBGridPlanning.Columns[1].Width;
  WidthColumn_Wk := dxDBGridPlanning.Columns[2].Width +
    dxDBGridPlanning.Columns[3].Width;
  Width_Calendar := DialogCalendarF.Width;
  Height_Calendar := DialogCalendarF.Height;
  LeftY := ((FPosX - WidthColumns_Empl) div WidthColumn_Wk) ;
  LeftY := WidthColumns_Empl + (LeftY + 1) * WidthColumn_Wk;

  if LeftY + Width_Calendar >= WorkspotPerEmployeeF.Width then
    LeftY := LeftY - Width_Calendar - WidthColumn_Wk;

  TopX := ((FPosY - Height_Header) div Height_Col);
  TopX := Height_Header + TopX * Height_Col;
  if TopX + Height_Calendar > WorkspotPerEmployeeF.Height then
    TopX := WorkspotPerEmployeeF.Height - Height_Calendar - ScrollBarGrid.Height;

  DialogCalendarF.Top := TopX ;
  DialogCalendarF.Left := LeftY + 3;

  DialogCalendarF.Visible := True;
  if DXdbGridPlanning.FocusedField <> nil then
    FFocusedColumn := DXdbGridPlanning.FocusedField.FieldName;
  FFocusedEmpl :=
    DialogWorkspotPerEmployeeDM.TablePivot.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if Trunc(DialogCalendarF.MonthCalendarGrid.Date) = 0 then
    DialogCalendarF.MonthCalendarGrid.Date := FCurrentDate;
end;

procedure TWorkspotPerEmployeeF.FormShow(Sender: TObject);
begin
  inherited;
  if (Edit_Workspot_Planning = ITEM_VISIBIL_EDIT) then
    dxDBGridPlanning.PopupMenu := PopupMenuLevel
  else
    dxDBGridPlanning.PopupMenu := Nil;
  DialogCalendarF.Visible := False;
end;

procedure TWorkspotPerEmployeeF.PopupItemLevelEmptyClick(Sender: TObject);
var
  ColumnName, Dept, WK: String;
  Empl, RecCountWK: Integer;
begin
  inherited;
  if DXdbGridPlanning.FocusedField = nil then
    Exit;

  ColumnName := DXdbGridPlanning.FocusedField.FieldName;
  if not WKEmpl_GetFieldsPivot(ColumnName,
    DialogWorkspotPerEmployeeDM.TablePivot, Empl, RecCountWK) then
    Exit;

  DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Filter := '';
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(RecCountWK);
  WK := DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('WKCODE').AsString;
  Dept := DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('DEPTCODE').AsString;
  if DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.FindKey([Empl, FPlant,
    Dept, WK]) then
  begin
    FEditing := True;
    DialogWorkspotPerEmployeeDM.Insert_SaveClientDataSet(Empl, Wk, Dept);

    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Delete;
    DialogWorkspotPerEmployeeDM.TablePivot.Edit;
    DialogWorkspotPerEmployeeDM.TablePivot.FieldByName(ColumnName).AsString := '';
    DialogWorkspotPerEmployeeDM.TablePivot.Post;
  end;
end;

procedure TWorkspotPerEmployeeF.EditWorkspotPerEmployee(SetLevel, SetDate: Boolean;
  ColumnName, LevelStr: String; DateStart: TDateTime);
var
  WK, Dept: String;
  Empl, RecCountWK: Integer;
begin
  if not WKEmpl_GetFieldsPivot(ColumnName,
    DialogWorkspotPerEmployeeDM.TablePivot, Empl, RecCountWK) then
    Exit;
  if Not SetLevel and Not SetDate then
    Exit;
    
  if SetDate then
    Empl := FFocusedEmpl;

  FEditing := True;
  DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Filter := '';
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
  DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(RecCountWK);
  WK :=
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('WKCODE').AsString;
  Dept :=
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('DEPTCODE').AsString;

  DialogWorkspotPerEmployeeDM.Insert_SaveClientDataSet(Empl, Wk, Dept);
  if DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.FindKey([Empl, FPlant,
    Dept, Wk]) then
  begin
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Edit;
    if SetLevel  then
    begin
      if DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('EMPLOYEE_LEVEL').AsString = LevelStr then
        FEditing := False;
      DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('EMPLOYEE_LEVEL').AsString := LevelStr;

    end;
    if SetDate then
    begin
      if DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('STARTDATE_LEVEL').AsDateTime = DateStart then
        FEditing := False;
      DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
        FieldByName('STARTDATE_LEVEL').AsDateTime := DateStart;

    end;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Post;
  end
  else
  begin
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Insert;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('EMPLOYEE_LEVEL').AsString := LevelStr;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('STARTDATE_LEVEL').AsDateTime := FCurrentDate;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('PLANT_CODE').AsString := FPlant;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('DEPARTMENT_CODE').AsString := Dept;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.
      FieldByName('WORKSPOT_CODE').AsString :=  Wk;
    DialogWorkspotPerEmployeeDM.ClientDataSetWKPerEmpl.Post;
  end;
  if SetLevel then
  begin
    DialogWorkspotPerEmployeeDM.TablePivot.Edit;
    DialogWorkspotPerEmployeeDM.TablePivot.FieldByName(ColumnName).AsString :=
      LevelStr;
    DialogWorkspotPerEmployeeDM.TablePivot.Post;
  end;
end;

procedure TWorkspotPerEmployeeF.PopupLevelItemAClick(Sender: TObject);
begin
  inherited;
  if DXdbGridPlanning.FocusedField = nil then
    Exit;
  EditWorkspotPerEmployee(True, False,
    DXdbGridPlanning.FocusedField.FieldName, 'A', FCurrentDate);
end;

procedure TWorkspotPerEmployeeF.PopupLevelItemBClick(Sender: TObject);
begin
  inherited;
  if DXdbGridPlanning.FocusedField = nil then
    Exit;
  EditWorkspotPerEmployee(True, False,
    DXdbGridPlanning.FocusedField.FieldName, 'B', FCurrentDate);
end;

procedure TWorkspotPerEmployeeF.PopupItemLevelCClick(Sender: TObject);
begin
  inherited;
  if DXdbGridPlanning.FocusedField = nil then
    Exit;
  EditWorkspotPerEmployee(True, False,
    DXdbGridPlanning.FocusedField.FieldName, 'C', FCurrentDate);
end;

{procedure TWorkspotPerEmployeeF.MonthCalendarGridClick(Sender: TObject);
begin
  inherited;
  if FChangeDate then
  begin
    EditWorkspotPerEmployee(False, True,
      FFocusedColumn, '', MonthCalendarGrid.Date);
    MonthCalendarGrid.Hide;
  end;
  FChangeDate := True;
end;

procedure TWorkspotPerEmployeeF.MonthCalendarGridGetMonthInfo(
  Sender: TObject; Month: Cardinal; var MonthBoldInfo: Cardinal);
begin
  inherited;
  FChangeDate := False;
end;
      }

// RV089.1. Ensure the form stays on top of the calling form.
procedure TWorkspotPerEmployeeF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.

