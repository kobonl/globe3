inherited DataCollectionEntryDM: TDataCollectionEntryDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 453
  Top = 106
  Height = 645
  Width = 805
  inherited TableMaster: TTable
    TableName = 'PLANT'
    Top = 8
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  inherited TableDetail: TTable
    Top = 60
  end
  inherited DataSourceMaster: TDataSource
    Left = 208
    Top = 8
  end
  inherited DataSourceDetail: TDataSource
    Top = 68
  end
  inherited TableExport: TTable
    Left = 356
    Top = 356
  end
  inherited DataSourceExport: TDataSource
    Top = 348
  end
  object DataSourceWorkspotManual: TDataSource
    DataSet = ClientDataSetWKManual
    Left = 208
    Top = 240
  end
  object TableJobcodeManual: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = dsQryPQManual
    TableName = 'JOBCODE'
    Left = 88
    Top = 352
    object TableJobcodeManualPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeManualWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeManualJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeManualBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeManualDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableJobcodeManualNORM_PROD_LEVEL: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
      Required = True
    end
    object TableJobcodeManualBONUS_LEVEL: TFloatField
      FieldName = 'BONUS_LEVEL'
      Required = True
    end
    object TableJobcodeManualNORM_OUTPUT_LEVEL: TFloatField
      FieldName = 'NORM_OUTPUT_LEVEL'
      Required = True
    end
    object TableJobcodeManualINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Size = 6
    end
    object TableJobcodeManualDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableJobcodeManualCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableJobcodeManualMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableJobcodeManualMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object DataSourceJobcodeManual: TDataSource
    DataSet = TableJobcodeManual
    Left = 216
    Top = 352
  end
  object DataSourceWorkspotAuto: TDataSource
    DataSet = ClientDataSetWKAuto
    Left = 208
    Top = 296
  end
  object TableJobcodeAuto: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = dsQryPQAuto
    TableName = 'JOBCODE'
    Left = 88
    Top = 408
    object TableJobcodeAutoPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeAutoWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeAutoJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeAutoBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableJobcodeAutoDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableJobcodeAutoNORM_PROD_LEVEL: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
      Required = True
    end
    object TableJobcodeAutoBONUS_LEVEL: TFloatField
      FieldName = 'BONUS_LEVEL'
      Required = True
    end
    object TableJobcodeAutoNORM_OUTPUT_LEVEL: TFloatField
      FieldName = 'NORM_OUTPUT_LEVEL'
      Required = True
    end
    object TableJobcodeAutoINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Size = 6
    end
    object TableJobcodeAutoDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableJobcodeAutoCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableJobcodeAutoMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableJobcodeAutoMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object DataSourceJobcodeAuto: TDataSource
    DataSet = TableJobcodeAuto
    Left = 216
    Top = 408
  end
  object QueryDeptPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  DISTINCT '
      '  D.PLANT_CODE, '
      '  D.DEPARTMENT_CODE, '
      '  D.DESCRIPTION '
      'FROM '
      '  DEPARTMENT D '
      'WHERE '
      '  D.PLANT_CODE = :PLANT_CODE '
      'ORDER BY '
      '  D.DEPARTMENT_CODE')
    Left = 464
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryWKPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  DISTINCT '
      '  W.PLANT_CODE, '
      '  W.WORKSPOT_CODE, '
      '  W.DESCRIPTION '
      'FROM '
      '  WORKSPOT W'
      'WHERE '
      '  W.PLANT_CODE = :PLANT_CODE '
      'ORDER BY '
      '  W.WORKSPOT_CODE ')
    Left = 352
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryPQManual: TQuery
    BeforePost = DefaultBeforePost
    AfterPost = qryPQManualAfterPost
    BeforeDelete = DefaultBeforeDelete
    OnCalcFields = qryPQManualCalcFields
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = qryPQManualNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    RequestLive = True
    SQL.Strings = (
      'SELECT'
      '  PQ.PLANT_CODE,'
      '  PQ.WORKSPOT_CODE,'
      '  PQ.JOB_CODE,'
      '  PQ.SHIFT_NUMBER,'
      '  PQ.MANUAL_YN,'
      '  PQ.START_DATE,'
      '  PQ.END_DATE,'
      '  PQ.QUANTITY,'
      '  PQ.COUNTER_VALUE,'
      '  PQ.AUTOMATIC_QUANTITY,'
      '  PQ.CREATIONDATE,'
      '  PQ.MUTATIONDATE,'
      '  PQ.MUTATOR,'
      '  PQ.SHIFT_DATE'
      'FROM'
      '  PRODUCTIONQUANTITY PQ'
      'WHERE'
      '  PQ.MANUAL_YN = '#39'Y'#39' AND'
      '  PQ.PLANT_CODE = :PLANT_CODE AND'
      '  ('
      '    (PQ.SHIFT_DATE >= :DATEFROM AND'
      '    PQ.SHIFT_DATE < :DATETO AND :ONSHIFT = 0)'
      '  OR'
      '    (PQ.START_DATE >= :DATEFROM AND'
      '    PQ.START_DATE < :DATETO AND :ONSHIFT = 1)'
      '  )'
      'ORDER BY'
      '  PQ.START_DATE DESC'
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 88
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end>
    object qryPQManualPLANT_CODE: TStringField
      Tag = 1
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryPQManualWORKSPOT_CODE: TStringField
      Tag = 1
      FieldName = 'WORKSPOT_CODE'
      OnChange = qryPQManualWORKSPOT_CODEChange
      Size = 6
    end
    object qryPQManualJOB_CODE: TStringField
      Tag = 1
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object qryPQManualSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object qryPQManualSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
    end
    object qryPQManualEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object qryPQManualQUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'QUANTITY'
    end
    object qryPQManualCOUNTER_VALUE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'COUNTER_VALUE'
    end
    object qryPQManualAUTOMATIC_QUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'AUTOMATIC_QUANTITY'
    end
    object qryPQManualWORKSPOT_CODELU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOT_CODELU'
      LookupDataSet = ClientDataSetWKManual
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'WORKSPOT_CODE'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQManualWORKSPOT_DESCRIPTIONLU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOT_DESCRIPTIONLU'
      LookupDataSet = ClientDataSetWKManual
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQManualAUTOMATIC_DATACOL_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'AUTOMATIC_DATACOL_YNLU'
      LookupDataSet = ClientDataSetWKManual
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'AUTOMATIC_DATACOL_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQManualUSE_JOBCODE_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'USE_JOBCODE_YNLU'
      LookupDataSet = ClientDataSetWKManual
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'USE_JOBCODE_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQManualMEASURE_PRODUCTIVITY_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'MEASURE_PRODUCTIVITY_YNLU'
      LookupDataSet = ClientDataSetWKManual
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'MEASURE_PRODUCTIVITY_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQManualMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
      Size = 1
    end
    object qryPQManualCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object qryPQManualMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object qryPQManualMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object qryPQManualJOBCODELU: TStringField
      FieldKind = fkLookup
      FieldName = 'JOBCODELU'
      LookupDataSet = TableJobcodeManual
      LookupKeyFields = 'JOB_CODE'
      LookupResultField = 'JOB_CODE'
      KeyFields = 'JOB_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
    object qryPQManualJOBDESC_CAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'JOBDESC_CAL'
      Size = 30
      Calculated = True
    end
    object qryPQManualSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
    end
    object qryPQManualSHIFT_DESC_LU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFT_DESC_LU'
      LookupDataSet = TableShiftManualLU
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      Size = 30
      Lookup = True
    end
    object qryPQManualMYROWID_CAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'MYROWID_CAL'
      Size = 255
      Calculated = True
    end
  end
  object dsQryPQManual: TDataSource
    DataSet = qryPQManual
    Left = 208
    Top = 120
  end
  object qryPQAuto: TQuery
    BeforePost = DefaultBeforePost
    AfterPost = qryPQAutoAfterPost
    OnCalcFields = qryPQAutoCalcFields
    OnEditError = DefaultEditError
    OnNewRecord = qryPQAutoNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    RequestLive = True
    SQL.Strings = (
      'SELECT'
      '  PQ.PLANT_CODE,'
      '  PQ.WORKSPOT_CODE,'
      '  PQ.JOB_CODE,'
      '  PQ.SHIFT_NUMBER,'
      '  PQ.MANUAL_YN,'
      '  PQ.START_DATE,'
      '  PQ.END_DATE,'
      '  PQ.QUANTITY,'
      '  PQ.COUNTER_VALUE,'
      '  PQ.AUTOMATIC_QUANTITY,'
      '  PQ.CREATIONDATE,'
      '  PQ.MUTATIONDATE,'
      '  PQ.MUTATOR,'
      '  PQ.SHIFT_DATE'
      'FROM'
      '  PRODUCTIONQUANTITY PQ'
      'WHERE'
      '  PQ.MANUAL_YN = '#39'N'#39' AND'
      '  PQ.PLANT_CODE = :PLANT_CODE AND'
      '  ('
      '    (PQ.SHIFT_DATE >= :DATEFROM AND'
      '    PQ.SHIFT_DATE < :DATETO AND :ONSHIFT = 0)'
      '    OR'
      '    (PQ.START_DATE >= :DATEFROM AND'
      '    PQ.START_DATE < :DATETO AND :ONSHIFT = 1)'
      '  )'
      'ORDER BY'
      '  PQ.START_DATE DESC'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 88
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end>
    object qryPQAutoPLANT_CODE: TStringField
      Tag = 1
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryPQAutoWORKSPOT_CODE: TStringField
      Tag = 1
      FieldName = 'WORKSPOT_CODE'
      OnChange = qryPQAutoWORKSPOT_CODEChange
      Size = 6
    end
    object qryPQAutoJOB_CODE: TStringField
      Tag = 1
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object qryPQAutoSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object qryPQAutoSTART_DATE: TDateTimeField
      FieldName = 'START_DATE'
    end
    object qryPQAutoEND_DATE: TDateTimeField
      FieldName = 'END_DATE'
    end
    object qryPQAutoQUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'QUANTITY'
    end
    object qryPQAutoCOUNTER_VALUE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'COUNTER_VALUE'
    end
    object qryPQAutoAUTOMATIC_QUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'AUTOMATIC_QUANTITY'
    end
    object qryPQAutoWORKSPOT_CODELU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOT_CODELU'
      LookupDataSet = ClientDataSetWKAuto
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'WORKSPOT_CODE'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQAutoWORKSPOT_DESCRIPTIONLU: TStringField
      FieldKind = fkLookup
      FieldName = 'WORKSPOT_DESCRIPTIONLU'
      LookupDataSet = ClientDataSetWKAuto
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQAutoAUTOMATIC_DATACOL_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'AUTOMATIC_DATACOL_YNLU'
      LookupDataSet = ClientDataSetWKAuto
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'AUTOMATIC_DATACOL_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQAutoUSE_JOBCODE_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'USE_JOBCODE_YNLU'
      LookupDataSet = ClientDataSetWKAuto
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'USE_JOBCODE_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQAutoMEASURE_PRODUCTIVITY_YNLU: TStringField
      FieldKind = fkLookup
      FieldName = 'MEASURE_PRODUCTIVITY_YNLU'
      LookupDataSet = ClientDataSetWKAuto
      LookupKeyFields = 'WORKSPOT_CODE'
      LookupResultField = 'MEASURE_PRODUCTIVITY_YN'
      KeyFields = 'WORKSPOT_CODE'
      LookupCache = True
      Lookup = True
    end
    object qryPQAutoMANUAL_YN: TStringField
      FieldName = 'MANUAL_YN'
      Size = 1
    end
    object qryPQAutoCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object qryPQAutoMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object qryPQAutoMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object qryPQAutoJOBCODELU: TStringField
      FieldKind = fkLookup
      FieldName = 'JOBCODELU'
      LookupDataSet = TableJobcodeAuto
      LookupKeyFields = 'JOB_CODE'
      LookupResultField = 'JOB_CODE'
      KeyFields = 'JOB_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
    object qryPQAutoJOBDESC_CAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'JOBDESC_CAL'
      Size = 30
      Calculated = True
    end
    object qryPQAutoSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
    end
    object qryPQAutoSHIFT_DESC_LU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFT_DESC_LU'
      LookupDataSet = TableShiftAutoLU
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      Size = 30
      Lookup = True
    end
    object qryPQAutoMYROWID_CAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'MYROWID_CAL'
      Size = 255
      Calculated = True
    end
  end
  object dsQryPQAuto: TDataSource
    DataSet = qryPQAuto
    Left = 208
    Top = 184
  end
  object QueryWKManual: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  W.WORKSPOT_CODE, W.DESCRIPTION,'
      '  W.AUTOMATIC_DATACOL_YN, W.USE_JOBCODE_YN,'
      '  W.MEASURE_PRODUCTIVITY_YN'
      'FROM'
      '  WORKSPOT W'
      'WHERE '
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.AUTOMATIC_DATACOL_YN = '#39'N'#39' AND'
      '  W.USE_JOBCODE_YN = '#39'Y'#39'  AND'
      '  W.MEASURE_PRODUCTIVITY_YN = '#39'Y'#39' '
      'ORDER BY '
      '  W.WORKSPOT_CODE')
    Left = 88
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryWKAuto: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  W.WORKSPOT_CODE, W.DESCRIPTION,'
      '  W.AUTOMATIC_DATACOL_YN, W.USE_JOBCODE_YN,'
      '  W.MEASURE_PRODUCTIVITY_YN  '
      'FROM'
      '  WORKSPOT W'
      'WHERE '
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.AUTOMATIC_DATACOL_YN = '#39'Y'#39'  AND'
      '  W.USE_JOBCODE_YN = '#39'Y'#39' AND'
      '  W.MEASURE_PRODUCTIVITY_YN  = '#39'Y'#39
      'ORDER BY '
      '  W.WORKSPOT_CODE')
    Left = 88
    Top = 296
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetWKManual: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'CLIENTDATASETWKMANUALINDEX1'
      end>
    Params = <>
    ProviderName = 'DataSetProviderWKManual'
    StoreDefs = True
    Left = 360
    Top = 240
  end
  object DataSetProviderWKManual: TDataSetProvider
    DataSet = QueryWKManual
    Constraints = True
    Left = 488
    Top = 240
  end
  object ClientDataSetWKAuto: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'CLIENTDATASETWKMANUALINDEX1'
      end>
    Params = <>
    ProviderName = 'DataSetProviderWKAuto'
    StoreDefs = True
    Left = 360
    Top = 296
  end
  object DataSetProviderWKAuto: TDataSetProvider
    DataSet = QueryWKAuto
    Constraints = True
    Left = 488
    Top = 296
  end
  object QueryJOB: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  J.PLANT_CODE, J.WORKSPOT_CODE, J.JOB_CODE, J.DESCRIPTION, J.NO' +
        'RM_PROD_LEVEL'
      'FROM '
      '  JOBCODE J'
      'ORDER BY '
      '  J.PLANT_CODE, J.WORKSPOT_CODE, J.JOB_CODE'
      ' ')
    Left = 352
    Top = 8
  end
  object ClientDataSetJob: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'CLIENTDATASETWKMANUALINDEX1'
      end>
    Params = <>
    ProviderName = 'DataSetProviderJob'
    StoreDefs = True
    Left = 464
    Top = 8
  end
  object DataSetProviderJob: TDataSetProvider
    DataSet = QueryJOB
    Constraints = True
    Left = 568
    Top = 8
  end
  object TableWorkspotManual: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQManual
    TableName = 'WORKSPOT'
    Left = 88
    Top = 472
    object TableWorkspotManualPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotManualDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotManualWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotManualCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotManualDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotManualMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotManualMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotManualUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotManualHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotManualMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotManualPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotManualQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotManualAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotManualCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotManualENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotManualDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object dsWorkspotManual: TDataSource
    DataSet = TableWorkspotManual
    Left = 220
    Top = 472
  end
  object TableWorkspotAuto: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQAuto
    TableName = 'WORKSPOT'
    Left = 360
    Top = 472
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField4: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField5: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField6: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField7: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object StringField8: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object StringField9: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object StringField11: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object StringField12: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object dsWorkspotAuto: TDataSource
    DataSet = TableWorkspotAuto
    Left = 492
    Top = 472
  end
  object TableShiftManual: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQManual
    TableName = 'SHIFT'
    Left = 356
    Top = 132
    object TableShiftManualSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object TableShiftManualPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TableShiftManualDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
  end
  object DataSourceShiftManual: TDataSource
    DataSet = TableShiftManual
    Left = 464
    Top = 132
  end
  object TableShiftManualLU: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQManual
    TableName = 'SHIFT'
    Left = 580
    Top = 132
    object TableShiftManualLUSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object TableShiftManualLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TableShiftManualLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
  end
  object TableShiftAuto: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQAuto
    TableName = 'SHIFT'
    Left = 356
    Top = 180
    object TableShiftAutoSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object TableShiftAutoPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TableShiftAutoDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
  end
  object DataSourceShiftAuto: TDataSource
    DataSet = TableShiftAuto
    Left = 464
    Top = 180
  end
  object TableShiftAutoLU: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = dsQryPQAuto
    TableName = 'SHIFT'
    Left = 580
    Top = 180
    object TableShiftAutoLUSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Required = True
    end
    object TableShiftAutoLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TableShiftAutoLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
  end
end
