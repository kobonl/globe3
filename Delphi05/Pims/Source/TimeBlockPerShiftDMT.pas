(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:15-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning
*)
unit TimeBlockPerShiftDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTimeBlockPerShiftDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailTIMEBLOCK_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableMasterPLANTLU: TStringField;
    TableTempTBPerShift: TTable;
    TableDetailPLANTDESC: TStringField;
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TimeBlockPerShiftDM: TTimeBlockPerShiftDM;

implementation

uses SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TTimeBlockPerShiftDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;
 { if (ValidateBlocksPerShift(TableDetail, TableMaster) <> 0) then
    Abort;
  if (ValidateIntervalTime(TableDetail, TableMaster, 'TimeBlocksPerShift') <> 0) then
    Abort;}
  if CheckBlocks(TimeBlockPerShiftDM.Handle_Grid, TableMaster, False) <> 0  then
  begin
    DisplayMessage(SPIMSTimeIntervalError,mtError, [mbOk]);
    Abort;
  end;
end;

procedure TTimeBlockPerShiftDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  if not CheckNrOfTimeblocks(TableDetail.RecordCount) then // GLOB3-60
    Abort;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger := Index + 1;

  if TableTempTBPerShift.FindKey([TableMaster.FieldByName('PLANT_CODE').AsString,
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
  for Index := 1 to 7 do
  begin
    DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
      TableTempTBPerShift.FieldByName('ENDTIME' + IntToStr(Index)).Value;
    DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
      TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
  end
  else
    InitializeTimeValues(TableDetail, TableMaster);
end;

procedure TTimeBlockPerShiftDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailTIMEBLOCK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

// RV050.8.
procedure TTimeBlockPerShiftDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  SystemDM.PlantTeamFilterEnable(TableMaster);
end;

// RV050.8.
procedure TTimeBlockPerShiftDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
