(*
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
*)
unit DialogWorkspotPerEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Dblup1a;

type
  TDialogWorkspotPerEmployeeF = class(TDialogBaseF)
    RadioGroupEmpl: TRadioGroup;
    RadioGroupWKSort: TRadioGroup;
    GroupBoxSelection: TGroupBox;
    Label5: TLabel;
    cmbPlusPlant: TComboBoxPlus;
    CheckBoxAllTeam: TCheckBox;
    Label12: TLabel;
    Label13: TLabel;
    ComboBoxPlusTeamFrom: TComboBoxPlus;
    Label14: TLabel;
    ComboBoxPlusTeamTo: TComboBoxPlus;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure ComboBoxPlusTeamToCloseUp(Sender: TObject);
    procedure ComboBoxPlusTeamFromCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }

    FEdit_Workspot_Planning: String;
    procedure FillPlants;
  public
    { Public declarations }
    property Edit_Workspot_Planning: String read FEdit_Workspot_Planning
      write FEdit_Workspot_Planning;
  end;

var
  DialogWorkspotPerEmployeeF: TDialogWorkspotPerEmployeeF;

// RV089.1.
function DialogWorkspotPerEmployeeForm: TDialogWorkspotPerEmployeeF;

implementation

{$R *.DFM}
uses ListProcsFRM,  SystemDMT, StaffPlanningUnit,
  UPimsMessageRes, UPimsConst, DialogWorkspotPerEmployeeDMT,
  WorkspotPerEmployeeFRM, DialogCalendarFRM;

// RV089.1.
var
  DialogWorkspotPerEmployeeF_HND: TDialogWorkspotPerEmployeeF;

// RV089.1.
function DialogWorkspotPerEmployeeForm: TDialogWorkspotPerEmployeeF;
begin
  if (DialogWorkspotPerEmployeeF_HND = nil) then
  begin
    DialogWorkspotPerEmployeeF_HND := TDialogWorkspotPerEmployeeF.Create(Application);
    with DialogWorkspotPerEmployeeF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogWorkspotPerEmployeeF_HND;
end;

// RV089.1.
procedure TDialogWorkspotPerEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  FWKList.Free;
  FDeptList.Free;
  FWKDescList.Free;
  DialogCalendarF.Free;
  DialogWorkspotPerEmployeeDM.Free;
  if (DialogWorkspotPerEmployeeF_HND <> nil) then
  begin
    DialogWorkspotPerEmployeeF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogWorkspotPerEmployeeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogWorkspotPerEmployeeF.FormCreate(Sender: TObject);
begin
  inherited;
  //CAR 6-1-2004 calculate the number of displayed employees
  // - depends on the screen resolution
  // 50 is height of header top bands
  //17 is height of one row
  //ScrollBarGrid has height 20
  EMPLOYEESHOW := ((Screen.Height - 50  - 21) div 17);
  FWKList := TStringList.Create;
  FDeptList := TStringList.Create;
  FWKDescList := TStringList.Create;
  DialogWorkspotPerEmployeeDM := TDialogWorkspotPerEmployeeDM.Create(Application);
end;

procedure TDialogWorkspotPerEmployeeF.FormShow(Sender: TObject);

begin
  inherited;
  FCurrentDate := Now;

  ComboBoxPlusTeamFrom.ShowSpeedButton := False;
  ComboBoxPlusTeamFrom.ShowSpeedButton := True;
  ComboBoxPlusTeamTo.ShowSpeedButton := False;
  ComboBoxPlusTeamTo.ShowSpeedButton := True;

  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;

  ListProcsF.FillComboBoxMaster(DialogWorkspotPerEmployeeDM.ClientDataSetTeam,
    'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(DialogWorkspotPerEmployeeDM.ClientDataSetTeam,
    'TEAM_CODE',  False, ComboBoxPlusTeamTo);
  FillPlants;

  RadioGroupEmpl.ItemIndex := 0;
  RadioGroupWKSort.ItemIndex := 0;
  //
  DialogCalendarF := TDialogCalendarF.Create(Application);
end;

procedure TDialogWorkspotPerEmployeeF.FillPlants;
begin
  ListProcsF.FillComboBoxPlant(DialogWorkspotPerEmployeeDM.ClientDataSetPlant,
    True, CmbPlusPlant);
end;

procedure TDialogWorkspotPerEmployeeF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
  end;
  FillPlants;
end;

procedure TDialogWorkspotPerEmployeeF.btnOkClick(Sender: TObject);
begin
  inherited;
  SetParameters(GetStrValue(cmbPlusPlant.Value),
    GetStrValue(ComboBoxPlusTeamFrom.Value),
    GetStrValue(ComboBoxPlusTeamTo.Value), 0, 0,
    RadioGroupWKSort.ItemIndex, RadioGroupEmpl.ItemIndex, 0,
    CheckBoxAllTeam.Checked, FCurrentDate ,False, False);
  if DialogWorkspotPerEmployeeDM.FillWorkspotPerEmployee then
  begin
    try
      WorkspotPerEmployeeF := TWorkspotPerEmployeeF.Create(Application);
      WorkspotPerEmployeeF.Caption :=  SPimsTeamSelection ;
      if CheckBoxAllTeam.Checked then
        WorkspotPerEmployeeF.Caption :=
          WorkspotPerEmployeeF.Caption + SPimsTeamSelectionAll
      else
        WorkspotPerEmployeeF.Caption :=
          WorkspotPerEmployeeF.Caption +
          SPimsTeam + GetStrValue(ComboBoxPlusTeamFrom.Value) + SPimsStaffTo +
          GetStrValue(ComboBoxPlusTeamTo.Value);
       
      WorkspotPerEmployeeF.Edit_Workspot_Planning :=  Edit_Workspot_Planning;
      WorkspotPerEmployeeF.ShowModal;

    finally
    // free form
      if WorkspotPerEmployeeF <> NIL then
        WorkspotPerEmployeeF.Free;
    end;
  end;
end;

procedure TDialogWorkspotPerEmployeeF.ComboBoxPlusTeamToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusTeamFrom.DisplayValue <> '') and
     (ComboBoxPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusTeamFrom.Value) >
      GetStrValue(ComboBoxPlusTeamTo.Value) then
      ComboBoxPlusTeamFrom.DisplayValue := ComboBoxPlusTeamTo.DisplayValue;
end;

procedure TDialogWorkspotPerEmployeeF.ComboBoxPlusTeamFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusTeamFrom.DisplayValue <> '') and
     (ComboBoxPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusTeamFrom.Value) >
      GetStrValue(ComboBoxPlusTeamTo.Value) then
    ComboBoxPlusTeamTo.DisplayValue := ComboBoxPlusTeamFrom.DisplayValue;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogWorkspotPerEmployeeF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogWorkspotPerEmployeeF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
