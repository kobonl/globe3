inherited SettingsF: TSettingsF
  Left = 272
  Top = 144
  Width = 656
  Height = 535
  Caption = 'Configuration'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 640
    Height = 87
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 83
      Width = 638
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 638
      Height = 82
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 267
    Width = 640
    Height = 229
    object pgControl: TPageControl
      Left = 1
      Top = 1
      Width = 638
      Height = 227
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'General'
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 630
          Height = 118
          Align = alTop
          TabOrder = 0
          object GroupBoxAutomatic: TGroupBox
            Left = 1
            Top = 1
            Width = 322
            Height = 116
            Align = alLeft
            Caption = 'Cleanup'
            TabOrder = 0
            object LabelOlder: TLabel
              Left = 151
              Top = 16
              Width = 122
              Height = 13
              Caption = 'Delete records older than'
            end
            object LabelDetailed: TLabel
              Left = 8
              Top = 67
              Width = 76
              Height = 13
              Caption = 'Employee hours'
            end
            object LabelProcess: TLabel
              Left = 8
              Top = 40
              Width = 101
              Height = 13
              Caption = 'Production quantities'
            end
            object LabelWeeksD: TLabel
              Left = 216
              Top = 67
              Width = 30
              Height = 13
              Caption = 'weeks'
            end
            object LabelWeeksP: TLabel
              Left = 216
              Top = 40
              Width = 30
              Height = 13
              Caption = 'weeks'
            end
            object Label1: TLabel
              Left = 8
              Top = 16
              Width = 71
              Height = 13
              Caption = 'Information on'
            end
            object Label2: TLabel
              Left = 8
              Top = 94
              Width = 97
              Height = 13
              Caption = 'Timerecording scans'
            end
            object Label3: TLabel
              Left = 216
              Top = 94
              Width = 30
              Height = 13
              Caption = 'weeks'
            end
            object dxSpinEditEmployeeHours: TdxSpinEdit
              Left = 152
              Top = 63
              Width = 57
              TabOrder = 3
              OnChange = dxSpinEditEmployeeHoursChange
              Value = 52
            end
            object dxSpinEditProductionQuantities: TdxSpinEdit
              Left = 152
              Top = 36
              Width = 57
              TabOrder = 0
              OnChange = dxSpinEditProductionQuantitiesChange
              Value = 52
            end
            object dxSpinEditTimerecordingScans: TdxSpinEdit
              Left = 152
              Top = 90
              Width = 57
              TabOrder = 4
              OnChange = dxSpinEditTimerecordingScansChange
              Value = 52
            end
            object btnCleanProductionQuantities: TButton
              Left = 256
              Top = 34
              Width = 59
              Height = 25
              Caption = 'Cleanup'
              TabOrder = 1
              OnClick = btnCleanProductionQuantitiesClick
            end
            object btnCleanEmployeeHours: TButton
              Left = 255
              Top = 60
              Width = 59
              Height = 25
              Caption = 'Cleanup'
              TabOrder = 2
              OnClick = btnCleanEmployeeHoursClick
            end
            object btnCleanTimerecordingScans: TButton
              Left = 255
              Top = 86
              Width = 59
              Height = 25
              Caption = 'Cleanup'
              TabOrder = 5
              OnClick = btnCleanTimerecordingScansClick
            end
          end
          object GroupBoxWeek: TGroupBox
            Left = 323
            Top = 1
            Width = 158
            Height = 116
            Align = alLeft
            Caption = 'Week starts on'
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 1
            object RadioButtonMonday: TRadioButton
              Left = 12
              Top = 16
              Width = 61
              Height = 17
              Caption = 'Monday'
              Checked = True
              TabOrder = 0
              TabStop = True
            end
            object RadioButtonSunday: TRadioButton
              Left = 12
              Top = 64
              Width = 61
              Height = 17
              Caption = 'Sunday'
              TabOrder = 1
            end
            object RadioButtonSaturday: TRadioButton
              Left = 12
              Top = 88
              Width = 61
              Height = 17
              Caption = 'Saturday'
              TabOrder = 2
            end
            object RadioButtonWednesday: TRadioButton
              Left = 12
              Top = 40
              Width = 113
              Height = 17
              Caption = 'Wednesday'
              TabOrder = 3
            end
          end
          object RadioGroupFirstWeekOfYear: TRadioGroup
            Left = 481
            Top = 1
            Width = 148
            Height = 116
            Align = alClient
            Caption = 'First week of year'
            Items.Strings = (
              'January 1 st'
              'January 4 th'
              'January 7 th')
            TabOrder = 2
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 118
          Width = 630
          Height = 81
          Align = alClient
          TabOrder = 1
          object GroupBoxUnits: TGroupBox
            Left = 1
            Top = 1
            Width = 122
            Height = 79
            Align = alLeft
            Caption = 'Units'
            TabOrder = 0
            object LabelWeight: TLabel
              Left = 8
              Top = 24
              Width = 34
              Height = 13
              Caption = 'Weight'
            end
            object LabelPieces: TLabel
              Left = 8
              Top = 48
              Width = 30
              Height = 13
              Caption = 'Pieces'
            end
            object EditWeight: TEdit
              Tag = 1
              Left = 56
              Top = 20
              Width = 57
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 0
            end
            object EditPieces: TEdit
              Tag = 1
              Left = 56
              Top = 44
              Width = 57
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              TabOrder = 1
            end
          end
          object GroupBox2: TGroupBox
            Left = 123
            Top = 1
            Width = 119
            Height = 79
            Align = alLeft
            Caption = 'Update'
            TabOrder = 1
            object CheckEditUpdateEmpl: TCheckBox
              Left = 8
              Top = 36
              Width = 89
              Height = 17
              Caption = 'Employee'
              TabOrder = 1
            end
            object CheckBoxPlant: TCheckBox
              Left = 8
              Top = 16
              Width = 89
              Height = 17
              Caption = 'Plant'
              TabOrder = 0
            end
            object CheckEditDeleteWK: TCheckBox
              Left = 8
              Top = 56
              Width = 89
              Height = 17
              Caption = 'Workspot'
              TabOrder = 2
            end
          end
          object GroupBox3: TGroupBox
            Left = 361
            Top = 1
            Width = 144
            Height = 79
            Align = alLeft
            TabOrder = 2
            object LabelDataBase: TLabel
              Left = 7
              Top = 48
              Width = 84
              Height = 13
              Caption = 'Database version'
            end
            object CheckEditReadPlanningOnce: TCheckBox
              Left = 7
              Top = 16
              Width = 121
              Height = 17
              Caption = 'Read planning once '
              TabOrder = 0
            end
          end
          object BitBtnSetpassword: TBitBtn
            Left = 510
            Top = 7
            Width = 121
            Height = 25
            Caption = 'Set new password'
            TabOrder = 3
            OnClick = BitBtnSetpasswordClick
          end
          object GroupBox1: TGroupBox
            Left = 242
            Top = 1
            Width = 119
            Height = 79
            Align = alLeft
            Caption = 'Delete'
            TabOrder = 4
            object CBoxPlantDelete: TCheckBox
              Left = 8
              Top = 16
              Width = 89
              Height = 17
              Caption = 'Plant'
              TabOrder = 0
            end
            object CBoxEmpDelete: TCheckBox
              Left = 8
              Top = 36
              Width = 89
              Height = 17
              Caption = 'Employee'
              TabOrder = 1
            end
            object CBoxWKDelete: TCheckBox
              Left = 8
              Top = 56
              Width = 89
              Height = 17
              Caption = 'Workspot'
              TabOrder = 2
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Additional'
        ImageIndex = 1
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 630
          Height = 97
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel5: TPanel
            Left = 0
            Top = 0
            Width = 305
            Height = 97
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object rgrpEffBOQuant: TRadioGroup
              Left = 0
              Top = 48
              Width = 97
              Height = 49
              Caption = 'Efficiency calculation based on'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Quantity'
                'Time')
              TabOrder = 1
              Visible = False
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 0
              Width = 305
              Height = 41
              Align = alTop
              Caption = 'Dialogs'
              TabOrder = 0
              object cboxEmbedDialogs: TCheckBox
                Left = 8
                Top = 16
                Width = 169
                Height = 17
                Caption = 'Embed Dialogs'
                TabOrder = 0
              end
            end
            object GroupBox11: TGroupBox
              Left = 0
              Top = 41
              Width = 305
              Height = 56
              Align = alTop
              Caption = 'Employee Dialog'
              TabOrder = 2
              object cBoxAllowChgEmpNr: TCheckBox
                Left = 8
                Top = 24
                Width = 281
                Height = 17
                Caption = 'Allow change of employee number'
                TabOrder = 0
              end
            end
          end
          object Panel6: TPanel
            Left = 305
            Top = 0
            Width = 325
            Height = 97
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox5: TGroupBox
              Left = 280
              Top = 56
              Width = 325
              Height = 47
              Caption = 'Final Run System'
              TabOrder = 0
              object cBoxFinalRun: TCheckBox
                Left = 8
                Top = 19
                Width = 313
                Height = 17
                Caption = 'Use Final Run System'
                TabOrder = 0
                Visible = False
              end
            end
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 325
              Height = 56
              Align = alTop
              Caption = 'Personal Screen / Timerecording /  Manual Reg.'
              TabOrder = 1
              object cBoxBreakBtnVisible: TCheckBox
                Left = 8
                Top = 16
                Width = 289
                Height = 17
                Caption = 'Show Break Button'
                TabOrder = 0
              end
              object cBoxLunchBtnVisible: TCheckBox
                Left = 8
                Top = 34
                Width = 145
                Height = 17
                Caption = 'Show Lunch Button'
                TabOrder = 1
                OnClick = cBoxLunchBtnVisibleClick
              end
              object GroupBoxLunchButton: TGroupBox
                Left = 168
                Top = 15
                Width = 155
                Height = 39
                Align = alRight
                Caption = 'Lunch Button'
                TabOrder = 2
                object cBoxRegisterTime: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 137
                  Height = 17
                  Caption = 'Register Time'
                  TabOrder = 0
                end
              end
            end
            object GroupBox7: TGroupBox
              Left = 0
              Top = 56
              Width = 325
              Height = 39
              Align = alTop
              Caption = 'Timerecording'
              TabOrder = 2
              object cBoxTREndOfDayBtnVisible: TCheckBox
                Left = 8
                Top = 16
                Width = 297
                Height = 17
                Caption = 'Show End Of Day Button'
                TabOrder = 0
              end
            end
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 97
          Width = 305
          Height = 102
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBoxProdReports: TGroupBox
            Left = 0
            Top = 0
            Width = 305
            Height = 102
            Align = alClient
            Caption = 'Productivity Reports'
            TabOrder = 0
            object cBoxWorkspotInclSel: TCheckBox
              Left = 8
              Top = 16
              Width = 289
              Height = 17
              Caption = 'Workspot Include/Exclude selection'
              TabOrder = 0
            end
            object cBoxDateTimeSel: TCheckBox
              Left = 8
              Top = 35
              Width = 289
              Height = 17
              Caption = 'Enable date/time selection'
              TabOrder = 1
              Visible = False
            end
            object cBoxInclOpenScans: TCheckBox
              Left = 8
              Top = 53
              Width = 289
              Height = 17
              Caption = 'Include open scans'
              TabOrder = 2
              Visible = False
            end
          end
          object rgrpTimeRecOccupied: TRadioGroup
            Left = 0
            Top = 0
            Width = 305
            Height = 102
            Align = alClient
            Caption = 'Timerecording and Occupied Workspot'
            Items.Strings = (
              'Show message '#39'Go to supervisor'#39
              'Give option to switch places'
              'Give option to switch places or scan-in to occupied-job')
            TabOrder = 1
          end
        end
        object Panel8: TPanel
          Left = 305
          Top = 97
          Width = 325
          Height = 102
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object GroupBox8: TGroupBox
            Left = 0
            Top = 0
            Width = 325
            Height = 39
            Align = alTop
            Caption = 'Personal Screen'
            TabOrder = 0
            object cBoxPSEndOfDayBtnVisible: TCheckBox
              Left = 8
              Top = 16
              Width = 297
              Height = 17
              Caption = 'Show End Of Day Button'
              TabOrder = 0
            end
          end
          object GroupBox9: TGroupBox
            Left = 0
            Top = 39
            Width = 325
            Height = 39
            Align = alTop
            Caption = 'Manual Registrations'
            TabOrder = 1
            object cBoxMANREGEndOfDayBtnVisible: TCheckBox
              Left = 8
              Top = 16
              Width = 297
              Height = 17
              Caption = 'Show End Of Day Button'
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Logging'
        ImageIndex = 2
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 630
          Height = 199
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 305
            Height = 199
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object gbABSLog: TGroupBox
              Left = 0
              Top = 0
              Width = 305
              Height = 97
              Align = alTop
              Caption = 'Log changes'
              TabOrder = 0
              object cbABSLogEmpHrs: TCheckBox
                Left = 8
                Top = 19
                Width = 169
                Height = 17
                Caption = 'Employee hours'
                TabOrder = 0
              end
              object cbABSLogTRS: TCheckBox
                Left = 8
                Top = 39
                Width = 161
                Height = 17
                Caption = 'Timerecording scans'
                TabOrder = 1
              end
            end
            object gbErrorLog: TGroupBox
              Left = 0
              Top = 97
              Width = 305
              Height = 102
              Align = alClient
              Caption = 'Error Logging'
              TabOrder = 1
              object cBoxLogErrorsToDB: TCheckBox
                Left = 8
                Top = 24
                Width = 281
                Height = 17
                Caption = 'Log Errors to database'
                TabOrder = 0
              end
            end
          end
          object Panel10: TPanel
            Left = 305
            Top = 0
            Width = 325
            Height = 199
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object gbABSLogCleanup: TGroupBox
              Left = 0
              Top = 0
              Width = 325
              Height = 199
              Align = alClient
              Caption = 'Log changes cleanup'
              TabOrder = 0
              object Label4: TLabel
                Left = 8
                Top = 16
                Width = 71
                Height = 13
                Caption = 'Information on'
              end
              object Label5: TLabel
                Left = 151
                Top = 16
                Width = 122
                Height = 13
                Caption = 'Delete records older than'
              end
              object Label6: TLabel
                Left = 8
                Top = 40
                Width = 113
                Height = 13
                Caption = 'Employee hours logging'
              end
              object Label7: TLabel
                Left = 8
                Top = 67
                Width = 134
                Height = 13
                Caption = 'Timerecording scans logging'
              end
              object Label8: TLabel
                Left = 216
                Top = 40
                Width = 30
                Height = 13
                Caption = 'weeks'
              end
              object Label9: TLabel
                Left = 216
                Top = 67
                Width = 30
                Height = 13
                Caption = 'weeks'
              end
              object dxSpinEditTRSLog: TdxSpinEdit
                Left = 152
                Top = 63
                Width = 57
                TabOrder = 0
                OnChange = dxSpinEditEmployeeHoursChange
                Value = 52
              end
              object dxSpinEditEmpHrsLog: TdxSpinEdit
                Left = 152
                Top = 36
                Width = 57
                TabOrder = 1
                OnChange = dxSpinEditProductionQuantitiesChange
                Value = 52
              end
              object btnCleanTRSLog: TButton
                Left = 255
                Top = 60
                Width = 59
                Height = 25
                Caption = 'Cleanup'
                TabOrder = 2
                OnClick = btnCleanTRSLogClick
              end
              object btnCleanEmpHrsLog: TButton
                Left = 256
                Top = 34
                Width = 59
                Height = 25
                Caption = 'Cleanup'
                TabOrder = 3
                OnClick = btnCleanEmpHrsLogClick
              end
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Personal Screen'
        ImageIndex = 3
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 313
          Height = 199
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBoxPersonalScreen: TGroupBox
            Left = 0
            Top = 0
            Width = 313
            Height = 199
            Align = alClient
            Caption = 'Personal Screen'
            TabOrder = 0
            object Label10: TLabel
              Left = 8
              Top = 19
              Width = 125
              Height = 13
              Caption = 'Autom. close dialogs after'
            end
            object Label11: TLabel
              Left = 216
              Top = 19
              Width = 39
              Height = 13
              Caption = 'seconds'
            end
            object dxSpinEditPSAutoCloseSecs: TdxSpinEdit
              Left = 152
              Top = 15
              Width = 57
              TabOrder = 0
              OnChange = dxSpinEditPSAutoCloseSecsChange
              MaxValue = 999
              Value = 20
              StoredValues = 16
            end
            object cBoxEnableMachTimeRec: TCheckBox
              Left = 8
              Top = 42
              Width = 297
              Height = 15
              Caption = 'Enable machine timerecording'
              TabOrder = 1
            end
            object cBoxPSShowEmpInfo: TCheckBox
              Left = 8
              Top = 1000
              Width = 297
              Height = 17
              Caption = 'Show Employee Info'
              TabOrder = 2
              Visible = False
            end
            object cBoxPSShowEmpEff: TCheckBox
              Left = 8
              Top = 1000
              Width = 297
              Height = 17
              Caption = 'Show Employee Efficiency'
              TabOrder = 3
              Visible = False
            end
            object cBoxTimeRecInSecs: TCheckBox
              Left = 8
              Top = 300
              Width = 297
              Height = 17
              Caption = 'Timerecording in seconds'
              TabOrder = 4
              Visible = False
            end
            object cBoxIgnoreBreaks: TCheckBox
              Left = 8
              Top = 313
              Width = 297
              Height = 17
              Caption = 'No Break Calculation'
              TabOrder = 5
              Visible = False
            end
            object cBoxAddJobComments: TCheckBox
              Left = 8
              Top = 1000
              Width = 300
              Height = 17
              Caption = 'Enter comments for Down-jobs'
              TabOrder = 6
              Visible = False
            end
            object cBoxUsePackageHourCalc: TCheckBox
              Left = 8
              Top = 60
              Width = 300
              Height = 17
              Caption = 'Calculate hours using a package'
              TabOrder = 7
            end
          end
        end
        object Panel12: TPanel
          Left = 313
          Top = 0
          Width = 317
          Height = 199
          Align = alClient
          TabOrder = 1
          object GroupBox10: TGroupBox
            Left = 1
            Top = 1
            Width = 315
            Height = 197
            Align = alClient
            Caption = 'Personal Screen'
            TabOrder = 0
            object rgrpReadExtDataFrom: TRadioGroup
              Left = 2
              Top = 15
              Width = 311
              Height = 58
              Align = alTop
              Caption = 'Read External Data From'
              Items.Strings = (
                'Production Quantity Table'
                'Real Time Efficiency Table')
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 113
    Width = 640
    Height = 154
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 150
      Width = 638
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 638
      Height = 149
      KeyField = 'COMPUTER_NAME'
      DataSource = SettingsDM.DataSourceDetail
      object dxDetailGridColumnComputer: TdxDBGridColumn
        Caption = 'Computer name'
        Width = 260
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPUTER_NAME'
      end
      object dxDetailGridColumnLicense: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'License version'
        Width = 144
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LICENSEVERSION'
      end
      object dxDetailGridColumnTimeRecording: TdxDBGridCheckColumn
        Caption = 'Time recording station'
        Width = 122
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMERECORDING_YN'
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object dxDetailGridColumnShowHrsYN: TdxDBGridCheckColumn
        Caption = 'Show hours '
        Width = 88
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHOW_HOUR_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnHideIDYN: TdxDBGridCheckColumn
        Caption = 'Hide ID'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HIDE_ID_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnCardReaderDelay: TdxDBGridSpinColumn
        Caption = 'Cardreader Delay'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CARDREADER_DELAY'
      end
      object dxDetailGridColumnManRegsMultJobs: TdxDBGridCheckColumn
        Caption = 'Man. Regs. Multiple jobs'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MANREGS_MULTJOBS_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnPlantCode: TdxDBGridLookupColumn
        Caption = 'Plant'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
        CanDeleteText = True
      end
      object dxDetailGridColumnShowOnlyDefJobYN: TdxDBGridCheckColumn
        Caption = 'Show only def. job'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHOW_ONLY_DEFJOB_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 320
    Top = 64
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 416
    Top = 64
  end
  inherited StandardMenuActionList: TActionList
    Left = 528
    Top = 64
  end
  inherited dsrcActive: TDataSource
    Top = 64
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 536
    Top = 193
  end
end
