inherited DialogReportProductionF: TDialogReportProductionF
  Left = 337
  Top = 111
  Caption = 'Report Production'
  ClientHeight = 512
  ClientWidth = 680
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 680
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 384
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 680
    Height = 391
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 624
      Visible = False
    end
    inherited LblEmployee: TLabel
      Top = 624
      Visible = False
    end
    inherited LblToPlant: TLabel
      Top = 16
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Top = 627
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 628
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 628
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 128
      Top = 46
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [15]
      Left = 8
      Top = 469
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [16]
      Left = 8
      Top = 506
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [17]
      Left = 315
      Top = 70
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [18]
      Left = 152
      Top = 593
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [19]
      Left = 360
      Top = 593
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [20]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [21]
      Left = 128
      Top = 507
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [22]
      Left = 336
      Top = 504
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [23]
      Left = 128
      Top = 470
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [24]
      Left = 336
      Top = 470
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [25]
      Left = 40
      Top = 506
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [26]
      Left = 40
      Top = 469
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [27]
      Left = 315
      Top = 506
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [28]
      Left = 8
      Top = 182
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [29]
      Left = 40
      Top = 182
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [30]
      Left = 315
      Top = 182
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel [31]
      Left = 315
      Top = 470
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [32]
      Left = 336
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 69
    end
    inherited LblDepartment: TLabel
      Top = 69
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 71
    end
    inherited LblStarTeamTo: TLabel
      Left = 337
      Top = 126
    end
    inherited LblToTeam: TLabel
      Top = 128
    end
    inherited LblStarTeamFrom: TLabel
      Top = 126
    end
    inherited LblTeam: TLabel
      Top = 126
    end
    inherited LblFromTeam: TLabel
      Top = 126
    end
    inherited LblFromShift: TLabel
      Top = 548
    end
    inherited LblShift: TLabel
      Top = 548
    end
    inherited LblStartShiftFrom: TLabel
      Top = 550
    end
    inherited LblToShift: TLabel
      Top = 550
    end
    inherited LblStarShiftTo: TLabel
      Top = 550
    end
    inherited LblFromPlant2: TLabel
      Top = 591
    end
    inherited LblPlant2: TLabel
      Top = 591
    end
    inherited LblStarPlant2From: TLabel
      Top = 593
    end
    inherited LblToPlant2: TLabel
      Top = 591
    end
    inherited LblStarPlant2To: TLabel
      Top = 593
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    inherited LblWorkspots: TLabel
      Top = 98
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 520
      Top = 95
    end
    inherited LblToJob: TLabel
      Top = 665
    end
    inherited LblJob: TLabel
      Top = 665
    end
    inherited LblFromJob: TLabel
      Top = 665
    end
    inherited LblFromDateTime: TLabel
      Top = 182
    end
    inherited LblDateTime: TLabel
      Top = 182
    end
    inherited LblToDateTime: TLabel
      Top = 182
    end
    object DateTo: TDateTimePicker [70]
      Left = 334
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 14
    end
    object DateFrom: TDateTimePicker [71]
      Left = 120
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 147
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 148
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 125
      ColCount = 159
      TabOrder = 10
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 125
      ColCount = 160
      TabOrder = 11
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 128
      TabOrder = 12
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 69
      ColCount = 160
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 69
      ColCount = 161
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 71
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 626
      TabOrder = 22
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 626
      TabOrder = 33
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Top = 547
      ColCount = 166
      TabOrder = 23
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Top = 547
      ColCount = 167
      TabOrder = 34
    end
    inherited CheckBoxAllShifts: TCheckBox
      Top = 547
      TabOrder = 30
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 29
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Top = 626
      TabOrder = 31
    end
    object GroupBoxShow: TGroupBox [87]
      Left = 8
      Top = 224
      Width = 137
      Height = 113
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 28
      Visible = False
      object CheckBoxBU: TCheckBox
        Left = 8
        Top = 12
        Width = 89
        Height = 17
        Caption = 'Business unit'
        TabOrder = 0
        OnClick = CheckBoxBUClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 94
        Width = 73
        Height = 17
        Caption = 'Employee'
        TabOrder = 1
      end
      object CheckBoxWK: TCheckBox
        Left = 8
        Top = 53
        Width = 73
        Height = 17
        Caption = 'Workspot'
        TabOrder = 2
        Visible = False
        OnClick = CheckBoxWKClick
      end
      object CheckBoxJobcode: TCheckBox
        Left = 8
        Top = 67
        Width = 73
        Height = 17
        Caption = 'Jobcode'
        TabOrder = 3
      end
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 39
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 4
        OnClick = CheckBoxDeptClick
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus [88]
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 148
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus [89]
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 149
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus [90]
      Left = 136
      Top = 505
      Width = 180
      Height = 19
      ColCount = 153
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 20
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus [91]
      Left = 350
      Top = 505
      Width = 180
      Height = 19
      ColCount = 154
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 21
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object GroupBoxSelection: TGroupBox [92]
      Left = 8
      Top = 224
      Width = 529
      Height = 145
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 19
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPageDept: TCheckBox
        Left = 192
        Top = 24
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 9
        Visible = False
      end
      object CheckBoxPageWK: TCheckBox
        Left = 192
        Top = 200
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 10
        Visible = False
      end
      object CheckBoxPageBU: TCheckBox
        Left = 8
        Top = 200
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 8
        Visible = False
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 122
        Width = 113
        Height = 17
        Caption = 'Export'
        TabOrder = 6
      end
      object CheckBoxShowCompareJobs: TCheckBox
        Left = 192
        Top = 49
        Width = 169
        Height = 17
        Caption = 'Include compare jobs'
        TabOrder = 5
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 192
        Top = 24
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 4
      end
      object CheckBoxShowJobs: TCheckBox
        Left = 8
        Top = 73
        Width = 169
        Height = 17
        Caption = 'Show jobs'
        TabOrder = 2
        OnClick = CheckBoxShowJobsClick
      end
      object CheckBoxShowOnlyJobs: TCheckBox
        Left = 8
        Top = 98
        Width = 169
        Height = 17
        Caption = 'Show only jobs'
        TabOrder = 3
      end
      object RadioGroupSortOnJob: TRadioGroup
        Left = 192
        Top = 72
        Width = 163
        Height = 67
        Caption = 'Sort on job'
        ItemIndex = 0
        Items.Strings = (
          'Code'
          'Name')
        TabOrder = 7
      end
      object CheckBoxShowPayrollInformation: TCheckBox
        Left = 8
        Top = 49
        Width = 185
        Height = 17
        Caption = 'Show payroll information'
        TabOrder = 1
      end
      object RadioGroupIncludeDowntime: TRadioGroup
        Left = 360
        Top = 40
        Width = 161
        Height = 99
        Caption = 'Include down-time quantities'
        ItemIndex = 0
        Items.Strings = (
          'No'
          'Yes'
          'Only show down-time')
        TabOrder = 11
      end
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 514
      Top = 547
      TabOrder = 32
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Top = 589
      ColCount = 169
      TabOrder = 24
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 334
      Top = 589
      ColCount = 170
      TabOrder = 25
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 96
      ColCount = 174
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 96
      ColCount = 175
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      Top = 96
      Width = 393
      TabOrder = 9
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Top = 663
      ColCount = 171
      TabOrder = 27
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 663
      ColCount = 170
      TabOrder = 26
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 180
      TabOrder = 16
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Left = 450
      Top = 180
      TabOrder = 18
      StoredValues = 4
    end
    object ProgressBar: TProgressBar [103]
      Left = 8
      Top = 376
      Width = 521
      Height = 9
      Min = 0
      Max = 100
      TabOrder = 35
      Visible = False
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 180
      TabOrder = 15
    end
    inherited DatePickerTo: TDateTimePicker
      Left = 334
      Top = 180
      TabOrder = 17
    end
  end
  inherited stbarBase: TStatusBar
    Top = 452
    Width = 680
  end
  inherited pnlBottom: TPanel
    Top = 471
    Width = 680
    inherited btnOk: TBitBtn
      Left = 188
    end
    inherited btnCancel: TBitBtn
      Left = 302
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 16
  end
  inherited QueryEmplFrom: TQuery
    Left = 200
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 232
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 560
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        43030000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C20104F7074
        696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564
        676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564
        676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F
        7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43
        616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E61
        7669676174696F6E116564676F436F6E6669726D44656C657465126564676F4C
        6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B730000
        0F546478444247726964436F6C756D6E0C436F6C756D6E4E756D626572074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02410942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274204E616D6505576964746802480942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D6507436170
        74696F6E06044E616D6505576964746803F4000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E060741646472657373055769647468022C0942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        52455353000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        FA020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E7402010D53756D6D61727947726F7570730E001053756D6D6172
        79536570617261746F7206022C20104F7074696F6E73437573746F6D697A650B
        0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E671065
        64676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E67
        0E6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F
        43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43
        616E496E73657274116564676F43616E4E617669676174696F6E116564676F43
        6F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72647310
        6564676F557365426F6F6B6D61726B7300000F546478444247726964436F6C75
        6D6E0A436F6C756D6E456D706C0743617074696F6E06064E756D62657206536F
        7274656407046373557005576964746802310942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E55
        4D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E53686F
        72744E616D650743617074696F6E060A53686F7274206E616D65055769647468
        024E0942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060A53484F52545F4E414D4500000F546478444247726964436F6C756D
        6E11436F6C756D6E4465736372697074696F6E0743617074696F6E06044E616D
        6505576964746803CC000942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060B4445534352495054494F4E00000F5464784442
        47726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E
        06074164647265737305576964746802650942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060741444452455353000000}
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    Left = 504
    Top = 23
  end
end
