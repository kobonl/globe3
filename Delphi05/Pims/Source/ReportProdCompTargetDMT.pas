unit ReportProdCompTargetDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, DBClient;

type
  TReportProdCompTargetDM = class(TReportBaseDM)
    QueryProductionComp: TQuery;
    DataSourceProduction: TDataSource;
    QueryPQ: TQuery;
    QueryProdHourPerEmployee: TQuery;
    ClientDataSetMachineHrs: TClientDataSet;
    ClientDataSetMachineHrsPLANT_CODE: TStringField;
    ClientDataSetMachineHrsWORKSPOT_CODE: TStringField;
    ClientDataSetMachineHrsJOB_CODE: TStringField;
    ClientDataSetMachineHrsBUSINESSUNIT_CODE: TStringField;
    ClientDataSetMachineHrsMACHINE_HOURS: TFloatField;
    qryMachineHours: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryProductionCompFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InitQueryTotalPiecesPQ(QueryPQ : TQuery;
      DateFrom, DateTo: TDateTime;
      PlantFrom, PlantTo: String);
  end;

var
  ReportProdCompTargetDM: TReportProdCompTargetDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportProdCompTargetDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV067.2.
  SystemDM.PlantTeamFilterEnable(QueryProductionComp);
  
  QueryPQ.Close;
  QueryPQ.Prepare;
  QueryProdHourPerEmployee.Close;
  QueryProdHourPerEmployee.Prepare;
  ClientDataSetMachineHrs.CreateDataSet;
  // MR:01-06-2006 Performance-improvement.
  ClientDataSetMachineHrs.LogChanges := False;
end;

procedure TReportProdCompTargetDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  QueryPQ.Close;
  QueryPQ.UnPrepare;
  QueryProdHourPerEmployee.Close;
  QueryProdHourPerEmployee.UnPrepare;
  ClientDataSetMachineHrs.EmptyDataSet;
end;

procedure TReportProdCompTargetDM.InitQueryTotalPiecesPQ(
  QueryPQ : TQuery;
  DateFrom, DateTo: TDateTime;
  PlantFrom, PlantTo: String);
begin
  QueryPQ.Active := False;
  QueryPQ.ParamByName('FSTARTDATE').AsDateTime := DateFrom;
  QueryPQ.ParamByName('FENDDATE').AsDateTime := DateTo;
  QueryPQ.ParamByName('PLANTFROM').AsString := PlantFrom;
  QueryPQ.ParamByName('PLANTTO').AsString := PlantTo;
  QueryPQ.Active := True;
end;

procedure TReportProdCompTargetDM.QueryProductionCompFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV067.2.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
