(*
 * Title:        FSideMenuUnitMaintenance
 * Description:  Form/Class which shows a Side-Menu with menu-options
 *               at the left.
 * Copyright:    Copyright (c) 2001
 * Company:      ABS
 * Author        Carmen Panturu
 * Version       1.0

  MRA:13-JAN-2010. RV050.9. 889966.
  - Allow user to enter a user-name as parameter. Show username in statusbar.
  MRA:17-MAY-2010. RV064.1. Order 550478.
  - Addition of Machine-dialog.
  SO:08-JUL-2010 RV067.4. 550497
  - PIMS Addition of country dialog with export type setting.
  - absence types per country dialog
  - absence reason per country dialog
  - hour types per country dialog
  MRA:23-SEP-2010 RV067.MRA.36 Bugfix.
  - The method for enabling menu-options can
    go wrong when certain options are disabled already
    before the menu-settings are checked that were made
    by the AdmTool.
  MRA:24-FEB-2011 RV087.3. Dialog Login Change
  - Dialog Login
    - The Dialog Login is kept open all the time,
      because of 2 variables:
      - UserTeamSelectionYN
      - LoginUser
    - This must be changed by storing them in SystemDM.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed dialogs in Pims, instead of
    open them as a modal dialog.
  - This can be triggered with system-setting 'EmbedDialog' (true/false).
  MRA:24-MAY-2011 RV093.1. SO-20011749. Tailor-made TLB.
  - Added menu-option for: Report Hrs Per Contract Group Cumulative
    - New report tailor-made TLB.
    - This should only be visible for TLB
      (PIMSSETTING.CODE = 'TLB').
  MRA:14-JUN-2011 RV093.6. SO-20011796.
  - New report: Report Holiday Card.
  MRA:12-MAR-2013 SO-20014037
  - New report: Report Changes per Scan
  MRA:15-MAY-2013 SO-20014137
  - New report employee illness.
  MRA:21-OCT-2013 20014715
  - New Report Incentive Program (WMU)
  - Set Report Incentive Program only visible for WMU
  MRA:24-FEB-2014 20011800
  - Related to this order
  - Added Form_Edit_YN for BankHoliday-dialog
  MRA:17-NOV-2014 20014826
  - Added option for Error-Log (dialog)
  MRA:24-JUN-2015 20014450.50
  - Real Time Eff.
  MRA:24-JUL-2015 PIM-50
  - New Report Job Comments
  MRA:2-NOV-2015 PIM-52
  - Work Schedule
  MRA:17-NOV-2015 PIM-23
  - Report Employee Info and Deviations (per period)
  MRA:9-FEB-2016 PIM-12
  - Because of translation-tool some 'dummy'-items are added for dxSideBar with
    Tag = 999 to prevent error-message when translation is run.
  - IMPORTANT: Now we use the translations from the original source without
    having to translate it in the FRM-file and copy it later.
  - Added procedure that can export the whole menu-structure.
  MRA:29-FEB-2016 PIM-142
  - Addition of option Real Time Efficiency Monitor
  MRA:22-MAR-2016 PIM-152
  - Addition of Report Ghost Counts
  MRA:9-APR-2018 GLOB3-83 (NTS)
  - Report In/Outscan, Absence, Overtime (new report)
  MRA:25-JAN-2019 GLOB3-204
  - Personal Time Off USA
*)
unit SideMenuUnitMaintenanceFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, Menus, StdCtrls, ExtCtrls, dxsbar, ComCtrls, {UGlobals,} DBTables,
  OneSolar, ActnList, dxBar, dxBarDBNav, dxDBGrid, ComDrvN, UMenuOptions,
  DialogBaseFRM, SideMenuBaseFRM, jpeg;

type
  TSideMenuUnitMaintenanceF = class(TSideMenuBaseF)
    GoTeamDept: TAction;
    GoActionList: TActionList;
    dxBarButtonDeptTeam: TdxBarButton;
    dxBarButtonBusinessUnit: TdxBarButton;
    dxBarButtonDepartment: TdxBarButton;
    dxBarButtonWorkSpot: TdxBarButton;
    dxBarButtonTeam: TdxBarButton;
    dxBarSubItemPlantStrcture: TdxBarSubItem;
    GoPSPlant: TAction;
    GoBusinessUnit: TAction;
    GoDepartment: TAction;
    GoWorkspot: TAction;
    GoTeam: TAction;
    GoShift: TAction;
    GoTimeBlockShift: TAction;
    dxSideBar: TdxSideBar;
    GoBreakPerShift: TAction;
    GoTimeBlockPerDept: TAction;
    GoBreakPerDept: TAction;
    GoTimeBlockPerEmpl: TAction;
    GoBreakPerEmpl: TAction;
    GoIDCard: TAction;
    dxBarButtonShifts: TdxBarButton;
    dxBarSubItemTimeStructure: TdxBarSubItem;
    dxBarButtonTBPerShift: TdxBarButton;
    dxBarButtonBreaksPerShift: TdxBarButton;
    dxBarSubItemContractGroup: TdxBarSubItem;
    dxBarButtonContractGroups: TdxBarButton;
    dxBarButtonExceptionalHours: TdxBarButton;
    GoContractGroups: TAction;
    GoEmployee: TAction;
    GoJobCodes: TAction;
    GoBUPerWorkSpot: TAction;
    GoExceptionalhours: TAction;
    GoOvertime: TAction;
    dxBarButtonOverTime: TdxBarButton;
    dxBarButtonWKPerWorkStation: TdxBarButton;
    GoWorkSpotWorkstation: TAction;
    GoWKPerEmpl: TAction;
    GoEmplContract: TAction;
    dxBarButtonTBPerDept: TdxBarButton;
    dxBarButtonBreakPerDepartment: TdxBarButton;
    dxBarSubItemFixedStaffData: TdxBarSubItem;
    dxBarButtonEmployee: TdxBarButton;
    dxBarButtonEmplContracts: TdxBarButton;
    dxBarButtonIDCards: TdxBarButton;
    dxBarButtonTBPerEmployee: TdxBarButton;
    dxBarButtonBreaksPerEmployee: TdxBarButton;
    dxBarButtonWKPerEmpl: TdxBarButton;
    GoHoursPerEmployee: TAction;
    dxBarSubItemTimeRecording: TdxBarSubItem;
    dxBarButtonHoursPerEmployee: TdxBarButton;
    GoReportEmployee: TAction;
    GoCheckListScans: TAction;
    GoCompHours: TAction;
    dxBarReports: TdxBarSubItem;
    dxBarButtonCheckList: TdxBarButton;
    dxBarButtonCompHours: TdxBarButton;
    GoRepHoursPerEmpl: TAction;
    dxBarButtonHoursEmpl: TdxBarButton;
    GoWTRAbsenceReport: TAction;
    dxBarButtonAbsenceShedule: TdxBarButton;
    GoTimeRecording: TAction;
    GoTimeRecScan: TAction;
    dxBarButtonTimeRecScan: TdxBarButton;
    dxBarButtonRepEmpl: TdxBarButton;
    GoAbsenceReport: TAction;
    GoRepDirIndHrsWeek: TAction;
    GoTransferToNextYear: TAction;
    dxBarButtonTransferToNextYear: TdxBarButton;
    dxBarButtonCalcEarnedWKT: TdxBarButton;
    GoCalculateEarnedWKR: TAction;
    GoRepHrsPerWorkspot: TAction;
    dxBarButtonRepHrsPerWK: TdxBarButton;
    dxBarButtonRepDirIndHrs: TdxBarButton;
    GoReportRatioIllHrs: TAction;
    dxBarButtonRepRatilIllHrs: TdxBarButton;
    GoReportAvailableUsedTime: TAction;
    dxBarButtonRepAvailUsedTime: TdxBarButton;
    GoActionBankHolidays: TAction;
    dxBarButtonBankHol: TdxBarButton;
    GoRepHrsPerWKCum: TAction;
    dxBarButtonRepHrsPerWKCum: TdxBarButton;
    GoActionProcessAbsence: TAction;
    dxBarButtonProcessPlannedAbs: TdxBarButton;
    GoIllnessMessages: TAction;
    GoStandardOccupation: TAction;
    GoStdStaffAvailability: TAction;
    GoShiftSchedule: TAction;
    GoStaffAvailability: TAction;
    GoEmplAvailability: TAction;
    dxBarButtonStdOccupation: TdxBarButton;
    dxBarButtonStdStaffAvailability: TdxBarButton;
    dxBarButtonShiftSchedule: TdxBarButton;
    dxBarButtonStaffAvailability: TdxBarButton;
    dxBarButtonEmplAvail: TdxBarButton;
    dxBarSubItemStaffPlanning: TdxBarSubItem;
    dxBarButtonIllnessMessages: TdxBarButton;
    GoStaffPlanningAction: TAction;
    dxBarButtonStaffPlanning: TdxBarButton;
    ReportStaffPlanning: TAction;
    GoReportStaffAvaillability: TAction;
    GoReportEMPAvailability: TAction;
    GoReportStaffPlanning: TAction;
    GoReportStaffPlanningCond: TAction;
    dxBarButtonREPSTAFFAVAIL: TdxBarButton;
    dxBarButtonREPEMPAVAIL: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButtonRepStaffPlanning: TdxBarButton;
    dxBarButtonRepStaffPlanningCond: TdxBarButton;
    dxBarSubItemProdControl: TdxBarSubItem;
    ProductivityControl: TAction;
    GoManualDataCollection: TAction;
    dxBarButtonManualDataCollection: TdxBarButton;
    GoExportPayroll: TAction;
    GoProductionReport: TAction;
    dxBarSubItemProductionReport: TdxBarSubItem;
    dxBarButtonProdRep: TdxBarButton;
    GoTeamIncentiveRep: TAction;
    dxBarSubItemExport: TdxBarSubItem;
    dxBarButtonExport: TdxBarButton;
    dxBarButtonTeamIncRep: TdxBarButton;
    dxBarSubItemSales: TdxBarSubItem;
    dxBarButtonRevenue: TdxBarButton;
    dxBarSubItemQuality: TdxBarSubItem;
    dxBarButtonMistakePerEmpl: TdxBarButton;
    dxBarButtonQualityCalc: TdxBarButton;
    GoRevenue: TAction;
    GoMistakePerEmpl: TAction;
    dxBarSubItemConfig: TdxBarSubItem;
    dxBarButtonSystemConfg: TdxBarButton;
    dxBarButtonDataCol: TdxBarButton;
    dxBarButtonTypesOfHrs: TdxBarButton;
    dxBarButtonAbsRsn: TdxBarButton;
    GoSettings: TAction;
    GoDataConnection: TAction;
    GoTypeOfHours: TAction;
    GoAbsenceRsn: TAction;
    dxBarAbsenceType: TdxBarButton;
    GoAbsType: TAction;
    dxBarButtonPaymentExpCode: TdxBarButton;
    dxBarButtonExtraPayments: TdxBarButton;
    GoPaymentExpCode: TAction;
    GoExtraPayment: TAction;
    GoComportConnection: TAction;
    dxBarButtonComport: TdxBarButton;
    GoReportStaffPlanDay: TAction;
    dxBarButtonStaffPlanDay: TdxBarButton;
    GoProductionDetailReport: TAction;
    dxBarButtonProdDetailRep: TdxBarButton;
    GoReportAbsenceCard: TAction;
    GoProductionCompReport: TAction;
    dxBarButtonRepProdComp: TdxBarButton;
    GoRepHrsPerDay: TAction;
    dxBarButtonAbsenceHours: TdxBarButton;
    dxBarButtonRepHrsPerDay: TdxBarButton;
    dxBarButtonRepAbsenceCard: TdxBarButton;
    dxBarButtonPlant: TdxBarButton;
    dxBarButtonQualityIncentiveConf: TdxBarButton;
    GoQualityIncentiveCalc: TAction;
    GoProductionCompTargetReport: TAction;
    dxBarButtonProdCompTarget: TdxBarButton;
    GoEmployeeRegistrations: TAction;
    dxBarSubItemGoRegistrations: TdxBarSubItem;
    dxBarButtonEmployeeRegistrations: TdxBarButton;
    GoReportEmployeePaylist: TAction;
    dxBarSubItemReportPayments: TdxBarSubItem;
    dxBarButtonReportEmployeePaylist: TdxBarButton;
    dxBarSubItemRepUnitMaintenance: TdxBarSubItem;
    dxBarButtonRepPlant: TdxBarButton;
    UnitMaintenance: TAction;
    GoRepPlantStructure: TAction;
    GoMonthlyGroupEfficiency: TAction;
    dxBarButtonMonthlyGroupEfficiency: TdxBarButton;
    GoMachine: TAction;
    dxBarButtonMachine: TdxBarButton;
    GoCountries: TAction;
    dxBarButtonCountries: TdxBarButton;
    GoAbsenceTypesPerCountry: TAction;
    GoAbsenceReasonsPerCountry: TAction;
    GoHourTypesPerCountry: TAction;
    dxBarButtonAbsenceTypesPerCountry: TdxBarButton;
    dxBarButtonAbsenceReasonsPerCountry: TdxBarButton;
    dxBarButtonHourTypesPerCountry: TdxBarButton;
    dxBarButtonRepHrsPerContractGrpCum: TdxBarButton;
    dxBarButtonRepHolidayCard: TdxBarButton;
    GoRepHrsPerContractGrpCum: TAction;
    GoReportHolidayCard: TAction;
    GoReportChangesPerScan: TAction;
    GoReportEmployeeIllness: TAction;
    dxBarButtonRepChangesPerScan: TdxBarButton;
    dxBarButtonRepEmployeeIllness: TdxBarButton;
    GoReportIncentiveProgram: TAction;
    dxBarButtonErrorLog: TdxBarButton;
    GoErrorLog: TAction;
    dxBarButtonRepIncentiveProgram: TdxBarButton;
    GoReportJobComments: TAction;
    GoWorkScheduleAction: TAction;
    GoWorkScheduleDetailsAction: TAction;
    dxBarButtonWorkSchedule: TdxBarButton;
    dxBarButtonWorkScheduleDetails: TdxBarButton;
    GoReportEmpInfoDevPeriod: TAction;
    dxBarButtonRepEmpInfoDevPeriod: TdxBarButton;
    dxBarButtonRealTimeEffMonitor: TdxBarButton;
    GoRealTimeEffMonitor: TAction;
    GoReportGhostCounts: TAction;
    dxBarButtonReportGhostCounts: TdxBarButton;
    GoReportInOutAbsenceOvertime: TAction;
    dxBarButtonRepInOutAbsenceOvertime: TdxBarButton;
    GoPTODef: TAction;
    dxBarButtonPTODef: TdxBarButton;
    procedure dxSideBarItemClick(Sender: TObject; Item: TdxSideBarItem);
    procedure dxSideBarChangeActiveGroup(Sender: TObject);
    procedure GoTeamDeptExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GoPSPlantExecute(Sender: TObject);
    procedure GoBusinessUnitExecute(Sender: TObject);
    procedure GoDepartmentExecute(Sender: TObject);
    procedure GoActionListChange(Sender: TObject);
    procedure GoTeamExecute(Sender: TObject);
    procedure GoShiftExecute(Sender: TObject);
    procedure GoTimeBlockShiftExecute(Sender: TObject);
    procedure GoBreakPerShiftExecute(Sender: TObject);
    procedure GoTimeBlockPerDeptExecute(Sender: TObject);
    procedure GoBreakPerDeptExecute(Sender: TObject);
    procedure GoTimeBlockPerEmplExecute(Sender: TObject);
    procedure GoBreakPerEmplExecute(Sender: TObject);
    procedure GoTypeHoursExecute(Sender: TObject);
    procedure GoIDCardExecute(Sender: TObject);
    procedure GoContractGroupsExecute(Sender: TObject);
    procedure GoEmployeeExecute(Sender: TObject);
    procedure GoWorkspotExecute(Sender: TObject);
    procedure GoMachineExecute(Sender: TObject);
    procedure GoErrorLogExecute(Sender: TObject);
    procedure GoAbsenceReasonExecute(Sender: TObject);
    procedure GoAbsTypeExecute(Sender: TObject);
    procedure GoExceptionalhoursExecute(Sender: TObject);
    procedure GoOvertimeExecute(Sender: TObject);
    procedure GoWorkSpotWorkstationExecute(Sender: TObject);
    procedure GoWKPerEmplExecute(Sender: TObject);
    procedure GoEmplContractExecute(Sender: TObject);
    procedure GoConfigurationExecute(Sender: TObject);
    procedure GoHoursPerEmployeeExecute(Sender: TObject);

    procedure GoReportEmployeeExecute(Sender: TObject);
    procedure GoCheckListScansExecute(Sender: TObject);
    procedure GoCompHoursExecute(Sender: TObject);
    procedure GoRepHoursPerEmplExecute(Sender: TObject);
    procedure GoWTRAbsenceReportExecute(Sender: TObject);
    procedure GoTimeRecScanExecute(Sender: TObject);
    procedure GoAbsenceReportExecute(Sender: TObject);
    procedure GoRepDirIndHrsWeekExecute(Sender: TObject);
    procedure GoTransferToNextYearExecute(Sender: TObject);
    procedure GoCalculateEarnedWKRExecute(Sender: TObject);
    procedure GoRepHrsPerWorkspotExecute(Sender: TObject);
    procedure GoReportRatioIllHrsExecute(Sender: TObject);
    procedure GoReportAvailableUsedTimeExecute(Sender: TObject);
    procedure GoActionBankHolidaysExecute(Sender: TObject);

    procedure GoRepHrsPerWKCumExecute(Sender: TObject);
    procedure GoActionProcessAbsenceExecute(Sender: TObject);
    procedure GoIllnessMessagesExecute(Sender: TObject);
    procedure GoStandardOccupationExecute(Sender: TObject);
    procedure GoStdStaffAvailabilityExecute(Sender: TObject);
    procedure GoShiftScheduleExecute(Sender: TObject);
    procedure GoEmplAvailabilityExecute(Sender: TObject);
    procedure GoStaffPlanningActionExecute(Sender: TObject);
    procedure GoReportStaffAvaillabilityExecute(Sender: TObject);
    procedure GoReportEMPAvailabilityExecute(Sender: TObject);
    procedure GoReportStaffPlanningExecute(Sender: TObject);
    procedure GoReportStaffPlanningCondExecute(Sender: TObject);
    procedure GoReportStaffPlanDayExecute(Sender: TObject);
    procedure GoStaffAvailabilityExecute(Sender: TObject);
    procedure GoQualityConfExecute(Sender: TObject);
    procedure GoQualityIncentiveCalcExecute(Sender: TObject);
    procedure GoManualDataCollectionExecute(Sender: TObject);
    procedure GoExportPayrollExecute(Sender: TObject);
    procedure GoProductionReportExecute(Sender: TObject);
    procedure GoProductionDetailReportExecute(Sender: TObject);
    procedure GoTeamIncentiveRepExecute(Sender: TObject);
    procedure GoColConnectionExecute(Sender: TObject);
    procedure GoRevenueExecute(Sender: TObject);
    procedure GoMistakePerEmplExecute(Sender: TObject);
    procedure GoPaymentExpCodeExecute(Sender: TObject);
    procedure GoExtraPaymentExecute(Sender: TObject);
    procedure GoComportConnectionExecute(Sender: TObject);
    procedure GoReportAbsenceCardExecute(Sender: TObject);
    procedure GoProductionCompReportExecute(Sender: TObject);
    procedure GoRepHrsPerDayExecute(Sender: TObject);
    procedure GoProductionCompTargetReportExecute(Sender: TObject);
    procedure GoEmployeeRegistrationsExecute(Sender: TObject);
    procedure GoReportEmployeePaylistExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GoRepPlantStructureExecute(Sender: TObject);
    procedure GoMonthlyGroupEffiencyExecute(Sender: TObject);
    procedure GoCountriesExecute(Sender: TObject);
    procedure GoAbsenceTypesPerCountryExecute(Sender: TObject);
    procedure GoAbsenceReasonsPerCountryExecute(Sender: TObject);
    procedure GoHourTypesPerCountryExecute(Sender: TObject);
    procedure SwitchSelectedItem(const ATag: Integer);
    procedure GoRepHrsPerContractGrpCumExecute(Sender: TObject);
    procedure GoReportHolidayCardExecute(Sender: TObject);
    procedure GoReportChangesPerScanExecute(Sender: TObject);
    procedure GoReportEmployeeIllnessExecute(Sender: TObject);
    procedure GoReportIncentiveProgramExecute(Sender: TObject);
    procedure GoReportJobCommentsExecute(Sender: TObject);
    procedure GoWorkScheduleActionExecute(Sender: TObject);
    procedure GoWorkScheduleDetailsActionExecute(Sender: TObject);
    procedure GoReportEmpInfoDevPeriodExecute(Sender: TObject);
    procedure GoRealTimeEffMonitorExecute(Sender: TObject);
    procedure GoReportGhostCountsExecute(Sender: TObject);
    procedure GoReportInOutAbsenceOvertimeExecute(Sender: TObject);
    procedure GoPTODefExecute(Sender: TObject);
  private
    FLastDxSideBarItem: TdxSideBarItem;
    FLastItemTag: Integer;
    FCurrentItemTag: Integer;
    { Private declarations }
    procedure SwitchSideMenuGroup(const GroupIndex: Integer);
    procedure SwitchToMenuOption(const PimsMenuOption: Integer);
    procedure MakeAllGroupsVisible;
    procedure SwitchForm(const AForm: TForm; const ATag: Integer);
    procedure UpdateSideGroups(const PimsMenuOption: Integer);
    procedure UpdateTopMenu(const PimsMenuOption: Integer);
    //CAR : 15-9-2003: changes for user rights
    // new functions, procedures
    function CheckValidSelectedItemBar(ATag: Integer): Boolean;

    function SwitchDialog(const AGroup: Integer; const ATag: Integer): Boolean;
    function MenuOptionInMenuSet(const AOption: Integer;
      const PimsMenuOption: Integer; QueryPimsMenu: TQuery): Boolean;
    procedure UpdateGroupButtons(const PimsMenuOption: Integer);
    procedure UpdateCategoryItems(const PimsMenuOption: Integer);
    //
    procedure NotVisibleIncentivePrg;
    procedure ConfigurationGroup_NotVisibleIncentivePrg;
    procedure NotVisibleExportPrg;
    procedure SetInVisible(MenuGroup, MenuItem: Integer);
    //
    function SideBarItemSearch(const ATag: Integer): TdxSideBarItem;
    procedure SideBarItemCaption(const ATag: Integer;
      const ACaption: String);
    function GetLastDxSideBarItem: TdxSideBarItem;
    procedure SetLastDxSideBarItem(const Value: TdxSideBarItem);
    procedure NotVisibleReportIncentiveProgram;
    procedure ExportMenuStructure(AExport: Boolean);
  public
    { Public declarations }
    ReportDialog: TDialogBaseF;

    procedure ShowAdminPlant;
    procedure ShowReportMenu;
    procedure ShowProductivity;
    procedure ShowTimeRecording;
    procedure ShowPlanning;
    procedure ShowPayment;
    procedure SwitchEmplAvailForm(Employee: Integer;  Year, Week: Word);
    procedure SwitchStaffAvailForm(Plant, Team: String; Year, Week: Integer);
    procedure SwitchDialogReportQuality(Team, Plant: String; Year, Week: Integer);
    //CAR 6-2-2004 - turn this form into an APPWINDOW
    procedure CreateParams(var Params: TCreateParams); override;
    property LastDxSideBarItem: TdxSideBarItem read GetLastDxSideBarItem
      write SetLastDxSideBarItem;
    property CurrentItemTag: Integer read FCurrentItemTag write FCurrentItemTag;
    property LastItemTag: Integer read FLastItemTag write FLastItemTag;
  end;

var
  SideMenuUnitMaintenanceF: TSideMenuUnitMaintenanceF;

implementation

{$R *.DFM}

uses
  GridBaseFRM, GridBaseDMT,
  PlantFRM, BusinessUnitFRM, DepartmentFRM,
  ShiftFRM, TimeBlockPerShiftFRM, BreakPerShiftFRM, TimeBlockPerDeptFRM,
  TimeBlockPerEmplFRM, BreakPerEmplFRM,
  TypeHourFRM, WorkSpotFRM, AbsReasonFRM, AbsTypeFRM, ExceptHourFRM,
  OverTimeDefFRM, TeamFRM, TeamDeptFRM, WorkSpotPerStationFRM,
  EmployeeContractsFRM, SettingsFRM, SystemDMT,
  BreakPerDeptFRM, IDCardFRM, ContractGroupFRM, EmployeeFRM, DialogPasswordFRM,
  UPimsMessageRes, JobCodeFRM, HoursPerEmployeeFRM,
  DialogReportCheckListScanFRM, DialogReportCompHoursFRM,
  DialogReportHoursPerEmplFRM, DialogReportAbsenceSheduleFRM,
  TimeRecScanningFRM, TeamDeptDMT, DialogReportEmployeeFRM,
  DialogReportAbsenceFRM, DialogReportDirIndHrsWeekFRM, DialogTransferFreeTimeFRM,
  DialogCalculatedEarnedWTRFRM, DialogReportHrsPerWKFRM,
  DialogReportRatioIllnessHrsFRM, DialogReportAvailableUsedTimeFRM,
  DialogBankHolidayFRM, DialogReportHrsPerWKCUMFRM,
  DialogProcessAbsenceHrsFRM, IllnessMessagesFRM, StandardOccupationFRM,
  StandStaffAvailFRM, ShiftScheduleFRM, EmployeeAvailabilityFRM,
  DialogStaffPlanningFRM, DialogReportStaffAvailabilityFRM,
  DialogReportEmpAvailabilityFRM, DialogReportStaffPlanningFRM,
  StaffAvailabilityFRM, QualityIncentiveConfFRM, MistakePerEmplFRM,
  DialogReportQualityIncCalculationFRM, DataCollectionEntryFRM,
  DialogExportPayrollFRM, DialogReportProductionFRM, CalculateTotalHoursDMT,
  DialogReportTeamIncentiveFRM, DataColConnectionFRM, RevenueFRM,
  PaymentExportCodeFRM, ExtraPaymentFRM, ComPortConnectionFRM,
  DialogReportStaffPlanDayFRM, DialogReportProductionDetailFRM,
  DialogReportAbsenceCardFRM, DialogReportProductionCompFRM,
  DialogReportHrsPerDayFRM,
  UPimsConst, DialogWorkspotPerEmployeeFRM,
  DialogReportProdCompTargetFRM,
  EmployeeRegsDMT, EmployeeRegsFRM,
  DialogReportEmployeePaylistFRM, DialogReportPlantStructure,
  DialogReportStaffPlanningCondFRM,
  DialogMonthlyGroupEfficiencyFRM,
  DialogMonthlyEmployeeEfficiencyFRM,
  MachineDMT, MachineFRM,
  CountriesDMT, CountriesFRM, AbsTypePerCountryFRM, AbsReasonPerCountryFRM,
  TypeHourPerCountryFRM, DialogReportHrsPerContractGrpCUMFRM,
  DialogReportHolidayCardFRM,
  DialogReportChangesperScanFRM,  // 20014037
  DialogReportEmployeeIllnessFRM,  // 20014137
  DialogReportIncentiveProgramFRM, // 20014715
  ErrorLogDMT, ErrorLogFRM, // 20014826
  DialogReportJobCommentsFRM, // PIM-50
  WorkScheduleFRM, WorkScheduleDetailsFRM, // PIM-52
  DialogReportEmpInfoDevPeriodFRM, // PIM-23
  RealTimeEffMonitorFRM, // PIM-142
  DialogReportGhostCountsFRM, // PIM-152
  DialogReportInOutAbsenceOvertimeFRM, // GLOB3-83
  PTODefFRM; // GLOB3-204

//CAR 6-2-2004 turn this form into an APPWINDOW
procedure TSideMenuUnitMaintenanceF.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  Params.ExStyle:= Params.ExStyle or WS_EX_APPWINDOW;
end;

//CAR: 24-9-2003
procedure TSideMenuUnitMaintenanceF.SetInVisible(MenuGroup, MenuItem: Integer);
var
  IndexButton: Integer;
begin
  for IndexButton := dxSideBar.Groups[MenuGroup].ItemCount - 1 downto 0 do
    if dxSideBar.Groups[MenuGroup].Items[IndexButton].Tag = MenuItem then
      dxSideBar.Groups[MenuGroup].Items.Delete(IndexButton);
end;

// RV067.MRA.36 Do this AFTER the configuration group has been determined,
//              or the determination of visible/not-visible items will go wrong,
//              because this item is not there anymore!
procedure TSideMenuUnitMaintenanceF.ConfigurationGroup_NotVisibleIncentivePrg;
begin
  if not SystemDM.GetQualityIncentiveConf then
  begin
    SetInVisible(GROUP_CONFIGURATION, MENU_ITEM_QUALITYCONF);
  end;
end;

procedure TSideMenuUnitMaintenanceF.NotVisibleIncentivePrg;
begin
  if not SystemDM.GetQualityIncentiveConf then
  begin
//    SetInVisible(GROUP_CONFIGURATION, MENU_ITEM_QUALITYCONF);
    SetInVisible(GROUP_QUALITY_INCENTIVE, MENU_ITEM_MISTAKESPEREMPL);
    SetInVisible(GROUP_QUALITY_INCENTIVE, MENU_ITEM_QUALITYINCENTIVECALC);
    dxBarButtonQualityIncentiveConf.Visible  := ivNever;
    dxBarButtonMistakePerEmpl.Visible := ivNever;
    dxBarButtonMistakePerEmpl.Enabled := False;
    dxBarButtonQualityCalc.Visible := ivNever;
    dxBarButtonQualityCalc.Enabled := False;
    //CAR 21-10-2003
    dxBarSubItemQuality.Visible := ivNever;
  end;
end;

procedure TSideMenuUnitMaintenanceF.NotVisibleExportPrg;
begin
  if not SystemDM.GetExportPayroll then
  begin
    SetInVisible(GROUP_PAYROLLSYSTEM, MENU_ITEM_EXPORTPAYROLL);
    dxBarButtonExport.Visible := ivNever;
  end;
  // MR:05-10-2004 Also item 'monthly group efficiency'
  if not SystemDM.GetMonthGroupEfficiency then
  begin
    SetInVisible(GROUP_PAYROLLSYSTEM, MENU_ITEM_MONTHLYGROUPEFFICIENCY);
    dxBarButtonMonthlyGroupEfficiency.Visible := ivNever;
  end
  else
  begin
    // MR:12-03-2007 Order 550443. Only show one type of option.
    SideBarItemCaption(MENU_ITEM_MONTHLYGROUPEFFICIENCY,
      SPimsGroupEfficiency)
{

    // If 'N' then the set caption to 'Monthly Efficiency'
    // Else set it to caption 'Monthly Group Efficiency'
    if (SystemDM.TableExportPayroll.
      FieldByName('MONTH_GROUP_EFFICIENCY_YN').AsString = 'N') then
      SideBarItemCaption(MENU_ITEM_MONTHLYGROUPEFFICIENCY,
        SPimsGroupEfficiency)
    else
        SideBarItemCaption(MENU_ITEM_MONTHLYGROUPEFFICIENCY,
          SPimsMonthGroupEfficiency);
}
  end;
end;

function TSideMenuUnitMaintenanceF.SideBarItemSearch(
  const ATag: Integer): TdxSideBarItem;
var
  I, J: Integer;
begin
  Result := nil;
  for I:=0 to dxSideBar.GroupCount - 1 do
  begin
    for J:=0 to dxSideBar.Groups.Items[I].ItemCount - 1 do
    begin
      Result := dxSideBar.Groups.Items[I].Items[J];
      if (Result <> nil) then
        if Result.Tag = ATag then
          break;
    end;
    if (Result <> nil) then
      if Result.Tag = ATag then
        break;
  end;
end;

procedure TSideMenuUnitMaintenanceF.SwitchSelectedItem(const ATag: Integer);
var
  MyDxSideBarItem: TdxSideBarItem;
begin
  MyDxSideBarItem := SideBarItemSearch(ATag);
  if (MyDxSideBarItem <> nil) then
    if MyDxSideBarItem.Tag = ATag then
      dxSideBar.SelectedItem := MyDxSideBarItem;
end;

procedure TSideMenuUnitMaintenanceF.SideBarItemCaption(const ATag: Integer;
  const ACaption: String);
var
  MyDxSideBarItem: TdxSideBarItem;
begin
  MyDxSideBarItem := SideBarItemSearch(ATag);
  if (MyDxSideBarItem <> nil) then
    if MyDxSideBarItem.Tag = ATag then
      MyDxSideBarItem.Caption := ACaption;
end;

// CAR 22-9-2003: user rights changes:

// check if selected item exists or if is visible
function TSideMenuUnitMaintenanceF.CheckValidSelectedItemBar(ATag: Integer): Boolean;
begin
  Result := False;
  if dxSideBar.SelectedItem = Nil then
  begin
    NilInsertForm;
    Exit;
  end;
  if dxSideBar.SelectedItem <> Nil then
    if (dxSideBar.SelectedItem.CustomData = ITEM_INVISIBIL) or
      (dxSideBar.SelectedItem.Tag <> ATag) then
    begin
      NilInsertForm;
      dxSideBar.SelectedItem := Nil;
      Exit;
    end;
  Result := True;
end;

//use this function when a form is opened by a button
procedure TSideMenuUnitMaintenanceF.SwitchForm(const AForm: TForm;
  const ATag: Integer);
begin
  SwitchSelectedItem(ATag);

  if not CheckValidSelectedItemBar(ATag) then
    Exit;
  InsertForm := AForm;
  if (AForm is TGridBaseF) then // RV089.1. It can also be another type of form!
    (AForm as TGridBaseF).Form_Edit_YN := dxSideBar.SelectedItem.CustomData;
  // forms are not show on SetInsertForm as in the past - because
  // one property Form_Edit_YN has to be set before
  // RV089.1. Some forms have properties to set before they are shown!
  if (AForm is TDialogStaffPlanningF) then
    (AForm as TDialogStaffPlanningF).Edit_Staff_Planning :=
      dxSideBar.SelectedItem.CustomData;
  // RV089.1. Some forms have properties to set before they are shown!
  if (AForm is TDialogWorkspotPerEmployeeF) then
    (AForm as TDialogWorkspotPerEmployeeF).Edit_Workspot_Planning :=
      dxSideBar.SelectedItem.CustomData;
  AForm.Show;
end;

// use this function when a dialog is opened
function TSideMenuUnitMaintenanceF.SwitchDialog(const AGroup: Integer;
  const ATag: Integer): Boolean;
begin
  // RV089.1. Prevent the same embedded dialog is called again!
  if SystemDM.EmbedDialogs then
    if InsertForm <> nil then
      if CurrentItemTag = LastItemTag then
       begin
         Result := False;
         Exit;
       end;
  Result := False;
  NilInsertForm;
  SwitchSideMenuGroup(AGroup);
  SwitchSelectedItem(ATag);
  //CAR 16-9-2003 rights user changes
  if not CheckValidSelectedItemBar(ATag) then
    Exit;
  Result := True;
end;

// particular calls of forms  - more parameters on Create function
procedure TSideMenuUnitMaintenanceF.SwitchEmplAvailForm(Employee: Integer;
  Year, Week: Word);
begin
  SwitchSelectedItem(MENU_EMPLOYEE_AVAILABILITY);

  if not CheckValidSelectedItemBar(MENU_EMPLOYEE_AVAILABILITY) then
    Exit;
  InsertForm := EmployeeAvailabilityF(Employee, Year, Week);
  //set properties of form
  (InsertForm as TGridBaseF).Form_Edit_YN := dxSideBar.SelectedItem.CustomData;
  (InsertForm as TEmployeeAvailabilityF).Visible_Staff_Avail :=
    (dxBarButtonStaffAvailability.Visible = ivAlways);
  InsertForm.Show;
end;

procedure TSideMenuUnitMaintenanceF.SwitchStaffAvailForm(Plant, Team: String;
  Year, Week: Integer);
begin
  SwitchSelectedItem(MENU_STAFF_AVAILABILITY);
  if not CheckValidSelectedItemBar(MENU_STAFF_AVAILABILITY) then
    Exit;
  InsertForm := StaffAvailabilityF(Plant, Team, Year, Week);
  //set properties of form
  (InsertForm as TGridBaseF).Form_Edit_YN := dxSideBar.SelectedItem.CustomData;
  (InsertForm as TStaffAvailabilityF).Visible_Empl_Avail :=
    (dxBarButtonEmplAvail.Visible = ivAlways);
  (InsertForm as TStaffAvailabilityF).Visible_Rep_Avail := True;
//    (dxBarButtonREPSTAFFAVAIL.Visible = ivAlways);
  InsertForm.Show;
end;

procedure TSideMenuUnitMaintenanceF.SwitchDialogReportQuality(Team, Plant: String;
  Year, Week: Integer);
begin

  SwitchSelectedItem(MENU_ITEM_QUALITYINCENTIVECALC);
  if not CheckValidSelectedItemBar(MENU_ITEM_QUALITYINCENTIVECALC) then
    Exit;
  NilInsertForm;

  // RV089.1.
  if not SystemDM.EmbedDialogs then
  begin
    DialogReportQualityIncCalculationF :=
      TDialogReportQualityIncCalculationF.Create(Application,Team,Plant,Year,Week);
    try
      DialogReportQualityIncCalculationF.ShowModal;
    finally
      DialogReportQualityIncCalculationF.Free;
    end;
  end
  else
    SwitchForm(DialogReportQualityIncCalculationForm, MENU_ITEM_QUALITYINCENTIVECALC);
end;

// Actions needed when user has selected an option
// in the Side-Menu.
procedure TSideMenuUnitMaintenanceF.dxSideBarItemClick(Sender: TObject;
  Item: TdxSideBarItem);
begin
  inherited;

  CurrentItemTag := Item.Tag;
  case Item.Tag of
    0                               : (Item.ItemObject as TdxBarButton).Click;
    MENU_ITEM_PLANT                 : GoPSPlantExecute(Sender);
    MENU_ITEM_BUSINESSUNIT          : GoBusinessUnitExecute(Sender);
    MENU_ITEM_DEPARTMENT            : GoDepartmentExecute(Sender);
    MENU_ITEM_WORKSPOTS             : GoWorkspotExecute(Sender);
    MENU_ITEM_TEAMS                 : GoTeamExecute(Sender);
    MENU_ITEM_DEPARTMENTTEAM        : GoTeamDeptExecute(Sender);
    MENU_ITEM_WORKSPOTWORKSTATION   : GoWorkSpotWorkstationExecute(Sender);
    MENU_ITEM_SETTINGS              : GoConfigurationExecute(Sender);
    MENU_ITEM_QUALITYCONF           : GoQualityConfExecute(Sender);
    MENU_ITEM_DATA_COL_CONNECTION   : GoColConnectionExecute(Sender);
    MENU_ITEM_COMPORT_CONNECTION    : GoComportConnectionExecute(Sender);
    MENU_ITEM_MACHINE               : GoMachineExecute(Sender);

    MENU_ITEM_SHIFTS                : GoShiftExecute(Sender);
    MENU_ITEM_TIMEBLOCKSPERSHIFT    : GoTimeBlockShiftExecute(Sender);
    MENU_ITEM_BREAKSPERSHIFTS       : GoBreakPerShiftExecute(Sender);
    MENU_ITEM_TIMEBLOCKSPERDEPT     : GoTimeBlockPerDeptExecute(Sender);
    MENU_ITEM_BREAKSPERDEPT         : GoBreakPerDeptExecute(Sender);
    MENU_ITEM_TYPEOFHOURS           : GoTypeHoursExecute(Sender);
    //RV067.4.
    MENU_ITEM_COUNTRIES             : GoCountriesExecute(Sender);
    MENU_ITEM_ABSENCETYPESPERCTR    : GoAbsenceTypesPerCountryExecute(Sender);
    MENU_ITEM_ABSENCEREASONSPERCTR  : GoAbsenceReasonsPerCountryExecute(Sender);
    MENU_ITEM_TYPEOFHOURSPERCTR     : GoHourTypesPerCountryExecute(Sender);
    MENU_ITEM_ERRORLOG              : GoErrorLogExecute(Sender); // 20014826

    MENU_ITEM_ABSENCEREASONS        : GoAbsenceReasonExecute(Sender);
    MENU_ITEM_ABSENCETYPES          : GoAbsTypeExecute(Sender);
    MENU_ITEM_CONTRACTGROUPS        : GoContractGroupsExecute(Sender);
    MENU_ITEM_EXCEPTIONALHOURS      : GoExceptionalhoursExecute(Sender);
    MENU_ITEM_OVERTIME              : GoOvertimeExecute(Sender);
    MENU_ITEM_PTODEF                : GoPTODefExecute(Sender);
    MENU_ITEM_EMPLOYEES             : GoEmployeeExecute(Sender);
    MENU_ITEM_EMPLOYEECONTRACTS     : GoEmplContractExecute(Sender);
    MENU_ITEM_IDCARDS               : GoIDCardExecute(Sender);
    MENU_ITEM_TIMEBLOCKSPEREMPLOYEE : GoTimeBlockPerEmplExecute(Sender);
    MENU_ITEM_BREAKSPEREMPLOYEE     : GoBreakPerEmplExecute(Sender);
    MENU_ITEM_WORKSPOTPEREMPLOYEE   : GoWKPerEmplExecute(Sender);

//PIMS FIRST PART

// TIME RECORDING START
    MENU_ITEM_TIMERECORDINGSCAN     : GoTimeRecScanExecute(Sender);
    MENU_ITEM_HOURSPEREMPLOYEE      : GoHoursPerEmployeeExecute(Sender);
    MENU_ITEM_PROCESSABSENCE        : GoActionProcessAbsenceExecute(Sender);
    MENU_ITEM_TRANSFER_FREE_TIME_NEXT_YEAR : GoTransferToNextYearExecute(Sender);
    MENU_ITEM_CALCULATE_EARNED_WTR  : GoCalculateEarnedWKRExecute(Sender);
    MENU_ITEM_BANK_HOLIDAY          : GoActionBankHolidaysExecute(Sender);
    MENU_ILLNESSMESSAGES            : GoIllnessMessagesExecute(Sender);
// END TIME RECORDING

//    ------REPORTS
    MENU_ITEM_REP_EMPLOYEE          : GoReportEmployeeExecute(Sender);
    MENU_ITEM_REP_CHECKLISTSCAN     : GoCheckListScansExecute(Sender);
    MENU_ITEM_REP_COMPHOURSWITHCONTRHOURS : GoCompHoursExecute(Sender);
    MENU_ITEM_REP_HOURSPEREMPLOYEE  : GoRepHoursPerEmplExecute(Sender);
    MENU_ITEM_REP_WTR_HOL_ILL       : GoWTRAbsenceReportExecute(Sender);
    MENU_ITEM_REP_ABSENCE           : GoAbsenceReportExecute(Sender);
    MENU_ITEM_REP_HOURS_PER_DAY     : GoRepHrsPerDayExecute(Sender);
    MENU_ITEM_REP_ABSENCECARD       : GoReportAbsenceCardExecute(Sender);
    MENU_ITEM_REP_DIRINDHRSWEEK     : GoRepDirIndHrsWeekExecute(Sender);
    MENU_ITEM_REP_HRSPERWORKSPOT    : GoRepHrsPerWorkspotExecute(Sender);
    MENU_ITEM_REP_RATILILLNESSHRS   : GoReportRatioIllHrsExecute(Sender);
    MENU_ITEM_REP_AVAILABLEUSEDTIME : GoReportAvailableUsedTimeExecute(Sender);
    MENU_ITEM_REP_HRSPERWORKSPOTCUM : GoRepHrsPerWKCumExecute(Sender);
    MENU_ITEM_REP_HRSPERCONTRACTGRPCUM : GoRepHrsPerContractGrpCumExecute(Sender);
    MENU_ITEM_REP_HOLIDAYCARD       : GoReportHolidayCardExecute(Sender);
    MENU_ITEM_REP_CHANGESPERSCAN    : GoReportChangesPerScanExecute(Sender);
    MENU_ITEM_REP_EMPLOYEEILLNESS   : GoReportEmployeeIllnessExecute(Sender);
    MENU_ITEM_REP_INCENTIVEPROGRAM  : GoReportIncentiveProgramExecute(Sender);
    MENU_ITEM_REP_JOBCOMMENTS       : GoReportJobCommentsExecute(Sender);
    MENU_ITEM_REP_EMPINFODEVPERIOD  : GoReportEmpInfoDevPeriodExecute(Sender);
    MENU_ITEM_REP_INOUTABSOVERTIME  : GoReportInOutAbsenceOvertimeExecute(Sender);

//END REPORTS
// Planning
    MENU_STANDARD_OCCUPATION        : GoStandardOccupationExecute(Sender);
    MENU_STANDARD_STAFF_AVAILABILITY: GoStdStaffAvailabilityExecute(Sender);
    MENU_SHIFT_SCHEDULE             : GoShiftScheduleExecute(Sender);
    MENU_STAFF_AVAILABILITY         : GoStaffAvailabilityExecute(Sender);
    MENU_EMPLOYEE_AVAILABILITY      : GoEmplAvailabilityExecute(Sender);
    MENU_STAFF_PLANNING             : GoStaffPlanningActionExecute(Sender);

    MENU_WORK_SCHEDULE              : GoWorkScheduleActionExecute(Sender); // PIM-52
    MENU_WORK_SCHEDULE_DETAILS      : GoWorkScheduleDetailsActionExecute(Sender); // PIM-52

//end planning
// BEGIN REPORTS STAFF PLANNING
    MENU_ITEM_REP_STAFF_AVAIL_EMP   : GoReportStaffAvaillabilityExecute(Sender);
    MENU_ITEM_REP_EMP_AVAIL         : GoReportEMPAvailabilityExecute(Sender);
    MENU_ITEM_REP_STAFF_PLANNING    : GoReportStaffPlanningExecute(Sender);
    MENU_ITEM_REP_STAFF_PLAN_DAY    : GoReportStaffPlanDayExecute(Sender);
    MENU_ITEM_REP_STAFF_PLANNING_COND : GoReportStaffPlanningCondExecute(Sender);
//END REPORTS
// start productivity
    MENU_ITEM_MISTAKESPEREMPL       : GoMistakePerEmplExecute(Sender);
// START PAYMENT
    MENU_ITEM_QUALITYINCENTIVECALC  : GoQualityIncentiveCalcExecute(Sender);
    MENU_ITEM_DATA_COLLECTION_ENTRY : GoManualDataCollectionExecute(Sender);
    MENU_ITEM_REAL_TIME_EFF_MONITOR : GoRealTimeEffMonitorExecute(Sender);
    MENU_ITEM_REVENUE               : GoRevenueExecute(Sender);
    //
//PAYROLL
    MENU_PAYMENT_EXP_CODE           : GoPaymentExpCodeExecute(Sender);
    MENU_EXTRA_PAYMENT              : GoExtraPaymentExecute(Sender);
    MENU_ITEM_EXPORTPAYROLL         : GoExportPayrollExecute(Sender);
    MENU_ITEM_MONTHLYGROUPEFFICIENCY: GoMonthlyGroupEffiencyExecute(Sender);
//END Payroll
    MENU_ITEM_PRODUCTIONREPORT      : GoProductionReportExecute(Sender);
    MENU_ITEM_PRODUCTIONDETAILREPORT: GoProductionDetailReportExecute(Sender);
    MENU_ITEM_TEAMINCENTIVEREPORT   : GoTeamIncentiveRepExecute(Sender);
    MENU_ITEM_PRODUCTIONCOMPREPORT  : GoProductionCompReportExecute(Sender);
    MENU_ITEM_PRODCOMPTARGETREPORT  : GoProductionCompTargetReportExecute(Sender);
    MENU_ITEM_REPORTGHOSTCOUNTS     : GoReportGhostCountsExecute(Sender);

    MENU_ITEM_EMPLOYEE_REGISTRATIONS: GoEmployeeRegistrationsExecute(Sender);
//Start Payments (reports)
    MENU_ITEM_REP_EMPLOYEE_PAYLIST  : GoReportEmployeePaylistExecute(Sender);
//End Payments (reports)
// rep - Unit Maintenance
    MENU_ITEM_REP_PLANT_STRUCTURE   : GoRepPlantStructureExecute(Sender);
  end;
  LastItemTag := Item.Tag;
end;

procedure TSideMenuUnitMaintenanceF.SwitchSideMenuGroup(const GroupIndex: Integer);
begin
  dxSideBar.SelectedItem := Nil;
  dxSideBar.ActiveGroup := dxSideBar.Groups[GroupIndex];
  dxSideBar.Repaint;
end;

// car 22-9-2003 user rights changes
procedure TSideMenuUnitMaintenanceF.SwitchToMenuOption(
  const PimsMenuOption: Integer);
begin
  UpdateTopMenu(PimsMenuOption);
  UpdateSideGroups(PimsMenuOption);
  // update buttons - make invisible - into the visible groups/or top menu category
  UpdateGroupButtons(PimsMenuOption);
  UpdateCategoryItems(PimsMenuOption);
  Update;
  Show;
  // sometimes groups are not correct displayed
  dxSideBar.Repaint;
end;

// delete invisible buttons - inside visible groups of menu option
// the corespondence between buttons of SideBar and menu numbers records of table
//PIMSMENUGROUPS is based on order of buttons inside groups
procedure TSideMenuUnitMaintenanceF.UpdateGroupButtons(
  const PimsMenuOption: Integer);
var
  QueryPimsButtons,
  QueryPimsMenuOption: TQuery;
  CheckUpdateDone, VisibleButton: Boolean;
  GroupIndex, ButtonIndex: Integer;
begin
// check if the Groups were already updated:
  CheckUpdateDone := False;
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
    if dxSideBar.Groups[GroupIndex].Visible then
    begin
    //CAR 29-10-2003
      if dxSideBar.Groups[GroupIndex].ItemCount = 0 then
        dxSideBar.Groups[GroupIndex].Visible := False;
      for ButtonIndex := 0 to dxSideBar.Groups[GroupIndex].ItemCount - 1 do
      begin
        if (dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData <>
          ITEM_DEFAULT) then
          CheckUpdateDone := True;
      end;
    end;
  if CheckUpdateDone then
    Exit;
// get query for groups
  QueryPimsMenuOption := SystemDM.GetMenuTreeOfParentMenu(
    SystemDM.GetMenuNumberOfMenuOption(PimsMenuOption), CHECKEDVALUE,
    CHECKEDVALUE);
  QueryPimsMenuOption.First;
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
  begin
    if dxSideBar.Groups[GroupIndex].Visible then
    begin
      if not QueryPimsMenuOption.Eof then
      begin
// get query for buttons inside a group
        QueryPimsButtons := SystemDM.GetMenuTreeOfParentMenu2(
          QueryPimsMenuOption.FieldByName('MENU_NUMBER').AsInteger,
          UNCHECKEDVALUE, UNCHECKEDVALUE);
        QueryPimsButtons.First;
        for ButtonIndex := 0 to dxSideBar.Groups[GroupIndex].ItemCount - 1 do
        begin
          VisibleButton := not QueryPimsButtons.Eof and
            (QueryPimsButtons.FieldByName('VISIBLE_YN').AsString = CHECKEDVALUE);
          if VisibleButton and
           (dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData =
           ITEM_DEFAULT) then
          begin
            if QueryPimsButtons.FieldByName('EDIT_YN').AsString = CHECKEDVALUE then
              dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData :=
                ITEM_VISIBIL_EDIT
            else
              dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData :=
                ITEM_VISIBIL;
          end;
          if not VisibleButton then
            if (dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData =
              ITEM_DEFAULT) then
              dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData :=
                ITEM_INVISIBIL;
          QueryPimsButtons.Next;
        end;
        // RV087.3. Do not free!
        // QueryPimsButtons.Free;
        // RV067.MRA.36
        // Menu number 121500 is the Configuration-group in menu.
        // Do this here: It will remove a menu-item!
        if QueryPimsMenuOption.FieldByName('MENU_NUMBER').AsInteger = 121500 then
          ConfigurationGroup_NotVisibleIncentivePrg;
        // next visible group
        QueryPimsMenuOption.Next;
      end;
    end;// if group is visible
  end;// for groups
  // RV087.3. Do not free!
  // QueryPimsMenuOption.Free;

 // delete invisible buttons
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
  begin
    if dxSideBar.Groups[GroupIndex].Visible then
    begin
      ButtonIndex := 0;
      while ButtonIndex <= dxSideBar.Groups[GroupIndex].ItemCount - 1 do
      begin
        if dxSideBar.Groups[GroupIndex].Items[ButtonIndex].CustomData =
          ITEM_INVISIBIL then
        begin
           dxSideBar.Groups[GroupIndex].Items.Delete(ButtonIndex);
           ButtonIndex := 0;
        end
        else
          Inc(ButtonIndex);
      end;
      if dxSideBar.Groups[GroupIndex].ItemCount = 0 then
        dxSideBar.Groups[GroupIndex].Visible := False;
    end;// if group is visible
  end;// for groups
  // PIM-12 Make all items invisible with tag=999 used as Dummy-properties
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
    SetInVisible(GroupIndex, 999);
end;

procedure TSideMenuUnitMaintenanceF.UpdateCategoryItems(
  const PimsMenuOption: Integer);
var
  QueryPimsButtons,
  QueryPimsMenuOption: TQuery;
  CategoryIndex, ItemIndex: Integer;
  VisibleCategory: Boolean;
begin
  QueryPimsMenuOption := SystemDM.GetMenuTreeOfParentMenu(
    SystemDM.GetMenuNumberOfMenuOption(PimsMenuOption), CHECKEDVALUE,
    CHECKEDVALUE);

  QueryPimsMenuOption.First;
  for CategoryIndex := 7 to dxBarManBase.Categories.Count - 1  do
  begin
    if dxBarManBase.CategoryItemsVisible[CategoryIndex] = ivAlways then
    begin
      VisibleCategory := False;
      if not QueryPimsMenuOption.Eof then
      begin
        QueryPimsButtons := SystemDM.GetMenuTreeOfParentMenu2(
          QueryPimsMenuOption.FieldByName('MENU_NUMBER').AsInteger,
          UNCHECKEDVALUE, UNCHECKEDVALUE);
        QueryPimsButtons.First;
        ItemIndex := 0;
        while (ItemIndex <= dxBarManBase.ItemCount - 1) do
        begin
          if (dxBarManBase.Items[ItemIndex].Category = CategoryIndex)and
            (dxBarManBase.Items[ItemIndex].Action <> Nil) then
          begin
             if QueryPimsButtons.Eof or
               (QueryPimsButtons.FieldByName('VISIBLE_YN').AsString <>
                CHECKEDVALUE) then
             begin
               dxBarManBase.Items[ItemIndex].Enabled := False;
               dxBarManBase.Items[ItemIndex].Visible := ivNever;
             end
             else
             //car 21-10-2003 - make invisible the category with no button enable
               if dxBarManBase.Items[ItemIndex].Enabled then
                 VisibleCategory := True;
             QueryPimsButtons.Next;
          end;
          Inc(ItemIndex);
        end;
        //
        if not VisibleCategory then
          dxBarManBase.CategoryItemsVisible[CategoryIndex] := ivNever;
        // RV087.3. Do not free!
        // QueryPimsButtons.Free;
        // next visible group
        QueryPimsMenuOption.Next;
      end;
    end;// if group is visible
  end;// for groups
  // RV087.3. Do not free!
  // QueryPimsMenuOption.Free;
end;

procedure TSideMenuUnitMaintenanceF.MakeAllGroupsVisible;
var
  I: Integer;
begin
  for I := 0 to dxSideBar.GroupCount - 1 do
    dxSideBar.Groups[I].Visible := True;
end;

// CAR 15-9-2003 changed for user rights
function TSideMenuUnitMaintenanceF.MenuOptionInMenuSet(const AOption: Integer;
  const PimsMenuOption: Integer;
  QueryPimsMenu: TQuery): Boolean;
var
  PosInSet, CountPos: Integer;
  AMenuSet: TPimsMenuSet;
begin
  case PimsMenuOption of
    MENU_OPTION_ADMIN:
      AMenuSet := MENU_GROUPS_ADMIN;
    MENU_OPTION_TIMERECORDING:
      AMenuSet := MENU_GROUPS_TIMERECORDING;
    MENU_OPTION_STAFFPLANNING:
      AMenuSet := MENU_GROUPS_PLANNING;
    MENU_OPTION_REPORT:
      AMenuSet := MENU_GROUPS_REPORT;
    MENU_OPTION_PRODUCTIVITY:
      AMenuSet := MENU_GROUPS_PRODUCTIVITY;
    MENU_OPTION_PAYMENT:
      AMenuSet := MENU_GROUPS_PAYMENT;
  end;{ case }
  Result := False;
  if (AOption in AMenuSet) then
  begin
  // determine the first element AMenuSet in order to determine the position
  // of AOption inside the AMenuSet
  //car 12-12-2003 - bug
    PosInSet := AOption;
    CountPos := 0;
    while (PosInSet >=0) do
    begin
      Dec(PosInSet);
      if (PosInSet in AMenuSet) then
        Inc(CountPos);
    end;
  //  Inc(PosInSet);
    // goto the record with position determined
    QueryPimsMenu.First;
    QueryPimsMenu.MoveBy(CountPos);
    Result := Not QueryPimsMenu.Eof and
      (QueryPimsMenu.FieldByName('VISIBLE_YN').AsString = CHECKEDVALUE);
  end;
end;

// CAR 15-9-2003
procedure TSideMenuUnitMaintenanceF.UpdateSideGroups(
  const PimsMenuOption: Integer);
var
  GroupIndex: Integer;
  QueryPimsMenuOption: TQuery;
begin
 // get menu tree corespondent to PimsMenuOption
  QueryPimsMenuOption := SystemDM.GetMenuTreeOfParentMenu(
    SystemDM.GetMenuNumberOfMenuOption(PimsMenuOption),
    UNCHECKEDVALUE, CHECKEDVALUE);
  dxSideBar.OnChangeActiveGroup := nil;
  dxSideBar.IsMakingUpdate := True;
  MakeAllGroupsVisible;
  for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
    dxSideBar.Groups[GroupIndex].Visible :=
      MenuOptionInMenuSet(GroupIndex, PimsMenuOption, QueryPimsMenuOption);

  dxSideBar.IsMakingUpdate := False;
  dxSideBar.OnChangeActiveGroup := dxSideBarChangeActiveGroup;
  // RV087.3. Do not free!
  // free query
//  QueryPimsMenuOption.Free;
end;

// CAR 15-9-2003
procedure TSideMenuUnitMaintenanceF.UpdateTopMenu(
  const PimsMenuOption: Integer);
var
  ShowMenuOption: Boolean;
  CategoryIndex: Integer;
  QueryPimsMenuOption: TQuery;
begin
 // get menu tree corespondent to PimsMenuOption
  QueryPimsMenuOption := SystemDM.GetMenuTreeOfParentMenu(
    SystemDM.GetMenuNumberOfMenuOption(PimsMenuOption), UNCHECKEDVALUE,
    CHECKEDVALUE);

  dxBarManBase.LockUpdate := True;
  for CategoryIndex := 7 to dxBarManBase.Categories.Count - 1 do
  begin
    ShowMenuOption :=
      MenuOptionInMenuSet((CategoryIndex - 7), PimsMenuOption, QueryPimsMenuOption);

    if ShowMenuOption then
      dxBarManBase.CategoryItemsVisible[CategoryIndex] := ivAlways
    else
      dxBarManBase.CategoryItemsVisible[CategoryIndex] := ivNever;
  end;
  dxBarManBase.LockUpdate := False;
    // free query
  // RV087.3. Do not free!
//  QueryPimsMenuOption.Free;
end;
// END: update menu for user rights

// START forms of Unit Maintenance part
procedure TSideMenuUnitMaintenanceF.GoPSPlantExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(PlantF, MENU_ITEM_PLANT);
end;

procedure TSideMenuUnitMaintenanceF.GoBusinessUnitExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(BusinessUnitF, MENU_ITEM_BUSINESSUNIT);
end;

procedure TSideMenuUnitMaintenanceF.GoDepartmentExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(DepartmentF, MENU_ITEM_DEPARTMENT);
end;

procedure TSideMenuUnitMaintenanceF.GoTeamExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(TeamF, MENU_ITEM_TEAMS);
end;

procedure TSideMenuUnitMaintenanceF.GoTeamDeptExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(TeamDeptF, MENU_ITEM_DEPARTMENTTEAM);
end;

procedure TSideMenuUnitMaintenanceF.GoShiftExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_TIMESTRUCTURE);
  SwitchForm(ShiftF, MENU_ITEM_SHIFTS);
end;

procedure TSideMenuUnitMaintenanceF.GoTimeBlockShiftExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_TIMESTRUCTURE);
  SwitchForm(TimeBlockPerShiftF, MENU_ITEM_TIMEBLOCKSPERSHIFT);
end;

procedure TSideMenuUnitMaintenanceF.GoBreakPerShiftExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_TIMESTRUCTURE);
  SwitchForm(BreakPerShiftF, MENU_ITEM_BREAKSPERSHIFTS);
end;

procedure TSideMenuUnitMaintenanceF.GoTimeBlockPerDeptExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_TIMESTRUCTURE);
  SwitchForm(TimeBlockPerDeptF, MENU_ITEM_TIMEBLOCKSPERDEPT);
end;

procedure TSideMenuUnitMaintenanceF.GoBreakPerDeptExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_TIMESTRUCTURE);
  SwitchForm(BreakPerDeptF, MENU_ITEM_BREAKSPERDEPT);
end;

procedure TSideMenuUnitMaintenanceF.GoTimeBlockPerEmplExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_FIXEDDATA);
  SwitchForm(TimeBlockPerEmplF, MENU_ITEM_TIMEBLOCKSPEREMPLOYEE);
end;

procedure TSideMenuUnitMaintenanceF.GoBreakPerEmplExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_FIXEDDATA);
  SwitchForm(BreakPerEmplF, MENU_ITEM_BREAKSPEREMPLOYEE);
end;

procedure TSideMenuUnitMaintenanceF.GoTypeHoursExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(TypeHourF, MENU_ITEM_TYPEOFHOURS);
end;

procedure TSideMenuUnitMaintenanceF.GoIDCardExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_FIXEDDATA);
  SwitchForm(IDCardF, MENU_ITEM_IDCARDS);
end;

procedure TSideMenuUnitMaintenanceF.GoContractGroupsExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_CONTRACT);
  SwitchForm(ContractGroupF, MENU_ITEM_CONTRACTGROUPS);
end;

procedure TSideMenuUnitMaintenanceF.GoEmployeeExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_FIXEDDATA);
  SwitchForm(EmployeeF, MENU_ITEM_EMPLOYEES);
end;

procedure TSideMenuUnitMaintenanceF.GoWorkspotExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(WorkSpotF, MENU_ITEM_WORKSPOTS);
end;

procedure TSideMenuUnitMaintenanceF.GoAbsenceReasonExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(AbsReasonF, MENU_ITEM_ABSENCEREASONS);
end;

procedure TSideMenuUnitMaintenanceF.GoAbsTypeExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(AbsTypeF, MENU_ITEM_ABSENCETYPES);
end;

procedure TSideMenuUnitMaintenanceF.GoExceptionalhoursExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_CONTRACT);
  SwitchForm(ExceptHourF, MENU_ITEM_EXCEPTIONALHOURS);
end;

procedure TSideMenuUnitMaintenanceF.GoOvertimeExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_CONTRACT);
  SwitchForm(OverTimeDefF, MENU_ITEM_OVERTIME);
end;

procedure TSideMenuUnitMaintenanceF.GoWorkSpotWorkstationExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(WorkSpotPerStationF, MENU_ITEM_WORKSPOTWORKSTATION);
end;

procedure TSideMenuUnitMaintenanceF.GoWKPerEmplExecute(Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_ADMIN_FIXEDDATA, MENU_ITEM_WORKSPOTPEREMPLOYEE) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogWorkspotPerEmployeeF := TDialogWorkspotPerEmployeeF.Create(Application);
      try
        DialogWorkspotPerEmployeeF.Edit_Workspot_Planning :=
          dxSideBar.SelectedItem.CustomData;
        DialogWorkspotPerEmployeeF.ShowModal;
      finally
        DialogWorkspotPerEmployeeF.Free;
      end;
    end
    else
      SwitchForm(DialogWorkspotPerEmployeeForm, MENU_ITEM_WORKSPOTPEREMPLOYEE);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoEmplContractExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_FIXEDDATA);
  SwitchForm(EmployeeContractsF, MENU_ITEM_EMPLOYEECONTRACTS);
end;

procedure TSideMenuUnitMaintenanceF.GoConfigurationExecute(
  Sender: TObject);
var
  PassOK: Boolean;
begin
  inherited;
  try
    DialogPasswordF := TDialogPasswordF.Create(Application);
    PassOK := DialogPasswordF.VerifyPassword;
  finally
    DialogPasswordF.free;
  end;
  If PassOK then
  begin
    SwitchSideMenuGroup(GROUP_CONFIGURATION);
    SwitchForm(SettingsF, MENU_ITEM_SETTINGS);
  end;
end;
// actions for PIMS first part - END -

procedure TSideMenuUnitMaintenanceF.GoActionListChange(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
end;

procedure TSideMenuUnitMaintenanceF.FormCreate(Sender: TObject);
begin
  inherited;
  // PIM-12 To export menu-structure -> Call this with True
  ExportMenuStructure(False);
  dxSideBar.Color := clWhite; // PIM-250 // clPimsBlue; // 20014450.50
  dxSideBar.ItemFont.Color := clDarkRed; // PIM-250
//  dxSideBar.ItemFont.Color := clBlack; // 20014450.50
  NotVisibleIncentivePrg;
  NotVisibleExportPrg;
  NotVisibleReportIncentiveProgram; // 20014715
  // RV050.9.
  stBarBase.Panels.Items[2].Text := SystemDM.RegistryLogin;
end;

// Change the Side-Menu-Bar to the correct group.
procedure TSideMenuUnitMaintenanceF.dxSideBarChangeActiveGroup(Sender: TObject);
begin
  inherited;
// set correct insertform
  if InsertForm <> Nil then
    if (InsertForm is TGridBaseF) then // RV089.1.
      ActiveTables((InsertForm as TGridBaseF).GridFormDM, False);
  NilInsertForm;
  dxSideBar.SelectedItem := Nil;
end;

procedure TSideMenuUnitMaintenanceF.ShowAdminPlant;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  if SideMenuUnitMaintenanceF <> Nil then
    ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_ADMIN);
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  NilInsertForm;
end;

procedure TSideMenuUnitMaintenanceF.ShowTimeRecording;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_TIMERECORDING);
  NilInsertForm;
  SwitchSideMenuGroup(GROUP_TIMERECORDING);
end;

procedure TSideMenuUnitMaintenanceF.ShowProductivity;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_PRODUCTIVITY);
  NilInsertForm;
  SwitchSideMenuGroup(GROUP_PRODUCTIVITY);
end;

procedure TSideMenuUnitMaintenanceF.ShowReportMenu;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_REPORT);
  NilInsertForm;
  //550124
  SwitchSideMenuGroup(GROUP_REPORT_UNIT_MAINTENANCE);
end;

procedure TSideMenuUnitMaintenanceF.ShowPlanning;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_STAFFPLANNING);
  NilInsertForm;
  SwitchSideMenuGroup(GROUP_PLANNING);
end;

procedure TSideMenuUnitMaintenanceF.ShowPayment;
begin
  Hide;
  //Car 23.2.2004 restore form if it is minimized
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  SwitchToMenuOption(MENU_OPTION_PAYMENT);
  NilInsertForm;
  SwitchSideMenuGroup(GROUP_PAYROLLSYSTEM);
end;

// actions for timerecoding ph2 + ph3 - START-
procedure TSideMenuUnitMaintenanceF.GoHoursPerEmployeeExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_TIMERECORDING);
  SwitchForm(HoursPerEmployeeF, MENU_ITEM_HOURSPEREMPLOYEE);
end;

procedure TSideMenuUnitMaintenanceF.GoTimeRecScanExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_TIMERECORDING);
  SwitchForm(TimeRecScanningF, MENU_ITEM_TIMERECORDINGSCAN);
end;

procedure TSideMenuUnitMaintenanceF.GoTransferToNextYearExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_TIMERECORDING, MENU_ITEM_TRANSFER_FREE_TIME_NEXT_YEAR) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogTransferFreeTimeF := TDialogTransferFreeTimeF.Create(Application);
      try
        DialogTransferFreeTimeF.ShowModal;
      finally
        DialogTransferFreeTimeF.Free;
      end;
    end
    else
      SwitchForm(DialogTransferFreeTimeForm, MENU_ITEM_TRANSFER_FREE_TIME_NEXT_YEAR);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoCalculateEarnedWKRExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_TIMERECORDING, MENU_ITEM_CALCULATE_EARNED_WTR) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogCalculateEarnedWTRF := TDialogCalculateEarnedWTRF.Create(Application);
      try
        DialogCalculateEarnedWTRF.ShowModal;
      finally
        DialogCalculateEarnedWTRF.Free;
      end;
    end
    else
      SwitchForm(DialogCalculateEarnedWTRForm, MENU_ITEM_CALCULATE_EARNED_WTR);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoActionBankHolidaysExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_TIMERECORDING, MENU_ITEM_BANK_HOLIDAY) then
  begin
    DialogBankHolidayF := TDialogBankHolidayF.Create(Application);
    DialogBankHolidayF.Form_Edit_YN := dxSideBar.SelectedItem.CustomData; // 20011800
    try
      DialogBankHolidayF.ShowModal;
    finally
      DialogBankHolidayF.Free;
    end;
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoActionProcessAbsenceExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_TIMERECORDING, MENU_ITEM_PROCESSABSENCE) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogProcessAbsenceHrsF := TDialogProcessAbsenceHrsF.Create(Application);
      try
        DialogProcessAbsenceHrsF.ShowModal;
      finally
        DialogProcessAbsenceHrsF.Free;
      end;
    end
    else
      SwitchForm(DialogProcessAbsenceHrsForm, MENU_ITEM_PROCESSABSENCE);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoIllnessMessagesExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_TIMERECORDING);
  SwitchForm(IllnessMessagesF, MENU_ILLNESSMESSAGES);
end;

// actions for reports - START -
procedure TSideMenuUnitMaintenanceF.GoReportEmployeeExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_UNIT_MAINTENANCE, MENU_ITEM_REP_EMPLOYEE) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportEmplF := TDialogReportEmplF.Create(Application);
      try
        DialogReportEmplF.ShowModal;
      finally
        DialogReportEmplF.Free;
      end;
    end
    else
      SwitchForm(DialogReportEmplForm, MENU_ITEM_REP_EMPLOYEE);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoCheckListScansExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_CHECKLISTSCAN) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportCheckListScanF := TDialogReportCheckListScanF.Create(Application);
      try
        DialogReportCheckListScanF.ShowModal;
      finally
        DialogReportCheckListScanF.Free;
      end;
    end
    else
      SwitchForm(DialogReportCheckListScanForm, MENU_ITEM_REP_CHECKLISTSCAN);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoCompHoursExecute(Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_COMPHOURSWITHCONTRHOURS) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportCompHoursF := TDialogReportCompHoursF.Create(Application);
      try
        DialogReportCompHoursF.ShowModal;
      finally
        DialogReportCompHoursF.Free;
      end;
    end
    else
      SwitchForm(DialogReportCompHoursForm, MENU_ITEM_REP_COMPHOURSWITHCONTRHOURS);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepHoursPerEmplExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HOURSPEREMPLOYEE) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHoursPerEmplF := TDialogReportHoursPerEmplF.Create(Application);
      try
        DialogReportHoursPerEmplF.ShowModal;
      finally
        DialogReportHoursPerEmplF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHoursPerEmplForm, MENU_ITEM_REP_HOURSPEREMPLOYEE);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoWTRAbsenceReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_WTR_HOL_ILL) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportAbsenceSheduleF := TDialogReportAbsenceSheduleF.Create(Application);
      try
        DialogReportAbsenceSheduleF.ShowModal;
      finally
        DialogReportAbsenceSheduleF.Free;
      end;
    end
    else
      SwitchForm(DialogReportAbsenceSheduleForm, MENU_ITEM_REP_WTR_HOL_ILL);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoAbsenceReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_ABSENCE) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportAbsenceF := TDialogReportAbsenceF.Create(Application);
      try
        DialogReportAbsenceF.ShowModal;
      finally
        DialogReportAbsenceF.Free;
      end;
    end
    else
      SwitchForm(DialogReportAbsenceForm, MENU_ITEM_REP_ABSENCE);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepDirIndHrsWeekExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_DIRINDHRSWEEK) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportDirIndHrsWeekF := TDialogReportDirIndHrsWeekF.Create(Application);
      try
        DialogReportDirIndHrsWeekF.ShowModal;
      finally
        DialogReportDirIndHrsWeekF.Free;
      end;
    end
    else
      SwitchForm(DialogReportDirIndHrsWeekForm, MENU_ITEM_REP_DIRINDHRSWEEK);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepHrsPerWorkspotExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HRSPERWORKSPOT) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHrsPerWKF := TDialogReportHrsPerWKF.Create(Application);
      try
        DialogReportHrsPerWKF.ShowModal;
      finally
        DialogReportHrsPerWKF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHrsPerWKForm, MENU_ITEM_REP_HRSPERWORKSPOT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportRatioIllHrsExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_RATILILLNESSHRS) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportRatioIllnessHrsF := TDialogReportRatioIllnessHrsF.Create(Application);
      try
        DialogReportRatioIllnessHrsF.ShowModal;
      finally
        DialogReportRatioIllnessHrsF.Free;
      end;
    end
    else
      SwitchForm(DialogReportRatioIllnessHrsForm, MENU_ITEM_REP_RATILILLNESSHRS);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportAvailableUsedTimeExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_AVAILABLEUSEDTIME) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportAvailableUsedTimeF := TDialogReportAvailableUsedTimeF.Create(Application);
      try
        DialogReportAvailableUsedTimeF.ShowModal;
      finally
        DialogReportAvailableUsedTimeF.Free;
      end
    end
    else
      SwitchForm(DialogReportAvailableUsedTimeForm, MENU_ITEM_REP_AVAILABLEUSEDTIME);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepHrsPerWKCumExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HRSPERWORKSPOTCUM) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHrsPerWKCUMF := TDialogReportHrsPerWKCumF.Create(Application);
      try
        DialogReportHrsPerWKCUMF.ShowModal;
      finally
        DialogReportHrsPerWKCUMF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHrsPerWKCUMForm, MENU_ITEM_REP_HRSPERWORKSPOTCUM);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepHrsPerContractGrpCumExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HRSPERCONTRACTGRPCUM) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHrsPerContractGrpCUMF := TDialogReportHrsPerContractGrpCumF.Create(Application);
      try
        DialogReportHrsPerContractGrpCUMF.ShowModal;
      finally
        DialogReportHrsPerContractGrpCUMF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHrsPerContractGrpCUMForm, MENU_ITEM_REP_HRSPERCONTRACTGRPCUM);
  end;
end;

// 93.6.
procedure TSideMenuUnitMaintenanceF.GoReportHolidayCardExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HOLIDAYCARD) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHolidayCardF := TDialogReportHolidayCardF.Create(Application);
      try
        DialogReportHolidayCardF.ShowModal;
      finally
        DialogReportHolidayCardF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHolidayCardForm, MENU_ITEM_REP_HOLIDAYCARD);
  end;
end;

// actions for reports - END-

// actions for staff planing - START -
procedure TSideMenuUnitMaintenanceF.GoStandardOccupationExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchForm(StandardOccupationF, MENU_STANDARD_OCCUPATION);
end;

procedure TSideMenuUnitMaintenanceF.GoStdStaffAvailabilityExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchForm(StandStaffAvailF, MENU_STANDARD_STAFF_AVAILABILITY);
end;

procedure TSideMenuUnitMaintenanceF.GoShiftScheduleExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchForm(ShiftScheduleF, MENU_SHIFT_SCHEDULE);
end;

procedure TSideMenuUnitMaintenanceF.GoStaffAvailabilityExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchStaffAvailForm('', '', -1, 0);
end;

procedure TSideMenuUnitMaintenanceF.GoEmplAvailabilityExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchEmplAvailForm(0, 0, 0);
end;

procedure TSideMenuUnitMaintenanceF.GoStaffPlanningActionExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_PLANNING, MENU_STAFF_PLANNING) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogStaffPlanningF := TDialogStaffPlanningF.Create(Application);
      try
        DialogStaffPlanningF.Edit_Staff_Planning :=
          dxSideBar.SelectedItem.CustomData;
        DialogStaffPlanningF.ShowModal;
      finally
        DialogStaffPlanningF.Free;
      end;
    end
    else
      SwitchForm(DialogStaffPlanningForm, MENU_STAFF_PLANNING);
  end;
end;
// actions for staff planing - END -

// reports staff planning  - START
procedure TSideMenuUnitMaintenanceF.GoReportStaffAvaillabilityExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PLANNING, MENU_ITEM_REP_STAFF_AVAIL_EMP) then
  begin
    // MR:17-02-2005 Order 550378 (TeamCode -> TeamFrom, TeamTo) in Create()
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportStaffAvailabilityF :=
        TDialogReportStaffAvailabilityF.Create(Application, '','','',-1, 0);
      try
        DialogReportStaffAvailabilityF.ShowModal;
      finally
        DialogReportStaffAvailabilityF.Free;
      end;
    end
    else
      SwitchForm(DialogReportStaffAvailabilityForm, MENU_ITEM_REP_STAFF_AVAIL_EMP);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportEMPAvailabilityExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PLANNING, MENU_ITEM_REP_EMP_AVAIL) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportEmpAvailabilityF := TDialogReportEmpAvailabilityF.Create(Application);
      try
        DialogReportEmpAvailabilityF.ShowModal;
      finally
        DialogReportEmpAvailabilityF.Free;
      end;
    end
    else
      SwitchForm(DialogReportEmpAvailabilityForm, MENU_ITEM_REP_EMP_AVAIL);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportStaffPlanningExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PLANNING, MENU_ITEM_REP_STAFF_PLANNING) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportStaffPlanningF := TDialogReportStaffPlanningF.Create(Application);
      try
        DialogReportStaffPlanningF.ShowModal;
      finally
        DialogReportStaffPlanningF.Free;
      end;
    end
    else
      SwitchForm(DialogReportStaffPlanningForm, MENU_ITEM_REP_STAFF_PLANNING);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportStaffPlanningCondExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PLANNING, MENU_ITEM_REP_STAFF_PLANNING_COND) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportStaffPlanningCondF := TDialogReportStaffPlanningCondF.Create(Application);
      try
        DialogReportStaffPlanningCondF.ShowModal;
      finally
        DialogReportStaffPlanningCondF.Free;
      end;
    end
    else
      SwitchForm(DialogReportStaffPlanningCondForm, MENU_ITEM_REP_STAFF_PLANNING_COND);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportStaffPlanDayExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PLANNING, MENU_ITEM_REP_STAFF_PLAN_DAY) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportStaffPlanDayF := TDialogReportStaffPlanDayF.Create(Application);
      try
        DialogReportStaffPlanDayF.ShowModal;
      finally
        DialogReportStaffPlanDayF.Free;
      end;
    end
    else
      SwitchForm(DialogReportStaffPlanDayForm, MENU_ITEM_REP_STAFF_PLAN_DAY);
  end;
end;
// reports staff planning  - END

// actions for pims
procedure TSideMenuUnitMaintenanceF.GoQualityConfExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(QualityIncentiveConfF, MENU_ITEM_QUALITYCONF);
end;

procedure TSideMenuUnitMaintenanceF.GoQualityIncentiveCalcExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_QUALITY_INCENTIVE);
  SwitchDialogReportQuality('','', -1, 0);
end;

procedure TSideMenuUnitMaintenanceF.GoManualDataCollectionExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PRODUCTIVITY);
  SwitchForm(DataCollectionEntryF, MENU_ITEM_DATA_COLLECTION_ENTRY);
end;

procedure TSideMenuUnitMaintenanceF.GoExportPayrollExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_PAYROLLSYSTEM, MENU_ITEM_EXPORTPAYROLL) then
  begin
    // Show as dialog
    if not SystemDM.EmbedDialogs then
    begin
      DialogExportPayrollF := TDialogExportPayrollF.Create(Application);
      try
        DialogExportPayrollF.ShowModal;
      finally
        DialogExportPayrollF.Free;
      end;
    end
    else // Show embedded
      SwitchForm(DialogExportPayrollForm, MENU_ITEM_EXPORTPAYROLL);
  end;
end;

// reports productivity
procedure TSideMenuUnitMaintenanceF.GoProductionReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_PRODUCTIONREPORT) then
  begin
    // Show as dialog
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportProductionF := TDialogReportProductionF.Create(Application);
      try
        DialogReportProductionF.ShowModal;
      finally
        DialogReportProductionF.Free;
      end;
    end
    else // Show embedded
      SwitchForm(DialogReportProductionForm, MENU_ITEM_PRODUCTIONREPORT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoProductionDetailReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_PRODUCTIONDETAILREPORT) then
  begin
    // Show as dialog
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportProductionDetailF := TDialogReportProductionDetailF.Create(Application);
      try
        DialogReportProductionDetailF.ShowModal;
      finally
        DialogReportProductionDetailF.Free;
      end;
    end
    else // Show embedded
      SwitchForm(DialogReportProductionDetailForm, MENU_ITEM_PRODUCTIONDETAILREPORT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoTeamIncentiveRepExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_TEAMINCENTIVEREPORT) then
  begin
    // Show as dialog
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportTeamIncentiveF := TDialogReportTeamIncentiveF.Create(Application);
      try
        DialogReportTeamIncentiveF.ShowModal;
      finally
        DialogReportTeamIncentiveF.Free;
      end
    end
    else // Show embedded
      SwitchForm(DialogReportTeamIncentiveForm, MENU_ITEM_TEAMINCENTIVEREPORT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoColConnectionExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(DataColConnectionF, MENU_ITEM_DATA_COL_CONNECTION);
end;

procedure TSideMenuUnitMaintenanceF.GoRevenueExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_SALES);
  SwitchForm(RevenueF, MENU_ITEM_REVENUE);
end;

procedure TSideMenuUnitMaintenanceF.GoMistakePerEmplExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_QUALITY_INCENTIVE);
  SwitchForm(MistakePerEmplF, MENU_ITEM_MISTAKESPEREMPL);
end;

procedure TSideMenuUnitMaintenanceF.GoPaymentExpCodeExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PAYROLLSYSTEM);
  SwitchForm(PaymentExportCodeF, MENU_PAYMENT_EXP_CODE);
end;

procedure TSideMenuUnitMaintenanceF.GoExtraPaymentExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PAYROLLSYSTEM);
  SwitchForm(ExtraPaymentF, MENU_EXTRA_PAYMENT);
end;

procedure TSideMenuUnitMaintenanceF.GoComportConnectionExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(ComPortConnectionF, MENU_ITEM_COMPORT_CONNECTION);
end;

procedure TSideMenuUnitMaintenanceF.GoReportAbsenceCardExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_ABSENCECARD) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportAbsenceCardF := TDialogReportAbsenceCardF.Create(Application);
      try
        DialogReportAbsenceCardF.ShowModal;
      finally
        DialogReportAbsenceCardF.Free;
      end;
    end
    else
      SwitchForm(DialogReportAbsenceCardForm, MENU_ITEM_REP_ABSENCECARD);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoProductionCompReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_PRODUCTIONCOMPREPORT) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportProductionCompF := TDialogReportProductionCompF.Create(Application);
      try
        DialogReportProductionCompF.ShowModal;
      finally
        DialogReportProductionCompF.Free;
      end;
    end
    else
      SwitchForm(DialogReportProductionCompForm, MENU_ITEM_PRODUCTIONCOMPREPORT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoRepHrsPerDayExecute(Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_HOURS_PER_DAY) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportHrsPerDayF := TDialogReportHrsPerDayF.Create(Application);
      try
        DialogReportHrsPerDayF.ShowModal;
      finally
        DialogReportHrsPerDayF.Free;
      end;
    end
    else
      SwitchForm(DialogReportHrsPerDayForm, MENU_ITEM_REP_HOURS_PER_DAY);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoProductionCompTargetReportExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_PRODCOMPTARGETREPORT) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportProdCompTargetF := TDialogReportProdCompTargetF.Create(Application);
      try
        DialogReportProdCompTargetF.ShowModal;
      finally
        DialogReportProdCompTargetF.Free;
      end;
    end
    else
      SwitchForm(DialogReportProdCompTargetForm, MENU_ITEM_PRODCOMPTARGETREPORT);
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoEmployeeRegistrationsExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REGISTRATIONS, MENU_ITEM_EMPLOYEE_REGISTRATIONS) then
  begin
    EmployeeRegsDM := TEmployeeRegsDM.Create(Application);
    EmployeeRegsF := TEmployeeRegsF.Create(Application);
    try
      EmployeeRegsF.ShowModal;
    finally
      EmployeeRegsF.Free;
      EmployeeRegsDM.Free;
    end;
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoReportEmployeePaylistExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PAYMENTS, MENU_ITEM_REP_EMPLOYEE_PAYLIST) then
  begin
    // RV089.1.
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportEmployeePaylistF :=
        TDialogReportEmployeePaylistF.Create(Application);
      try
        DialogReportEmployeePaylistF.ShowModal;
      finally
        DialogReportEmployeePaylistF.Free;
      end;
    end
    else
      SwitchForm(DialogReportEmployeePaylistForm, MENU_ITEM_REP_EMPLOYEE_PAYLIST);
  end;
end;


procedure TSideMenuUnitMaintenanceF.FormShow(Sender: TObject);
begin
  inherited;
  //CAR 6-2-2004 remove from the task bar pims application -
  // only one active button will be in taskbar for pims
  ShowWindow(Application.Handle, SW_HIDE);
end;

procedure TSideMenuUnitMaintenanceF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  //CAR 6-4-2004 restore application button in task bar
  ShowWindow(Application.Handle, SW_RESTORE);
  //free insertform
  NilInsertForm;
end;

procedure TSideMenuUnitMaintenanceF.GoRepPlantStructureExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_UNIT_MAINTENANCE, MENU_ITEM_REP_PLANT_STRUCTURE) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportPlantStructureF :=
        TDialogReportPlantStructureF.Create(Application);
      try
        DialogReportPlantStructureF.ShowModal;
      finally
        DialogReportPlantStructureF.Free;
      end;
    end
    else
      SwitchForm(DialogReportPlantStructureForm, MENU_ITEM_REP_PLANT_STRUCTURE);
  end;
end;

//Car 6-5-2004

// MR:05-10-2004
procedure TSideMenuUnitMaintenanceF.GoMonthlyGroupEffiencyExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_PAYROLLSYSTEM, MENU_ITEM_MONTHLYGROUPEFFICIENCY) then
  begin
    // MR:12-03-2007 Order 550443 Show only one dialog
    if SystemDM.GetMonthGroupEfficiency then
    begin
      if not SystemDM.EmbedDialogs then
      begin
        DialogMonthlyEmployeeEfficiencyF :=
          TDialogMonthlyEmployeeEfficiencyF.Create(Application);
        try
          DialogMonthlyEmployeeEfficiencyF.ShowModal;
        finally
          DialogMonthlyEmployeeEfficiencyF.Free;
        end;
      end
      else
        SwitchForm(DialogMonthlyEmployeeEfficiencyForm, MENU_ITEM_MONTHLYGROUPEFFICIENCY);
    end;
{
    // If 'N' then the set caption to 'Monthly Efficiency'
    // Else set it to caption 'Monthly Group Efficiency'
    if (SystemDM.TableExportPayroll.
      FieldByName('MONTH_GROUP_EFFICIENCY_YN').AsString = 'N') then
    begin
      DialogMonthlyEmployeeEfficiencyF :=
        TDialogMonthlyEmployeeEfficiencyF.Create(Application);
      try
        DialogMonthlyEmployeeEfficiencyF.ShowModal;
      finally
        DialogMonthlyEmployeeEfficiencyF.Free;
      end;
    end
    else
    begin
      DialogMonthlyGroupEfficiencyF :=
        TDialogMonthlyGroupEfficiencyF.Create(Application);
      try
        DialogMonthlyGroupEfficiencyF.ShowModal;
      finally
        DialogMonthlyGroupEfficiencyF.Free;
      end;
    end;
}
  end;
end;

procedure TSideMenuUnitMaintenanceF.GoMachineExecute(Sender: TObject);
begin
  SwitchSideMenuGroup(GROUP_ADMIN_PLANT);
  SwitchForm(MachineF, MENU_ITEM_MACHINE);
end;

//RV067.4.
procedure TSideMenuUnitMaintenanceF.GoCountriesExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(CountriesF, MENU_ITEM_COUNTRIES);
end;

procedure TSideMenuUnitMaintenanceF.GoAbsenceTypesPerCountryExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(AbsTypePerCountryF, MENU_ITEM_ABSENCETYPESPERCTR);
end;

procedure TSideMenuUnitMaintenanceF.GoAbsenceReasonsPerCountryExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(AbsReasonPerCountryF, MENU_ITEM_ABSENCEREASONSPERCTR);
end;

procedure TSideMenuUnitMaintenanceF.GoHourTypesPerCountryExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(TypeHourPerCountryF, MENU_ITEM_TYPEOFHOURSPERCTR);
end;

function TSideMenuUnitMaintenanceF.GetLastDxSideBarItem: TdxSideBarItem;
begin
  Result := FLastDxSideBarItem;
end;

procedure TSideMenuUnitMaintenanceF.SetLastDxSideBarItem(
  const Value: TdxSideBarItem);
begin
  FLastDxSideBarItem := Value;
end;

// 20014037
procedure TSideMenuUnitMaintenanceF.GoReportChangesPerScanExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_CHANGESPERSCAN) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportChangesPerScanF := TDialogReportChangesPerScanF.Create(Application);
      try
        DialogReportChangesPerScanF.ShowModal;
      finally
        DialogReportChangesPerScanF.Free;
      end;
    end
    else
      SwitchForm(DialogReportChangesPerScanForm, MENU_ITEM_REP_CHANGESPERSCAN);
  end;
end;

// 20014137
procedure TSideMenuUnitMaintenanceF.GoReportEmployeeIllnessExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_EMPLOYEEILLNESS) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportEmployeeIllnessF := TDialogReportEmployeeIllnessF.Create(Application);
      try
        DialogReportEmployeeIllnessF.ShowModal;
      finally
        DialogReportEmployeeIllnessF.Free;
      end;
    end
    else
      SwitchForm(DialogReportEmployeeIllnessForm, MENU_ITEM_REP_EMPLOYEEILLNESS);
  end;
end;

// 20014715
procedure TSideMenuUnitMaintenanceF.GoReportIncentiveProgramExecute(
  Sender: TObject);
begin
  if SwitchDialog(GROUP_REPORT, MENU_ITEM_REP_INCENTIVEPROGRAM) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportIncentiveProgramF := TDialogReportIncentiveProgramF.Create(Application);
      try
        DialogReportIncentiveProgramF.ShowModal;
      finally
        DialogReportIncentiveProgramF.Free;
      end;
    end
    else
      SwitchForm(DialogReportIncentiveProgramForm, MENU_ITEM_REP_INCENTIVEPROGRAM);
  end;
end;

// 20014715
procedure TSideMenuUnitMaintenanceF.NotVisibleReportIncentiveProgram;
begin
  if not SystemDM.IsWMU then
  begin
    SetInVisible(GROUP_REPORT, MENU_ITEM_REP_INCENTIVEPROGRAM);
    dxBarButtonRepIncentiveProgram.Visible := ivNever;
  end;
end;

// 20014826
procedure TSideMenuUnitMaintenanceF.GoErrorLogExecute(Sender: TObject);
begin
  SwitchSideMenuGroup(GROUP_CONFIGURATION);
  SwitchForm(ErrorLogF, MENU_ITEM_ERRORLOG);
end;

// PIM-50
procedure TSideMenuUnitMaintenanceF.GoReportJobCommentsExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_UNIT_MAINTENANCE, MENU_ITEM_REP_JOBCOMMENTS) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportJobCommentsF := TDialogReportJobCommentsF.Create(Application);
      try
        DialogReportJobCommentsF.ShowModal;
      finally
        DialogReportJobCommentsF.Free;
      end;
    end
    else
      SwitchForm(DialogReportJobCommentsForm, MENU_ITEM_REP_JOBCOMMENTS);
  end;
end;

// PIM-52
procedure TSideMenuUnitMaintenanceF.GoWorkScheduleActionExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchForm(WorkScheduleF, MENU_WORK_SCHEDULE);
end;

// PIM-52
procedure TSideMenuUnitMaintenanceF.GoWorkScheduleDetailsActionExecute(
  Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_PLANNING);
  SwitchForm(WorkScheduleDetailsF, MENU_WORK_SCHEDULE_DETAILS);
end;

// PIM-23
procedure TSideMenuUnitMaintenanceF.GoReportEmpInfoDevPeriodExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_UNIT_MAINTENANCE, MENU_ITEM_REP_EMPINFODEVPERIOD) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportEmpInfoDevPeriodF := TDialogReportEmpInfoDevPeriodF.Create(Application);
      try
        DialogReportEmpInfoDevPeriodF.ShowModal;
      finally
        DialogReportEmpInfoDevPeriodF.Free;
      end;
    end
    else
      SwitchForm(DialogReportEmpInfoDevPeriodForm, MENU_ITEM_REP_EMPINFODEVPERIOD);
  end;
end;

// PIM-12 Help function for translation:
// Export the whole menu-structure to a file.
procedure TSideMenuUnitMaintenanceF.ExportMenuStructure(AExport: Boolean);
var
  GroupIndex, ButtonIndex: Integer;
  MyList: TStringList;
begin
  if AExport then
  begin
    MyList := TStringList.Create;
    try
      for GroupIndex := 0 to dxSideBar.GroupCount - 1 do
        for ButtonIndex := 0 to dxSideBar.Groups[GroupIndex].ItemCount - 1 do
          MyList.Add(IntToStr(GroupIndex) + ';' + IntToStr(ButtonIndex) + ';' +
            dxSideBar.Groups[GroupIndex].Items[ButtonIndex].Caption);
    finally
      MyList.SaveToFile('c:\temp\SideMenuUnitMaintenance.txt');
      MyList.Free;
    end;
  end;
end; // ExportMenuStructure;

// PIM-142
procedure TSideMenuUnitMaintenanceF.GoRealTimeEffMonitorExecute(
  Sender: TObject);
begin
  SwitchSideMenuGroup(GROUP_PRODUCTIVITY);
  SwitchForm(RealTimeEffMonitorF, MENU_ITEM_REAL_TIME_EFF_MONITOR);
end;

// PIM-152
procedure TSideMenuUnitMaintenanceF.GoReportGhostCountsExecute(
  Sender: TObject);
begin
  inherited;
  if SwitchDialog(GROUP_REPORT_PRODUCTIVITY, MENU_ITEM_REPORTGHOSTCOUNTS) then
  begin
    // Show as dialog
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportGhostCountsF := TDialogReportGhostCountsF.Create(Application);
      try
        DialogReportGhostCountsF.ShowModal;
      finally
        DialogReportGhostCountsF.Free;
      end;
    end
    else // Show embedded
      SwitchForm(DialogReportGhostCountsForm, MENU_ITEM_REPORTGHOSTCOUNTS);
  end;
end;

// GLOB3-83
procedure TSideMenuUnitMaintenanceF.GoReportInOutAbsenceOvertimeExecute(
  Sender: TObject);
begin
  if SwitchDialog(GROUP_REPORT_UNIT_MAINTENANCE, MENU_ITEM_REP_INOUTABSOVERTIME) then
  begin
    if not SystemDM.EmbedDialogs then
    begin
      DialogReportInOutAbsenceOvertimeF := TDialogReportInOutAbsenceOvertimeF.Create(Application);
      try
        DialogReportInOutAbsenceOvertimeF.ShowModal;
      finally
        DialogReportInOutAbsenceOvertimeF.Free;
      end;
    end
    else
      SwitchForm(DialogReportInOutAbsenceOvertimeForm, MENU_ITEM_REP_INOUTABSOVERTIME);
  end;
end; // GoReportInOutAbsenceOvertimeExecute

// GLOB3-204
procedure TSideMenuUnitMaintenanceF.GoPTODefExecute(Sender: TObject);
begin
  inherited;
  SwitchSideMenuGroup(GROUP_ADMIN_CONTRACT);
  SwitchForm(PTODefF, MENU_ITEM_PTODEF);
end; // GoPTODefExecute

end.
