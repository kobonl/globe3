inherited PlantFinalRunF: TPlantFinalRunF
  Left = 480
  Top = 258
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Final Run Info'
  ClientHeight = 227
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Height = 5
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 1
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 0
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 127
    Height = 100
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 98
      Align = alClient
      Caption = 'Final Run Info'
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 19
        Width = 50
        Height = 13
        Caption = 'Plant code'
      end
      object Label9: TLabel
        Left = 8
        Top = 43
        Width = 59
        Height = 13
        Caption = 'Export Type'
      end
      object Label1: TLabel
        Left = 8
        Top = 67
        Width = 58
        Height = 13
        Caption = 'Export Date'
      end
      object DBEditPlant: TDBEdit
        Tag = 1
        Left = 86
        Top = 15
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = PlantDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditPlantDesc: TDBEdit
        Tag = 1
        Left = 150
        Top = 15
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = PlantDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditET: TDBEdit
        Tag = 1
        Left = 86
        Top = 41
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'EXPORT_TYPE'
        DataSource = PlantDM.DataSourceFinalRun
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
      end
      object dxDBDateEditExportDate: TdxDBDateEdit
        Tag = 1
        Left = 86
        Top = 66
        Width = 108
        TabOrder = 3
        DataField = 'EXPORT_DATE'
        DataSource = PlantDM.DataSourceFinalRun
        DateButtons = [btnToday]
        DateValidation = True
        SaveTime = False
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 31
    Height = 96
    inherited spltDetail: TSplitter
      Top = 92
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 91
      KeyField = 'PLANT_CODE'
      DataSource = PlantDM.DataSourceFinalRun
      object dxDetailGridPLANT_CODE: TdxDBGridMaskColumn
        Caption = 'Plant'
        ReadOnly = True
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxDetailGridPLANTLU: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDetailGridEXPORT_TYPE: TdxDBGridMaskColumn
        Caption = 'Export Type'
        ReadOnly = True
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_TYPE'
      end
      object dxDetailGridEXPORT_DATE: TdxDBGridDateColumn
        Caption = 'Export Date'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_DATE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
    end
  end
  inherited dsrcActive: TDataSource
    DataSet = PlantDM.TableFinalRun
  end
end
