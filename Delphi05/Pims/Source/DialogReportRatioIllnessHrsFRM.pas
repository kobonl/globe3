(*
  Changes:
    MR:08-10-2004 Use datefrom-dateto instead of
    year, weekfrom and weekto. Order 550343.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
*)

unit DialogReportRatioIllnessHrsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportRatioIllnessHrsF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    CheckBoxExport: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    DateFrom: TDateTimePicker;
    Label9: TLabel;
    DateTo: TDateTimePicker;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DateFromCloseUp(Sender: TObject);
    procedure DateToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportRatioIllnessHrsF: TDialogReportRatioIllnessHrsF;

// RV089.1.
function DialogReportRatioIllnessHrsForm: TDialogReportRatioIllnessHrsF;

implementation

{$R *.DFM}
uses
  ListProcsFRM,
  ReportRatioIllnessHrsQRPT, ReportRatioIllnessHrsDMT, UPimsMessageRes;

var
  DialogReportRatioIllnessHrsF_HND: TDialogReportRatioIllnessHrsF;

// RV089.1.
function DialogReportRatioIllnessHrsForm: TDialogReportRatioIllnessHrsF;
begin
  if (DialogReportRatioIllnessHrsF_HND = nil) then
  begin
    DialogReportRatioIllnessHrsF_HND := TDialogReportRatioIllnessHrsF.Create(Application);
    with DialogReportRatioIllnessHrsF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportRatioIllnessHrsF_HND;
end;

// RV089.1.
procedure TDialogReportRatioIllnessHrsF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportRatioIllnessHrsF_HND <> nil) then
  begin
    DialogReportRatioIllnessHrsF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportRatioIllnessHrsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportRatioIllnessHrsF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportRatioIllnessHrsQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    // MR:13-07-2004 Order 550322
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    //550284
    IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
    Trunc(DateFrom.DateTime),
    Trunc(DateTo.DateTime),
    CheckBoxShowSelection.Checked,
    CheckBoxExport.Checked)
  then
    ReportRatioIllnessHrsQR.ProcessRecords;
end;

procedure TDialogReportRatioIllnessHrsF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, False, True, False, False, False);
  inherited;
  ListProcsF.WeekUitDat(Now, Year, Week);
  DateFrom.DateTime := ListProcsF.DateFromWeek(Year, Week, 1);
  DateTo.DateTime := ListProcsF.DateFromWeek(Year, Week, 7);
  CheckBoxShowSelection.Checked := True;
end;

procedure TDialogReportRatioIllnessHrsF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportRatioIllnessHrsDM := CreateReportDM(TReportRatioIllnessHrsDM);
  ReportRatioIllnessHrsQR := CreateReportQR(TReportRatioIllnessHrsQR);
end;

procedure TDialogReportRatioIllnessHrsF.DateFromCloseUp(Sender: TObject);
begin
  inherited;
  if DateFrom.DateTime > DateTo.DateTime then
    DateTo.DateTime := DateFrom.DateTime;
end;

procedure TDialogReportRatioIllnessHrsF.DateToCloseUp(Sender: TObject);
begin
  inherited;
  if DateTo.DateTime < DateFrom.DateTime then
    DateFrom.DateTime := DateTo.DateTime;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportRatioIllnessHrsF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportRatioIllnessHrsF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
