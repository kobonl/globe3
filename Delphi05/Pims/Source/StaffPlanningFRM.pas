(*
  Changes:
    MRA:7-JAN-2010 RV050.2. 889951.
    - Problem when using CTRL-ALT.
    Examples:
    1. When there are 4 timeblocks and blocks 3+4 were already
       planned. When using CTRL-ALT, it does not plan block 1,
       only 2, as a result blocks 2,3,4 are planned.
    2. When there are only 2 timeblocks and block 2 is already
       planned, when using CTR-ALT, it does not plan block 1.
       As a result only block 2 is planned.
    Cause: Variable ValStr was used for current block, but was
           overwritten when multiple blocks were determined.
    - Also: Made a procedure for part of the code to prevent multiple
            code in 2 places.
  SO:19-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
  MRA:11-NOV-2010 RV079.3. Optimizing.
  - When sorting is on name, it appears to be slower than
    sorting on number.
  MRA:3-DEC-2010 RV082.7.
  - Staff Planning Dialog
    - It does not refresh correct since it was optimized for
      sorting on name.
    - Reason: Refresh is wrong.
  MRA:17-DEC-2010 RV083.2. SR-890041 SO-550497.
  - Staff Planning Dialog
    - This does not filter on absence reasons-per-country.
      For pop-up in this dialog, all absence reasons are shown.
      But it must only show absence reasons for the country
      of the selected plant.
    - Remark: This was implemented already: RV067.4. 550497.
              But this did not work!
  MRA:21-JAN-2011 RV085.10. Bugfix. SC-50016634.
  - Staff Planning Dialog
    - When the planning is shown this is very slow with selection
      'sort on name'.
    - MRA: Is there an index on description-field on the table
           is sorted: Yes. Which table is sorted? PIVOT-table: Yes.
  MRA:9-MAR-2018 GLOB3-84
  - Dialog Staff Planning and Totals
  - Show totals at bottom-right-part for occupied and planned.
  MRA:9-APR-2018 GLOB3-84 Rework
  - Dialog Staff Planning and Totals
  - It calculated the totals only for what was (workspots) shown in the grids,
    when scrolled (to left/right) it recalculated it again.
  - It now calculates it via a query.
  MRA:16-APR-2018 GLOB3-84 Rework
  - Dialog Staff Planning and Totals
  - Only use this functionality for NTS (SystemDM.IsNTS).
  MRA:25-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - Changed color for LightGreen:
    - This color is not really used, it uses images for this. See
      ImgListBase. This has fixed images with fixed colors, which
      are the buttons you can click in the grid.
  MRA:12-FEB-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
  MRA:15-FEB-2019 GLOB3-223
  - Bugfix: Because NTS-functionality was made before extension 4 to 10
    timeblocks, it gave an access-violation, because an array was used
    for max. 4 positions! (TotalArray).
*)

unit StaffPlanningFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PimsFRM, DBCGrids, StdCtrls, ExtCtrls, Grids, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, dxDBTLCl, dxGrClms, ImgList, DB, Menus, StaffPlanningUnit,
  UPimsConst;
                                            
type
  TStaffPlanningF = class(TPimsF)
    PanelOCI: TPanel;
    Panel2: TPanel;
    dxDBGridPlanning: TdxDBGrid;
    ImgListBase: TImageList;
    dxDBGridOCI: TdxDBGrid;
    PopupMenuAbsRsn: TPopupMenu;
    MainMenu: TMainMenu;
    Help1: TMenuItem;
    File1: TMenuItem;
    Save1: TMenuItem;
    ExitMenu: TMenuItem;
    LegendMenu: TMenuItem;
    PopupMenuForm: TPopupMenu;
    Tobeplanned1: TMenuItem;
    Planned1: TMenuItem;
    Plannedspecialtimeblocks1: TMenuItem;
    ScrollBarGrid: TScrollBar;
    imgBandTexture: TImage;
    procedure dxDBGridPlanningCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure dxDBGridPlanningEditing(Sender: TObject;
      Node: TdxTreeListNode; var Allow: Boolean);
    procedure dxDBGridPlanningClick(Sender: TObject);
    procedure dxDBGridOCICustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dxDBGridOCIEditing(Sender: TObject; Node: TdxTreeListNode;
      var Allow: Boolean);
    procedure dxDBGridPlanningMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure PopupMenuAbsRsnPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Save1Click(Sender: TObject);
    procedure ExitMenuClick(Sender: TObject);
    procedure LegendMenuClick(Sender: TObject);
    procedure ScrollBarGridScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: Integer);
    procedure dxDBGridPlanningCustomDrawBand(Sender: TObject;
      ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxDBGridPlanningMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FCaption: String;
    FEdit_Staff_Planning: String;
   // FCtrl, FAlt: Boolean;
    FNotClick: Boolean;
    // number of columns display depending on screen resolution
    FScreenShowColumn: Integer;
    FShowTotals: Boolean;
    procedure PopupItemClick(Sender:TObject);
{    function WKWidth: Integer; }
    function TBWidth: Integer;
    function MaxTB: Integer;
    function EmpWidth: Integer;
    procedure DrawCaption;
    procedure SetShowTotals(const Value: Boolean);
  public
    { Public declarations }
    FCtrl, FAlt: Boolean;
    FColumnName: String;
    FColIndex: Integer;
    FShowAbsenceReason, FEditing: Boolean;
    TotalArray: array[0..MAX_TBS] of Integer; // GLOB3-84 // GLOB3-223
    procedure BuildGridPlanning;
    procedure BuilGridOccInd;
    procedure NotPlannedEMP(Empl, ValColor, ColIndex: Integer;
      ValStr, ColumnStr: String);
    procedure PlannedEMP(Empl, ValColor, ColIndex: Integer;
      ValStr, ColumnStr: String);
    procedure ScrollGrid(ScrollPos: Integer);
    procedure ChangesForUseFAlt(WkCode, ColumnStr: String; AColor, ColIndex: Integer);
    procedure CreateFixColumn(IndexCol, IndexBand: Integer; dxGridTMP: TdxDBGrid);
    procedure SetToInvisibleRedButton(Empl, ColIndex: Integer);
    //CAR: User rights 27-10-2003
    property Edit_Staff_Planning: String read FEdit_Staff_Planning
       write FEdit_Staff_Planning;
    procedure BuildPopupMenu(AEmployeeNumber: Integer);
    property ShowTotals: Boolean read FShowTotals write SetShowTotals;
  end;

var
  StaffPlanningF: TStaffPlanningF;

implementation

{$R *.DFM}

uses
  SystemDMT, StaffPlanningEMPDMT, StaffPlanningOCIDMT, UPimsMessageRes,
  StaffPlanningLegendFRM, DialogStaffPlanningDMT, UGlobalFunctions;

procedure TStaffPlanningF.BuilGridOccInd;
var
  i, j, k, RecCount: Integer;
  Widthband : Integer;
begin
  dxDBGridOCI.Bands[0].Fixed := bfLeft;

  Widthband := FDescriptionWidth + EmpWidth;
  dxDBGridOCI.Bands[0].Width := Widthband;
  CreateColumnOfBand(dxDBGridOCI, 0, 0, Widthband, '', 'OCI_LEVEL', 0);
  i:= 1; j:= 1;
  for RecCount := 0 to FShowColumn - 1 do
  begin
    CreateBand(dxDBGridOCI, i, (MaxTB * TBWidth){80}, getWKCode(FWKList.Strings[RecCount], RecCount)
      +  CHR_SEP +  Copy(GetDescWK(FWKDescList.Strings[RecCount]), 0, 20));
    for k := j to j + (MaxTB-1) {3} do
      CreateColumnOfBand(dxDBGridOCI, k, i, TBWidth, '',
      'WK' + IntToStr(RecCount),(k-j +1) );
    if (i < FShowColumn ) then
    begin
      CreateFixColumn(j + MaxTB {4}, I, dxDBGridOCI);
      j := j + (MaxTB+1) {5};
    end
    else
      j:=j + MaxTB{4};
    inc(i);
  end;
  // GLOB3-84 Create 4 columns for totals.
//  CreateBand(dxDBGridOCI, i, 80, '');
//  CreateColumnOfBand(dxDBGridOCI, j, i, 80, '', '', 0);
  CreateBand(dxDBGridOCI, i, (MaxTB * TBWidth){80}, '');
  for k := j to j + (MaxTB-1) {3} do
    CreateColumnOfBand(dxDBGridOCI, k, i, TBWidth, '', '', (k-j +1) );

  dxDBGridOCI.Bands[i].Fixed := bfRight;
end; // BuilGridOccInd

procedure TStaffPlanningF.CreateFixColumn(IndexCol, IndexBand: Integer;
  dxGridTMP: TdxDBGrid);
begin
  dxGridTMP.Columns[IndexCol] := dxGridTMP.CreateColumn(TdxDBGridColumn);
  dxGridTMP.Columns[IndexCol].Caption := '';
  dxGridTMP.Columns[IndexCol].BandIndex := IndexBand;
  dxGridTMP.Columns[IndexCol].MinWidth := 2;
  dxGridTMP.Columns[IndexCol].Width := 2;
  dxGridTMP.Columns[IndexCol].Sizing := False;
  dxGridTMP.Columns[IndexCol].DisableDragging := True;
  dxGridTMP.Columns[IndexCol].Tag := 100;
  dxGridTMP.Columns[IndexCol].Color := clBlack;
end; // CreateFixColumn

procedure TStaffPlanningF.BuildGridPlanning;
var
  I, J, K, RecCount: Integer;
  StringList: TStrings;
  procedure CreateColumnOfBand(IndexCol, IndexBand, Width: Integer;
    CaptionStr, FieldByName: String; TagColumn: Integer);
  begin
    if IndexBand = 0 then
      dxDBGridPlanning.Columns[IndexCol] :=
        dxDBGridPlanning.CreateColumn(TdxDBGridColumn);
    if IndexBand >= 1 then
      dxDBGridPlanning.Columns[IndexCol] :=
        dxDBGridPlanning.CreateColumn(TdxDBGridImageColumn);
    dxDBGridPlanning.Columns[IndexCol].Caption := CaptionStr;
    dxDBGridPlanning.Columns[IndexCol].BandIndex := IndexBand;
    dxDBGridPlanning.Columns[IndexCol].Width := Width;
    dxDBGridPlanning.Columns[IndexCol].Sizing := False;
    dxDBGridPlanning.Columns[IndexCol].DisableDragging := True;
    if (IndexBand > 0) then
       FieldByName := FieldByName;
    if (FieldByName <> '')  then
      dxDBGridPlanning.Columns[IndexCol].FieldName := FieldByName;
    dxDBGridPlanning.Columns[IndexCol].Tag := TagColumn;
    if IndexBand >= 1 then
    begin
      (dxDBGridPlanning.Columns[IndexCol] as TdxDBGridImageColumn).Images :=
        imgListBase;
      (dxDBGridPlanning.Columns[IndexCol] as TdxDBGridImageColumn).ImageIndexes.
        Assign(StringList);
      (dxDBGridPlanning.Columns[IndexCol] as TdxDBGridImageColumn).Values.
        Assign(StringList);
    end;
  end; // CreateColumnOfBand
begin
//CAR 28-7-2003 - changes for performance
  StringList := TStringList.Create;
  for i:= 0 to MAX_NR_BMP do
    StringList.Add(IntToStr(i));

  CreateColumnOfBand( 0, 0, EmpWidth, SStaffPlnEmployee, 'EMPLOYEE_NUMBER', 0);
  CreateColumnOfBand( 1, 0, FDescriptionWidth, SStaffPlnName, 'DESCRIPTION', 0);
  i:= 1; j:= 2;
  for RecCount := 0 to (FShowColumn -1) do
  begin
    CreateBand(dxDBGridPlanning, i, 80,
      getWKCode(FWKList.Strings[RecCount], RecCount) +
      CHR_SEP + Chr(Ord(#13)) + Copy(GetDescWK(FWKDescList.Strings[RecCount]), 0, 20));

    for k := j to j + (MaxTB-1) {3} do
      CreateColumnOfBand(k, i, TBWidth, IntToStr(k -j + 1), 'WK' + IntToStr(RecCount),
        k -j + 1);
    if i < FShowColumn then
    begin
      CreateFixColumn(j + MaxTB {4}, I, dxDBGridPlanning);
      j := j + (MaxTB+1) {5};
    end
    else
      j := j + MaxTB {4};
    inc(i);
// WLog('k='+IntToStr(k)+' j='+IntToStr(j)+' i='+IntToStr(i)+' MaxTB='+IntToStr(MaxTB));
  end;
  CreateBand(dxDBGridPlanning, i, 80, '');
  for k := j to j + (MaxTB-1) {3} do
    CreateColumnOfBand(k, i, TBWidth, IntToStr(k -j + 1), 'WKEMPL',(k -j + 1) );
  dxDBGridPlanning.Bands[0].Fixed := bfLeft;
  dxDBGridPlanning.Bands[i].Fixed := bfRight;
  StringList.free;
end; // BuildGridPlanning

procedure TStaffPlanningF.ScrollGrid(ScrollPos: Integer);
var
  i, j1, j2, k, RecCount, MaxIndexWK, LastIndexWK: Integer;
begin
  Screen.Cursor := crHourGlass;
  LastIndexWK :=  ScrollPos - 1;
  MaxIndexWK := LastIndexWK + FShowColumn - 1;
  if MaxIndexWK > FwkCount then
  begin
    MaxIndexWK := FWKCount;
    LastIndexWK := MaxIndexWK - FShowColumn + 1;
  end;
//
  dxDBGridPlanning.Items[0].Focused := True;
  i := 1; j1:= 2; J2 := 1;
  dxDBGridOCI.DataSource := Nil;
  dxDBGridPlanning.DataSource := Nil;
  for RecCount := LastIndexWK to MaxIndexWK do
  begin
    dxDBGridOCI.Bands[i].Caption := getWKCode(FWKList.Strings[RecCount], RecCount) +
      ' ' +
      Chr(Ord(#13)) +  Copy(GetDescWK(FWKDescList.Strings[RecCount]), 0 ,20);

    for k := j2 to j2 + (MaxTB-1) {3} do
    begin
      dxDBGridOCI.Columns[k].FieldName := 'WK' + IntToStr(RecCount);
    end;
    j2 := j2 + 1;// skip separator column
    dxDBGridPlanning.Bands[i].Caption :=
      getWKCode(FWKList.Strings[RecCount], RecCount) + ' ' +
      Chr(Ord(#13)) +  Copy(GetDescWK(FWKDescList.Strings[RecCount]), 0 ,20);

    for k := j1 to j1 + (MaxTB-1) {3} do
      dxDBGridPlanning.Columns[k].FieldName := 'WK' +
         IntToStr(RecCount);
    j1 := j1 + 1;// skip separator column
    j1 := j1 + MaxTB {4};
    j2 := j2 + MaxTB {4};
    inc(i);
  end;
  dxDBGridOCI.DataSource := StaffPlanningOCIDM.DataSourceOCI;
  dxDBGridPlanning.DataSource := StaffPlanningEMPDM.DataSourcePIVOT;
  Screen.Cursor := crDefault;
end; // ScrollGrid

procedure TStaffPlanningF.dxDBGridPlanningCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if (AColumn.Tag >= 1) and (AColumn.Tag <= MaxTB {4}) then
     AText := Trim(Copy(AText, (AColumn.Tag * 2)-1, 2));

  if AColumn.Tag = 100 then
    AColor := ClBlack
  else
    if ANode.Index mod 2 <> 0 then
      AColor := clScanRouteP1{Normal }
    else
      AColor := clInactiveBorder;{clGray;}
end; // dxDBGridPlanningCustomDrawCell

//RV067.4.
procedure TStaffPlanningF.BuildPopupMenu(AEmployeeNumber: Integer);
var
  NewItem: TMenuItem;
  SaveSql: String;
  CountryId: Integer;
begin
  if FSelection <> 0 then Exit;

  // RV083.2. Do not select on employee, because this is not known here.
  //          But select on the plant: Always 1 plant is selected.
  // RV083.2. Old:
{  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(AEmployeeNumber), 0); }
  // RV083.2. New:
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) ' +
  ' FROM PLANT P ' +
  ' WHERE ' +
  '   P.PLANT_CODE =  ' + '''' + FPlant + '''', 0);
  // RV083.2. Old:
//  if (AEmployeeNumber <> 0) and (CountryId <> 0) then
  // RV083.2. New:
  if (CountryId <> 0) then
  begin
    SaveSql := StaffPlanningEMPDM.QueryAbs.Sql.Text;
    SystemDM.UpdateAbsenceReasonPerCountry(
      CountryId,
      StaffPlanningEMPDM.QueryAbs
    );

    StaffPlanningEMPDM.ClientDataSetAbsRsn.Close;
  end;
  if not StaffPlanningEMPDM.QueryAbs.Active then
    StaffPlanningEMPDM.QueryAbs.Open;
  if not StaffPlanningEMPDM.ClientDataSetAbsRsn.Active then
    StaffPlanningEMPDM.ClientDataSetAbsRsn.Open;
  StaffPlanningEMPDM.ClientDataSetAbsRsn.First;
  while not StaffPlanningEMPDM.ClientDataSetAbsRsn.Eof do
  begin
    NewItem := TMenuItem.Create(Self);
    NewItem.Caption := StaffPlanningEMPDM.ClientDataSetAbsRsn.
      FieldByName('ABSENCEREASON_CODE').AsString +  ' ' +
      StaffPlanningEMPDM.ClientDataSetAbsRsn.FieldByName('DESCRIPTION').AsString;
    PopupMenuAbsRsn.Items.Add(NewItem);
    NewItem.OnClick := PopupItemClick;
    StaffPlanningEMPDM.ClientDataSetAbsRsn.Next;
  end;
  if (AEmployeeNumber <> 0) and (CountryId <> 0) then
  begin
    StaffPlanningEMPDM.QueryAbs.Close;
    StaffPlanningEMPDM.QueryAbs.Sql.Text := SaveSql;
  end;
end; // BuildPopupMenu

procedure TStaffPlanningF.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  PanelOCI.Tag := PanelOCI.Height;
  for I := 0 to SystemDM.MaxTimeblocks-1 do
    TotalArray[I] := 0; // GLOB3-84
  with Screen do
  begin
    //CAR 550278 - depends on the resolution
    // GLOB3-60: 80 is used here as fixed width for workspot with 4 timeblocks
    //           4 x 20 = 80, but now we can have max. 10 timeblocks.
    FScreenShowColumn := ((Width - 34 - 190) div (MaxTB * TBWidth) {80}) - 2; // GLOB3-60 -> minus 2 not 1.
    // width of scroll of grid planning is almost 34;
    // 190 is minimum width of bands[0] (employee number + description)
    // 60 is width of employee number
    // GLOB3-60: To prevent the timeblocks of last shown workspot (per screen)
    //           is not always shown completely, we multiple here with 9 *10,
    //           to give more space.
    FDescriptionWidth := Round((Width - 34 -
      ((FScreenShowColumn + 1) *  (MaxTB * TBWidth) {80}) - EmpWidth) * 9/10); // GLOB3-60 -> * 9/10
    // GLOB3-60 Do this, to prevent it is too width.
    FDescriptionWidth := Round(EmpWidth * 3.34);
  end;

  FSaveChanges := False;
  FListSaveEmp := TStringList.Create;

  // GLOB3-60
  StaffPlanningOCIDM.PlantGetSTDOCCFlags(FPlant);

  //RV067.4.
  BuildPopupMenu(0);
  //CAR 28-7-2003 = changes for performance
  dxDBGridPlanning.DataSource := Nil;
  dxDBGridOCI.DataSource := Nil;
  FShowColumn := Min(FScreenShowColumn, FWKCount + 1);
  BuildGridPlanning;
  BuilGridOccInd;
  dxDBGridPlanning.DataSource := StaffPlanningEMPDM.DataSourcePIVOT;
  dxDBGridOCI.DataSource := StaffPlanningOCIDM.DataSourceOCI;
  // CAR: user rights 27-10-2003
  if (Fselection = 0)  and (Edit_Staff_Planning = ITEM_VISIBIL_EDIT) then
    dxDBGridPlanning.PopupMenu := PopupMenuAbsRsn
  else
    dxDBGridPlanning.PopupMenu := Nil;

  if (FWKCount + 1 - FShowColumn - 1) < 0 then
  begin
    ScrollBarGrid.Visible := False;
  end
  else
  begin
    ScrollBarGrid.Visible := True;
    ScrollBarGrid.Min := 1;
    ScrollBarGrid.Max := FWKCount + 5; // GLOB3-60 // 1 -> 5
    ScrollBarGrid.LargeChange := FShowColumn;
    ScrollBarGrid.SmallChange := 1;
    ScrollBarGrid.PageSize := FShowColumn;
  end;
  // GLOB3-60: The totals are always read/written to DB, but are hidden
  //           when needed. Reason: Totals are used to test for overplanning.
  PanelOCI.Height := dxDBGridOCI.DefaultRowHeight; // Minimum height
  // Make optionally higher for horizontal scrollbar
  if ScrollBarGrid.Visible then
    PanelOCI.Height := PanelOCI.Height + dxDBGridOCI.DefaultRowHeight;
  //CAR : 6-5-2004
  if (not StaffPlanningOCIDM.ShowLevel) then // Do not show any level, only totals
  begin
    PanelOCI.Height := PanelOCI.Height + (dxDBGridOCI.DefaultRowHeight * 2);
  end
  else
  begin
    // Optionally show levels A, B, C
    if StaffPlanningOCIDM.LevelABCCount > 1 then // Show totals?
      PanelOCI.Height := PanelOCI.Height + (dxDBGridOCI.DefaultRowHeight * 2);
    if StaffPlanningOCIDM.StdOccA then
      PanelOCI.Height := PanelOCI.Height + (dxDBGridOCI.DefaultRowHeight * 2);
    if StaffPlanningOCIDM.StdOccB then
      PanelOCI.Height := PanelOCI.Height + (dxDBGridOCI.DefaultRowHeight * 2);
    if StaffPlanningOCIDM.StdOccC then
      PanelOCI.Height := PanelOCI.Height + (dxDBGridOCI.DefaultRowHeight * 2);
  end;
end; // FormCreate

//Set buttons from red to invisible
procedure TStaffPlanningF.SetToInvisibleRedButton(Empl, ColIndex: Integer);
var
  i: Integer;
  StrTmp, StrNewTmp: String;
begin
  StaffPlanningEMPDM.TablePivot.Edit;
  StrNewTmp := FillLeadSpace(GetTableBTN(Invisible, '', ''),2);
  for i:= 0 to FWKCount do
  begin
   StrTmp := StaffPlanningEMPDM.TablePivot.FieldByName('WK' + IntToStr(i)).AsString;
   StrTmp[ColIndex * 2 - 1] := StrNewTmp[1];
   StrTmp[ColIndex * 2 ] := StrNewTmp[2];
   StaffPlanningEMPDM.TablePivot.FieldByName('WK' + IntToStr(i)).AsString := StrTmp;
  end;
  StrTmp := StaffPlanningEMPDM.TablePivot.FieldByName('WKEMPL').AsString;
  StrTmp[ColIndex * 2 - 1] := StrNewTmp[1];
  StrTmp[ColIndex * 2 ] := StrNewTmp[2];

  StaffPlanningEMPDM.TablePivot.FieldByName('WKEMPL').AsString := StrTmp;
  StaffPlanningEMPDM.TablePivot.Post;
  StaffPlanningEMPDM.AddInList(IntToStr(Empl), FListSaveEmp);
end; // SetToInvisibleRedButton

procedure TStaffPlanningF.PlannedEMP(Empl, ValColor, ColIndex: Integer;
  ValStr, ColumnStr: String);
var
  AColor: Integer;
begin
  AColor := Green;
  if StaffPlanningEMPDM.ClientDataSetTBPerEmpl.
    FindKey([Empl, FPlant, FShift, ColIndex]) then
    AColor := LightGreen;
  StaffPlanningEMPDM.TablePivot.Edit;
  StaffPlanningEMPDM.SetFieldPIVOT(Empl, ColIndex,
    ValStr, ColumnStr, ValColor, AColor, False, True);
  StaffPlanningEMPDM.TablePivot.Post;
  StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
    FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);
  STaffPlanningOCIDM.UpdateOCI;
end; // PlannedEMP

procedure TStaffPlanningF.NotPlannedEMP(Empl, ValColor, ColIndex: Integer;
  ValStr, ColumnStr: String);
begin
  if not(StaffPlanningEMPDM.TablePivot.State in [dsEdit, dsInsert]) then
    StaffPlanningEMPDM.TablePivot.Edit;
  StaffPlanningEMPDM.SetFieldPIVOT(Empl, ColIndex,
    ValStr, ColumnStr, ValColor, Normal, False, False);

  StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
    FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);

  StaffPlanningEMPDM.TablePivot.Post;
  STaffPlanningOCIDM.UpdateOCI;
end; // NotPlannedEMP

procedure TStaffPlanningF.ChangesForUseFAlt(WkCode, ColumnStr: String;
  AColor, ColIndex: Integer);
var
  ValColor: Integer;
  ValPressed, ValLevel: String;
  OldStr, NewStr: String;
begin
  OldStr := StaffPlanningEMPDM.TablePivot.FieldByName(ColumnStr).AsString;
  GetAtributeBTN(Trim(Copy(OldStr, 2*ColIndex - 1, 2)), ValColor, ValPressed,
    Vallevel);
  NewStr := FillLeadSpace(GetTableBTN(AColor, ValPressed, Vallevel), 2);
  OldStr[2*ColIndex - 1] := NewStr[1];
  OldStr[2*ColIndex] := NewStr[2];
  StaffPlanningEMPDM.TablePivot.FieldByName(ColumnStr).AsString :=
    OldStr;
  if WKCode <> '' then
  begin
    OldStr := StaffPlanningEMPDM.TablePivot.FieldByName(WKCode).AsString;
    GetAtributeBTN(Trim(Copy(OldStr, 2*ColIndex - 1, 2)),
      ValColor, ValPressed, Vallevel);
    NewStr :=  FillLeadSpace(GetTableBTN(Normal, ValPressed, Vallevel), 2);
    OldStr[2*ColIndex - 1] := NewStr[1];
    OldStr[2*ColIndex] := NewStr[2];

    StaffPlanningEMPDM.TablePivot.FieldByName(WKCode).AsString := OldStr;
// update OCI Table
    // GLOB3-60 10 must be converted to A via Int2Hex here.
    StaffPlanningOCIDM.UpdateLineOCI(WKCode + Int2Hex(ColIndex),
      Trim(Copy(StaffPlanningEMPDM.TablePivot.
        FieldByName(WKCode).AsString, 2*ColIndex -1, 2)),  -1);
   end;

    // GLOB3-60 10 must be converted to A via Int2Hex here.
   StaffPlanningOCIDM.UpdateLineOCI(ColumnStr + Int2Hex(ColIndex),
     Trim(Copy(StaffPlanningEMPDM.TablePivot.
       FieldByName(ColumnStr).AsString, 2*ColIndex - 1, 2)), 1);
end; // ChangesForUseFAlt

procedure TStaffPlanningF.dxDBGridPlanningEditing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
var
  ValStr, ValStrJ, ColumnStr, AbsenceReason, WKCode: String;
    Empl, AColor, ValColor, j, Index, ValColor_EMP: Integer;
  ValPressed, ValLevel,
  ValPressed_EMP,  ValLevel_EMP: String;
  // RV050.2. Use a procedure to prevent multiple code in 2 places.
  procedure PlanningAction(const AColIndex: Integer);
  begin
    WKCode := GetPlannedWKFromPivot(AColIndex, StaffPlanningEMPDM.TablePivot);
    if WKCode <> '' then
    begin
      AColor := Green;
      if StaffPlanningEMPDM.
        ClientDataSetTBPerEmpl.FindKey([Empl, FPlant, FShift, AColIndex]) then
        AColor := LightGreen;
      ChangesForUseFAlt(WKCode, ColumnStr, AColor, AColIndex);
      StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
        FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);
    end;
  end;
begin
  inherited;
  Allow := False;
  if FNotClick then
    Exit;
  ValStr := IntToStr(Node.ImageIndex);

  if TDXDBGrid(Sender).FocusedField <> nil then
  begin
    ColumnStr :=  TDXDBGrid(Sender).FocusedField.FieldName;
    FColumnName := DXdbGridPlanning.FocusedField.FieldName;
  end;
  if (Pos('EMPLOYEE_NUMBER',ColumnStr) > 0) or
    (Pos('DESCRIPTION', ColumnStr)> 0) then
    TDXDBGrid(Sender).Hint := '';

  // MR:14-03-2007 No action needed when 'total'-column is clicked.
  //               This is the most-right column.
  if (Pos('EMPL', ColumnStr) > 0) then
    Exit;

  if not GetFieldsPivot(ColumnStr, StaffPlanningEMPDM.TablePivot, FColIndex,
    ValStr, Empl) then
    Exit;
  if (ValStr = '20') then
  begin
    TDXDBGrid(Sender).Hint := '';
    Exit;
  end;
  //Car 8-4-2004

  GetAtributeBTN(ValStr, ValColor, ValPressed, ValLevel);
  GetAtributeBTN(
    Trim(Copy(StaffPlanningEMPDM.TablePivot.FieldByName('WKEMPL').AsString,
      2 * FColIndex - 1, 2)),
      ValColor_EMP, ValPressed_EMP,  ValLevel_EMP);

  if (FAlt or FCtrl) and
    ( (ValColor = Red) or (ValColor_EMP  = Red)) then
    Exit;

// if the button is red = > make it invisible
  if (ValColor = Red) and (Edit_Staff_Planning = ITEM_VISIBIL_EDIT) then
  begin
    FSaveChanges := True;
    SetToInvisibleRedButton(Empl, FColIndex);

// the same action for all buttons of WK/ EMPL
    if FCtrl then
      for j:= 1 to MaxTB {4} do
        if (j <> FColIndex) then
          SetToInvisibleRedButton(Empl, j);
  end;
// button pressed and grey colored
  if (IsPressed(ValStr) and (ValColor = Normal)) and (Not FAlt) then
    Exit;
 // button pressed and darkgrey colored - hint
  FShowAbsenceReason := False;
  AbsenceReason := '';
  if (IsPressed(ValStr)) and (ValColor = DarkGrey) then
  begin
    Index := FListAbsRsn.IndexOf(IntToStr(Empl) + CHR_SEP + IntToStr(FColIndex));
    if Index >= 0 then
       AbsenceReason := FLISTABSRSN.Strings[Index + 1];
    if Pos('&', AbsenceReason) > 0 then
      AbsenceReason := Copy(AbsenceReason, 0 , Pos('&', AbsenceReason) - 1) +
        Copy(AbsenceReason, Pos('&', AbsenceReason) + 1, Length(AbsenceReason))
    else
    begin
      Index := FABSList.IndexOf(AbsenceReason);
      if (FABSList.Strings[Index] <> AbsenceReason) then
        Index := FABSList.IndexOf(AbsenceReason + '0');

      if Index >= 0 then
        AbsenceReason := AbsenceReason + ' ' + FABSDescList.Strings[Index];
    end;
    TDXDBGrid(Sender).ShowHint := True;
    TDXDBGrid(Sender).Hint := AbsenceReason;
    FShowAbsenceReason := True;
  end;

  if not FShowAbsenceReason then
    TDXDBGrid(Sender).Hint := '';

  If Edit_Staff_Planning <> ITEM_VISIBIL_EDIT then
  begin
    FEditing := False;
    Exit;
  end;

// MR:14-03-2007 Moved higher to prevent something is done
// when in the most right column is clicked, which is a 'total'-column.
{  if (Pos('EMPL', ColumnStr) > 0) then
    Exit; }

  if not FEditing then Exit;
  FEditing := False;
  FOverPlanning := False;

// if button is not pressed => will become green or light green
// update PIVOT + OCI TABLE
  // RV050.2. ValStrJ is used, to prevent the loss of contents for ValStr,
  //          which is used later when CTRL-ALT is pressed.
  if (not IsPressed(ValStr) ) and (ValColor <> DarkGrey) then
  begin
    FSaveChanges := True;
    PlannedEMP(Empl, ValColor, FColIndex, ValStr, ColumnStr);

// the same action for all buttons of WK/ EMPL
    if FCtrl then
      for j:= 1 to MaxTB {4} do
        if (j <> FColIndex) then
        begin
          WKCode := ColumnStr;
          ValStrJ := Trim(Copy(StaffPlanningEMPDM.
            TablePivot.FieldByName(WKcode).AsString, 2* j -1, 2));
          if (not IsPressed(ValStrJ)) then
          begin
            GetAtributeBTN(ValStrJ, ValColor, ValPressed, ValLevel);
            ColumnStr := WKcode;
            PlannedEMP(Empl, ValColor, j, ValStrJ, ColumnStr);
          end;
        end;
  end;
// if pressed and ALt function
  // RV050.2. ValStrJ is used, to prevent the loss of contents for ValStr,
  //          which is used later when CTRL-ALT is pressed.
  if FAlt then
    if (IsPressed(ValStr)) and (ValColor = Normal)  then
    begin
      FSaveChanges := True;
// determine if OCI Table after updating is Ok
  //  if Pos('BTN', ColumnStr) > 0 then
//        ColumnStr := Copy(ColumnStr, 0, Pos('BTN', ColumnStr) - 2)
  //    else
    //    Exit;

// planned  PIVOT Table
      // RV050.2. Use a procedure.
      StaffPlanningEMPDM.TablePivot.Edit;
      PlanningAction(FColIndex);
      if FCtrl then
        for j:= 1 to MaxTB {4} do
          if (j <> FColIndex) then
          begin
            WKCode := ColumnStr;
            ValStrJ := Trim(Copy(
            StaffPlanningEMPDM.TablePivot.
              FieldByName(ColumnStr).AsString, 2 * j -1,2));
            GetAtributeBTN(ValStrJ, ValColor, ValPressed, ValLevel);
            if (IsPressed(ValStrJ)) and (ValColor = Normal) then
            begin
              // RV050.2. Use a procedure.
              PlanningAction(j);
            end;
          end;
      GiveMessageOverplanning;
      Exit;
    end
    else
    begin
      GiveMessageOverplanning;
      Exit;
    end;
// IF BUTTON IS PRESSED AND GREEN OR LIGHT GREEN  => UPDATE PIVOT + OCI TABLE
  // RV050.2. ValStrJ is used, to prevent the loss of contents for ValStr,
  //          which is used later when CTRL-ALT is pressed.
  if IsPressed(ValStr) and ((ValColor = Green) or (ValColor = LightGreen)) then
  begin
    FSaveChanges := True;
    NotPlannedEMP(Empl, ValColor, FColIndex, ValStr, ColumnStr);
    // the same action for all buttons of WK/ EMPL
    if FCtrl then
      for j:= 1 to MaxTB {4} do
        if (j <> FColIndex) then
        begin
          WKCode := ColumnStr;
          ValStrJ := Trim(Copy(StaffPlanningEMPDM.
            TablePivot.FieldByName(ColumnStr).AsString, 2 * j -1, 2));
          GetAtributeBTN(ValStrJ, ValColor, ValPressed, ValLevel);

          if (IsPressed(ValStrJ)) and ((ValColor = Green) or (ValColor = LightGreen)) then
          begin
            ColumnStr := WKcode;
            GetAtributeBTN(Trim(Copy(StaffPlanningEMPDM.TablePivot.
              FieldByName(ColumnStr).AsString, 2* FColIndex-1, 2)),
              ValColor, ValPressed, ValLevel);
            NotPlannedEMP(Empl, ValColor, j, ValStrJ, ColumnStr);
          end;
        end;
    end;
  GiveMessageOverplanning;
end; // dxDBGridPlanningEditing

procedure TStaffPlanningF.dxDBGridPlanningClick(Sender: TObject);
begin
  inherited;
  //CAR User rights 27-10-2003
  If Edit_Staff_Planning <> ITEM_VISIBIL_EDIT then
    Exit;
  //
  if FNotClick then
    Exit;
  if dxDBGridPlanning.DataSource.State in [dsEdit, dsInsert] then
  begin
    StaffPlanningEMPDM.TablePivot.Post;
    FSaveChanges := True;
  end;

 if not FShowAbsenceReason then
    dxDBGridPlanning.Hint := '';
end;

procedure TStaffPlanningF.dxDBGridOCICustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  PosWK, ColorLevel, ColorX, Nrplanned, Index, IndexList: Integer;
  WKCode, Level: String;
  IndexNode: Integer;
  LevelsCount: Integer;
  // GLOB3-84
  function ExtractFieldSep(VAR ALine: String; ASeparator: String): String;
  var
    i: Integer;
    Part: String;
  begin
    i := Pos(ASeparator, ALine);
    if i > 0 then
    begin
      Part := Copy(ALine, 1, i-1);
      ALine := Copy(ALine, i+1, Length(ALine));
    end
    else
    begin
      Part := ALine;
      ALine := '';
    end;
    Result := Part;
  end;
  // GLOB3-84
  function Totals: String;
  var
    I, J: Integer;
    Value: String;
    Field: String;
  begin
    Result := '';
    for I := 0 to SystemDM.MaxTimeblocks-1 do
      TotalArray[I] := 0;
    with StaffPlanningOCIDM.qryOCIPlanning do
    begin
      Close;
      ParamByName('COMPUTER_NAME').AsString := SystemDM.CurrentCOMPUTERName;
      ParamByName('OCI_LEVEL').AsString := StaffPlanningOCIDM.TableOCI.FieldByName('OCI_LEVEL').AsString;
      Open;
      First;
      while not Eof do
      begin
        for I := 0 to 999 do
        begin
          if Assigned(FindField('WK' + IntToStr(I))) then
          begin
            Value := FieldByName('WK' + IntToStr(I)).AsString;
            for J := 0 to SystemDM.MaxTimeblocks-1 do
            begin
              Value := Trim(Value);
              Field := ExtractFieldSep(Value, ' ');
              if Field <> '' then
                TotalArray[J] := TotalArray[J] + StrToInt(Field);
            end;
          end
          else
            Break;
        end;
        Next;
      end; // while
      Close;
    end;
    for J := 0 to SystemDM.MaxTimeblocks-1 do
    begin
      if TotalArray[J] = 0 then
        Result := Result + '      '
      else
        Result := Result + Format('%6d', [TotalArray[J]]);
    end;
  end; // Totals
  function LevelText: String;
  begin
    if (AText = 'A1') or (AText = 'B1') or (AText = 'C1') then
      Result := SOCILevel + Copy(AText,0,1)
    else
      if (AText = 'A2') or (AText = 'B2') or (AText = 'C2') then
        Result := SPlannedLevel + Copy(AText,0,1)
      else
        if AText = 'D1' then
          Result := SOCIAllLevel
        else
          Result := SPlannedAllLevels;
  end; // LevelText
  function OCILevel: String;
  begin
    // Contains OCI_LEVEL (A1, A2, B1, B2, C1, C2, D1, D2
    Result := ANode.Values[0];
  end;
begin
  inherited;
  // GLOB3-60:
  // If only 1 (A, B or C) is shown: Use only 0 and 1 position (no total is shown).
  // If 2 are shown: Use 0,1,2,3,4,5 position (incl. total).
  // If 3 are shown: Normal behaviour
  LevelsCount := StaffPlanningOCIDM.LevelABCCount;

  //CAR 6-5-2004
  // Left column where level-descriptions are shown.
  if AColumn.BandIndex = 0 then
  begin
    if (StaffPlanningOCIDM.ShowLevel) then
    begin
      if LevelsCount > 1 then
      begin
        if (AText = 'D1') or (AText = 'D2') {ANode.Index in [6,7])} then
          AColor := clScanRouteP1
        else
          AColor := Normal;
      end;
      if LevelsCount = 1 then // Only 1 level is shown, without totals.
      begin
        case ANode.AbsoluteIndex of
          0..1: AText := LevelText;
        end;
      end
      else
        if LevelsCount = 2 then // 2 levels are shown with totals
        begin
          case ANode.AbsoluteIndex of
            0..5: AText := LevelText;
          end;
        end
        else
        begin // All levels are shown with totals
          case ANode.AbsoluteIndex of
          0..7: AText := LevelText;
          end;
        end;
    end
    else
    begin
      AColor := clScanRouteP1;
      case ANode.AbsoluteIndex of
        0..1: AText := LevelText;
      end;
    end;
    Exit;
  end;

  if AColumn.Tag = 100  then
  begin
    AColor := clBlack;
    Exit;
  end;
  AText := Copy(AText, (Acolumn.ColIndex + 1) * 6 - 5, 6);
  AText := Trim(AText);

  // GLOB3-84
{
WDebugLog('BandIndex=' + IntToStr(AColumn.BandIndex) + ';' +
  'ColIndex=' + IntToStr(AColumn.ColIndex) + ';' +
  'AText=' + AText + ';' +
  'OCILevel=' + OCILevel);
}
  WKCode := dxDBGridOCI.Bands[AColumn.BandIndex].Caption;
  if WKCode = '' then
  begin
// Last band

    // GLOB3-84
    if ShowTotals then
    begin
      Totals;
      AText := IntToStr(TotalArray[AColumn.ColIndex]);
      TotalArray[AColumn.ColIndex] := 0;
      if (AText = '0') then
        AText := '';
    end;

//    AText := '';
    Exit;
  end;

  PosWK := Pos(' ', WKCode);
  if PosWK > 0 then
  begin
    IndexList := FWKList.IndexOf(Copy(WKCode, 0, PosWK - 1));
    if IndexList < 0 then
        IndexList := FDeptList.IndexOf(Copy(WKCode, 0, PosWK - 1));

    WKCode := 'WK' + IntToStr(IndexList) + IntToStr(Acolumn.ColIndex + 1);
    IndexNode := ANode.AbsoluteIndex;
    // GLOB3-60 Not needed here.
{
    if (not StaffPlanningOCIDM.ShowLevel) then
    begin
      if IndexNode = 0 then
        IndexNode := 6;
      if IndexNode = 1 then
        IndexNode := 7;
    end;
}
    // GLOB3-60
    if (IndexNode in [0, 2, 4, 6]) then
    begin
      ColorLevel := Normal;
      if OCILevel = 'A1' then
      begin
        ColorLevel := Red;
        Level := 'A';
      end
      else
        if OCILevel = 'B1' then
        begin
          ColorLevel := Red;
          Level := 'B';
        end
        else
          if OCILevel = 'C1' then
          begin
            ColorLevel := Red;
            Level := 'C';
          end
          else
            if OCILevel = 'D1' then
            begin
              ColorLevel := Red;
              Level := 'D';
            end;
      // GLOB3-60 Not needed anymore
{
      case IndexNode of
        0:
        begin
          ColorLevel := Red;//DarkRed;//DarkRed;
          Level := 'A';
        end;
        2:
        begin
          ColorLevel := Red;
          Level := 'B';
        end;
        4:
        begin
          ColorLevel := Red;//LightRed;
          Level := 'C';
        end;
        6: // set color for sum line
        begin
          ColorLevel := Red;//Red_Levels;
          Level := 'D';
        end;
      end; //case
}
      // Calculate nr of planned employees here.
      NrPlanned := 0;
      Index := FListOCIPLANNED.IndexOf(WKCODE + Level);
      if Index >= 0 then
        if FListOCIPlanned.Strings[Index + 1] <> '' then
          NrPlanned := StrToInt(FListOCIPlanned.Strings[Index + 1]);
      if AText <> '' then
        GetEmpColor(StrToInt(AText), NrPlanned, ColorLevel, ColorX);
      AColor := ColorX;
    end  // if 0,2,4,6
    else
      AColor := Normal;

    if (AText = '0') or (AText = '') then
    begin
      AText := '';
      AColor := Normal;
    end;
    if AColor = Normal then
    begin
      // GLOB3-60
      if (OCILevel = 'D1') or (OCILevel = 'D2') then
//      if (ANode.Index in [6, 7]) then
        AColor := clScanRouteP1{light Normal }
      else
       AColor := Normal;
    end;
  end;
end; // dxDBGridOCICustomDrawCell

procedure TStaffPlanningF.dxDBGridOCIEditing(Sender: TObject;
  Node: TdxTreeListNode; var Allow: Boolean);
begin
  inherited;
  Allow := False;
end;

procedure TStaffPlanningF.dxDBGridPlanningMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ColumnName, ValCol, ValPressed, ValLevel,
   ValPressed_EMP, ValLevel_EMP: String;
  Employee, {IndexColumn, }ValColor, ValColor_EMP: Integer;
  Allow: Boolean;
  ColFocus : TdxDBTreeListColumn;
begin
  inherited;
  FEditing := True;
  //CAR USER rights 27-10-2003
  if Edit_Staff_Planning <> ITEM_VISIBIL_EDIT then
    Exit;
  //
  // Ctrl and ALT STATE
  if (ssCtrl in Shift) then
     FCtrl := True
  else
     FCtrl := False;
  if (ssAlt in Shift) then
     FAlt := True
  else
     FAlt := False;
  ColFocus :=  DXdbGridPlanning.GetColumnAT(X,Y);
  if ColFocus = Nil then
  begin
    DXdbGridPlanning.Hint := '';
    Exit;
  end;
  //here!!
  FColIndex := ColFocus.ColIndex + 1;

  if DXdbGridPlanning.FocusedField <> nil then
    ColumnName := DXdbGridPlanning.FocusedField.FieldName;
  if ColumnName = '' then
    Exit;
  dxDBGridPlanning.PopupMenu := PopupMenuAbsRsn;
 //Car 8-4-2004
  if not (ssRight in Shift) then
    dxDBGridPlanningEditing(Sender, DXdbGridPlanning.FocusedNode, Allow);

  if not GetFieldsPivot(ColumnName, StaffPlanningEMPDM.TablePivot,FColIndex,
     ValCol, Employee ) then
    dxDBGridPlanning.PopupMenu := Nil;

  GetAtributeBTN(ValCol, ValColor, ValPressed, ValLevel);

  if Pos('EMPL', ColumnName) > 0 then
   dxDBGridPlanning.PopupMenu := Nil;
// button pressed and not dark grey
  if (ValColor <> DarkGrey) and (IsPressed(ValCol)) then
    dxDBGridPlanning.PopupMenu := Nil;
//Car 5-6-2004 - bug
   if FColIndex > 0 then
    GetAtributeBTN(Trim(Copy(StaffPlanningEMPDM.TablePivot.FieldByName('WKEMPL'{+
      IntToStr(IndexColumn) + '_BTN'}).AsString, FColIndex * 2 -1, 2)),
      ValColor_EMP, ValPressed_EMP,  ValLevel_EMP);

  if (ValColor_EMP  = Red) then
    dxDBGridPlanning.PopupMenu := Nil;

   //Car 8-4-2004
  if Button = mbRight then
   if dxDBGridPlanning.PopupMenu <> Nil then
    PopupMenuAbsRsn.Popup(X, Y+50);
end;

procedure TStaffPlanningF.PopupMenuAbsRsnPopup(Sender: TObject);
var
  ValPressed_EMP, ValLevel_EMP,
  ValCol, ValPressed, ValLevel: String;
  Employee, {IndexColumn,} ValColor, ValColor_EMP: Integer;
  NewItem: TMenuItem;
begin
  FColumnName := DXdbGridPlanning.FocusedField.FieldName;

  if (FSelection <> 0) then
  begin
    dxDBGridPlanning.PopupMenu := Nil;
    Exit;
  end;
  if not GetFieldsPivot(FColumnName, StaffPlanningEMPDM.TablePivot, FColIndex,
    ValCol, Employee) then
    Exit;
  GetAtributeBTN(ValCol, ValColor, ValPressed, ValLevel);

  GetAtributeBTN(Trim(Copy(StaffPlanningEMPDM.
    TablePivot.FieldByName('WKEMPL').AsString, 2 * FColIndex - 1, 2)),
    ValColor_EMP, ValPressed_EMP, ValLevel_EMP);

  if (ValColor = Red) or (ValColor_EMP  = Red) then
  begin
  //car 8-4-2004
    dxDBGridPlanning.PopupMenu := Nil;
    Exit;
  end
  else
  // button pressed and not dark grey
    dxDBGridPlanning.PopupMenu := PopupMenuAbsRsn;

  inherited;
  with StaffPlanningEMPDM do
  begin
    if not IsPressed(ValCol) then
    begin
// delete last item
      if PopupMenuAbsRsn.Items[PopupMenuAbsRsn.Items.Count - 1].Tag = 1 then
        PopupMenuAbsRsn.Items.Delete(PopupMenuAbsRsn.Items.Count - 1);
    end;
    if  IsPressed(ValCol) then
    begin
// insert last item
      if (PopupMenuAbsRsn.Items[PopupMenuAbsRsn.Items.Count - 1].Tag <> 1) then
      begin
        NewItem := TMenuItem.Create(Self);
        NewItem.Caption := '&' + SMakeAvailable;
        NewItem.Tag := 1;
        PopupMenuAbsRsn.Items.Add(NewItem);
        NewItem.OnClick := PopupItemClick;
      end;
    end;
  end;
end;

procedure TStaffPlanningF.PopupItemClick(Sender: TObject);
var
  Employee,   ValColor, j: Integer;
{  ColumnStr,}Absence, ValColumn,{ WKCode,} {ValStr, } ValStrJ: String;
  ValLevel, ValPressed: String;
begin
  inherited;
  FOverPlanning := False;
  if (FSelection <> 0) then
  begin
    dxDBGridPlanning.PopupMenu := Nil;
    Exit;
  end;
  if (dxDBGridPlanning.PopupMenu = Nil)  then
  begin
    dxDBGridPlanning.PopupMenu := PopupMenuAbsRsn;
    Exit;
  end;
  if not GetFieldsPivot(FColumnName, StaffPlanningEMPDM.TablePivot, FColIndex,
     ValColumn, Employee) then
    Exit;
// invisible column
  if ValColumn = '20' then
    Exit;
//red column
  if ((ValColumn = '22') or (ValColumn ='23') or
    (ValColumn = '24') or (ValColumn = '25')) then
    Exit;

  Absence := (Sender as TMenuItem).Caption;
  if Absence = '' then
    Exit;
  FSaveChanges := True;
  GetAtributeBTN(ValColumn, ValColor, ValPressed, ValLevel);

// no reason make available employee, not pressed and normal color
  if ((Sender as TMenuItem).Tag = 1) then
  begin
    StaffPlanningEMPDM.TablePivot.Edit;
    StaffPlanningEMPDM.SetFieldPIVOT(Employee, FColIndex,
      ValColumn, FColumnName, ValColor, Normal, True, False);
    StaffPlanningEMPDM.TablePivot.POST;
    UpdateAbsList(False, FColIndex, Employee,'');
    dxDBGridPlanning.Hint := '';
    StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
      FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);

    STaffPlanningOCIDM.TableOCI.Edit;
    STaffPlanningOCIDM.UpdateOCI;
    if FCtrl then
    begin
      for j:= 1 to MaxTB {4} do
        if (j <> FColIndex) then
        begin
          ValStrJ := Trim(Copy(StaffPlanningEMPDM.
            TablePivot.FieldByName(FColumnName).AsString, 2 * j -1, 2));
          GetAtributeBTN(ValStrJ, ValColor, ValPressed, ValLevel);
          if IsPressed(ValStrJ) and (ValColor = DarkGrey) then
          begin
            ValColumn := Trim(Copy(StaffPlanningEMPDM.
              TablePivot.FieldByName(FColumnName).AsString, 2*j -1,2));
            GetAtributeBTN(ValColumn, ValColor, ValPressed, ValLevel);
            StaffPlanningEMPDM.TablePivot.Edit;
            StaffPlanningEMPDM.SetFieldPIVOT(Employee, j,
              ValColumn, FColumnName, ValColor, Normal, True, False);
            StaffPlanningEMPDM.TablePivot.POST;
            UpdateAbsList(False, j, Employee,'');

            STaffPlanningOCIDM.TableOCI.Edit;
            STaffPlanningOCIDM.UpdateOCI;
          end;
        end;
      StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
        FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);
    end;
    Exit;
  end;

// press buttons and drak grey color
  StaffPlanningEMPDM.TablePivot.Edit;
  StaffPlanningEMPDM.SetFieldPIVOT(Employee, FColIndex,
    ValColumn, FColumnName, ValColor, DarkGrey, True, True);

  StaffPlanningEMPDM.TablePivot.POST;
  StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
    FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);

  STaffPlanningOCIDM.TableOCI.Edit;
  STaffPlanningOCIDM.UpdateOCI;
  UpdateAbsList(True, FColIndex, Employee, Absence);
  if FCtrl then
  begin
    for j:= 1 to MaxTB {4} do
      if (j <> FColIndex) then
      begin
        ValStrJ := Trim(Copy(StaffPlanningEMPDM.
          TablePivot.FieldByName(FColumnName).AsString, 2*j - 1, 2));
        ValColumn := Trim(Copy(StaffPlanningEMPDM.
          TablePivot.FieldByName(FColumnName).AsString, 2*j - 1, 2));
        GetAtributeBTN(ValColumn, ValColor, ValPressed, ValLevel);
        if Not IsPressed(ValStrJ) or (ValColor = DarkGrey) then
        begin
          StaffPlanningEMPDM.TablePivot.Edit;
          StaffPlanningEMPDM.SetFieldPIVOT(Employee, j,
            ValColumn, FColumnName, ValColor, DarkGrey, True, True);
          StaffPlanningEMPDM.TablePivot.POST;

          STaffPlanningOCIDM.TableOCI.Edit;
          STaffPlanningOCIDM.UpdateOCI;
          UpdateAbsList(True, j, Employee, Absence);
        end;
      end;
      StaffPlanningEMPDM.AddInList(StaffPlanningEMPDM.TablePivot.
        FieldByName('EMPLOYEE_NUMBER').AsString, FListSaveEmp);
    end;
   GiveMessageOverplanning;
end;

procedure TStaffPlanningF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  StaffPlanningOCIDM.SaveOCP;
  if FSaveChanges then
    if DisplayMessage(SSaveChanges, mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      StaffPlanningEMPDM.SaveEMP_SPL;
      FSaveChanges := False;
    end;
  inherited;
  FListSaveEmp.Free;
end;

procedure TStaffPlanningF.Save1Click(Sender: TObject);
begin
  inherited;
  if not FSaveChanges then
    Exit;
  if DisplayMessage(SSaveChanges, mtConfirmation, [mbYes, mbNo]) = mrYes then
  begin
    StaffPlanningEMPDM.SaveEMP_SPL;
    FSaveChanges := False;
  end;
end;

procedure TStaffPlanningF.ExitMenuClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TStaffPlanningF.LegendMenuClick(Sender: TObject);
begin
  inherited;
  StaffPlanningLegendF := TStaffPlanningLegendF.Create(Application);
  try
    StaffPlanningLegendF.ShowModal;
  finally
    StaffPlanningLegendF.Free;
  end;
end;

procedure TStaffPlanningF.ScrollBarGridScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  if (Scrollcode = scEndScroll) then
  begin
    if ScrollPos > (ScrollBarGrid.Max - ScrollBarGrid.PageSize + 1) then
      ScrollPos := (ScrollBarGrid.Max - ScrollBarGrid.PageSize + 1);

    if ScrollPos < 0 then
      ScrollPos := 0;

    ScrollGrid(ScrollPos);
  end;
  dxDBGridOCI.SetFocus;
end;

procedure TStaffPlanningF.dxDBGridPlanningCustomDrawBand(Sender: TObject;
  ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
 if ABand.Index = 0 then
  begin
   ACanvas.Font.Size := 12;
   ACanvas.Font.Style := [fsBold];
  end;
end;

procedure TStaffPlanningF.dxDBGridPlanningMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  p, IndexWK: Integer;
  WK, StrTmp, LongDesc: String;

  G: TdxDBGrid;
  HitTest: TdxTreeListHitTest;
  CurCol: TdxDBTreeListColumn;
  BandCurrent: TdxTreeListBand;
begin
  inherited;
  G := Sender as TdxDBGrid;
 
  FNotClick := (y >=0) and (y <= 50);
  CurCol := G.GetColumnAt(X, Y);
  if CurCol = Nil then
    FNotClick := True;

  if not ((y >= 0) and (y <= 50)) then
  begin
    Exit;
  end;

  try
    HitTest := G.GetHitTestInfoAt(X, Y);
    BandCurrent :=  G.GetBandAt(X, Y);
    if BandCurrent = Nil then
    begin
      G.ShowHint := False;
      exit;
    end;
    if HitTest = htBand then
    begin
      StrTmp := BandCurrent.Caption;
      if StrTmp = '' then
      begin
        G.ShowHint := False;
        Exit;
      end;

      p := pos(#13, StrTmp);
      if p = 0 then
      begin
        G.ShowHint := False;
        Exit;
      end;
      WK := Copy(StrTmp, 0 , p - 2);
      IndexWK := FWKList.IndexOf(WK);
      LongDesc := Copy(StrTmp, p + 1, Length(StrTmp) +1);
      if (IndexWK >= 0) then
        LongDesc := GetLongDescWK(FWKDescList.Strings[IndexWK]);
      G.Hint := LongDesc;
      G.ShowHint := True
    end;
  except
      // do nothing
  end;
end;

procedure TStaffPlanningF.FormDestroy(Sender: TObject);
begin
  inherited;
  StaffPlanningF := Nil;
end;

procedure TStaffPlanningF.FormShow(Sender: TObject);
begin
  inherited;
  ShowTotals := False; // GLOB3-223
//  Self.Font.Color := LightGreen;
  FCaption := Caption;
  DrawCaption;
  // Pims -> Oracle
  // This is needed to prevent a wrong output is shown in the grid,
  // (part of the grid was shown wrong).
  if not StaffPlanningEMPDM.TablePivot.IsEmpty then
  begin
    dxDBGridPlanning.BeginUpdate;
    // RV079.3. BEGIN
    // When this is done for sorting on name, it is much slower
    // then sorting on number.
    // To improve speed the 'last', 'first' is left out.
{
    StaffPlanningEMPDM.TablePivot.Last;
    StaffPlanningEMPDM.TablePivot.First;
}


//    ShowMessage(IntToStr(Count));

    // RV082.7. Statements are needed for correct refresh!
    // RV085.10. Disabled here.
//    StaffPlanningEMPDM.TablePivot.Last;
//    StaffPlanningEMPDM.TablePivot.First;

    // RV085.10. This is quicker, then Last + First.
    StaffPlanningEMPDM.TablePivot.First;
    while not StaffPlanningEMPDM.TablePivot.Eof do
      StaffPlanningEMPDM.TablePivot.Next;
    StaffPlanningEMPDM.TablePivot.First;

    // RV079.3. End
    dxDBGridPlanning.FullRefresh;
    dxDBGridPlanning.EndUpdate;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TStaffPlanningF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

// Workspot width
{
function TStaffPlanningF.WKWidth: Integer;
begin
  // 20 was default width for 1 position (for timeblock)
  Result := StaffPlanningEMPDM.MaxTimeblocksToShow * TBWidth;
end;
}
function TStaffPlanningF.TBWidth: Integer;
begin
  // Width of 1 timeblock shown per workspot in grid
  Result := 20;
end;

function TStaffPlanningF.MaxTB: Integer;
begin
  Result := StaffPlanningEMPDM.MaxTimeblocksToShow;
end;

function TStaffPlanningF.EmpWidth: Integer;
begin
  // Width of band where employee-nr+name is shown.
  Result := 60;
end;

procedure TStaffPlanningF.DrawCaption;
var
  yFrame,
  ySize : Integer;
  r : TRect;
begin
  //Height of Sizeable Frame
  yFrame := GetSystemMetrics(SM_CYFRAME);

  //Height of Caption Buttons
  ySize := GetSystemMetrics(SM_CYSIZE);

  //Get the handle to canvas using Form's device context
  Canvas.Handle := GetWindowDC(Self.Handle);
  try
//    SetFontToCaptionFont(Canvas.Font);
//    Canvas.Font.Color := clWhite;
//    Canvas.Brush.Style := bsClear;

    r := Rect(0, yFrame, Width, yFrame +ySize);
    DrawText(Canvas.Handle, PChar(FCaption), -1, r,
      DT_SINGLELINE OR DT_LEFT OR DT_VCENTER OR DT_NOPREFIX);
  finally
    //Get rid of the device context and set the canvas handle to default
    ReleaseDC(Self.Handle, Canvas.Handle);
    Canvas.Handle := 0;
  end;
end; // DrawCaption

// GLOB3-223
procedure TStaffPlanningF.SetShowTotals(const Value: Boolean);
begin
  try
    FShowTotals := SystemDM.GetDBValue(
    ' SELECT NVL(P.STAFFPLAN_SHOWTOT_YN, ''N'') ' +
    ' FROM PLANT P ' +
    ' WHERE ' +
    '   P.PLANT_CODE =  ' + '''' + FPlant + '''', 0) = 'Y';
  except
    FShowTotals := False;
  end;
end;

end.

