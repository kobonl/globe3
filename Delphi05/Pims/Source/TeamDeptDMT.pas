(*
  MRA:12-JAN-2010. RV050.8. 889955.
  - Added Plant-level for teams/team-departments.
  - Restrict plants using teamsperuser.
  MRA:21-OCT-2010 RV073.2. Small bugs.
  - Team-grid was not sorted on team: TableMaster, IndexFieldName changed.
  MRA:RV086.2. SC-50014392.
  - Plan in other plants, related issues:
    - When teams-per-users-filtering is used, you still see
      plants/teams/department/workspots not belonging to the
      teams-per-user-selection.
*)
unit TeamDeptDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTeamDeptDM = class(TGridBaseDM)
    TableMasterTEAM_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterRESP_EMPLOYEE_NUMBER: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableDetailTEAM_CODE: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterEMPLOYEELU: TStringField;
    TablePlant: TTable;
    DataSourcePlant: TDataSource;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableDetailDEPARTMENTCAL: TStringField;
    TableDepartmentDsc: TTable;
    TableDepartmentDscDEPARTMENT_CODE: TStringField;
    TableDepartmentDscPLANT_CODE: TStringField;
    TableDepartmentDscDESCRIPTION: TStringField;
    TableDepartmentDscBUSINESSUNIT_CODE: TStringField;
    TableDepartmentDscCREATIONDATE: TDateTimeField;
    TableDepartmentDscDIRECT_HOUR_YN: TStringField;
    TableDepartmentDscMUTATIONDATE: TDateTimeField;
    TableDepartmentDscMUTATOR: TStringField;
    TableDepartmentDscPLAN_ON_WORKSPOT_YN: TStringField;
    TableDepartmentDscREMARK: TStringField;
    TableDetailPLANTCAL: TStringField;
    TableDeptPerTeam: TTable;
    TableEmployee: TTable;
    TableEmployeeEMPLOYEE_NUMBER: TIntegerField;
    TableEmployeeSHORT_NAME: TStringField;
    TableEmployeePLANT_CODE: TStringField;
    TableEmployeeDESCRIPTION: TStringField;
    TableEmployeeDEPARTMENT_CODE: TStringField;
    TableEmployeeCREATIONDATE: TDateTimeField;
    TableEmployeeMUTATIONDATE: TDateTimeField;
    TableEmployeeMUTATOR: TStringField;
    TableEmployeeADDRESS: TStringField;
    TableEmployeeZIPCODE: TStringField;
    TableEmployeeCITY: TStringField;
    TableEmployeeSTATE: TStringField;
    TableEmployeeLANGUAGE_CODE: TStringField;
    TableEmployeePHONE_NUMBER: TStringField;
    TableEmployeeEMAIL_ADDRESS: TStringField;
    TableEmployeeDATE_OF_BIRTH: TDateTimeField;
    TableEmployeeSEX: TStringField;
    TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField;
    TableEmployeeSTARTDATE: TDateTimeField;
    TableEmployeeTEAM_CODE: TStringField;
    TableEmployeeENDDATE: TDateTimeField;
    TableEmployeeCONTRACTGROUP_CODE: TStringField;
    TableEmployeeIS_SCANNING_YN: TStringField;
    TableEmployeeCUT_OF_TIME_YN: TStringField;
    TableEmployeeREMARK: TStringField;
    TableEmployeeCALC_BONUS_YN: TStringField;
    TableEmployeeFIRSTAID_YN: TStringField;
    TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField;
    DataSourceEmployee: TDataSource;
    TableDetailDEPARTMENTLU: TStringField;
    TableDepartmentTeam: TTable;
    TableDepartmentTeamDEPARTMENT_CODE: TStringField;
    TableDepartmentTeamPLANT_CODE: TStringField;
    TableDepartmentTeamDESCRIPTION: TStringField;
    TableDepartmentTeamBUSINESSUNIT_CODE: TStringField;
    TableDepartmentTeamCREATIONDATE: TDateTimeField;
    TableDepartmentTeamDIRECT_HOUR_YN: TStringField;
    TableDepartmentTeamMUTATIONDATE: TDateTimeField;
    TableDepartmentTeamMUTATOR: TStringField;
    TableDepartmentTeamPLAN_ON_WORKSPOT_YN: TStringField;
    TableDepartmentTeamREMARK: TStringField;
    DataSourceDepartmentTeam: TDataSource;
    TableDetailPLANTLU: TStringField;
    TableP: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    TableMasterPlant: TTable;
    DataSourceMasterPlant: TDataSource;
    TableMasterPlantPLANT_CODE: TStringField;
    TableMasterPlantDESCRIPTION: TStringField;
    TableMasterPlantCITY: TStringField;
    TableMasterPlantSTATE: TStringField;
    TableMasterPlantPHONE: TStringField;
    TableMasterPLANT_CODE: TStringField;
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableExportCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableMasterPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDepartmentTeamFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TeamDeptDM: TTeamDeptDM;

implementation

uses SystemDMT, UPimsMessageRes, TeamDeptFRM;

{$R *.DFM}

procedure TTeamDeptDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not TableDepartmentDsc.Active then TableDepartmentDsc.Active := True;
  if (TableDetail.FieldByName('DEPARTMENT_CODE').asString <> '') then
    if (TableDepartmentDsc.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString,
    	TableDetail.FieldByName('DEPARTMENT_CODE').AsString])) then
        TableDetailDEPARTMENTCAL.Value :=
      	  TableDepartmentDsc.FieldByName('DESCRIPTION').AsString;
  // RV050.8. Plant is already shown as master.
{
  if not TablePlant.Active then
    TablePlant.Open;
  if TablePlant.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString]) then
    TableDetail.FieldByName('PLANTCAL').Value  :=
      TablePlant.FieldByName('DESCRIPTION').AsString;
}
end;

procedure TTeamDeptDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // RV050.8.
  TableDetail.FieldByName('PLANT_CODE').Value :=
    TableMasterPlant.FieldByName('PLANT_CODE').Value;
{
  if TableDeptPerTeam.RecordCount = 0 then
  begin
    TablePlant.First;
    TableDetail.FieldByName('PLANT_CODE').Value :=
      TablePlant.FieldByName('PLANT_CODE').Value;
  end
  else
  begin
    TableDetail.FieldByName('PLANT_CODE').Value :=
      TableDeptPerTeam.FieldByName('PLANT_CODE').Value;
  end;
}
end;

procedure TTeamDeptDM.TableDetailBeforePost(DataSet: TDataSet);
{var
  Plant: String;}
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  TableDetail.FieldByName('TEAM_CODE').AsString :=
    TableMaster.FieldByName('TEAM_CODE').AsString;
  // RV050.8.
{
  if (TableDetail.RecordCount >= 1) then
  begin
    Plant := TableDetail.FieldByName('PLANT_CODE').Value;
    TableDeptPerTeam.Close;
    TableDeptPerTeam.Open;
    TableDeptPerTeam.First;
    if (TableDeptPerTeam.FieldByName('PLANT_CODE').Value <> Plant) then
    begin
      DisplayMessage(SPimsNoMorePlantsinTeam, mtWarning, [mbOK]);
      Abort;
    end;
  end;
}
end;

procedure TTeamDeptDM.TableExportCalcFields(DataSet: TDataSet);
begin
  inherited;
  If not TableDepartmentDsc.Active then TableDepartmentDsc.Active := True;
  if (TableDetail.FieldByName('DEPARTMENT_CODE').asString <> '') then
    if (TableDepartmentDsc.FindKey([TableExport.FieldByName('PLANT_CODE').AsString,
    	TableExport.FieldByName('DEPARTMENT_CODE').AsString])) then
        TableExport.FieldByName('DEPARTMENTCAL').Value  :=
          TableDepartmentDsc.FieldByName('DESCRIPTION').AsString;
  if not TableP.Active then
    TableP.Open;
  if TableP.FindKey([TableExport.FieldByName('PLANT_CODE').AsString]) then
    TableExport.FieldByName('PLANTCAL').Value  :=
      TableP.FieldByName('DESCRIPTION').AsString;
end;

procedure TTeamDeptDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableMasterPlant);
  TableMasterPlant.Open;
  // RV086.2.
  SystemDM.PlantTeamFilterEnable(TableMaster);
  SystemDM.PlantTeamFilterEnable(TableDetail);
  SystemDM.PlantTeamFilterEnable(TableDepartmentTeam);
end;

procedure TTeamDeptDM.TableMasterPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TTeamDeptDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.2.
  Accept := SystemDM.PlantTeamTeamFilter(DataSet);
end;

procedure TTeamDeptDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.2.
  Accept := SystemDM.PlantTeamTeamFilter(DataSet);
  if Accept then
    Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

procedure TTeamDeptDM.TableDepartmentTeamFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.2.
  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

end.
