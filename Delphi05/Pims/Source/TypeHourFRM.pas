(*
  MRA:20-NOV-2009 RV045.1. SR-889942.
  - Addition of 2 fields per hourtype related to Overtime:
    - Time for Time
    - Bonus in Money
    This can be used instead of setting on Contract Group-level.
    But only if on Contract Group-level these setings are not used!
    If in Contract Group:
    - Time for Time = Y OR Bonus in Money = Y:
      - Disable/do not use in hourtype
    If in Contract Group:
    - Time for Time = N AND Bonus in Money = N:
      - Enable/use in hourtype
  SO:08-JUL-2010 RV067.4. 550497
    - saturday credit and ADV fields added
  MRA:27-AUG-2010 RV067.MRA.15. Small change.
  - Changed text 'ADV' to 'Shorter Working Week' (also in grid as 'SWW').
  MRA:8-NOV-2010. RV077.5.
  - Only show 'Saturday Credit' when there is an export type 'ATTENT'.
  MRA:27-JAN-2011 RV085.15. Small change. SC-50016904.
  - Checkbox 'ADV' must only be visible when in plants
    there is a country set as 'BEL'.
    This must also be based for plants defined in teams-per-users.
  MRA:16-MAY-2012 20013183. Extra counter Travel Time.
  - Type of Hours
    - Show an extra checkbox for 'Travel Time'. Default 'N'.
      This makes it possible to book hours in balance for
      earned travel time.
      NOTE: This does not fall under 'overtime'.
  MRA:2-NOV-2012 20013723
  - Addition of 2 fields: EXPORT_OVERTIME_YN and EXPORT_PAYROLL_YN.
*)

unit TypeHourFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, dxEditor, dxEdLib, dxDBELib, Mask,
  dxDBTLCl, dxGrClms;

type
  TTypeHourF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEditTypeHours: TDBEdit;
    DBEditDescription: TDBEdit;
    DBEditExportCode: TDBEdit;
    dxDBEditBonusPerc: TdxDBEdit;
    GroupBox2: TGroupBox;
    DBCheckBoxOverTime: TDBCheckBox;
    DBCheckBoxCountDays: TDBCheckBox;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridCheckColumn;
    dxDetailGridColumn6: TdxDBGridCheckColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    Label6: TLabel;
    dxDBEdit1: TdxDBEdit;
    DBCheckBox1: TDBCheckBox;
    dxDetailGridColumn8: TdxDBGridCheckColumn;
    DBCheckBox2: TDBCheckBox;
    DBCheckBoxTimeForTime: TDBCheckBox;
    DBCheckBoxBonusInMoney: TDBCheckBox;
    dxDetailGridColumnTimeForTime: TdxDBGridCheckColumn;
    dxDetailGridColumnBonusInMoney: TdxDBGridCheckColumn;
    DBCheckBoxADV: TDBCheckBox;
    DBCheckBoxSatCredit: TDBCheckBox;
    dxDetailGridColumnADV: TdxDBGridCheckColumn;
    dxDetailGridColumn12: TdxDBGridCheckColumn;
    DBCheckBoxTravelTime: TDBCheckBox;
    dxDetailGridColumnTravelTime: TdxDBGridCheckColumn;
    DBCheckBoxExportOvertimeADP: TDBCheckBox;
    DBCheckBoxExportPayrollADP: TDBCheckBox;
    dxDetailGridColumnExportOvertime: TdxDBGridCheckColumn;
    dxDetailGridColumnExportPayroll: TdxDBGridCheckColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBCheckBoxOverTimeClick(Sender: TObject);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
    procedure TimeForTimeEnableCheck;
  public
    { Public declarations }
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
  end;

function TypeHourF: TTypeHourF;

implementation

{$R *.DFM}

uses
  TypeHourDMT, GlobalDMT, SystemDMT;

var
  TypeHourF_HND: TTypeHourF;

function TypeHourF: TTypeHourF;
begin
  if (TypeHourF_HND = nil) then
    TypeHourF_HND := TTypeHourF.Create(Application);
  Result := TypeHourF_HND;
end;

procedure TTypeHourF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditTypeHours.SetFocus;
end;

procedure TTypeHourF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditTypeHours.SetFocus;
end;

procedure TTypeHourF.FormCreate(Sender: TObject);
begin
  // RV045.1. Bug! Datamodule was not assigned!
//  CreateFormDM(TTypeHourDM);
  TypeHourDM := CreateFormDM(TTypeHourDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := TypeHourDM.DataSourceDetail;
  // RV077.5. Only show for export type Attent.
  DBCheckBoxSatCredit.Visible := SystemDM.IsAttent;
  dxDetailGridColumn12.Visible := SystemDM.IsAttent;
  DBCheckBoxADV.Visible := GlobalDM.PlantIsBelgium; // RV085.15.
  // 20013723
  DBCheckBoxExportOvertimeADP.Visible := False;
  DBCheckBoxExportPayrollADP.Visible := False;
  dxDetailGridColumnExportOvertime.Visible := False;
  dxDetailGridColumnExportPayroll.Visible := False;
  if SystemDM.IsCLF then
    if not TypeHourDM.TestOnHourtypePerCountryADP then
    begin
      dxDetailGridColumnExportOvertime.Visible := True;
      dxDetailGridColumnExportPayroll.Visible := True;
      DBCheckBoxExportOvertimeADP.Visible := True;
      DBCheckBoxExportPayrollADP.Visible := True;
    end;
  inherited;
end;

procedure TTypeHourF.FormDestroy(Sender: TObject);
begin
  inherited;
  TypeHourF_HND := Nil;
end;

procedure TTypeHourF.FormShow(Sender: TObject);
begin
  inherited;
  // RV045.1. Check on Contract group
  ContractGroupTFT := False;
  if GlobalDM.ContractGroupTFTExists then
  begin
    ContractGroupTFT := True;
    DBCheckBoxTimeForTime.Enabled := False;
    DBCheckBoxBonusInMoney.Enabled := False;
  end;
end;

// RV045.1.
procedure TTypeHourF.TimeForTimeEnableCheck;
begin
  if ContractGroupTFT then
  begin
    DBCheckBoxTimeForTime.Enabled := False;
    DBCheckBoxBonusInMoney.Enabled := False;
    //RV067.4.
    DBCheckBoxADV.Enabled := False;
    dxDetailGridColumnTimeForTime.DisableEditor := True;
    dxDetailGridColumnBonusInMoney.DisableEditor := True;
    //RV067.4.
    dxDetailGridColumnADV.DisableEditor := True;
    dxDetailGridColumnTimeForTime.ReadOnly := True;
    dxDetailGridColumnBonusInMoney.ReadOnly := True;
    //RV067.4.
    dxDetailGridColumnADV.ReadOnly := True;
//    dxDetailGridColumnTimeForTime.Visible := False;
//    dxDetailGridColumnBonusInMoney.Visible := False;
  end
  else
  begin
    DBCheckBoxTimeForTime.Enabled := DBCheckBoxOverTime.Checked;
    DBCheckBoxBonusInMoney.Enabled := DBCheckBoxOverTime.Checked;
    //RV067.4.
    DBCheckBoxADV.Enabled := DBCheckBoxOverTime.Checked;
    dxDetailGridColumnTimeForTime.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnBonusInMoney.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    //RV067.4.
    dxDetailGridColumnADV.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnTimeForTime.ReadOnly := not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnBonusInMoney.ReadOnly := not DBCheckBoxOverTime.Checked;
    //RV067.4.
    dxDetailGridColumnADV.ReadOnly := not DBCheckBoxOverTime.Checked;
//    dxDetailGridColumnTimeForTime.Visible := True;
//    dxDetailGridColumnBonusInMoney.Visible := True;
  end;
end;

procedure TTypeHourF.DBCheckBoxOverTimeClick(Sender: TObject);
begin
  inherited;
  TimeForTimeEnableCheck; // RV045.1.
end;

// RV045.1.
procedure TTypeHourF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if AFocused or ASelected then
  begin
    if AColumn = dxDetailGridColumn5 then
    begin
      TimeForTimeEnableCheck;
    end;
  end;
end;

end.




