inherited DialogNotCompleteDateF: TDialogNotCompleteDateF
  Left = 499
  Top = 316
  Caption = 'Completion Date/Time'
  ClientHeight = 200
  ClientWidth = 363
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 363
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 65
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 363
    Height = 80
    Font.Color = clBlack
    Font.Height = -11
    TabOrder = 4
    object gBoxDateTime: TGroupBox
      Left = 0
      Top = 0
      Width = 363
      Height = 80
      Align = alClient
      Caption = 'Completion Date/Time'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 23
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 22
        Height = 13
        Caption = 'Time'
      end
      object dxDateEditDate: TdxDateEdit
        Left = 232
        Top = 16
        Width = 121
        TabOrder = 0
        Date = -700000
        DateOnError = deToday
        SaveTime = False
        UseEditMask = True
        StoredValues = 4
      end
      object dxTimeEditTime: TdxTimeEdit
        Left = 232
        Top = 43
        Width = 121
        TabOrder = 1
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 181
    Width = 363
  end
  inherited pnlBottom: TPanel
    Top = 140
    Width = 363
    inherited btnOk: TBitBtn
      Left = 80
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 194
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      23
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
