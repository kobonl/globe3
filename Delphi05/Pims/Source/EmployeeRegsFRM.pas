(*
  MRA:25-MAR-2009 RV024.
    Changed Date-selection in 'GetScans'.
  SO: 07-JUN-2010 RV065.4
  - added contract hours per week field
  MRA:23-JUL-2010. Bugfix. RV067.MRA.5.
  - Because of this change: RV067.4. it gave
    an error about 'LAST_YEAR_WTR_MINUTE' not found.
    Reason: A missing call to 'getcounters'.
  SO:06-AUG-2010 RV067.9. 550510
  - Enhancemnets Employee Registrations Dialog, Production hours
  MRA:24-AUG-2010 RV067.MRA.6. SO-550482.
  - Give indication that employee is non-active.
  MRA:30-AUG-2010 RV067.MRA.17 Bugfix.
  - When this dialog was just started, it did
    not show all data like 'salary hours'.
    Reason: It used a date with a time-part. Solved
            by truncating this.
  MRA:3-SEP-2010 RV067.MRA.25. Changes for 550510.
  - When using teams-per-user, it must also show employee who worked
    on the same plant based on teams-per-user, but can also be
    from other plants.
  MRA:29-SEP-2010 RV069.1. 550494. Bugfix.
  - Added TeamCode and DeptCode in Employee-selection-part.
  - It refreshed too late when date was changed and dialog re-opened.
  MRA:29-OCT-2010 RV075.7. Bugfix.
  - When a dialog is opened, the keyboard short-cuts can not be used.
  - EmployeeRegsBaseFRM changed so it is inherited from SideMenuBase that
    has functionality for the showing of the form. (InsertForm).
  MRA:1-NOV-2010 RV075.8.
  - When HoursPerEmployee-form is called from Emp. Regs. 
    and a key-short-cut is used like F5 (search), then
    it gives an 'access violation'.
  - When HoursPerEmployee-form is directly called
    it does not give this error.
  - Reason: The Form_Edit_YN is not used! This is handled only
    in SideMenuUnitMaintenanceFRM, but must also be done here!
  - Reason 2: It can happen the DatabaseModule of ActiveGrid is nil!
              For this changes are made to GridBase-form.
  MRA:12-NOV-2010 RV079.4. 550497. Addition.
  - GlobalDM-routines are now used for Balance-information, because
    it is needed in several places.
  MRA:29-NOV-2010 RV081.1.
  - Employee Registrations (RFL)
    - When logged in as WDB and password WDBWDB then
      when first employee 8165 is selected, and then
      a different date is selected, it is possible
      the original employee has been changed.
      Cause: It refreshes the employee-list based on date.
      Solution: After it has refreshed, always point to the
      original employee!
  MRA:1-DEC-2010 RV082.5.
  - Store ContractgroupCode for selected employee in memory,
    to use in Contractgroup-related dialogs, so it can
    be looked up when showing these dialogs.
  MRA:15-APR-2011 RV089.1.
  - When SystemDM.EmbedDialogs is true, the use 'fsStayOnTop' to
    get the dialog on top.
  MRA:10-MAY-2011 RV092.13. Bugfix. 550518
  - This dialog shows the planning/availability for an employee,
    also when this is done for a different plant.
    But it is not clear from which plant is it.
  - To be changed:
    - Add plant-code to the planning/availability-info.
    - Show it in red when it is a different plant.
  - Determine shift from 'shift schedule'.
  MRA:12-MAY-2011 Addition.
  - Shift Schedule
    - When called from Employee Regs:
      - Automatic refresh
      - Jump to employee
      - Show correct Plant
  MRA:11-JUL-2011 RV094.8. Bugfix.
  - When Availability-button is clicked for an employee
    who is shown as grey (end-date is in past), then
    it shows an availability-dialog for a different employee.
  - Solve this by showing a message like 'employee is inactive'?
  - Extra: An employee is now inactive by comparing the end-date, but
           he can also be inactive by comparing the start-date!
  MRA:5-AUG-2011 RV095.7. Rework
  - When an employee is not-active, then when
    Shiftscheme-button is clicked, do not show
    the dialog, but only a message 'Not active'.
  - Note: This is already done for Availability-button.
  MRA:4-OCT-2012 20013489 Overnight-Shift-System:
  - Show scans based on the shift
    by filtering on SHIFT_DATE instead of on scan-date.
  MRA:28-MAR-2013 TD-21429 Maximize Dialogs in Pims
  - Properties of dialog changed to:
    - BorderStyle: bsSingle
    - Constraints:
      - MinHeight: 579
      - MinWidth: 973
    - WindowState: wsMaximized
  - pnl0202.align: alTop
  - pnl0203.align: alClient
  - Added panels to pnl0203:
    - pnlScansClient.align: alClient
    - pnlScansRight.align: alRight
      - pnlScans2Client.align: alClient
      - pnlScans2Bottom.align: alBottom
  MRA:9-APR-2013 20012142
  - Show calculated total of breaks.
  MRA:24-APR-20013 20012142.10 Rework
  - Show breaks in employee registrations dialog
    - Round the shown calculated breaks to 15 minutes.
  MRA:25-APR-2013 TD-22429 Employee Active in Future
  - When an employee is defined as 'active in future', then
    it should still be possible to open Shift Schedule- and
    Availability-dialog.
  - Added showing of start-date to make it clear when an employee is
    inactive but active-in-future, based on selected date.
  MRA:1-MAY-2013
  - Add (optionally) weeknumbers in Calendar.
  - Component TMonthCalendar is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:7-OCT-2014 20012155
  - Emp. Regs. and change of hours for employee of other team
  - When an employee of an other team (not part of teams-per-user), worked
    on a department belonging to teams-per-user, then show this employee and
    allow changes of hours.
  - Related to this order:
    - It did not refresh correct. Cause: It used a selection-date that had
      a timestamp. To solve it, a trunc is used for date-assignments.
  MRA:17-MAR-2015 20015346
  - Do not show scan-seconds in reports/dialogs
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - Add 'Show only active' to show only active employees
  MRA:7-MAY-2015 20014450.50
  - Real Time Efficiency
  - Show scans including seconds
  MRA:2-MAY-2016 ABS-27382
  - Show only active (employees)-checkbox must be default checked.
  MRA:29-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)
unit EmployeeRegsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  PimsFRM, ComCtrls, StdCtrls, ExtCtrls, dxTL, dxCntner,
  EmployeeRegsDMT, dxEditor, dxExEdtr, dxExGrEd, dxExELib,
  dxLayout, DB, dxBar, dxBarDBNav, Buttons, dxDBCtrl, dxDBGrid, SystemDMT;

type
  TEmployeeRegsF = class(TPimsF)
    pnl0101: TPanel;
    pnl0201: TPanel;
    pnl0202: TPanel;
    pnl0203: TPanel;
    pnl0204: TPanel;
    pnl0302: TPanel;
    pnl0301: TPanel;
    gBoxDate: TGroupBox;
    gBoxEmployee: TGroupBox;
    gBoxPlanningBase: TGroupBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    btnEmployee: TButton;
    MonthCalendar: TMonthCalendar;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btnPlanning: TButton;
    btnAvailabilty: TButton;
    btnShiftSchedule: TButton;
    pnl0304: TPanel;
    pnl0303: TPanel;
    gBoxHours: TGroupBox;
    gBoxSalaryHours: TGroupBox;
    gBoxAbsenceHours: TGroupBox;
    btnHours: TButton;
    gBoxScans: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    dxTLPlanned: TdxTreeList;
    dxTLPlannedColumnTb: TdxTreeListColumn;
    dxTLPlannedColumnWorkspot: TdxTreeListColumn;
    dxTLAvailable: TdxTreeList;
    dxTLAvailabilityColumnTb: TdxTreeListColumn;
    dxTLAvailabilityColumnAvailabilty: TdxTreeListColumn;
    dxTLSalaryHours: TdxTreeList;
    dxTLSalaryHoursColumnHourtype: TdxTreeListColumn;
    dxTLSalaryHoursColumnHours: TdxTreeListColumn;
    dxTLAbsenceHours: TdxTreeList;
    dxTLAbsenceHoursColumnAbsenceHours: TdxTreeListColumn;
    dxTLAbsenceHoursColumnHours: TdxTreeListColumn;
    edtShift: TEdit;
    edtTotalSalaryHours: TEdit;
    edtPlannedSalaryHours: TEdit;
    edtTotalAbsenceHours: TEdit;
    edtPlannedAbsenceHours: TEdit;
    edtTotalHours: TEdit;
    edtPlannedHours: TEdit;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeItemEmployee: TdxDBGridLayout;
    LabelEmpDesc: TLabel;
    gBoxProcessPlanned: TGroupBox;
    gBoxAvailableFreeTime: TGroupBox;
    btnProcessPlanned: TButton;
    btnCalculateWorktimeReduction: TButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    edtHoliday: TEdit;
    edtWorktimeReduction: TEdit;
    edtTimeForTime: TEdit;
    BitBtnFirst: TBitBtn;
    BitBtnPrev: TBitBtn;
    BitBtnNext: TBitBtn;
    BitBtnLast: TBitBtn;
    Label12: TLabel;
    edtContractHoursPerWeek: TEdit;
    gBoxProdHours: TGroupBox;
    Label13: TLabel;
    dxTLProdHours: TdxTreeList;
    dxTLProdHoursColumnWkJob: TdxTreeListColumn;
    dxTLProdHoursColumnHours: TdxTreeListColumn;
    edtTotalProdHours: TEdit;
    Label15: TLabel;
    dxTLPlannedColumnPlant: TdxTreeListColumn;
    dxTLAvailableColumnPlant: TdxTreeListColumn;
    pnlScansClient: TPanel;
    dxTLScans: TdxTreeList;
    dxTLScansColumnDateTimeIn: TdxTreeListColumn;
    dxTLScansColumnDateTimeOut: TdxTreeListColumn;
    dxTLScansColumnPlant: TdxTreeListColumn;
    dxTLScansColumnWorkspot: TdxTreeListColumn;
    dxTLScansColumnJob: TdxTreeListColumn;
    dxTLScansColumnShift: TdxTreeListColumn;
    pnlScansRight: TPanel;
    pnlScans2Client: TPanel;
    btnScans: TButton;
    pnlScans2Bottom: TPanel;
    cBoxCutOffTime: TCheckBox;
    edtBreaks: TEdit;
    Label16: TLabel;
    Label14: TLabel;
    LabelStart: TLabel;
    cBoxShowOnlyActive: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure EmployeeFilter(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure MonthCalendarClick(Sender: TObject);
    procedure btnEmployeeClick(Sender: TObject);
    procedure btnShiftScheduleClick(Sender: TObject);
    procedure btnHoursClick(Sender: TObject);
    procedure btnScansClick(Sender: TObject);
    procedure btnAvailabiltyClick(Sender: TObject);
    procedure cBoxCutOffTimeClick(Sender: TObject);
    procedure btnPlanningClick(Sender: TObject);
    procedure btnProcessPlannedClick(Sender: TObject);
    procedure btnCalculateWorktimeReductionClick(Sender: TObject);
    procedure BitBtnFirstClick(Sender: TObject);
    procedure BitBtnPrevClick(Sender: TObject);
    procedure BitBtnNextClick(Sender: TObject);
    procedure BitBtnLastClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cBoxShowOnlyActiveClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    FEmployeeNumber: Integer;
    FDate: TDateTime;
    FAbsenceReasonCode: String;
    FShiftNumber: Integer;
    FTotalSalaryMinutes: Integer;
    FTotalAbsenceMinutes: Integer;
    FTotalPlannedSalaryMinutes: Integer;
    FTotalPlannedAbsenceMinutes: Integer;
    FEmployeeDepartmentCode: String;
    FPlantCode: String;
    FEmployeeNumberPrevious: Integer;
    FYearPrevious: Word;
    FTeamCode: String;
    RefreshAvailableTime: Boolean;
    FTotalProdMinutes: Integer;
    FMyContractGroupCode: String;
    FMyEmpIsActive: Boolean;
    FMyEmpActiveInFuture: Boolean;
    FMyUserTeamSelectionYN: String;
    { Private declarations }
    procedure MainAction;
    procedure ShowShift;
    function GetAbsenceReason: String;
    function GetShift: String;
    function GetPlant: String;
//    function GetDepartment: String;
    function GetTeam: String;
    procedure DetermineShiftStartEndTB(
      const ADate: TDateTime;
      const APlantCode, ADepartmentCode: String;
      const AEmployeeNumber, AShiftNumber, ATimeBlockNumber: Integer;
      VAR AStart, AEnd: TDateTime;
      VAR AValid: Boolean);
    procedure GetPlanning;
    procedure GetAvailability;
    procedure GetSalaryHours;
    procedure GetAbsenceHours;
    procedure GetScans;
    procedure GetAvailableFreeTime;
    procedure SetDate(const Value: TDateTime);
    procedure SetEmployeeNumber(const Value: Integer);
    procedure EmployeeDateFilter;
    procedure SetAbsenceReasonCode(const Value: String);
    procedure SetShiftNumber(const Value: Integer);
    procedure SetTotalAbsenceMinutes(const Value: Integer);
    procedure SetTotalSalaryMinutes(const Value: Integer);
    procedure SetTotalPlannedAbsenceMinutes(const Value: Integer);
    procedure SetTotalPlannedSalaryMinutes(const Value: Integer);
    procedure SetEmployeeDepartmentCode(const Value: String);
    procedure SwitchForm(AForm: TForm; const ATag: Integer);
    procedure SwitchFormInit; // 20012155
    procedure SwitchFormEnd; // 20012155
    procedure InitCutOffTime;
    procedure SetPlantCode(const Value: String);
    procedure SetEmployeeNumberPrevious(const Value: Integer);
    procedure SetYearPrevious(const Value: Word);
    procedure SetTeamCode(const Value: String);
    //CAR 550285
    procedure GoToRecord(Which: Integer);
    procedure GetContractHoursPerWeek;
    procedure GetProductionHours;
    function EmployeeOtherTeamCheck: Boolean;
  public
    { Public declarations }
    property MyEmployeeNumber: Integer read FEmployeeNumber
      write SetEmployeeNumber;
    property MyPlantCode: String read FPlantCode write SetPlantCode;
    property MyTeamCode: String read FTeamCode write SetTeamCode;
    property MyEmployeeDepartmentCode: String read FEmployeeDepartmentCode
      write SetEmployeeDepartmentCode;
    property MyDate: TDateTime read FDate write SetDate;
    property MyAbsenceReasonCode: String read FAbsenceReasonCode
      write SetAbsenceReasonCode;
    property MyShiftNumber: Integer read FShiftNumber write SetShiftNumber;
    property MyTotalSalaryMinutes: Integer read FTotalSalaryMinutes write
      SetTotalSalaryMinutes;
    property MyTotalProdMinutes: Integer read FTotalProdMinutes write
      FTotalProdMinutes;
    property MyTotalAbsenceMinutes: Integer read FTotalAbsenceMinutes write
      SetTotalAbsenceMinutes;
    property MyTotalPlannedSalaryMinutes: Integer
      read FTotalPlannedSalaryMinutes write SetTotalPlannedSalaryMinutes;
    property MyTotalPlannedAbsenceMinutes: Integer
      read FTotalPlannedAbsenceMinutes write SetTotalPlannedAbsenceMinutes;
    property MyEmployeeNumberPrevious: Integer read FEmployeeNumberPrevious
      write SetEmployeeNumberPrevious;
    property MyYearPrevious: Word read FYearPrevious
      write SetYearPrevious;
    property MyContractGroupCode: String read FMyContractGroupCode
      write FMyContractGroupCode; // RV082.5.
    property MyEmpIsActive: Boolean read FMyEmpIsActive write FMyEmpIsActive;
    property MyEmpActiveInFuture: Boolean read FMyEmpActiveInFuture
      write FMyEmpActiveInFuture default False; // TD-22429
    property MyUserTeamSelectionYN: String
      read FMyUserTeamSelectionYN write FMyUserTeamSelectionYN;
  end;

var
  EmployeeRegsF: TEmployeeRegsF;

implementation

{$R *.DFM}

uses
  UGlobalFunctions, UScannedIDCard, CalculateTotalHoursDMT, UPimsConst,
  UPimsMessageRes, ListProcsFRM, EmployeeFRM, ShiftScheduleFRM,
  HoursPerEmployeeFRM, TimeRecScanningFRM, EmployeeAvailabilityFRM,
  DialogStaffPlanningFRM, DialogProcessAbsenceHrsFRM,
  DialogCalculatedEarnedWTRFRM, {HoursPerEmployeeDMT, } GridBaseFRM,
  EmployeeRegsBaseFRM,
  SideMenuUnitMaintenanceFRM, UMenuOptions, GlobalDMT;

procedure TEmployeeRegsF.FormCreate(Sender: TObject);
begin
  inherited;
  MyUserTeamSelectionYN := SystemDM.UserTeamSelectionYN;
//  if SystemDM.EmbedDialogs then
//    FormStyle := fsStayOnTop;
  // Open the TClientDataSets
  EmployeeRegsDM.cdsFilterEmployee.Open;
  EmployeeRegsDM.cdsShift.Open;
  EmployeeRegsDM.cdsJob.Open;
  EmployeeRegsDM.cdsPlant.Open;
  EmployeeRegsDM.cdsDepartment.Open;
  EmployeeRegsDM.cdsTeam.Open;
  EmployeeRegsDM.cdsAbsenceReason.Open;
  MonthCalendar.Date := Trunc(Date);
  LabelEmpDesc.Caption :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
  LabelStart.Caption := ''; // TD-22429

  MyEmpIsActive := True;
  MyEmployeeNumberPrevious := -1;
  MyContractGroupCode := ''; // RV082.5.
  MyYearPrevious := 0;
  RefreshAvailableTime := False;

  // RV075.7.
  EmployeeRegsBaseF := TEmployeeRegsBaseF.Create(Application);
  EmployeeRegsBaseF.Hide;

  // RV075.8
  // Set menu-item-id's to tags of buttons here.
  // These are used during SwitchForm!
  btnEmployee.Tag := MENU_ITEM_EMPLOYEES;
  btnShiftSchedule.Tag := MENU_SHIFT_SCHEDULE;
  btnPlanning.Tag := MENU_STAFF_PLANNING;
  btnAvailabilty.Tag := MENU_EMPLOYEE_AVAILABILITY;
  btnHours.Tag := MENU_ITEM_HOURSPEREMPLOYEE;
  btnScans.Tag := MENU_ITEM_TIMERECORDINGSCAN;
  btnProcessPlanned.Tag := MENU_ITEM_PROCESSABSENCE;
  btnCalculateWorktimeReduction.Tag := MENU_ITEM_CALCULATE_EARNED_WTR;
  // RV075.8 Store the old item for the main menu.
  SideMenuUnitMaintenanceF.LastDxSideBarItem :=
    SideMenuUnitMaintenanceF.dxSideBar.SelectedItem;
  // 20012944
//  SystemDM.MonthCalendarWeeknumberProperties(MonthCalendar);

//  MainAction;
  if not SystemDM.UseShiftDateSystem then
  begin
    gBoxDate.Caption := SDate;
  end;
end;

procedure TEmployeeRegsF.ShowShift;
var
  PlantCode: String;
  ShiftNumber: Integer;
  ShiftDescription: String;
begin
  edtShift.Color := clWindow;
  if MyShiftNumber = -1 then
    edtShift.Text := ''
  else
  begin
    if EmployeeRegsDM.DetermineShiftSchedule(MyDate, MyEmployeeNumber,
      PlantCode, ShiftNumber, ShiftDescription) then
    begin
      edtShift.Text := PlantCode + ' - ' + IntToStr(ShiftNumber) + ' ' +
        ShiftDescription;
      if (PlantCode <> MyPlantCode) then
        edtShift.Color := clOtherPlant;
    end
    else
      edtShift.Text := IntToStr(MyShiftNumber) + ' ' + GetShift;
  end;
end;

function TEmployeeRegsF.GetAbsenceReason: String;
begin
  Result := '';
  if EmployeeRegsDM.cdsAbsenceReason.FindKey([MyAbsenceReasonCode]) then
    Result :=
      EmployeeRegsDM.cdsAbsenceReason.FieldByName('DESCRIPTION').AsString;
end;

function TEmployeeRegsF.GetShift: String;
begin
  Result := '';
  if EmployeeRegsDM.cdsShift.FindKey([MyPlantCode, MyShiftNumber]) then
    Result := EmployeeRegsDM.cdsShift.FieldByName('DESCRIPTION').AsString;
end;

function TEmployeeRegsF.GetPlant: String;
begin
  Result := '';
  if EmployeeRegsDM.cdsPlant.FindKey([MyPlantCode]) then
    Result := EmployeeRegsDM.cdsPlant.FieldByName('DESCRIPTION').AsString;
end;
(*
function TEmployeeRegsF.GetDepartment: String;
begin
  Result := '';
  if EmployeeRegsDM.cdsDepartment.FindKey([MyPlantCode,
    MyEmployeeDepartmentCode]) then
    Result := EmployeeRegsDM.cdsDepartment.FieldByName('DESCRIPTION').AsString;
end;
*)
function TEmployeeRegsF.GetTeam: String;
begin
  Result := '';
  if EmployeeRegsDM.cdsTeam.FindKey([MyTeamCode]) then
    Result := EmployeeRegsDM.cdsTeam.FieldByName('DESCRIPTION').AsString;
end;

// Determine for 1 TimeBlock the start and end time
procedure TEmployeeRegsF.DetermineShiftStartEndTB(
  const ADate: TDateTime;
  const APlantCode, ADepartmentCode: String;
  const AEmployeeNumber, AShiftNumber, ATimeBlockNumber: Integer;
  VAR AStart, AEnd: TDateTime;
  VAR AValid: Boolean);
var
  Day: Integer;
  SDay: String;
  Ready: Boolean;
  ADataSet: TDataSet;
begin
  Day:= DayInWeek(SystemDM.WeekStartsOn, ADate);
  SDay := IntToStr(Day);

  // Init the start and end
  AStart := Trunc(ADate);
  AEnd := Trunc(ADate);
  AValid := False;

  // Timeblocks Per Employee
  ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
  ADataSet.Filtered := False;
  ADataSet.Filter :=
    'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) + '''' +
    ' AND ' +
    'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
    'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) + ' AND ' +
    'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
  ADataSet.Filtered := True;
  Ready := not ADataSet.IsEmpty;

  // Timeblocks Per Department
  if not Ready then
  begin
    // Timeblocks per Department
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
      '''' + ' AND ' +
      'DEPARTMENT_CODE = ''' + DoubleQuote(ADepartmentCode) +
      '''' + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) + ' AND ' +
      'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
    ADataSet.Filtered := True;
    Ready := not ADataSet.IsEmpty;

    // Timeblocks Per Shift
    if not Ready then
    begin
      // Timeblocks Per Shift
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + '''' + DoubleQuote(APlantCode) +
        '''' + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) + ' AND ' +
        'TIMEBLOCK_NUMBER = ' + IntToStr(ATimeBlockNumber);
      ADataSet.Filtered := True;
      Ready := not ADataSet.IsEmpty;
    end;
  end;

  if Ready then
  begin
    // Search and Set a valid STARTTIME
    // Start looking for FIRST valid timeblock to LAST.
    ADataSet.First;
    while not ADataSet.Eof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AStart :=
          Trunc(ADate) +
          Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime);
        AValid := True;
        Break;
      end;
      ADataSet.Next;
    end;
    // Search and Set a valid ENDDTIME
    // Start looking from LAST valid timeblock to FIRST.
    ADataSet.Last;
    while not ADataSet.Bof do
    begin
      // If times are not the same, it's a valid time
      // otherwise they are not valid, like
      // Starttime='00:00' and endtime='00:00'
      if Frac(ADataSet.FieldByName('STARTTIME' + SDay).AsDateTime) <>
        Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime) then
      begin
        AEnd :=
          Trunc(ADate) +
          Frac(ADataSet.FieldByName('ENDTIME' + SDay).AsDateTime);
        begin
          AValid := True;
          Break;
        end;
      end;
      ADataSet.Prior;
    end;
  end;
end;

procedure TEmployeeRegsF.GetPlanning;
var
  Item: array[1..MAX_TBS] of TdxTreeListNode;
  TB: Integer;
  Field: String;
  AStart, AEnd: TDateTime;
  AValid: Boolean;
  AEmployeeData: TScannedIDCard;
  ProdMin, BreaksMin, PayedBreaks: Integer;
begin
  dxTLPlanned.ClearNodes;
  MyTotalPlannedSalaryMinutes := 0;
  edtPlannedSalaryHours.Text := IntMin2StringTime(0, True);
  edtShift.Text := '';
  // RV092.13. Change color.
  dxTLPlannedColumnPlant.Color := clWindow;
  with EmployeeRegsDM do
  begin
    // Get Planning-data
    qryPlanning.Close;
    qryPlanning.ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime := MyDate;
    qryPlanning.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qryPlanning.Open;
    if not qryPlanning.IsEmpty then
    begin
      // Create n items and initialize them
      for TB := 1 to SystemDM.MaxTimeblocks do
      begin
        Item[TB] := dxTLPlanned.Add;
        Item[TB].Values[0] := IntToStr(TB); // Tb
        Item[TB].Values[1] := '';           // Plant // RV092.13. Add plant.
        Item[TB].Values[2] := '';           // Workspot
      end;
      qryPlanning.First;
      while not qryPlanning.Eof do
      begin
        MyShiftNumber := qryPlanning.FieldByName('SHIFT_NUMBER').AsInteger;
        for TB := 1 to SystemDM.MaxTimeblocks do
        begin
          Field := qryPlanning.FieldByName('SCHEDULED_TIMEBLOCK_' +
            IntToStr(TB)).AsString;
          if Field <> '' then
          begin
            if Field[1] in ['A','B','C'] then
            begin
              // RV092.13. Add planning info.
              Item[TB].Values[1] :=
                qryPlanning.FieldByName('PLANT_CODE').AsString;
              // RV092.13. Change color.
              if MyPlantCode <> qryPlanning.FieldByName('PLANT_CODE').AsString then
                dxTLPlannedColumnPlant.Color := clOtherPlant
              else
                dxTLPlannedColumnPlant.Color := clWindow;
              Item[TB].Values[2] :=
                qryPlanning.FieldByName('WORKSPOT_CODE').AsString + ' ' +
                  qryPlanning.FieldByName('WDESCRIPTION').AsString;
              // Now get total 'planned salary hours'
              DetermineShiftStartEndTB(
                MyDate, qryPlanning.FieldByName('PLANT_CODE').AsString,
                qryPlanning.FieldByName('DEPARTMENT_CODE').AsString,
                MyEmployeeNumber, MyShiftNumber, TB,
                AStart, AEnd, AValid);
              if AValid then
              begin
//                BlockMin := Round((AEnd - AStart) * DayToMin);
                AEmployeeData.EmployeeCode := MyEmployeeNumber;
                AEmployeeData.PlantCode :=
                  qryPlanning.FieldByName('PLANT_CODE').AsString;
                AEmployeeData.ShiftNumber := MyShiftNumber;
                AEmployeeData.DepartmentCode := MyEmployeeDepartmentCode;
                // Get the ProdMin (without breaks).
                AProdMinClass.ComputeBreaks(AEmployeeData, AStart, AEnd, 0,
                  ProdMin, BreaksMin, PayedBreaks);
                MyTotalPlannedSalaryMinutes := MyTotalPlannedSalaryMinutes +
                  ProdMin;
              end;
            end
            else
              if Item[TB].Values[2] = '' then
                Item[TB].Values[2] := Field;
          end;
        end;
        qryPlanning.Next;
      end;
      qryPlanning.Close;
    end;
  end;
  edtPlannedSalaryHours.Text :=
    IntMin2StringTime(MyTotalPlannedSalaryMinutes, True);
end; // GetPlanning

procedure TEmployeeRegsF.GetAvailability;
var
  Item: TdxTreeListNode;
  TB: Integer;
  Field: String;
  AStart, AEnd: TDateTime;
  AValid: Boolean;
  AEmployeeData: TScannedIDCard;
  ProdMin, BreaksMin, PayedBreaks: Integer;
begin
  dxTLAvailable.ClearNodes;
  MyTotalPlannedAbsenceMinutes := 0;
  edtPlannedAbsenceHours.Text := IntMin2StringTime(0, True);
  edtPlannedHours.Text := IntMin2StringTime(0, True);
  // RV092.13. Change color.
  dxTLAvailable.HeaderColor := clBtnFace;
  dxTLAvailableColumnPlant.Color := clWindow;
  with EmployeeRegsDM do
  begin
    qryAvailable.Close;
    qryAvailable.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := MyDate;
    qryAvailable.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qryAvailable.Open;
    if not qryAvailable.IsEmpty then
    begin
      qryAvailable.First;
      while not qryAvailable.Eof do
      begin
        MyShiftNumber := qryAvailable.FieldByName('SHIFT_NUMBER').AsInteger;
        for TB := 1 to SystemDM.MaxTimeblocks do
        begin
          Item := dxTLAvailable.Add;
          Item.Values[0] := IntToStr(TB); // Tb
          Item.Values[1] := ''; // Plant // RV092.13. Add plant.
          Item.Values[2] := ''; // Availability
          Field := qryAvailable.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(TB)).AsString;
          if Field <> '' then
          begin
            if Field[1] in ['*','-'] then
            begin
              // RV092.13. Add plant.
              Item.Values[1] :=
                qryAvailable.FieldByName('PLANT_CODE').AsString;
              // RV092.13. Change color.
              if MyPlantCode <> qryAvailable.FieldByName('PLANT_CODE').AsString then
                dxTLAvailableColumnPlant.Color := clOtherPlant
              else
                dxTLAvailableColumnPlant.Color := clWindow;
              Item.Values[2] := Field;
            end
            else
            begin
              MyAbsenceReasonCode := Field;
              Item.Values[2] := Field + ' ' + GetAbsenceReason;
              // Now get total 'planned absence hours'
              DetermineShiftStartEndTB(
                MyDate, qryAvailable.FieldByName('PLANT_CODE').AsString,
                MyEmployeeDepartmentCode, MyEmployeeNumber, MyShiftNumber, TB,
                AStart, AEnd, AValid);
              if AValid then
              begin
//                BlockMin := Round((AEnd - AStart) * DayToMin);
                AEmployeeData.EmployeeCode := MyEmployeeNumber;
                AEmployeeData.PlantCode :=
                  qryAvailable.FieldByName('PLANT_CODE').AsString;
                AEmployeeData.ShiftNumber := MyShiftNumber;
                AEmployeeData.DepartmentCode := MyEmployeeDepartmentCode;
                // Get the ProdMin (without breaks)
                AProdMinClass.ComputeBreaks(AEmployeeData, AStart, AEnd, 0,
                  ProdMin, BreaksMin, PayedBreaks);
                MyTotalPlannedAbsenceMinutes := MyTotalPlannedAbsenceMinutes +
                  ProdMin;
              end;
            end;
          end;
        end;
        qryAvailable.Next;
      end;
      qryAvailable.Close;
    end;
  end;
  edtPlannedAbsenceHours.Text :=
    IntMin2StringTime(MyTotalPlannedAbsenceMinutes, True);
  edtPlannedHours.Text := IntMin2StringTime(MyTotalPlannedSalaryMinutes +
    MyTotalPlannedAbsenceMinutes, True);
end; // GetAvailability

procedure TEmployeeRegsF.GetSalaryHours;
var
  Item: TdxTreeListNode;
begin
  edtTotalSalaryHours.Text := IntMin2StringTime(0, True);
  edtTotalHours.Text := IntMin2StringTime(0, True);

  MyTotalSalaryMinutes := 0;
  dxTLSalaryHours.ClearNodes;
  with EmployeeRegsDM do
  begin
    qrySalaryHours.Close;
    qrySalaryHours.ParamByName('SALARY_DATE').AsDateTime := MyDate;
    qrySalaryHours.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qrySalaryHours.Open;
    if not qrySalaryHours.IsEmpty then
    begin
      qrySalaryHours.First;
      while not qrySalaryHours.Eof do
      begin
        Item := dxTLSalaryHours.Add;
        Item.Values[0] :=
          qrySalaryHours.FieldByName('HOURTYPE_NUMBER').AsString + ' ' +
            qrySalaryHours.FieldByName('HDESCRIPTION').AsString;
        Item.Values[1] :=
          IntMin2StringTime(
            qrySalaryHours.FieldByName('SALARY_MINUTE').AsInteger, True);
        MyTotalSalaryMinutes := MyTotalSalaryMinutes +
          qrySalaryHours.FieldByName('SALARY_MINUTE').AsInteger;
        qrySalaryHours.Next;
      end;
    end;
    qrySalaryHours.Close;
  end;
  edtTotalSalaryHours.Text := IntMin2StringTime(MyTotalSalaryMinutes, True);
end;

//RV065.9
procedure TEmployeeRegsF.GetProductionHours;
var
  Item: TdxTreeListNode;
begin
  edtTotalProdHours.Text := IntMin2StringTime(0, True);

  MyTotalProdMinutes := 0;
  dxTLProdHours.ClearNodes;
  with EmployeeRegsDM do
  begin
    qryProdHours.Close;
    qryProdHours.ParamByName('PROD_DATE').AsDateTime := MyDate;
    qryProdHours.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qryProdHours.Open;
    if not qryProdHours.IsEmpty then
    begin
      qryProdHours.First;
      while not qryProdHours.Eof do
      begin
        Item := dxTLProdHours.Add;
        Item.Values[0] :=
          qryProdHours.FieldByName('DESCR').AsString;
        Item.Values[1] :=
          IntMin2StringTime(
            qryProdHours.FieldByName('PRODUCTION_MINUTE').AsInteger, True);
        MyTotalProdMinutes := MyTotalProdMinutes +
          qryProdHours.FieldByName('PRODUCTION_MINUTE').AsInteger;
        qryProdHours.Next;
      end;
    end;
    qryProdHours.Close;
  end;
  edtTotalProdHours.Text := IntMin2StringTime(MyTotalProdMinutes, True);
end;

procedure TEmployeeRegsF.GetContractHoursPerWeek;
var
  ContractMins: Integer;
begin
  ContractMins := 0;

  try
    with EmployeeRegsDM do
    begin
      qryEmployeeContract.Close;
      qryEmployeeContract.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
      qryEmployeeContract.ParamByName('START_DATE')     .AsDateTime := MyDate;
      qryEmployeeContract.ParamByName('END_DATE')       .AsDateTime := MyDate;
      qryEmployeeContract.Open;
      if not qryEmployeeContract.IsEmpty then
      begin
        qryEmployeeContract.First;
        if not qryEmployeeContract.Eof then
          ContractMins := Round(qryEmployeeContract.FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat * 60);
      end;
      qrySalaryHours.Close;
    end;
  finally
    edtContractHoursPerWeek.Text := IntMin2StringTime(ContractMins, True);
  end;
end;

procedure TEmployeeRegsF.GetAbsenceHours;
var
  Item: TdxTreeListNode;
begin
  edtTotalAbsenceHours.Text := IntMin2StringTime(0, True);

  MyTotalAbsenceMinutes := 0;
  dxTLAbsenceHours.ClearNodes;
  with EmployeeRegsDM do
  begin
    qryAbsenceHours.Close;
    qryAbsenceHours.ParamByName('ABSENCEHOUR_DATE').AsDateTime := MyDate;
    qryAbsenceHours.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qryAbsenceHours.Open;
    if not qryAbsenceHours.IsEmpty then
    begin
      qryAbsenceHours.First;
      while not qryAbsenceHours.Eof do
      begin
        Item := dxTLAbsenceHours.Add;
        Item.Values[0] :=
          qryAbsenceHours.FieldByName('ABSENCEREASON_CODE').AsString + ' ' +
            qryAbsenceHours.FieldByName('ADESCRIPTION').AsString;
        Item.Values[1] :=
          IntMin2StringTime(
            qryAbsenceHours.FieldByName('ABSENCE_MINUTE').AsInteger, True);
        MyTotalAbsenceMinutes := MyTotalAbsenceMinutes +
          qryAbsenceHours.FieldByName('ABSENCE_MINUTE').AsInteger;
        qryAbsenceHours.Next;
      end;
    end;
    qryAbsenceHours.Close;
  end;
  edtTotalAbsenceHours.Text := IntMin2StringTime(MyTotalAbsenceMinutes, True);

  // totals
  edtTotalHours.Text := IntMin2StringTime(
    MyTotalSalaryMinutes + MyTotalAbsenceMinutes, True);
end;

// 20013489
// Filter on SHIFT_DATE instead of on Scan-date.
procedure TEmployeeRegsF.GetScans;
var
  Item: TdxTreeListNode;
  AEmployeeData: TScannedIDCard; // 20012142
  ProdMin, BreaksMin, PayedBreaks: Integer; // 20012142
  TotalBreaksMin: Integer; // 20012142
  // 20012142.10 Round (break-) minutes to 15 minutes.
  function RoundMinutes(AMinutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := AMinutes DIV 60;
    Mins := AMinutes MOD 60;
    Mins := Round(Mins / 15) * 15;
    Result := Hours * 60 + Mins;
  end;
begin
  TotalBreaksMin := 0; // 20012142
  edtBreaks.Text := IntMin2StringTime(0, True); // 20012142
  dxTLScans.ClearNodes;
  with EmployeeRegsDM do
  begin
    qryScans.Close;
    // 20014327
    if SystemDM.UseShiftDateSystem then
      qryScans.ParamByName('ONSHIFT').AsInteger := 1
    else
      qryScans.ParamByName('ONSHIFT').AsInteger := 0;
    // 20013489
    qryScans.ParamByName('DATEFROM').AsDateTime := Trunc(MyDate);
    qryScans.ParamByName('DATETO').AsDateTime := Trunc(MyDate + 1);
//    qryScans.ParamByName('SHIFT_DATE').AsDateTime := Trunc(MyDate);
    qryScans.ParamByName('EMPLOYEE_NUMBER').AsInteger := MyEmployeeNumber;
    qryScans.Open;
    if not qryScans.IsEmpty then
    begin
      qryScans.First;
      while not qryScans.Eof do
      begin
        Item := dxTLScans.Add;
        Item.Values[0] :=
//          DateTimeToStr(RoundTime(qryScans.FieldByName('DATETIME_IN').AsDateTime, 1)); // 20015346
          DateTimeToStr(qryScans.FieldByName('DATETIME_IN').AsDateTime); // 20014450.50
        Item.Values[1] :=
//          DateTimeToStr(RoundTime(qryScans.FieldByName('DATETIME_OUT').AsDateTime, 1)); // 20015346
          DateTimeToStr(qryScans.FieldByName('DATETIME_OUT').AsDateTime); // 20014450.50
        Item.Values[2] :=
          qryScans.FieldByName('PLANT_CODE').AsString + ' ' +
            qryScans.FieldByName('PDESCRIPTION').AsString;
        Item.Values[3] :=
          qryScans.FieldByName('WORKSPOT_CODE').AsString + ' ' +
            qryScans.FieldByName('WDESCRIPTION').AsString;
        if qryScans.FieldByName('JOB_CODE').AsString = '0' then
          Item.Values[4] := ''
        else
          Item.Values[4] := qryScans.FieldByName('JOB_CODE').AsString + ' ' +
            qryScans.FieldByName('CALCJDESCRIPTION').AsString;
        Item.Values[5] :=
          qryScans.FieldByName('SHIFT_NUMBER').AsString + ' ' +
            qryScans.FieldByName('SDESCRIPTION').AsString;
        try
          MyShiftNumber := qryScans.FieldByName('SHIFT_NUMBER').AsInteger;
        except
          MyShiftNumber := -1;
        end;
        // 20012142
        AEmployeeData.EmployeeCode := MyEmployeeNumber;
        AEmployeeData.PlantCode :=
          qryScans.FieldByName('PLANT_CODE').AsString;
        AEmployeeData.ShiftNumber := MyShiftNumber;
        // Department must be taken from workspot!
        AEmployeeData.DepartmentCode :=
          qryScans.FieldByName('DEPARTMENT_CODE').AsString;
          // MyEmployeeDepartmentCode;
        // Get the breaks
        AProdMinClass.ComputeBreaks(AEmployeeData,
          qryScans.FieldByName('DATETIME_IN').AsDateTime,
          qryScans.FieldByName('DATETIME_OUT').AsDateTime, 0,
          ProdMin, BreaksMin, PayedBreaks);
        TotalBreaksMin := TotalBreaksMin + BreaksMin - PayedBreaks;
        qryScans.Next;
      end;
    end;
    qryScans.Close;
  end;
  edtBreaks.Text := IntMin2StringTime(
    RoundMinutes(TotalBreaksMin), // 20012142.10
    True); // 20012142
end;

procedure TEmployeeRegsF.GetAvailableFreeTime;
var
{  AFilter: TFilterData; }
  AYear, AWeek: Word;
  Remaining, Normal, Used, Planned, Available: String;
begin
  ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
  if RefreshAvailableTime or
    (MyEmployeeNumber <> MyEmployeeNumberPrevious) or
    (AYear <> MyYearPrevious) then
  begin
    edtHoliday.Text := IntMin2StringTime(0, True);
    edtWorktimeReduction.Text := IntMin2StringTime(0, True);
    edtTimeForTime.Text := IntMin2StringTime(0, True);

    try
      // RV079.4. Old:
{
      HoursPerEmployeeDM := THoursPerEmployeeDM.Create(Application);
      with HoursPerEmployeeDM do
      begin
        QueryWorkspotLU.Open;
        QueryShiftLU.Open;
        QueryJobLU.Open;
        cdsHREPRODUCTION.CreateDataSet;
        cdsHRESALARY.CreateDataSet;
        cdsHREABSENCE.CreateDataSet;
      end;
}

      // RV079.4. New:
      // Use a GlobalDMT-class here to get the balances.
      ABalanceCounters.EmployeeNumber := MyEmployeeNumber;
      ABalanceCounters.Year := AYear;
      ABalanceCounters.DateFrom := Trunc(Now);

      ABalanceCounters.GetCounters;

      ABalanceCounters.ComputeHoliday(Remaining, Normal, Used, Planned,
        Available);
      edtHoliday.Text := Available;
      ABalanceCounters.ComputeWorkTime(Remaining, Normal, Used, Planned,
        Available);
      edtWorktimeReduction.Text := Available;
      ABalanceCounters.ComputeTimeForTime(Remaining, Normal, Used, Planned,
        Available);
      edtTimeForTime.Text := Available;

      // RV079.4. Old:
{
      AFilter := HoursPerEmployeeDM.FilterData;
      AFilter.Employee_Number := MyEmployeeNumber;
      Afilter.Year := AYear;
      HoursPerEmployeeDM.FilterData := AFilter;

      // RV067.MRA.5.
      //RV067.4.
      //Get all counters here
      HoursPerEmployeeDM.GetCounters;
      HoursPerEmployeeDM.ComputeHoliday(Remaining, Normal, Used, Planned,
        Available);
      edtHoliday.Text := Available;
      HoursPerEmployeeDM.ComputeWorkTime(Remaining, Normal, Used, Planned,
        Available);
      edtWorktimeReduction.Text := Available;
      HoursPerEmployeeDM.ComputeTimeForTime(Remaining, Normal, Used, Planned,
        Available);
      edtTimeForTime.Text := Available;
}
    finally
      RefreshAvailableTime := False;
      // RV079.4. Old:
{      HoursPerEmployeeDM.Free; }
    end;
  end;
  MyEmployeeNumberPrevious := MyEmployeeNumber;
  MyYearPrevious := AYear;
end;

procedure TEmployeeRegsF.InitCutOffTime;
begin
  CBoxCutOffTime.Checked :=
    (EmployeeRegsDM.cdsFilterEmployee.
      FieldByName('CUT_OF_TIME_YN').AsString = 'Y');
end;

procedure TEmployeeRegsF.MainAction;
  // 20012155 Allow changes for employee worked in other team
(*
  // RV067.MRA.25.
  // When employee had production on other plant then its own,
  // then disable buttons.
  procedure EnableButtons(AEnable: Boolean);
  begin
    btnEmployee.Enabled := AEnable;
    btnShiftSchedule.Enabled := AEnable;
    btnPlanning.Enabled := AEnable;
    btnAvailabilty.Enabled := AEnable;
    btnHours.Enabled := AEnable;
    btnProcessPlanned.Enabled := AEnable;
    btnCalculateWorktimeReduction.Enabled := AEnable;
    btnScans.Enabled := AEnable;
  end;
  // RV067.MRA.25.
  procedure ProductionPlantCheck;
  begin
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    begin
      with EmployeeRegsDM.cdsFilterEmployee do
      begin
        // Has employee worked on other plant?
        if FieldByName('EMP_PLANT_IN_TEAM').AsString = '0' then
          EnableButtons(False)
        else
          EnableButtons(True);
      end
    end;
  end;
*)
begin
  // RV067.MRA.25.
  // 20012155 Allow changes for employee worked in other team
//  ProductionPlantCheck;
  // Set parameters
  MyEmployeeNumber :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  // RV082.5.
  MyContractGroupCode :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('CONTRACTGROUP_CODE').AsString;
  MyPlantCode :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('PLANT_CODE').AsString;
  MyEmployeeDepartmentCode :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('DEPARTMENT_CODE').AsString;
  MyTeamCode :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('TEAM_CODE').AsString;
  InitCutOffTime;
  MyDate := Trunc(MonthCalendar.Date);
  MyShiftNumber := -1;
  LabelEmpDesc.Caption :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString +
    ' (' +
    SPimsTeam + // 'Team '
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('TEAM_CODE').AsString +
    ' ' + GetTeam + ')';

  // RV067.MRA.6. SO-550482.
  if EmployeeRegsDM.cdsFilterEmployee.FieldByName('ENDDATE').AsString <> '' then
  begin
    // RV094.8. Also check for start-date!
    if (MyDate >= EmployeeRegsDM.cdsFilterEmployee.
      FieldByName('STARTDATE').AsDateTime) and
      (MyDate <= EmployeeRegsDM.cdsFilterEmployee.
      FieldByName('ENDDATE').AsDateTime) then
      MyEmpIsActive := True
    else
      MyEmpIsActive := False;
  end
  else
  begin
    // RV094.8. Also check for start-date!
    if (MyDate >= EmployeeRegsDM.cdsFilterEmployee.
      FieldByName('STARTDATE').AsDateTime) then
      MyEmpIsActive := True
    else
      MyEmpIsActive := False;
  end;
  MyEmpActiveInFuture := False; // TD-22429
  if MyEmpIsActive then
  begin
    dxDBExtLookupEditEmployee.Color := clWindow;
    LabelEmpDesc.Enabled := True;
  end
  else
  begin
    dxDBExtLookupEditEmployee.Color := clScrollBar;
    LabelEmpDesc.Enabled := False;
    if (MyDate < EmployeeRegsDM.cdsFilterEmployee.
      FieldByName('STARTDATE').AsDateTime) then
      MyEmpActiveInFuture := True; // TD-22429
  end;

  // TD-22429
  if MyEmpActiveInFuture then
    LabelStart.Caption := SPimsStart + ': ' +
      DateToStr(EmployeeRegsDM.cdsFilterEmployee.
        FieldByName('STARTDATE').AsDateTime)
  else
    LabelStart.Caption := '';

  EmployeeRegsDM.UpdateSalaryHours(MyEmployeeNumber);
  ShowShift;

  // Get information
  GetPlanning;
  GetAvailability;
  GetSalaryHours;
  GetProductionHours;
  GetAbsenceHours;
  GetScans;
  GetAvailableFreeTime;
  GetContractHoursPerWeek; //RV065.4
  ShowShift;
end;

procedure TEmployeeRegsF.EmployeeDateFilter;
begin
  MainAction;
end;

procedure TEmployeeRegsF.EmployeeFilter(Sender: TObject;
  var Text: String; var Accept: Boolean);
begin
  inherited;
  EmployeeDateFilter;
end;

procedure TEmployeeRegsF.MonthCalendarClick(Sender: TObject);
begin
  inherited;
  // RV067.MRA.25.
  // RV081.1.
  EmployeeRegsDM.EmployeeFilterRefresh(Trunc(MonthCalendar.Date), MyEmployeeNumber);
  EmployeeDateFilter;
end;

procedure TEmployeeRegsF.SetDate(const Value: TDateTime);
begin
  // RV067.MRA.17
  FDate := Trunc(Value);
end;

procedure TEmployeeRegsF.SetEmployeeNumber(const Value: Integer);
begin
  FEmployeeNumber := Value;
  
end;

procedure TEmployeeRegsF.SetAbsenceReasonCode(const Value: String);
begin
  FAbsenceReasonCode := Value;
end;

procedure TEmployeeRegsF.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;

procedure TEmployeeRegsF.SetTotalAbsenceMinutes(const Value: Integer);
begin
  FTotalAbsenceMinutes := Value;
end;

procedure TEmployeeRegsF.SetTotalSalaryMinutes(const Value: Integer);
begin
  FTotalSalaryMinutes := Value;
end;

procedure TEmployeeRegsF.SetTotalPlannedAbsenceMinutes(
  const Value: Integer);
begin
  FTotalPlannedAbsenceMinutes := Value;
end;

procedure TEmployeeRegsF.SetTotalPlannedSalaryMinutes(
  const Value: Integer);
begin
  FTotalPlannedSalaryMinutes := Value;
end;

procedure TEmployeeRegsF.SetEmployeeDepartmentCode(const Value: String);
begin
  FEmployeeDepartmentCode := Value;
end;

procedure TEmployeeRegsF.SetPlantCode(const Value: String);
begin
  FPlantCode := Value;
end;

procedure TEmployeeRegsF.SetEmployeeNumberPrevious(const Value: Integer);
begin
  FEmployeeNumberPrevious := Value;
end;

procedure TEmployeeRegsF.SetYearPrevious(const Value: Word);
begin
  FYearPrevious := Value;
end;

procedure TEmployeeRegsF.SetTeamCode(const Value: String);
begin
  FTeamCode := Value;
end;

procedure TEmployeeRegsF.cBoxCutOffTimeClick(Sender: TObject);
begin
  inherited;
  InitCutOffTime; // The checkbox cannot be changed.
end;

procedure TEmployeeRegsF.SwitchForm(AForm: TForm; const ATag: Integer);
begin
  SideMenuUnitMaintenanceF.SwitchSelectedItem(ATag);

//  AForm.Height := Height - Trunc(Height / 10);
//  AForm.Width := Width - Trunc(Width / 10);
  // MR:03-05-2004
  // RV075.7.
//  EmployeeRegsBaseF.Height := AForm.Height;
//  EmployeeRegsBaseF.Width := AForm.Width;

  // RV075.7.
  try
    EmployeeRegsBaseF.InsertForm := AForm;
    (AForm As TGridBaseF).Form_Edit_YN :=
      SideMenuUnitMaintenanceF.dxSideBar.SelectedItem.CustomData;
    EmployeeRegsBaseF.Caption := AForm.Caption;
    AForm.Show;
{    if SystemDM.EmbedDialogs then
    begin
      AForm.FormStyle := fsStayOnTop;
      EmployeeRegsBaseF.FormStyle := fsStayOnTop;
    end; }
    EmployeeRegsBaseF.ShowModal;
  finally
    EmployeeRegsBaseF.NilInsertForm;
  end;
end;

procedure TEmployeeRegsF.btnEmployeeClick(Sender: TObject);
begin
  inherited;
  try
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SwitchFormInit; // 20012155
    SwitchForm(EmployeeF, btnEmployee.Tag);
  finally
    SwitchFormEnd; // 20012155
    EmployeeRegsDM.cdsFilterEmployee.Refresh;
    MainAction;
  end;
{
  try
    EmployeeF_HND := TEmployeeF.Create(Application);
    InitForm(EmployeeF_HND);
    InitForm(EmployeeF);
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    // MR:10-12-2003 Set 'OnClose' to alternative event!
    //               Otherwise an access-violiation is raised, during
    //               ask-for-save.
    EmployeeF_HND.OnClose := GridBaseF.FormCloseAskForSave;
    // MR:03-05-2004
    EmployeeF_HND.ShowModal;
    EmployeeRegsBaseF.ShowModal;
  finally
    EmployeeF_HND.Free;
    // If employee-data has been changed, refresh it here.
    EmployeeRegsDM.cdsFilterEmployee.Refresh;
    MainAction;
  end;
}
end;

procedure TEmployeeRegsF.btnShiftScheduleClick(Sender: TObject);
var
  AYear, AWeek: Word;
begin
  inherited;
  // RV095.7.
  if (not MyEmpIsActive) and
    (not MyEmpActiveInFuture) {TD-22429} then
  begin
    DisplayMessage(SPimsEmployeeNotActive, mtInformation, [mbOk]);
    Exit;
  end;
  try
    // RV092.16. Save Employee.
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
    SwitchFormInit; // 20012155
    SwitchForm(ShiftScheduleF(MyPlantCode + Str_Sep + GetPlant,
      MyTeamCode + Str_Sep + GetTeam, AYear, AWeek), btnShiftSchedule.Tag);
  finally
    SwitchFormEnd; // 20012155
    MainAction;
  end;
{
  try
    ShiftScheduleF_HDN := TShiftScheduleF.Create(Application);
    InitForm(ShiftScheduleF_HDN);
    ShiftScheduleF_HDN.MyPlant := MyPlantCode + Str_Sep + GetPlant;
    ShiftScheduleF_HDN.MyTeam := MyTeamCode + Str_Sep + GetTeam;
    ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
    ShiftScheduleF_HDN.MyYear := AYear;
    ShiftScheduleF_HDN.MyWeek := AWeek;
    ShiftScheduleF_HDN.OnClose := GridBaseF.FormCloseAskForSave;
    // MR:03-05-2004
//    ShiftScheduleF_HDN.ShowModal;
    EmployeeRegsBaseF.ShowModal;
  finally
    ShiftScheduleF_HDN.Free;
    MainAction;
  end;
}
end;

procedure TEmployeeRegsF.btnHoursClick(Sender: TObject);
begin
  inherited;
  try
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SystemDM.ASaveTimeRecScanning.ADateScanning := MyDate;
    SwitchFormInit; // 20012155
    SwitchForm(HoursPerEmployeeF, btnHours.Tag);
  finally
    SwitchFormEnd; // 20012155
    RefreshAvailableTime := True;
    MainAction;
  end;
{
  try
    HoursPerEmployeeF_HND := THoursPerEmployeeF.Create(Application);
    InitForm(HoursPerEmployeeF_HND);
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SystemDM.ASaveTimeRecScanning.ADateScanning := MyDate;
    HoursPerEmployeeF_HND.OnClose := GridBaseF.FormCloseAskForSave;
    // MR:03-05-2004
//    HoursPerEmployeeF_HND.ShowModal;
    EmployeeRegsBaseF.ShowModal;
  finally
    HoursPerEmployeeF_HND.Free;
    RefreshAvailableTime := True;
    MainAction;
  end;
}
end;

procedure TEmployeeRegsF.btnScansClick(Sender: TObject);
begin
  inherited;
  try
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SystemDM.ASaveTimeRecScanning.ADateScanning := MyDate;
    SystemDM.ASaveTimeRecScanning.AShiftNumber := MyShiftNumber;
    SwitchFormInit; // 20012155
    SwitchForm(TimeRecScanningF, btnScans.Tag);
  finally
    SwitchFormEnd; // 20012155
    RefreshAvailableTime := True;
    MainAction;
  end;
{
  try
    TimeRecScanningF_HND := TTimeRecScanningF.Create(Application);
    InitForm(TimeRecScanningF_HND);
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SystemDM.ASaveTimeRecScanning.ADateScanning := MyDate;
    SystemDM.ASaveTimeRecScanning.AShiftNumber := MyShiftNumber;
    TimeRecScanningF_HND.OnClose := GridBaseF.FormCloseAskForSave;
    // MR:03-05-2004
//    TimeRecScanningF_HND.ShowModal;
    EmployeeRegsBaseF.ShowModal;
  finally
    TimeRecScanningF_HND.Free;
    RefreshAvailableTime := True;
    MainAction;
  end;
}
end;

procedure TEmployeeRegsF.btnAvailabiltyClick(Sender: TObject);
var
  AYear, AWeek: Word;
begin
  inherited;
  // RV094.8.
  if (not MyEmpIsActive) and
    (not MyEmpActiveInFuture) {TD-22429}  then
  begin
//    if dxTLAvailable.Count <= 0 then
    begin
      DisplayMessage(SPimsEmployeeNotActive, mtInformation, [mbOk]);
      Exit;
    end;
  end;
  try
    ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
    // RV094.8. Added 'MyDate' as param.
    SwitchFormInit; // 20012155
    SwitchForm(EmployeeAvailabilityF(MyEmployeeNumber, AYear, AWeek, MyDate),
      btnAvailabilty.Tag);
  finally
    SwitchFormEnd; // 20012155
    RefreshAvailableTime := True;
    MainAction;
  end;
{
  try
    EmployeeAvailabilityF_HND := TEmployeeAvailabilityF.Create(Application);
    InitForm(EmployeeAvailabilityF_HND);
    ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
    EmployeeAvailabilityF_HND.SetFormParameters(MyEmployeeNumber, AYear, AWeek);
    EmployeeAvailabilityF_HND.OnClose := GridBaseF.FormCloseAskForSave;
    // MR:03-05-2004
//    EmployeeAvailabilityF_HND.ShowModal;
    EmployeeRegsBaseF.ShowModal;
  finally
    EmployeeAvailabilityF_HND.Free;
    RefreshAvailableTime := True;
    MainAction;
  end;
}
end;

procedure TEmployeeRegsF.btnPlanningClick(Sender: TObject);
begin
  inherited;
  try
    SwitchFormInit; // 20012155
    DialogStaffPlanningF := TDialogStaffPlanningF.Create(Application);
    // Following should be changed for user-rights?
    DialogStaffPlanningF.Edit_Staff_Planning := '2';
    DialogStaffPlanningF.MyPlant := MyPlantCode + Str_Sep + GetPlant;
    if MyShiftNumber <> -1 then
      DialogStaffPlanningF.MyShift := IntToStr(MyShiftNumber) +
        Str_Sep + GetShift;
    DialogStaffPlanningF.MyDate := MyDate;
    DialogStaffPlanningF.MyTeam := MyTeamCode + Str_Sep + GetTeam;
//    if SystemDM.EmbedDialogs then
//      DialogStaffPlanningF.FormStyle := fsStayOnTop;
    DialogStaffPlanningF.ShowModal;
  finally
    DialogStaffPlanningF.Free;
    RefreshAvailableTime := True;
    SwitchFormEnd; // 20012155
    MainAction;
  end;
end;

procedure TEmployeeRegsF.btnProcessPlannedClick(Sender: TObject);
begin
  inherited;
  try
    SwitchFormInit; // 20012155
    DialogProcessAbsenceHrsF := TDialogProcessAbsenceHrsF.Create(Application);
    // MR:03-05-2004 Don't use 'Str_Sep'.
    DialogProcessAbsenceHrsF.MyPlant := MyPlantCode;
    DialogProcessAbsenceHrsF.MyEmployee := IntToStr(MyEmployeeNumber);
//    DialogProcessAbsenceHrsF.MyPlant := MyPlantCode + Str_Sep + GetPlant;
//    DialogProcessAbsenceHrsF.MyEmployee := IntToStr(MyEmployeeNumber) +
//      Str_Sep +
//        EmployeeRegsDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
    DialogProcessAbsenceHrsF.StartDate := MyDate;
    DialogProcessAbsenceHrsF.EndDate := MyDate;
//    if SystemDM.EmbedDialogs then
//      DialogProcessAbsenceHrsF.FormStyle := fsStayOnTop;
    DialogProcessAbsenceHrsF.ShowModal;
  finally
    DialogProcessAbsenceHrsF.Free;
    RefreshAvailableTime := True;
    SwitchFormEnd; // 20012155
    MainAction;
  end;
end;

procedure TEmployeeRegsF.btnCalculateWorktimeReductionClick(
  Sender: TObject);
var
  AYear, AWeek: Word;
begin
  inherited;
  try
    SwitchFormInit; // 20012155
    DialogCalculateEarnedWTRF := TDialogCalculateEarnedWTRF.Create(Application);
    // MR:03-05-2004 Don't use 'Str_Sep'.
    DialogCalculateEarnedWTRF.MyPlant := MyPlantCode;
    DialogCalculateEarnedWTRF.MyDepartment := MyEmployeeDepartmentCode;
    DialogCalculateEarnedWTRF.MyEmployee := IntToStr(MyEmployeeNumber);
//    DialogCalculateEarnedWTRF.MyPlant := MyPlantCode + Str_Sep + GetPlant;
//    DialogCalculateEarnedWTRF.MyDepartment := MyEmployeeDepartmentCode +
//      Str_Sep + GetDepartment;
//    DialogCalculateEarnedWTRF.MyEmployee := IntToStr(MyEmployeeNumber) +
//      Str_Sep +
//        EmployeeRegsDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
    ListProcsF.WeekUitDat(MyDate, AYear, AWeek);
    DialogCalculateEarnedWTRF.MyYear := AYear;
    DialogCalculateEarnedWTRF.MyWeek := AWeek;
//    if SystemDM.EmbedDialogs then
//      DialogCalculateEarnedWTRF.FormStyle := fsStayOnTop;
    DialogCalculateEarnedWTRF.ShowModal;
  finally
    DialogCalculateEarnedWTRF.Free;
    RefreshAvailableTime := True;
    SwitchFormEnd; // 20012155
    MainAction;
  end;
end;

//Car: 550285
procedure TEmployeeRegsF.GoToRecord(Which: Integer);
begin
  case Which of
    NAV_FIRST : EmployeeRegsDM.cdsFilterEmployee.First;
    NAV_PRIOR : EmployeeRegsDM.cdsFilterEmployee.Prior;
    NAV_NEXT  : EmployeeRegsDM.cdsFilterEmployee.Next;
    NAV_LAST  : EmployeeRegsDM.cdsFilterEmployee.Last;
  end;
  LabelEmpDesc.Caption :=
    EmployeeRegsDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
  MainAction;
end;

//Car: 550285
procedure TEmployeeRegsF.BitBtnFirstClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_FIRST);
  BitBtnFirst.Enabled := False;
  BitBtnPrev.Enabled := False;
  BitBtnNext.Enabled := True;
  BitBtnLast.Enabled := True;
end;

//Car: 550285
procedure TEmployeeRegsF.BitBtnPrevClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_PRIOR);
  BitBtnFirst.Enabled := True;
  BitBtnNext.Enabled := True;
  BitBtnLast.Enabled := True;
end;

//Car: 550285
procedure TEmployeeRegsF.BitBtnNextClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_NEXT);
  BitBtnFirst.Enabled := True;
  BitBtnPrev.Enabled := True;
  BitBtnLast.Enabled := True;
end;

//Car: 550285
procedure TEmployeeRegsF.BitBtnLastClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_LAST);
  BitBtnFirst.Enabled := True;
  BitBtnPrev.Enabled := True;
  BitBtnNext.Enabled := False;
  BitBtnLast.Enabled := False;
end;

procedure TEmployeeRegsF.FormShow(Sender: TObject);
begin
  inherited;
  // MR:03-05-2004 Get saved values first.
  if SystemDM.ASaveTimeRecScanning.AEmployeeNumber <> -1 then
  begin
    EmployeeRegsDM.cdsFilterEmployee.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee, []);
  end;
  if SystemDM.ASaveTimeRecScanning.DateScanning <> EncodeDate(1900,1,1) then
  begin
    MonthCalendar.Date := Trunc(SystemDM.ASaveTimeRecScanning.DateScanning);
  end;
  // RV069.1.
  MonthCalendarClick(Sender);
//  MainAction;
  cBoxShowOnlyActiveClick(cBoxShowOnlyActive); // ABS-27382
end;

procedure TEmployeeRegsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  try
  finally
    SystemDM.UserTeamSelectionYN := MyUserTeamSelectionYN;
    // RV075.8 Restore the old item for the main menu.
    SideMenuUnitMaintenanceF.dxSideBar.SelectedItem :=
      SideMenuUnitMaintenanceF.LastDxSideBarItem;
    EmployeeRegsBaseF.Free;
    // MR:03-05-2004 Save employee for other forms
    SystemDM.ASaveTimeRecScanning.AEmployeeNumber := MyEmployeeNumber;
    SystemDM.ASaveTimeRecScanning.DateScanning := MyDate;
    // RV082.5.
    SystemDM.ASaveTimeRecScanning.ContractGroupCode := MyContractGroupCode;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TEmployeeRegsF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

// 20012155
function TEmployeeRegsF.EmployeeOtherTeamCheck: Boolean;
begin
  Result := False;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    with EmployeeRegsDM.cdsFilterEmployee do
    begin
      // Has employee worked on other plant?
      if FieldByName('EMP_PLANT_IN_TEAM').AsString = '0' then
        Result := True;
    end
  end;
end;

// 20012155
procedure TEmployeeRegsF.SwitchFormInit;
begin
  // Temporarily switch this off, to allow changes to employees worked
  // in other plants/teams.
  if EmployeeOtherTeamCheck then
  begin
    SystemDM.UserTeamSelectionYN := UNCHECKEDVALUE;
    SystemDM.PlantTeamFilterOn := False;
  end;
end;

// 20012155
procedure TEmployeeRegsF.SwitchFormEnd;
begin
  // 20012155
  SystemDM.UserTeamSelectionYN := MyUserTeamSelectionYN;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    SystemDM.PlantTeamFilterOn := True;
end;

// 20013035
procedure TEmployeeRegsF.cBoxShowOnlyActiveClick(Sender: TObject);
begin
  inherited;
  with EmployeeRegsDM do
  begin
    ShowOnlyActive := cBoxShowOnlyActive.Checked;
    FilterDate := MonthCalendar.Date;
    ShowOnlyActiveSwitch;
    EmployeeDateFilter;
  end;
end;

end.
