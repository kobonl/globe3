inherited TimeBlockPerShiftDM: TTimeBlockPerShiftDM
  OldCreateOrder = True
  Left = 238
  Top = 260
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnFilterRecord = TableMasterFilterRecord
    TableName = 'SHIFT'
    object TableMasterSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterSTARTTIME1: TDateTimeField
      FieldName = 'STARTTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME1: TDateTimeField
      FieldName = 'ENDTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME2: TDateTimeField
      FieldName = 'STARTTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME2: TDateTimeField
      FieldName = 'ENDTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME3: TDateTimeField
      FieldName = 'STARTTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME3: TDateTimeField
      FieldName = 'ENDTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME4: TDateTimeField
      FieldName = 'STARTTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME4: TDateTimeField
      FieldName = 'ENDTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME5: TDateTimeField
      FieldName = 'STARTTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME5: TDateTimeField
      FieldName = 'ENDTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME6: TDateTimeField
      FieldName = 'STARTTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME6: TDateTimeField
      FieldName = 'ENDTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterSTARTTIME7: TDateTimeField
      FieldName = 'STARTTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterENDTIME7: TDateTimeField
      FieldName = 'ENDTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    BeforeDelete = TableDetailBeforeDelete
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'SHIFT_NUMBER;PLANT_CODE'
    MasterFields = 'SHIFT_NUMBER;PLANT_CODE'
    TableName = 'TIMEBLOCKPERSHIFT'
    Top = 116
    object TableDetailSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailTIMEBLOCK_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'TIMEBLOCK_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailSTARTTIME1: TDateTimeField
      FieldName = 'STARTTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME1: TDateTimeField
      FieldName = 'ENDTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME2: TDateTimeField
      FieldName = 'STARTTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME2: TDateTimeField
      FieldName = 'ENDTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME3: TDateTimeField
      FieldName = 'STARTTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME3: TDateTimeField
      FieldName = 'ENDTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME4: TDateTimeField
      FieldName = 'STARTTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME4: TDateTimeField
      FieldName = 'ENDTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME5: TDateTimeField
      FieldName = 'STARTTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME5: TDateTimeField
      FieldName = 'ENDTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME6: TDateTimeField
      FieldName = 'STARTTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME6: TDateTimeField
      FieldName = 'ENDTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME7: TDateTimeField
      FieldName = 'STARTTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME7: TDateTimeField
      FieldName = 'ENDTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailPLANTDESC: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 192
  end
  inherited DataSourceDetail: TDataSource
    Left = 192
    Top = 108
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 88
    Top = 184
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableTempTBPerShift: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'TIMEBLOCKPERSHIFT'
    Left = 104
    Top = 248
  end
end
