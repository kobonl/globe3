inherited DialogSettingsF: TDialogSettingsF
  Left = 446
  Top = 278
  BorderIcons = [biSystemMenu]
  Caption = 'Settings'
  ClientHeight = 210
  ClientWidth = 358
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 358
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 60
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 358
    Height = 90
    Font.Color = clBlack
    Font.Height = -13
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 358
      Height = 90
      Align = alClient
      Caption = 'Settings'
      TabOrder = 0
      object CheckBoxShowLevel: TCheckBox
        Left = 12
        Top = 24
        Width = 137
        Height = 17
        Caption = 'Show level'
        TabOrder = 0
      end
      object CheckBoxShowLevelC: TCheckBox
        Left = 12
        Top = 53
        Width = 185
        Height = 17
        Caption = 'Show Level C'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 191
    Width = 358
  end
  inherited pnlBottom: TPanel
    Top = 150
    Width = 358
    inherited btnOk: TBitBtn
      Left = 68
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 190
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      23
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
