(*
  SO:08-JUL-2010 RV067.4. 550497
  MRA:24-AUG-2010. RV067.MRA.8. 550497.
  - Changed hardcoded strings to UPimsMessageRes.
  MRA:31-AUG-2010 RV067.MRA.21. Changes linked to order 550497.
  - Set Active-field to invisible: When an AbsenceType is added for
    AbsenceTypePerCountry, then it is automatically 'active'. 
*)
unit AbsTypePerCountryFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, dxEditor, dxEdLib, dxDBELib,
  dxExEdtr, dxDBTLCl, dxGrClms;

type
  TAbsTypePerCountryF = class(TGridBaseF)
    LabelDescription: TLabel;
    DBEditDescription: TdxDBEdit;
    LabelExportCode: TLabel;
    DBEditExportCode: TDBEdit;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnExportCode: TdxDBGridColumn;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dxDBCheckEditActive: TdxDBCheckEdit;
    DBEditCounterName: TDBEdit;
    dxDBCheckEditCounterActive: TdxDBCheckEdit;
    dxMasterGridColumnExportType: TdxDBGridColumn;
    dxDetailGridColumnCounterActive: TdxDBGridCheckColumn;
    dxDetailGridColumnCounterDescription: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxDetailGridEnter(Sender: TObject);
  private
    procedure SelectRecords;
    { Private declarations }
  public
    { Public declarations }
  end;

function AbsTypePerCountryF: TAbsTypePerCountryF;

implementation

uses
  AbsTypePerCountryDMT, MultipleSelectFRM, UPimsConst,
  UPimsMessageRes, SystemDMT;

{$R *.DFM}

var
  AbsTypePerCountryF_HND: TAbsTypePerCountryF;

function AbsTypePerCountryF: TAbsTypePerCountryF;
begin
  if (AbsTypePerCountryF_HND = nil) then
    AbsTypePerCountryF_HND := TAbsTypePerCountryF.Create(Application);
  Result := AbsTypePerCountryF_HND;
end;

procedure TAbsTypePerCountryF.FormDestroy(Sender: TObject);
begin
  inherited;
  AbsTypePerCountryF_HND:=Nil;
end;

procedure TAbsTypePerCountryF.FormCreate(Sender: TObject);
begin
  AbsTypePerCountryDM := CreateFormDM(TAbsTypePerCountryDM);
  if (dxMasterGrid.DataSource = Nil) then
    dxMasterGrid.DataSource := AbsTypePerCountryDM.DataSourceMaster;
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := AbsTypePerCountryDM.DataSourceDetail;
  inherited;
  if Form_Edit_YN = ITEM_VISIBIL_EDIT then
    SetEditable;
end;

procedure TAbsTypePerCountryF.SelectRecords;
var
  frm: TDialogMultipleSelectF;
  CountryId: String;
begin
  frm := TDialogMultipleSelectF.Create(Application);
  try
    CountryId := AbsTypePerCountryDM.TableMaster.FieldByName('COUNTRY_ID').AsString;
    frm.Init(SPimsAbsenceTypes,
      'select t.code || '' | '' || description from country t where t.country_id = ' + CountryId,
      //Available,
      'select t.absencetype_code code, t.description from absencetype t ' +
      'where t.absencetype_code not in (select ac.absencetype_code from absencetypepercountry ac where ac.country_id =  ' + CountryId + ') ' +
      'order by t.absencetype_code',
      //Selected,
      'select ac.absencetype_code code, ac.description from absencetypepercountry ac where ac.country_id = ' + CountryId,
      //Insert
      'insert into absencetypepercountry(country_id, creationdate, mutationdate, mutator, absencetype_code, description) values ('
      + CountryId + ', sysdate, sysdate, ''' + SystemDM.CurrentProgramUser + ''', ''%s'', ''%s'')',
      //Delete
      'delete from absencetypepercountry ac where ac.country_id = ' + CountryId +
      ' and absencetype_code = ''%s''',
      //Exists
      'select NVL(count(*), 0) from absencetypepercountry ac where ac.country_id = ' + CountryId +
      ' and absencetype_code = ''%s'''
      );
    if frm.ShowModal = mrOk then
      AbsTypePerCountryDM.TableDetail.Refresh;
  finally
    frm.Free();
  end;
end;

procedure TAbsTypePerCountryF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TAbsTypePerCountryF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TAbsTypePerCountryF.FormShow(Sender: TObject);
begin
  inherited;
  dxMasterGrid.SetFocus;
end;

procedure TAbsTypePerCountryF.dxDetailGridEnter(Sender: TObject);
begin
  inherited dxGridEnter(Sender);
  dxBarButtonEditMode.Visible := ivNever;
end;

end.
