object DialogMonthlyGroupEfficiencyDM: TDialogMonthlyGroupEfficiencyDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 191
  Top = 107
  Height = 640
  Width = 870
  object qryPQWorkspot: TQuery
    OnCalcFields = qryPQWorkspotCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PQ.PLANT_CODE, PQ.WORKSPOT_CODE, '
      '  SUM(PQ.QUANTITY) AS SUMQUANTITY '
      'FROM '
      '  PRODUCTIONQUANTITY PQ INNER JOIN WORKSPOT W ON '
      '    PQ.PLANT_CODE = W.PLANT_CODE AND '
      '    PQ.WORKSPOT_CODE = W.WORKSPOT_CODE '
      'WHERE '
      '  (PQ.PLANT_CODE >= :PLANTFROM AND '
      '   PQ.PLANT_CODE <= :PLANTTO) AND '
      '  (PQ.START_DATE >= :DATEMIN AND '
      '   PQ.END_DATE <= :DATEMAX) AND'
      '  (W.DEPARTMENT_CODE >= :DEPTFROM AND '
      '   W.DEPARTMENT_CODE <= :DEPTTO) AND '
      '  (PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND '
      '   PQ.WORKSPOT_CODE <= :WORKSPOTTO) '
      'GROUP BY '
      '  PQ.PLANT_CODE, PQ.WORKSPOT_CODE')
    Left = 56
    Top = 32
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end>
    object qryPQWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryPQWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object qryPQWorkspotSUMQUANTITY: TFloatField
      FieldName = 'SUMQUANTITY'
    end
    object qryPQWorkspotEFFICIENCY: TFloatField
      FieldKind = fkCalculated
      FieldName = 'EFFICIENCY'
      Calculated = True
    end
  end
  object qryNormProdTotal: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, '
      '  J.NORM_PROD_LEVEL, '
      '  SUM(P.PRODUCTION_MINUTE) AS SUMPRODMINUTE '
      'FROM '
      '  PRODHOURPEREMPLOYEE P INNER JOIN WORKSPOT W ON '
      '    P.PLANT_CODE = W.PLANT_CODE AND '
      '    P.WORKSPOT_CODE = W.WORKSPOT_CODE '
      '  LEFT JOIN JOBCODE J ON '
      '    P.PLANT_CODE = J.PLANT_CODE AND '
      '    P.WORKSPOT_CODE = J.WORKSPOT_CODE AND '
      '    P.JOB_CODE = J.JOB_CODE '
      'WHERE '
      '  (P.PLANT_CODE >= :PLANTFROM AND P.PLANT_CODE <= :PLANTTO) AND '
      '  (W.DEPARTMENT_CODE >= :DEPTFROM AND '
      '   W.DEPARTMENT_CODE <= :DEPTTO) AND '
      '  (P.WORKSPOT_CODE >= :WORKSPOTFROM AND '
      '   P.WORKSPOT_CODE <= :WORKSPOTTO) AND '
      '  (P.PRODHOUREMPLOYEE_DATE >= :DATEMIN AND '
      '   P.PRODHOUREMPLOYEE_DATE <= :DATEMAX) '
      'GROUP BY '
      '  P.PLANT_CODE, P.WORKSPOT_CODE, '
      '  P.JOB_CODE, J.NORM_PROD_LEVEL')
    Left = 56
    Top = 92
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
    object qryNormProdTotalPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object qryNormProdTotalWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object qryNormProdTotalJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object qryNormProdTotalNORM_PROD_LEVEL: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
    end
    object qryNormProdTotalSUMPRODMINUTE: TFloatField
      FieldName = 'SUMPRODMINUTE'
    end
  end
  object qryGroupEfficiencyInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO GROUPEFFICIENCY'
      '(YEAR_NUMBER,'
      'MONTH_NUMBER,'
      'PLANT_CODE,'
      'WORKSPOT_CODE,'
      'EFFICIENCY,'
      'CREATIONDATE,'
      'MUTATIONDATE,'
      'MUTATOR)'
      'VALUES'
      '(:YEAR_NUMBER,'
      ':MONTH_NUMBER,'
      ':PLANT_CODE,'
      ':WORKSPOT_CODE,'
      ':EFFICIENCY,'
      ':CREATIONDATE,'
      ':MUTATIONDATE,'
      ':MUTATOR)'
      '')
    Left = 56
    Top = 148
    ParamData = <
      item
        DataType = ftInteger
        Name = 'YEAR_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MONTH_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'EFFICIENCY'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryGroupEfficiencyDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM GROUPEFFICIENCY'
      'WHERE YEAR_NUMBER = :YEAR_NUMBER AND'
      'MONTH_NUMBER = :MONTH_NUMBER AND'
      'PLANT_CODE >= :PLANTFROM AND'
      'PLANT_CODE <= :PLANTTO AND'
      'WORKSPOT_CODE >= :WORKSPOTFROM AND'
      'WORKSPOT_CODE <= :WORKSPOTTO')
    Left = 56
    Top = 204
    ParamData = <
      item
        DataType = ftInteger
        Name = 'YEAR_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MONTH_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end>
  end
end
