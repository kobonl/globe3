inherited TeamDeptF: TTeamDeptF
  Left = 234
  Top = 180
  Width = 729
  Height = 486
  Caption = 'Departments per team'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 129
    Width = 713
    Height = 96
    TabOrder = 1
    inherited spltMasterGrid: TSplitter
      Top = 92
      Width = 711
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 711
      Height = 91
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Teams'
          Width = 716
        end>
      KeyField = 'TEAM_CODE'
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 150
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TEAM_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 296
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnResponsible: TdxDBGridColumn
        Caption = 'Responsible'
        Width = 270
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEELU'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 355
    Width = 713
    Height = 93
    TabOrder = 5
    OnEnter = pnlDetailEnter
    object Label1: TLabel
      Left = 12
      Top = 24
      Width = 57
      Height = 13
      Caption = 'Department'
    end
    object LabelPlant: TLabel
      Left = 12
      Top = 56
      Width = 24
      Height = 13
      Caption = 'Plant'
      Visible = False
    end
    object dxDBLookupEditDept: TdxDBLookupEdit
      Tag = 1
      Left = 80
      Top = 22
      Width = 257
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'DEPARTMENTLU'
      DataSource = TeamDeptDM.DataSourceDetail
      DropDownRows = 5
      ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 225
    Width = 713
    Height = 130
    TabOrder = 4
    inherited spltDetail: TSplitter
      Top = 126
      Width = 711
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 711
      Height = 125
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Departments per team'
          Width = 717
        end>
      KeyField = 'DEPARTMENT_CODE'
      DataSource = TeamDeptDM.DataSourceDetail
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn3: TdxDBGridColumn
        Caption = 'Plant code'
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        Width = 80
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'PLANT_CODE'
        DisableFilter = True
      end
      object dxDetailGridColumnPlant: TdxDBGridColumn
        Caption = 'Plant description'
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        Width = 219
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'PLANTCAL'
        DisableFilter = True
      end
      object dxDetailGridColumnDeptCode: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENT_CODE'
      end
      object dxDetailGridColumnDepartment: TdxDBGridColumn
        Caption = 'Description'
        DisableEditor = True
        Width = 402
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENTCAL'
      end
    end
  end
  object pnlMasterPlantGrid: TPanel [4]
    Left = 0
    Top = 26
    Width = 713
    Height = 103
    Align = alTop
    Caption = 'pnlMasterPlantGrid'
    TabOrder = 0
    object dxDBMasterPlantGrid: TdxDBGrid
      Left = 1
      Top = 1
      Width = 711
      Height = 101
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'PLANT_CODE'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      OnEnter = dxGridEnter
      DataSource = TeamDeptDM.DataSourceMasterPlant
      HideSelectionTextColor = clWindowText
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
      OnCustomDrawBand = dxGridCustomDrawBand
      OnCustomDrawCell = dxGridCustomDrawCell
      OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
      OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
      object dxMasterPlantGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterPlantGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterPlantGridColumnCity: TdxDBGridColumn
        Caption = 'City'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxMasterPlantGridColumnState: TdxDBGridColumn
        Caption = 'State'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STATE'
      end
      object dxMasterPlantGridColumnPhone: TdxDBGridColumn
        Caption = 'Phone'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
