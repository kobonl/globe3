inherited ReportStaffPlanDayDM: TReportStaffPlanDayDM
  OldCreateOrder = True
  Left = 249
  Top = 247
  Height = 479
  Width = 741
  inherited qryPlantMinMax: TQuery
    Left = 448
    Top = 312
  end
  inherited qryTeamMinMax: TQuery
    Left = 448
    Top = 144
  end
  inherited qryEmplMinMax: TQuery
    Left = 448
    Top = 256
  end
  inherited qryBUMinMax: TQuery
    Left = 448
    Top = 96
  end
  inherited qryWeekDelete: TQuery
    Left = 72
    Top = 320
  end
  inherited qryWeekInsert: TQuery
    Left = 168
    Top = 320
  end
  inherited qryWorkspotMinMax: TQuery
    Left = 448
    Top = 200
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 176
  end
  object QueryEMP: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEMPFilterRecord
    Left = 64
    Top = 16
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryEMP
    Left = 176
    Top = 16
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 280
    Top = 16
  end
  object TableShift: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'SHIFT'
    Left = 280
    Top = 128
  end
  object TableTeam: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'TEAM'
    Left = 280
    Top = 184
  end
  object TableDept: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'DEPARTMENT'
    Left = 280
    Top = 72
  end
  object TableWK: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 64
    Top = 72
  end
  object QueryExec: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 368
    Top = 16
  end
  object qrySTB_EMPXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT EP.SCHEDULED_TIMEBLOCK_1,'
      'EP.SCHEDULED_TIMEBLOCK_2,'
      'EP.SCHEDULED_TIMEBLOCK_3,'
      'EP.SCHEDULED_TIMEBLOCK_4'
      'FROM EMPLOYEEPLANNING EP'
      'WHERE EP.PLANT_CODE = :PLANT_CODE AND'
      'EP.EMPLOYEEPLANNING_DATE = :EMPLOYEEPLANNING_DATE AND'
      'EP.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      'EP.DEPARTMENT_CODE = :DEPARTMENT_CODE AND'
      'EP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      'EP.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 72
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEPLANNING_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryRequestEarlyLate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT T.EMPLOYEE_NUMBER, T.REQUEST_DATE, T.REQUESTED_EARLY_TIME' +
        ', T.REQUESTED_LATE_TIME'
      'FROM REQUESTEARLYLATE T'
      'WHERE T.REQUEST_DATE >= :DATEFROM AND T.REQUEST_DATE <= :DATETO'
      'ORDER BY T.EMPLOYEE_NUMBER'
      ' ')
    Left = 272
    Top = 312
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
