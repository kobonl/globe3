(*
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machines.
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
  MRA:1-NOV-2011. RV100.1. 20012124. Tailor made CVL.
  - Add field to enter an 'overruled contract group'.
    - This will then be used to determine hour type based
      on exceptional hour definitions and overtime definitions
      linked to that 'overruled contract group', instead of using
      the hour type linked to the workspot.
  MRA:18-OCT-2012 20013551 Info Tunnels
  - Addition of 1 field EFF_RUNNING_HRS_YN.
  - Addition of shift-button with option
    to define shifts when EFF_RUNNING_HRS_YN equals 'Y'.
  MRA:8-NOV-2012 20013551 - Rework
  - For check overlapping shifts: Use 4 parameters to check for overlap, this
    means there can a maximum of 4 non-overlapping shifts.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:23-DEC-2014 20016016
  - Added setting for Personal Screen: Receive qty via
    blackbox or external application.
  MRA:2-MAR-2015 TD-26387
  - Additional Tab is not always shown issue
  - Solution: Make it always visible, but part about
    'Measure performance without employees' must be made
    enabled based on pims-system-setting.
  MRA:29-JUN-2016 PIM-181
  - When Automatic-datacol-yn equals yes then do not allow a change of
    Description and ShortName, when using hybrid-mode.
  - Also editing in grid has been disabled for hybrid-mode, because
    it was still possible to modify the Description or ShortName.
  MRA:11-JUL-2016 PIM-203
  - Read ID's without visual information
  - Add 2 fields: Host and Port, only for hybrid-mode.
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
*)
unit WorkSpotFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, dxEditor, dxExEdtr, dxEdLib,
  dxDBELib, Mask, dxDBTLCl, dxGrClms, Buttons, dxDBEdtr, ComCtrls, DBPicker,
  Menus;

type
  TWorkSpotF = class(TGridBaseF)
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnCity: TdxDBGridColumn;
    dxMasterGridColumnState: TdxDBGridColumn;
    dxMasterGridColumnPhone: TdxDBGridColumn;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnHourType: TdxDBGridLookupColumn;
    dxDetailGridColumnProductiveHour: TdxDBGridCheckColumn;
    dxDetailGridColumnUseJobCodes: TdxDBGridCheckColumn;
    dxDetailGridColumnMeasureProd: TdxDBGridCheckColumn;
    dxDetailGridColumnAutomatic: TdxDBGridCheckColumn;
    dxDetailGridColumnCounter: TdxDBGridCheckColumn;
    dxDetailGridColumnEnter: TdxDBGridCheckColumn;
    dxDetailGridColumnInactiveDate: TdxDBGridDateColumn;
    dxDetailGridColumnAutRescan: TdxDBGridCheckColumn;
    MainMenu1: TMainMenu;
    dxDetailGridColumnShortName: TdxDBGridColumn;
    dxDetailGridColumnDepartment: TdxDBGridCalcColumn;
    dxDetailGridColumnGroupEfficiency: TdxDBGridCheckColumn;
    dxDetailGridColumnMachine: TdxDBGridLookupColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    LabelCode: TLabel;
    DBEditCode: TDBEdit;
    Label3: TLabel;
    DBEditShortName: TDBEdit;
    DBEditDescription: TDBEdit;
    Label2: TLabel;
    dxDBDateEdit1: TdxDBDateEdit;
    Label1: TLabel;
    LabelDepartment: TLabel;
    DBLookupComboBoxDep: TDBLookupComboBox;
    dxDBLookupEdit2: TdxDBLookupEdit;
    LabelHourType: TLabel;
    dxDBLookupEdit1: TdxDBLookupEdit;
    LabelMachine: TLabel;
    LabelContractGroup: TLabel;
    dxDBLookupEditContractGroup: TdxDBLookupEdit;
    DBCheckBoxGroupEfficiency: TDBCheckBox;
    LabelGrade: TLabel;
    DBEditGrade: TDBEdit;
    DBCheckBoxAutRescan: TDBCheckBox;
    DBCheckBoxAutomaticDC: TDBCheckBox;
    DBCheckBoxMeasureProd: TDBCheckBox;
    DBCheckBoxJobCode_YN: TDBCheckBox;
    DBCheckBoxProductive: TDBCheckBox;
    DBRadioGroup1: TDBRadioGroup;
    ButtonShowForm: TButton;
    btnShifts: TButton;
    GroupBox1: TGroupBox;
    DBCheckBoxCounterValue: TDBCheckBox;
    DBCheckBoxEnterCounter: TDBCheckBox;
    DBRgrpEffBasedOn: TDBRadioGroup;
    Panel1: TPanel;
    Panel2: TPanel;
    gboxPersonalScreen: TGroupBox;
    DBRGrpTimeRecBasedOn: TDBRadioGroup;
    LblFakeEmpNr: TLabel;
    DBEditFakeEmpNr: TDBEdit;
    dxDetailGridColumnTRByMachine: TdxDBGridCalcColumn;
    dxDetailGridColumnFakeEmpNr: TdxDBGridColumn;
    GroupBox2: TGroupBox;
    DBRadioGroup2: TDBRadioGroup;
    gBoxCardReader: TGroupBox;
    Label4: TLabel;
    dxDBEdit1: TdxDBEdit;
    dxDBEdit2: TdxDBEdit;
    Label5: TLabel;
    dxDetailGridColumnHost: TdxDBGridColumn;
    dxDetailGridColumnPort: TdxDBGridColumn;
    dxDetailGridColumnBatch: TdxDBGridColumn;
    Label6: TLabel;
    dxDBSpinEdit1: TdxDBSpinEdit;
    Panel3: TPanel;
    DBCheckBoxRoaming: TDBCheckBox;
    dxDetailGridColumnRoaming: TdxDBGridCheckColumn;
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ButtonShowFormClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxDetailGridClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure DBCheckBoxMeasureProdMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBoxMeasureProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBoxAutomaticDCMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBoxAutRescanMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBoxEnterCounterMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBoxCounterValueMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBoxJobCode_YNMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure DBEditCodeChange(Sender: TObject);
    procedure DBCheckBoxJobCode_YNClick(Sender: TObject);
    procedure DBCheckBoxJobCode_YNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBoxMeasureProdClick(Sender: TObject);
    procedure DBCheckBoxAutomaticDCClick(Sender: TObject);
    procedure DBCheckBoxAutomaticDCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBoxAutRescanClick(Sender: TObject);
    procedure DBCheckBoxAutRescanKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBoxEnterCounterClick(Sender: TObject);
    procedure DBCheckBoxEnterCounterKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBoxCounterValueClick(Sender: TObject);
    procedure DBCheckBoxCounterValueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure DBRgrpEffBasedOnChange(Sender: TObject);
    procedure btnShiftsClick(Sender: TObject);
    procedure dxDetailGridColumnTRByMachineGetText(Sender: TObject;
      ANode: TdxTreeListNode; var AText: String);
    procedure DBRGrpTimeRecBasedOnChange(Sender: TObject);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure DBEditDescriptionEnter(Sender: TObject);
    procedure DBEditShortNameEnter(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
  private
    { Private declarations }
     InsertForm: TForm;
     procedure ShortNameEnable(AEnable: Boolean);
     procedure DescriptionEnable(AENable: Boolean);
  public
    { Public declarations }
    FClickOnJobCode, FClickOnMP, FClickOnAutDC, FClickOnAutRescan,
    FClickOnEnterCounter, FClickOnCounterVal :Boolean;
    procedure JobCodeSetActiveSB(Status: Boolean);
    procedure ActiveEnterCounter(Status: Boolean);
    procedure SetDetailPanel;
    procedure SetBtnCaption;
    procedure CallForm;
{    function ActionTestShiftOverlap(AField: String): Boolean; }
  end;

function WorkSpotF: TWorkSpotF;


implementation

{$R *.DFM}
uses SystemDMT, WorkSpotDMT, JobCodeFRM, BUPerWorkSpotFRM, UPimsMessageRes,
  UPimsConst, MultipleSelectFRM;

var
  WorkSpotF_HND: TWorkSpotF;

function WorkSpotF: TWorkSpotF;
begin
  if (WorkSpotF_HND = nil) then
    WorkSpotF_HND := TWorkSpotF.Create(Application);
  Result := WorkSpotF_HND;
end;

procedure TWorkSpotF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkSpotF_HND := Nil;
end;

procedure TWorkSpotF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
  DBEditCode.SetFocus;
  DBCheckBoxJobCode_YN.Checked := False;
  DBCheckBoxMeasureProd.Checked := False;
  JobCodeSetActiveSB(False);
  lblFakeEmpNr.Visible := False; // 20015178
  DBEditFakeEmpNr.Visible := False; // 20015178
end;

procedure TWorkSpotF.FormShow(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  PageControl1.ActivePageIndex := 0;
  FClickOnJobCode := False;
  FClickOnMP := False;
  FClickOnAutDC := False;
  FClickOnAutRescan := False;
  FClickOnEnterCounter := False;
  FClickOnCounterVal := False;
  SetDetailPanel;
  dxMasterGrid.SetFocus;
  // MR:11-11-2003 Do following, so DetailGrid is shown OK,
  // otherwise the vertical scrollbar is not shown completely.
  pnlDetail.Align := alNone;
  pnlDetail.Align := alBottom;
  //CAR: 550299
  if not SystemDM.GetGradeYN then
  begin
    LabelGrade.Visible := False;
    DBEditGrade.Visible := False;
  end;

  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      WorkSpotDM.TableMaster.FieldByName('PLANT_CODE').AsString
  else
    WorkSpotDM.TableMaster.FindKey([SystemDM.ASaveTimeRecScanning.Plant]);

  // 20015178
  if not SystemDM.EnableMachineTimeRec then
  begin
    // TD-26387
    gboxPersonalScreen.Enabled := False;
    DBRGrpTimeRecBasedOn.Enabled := False;
    LblFakeEmpNr.Enabled := False;
    DBEditFakeEmpNr.Enabled := False;
  end;

  // 20015178 Related to this order
  // Disable only contents of tabsheets, not the tabsheets themselves.
  if not pnlDetail.Enabled then
  begin
    pnlDetail.Enabled := True;
    for I := 0 to PageControl1.PageCount - 1 do
      PageControl1.Pages[I].Enabled := False;
  end;
  PageControl1.ActivePageIndex := 0;
end;

// STATUS = CHECK MEASURE PRODUCTIVITY
procedure TWorkSpotF.JobCodeSetActiveSB(Status: Boolean);
begin
  DBCheckBoxAutomaticDC.Enabled := Status;
  DBCheckBoxAutRescan.Enabled := False;

  DBCheckBoxEnterCounter.Enabled := Status;
  DBCheckBoxCounterValue.Enabled := False;

  DBCheckBoxAutomaticDC.Checked :=  False;
  DBCheckBoxAutRescan.Checked :=  False;
  DBCheckBoxEnterCounter.Checked :=  False;
  DBCheckBoxCounterValue.Checked :=  False;
  if WorkspotDM <> nil then
  begin
    WorkspotDM.FAutomaticYN := DBCheckBoxAutomaticDC.Checked;
    WorkspotDM.FCounterYN := DBCheckBoxCounterValue.Checked;
    WorkspotDM.FEnterCounterYN := DBCheckBoxEnterCounter.Checked;
    WorkspotDM.FAutoRescanYN := DBCheckBoxAutRescan.Checked;
    WorkspotDM.FMeasureProd := DBCheckBoxMeasureProd.Checked;
  end;
end;

procedure TWorkSpotF.ButtonShowFormClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  if WorkSpotDM.TableDetail.FieldByName('WORKSPOT_CODE').AsString = '' then
    Exit;
  if ButtonShowForm.Caption = SShowJobCode then
  begin
    // RV064.1.
    WorkspotDM.AddMachineJobs(
      WorkspotDM.TableDetail.FieldByName('PLANT_CODE').AsString,
      WorkSpotDM.TableDetail.FieldByName('WORKSPOT_CODE').AsString,
      WorkSpotDM.TableDetail.FieldByName('MACHINE_CODE').AsString);
    InsertForm := JobCodeF;
  end
  else
    InsertForm := BUPerWorkSpotF;
  InsertForm.Show;
end;

procedure TWorkSpotF.FormActivate(Sender: TObject);
begin
  inherited;
  if InsertForm <> Nil then
    if (InsertForm.Visible) then
      InsertForm.Show;
end;

procedure TWorkSpotF.FormCreate(Sender: TObject);
begin
  WorkSpotDM := CreateFormDM(TWorkSpotDM);
  if (dxDetailGrid.DataSource = nil) or (dxMasterGrid.DataSource = nil) then
  begin
    dxMasterGrid.DataSource := WorkSpotDM.DataSourceMaster;
    dxDetailGrid.DataSource := WorkSpotDM.DataSourceDetail;
  end;
  // Open the Query here!
  // MR:29-03-2005 Moved to DataModule
//  WorkspotDM.QueryDeptLU.Open;
  // MR:12-03-2007 Order 550443 Group Efficiency, set per workspot.
  if SystemDM.GetMonthGroupEfficiency then
  begin
    DBCheckBoxGroupEfficiency.Visible := True;
    dxDetailGridColumnGroupEfficiency.Visible := True;
  end
  else
  begin
    DBCheckBoxGroupEfficiency.Visible := False;
    dxDetailGridColumnGroupEfficiency.Visible := False;
  end;
  // RV100.1.
  LabelContractGroup.Visible := SystemDM.IsCVL;
  dxDBLookupEditContractGroup.Visible := SystemDM.IsCVL;
  // 20015178
  if not SystemDM.EnableMachineTimeRec then
  begin
    dxDetailGridColumnTRByMachine.Visible := False;
    dxDetailGridColumnTRByMachine.DisableCustomizing := True;
    dxDetailGridColumnFakeEmpNr.Visible := False;
    dxDetailGridColumnFakeEmpNr.DisableCustomizing := True;
  end;
  // PIM-203
  if SystemDM.HybridMode = hmHybrid then
  begin
    gBoxCardReader.Visible := True;
    dxDetailGridColumnHost.Visible := True;
    dxDetailGridColumnHost.DisableCustomizing := False;
    dxDetailGridColumnPort.Visible := True;
    dxDetailGridColumnPort.DisableCustomizing := False;
  end;
  // GLOB3-202
  DBCheckBoxRoaming.Visible := SystemDM.TimeRecCRDoNotScanOut;
  dxDetailGridColumnRoaming.Visible := SystemDM.TimeRecCRDoNotScanOut;
  dxDetailGridColumnRoaming.DisableCustomizing := SystemDM.TimeRecCRDoNotScanOut;
  inherited;
end;

procedure TWorkSpotF.CallForm;
begin
  if WorkSpotDM.FCallNewForm = True then
  begin
    if dxBarBDBNavPost.Enabled then
       Exit;
    if ButtonShowForm.Caption = SShowJobCode then
      InsertForm := JobCodeF
    else
      InsertForm := BUPerWorkSpotF;
    InsertForm.Show;
    WorkSpotDM.FCallNewForm := False;
  end;
end;

procedure TWorkSpotF.ActiveEnterCounter(Status: Boolean);
begin
  if not DBCheckBoxMeasureProd.Checked then
  begin
    DBCheckBoxEnterCounter.Enabled := False;
    DBCheckBoxEnterCounter.Checked := False;
    DBCheckBoxCounterValue.Enabled := False;
    DBCheckBoxCounterValue.Checked := False;
  end
  else
  begin
    DBCheckBoxEnterCounter.Enabled := Status;
    DBCheckBoxEnterCounter.Checked := False;
    DBCheckBoxCounterValue.Enabled := False;
    DBCheckBoxCounterValue.Checked := False;
  end;
end;

procedure TWorkSpotF.SetBtnCaption;
begin
  if DBCheckBoxJobCode_YN.Checked then
    ButtonShowForm.Caption := SShowJobCode
  else
    ButtonShowForm.Caption := SShowBUWK;
end;

procedure TWorkSpotF.dxDetailGridClick(Sender: TObject);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  inherited;
  SetBtnCaption;
end;

procedure TWorkSpotF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  (dxDetailGrid as TdxDBGrid).DataSource.DataSet.Post;
  SetDetailPanel;
end;

procedure TWorkSpotF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;

  SystemDM.ASaveTimeRecScanning.Plant :=
      WorkSpotDM.TableMaster.FieldByName('PLANT_CODE').AsString;
end;

procedure TWorkSpotF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  if WorkspotDM <> Nil then
  begin
    WorkspotDM.TableDetail.Refresh;
    SetBtnCaption;
    SetDetailPanel;
  end;
end;

procedure TWorkSpotF.SetDetailPanel;
begin
  if WorkspotDM = Nil then
    exit;

  // 20015178
  if SystemDM.EnableMachineTimeRec then
  begin
    if DBRGrpTimeRecBasedOn.ItemIndex = 0 then
    begin
      LblFakeEmpNr.Visible := True;
      DBEditFakeEmpNr.Visible := True;
    end
    else
    begin
      LblFakeEmpNr.Visible := False;
      DBEditFakeEmpNr.Visible := False;
    end;
  end;

  // 20013551
  btnShifts.Enabled := (DBRgrpEffBasedOn.Value = 'Y');

  WorkspotDM.FMeasureProd := DBCheckBoxMeasureProd.Checked;
  WorkspotDM.FAutomaticYN := DBCheckBoxAutomaticDC.Checked;
  WorkspotDM.FAutoRescanYN := DBCheckBoxAutRescan.Checked;
  WorkspotDM.FEnterCounterYN := DBCheckBoxEnterCounter.Checked;
  WorkspotDM.FCounterYN := DBCheckBoxCounterValue.Checked;

  if DBCheckBoxJobCode_YN.Checked = False then
  begin
    DBCheckBoxMeasureProd.Enabled := False;
    DBCheckBoxAutomaticDC.Enabled := False;
    DBCheckBoxAutRescan.Enabled := False;
    DBCheckBoxEnterCounter.Enabled := False;
    DBCheckBoxCounterValue.Enabled := False;
    ButtonShowForm.Caption := SShowBUWK;
    exit;
  end;
  ButtonShowForm.Caption := SShowJobCode;
  DBCheckBoxMeasureProd.Enabled := True;
  if not DBCheckBoxMeasureProd.Checked then
  begin
    DBCheckBoxAutomaticDC.Enabled := False;
    DBCheckBoxEnterCounter.Enabled := False;
    DBCheckBoxCounterValue.Enabled := False;
    DBCheckBoxAutRescan.Enabled := False;
    Exit;
  end;

  DBCheckBoxAutomaticDC.Enabled := True;
  DBCheckBoxEnterCounter.Enabled := True;

  if DBCheckBoxAutomaticDC.Checked then
  begin
    DBCheckBoxEnterCounter.Enabled := False;
    DBCheckBoxCounterValue.Enabled := False;
    DBCheckBoxAutRescan.Enabled := True;
  end
  else
  begin
    DBCheckBoxAutRescan.Enabled := False;
    DBCheckBoxEnterCounter.Enabled := True;
    if DBCheckBoxEnterCounter.Checked then
       DBCheckBoxAutomaticDC.Enabled := False;
  end;
  DBCheckBoxCounterValue.Enabled := DBCheckBoxEnterCounter.Checked;
end; // SetDetailPanel

procedure TWorkSpotF.DBCheckBoxMeasureProdMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnMP := True;
end;

procedure TWorkSpotF.DBCheckBoxMeasureProdKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnMP := True;
end;

procedure TWorkSpotF.DBCheckBoxAutomaticDCMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnAutDC := True;
end;

procedure TWorkSpotF.DBCheckBoxAutRescanMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnAutRescan := True;
end;

procedure TWorkSpotF.DBCheckBoxEnterCounterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnEnterCounter := True;
end;

procedure TWorkSpotF.DBCheckBoxCounterValueMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnCounterVal := True; 
end;

procedure TWorkSpotF.DBCheckBoxJobCode_YNMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FClickOnJobCode := True;
end;

procedure TWorkSpotF.DBEditCodeChange(Sender: TObject);
begin
  inherited;
  SetDetailPanel;
end;

procedure TWorkSpotF.DBCheckBoxJobCode_YNClick(Sender: TObject);
var
  MyEvent: TNotifyEvent;
begin
  inherited;
  if not FClickOnJobCode then
    Exit;
  // MR:27-09-2005 Order 550410
  if WorkspotDM.CheckForOpenScans then
  begin
    MyEvent := DBCheckBoxJobCode_YN.OnClick;
    DBCheckBoxJobCode_YN.OnClick := nil;
    DBCheckBoxJobCode_YN.Checked := not DBCheckBoxJobCode_YN.Checked;
    DBCheckBoxJobCode_YN.OnClick := MyEvent;
    Exit;
  end;
  if DBCheckBoxJobCode_YN.Checked then
  begin
    ButtonShowForm.Caption := SShowJobCode;
    DBCheckBoxMeasureProd.Enabled := True;
    DBCheckBoxMeasureProd.Checked := False;
  end
  else
  begin
    DBCheckBoxMeasureProd.Enabled := False;
    DBCheckBoxMeasureProd.Checked := False;
    ButtonShowForm.Caption := SShowBUWK;
  end;
  JobCodeSetActiveSB(DBCheckBoxMeasureProd.Checked);
  FClickOnJobCode := False;
end;

procedure TWorkSpotF.DBCheckBoxJobCode_YNKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnJobCode := True;
end;

procedure TWorkSpotF.DBCheckBoxMeasureProdClick(Sender: TObject);
begin
  inherited;
  if FClickOnMP then
   JobCodeSetActiveSB(DBCheckBoxMeasureProd.Checked);
  FClickOnMP := False; 
end;

procedure TWorkSpotF.DBCheckBoxAutomaticDCClick(Sender: TObject);
begin
  inherited;
  if not FClickOnAutDC then
    exit;
  ActiveEnterCounter(not DBCheckBoxAutomaticDC.Checked);
  DBCheckBoxAutRescan.Enabled := DBCheckBoxAutomaticDC.Checked;
  if not DBCheckBoxAutomaticDC.Checked then
  begin
    DBCheckBoxAutRescan.Checked := False;
    DBCheckBoxAutomaticDC.Enabled := True;
  end;
  if WorkspotDM <> nil then
  begin
    WorkspotDM.FAutomaticYN := DBCheckBoxAutomaticDC.Checked;
    WorkspotDM.FAutoRescanYN := DBCheckBoxAutRescan.Checked;
    WorkspotDM.FEnterCounterYN := DBCheckBoxEnterCounter.Checked;
    WorkspotDM.FCounterYN := DBCheckBoxCounterValue.Checked;
  end;
  FClickONAutDC := False;
end;

procedure TWorkSpotF.DBCheckBoxAutomaticDCKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnAutDC := True;
end;

procedure TWorkSpotF.DBCheckBoxAutRescanClick(Sender: TObject);
begin
  inherited;
  if not FClickOnAutRescan then
    Exit;
  if WorkSpotDM = nil then
    Exit;
  WorkSpotDM.FChangeRescan := DBCheckBoxAutRescan.Checked;
  WorkspotDM.FAutoRescanYN := DBCheckBoxAutRescan.Checked;
  FClickOnAutRescan := False;
end;

procedure TWorkSpotF.DBCheckBoxAutRescanKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnAutRescan := True;
end;

procedure TWorkSpotF.DBCheckBoxEnterCounterClick(Sender: TObject);
begin
  inherited;
  if Not FClickOnEnterCounter then
    Exit;
  DBCheckBoxCounterValue.Enabled := DBCheckBoxEnterCounter.Checked;
  if not DBCheckBoxEnterCounter.Checked then
    DBCheckBoxCounterValue.Checked := False;
  if DBCheckBoxEnterCounter.Checked then
  begin
    DBCheckBoxAutomaticDC.Enabled := False;
    DBCheckBoxAutomaticDC.Checked := False;
    DBCheckBoxAutRescan.Enabled := False;
    DBCheckBoxAutRescan.Checked := False;
  end
  else
  begin
    DBCheckBoxAutomaticDC.Enabled := True;
    DBCheckBoxAutomaticDC.Checked := False;
    DBCheckBoxAutRescan.Enabled := False;
    DBCheckBoxAutRescan.Checked := False;
  end;
  if WorkspotDM <> nil then
  begin
    WorkspotDM.FAutomaticYN := DBCheckBoxAutomaticDC.Checked;
    WorkspotDM.FAutoRescanYN := DBCheckBoxAutRescan.Checked;
    WorkspotDM.FEnterCounterYN := DBCheckBoxEnterCounter.Checked;
    WorkspotDM.FCounterYN := DBCheckBoxCounterValue.Checked;
  end;
  FClickOnEnterCounter := False;
end;

procedure TWorkSpotF.DBCheckBoxEnterCounterKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnEnterCounter := True;
end;

procedure TWorkSpotF.DBCheckBoxCounterValueClick(Sender: TObject);
begin
  inherited;
  if Not FClickOnCounterVal then
    Exit;
  if WorkSpotDM <> nil then
    WorkspotDM.FCounterYN := DBCheckBoxCounterValue.Checked;
  FClickOnCounterVal := False;  
end;

procedure TWorkSpotF.DBCheckBoxCounterValueKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FClickOnCounterVal := True;
end;

procedure TWorkSpotF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  (dxDetailGrid as TdxDBGrid).DataSource.DataSet.Cancel;
  SetDetailPanel;
end;

// 20013551
procedure TWorkSpotF.DBRgrpEffBasedOnChange(Sender: TObject);
begin
  inherited;
  SetDetailPanel;
end;

{
// 20013551
function TWorkspotF.ActionTestShiftOverlap(AField: String): Boolean;
begin
  Result := False;
  with WorkspotDM.qryShiftOverlap do
  begin
    Close;
    ParamByName('SHIFT_NUMBER').AsString := AField;
    Open;
    while (not Result) and (not Eof) do
    begin
      if AField = FieldByName('SHIFT_NUMBER').AsString then
        Result := True;
      Next;
    end;
  end;
end; // ActionTestShiftOverlap
}

// 20013551
procedure TWorkSpotF.btnShiftsClick(Sender: TObject);
  procedure SelectShifts;
  var
    frm: TDialogMultipleSelectF;
    PlantCode, WorkspotCode: String;
  begin
    frm := TDialogMultipleSelectF.Create(Application);
{    frm.MyAction := ActionTestShiftOverlap; }
    // Only allow 1 item at a time for add/delete, or 'unselect all'.
    frm.btnSelectAll.Visible := False;
    try
      frm.lblCountry.Caption := SPimsWorkspotHeader;
      PlantCode := WorkspotDM.TableDetail.FieldByName('PLANT_CODE').AsString;
      WorkspotCode := WorkspotDM.TableDetail.FieldByName('WORKSPOT_CODE').AsString;
      frm.Init(SPimsShifts,
        'select t.workspot_code || '' | '' || t.description ' +
        ' from workspot t where t.plant_code = ' + '''' + PlantCode + '''' +
        ' and t.workspot_code = ' + '''' + WorkspotCode + '''',
        //Available,
        'select t.shift_number id, t.shift_number code, t.description description ' +
        ' from shift t ' +
        ' where t.shift_number not in ' +
        ' (select ws.shift_number from workspotshift ws ' +
        ' where ws.plant_code =  ' + '''' + PlantCode + '''' +
        ' and ws.workspot_code = ' + '''' + WorkspotCode + '''' + ') ' +
        ' order by t.shift_number',
        //Selected,
        'select t.shift_number id, t.shift_number code, t.description description ' +
        ' from shift t ' +
        ' where t.shift_number in ' +
        ' (select ws.shift_number from workspotshift ws ' +
        ' where ws.plant_code =  ' + '''' + PlantCode + '''' +
        ' and ws.workspot_code = ' + '''' + WorkspotCode + '''' + ') ' +
        ' order by t.shift_number',
        //Insert
        'insert into workspotshift(plant_code, workspot_code, ' +
        ' creationdate, mutationdate, mutator, shift_number) values ('
        + '''' + PlantCode + '''' + ','
        + '''' + WorkspotCode + '''' + ', sysdate, sysdate, ''' +
        SystemDM.CurrentProgramUser + ''', %s)',
        //Delete
        'delete from workspotshift ws ' +
        ' where ws.plant_code = ' + '''' + PlantCode + '''' +
        ' and ws.workspot_code = ' + '''' + WorkspotCode + '''' +
        ' and ws.shift_number = %s',
        //Exists
        'select NVL(count(*), 0) from workspotshift ws ' +
         ' where ws.plant_code = ' + '''' + PlantCode + '''' +
         ' and ws.workspot_code = ' + '''' + WorkspotCode + '''' +
         ' and ws.shift_number = %s'
         // This does not work correct yet!
       ,
       // Available for non-overlapping shifts
       'select s1.shift_number id, s1.shift_number code, s1.description ' +
       'from ' +
       '( ' +
       'select ' +
       '    s.plant_code, s.shift_number, s.description, ' +
       '    s.starttime1 shiftstarttime1, s.endtime1 shiftendtime1, ' +
       '    to_number(to_char(s.starttime1,''HH24MI'')) starttime1, ' +
       '    case when s.endtime1 < s.starttime1 ' +
       '     then to_number(to_char(trunc(s.endtime1),''HH24MI'')) + 2400 ' +
       '     else to_number(to_char(s.endtime1,''HH24MI'')) ' +
       '     end endtime1, ' +
       '     case when s.endtime1 < s.starttime1 ' +
       '     then to_number(to_char(trunc(s.endtime1),''HH24MI'')) ' +
       '     else to_number(to_char(s.starttime1,''HH24MI'')) ' +
       '     end starttime2, ' +
       '    to_number(to_char(s.endtime1,''HH24MI'')) endtime2 ' +
       'from shift s ' +
       'where s.plant_code = ' + '''' + PlantCode + '''' +
       ') ' +
       's1 left join ' +
       '( ' +
       'select ' +
       '    s.plant_code, s.shift_number, s.description, ' +
       '    s.starttime1 shiftstarttime1, s.endtime1 shiftendtime1, ' +
       '    to_number(to_char(s.starttime1,''HH24MI'')) starttime1, ' +
       '    case when s.endtime1 < s.starttime1 ' +
       '     then to_number(to_char(trunc(s.endtime1),''HH24MI'')) + 2400 ' +
       '     else to_number(to_char(s.endtime1,''HH24MI'')) ' +
       '     end endtime1, ' +
       '     case when s.endtime1 < s.starttime1 ' +
       '     then to_number(to_char(trunc(s.endtime1),''HH24MI'')) ' +
       '     else to_number(to_char(s.starttime1,''HH24MI'')) ' +
       '     end starttime2, ' +
       '    to_number(to_char(s.endtime1,''HH24MI'')) endtime2 ' +
       'from shift s ' +
       'where s.plant_code = ' + '''' + PlantCode + '''' +
       ') ' +
       's2 on ' +
       '  s1.plant_code = s2.plant_code and ' +
       '  not (s1.starttime1 > s2.starttime1 and s1.starttime1 < s2.endtime1) and ' +
       '  not (s1.endtime1 > s2.starttime1 and s1.endtime1 < s2.endtime1) and ' +
       '  not (s1.starttime1 > s2.starttime2 and s1.starttime1 < s2.endtime2) and ' +
       '  not (s1.endtime1 > s2.starttime2 and s1.endtime1 < s2.endtime2) and ' +
       '  not (s2.starttime1 > s1.starttime1 and s2.starttime1 < s1.endtime1) and ' +
       '  not (s2.endtime1 > s1.starttime1 and s2.endtime1 < s1.endtime1) and ' +
       '  not (s2.starttime1 > s1.starttime2 and s2.starttime1 < s1.endtime2) and ' +
       '  not (s2.endtime1 > s1.starttime2 and s2.endtime1 < s1.endtime2) ' +
       'where s1.plant_code = ' + '''' + PlantCode + '''' +
       ' and ' +
       '(s2.shift_number = %s and s1.shift_number <> %s) and ' +
       '(s2.shift_number = %s and s1.shift_number <> %s) and ' +
       '(s2.shift_number = %s and s1.shift_number <> %s) and ' +
       '(s2.shift_number = %s and s1.shift_number <> %s)' +
       'group by s1.shift_number, s1.shift_number, s1.description ' +
       'order by 1 ',
{
       'select s1.shift_number id, s1.shift_number code, s1.description ' +
       'from shift s1 left join shift s2 on ' +
       '  s1.plant_code = s2.plant_code and ' +
       '  not (s1.starttime1 > s2.starttime1 and s1.starttime1 < s2.endtime1) and ' +
       '  not (s1.endtime1 > s2.starttime1 and s1.endtime1 < s2.endtime1) ' +
       'where s1.plant_code = ' + '''' + PlantCode + '''' +
       ' and ' +
       's2.shift_number = %s and s1.shift_number <> %s ' +
       'order by 1 ',
}
        // All possible shifts
        'select t.shift_number id, t.shift_number code, t.description description ' +
        ' from shift t ' +
        ' order by t.shift_number'
      );
      if frm.ShowModal = mrOk then
        WorkspotDM.TableDetail.Refresh;
    finally
      frm.Free;
    end;
  end;
begin
  inherited;
  SelectShifts;
end;

// 20015178
procedure TWorkSpotF.dxDetailGridColumnTRByMachineGetText(Sender: TObject;
  ANode: TdxTreeListNode; var AText: String);
begin
  inherited;
  if AText = 'Y' then
    AText := DBRGrpTimeRecBasedOn.Items.Strings[0]
  else
    AText := DBRGrpTimeRecBasedOn.Items.Strings[1];
end;

// 20015178
procedure TWorkSpotF.DBRGrpTimeRecBasedOnChange(Sender: TObject);
begin
  inherited;
  SetDetailPanel;
  if SystemDM.EnableMachineTimeRec then
  begin
    with WorkspotDM do
    begin
      if DBRGrpTimeRecBasedOn.ItemIndex = 0 then
      begin
        if (TableDetail.State in [dsInsert, dsEdit]) then
        begin
          if TableDetail.FieldByName('FAKE_EMPLOYEE_NUMBER').AsString = '' then
          begin
            if WorkspotDM.FakeEmployeeNumber <> -1 then
              TableDetail.FieldByName('FAKE_EMPLOYEE_NUMBER').Value :=
                IntToStr(WorkspotDM.FakeEmployeeNumber);
          end;
        end;
      end
    end;
  end;
end;

// PIM-181
procedure TWorkSpotF.DescriptionEnable(AENable: Boolean);
begin
  if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
  begin
    if (WorkspotDM.TableDetail.State in [dsInsert]) then
      AEnable := True;
    if AEnable then
    begin
      DBeditDescription.Color := clRequired;
      DBEditDescription.ReadOnly := False;
      dxDetailGridColumnDescription.ReadOnly := False;
      dxDetailGridColumnDescription.DisableEditor := False;
    end
    else
    begin
      DBeditDescription.Color := clDisabled;
      DBEditDescription.ReadOnly := True;
      dxDetailGridColumnDescription.ReadOnly := True;
      dxDetailGridColumnDescription.DisableEditor := True;
    end;
  end;
end; // DescriptionEnable

// PIM-181
procedure TWorkSpotF.ShortNameEnable(AEnable: Boolean);
begin
  if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
  begin
    if (WorkspotDM.TableDetail.State in [dsInsert]) then
      AEnable := True;
    if AEnable then
    begin
      DBEditShortName.Color := clRequired;
      DBEditShortName.ReadOnly := False;
      dxDetailGridColumnShortName.ReadOnly := False;
      dxDetailGridColumnShortName.DisableEditor := False;
    end
    else
    begin
      DBEditShortName.Color := clDisabled;
      DBEditShortName.ReadOnly := True;
      dxDetailGridColumnShortName.ReadOnly := True;
      dxDetailGridColumnShortName.DisableEditor := True;
    end;
  end;
end; // ShortNameEnable

// PIM-181
procedure TWorkSpotF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  G: TCustomdxTreeListControl;
begin
  inherited;
  G := Sender as TCustomdxTreeListControl;
  if (G is TdxDBGrid) and ANode.HasChildren then
    Exit;
  if SystemDM.HybridMode <> hmHybrid then
    Exit;
  if ASelected then
  begin
//    if AFocused then
    begin
      if AColumn = dxDetailGridColumnDescription then
      begin
        DescriptionEnable(not (dxDetailGridColumnAutomatic.Field.AsString = 'Y'));
        ShortNameEnable(not (dxDetailGridColumnAutomatic.Field.AsString = 'Y'));
      end;
      if AColumn = dxDetailGridColumnShortName then
      begin
        DescriptionEnable(not (dxDetailGridColumnAutomatic.Field.AsString = 'Y'));
        ShortNameEnable(not (dxDetailGridColumnAutomatic.Field.AsString = 'Y'));
      end;
    end;
  end;
end; // dxGridCustomDrawCell

// PIM-181
procedure TWorkSpotF.DBEditDescriptionEnter(Sender: TObject);
begin
  inherited;
  if SystemDM.HybridMode = hmHybrid then
  begin
    DescriptionEnable(not DBCheckBoxAutomaticDC.Checked);
    ShortNameEnable(not DBCheckBoxAutomaticDC.Checked);
  end;
end;

// PIM-181
procedure TWorkSpotF.DBEditShortNameEnter(Sender: TObject);
begin
  inherited;
  if SystemDM.HybridMode = hmHybrid then
  begin
    DescriptionEnable(not DBCheckBoxAutomaticDC.Checked);
    ShortNameEnable(not DBCheckBoxAutomaticDC.Checked);
  end;
end;

procedure TWorkSpotF.dxGridEnter(Sender: TObject);
begin
  inherited;
  // PIM-181
  if SystemDM.HybridMode = hmHybrid then
  begin
    dxBarButtonEditMode.Visible := ivNever;
    dxDetailGrid.OptionsBehavior :=
      dxDetailGrid.OptionsBehavior -
        [edgoEditing, edgoImmediateEditor, edgoEnterShowEditor];
  end;
end;

end.
