unit QualityIncentiveConfDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TQualityIncentiveConfDM = class(TGridBaseDM)
    TableMasterMISTAKE: TIntegerField;
    TableMasterBONUS_PER_HOUR: TFloatField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterMISTAKECAL: TStringField;
    procedure TableMasterBeforePost(DataSet: TDataSet);
    procedure TableMasterCalcFields(DataSet: TDataSet);
    procedure TableMasterAfterPost(DataSet: TDataSet);
    procedure TableMasterAfterDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FLastMistake: Integer;
  public
    { Public declarations }
    procedure UpdateLastLine;
  end;

var
  QualityIncentiveConfDM: TQualityIncentiveConfDM;

implementation

uses SystemDMT, UPimsMessageRes, QualityIncentiveConfFRM;

{$R *.DFM}


procedure TQualityIncentiveConfDM.TableMasterBeforePost(DataSet: TDataSet);
var
  Bonus: String;
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if TableMaster.FieldByName('MISTAKE').AsInteger < 0 then
  begin
     DisplayMessage(SInvalidMistakeValue, mtInformation, [mbOk]);
     Exit;
  end;
  Bonus  :=
    Format('%*.*f', [4, 2, TableMasterBONUS_PER_HOUR.Value]);
  TableMasterBONUS_PER_HOUR.Value := StrToFloat(Bonus);  
end;

procedure TQualityIncentiveConfDM.TableMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  TableMaster.FieldByName('MISTAKECAL').AsString := '';
  if (TableMaster.FieldByName('MISTAKE').AsInteger = 999999) then
  begin
    if (FLastMistake <> 999999) then
      TableMaster.FieldByName('MISTAKECAL').AsString :=
        SMoreThan + IntToStr(FLastMistake);
  end
  else
    TableMaster.FieldByName('MISTAKECAL').AsString :=
      IntToStr(TableMaster.FieldByName('MISTAKE').AsInteger);
end;

procedure TQualityIncentiveConfDM.TableMasterAfterPost(DataSet: TDataSet);
var
  LastBonus: Real;
begin
  inherited;
  if (TableDetail.RecordCount = 1) then
  begin
    TableDetail.Refresh;
    TableDetail.First;
    FLastMistake := TableDetail.FieldByName('MISTAKE').AsInteger;
    LastBonus :=  TableDetail.FieldByName('BONUS_PER_HOUR').AsFloat;
    TableDetail.Insert;
    TableDetail.FieldByName('MISTAKE').AsInteger := 999999;
    TableDetail.FieldByName('BONUS_PER_HOUR').AsFloat := LastBonus;
    TableDetail.FieldByName('CREATIONDATE').Value := now();
    TableDetail.FieldByName('MUTATIONDATE').Value := now;
    TableDetail.FieldByName('MUTATOR').Value := SystemDM.CurrentProgramUser;
    TableDetail.Post;
  end
  else
    UpdateLastLine;
  TableMaster.Close;
  TableMaster.Open;
end;

procedure TQualityIncentiveConfDM.UpdateLastLine;
var
  LastBonus: Real;
begin
  TableDetail.Last;
  if (TableDetail.FieldByName('MISTAKE').AsInteger = 999999) then
    TableDetail.Prior;
  FLastMistake := TableDetail.FieldByName('MISTAKE').AsInteger;
  LastBonus :=  TableDetail.FieldByName('BONUS_PER_HOUR').AsFloat;
  TableDetail.FindKey([999999]);
  TableDetail.Edit;
  TableDetail.FieldByName('BONUS_PER_HOUR').AsFloat := LastBonus;
  TableDetail.Post;
end;

procedure TQualityIncentiveConfDM.TableMasterAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  if (TableDetail.RecordCount = 1) then
  begin
    TableDetail.Last;
    if (TableDetail.FieldByName('MISTAKE').AsInteger = 999999) then
         TableDetail.Delete;
  end
  else
    UpdateLastLine;

  TableMaster.Close;
  TableMaster.Open;
end;

procedure TQualityIncentiveConfDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableDetail.Open;
  if (TableDetail.RecordCount > 1) then
  begin
    TableDetail.Last;
    TableDetail.Prior;
    FLastMistake :=  TableDetail.FieldByName('MISTAKE').AsInteger;
  end
end;

end.
