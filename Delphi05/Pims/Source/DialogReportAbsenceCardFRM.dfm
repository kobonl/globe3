inherited DialogReportAbsenceCardF: TDialogReportAbsenceCardF
  Left = 276
  Top = 186
  Caption = 'Report absence card'
  ClientHeight = 281
  ClientWidth = 575
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 575
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 458
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 575
    Height = 161
    Align = alTop
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 26
      Visible = False
    end
    inherited LblPlant: TLabel
      Left = 48
      Top = 26
    end
    inherited LblFromEmployee: TLabel
      Top = 53
      Visible = False
    end
    inherited LblEmployee: TLabel
      Left = 48
      Top = 53
    end
    inherited LblToPlant: TLabel
      Top = 26
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Top = 53
      Visible = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 144
      Top = 308
      Visible = False
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 284
      Visible = False
    end
    object Label1: TLabel [8]
      Left = 48
      Top = 80
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Left = 16
      Top = 210
      Visible = False
    end
    inherited LblDepartment: TLabel
      Left = 48
      Top = 210
      Visible = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 136
      Top = 212
      Visible = False
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 352
      Top = 212
      Visible = False
    end
    inherited LblToDepartment: TLabel
      Left = 323
      Top = 212
      Visible = False
    end
    inherited LblStarPlantFrom: TLabel
      Left = 152
      Top = 27
    end
    inherited LblStarPlantTo: TLabel
      Left = 192
      Top = 27
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      Top = 24
      ColCount = 114
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Top = 24
      ColCount = 115
      TabOrder = 6
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 137
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 138
      TabOrder = 30
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Left = 144
      Top = 209
      ColCount = 129
      TabOrder = 7
      Visible = False
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 366
      Top = 209
      ColCount = 130
      TabOrder = 29
      Visible = False
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 538
      Top = 212
      Visible = False
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 50
      TabOrder = 1
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 53
      TabOrder = 5
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 137
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 138
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 150
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 151
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 151
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 152
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 158
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 157
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 120
      Top = 80
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object GroupBox1: TGroupBox
      Left = 48
      Top = 107
      Width = 281
      Height = 45
      Caption = 'Selections'
      TabOrder = 3
      object CheckBoxExport: TCheckBox
        Left = 10
        Top = 19
        Width = 247
        Height = 17
        Caption = 'Export'
        TabOrder = 0
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 222
    Width = 575
  end
  inherited pnlBottom: TPanel
    Top = 241
    Width = 575
    Height = 40
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  object Panel1: TPanel [5]
    Left = 0
    Top = 222
    Width = 575
    Height = 0
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 8
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 496
    Top = 320
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 528
    Top = 320
  end
  inherited StandardMenuActionList: TActionList
    Left = 456
    Top = 320
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 464
    Top = 80
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        39040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072B4469616C6F675265706F7274416273656E636543617264
        462E44617461536F75726365456D706C46726F6D104F7074696F6E7343757374
        6F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E645369
        7A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E
        53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E734442
        0B106564676F43616E63656C4F6E457869740D6564676F43616E44656C657465
        0D6564676F43616E496E73657274116564676F43616E4E617669676174696F6E
        116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265
        636F726473106564676F557365426F6F6B6D61726B7300000F54647844424772
        6964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E
        756D62657206536F7274656407046373557005576964746802410942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F454D50
        4C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F43
        6F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274204E61
        6D6505576964746802480942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060A53484F52545F4E414D4500000F546478444247
        726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E61
        6D6505576964746803F4000942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060B4445534352495054494F4E00000F54647844
        4247726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F
        6E060741646472657373055769647468022C0942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D6506074144445245535300000F54
        6478444247726964436F6C756D6E07436F6C756D6E350743617074696F6E060F
        4465706172746D656E7420636F64650942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060F4445504152544D454E545F434F44
        4500000F546478444247726964436F6C756D6E07436F6C756D6E360743617074
        696F6E06095465616D20636F64650942616E64496E646578020008526F77496E
        6465780200094669656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        4A030000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507294469616C6F675265706F72
        74416273656E636543617264462E44617461536F75726365456D706C546F104F
        7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E
        6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E6710
        6564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700
        094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D656467
        6F43616E44656C6574650D6564676F43616E496E73657274116564676F43616E
        4E617669676174696F6E116564676F436F6E6669726D44656C65746512656467
        6F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B73
        00000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02310942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274206E616D65055769647468024E0942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E11436F6C756D6E4465736372697074
        696F6E0743617074696F6E06044E616D6505576964746803CC000942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060B444553
        4352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D
        6E416464726573730743617074696F6E06074164647265737305576964746802
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D65060741444452455353000000}
    end
  end
end
