(*
  Changed:
  24-JUN-2019 GLOB3-321
  - Report inscan/outscan/absence/overtime scan-times do not fit
*)
unit ReportInOutAbsenceOvertimeQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls, SystemDMT, ReportInOutAbsenceOvertimeDMT;

type
  TQRParameters = class
  private
    FTeamFrom, FTeamTo: String;
    FDateFrom, FDateTo: TDateTime;
    FShowAllTeam,
    FShowSelection, FExportToFile, FNewPagePerTeam, FNewPagePerDate: Boolean;
  public
    procedure SetValues(
      TeamFrom, TeamTo: String;
      DateFrom, DateTo: TDateTime;
      ShowAllTeam,
      ShowSelection, ExportToFile, NewPagePerTeam, NewPagePerDate: Boolean
      );
  end;

type
  TReportInOutAbsenceOvertimeQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLBLToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblFromEmp: TQRLabel;
    QRLblEmp: TQRLabel;
    QRLblEmpFrom: TQRLabel;
    QRLblToEmp: TQRLabel;
    QRLblEmpTo: TQRLabel;
    QRLblFromTeam: TQRLabel;
    QRLblTeam: TQRLabel;
    QRLblTeamFrom: TQRLabel;
    QRLblToTeam: TQRLabel;
    QRLblTeamTo: TQRLabel;
    QRLblFromDate: TQRLabel;
    QRLblDate: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLblToDate: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRGroupHDTeam: TQRGroup;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRGroupHDDate: TQRGroup;
    QRLabel2: TQRLabel;
    QRDBText3: TQRDBText;
    QRGroupHDEmployee: TQRGroup;
    QRBandDetail: TQRBand;
    QRBandGroupFTEmployee: TQRBand;
    QRChildBandDateHeader: TQRChildBand;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLblInscan: TQRLabel;
    QRLblOutscan: TQRLabel;
    QRLblAbsCode: TQRLabel;
    QRLblAbsHrs: TQRLabel;
    QRLblRegHrs: TQRLabel;
    QRLblOT50: TQRLabel;
    QRLblOT100: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    ChildBandFTEmployee: TQRChildBand;
    QRMemoAbsCode: TQRMemo;
    QRMemoAbsHrs: TQRMemo;
    QRBandSummary: TQRBand;
    procedure FormCreate(Sender: TObject);
    procedure QRGroupHDEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFTEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFTEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDTeamAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDateAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRChildBandDateHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    FAbsenceCount: Integer;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    function QRSendReportParameters(
      const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
      const TeamFrom, TeamTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowAllTeam: Boolean;
      const ShowSelection, ExportToFile, NewPagePerTeam,
      NewPagePerDate: Boolean): Boolean;
    procedure FreeMemory; override;
    property QRParameters: TQRParameters read FQRParameters;
    property AbsenceCount: Integer read FAbsenceCount write FAbsenceCount;
  end;

var
  ReportInOutAbsenceOvertimeQR: TReportInOutAbsenceOvertimeQR;

implementation

{$R *.DFM}

uses
  UPimsMessageRes,
  UPimsConst,
  ListProcsFRM,
  UGlobalFunctions;

procedure TQRParameters.SetValues(
  TeamFrom, TeamTo: String;
  DateFrom, DateTo: TDateTime;
  ShowAllTeam,
  ShowSelection, ExportToFile, NewPagePerTeam, NewPagePerDate: Boolean);
begin
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowAllTeam := ShowAllTeam;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  FNewPagePerTeam := NewPagePerTeam;
  FNewPagePerDate := NewPagePerDate;
end;

{ TReportInOutAbsenceOvertimeQR }

procedure TReportInOutAbsenceOvertimeQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblTeamFrom.Caption := '*';
  QRLblTeamTo.Caption := '*';
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);;
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);;
  QRLblEmpFrom.Caption := '*';
  QRLblEmpTo.Caption := '*';
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    QRLblEmpFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmpTo.Caption := QRBaseParameters.FEmployeeTo;
  end;
  if not QRParameters.FShowAllTeam then
  begin
    QRLblTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLblTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRGroupHDTeam.ForceNewPage := QRParameters.FNewPagePerTeam;
  QRGroupHDDate.ForceNewPage := QRParameters.FNewPagePerDate;
end;

function TReportInOutAbsenceOvertimeQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  WhereStr: String;
begin
  with ReportInOutAbsenceOvertimeDM do
  begin
    WhereStr :=
        '  AND E.PLANT_CODE >= ' + '''' + QRBaseParameters.FPlantFrom + '''' + NL +
        '  AND E.PLANT_CODE <= ' + '''' + QRBaseParameters.FPlantTo + '''' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      if not QRParameters.FShowAllTeam then
        WhereStr := WhereStr +
          '  AND E.TEAM_CODE >= ' + '''' + QRParameters.FTeamFrom + '''' + NL +
          '  AND E.TEAM_CODE <= ' + '''' + QRParameters.FTeamTo + '''' + NL;
      WhereStr := WhereStr +
        '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    end;
    with qryEmpData do
    begin
      SelectStr :=
        'select ' + NL +
        '  e.team_code, ' + NL +
        '    te.description, ' + NL +
        '    t.shift_date, ' + NL +
        '    e.employee_number, ' + NL +
        '    e.description name, ' + NL +
        '    ''1-scan'' type, ' + NL +
        '    (select min(t2.datetime_in) from timeregscanning t2 where t2.employee_number = t.employee_number and t2.shift_date = t.shift_date) Inscan, ' + NL +
        '    (select max(t2.datetime_out) from timeregscanning t2 where t2.employee_number = t.employee_number and t2.shift_date = t.shift_date) Outscan, ' + NL +
        '    null as absencecode, ' + NL +
        '    null as absencedescription, ' + NL +
        '    null as absencehours, ' + NL +
        '    null as regular, ' + NL +
        '    null as overtime50, ' + NL +
        '    null as overtime100 ' + NL +
        '  from ' + NL +
        '    timeregscanning t inner join employee e on ' + NL +
        '      t.employee_number = e.employee_number ' + NL +
        '    inner join team te on ' + NL +
        '      e.team_code = te.team_code ' + NL +
        '  where ' + NL +
        '    t.shift_date >= :datefrom ' + NL +
        '    and t.shift_date <= :dateto ' + NL +
        WhereStr;
      SelectStr := SelectStr +
        '  union ' + NL +
        '    select ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      ahe.absencehour_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description name, ' + NL +
        '      ''5-absence'' type, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      ar.absencereason_code, ' + NL +
        '      ar.description, ' + NL +
        '      sum(ahe.absence_minute), ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null ' + NL +
        '    from ' + NL +
        '      absencehourperemployee ahe inner join employee e on ' + NL +
        '        ahe.employee_number = e.employee_number ' + NL +
        '      inner join absencereason ar on ' + NL +
        '        ahe.absencereason_id = ar.absencereason_id ' + NL +
        '      inner join team te on ' + NL +
        '        e.team_code = te.team_code ' + NL +
        '    where ' + NL +
        '      ahe.absencehour_date >= :datefrom ' + NL +
        '      and ahe.absencehour_date <= :dateto ' + NL +
        WhereStr;
      SelectStr := SelectStr +
        '    group by ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      ahe.absencehour_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      ar.absencereason_code, ' + NL +
        '      ar.description ' + NL +
        '  union ' + NL +
        '    select ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description name, ' + NL +
        '      ''2-regular'' type, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      sum(she.salary_minute), ' + NL +
        '      null, ' + NL +
        '      null ' + NL +
        '    from salaryhourperemployee she inner join employee e on ' + NL +
        '      she.employee_number = e.employee_number ' + NL +
        '      inner join team te on ' + NL +
        '        e.team_code = te.team_code ' + NL +
        '    where ' + NL +
        '      she.hourtype_number = 1 and ' + NL +
        '      she.salary_date >= :datefrom ' + NL +
        '      and she.salary_date <= :dateto ' + NL +
        WhereStr;
      SelectStr := SelectStr +
        '    group by ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null ' + NL +
        '  union ' + NL +
        '    select ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description name, ' + NL +
        '      ''3-overtime50'' type, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      sum(she.salary_minute), ' + NL +
        '      null ' + NL +
        '    from salaryhourperemployee she inner join employee e on ' + NL +
        '      she.employee_number = e.employee_number ' + NL +
        '      inner join hourtype h on ' + NL +
        '        she.hourtype_number = h.hourtype_number ' + NL +
        '      inner join team te on ' + NL +
        '        e.team_code = te.team_code ' + NL +
        '    where ' + NL +
        '      h.bonus_percentage = 50 and ' + NL +
        '      she.salary_date >= :datefrom ' + NL +
        '      and she.salary_date <= :dateto ' + NL +
        WhereStr;
      SelectStr := SelectStr +
        '    group by ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null ' + NL +
        '  union ' + NL +
        '    select ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description name, ' + NL +
        '      ''4-overtime100'' type, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      sum(she.salary_minute) ' + NL +
        '    from salaryhourperemployee she inner join employee e on ' + NL +
        '      she.employee_number = e.employee_number ' + NL +
        '      inner join hourtype h on ' + NL +
        '        she.hourtype_number = h.hourtype_number ' + NL +
        '      inner join team te on ' + NL +
        '        e.team_code = te.team_code ' + NL +
        '    where ' + NL +
        '      h.bonus_percentage = 100 and ' + NL +
        '      she.salary_date >= :datefrom ' + NL +
        '      and she.salary_date <= :dateto ' + NL +
        WhereStr;
      SelectStr := SelectStr +
        '    group by ' + NL +
        '      e.team_code, ' + NL +
        '      te.description, ' + NL +
        '      she.salary_date, ' + NL +
        '      e.employee_number, ' + NL +
        '      e.description, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null, ' + NL +
        '      null ' + NL +
        '  order by ' + NL +
        '    1, 3, 5, 6 ' + NL;
      Close;
      SQL.Clear;
      SQL.Add(SelectStr);
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      Open;
      Result := Not Eof;
    end;
  end;
end; // ExistsRecords

procedure TReportInOutAbsenceOvertimeQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

function TReportInOutAbsenceOvertimeQR.QRSendReportParameters(
  const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo,
  TeamFrom, TeamTo: String; const DateFrom,
  DateTo: TDateTime; const ShowAllTeam, ShowSelection,
  ExportToFile, NewPagePerTeam, NewPagePerDate: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(TeamFrom, TeamTo,
      DateFrom, DateTo,
      ShowAllTeam,
      ShowSelection, ExportToFile, NewPagePerTeam, NewPagePerDate);
  end;
  SetDataSetQueryReport(ReportInOutAbsenceOvertimeDM.qryEmpData);
end;

procedure TReportInOutAbsenceOvertimeQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption;
end;

procedure TReportInOutAbsenceOvertimeQR.QRGroupHDEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  AbsenceCount := 0;
  QRMemoAbsCode.Lines.Clear;
  QRMemoAbsHrs.Lines.Clear;
  QRLblInscan.Caption := '';
  QRLblOutscan.Caption := '';
  QRLblAbsCode.Caption := '';
  QRLblAbsHrs.Caption := '';
  QRLblRegHrs.Caption := '';
  QRLblOT50.Caption := '';
  QRLblOT100.Caption := '';
end;

procedure TReportInOutAbsenceOvertimeQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportInOutAbsenceOvertimeQR.QRBandDetailAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
  function TimeIn: String;
  var
    Hour, Min, Sec, MSec: Word;
  begin
    DecodeTime(
      ReportInOutAbsenceOvertimeDM.qryEmpData.FieldByName('INSCAN').AsDatetime,
      Hour, Min, Sec, MSec);
    Result := FillZero(Hour) + ':' + FillZero(Min) + ':' + FillZero(Sec);
  end;
  function TimeOut: String;
  var
    Hour, Min, Sec, MSec: Word;
  begin
    if ReportInOutAbsenceOvertimeDM.qryEmpData.FieldByName('OUTSCAN').AsString <> '' then
    begin
      DecodeTime(
        ReportInOutAbsenceOvertimeDM.qryEmpData.FieldByName('OUTSCAN').AsDatetime,
        Hour, Min, Sec, MSec);
      Result := FillZero(Hour) + ':' + FillZero(Min) + ':' + FillZero(Sec);
    end
    else
      Result := '';
  end;
begin
  inherited;
  with ReportInOutAbsenceOvertimeDM do
  begin
    with qryEmpData do
    begin
      if FieldByName('TYPE').AsString = '1-scan' then
      begin
        // GLOB3-321
        QRLblInscan.Caption := TimeIn;
        QRLblOutscan.Caption := TimeOut;
{
        QRLblInscan.Caption := TimeToStr(FieldByName('INSCAN').AsDatetime);
        if FieldByName('OUTSCAN').AsString <> '' then
          QRLblOutscan.Caption := TimeToStr(FieldByName('OUTSCAN').AsDateTime);
}
      end;
      if FieldByName('TYPE').AsString = '5-absence' then
      begin
        // There can be more absence lines!
        if AbsenceCount = 0 then
        begin
          QRLblAbsCode.Caption := FieldByName('ABSENCECODE').AsString + '-' + FieldByName('ABSENCEDESCRIPTION').AsString;
          QRLblAbsHrs.Caption := IntMin2StringTime(FieldByName('ABSENCEHOURS').AsInteger);
        end
        else
        begin
          QRMemoAbsCode.Lines.Add(FieldByName('ABSENCECODE').AsString + '-' + FieldByName('ABSENCEDESCRIPTION').AsString);
          QRMemoAbsHrs.Lines.Add(IntMin2StringTime(FieldByName('ABSENCEHOURS').AsInteger));
        end;
        AbsenceCount := AbsenceCount + 1;
      end;
      if FieldByName('TYPE').AsString = '2-regular' then
      begin
        QRLblRegHrs.Caption := IntMin2StringTime(FieldByName('REGULAR').AsInteger);
      end;
      if FieldByName('TYPE').AsString = '3-overtime50' then
      begin
        QRLblOT50.Caption := IntMin2StringTime(FieldByName('OVERTIME50').AsInteger);
      end;
      if FieldByName('TYPE').AsString = '4-overtime100' then
      begin
        QRLblOT100.Caption := IntMin2StringTime(FieldByName('OVERTIME100').AsInteger);
      end;
    end; // with
  end; /// with
end; // QRBandDetailAfterPrint

procedure TReportInOutAbsenceOvertimeQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  Printband := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      ExportClass.AddText(QRLblFromTeam.Caption + ' ' +
        QRLblTeam.Caption + ' ' + QRLblTeamFrom.Caption + ' ' +
        QRLblToTeam.Caption + ' ' + QRLblTeamTo.Caption);
      ExportClass.AddText(QRLblFromEmp.Caption + ' ' +
        QRLblEmp.Caption + ' ' + QRLblEmpFrom.Caption + ' ' +
        QRLblToEmp.Caption + ' ' + QRLblEmpTo.Caption);
      ExportClass.AddText(QRLblFromDate.Caption + ' ' +
        QRLblDate.Caption + ' ' +
        QRLblDateFrom.Caption + ' ' +
        QRLblToDate.Caption + ' ' +
        QRLblDateTo.Caption);
    end;
  end;
end;

procedure TReportInOutAbsenceOvertimeQR.ChildBandFTEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := AbsenceCount > 1;
end;

procedure TReportInOutAbsenceOvertimeQR.QRBandGroupFTEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  with ReportInOutAbsenceOvertimeDM do
  begin
    with qryEmpData do
    begin
      if QRParameters.FExportToFile and BandPrinted then
      begin
        ExportClass.AddText(
          FieldByName('EMPLOYEE_NUMBER').AsString + ExportClass.Sep +
          FieldByName('NAME').AsString + ExportClass.Sep +
          QRLblInscan.Caption + ExportClass.Sep +
          QRLblOutscan.Caption + ExportClass.Sep +
          QRLblAbsCode.Caption + ExportClass.Sep +
          QRLblAbsHrs.Caption + ExportClass.Sep +
          QRLblRegHrs.Caption + ExportClass.Sep +
          QRLblOT50.Caption + ExportClass.Sep +
          QRLblOT100.Caption
          );
        for I := 0 to QRMemoAbsCode.Lines.Count - 1 do
        begin
          ExportClass.AddText(
            ExportClass.Sep +
            ExportClass.Sep +
            ExportClass.Sep +
            ExportClass.Sep +
            QRMemoAbsCode.Lines.Strings[I] + ExportClass.Sep +
            QRMemoAbsHrs.Lines.Strings[I] + ExportClass.Sep +
            ExportClass.Sep +
            ExportClass.Sep
            );
        end;
      end;
    end; // with
  end; // with
end;

procedure TReportInOutAbsenceOvertimeQR.QRGroupHDTeamAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  with ReportInOutAbsenceOvertimeDM do
  begin
    with qryEmpData do
    begin
      if QRParameters.FExportToFile and BandPrinted then
      begin
        ExportClass.AddText(
          QRLabel1.Caption + ' ' + // Team:
          FieldByName('TEAM_CODE').AsString + ' ' +
          FieldByName('DESCRIPTION').AsString
          );
      end;
    end; // with
  end; // with
end;

procedure TReportInOutAbsenceOvertimeQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportInOutAbsenceOvertimeQR.QRGroupHDDateAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  with ReportInOutAbsenceOvertimeDM do
  begin
    with qryEmpData do
    begin
      if QRParameters.FExportToFile and BandPrinted then
      begin
        ExportClass.AddText(
          QRLabel2.Caption + ' ' + // Date:
          DateToStr(FieldByName('SHIFT_DATE').AsDateTime)
          );
      end;
    end; // with
  end; // with
end;

procedure TReportInOutAbsenceOvertimeQR.QRChildBandDateHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel3.Caption + ' ' + QRLabel4.Caption + ExportClass.Sep + // Employee Nr
      QRLabel5.Caption + ExportClass.Sep + // Name
      QRLabel6.Caption + ExportClass.Sep + // Inscan
      QRLabel7.Caption + ExportClass.Sep + // Outscan
      QRLabel8.Caption + ' ' + QRLabel9.Caption + ExportClass.Sep + // Absence Code
      QRLabel10.Caption + ExportClass.Sep + // Hrs
      QRLabel12.Caption + ' ' + QRLabel13.Caption + ExportClass.Sep + // Regular hrs
      QRLabel14.Caption + ' ' + QRLabel15.Caption + ExportClass.Sep + // Overtime50 hrs
      QRLabel16.Caption + ' ' + QRLabel17.Caption // Overtime100 hrs
      );
  end;
end;

end.
