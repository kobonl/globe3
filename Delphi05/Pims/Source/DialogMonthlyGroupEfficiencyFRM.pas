unit DialogMonthlyGroupEfficiencyFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls, SystemDMT, dxEdLib;

type
  TDialogMonthlyGroupEfficiencyF = class(TDialogReportBaseF)
    Label10: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    QueryWorkSpot: TQuery;
    QueryWorkSpotWORKSPOT_CODE: TStringField;
    QueryWorkSpotDESCRIPTION: TStringField;
    QueryWorkSpotPLANT_CODE: TStringField;
    Label22: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditMonth: TdxSpinEdit;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FMonth: Integer;
    FYear: Integer;
    FPlantFrom: String;
    FDeptFrom: String;
    FWorkspotFrom: String;
    FPlantTo: String;
    FWorkspotTo: String;
    FDeptTo: String;
    procedure FillDept;
    procedure FillWorkSpot;
    procedure CreateMonthlyGroupEfficiencyRecords;
  public
    { Public declarations }
    property Year: Integer read FYear write FYear;
    property Month: Integer read FMonth write FMonth;
    property PlantFrom: String read FPlantFrom write FPlantFrom;
    property PlantTo: String read FPlantTo write FPlantTo;
    property DeptFrom: String read FDeptFrom write FDeptFrom;
    property DeptTo: String read FDeptTo write FDeptTo;
    property WorkspotFrom: String read FWorkspotFrom write FWorkspotFrom;
    property WorkspotTo: String read FWorkspotTo write FWorkspotTo;
  end;

var
  DialogMonthlyGroupEfficiencyF: TDialogMonthlyGroupEfficiencyF;

implementation

{$R *.DFM}

uses
  ReportMonthlyGroupEfficiencyQRPT, ListProcsFRM, UPimsConst,
  DialogMonthlyGroupEfficiencyDMT;

procedure TDialogMonthlyGroupEfficiencyF.FormCreate(Sender: TObject);
begin
  inherited;
  DialogMonthlyGroupEfficiencyDM := TDialogMonthlyGroupEfficiencyDM.Create(nil);
  ReportMonthlyGroupEfficiencyQR :=
    CreateReportQR(TReportMonthlyGroupEfficiencyQR);
end;

procedure TDialogMonthlyGroupEfficiencyF.FormShow(Sender: TObject);
var
  AYear, AMonth, ADay: Word;
begin
  InitDialog(True, True, False, False, False, True, False);
  inherited;
  FillDept;
  FillWorkSpot;
  DecodeDate(Now, AYear, AMonth, ADay);
  dxSpinEditYear.IntValue := AYear;
  dxSpinEditMonth.IntValue := AMonth;
end;

procedure TDialogMonthlyGroupEfficiencyF.FillDept;
begin
{
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'DEPARTMENT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptTo, GetStrValue(CmbPlusPlantFrom.Value),
      'DEPARTMENT_CODE', False);
    ComboBoxPlusDeptFrom.Visible := True;
    ComboBoxPlusDeptTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusDeptFrom.Visible := False;
    ComboBoxPlusDeptTo.Visible := False;
  end;
}  
end;

procedure TDialogMonthlyGroupEfficiencyF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;

procedure TDialogMonthlyGroupEfficiencyF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue;
end;

procedure TDialogMonthlyGroupEfficiencyF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue;
end;

procedure TDialogMonthlyGroupEfficiencyF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillDept;
  FillWorkspot;
end;

procedure TDialogMonthlyGroupEfficiencyF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillDept;
  FillWorkspot;
end;

procedure TDialogMonthlyGroupEfficiencyF.CreateMonthlyGroupEfficiencyRecords;
begin
  try
    Screen.Cursor := crHourGlass;
    DialogMonthlyGroupEfficiencyDM.DetermineGroupEfficiency(Year, Month,
      PlantFrom, PlantTo, DeptFrom, DeptTo, WorkspotFrom, WorkspotTo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TDialogMonthlyGroupEfficiencyF.btnOkClick(Sender: TObject);
begin
  inherited;
  Year := Round(dxSpinEditYear.Value);
  Month := Round(dxSpinEditMonth.Value);
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);
  DeptFrom := GetStrValue(CmbPlusDepartmentFrom.Value);
  DeptTo := GetStrValue(CmbPlusDepartmentTo.Value);
  WorkspotFrom := GetStrValue(ComboBoxPlusWorkspotFrom.Value);
  WorkspotTo := GetStrValue(ComboBoxPlusWorkspotTo.Value);
  // First select and store all values (for MonthlyGroupEfficiency).
  try
    SystemDM.Pims.StartTransaction;
    CreateMonthlyGroupEfficiencyRecords;
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    On E: EDBEngineError do
    begin
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      DisplayMessage(GetErrorMessage(E, PIMS_POSTING), mtError, [mbOK]);
    end;
  end;
  // Now show report (only contents of MonthlyGroupEfficiency).
  if ReportMonthlyGroupEfficiencyQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(ComboBoxPlusWorkspotFrom.Value),
    GetStrValue(ComboBoxPlusWorkspotTo.Value),
    Round(dxSpinEditYear.Value),
    Round(dxSpinEditMonth.Value))
  then
    ReportMonthlyGroupEfficiencyQR.ProcessRecords;
end;

procedure TDialogMonthlyGroupEfficiencyF.FormDestroy(Sender: TObject);
begin
  inherited;
  DialogMonthlyGroupEfficiencyDM.Free;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogMonthlyGroupEfficiencyF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
