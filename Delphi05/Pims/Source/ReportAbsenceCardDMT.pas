(*
  MRA:17-DEC-2010 RV083.1.
  - This does not filter on absence reasons-per-country.
      Only show absence reasons at bottom of report
      that were shown in absence card.
  MRA:9-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
*)

unit ReportAbsenceCardDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportAbsenceCardDM = class(TReportBaseDM)
    QueryAbsence: TQuery;
    DataSourceAbsence: TDataSource;
    TablePlant: TTable;
    TableAbs: TTable;
    TableEmpl: TTable;
    QueryAbsencePlants: TQuery;
    qryAbsenceReasons: TQuery;
    qryEmployeeAvailability: TQuery;
    qryWork: TQuery; // RV083.1.
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportAbsenceCardDM: TReportAbsenceCardDM;

implementation

{$R *.DFM}

end.
