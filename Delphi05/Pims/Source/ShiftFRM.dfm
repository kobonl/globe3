inherited ShiftF: TShiftF
  Left = 269
  Top = 115
  Width = 738
  Caption = 'Shifts'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 722
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 720
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 720
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
        end>
      DefaultLayout = False
      KeyField = 'PLANT_CODE'
      DataSource = ShiftDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        DisableEditor = True
        Width = 75
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 279
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridColumn
        Caption = 'Address'
        DisableEditor = True
        Width = 490
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
    end
  end
  inherited pnlDetail: TPanel
    Width = 722
    OnEnter = pnlDetailEnter
    object GroupBoxDetail: TGroupBox
      Left = 1
      Top = 1
      Width = 720
      Height = 73
      Align = alTop
      Caption = 'Shifts'
      TabOrder = 0
      object LabelUnit: TLabel
        Left = 40
        Top = 15
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object LabelShift: TLabel
        Left = 40
        Top = 40
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object LabelHourType: TLabel
        Left = 368
        Top = 15
        Width = 48
        Height = 13
        Caption = 'Hour type'
      end
      object DBEditProcessUnit: TDBEdit
        Tag = 1
        Left = 80
        Top = 15
        Width = 65
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = ShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditShift: TDBEdit
        Tag = 1
        Left = 80
        Top = 40
        Width = 65
        Height = 19
        Ctl3D = False
        DataField = 'SHIFT_NUMBER'
        DataSource = ShiftDM.DataSourceDetail
        MaxLength = 2
        ParentCtl3D = False
        TabOrder = 2
      end
      object DBEditDescription: TDBEdit
        Tag = 1
        Left = 151
        Top = 40
        Width = 202
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = ShiftDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 3
      end
      object DBEdit1: TDBEdit
        Tag = 1
        Left = 151
        Top = 15
        Width = 202
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = ShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
      end
      object dxDBLookupEdit2: TdxDBLookupEdit
        Left = 440
        Top = 15
        Width = 209
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'HOURTYPELU'
        DataSource = ShiftDM.DataSourceDetail
        DropDownWidth = 220
        ClearKey = 46
        ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 74
      Width = 720
      Height = 90
      Align = alClient
      Caption = 'Times per Day'
      TabOrder = 1
      object Label4: TLabel
        Left = 40
        Top = 26
        Width = 24
        Height = 13
        Caption = 'Start'
      end
      object Label5: TLabel
        Left = 40
        Top = 50
        Width = 18
        Height = 13
        Caption = 'End'
      end
      object LabelMO: TLabel
        Left = 83
        Top = 12
        Width = 38
        Height = 13
        Caption = 'Monday'
      end
      object LabelTU: TLabel
        Left = 155
        Top = 12
        Width = 41
        Height = 13
        Caption = 'Tuesday'
      end
      object LabelWE: TLabel
        Left = 227
        Top = 12
        Width = 57
        Height = 13
        Caption = 'Wednesday'
      end
      object LabelTH: TLabel
        Left = 299
        Top = 12
        Width = 45
        Height = 13
        Caption = 'Thursday'
      end
      object LabelFR: TLabel
        Left = 371
        Top = 12
        Width = 30
        Height = 13
        Caption = 'Friday'
      end
      object LabelSA: TLabel
        Left = 443
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Saturday'
      end
      object LabelSU: TLabel
        Left = 515
        Top = 12
        Width = 36
        Height = 13
        Caption = 'Sunday'
      end
      object dxDBTimeEditST3: TdxDBTimeEdit
        Left = 224
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'STARTTIME3'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST4: TdxDBTimeEdit
        Left = 296
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 6
        DataField = 'STARTTIME4'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST5: TdxDBTimeEdit
        Left = 368
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 8
        DataField = 'STARTTIME5'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST6: TdxDBTimeEdit
        Left = 440
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 10
        DataField = 'STARTTIME6'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST7: TdxDBTimeEdit
        Left = 512
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 12
        DataField = 'STARTTIME7'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET3: TdxDBTimeEdit
        Left = 224
        Top = 52
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 5
        DataField = 'ENDTIME3'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET4: TdxDBTimeEdit
        Left = 296
        Top = 52
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 7
        DataField = 'ENDTIME4'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET5: TdxDBTimeEdit
        Left = 368
        Top = 52
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 9
        DataField = 'ENDTIME5'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET6: TdxDBTimeEdit
        Left = 440
        Top = 52
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 11
        DataField = 'ENDTIME6'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET7: TdxDBTimeEdit
        Left = 512
        Top = 52
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 13
        DataField = 'ENDTIME7'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEdit1: TdxDBTimeEdit
        Left = 80
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'STARTTIME1'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEdit2: TdxDBTimeEdit
        Left = 80
        Top = 51
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'ENDTIME1'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEdit3: TdxDBTimeEdit
        Left = 152
        Top = 27
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'STARTTIME2'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEdit4: TdxDBTimeEdit
        Left = 152
        Top = 51
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'ENDTIME2'
        DataSource = ShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 722
    TabOrder = 3
    inherited spltDetail: TSplitter
      Width = 720
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 720
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Shifts'
          Width = 201
        end
        item
          Caption = 'Monday'
          Width = 84
        end
        item
          Caption = 'Tuesday'
          Width = 81
        end
        item
          Caption = 'Wednesday'
          Width = 95
        end
        item
          Caption = 'Thursday'
          Width = 88
        end
        item
          Caption = 'Friday'
          Width = 64
        end
        item
          Caption = 'Saturday'
          Width = 74
        end
        item
          Caption = 'Sunday'
          Width = 74
        end>
      DefaultLayout = False
      KeyField = 'SHIFT_NUMBER'
      DataSource = ShiftDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        Width = 52
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHIFT_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        Width = 86
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 41
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 43
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 42
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 43
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 49
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 49
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 42
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 48
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 41
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 40
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 45
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn17: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 73
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
