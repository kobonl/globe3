(*
  Changes:
    MRA:24-MAY-2011. RV093.1. SO-20011749.
    - New report: Report Hours Per Contract Group Cumulative.
    - Based on ReportHrsPerWKCUMQRPT (Hrs Per Workspot Cumulative).
      - It can show hours and percentages:
        - per plant (always shown)
        - per business unit (optional)
        - per department (optional)
        - per workspot (optional)
        - per job (optional)
        - per contract group (always shown)
      - It can also show optional details about hours
        made on workspots and employees.
    MRA:9-DEC-2011. RV103.3. TODO-2019. Bugfix.
    - Report Hours per Contract Group Cumulative
      - Export is wrong, it exports hours as minutes, instead
        of hours:minutes-format.
    MRA:23-JAN-2012 RV104.3. 20011749. Addition.
    - It should also have a selection on contract group.
    MRA:25-JAN-2012 2.0.161.213.1. 20011749. Small change.
    - Report hours per Contract Group Cumulative
      - Display of 100 percent is not the same totals:
        - At 'totals' it is shown as '100 %'.
        - At other places it is shown as '100,00 %'.
      - Change this so it is always shown as '100,00 %'.
    MRA:27-JAN-2012 2.0.161.213.1. 20011749. Small change.
    - Change for selections shown in report:
      - The to'-date was overlapping, more space
        is needed.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  MRA:22-MAR-2013 TD-21527.
  - Also format hrs:minutes using DecodeHrsMin(value, True)
*)

unit ReportHrsPerContractGrpCUMQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo, FContractGroupFrom, FContractGroupTo: String;
    FShowBU, FShowDept, FShowWK, FShowJob, FShowEmpl,
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK, FPageJob: Boolean;
    FExportToFile, FAllEmployees, FIncludeNotConnectedEmp: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ContractGroupFrom, ContractGroupTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
      ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean);
  end;

  TAmountsPerDay = Array[1..7] of Integer;
  TReportHrsPerContractGrpCUMQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDContractGrp: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDJob: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelJobCodeFrom: TQRLabel;
    QRLabelJobCodeTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRBandFooterWK: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBandFooterDEPT: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRBandFooterJOB: TQRBand;
    QRLabel60: TQRLabel;
    QRDBText2: TQRDBText;
    QRBandFooterEmpl: TQRBand;
    QRLabel106: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRLabelTotJob: TQRLabel;
    QRDBTextJobCode: TQRDBText;
    QRDBTextJobDesc: TQRDBText;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabelEmplHrs: TQRLabel;
    QRLabelAvgEmpl: TQRLabel;
    QRLabelEmplDay: TQRLabel;
    QRLabelJobHrs: TQRLabel;
    QRLabelAvgJob: TQRLabel;
    QRLabelJobDay: TQRLabel;
    QRLabelDeptHrs: TQRLabel;
    QRLabelAvgDept: TQRLabel;
    QRLabelDeptDay: TQRLabel;
    QRLabelBUHrs: TQRLabel;
    QRLabelAvgBU: TQRLabel;
    QRLabelBUDay: TQRLabel;
    QRLabelPlantHrs: TQRLabel;
    QRLabelAvgPlant: TQRLabel;
    QRLabelPlantDay: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRBand1: TQRBand;
    QRLabel8: TQRLabel;
    QRLabel10: TQRLabel;
    QRBandSummary: TQRBand;
    QRDBText18: TQRDBText;
    QRDBText7: TQRDBText;
    ChildBandFooterWK: TQRChildBand;
    QRLabelTotWK: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelWKHrs: TQRLabel;
    QRLabelAvgWK: TQRLabel;
    QRLabelWKDay: TQRLabel;
    QRLabelWKUnknownBU: TQRLabel;
    QRLabelWKHrsUN: TQRLabel;
    QRLabelAvgWKUN: TQRLabel;
    QRLabelWKDayUN: TQRLabel;
    ChildBandFooterWK2: TQRChildBand;
    QRLabelUnknownBU2: TQRLabel;
    QRLabelWKHrsUN2: TQRLabel;
    QRLabelAvgWKUN2: TQRLabel;
    QRLabelWKDayUN2: TQRLabel;
    QRLabel4: TQRLabel;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRBandFooterContractGrp: TQRBand;
    QRLabelTotCG: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRLabelCGHrs: TQRLabel;
    QRLabelCGPerc: TQRLabel;
    QRLabelContractGroupTo: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabelContractGroupFrom: TQRLabel;
    QRLabelContractGroup: TQRLabel;
    QRLabel14: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJOBAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDJobAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure ChildBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandFooterWK2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterWK2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDContractGrpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterContractGrpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterContractGrpAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
    Average: Integer; // MR:17-12-2002
    AverageUnknown: Integer;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    LevelPlant, LevelBU, LevelDept,
    LevelWK, LevelJob, LevelEmpl, WKHasJobDummy: Boolean;
    LastLevel: Char;
    TotalEmpl, TotalBU, TotalWK, TotalJob, TotalDept, TotalPlant, TotalContrGrp,
    FDaysBU, FDaysDept, FDaysPlant, FDaysWK, FDaysJob, FDaysEmpl: Integer;
    AllPlants, AllBusinessUnits: Boolean;
    TotalWKUnknown, TotalJobUnknown: Integer;
    FDaysWKUnknown, FDaysJobUnknown: Integer;
    procedure SetLevelReport;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ContractGroupFrom, ContractGroupTo,
      EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob: Boolean;
      const ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure AddAmounts(Amount: Integer; BU, Dept, WK, Job, Empl: Boolean);
    function DetermineSUMMIN: Integer;
  end;

var
  ReportHrsPerContractGrpCUMQR: TReportHrsPerContractGrpCUMQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportHrsPerContractGrpCUMDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes;

// MR:17-12-2002
procedure ExportDetail(
  Workspot, WorkspotCode, WorkspotDescription, TotalHours,
  AveragePerDay, WorkingDay: String);
begin
  ExportClass.AddText(
    Workspot + ExportClass.Sep +
    WorkspotCode + ExportClass.Sep +
    WorkspotDescription + ExportClass.Sep +
    TotalHours + ExportClass.Sep +
    AveragePerDay + ExportClass.Sep +
    WorkingDay
    );
end;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ContractGroupFrom, ContractGroupTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
      ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FWKFrom := WKFrom;
  FWKTo := WKTo;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FContractGroupFrom := ContractGroupFrom;
  FContractGroupTo := ContractGroupTo;
  FShowBU := ShowBU;
  FShowDept := ShowDept;
  FShowWK := ShowWK;
  FShowJob := ShowJob;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FPageJob := PageJob;
  FExportToFile := ExportToFile;
  FAllEmployees := AllEmployees;
  FIncludeNotConnectedEmp := IncludeNotConnectedEmp;
end;

function TReportHrsPerContractGrpCUMQR.QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ContractGroupFrom, ContractGroupTo,
      EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob: Boolean;
      const ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ContractGroupFrom, ContractGroupTo,
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
      ExportToFile, AllEmployees, IncludeNotConnectedEmp);
  end;
  SetDataSetQueryReport(ReportHrsPerContractGrpCUMDM.QueryProdHour);
end;

function TReportHrsPerContractGrpCUMQR.ExistsRecords: Boolean;
var
  SelectStr, SelectStr1, SelectStr2, SelectStr3, TempStr: String;
  SelectEmployeeStr: String;
  SelectStrSummarize, SumSelectStr, SumGroupByStr: String;
//  DateTmp: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  SetLevelReport;

  AllPlants := ReportHrsPerContractGrpCUMDM.DetermineAllPlants(
    QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllBusinessUnits := ReportHrsPerContractGrpCUMDM.DetermineAllBusinessUnits(
    QRParameters.FBusinessFrom, QRParameters.FBusinessTo);

  // Plant
  TempStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, PL.DESCRIPTION AS PDESC ' + NL;
  SelectStr1 := TempStr;
  SelectStr2 := TempStr;
  SelectStr3 := TempStr;

  // Summarize
  SumSelectStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE ' + NL;
  SumGroupByStr :=
    'GROUP BY ' + NL +
    '  A.PLANT_CODE ' + NL;

  if QRParameters.FShowBU then
  begin
    // Summarize
    SumSelectStr := SumSelectStr +
      ' , A.BUSINESSUNIT_CODE ' + NL;
    SumGroupByStr := SumGroupByStr +
      ' , A.BUSINESSUNIT_CODE ' + NL;
//
    TempStr :=
      ', J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', BW.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      ', J.BUSINESSUNIT_CODE, CAST ('''' AS VARCHAR(30)) as BDESC ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
//
  end;
  if QRParameters.FShowDept then
  begin
    // Summarize
    SumSelectStr := SumSelectStr +
      ' , A.DEPARTMENT_CODE ' + NL;
    SumGroupByStr := SumGroupByStr +
      ' , A.DEPARTMENT_CODE ' + NL;

    TempStr :=
      ', W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;

  // Contract Group
  TempStr :=
    ', CG.CONTRACTGROUP_CODE, CG.DESCRIPTION AS CGDESC ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  if QRParameters.FShowWK then
  begin
    TempStr :=
      ', P.WORKSPOT_CODE, W.DESCRIPTION AS WDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowJob then
  begin
//
    TempStr :=
      ', P.JOB_CODE, J.DESCRIPTION AS JDESC ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', P.JOB_CODE, CAST ('''' AS VARCHAR(30)) AS JDESC ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      ', P.JOB_CODE, CAST ('''' AS VARCHAR(30)) AS JDESC ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
//
  end;
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      ', P.EMPLOYEE_NUMBER ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  // MR:06-10-2005 The 'businessunit' is always needed for determination
  // of the Businessunitperworkspot.percentage.
  // But if it is put in the 'SELECT' (when FShowBU is False),
  // this will trigger the Workspot, even if the plant+workspot is the same,
  // if there are 2 same workspots for 2 different businessunits.
  // By putting the businessunitcode as a 'max'-value this can be solved.
  TempStr :=
    ', P.PRODHOUREMPLOYEE_DATE ' + NL +
    ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  // RV067.MRA.2. 550496.
  // MRA:21-JUL-2010 Calculate the total minutes here!
  TempStr :=
    ', P.PRODHOUREMPLOYEE_DATE ' + NL +
    ', SUM(P.PRODUCTION_MINUTE * BW.PERCENTAGE / 100) AS SUMMIN ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    ', P.PRODHOUREMPLOYEE_DATE ' + NL +
    ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(1) AS LISTCODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    ', MAX(BW.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(2) AS LISTCODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(3) AS LISTCODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  // MR:21-1-2005 should be '< :FENDDATE' instead of '<= :FENDDATE'!,
  // because of 'enddate + 1'.
  TempStr :=
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN PLANT PL ON ' + NL +
    '    P.PLANT_CODE = PL.PLANT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if not AllPlants then
  begin
    TempStr :=
      '    AND P.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '    AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    '    AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
    '    AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  // Contract group
  TempStr :=
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN CONTRACTGROUP CG ON ' + NL +
    '    E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  TempStr :=
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (P.JOB_CODE <> ''' + DUMMYSTR + ''')' + NL +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND P.JOB_CODE = J.JOB_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''N'' ' + NL +
    '  INNER JOIN BUSINESSUNITPERWORKSPOT BW ON' + NL +
    '    BW.PLANT_CODE = P.PLANT_CODE ' + NL +
    '    AND BW.WORKSPOT_CODE = P.WORKSPOT_CODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (P.JOB_CODE = ''' + DUMMYSTR + ''')' + NL +
    '  LEFT JOIN JOBCODE J ON ' + NL + // Left Join -> Jobs do not exist
    '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND P.JOB_CODE = J.JOB_CODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  if QRParameters.FShowDept then
  begin
    TempStr :=
      '  INNER JOIN DEPARTMENT D ON ' + NL +
      '    W.PLANT_CODE = D.PLANT_CODE ' + NL +
      '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowBU then
  begin
//
    TempStr :=
      '  INNER JOIN BUSINESSUNIT B ON ' + NL +
      '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      '  INNER JOIN BUSINESSUNIT B ON ' + NL +
      '    BW.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      '  LEFT JOIN BUSINESSUNIT B ON ' + NL + // Left Join -> Jobs do not exist
      '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
//
  end;
  // This join gives an 'endless loop' (?)
(*
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      '  INNER JOIN EMPLOYEE E ON ' + NL +
      '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;

    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
*)
  // RV104.3.
  TempStr :=
    'WHERE ' + NL +
    '  E.CONTRACTGROUP_CODE >= ''' +
    DoubleQuote(QRParameters.FContractGroupFrom) + '''' + NL +
    '  AND E.CONTRACTGROUP_CODE <= ''' +
    DoubleQuote(QRParameters.FContractGroupTo) + '''' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
//
    // RV104.3.
{
    TempStr :=
      'WHERE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
}
    if not AllBusinessUnits then
    begin
      TempStr :=
        // RV104.3. AND added.
        '  AND J.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND J.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
    end;
    if not AllBusinessUnits then
    begin
      TempStr :=
        // RV104.3. AND added.
        '  AND BW.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND BW.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
      SelectStr2 := SelectStr2 + TempStr;
    end;
    // SelectStr3 -> There are no jobs / no businesunits.
//
    // RV104.3.
{
    if not AllBusinessUnits then
    begin
      TempStr := ' AND ';
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      // SelectStr3 -> There are no jobs / no businessunits.
    end;
}
    // RV050.4.
    if QRParameters.FAllEmployees then
      SelectEmployeeStr := ''
    else
      // RV104.3. AND added.
      SelectEmployeeStr :=
        '  AND P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    // RV104.3.
//        '  AND ';
    TempStr := SelectEmployeeStr +
      // RV104.3. AND added.
      ' AND W.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      ' AND W.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      ' AND P.WORKSPOT_CODE >= ''' +
      DoubleQuote(QRParameters.FWKFrom) + '''' + NL +
      ' AND P.WORKSPOT_CODE <= ''' +
      DoubleQuote(QRParameters.FWKTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    if (QRParameters.FWKFrom = QRParameters.FWKTo) then
    begin
      TempStr :=
        ' AND P.JOB_CODE >= ''' +
        DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
        ' AND P.JOB_CODE <= ''' +
        DoubleQuote(QRParameters.FJobTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      if (QRParameters.FJobTo <> '') and (QRParameters.FJobFrom <> '') then
      begin
        SelectStr2 := SelectStr2 + TempStr;
        SelectStr3 := SelectStr3 + TempStr;
      end;
    end;
  end; // if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  TempStr := ' ' +
    'GROUP BY ' + NL +
    '  P.PLANT_CODE, PL.DESCRIPTION ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if QRParameters.FShowBU then
  begin
//
    TempStr :=
      ', J.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', BW.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      ', J.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
//
  end;
  if QRParameters.FShowDept then
  begin
    TempStr :=
      ', W.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;

  // Contract group
  TempStr :=
    ', CG.CONTRACTGROUP_CODE, CG.DESCRIPTION ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  if QRParameters.FShowWK then
  begin
    TempStr :=
      ', P.WORKSPOT_CODE, W.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowJob then
  begin
    TempStr :=
      ', P.JOB_CODE, J.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', P.JOB_CODE ' + NL; // Oracle->Leave out J.DESCRIPTION
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      ', P.EMPLOYEE_NUMBER ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    ', P.PRODHOUREMPLOYEE_DATE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  SelectStr := SelectStr1 + ' UNION ' + NL +
    SelectStr2 + ' UNION ' + NL +
    SelectStr3 + NL;

  // Order by
  SelectStr := SelectStr +
    ' ORDER BY ' + NL +
    '   PLANT_CODE ' + NL;
  if QRParameters.FShowBU then
    SelectStr := SelectStr +
      ', BUSINESSUNIT_CODE ' + NL;
  if QRParameters.FShowDept then
    SelectStr := SelectStr +
      ', DEPARTMENT_CODE ' + NL;
  SelectStr := SelectStr +
    ', CONTRACTGROUP_CODE ' + NL;
  if QRParameters.FShowWK then
    SelectStr := SelectStr +
      ', WORKSPOT_CODE ' + NL;
  if QRParameters.FShowJob then
    SelectStr := SelectStr +
      ', JOB_CODE ' + NL;
  if QRParameters.FShowEmpl then
    SelectStr := SelectStr +
      ', EMPLOYEE_NUMBER ' + NL;

  // Select needed for calculation per pecentage based on sum
  // per Plant and/or Businessunit_code and/or Department.
  SelectStrSummarize :=
    SumSelectStr +
    '  , SUM(A.SUMMIN) SUMMIN ' + NL +
    ' FROM (' + NL +
    SelectStr +
    ') A ' + NL +
    SumGroupByStr;
  SelectStrSummarize := SelectStrSummarize +
    ' ORDER BY ' + NL +
    '   PLANT_CODE ' + NL;
  if QRParameters.FShowBU then
    SelectStrSummarize := SelectStrSummarize +
      '  ,BUSINESSUNIT_CODE ' + NL;
  if QRParameters.FShowDept then
    SelectStrSummarize := SelectStrSummarize +
      '  ,DEPARTMENT_CODE ' + NL;

  with ReportHrsPerContractGrpCUMDM.QueryProdHour do
  begin
    Active := False;
    Unidirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
// SQL.SaveToFile('c:\temp\rephrspercontractgrpcum.sql');
//CAR 12.12.2002
    ParamByName('FSTARTDATE').Value := GetDate(QRParameters.FDateFrom);
    ParamByName('FENDDATE').Value := GetDate(QRParameters.FDateTo + 1);
    if not Prepared then
      Prepare;
    Active := True;
    Result := (not IsEmpty);
  end;
  if Result then
  begin
    with ReportHrsPerContractGrpCUMDM.qryProdHourSum do
    begin
      Close;
      SQL.Clear;
      SQL.Add(SelectStrSummarize);
// SQL.SaveToFile('c:\temp\rephrspercontractgrpcumsum.sql');
      ParamByName('FSTARTDATE').Value := GetDate(QRParameters.FDateFrom);
      ParamByName('FENDDATE').Value := GetDate(QRParameters.FDateTo + 1);
      Open;
    end;
    ReportHrsPerContractGrpCUMDM.cdsBUPerWK.Close;
    ReportHrsPerContractGrpCUMDM.cdsBUPerWK.Open;
    ReportHrsPerContractGrpCUMDM.cdsEmpl.Close;
    ReportHrsPerContractGrpCUMDM.cdsEmpl.Open;
  end;
  Screen.Cursor := crDefault;
end;

function TReportHrsPerContractGrpCUMQR.DetermineSUMMIN: Integer;
begin
  Result := ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('SUMMIN').AsInteger;
end;

procedure TReportHrsPerContractGrpCUMQR.SetLevelReport;
begin
  LevelPlant := True;
  LevelBU := False;
  LevelDept := False;
  LevelWK := False;
  LevelJob := False;
  LevelEmpl := False;
  LastLevel := 'P';
  if QRParameters.FShowBU then
  begin
    LevelBU := True;
    LastLevel := 'B';
  end;
  if QRParameters.FShowDept then
  begin
    LevelDept := True;
    LastLevel := 'D';
  end;
  if QRParameters.FShowWK then
  begin
    LevelWK := True;
    LastLevel := 'W';
  end;
  if QRParameters.FShowJob then
  begin
    LevelJob := True;
    LastLevel := 'J';
  end;
  if QRParameters.FShowEmpl then
  begin
    LevelEmpl := True;
    LastLevel := 'E';
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.ConfigReport;
begin
  // MR:17-12-2002
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := QRParameters.FWKFrom;
  QRLabelWKTo.Caption := QRParameters.FWKTo;
  QRLabelJobCodeFrom.Caption := QRParameters.FJobFrom;
  QRLabelJobCodeTo.Caption := QRParameters.FJobTo;
  // RV104.3. ContractGroup
  QRLabelContractGroupFrom.Caption := QRParameters.FContractGroupFrom;
  QRLabelContractGroupTo.Caption := QRParameters.FContractGroupTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;

  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
  if (QRParameters.FWKFrom <> QRParameters.FWKTo) then
  begin
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
  end;
  // RV050.4.
  if QRParameters.FAllEmployees then
  begin
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
{
  // Contract Group Report
  if not QRParameters.FShowJob then
  begin
    QRLabel18.Caption := '';
    QRLabelAbsenceRsn.Caption := '';
    QRLabelJobCodeFrom.Caption := '';
    QRLabel22.Caption := '';
    QRLabelJobCodeTo.Caption := '';
  end;
}  
(*
  QRGroupHDBU.Enabled := False;
  QRBandFooterBU.Enabled := False;
  QRGroupHDDept.Enabled := False;
  QRBandFooterDept.Enabled := False;
  QRGroupHDWK.Enabled := False;
  QRBandFooterWK.Enabled := False;
  QRGroupHDJob.Enabled := False;
  QRBandFooterJob.Enabled := False;
  QRGroupHDEmpl.Enabled := False;
  QRBandFooterEmpl.Enabled := False;

  if QRParameters.FShowBU then
  begin
    if LastLevel <> 'B' then
      QRGroupHDBU.Enabled := True;
    QRBandFooterBU.Enabled := True;
  end;
  if QRParameters.FShowDept then
  begin
    if LastLevel <> 'D' then
      QRGroupHDDept.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FShowWK then
  begin
    if LastLevel <> WORKTIMEREDUCTIONAVAILABILITY then
      QRGroupHDWK.Enabled := True;
    QRBandFooterWK.Enabled := True;
  end;
  if QRParameters.FShowJob then
  begin
    if LastLevel <> 'J' then
      QRGroupHDJob.Enabled := True;
    QRBandFooterJob.Enabled := True;
  end;
  if QRParameters.FShowEmpl then
    QRBandFooterEmpl.Enabled := True;
*)
end;

procedure TReportHrsPerContractGrpCUMQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportHrsPerContractGrpCUMQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:17-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  TotalPlant := 0;
  TotalEmpl := 0;
  TotalWK := 0;
  TotalJob := 0;
  TotalDept := 0;
  TotalContrGrp := 0;
  TotalBU := 0;
  FDaysPlant := 0;
  FDaysBU := 0;
  FDaysDept := 0;
  FDaysWK := 0;
  FDaysJob := 0;
  FDaysEmpl := 0;
  TotalWKUnknown := 0;
  TotalJobUnknown := 0;
  FDaysWKUnknown := 0;
  FDaysJobUnknown := 0;

  // MR:17-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(QRLabel11.Caption);
        // From Plant to Plant
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelPlantTo.Caption
          );
        // From BU to BU
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelBusinessTo.Caption
          );
        // From Dept to Dept
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelDeptTo.Caption
          );
        // From Workspot to Workspot
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel88.Caption + ' ' +
          QRLabelWKFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelWKTo.Caption
          );
        // From Jobcode to Jobcode
        // Contract Group report
        if QRParameters.FShowJob then
          ExportClass.AddText(
            QRLabel13.Caption + ' ' + { From } QRLabelAbsenceRsn.Caption + ' ' +
            QRLabelJobCodeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
            QRLabelJobCodeTo.Caption
            );
        // RV104.3.
        // From ContractGroup to ContractGroup
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelContractGroup.Caption + ' ' +
          QRLabelContractGroupFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelContractGroupTo.Caption
          );
        // From Employee to Employee
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelEmployeeTo.Caption
          );
        // From date to date
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelDate.Caption + ' ' +
          QRLabelDateFrom.Caption + ' ' + QRLabel2.Caption + ' ' + { To }
          QRLabelDateTo.Caption
          );
        // Column-headers
        ExportDetail('', '', '', QRLabel8.Caption, {QRLabel9.Caption,}  '',
          QRLabel10.Caption);
      end;
    end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowWK then
  begin
    PrintBand := False;
    Exit;
  end;
  TotalWK := 0;
  FDaysWK := 0;
  TotalWKUnknown := 0;
  FDaysWKUnknown := 0;
  WKHasJobDummy := False;
  if LastLevel = 'W' then
    PrintBand := False;
  QRGroupHdWK.ForceNewPage := QRParameters.FPageWK;
(*
//  WKHasJobDummy := False;
  if LevelWK then
 // if (ReportHrsPerContractGrpCUMDM.QueryBUPerWK.FieldByName('SUMMin').AsFloat <> 0) then
 //   WKHasJobDummy := True;
*)
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowWK then
  begin
    PrintBand := False;
    Exit;
  end;
  // UNKNOWN (WORKSPOT)
  PrintBand := False;
  if (LastLevel <> 'W')  then
    if (TotalWKUnknown <> 0) then
    begin
      PrintBand := True;
      QRLabelWKHrsUN.Caption := DecodeHrsMin(TotalWKUnknown, True);
      QRLabelAvgWKUN.Caption := '00:00';
      QRLabelWKDayUN.Caption := IntToStr(FDaysWKUnknown);
      AverageUnknown := 0;
      if FDaysWKUnknown <> 0 then
      begin
        AverageUnknown := Round(TotalWKUnknown / FDaysWKUnknown);
        QRLabelAvgWKUN.Caption := DecodeHrsMin(AverageUnknown, True);
      end;
    end;
end;

// MR: 10-11-2005 Workspot - Unknown Business Unit
procedure TReportHrsPerContractGrpCUMQR.ChildBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowWK then
  begin
    PrintBand := False;
    Exit;
  end;
  // WORKSPOT
  if LastLevel = 'W'  then
    QRLabelTotWK.Caption := SRepHrsCUMWK
  else
  begin
    QRLabelTotWK.Caption := SRepHrsTotalCUM;
    // Also add unknown to total workspot!
    TotalWK := TotalWK + TotalWKUnknown;
  end;
(*
  if WKHasJobDummy then
    AddAmounts(DetermineSUMMIN, True, True, True,False, False);
*)
  QRLabelWKHrs.Caption := DecodeHrsMin(TotalWK, True);
  QRLabelAvgWK.Caption := '00:00';
  QRLabelWKDay.Caption := IntToStr(FDaysWK);
  Average := 0;
  if FDaysWK <> 0 then
  begin
    Average := Round(TotalWK / FDaysWK);
    QRLabelAvgWK.Caption := DecodeHrsMin(Average, True);
  end;
  // Contract group report
  QRLabelAvgWK.Caption := '';
  QRLabelWKDay.Caption := '';
end;

// MR: 10-11-2005 Workspot - Unknown Business Unit
procedure TReportHrsPerContractGrpCUMQR.ChildBandFooterWK2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowWK then
  begin
    PrintBand := False;
    Exit;
  end;
  // UNKNOWN (WORKSPOT)
  PrintBand := False;
  if (LastLevel = 'W')  then
    if (TotalWKUnknown <> 0) then
    begin
      PrintBand := True;
      QRLabelWKHrsUN2.Caption := DecodeHrsMin(TotalWKUnknown, True);
      QRLabelAvgWKUN2.Caption := '00:00';
      QRLabelWKDayUN2.Caption := IntToStr(FDaysWKUnknown);
      AverageUnknown := 0;
      if FDaysWKUnknown <> 0 then
      begin
        AverageUnknown := Round(TotalWKUnknown / FDaysWKUnknown);
        QRLabelAvgWKUN2.Caption := DecodeHrsMin(AverageUnknown, True);
      end;
    end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TotalPlant := 0;
  FDaysPlant := 0;
  QRGroupHDPlant.ForceNewPage := QRParameters.FPagePlant;
//  WKHasJobDummy := False;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // Contract Group Report
  // Should always be 'total plant' because contract group is always shown
{  if (LastLevel = 'P') then
    QRLabelTotPlant.Caption := SRepHrsCUMWKPlant
  else }
    QRLabelTotPlant.Caption := SRepHrsTotalCUMPlant;
  QRLabelPlantHrs.Caption := DecodeHrsMin(TotalPlant, True);
  QRLabelAvgPlant.Caption := '00:00';
  QRLabelPlantDay.Caption := IntToStr(FDaysPlant);
  Average := 0;
  if FDaysPlant <> 0 then
  begin
    Average := Round(TotalPlant / FDaysPlant);
    QRLabelAvgPlant.Caption := DecodeHrsMin(Average, True);
  end;
  // Contract Group Report
  QRLabelAvgPlant.Caption := '';
  if (not QRParameters.FShowBU) and (not QRParameters.FShowDept) then
    QRLabelPlantDay.Caption := // '100 %'
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [100.0]) + ' %' // 2.0.161.213.1.
      Format('%.2n', [100.0]) + ' %' // 2.0.161.213.1.
  else
    QRLabelPlantDay.Caption := ''
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowBU;
  if PrintBand then
  begin
    TotalBU := 0;
    FDaysBU := 0;
    PrintBand := QRParameters.FShowBU;
    QRGroupHdBU.ForceNewPage := PrintBand and QRParameters.FPageBU;
  end;
(*  if LastLevel = 'B' then
    PrintBand := False; *)
//  WKHasJobDummy := False;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowBU;
  if PrintBand then
  begin
    // Contract Group Report
    // Should always be 'total bu.' because contract group is always shown.
{    if ( LastLevel = 'B') then
      QRLabelTotBU.Caption := SRepHrsCUMWKBU
    else }
      QRLabelTotBU.Caption := SRepHrsTotalCUMBU;
    QRLabelBUHrs.Caption := DecodeHrsMin(TotalBU, True);
    QRLabelAvgBU.Caption := '00:00';
    QRLabelBUDay.Caption := IntToStr(FDaysBU);
    Average := 0;
    if FDaysBU <> 0 then
    begin
      Average := Round(TotalBU / FDaysBU);
      QRLabelAvgBU.Caption := DecodeHrsMin(Average, True);
    end;
    // Contract Group Report
    QRLabelAvgBU.Caption := '';
    if (not QRParameters.FShowDept) then
      QRLabelBUDay.Caption := // '100 %'
// ROP 05-MAR-2013 TD-21527        Format('%.2f', [100.0]) + ' %' // 2.0.161.213.1.
        Format('%.2n', [100.0]) + ' %' // 2.0.161.213.1.
    else
      QRLabelBUDay.Caption := ''
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDept;
  if PrintBand then
  begin
    TotalDept := 0;
    FDaysDept := 0;
(*  if LastLevel = 'D' then
    PrintBand := False; *)
    QRGroupHdDept.ForceNewPage := PrintBand and QRParameters.FPageDept;
//  WKHasJobDummy := False;
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDept;
  if PrintBand then
  begin
    // Contract Group Report - Should always be 'total dept.'
    // Because contract group is always shown.
{    if (LastLevel = 'D') then
      QRLabelTotDept.Caption := SRepHrsCUMWKDept
    else }
      QRLabelTotDept.Caption := SRepHrsTotalCUMDept;
    QRLabelDeptHrs.Caption := DecodeHrsMin(TotalDept, True);
    QRLabelAvgDept.Caption := '00:00';
    QRLabelDeptDay.Caption := IntToStr(FDaysDept);
    Average := 0;
    if FDaysDept <> 0 then
    begin
      Average := Round(TotalDept / FDaysDept);
      QRLabelAvgDept.Caption := DecodeHrsMin(Average, True);
    end;
    // Contract Group Report
    QRLabelAvgDept.Caption := '';
    QRLabelDeptDay.Caption := // '100 %';
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [100.0]) + ' %'; // 2.0.161.213.1.
      Format('%.2n', [100.0]) + ' %'; // 2.0.161.213.1.
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand := QRParameters.FShowJob;
  if PrintBand then
  begin
    TotalJob := 0;
    FDaysJob := 0;
    TotalJobUnknown := 0;
    FDaysJobUnknown := 0;
    if Lastlevel = 'J' then
      PrintBand := False;
    if PrintBand then
      if (ReportHrsPerContractGrpCUMDM.
        QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
        (ReportHrsPerContractGrpCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
      begin
        PrintBand := False;
        if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
          (not AllBusinessUnits)) then
          PrintBand := True;
      end;
    QRGroupHDJob.ForceNewPage := PrintBand and QRParameters.FPageJob;
  end;
  inherited;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowJob;
  if PrintBand then
  begin
    if (ReportHrsPerContractGrpCUMDM.
      QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
      (ReportHrsPerContractGrpCUMDM.
      QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
    begin
      PrintBand := False;
      if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
        (not AllBusinessUnits)) then
        PrintBand := True;
    end;
    if PrintBand then
    begin
      if LastLevel = 'J' then
        QRLabelTotJob.Caption := SRepJobCode
      else
        QRLabelTotJob.Caption := SRepTotJobCode;
      QRLabelJobHrs.Caption := DecodeHrsMin(TotalJob, True);
      QRLabelAvgJob.Caption := '00:00';
      QRLabelJobDay.Caption := IntToStr(FDaysJob);
      Average := 0;
      if FDaysJob <> 0 then
      begin
        Average := Round(TotalJob / FDaysJob);
        QRLabelAvgEmpl.Caption := DecodeHrsMin(Average, True);
      end;
    end;
    // Contract Group Report
    QRLabelAvgJob.Caption := '';
    QRLabelJobDay.Caption := '';
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // Never show this group!
  TotalEmpl := 0;
  FDaysEmpl := 0;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowEmpl;
  if PrintBand then
  begin
    if QRParameters.FShowJob then
    begin
      if (ReportHrsPerContractGrpCUMDM.
        QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
        (ReportHrsPerContractGrpCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
      begin
        PrintBand := False;
        if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
          (not AllBusinessUnits)) then
          PrintBand := True;
      end;
    end;
    if PrintBand then
    begin
      QRLabelEmplHrs.Caption := DecodeHrsMin(TotalEmpl, True);
      QRLabelAvgEmpl.Caption := '00:00';
      QRLabelEmplDay.Caption := IntToStr(FDaysEmpl);
      Average := 0;
      if FDaysEmpl <> 0 then
      begin
        Average := Round(TotalEmpl / FDaysEmpl);
        QRLabelAvgEmpl.Caption := DecodeHrsMin(Average, True);
      end;
    end;
  end;
  // Contract Group
  if PrintBand then
  begin
    QRLabelAvgEmpl.Caption := '';
    QRLabelEmplDay.Caption := '';
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.AddAmounts(Amount: Integer; BU, Dept, WK, Job,
  Empl: Boolean);
var
  Unknown: Boolean;
begin
  // MR: If ONE Plant and NOT ALL BusinessUnits are selected,
  //     determine 'unknown', and show this on workspot-level.
  //     Because this can be of another BusinessUnit.
  Unknown :=
    (
      (ReportHrsPerContractGrpCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) and
         ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
           (not AllBusinessUnits))
    );
  if BU then
  begin
    TotalBU := TotalBU + Amount;
    Inc(FDaysBU);
  end;
  if Dept then
  begin
    TotalDept:= TotalDept + Amount;
    Inc(FDaysDept);
  end;
  // Contract Group
  TotalContrGrp := TotalContrGrp + Amount;
  if WK then
  begin
    if not Unknown then
    begin
      TotalWK := TotalWK + Amount;
      Inc(FDaysWK);
    end
    else
    begin
      TotalWKUnknown := TotalWKUnknown + Amount;
      Inc(FDaysWKUnknown);
    end;
  end;
  if Job then
  begin
    if not Unknown then
    begin
      TotalJob := TotalJob + Amount;
      Inc(FDaysJob);
    end
    else
    begin
      TotalJobUnknown := TotalJobUnknown + Amount;
      TotalJob := TotalJob + Amount;
      Inc(FDaysJobUnknown);
    end;
  end;
  if Empl then
  begin
    if (not Unknown) then
    begin
      TotalEmpl := TotalEmpl + Amount;
      Inc(FDaysEmpl);
    end;
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  SumMin: Integer;
begin
  inherited;
  PrintBand := False;
  SumMin := DetermineSUMMIN;
  TotalPlant := TotalPlant + SumMin;
  Inc(FDaysPlant);
  case LastLevel of
    'P': AddAmounts(SumMin, False, False, False, False, False);
    'B': AddAmounts(SumMin, True, False, False, False, False);
    'D': AddAmounts(SumMin, True, True, False, False, False);
    'W': AddAmounts(SumMin, True, True, True, False, False);
    'J': AddAmounts(SumMin, True, True, True, True, False);
    'E': AddAmounts(SumMin, True, True, True, True, True);
   end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotBU.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('BDESC').AsString,
        DecodeHrsMin(TotalBU, True), // RV103.3.
        {IntToStr(Average),} '',
        QRLabelBUDay.Caption
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotDept.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('DDESC').AsString,
        DecodeHrsMin(TotalDept, True), // RV103.3.
        {IntToStr(Average)} '',
        QRLabelDeptDay.Caption {IntToStr(FDaysDept)}
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // UNKNOWN (WORKSPOT)
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelWKUnknownBU.Caption,
        '',
        '',
        DecodeHrsMin(TotalWKUnknown, True), // RV103.3.
        {IntToStr(AverageUnknown),} '',
        {IntToStr(FDaysWKUnknown)} ''
        );
end;

// MR:10-11-2005 Workspot Unknown
procedure TReportHrsPerContractGrpCUMQR.ChildBandFooterWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // WORKSPOT
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotWK.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('WDESC').AsString,
        DecodeHrsMin(TotalWK, True), // RV103.3.
        {IntToStr(Average),} '',
        {IntToStr(FDaysWK)} ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.ChildBandFooterWK2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelWKUnknownBU.Caption,
        '',
        '',
        DecodeHrsMin(TotalWKUnknown, True), // RV103.3.
        {IntToStr(AverageUnknown),} '',
        {IntToStr(FDaysWKUnknown)} ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterJOBAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotJob.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('JDESC').AsString,
        DecodeHrsMin(TotalJob, True), // RV103.3.
        {IntToStr(Average),} '',
        {IntToStr(FDaysJob)} ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel106.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsString,
        ReportHrsPerContractGrpCUMDM.cdsEmpl.FieldByName('DESCRIPTION').AsString,
        DecodeHrsMin(TotalEmpl, True), // RV103.3.
        {IntToStr(Average),} '',
        {IntToStr(FDaysEmpl)} ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandSummaryAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel55.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('BDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel54.Caption,
      ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
      ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('PDESC').AsString,
      '',
      '',
      ''
      );
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel35.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('DDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel3.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('WDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDJobAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel6.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('JDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotPlant.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('PDESC').AsString,
        DecodeHrsMin(TotalPlant, True), // RV103.3.
        {IntToStr(Average),} '',
        QRLabelPlantDay.Caption {IntToStr(FDaysPlant)}
        );
end;

procedure TReportHrsPerContractGrpCUMQR.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  with ReportHrsPerContractGrpCUMDM do
  begin
    if cdsEmpl.Locate('EMPLOYEE_NUMBER',
      VarArrayOf([QueryProdHour.
      FieldByName('EMPLOYEE_NUMBER').AsInteger]), []) then
        Value := Copy(cdsEmpl.FieldByName('DESCRIPTION').AsString, 0,25);
  end;
end;

procedure TReportHrsPerContractGrpCUMQR.QRGroupHDContractGrpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // Show the Contractgroup-header or not.
  PrintBand :=
    (QRParameters.FShowWK or QRParameters.FShowEmpl or QRParameters.FShowJob);
  TotalContrGrp := 0;
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterContractGrpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Sum: Integer;
  Perc: Double;
begin
  inherited;
  QRLabelCGHrs.Caption := DecodeHrsMin(TotalContrGrp, True);
  // Show 'Total Contractgroup' or only 'Contractgroup'
  if (QRParameters.FShowWK or QRParameters.FShowEmpl or QRParameters.FShowJob) then
    QRLabelTotCG.Caption := SPimsFooterTotalContractGroup
  else
    QRLabelTotCG.Caption := SPimsFooterContractGroup;
  // Determine sum for calculation of percentage on levels:
  // Plant
  // Department
  // BU
  with ReportHrsPerContractGrpCUMDM do
  begin
    Sum := 0;
    Perc := 0;
    if QRParameters.FShowBU and QRParameters.FShowDept then
    begin
      if qryProdHourSum.Locate('PLANT_CODE;BUSINESSUNIT_CODE;DEPARTMENT_CODE',
        VarArrayOf([QueryProdHour.FieldByName('PLANT_CODE').AsString,
          QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
          QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString]), []) then
        Sum := qryProdHourSum.FieldByName('SUMMIN').AsInteger;
    end
    else
      if QRParameters.FShowBU and (not QRParameters.FShowDept) then
      begin
        if qryProdHourSum.Locate('PLANT_CODE;BUSINESSUNIT_CODE',
          VarArrayOf([QueryProdHour.FieldByName('PLANT_CODE').AsString,
            QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString]), []) then
          Sum := qryProdHourSum.FieldByName('SUMMIN').AsInteger;
      end
      else
        if (not QRParameters.FShowBU) and QRParameters.FShowDept then
        begin
          if qryProdHourSum.Locate('PLANT_CODE;DEPARTMENT_CODE',
            VarArrayOf([QueryProdHour.FieldByName('PLANT_CODE').AsString,
              QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString]), []) then
            Sum := qryProdHourSum.FieldByName('SUMMIN').AsInteger;
        end
        else
        begin
          if qryProdHourSum.Locate('PLANT_CODE',
            QueryProdHour.FieldByName('PLANT_CODE').AsString, []) then
            Sum := qryProdHourSum.FieldByName('SUMMIN').AsInteger;
        end;
    if Sum <> 0 then
      Perc := TotalContrGrp / Sum * 100;
  end;
// ROP 05-MAR-2013 TD-21527  QRLabelCGPerc.Caption := Format('%.2f', [Perc]) + ' %';
  QRLabelCGPerc.Caption := Format('%.2n', [Perc]) + ' %';
end;

procedure TReportHrsPerContractGrpCUMQR.QRBandFooterContractGrpAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotCG.Caption,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('CONTRACTGROUP_CODE').AsString,
        ReportHrsPerContractGrpCUMDM.QueryProdHour.FieldByName('CGDESC').AsString,
        DecodeHrsMin(TotalContrGrp, True), // RV103.3.
        '',
        QRLabelCGPerc.Caption
        );
end;

end.
