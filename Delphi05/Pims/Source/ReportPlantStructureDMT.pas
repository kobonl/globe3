(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportPlantStructureDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, ReportBaseDMT, DBTables, Db, Provider, DBClient;

type
  TReportPlantStructureDM = class(TReportBaseDM)
    QueryPlantStructure: TQuery;
    ClientDataSetDataCol: TClientDataSet;
    DataSetProviderDataCol: TDataSetProvider;
    QueryDataCol: TQuery;
    QueryCompareJob: TQuery;
    DataSetProviderCompareJob: TDataSetProvider;
    ClientDataSetCompareJob: TClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryPlantStructureFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportPlantStructureDM: TReportPlantStructureDM;

implementation

{$R *.DFM}

procedure TReportPlantStructureDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  ClientDataSetDataCol.Open;
  ClientDataSetCompareJob.Open;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlantStructure);
end;

procedure TReportPlantStructureDM.QueryPlantStructureFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
