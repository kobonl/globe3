inherited ReportHrsPerWKDM: TReportHrsPerWKDM
  OldCreateOrder = True
  Left = 216
  Top = 181
  Height = 479
  Width = 741
  inherited qryWorkspotMinMax: TQuery
    Left = 416
    Top = 128
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 416
    Top = 184
  end
  object QueryProdHour: TQuery
    AutoCalcFields = False
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProdHourFilterRecord
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE, PL.DESCRIPTION AS PDESC'
      ', J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC'
      ', W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC'
      ', P.WORKSPOT_CODE, W.DESCRIPTION AS WDESC'
      ', P.JOB_CODE, J.DESCRIPTION AS JDESC'
      ', CAST(P.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER'
      ', CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER'
      ', P.PRODHOUREMPLOYEE_DATE,'
      '  SUM(PRODUCTION_MINUTE) AS SUMMIN,'
      '  MAX(J.BUSINESSUNIT_CODE) AS BUCODE,'
      '  MAX(1) AS LISTCODE'
      'FROM'
      '  PRODHOURPEREMPLOYEE P INNER JOIN PLANT PL ON'
      '    P.PLANT_CODE = PL.PLANT_CODE'
      '  INNER JOIN WORKSPOT W ON'
      '    P.PLANT_CODE = W.PLANT_CODE'
      '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '    AND W.USE_JOBCODE_YN = '#39'Y'#39
      '    AND (P.JOB_CODE <> '#39'0'#39')'
      '  INNER JOIN WEEK WK ON'
      '    WK.DATEFROM >= P.PRODHOUREMPLOYEE_DATE AND'
      '    WK.DATETO <= P.PRODHOUREMPLOYEE_DATE'
      '  INNER JOIN JOBCODE J ON'
      '    P.PLANT_CODE = J.PLANT_CODE'
      '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE'
      '    AND P.JOB_CODE = J.JOB_CODE'
      '  INNER JOIN BUSINESSUNIT B ON'
      '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE'
      '  INNER JOIN DEPARTMENT D ON'
      '    W.PLANT_CODE = D.PLANT_CODE'
      '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      'GROUP BY P.PLANT_CODE, PL.DESCRIPTION'
      '  , J.BUSINESSUNIT_CODE, B.DESCRIPTION'
      '  , PL.AVERAGE_WAGE'
      '  , W.DEPARTMENT_CODE'
      '  , D.DESCRIPTION'
      '  , P.WORKSPOT_CODE'
      '  , W.DESCRIPTION'
      '  , P.JOB_CODE'
      '  , J.DESCRIPTION'
      '  , P.EMPLOYEE_NUMBER'
      '  , WK.WEEKNUMBER'
      '  , P.PRODHOUREMPLOYEE_DATE'
      ''
      ''
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 48
    Top = 24
    object QueryProdHourPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryProdHourPDESC: TStringField
      FieldName = 'PDESC'
      Size = 30
    end
    object QueryProdHourWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryProdHourWDESC: TStringField
      FieldName = 'WDESC'
      Size = 30
    end
    object QueryProdHourPRODHOUREMPLOYEE_DATE: TDateTimeField
      FieldName = 'PRODHOUREMPLOYEE_DATE'
    end
    object QueryProdHourSUMMIN: TFloatField
      FieldName = 'SUMMIN'
    end
    object QueryProdHourBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object QueryProdHourBDESC: TStringField
      FieldName = 'BDESC'
      Size = 30
    end
    object QueryProdHourDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryProdHourDDESC: TStringField
      FieldName = 'DDESC'
      Size = 30
    end
    object QueryProdHourJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object QueryProdHourJDESC: TStringField
      FieldName = 'JDESC'
      Size = 30
    end
    object QueryProdHourEMPLOYEE_NUMBER: TFloatField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryProdHourBUCODE: TStringField
      FieldName = 'BUCODE'
      Size = 6
    end
    object QueryProdHourLISTCODE: TFloatField
      FieldName = 'LISTCODE'
    end
    object QueryProdHourWEEKNUMBER: TFloatField
      FieldName = 'WEEKNUMBER'
    end
  end
  object DataSourceProd: TDataSource
    DataSet = QueryProdHour
    Left = 136
    Top = 24
  end
  object QueryBUPerWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE, '
      '  SUM(PERCENTAGE) AS SUMPERC'
      'FROM '
      '  BUSINESSUNITPERWORKSPOT'
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE')
    Left = 48
    Top = 264
  end
  object cdsQueryBUWK: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspQueryBUWK'
    Left = 272
    Top = 264
  end
  object dspQueryBUWK: TDataSetProvider
    DataSet = QueryBUPerWK
    Constraints = True
    Left = 152
    Top = 264
  end
  object cdsEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEmpl'
    Left = 264
    Top = 128
  end
  object dspEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 152
    Top = 128
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, DESCRIPTION '
      'FROM '
      '  EMPLOYEE '
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 48
    Top = 128
  end
end
