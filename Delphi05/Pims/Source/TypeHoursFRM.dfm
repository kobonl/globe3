inherited TypeHoursF: TTypeHoursF
  Left = 257
  Top = 154
  Width = 677
  Caption = 'Type Of Hours'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 669
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 667
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 667
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Width = 669
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 329
      Height = 157
      Align = alLeft
      Caption = 'Type of hours'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 37
        Height = 13
        Caption = 'Number'
      end
      object Label3: TLabel
        Left = 8
        Top = 52
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object Label2: TLabel
        Left = 8
        Top = 84
        Width = 87
        Height = 13
        Caption = 'Bonus percentage'
      end
      object Label4: TLabel
        Left = 178
        Top = 88
        Width = 11
        Height = 13
        Caption = '%'
      end
      object Label5: TLabel
        Left = 8
        Top = 116
        Width = 58
        Height = 13
        Caption = 'Export code'
      end
      object DBEditTypeHours: TDBEdit
        Tag = 1
        Left = 115
        Top = 15
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'HOURTYPE_NUMBER'
        DataSource = TypeHoursDM.DataSourceMaster
        MaxLength = 6
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditDescription: TDBEdit
        Tag = 1
        Left = 115
        Top = 48
        Width = 200
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = TypeHoursDM.DataSourceMaster
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditExportCode: TDBEdit
        Left = 115
        Top = 114
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'EXPORT_CODE'
        DataSource = TypeHoursDM.DataSourceMaster
        ParentCtl3D = False
        TabOrder = 3
      end
      object dxDBEditBonusPerc: TdxDBEdit
        Left = 115
        Top = 80
        Width = 62
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'BONUS_PERCENTAGE'
        DataSource = TypeHoursDM.DataSourceMaster
      end
    end
    object GroupBox2: TGroupBox
      Left = 330
      Top = 1
      Width = 338
      Height = 157
      Align = alClient
      TabOrder = 1
      object DBCheckBoxOverTime: TDBCheckBox
        Left = 16
        Top = 15
        Width = 97
        Height = 17
        Caption = 'Overtime'
        Ctl3D = False
        DataField = 'OVERTIME_YN'
        DataSource = TypeHoursDM.DataSourceMaster
        ParentCtl3D = False
        TabOrder = 0
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxCountDays: TDBCheckBox
        Left = 16
        Top = 48
        Width = 97
        Height = 17
        Caption = 'Count days'
        Ctl3D = False
        DataField = 'COUNT_DAY_YN'
        DataSource = TypeHoursDM.DataSourceMaster
        ParentCtl3D = False
        TabOrder = 1
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 669
    inherited spltDetail: TSplitter
      Width = 667
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 667
      Bands = <
        item
          Caption = 'Type of Hours'
          Width = 649
        end>
      DataSource = TypeHoursDM.DataSourceMaster
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        Width = 84
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPE_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 191
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Bonus '
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_PERCENTAGE'
      end
      object dxDetailGridColumn5: TdxDBGridCheckColumn
        Caption = 'Overtime'
        MinWidth = 20
        Width = 87
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OVERTIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn6: TdxDBGridCheckColumn
        Caption = 'Count'
        Width = 88
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNT_DAY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn3: TdxDBGridColumn
        Caption = 'Export Code'
        Width = 86
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
  end
end
