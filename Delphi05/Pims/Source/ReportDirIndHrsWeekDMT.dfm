inherited ReportDirIndHrsWeekDM: TReportDirIndHrsWeekDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryDirIndHrs: TQuery
    AutoCalcFields = False
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryDirIndHrsFilterRecord
    SQL.Strings = (
      'SELECT'
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER),'
      '  A.PRODHOUREMPLOYEE_DATE,  '
      '  D.DIRECT_HOUR_YN,  '
      '  A.PLANT_CODE,  '
      '  A.WORKSPOT_CODE,  '
      '  SUM(A.PRODUCTION_MINUTE) AS SUMMIN,  '
      '  MAX(J.BUSINESSUNIT_CODE) AS BUCODE,  '
      '  MAX(1) AS LISTCODE  '
      'FROM  '
      '  PRODHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON  '
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER  '
      '    AND (A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE  '
      '    AND A.PRODHOUREMPLOYEE_DATE <= :FENDDATE)  '
      '    AND A.PLANT_CODE >= '#39'80'#39' '
      '    AND A.PLANT_CODE <= '#39'80'#39' '
      '  INNER JOIN WEEK WK ON'
      '    WK.DATEFROM >= A.PRODHOUREMPLOYEE_DATE AND'
      '    WK.DATETO <= A.PRODHOUREMPLOYEE_DATE'
      '  INNER JOIN DEPARTMENT D ON    '
      '    A.PLANT_CODE = D.PLANT_CODE  '
      '    AND E.DEPARTMENT_CODE = D.DEPARTMENT_CODE    '
      '  INNER JOIN WORKSPOT W ON  '
      '    A.PLANT_CODE = W.PLANT_CODE  '
      '    AND A.WORKSPOT_CODE = W.WORKSPOT_CODE  '
      '    AND W.USE_JOBCODE_YN = '#39'Y'#39'  '
      '    AND (A.JOB_CODE <> '#39'0'#39')  '
      '  AND A.EMPLOYEE_NUMBER >= 1 '
      '  AND A.EMPLOYEE_NUMBER <= 999999 '
      '  AND D.DEPARTMENT_CODE >= '#39'100'#39' '
      '  AND D.DEPARTMENT_CODE <= '#39'999'#39' '
      '  AND W.WORKSPOT_CODE >= '#39'1000'#39' '
      '  AND W.WORKSPOT_CODE <= '#39'9999'#39' '
      '  INNER JOIN JOBCODE J ON  '
      '    A.PLANT_CODE = J.PLANT_CODE  '
      '    AND A.WORKSPOT_CODE = J.WORKSPOT_CODE  '
      '    AND A.JOB_CODE = J.JOB_CODE    '
      'WHERE    '
      '     J.BUSINESSUNIT_CODE >= '#39'80'#39' '
      '    AND J.BUSINESSUNIT_CODE <= '#39'81'#39' '
      'GROUP BY  '
      '  WK.WEEKNUMBER,'
      '  A.PRODHOUREMPLOYEE_DATE,  '
      '  D.DIRECT_HOUR_YN,  '
      '  A.PLANT_CODE,  '
      '  A.WORKSPOT_CODE  '
      ' ')
    Left = 64
    Top = 40
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
    object QueryDirIndHrsPRODHOUREMPLOYEE_DATE: TDateTimeField
      FieldName = 'PRODHOUREMPLOYEE_DATE'
    end
    object QueryDirIndHrsDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object QueryDirIndHrsPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryDirIndHrsWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryDirIndHrsSUMMIN: TFloatField
      FieldName = 'SUMMIN'
    end
    object QueryDirIndHrsBUCODE: TStringField
      FieldName = 'BUCODE'
      Size = 6
    end
    object QueryDirIndHrsLISTCODE: TFloatField
      FieldName = 'LISTCODE'
    end
    object QueryDirIndHrsWEEKNUMBER: TFloatField
      FieldName = 'WEEKNUMBER'
    end
  end
  object DataSourceDirIndHrs: TDataSource
    DataSet = QueryDirIndHrs
    Left = 64
    Top = 104
  end
  object QueryBUPerWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE, '
      '  SUM(PERCENTAGE) AS SUMPERC'
      'FROM '
      '  BUSINESSUNITPERWORKSPOT'
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE')
    Left = 48
    Top = 264
  end
  object dspQueryBUWK: TDataSetProvider
    DataSet = QueryBUPerWK
    Constraints = True
    Left = 152
    Top = 264
  end
  object cdsQueryBUWK: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspQueryBUWK'
    Left = 272
    Top = 264
  end
end
