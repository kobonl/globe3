inherited BUPerWorkSpotF: TBUPerWorkSpotF
  Left = 345
  Top = 177
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Business unit per workspot'
  ClientHeight = 352
  ClientWidth = 553
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 553
    Height = 15
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = -80
      Width = 551
      Height = 94
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 551
      Height = 63
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Workspot'
        end>
      Visible = False
      ShowBands = True
      object dxMasterGridColumn12: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 166
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        DisableEditor = True
        Width = 50
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridLookupColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTLU'
      end
      object dxMasterGridColumn4: TdxDBGridLookupColumn
        Caption = 'HourType'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxMasterGridColumn8: TdxDBGridDateColumn
        Caption = 'Date Inative'
        DisableEditor = True
        MinWidth = 16
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
      end
      object dxMasterGridColumn11: TdxDBGridCheckColumn
        Caption = 'Use Job Codes'
        DisableEditor = True
        MinWidth = 20
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USE_JOBCODE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn5: TdxDBGridCheckColumn
        Caption = 'Automatic Data Colection'
        DisableEditor = True
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_DATACOL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn6: TdxDBGridCheckColumn
        Caption = 'Counter Value'
        DisableEditor = True
        Width = 76
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_VALUE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn7: TdxDBGridCheckColumn
        Caption = 'Enter Scan Counter '
        DisableEditor = True
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn9: TdxDBGridCheckColumn
        Caption = 'Measure Productivity'
        DisableEditor = True
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MEASURE_PRODUCTIVITY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn10: TdxDBGridCheckColumn
        Caption = 'Productive Hour'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRODUCTIVE_HOUR_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 224
    Width = 553
    Height = 128
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 551
      Height = 126
      Align = alClient
      Caption = 'Business unit per workspot'
      TabOrder = 0
      object Label2: TLabel
        Left = 8
        Top = 80
        Width = 62
        Height = 13
        Caption = 'Business unit'
      end
      object Label3: TLabel
        Left = 8
        Top = 107
        Width = 55
        Height = 13
        Caption = 'Percentage'
      end
      object Label8: TLabel
        Left = 8
        Top = 27
        Width = 50
        Height = 13
        Caption = 'Plant code'
      end
      object Label9: TLabel
        Left = 8
        Top = 54
        Width = 75
        Height = 13
        Caption = 'Workspot  code'
      end
      object DBLookupComboBoxBusinessUnit: TDBLookupComboBox
        Tag = 1
        Left = 94
        Top = 77
        Width = 275
        Height = 19
        Ctl3D = False
        DataField = 'BUSINESSUNIT_CODE'
        DataSource = WorkSpotDM.DataSourceBUWorkSpot
        DropDownWidth = 243
        KeyField = 'BUSINESSUNIT_CODE'
        ListField = 'DESCRIPTION;BUSINESSUNIT_CODE'
        ListSource = WorkSpotDM.DataSourceBULU
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditPercentage: TDBEdit
        Left = 94
        Top = 104
        Width = 81
        Height = 19
        Ctl3D = False
        DataField = 'PERCENTAGE'
        DataSource = WorkSpotDM.DataSourceBUWorkSpot
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditPlant: TDBEdit
        Tag = 1
        Left = 94
        Top = 23
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
      end
      object DBEditWK: TDBEdit
        Tag = 1
        Left = 94
        Top = 50
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'WORKSPOT_CODE'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 3
      end
      object DBEditPlantDesc: TDBEdit
        Tag = 1
        Left = 158
        Top = 23
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = WorkSpotDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 4
      end
      object DBEditWKDesc: TDBEdit
        Tag = 1
        Left = 158
        Top = 49
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = WorkSpotDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 5
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 41
    Width = 553
    Height = 183
    inherited spltDetail: TSplitter
      Top = 179
      Width = 551
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 551
      Height = 178
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Business unit per workspot'
          Width = 711
        end>
      KeyField = 'BUSINESSUNIT_CODE'
      DataSource = WorkSpotDM.DataSourceBUWorkSpot
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridLookupColumn
        Caption = 'Code'
        Width = 246
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BUSINESSUNITLU'
        ListFieldName = 'DESCRIPTION;BUSINESSUNIT_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Percentage'
        Width = 284
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PERCENTAGE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 536
    Top = 104
  end
  inherited dsrcActive: TDataSource
    Left = 8
    Top = 136
  end
end
