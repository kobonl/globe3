inherited StandStaffAvailDM: TStandStaffAvailDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 679
  Top = 195
  Height = 542
  Width = 731
  inherited TableMaster: TTable
    Top = 16
  end
  inherited TableDetail: TTable
    TableName = 'STANDARDAVAILABILITY'
    Top = 68
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDAY_OF_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
    end
    object TableDetailSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableDetailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object TableDetailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Size = 4
    end
    object TableDetailAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Size = 4
    end
    object TableDetailAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Size = 4
    end
    object TableDetailAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Size = 4
    end
    object TableDetailAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Size = 4
    end
    object TableDetailAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Size = 4
    end
  end
  inherited DataSourceMaster: TDataSource
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    DataSet = QueryDetail
    Left = 200
    Top = 68
  end
  inherited TableExport: TTable
    Left = 452
  end
  inherited DataSourceExport: TDataSource
    Left = 544
  end
  object QueryDetail: TQuery
    AutoCalcFields = False
    ObjectView = True
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE PLANT, S.SHIFT_NUMBER SHIFT,'
      '  E.DEPARTMENT_CODE DEPARTMENT, E.TEAM_CODE TEAM,'
      '  CAST(MAX(ROWNUM) AS NUMBER) AS RECNO,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D11,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D12,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D13,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D14,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D15,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D16,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D17,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D18,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D19,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 1 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D110,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D21,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D22,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D23,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D24,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D25,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D26,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D27,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D28,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D29,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 2 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D210,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D31,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D32,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D33,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D34,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D35,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D36,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D37,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D38,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D39,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 3 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D310,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D41,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D42,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D43,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D44,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D45,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D46,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D47,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D48,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D49,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 4 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D410,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D51,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D52,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D53,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D54,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D55,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D56,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D57,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D58,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D59,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 5 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D510,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D61,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D62,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D63,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D64,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D65,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D66,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D67,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D68,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D69,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 6 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D610,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_1 ELSE NULL END) D71,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_2 ELSE NULL END) D72,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_3 ELSE NULL END) D73,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_4 ELSE NULL END) D74,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_5 ELSE NULL END) D75,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_6 ELSE NULL END) D76,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_7 ELSE NULL END) D77,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_8 ELSE NULL END) D78,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_9 ELSE NULL END) D79,'
      
        '  MAX(CASE WHEN SA.DAY_OF_WEEK    = 7 THEN SA.AVAILABLE_TIMEBLOC' +
        'K_10 ELSE NULL END) D710,'
      '  MAX(EC.CONTRACT_HOUR_PER_WEEK)  CONTRACTHOURS'
      'FROM'
      '  EMPLOYEE E INNER JOIN SHIFT S ON'
      '    E.PLANT_CODE = S.PLANT_CODE'
      '  LEFT OUTER JOIN EMPLOYEECONTRACT EC ON'
      '    EC.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    AND (EC.ENDDATE >= :SDATE)'
      '  LEFT OUTER JOIN STANDARDAVAILABILITY SA ON'
      '    (SA.PLANT_CODE = E.PLANT_CODE)'
      '    AND (SA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '    AND (SA.SHIFT_NUMBER = S.SHIFT_NUMBER)'
      'WHERE'
      '  (:PLANT_CODE = '#39'*'#39' OR E.PLANT_CODE = :PLANT_CODE) AND'
      '  ((:TEAMFROM = '#39'*'#39') OR'
      '   (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO )) AND'
      '  (:SHIFT_NUMBER = -1 OR S.SHIFT_NUMBER = :SHIFT_NUMBER) AND'
      '  ('
      '  :ALL_EMP = 1 OR ((E.ENDDATE >= :SDATE OR E.ENDDATE IS NULL))'
      '  ) AND'
      '  ('
      '  :USER_NAME = '#39'*'#39' OR'
      '    (E.TEAM_CODE IN'
      '       ('
      '       SELECT TPU.TEAM_CODE'
      '       FROM TEAMPERUSER TPU'
      '       WHERE TPU.USER_NAME = :USER_NAME)'
      '       )'
      '  )'
      'GROUP BY'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE, S.SHIFT_NUMBER, '
      '  E.DEPARTMENT_CODE, E.TEAM_CODE '
      'ORDER BY '
      '  E.EMPLOYEE_NUMBER, E.PLANT_CODE, S.SHIFT_NUMBER'
      ''
      ' ')
    UpdateMode = upWhereKeyOnly
    Left = 320
    Top = 68
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ALL_EMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryDetailSHIFT: TIntegerField
      FieldName = 'SHIFT'
    end
    object QueryDetailPLANT: TStringField
      FieldName = 'PLANT'
      Size = 6
    end
    object QueryDetailD11: TStringField
      DisplayWidth = 1
      FieldName = 'D11'
      Size = 1
    end
    object QueryDetailD12: TStringField
      FieldName = 'D12'
      Size = 1
    end
    object QueryDetailD13: TStringField
      FieldName = 'D13'
      Size = 1
    end
    object QueryDetailD21: TStringField
      DisplayWidth = 1
      FieldName = 'D21'
      Size = 1
    end
    object QueryDetailD14: TStringField
      FieldName = 'D14'
      Size = 1
    end
    object QueryDetailD22: TStringField
      FieldName = 'D22'
      Size = 1
    end
    object QueryDetailD23: TStringField
      FieldName = 'D23'
      Size = 1
    end
    object QueryDetailD24: TStringField
      FieldName = 'D24'
      Size = 1
    end
    object QueryDetailD31: TStringField
      DisplayWidth = 1
      FieldName = 'D31'
      Size = 1
    end
    object QueryDetailD32: TStringField
      FieldName = 'D32'
      Size = 1
    end
    object QueryDetailD33: TStringField
      FieldName = 'D33'
      Size = 1
    end
    object QueryDetailD34: TStringField
      FieldName = 'D34'
      Size = 1
    end
    object QueryDetailD4: TStringField
      DisplayWidth = 1
      FieldName = 'D41'
      Size = 1
    end
    object QueryDetailD42: TStringField
      FieldName = 'D42'
      Size = 1
    end
    object QueryDetailD43: TStringField
      FieldName = 'D43'
      Size = 1
    end
    object QueryDetailD44: TStringField
      FieldName = 'D44'
      Size = 1
    end
    object QueryDetailD51: TStringField
      DisplayWidth = 1
      FieldName = 'D51'
      Size = 4
    end
    object QueryDetailD52: TStringField
      FieldName = 'D52'
      Size = 1
    end
    object QueryDetailD53: TStringField
      FieldName = 'D53'
      Size = 1
    end
    object QueryDetailD54: TStringField
      FieldName = 'D54'
      Size = 1
    end
    object QueryDetailD61: TStringField
      DisplayWidth = 1
      FieldName = 'D61'
      Size = 1
    end
    object QueryDetailD62: TStringField
      FieldName = 'D62'
      Size = 1
    end
    object QueryDetailD63: TStringField
      FieldName = 'D63'
      Size = 1
    end
    object QueryDetailD64: TStringField
      FieldName = 'D64'
      Size = 1
    end
    object QueryDetailD71: TStringField
      DisplayWidth = 1
      FieldName = 'D71'
      Size = 1
    end
    object QueryDetailD72: TStringField
      FieldName = 'D72'
      Size = 1
    end
    object QueryDetailD73: TStringField
      FieldName = 'D73'
      Size = 1
    end
    object QueryDetailD74: TStringField
      FieldName = 'D74'
      Size = 1
    end
    object QueryDetailTotalHrs: TStringField
      DisplayWidth = 8
      FieldKind = fkCalculated
      FieldName = 'TOTALHRS'
      Size = 8
      Calculated = True
    end
    object QueryDetailTEAM: TStringField
      FieldName = 'TEAM'
      Size = 6
    end
    object QueryDetailDEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Size = 6
    end
    object QueryDetailRECNO: TFloatField
      Alignment = taLeftJustify
      FieldName = 'RECNO'
    end
    object QueryDetailD11CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D11CALC'
      Calculated = True
    end
    object QueryDetailD12CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D12CALC'
      Calculated = True
    end
    object QueryDetailD13CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D13CALC'
      Calculated = True
    end
    object QueryDetailD14CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D14CALC'
      Calculated = True
    end
    object QueryDetailD15CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D15CALC'
      Calculated = True
    end
    object QueryDetailD16CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D16CALC'
      Calculated = True
    end
    object QueryDetailD17CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D17CALC'
      Calculated = True
    end
    object QueryDetailD18CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D18CALC'
      Calculated = True
    end
    object QueryDetailD19CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D19CALC'
      Calculated = True
    end
    object QueryDetailD110CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D110CALC'
      Calculated = True
    end
    object QueryDetailD21CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D21CALC'
      Calculated = True
    end
    object QueryDetailD22CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D22CALC'
      Calculated = True
    end
    object QueryDetailD23CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D23CALC'
      Calculated = True
    end
    object QueryDetailD24CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D24CALC'
      Calculated = True
    end
    object QueryDetailD25CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D25CALC'
      Calculated = True
    end
    object QueryDetailD26CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D26CALC'
      Calculated = True
    end
    object QueryDetailD27CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D27CALC'
      Calculated = True
    end
    object QueryDetailD28CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D28CALC'
      Calculated = True
    end
    object QueryDetailD29CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D29CALC'
      Calculated = True
    end
    object QueryDetailD210CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D210CALC'
      Calculated = True
    end
    object QueryDetailD31CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D31CALC'
      Calculated = True
    end
    object QueryDetailD32CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D32CALC'
      Calculated = True
    end
    object QueryDetailD33CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D33CALC'
      Calculated = True
    end
    object QueryDetailD34CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D34CALC'
      Calculated = True
    end
    object QueryDetailD35CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D35CALC'
      Calculated = True
    end
    object QueryDetailD36CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D36CALC'
      Calculated = True
    end
    object QueryDetailD37CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D37CALC'
      Calculated = True
    end
    object QueryDetailD38CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D38CALC'
      Calculated = True
    end
    object QueryDetailD39CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D39CALC'
      Calculated = True
    end
    object QueryDetailD310CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D310CALC'
      Calculated = True
    end
    object QueryDetailD41CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D41CALC'
      Calculated = True
    end
    object QueryDetailD42CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D42CALC'
      Calculated = True
    end
    object QueryDetailD43CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D43CALC'
      Calculated = True
    end
    object QueryDetailD44CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D44CALC'
      Calculated = True
    end
    object QueryDetailD45CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D45CALC'
      Calculated = True
    end
    object QueryDetailD46CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D46CALC'
      Calculated = True
    end
    object QueryDetailD47CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D47CALC'
      Calculated = True
    end
    object QueryDetailD48CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D48CALC'
      Calculated = True
    end
    object QueryDetailD49CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D49CALC'
      Calculated = True
    end
    object QueryDetailD410CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D410CALC'
      Calculated = True
    end
    object QueryDetailD51CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D51CALC'
      Calculated = True
    end
    object QueryDetailD52CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D52CALC'
      Calculated = True
    end
    object QueryDetailD53CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D53CALC'
      Calculated = True
    end
    object QueryDetailD54CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D54CALC'
      Calculated = True
    end
    object QueryDetailD55CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D55CALC'
      Calculated = True
    end
    object QueryDetailD56CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D56CALC'
      Calculated = True
    end
    object QueryDetailD57CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D57CALC'
      Calculated = True
    end
    object QueryDetailD58CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D58CALC'
      Calculated = True
    end
    object QueryDetailD59CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D59CALC'
      Calculated = True
    end
    object QueryDetailD510CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D510CALC'
      Calculated = True
    end
    object QueryDetailD61CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D61CALC'
      Calculated = True
    end
    object QueryDetailD62CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D62CALC'
      Calculated = True
    end
    object QueryDetailD63CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D63CALC'
      Calculated = True
    end
    object QueryDetailD64CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D64CALC'
      Calculated = True
    end
    object QueryDetailD65CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D65CALC'
      Calculated = True
    end
    object QueryDetailD66CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D66CALC'
      Calculated = True
    end
    object QueryDetailD67CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D67CALC'
      Calculated = True
    end
    object QueryDetailD68CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D68CALC'
      Calculated = True
    end
    object QueryDetailD69CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D69CALC'
      Calculated = True
    end
    object QueryDetailD610CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D610CALC'
      Calculated = True
    end
    object QueryDetailD71CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D71CALC'
      Calculated = True
    end
    object QueryDetailD72CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D72CALC'
      Calculated = True
    end
    object QueryDetailD73CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D73CALC'
      Calculated = True
    end
    object QueryDetailD74CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D74CALC'
      Calculated = True
    end
    object QueryDetailD75CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D75CALC'
      Calculated = True
    end
    object QueryDetailD76CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D76CALC'
      Calculated = True
    end
    object QueryDetailD77CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D77CALC'
      Calculated = True
    end
    object QueryDetailD78CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D78CALC'
      Calculated = True
    end
    object QueryDetailD79CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D79CALC'
      Calculated = True
    end
    object QueryDetailD710CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D710CALC'
      Calculated = True
    end
    object QueryDetailEMP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object QueryDetailCONTRACTHOURS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'CONTRACTHOURS'
    end
    object QueryDetailD15: TStringField
      FieldName = 'D15'
      Size = 4
    end
    object QueryDetailD16: TStringField
      FieldName = 'D16'
      Size = 4
    end
    object QueryDetailD17: TStringField
      FieldName = 'D17'
      Size = 4
    end
    object QueryDetailD18: TStringField
      FieldName = 'D18'
      Size = 4
    end
    object QueryDetailD19: TStringField
      FieldName = 'D19'
      Size = 4
    end
    object QueryDetailD110: TStringField
      FieldName = 'D110'
      Size = 4
    end
    object QueryDetailD25: TStringField
      FieldName = 'D25'
      Size = 4
    end
    object QueryDetailD26: TStringField
      FieldName = 'D26'
      Size = 4
    end
    object QueryDetailD27: TStringField
      FieldName = 'D27'
      Size = 4
    end
    object QueryDetailD28: TStringField
      FieldName = 'D28'
      Size = 4
    end
    object QueryDetailD29: TStringField
      FieldName = 'D29'
      Size = 4
    end
    object QueryDetailD210: TStringField
      FieldName = 'D210'
      Size = 4
    end
    object QueryDetailD35: TStringField
      FieldName = 'D35'
      Size = 4
    end
    object QueryDetailD36: TStringField
      FieldName = 'D36'
      Size = 4
    end
    object QueryDetailD37: TStringField
      FieldName = 'D37'
      Size = 4
    end
    object QueryDetailD38: TStringField
      FieldName = 'D38'
      Size = 4
    end
    object QueryDetailD39: TStringField
      FieldName = 'D39'
      Size = 4
    end
    object QueryDetailD310: TStringField
      FieldName = 'D310'
      Size = 4
    end
    object QueryDetailD45: TStringField
      FieldName = 'D45'
      Size = 4
    end
    object QueryDetailD46: TStringField
      FieldName = 'D46'
      Size = 4
    end
    object QueryDetailD47: TStringField
      FieldName = 'D47'
      Size = 4
    end
    object QueryDetailD48: TStringField
      FieldName = 'D48'
      Size = 4
    end
    object QueryDetailD49: TStringField
      FieldName = 'D49'
      Size = 4
    end
    object QueryDetailD410: TStringField
      FieldName = 'D410'
      Size = 4
    end
    object QueryDetailD55: TStringField
      FieldName = 'D55'
      Size = 4
    end
    object QueryDetailD56: TStringField
      FieldName = 'D56'
      Size = 4
    end
    object QueryDetailD57: TStringField
      FieldName = 'D57'
      Size = 4
    end
    object QueryDetailD58: TStringField
      FieldName = 'D58'
      Size = 4
    end
    object QueryDetailD59: TStringField
      FieldName = 'D59'
      Size = 4
    end
    object QueryDetailD510: TStringField
      FieldName = 'D510'
      Size = 4
    end
    object QueryDetailD65: TStringField
      FieldName = 'D65'
      Size = 4
    end
    object QueryDetailD66: TStringField
      FieldName = 'D66'
      Size = 4
    end
    object QueryDetailD67: TStringField
      FieldName = 'D67'
      Size = 4
    end
    object QueryDetailD68: TStringField
      FieldName = 'D68'
      Size = 4
    end
    object QueryDetailD69: TStringField
      FieldName = 'D69'
      Size = 4
    end
    object QueryDetailD610: TStringField
      FieldName = 'D610'
      Size = 4
    end
    object QueryDetailD75: TStringField
      FieldName = 'D75'
      Size = 4
    end
    object QueryDetailD76: TStringField
      FieldName = 'D76'
      Size = 4
    end
    object QueryDetailD77: TStringField
      FieldName = 'D77'
      Size = 4
    end
    object QueryDetailD78: TStringField
      FieldName = 'D78'
      Size = 4
    end
    object QueryDetailD79: TStringField
      FieldName = 'D79'
      Size = 4
    end
    object QueryDetailD710: TStringField
      FieldName = 'D710'
      Size = 4
    end
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '   PLANT_CODE,'
      '   EMPLOYEE_NUMBER,'
      '   EMPLOYEE_NUMBER || '#39' | '#39' || DESCRIPTION AS VALUEDISPLAY,'
      '   DESCRIPTION,'
      '   SHORT_NAME,'
      '   DEPARTMENT_CODE,'
      '   TEAM_CODE,'
      '  ADDRESS'
      'FROM  '
      '  EMPLOYEE     '
      'ORDER BY '
      '  EMPLOYEE_NUMBER'
      '')
    Left = 88
    Top = 272
  end
  object QueryExec: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 544
    Top = 72
  end
  object QuerySelect: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 320
    Top = 272
  end
  object ClientDataSetTotalHours: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetTotalHoursIndex'
        Fields = 'PLANT_CODE;EMPLOYEE_NUMBER;DEPARTMENT_CODE;SHIFT_NUMBER'
        Options = [ixUnique]
      end>
    IndexName = 'ClientDataSetTotalHoursIndex'
    Params = <>
    StoreDefs = True
    Left = 200
    Top = 384
    object ClientDataSetTotalHoursEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object ClientDataSetTotalHoursPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object ClientDataSetTotalHoursDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object ClientDataSetTotalHoursSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object ClientDataSetTotalHoursTOTALHRS: TIntegerField
      FieldName = 'TOTALHRS'
    end
    object ClientDataSetTotalHoursTB1: TIntegerField
      FieldName = 'TB1'
    end
    object ClientDataSetTotalHoursTB2: TIntegerField
      FieldName = 'TB2'
    end
    object ClientDataSetTotalHoursTB3: TIntegerField
      FieldName = 'TB3'
    end
    object ClientDataSetTotalHoursTB4: TIntegerField
      FieldName = 'TB4'
    end
    object ClientDataSetTotalHoursTB5: TIntegerField
      FieldName = 'TB5'
    end
    object ClientDataSetTotalHoursTB6: TIntegerField
      FieldName = 'TB6'
    end
    object ClientDataSetTotalHoursTB7: TIntegerField
      FieldName = 'TB7'
    end
    object ClientDataSetTotalHoursTB8: TIntegerField
      FieldName = 'TB8'
    end
    object ClientDataSetTotalHoursTB9: TIntegerField
      FieldName = 'TB9'
    end
    object ClientDataSetTotalHoursTB10: TIntegerField
      FieldName = 'TB10'
    end
  end
  object ClientDataSetSave: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetRecNoIndex'
        Fields = 'RECNO'
        Options = [ixUnique]
      end>
    IndexName = 'ClientDataSetRecNoIndex'
    Params = <>
    StoreDefs = True
    Left = 88
    Top = 384
    object ClientDataSetSaveRECNO: TFloatField
      FieldName = 'RECNO'
    end
    object ClientDataSetSaveDAY1: TStringField
      FieldName = 'DAY1'
      Size = 10
    end
    object ClientDataSetSaveDAY2: TStringField
      FieldName = 'DAY2'
      Size = 10
    end
    object ClientDataSetSaveDAY3: TStringField
      FieldName = 'DAY3'
      Size = 10
    end
    object ClientDataSetSaveDAY4: TStringField
      FieldName = 'DAY4'
      Size = 10
    end
    object ClientDataSetSaveDAY5: TStringField
      FieldName = 'DAY5'
      Size = 10
    end
    object ClientDataSetSaveDAY6: TStringField
      FieldName = 'DAY6'
      Size = 10
    end
    object ClientDataSetSaveDAY7: TStringField
      FieldName = 'DAY7'
      Size = 10
    end
  end
  object QueryUpdateSTA: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE STANDARDAVAILABILITY'
      '  SET AVAILABLE_TIMEBLOCK_1 = :AV1,'
      '  AVAILABLE_TIMEBLOCK_2 = :AV2,'
      '  AVAILABLE_TIMEBLOCK_3 = :AV3,'
      '  AVAILABLE_TIMEBLOCK_4 = :AV4,'
      '  AVAILABLE_TIMEBLOCK_5 = :AV5,'
      '  AVAILABLE_TIMEBLOCK_6 = :AV6,'
      '  AVAILABLE_TIMEBLOCK_7 = :AV7,'
      '  AVAILABLE_TIMEBLOCK_8 = :AV8,'
      '  AVAILABLE_TIMEBLOCK_9 = :AV9,'
      '  AVAILABLE_TIMEBLOCK_10 = :AV10,'
      '  MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE'
      '  WHERE PLANT_CODE = :PLANT_CODE AND'
      '  DAY_OF_WEEK = :DAY_OF_WEEK AND '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
      '  AND SHIFT_NUMBER = :SHIFT_NUMBER'
      ' ')
    Left = 88
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'AV1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryInsertSTA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO STANDARDAVAILABILITY ('
      '  PLANT_CODE, SHIFT_NUMBER, DAY_OF_WEEK, EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10,'
      '  MUTATOR,'
      '  MUTATIONDATE, CREATIONDATE) VALUES'
      '  (:PLANT_CODE, :SHIFT_NUMBER,'
      '  :DAY_OF_WEEK, :EMPLOYEE_NUMBER, :AV1, :AV2, :AV3, :AV4,'
      '  :AV5, :AV6, :AV7, :AV8, :AV9, :AV10, '
      '  :MUTATOR, :MUTATIONDATE, :CREATIONDATE)')
    Left = 200
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV4'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV5'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV6'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV7'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV8'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV9'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AV10'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end>
  end
  object QuerySTAOldValues: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DAY_OF_WEEK,'
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY'
      'WHERE '
      '  PLANT_CODE =  :PLANT  AND SHIFT_NUMBER = :SHIFT '
      '  AND EMPLOYEE_NUMBER =:EMPLOYEE '
      'ORDER BY '
      '  DAY_OF_WEEK'
      ' ')
    Left = 320
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end>
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantFilterRecord
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DESCRIPTION, COUNTRY_ID '
      'FROM '
      '  PLANT '
      'ORDER BY '
      '  PLANT_CODE')
    Left = 204
    Top = 128
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  TEAM_CODE, DESCRIPTION'
      'FROM '
      '  TEAM '
      'ORDER BY '
      '  TEAM_CODE')
    Left = 316
    Top = 128
  end
  object QueryPlantTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DISTINCT DPT.PLANT_CODE,'
      '  P.DESCRIPTION'
      'FROM'
      '  DEPARTMENTPERTEAM DPT,'
      '  PLANT P'
      'WHERE'
      '  DPT.TEAM_CODE = :TEAM_CODE AND'
      '  DPT.PLANT_CODE = P.PLANT_CODE')
    Left = 440
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryShiftPlant: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DISTINCT PLANT_CODE, '
      '  SHIFT_NUMBER,'
      '  DESCRIPTION'
      'FROM'
      '  SHIFT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      '  ')
    Left = 96
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceEmpl: TDataSource
    DataSet = QueryEmpl
    Left = 200
    Top = 272
  end
  object QueryAbs: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ABSENCEREASON_CODE, DESCRIPTION '
      'FROM '
      '  ABSENCEREASON '
      'ORDER BY '
      '  DESCRIPTION')
    Left = 376
    Top = 16
  end
end
