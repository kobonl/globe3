inherited TeamDeptDM: TTeamDeptDM
  OldCreateOrder = True
  Left = 216
  Top = 132
  Height = 552
  Width = 750
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    OnFilterRecord = TableMasterFilterRecord
    IndexFieldNames = 'PLANT_CODE;TEAM_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceMasterPlant
    TableName = 'TEAM'
    Left = 96
    Top = 64
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object TableMasterTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterRESP_EMPLOYEE_NUMBER: TIntegerField
      FieldName = 'RESP_EMPLOYEE_NUMBER'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object TableMasterEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupDataSet = TableEmployee
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'RESP_EMPLOYEE_NUMBER'
      LookupCache = True
      Lookup = True
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'TEAM_CODE;PLANT_CODE'
    MasterFields = 'TEAM_CODE;PLANT_CODE'
    TableName = 'DEPARTMENTPERTEAM'
    Left = 96
    Top = 119
    object TableDetailTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailDEPARTMENTCAL: TStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'DEPARTMENTCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailDEPARTMENTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPARTMENTLU'
      LookupDataSet = TableDepartmentTeam
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableDetailPLANTCAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'PLANTCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = TablePlant
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Top = 64
  end
  inherited DataSourceDetail: TDataSource
    Top = 118
  end
  inherited TableExport: TTable
    Left = 340
    Top = 316
  end
  inherited DataSourceExport: TDataSource
    Left = 432
    Top = 316
  end
  object TablePlant: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 64
    Top = 261
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 200
    Top = 258
  end
  object TableDepartmentDsc: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'DEPARTMENT'
    Left = 88
    Top = 376
    object TableDepartmentDscDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentDscPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentDscDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDepartmentDscBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDepartmentDscCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDepartmentDscDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDepartmentDscMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDepartmentDscMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDepartmentDscPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDepartmentDscREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
  object TableDeptPerTeam: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'TEAM_CODE'
    MasterFields = 'TEAM_CODE'
    MasterSource = DataSourceMaster
    TableName = 'DEPARTMENTPERTEAM'
    Left = 344
    Top = 128
  end
  object TableEmployee: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourcePlant
    TableName = 'EMPLOYEE'
    Left = 96
    Top = 190
    object TableEmployeeEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableEmployeeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableEmployeeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableEmployeeADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableEmployeeZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableEmployeeCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableEmployeeSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableEmployeeLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableEmployeePHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableEmployeeEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableEmployeeDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableEmployeeSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableEmployeeSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableEmployeeENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableEmployeeCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableEmployeeCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableEmployeeREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableEmployeeCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
  end
  object DataSourceEmployee: TDataSource
    DataSet = TableEmployee
    Left = 200
    Top = 188
  end
  object TableDepartmentTeam: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = TableDepartmentTeamFilterRecord
    IndexFieldNames = 'PLANT_CODE;DEPARTMENT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'DEPARTMENT'
    Left = 96
    Top = 316
    object TableDepartmentTeamDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentTeamPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDepartmentTeamDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDepartmentTeamBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDepartmentTeamCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDepartmentTeamDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDepartmentTeamMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDepartmentTeamMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDepartmentTeamPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDepartmentTeamREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
  object DataSourceDepartmentTeam: TDataSource
    DataSet = TableDepartmentTeam
    Left = 224
    Top = 320
  end
  object TableP: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 120
    Top = 261
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object StringField6: TStringField
      FieldName = 'STATE'
    end
    object StringField7: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object IntegerField1: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object IntegerField2: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField9: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object IntegerField4: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableMasterPlant: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = TableMasterPlantFilterRecord
    IndexFieldNames = 'PLANT_CODE'
    TableName = 'PLANT'
    Left = 96
    Top = 8
    object TableMasterPlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterPlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterPlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterPlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
  end
  object DataSourceMasterPlant: TDataSource
    DataSet = TableMasterPlant
    Left = 200
    Top = 8
  end
end
