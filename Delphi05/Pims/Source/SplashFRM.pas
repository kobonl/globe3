unit SplashFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LogoPimsFRM, ExtCtrls, dxfProgressBar, StdCtrls;

type
  TSplashF = class(TPimsLogoF)
    lblInfo: TLabel;
    dxfProgressBar: TdxfProgressBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure UpdateProgressBar(Sender: TObject);
  end;

var
  SplashF: TSplashF;
  Starting  : Boolean;

implementation

uses
  SystemDMT, UPimsConst;

{$R *.DFM}

procedure TSplashF.UpdateProgressBar(Sender: TObject);
begin
  dxfProgressBar.StepIt;
  Update;
end;

procedure TSplashF.FormCreate(Sender: TObject);
begin
  inherited;
  // PIM-250
  lblInfo.Font.Color := clDarkRed;
end;

initialization
  Starting := False;

end.
