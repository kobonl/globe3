inherited DialogStaffPlanningF: TDialogStaffPlanningF
  Left = 370
  Top = 137
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Dialog staff planning'
  ClientHeight = 469
  ClientWidth = 631
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 631
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 514
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 631
    Height = 348
    Caption = ''
    Font.Color = clBlack
    TabOrder = 4
    object RadioGroupSelection: TRadioGroup
      Left = 8
      Top = 208
      Width = 233
      Height = 105
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Day planning'
        'Standard planning'
        'Read standard planning into day planning '
        'Read past into day planning')
      ParentFont = False
      TabOrder = 1
      OnClick = RadioGroupSelectionClick
    end
    object RadioGroupEmpl: TRadioGroup
      Left = 256
      Top = 208
      Width = 153
      Height = 105
      Caption = 'Employee sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Number'
        'Name')
      ParentFont = False
      TabOrder = 2
    end
    object RadioGroupWKSort: TRadioGroup
      Left = 424
      Top = 208
      Width = 169
      Height = 105
      Caption = 'WorkSpot/Department sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Code'
        'Description')
      ParentFont = False
      TabOrder = 3
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 8
      Width = 585
      Height = 137
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label5: TLabel
        Left = 5
        Top = 52
        Width = 24
        Height = 13
        Caption = 'Plant'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 5
        Top = 80
        Width = 22
        Height = 13
        Caption = 'Shift'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 5
        Top = 21
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 33
        Top = 21
        Width = 26
        Height = 13
        Caption = 'Team'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 270
        Top = 21
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 72
        Top = 22
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 296
        Top = 22
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cmbPlusShift: TComboBoxPlus
        Left = 64
        Top = 78
        Width = 200
        Height = 19
        ColCount = 212
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 4
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusShiftCloseUp
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 64
        Top = 50
        Width = 200
        Height = 19
        ColCount = 212
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusPlantCloseUp
      end
      object CheckBoxAllTeam: TCheckBox
        Left = 496
        Top = 20
        Width = 73
        Height = 17
        Caption = 'All teams'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = CheckBoxAllTeam1Click
      end
      object cmbPlusTeamFrom: TComboBoxPlus
        Left = 64
        Top = 20
        Width = 200
        Height = 19
        ColCount = 234
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamFromCloseUp
      end
      object cmbPlusTeamTo: TComboBoxPlus
        Left = 286
        Top = 20
        Width = 200
        Height = 19
        ColCount = 234
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamToCloseUp
      end
      object CheckBoxOnlyAvailEmp: TCheckBox
        Left = 64
        Top = 104
        Width = 217
        Height = 17
        Caption = 'Show only available employees'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object CheckBoxWK: TCheckBox
        Left = 288
        Top = 104
        Width = 289
        Height = 17
        Caption = 'Show only workspots with occupation'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object GroupBoxDate: TGroupBox
        Left = 280
        Top = 51
        Width = 289
        Height = 49
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object LabelDate: TLabel
          Left = 8
          Top = 23
          Width = 23
          Height = 13
          Caption = 'Date'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object LabelDay: TLabel
          Left = 8
          Top = 23
          Width = 19
          Height = 13
          Caption = 'Day'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DateSelection: TDateTimePicker
          Left = 62
          Top = 20
          Width = 137
          Height = 21
          CalAlignment = dtaLeft
          Date = 37242.5624104051
          Time = 37242.5624104051
          DateFormat = dfShort
          DateMode = dmComboBox
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Kind = dtkDate
          ParseInput = False
          ParentFont = False
          TabOrder = 0
        end
        object DaySelection: TComboBox
          Left = 62
          Top = 20
          Width = 201
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
        end
      end
    end
    object GroupBoxReadStdPln: TGroupBox
      Left = 8
      Top = 152
      Width = 617
      Height = 185
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      object LabelFillDay: TLabel
        Left = 8
        Top = 96
        Width = 118
        Height = 13
        Caption = 'Fill day planning for date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 340
        Top = 95
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblFromPlant: TLabel
        Left = 8
        Top = 24
        Width = 120
        Height = 13
        Caption = 'Fill day planning for plant'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LblToPlant: TLabel
        Left = 340
        Top = 24
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelReadPast: TLabel
        Left = 8
        Top = 132
        Width = 139
        Height = 13
        Caption = 'Read day planning from date'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelTo: TLabel
        Left = 340
        Top = 130
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelTeamReadSTD: TLabel
        Left = 8
        Top = 60
        Width = 120
        Height = 13
        Caption = 'Fill day planning for team'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 340
        Top = 59
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 152
        Top = 62
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 360
        Top = 62
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DateFrom: TDateTimePicker
        Left = 152
        Top = 95
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 5
        OnChange = DateFromChange
      end
      object DateTo: TDateTimePicker
        Left = 358
        Top = 95
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 6
        OnChange = DateToChange
      end
      object cmbPlusPlantFrom: TComboBoxPlus
        Left = 152
        Top = 24
        Width = 180
        Height = 19
        ColCount = 233
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusPlantFromCloseUp
      end
      object cmbPlusPlantTo: TComboBoxPlus
        Left = 358
        Top = 24
        Width = 180
        Height = 19
        ColCount = 234
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusPlantToCloseUp
      end
      object DateSourceFrom: TDateTimePicker
        Left = 152
        Top = 130
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 7
        OnChange = DateSourceFromChange
      end
      object DateSourceTo: TDateTimePicker
        Left = 358
        Top = 130
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 8
      end
      object cmbPlusTeamSFrom: TComboBoxPlus
        Left = 152
        Top = 59
        Width = 180
        Height = 19
        ColCount = 234
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamSFromCloseUp
      end
      object cmbPlusTeamSTo: TComboBoxPlus
        Left = 358
        Top = 59
        Width = 180
        Height = 19
        ColCount = 235
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamSToCloseUp
      end
      object CheckBoxRDPAllTeam: TCheckBox
        Left = 548
        Top = 60
        Width = 65
        Height = 17
        Caption = 'All teams'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = CheckBoxRDPAllTeamClick
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 450
    Width = 631
  end
  inherited pnlBottom: TPanel
    Top = 409
    Width = 631
    inherited btnOk: TBitBtn
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 306
      OnClick = btnCancelClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 488
    Top = 24
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonSettings
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonSettings: TdxBarButton
      OnClick = dxBarButtonSettingsClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 592
    Top = 24
  end
  inherited StandardMenuActionList: TActionList
    Left = 560
    Top = 24
  end
end
