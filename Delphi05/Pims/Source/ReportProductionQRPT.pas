(*
  MRA: 30-NOV-2009 RV045.1.
    - Time For Time and Bonus In Money can be set on ContractGroup-level or
      HourType-level.
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:25-NOV-2011 RV102.4. SC-50021318 SO-20012421
  - Report Production
    - Workspot description can be too long,
      resulting in overwriting of figures in report.
  MRA:12-JAN-2012 RV104.2. 20012421. Small change.
  - Report Production
    - The workspot description was truncated to prevent it
      overwrites the column at the right. But now some
      descriptions ar not shown correct. Can this be solved?
  MRA:8-MAY-2012 20012858. Production Reports and NOJOB.
  - Filter out the NOJOB-job to prevent wrong quantities when
    this job has a negative quantity.
  MRA:23-JUL-2012 20012858.130.3. Change.
  - Report Production
    - Add option to show compare jobs or not using
      a checkbox.
      Note: A compare job is a job where field COMPARATION_JOB_YN
      is set to 'Y'.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - When a job (JOBCODE-table) has field IGNOREQUANTITIESINREPORTS_YN set
    to 'Y' then the report should show no quantities for that job, only
    the time must be shown.
  MRA:8-OCT-2012 20013489 Overnight-shift-system
  - Filter production-quantities on SHIFT_DATE to get the right records for
    a complete (overnight-) shift.
  - Filter scans also on SHIFT_DATE to get the right records for a complete
    (overnight-) shift (QueryTimeRegScanning).
  MRA:30-OCT-2012 20013489 Bugfix.
  - This report gave wrong quantities when 1 date was selected.
    Cause: The 'DateTo'-variable still had a time-part.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  MRA:7-MAR-2013 20014002 Show jobs
  - Add option to show jobs:
    - Show jobs (workspots + jobs)
    - Show only jobs (jobs)
    - Sort on job-code or job-name
  MRA:21-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  - Added this for jobs because of new 'show jobs'-option.
  ROP 13-MAR-2013 SR-20013769 - reorganised header
  MRA:21-MAR-2013 SR-20013769
  - Reconstructed the report with source from ROP, reason:
    - All labels that were originally used in column-header had a different
      name.
  - To get the plant/business/department-headers above the column-headers the
    following is done:
    - A TQRGroup is added under these headers.
    - Use right-mouse-button and Move Up to move the groups to the correct
      places. Due to a bug the components disappear in .pas-file. These must
      be added again after the move.
    - The columns from the original columns-header-band were moved (using
      CUT and PASTE, do not use COPY and PASTE or they are renumbered!).
    - After that the original columns-header-band was removed.
  MRA:21-MAR-2013 TD-21527
  - Also do this for time-values (DecodeHrsMin). For this purpose function
    DecodeHrsMin has extra argument that decides if it should new format or not.
    Default not.
  MRA:7-MAR-2013 20014002 Show jobs
  - Small fix for export: Jobcode + description must be shown in 1 column.
  - Also other levels:
    - Pieces must always be shown in second column during export:
      - Column 1: Code + Description
      - Column 2: Pieces
      - Column 3: Worked hrs
      - Etcetera.
  ROP: 26-MAR-2013 - 20013965
  - Add a switch to show or hide payroll info from report Production
  MRA:27-MAR-2013 - 20013965
  - Only export a column-header when it is printed.
  - Do not show totals-description when exporting.
  - Show text 'Total Workspot' or not based on dialog-selections when exporting.
  MRA:12-APR-2013 TD-21527
  - Use CurrencyFormat to set the currency symbol (instead of dollar-sign).
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:19-AUG-2014 20015220.90 Rework
  - Make more room for from-to-columns in selection-part, because of
    from-to-date that can be much larger when time-part is shown.
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
  MRA:27-OCT-2014 20015220.110 Rework
  - When ProductionQuantity-records are entered over longer periods than
    5 min. then it cannot always find it, depending on what was entered
    for 'selection on time'.
  MRA:2-JAN-2015 20015220.120 Rework
  - This report gives a too high value for pieces.
  - It looks like the previous change made (20015220.110) is giving this
    wrong result.
  MRA:9-JUN-2015 SO-20014450.50
  - Real Time Efficiency
  - Get data from a view
  MRA:22-DEC-2015 PIM-12
  - Bugfix: Do not look for overtimemins!
  MRA:20-JAN-2016 PIM-135
  - Add salary and diff. hours to report.
  - Extra: Also show overtime hours on job-level.
  MRA:5-FEB-2016 PIM-135
  - Production reports add salary-hours column
  - Related to this order:
  - Also show down-time-jobs + breaks.
  - Show down-time-jobs based on dialog-setting:
    - Show it not (default), yes, or only.
  MRA:15-FEB-2016 PIM-135
  - Do not show salary/diff columns.
  MRA:18-MAR-2016 PIM-12.4
  - Also filter on CompareJobs for RealTimeEff-reports.
*)

unit ReportProductionQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, DB,
  CalculateTotalHoursDMT, GlobalDMT, UScannedIDCard, QRExport, QRXMLSFilt,
  QRWebFilt, QRPDFFilt, ComCtrls;

type
  PTSalaryRecord=^TSalaryRecord;
  TSalaryRecord=record
    SalaryMinute: Integer;
    BonusPercentage: Double;
    HTBonusInMoney: Boolean;
  end;

type
  PTTimeRegRecord=^TTimeRegRecord;
  TTimeRegRecord=record
    WorkspotCode: String;
    JobCode: String;
    SalMinute: Integer;
  end;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FTeamFrom, FTeamTo: String; FShowBU, FShowDept, FShowWK, FShowJob, FShowEmpl,
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK,
    FShowCompareJobs: Boolean;
    FShowJobs, FShowOnlyJobs: Boolean; // 20014002
    FSortOnJob: Integer; // 20014002
    FExportToFile: Boolean;
    FShowPayrollInformation: Boolean; // [ROP] 20013965
    FIncludeDownTime: Integer; // 20015586
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      TeamFrom, TeamTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK,
      ShowCompareJobs: Boolean;
      ShowJobs, ShowOnlyJobs: Boolean; // 20014002
      SortOnJob: Integer; // 20014002
      ExportToFile: Boolean;
      ShowPayrollInformation: Boolean; // [ROP] 20013965
      IncludeDownTime: Integer); // 20015586
  end;

  TAmountsPerDay = Array[1..7] of Integer;

  TReportProductionQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDJOB: TQRGroup;
    QRGroupColumnHeader: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDescBU: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRBandFooterWK: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBandFooterDEPT: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabelWKOvertimeHrs: TQRLabel;
    QRLabelWKPayrollDollars: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelWKCostsPerPiece: TQRLabel;
    QRLabelWKCostsPerHr: TQRLabel;
    QRLabelWKPiecesPerHr: TQRLabel;
    QRLabelTotalDeptPieces: TQRLabel;
    QRLabelTotalBUPieces: TQRLabel;
    QRLabelTotalPlantPieces: TQRLabel;
    QRLabelWKWorkedHrs: TQRLabel;
    QRLabelTotalDeptWorkedHrs: TQRLabel;
    QRLabelTotalBUWorkedHrs: TQRLabel;
    QRLabelTotalPlantWorkedHrs: TQRLabel;
    QRLabelTotalDeptOvertimeHrs: TQRLabel;
    QRLabelTotalBUOvertimeHrs: TQRLabel;
    QRLabelTotalPlantOvertimeHrs: TQRLabel;
    QRLabelTotalDeptPayrollDollars: TQRLabel;
    QRLabelTotalBUPayrollDollars: TQRLabel;
    QRLabelTotalPlantPayrollDollars: TQRLabel;
    QRLabelTotalDeptCostsPerPiece: TQRLabel;
    QRLabelTotalBUCostsPerPiece: TQRLabel;
    QRLabelTotalPlantCostsPerPiece: TQRLabel;
    QRLabelTotalDeptCostsPerHr: TQRLabel;
    QRLabelTotalBUCostsPerHr: TQRLabel;
    QRLabelTotalPlantCostsPerHr: TQRLabel;
    QRLabelTotalDeptPiecesPerHr: TQRLabel;
    QRLabelTotalBUPiecesPerHr: TQRLabel;
    QRLabelTotalPlantPiecesPerHr: TQRLabel;
    QRLabelWKPieces: TQRLabel;
    AQuery: TQuery;
    QRLabelTotalBUSales: TQRLabel;
    QRLabelTotalBUPercPayrollToSales: TQRLabel;
    QRLabelTotalBUIncomePerWorkedHour: TQRLabel;
    QRLabelTotalBUMarginalRatePerHour: TQRLabel;
    QRBandSummary: TQRBand;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel198: TQRLabel;
    QRBandFooterJob: TQRBand;
    QRLabelJobCode: TQRLabel;
    QRLabelJobDesc: TQRLabel;
    QRLabelJobPieces: TQRLabel;
    QRLabelJobWorkedHrs: TQRLabel;
    QRLabelJobPayrollDollars: TQRLabel;
    QRLabelJobCostsPerPiece: TQRLabel;
    QRLabelJobCostsPerHr: TQRLabel;
    QRLabelJobPiecesPerHr: TQRLabel;
    QRLabelWorkspotColumnHeader: TQRLabel;
    QRLabelJobColumnHeader: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabelIncludeDowntime: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabelJobSalHrs: TQRLabel;
    QRLabelWKSalHrs: TQRLabel;
    QRLabelTotalDeptSalHrs: TQRLabel;
    QRLabelTotalBUSalHrs: TQRLabel;
    QRLabelTotalPlantSalHrs: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabelJobDiffHrs: TQRLabel;
    QRLabelWKDiffHrs: TQRLabel;
    QRLabelTotalDeptDiffHrs: TQRLabel;
    QRLabelTotalBUDiffHrs: TQRLabel;
    QRLabelTotalPlantDiffHrs: TQRLabel;
    QRLabelJobOvertimeHrs: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
(*    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean); *)
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDescDeptPrint(sender: TObject; var Value: String);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJOBAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHdPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRBandFooterJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRLabelJobDescPrint(sender: TObject; var Value: String);
    procedure QRGroupColumnHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupColumnHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    SalaryList: TList;
    TimeRegList: TList;
    AllPlants, AllTeams, AllBusinessUnits, AllWorkspots,
    AllDepartments: Boolean;
    FJobColumnHeader: String;
    FWorkspotColumnHeader: String;
//    function DetermineTimeRegScanningMinutes: Integer;
{    function DetermineOvertime(
      const CurrentWorkspotCode, CurrentJobCode: String; HourlyWage: Double;
      EmployeeNumber: Integer): Integer; }
//    function DetermineOvertimeXXX: Integer;
    procedure DetermineWorkedHours;
//    function DeterminePieces: Double;
    function DecodeHrsSalMin(AValue: Integer): String;
    function DecodeHrsDiffMin(AValue: Integer): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    LevelPlant, LevelBU, LevelDept,
    LevelWK, LevelJob, LevelEmpl, WKHasJobDummy: Boolean;
    LastLevel: Char;
    DetailEmployeeNumber: Integer;
    TotalDetailPieces: Double;
    TotalPlantPieces, TotalBUPieces, TotalDeptPieces: Double;
    TotalWorkspotPieces: Double; // 20014002
    TotalJobPieces: Double; // 20014002
    TotalPlantWorkedHrs, TotalBUWorkedHrs, TotalDeptWorkedHrs: Integer;
    TotalWorkspotWorkedHrs, TotalJobWorkedHrs: Integer; // 20014002
    TotalPlantSalHrs, TotalBUSalHrs, TotalDeptSalHrs,
    TotalWorkspotSalHrs, TotalJobSalHrs: Integer; // PIM-135
    TotalPlantDiffHrs, TotalBUDiffHrs, TotalDeptDiffHrs,
    TotalWorkspotDiffHrs, TotalJobDiffHrs: Integer; // PIM-135
    TotalPlantOvertimeHrs, TotalBUOvertimeHrs, TotalDeptOvertimeHrs,
    TotalWorkspotOvertimeHrs, TotalJobOvertimeHrs: Integer;
    TotalPlantPayrollDollars, TotalBUPayrollDollars,
    TotalDeptPayrollDollars, TotalWorkspotPayrollDollars,
    TotalJobPayrollDollars: Double;
    FDaysBU, FDaysDept, FDaysPlant, FDaysWK, FDaysJob, FDaysEmpl: Integer;
    LastPlantCode, LastBUCode, LastDeptCode, LastWorkspotCode,
    LastJob: String;
    MyProgressBar: TProgressBar;
    procedure SetLevelReport;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, ShowCompareJobs: Boolean;
      const ShowJobs, ShowOnlyJobs: Boolean; // 20014002
      const SortOnJob: Integer; // 20014002
      const ExportToFile: Boolean;
      const ShowPayrollInformation: Boolean; // [ROP] 20013965
      const IncludeDownTime: Integer): Boolean; // 20015586
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure AddAmounts(Amount: Integer; BU, Dept, WK, Job, Empl: Boolean);
    function ExecuteForDummy(Plant, WK, BU:String): Integer;
    property WorkspotColumnHeader: String read FWorkspotColumnHeader
      write FWorkspotColumnHeader; // 20014002
    property JobColumnHeader: String read FJobColumnHeader
      write FJobColumnHeader; // 20014002
  end;

var
  ReportProductionQR: TReportProductionQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportProductionDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes, ReportProductionDetailDMT;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      TeamFrom, TeamTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK,
      ShowCompareJobs: Boolean;
      ShowJobs, ShowOnlyJobs: Boolean; // 20014002
      SortOnJob: Integer; // 20014002
      ExportToFile: Boolean;
      ShowPayrollInformation: Boolean;  // [ROP] 20013965
      IncludeDownTime: Integer); // 20015586
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    FWKFrom := ReportProductionQR.InclExclWorkspotsResult;
    FWKTo := ReportProductionQR.InclExclWorkspotsResult;
  end
  else
  begin
    FWKFrom := WKFrom;
    FWKTo := WKTo;
  end;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShowBU := ShowBU;
  FShowDept := ShowDept;
  FShowWK := ShowWK;
  FShowJob := ShowJob;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FShowJobs := ShowJobs; // 20014002
  FShowOnlyJobs := ShowOnlyJobs; // 20014002
  FSortOnJob := SortOnJob; // 20014002
  FExportToFile := ExportToFile;
  FShowCompareJobs := ShowCompareJobs;
  FShowPayrollInformation := ShowPayrollInformation; // [ROP] 20013965
  FIncludeDownTime := IncludeDownTime; // 20015586
end;

function TReportProductionQR.QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection,
      PagePlant, PageBU, PageDept, PageWK, ShowCompareJobs: Boolean;
      const ShowJobs, ShowOnlyJobs: Boolean; // 20014002
      const SortOnJob: Integer; // 20014002
      const ExportToFile: Boolean;
      const ShowPayrollInformation: Boolean;
      const IncludeDownTime: Integer): Boolean; // [ROP] 20013695
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, ShowCompareJobs,
      ShowJobs, ShowOnlyJobs, // 20014002
      SortOnJob, // 20014002
      ExportToFile,
      ShowPayrollInformation,
      IncludeDownTime); // [ROP] 20013695
  end;
  SetDataSetQueryReport(ReportProductionDM.QueryProduction);
end;

function TReportProductionQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;

  if UseDateTime or IncludeOpenScans then // 20015220 + 20015221
  begin
    // Init/Reset Client Datasets.
    ReportProductionDetailDM.cdsEmpl.Close;
    ReportProductionDetailDM.cdsEmpl.Filtered := False;
    ReportProductionDetailDM.cdsEmpl.Open;
  end;

  // PIM-12.4.
  ReportProductionDetailDM.ShowCompareJobs := QRParameters.FShowCompareJobs;

  with ReportProductionDM do
  begin
    AllPlants := DetermineAllPlants(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
    AllTeams := DetermineAllTeams(
      QRParameters.FTeamFrom, QRParameters.FTeamTo);
    AllBusinessUnits := DetermineAllBusinessUnits(
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
    AllWorkspots := False;
    AllDepartments := False;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      AllWorkspots := DetermineAllWorkspots(QRBaseParameters.FPlantFrom,
        QRParameters.FWKFrom, QRParameters.FWKTo);
      AllDepartments := DetermineAllDepartments(QRBaseParameters.FPlantFrom,
        QRParameters.FDeptFrom, QRParameters.FDeptTo);
    end;
  end;

  // 20014450.50
  SelectStr :=
    ReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo,
      QRParameters.FDeptFrom, QRParameters.FDeptTo,
      QRParameters.FWKFrom, QRParameters.FWKTo,
      '1', 'zzzzzz',
      QRParameters.FTeamFrom, QRParameters.FTeamTo,
      '0', '999', // ShiftFrom, ShiftTo
      '', '', // EmployeeFrom, EmployeeTo PIM-137
      Trunc(QRParameters.FDateFrom),
      Trunc(QRParameters.FDateTo),
      AllPlants,
      AllBusinessUnits,
      AllDepartments,
      AllWorkspots,
      AllTeams,
      True,
      False, // ShowShifts
      QRParameters.FShowOnlyJobs, // ShowOnlyJob
      QRParameters.FIncludeDownTime, // PIM-135
      False, // SelectShift
      False, // SelectDate
      0,  // 0=GroupBy BU + Dept 1=GroupBy BU 2=GroupBy Dept
      0,  // 0=SortByEmp Nr 1=SortByEmp Name
      False, // Ttue=Filter on job False=not
      False, // TeamIncentiveReport: Boolean=False;
      QRParameters.FSortOnJob // 0=SortByJob Code 1=SortByJob Description
      );

  with ReportProductionDM.QueryProduction do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(SelectStr);
//SQL.SaveToFile('c:\temp\ReportProduction.sql');
    Open;
    Result := not Eof;
  end;

  with ReportProductionDetailDM do
  begin
    // 20014450.50 This is needed for payroll-info
    if Result then
      InitPHEPTQuery(
        QRBaseParameters.FPlantFrom,
        QRBaseParameters.FPlantTo,
        QRParameters.FWKFrom,
        QRParameters.FWKTo,
        Trunc(QRParameters.FDateFrom),
        Trunc(QRParameters.FDateTo),
        AllPlants, AllWorkspots, False);
  end;
{
  SelectStr :=
    'SELECT DISTINCT ' + NL +
    '  PQ.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '  PQ.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION, ' + NL +
    '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION, ' + NL +
    '  PQ.SHIFT_NUMBER, ' + NL +
    '  PQ.JOB_CODE, J.DESCRIPTION AS JOBDESCRIPTION, ' + NL +
    '  NVL(J.IGNOREQUANTITIESINREPORTS_YN,' + '''' +  'N' + '''' + ') ' +
    '    AS IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478
    '  PQ.QUANTITY AS PIECES, PQ.START_DATE, PQ.MANUAL_YN ' + NL +
    'FROM ' + NL +
    '  PRODUCTIONQUANTITY PQ INNER JOIN PLANT P ON ' + NL +
    '    PQ.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    PQ.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND PQ.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectStr := SelectStr +
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PQ.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
    begin
      if not AllWorkspots then
        SelectStr := SelectStr +
          '  AND (PQ.WORKSPOT_CODE >= :WORKSPOTFROM ' + NL +
          '  AND PQ.WORKSPOT_CODE <= :WORKSPOTTO) ' + NL;
    end;
  end;
  SelectStr := SelectStr +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    PQ.PLANT_CODE = D.PLANT_CODE ' + NL +
    '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    if not AllDepartments then
      SelectStr := SelectStr +
        '  AND (D.DEPARTMENT_CODE >= :DEPARTMENTFROM ' + NL +
        '  AND D.DEPARTMENT_CODE <= :DEPARTMENTTO) ' + NL;
  SelectStr := SelectStr +
//CAR 22-10-2003 550263
    '  LEFT JOIN JOBCODE J ON ' + NL +
    '    PQ.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND PQ.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND PQ.JOB_CODE = J.JOB_CODE ' + NL +
    '  LEFT JOIN BUSINESSUNIT B ON ' + NL +
    '    PQ.PLANT_CODE = B.PLANT_CODE ' + NL +
    '    AND J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL +
//CAR 550254
// 20014002 Related to this order.
//          This is wrong! This join will give multiple records!
//          Which is solved by using a 'distinct'.
//          The problem here, it is used to filter on teams, how can this
//          be done in a different way?
    '  LEFT JOIN DEPARTMENTPERTEAM T ON  ' + NL +
    '    T.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND T.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL +
    'WHERE ' + NL;
  // 20015586
  if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
  begin
    SelectStr := SelectStr +
      '  ((PQ.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PQ.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  end;
  if SystemDM.UseShiftDateSystem then
  begin
    SelectStr := SelectStr +
// 20013489 Use SHIFT_DATE to filter on date. Als use < instead of <=
      '    (PQ.SHIFT_DATE >= :DATEFROM ' + NL +
      '    AND PQ.SHIFT_DATE < :DATETO) AND ' + NL;
    // 20015220 (*) -> Use < at this line, to prevent it takes 2 PQ-records when
    //                 period is something like: 9:00 to 9:05.
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        '  DATETIME_RANGE_OVERLAP_MINS(:DATETIMEFROM, :DATETIMETO, PQ.START_DATE, PQ.END_DATE) > 0 AND ' + NL; // 20015220.110
//        '   (PQ.START_DATE >= :DATETIMEFROM ' + NL +
//        '    AND PQ.START_DATE < :DATETIMETO) AND ' + NL; // (*)
  end
  else
  begin
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        '  DATETIME_RANGE_OVERLAP_MINS(:DATETIMEFROM, :DATETIMETO, PQ.START_DATE, PQ.END_DATE) > 0 AND ' + NL // 20015220.110
//        '   (PQ.START_DATE >= :DATETIMEFROM ' + NL +
//        '    AND PQ.START_DATE < :DATETIMETO) AND ' + NL // (*)
    else
      SelectStr := SelectStr +
        '   (PQ.START_DATE >= :DATEFROM ' + NL +
        '    AND PQ.START_DATE < :DATETO) AND ' + NL;
  end;
  // 20012858.
  SelectStr := SelectStr +
    '  (PQ.JOB_CODE <> ' + '''' + NOJOB + '''' + ') AND ' + NL +
    '  (PQ.QUANTITY <> 0) ' + NL +
  //RV067.2.
    '  AND ( ' + NL +
    '    (:USER_NAME = ''*'') OR ' + NL +
    '    (T.TEAM_CODE IN ' + NL +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '  ) ' + NL;
  // 20012858.130.3.
  if not QRParameters.FShowCompareJobs then
    SelectStr := SelectStr +
      '  AND (J.COMPARATION_JOB_YN = ' +
      '''' + UNCHECKEDVALUE + '''' + ') ' + NL;
  if not AllPlants then
    SelectStr := SelectStr +
      '  AND (PQ.PLANT_CODE >= :PLANTFROM ' + NL +
      '  AND PQ.PLANT_CODE <= :PLANTTO) ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    if not AllBusinessUnits then
      SelectStr := SelectStr +
        '  AND (B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM ' + NL +
        '  AND B.BUSINESSUNIT_CODE <= :BUSINESSUNITTO) ' + NL;
    //CAR 550254
  if not AllTeams then
    SelectStr := SelectStr +
      '  AND (T.TEAM_CODE >= :TEAMFROM ' +
      '  AND T.TEAM_CODE <= :TEAMTO) ' + NL;

  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  PQ.PLANT_CODE, ' + NL +
    '  B.BUSINESSUNIT_CODE, ' + NL +
    '  D.DEPARTMENT_CODE, ' + NL;

  // 20014002
  if QRParameters.FShowJobs then
  begin
    if not QRParameters.FShowOnlyJobs then
      SelectStr := SelectStr +
        '  PQ.WORKSPOT_CODE, ' + NL;
    if QRParameters.FSortOnJob = 0 then // Code
      SelectStr := SelectStr +
        '  PQ.JOB_CODE, ' + NL +
        '  PQ.WORKSPOT_CODE '
    else // Name
      SelectStr := SelectStr +
        '  J.DESCRIPTION, ' +
        '  PQ.WORKSPOT_CODE ';
  end
  else
    SelectStr := SelectStr +
      '  PQ.WORKSPOT_CODE, ' + NL +
      '  PQ.JOB_CODE ';

  with ReportProductionDM.QueryProduction do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
//SQL.SaveToFile('c:\temp\ReportProduction.sql');
    //RV067.2.
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      ParamByName('USER_NAME').AsString := '*';
//    QRParameters.FDateFrom := Trunc(QRParameters.FDateFrom); // 20015220
    // 20013489
    // Only trunc these dates (it still has a time after it).
//    QRParameters.FDateTo := Trunc(QRParameters.FDateTo); // 20015220
// 20013489 Filter < for DateTo instead of <=
//    QRParameters.FDateTo := Trunc(QRParameters.FDateTo) +
//      Frac(EncodeTime(23, 59, 59, 0));
    if not AllPlants then
    begin
      ParamByName('PLANTFROM').AsString := QRBaseParameters.FPlantFrom;
      ParamByName('PLANTTO').AsString := QRBaseParameters.FPlantTo;
    end;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      if not AllBusinessUnits then
      begin
        ParamByName('BUSINESSUNITFROM').AsString := QRParameters.FBusinessFrom;
        ParamByName('BUSINESSUNITTO').AsString := QRParameters.FBusinessTo;
      end;
      if not SystemDM.WorkspotInclSel then // 20015223
        if not AllWorkspots then
        begin
          ParamByName('WORKSPOTFROM').AsString := QRParameters.FWKFrom;
          ParamByName('WORKSPOTTO').AsString := QRParameters.FWKTo;
        end;
      if not AllDepartments then
      begin
        ParamByName('DEPARTMENTFROM').AsString := QRParameters.FDeptFrom;
        ParamByName('DEPARTMENTTO').AsString := QRParameters.FDeptTo;
      end;
    end;
    // 20015220
    if SystemDM.UseShiftDateSystem then
    begin
      ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
      ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo) + 1;
    end;
    if UseDateTime then // 20015220
    begin
      ParamByName('DATETIMEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETIMETO').AsDateTime := QRParameters.FDateTo;
    end
    else
    begin
      ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
      ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo) + 1;
    end;
    //550254 car
    if not AllTeams then
    begin
      ParamByName('TEAMFROM').AsString := QRParameters.FTeamFrom;
      ParamByName('TEAMTO').AsString := QRParameters.FTeamTo;
    end;
    if not Prepared then
      Prepare;
    Active := True;
    Result := (not IsEmpty);
  end;
  // 20015220 We need this to determine worked hours based on date+time
  // 20015221
  if (UseDateTime or IncludeOpenScans) and Result then
  begin
    ReportProductionDetailDM.InitAll(
      QRBaseParameters.FPlantFrom,
      QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom,
      QRParameters.FBusinessTo,
      QRParameters.FDeptFrom,
      QRParameters.FDeptTo,
      QRParameters.FWKFrom,
      QRParameters.FWKTo,
      QRParameters.FTeamFrom,
      QRParameters.FTeamTo,
      '1', // QRParameters.FShiftFrom,
      '1', // QRParameters.FShiftTo,
      QRParameters.FDateFrom,
      QRParameters.FDateTo,
      AllPlants,
      AllBusinessUnits,
      AllDepartments,
      AllWorkspots,
      AllTeams,
      True, // QRParameters.FAllShifts,
      False, // QRParameters.FShowShifts,
      MyProgressBar,
      QRParameters.FIncludeDownTime = DOWNTIME_ONLY // 20015586
      );
  end;
}
  Screen.Cursor := crDefault;
end;

function TReportProductionQR.ExecuteForDummy(Plant, WK, BU:String): Integer;
var
  SelectStr: String;
begin
//  result := 0;
  SelectStr := ' SELECT PLANT_CODE, WORKSPOT_CODE , ' ;
  if BU <> '' then
    SelectStr := SelectStr + ' BUSINESSUNIT_CODE, ';
  SelectStr := SelectStr +
    ' SUM(PERCENTAGE) AS SUMMIN FROM BUSINESSUNITPERWORKSPOT ' +
    ' WHERE PLANT_CODE= :PLANT_CODE AND WORKSPOT_CODE = :WORKSPOT_CODE ';
  if BU <> '' then
    SelectStr := SelectStr + ' AND BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE';
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    if (QRParameters.FBusinessFrom <> '') and ( QRParameters.FBusinessTo <> '') and
      (BU = '') then
      SelectStr := SelectStr +
        ' AND BUSINESSUNIT_CODE >= ''' + QRParameters.FBusinessFrom + ''''+
        ' AND BUSINESSUNIT_CODE <= ''' + QRParameters.FBusinessTo + '''';
  SelectStr := SelectStr + ' GROUP BY PLANT_CODE, WORKSPOT_CODE ';
  if BU <> '' then
     SelectStr := SelectStr + ', BUSINESSUNIT_CODE ';

  ReportProductionDM.QueryBUPerWK.Active := False;
  ReportProductionDM.QueryBUPerWK.UniDirectional := False;
  ReportProductionDM.QueryBUPerWK.SQL.Clear;
  ReportProductionDM.QueryBUPerWK.SQL.Add(UpperCase(SelectStr));
  ReportProductionDM.QueryBUPerWK.ParamByName('PLANT_CODE').Value := PLANT;
  ReportProductionDM.QueryBUPerWK.ParamByName('WORKSPOT_CODE').Value := WK;
  if BU <> '' then
    ReportProductionDM.QueryBUPerWK.ParamByName('BUSINESSUNIT_CODE').Value := BU;
  if not ReportProductionDM.QueryBUPerWK.Prepared then
    ReportProductionDM.QueryBUPerWK.Prepare;
  ReportProductionDM.QueryBUPerWK.Active := True;
  ReportProductionDM.QueryBUPerWK.First;
  Result := Round(ReportProductionDM.QueryBUPerWK.FieldByName('SUMMIN').AsFloat);
end;

procedure TReportProductionQR.SetLevelReport;
begin
  LevelPlant := True;
  LevelBU := False;
  LevelDept := False;
  LevelWK := False;
  LevelJob := False;
  LevelEmpl := False;
  LastLevel := 'P';
 if QRParameters.FShowBU then
  begin
    LevelBU := True;
    LastLevel := 'B';
  end;
  if QRParameters.FShowDept then
  begin
    LevelDept := True;
    LastLevel := 'D';
  end;
  if QRParameters.FShowWK then
  begin
    LevelWK := True;
    LastLevel := 'W';
  end;
  if QRParameters.FShowJobs then // 20014002
  begin
    LevelJob := True;
    LastLevel := 'J';
  end;
  if QRParameters.FShowEmpl then
  begin
    LevelEmpl := True;
    LastLevel := 'E';
  end;
end;

procedure TReportProductionQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := DisplayWorkspotFrom(QRParameters.FWKFrom); // 20015223
  QRLabelWKTo.Caption := DisplayWorkspotTo(QRParameters.FWKTo); // 20015223
  QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
  QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  if UseDateTime then // 20015220
  begin
    QRLabelDateFrom.Caption := DateTimeToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateTimeToStr(QRParameters.FDateTo);
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  end;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end;
  // 20014002
  QRLabelWorkspotColumnHeader.Caption := WorkspotColumnHeader;
  QRLabel38.Caption := WorkSpotColumnHeader; // [ROP] 20013965
  QRGroupHDWK.Enabled := True;
  QRGroupHDWK.Expression := 'QueryProduction.WORKSPOT_CODE';
  QRBandFooterWK.Enabled := True;
  if QRParameters.FShowJobs then
  begin
    QRLabelJobColumnHeader.Caption := JobColumnHeader;
    QRLabel39.Caption := JobColumnHeader; // [ROP] 20013965
    if QRParameters.FShowOnlyJobs then
    begin
      QRGroupHDWK.Enabled := False;
      QRGroupHDWK.Expression := '';
      QRBandFooterWK.Enabled := False;
      QRLabelWorkspotColumnHeader.Caption := '';
      QRLabel38.Caption := ''; // [ROP] 20013965
    end;
    if QRParameters.FSortOnJob = 0 then // Code
      QRGroupHDJob.Expression := 'QueryProduction.JOB_CODE'
    else
      QRGroupHDJob.Expression := 'QueryProduction.JDESCRIPTION';
  end
  else
    begin
      QRLabelJobColumnHeader.Caption := '';
      QRLabel39.Caption := ''; // [ROP] 20013965
    end;
  // 20015586
  case QRParameters.FIncludeDownTime of
  DOWNTIME_NO:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeNo;
  DOWNTIME_YES:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeYes;
  DOWNTIME_ONLY:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeOnly;
  end;
end;

procedure TReportProductionQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportProductionQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  WorkspotColumnHeader := QRLabelWorkspotColumnHeader.Caption; // 20014002
  JobColumnHeader := QRLabelJobColumnHeader.Caption; // 20014002
  QRLabel7.Caption := CurrencyString; // TD-21527
  // PIM-135 Do not show Salary/Diff-columns
  QRLabel45.Caption := '';
  QRLabel46.Caption := '';
  QRLabel53.Caption := '';
  QRLabel51.Caption := '';
  QRLabel52.Caption := '';
  QRLabel56.Caption := '';
end;

procedure TReportProductionQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;

  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Selections
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel25.Caption + ' ' + QRLabel48.Caption + ' ' +
        QRLabelBusinessFrom.Caption + ' ' + QRLabel16.Caption + ' ' +
        QRLabelBusinessTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelWKFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' + QRLabelWKTo.Caption);
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' +
        QRLabelDate.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLabelDateTo.Caption);
      ExportClass.AddText(QRLabelIncludeDowntime.Caption); // 20015586
    end;
  end;

  TotalPlantPieces := 0;
  TotalBUPieces := 0;
  TotalDeptPieces := 0;
  TotalWorkspotPieces := 0; // 20014002
  TotalJobPieces := 0; // 20014002

  TotalPlantWorkedHrs := 0;
  TotalBUWorkedHrs := 0;
  TotalDeptWorkedHrs := 0;
  TotalWorkspotWorkedHrs := 0; // 20014002
  TotalJobWorkedHrs := 0; // 20014002

  TotalPlantOvertimeHrs := 0;
  TotalBUOvertimeHrs := 0;
  TotalDeptOvertimeHrs := 0;
  TotalWorkspotOvertimeHrs := 0;
  TotalJobOvertimeHrs := 0;

  TotalPlantPayrollDollars := 0;
  TotalBUPayrollDollars := 0;
  TotalDeptPayrollDollars := 0;
  TotalWorkspotPayrollDollars := 0;
  TotalJobPayrollDollars := 0; // 20014002

  // PIM-135
  TotalPlantSalHrs := 0;
  TotalBUSalHrs := 0;
  TotalDeptSalHrs := 0;
  TotalWorkspotSalHrs := 0;
  TotalJobSalHrs := 0;
  // PIM-135
  TotalPlantDiffHrs := 0;
  TotalBUDiffHrs := 0;
  TotalDeptDiffHrs := 0;
  TotalWorkspotDiffHrs := 0;
  TotalJobDiffHrs := 0;

  FDaysPlant := 0;
  FDaysBU := 0;
  FDaysDept := 0;
  FDaysWK := 0;
  FDaysJob := 0;
  FDaysEmpl := 0;

  LastPlantCode := '';
  LastBUCode := '';
  LastDeptCode := '';
  LastWorkspotCode := '';
end;

procedure TReportProductionQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
//  SumMin: Integer;
//  EmployeeNumber: Integer;
//  HourlyWage: Double;
  WorkedHrs: Integer;
//  OvertimeMinute: Integer;
  OvertimeHrs: Integer;
  Pieces: Double;
  PayrollDollars: Double;
  SalHrs: Integer;
  DiffHrs: Integer;
begin
  inherited;
  LastWorkspotCode :=
    ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
  // 20014002
  if QRParameters.FShowJobs and QRParameters.FShowOnlyJobs then
    PrintBand := False
  else
    PrintBand := True;

  Pieces := TotalDetailPieces;

  // 20024002 TotalJobWorkedHrs is now initialised for each job, so use
  //          a different variable here.
//  WorkedHrs := TotalJobWorkedHrs; // 20014002
  WorkedHrs := TotalWorkspotWorkedHrs; // 20014002
  PayrollDollars := TotalWorkspotPayrollDollars;
  OvertimeHrs := TotalWorkspotOvertimeHrs;

  SalHrs := TotalWorkspotSalHrs;
  DiffHrs := TotalWorkspotDiffHrs;

//  Pieces :=
//    ReportProductionDM.QueryProduction.FieldByName('PIECES').AsFloat;
  QRLabelWKPieces.Caption := Format('%.0n', [Pieces]);

//  WorkedHours := TotalDetailWorkedHours;
//  WorkedHours :=
//    ReportProductionDM.QueryProduction.FieldByName('WORKEDHOURS').AsInteger;
  QRLabelWKWorkedHrs.Caption := DecodeHrsMin(WorkedHrs, True);

  QRLabelWKSalHrs.Caption := DecodeHrsSalMin(SalHrs);
  QRLabelWKDiffHrs.Caption := DecodeHrsDiffMin(DiffHrs);

  // Must be calculated!
  QRLabelWKOvertimeHrs.Caption := DecodeHrsMin(OvertimeHrs, True);

  QRLabelWKPayrollDollars.Caption := Format('%.2n', [PayrollDollars]);

  // Costs per Piece
  if Pieces > 0 then
    QRLabelWKCostsPerPiece.Caption :=
      Format('%.3n', [PayrollDollars / Pieces])
  else
    QRLabelWKCostsPerPiece.Caption := Format('%.3n', [0.0]);
  // Costs per Hour
  if WorkedHrs > 0 then
    QRLabelWKCostsPerHr.Caption :=
      Format('%.2n', [PayrollDollars / (WorkedHrs / 60)])
  else
    QRLabelWKCostsPerHr.Caption := Format('%.2n', [0.0]);
  // Pieces per Hour
  if WorkedHrs > 0 then
    QRLabelWKPiecesPerHr.Caption :=
      Format('%.0n', [Pieces / (WorkedHrs / 60)])
  else
    QRLabelWKPiecesPerHr.Caption := Format('%.0n', [0.0]);

  // 20014002 Do (pieces) this in details-part
{
  TotalPlantPieces := TotalPlantPieces + Pieces;
  TotalBUPieces := TotalBUPieces + Pieces;
  TotalDeptPieces := TotalDeptPieces + Pieces;
}
  // 20014002 Worked hours must be calculated here, but only when no jobs
  //          are shown.
  if (not QRParameters.FShowJobs) then
  begin
    TotalPlantWorkedHrs := TotalPlantWorkedHrs + WorkedHrs;
    TotalBUWorkedHrs := TotalBUWorkedHrs + WorkedHrs;
    TotalDeptWorkedHrs := TotalDeptWorkedHrs + WorkedHrs;

    TotalPlantSalHrs := TotalPlantSalHrs + SalHrs;
    TotalBUSalHrs := TotalBUSalHrs + SalHrs;
    TotalDeptSalHrs := TotalDeptSalHrs + SalHrs;

    TotalPlantDiffHrs := TotalPlantDiffHrs + DiffHrs;
    TotalBUDiffHrs := TotalBUDiffHrs + DiffHrs;
    TotalDeptDiffHrs := TotalDeptDiffHrs + DiffHrs;

    TotalPlantOvertimeHrs := TotalPlantOvertimeHrs + OvertimeHrs;
    TotalBUOvertimeHrs := TotalBUOvertimeHrs + OvertimeHrs;
    TotalDeptOvertimeHrs := TotalDeptOvertimeHrs + OvertimeHrs;

    TotalPlantPayrollDollars := TotalPlantPayrollDollars + PayrollDollars;
    TotalBUPayrollDollars := TotalBUPayrollDollars + PayrollDollars;
    TotalDeptPayrollDollars := TotalDeptPayrollDollars + PayrollDollars;
  end;

  //[ROP] 20013965
  if not QRParameters.FShowPayrollInformation then
    begin
      QRLabelWKPayrollDollars.Caption := '';
      QRLabelWKCostsPerPiece.Caption := '';
      QRLabelWKCostsPerHr.Caption := '';
    end;
  // [ROP] end 20013965
end; // QRBandFooterWKBeforePrint

procedure TReportProductionQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  // 20014002
  if QRParameters.FShowJobs and (not QRParameters.FShowOnlyJobs) then
    PrintBand := True
  else
    PrintBand := False;
  TotalDetailPieces := 0;

  TotalWorkspotPieces := 0; // 20014002
  TotalWorkspotWorkedHrs := 0; // 20014002
  TotalWorkspotSalHrs := 0;
  TotalWorkspotDiffHrs := 0;
  TotalWorkspotPayrollDollars := 0;
  TotalWorkspotOvertimeHrs := 0;

  TotalJobWorkedHrs := 0;
  TotalJobSalHrs := 0;
  TotalJobDiffHrs := 0;
  TotalJobPayrollDollars := 0;
  TotalJobOvertimeHrs := 0;
  LastWorkspotCode := '';
end;

procedure TReportProductionQR.QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TotalBUPieces := 0;
  TotalBUWorkedHrs := 0;
  TotalBUSalHrs := 0;
  TotalBUDiffHrs := 0;
  TotalBUOvertimeHrs := 0;
  TotalBUPayrollDollars := 0;
  LastBUCode := '';
(*
  if LastLevel = 'B' then
    PrintBand := False;
//  WKHasJobDummy := False;
  QRGroupHdBU.ForceNewPage := QRParameters.FPageBU;
  inherited;
*)
end;

procedure TReportProductionQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
//  TotalPlant := 0;
  FDaysPlant := 0;
  LastPlantCode := '';

  // 20014002 This must also be done here!
  TotalPlantPieces := 0;
  TotalPlantWorkedHrs := 0;
  TotalPlantSalHrs := 0;
  TotalPlantDiffHrs := 0;
  TotalPlantOvertimeHrs := 0;
  TotalPlantPayrollDollars := 0;
end;

procedure TReportProductionQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalBUSales: Double;
  TotalBUPayrollToSales: Double;
  TotalBUIncomePerWorkedHour: Double;
  TotalBUMarginalRatePerHour: Double;
begin
  inherited;
  LastBUCode :=
    ReportProductionDM.QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString;
// ROP 05-MAR-2013 TD-21527  QRLabelTotalBUPieces.Caption := Format('%.0f', [TotalBUPieces]);
  QRLabelTotalBUPieces.Caption := Format('%.0n', [TotalBUPieces]);
  QRLabelTotalBUWorkedHrs.Caption := DecodeHrsMin(TotalBUWorkedHrs, True);
  QRLabelTotalBUSalHrs.Caption := DecodeHrsSalMin(TotalBUSalHrs);
  QRLabelTotalBUDiffHrs.Caption := DecodeHrsDiffMin(TotalBUDiffHrs);
  QRLabelTotalBUOverTimeHrs.Caption := DecodeHrsMin(TotalBUOvertimeHrs, True);
  QRLabelTotalBUPayrollDollars.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUPayrollDollars]);
    Format('%.2n', [TotalBUPayrollDollars]);
  if TotalBUPieces > 0 then
    QRLabelTotalBUCostsPerPiece.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.3f', [TotalBUPayrollDollars / TotalBUPieces])
      Format('%.3n', [TotalBUPayrollDollars / TotalBUPieces])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUCostsPerPiece.Caption := Format('%.3f', [0.0]);
    QRLabelTotalBUCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalBUWorkedHrs > 0 then
    QRLabelTotalBUCostsPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [TotalBUPayrollDollars / (TotalBUWorkedHrs / 60)])
      Format('%.2n', [TotalBUPayrollDollars / (TotalBUWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUCostsPerHr.Caption := Format('%.2f', [0.0]);
    QRLabelTotalBUCostsPerHr.Caption := Format('%.2n', [0.0]);
  if TotalBUWorkedHrs > 0 then
    QRLabelTotalBUPiecesPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.0f', [TotalBUPieces / (TotalBUWorkedHrs / 60)])
      Format('%.0n', [TotalBUPieces / (TotalBUWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUPiecesPerHr.Caption := Format('%.0f', [0.0]);
    QRLabelTotalBUPiecesPerHr.Caption := Format('%.0n', [0.0]);

  //
  // MR:22-7-2002
  // Now get information about revenues for this Business Unit
  //

  // Init the variables
  TotalBUSales := 0;
  TotalBUPayrollToSales := 0;
  TotalBUIncomePerWorkedHour := 0;
  TotalBUMarginalRatePerHour := 0;

  // Now get information about sales
  with ReportProductionDM do
  begin
    QueryRevenue.Close;
    QueryRevenue.ParamByName('BUSINESSUNIT_CODE').Value :=
      QueryProduction.FieldByName('BUSINESSUNIT_CODE').Value;
    QueryRevenue.ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
    QueryRevenue.ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo);
    QueryRevenue.Open;
    if QueryRevenue.RecordCount > 0 then
    begin
      QueryRevenue.First;
      try
        // Revenue - calculations
        (* Column 8 *)
        TotalBUSales := QueryRevenue.FieldByName('REVENUESUM').AsFloat;
      except
        TotalBUSales := 0;
      end;
    end;
    QueryRevenue.Close;
  end;

  // Now calculate other values

  (* Column 9 *)
  if TotalBUSales > 0 then
    TotalBUPayrollToSales := TotalBUPayrollDollars / TotalBUSales * 100;
  (* Column 10 *)
  if (TotalBUSales > 0) and (TotalBUWorkedHrs > 0) then
    TotalBUIncomePerWorkedHour := TotalBUSales / (TotalBUWorkedHrs / 60);
  (* Column 11 *)
  if (TotalBUIncomePerWorkedHour > 0) and (TotalBUWorkedHrs > 0) then
    TotalBUMarginalRatePerHour := TotalBUIncomePerWorkedHour
      - (TotalBUPayrollDollars / (TotalBUWorkedHrs / 60));

      // Now put them to labels in the report
  QRLabelTotalBUSales.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUSales]);
    Format('%.2n', [TotalBUSales]);
  QRLabelTotalBUPercPayrollToSales.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.0f', [TotalBUPayrollToSales]);
    Format('%.0n', [TotalBUPayrollToSales]);
  QRLabelTotalBUIncomePerWorkedHour.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUIncomePerWorkedHour]);
    Format('%.2n', [TotalBUIncomePerWorkedHour]);
  QRLabelTotalBUMarginalRatePerHour.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUMarginalRatePerHour]);
    Format('%.2n', [TotalBUMarginalRatePerHour]);

  //[ROP] 20013965
  if not QRParameters.FShowPayrollInformation then
    begin
      QRLabelTotalBUPayrollDollars.Caption := '';
      QRLabelTotalBUCostsPerPiece.Caption := '';
      QRLabelTotalBUCostsPerHr.Caption := '';
      QRLabelTotalBUSales.Caption := '';
      QRLabelTotalBUPercPayrollToSales.Caption := '';
      QRLabelTotalBUIncomePerWorkedHour.Caption := '';
      QRLabelTotalBUMarginalRatePerHour.Caption := '';
    end;
  // [ROP] end 20013965
end;

procedure TReportProductionQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  LastPlantCode :=
    ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString;
// ROP 05-MAR-2013 TD-21527  QRLabelTotalPlantPieces.Caption := Format('%.0f', [TotalPlantPieces]);
  QRLabelTotalPlantPieces.Caption := Format('%.0n', [TotalPlantPieces]);
  QRLabelTotalPlantWorkedHrs.Caption := DecodeHrsMin(TotalPlantWorkedHrs, True);
  QRLabelTotalPlantSalHrs.Caption := DecodeHrsSalMin(TotalPlantSalHrs);
  QRLabelTotalPlantDiffHrs.Caption := DecodeHrsDiffMin(TotalPlantDiffHrs);
  QRLabelTotalPlantOvertimeHrs.Caption := DecodeHrsMin(TotalPlantOvertimeHrs, True);
  QRLabelTotalPlantPayrollDollars.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalPlantPayrollDollars]);
    Format('%.2n', [TotalPlantPayrollDollars]);
  if TotalPlantPieces > 0 then
    QRLabelTotalPlantCostsPerPiece.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.3f', [TotalPlantPayrollDollars / TotalPlantPieces])
      Format('%.3n', [TotalPlantPayrollDollars / TotalPlantPieces])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalPlantCostsPerPiece.Caption := Format('%.3f', [0.0]);
    QRLabelTotalPlantCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalPlantWorkedHrs > 0 then
    QRLabelTotalPlantCostsPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [TotalPlantPayrollDollars / (TotalPlantWorkedHrs / 60)])
      Format('%.2n', [TotalPlantPayrollDollars / (TotalPlantWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalPlantCostsPerHr.Caption := Format('%.2f', [0.0]);
    QRLabelTotalPlantCostsPerHr.Caption := Format('%.2n', [0.0]);
  if TotalPlantWorkedHrs > 0 then
    QRLabelTotalPlantPiecesPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.0f', [TotalPlantPieces / (TotalPlantWorkedHrs / 60)])
      Format('%.0n', [TotalPlantPieces / (TotalPlantWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalPlantPiecesPerHr.Caption := Format('%.0f', [0.0]);
    QRLabelTotalPlantPiecesPerHr.Caption := Format('%.0n', [0.0]);
  //[ROP] 20013965
  if not QRParameters.FShowPayrollInformation then
    begin
      QRLabelTotalPlantPayrollDollars.Caption := '';
      QRLabelTotalPlantCostsPerPiece.Caption := '';
      QRLabelTotalPlantCostsPerHr.Caption := '';
    end;
  // [ROP] end 20013965
end;

procedure TReportProductionQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TotalDeptPieces := 0;
  TotalDeptWorkedHrs := 0;
  TotalDeptSalHrs := 0;
  TotalDeptDiffHrs := 0;
  TotalDeptOvertimeHrs := 0;
  TotalDeptPayrollDollars := 0;
  LastDeptCode := '';
  if QRParameters.FShowJobs and QRParameters.FShowOnlyJobs then
    LastWorkspotCode := '';
end;

procedure TReportProductionQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  LastDeptCode :=
    ReportProductionDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString;
  if QRParameters.FShowJobs and QRParameters.FShowOnlyJobs then
    LastWorkspotCode :=
      ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
// ROP 05-MAR-2013 TD-21527  QRLabelTotalDeptPieces.Caption := Format('%.0f', [TotalDeptPieces]);
  QRLabelTotalDeptPieces.Caption := Format('%.0n', [TotalDeptPieces]);
  QRLabelTotalDeptWorkedHrs.Caption := DecodeHrsMin(TotalDeptWorkedHrs, True);
  QRLabelTotalDeptSalHrs.Caption := DecodeHrsSalMin(TotalDeptSalHrs);
  QRLabelTotalDeptDiffHrs.Caption := DecodeHrsDiffMin(TotalDeptDiffHrs);
  QRLabelTotalDeptOvertimeHrs.Caption := DecodeHrsMin(TOtalDeptOvertimeHrs, True);
  QRLabelTotalDeptPayrollDollars.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalDeptPayrollDollars]);
    Format('%.2n', [TotalDeptPayrollDollars]);
  if TotalDeptPieces > 0 then
    QRLabelTotalDeptCostsPerPiece.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.3f', [TotalDeptPayrollDollars / TotalDeptPieces])
      Format('%.3n', [TotalDeptPayrollDollars / TotalDeptPieces])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalDeptCostsPerPiece.Caption := Format('%.3f', [0.0]);
    QRLabelTotalDeptCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalDeptWorkedHrs > 0 then
    QRLabelTotalDeptCostsPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [TotalDeptPayrollDollars / (TotalDeptWorkedHrs / 60)])
      Format('%.2n', [TotalDeptPayrollDollars / (TotalDeptWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalDeptCostsPerHr.Caption := Format('%.2f', [0.0]);
    QRLabelTotalDeptCostsPerHr.Caption := Format('%.2n', [0.0]);
  if TotalDeptWorkedHrs > 0 then
    QRLabelTotalDeptPiecesPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.0f', [TotalDeptPieces / (TotalDeptWorkedHrs / 60)])
      Format('%.0n', [TotalDeptPieces / (TotalDeptWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalDeptPiecesPerHr.Caption := Format('%.0f', [0.0]);
    QRLabelTotalDeptPiecesPerHr.Caption := Format('%.0n', [0.0]);

  //[ROP] 20013965
  if not QRParameters.FShowPayrollInformation then
    begin
      QRLabelTotalDeptPayrollDollars.Caption := '';
      QRLabelTotalDeptCostsPerPiece.Caption := '';
      QRLabelTotalDeptCostsPerHr.Caption := '';
    end;
  // [ROP] end 20013965
end;

procedure TReportProductionQR.AddAmounts(Amount: Integer; BU, Dept, WK, Job,
  Empl: Boolean);
begin
  if BU then
  begin
//    TotalBU := TotalBU + Amount;
    Inc(FDaysBU);
  end;
  if Dept then
  begin
//    TotalDept:= TotalDept + Amount;
    Inc(FDaysDept);
  end;
  if WK then
  begin
//    TotalWK := TotalWK + Amount;
    Inc(FDaysWK);
  end;
  if Job then
  begin
//    TotalJob := TotalJob + Amount;
    Inc(FDaysJob);
  end;
  if Empl then
  begin
//    TotalEmpl := TotalEmpl + Amount;
    Inc(FDaysEmpl);
  end;
end;

procedure TReportProductionQR.QRBandDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
//  JobField: String;
  IncludeJob: Boolean;
  EmpPieces: Double; // 20015220.110
begin
  inherited;
  // 20015586
  IncludeJob := (ReportProductionDM.QueryProduction.
    FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y');
  if (ReportProductionDM.
    QueryProduction.FieldByName('JOB_CODE').AsString = DOWNJOB_1) or
    (ReportProductionDM.
    QueryProduction.FieldByName('JOB_CODE').AsString = DOWNJOB_2) then
  begin
    IncludeJob := QRParameters.FIncludeDownTime <> DOWNTIME_NO;
  end;
  // 20013478.
  if IncludeJob then
  begin
    EmpPieces :=
      ReportProductionDM.QueryProduction.FieldByName('EMPLOYEEQUANTITY').AsFloat;

    TotalDetailPieces := TotalDetailPieces + EmpPieces;
    // 20014002
    TotalJobPieces := TotalJobPieces + EmpPieces;
    TotalPlantPieces := TotalPlantPieces + EmpPieces;
    TotalBUPieces := TotalBUPieces + EmpPieces;
    TotalDeptPieces := TotalDeptPieces + EmpPieces;
  end;

  DetermineWorkedHours;

{
  // 20014002
  with ReportProductionDM do
  begin
    if (QRParameters.FShowJobs and QRParameters.FShowOnlyJobs) then
    begin
      if QRParameters.FSortOnJob = 0 then // Code
        JobField := 'JOB_CODE'
      else
        JobField := 'JOBDESCRIPTION';
      // Do this only when this combination is changing.
      // Reason: The query does not give unique combinations.
      if not
      (
        (LastPlantCode = QueryProduction.FieldByName('PLANT_CODE').AsString) and
        (LastBUCode = QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString) and
        (LastDeptCode = QueryProduction.FieldByName('DEPARTMENT_CODE').AsString) and
        (LastWorkspotCode = QueryProduction.FieldByName('WORKSPOT_CODE').AsString) and
        (LastJob = QueryProduction.FieldByName(JobField).AsString)
      ) then
        DetermineWorkedHours;
      LastPlantCode := QueryProduction.FieldByName('PLANT_CODE').AsString;
      LastBUCode := QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString;
      LastDeptCode := QueryProduction.FieldByName('DEPARTMENT_CODE').AsString;
      LastWorkspotCode := QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
      LastJob := QueryProduction.FieldByName(JobField).AsString;
    end;
  end;
}
  PrintBand := False;
end; // QRBandDetailBeforePrint

procedure TReportProductionQR.QRDBTextDescDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20014002 This does not fit, so leave it out.
  Value := '';
//  Value := Copy(Value,0,20);
end;

procedure TReportProductionQR.QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  FDaysBU := 0;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
    // [ROP] 20013965
      if not QRParameters.FShowPayrollInformation then
      ExportClass.AddText(
        QRLabelTotBU.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + ExportClass.Sep +
        QRLabelTotalBUPieces.Caption + ExportClass.Sep +
        QRLabelTotalBUWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUPiecesPerHr.Caption
        )
      else
    // [ROP] 20013965 end
      ExportClass.AddText(
        QRLabelTotBU.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + ExportClass.Sep +
        QRLabelTotalBUPieces.Caption + ExportClass.Sep +
        QRLabelTotalBUWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalBUCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalBUCostsPerHr.Caption + ExportClass.Sep +
        QRLabelTotalBUPiecesPerHr.Caption + ExportClass.Sep +
        QRLabelTotalBUSales.Caption + ExportClass.Sep +
        QRLabelTotalBUPercPayrollToSales.Caption + ExportClass.Sep +
        QRLabelTotalBUIncomePerWorkedHour.Caption + ExportClass.Sep +
        QRLabelTotalBUMarginalRatePerHour.Caption
        );
    end;
  end;
end; // QRBandFooterBUAfterPrint

procedure TReportProductionQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  FDaysDept := 0;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
    // [ROP] 20013965
      if not QRParameters.FShowPayrollInformation then
      ExportClass.AddText(QRLabelTotDept.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
        // 20013965
//        QueryProduction.FieldByname('DEPARTMENTDESCRIPTION').AsString +
        ExportClass.Sep +
        QRLabelTotalDeptPieces.Caption + ExportClass.Sep +
        QRLabelTotalDeptWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptSalhrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptPiecesPerHr.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep)
      else
    // [ROP] 20013965 end
      ExportClass.AddText(QRLabelTotDept.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
//        QueryProduction.FieldByname('DEPARTMENTDESCRIPTION').AsString +
        ExportClass.Sep +
        QRLabelTotalDeptPieces.Caption + ExportClass.Sep +
        QRLabelTotalDeptWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalDeptCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalDeptCostsPerHr.Caption + ExportClass.Sep +
        QRLabelTotalDeptPiecesPerHr.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep);
    end;
  end;
end; // QRBandFooterDEPTAfterPrint

procedure TReportProductionQR.QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
var
  Workspot: String;
begin
  inherited;
  FDaysWK := 0;
  WKHasJobDummy := False;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      // 20013965
      if QRParameters.FShowJobs and (not QRParameters.FShowOnlyJobs) then
        Workspot := SPimsTotalWorkspot + ' ' +
          QueryProduction.FieldByName('WORKSPOT_CODE').AsString
      else
        Workspot := QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
    // [ROP] 20013965
      if not QRParameters.FShowPayrollInformation then
        ExportClass.AddText(
          // 20014002
          // 20013965
          // QueryProduction.FieldByName('WORKSPOT_CODE').AsString +
          // ' ' + // ExportClass.Sep +
          //  QueryProduction.FieldByName('WORKSPOTDESCRIPTION').AsString +
          Workspot +
          ExportClass.Sep +
          QRLabelWKPieces.Caption + ExportClass.Sep +
          QRLabelWKWorkedHrs.Caption + ExportClass.Sep +
          QRLabelWKSalHrs.Caption + ExportClass.Sep +
          QRLabelWKDiffHrs.Caption + ExportClass.Sep +
          QRLabelWKOvertimeHrs.Caption + ExportClass.Sep +
          QRLabelWKPiecesPerHr.Caption + ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep)
      else
    // [ROP] 20013965 end
        ExportClass.AddText(
          // 20014002
          // QueryProduction.FieldByName('WORKSPOT_CODE').AsString +
          // ' ' + // ExportClass.Sep +
          // QueryProduction.FieldByName('WORKSPOTDESCRIPTION').AsString +
          Workspot +
          ExportClass.Sep +
          QRLabelWKPieces.Caption + ExportClass.Sep +
          QRLabelWKWorkedHrs.Caption + ExportClass.Sep +
          QRLabelWKSalHrs.Caption + ExportClass.Sep +
          QRLabelWKDiffHrs.Caption + ExportClass.Sep +
          QRLabelWKOvertimeHrs.Caption + ExportClass.Sep +
          QRLabelWKPayrollDollars.Caption + ExportClass.Sep +
          QRLabelWKCostsPerPiece.Caption + ExportClass.Sep +
          QRLabelWKCostsPerHr.Caption + ExportClass.Sep +
          QRLabelWKPiecesPerHr.Caption + ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep);
    end;
  end;
end; // QRBandFooterWKAfterPrint

procedure TReportProductionQR.QRBandFooterJOBAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  FDaysJob := 0;
  // 20014002
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
    // [ROP] 20013965
      if not QRParameters.FShowPayrollInformation then
      ExportClass.AddText(
        QRLabelJobCode.Caption + ' ' + QRLabelJobDesc.Caption + ExportClass.Sep +
        QRLabelJobPieces.Caption  + ExportClass.Sep +
        QRLabelJobWorkedHrs.Caption + ExportClass.Sep +
        QRLabelJobSalHrs.Caption + ExportClass.Sep +
        QRLabelJobDiffHrs.Caption + ExportClass.Sep +
        QRlabelJobOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelJobPiecesPerHr.Caption
      )
      else
    // [ROP] 20013965 end
      ExportClass.AddText(
        //ExportClass.Sep +
        //ExportClass.Sep +
        // 20014002 Jobcode + description must be shown in 1 column
        QRLabelJobCode.Caption + ' ' + // ExportClass.Sep +
        QRLabelJobDesc.Caption + ExportClass.Sep +
        QRLabelJobPieces.Caption  + ExportClass.Sep +
        QRLabelJobWorkedHrs.Caption + ExportClass.Sep +
        QRLabelJobSalHrs.Caption + ExportClass.Sep +
        QRLabelJobDiffHrs.Caption + ExportClass.Sep +
        QRlabelJobOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelJobPayrollDollars.Caption + ExportClass.Sep +
        QRLabelJobCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelJobCostsPerHr.Caption + ExportClass.Sep +
        QRLabelJobPiecesPerHr.Caption
      );
    end;
  end;
end;

procedure TReportProductionQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  FDaysEmpl := 0;
end;
(*
function TReportProductionQR.DetermineTimeRegScanningMinutes: Integer;
var
  EmployeeData: TScannedIDCard;
  Minutes, BreaksMin, PayedBreaks: Integer;
  APTimeRegRecord: PTTimeRegRecord;
  BookingDay: TDateTime;
begin
  Result := 0;
  with ReportProductionDM do
  begin
    QueryTimeRegScanning.Close;
    QueryTimeRegScanning.ParamByName('EMPLOYEE_NUMBER').Value :=
      QueryProdHourPerEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
    // 20014327 Compare dates depends on system-setting.
    //          To Date changed to < instead of <=
    if SystemDM.UseShiftDateSystem then
    begin
      // 20014327
      QueryTimeRegScanning.ParamByName('ONSHIFT').AsInteger := 1;
      // 20013489 Filter on SHIFT_DATE
      QueryTimeRegScanning.ParamByName('DATEFROM').AsDateTime :=
        Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime);
      QueryTimeRegScanning.ParamByName('DATETO').AsDateTime :=
        Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime)
          + 1;
    end
    else
    begin
      // 20014327
      QueryTimeRegScanning.ParamByName('ONSHIFT').AsInteger := 0;
      // 20014327 Filter on scan-dates (DATETIME_IN and DATETIME_OUT)
      QueryTimeRegScanning.ParamByName('DATEFROM').AsDateTime :=
        Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime)
          - 1;
      QueryTimeRegScanning.ParamByName('DATETO').AsDateTime :=
        Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime)
          + 1;
    end;
{
    QueryTimeRegScanning.ParamByName('DATETIME_IN').AsDateTime :=
      Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime)
        - 1;
    QueryTimeRegScanning.ParamByName('DATETIME_OUT').AsDateTime :=
      Trunc(QuerySalaryHourPerEmployee.FieldByName('SALARY_DATE').AsDateTime)
        + 1;
}
    QueryTimeRegScanning.Open;
    if QueryTimeRegScanning.RecordCount > 0 then
    begin
      QueryTimeRegScanning.First;
      while not QueryTimeRegScanning.Eof do
      begin
        EmptyIDCard(EmployeeData);
        // MR:30-07-2004 Use 'GetProdMin' instead of 'ComputeBreaks'.
        GlobalDM.PopulateIDCard(EmployeeData, QueryTimeRegScanning);
        // MR:22-07-2004
        // Use shift of TimeRecScanning instead of ProductionQuantity
        // Check if SHIFT_NUMBER is '0'? It should be '1'
        if EmployeeData.ShiftNumber = 0 then
          EmployeeData.ShiftNumber := 1;
        AProdMinClass.GetProdMin(EmployeeData,
          False, False,
          False, EmployeeData,
          BookingDay,
          Minutes, BreaksMin, PayedBreaks);
        // 20013489 Filter on SHIFT_DATE
        // 20014327 Or on DATETIME_IN
        if (SystemDM.UseShiftDateSystem and (Trunc(QueryTimeRegScanning.
            FieldByName('SHIFT_DATE').AsDateTime) = BookingDay))
            or
            ((not SystemDM.UseShiftDateSystem) and (Trunc(QueryTimeRegScanning.
            FieldByName('DATETIME_IN').AsDateTime) = BookingDay))
            then
{        if Trunc(QueryTimeRegScanning.
            FieldByName('DATETIME_IN').AsDateTime) = BookingDay then }
        begin
          // Store TimeReg-fields in a list for later use
          new(APTimeRegRecord);
          TimeRegList.Add(APTimeRegRecord);
          APTimeRegRecord.WorkspotCode :=
            QueryTimeRegScanning.FieldByName('WORKSPOT_CODE').AsString;
          APTimeRegRecord.JobCode :=
            QueryTimeRegScanning.FieldByName('JOB_CODE').AsString;
          APTimeRegRecord.SalMinute := Minutes + PayedBreaks;
        end;
        QueryTimeRegScanning.Next;
      end;
    end;
    QueryTimeRegScanning.Close;
  end;
end;
*)

(*
// MR:29-07-2004
// This function has been rewritten to get the correct result.
// PIM-135 This function is not used anymore
function TReportProductionQR.DetermineOvertimeXXX: Integer;
//var
//  I, J: Integer;
//  APSalaryRecord: PTSalaryRecord;
//  APTimeRegRecord: PTTimeRegRecord;
//  SalaryMinuteLeft: Integer;
  // Result-Vars
//  OvertimeMinute: Integer;
//  BonusInMoney: Boolean;
begin
  // 20014450.50 New way of determining overtime
  // PIM-137
//  OvertimeMinute := 0;

{
  with ReportProductionDetailDM do
  begin
    with qryPHEPT do
    begin
      Filtered := False;
      Filter :=
        'PLANT_CODE=' +
        MyQuotes(ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString) +
        ' AND WORKSPOT_CODE=' +
        MyQuotes(ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString) +
        ' AND JOB_CODE=' +
        MyQuotes(ReportProductionDM.QueryProduction.FieldByName('JOB_CODE').AsString) +
        ' AND EMPLOYEE_NUMBER=' +
        ReportProductionDM.QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsString +
        ' AND DEPARTMENT_CODE=' +
        ReportProductionDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString;
      Filtered := True;
      if not Eof then
      begin
        // PIMS-12 MRA:22-DEC-2015 Bugfix: Do not look for overtimemins!
{
        if FieldByName('OVERTIME_YN').AsString = 'Y' then
          OvertimeMinute := Round(
            FieldByName('OVERTIMEMINS').AsFloat);
}
{        HourlyWage :=
          FieldByName('HOURLY_WAGE').AsFloat;
        BonusPercentage :=
          FieldByName('BONUS_PERCENTAGE').AsFloat;
        if ContractGroupTFT then
          BonusInMoney :=
            FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y'
        else
          BonusInMoney :=
            FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y'; }
      end;
    end;
  end;
}

{
  with ReportProductionDM do
  begin
    OvertimeMinute := 0;
    for I := 0 to SalaryList.Count - 1 do
    begin
      APSalaryRecord := SalaryList.Items[I];
      SalaryMinuteLeft := APSalaryRecord.SalaryMinute;
      for J := 0 to TimeRegList.Count - 1 do
      begin
        APTimeRegRecord := TimeRegList.Items[J];
        if APTimeRegRecord.SalMinute <= SalaryMinuteLeft then
        begin
          if (APTimeRegRecord.WorkspotCode = CurrentWorkspotCode) and
            (APTimeRegRecord.JobCode = CurrentJobCode) then
            OvertimeMinute := OvertimeMinute + APTimeRegRecord.SalMinute;
          SalaryMinuteLeft := SalaryMinuteLeft - APTimeRegRecord.SalMinute;
          APTimeRegRecord.SalMinute := 0;
        end // if APTimeRegRecord.SalMinute <= SalaryMinuteLeft then
        else
        begin
          if (APTimeRegRecord.WorkspotCode = CurrentWorkspotCode) and
            (APTimeRegRecord.JobCode = CurrentJobCode) then
            OvertimeMinute := OvertimeMinute + SalaryMinuteLeft;
          APTimeRegRecord.SalMinute :=
            APTimeRegRecord.SalMinute - SalaryMinuteleft;
          // Add Bonus to Payroll, if needed
          if (OvertimeMinute > 0) and (APSalaryRecord.BonusPercentage > 0) then
          begin
            if TableEmpl.FindKey([EmployeeNumber]) then
            begin
              if TableContractgroup.FindKey(
                [TableEmpl.FieldByName('CONTRACTGROUP_CODE').Value]) then
              begin
                // RV045.1.
                if ReportProductionDM.ContractGroupTFT then
                  BonusInMoney :=
                    (TableContractgroup.
                      FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y')
                else
                  BonusInMoney := APSalaryRecord.HTBonusInMoney;
                if BonusInMoney then
                  TotalJobPayrollDollars := TotalJobPayrollDollars +
                    (HourlyWage * APSalaryRecord.BonusPercentage / 100 *
                      (OvertimeMinute / 60));
              end;
            end;
          end;
          Break;
        end; // else if APTimeRegRecord.SalMinute <= SalaryMinuteLeft then
      end; // for J := 0 to TimeRegList.Count - 1 do
    end; // for I := 0 to SalaryList.Count - 1 do
  end;
}
  Result := 0; //OvertimeMinute;
end; // DetermineOvertimeXXX
*)

procedure TReportProductionQR.QRGroupHDJOBBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
//var
//  HourlyWage: Double;
//  BonusPercentage: Double;
//  BonusInMoney: Boolean;
//  EmplWorkedHrs: Integer;
//  OvertimeMinute: Integer;
{
var
  EmployeeNumber: Integer;
  APSalaryRecord: PTSalaryRecord;
  OvertimeMinute: Integer;
  PayedBreakMinute: Integer;
  procedure TimeRegListFree;
  var
    I: Integer;
    APTimeRegRecord: PTTimeRegRecord;
  begin
    for I:=0 to TimeRegList.Count-1 do
    begin
      APTimeRegRecord := TimeRegList.Items[I];
      Dispose(APTimeRegRecord);
    end;
    TimeRegList.Free;
  end;
  procedure SalaryListFree;
  var
    I: Integer;
    APSalaryRecord: PTSalaryRecord;
  begin
    for I:=0 to SalaryList.Count-1 do
    begin
      APSalaryRecord := SalaryList.Items[I];
      Dispose(APSalaryRecord);
    end;
    SalaryList.Free;
  end; }
begin
  inherited;
  LastJob := '';
  PrintBand := False; // QRParameters.FShowJobs; // 20014002
  
//  HourlyWage := 0;
//  BonusPercentage := 0;

  TotalJobPieces := 0; // 20014002
  TotalJobWorkedHrs := 0; // 20014002
  TotalJobSalHrs := 0;
  TotalJobDiffHrs := 0;
  TotalJobPayrollDollars := 0; // 20014002
  TotalJobOvertimeHrs := 0;

  // 20014002
  with ReportProductionDM do
  begin
    if QRParameters.FShowJobs then
    begin
//      if QRParameters.FSortOnJob = 0 then // Code
      begin
        QRLabelJobCode.Caption :=
          QueryProduction.FieldByName('JOB_CODE').AsString;
        QRLabelJobDesc.Caption :=
          QueryProduction.FieldByName('JDESCRIPTION').AsString;
      end
{      else // Name
      begin
        QRLabelJobCode.Caption :=
          QueryProduction.FieldByName('JOBDESCRIPTION').AsString;
        QRLabelJobDesc.Caption :=
          QueryProduction.FieldByName('JOB_CODE').AsString;
      end;
}
    end;
  end;

//  if not (QRParameters.FShowJobs and QRParameters.FShowOnlyJobs) then
//    DetermineWorkedHours;

  // PIM-135
  // Do this on a different level, because we need to know info about
  // all employee worked on the job.
  // 20014450.50
//  TotalJobOvertimeHrs := TotalJobOvertimeHrs + DetermineOvertime;
{
  OvertimeMinute :=
    ReportProductionDetailDM.DetermineOvertime(
      ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('JOB_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      ReportProductionDetailDM.ContractGroupTFT,
      BonusInMoney);
  TotalJobOvertimeHrs := TotalJobOvertimeHrs + OvertimeMinute;

  if ReportProductionDM.QueryProduction.FieldByName('HOURLY_WAGE').AsString <> '' then
    HourlyWage := ReportProductionDM.QueryProduction.FieldByName('HOURLY_WAGE').AsFloat;
  if ReportProductionDM.QueryProduction.FieldByName('BONUS_FACTOR').AsString <> '' then
    BonusPercentage := ReportProductionDM.QueryProduction.FieldByName('BONUS_FACTOR').AsFloat;
  EmplWorkedHrs :=
    Round(ReportProductionDM.QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60); // Convert to minutes!

  // Get Payroll
  if EmplWorkedHrs > 0 then
    TotalJobPayrollDollars := TotalJobPayrollDollars +
     (HourlyWage * (EmplWorkedHrs / 60));

  // Overtime and Bonus
  if (OvertimeMinute > 0) and (BonusPercentage > 0) and BonusInMoney then
     TotalJobPayrollDollars := TotalJobPayrollDollars +
      (HourlyWage * BonusPercentage / 100 *
        (OvertimeMinute / 60));
}


{

  with ReportProductionDM do
  begin
    QueryProdHourPerEmployee.Close;
    QueryProdHourPerEmployee.ParamByName('PLANT_CODE').Value :=
      QueryProduction.FieldByName('PLANT_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('WORKSPOT_CODE').Value :=
      QueryProduction.FieldByName('WORKSPOT_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('JOB_CODE').Value :=
      QueryProduction.FieldByName('JOB_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('DATEFROM').AsDate :=
      Trunc(QRParameters.FDateFrom);
    QueryProdHourPerEmployee.ParamByName('DATETO').AsDate :=
      Trunc(QRParameters.FDateTo);
    QueryProdHourPerEmployee.Open;
    if QueryProdHourPerEmployee.RecordCount > 0 then
    begin
      QueryProdHourPerEmployee.First;
      while not QueryProdHourPerEmployee.Eof do
      begin
//        TotalWorkspotWorkedHrs := TotalWorkspotWorkedHrs +
//          QueryProdHourPerEmployee.FieldByName('PRODUCTION_MINUTE').Value;
//        TotalJobWorkedHrs := TotalJobWorkedHrs +
//          QueryProdHourPerEmployee.FieldByName('PRODUCTION_MINUTE').Value;
        EmployeeNumber :=
          QueryProdHourPerEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
        // Get Payroll
        if TableEmployeeContract.FindKey([EmployeeNumber]) then
        begin
          HourlyWage :=
            TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat;
          // MR:8-12-2004 Payed_Break_Minute can be NULL!
          if QueryProdHourPerEmployee.
            FieldByName('PAYED_BREAK_MINUTE').AsString = '' then
            PayedBreakMinute := 0
          else
            PayedBreakMinute :=
              QueryProdHourPerEmployee.
                FieldByName('PAYED_BREAK_MINUTE').AsInteger;
          TotalJobPayrollDollars := TotalJobPayrollDollars +
            (TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat *
            ((
              QueryProdHourPerEmployee.
                FieldByName('PRODUCTION_MINUTE').AsInteger +
              PayedBreakMinute) / 60));
        end;
        // Get OverTime
        QuerySalaryHourPerEmployee.Close;
        QuerySalaryHourPerEmployee.ParamByName('EMPLOYEE_NUMBER').Value :=
          EmployeeNumber;
        QuerySalaryHourPerEmployee.ParamByName('DATEFROM').AsDate :=
          QueryProdHourPerEmployee.
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        QuerySalaryHourPerEmployee.ParamByName('DATETO').AsDate :=
          QueryProdHourPerEmployee.
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        QuerySalaryHourPerEmployee.Open;
        SalaryList := TList.Create;
        if QuerySalaryHourPerEmployee.RecordCount > 0 then
        begin
          QuerySalaryHourPerEmployee.First;
          while not QuerySalaryHourPerEmployee.Eof do
          begin
            // Store SalaryMinute in a list for later use
            new(APSalaryRecord);
            SalaryList.Add(APSalaryRecord);
            APSalaryRecord.SalaryMinute := QuerySalaryHourPerEmployee.
              FieldByName('SALARY_MINUTE').AsInteger;
            APSalaryRecord.BonusPercentage := QuerySalaryHourPerEmployee.
              FieldByName('BONUS_PERCENTAGE').AsFloat;
            // RV045.1.
            APSalaryRecord.HTBonusInMoney := (QuerySalaryHourPerEmployee.
              FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y');

            QuerySalaryHourPerEmployee.Next;
          end;
//          TimeRegList := TList.Create;
//          DetermineTimeRegScanningMinutes;
          OvertimeMinute :=
            DetermineOvertime(
              QueryProduction.FieldByName('WORKSPOT_CODE').Value,
              QueryProduction.FieldByName('JOB_CODE').Value,
              HourlyWage, EmployeeNumber);
          TotalJobOvertimeHrs := TotalJobOvertimeHrs + OvertimeMinute;
//          TimeRegListFree;
        end;  // if QuerySalaryHourPerEmployee.RecordCount > 0 then
        QuerySalaryHourPerEmployee.Close;
        SalaryListFree;
        QueryProdHourPerEmployee.Next;
      end;
    end;
    QueryProdHourPerEmployee.Close;
  end;
}
end; // QRGroupHDJOBBeforePrint

procedure TReportProductionQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProductionQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportProductionQR.QRGroupHdPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      ExportClass.AddText(QRLabel54.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('PLANT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('PDESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportProductionQR.QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      ExportClass.AddText(QRLabel55.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('BDESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportProductionQR.QRGroupHDDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      ExportClass.AddText(QRLabel35.Caption + ' ' +  // ExportClass.Sep +
        QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('DDESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportProductionQR.QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      ExportClass.AddText(QRLabel3.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('WORKSPOT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('WDESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportProductionQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDM do
    begin
      // [ROP] 20013965
      if not QRParameters.FShowPayrollInformation then
        ExportClass.AddText(QRLabelTotPlant.Caption + ' ' + // ExportClass.Sep +
          QueryProduction.FieldByName('PLANT_CODE').AsString + ' ' +
          // 20013965
//          TablePlant.FieldByName('DESCRIPTION').AsString +
          ExportClass.Sep +
          QRLabelTotalPlantPieces.Caption + ExportClass.Sep +
          QRLabelTotalPlantWorkedHrs.Caption + ExportClass.Sep +
          QRLabelTotalPlantSalHrs.Caption + ExportClass.Sep +
          QRLabelTotalPlantDiffHrs.Caption + ExportClass.Sep +
          QRLabelTotalPlantOvertimeHrs.Caption + ExportClass.Sep +
          QRLabelTotalPlantPiecesPerHr.Caption + ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep)
      else
      // [ROP] 20013965 end
      ExportClass.AddText(QRLabelTotPlant.Caption + ' ' + // ExportClass.Sep +
        QueryProduction.FieldByName('PLANT_CODE').AsString + ' ' +
        // 20013965
//        TablePlant.FieldByName('DESCRIPTION').AsString +
        ExportClass.Sep +
        QRLabelTotalPlantPieces.Caption + ExportClass.Sep +
        QRLabelTotalPlantWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalPlantCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalPlantCostsPerHr.Caption + ExportClass.Sep +
        QRLabelTotalPlantPiecesPerHr.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep);
    end;
  end;
end;

procedure TReportProductionQR.QRDBText11Print(sender: TObject;
  var Value: String);
begin
  inherited;
  // Plant-description
  // 20014002 Made smaller (20 -> 18)
//  Value := Copy(Value, 1, 18 {20} {25} ); // RV102.4. Also made shorter
  // 20014002 Do not show this at all, to make more room for pieces.
  Value := '';
end;

// RV102.4. Workspot
procedure TReportProductionQR.QRDBText9Print(sender: TObject;
  var Value: String);
begin
  inherited;
//  Value := Copy(Value, 1, 21);
  // 20014002
  if QRParameters.FShowJobs and (not QRParameters.FShowOnlyJobs) then
    Value := ''
  else
    Value := Copy(Value, 1, 30); // RV104.2. Font is made somewhat smaller.
end;

procedure TReportProductionQR.QRBandFooterJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  JobField: String;
begin
  inherited;
  if QRParameters.FSortOnJob = 0 then // Code
    JobField := 'JOB_CODE'
  else
    JobField := 'JDESCRIPTION';
  LastJob :=
    ReportProductionDM.QueryProduction.FieldByName(JobField).AsString;
  PrintBand := QRParameters.FShowJobs; // 20014002
  // 20014002
  if PrintBand then
  begin
    // TD-21527
    // QRLabelJobPieces.Caption := Format('%.0f', [TotalJobPieces]);
    QRLabelJobPieces.Caption := Format('%.0n', [TotalJobPieces]);
    QRLabelJobWorkedHrs.Caption := DecodeHrsMin(TotalJobWorkedHrs, True);
    QRLabelJobSalHrs.Caption := DecodeHrsSalMin(TotalJobSalHrs);
    QRLabelJobDiffHrs.Caption := DecodeHrsDiffMin(TotalJobDiffHrs);
    QRLabelJobOvertimeHrs.Caption := DecodeHrsMin(TotalJobOvertimeHrs, True);

    // Payroll
    // TD-21527
    // QRLabelJobPayrollDollars.Caption := Format('%.2f', [TotalJobPayrollDollars]);
    QRLabelJobPayrollDollars.Caption := Format('%.2n', [TotalJobPayrollDollars]);
    // Costs per Piece
    if TotalJobPieces > 0 then
      QRLabelJobCostsPerPiece.Caption :=
        // TD-21527
        // Format('%.3f', [TotalJobPayrollDollars / TotalJobPieces])
        Format('%.3n', [TotalJobPayrollDollars / TotalJobPieces])
    else
      QRLabelJobCostsPerPiece.Caption := Format('%.3f', [0.0]);
    // Costs per Hour
    if TotalJobWorkedHrs > 0 then
      QRLabelJobCostsPerHr.Caption :=
        // TD-21527
        // Format('%.2f', [TotalJobPayrollDollars / (TotalJobWorkedHrs / 60)])
        Format('%.2n', [TotalJobPayrollDollars / (TotalJobWorkedHrs / 60)])
    else
      QRLabelJobCostsPerHr.Caption := Format('%.2f', [0.0]);
    // Pieces per Hour
    if TotalJobWorkedHrs > 0 then
      QRLabelJobPiecesPerHr.Caption :=
        // TD-21527
        // Format('%.0f', [TotalJobPieces / (TotalJobWorkedHrs / 60)])
        Format('%.0n', [TotalJobPieces / (TotalJobWorkedHrs / 60)])
    else
      QRLabelJobPiecesPerHr.Caption := Format('%.0f', [0.0]);

    // 20014002
    TotalPlantWorkedHrs := TotalPlantWorkedHrs + TotalJobWorkedHrs;
    TotalBUWorkedHrs := TotalBUWorkedHrs + TotalJobWorkedHrs;
    TotalDeptWorkedHrs := TotalDeptWorkedHrs + TotalJobWorkedHrs;

    TotalPlantSalHrs := TotalPlantSalhrs + TotalJobSalHrs;
    TotalBUSalHrs := TotalBUSalHrs + TotalJobSalHrs;
    TotalDeptSalHrs := TotalDeptSalHrs + TotalJobSalHrs;

    TotalPlantDiffHrs := TotalPlantDiffHrs + TotalJobDiffHrs;
    TotalBUDiffHrs := TotalBUDiffHrs + TotalJobDiffHrs;
    TotalDeptDiffHrs := TotalDeptDiffHrs + TotalJobDiffHrs;

    TotalPlantOvertimeHrs := TotalPlantOvertimeHrs + TotalJobOvertimeHrs;
    TotalBUOvertimeHrs := TotalBUOvertimeHrs + TotalJobOvertimeHrs;
    TotalDeptOvertimeHrs := TotalDeptOvertimeHrs + TotalJobOvertimeHrs;

    TotalPlantPayrollDollars := TotalPlantPayrollDollars + TotalJobPayrollDollars;
    TotalBUPayrollDollars := TotalBUPayrollDollars + TotalJobPayrollDollars;
    TotalDeptPayrollDollars := TotalDeptPayrollDollars + TotalJobPayrollDollars;
    //[ROP] 20013965
    if not QRParameters.FShowPayrollInformation then
      begin
        QRLabelJobPayrollDollars.Caption := '';
        QRLabelJobCostsPerPiece.Caption := '';
        QRLabelJobCostsPerHr.Caption := '';
      end;
    // [ROP] end 20013965
  end;
end;

// 20014002
procedure TReportProductionQR.DetermineWorkedHours;
var
//  EmployeeNumber: Integer;
//  APSalaryRecord: PTSalaryRecord;
//  OvertimeMinute: Integer;
//  PayedBreakMinute: Integer;
//  ProdMin: Integer;
//  TotalJobMin: Integer; // 20015220
  HourlyWage: Double;
  BonusPercentage: Double;
  BonusInMoney: Boolean;
  EmplWorkedHrs: Integer;
  EmplSalHrs: Integer;
  EmplDiffHrs: Integer;
  OvertimeHrs: Integer;
//  TimeRecProdMin: Integer; // 20015220
  procedure TimeRegListFree;
  var
    I: Integer;
    APTimeRegRecord: PTTimeRegRecord;
  begin
    for I:=0 to TimeRegList.Count-1 do
    begin
      APTimeRegRecord := TimeRegList.Items[I];
      Dispose(APTimeRegRecord);
    end;
    TimeRegList.Free;
  end;
  procedure SalaryListFree;
  var
    I: Integer;
    APSalaryRecord: PTSalaryRecord;
  begin
    for I:=0 to SalaryList.Count-1 do
    begin
      APSalaryRecord := SalaryList.Items[I];
      Dispose(APSalaryRecord);
    end;
    SalaryList.Free;
  end;
  function DetermineProdMins(AValue: Integer): Integer; // 20015220
  begin
    Result := AValue;
    if UseDateTime then
    begin
      with ReportProductionDetailDM do
      begin
        if cdsEmpPieces.Locate(
          'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;SHIFT_NUMBER_FILTER',
          VarArrayOf([
            ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString,
            ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
            ReportProductionDM.QueryProduction.FieldByName('JOB_CODE').AsString,
            ReportProductionDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
            ReportProductionDM.QueryProdHourPerEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            0 // MyShiftNumber
          ]),
          []) then
        begin
          // Also check on business unit?
          if cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString =
            ReportProductionDM.QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString then
          begin
//WDebugLog('-> EmpPieces=' + cdsEmpPieces.FieldByName('EMPPIECES').AsString);
            Result := cdsEmpPieces.FieldByName('PRODMINS').AsInteger;
          end
          else
            Result := 0;
        end; // if
      end; // with
    end; // if
  end; // DetermineProdMins
  // 20015221 For 'include open scans': We can not look in
  // prodhourperemployee-table, because for open scans there are no hours yet.
  // PIM-135 Not used anymore!
{
  procedure IncludeOpenScansHandler; // 20015221
  begin
    with ReportProductionDM do
    begin
      ReportProductionDetailDM.cdsEmpPieces.Filtered := False;
      ReportProductionDetailDM.cdsEmpPieces.Filter :=
        'PLANT_CODE = ' + '''' + QueryProduction.FieldByName('PLANT_CODE').AsString + '''' + ' AND ' +
        'WORKSPOT_CODE =  ' + '''' + QueryProduction.FieldByName('WORKSPOT_CODE').AsString + '''' + ' AND ' +
        'JOB_CODE = ' + '''' + QueryProduction.FieldByName('JOB_CODE').AsString + '''';
      ReportProductionDetailDM.cdsEmpPieces.Filtered := True;
      if not ReportProductionDetailDM.cdsEmpPieces.IsEmpty then
      begin
        ReportProductionDetailDM.cdsEmpPieces.First;
        while not ReportProductionDetailDM.cdsEmpPieces.Eof do
        begin
          // Also check on business unit?
          if ReportProductionDetailDM.cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString =
            ReportProductionDM.QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString then
            ProdMin := ReportProductionDetailDM.cdsEmpPieces.FieldByName('PRODMINS').AsInteger
          else
            ProdMin := 0;
          TotalWorkspotWorkedHrs := TotalWorkspotWorkedHrs +
            ProdMin;
          TotalJobWorkedHrs := TotalJobWorkedHrs +
            ProdMin;
          // Get Payroll
          if TableEmployeeContract.FindKey([
            ReportProductionDetailDM.cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsInteger]) then
          begin
            HourlyWage :=
              TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat;
            PayedBreakMinute := 0; // ???
            TotalJobPayrollDollars := TotalJobPayrollDollars +
              (TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat *
              ((
                ProdMin +
                PayedBreakMinute) / 60));
          end;
          ReportProductionDetailDM.cdsEmpPieces.Next;
        end;
      end;
      ReportProductionDetailDM.cdsEmpPieces.Filtered := False;
    end; // with
  end; // IncludeOpenScansHandler
}
begin
  HourlyWage := 0;
  BonusPercentage := 0;
  BonusInMoney := False;

//  TotalJobMin :=
//    Round(ReportProductionDM.
//      QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60);

  // PIM-135 Here we must handle the employee-related data like
  // overtime/bonus/payroll for all employees working per job.
  OvertimeHrs :=
    ReportProductionDetailDM.DetermineOvertime(
      ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('JOB_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      ReportProductionDetailDM.ContractGroupTFT,
      BonusInMoney);

  // Determine HourlyWage, BonusPercentage and EmplWorkedHrs
  if ReportProductionDM.QueryProduction.FieldByName('HOURLY_WAGE').AsString <> '' then
    HourlyWage := ReportProductionDM.QueryProduction.FieldByName('HOURLY_WAGE').AsFloat;
  if ReportProductionDM.QueryProduction.FieldByName('BONUS_FACTOR').AsString <> '' then
    BonusPercentage := ReportProductionDM.QueryProduction.FieldByName('BONUS_FACTOR').AsFloat;
  EmplWorkedHrs :=
    Round(ReportProductionDM.QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60); // Convert to minutes!
  EmplSalHrs :=
    Round(ReportProductionDM.QueryProduction.FieldByName('PRODHOURMIN').AsFloat); // PIM-135
  EmplDiffHrs := EmplWorkedHrs - EmplSalHrs; // PIM-135


  // Get Payroll
  if EmplWorkedHrs > 0 then
    TotalJobPayrollDollars := TotalJobPayrollDollars +
     (HourlyWage * (EmplWorkedHrs / 60));

  // Overtime and Bonus
  if (OvertimeHrs > 0) and (BonusPercentage > 0) and BonusInMoney then
     TotalJobPayrollDollars := TotalJobPayrollDollars +
      (HourlyWage * BonusPercentage / 100 *
        (OvertimeHrs / 60));

{
  // 20015220 + 20015221 Determine prodmins based on scans
  TimeRecProdMin :=
    ReportProductionDetailDM.DetermineTimeRecProdMins(
      QRParameters.FDateFrom, QRParameters.FDateTo,
      ReportProductionDM.QueryProduction.FieldByName('PLANT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
      ReportProductionDM.QueryProduction.FieldByName('JOB_CODE').AsString,
      '',
      ReportProductionDM.QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString,
      -1,
      0,
      NullDate,
      0);
  with ReportProductionDM do
  begin
    QueryProdHourPerEmployee.Close;
    QueryProdHourPerEmployee.ParamByName('PLANT_CODE').Value :=
      QueryProduction.FieldByName('PLANT_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('WORKSPOT_CODE').Value :=
      QueryProduction.FieldByName('WORKSPOT_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('JOB_CODE').Value :=
      QueryProduction.FieldByName('JOB_CODE').Value;
    QueryProdHourPerEmployee.ParamByName('DATEFROM').AsDate :=
      Trunc(QRParameters.FDateFrom);
    QueryProdHourPerEmployee.ParamByName('DATETO').AsDate :=
      Trunc(QRParameters.FDateTo);
    QueryProdHourPerEmployee.Open;
    if QueryProdHourPerEmployee.RecordCount > 0 then
    begin
      QueryProdHourPerEmployee.First;
      while not QueryProdHourPerEmployee.Eof do
      begin
        TotalJobMin := TotalJobMin +
          DetermineProdMins(QueryProdHourPerEmployee.FieldByName('PRODUCTION_MINUTE').AsInteger); // 20014002 // 20015220
        EmployeeNumber :=
          QueryProdHourPerEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
        // Get Payroll
        if TableEmployeeContract.FindKey([EmployeeNumber]) then
        begin
          HourlyWage :=
            TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat;
          // MR:8-12-2004 Payed_Break_Minute can be NULL!
          if QueryProdHourPerEmployee.
            FieldByName('PAYED_BREAK_MINUTE').AsString = '' then
            PayedBreakMinute := 0
          else
            PayedBreakMinute :=
              QueryProdHourPerEmployee.
                FieldByName('PAYED_BREAK_MINUTE').AsInteger;
          TotalJobPayrollDollars := TotalJobPayrollDollars +
            (TableEmployeeContract.FieldByName('HOURLY_WAGE').AsFloat *
            ((
              QueryProdHourPerEmployee.
                FieldByName('PRODUCTION_MINUTE').AsInteger +
              PayedBreakMinute) / 60));
        end;
        // Get OverTime
        QuerySalaryHourPerEmployee.Close;
        QuerySalaryHourPerEmployee.ParamByName('EMPLOYEE_NUMBER').Value :=
          EmployeeNumber;
        QuerySalaryHourPerEmployee.ParamByName('DATEFROM').AsDate :=
          QueryProdHourPerEmployee.
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        QuerySalaryHourPerEmployee.ParamByName('DATETO').AsDate :=
          QueryProdHourPerEmployee.
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
        QuerySalaryHourPerEmployee.Open;
        SalaryList := TList.Create;
        if QuerySalaryHourPerEmployee.RecordCount > 0 then
        begin
          QuerySalaryHourPerEmployee.First;
          while not QuerySalaryHourPerEmployee.Eof do
          begin
            // Store SalaryMinute in a list for later use
            new(APSalaryRecord);
            SalaryList.Add(APSalaryRecord);
            APSalaryRecord.SalaryMinute := QuerySalaryHourPerEmployee.
              FieldByName('SALARY_MINUTE').AsInteger;
            APSalaryRecord.BonusPercentage := QuerySalaryHourPerEmployee.
              FieldByName('BONUS_PERCENTAGE').AsFloat;
            // RV045.1.
            APSalaryRecord.HTBonusInMoney := (QuerySalaryHourPerEmployee.
              FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y');

            QuerySalaryHourPerEmployee.Next;
          end;
          TimeRegList := TList.Create;
          DetermineTimeRegScanningMinutes;
          OvertimeMinute :=
            DetermineOvertime(
              QueryProduction.FieldByName('WORKSPOT_CODE').Value,
              QueryProduction.FieldByName('JOB_CODE').Value,
              HourlyWage, EmployeeNumber);
          TotalJobOvertimeHrs := TotalJobOvertimeHrs + OvertimeMinute;
          TimeRegListFree;
        end;  // if QuerySalaryHourPerEmployee.RecordCount > 0 then
        QuerySalaryHourPerEmployee.Close;
        SalaryListFree;
        QueryProdHourPerEmployee.Next;
      end;
    end;
    QueryProdHourPerEmployee.Close;
  end;
}
//  if TimeRecProdMin > 0 then // 20015220
//    if TimeRecProdMin > TotalJobMin then
//      TotalJobMin := TimeRecProdMin;
  TotalWorkspotWorkedHrs := TotalWorkspotWorkedHrs + EmplWorkedHrs;
  TotalWorkspotSalHrs := TotalWorkspotSalHrs + EmplSalhrs;
  TotalWorkspotDiffHrs := TotalWorkspotDiffHrs + EmplDiffHrs;
  TotalWorkspotOvertimeHrs := TotalWorkspotOvertimeHrs + OvertimeHrs;
  TotalWorkspotPayrollDollars := TotalWorkspotPayrollDollars + TotalJobPayrollDollars;
  TotalJobWorkedHrs := TotalJobWorkedHrs + EmplWorkedHrs;
  TotalJobSalHrs := TotalJobSalHrs + EmplSalHrs;
  TotalJobDiffHrs := TotalJobDiffHrs + EmplDiffHRs;
  TotalJobOvertimeHrs := TotalJobOvertimeHrs + OvertimeHrs;
end; // DetermineWorkedHours

procedure TReportProductionQR.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20014002
  if QRParameters.FShowJobs and (not QRParameters.FShowOnlyJobs) then
    Value := SPimsTotalWorkspot + ' ' + Value;
end;

procedure TReportProductionQR.QRLabelJobDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20014002
  Value := Copy(Value, 1, 25);
end;

// SR-20013769
procedure TReportProductionQR.QRGroupColumnHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  WorkspotJobHeader: String;
begin
  inherited;
  // 20013965 Export only when this band is printed.
  if QRParameters.FExportToFile and BandPrinted then
  begin
    WorkspotJobHeader := '';
    if QRLabelWorkspotColumnHeader.Caption <> '' then
    begin
       WorkspotJobHeader := QRLabelWorkspotColumnHeader.Caption;
       if QRLabelJobColumnHeader.Caption <> '' then
         WorkspotJobHeader := WorkspotJobHeader + '/' +
           QRLabelJobColumnHeader.Caption;
    end
    else
      WorkspotJobHeader := QRLabelJobColumnHeader.Caption;
    // Column header
    ExportClass.AddText(
      WorkspotJobHeader + // 20014002
  //    ExportClass.Sep + // title
      ExportClass.Sep + // Description
      QRLabel8.Caption + ExportClass.Sep + // Pieces
      QRLabel9.Caption + ' ' + QRLabel4.Caption + ExportClass.Sep + // Worked hours
      QRLabel45.Caption + ' ' + QRLabel46.Caption + ExportClass.Sep + // Salary hours
      QRLabel53.Caption + ExportClass.Sep + // Diff.
      QRLabel10.Caption + QRLabel5.Caption + ExportClass.Sep + // Overtime
      QRLabel6.Caption + ' ' + QRLabel7.Caption + ExportClass.Sep + // Payroll $
      QRLabel12.Caption + ' ' + QRLabel14.Caption + ' ' + QRLabel22.Caption +
        ExportClass.Sep + // Cost per piece
      QRLabel15.Caption + ' ' + QRLabel17.Caption + ' ' + QRLabel23.Caption +
        ExportClass.Sep + // Label rate per hour
      QRLabel18.Caption + ' ' + QRLabel19.Caption + ' ' + QRLabel24.Caption +
        ExportClass.Sep + // Pieces per hour
      QRLabel20.Caption + ExportClass.Sep + // Sales
      QRLabel21.Caption + ' ' + QRLabel26.Caption + ' ' + QRLabel27.Caption +
        ' ' + QRLabel28.Caption + ExportClass.Sep + // $ payroll to sales
      QRLabel29.Caption + ' ' + QRLabel30.Caption + ' ' + QRLabel31.Caption +
        ' ' + QRLabel32.Caption + ExportClass.Sep + // Income per worked hour
      QRLabel33.Caption + ' ' + QRLabel34.Caption + ' ' + QRLabel36.Caption +
        ' ' + QRLabel37.Caption // Marginal rate per hour
      );
  end;
end;

procedure TReportProductionQR.ChildBand1AfterPrint(Sender: TQRCustomBand; // [ROP] 20013965
  BandPrinted: Boolean);
var
  WorkspotJobHeader: String;
begin
  inherited;
  // 20013965 Export only when this band is printed.
  if QRParameters.FExportToFile and BandPrinted then
    begin
      WorkspotJobHeader := '';
      if QRLabel38.Caption <> '' then
        begin
          WorkspotJobHeader := QRLabel38.Caption;
          if QRLabel39.Caption <> '' then
            WorkspotJobHeader := WorkspotJobHeader + '/' + QRLabel39.Caption;
        end
      else
        WorkspotJobHeader := QRLabel39.Caption;
      // Column header
      ExportClass.AddText(
        WorkspotJobHeader + // 20014002
//      ExportClass.Sep + // title
        ExportClass.Sep + // Description
        QRLabel40.Caption + ExportClass.Sep + // Pieces
        QRLabel41.Caption + ' ' + QRLabel42.Caption + ExportClass.Sep + // Worked hours
        QRLabel51.Caption + ' ' + QRLabel52.Caption + ExportClass.Sep + // Salary hours
        QRLabel56.Caption + ExportClass.Sep + // Diff.
        QRLabel43.Caption + QRLabel44.Caption + ExportClass.Sep + // Overtime
        QRLabel59.Caption + ' ' + QRLabel60.Caption + ' ' + QRLabel61.Caption // Pieces per hour
      );
    end;
end;


procedure TReportProductionQR.QRGroupColumnHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowPayrollInformation;
end;

procedure TReportProductionQR.ChildBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FShowPayrollInformation;
end;

(*
// 20015220.110 Determine the pieces on job-level,
//              for all employees that worked on that job
function TReportProductionQR.DeterminePieces: Double;
begin
  Result := 0;
  // There can be more than 1 record so use a filter here.
  with ReportProductionDetailDM do
  begin
    cdsEmpPieces.Filtered := False;
    cdsEmpPieces.Filter :=
      'PLANT_CODE = ' +
        QuotedStr(ReportProductionDM.
          QueryProduction.FieldByName('PLANT_CODE').AsString) + ' AND ' +
      'WORKSPOT_CODE = ' +
        QuotedStr(ReportProductionDM.
          QueryProduction.FieldByName('WORKSPOT_CODE').AsString) + ' AND ' +
      'JOB_CODE = ' +
        QuotedStr(ReportProductionDM.
          QueryProduction.FieldByName('JOB_CODE').AsString) + ' AND ' +
      'DEPARTMENT_CODE = ' +
        QuotedStr(ReportProductionDM.
          QueryProduction.FieldByName('DEPARTMENT_CODE').AsString) + ' AND ' +
      'BUSINESSUNIT_CODE = ' +
        QuotedStr(ReportProductionDM.
          QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString);
    cdsEmpPieces.Filtered := True;
    while not cdsEmpPieces.Eof do
    begin
      Result := Result + cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
      cdsEmpPieces.Next;
    end;
    cdsEmpPieces.Filtered := False;
  end; // with
end; // DeterminePieces
*)

// PIM-135
function TReportProductionQR.DecodeHrsSalMin(AValue: Integer): String;
begin
  Result := '';
end;

// PIM-135
function TReportProductionQR.DecodeHrsDiffMin(AValue: Integer): String;
begin
  Result := '';
end;

end.
