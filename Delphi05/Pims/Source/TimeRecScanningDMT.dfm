inherited TimeRecScanningDM: TTimeRecScanningDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 259
  Top = 151
  Height = 581
  Width = 764
  inherited TableMaster: TTable
    AutoCalcFields = False
    BeforePost = TableMasterBeforePost
    BeforeDelete = TableMasterBeforeDelete
    OnNewRecord = TableMasterNewRecord
    Filtered = True
    IndexFieldNames = 'MUTATIONDATE'
    TableName = 'REQUESTEARLYLATE'
    Left = 48
    Top = 20
    object TableMasterREQUEST_DATE: TDateTimeField
      FieldName = 'REQUEST_DATE'
      OnChange = TableMasterREQUEST_DATEChange
      DisplayFormat = 'dd/MM/yyyy'
      EditMask = '!90/90/00;1;_'
    end
    object TableMasterEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableMasterREQUESTED_EARLY_TIME: TDateTimeField
      FieldName = 'REQUESTED_EARLY_TIME'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterREQUESTED_LATE_TIME: TDateTimeField
      FieldName = 'REQUESTED_LATE_TIME'
      DisplayFormat = 'hh:mm'
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupDataSet = QueryEmployeeLU
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Lookup = True
    end
  end
  inherited TableDetail: TTable
    AutoCalcFields = False
    BeforeOpen = TableDetailBeforeOpen
    BeforeInsert = TableDetailBeforeInsert
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    AfterCancel = DefaultSaveCurrentRecord
    BeforeDelete = TableDetailBeforeDelete
    AfterDelete = TableDetailAfterDelete
    AfterRefresh = DefaultSaveCurrentRecord
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    Filter = 'PROCESSED_YN = '#39'N'#39
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'DATETIME_IN;EMPLOYEE_NUMBER'
    MasterSource = nil
    TableName = 'TIMEREGSCANNING'
    Left = 48
    Top = 76
    object TableDetailDATETIME_IN: TDateTimeField
      FieldName = 'DATETIME_IN'
      Required = True
      OnChange = TableDetailDATETIME_INChange
    end
    object TableDetailDATETIME_OUT: TDateTimeField
      FieldName = 'DATETIME_OUT'
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnChange = TableDetailPLANT_CODEChange
      Size = 6
    end
    object TableDetailWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      OnChange = TableDetailWORKSPOT_CODEChange
      Size = 6
    end
    object TableDetailJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      OnChange = TableDetailJOB_CODEChange
      Size = 6
    end
    object TableDetailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
      OnChange = TableDetailSHIFT_NUMBERChange
    end
    object TableDetailPROCESSED_YN: TStringField
      FieldName = 'PROCESSED_YN'
      Size = 1
    end
    object TableDetailIDCARD_IN: TStringField
      FieldName = 'IDCARD_IN'
      Size = 15
    end
    object TableDetailIDCARD_OUT: TStringField
      FieldName = 'IDCARD_OUT'
      Size = 15
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailPLANTCAL: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTCAL'
      LookupDataSet = QueryPlantLU
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Lookup = True
    end
    object TableDetailEMPLOYEECAL: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEECAL'
      LookupDataSet = QueryEmployeeLU
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      Lookup = True
    end
    object TableDetailEMP_PLANT_CODE: TStringField
      FieldKind = fkLookup
      FieldName = 'EMP_PLANT_CODE'
      LookupDataSet = QueryEmployeeLU
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'EMP_PLANT_CODE'
      KeyFields = 'EMPLOYEE_NUMBER'
      Size = 6
      Lookup = True
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPARTMENT_CODE'
      LookupDataSet = QueryEmployeeLU
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DEPARTMENT_CODE'
      KeyFields = 'EMPLOYEE_NUMBER'
      Lookup = True
    end
    object TableDetailTEAM_CODE: TStringField
      FieldKind = fkLookup
      FieldName = 'TEAM_CODE'
      LookupDataSet = QueryEmployeeLU
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'TEAM_CODE'
      KeyFields = 'EMPLOYEE_NUMBER'
      Size = 6
      Lookup = True
    end
    object TableDetailWORKSPOTCAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'WORKSPOTCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailJOBCAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'JOBCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailSHIFTCAL: TStringField
      FieldKind = fkCalculated
      FieldName = 'SHIFTCAL'
      Size = 30
      Calculated = True
    end
    object TableDetailSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
    end
    object TableDetailSHIFT_HOURTYPE_NUMBER: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SHIFT_HOURTYPE_NUMBER'
      Calculated = True
    end
    object TableDetailWORKSPOT_HOURTYPE_NUMBER: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'WORKSPOT_HOURTYPE_NUMBER'
      Calculated = True
    end
    object TableDetailIGNORE_FOR_OVERTIME_YN: TStringField
      FieldKind = fkCalculated
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Size = 1
      Calculated = True
    end
    object TableDetailWORKSPOT_DEPARTMENT_CODE: TStringField
      FieldKind = fkCalculated
      FieldName = 'WORKSPOT_DEPARTMENT_CODE'
      Size = 6
      Calculated = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 144
    Top = 20
  end
  inherited DataSourceDetail: TDataSource
    Left = 144
    Top = 76
  end
  inherited TableExport: TTable
    Left = 268
    Top = 444
  end
  inherited DataSourceExport: TDataSource
    Left = 392
    Top = 444
  end
  object DataSourcePlantLU: TDataSource
    DataSet = QueryPlantLU
    Left = 392
    Top = 68
  end
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 540
    Top = 12
  end
  object QueryDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 540
    Top = 68
  end
  object DataSourceWorkSpotLU: TDataSource
    DataSet = QueryWorkspotLU
    Left = 392
    Top = 124
  end
  object DataSourceJobLU: TDataSource
    DataSet = QueryJobLU
    Left = 392
    Top = 176
  end
  object DataSourceEmployeeLU: TDataSource
    DataSet = QueryEmployeeLU
    Left = 392
    Top = 16
  end
  object DataSourceShiftLU: TDataSource
    DataSet = QueryShiftLU
    Left = 392
    Top = 232
  end
  object DataSourceFilterEmployee: TDataSource
    DataSet = cdsFilterEmployee
    Left = 392
    Top = 320
  end
  object DataSourceFilterShift: TDataSource
    DataSet = cdsFilterShift
    Left = 392
    Top = 376
  end
  object QueryFilterEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.SHORT_NAME,'
      '  E.DESCRIPTION,'
      '  E.PLANT_CODE,'
      '  P.DESCRIPTION AS PLANTLU,'
      '  E.IS_SCANNING_YN,'
      '  E.BOOK_PROD_HRS_YN,'
      '  E.STARTDATE,'
      '  E.ENDDATE'
      'FROM '
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE '
      '   ( '
      '     (:USER_NAME = '#39'*'#39') OR'
      '     (E.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME))'
      '   )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      '')
    Left = 64
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryFilterEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryFilterEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object QueryFilterEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryFilterEmployeePLANTLU: TStringField
      FieldName = 'PLANTLU'
      Size = 30
    end
    object QueryFilterEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryFilterEmployeeIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object QueryFilterEmployeeBOOK_PROD_HRS_YN: TStringField
      FieldName = 'BOOK_PROD_HRS_YN'
      Origin = 'EMPLOYEE.BOOK_PROD_HRS_YN'
      Size = 1
    end
    object QueryFilterEmployeeSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      Origin = 'EMPLOYEE.STARTDATE'
    end
    object QueryFilterEmployeeENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
      Origin = 'EMPLOYEE.ENDDATE'
    end
  end
  object dspFilterEmployee: TDataSetProvider
    DataSet = QueryFilterEmployee
    Constraints = True
    Left = 168
    Top = 320
  end
  object cdsFilterEmployee: TClientDataSet
    Aggregates = <>
    Filtered = True
    FieldDefs = <
      item
        Name = 'EMPLOYEE_NUMBER'
        DataType = ftInteger
      end
      item
        Name = 'SHORT_NAME'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'DESCRIPTION'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'PLANTLU'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'PLANT_CODE'
        DataType = ftString
        Size = 6
      end
      item
        Name = 'IS_SCANNING_YN'
        DataType = ftString
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspFilterEmployee'
    StoreDefs = True
    OnFilterRecord = cdsFilterEmployeeFilterRecord
    Left = 272
    Top = 320
  end
  object QueryFilterShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.PLANT_CODE, '
      '  S.SHIFT_NUMBER, '
      '  S.DESCRIPTION,'
      '  P.DESCRIPTION AS PLANTLU'
      'FROM'
      '  SHIFT S, PLANT P'
      'WHERE'
      '  S.PLANT_CODE = P.PLANT_CODE'
      '  ')
    Left = 64
    Top = 376
    object QueryFilterShiftPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryFilterShiftSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object QueryFilterShiftDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object QueryFilterShiftPLANTLU: TStringField
      FieldName = 'PLANTLU'
      Size = 30
    end
  end
  object dspFilterShift: TDataSetProvider
    DataSet = QueryFilterShift
    Constraints = True
    Left = 168
    Top = 376
  end
  object cdsFilterShift: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPlantShift'
        Fields = 'PLANT_CODE;SHIFT_NUMBER'
      end>
    IndexName = 'byPlantShift'
    Params = <>
    ProviderName = 'dspFilterShift'
    StoreDefs = True
    Left = 272
    Top = 376
  end
  object QueryEmployeeLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE EMP_PLANT_CODE,'
      '  E.TEAM_CODE, E.DEPARTMENT_CODE'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '   ('
      '     (:USER_NAME = '#39'*'#39') OR'
      '     (E.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME))'
      '   )'
      'ORDER BY'
      '  E.DESCRIPTION'
      ' '
      ' '
      ' ')
    Left = 272
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object QueryWorkspotLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlantLU
    SQL.Strings = (
      'SELECT'
      '  W.PLANT_CODE,'
      '  W.WORKSPOT_CODE,'
      '  W.DESCRIPTION,'
      '  W.USE_JOBCODE_YN,'
      '  W.DEPARTMENT_CODE'
      'FROM'
      '  WORKSPOT W'
      'WHERE'
      '  W.PLANT_CODE = :PLANT_CODE'
      'ORDER BY'
      '  W.WORKSPOT_CODE')
    Left = 272
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkspot
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, DESCRIPTION'
      'FROM '
      '  JOBCODE'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE'
      'ORDER BY '
      '  JOB_CODE')
    Left = 272
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryShiftLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlantLU
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION,'
      '  HOURTYPE_NUMBER'
      'FROM '
      '  SHIFT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  SHIFT_NUMBER')
    Left = 272
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryPlantLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantLUFilterRecord
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DESCRIPTION'
      'FROM '
      '  PLANT'
      'ORDER BY '
      '  PLANT_CODE')
    Left = 272
    Top = 72
  end
  object TableWorkspot: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = TableWorkspotFilterRecord
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'WORKSPOT'
    Left = 272
    Top = 280
    object TableWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableWorkspotDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableWorkspotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableWorkspotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableWorkspotUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableWorkspotHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableWorkspotPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableWorkspotQUANT_PIECE_YN: TStringField
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableWorkspotAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableWorkspotCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableWorkspotDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
  end
  object DataSourceWorkspot: TDataSource
    DataSet = TableWorkspot
    Left = 388
    Top = 280
  end
  object qryTRS_BICheck1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) RECCOUNT'
      'FROM'
      '  TIMEREGSCANNING T'
      'WHERE'
      '  (T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND'
      '  ('
      '    (T.DATETIME_OUT IS NOT NULL) AND'
      '    (T.DATETIME_IN < :DATETIME_IN) AND'
      '    (T.DATETIME_OUT > :DATETIME_IN)'
      '  )'
      ''
      ' ')
    Left = 520
    Top = 168
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end>
  end
  object qryTRS_BICheck2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) RECCOUNT'
      'FROM'
      '  TIMEREGSCANNING T'
      'WHERE'
      '  (T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND'
      '  ( ((T.DATETIME_OUT IS NOT NULL) AND'
      '    (T.DATETIME_IN < :DATETIME_OUT) AND'
      '    (T.DATETIME_OUT > :DATETIME_IN )) OR'
      '    ((T.DATETIME_OUT IS NULL) AND'
      '    (T.DATETIME_IN > :DATETIME_IN) AND'
      '    (T.DATETIME_IN < :DATETIME_OUT)) )'
      ' ')
    Left = 520
    Top = 224
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end>
  end
  object qryTRS_BUCheck1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) RECCOUNT'
      'FROM'
      '  TIMEREGSCANNING T'
      'WHERE'
      '  ((T.DATETIME_IN <> :OLD_DATETIME_IN) OR'
      '  (T.EMPLOYEE_NUMBER <> :OLD_EMPLOYEE_NUMBER)) AND'
      '  (T.EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER) AND'
      '  (T.DATETIME_OUT IS NOT NULL) AND'
      '  (T.DATETIME_IN < :NEW_DATETIME_IN) AND'
      '  (T.DATETIME_OUT > :NEW_DATETIME_IN)'
      ' ')
    Left = 520
    Top = 288
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'OLD_DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OLD_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_IN'
        ParamType = ptUnknown
      end>
  end
  object qryTRS_BUCheck2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  COUNT(*) RECCOUNT'
      'FROM'
      '  TIMEREGSCANNING T'
      'WHERE'
      '  ((T.DATETIME_IN <> :OLD_DATETIME_IN) OR'
      '   (T.EMPLOYEE_NUMBER <> :OLD_EMPLOYEE_NUMBER)) AND'
      '   (T.EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER) AND'
      '   ( ((T.DATETIME_OUT IS NOT NULL) AND'
      '   (T.DATETIME_IN < :NEW_DATETIME_OUT) AND'
      '   (T.DATETIME_OUT > :NEW_DATETIME_IN )) OR'
      '   ((T.DATETIME_OUT IS NULL) AND'
      '   (T.DATETIME_IN > :NEW_DATETIME_IN) AND'
      '   (T.DATETIME_IN < :NEW_DATETIME_OUT)) )'
      ' ')
    Left = 520
    Top = 344
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'OLD_DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'OLD_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_DATETIME_OUT'
        ParamType = ptUnknown
      end>
  end
  object qryPQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.START_DATE, P.PLANT_CODE, P.WORKSPOT_CODE,'
      '  P.JOB_CODE, P.MANUAL_YN, P.SHIFT_NUMBER'
      'FROM'
      '  PRODUCTIONQUANTITY P'
      'WHERE'
      '  P.START_DATE >= :DATEFROM AND'
      '  P.END_DATE <= :DATETO AND'
      '  P.PLANT_CODE = :PLANT_CODE AND'
      '  P.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  P.JOB_CODE = :NO_SCAN_JOB_CODE AND'
      '  P.MANUAL_YN = :MANUAL_YN '
      'ORDER BY'
      '  P.START_DATE'
      ''
      ' '
      ' '
      ' ')
    Left = 68
    Top = 436
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NO_SCAN_JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryUpdatePQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE PRODUCTIONQUANTITY P'
      'SET P.JOB_CODE = :TRS_JOB_CODE,'
      'P.SHIFT_NUMBER = :SHIFT_NUMBER,'
      'P.MUTATIONDATE = :MUTATIONDATE,'
      'P.MUTATOR = :MUTATOR'
      'WHERE'
      '  P.START_DATE = :START_DATE AND'
      '  P.PLANT_CODE = :PLANT_CODE AND'
      '  P.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  P.JOB_CODE = :JOB_CODE AND'
      '  P.MANUAL_YN = :MANUAL_YN'
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 164
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'TRS_JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object QueryInsertScanning: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'insert into'
      
        '  timeregscanning(DATETIME_IN, DATETIME_OUT, EMPLOYEE_NUMBER, PL' +
        'ANT_CODE, WORKSPOT_CODE, JOB_CODE, SHIFT_NUMBER, PROCESSED_YN, I' +
        'DCARD_IN, IDCARD_OUT, CREATIONDATE, MUTATIONDATE, MUTATOR, SHIFT' +
        '_DATE)'
      'values'
      
        '  (:DATETIME_IN, :DATETIME_OUT, :EMPLOYEE_NUMBER, :PLANT_CODE, :' +
        'WORKSPOT_CODE, :JOB_CODE, :SHIFT_NUMBER, :PROCESSED_YN, :IDCARD_' +
        'IN, :IDCARD_OUT, sysdate, sysdate, :MUTATOR, :SHIFT_DATE)'
      ''
      ' ')
    Left = 540
    Top = 124
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PROCESSED_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IDCARD_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IDCARD_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryTRSSetEnddate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE TIMEREGSCANNING'
      'SET DATETIME_OUT = :DATETIME_OUT'
      'WHERE DATETIME_IN = :DATETIME_IN'
      'AND EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 520
    Top = 400
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATETIME_OUT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETIME_IN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryWorkspotLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.PLANT_CODE,'
      '  W.WORKSPOT_CODE,'
      '  W.DESCRIPTION,'
      '  W.HOURTYPE_NUMBER,'
      '  CASE'
      '    WHEN W.HOURTYPE_NUMBER IS NOT NULL THEN'
      
        '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H WHERE H.H' +
        'OURTYPE_NUMBER = W.HOURTYPE_NUMBER)'
      '    ELSE'
      '      '#39#39
      '  END IGNORE_FOR_OVERTIME_YN,'
      '  W.DEPARTMENT_CODE'
      'FROM'
      '  WORKSPOT W'
      'ORDER BY'
      '  W.PLANT_CODE,'
      '  W.WORKSPOT_CODE'
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 52
    Top = 148
  end
  object qryJobLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, DESCRIPTION'
      'FROM '
      '  JOBCODE'
      'ORDER BY '
      '  PLANT_CODE, '
      '  WORKSPOT_CODE, '
      '  JOB_CODE'
      '')
    Left = 52
    Top = 196
  end
  object qryShiftLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.PLANT_CODE, S.SHIFT_NUMBER, S.DESCRIPTION,'
      '  S.HOURTYPE_NUMBER,'
      '  CASE'
      '    WHEN S.HOURTYPE_NUMBER IS NOT NULL THEN'
      
        '      (SELECT H.IGNORE_FOR_OVERTIME_YN FROM HOURTYPE H WHERE H.H' +
        'OURTYPE_NUMBER = S.HOURTYPE_NUMBER)'
      '    ELSE'
      '      '#39#39
      '  END IGNORE_FOR_OVERTIME_YN'
      'FROM'
      '  SHIFT S'
      'ORDER BY'
      '  S.PLANT_CODE,'
      '  S.SHIFT_NUMBER'
      ''
      ' ')
    Left = 52
    Top = 244
  end
  object qryTRSLastRec: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT MAX(DATETIME_OUT) DATETIME_OUT'
      'FROM TIMEREGSCANNING'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_DATE = :SHIFT_DATE')
    Left = 524
    Top = 452
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end>
  end
  object StoredProcRecalcEfficiency: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'RECALC_EFFICIENCY'
    Left = 160
    Top = 240
    ParamData = <
      item
        DataType = ftString
        Name = 'FP_PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'FP_SHIFT_DATE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FP_SHIFT_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FP_WORKSPOT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FP_JOB_CODE'
        ParamType = ptInput
      end>
  end
  object StoredProcMoveEfficiency: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'MOVE_EFFICIENCY'
    Left = 160
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'FP_EMPLOYEE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'FP_DATETIME_IN'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'FP_NEW_SHIFT_NR'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FP_NEW_WORKSPOT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FP_NEW_JOBCODE'
        ParamType = ptInput
      end>
  end
  object qryEarlyLateScanCheck: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MIN(DATETIME_IN) DATETIME_IN, MAX(DATETIME_OUT) DATETIME_OUT'
      'FROM '
      '  TIMEREGSCANNING'
      'WHERE '
      '  DATETIME_IN >= :STARTDATE AND '
      '  DATETIME_IN <= :ENDDATE AND'
      '  EMPLOYEE_NUMBER = :EMPNO'
      'ORDER BY '
      '  DATETIME_IN'
      ' ')
    Left = 468
    Top = 60
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
end
