(*
  Changes:
    MRA:16-FEB-2010. RV054.4.
    - Optimise: Set LogChanges to false for ClientDataSets.
    MRA:4-JUL-2016 PIM-197
    - Addition of Team-selection.
    MRA:26-APR-2019 GLOB3-296
    - When all columns give 0 then it should not show the line
    - Also at some parts it still used a max. of 4 timeblocks.
*)
unit ReportCompHoursDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, Provider, DBClient;

type
  TReportCompHoursDM = class(TReportBaseDM)
    QueryAbsMinute_Ill: TQuery;
    IntegerField1: TIntegerField;
    QueryAbsMinute_Hol: TQuery;
    QueryAbsMinute_HolEMPLOYEE_NUMBER: TIntegerField;
    QueryAbsMinute_Paid: TQuery;
    IntegerField3: TIntegerField;
    QueryAbsMinute_Unpaid: TQuery;
    IntegerField2: TIntegerField;
    QuerySalaryMinute: TQuery;
    QuerySalaryMinuteEMPLOYEE_NUMBER: TIntegerField;
    QueryEmpl: TQuery;
    TableCtrEmpl: TTable;
    qryEmployeePlanning: TQuery;
    qryEmployeeAvailable: TQuery;
    ClientDataSetAbsIll: TClientDataSet;
    DataSetProviderAbsIll: TDataSetProvider;
    ClientDataSetAbsHol: TClientDataSet;
    DataSetProviderAbsHol: TDataSetProvider;
    ClientDataSetAbsPaid: TClientDataSet;
    DataSetProviderAbsPaid: TDataSetProvider;
    DataSetProviderAbsUnpaid: TDataSetProvider;
    ClientDataSetAbsUnpaid: TClientDataSet;
    ClientDataSetSal: TClientDataSet;
    DataSetProviderSal: TDataSetProvider;
    ClientDataSetAbsIllEMPLOYEE_NUMBER: TIntegerField;
    ClientDataSetAbsHolEMPLOYEE_NUMBER: TIntegerField;
    ClientDataSetEmplContr: TClientDataSet;
    DataSetProviderEmplContr: TDataSetProvider;
    ClientDataSetEmpAv: TClientDataSet;
    DataSetProviderEmpAv: TDataSetProvider;
    ClientDataSetEmpPln: TClientDataSet;
    DataSetProviderEmpPln: TDataSetProvider;
    // MR:21-02-2005 Oracle:
    // ABS_MIN-fields changed from Integer to Float
    QueryAbsMinute_IllABS_MIN: TFloatField;
    QueryAbsMinute_HolABS_MIN: TFloatField;
    QueryAbsMinute_PaidABS_MIN: TFloatField;
    QueryAbsMinute_UnpaidABS_MIN: TFloatField;
    QuerySalaryMinuteABS_MIN: TFloatField;
    ClientDataSetAbsIllABS_MIN: TFloatField;
    ClientDataSetAbsHolABS_MIN: TFloatField;
    QueryRequest: TQuery;
    ClientDataSetEmpReq: TClientDataSet;
    DataSetProviderEmpRequest: TDataSetProvider;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetParamEmpl(ClientDataSetWork: TClientDataSet;
      Empl: Integer; var Min: Integer);
  end;

var
  ReportCompHoursDM: TReportCompHoursDM;

implementation

uses CalculateTotalHoursDMT, SystemDMT;

{$R *.DFM}

procedure TReportCompHoursDM.SetParamEmpl(ClientDataSetWork: TClientDataSet;
  Empl: Integer; var Min: Integer);
begin
  Min := 0;
  if ClientDataSetWork.Locate('EMPLOYEE_NUMBER', VarArrayOf([Empl]), []) then
    Min := Round(ClientDataSetWork.FieldByName('ABS_MIN').AsFloat);
end;

procedure TReportCompHoursDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV054.4.
  try
    ClientDataSetAbsIll.LogChanges := False;
    ClientDataSetAbsHol.LogChanges := False;
    ClientDataSetAbsPaid.LogChanges := False;
    ClientDataSetAbsUnpaid.LogChanges := False;
    ClientDataSetSal.LogChanges := False;
    ClientDataSetEmplContr.LogChanges := False;
    ClientDataSetEmpAv.LogChanges := False;
    ClientDataSetEmpPln.LogChanges := False;
    ClientDataSetEmpReq.LogChanges := False;
  except
    // Ignore errors
  end;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
end;

procedure TReportCompHoursDM.QueryEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
