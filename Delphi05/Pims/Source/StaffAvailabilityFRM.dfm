inherited StaffAvailabilityF: TStaffAvailabilityF
  Tag = 1
  Left = 253
  Top = 155
  Width = 793
  Height = 502
  ActiveControl = BitBtnRefresh
  Caption = 'Staff Availability'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 777
    Height = 323
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 318
      Width = 775
      Height = 4
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 89
      Width = 775
      Height = 229
      Bands = <
        item
          Caption = 'Employee'
          Width = 132
        end
        item
          Caption = 'Day1'
          Width = 123
        end
        item
          Caption = 'Day2'
          Width = 131
        end
        item
          Caption = 'Day3'
          Width = 137
        end
        item
          Caption = 'Day4'
          Width = 158
        end
        item
          Caption = 'Day5'
          Width = 133
        end
        item
          Caption = 'Day7'
          Width = 129
        end
        item
          Caption = 'Day6'
          Width = 134
        end>
      DefaultLayout = False
      KeyField = 'EMPLOYEE_NUMBER'
      TabOrder = 1
      DataSource = StaffAvailabilityDM.DataSourceAvailability
      OptionsDB = [edgoCanNavigation, edgoSmartRefresh, edgoUseBookmarks]
      OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      object dxMasterGridColumn38: TdxDBGridColumn
        Sizing = False
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'RECNO'
      end
      object dxMasterGridColumnNo: TdxDBGridColumn
        Caption = 'No.'
        Width = 49
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
        SummaryType = cstCount
      end
      object dxMasterGridColumnName: TdxDBGridColumn
        Caption = 'Name'
        Width = 92
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnC111: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D111CALC'
      end
      object dxMasterGridColumnC11: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D11CALC'
      end
      object dxMasterGridColumnC12: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D12CALC'
      end
      object dxMasterGridColumnC13: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D13CALC'
      end
      object dxMasterGridColumnC14: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D14CALC'
      end
      object dxMasterGridColumnC211: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D211CALC'
      end
      object dxMasterGridColumnC21: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D21CALC'
      end
      object dxMasterGridColumnC22: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D22CALC'
      end
      object dxMasterGridColumnC23: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D23CALC'
      end
      object dxMasterGridColumnC24: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D24CALC'
      end
      object dxMasterGridColumnC311: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D311CALC'
      end
      object dxMasterGridColumnC31: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D31CALC'
      end
      object dxMasterGridColumnC32: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D32CALC'
      end
      object dxMasterGridColumnC33: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D33CALC'
      end
      object dxMasterGridColumnC34: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D34CALC'
      end
      object dxMasterGridColumnC411: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D411CALC'
      end
      object dxMasterGridColumnC41: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D41CALC'
      end
      object dxMasterGridColumnC42: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D42CALC'
      end
      object dxMasterGridColumnC43: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D43CALC'
      end
      object dxMasterGridColumnC44: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D44CALC'
      end
      object dxMasterGridColumnC511: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D511CALC'
      end
      object dxMasterGridColumnC51: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D51CALC'
      end
      object dxMasterGridColumnC52: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D52CALC'
      end
      object dxMasterGridColumnC53: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D53CALC'
      end
      object dxMasterGridColumnC54: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D54CALC'
      end
      object dxMasterGridColumnC611: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D611CALC'
      end
      object dxMasterGridColumnC61: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D61CALC'
      end
      object dxMasterGridColumnC62: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D62CALC'
      end
      object dxMasterGridColumnC63: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D63CALC'
      end
      object dxMasterGridColumnC64: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D64CALC'
      end
      object dxMasterGridColumnC711: TdxDBGridColumn
        Caption = '  '
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D711CALC'
      end
      object dxMasterGridColumnC71: TdxDBGridColumn
        Caption = '1'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D71CALC'
      end
      object dxMasterGridColumnC72: TdxDBGridColumn
        Caption = '2'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D72CALC'
      end
      object dxMasterGridColumnC73: TdxDBGridColumn
        Caption = '3'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D73CALC'
      end
      object dxMasterGridColumnC74: TdxDBGridColumn
        Caption = '4'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D74CALC'
      end
      object dxMasterGridColumnSTARTDATE: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'EMPSTARTDATE'
        DisableFilter = True
      end
      object dxMasterGridColumnENDDATE: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'EMPENDDATE'
        DisableFilter = True
      end
      object dxMasterGridColumnP1: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P1'
        DisableFilter = True
      end
      object dxMasterGridColumnP2: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P2'
        DisableFilter = True
      end
      object dxMasterGridColumnP3: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P3'
        DisableFilter = True
      end
      object dxMasterGridColumnP4: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P4'
        DisableFilter = True
      end
      object dxMasterGridColumnP5: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P5'
        DisableFilter = True
      end
      object dxMasterGridColumnP6: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P6'
        DisableFilter = True
      end
      object dxMasterGridColumnP7: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'P7'
        DisableFilter = True
      end
      object dxMasterGridColumnEMPPLANT_CODE: TdxDBGridColumn
        DisableCaption = True
        DisableCustomizing = True
        DisableDragging = True
        DisableEditor = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        DisableGrouping = True
        FieldName = 'EMPPLANT_CODE'
        DisableFilter = True
      end
      object dxMasterGridColumnC15: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D15CALC'
      end
      object dxMasterGridColumnC16: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D16CALC'
      end
      object dxMasterGridColumnC17: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D17CALC'
      end
      object dxMasterGridColumnC18: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D18CALC'
      end
      object dxMasterGridColumnC19: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D19CALC'
      end
      object dxMasterGridColumnC110: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 1
        RowIndex = 0
        FieldName = 'D110CALC'
      end
      object dxMasterGridColumnC25: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D25CALC'
      end
      object dxMasterGridColumnC26: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D26CALC'
      end
      object dxMasterGridColumnC27: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D27CALC'
      end
      object dxMasterGridColumnC28: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D28CALC'
      end
      object dxMasterGridColumnC29: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D29CALC'
      end
      object dxMasterGridColumnC210: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 2
        RowIndex = 0
        FieldName = 'D210CALC'
      end
      object dxMasterGridColumnC35: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D35CALC'
      end
      object dxMasterGridColumnC36: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D36CALC'
      end
      object dxMasterGridColumnC37: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D37CALC'
      end
      object dxMasterGridColumnC38: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D38CALC'
      end
      object dxMasterGridColumnC39: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D39CALC'
      end
      object dxMasterGridColumnC310: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D310CALC'
      end
      object dxMasterGridColumnC45: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D45CALC'
      end
      object dxMasterGridColumnC46: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D46CALC'
      end
      object dxMasterGridColumnC47: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D47CALC'
      end
      object dxMasterGridColumnC48: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D48CALC'
      end
      object dxMasterGridColumnC49: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D49CALC'
      end
      object dxMasterGridColumnC410: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D410CALC'
      end
      object dxMasterGridColumnC55: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D55CALC'
      end
      object dxMasterGridColumnC56: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D56CALC'
      end
      object dxMasterGridColumnC57: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D57CALC'
      end
      object dxMasterGridColumnC58: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D58CALC'
      end
      object dxMasterGridColumnC59: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D59CALC'
      end
      object dxMasterGridColumnC510: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D510CALC'
      end
      object dxMasterGridColumnC65: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D65CALC'
      end
      object dxMasterGridColumnC66: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D66CALC'
      end
      object dxMasterGridColumnC67: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D67CALC'
      end
      object dxMasterGridColumnC68: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D68CALC'
      end
      object dxMasterGridColumnC69: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D69CALC'
      end
      object dxMasterGridColumnC610: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D610CALC'
      end
      object dxMasterGridColumnC75: TdxDBGridColumn
        Caption = '5'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D75CALC'
      end
      object dxMasterGridColumnC76: TdxDBGridColumn
        Caption = '6'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D76CALC'
      end
      object dxMasterGridColumnC77: TdxDBGridColumn
        Caption = '7'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D77CALC'
      end
      object dxMasterGridColumnC78: TdxDBGridColumn
        Caption = '8'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D78CALC'
      end
      object dxMasterGridColumnC79: TdxDBGridColumn
        Caption = '9'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D79CALC'
      end
      object dxMasterGridColumnC710: TdxDBGridColumn
        Caption = '10'
        Width = 20
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D710CALC'
      end
    end
    object GroupBox15: TGroupBox
      Left = 1
      Top = 1
      Width = 775
      Height = 88
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 40
        Top = 13
        Width = 26
        Height = 13
        Caption = 'Team'
      end
      object Label2: TLabel
        Left = 8
        Top = 35
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label3: TLabel
        Left = 34
        Top = 62
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object Label4: TLabel
        Left = 160
        Top = 62
        Width = 27
        Height = 13
        Caption = 'Week'
      end
      object Label14: TLabel
        Left = 286
        Top = 13
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 8
        Top = 13
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lblPeriod: TLabel
        Left = 291
        Top = 61
        Width = 40
        Height = 13
        Caption = 'lblPeriod'
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 80
        Top = 58
        Width = 65
        TabOrder = 5
        OnChange = ChangeFilterItem
        MaxValue = 2098
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
      object dxSpinEditWeek: TdxSpinEdit
        Left = 208
        Top = 58
        Width = 73
        TabOrder = 6
        OnChange = ChangeFilterItem
        MaxValue = 52
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
      object CheckBoxAllTeams: TCheckBox
        Left = 512
        Top = 15
        Width = 69
        Height = 17
        Caption = 'All teams'
        TabOrder = 2
        OnClick = ChangeFilterItem
      end
      object ComboBoxPlusPlants: TComboBoxPlus
        Left = 80
        Top = 35
        Width = 201
        Height = 19
        ColCount = 176
        Ctl3D = False
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        ParentCtl3D = False
        TabOrder = 3
        TitleColor = clBtnFace
        OnChange = ChangeFilterItem
      end
      object BitBtnRefresh: TBitBtn
        Left = 557
        Top = 55
        Width = 75
        Height = 25
        Caption = '&Refresh'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = BitBtnRefreshClick
      end
      object CheckBoxPlanInOtherPlants: TCheckBox
        Left = 512
        Top = 36
        Width = 118
        Height = 17
        Caption = 'Plan in other plants'
        TabOrder = 7
        OnClick = CheckBoxPlanInOtherPlantsClick
      end
      object CheckBoxAllPlant: TCheckBox
        Left = 291
        Top = 37
        Width = 73
        Height = 17
        Caption = 'All plants'
        TabOrder = 4
        OnClick = CheckBoxAllPlantClick
      end
      object cmbPlusTeamFrom: TComboBoxPlus
        Left = 80
        Top = 12
        Width = 201
        Height = 19
        ColCount = 256
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamFromCloseUp
      end
      object cmbPlusTeamTo: TComboBoxPlus
        Left = 302
        Top = 12
        Width = 201
        Height = 19
        ColCount = 256
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamToCloseUp
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 349
    Width = 777
    Height = 114
    TabOrder = 3
    object GroupBoxDays: TGroupBox
      Left = 1
      Top = 1
      Width = 775
      Height = 80
      Align = alTop
      Caption = 'Availability'
      TabOrder = 0
      object ScrollBox1: TScrollBox
        Left = 2
        Top = 15
        Width = 771
        Height = 63
        Align = alClient
        BorderStyle = bsNone
        TabOrder = 0
        object GroupBoxDay1: TGroupBox
          Left = 0
          Top = 0
          Width = 200
          Height = 46
          Align = alLeft
          Caption = 'Day1'
          TabOrder = 0
          object Edit11: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit12: TEdit
            Tag = 1
            Left = 40
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit13: TEdit
            Tag = 1
            Left = 57
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit14: TEdit
            Tag = 1
            Left = 74
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit111: TEdit
            Tag = 5
            Left = 3
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit15: TEdit
            Tag = 1
            Left = 91
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit16: TEdit
            Tag = 1
            Left = 109
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit17: TEdit
            Tag = 1
            Left = 126
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit18: TEdit
            Tag = 1
            Left = 143
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit19: TEdit
            Tag = 1
            Left = 160
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit110: TEdit
            Tag = 1
            Left = 177
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay2: TGroupBox
          Left = 200
          Top = 0
          Width = 190
          Height = 46
          Align = alLeft
          Caption = 'Day2'
          TabOrder = 1
          object Edit21: TEdit
            Tag = 1
            Left = 24
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit22: TEdit
            Tag = 1
            Left = 40
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit23: TEdit
            Tag = 1
            Left = 56
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit24: TEdit
            Tag = 1
            Left = 72
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit211: TEdit
            Tag = 5
            Left = 3
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit25: TEdit
            Tag = 1
            Left = 88
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit26: TEdit
            Tag = 1
            Left = 104
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit27: TEdit
            Tag = 1
            Left = 120
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit28: TEdit
            Tag = 1
            Left = 136
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit29: TEdit
            Tag = 1
            Left = 152
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit210: TEdit
            Tag = 1
            Left = 168
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay3: TGroupBox
          Left = 390
          Top = 0
          Width = 188
          Height = 46
          Align = alLeft
          Caption = 'Day3'
          TabOrder = 2
          object Edit31: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 18
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit32: TEdit
            Tag = 1
            Left = 39
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit33: TEdit
            Tag = 1
            Left = 55
            Top = 20
            Width = 18
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit34: TEdit
            Tag = 1
            Left = 71
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit311: TEdit
            Tag = 5
            Left = 2
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit35: TEdit
            Tag = 1
            Left = 87
            Top = 20
            Width = 18
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit36: TEdit
            Tag = 1
            Left = 103
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit37: TEdit
            Tag = 1
            Left = 119
            Top = 20
            Width = 18
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit38: TEdit
            Tag = 1
            Left = 135
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit39: TEdit
            Tag = 1
            Left = 151
            Top = 20
            Width = 18
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit310: TEdit
            Tag = 1
            Left = 167
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = DefaultLookupComboBoxKeyUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay4: TGroupBox
          Left = 578
          Top = 0
          Width = 191
          Height = 46
          Align = alLeft
          Caption = 'Day4'
          TabOrder = 3
          object Edit41: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit42: TEdit
            Tag = 1
            Left = 39
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit43: TEdit
            Tag = 1
            Left = 55
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit44: TEdit
            Tag = 1
            Left = 71
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit411: TEdit
            Tag = 5
            Left = 2
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit45: TEdit
            Tag = 1
            Left = 87
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit46: TEdit
            Tag = 1
            Left = 104
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit47: TEdit
            Tag = 1
            Left = 120
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit48: TEdit
            Tag = 1
            Left = 136
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit49: TEdit
            Tag = 1
            Left = 152
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit410: TEdit
            Tag = 1
            Left = 168
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay5: TGroupBox
          Left = 769
          Top = 0
          Width = 188
          Height = 46
          Align = alLeft
          Caption = 'Day5'
          TabOrder = 4
          object Edit51: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit52: TEdit
            Tag = 1
            Left = 39
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit53: TEdit
            Tag = 1
            Left = 55
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit54: TEdit
            Tag = 1
            Left = 71
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit511: TEdit
            Tag = 5
            Left = 2
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit55: TEdit
            Tag = 1
            Left = 87
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit56: TEdit
            Tag = 1
            Left = 103
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit57: TEdit
            Tag = 1
            Left = 119
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit58: TEdit
            Tag = 1
            Left = 135
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit59: TEdit
            Tag = 1
            Left = 151
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit510: TEdit
            Tag = 1
            Left = 167
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay6: TGroupBox
          Left = 957
          Top = 0
          Width = 191
          Height = 46
          Align = alLeft
          Caption = 'Day6'
          TabOrder = 5
          object Edit61: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit62: TEdit
            Tag = 1
            Left = 39
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit63: TEdit
            Tag = 1
            Left = 55
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit64: TEdit
            Tag = 1
            Left = 71
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit611: TEdit
            Tag = 5
            Left = 2
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit65: TEdit
            Tag = 1
            Left = 87
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit66: TEdit
            Tag = 1
            Left = 103
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit67: TEdit
            Tag = 1
            Left = 119
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit68: TEdit
            Tag = 1
            Left = 135
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit69: TEdit
            Tag = 1
            Left = 151
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit610: TEdit
            Tag = 1
            Left = 167
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
        object GroupBoxDay7: TGroupBox
          Left = 1148
          Top = 0
          Width = 205
          Height = 46
          Align = alLeft
          Caption = 'Day7'
          TabOrder = 6
          object Edit71: TEdit
            Tag = 1
            Left = 23
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 1
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit72: TEdit
            Tag = 1
            Left = 39
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 2
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit73: TEdit
            Tag = 1
            Left = 55
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 3
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit74: TEdit
            Tag = 1
            Left = 71
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 4
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit711: TEdit
            Tag = 5
            Left = 2
            Top = 20
            Width = 21
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            MaxLength = 2
            PopupMenu = PopupMenuShift
            TabOrder = 0
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultShiftEditValidation
            OnKeyUp = DefaultChangeShiftNo
            OnMouseDown = Edit11MouseDown
          end
          object Edit75: TEdit
            Tag = 1
            Left = 87
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 5
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit76: TEdit
            Tag = 1
            Left = 103
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 6
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit77: TEdit
            Tag = 1
            Left = 119
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 7
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit78: TEdit
            Tag = 1
            Left = 135
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 8
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit79: TEdit
            Tag = 1
            Left = 151
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 9
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
          object Edit710: TEdit
            Tag = 1
            Left = 167
            Top = 20
            Width = 17
            Height = 21
            Enabled = False
            MaxLength = 1
            PopupMenu = PopupMenuTB
            TabOrder = 10
            OnChange = DefaultEditChange
            OnContextPopup = DefaultEditPopUp
            OnKeyDown = EditKeyDown
            OnKeyPress = DefaultEditValidation
            OnMouseDown = Edit11MouseDown
          end
        end
      end
    end
    object BitBtnEmployeeAv: TBitBtn
      Left = 4
      Top = 80
      Width = 113
      Height = 25
      Caption = 'Employee availability'
      TabOrder = 1
      OnClick = BitBtnEmployeeAvClick
    end
    object BitBtnStandard: TBitBtn
      Left = 121
      Top = 80
      Width = 113
      Height = 25
      Caption = 'Restore standard'
      TabOrder = 2
      OnClick = BitBtnStandardClick
    end
    object BitBtnFillStav: TBitBtn
      Left = 239
      Top = 80
      Width = 113
      Height = 25
      Caption = 'Fill staff availability'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BitBtnFillStavClick
    end
    object BitBtnReport: TBitBtn
      Left = 356
      Top = 80
      Width = 113
      Height = 25
      Caption = 'Report'
      TabOrder = 4
      OnClick = BitBtnReportClick
    end
    object EditP1: TEdit
      Left = 476
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 5
      Text = 'EditP1'
      Visible = False
    end
    object EditP2: TEdit
      Left = 500
      Top = 80
      Width = 29
      Height = 21
      TabOrder = 6
      Text = 'Edit1'
      Visible = False
    end
    object EditP3: TEdit
      Left = 524
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 7
      Text = 'Edit1'
      Visible = False
    end
    object EditP4: TEdit
      Left = 548
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 8
      Text = 'Edit1'
      Visible = False
    end
    object EditP5: TEdit
      Left = 572
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 9
      Text = 'Edit1'
      Visible = False
    end
    object EditP6: TEdit
      Left = 596
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 10
      Text = 'Edit1'
      Visible = False
    end
    object EditP7: TEdit
      Left = 620
      Top = 80
      Width = 25
      Height = 21
      TabOrder = 11
      Text = 'Edit1'
      Visible = False
    end
    object BitBtnWorkSchedule: TBitBtn
      Left = 475
      Top = 80
      Width = 113
      Height = 25
      Caption = 'Process workschedule'
      TabOrder = 12
      OnClick = BitBtnWorkScheduleClick
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 349
    Width = 777
    Height = 0
    Align = alBottom
    inherited spltDetail: TSplitter
      Top = -4
      Width = 775
    end
    inherited dxDetailGrid: TdxDBGrid
      Width = 775
      Height = 278
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 576
    Top = 204
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 652
    Top = 84
  end
  inherited StandardMenuActionList: TActionList
    Left = 532
    Top = 196
  end
  inherited dsrcActive: TDataSource
    Left = 108
    Top = 188
  end
  object PopupMenuTB: TPopupMenu
    AutoHotkeys = maManual
    OnPopup = PopupMenuTBPopup
    Left = 145
    Top = 187
  end
  object PopupMenuShift: TPopupMenu
    AutoHotkeys = maManual
    OnPopup = PopupMenuTBPopup
    Left = 41
    Top = 187
  end
end
