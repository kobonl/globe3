(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:6-FEB-2013. 20013910 Plan on depts give problems.
  - Related to this order.
    - Performance-issue: Assign timeblocks without using a procedure.
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)
unit ReportStaffPlanningDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, ReportStaffPlanningQRPT, DBClient, UPimsConst;

type
  TReportStaffPlanningDM = class(TReportBaseDM)
    QueryEMP: TQuery;
    TableSTO: TTable;
    QueryExec: TQuery;
    QueryDept: TQuery;
    QueryShift: TQuery;
    TablePlant: TTable;
    QueryWK: TQuery;
    QueryOCP: TQuery;
    QueryEMPPLN: TQuery;
    cdsPlannedEmployee: TClientDataSet;
    cdsPlannedEmployeeEMPLOYEE_NUMBER: TIntegerField;
    cdsPlannedEmployeeSHIFT_NUMBER: TIntegerField;
    cdsPlannedEmployeePLANT_CODE: TStringField;
    cdsPlannedEmployeePLAN_DATE: TDateTimeField;
    QueryEmployee: TQuery;
    qryRequestEarlyLate: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryEMPFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure GetSTO(Plant, WKCode, DeptCODE: String;
      Shift: Integer; PaidBreaks: Boolean;
      FStartDate, FEndDate: TDateTime; var FSTOWK: TTBWeekArray);
    function CheckWKIsPrinted(Plant, WKCode, DeptCODE: String;
      Shift: Integer; PaidBreaks: Boolean; FStartDay: Integer;
      FStartDate, FEndDate: TDateTime): Boolean;
    function ISNotDeptValid(Dept,Plant, TeamFrom, TeamTo: String) : Boolean;
    procedure RequestEarlyLateOpen(ADateFrom, ADateTo: TDateTime);
    procedure RequestEarlyLateClose;
    function RequestEarlyLateFindEarlyTime(ADate: TDateTime; AEmployeeNumber: Integer; ADefault: TDateTime): TDateTime;
    function RequestEarlyLateFindLateTime(ADate: TDateTime; AEmployeeNumber: Integer; ADefault: TDateTime): TDateTime;
{    procedure GetTBFromEMP(Empl, Shift: Integer;
      Plant, Workspot, Department: String;
      DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE); }
  end;

var
  ReportStaffPlanningDM: TReportStaffPlanningDM;

implementation

uses CalculateTotalHoursDMT, ListProcsFRM, SystemDMT;

{$R *.DFM}

// 20013910 Related to this order.
// Assign this without using this procedure.
(*
procedure TReportStaffPlanningDM.GetTBFromEMP(Empl, Shift: Integer;
  Plant, Workspot, Department: String;
  DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE);
var
  i: Integer;
begin
  for i:= 1 to 4 do
    Schedule_TB[i] := '';
  for i := 1 to 4 do
    Schedule_TB[I] :=
      QueryEmp.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
end;
*)

procedure TReportStaffPlanningDM.GetSTO(Plant, WKCode, DeptCODE: String;
  Shift: Integer; PaidBreaks: Boolean;
  FStartDate, FEndDate: TDateTime; var FSTOWK: TTBWeekArray);
var
  TmpTable: TDataSet;
  Ok: Boolean;
  K, I,DayOCP: Integer;
  DateOCP: TDateTime;
begin
//CAR 22-7-2003
  ATBLengthClass.FillTBLength(-1, Shift, Plant, DeptCODE, PaidBreaks);

  //CAR 21-7-2003
{  for k:= 1 to 4 do
    CalculateTotalHoursDM.FTBValid[k] := k;}
  for k := 0 to 6 do
  begin
    DateOCP := FStartDate + k;
    DayOCP := ListProcsF.DayWStartOnFromDate(DateOCP);
    if DateOCP > FEndDate then
      Exit;
    SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
    Ok := QueryOCP.Locate(
      'PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE;SHIFT_NUMBER;OCC_PLANNING_DATE',
       VarArrayOf([Plant, DeptCode, WKCode, Shift,
       FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, DateOCP)]), []);
    SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
    TmpTable :=(QueryOCP as TDataSet);
    if Ok then
      TmpTable :=(QueryOCP as TDataSet)
    else
    begin
      Ok := TableSTO.FindKey([Plant, DeptCode, WKCode, Shift,DayOCP]) ;
      if Ok then
        TmpTable := (TableSTO as TDataSet);
    end;
    if OK then
      for i := 1 to SystemDM.MaxTimeblocks do
        FSTOWK[k+1][i] := FSTOWK[k+ 1][I] +
          (TmpTable.FieldByName('OCC_A_TIMEBLOCK_'+IntToStr(i)).AsInteger +
          TmpTable.FieldByName('OCC_B_TIMEBLOCK_'+IntToStr(i)).AsInteger +
          TmpTable.FieldByName('OCC_C_TIMEBLOCK_'+IntToStr(i)).AsInteger ) *
          //CAR 21-7-2003
           ATBLengthClass.GetLengthTB(I, DayOCP);
  end;//for k := 0 to 6
end;

function TReportStaffPlanningDM.ISNotDeptValid(Dept,Plant, TeamFrom,
  TeamTo: String) : Boolean;
var
  SelectStr: String;
begin
  // MR:28-04-2005 Pims -> Oracle If 'dept' is empty then force result
  // to 'True', otherwise the 'query' will go wrong.
  if Dept = '' then
  begin
    Result := True;
    Exit;
  end;
  SelectStr :=
    'SELECT ' +
    '  COUNT(TEAM_CODE) AS COUNTTEAM ' +
    'FROM ' +
    '  DEPARTMENTPERTEAM ' +
  //Car 20.2.2004 add doublequote function
    'WHERE ' +
    '  PLANT_CODE = ''' + DoubleQuote(Plant) + '''' +
    '  AND DEPARTMENT_CODE = ''' + DoubleQuote(Dept) + '''' +
    '  AND TEAM_CODE >= ''' + DoubleQuote(TeamFrom) + '''' +
    '  AND TEAM_CODE <= ''' + DoubleQuote(TeamTo) + '''';
  QueryExec.Active := False;
  QueryExec.SQL.Clear;
  QueryExec.SQL.Add(Selectstr);
  QueryExec.ExecSQL;
  QueryExec.Active := True;
  // Pims -> Oracle
  Result := QueryExec.FieldByName('COUNTTEAM').AsFloat > 0;
//  Result := QueryExec.RecordCount > 0 ;
end;

function TReportStaffPlanningDM.CheckWKIsPrinted(Plant, WKCode, DeptCODE: String;
      Shift: Integer; PaidBreaks: Boolean;
      FStartDay: Integer; FStartDate, FEndDate: TDateTime): Boolean;
var
  TBPlanned: Array[1..MAX_TBS] of Integer;
// 20013910 Related to this order. Variables are not used here.
//  Planning: Array[1..7,1..4] of Integer;
//  i: Integer;
  k, j: Integer;
  DateEMP: TDateTime;
  DayEMP, Empl: Integer;
  TotalPlannedWK, TotalSTOWK: Integer;
  Calculate: Boolean;
begin
  TotalPlannedWK := 0;
  TotalSTOWK := 0;
  for j := 1 to 7 do
    for k := 1 to SystemDM.MaxTimeblocks do
        TotalSTOWK := TotalSTOWK + ReportStaffPlanningQR.FSTOWK[j][k];

  // 20013910 Related to this order: This is not used here.
{  for i := 1 to 7 do
    for k := 1 to 4 do
      Planning[i, k] := 0; }

  QueryEmpPln.First;
  if QueryEmpPln.Locate('PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE;SHIFT_NUMBER',
    VarArrayOf([Plant, DeptCode, WKCode, Shift]), []) then
  begin
    while not QueryEmpPln.Eof and
      (QueryEmpPln.FieldByName('PLANT_CODE').AsString = Plant) and
      (QueryEmpPln.FieldByName('SHIFT_NUMBER').AsInteger = Shift) and
      (QueryEmpPln.FieldByName('DEPARTMENT_CODE').AsString = DeptCode) and
      (QueryEmpPln.FieldByName('WORKSPOT_CODE').AsString = WKCode) do
    begin
      Empl := QueryEmpPln.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      ATBLengthClass.FillTBArray(Empl, Shift, Plant, DeptCode);
      DateEMP := QueryEmpPln.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
      Calculate := False;
      for k := 1 to SystemDM.MaxTimeblocks do
      begin
        if (QueryEmpPln.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(k)).AsString = 'A') or
          (QueryEmpPln.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(k)).AsString = 'B') or
          (QueryEmpPln.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(k)).AsString = 'C') then
        begin
          TBPlanned[k]:= k;
          Calculate := True;
        end
        else
          TBPlanned[k] := 0;
      end; // for
      if Calculate then
      begin
        ATBLengthClass.FillBreaks(Empl, Shift, Plant, DeptCode, PaidBreaks);
        DayEMP := ListProcsF.DayWStartOnFromDate(DateEMP);
        for k:= 1 to SystemDM.MaxTimeblocks do
          if  ATBLengthClass.ExistTB(k) and (TBPlanned[k] <> 0) then
            TotalPlannedWK := TotalPlannedWK +
              ATBLengthClass.GetLengthTB(k, DayEMP);
      end; // if
      QueryEmpPln.Next;
    end; // while
  end; // if Locate
  Result := ((TotalPlannedWK - TotalSTOWK) <> 0);
end;

procedure TReportStaffPlanningDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryShift.Open;
  QueryDept.Open;
  QueryWK.Open;
  QueryOCP.Prepare;
  QueryEmpPln.Prepare;
  cdsPlannedEmployee.CreateDataSet;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEMP);
end;

procedure TReportStaffPlanningDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  RequestEarlyLateClose;
  QueryShift.Close;
  QueryDept.Close;
  QueryWK.Close;
  QueryOCP.Close;
  QueryOCP.UnPrepare;
  QueryEmpPln.Close;
  QueryEmpPln.UnPrepare;
  cdsPlannedEmployee.Close;
end;

procedure TReportStaffPlanningDM.QueryEMPFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TReportStaffPlanningDM.RequestEarlyLateOpen(ADateFrom,
  ADateTo: TDateTime);
begin
  with qryRequestEarlyLate do
  begin
    Close;
    ParamByName('DATEFROM').AsDateTime := ADateFrom;
    ParamByName('DATETO').AsDateTime := ADateTo;
    Open;
  end;
end;

procedure TReportStaffPlanningDM.RequestEarlyLateClose;
begin
  qryRequestEarlyLate.Close;
end;

function TReportStaffPlanningDM.RequestEarlyLateFindEarlyTime(
  ADate: TDateTime; AEmployeeNumber: Integer;
  ADefault: TDateTime): TDateTime;
begin
  Result := ADefault;
  with qryRequestEarlyLate do
  begin
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
    while not Eof do
    begin
      if FieldByName('REQUEST_DATE').AsDateTime = ADate then
        if FieldByName('REQUESTED_EARLY_TIME').AsString <> '' then
          Result := FieldByName('REQUESTED_EARLY_TIME').AsDateTime;
      Next;
    end;
  end;
end;

function TReportStaffPlanningDM.RequestEarlyLateFindLateTime(
  ADate: TDateTime; AEmployeeNumber: Integer;
  ADefault: TDateTime): TDateTime;
begin
  Result := ADefault;
  with qryRequestEarlyLate do
  begin
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
    while not Eof do
    begin
      if FieldByName('REQUEST_DATE').AsDateTime = ADate then
        if FieldByName('REQUESTED_LATE_TIME').AsString <> '' then
          Result := FieldByName('REQUESTED_LATE_TIME').AsDateTime;
      Next;
    end;
  end;
end;

end.

