inherited ReportTeamIncentiveDM: TReportTeamIncentiveDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited qryPlantMinMax: TQuery
    Left = 416
    Top = 344
  end
  inherited qryTeamMinMax: TQuery
    Left = 416
    Top = 296
  end
  inherited qryEmplMinMax: TQuery
    Left = 416
    Top = 248
  end
  inherited qryBUMinMax: TQuery
    Left = 416
    Top = 192
  end
  inherited qryWorkspotMinMax: TQuery
    Left = 320
    Top = 240
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 320
    Top = 184
  end
  object QueryProdHour: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProdHourFilterRecord
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE,'
      '  P.SHIFT_NUMBER,'
      '  W.DEPARTMENT_CODE,'
      '  P.WORKSPOT_CODE, P.JOB_CODE,'
      '  P.EMPLOYEE_NUMBER,'
      '  P.PRODHOUREMPLOYEE_DATE ,'
      '  SUM(P.PRODUCTION_MINUTE)  AS SUMMIN '
      'FROM '
      '  PRODHOURPEREMPLOYEE P , WORKSPOT W,'
      '  DEPARTMENT D'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE, '
      '  P.SHIFT_NUMBER, '
      '  W.DEPARTMENT_CODE,  P.WORKSPOT_CODE, '
      '  P.JOB_CODE, '
      '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE '
      ' ')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProd: TDataSource
    DataSet = QueryProdHour
    Left = 168
    Top = 24
  end
  object TableProdQuant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PRODUCTIONQUANTITY'
    Left = 80
    Top = 296
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 168
    Top = 96
  end
  object TableTRS: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'TIMEREGSCANNING'
    Left = 80
    Top = 240
  end
  object QueryPQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE,'
      '  START_DATE, END_DATE,'
      '  QUANTITY'
      'FROM'
      '  PRODUCTIONQUANTITY'
      'WHERE'
      '  QUANTITY > 0'
      '  AND START_DATE >= :FSTARTDATE AND START_DATE < :FENDDATE'
      '  AND END_DATE >= :FSTARTDATE AND END_DATE < :FENDDATE AND'
      '  SHIFT_NUMBER >= :FSHIFTFROM AND SHIFT_NUMBER <= :FSHIFTTO '
      '  AND PLANT_CODE = :PLANT_CODE'
      '  AND WORKSPOT_CODE = :WORKSPOT_CODE'
      '  AND JOB_CODE = :JOB_CODE '
      'ORDER BY'
      '  START_DATE, END_DATE')
    Left = 320
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FSHIFTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FSHIFTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryTRS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 320
    Top = 72
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 320
    Top = 128
  end
  object QueryTRSCNT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 416
    Top = 128
  end
end
