(*
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:26-FEB-2014 SO-20014442
    - Adjustments to report
    - Custom-made solution, only for VOS:
      - Addition of checkbox 'Only SANA-employees'
      - Addition of Month (for From-To-Year-selection)
    MRA:28-MAR-2014 SO-20014442 Rework
    - It gave a wrong result for totals for illness, etc.
     - Reason: When the last week ends in next year, then it used
               the next year to determine the totals for illness, etc.
    - EXTRA: It should NOT allow a from-to-period for more than 1 year?
             Or the totals will be calculated wrong, because they are only
             calculated for 1 year at the end of the report.
    - NOTE: It should calculate balance always based on last year.
*)
unit DialogReportEmpAvailabilityFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib;

type
  TDialogReportEmpAvailabilityF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    Label4: TLabel;
    CheckBoxShowSelection: TCheckBox;
    Label6: TLabel;
    Label1: TLabel;
    dxSpinEditYearFrom: TdxSpinEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    dxSpinEditYearTo: TdxSpinEdit;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    CheckBoxAllEmployee: TCheckBox;
    CheckBoxOnlySANAEmps: TCheckBox;
    dxSpinEditMonthFrom: TdxSpinEdit;
    dxSpinEditMonthTo: TdxSpinEdit;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxAllEmployeeClick(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
  public
    { Public declarations }
  end;

var
  DialogReportEmpAvailabilityF: TDialogReportEmpAvailabilityF;

// RV089.1.
function DialogReportEmpAvailabilityForm: TDialogReportEmpAvailabilityF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  SystemDMT, ListProcsFRM, UPimsMessageRes,
  ReportEmpAvailabilityQRPT, ReportEmpAvailabilityDMT,
  CalculateTotalHoursDMT;

// RV089.1.
var
  DialogReportEmpAvailabilityF_HND: TDialogReportEmpAvailabilityF;

// RV089.1.
function DialogReportEmpAvailabilityForm: TDialogReportEmpAvailabilityF;
begin
  if (DialogReportEmpAvailabilityF_HND = nil) then
  begin
    DialogReportEmpAvailabilityF_HND := TDialogReportEmpAvailabilityF.Create(Application);
    with DialogReportEmpAvailabilityF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportEmpAvailabilityF_HND;
end;

// RV089.1.
procedure TDialogReportEmpAvailabilityF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportEmpAvailabilityF_HND <> nil) then
  begin
    DialogReportEmpAvailabilityF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportEmpAvailabilityF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportEmpAvailabilityF.btnOkClick(Sender: TObject);
var
  DateFrom, DateTo: TDateTime; // 20014442
begin
  inherited;
  DateFrom := 0;
  DateTo := 0;
  // 20014442 Only allow 1 year?
(*
  if dxSpinEditYearFrom.Value <> dxSpinEditYearTo.Value then
    begin
      DisplayMessage(SPimWrongYearSelection, mtInformation, [mbOK]);
      Exit;
    end;
*)
  if dxSpinEditYearFrom.Value > dxSpinEditYearTo.Value then
    begin
      DisplayMessage(SStartEndYear, mtInformation, [mbOK]);
      Exit;
    end;
  // 20014442
  if SystemDM.IsVOS then
  begin
    DateFrom :=
      EncodeDate(dxSpinEditYearFrom.IntValue, dxSpinEditMonthFrom.IntValue, 1);
    DateTo := EncodeDate(dxSpinEditYearTo.IntValue, dxSpinEditMonthTo.IntValue,
      ListProcsF.LastMounthDay(dxSpinEditYearTo.IntValue,
        dxSpinEditMonthTo.IntValue));
    if DateFrom > DateTo then
    begin
      DisplayMessage(SPimsStartEndYearMonth, mtInformation, [mbOK]);
      Exit;
    end;
  end;

  if ReportEmpAvailabilityQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value), GetStrValue(CmbPlusPlantTo.Value),
      //Car 550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      Round(dxSpinEditYearFrom.Value),  Round(dxSpinEditYearTo.Value),
      CheckBoxShowSelection.Checked, CheckBoxAllTeams.Checked,
      CheckBoxAllEmployee.Checked,
      CheckBoxExport.Checked,
      DateFrom, DateTo, // 20014442
      CheckBoxOnlySANAEmps.Checked) // 20014442
  then
    ReportEmpAvailabilityQR.ProcessRecords;
end;


procedure TDialogReportEmpAvailabilityF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, False, True, True, False, False, False);
  inherited;
//show team
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}
//end team
  ListProcsF.WeekUitDat(Now, Year, Week);
//  DecodeDate(Now, Year, Month, Day);
  dxSpinEditYearFrom.Value := Year;
  dxSpinEditYearTo.Value := Year;
  CheckBoxShowSelection.Checked := True;
  CheckBoxAllEmployeeClick(Sender);
end;

procedure TDialogReportEmpAvailabilityF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportEmpAvailabilityDM := CreateReportDM(TReportEmpAvailabilityDM);
  ReportEmpAvailabilityQR := CreateReportQR(TReportEmpAvailabilityQR);

  // 20014442
  CheckBoxOnlySANAEmps.Visible := SystemDM.IsVOS;
  dxSpinEditMonthFrom.Visible := SystemDM.IsVOS;
  dxSpinEditMonthTo.Visible := SystemDM.IsVOS;
  if SystemDM.IsVOS then
  begin
    CheckBoxOnlySANAEmps.Caption :=  SPimsOnlySANAEmps; 
    Label1.Caption := SPimsYearMonth;
  end;
end;


procedure TDialogReportEmpAvailabilityF.CheckBoxAllTeamClick(
  Sender: TObject);
begin
  inherited;
{
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
  end;
}  
end;

procedure TDialogReportEmpAvailabilityF.CheckBoxAllEmployeeClick(
  Sender: TObject);
begin
  inherited;
  if not ((CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)) then
    CheckBoxAllEmployee.Checked := True;
  dxDBExtLookupEditEmplFrom.Visible := not CheckBoxAllEmployee.Checked;
  dxDBExtLookupEditEmplTo.Visible := not CheckBoxAllEmployee.Checked;
end;

procedure TDialogReportEmpAvailabilityF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  CheckBoxAllEmployeeClick(Sender);
end;

procedure TDialogReportEmpAvailabilityF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  CheckBoxAllEmployeeClick(Sender);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportEmpAvailabilityF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportEmpAvailabilityF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
