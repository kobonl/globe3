inherited EmployeeF: TEmployeeF
  Left = 356
  Top = 162
  Width = 669
  Height = 545
  Caption = 'Employees'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 653
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 651
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 651
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 304
    Width = 653
    Height = 202
    TabOrder = 3
    object PageControlEmpl: TPageControl
      Left = 1
      Top = 1
      Width = 651
      Height = 200
      ActivePage = TabSheetGeneral
      Align = alClient
      TabOrder = 0
      object TabSheetGeneral: TTabSheet
        Caption = 'General'
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 304
          Height = 172
          Align = alLeft
          Caption = 'Employees'
          TabOrder = 0
          object Label1: TLabel
            Left = 4
            Top = 20
            Width = 37
            Height = 13
            Caption = 'Number'
          end
          object Label2: TLabel
            Left = 148
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Short name'
          end
          object Label3: TLabel
            Left = 4
            Top = 68
            Width = 18
            Height = 13
            Caption = 'Sex'
          end
          object Label11: TLabel
            Left = 4
            Top = 92
            Width = 73
            Height = 13
            Caption = 'Contract group'
          end
          object Label9: TLabel
            Left = 4
            Top = 44
            Width = 27
            Height = 13
            Caption = 'Name'
          end
          object Label22: TLabel
            Left = 4
            Top = 116
            Width = 47
            Height = 13
            Caption = 'Language'
          end
          object Label10: TLabel
            Left = 4
            Top = 140
            Width = 26
            Height = 13
            Caption = 'Team'
          end
          object DBEditEmployeeNumber: TDBEdit
            Tag = 1
            Left = 80
            Top = 15
            Width = 59
            Height = 19
            Ctl3D = False
            DataField = 'EMPLOYEE_NUMBER'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
            OnEnter = DBEditEmployeeNumberEnter
          end
          object DBEditName: TDBEdit
            Tag = 1
            Left = 208
            Top = 15
            Width = 91
            Height = 19
            Ctl3D = False
            DataField = 'SHORT_NAME'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 1
          end
          object dbrgRetired: TDBRadioGroup
            Tag = 1
            Left = 80
            Top = 58
            Width = 217
            Height = 29
            Columns = 3
            Ctl3D = False
            DataField = 'SEX'
            DataSource = EmployeeDM.DataSourceDetail
            Items.Strings = (
              'Male'
              'Female'
              'Unspec.')
            ParentCtl3D = False
            TabOrder = 3
            Values.Strings = (
              'M'
              'F'
              'X')
          end
          object DBEditDescription: TDBEdit
            Tag = 1
            Left = 80
            Top = 40
            Width = 219
            Height = 19
            Ctl3D = False
            DataField = 'DESCRIPTION'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 2
          end
          object dxDBLookupEdit1: TdxDBLookupEdit
            Tag = 1
            Left = 80
            Top = 90
            Width = 219
            Style.BorderStyle = xbsSingle
            TabOrder = 4
            DataField = 'CONTRACTLU'
            DataSource = EmployeeDM.DataSourceDetail
            DropDownRows = 6
            DropDownWidth = 240
            ListFieldName = 'DESCRIPTION;CONTRACTGROUP_CODE'
          end
          object dxDBLookupEdit2: TdxDBLookupEdit
            Tag = 1
            Left = 80
            Top = 114
            Width = 219
            Style.BorderStyle = xbsSingle
            TabOrder = 5
            DataField = 'LANGUAGELU'
            DataSource = EmployeeDM.DataSourceDetail
            DropDownRows = 4
            DropDownWidth = 240
            ListFieldName = 'DESCRIPTION;LANGUAGE_CODE'
          end
          object dxDBLookupEditTeam: TdxDBLookupEdit
            Left = 80
            Top = 138
            Width = 219
            Style.BorderStyle = xbsSingle
            TabOrder = 6
            DataField = 'TEAMLU'
            DataSource = EmployeeDM.DataSourceDetail
            DropDownWidth = 240
            ListFieldName = 'DESCRIPTION;TEAM_CODE'
            CanDeleteText = True
          end
        end
        object GroupBox2: TGroupBox
          Left = 304
          Top = 0
          Width = 339
          Height = 172
          Align = alClient
          TabOrder = 1
          object Label13: TLabel
            Left = 4
            Top = 16
            Width = 24
            Height = 13
            Caption = 'Plant'
          end
          object Label5: TLabel
            Left = 4
            Top = 39
            Width = 57
            Height = 13
            Caption = 'Department'
          end
          object Label20: TLabel
            Left = 4
            Top = 87
            Width = 106
            Height = 13
            Caption = 'Employment start/end'
          end
          object LabelGrade: TLabel
            Left = 8
            Top = 149
            Width = 29
            Height = 13
            Caption = 'Grade'
          end
          object Label16: TLabel
            Left = 112
            Top = 128
            Width = 84
            Height = 13
            Caption = 'First aid exp date'
          end
          object Label14: TLabel
            Left = 4
            Top = 64
            Width = 22
            Height = 13
            Caption = 'Shift'
          end
          object LabelExtRef: TLabel
            Left = 4
            Top = 149
            Width = 93
            Height = 13
            Caption = 'External Reference'
          end
          object DBPicker1: TDBPicker
            Tag = 1
            Left = 111
            Top = 83
            Width = 105
            Height = 21
            CalAlignment = dtaLeft
            Date = 44221
            Time = 44221
            DateFormat = dfShort
            DateMode = dmComboBox
            Kind = dtkDate
            ParseInput = False
            TabOrder = 3
            DataField = 'STARTDATE'
            DataSource = EmployeeDM.DataSourceDetail
          end
          object dxDBLookupEditDept: TdxDBLookupEdit
            Tag = 1
            Left = 110
            Top = 35
            Width = 226
            Style.BorderStyle = xbsSingle
            TabOrder = 1
            OnEnter = dxDBLookupEditDeptEnter
            DataField = 'DEPTLU'
            DataSource = EmployeeDM.DataSourceDetail
            OnMouseEnter = dxDBLookupEditDeptEnter
            DropDownWidth = 250
            ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
          end
          object dxDBLookupEditPlant: TdxDBLookupEdit
            Tag = 1
            Left = 110
            Top = 10
            Width = 226
            Style.BorderStyle = xbsSingle
            TabOrder = 0
            DataField = 'PLANTLU'
            DataSource = EmployeeDM.DataSourceDetail
            OnSelectionChange = dxDBLookupEditPlantSelectionChange
            OnCloseUp = dxDBLookupEditPlantCloseUp
            DropDownWidth = 226
            ListFieldName = 'DESCRIPTION;PLANT_CODE'
          end
          object dxDBDateEdit2: TdxDBDateEdit
            Left = 227
            Top = 83
            Width = 108
            TabOrder = 4
            DataField = 'ENDDATE'
            DataSource = EmployeeDM.DataSourceDetail
            DateValidation = True
            SaveTime = False
          end
          object DBCheckBoxFirstAid: TDBCheckBox
            Tag = 7
            Left = 8
            Top = 106
            Width = 97
            Height = 17
            Caption = 'First aid'
            Ctl3D = False
            DataField = 'FIRSTAID_YN'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 5
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object DBCheckBox2: TDBCheckBox
            Tag = 4
            Left = 8
            Top = 127
            Width = 89
            Height = 17
            Caption = 'Cut off time'
            Ctl3D = False
            DataField = 'CUT_OF_TIME_YN'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 8
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object DBEditGrade: TDBEdit
            Left = 46
            Top = 147
            Width = 59
            Height = 19
            Ctl3D = False
            DataField = 'GRADE'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 10
          end
          object DBCheckBox1: TDBCheckBox
            Tag = 5
            Left = 111
            Top = 106
            Width = 81
            Height = 17
            Caption = 'Is scanning'
            Ctl3D = False
            DataField = 'IS_SCANNING_YN'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 6
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
            OnClick = DBCheckBox1Click
          end
          object dxDBDateEdit1: TdxDBDateEdit
            Left = 227
            Top = 124
            Width = 105
            TabOrder = 9
            DataField = 'FIRSTAID_EXP_DATE'
            DataSource = EmployeeDM.DataSourceDetail
            DateValidation = True
            SaveTime = False
          end
          object DBCheckBoxCalcBonusYN: TDBCheckBox
            Tag = 6
            Left = 227
            Top = 106
            Width = 105
            Height = 17
            Caption = 'Efficiency bonus'
            Ctl3D = False
            DataField = 'CALC_BONUS_YN'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 7
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
          end
          object dxDBLookupEditShift: TdxDBLookupEdit
            Tag = 1
            Left = 110
            Top = 59
            Width = 226
            Style.BorderStyle = xbsSingle
            TabOrder = 2
            OnEnter = dxDBLookupEditShiftEnter
            OnKeyDown = dxDBLookupEditShiftKeyDown
            DataField = 'SHIFTLU'
            DataSource = EmployeeDM.DataSourceDetail
            OnMouseEnter = dxDBLookupEditShiftEnter
            DropDownWidth = 226
            ListFieldName = 'DESCRIPTION;SHIFT_NUMBER'
          end
          object DBCheckBoxBookProdHrs: TDBCheckBox
            Tag = 4
            Left = 112
            Top = 147
            Width = 233
            Height = 17
            Caption = 'Autom. scans based on standard planning'
            Ctl3D = False
            DataField = 'BOOK_PROD_HRS_YN'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 11
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
            OnClick = DBCheckBoxBookProdHrsClick
          end
          object DBEditExtRef: TDBEdit
            Left = 112
            Top = 147
            Width = 222
            Height = 19
            Ctl3D = False
            DataField = 'EXT_REF'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 12
          end
        end
      end
      object TabSheetAddress: TTabSheet
        Caption = 'Address'
        ImageIndex = 1
        object GroupBox3: TGroupBox
          Left = 0
          Top = 0
          Width = 281
          Height = 172
          Align = alLeft
          Caption = 'Employee'
          TabOrder = 0
          object Label23: TLabel
            Left = 8
            Top = 65
            Width = 26
            Height = 13
            Caption = 'State'
          end
          object Label7: TLabel
            Left = 8
            Top = 17
            Width = 39
            Height = 13
            Caption = 'Address'
          end
          object Label8: TLabel
            Left = 8
            Top = 41
            Width = 60
            Height = 13
            Caption = 'Zipcode/City'
          end
          object LabelEmail: TLabel
            Left = 8
            Top = 112
            Width = 63
            Height = 13
            Caption = 'EmailAddress'
          end
          object Label6: TLabel
            Left = 8
            Top = 87
            Width = 50
            Height = 13
            Caption = 'Telephone'
          end
          object DBEditAddress: TDBEdit
            Left = 86
            Top = 17
            Width = 182
            Height = 19
            Ctl3D = False
            DataField = 'ADDRESS'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
          end
          object DBEditZipcode: TDBEdit
            Left = 86
            Top = 40
            Width = 59
            Height = 19
            Ctl3D = False
            DataField = 'ZIPCODE'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 1
          end
          object DBEditCity: TDBEdit
            Left = 150
            Top = 40
            Width = 118
            Height = 19
            Ctl3D = False
            DataField = 'CITY'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 2
          end
          object DBEditEMail: TDBEdit
            Left = 86
            Top = 111
            Width = 182
            Height = 19
            Ctl3D = False
            DataField = 'EMAIL_ADDRESS'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 5
          end
          object DBEditState: TDBEdit
            Left = 86
            Top = 65
            Width = 182
            Height = 19
            Ctl3D = False
            DataField = 'STATE'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 3
          end
          object DBEditPhone: TDBEdit
            Left = 86
            Top = 86
            Width = 182
            Height = 19
            Ctl3D = False
            DataField = 'PHONE_NUMBER'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 4
          end
        end
        object GroupBox4: TGroupBox
          Left = 281
          Top = 0
          Width = 362
          Height = 172
          Align = alClient
          TabOrder = 1
          object Label4: TLabel
            Left = 8
            Top = 43
            Width = 61
            Height = 13
            Caption = 'Date of birth'
          end
          object Label12: TLabel
            Left = 8
            Top = 16
            Width = 89
            Height = 13
            Caption = 'Social sec. number'
          end
          object Label24: TLabel
            Left = 8
            Top = 69
            Width = 41
            Height = 13
            Caption = 'Remarks'
          end
          object Label15: TLabel
            Left = 8
            Top = 96
            Width = 41
            Height = 13
            Caption = 'Ethnicity'
          end
          object Label17: TLabel
            Left = 8
            Top = 120
            Width = 38
            Height = 13
            Caption = 'Job title'
          end
          object Label18: TLabel
            Left = 8
            Top = 144
            Width = 65
            Height = 13
            Caption = 'Marital status'
          end
          object DBEditSocialNumber: TDBEdit
            Left = 110
            Top = 15
            Width = 182
            Height = 19
            Ctl3D = False
            DataField = 'SOCIAL_SECURITY_NUMBER'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
          end
          object DBEdit10: TDBEdit
            Left = 110
            Top = 70
            Width = 219
            Height = 19
            Ctl3D = False
            DataField = 'REMARK'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 2
          end
          object dxDBDateEdit3: TdxDBDateEdit
            Left = 110
            Top = 43
            Width = 121
            TabOrder = 1
            DataField = 'DATE_OF_BIRTH'
            DataSource = EmployeeDM.DataSourceDetail
            DateValidation = True
            SaveTime = False
          end
          object DBEdit1: TDBEdit
            Left = 110
            Top = 94
            Width = 219
            Height = 19
            Ctl3D = False
            DataField = 'ETHNICITY'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 3
          end
          object DBEdit2: TDBEdit
            Left = 110
            Top = 118
            Width = 219
            Height = 19
            Ctl3D = False
            DataField = 'JOBTITLE'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 4
          end
          object DBEdit3: TDBEdit
            Left = 110
            Top = 142
            Width = 219
            Height = 19
            Ctl3D = False
            DataField = 'MARITALSTATE'
            DataSource = EmployeeDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 5
          end
        end
      end
      object TabSheetFreeText: TTabSheet
        Caption = 'Free Text'
        ImageIndex = 2
        object GroupBox5: TGroupBox
          Left = 0
          Top = 0
          Width = 651
          Height = 172
          Align = alLeft
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = 'Employee'
          TabOrder = 0
          object dxDBMemo1: TdxDBMemo
            Left = 6
            Top = 16
            Width = 638
            Style.Edges = [edgLeft, edgTop, edgRight, edgBottom]
            TabOrder = 0
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataField = 'FREETEXT'
            DataSource = EmployeeDM.DataSourceDetail
            ScrollBars = ssBoth
            Height = 149
          end
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 653
    Height = 149
    inherited spltDetail: TSplitter
      Top = 145
      Width = 651
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 651
      Height = 144
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Employees'
        end>
      KeyField = 'EMPLOYEE_NUMBER'
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        DisableEditor = True
        Width = 91
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumn9: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn8: TdxDBGridColumn
        Caption = 'Short name'
        DisableEditor = True
        Width = 56
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHORT_NAME'
      end
      object dxDetailGridColumn2: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 85
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDetailGridColumn3: TdxDBGridColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 94
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTDESC'
      end
      object dxDetailGridColumn6: TdxDBGridLookupColumn
        Caption = 'Contract'
        DisableEditor = True
        Width = 65
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACTLU'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Address'
        DisableEditor = True
        Width = 108
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Caption = 'Zipcode'
        DisableEditor = True
        Width = 44
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ZIPCODE'
      end
      object dxDetailGridColumn7: TdxDBGridLookupColumn
        Caption = 'Language'
        DisableEditor = True
        Width = 92
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LANGUAGELU'
      end
      object dxDetailGridColumn10: TdxDBGridColumn
        Caption = 'Sex'
        MinWidth = 16
        Visible = False
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SEX'
      end
      object dxDetailGridColumn11: TdxDBGridLookupColumn
        Caption = 'Team'
        Visible = False
        Width = 163
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TEAMLU'
      end
      object dxDetailGridColumn12: TdxDBGridDateColumn
        Caption = 'Start date '
        Visible = False
        Width = 57
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTDATE'
      end
      object dxDetailGridColumn13: TdxDBGridDateColumn
        Caption = 'End date'
        Visible = False
        Width = 99
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENDDATE'
      end
      object dxDetailGridColumn14: TdxDBGridCheckColumn
        Caption = 'Cut off time'
        Visible = False
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CUT_OF_TIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn15: TdxDBGridCheckColumn
        Caption = 'Is scanning'
        Visible = False
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IS_SCANNING_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn16: TdxDBGridCheckColumn
        Caption = 'First aid'
        Visible = False
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FIRSTAID_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn17: TdxDBGridDateColumn
        Caption = 'First aid exp date'
        Visible = False
        Width = 99
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FIRSTAID_EXP_DATE'
      end
      object dxDetailGridColumn18: TdxDBGridCheckColumn
        Caption = 'Efficency bonus'
        Visible = False
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CALC_BONUS_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn19: TdxDBGridColumn
        Caption = 'City'
        Visible = False
        Width = 163
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxDetailGridColumn20: TdxDBGridColumn
        Caption = 'State'
        Visible = False
        Width = 36
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STATE'
      end
      object dxDetailGridColumn21: TdxDBGridColumn
        Caption = 'Phone'
        Visible = False
        Width = 83
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE_NUMBER'
      end
      object dxDetailGridColumn22: TdxDBGridColumn
        Caption = 'Email address'
        Visible = False
        Width = 216
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMAIL_ADDRESS'
      end
      object dxDetailGridColumn23: TdxDBGridColumn
        Caption = 'Social security number'
        Visible = False
        Width = 109
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SOCIAL_SECURITY_NUMBER'
      end
      object dxDetailGridColumn24: TdxDBGridDateColumn
        Caption = 'Date of birth'
        Visible = False
        Width = 99
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_OF_BIRTH'
      end
      object dxDetailGridColumn25: TdxDBGridColumn
        Caption = 'Remark'
        Visible = False
        Width = 322
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REMARK'
      end
      object dxDetailGridColumn26: TdxDBGridColumn
        Caption = 'Shift'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHIFTDESC'
      end
      object dxDetailGridColumn27: TdxDBGridColumn
        Caption = 'ID'
        ReadOnly = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_ID'
      end
      object dxDetailGridColumn28: TdxDBGridColumn
        Caption = 'Ethnicity'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ETHNICITY'
      end
      object dxDetailGridColumn29: TdxDBGridColumn
        Caption = 'Job title'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'JOBTITLE'
      end
      object dxDetailGridColumn30: TdxDBGridColumn
        Caption = 'Marital status'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MARITALSTATE'
      end
      object dxDetailGridColumn31: TdxDBGridColumn
        Caption = 'External Reference'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXT_REF'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Top = 88
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 648
    Top = 88
  end
  inherited StandardMenuActionList: TActionList
    Top = 40
  end
  inherited dsrcActive: TDataSource
    Top = 216
  end
end
