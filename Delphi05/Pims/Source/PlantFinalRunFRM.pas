(*
  MRA:6-FEB-2014 20011800 Final Run System
  - New form that shows the final run info per plant.
  - It is opened via plant-form.
  MRA:28-APR-2014 20011800.70 Rework
  - Final Run System Rework
    - When Edit is not allowed based on menu-settings, it is still
      possible to edit the last-export-date of final run.
    - Reason: The Form_Edit_YN was not set, because this form is not a
              separate form and not selectable in menu. To solve it, it should
              use the Form_Edit_YN from calling form (Plant).
*)
unit PlantFinalRunFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, StdCtrls, Mask, DBCtrls,
  dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TPlantFinalRunF = class(TGridBaseF)
    dxDetailGridPLANT_CODE: TdxDBGridMaskColumn;
    dxDetailGridEXPORT_TYPE: TdxDBGridMaskColumn;
    dxDetailGridEXPORT_DATE: TdxDBGridDateColumn;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DBEditPlant: TDBEdit;
    DBEditPlantDesc: TDBEdit;
    Label9: TLabel;
    DBEditET: TDBEdit;
    Label1: TLabel;
    dxDBDateEditExportDate: TdxDBDateEdit;
    dxDetailGridPLANTLU: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function PlantFinalRunF: TPlantFinalRunF;

var
  PlantFinalRunF_HDN: TPlantFinalRunF;

implementation

{$R *.DFM}

uses
  SystemDMT, PlantDMT, UPimsMessageRes, UPimsConst;

function PlantFinalRunF: TPlantFinalRunF;
begin
  if (PlantFinalRunF_HDN = nil) then
    PlantFinalRunF_HDN := TPlantFinalRunF.Create(Application);
  Result := PlantFinalRunF_HDN;
end;

// Ensure the form stays on top of the calling form.
procedure TPlantFinalRunF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TPlantFinalRunF.FormDestroy(Sender: TObject);
begin
  inherited;
  PlantFinalRunF_HDN := nil;
end;

procedure TPlantFinalRunF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  dxDBDateEditExportDate.SetFocus;
end;

procedure TPlantFinalRunF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
  inherited;
  PlantFinalRunF_HDN := nil;
end;

procedure TPlantFinalRunF.FormCreate(Sender: TObject);
begin
  if dxDetailGrid.DataSource = nil then
    dxDetailGrid.DataSource := PlantDM.DataSourceFinalRun;
  inherited;
end;

procedure TPlantFinalRunF.FormShow(Sender: TObject);
begin
  inherited;
  if (Form_Edit_YN = ITEM_VISIBIL) then // 20011800.70
  begin
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    SetNotEditableNow; // 20011800.70
    Exit;
  end;
  dxDBDateEditExportDate.Enabled := SystemDM.IsAdmin;
  if SystemDM.IsAdmin then
  begin
    dxBarBDBNavPost.Visible := ivAlways;
    dxBarBDBNavCancel.Visible := ivAlways;
  end
  else
  begin
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
  end;
end;

end.
