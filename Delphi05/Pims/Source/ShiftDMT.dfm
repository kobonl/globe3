inherited ShiftDM: TShiftDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnFilterRecord = TableMasterFilterRecord
    IndexFieldNames = 'PLANT_CODE'
    TableName = 'PLANT'
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableMasterFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TableMasterINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TableMasterOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  inherited TableDetail: TTable
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    TableName = 'SHIFT'
    Top = 132
    object TableDetailSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailSTARTTIME1: TDateTimeField
      FieldName = 'STARTTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME1: TDateTimeField
      FieldName = 'ENDTIME1'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME2: TDateTimeField
      FieldName = 'STARTTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME2: TDateTimeField
      FieldName = 'ENDTIME2'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME3: TDateTimeField
      FieldName = 'STARTTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME3: TDateTimeField
      FieldName = 'ENDTIME3'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME4: TDateTimeField
      FieldName = 'STARTTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME4: TDateTimeField
      FieldName = 'ENDTIME4'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME5: TDateTimeField
      FieldName = 'STARTTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME5: TDateTimeField
      FieldName = 'ENDTIME5'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME6: TDateTimeField
      FieldName = 'STARTTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME6: TDateTimeField
      FieldName = 'ENDTIME6'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailSTARTTIME7: TDateTimeField
      FieldName = 'STARTTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailENDTIME7: TDateTimeField
      FieldName = 'ENDTIME7'
      DisplayFormat = 'hh:mm'
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourtype
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 30
      Lookup = True
    end
  end
  inherited TableExport: TTable
    Left = 324
    Top = 332
  end
  inherited DataSourceExport: TDataSource
    Left = 424
    Top = 332
  end
  object TableHourtype: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 88
    Top = 208
  end
end
