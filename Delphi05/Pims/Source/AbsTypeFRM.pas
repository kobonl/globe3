unit AbsTypeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, dxEditor, dxEdLib, dxDBELib;

type
  TAbsTypeF = class(TGridBaseF)
    LabelDescription: TLabel;
    DBEditDescription: TdxDBEdit;
    LabelExportCode: TLabel;
    DBEditExportCode: TDBEdit;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnExportCode: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function AbsTypeF: TAbsTypeF;

implementation

uses
  AbsTypeDMT;
{$R *.DFM}
var
  AbsTypeF_HND: TAbsTypeF;

function AbsTypeF: TAbsTypeF;
begin
  if (AbsTypeF_HND = nil) then
  begin
    AbsTypeF_HND := TAbsTypeF.Create(Application);
  end;
  Result := AbsTypeF_HND;
end;

procedure TAbsTypeF.FormDestroy(Sender: TObject);
begin
  inherited;
  AbsTypeF_HND:=Nil;
end;

procedure TAbsTypeF.FormCreate(Sender: TObject);
begin
  AbsTypeDM := CreateFormDM(TAbsTypeDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := AbsTypeDM.DataSourceDetail;
  inherited;
end;

procedure TAbsTypeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditDescription.SetFocus;
end;

end.
