(*
  MRA:6-NOV-2015 PIM-52
  - Work Schedule Functionality
  - The actual Processing of the work schedules to availability is done here.
  MRA:20-APR-2016 PIM-52.2 Bugfix
  - It did not work when employee contract began later then start-date of
    workschedule.
  - Check for the current employee contract and use the startdate/enddate
    during the process.
  MRA:28-NOV-2016 PIM-52.3
  - Work Schedules and check on absence
  - During process of work schedule, it should check on absence and give a
    message about that for the user to decide to overwrite it or not.
  MRA:16-OCT-2017 PIM-311
  - Check for Year! When target-year is different from start-date (reference
    date, then take the first week-day of target-year as start-date.
  MRA:7-MAY-2018 PIM-350
  - Illness-message and workschedule issue
  - When an illness-message is defined it should take that into account
    when processing workschedules.
  MRA:25-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:27-AUG-2018 GLOB3-142
  - Process workschedules does not use defined reference week 
*)
unit WorkScheduleProcessDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, Db, DBTables, StdCtrls;

type
  PTDayRecord = ^TDayRecord;
  TDayRecord = record
    AbsRsn: String;
    ShiftNumber: Integer;
    AType: Integer;
    Repeats: Integer;
    NextDay: Integer;
    MoveAbsRsn: String;
    MoveShiftNumber: Integer;
  end;

type
  PTLineRecord = ^TLineRecord;
  TLineRecord = record
    DayList: array[1..7] of PTDayRecord;
  end;

type
  TWorkScheduleProcessDM = class(TDataModule)
    qryWorkScheduleDetails: TQuery;
    qryCopyThis: TQuery;
    qryStandardAvailability: TQuery;
    qryShiftSchedule: TQuery;
    qryEmployeeAvailability: TQuery;
    qryEmployeePlanning: TQuery;
    qryEmpInfo: TQuery;
    qryEMAInsert: TQuery;
    qryEMAUpdate: TQuery;
    qrySHSMergeXXX: TQuery;
    qryEMPDelete: TQuery;
    qrySHSInsert: TQuery;
    qrySHSUpdate: TQuery;
    qryEMADelete: TQuery;
    qryIllnessMessage: TQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    LineList: TList;
    FToWeek: Integer;
    FFromWeek: Integer;
    FYear: Integer;
    FqryEmployee: TQuery;
    FMemo1: TMemo;
    FPlanningMode: Integer;
    procedure BuildList(AEmployeeNumber: Integer; var AStartDate: TDateTime);
    procedure ClearLineList;
    function FindNextDay(var ALineRecord: PTLineRecord;
      ANextDay: Integer; ADay: Integer;
      AAbsRsn: String; AShiftNumber: Integer): Integer;
    procedure DetermineMoveDays(var ALineRecord: PTLineRecord);
    procedure WLog(AMsg: String);
    procedure WErrorLog(AMsg: String);
  public
    { Public declarations }
    procedure ProcessWorkScheduleEmployee(AEmployeeNumber: Integer);
    procedure ProcessWorkSchedule;
    function ShiftScheduleHandling(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime; APlantCode: String;
      AShiftNumber: Integer): Boolean;
    function EmployeeAvailabilityHandling(AEmployeeNumber: Integer;
      AEmployeeAvailabilityDate: TDateTime; APlantCode: String;
      AShiftNumber: Integer;
      AAbsenceReason,
      AAV1, AAV2, AAV3, AAV4, AAV5, AAV6, AAV7, AAV8, AAV9, AAV10: String;
      var ADlgMsgResult: Integer): Boolean;
    function EmployeePlanningHandling(APlantCode: String;
      AEmployeePlanningDate: TDateTime; AShiftNumber: Integer;
      AEmployeeNumber: Integer): Boolean;
    property Year: Integer read FYear write FYear;
    property FromWeek: Integer read FFromWeek write FFromWeek;
    property ToWeek: Integer read FToWeek write FToWeek;
    property qryEmployee: TQuery read FqryEmployee write FqryEmployee;
    property Memo1: TMemo read FMemo1 write FMemo1;
    property PlanningMode: Integer read FPlanningMode write FPlanningMode;
  end;

var
  WorkScheduleProcessDM: TWorkScheduleProcessDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UGlobalFunctions, UPimsConst, UPimsMessageRes;

{ TWorkScheduleProcessDM }

function TWorkScheduleProcessDM.ShiftScheduleHandling(
  AEmployeeNumber: Integer; AShiftScheduleDate: TDateTime;
  APlantCode: String; AShiftNumber: Integer): Boolean;
begin
  Result := False;
  try
    if qryShiftSchedule.Locate('SHIFT_SCHEDULE_DATE;SHIFT_NUMBER',
      VarArrayOf([AShiftScheduleDate, AShiftNumber]), []) then
      Result := True
    else
    begin
//      WLog('Updating Shift Schedule for Date ' +
//        DateToStr(AShiftScheduleDate) + ' and shift ' + IntToStr(AShiftNumber));
      if qryShiftSchedule.Locate('SHIFT_SCHEDULE_DATE',
        AShiftScheduleDate, []) then
      begin
        // Update
        with qrySHSUpdate do
        begin
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
          ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
          Result := True;
        end;
      end
      else
      begin
        // Insert
        with qrySHSInsert do
        begin
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
          ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
          Result := True;
        end;
      end;
    end;
  except
    on E:EDBEngineError do
      if not (E.Errors[0].ErrorCode = PIMS_DBIERR_KEYVIOL) then // duplicate value
        WErrorLog(E.Message);
    on E:EDatabaseError do
      WErrorLog(E.Message);
  end;
end; // ShiftScheduleHandling

function TWorkScheduleProcessDM.EmployeeAvailabilityHandling(
  AEmployeeNumber: Integer; AEmployeeAvailabilityDate: TDateTime;
  APlantCode: String; AShiftNumber: Integer; AAbsenceReason, AAV1, AAV2,
  AAV3, AAV4, AAV5, AAV6, AAV7, AAV8, AAV9, AAV10: String;
  var ADlgMsgResult: Integer): Boolean;
var
  IllnessMessageFound: Boolean;
  // PIM-52.3.
  function CheckAbsence(AAvail: String): Boolean;
  var
    mySet: Set Of Char;
  begin
    mySet := ['*', '-', ' '];
    Result := False;
    if AAvail <> '' then
      if not (AAvail[1] in mySet) then
        Result := True;
  end;
  // PIM-350
  function CheckAvailOrAbsence(AAvail: String): Boolean;
  var
    mySet: Set Of Char;
  begin
    mySet := ['*'];
    Result := False;
    if AAvail <> '' then
      if (AAvail[1] in mySet) or CheckAbsence(AAvail) then
        Result := True;
  end;
  // PIM-52.3.
  function AvailString(A1, A2, A3, A4, A5, A6, A7, A8, A9, A10: String): String;
  begin
    Result := '';
    if A1 <> '' then
      Result := Result + '[' + A1 + ']';
    if A2 <> '' then
      Result := Result + ' [' + A2 + ']';
    if A3 <> '' then
      Result := Result + ' [' + A3 + ']';
    if A4 <> '' then
      Result := Result + ' [' + A4 + ']';
    if A5 <> '' then
      Result := Result + ' [' + A5 + ']';
    if A6 <> '' then
      Result := Result + ' [' + A6 + ']';
    if A7 <> '' then
      Result := Result + ' [' + A7 + ']';
    if A8 <> '' then
      Result := Result + ' [' + A8 + ']';
    if A9 <> '' then
      Result := Result + ' [' + A9 + ']';
    if A10 <> '' then
      Result := Result + ' [' + A10 + ']';
  end;
  // PIM-350
  function CheckIllnessMessage(AAvail: String): String;
  var
    DateTo: TDateTime;
  begin
    Result := '';
    with qryIllnessMessage do
    begin
      First;
      while (not Eof) and (Result = '') do
      begin
        if FieldByName('ILLNESSMESSAGE_ENDDATE').Value = Null then
          DateTo := EncodeDate(2099, 12, 31)
        else
          DateTo := FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
        if (AEmployeeAvailabilityDate >= FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime) and
          (AEmployeeAvailabilityDate <= DateTo)
          then
          Result := FieldByName('ABSENCEREASON_CODE').AsString;
        Next;
      end;
      if Result = '' then
        Result := AAvail
      else
        if CheckAvailOrAbsence(AAvail) then
          IllnessMessageFound := True
        else
          Result := AAvail;
    end;
  end; // CheckIllnessMessage
begin
  Result := False;
  if AAV1 = '*' then
    AAV1 := AAbsenceReason;
  if AAV2 = '*' then
    AAV2 := AAbsenceReason;
  if AAV3 = '*' then
    AAV3 := AAbsenceReason;
  if AAV4 = '*' then
    AAV4 := AAbsenceReason;
  if AAV5 = '*' then
    AAV5 := AAbsenceReason;
  if AAV6 = '*' then
    AAV6 := AAbsenceReason;
  if AAV7 = '*' then
    AAV7 := AAbsenceReason;
  if AAV8 = '*' then
    AAV8 := AAbsenceReason;
  if AAV9 = '*' then
    AAV9 := AAbsenceReason;
  if AAV10 = '*' then
    AAV10 := AAbsenceReason;
  // PIM-350
  IllnessMessageFound := False;
  AAV1 := CheckIllnessMessage(AAV1);
  AAV2 := CheckIllnessMessage(AAV2);
  AAV3 := CheckIllnessMessage(AAV3);
  AAV4 := CheckIllnessMessage(AAV4);
  AAV5 := CheckIllnessMessage(AAV5);
  AAV6 := CheckIllnessMessage(AAV6);
  AAV7 := CheckIllnessMessage(AAV7);
  AAV8 := CheckIllnessMessage(AAV8);
  AAV9 := CheckIllnessMessage(AAV9);
  AAV10 := CheckIllnessMessage(AAV10);
  try
    if qryEmployeeAvailability.Locate('EMPLOYEEAVAILABILITY_DATE;SHIFT_NUMBER',
      VarArrayOf([AEmployeeAvailabilityDate, AShiftNumber]), []) then
    begin
      if not IllnessMessageFound then // PIM-350
      begin
        // PIM-52.3.
        if (ADlgMsgResult <> mrAll) and (ADlgMsgResult <> mrNoToAll) then
        begin
          // PIM-52.3.
          if CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString) or
            CheckAbsence(qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString) then
          begin
            ADlgMsgResult := DisplayMessage4Buttons(
              SEMAOverwrite + IntToStr(AEmployeeNumber)+ ' ' +
              SEMAExists + #13 +
              DateToStr(AEmployeeAvailabilityDate) + ': ' +
              AvailString(
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString,
              qryEmployeeAvailability.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString)
              );
          end;
        end;
      end;
      if (ADlgMsgResult = mrAll) or (ADlgMsgResult = mrYes) or
        (ADlgMsgResult = 0) then
      begin
        // Update Employee Availability
        with qryEMAUpdate do
        begin
//          Wlog('Update Employee Availability for plant ' + APlantCode +
//            ' and employee ' + IntToStr(AEmployeeNumber) +
//            ' and date ' + DateToStr(AEmployeeAvailabilityDate) + ' and shift ' +
//            IntToStr(AShiftNumber) + ' AV1=' + AAV1 + ' AV2=' + AAV2 +
//            ' AV3=' + AAV3 + ' AV4=' + AAV4);
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := AEmployeeAvailabilityDate;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ParamByName('AVAILABLE_TIMEBLOCK_1').AsString := AAV1;
          ParamByName('AVAILABLE_TIMEBLOCK_2').AsString := AAV2;
          ParamByName('AVAILABLE_TIMEBLOCK_3').AsString := AAV3;
          ParamByName('AVAILABLE_TIMEBLOCK_4').AsString := AAV4;
          ParamByName('AVAILABLE_TIMEBLOCK_5').AsString := AAV5;
          ParamByName('AVAILABLE_TIMEBLOCK_6').AsString := AAV6;
          ParamByName('AVAILABLE_TIMEBLOCK_7').AsString := AAV7;
          ParamByName('AVAILABLE_TIMEBLOCK_8').AsString := AAV8;
          ParamByName('AVAILABLE_TIMEBLOCK_9').AsString := AAV9;
          ParamByName('AVAILABLE_TIMEBLOCK_10').AsString := AAV10;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end;
        Result := True;
      end; // if (DlgMsgResult = mrAll)
    end // if exists
    else
    begin // if exists
      ADlgMsgResult := 0; // PIM-52.3.
      // Insert Employee Availability
      with qryEMAInsert do
      begin
//        Wlog('Insert Employee Availability for plant ' + APlantCode +
//          ' and employee ' + IntToStr(AEmployeeNumber) +
//          ' and date ' + DateToStr(AEmployeeAvailabilityDate) + ' and shift ' +
//          IntToStr(AShiftNumber) + ' AV1=' + AAV1 + ' AV2=' + AAV2 +
//          ' AV3=' + AAV3 + ' AV4=' + AAV4);
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := AEmployeeAvailabilityDate;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
        ParamByName('AVAILABLE_TIMEBLOCK_1').AsString := AAV1;
        ParamByName('AVAILABLE_TIMEBLOCK_2').AsString := AAV2;
        ParamByName('AVAILABLE_TIMEBLOCK_3').AsString := AAV3;
        ParamByName('AVAILABLE_TIMEBLOCK_4').AsString := AAV4;
        ParamByName('AVAILABLE_TIMEBLOCK_5').AsString := AAV5;
        ParamByName('AVAILABLE_TIMEBLOCK_6').AsString := AAV6;
        ParamByName('AVAILABLE_TIMEBLOCK_7').AsString := AAV7;
        ParamByName('AVAILABLE_TIMEBLOCK_8').AsString := AAV8;
        ParamByName('AVAILABLE_TIMEBLOCK_9').AsString := AAV9;
        ParamByName('AVAILABLE_TIMEBLOCK_10').AsString := AAV10;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
      end;
      Result := True;
    end; // if exists
    // PIM-52.3.
    if (ADlgMsgResult = mrAll) or (ADlgMsgResult = mrYes) or
      (ADlgMsgResult = 0) then
    begin
      // Delete any Employee Available <> AShiftNumber
      with qryEMADelete do
      begin
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := AEmployeeAvailabilityDate;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
        ExecSQL;
      end;
    end;
  except
    on E:EDBEngineError do
      if not (E.Errors[0].ErrorCode = PIMS_DBIERR_KEYVIOL) then // duplicate value
        WErrorLog(E.Message);
    on E:EDatabaseError do
      WErrorLog(E.Message);
  end;
end; // EmployeeAvailabilityHandling

function TWorkScheduleProcessDM.EmployeePlanningHandling(
  APlantCode: String; AEmployeePlanningDate: TDateTime; AShiftNumber,
  AEmployeeNumber: Integer): Boolean;
begin
  Result := False; // No planning
  try
    if qryEmployeePlanning.Locate('EMPLOYEEPLANNING_DATE;SHIFT_NUMBER',
      VarArrayOf([AEmployeePlanningDate, AShiftNumber]), []) then
    begin
      if PlanningMode = 0 then // Do not process when there is planning
      begin
        WLog(SPimsPlanningMessage1 + ' ' + IntToStr(AEmployeeNumber) +
          ' ' + SPimsPlanningMessage2 + ' ' +
          DateToStr(AEmployeePlanningDate) + ' ' + SPimsPlanningMessage3);
        Result := True // There is planning
      end
      else
      begin
        with qryEMPDelete do
        begin
//        WLog('Delete Employee Planning for plant ' + APlantCode +
//          ' and employee ' + IntToStr(AEmployeeNumber) +
//          ' and date ' + DateToStr(AEmployeePlanningDate) +
//          ' and shift ' + IntToStr(AShiftNumber));
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime := AEmployeePlanningDate;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ExecSQL;
          Result := False; // No planning (has been deleted)
        end;
      end;
    end;
  except
    on E:EdbEngineError do
      WErrorLog(E.Message);
  end;
end; // EmployeePlanningHandling

procedure TWorkScheduleProcessDM.ProcessWorkScheduleEmployee(
  AEmployeeNumber: Integer);
var
  StartDate: TDateTime;
  Week: Integer;
  Day: Integer;
  DayStr: String;
  Line: Integer;
  StartYear, StartWeek: Word;
  DateLoop: TDateTime;
  CurrentDate: TDateTime;
  AbsRsn: String;
  ShiftNumber: Integer;
//  AType: Integer;
//  Repeats: Integer;
  LineRecord: PTLineRecord;
  DayRecord: PTDayRecord;
  PlantCode: String;
  ECStartDate, ECEndDate: TDateTime; // Employee Contract StartDate/EndDate
  DlgMsgResult: Integer; // PIM-52.3.
  SDYear, SDWeek, MaxWeek, LoopYear: Word;
begin
  try
    LineList := TList.Create;
    DlgMsgResult := 0;
    StartDate := 0;
    BuildList(AEmployeeNumber, StartDate);
    if StartDate = 0 then
      Exit;

    // Employee plant
    // We need to know the plant_code of the employee
    // Also: We need to know the start + enddate of current employee contract
    PlantCode := '1';
    qryEmpInfo.Close;
    qryEmpInfo.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryEmpInfo.Open;
    if not qryEmpInfo.Eof then
    begin
      PlantCode := qryEmpInfo.FieldByName('PLANT_CODE').AsString;
      ECStartDate := qryEmpInfo.FieldByName('STARTDATE').AsDateTime;
      ECEndDate := qryEmpInfo.FieldByName('ENDDATE').AsDateTime;
    end
    else
    begin
      WLog(SPimsNoCurrentEmpContFound + ' ' + IntToStr(AEmployeeNumber));
      Exit;
    end;

    WLog(SPimsProcessWorkScheduleMessage1 + ' ' + IntToStr(AEmployeeNumber) +
      ' ' + SPimsProcessWorkScheduleMessage2 + ' ' + DateToStr(StartDate) + '.'
      );
      
    // Standard availability
    // Get all needed data for the employee (all shifts) for later use
    // We need this to know the available-timeblocks (max. 4) that need
    // to be filled.
    qryStandardAvailability.Close;
    qryStandardAvailability.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryStandardAvailability.Open;

    // Shift Schedule
    // Get all defined shift schedules for employee and >= StartDate
    // We need this to know if a shift-schedule exists. If not then
    // the shift-schedule must be updated to the new shift (old shift ->
    // new shift), or it must be added (no shift-schedule-record could be found).
    qryShiftSchedule.Close;
    qryShiftSchedule.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryShiftSchedule.ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := StartDate;
    qryShiftSchedule.Open;

    // Employee Availability
    // Get all defined employee availability records for employee and >= StartDate
    // It is possible these must be updated to the new values.
    // When not exist, they must be added.
    qryEmployeeAvailability.Close;
    qryEmployeeAvailability.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryEmployeeAvailability.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := StartDate;
    qryEmployeeAvailability.Open;

    // Employee Planning
    // Any existing planning must be deleted
    qryEmployeePlanning.Close;
    qryEmployeePlanning.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryEmployeePlanning.ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime := StartDate;
    qryEmployeePlanning.Open;

    // Illness Message
    qryIllnessMessage.Close;
    qryIllnessMessage.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryIllnessMessage.ParamByName('ILLNESSMESSAGE_STARTDATE').AsDateTime := StartDate;
    qryIllnessMessage.Open;

    ListProcsF.WeekUitDat(StartDate, StartYear, StartWeek);
    DateLoop := StartDate;
    Line := 0;
    SDYear := StartYear;
    SDWeek := StartWeek;
    for LoopYear := SDYear to Self.Year do // GLOB3-142
    begin
      MaxWeek := ListProcsF.WeeksInYear(LoopYear);
      if (LoopYear = Self.Year) then
        MaxWeek := ToWeek;
      // Now process the availability
      for Week := SDWeek to MaxWeek do
      begin
        LineRecord := LineList.Items[Line];
        if Line < LineList.Count - 1 then
          inc(Line)
        else
          Line := 0;
        // First determine Move Days
        DetermineMoveDays(LineRecord);
        // Now process the week
        for Day := 1 to 7 do
        begin
          DayRecord := LineRecord.DayList[Day];
          CurrentDate := DateLoop + Day - 1;
          DayStr := 'DAY' + IntToStr(Day);
          AbsRsn := DayRecord.AbsRsn;
          ShiftNumber := DayRecord.ShiftNumber;
//        AType := DayRecord.AType;
//        Repeats := DayRecord.Repeats;

        // Assign moving data here
          if Dayrecord.MoveAbsRsn <> '' then
          begin
            AbsRsn := DayRecord.MoveAbsRsn;
            ShiftNumber := DayRecord.MoveShiftNumber;
          end;

          if (LoopYear = Self.Year) and
            ((Week >= FromWeek) and (Week <= ToWeek)) and
            ((CurrentDate >= ECStartDate) and
            (CurrentDate <= ECEndDate)) // Check with current employee contract
            then
          begin
{
WLog('E=' + IntToStr(AEmployeeNumber) +
  ' W=' + IntToStr(Week) +
  ' D=' + IntToStr(Day) +
  ' CurrentDate=' + DateToStr(CurrentDate) +
  ' NextDay=' + IntToStr(DayRecord.NextDay) +
  ' Line=' + IntToStr(Line) +
  ' Abs=' + AbsRsn +
  ' Shft=' + IntToStr(ShiftNumber) +
  ' Type=' + IntToStr(AType) +
  ' Rpt=' + IntToStr(Repeats)
  );
}
            // Standard Availability must exist to know what time-blocks
            // should be filled.
            if qryStandardAvailability.Locate('DAY_OF_WEEK;SHIFT_NUMBER',
              VarArrayOf([Day, ShiftNumber]), []) then
            begin
              // Employee Planning
              if not EmployeePlanningHandling(PlantCode, CurrentDate,
                ShiftNumber, AEmployeeNumber) then
              begin
                // Shift Schedule
                ShiftScheduleHandling(AEmployeeNumber, CurrentDate,
                  PlantCode, ShiftNumber);
                // Employee Availability
                EmployeeAvailabilityHandling(AEmployeeNumber,
                  CurrentDate, PlantCode, ShiftNumber,
                  AbsRsn,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString,
                  qryStandardAvailability.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString,
                  DlgMsgResult);
              end;
            end
            else
              WLog(SPimsNoStandAvailFound1 + ' ' +
                IntToStr(AEmployeeNumber) + ' ' + SPimsNoStandAvailFound2 + ' ' +
                IntToStr(ShiftNumber) + '!');
          end; // if Week
        end; // for Day
        DateLoop := DateLoop + 7;
      end; // for Week
      SDWeek := 1;
    end; // for LoopYear
  finally
    ClearLineList;
    LineList.Free;
//    if StartDate <> 0 then
//      WLog('-> Process Workschedule E=' + IntToStr(AEmployeeNumber) + ' (END)');
  end;
end; // ProcessWorkScheduleEmployee

procedure TWorkScheduleProcessDM.ProcessWorkSchedule;
begin
  try
    if not qryEmployee.IsEmpty then
    begin
      qryEmployee.First;
      while not qryEmployee.Eof do
      begin
        // Determine Employee
        ProcessWorkScheduleEmployee(
          qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger);
        Application.ProcessMessages;
        qryEmployee.Next;
      end; // while
    end; // if
  finally
  end;
end; // ProcessWorkSchedule

procedure TWorkScheduleProcessDM.ClearLineList;
var
  I: Integer;
  Day: Integer;
  LineRecord: PTLineRecord;
  DayRecord: PTDayRecord;
begin
  for I := LineList.Count - 1 downto 0 do
  begin
    LineRecord := LineList.Items[I];
    for Day := 1 to 7 do
    begin
      DayRecord := LineRecord.DayList[Day];
      Dispose(DayRecord);
    end;
    LineList.Remove(LineRecord);
    Dispose(LineRecord);
  end;
  LineList.Clear;
end; // CLearLineList

procedure TWorkScheduleProcessDM.BuildList(AEmployeeNumber: Integer;
  var AStartDate: TDateTime);
var
  Day: Integer;
  LineRecord: PTLineRecord;
  DayRecord: PTDayRecord;
  DayStr: String;
begin
  try
    // Determine Workschedule + Details based on Employee Contract
    qryWorkScheduleDetails.Close;
    qryWorkScheduleDetails.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryWorkScheduleDetails.Open;
    while not qryWorkScheduleDetails.Eof do
    begin
      LineRecord := new(PTLineRecord);
      AStartDate := qryWorkScheduleDetails.FieldByName('STARTDATE').AsDateTime;
      // PIM-311 Check for Year !
      // If year is different from source-year, then change it!
      // StartDate is the reference data of the shift schedule.
      // For example: 2-1-2017 (first week of year starting at monday)
      // When Year (entered in Emplyee/Staff-Availability) is higher like 2018
      // then it is not in the same year!
      // In that case take the firs week-day of that year
      // GLOB3-142 Do no change StartDate !
(*      if AStartDate <> StartOfYear(Self.Year) then
        AStartDate := StartOfYear(Self.Year); // This returns the first week-day of year. *)
      // Keep track of the move-data and put it in a list for later use
      for Day := 1 to 7 do
      begin
        DayRecord := new(PTDayRecord);
        DayStr := 'DAY' + IntToStr(Day);
        DayRecord.AbsRsn :=
          qryWorkScheduleDetails.FieldByName(DayStr + '_ABSRSN_CODE').AsString;
        DayRecord.ShiftNumber :=
          qryWorkScheduleDetails.FieldByName(DayStr + '_SHIFT_NUMBER').AsInteger;
        DayRecord.AType :=
          qryWorkScheduleDetails.FieldByName(DayStr + '_TYPE').AsInteger;
        DayRecord.Repeats :=
          qryWorkScheduleDetails.FieldByName(DayStr + '_REPEATS').AsInteger;
        DayRecord.NextDay := 0;
        DayRecord.MoveAbsRsn := '';
        DayRecord.MoveShiftNumber := 1;
        LineRecord.DayList[Day] := DayRecord;
      end; // for
      LineList.Add(LineRecord);
      qryWorkScheduleDetails.Next;
    end; // while
  finally
  end;
end; // BuildList

// Find Next Day within the week
function TWorkScheduleProcessDM.FindNextDay(var ALineRecord: PTLineRecord;
  ANextDay: Integer; ADay: Integer;
  AAbsRsn: String; AShiftNumber: Integer): Integer;
var
  Day, NewDay: Integer;
  DayRecord: PTDayRecord;
  DayFound: Boolean;
  function NextDayCheck(ATestDay: Integer): Boolean;
  var
    I: Integer;
    TestDayRecord: PTDayRecord;
  begin
    Result := False;
    for I := 1 to 7 do
    begin
      TestDayRecord := ALineRecord.DayList[I];
      if TestDayRecord.NextDay = ATestDay then
      begin
        Result := True;
        Break;
      end;
    end;
  end; // NextDayCheck
begin
  NewDay := ANextDay;
  // Init: First NextDay is same as MoveDay
  if NewDay = 0 then
  begin
    DayRecord := ALineRecord.DayList[ADay];
    DayRecord.MoveAbsRsn := AAbsRsn;
    DayRecord.MoveShiftNumber := AShiftNumber;
    Result := ADay;
    Exit;
  end;
  // Search for a NextDay
  // Look for a later day in the week
  DayFound := False;
  for Day := NewDay + 1 to 7 do
  begin
    DayRecord := ALineRecord.DayList[Day];
    if (DayRecord.MoveAbsRsn = '') then
      if not (NextDayCheck(Day)) then
        if (DayRecord.AbsRsn = '*') or (Day = ADay) then
        begin
          DayRecord.MoveAbsRsn := AAbsRsn;
          DayRecord.MoveShiftNumber := AShiftNumber;
          DayFound := True;
          NewDay := Day;
          Break;
        end;
  end;
  if not DayFound then
  begin
    // Look from start of week to NewDay
    for Day := 1 to NewDay do
    begin
      DayRecord := ALineRecord.DayList[Day];
      if (DayRecord.MoveAbsRsn = '') then
        if not (NextDayCheck(Day)) then
          if (DayRecord.AbsRsn = '*') or (Day = ADay) then
          begin
            DayRecord.MoveAbsRsn := AAbsRsn;
            DayRecord.MoveShiftNumber := AShiftNumber;
            DayFound := True;
            NewDay := Day;
            Break;
          end;
    end;
  end;
  if not DayFound then
    NewDay := ADay;
  if NewDay <> ADay then
  begin
    DayRecord := ALineRecord.DayList[ADay];
    DayRecord.MoveAbsRsn := '*';
    DayRecord.MoveShiftNumber := AShiftNumber;
  end
  else
  begin
    DayRecord := ALineRecord.DayList[ADay];
    DayRecord.MoveAbsRsn := AAbsRsn;
    DayRecord.MoveShiftNumber := AShiftNumber;
  end;
  if DayFound then
    Result := NewDay
  else
    Result := ANextDay;
end; // FindNextDate

// Determine moving-days information. When a reason code should be moved
// within the week, we assign the 'MoveAbsRsn' for DayRecord to know
// if that is moving-reason-code.
procedure TWorkScheduleProcessDM.DetermineMoveDays(
  var ALineRecord: PTLineRecord);
var
  DayRecord: PTDayRecord;
  Day: Integer;
begin
  // Init Move AbsRsn
  for Day := 1 to 7 do
  begin
    DayRecord := ALineRecord.DayList[Day];
    DayRecord.MoveAbsRsn := '';
    DayRecord.MoveShiftNumber := 1;
  end;
  // Determine Next-Days for Move-Days before the week is processed!
  for Day := 1 to 7 do
  begin
    DayRecord := ALineRecord.DayList[Day];
    if DayRecord.AType = 2 then
    begin
      DayRecord.NextDay := FindNextDay(ALineRecord, DayRecord.NextDay, Day,
        DayRecord.AbsRsn, DayRecord.ShiftNumber);
    end;
  end;
{
  // Test
  for Day := 1 to 7 do
  begin
    DayRecord := ALineRecord.DayList[Day];
    WLog('-> Day=' + IntToStr(Day) +
      ' NextDay=' + IntTostr(DayRecord.NextDay) +
      ' MoveAbsRsn=' + DayRecord.MoveAbsrsn);
  end;
}  
end; // DetermineMoveDays

procedure TWorkScheduleProcessDM.WLog(AMsg: String);
begin
  if Assigned(Memo1) then
    Memo1.Lines.Add(AMsg);
  UGLobalFunctions.WLog(AMsg);
end;

procedure TWorkScheduleProcessDM.WErrorLog(AMsg: String);
begin
  if Assigned(Memo1) then
    Memo1.Lines.Add(AMsg);
  UGLobalFunctions.WErrorLog(AMsg);
end;

procedure TWorkScheduleProcessDM.DataModuleCreate(Sender: TObject);
begin
  Memo1 := nil;
  PlanningMode := 0;
end;

end.
