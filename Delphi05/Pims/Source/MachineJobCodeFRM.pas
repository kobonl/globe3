(*
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machine-jobcodes-dialog.
*)
unit MachineJobCodeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEdLib, dxDBELib, dxEditor, dxExEdtr, DBCtrls,
  StdCtrls, Mask, dxDBTLCl, dxGrClms, dxDBEdtr;

type
  TMachineJobCodeF = class(TGridBaseF)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    DBEditJobCodes: TDBEdit;
    DBEditDescription: TDBEdit;
    dxDBDateEditInactiveDate: TdxDBDateEdit;
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridLookupColumn;
    dxMasterGridColumn4: TdxDBGridLookupColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridLookupColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxMasterGridColumn5: TdxDBGridCheckColumn;
    dxMasterGridColumn6: TdxDBGridCheckColumn;
    dxMasterGridColumn7: TdxDBGridCheckColumn;
    dxMasterGridColumn8: TdxDBGridDateColumn;
    dxMasterGridColumn9: TdxDBGridCheckColumn;
    dxMasterGridColumn10: TdxDBGridCheckColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxMasterGridColumn11: TdxDBGridCheckColumn;
    dxMasterGridColumn12: TdxDBGridLookupColumn;
    Label8: TLabel;
    DBEditPlant: TDBEdit;
    Label9: TLabel;
    DBEditWK: TDBEdit;
    DBEditPlantDesc: TDBEdit;
    DBEditWKDesc: TDBEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dxDBMaskEdit1: TdxDBMaskEdit;
    dxDBMaskEdit2: TdxDBMaskEdit;
    dxDBMaskEdit3: TdxDBMaskEdit;
    DBCheckBox1: TDBCheckBox;
    dxDetailGridColumn9: TdxDBGridDateColumn;
    DBCheckBox2: TDBCheckBox;
    DBLookupComboBoxBusinessUnit: TDBLookupComboBox;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MachineJobCodeF: TMachineJobCodeF;

var
  MachineJobCodeF_HDN: TMachineJobCodeF;

implementation

{$R *.DFM}

uses
  SystemDMT, MachineDMT, UPimsMessageRes, UPimsConst{, MachineFRM};

function MachineJobCodeF: TMachineJobCodeF;
begin
  if (MachineJobCodeF_HDN = nil) then
    MachineJobCodeF_HDN := TMachineJobCodeF.Create(Application);
  Result := MachineJobCodeF_HDN;
end;

procedure TMachineJobCodeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditJobCodes.SetFocus;
end;

procedure TMachineJobCodeF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditJobCodes.SetFocus;
end;

procedure TMachineJobCodeF.FormDestroy(Sender: TObject);
begin
  inherited;
  MachineJobCodeF_HDN := nil;
end;

procedure TMachineJobCodeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
(*  if (dxDetailGrid.Items[0].Values[0] = NULL ) then
  begin
    DisplayMessage(SPimsJobCodeWorkSpot, mtError, [mbOk]);
    Abort;
  end; *)
  inherited;
  MachineJobCodeF_HDN := nil;

//  MachineF.SetGridForm(MachineF);
end;

procedure TMachineJobCodeF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  if not(MachineDM.TableJobCode.State in [dsInsert, dsEdit]) then
    MachineDM.TableJobCode.Edit;

  MachineDM.TableJobCode.Post;
end;

procedure TMachineJobCodeF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  if (MachineDM.DataSourceJobCode.DataSet.State in [dsInsert]) then
    Exit;
end;

procedure TMachineJobCodeF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  inherited;
  MachineDM.TableJobCode.Delete;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TMachineJobCodeF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
