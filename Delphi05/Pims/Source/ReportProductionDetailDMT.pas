(*
  Changes:
    MRA: 05-MAY-2008 RV006.
      Added 'Order By' for all queries, otherwise sorting can go wrong under
      Oracle 10.
    MRA: 24-FEB-2009 RV022.
      Team-selection is now only on DEPARTMENTPERTEAM, but it must be
      on EMPLOYEE.TEAM_CODE-level !
      This can only be done when a query is also getting employee information:
      - qryMain is changed.
    MRA: 30-NOV-2009 RV045.1.
      - Time For Time and Bonus In Money can be on ContractGroup-level
        or HourType-level.
    MRA:20-MAY-2010 RV063.2. 889997.
    - Addition of shift selection. Search also on Shift in Production Quantity.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed dialogs in Pims, instead of
    open it as a modal dialog.
  - Because of embedding, a different var must be used for
    ReportProductionDetailDM -> Use ReportProductionDetailDM2 for a
    report that needs this DataModule to prevent problems when
    switching to 'Report Production Details'.
    Reason: It is possible one dialog is freeing this DataModule that
    another dialog had created.
  MRA:2-MAY-2011 RV092.3. Bugfix.
  - Leave out all 'with ReportProductionDetailDM' or
    ReportProductionDetailDM.nnn constructions, to
    prevent an access violation.
    Reason: It is possible another variable then 'ReportProductionDetailDM'
            is used.
  MRA:4-MAY-2012 20012858 Bugfix.
  - When there were 2 jobs on different shifts for 1 workspot, it showed
    them twice, resulting in double pieces. Reason: It did not add the shift
    in the cdsPieces that stores the pieces.
  MRA:8-MAY-2012 20012858. Production Reports and NOJOB.
  - Filter out the NOJOB-job to prevent wrong quantities when
    this job has a negative quantity.
  MRA:12-JUL-2012 20012858.130. Bugfix.
  - This report gives no hours for employees when shifts do not match.
    There is a shift on productionquantity-level and a shift
    on scan-level, but when they do not match, then
    it will not show hours (based on scans).
  - To solve this, the shift must always be based on scans.
    Just ignore the shifts from the productionquantity.
  - Addition of ShowShifts-boolean. This determines if shifts must be
    shown or not in report.
  MRA:23-JUL-2012. 20012858.130.2. Bugfix.
  - Report Production Details
    - This gives wrong (higher) quantities (compared with
      production report).
    - Compared with RV065. There was a field missing in an
      order by, for qryMain! (START_DATE).
  MRA:23-JUL-2012 20012858.130.3. Change.
  - Report Production Details
    - Add option to show compare jobs or not using
      a checkbox.
      Note: A compare job is a job where field COMPARATION_JOB_YN
      is set to 'Y'.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - When a job (JOBCODE-table) has field IGNOREQUANTITIESINREPORTS_YN set
    to 'Y' then the report should show no quantities for that job, only
    the time must be shown.
  MRA:8-OCT-2012 20013489 Overnight-shift-system
  - Filter on SHIFT_DATE to make it possible to see info about a whole
    (overnight-) shift.
  MRA:8-JAN-2013 TD-21167
  - Report shows incorrect data
    - When showing shifts, the data is incorrect!
    - A ClientDataSet (cdsEmpPieces...) is used to gather the data about pieces.
      This divides pieces when more than 1 employee worked on the same job.
      Without showing shifts this is OK. But when shifts should be shown, it
      goes wrong, then it gives too high values for pieces.
    - IMPORTANT:
      - Do NOT sort on Shift for qryMain
        or the determination of pieces goes wrong!
  MRA:18-MAR-2013 TD-22296
  - Report production details does not filter on department anymore.
  - Also: Filter on business units was missing.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:3-JUL-2014 20015220 + 20015221
  - Only create this datamodule once, because it is used by multiple reports.
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
  MRA:20-OCT-2014 TD-25945
  - Report changed after update
  - Because of previous changes that were made, the report did not
    show employees who made hours but did not made production (zero pieces).
  - Also because of a previous bugfix: The first employee was never shown.
    - Cause: It checked on a start-date being negative and this resulted in
             a skip of first record.
  MRA:27-OCT-2014 20015220.110 Rework
  - When ProductionQuantity-records are entered over longer periods than
    5 min. then it cannot always find it, depending on what was entered
    for 'selection on time'.
  MRA:2-JAN-2015 TD-26211
  - Be sure AProgressBar is assigned before it is used.
  MRA:10-JUN-2015 SO-20014450.50
  - Real Time Efficiency
  MRA:15-JAN-2016 PIM-135
  - Added PRODMINHOUR to get calculated hours from real-time-eff-view.
  MRA:15-JAN-2016 PIM-137
  - Add EmployeeFrom, EmployeeTo to real-time-eff-build-query for reports where
    employees can be selected.
  MRA:5-FEB-2016 PIM-135
  - Production reports add salary-hours column
  - Related to this order:
  - Also show down-time-jobs + breaks.
  - Show down-time-jobs based on dialog-setting:
    - Show it not (default), yes, or only.
  MRA:18-MAR-2016 PIM-12.4
  - Also filter on CompareJobs for RealTimeEff-reports.
*)

unit ReportProductionDetailDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, ReportBaseDMT, DBTables, Db, CalculateTotalHoursDMT, UScannedIDCard,
  UGlobalFunctions, Provider, DBClient, ComCtrls, UPimsConst;

const
  NO_EMPLOYEE: Integer = -1;

type
  TReportProductionDetailDM = class(TReportBaseDM)
    QueryProduction: TQuery;
    DataSourceProduction: TDataSource;
    qryRevenue: TQuery;
    cdsEmpPieces: TClientDataSet;
    cdsEmpPiecesPLANT_CODE: TStringField;
    cdsEmpPiecesWORKSPOT_CODE: TStringField;
    cdsEmpPiecesDEPARTMENT_CODE: TStringField;
    cdsEmpPiecesSHIFT_NUMBER: TIntegerField;
    cdsEmpPiecesEMPLOYEE_NUMBER: TIntegerField;
    cdsEmpPiecesJOB_CODE: TStringField;
    cdsEmpPiecesEMPPIECES: TFloatField;
    cdsEmpPiecesEMPTOTALMINUTES: TIntegerField;
    cdsEmpPiecesEMPTOTALBREAKMINUTES: TIntegerField;
    cdsEmpPiecesFOUND: TIntegerField;
    cdsEmpPiecesBUSINESSUNIT_CODE: TStringField;
    qryEmpl: TQuery;
    dspEmpl: TDataSetProvider;
    cdsEmpl: TClientDataSet;
    cdsEmplEMPLOYEE_NUMBER: TIntegerField;
    cdsEmplDESCRIPTION: TStringField;
    cdsEmplDEPARTMENT_CODE: TStringField;
    cdsEmplCUT_OF_TIME_YN: TStringField;
    cdsEmplINSCAN_MARGIN_EARLY: TIntegerField;
    cdsEmplINSCAN_MARGIN_LATE: TIntegerField;
    cdsEmplOUTSCAN_MARGIN_EARLY: TIntegerField;
    cdsEmplOUTSCAN_MARGIN_LATE: TIntegerField;
    cdsEmplCONTRACTGROUP_CODE: TStringField;
    qryJob: TQuery;
    cdsEmpPiecesPRODMINS: TIntegerField;
    qryMain: TQuery;
    qryPHEPT: TQuery;
    cdsEmpPiecesOVERTIMEMINS: TIntegerField;
    cdsEmpPiecesBONUS_PERCENTAGE: TFloatField;
    cdsEmpPiecesHOURLY_WAGE: TFloatField;
    cdsEmpPiecesBONUS_IN_MONEY_YN: TStringField;
    qryPHE: TQuery;
    cdsEmpPiecesDate: TClientDataSet;
    cdsEmpPiecesDatePLANT_CODE: TStringField;
    cdsEmpPiecesDateWORKSPOT_CODE: TStringField;
    cdsEmpPiecesDateJOB_CODE: TStringField;
    cdsEmpPiecesDateDEPARTMENT_CODE: TStringField;
    cdsEmpPiecesDateSHIFT_NUMBER: TIntegerField;
    cdsEmpPiecesDateEMPLOYEE_NUMBER: TIntegerField;
    cdsEmpPiecesDateEMPPIECES: TFloatField;
    cdsEmpPiecesDateEMPTOTALMINUTES: TIntegerField;
    cdsEmpPiecesDateEMPTOTALBREAKMINUTES: TIntegerField;
    cdsEmpPiecesDateFOUND: TIntegerField;
    cdsEmpPiecesDateBUSINESSUNIT_CODE: TStringField;
    cdsEmpPiecesDatePRODMINS: TIntegerField;
    cdsEmpPiecesDateOVERTIMEMINS: TIntegerField;
    cdsEmpPiecesDateBONUS_PERCENTAGE: TFloatField;
    cdsEmpPiecesDateHOURLY_WAGE: TFloatField;
    cdsEmpPiecesDateBONUS_IN_MONEY_YN: TStringField;
    cdsEmpPiecesDateSTART_DATE: TDateField;
    qryDeleteShiftPeriod: TQuery;
    qryInsertShiftPeriod: TQuery;
    qryProdSummary: TQuery;
    cdsEmpPiecesIGNOREQUANTITIESINREPORTS_YN: TStringField;
    cdsEmpPiecesDateIGNOREQUANTITIESINREPORTS_YN: TStringField;
    cdsEmpPiecesSHIFT_NUMBER_FILTER: TIntegerField;
    cdsEmpPiecesDateSHIFT_NUMBER_FILTER: TIntegerField;
    cdsEmpPiecesBREAKMINS: TIntegerField;
    cdsEmpPiecesDateBREAKMINS: TIntegerField;
    procedure TablePQFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryProductionFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    PQCount, PQMax: Integer;
    FGroupEmployeeEfficiency: Boolean;
    FEmpPiecesPerDate: Boolean;
    FContractGroupTFT: Boolean;
    FShowCompareJobs: Boolean;
    FEnableWorkspotIncludeForThisReport: Boolean;
//    procedure ShiftPeriodDelete;
//    procedure ShiftPeriodInsert(APlantCode: String; AShiftNumber: Integer;
//      AStartTime, AEndTime: TDateTime);
//    procedure ShiftPeriodFill;
    function MyShiftNumber(AShowShifts: Boolean;
      AQuery: TQuery; ADefault: String): String; overload;
    function MyShiftNumber(AShowShifts: Boolean;
      AQuery: TQuery; ADefault: Integer): Integer; overload;
    procedure DetermineTotalPiecesForAllEmployees(
      cdsEmpPieces: TClientDataSet;
      DateFrom, DateTo: TDateTime;
      ShiftFrom, ShiftTo: Integer;
      CheckDate: Boolean;
      AAllShifts: Boolean;
      AShowShifts: Boolean;
      AProgressBar: TProgressBar);
    procedure DeterminePHEPT(
      cdsEmpPieces: TClientDataSet;
      AShowShifts: Boolean;
      AProgressBar: TProgressBar);
    procedure DeterminePHE(
      cdsEmpPieces: TClientDataSet;
      AShowShifts: Boolean;
      AProgressBar: TProgressBar);
    procedure InitPHEQuery(const APlantFrom,
      APlantTo, AWorkspotFrom, AWorkspotTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants, AAllWorkspots, AShowShifts: Boolean);
    procedure InitMainQuery(
      const APlantFrom, APlantTo: String;
      const ABusinessUnitFrom, ABusinessUnitTo: String; // TD-22296
      const ADeptFrom, ADeptTo: String;
      const AWorkspotFrom, AWorkspotTo: String;
      const ATeamFrom, ATeamTo: String;
      const AShiftFrom, AShiftTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants,
      AAllBusinessUnits, // TD-22296
      AAllDepartments, // TD-22296
      AAllWorkspots, AAllTeams, AAllShifts,
      AShowShifts: Boolean;
      AShowOnlyDownTime: Boolean=False // 20015586
      );
  public
    { Public declarations }
    procedure InitPHEPTQuery(const APlantFrom,
      APlantTo, AWorkspotFrom, AWorkspotTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants, AAllWorkspots, AShowShifts: Boolean);
    function FindEmployee(const AEmployeeNumber: Integer): Boolean;
    // MR:18-01-2006 Optimising
{    procedure InitShiftPeriod(
      const APlantFrom, APlantTo: String;
      const ABusinessFrom, ABusinessTo: String;
      const ADeptFrom, ADeptTo: String;
      const AWorkspotFrom, AWorkspotTo: String;
      const ATeamFrom, ATeamTo: String;
      const AShiftFrom, AShiftTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants, AAllBusinessUnits, AAllDepartments, AAllWorkspots,
        AAllTeams, AAllShifts: Boolean); }
    procedure InitAll(const APlantFrom, APlantTo: String;
      const ABusinessUnitFrom, ABusinessUnitTo: String; // TD-22296
      const ADeptFrom, ADeptTo: String; // TD-22296
      const AWorkspotFrom, AWorkspotTo: String;
      const ATeamFrom, ATeamTo: String;
      const AShiftFrom, AShiftTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants,
      AAllBusinessUnits, // TD-22296
      AAllDepartments, // TD-22296
      AAllWorkspots, AAllTeams, AAllShifts,
      AShowShifts: Boolean;
      AProgressBar: TProgressBar;
      AShowOnlyDownTime: Boolean=False // 20015586
      );
    function ReturnMainQuery(const APlantFrom, APlantTo: String;
      const ABusinessUnitFrom, ABusinessUnitTo: String; // TD-22296
      const ADeptFrom, ADeptTo: String;
      const AWorkspotFrom, AWorkspotTo: String;
      const ATeamFrom, ATeamTo: String;
      const AShiftFrom, AShiftTo: String;
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants,
      AAllBusinessUnits, // TD-22296
      AAllDepartments, // TD-22296
      AAllWorkspots, AAllTeams, AAllShifts,
      AShowShifts: Boolean;
      ASummary: Boolean=False;
      AShowOnlyDownTime: Boolean=False // 20015586
      ): String;
    function OpenScan(AValue: String; ASelect: Boolean=False): String; // 20015221
    function DetermineTimeRecProdMins(
      ADateFrom, ADateTo: TDateTime;
      APlantCode, AWorkspotCode, AJobCode, ADepartmentCode, ABusinessUnitCode: String;
      AEmployeeNumber: Integer;
      AShiftNumber: Integer;
      ADate: TDateTime;
      ADefaultValue: Integer): Integer; // 20015220 + 20015221
    function ToDate(ADate: TDateTime): String;
    procedure Init;
    function RealTimeEfficiencyBuildQuery(
      const APlantFrom, APlantTo: String;
      const ABusinessUnitFrom, ABusinessUnitTo: String;
      const ADeptFrom, ADeptTo: String;
      const AWorkspotFrom, AWorkspotTo: String;
      const AJobFrom, AJobTo: String;
      const ATeamFrom, ATeamTo: String;
      const AShiftFrom, AShiftTo: String;
      const AEmployeeFrom, AEmployeeTo: String; // PIM-135
      const ADateFrom, ADateTo: TDateTime;
      const AAllPlants, AAllBusinessUnits, AAllDepartments,
        AAllWorkspots, AAllTeams, AAllShifts,
        AShowShifts: Boolean;
      const AShowOnlyJob: Boolean;
      const AIncludeDownTime: Integer; // PIM-135
      const ASelectShift, ASelectDate: Boolean;
      const GroupBy: Integer=0; // 0=GroupBy BU 1=GroupBy Dept
      const SortByEmp: Integer=0; // 0=SortByEmp Nr 1=SortByEmp Name
      const FilterJob: Boolean=False;
      const TeamIncentiveReport: Boolean=False;
      const SortByJob: Integer = 0 // 0=SortByJob Code 1=SortByJob Description
      ): String;
    function DetermineOvertime(APlantCode, AWorkspotCode, AJobCode,
      ADepartmentCode: String; AEmployeeNumber: Integer;
      AContractGroupTFT: Boolean;
      var ABonusInMoney: Boolean): Integer;
    property GroupEmployeeEfficiency: Boolean read FGroupEmployeeEfficiency
      write FGroupEmployeeEfficiency;
    property EmpPiecesPerDate: Boolean read FEmpPiecesPerDate write FEmpPiecesPerDate;
    property ContractGroupTFT: Boolean read FContractGroupTFT write FContractGroupTFT;
    property ShowCompareJobs: Boolean read FShowCompareJobs write FShowCompareJobs;
    property EnableWorkspotIncludeForThisReport: Boolean
      read FEnableWorkspotIncludeForThisReport write FEnableWorkspotIncludeForThisReport; // 20015223
  end;

var
  ReportProductionDetailDM: TReportProductionDetailDM;
  // RV089.1.
  // Use this var when it should be used by a different part that
  // is not 'Report Production Deta�l'.
  // 20015220 + 20015221
  // Create this DM only ONCE in project: It is used by multiple reports!
//  ReportProductionDetailDM2: TReportProductionDetailDM;
  // 20015220 Extra var when more reports are using this datamodule.
//  ReportProductionDetailDM3: TReportProductionDetailDM;
  // 20015220 Extra var when more reports are using this datamodule.
//  ReportProductionDetailDM4: TReportProductionDetailDM;

implementation

{$R *.DFM}

uses
  GlobalDMT;

// Get difference in seconds:
function DateTimeDiff(Start, Stop : TDateTime) : int64;
var TimeStamp : TTimeStamp;
begin
  TimeStamp := DateTimeToTimeStamp(Stop - Start);
  Dec(TimeStamp.Date, TTimeStamp(DateTimeToTimeStamp(0)).Date);
  Result := (TimeStamp.Date*24*60*60)+(TimeStamp.Time div 1000);
end;

function MinutesBetween(const ANow, AThen: TDateTime): Int64;
begin
  Result := Round(DateTImeDiff(ANow, AThen) / 60);
end;

{
function Quantize(DateIn : TDateTime; Quanta : integer) : TDateTime;
begin
  Result := Round(DateIn*Quanta)/Quanta;
end;

function MyMinutesBetween(const ANow, AThen: TDateTime): Int64;
begin
  Result := Trunc(Quantize(MinuteSpan(ANow, AThen), 60*60*24*1000));
end;
}
{
function DateTimeToMilliseconds(const ADateTime: TDateTime): Int64;
var
  LTimeStamp: TTimeStamp;
begin
  LTimeStamp := DateTimeToTimeStamp(ADateTime);
  Result := LTimeStamp.Date;
  Result := (Result * MSecsPerDay) + LTimeStamp.Time;
end;

function MinutesBetween(const ANow, AThen: TDateTime): Int64;
begin
  Result := Abs(DateTimeToMilliseconds(ANow) - DateTimeToMilliseconds(AThen))
    div (MSecsPerSec * SecsPerMin);
end;
}
procedure TReportProductionDetailDM.TablePQFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
//  Accept := (DataSet.FieldByName('QUANTITY').AsFloat > 0 );

end;

procedure TReportProductionDetailDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  ShowCompareJobs := False;
  EmpPiecesPerDate := False;
  cdsEmpPieces.CreateDataSet;
  cdsEmpPiecesDate.CreateDataSet;
  // To prevent changes are logged, which slows down the performance,
  // and can also result in the message 'not enough memory':
  cdsEmpPieces.LogChanges := False;
  cdsEmpPiecesDate.LogChanges := False;
  cdsEmpl.Open;
  cdsEmpl.LogChanges := False;
  cdsEmpl.Close;
  // MR:13-03-2007 Order 550443
  //               Used to determine if this datamodule is used to
  //               determine GroupEmployeeEffiency (VOSS) or not:
  // True: This datamodule is called from DialogMonthlyEmployeeEfficiency.
  //   -> An extra where-clause is used.
  // False: This datamodule is called from ReportProductionDetails.
  //   -> No extra where-clause is used.
  GroupEmployeeEfficiency := False;
  ContractGroupTFT := GlobalDM.ContractGroupTFTExists;
  // RV067.2.
  SystemDM.PlantTeamFilterEnable(QueryProduction);
  EnableWorkspotIncludeForThisReport := False;
end;

procedure TReportProductionDetailDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  cdsEmpl.Close;
  cdsEmpPieces.EmptyDataSet;
  cdsEmpPiecesDate.EmptyDataSet;
  cdsEmpPieces.Close;
  cdsEmpPiecesDate.Close;
  qryMain.Close;
  qryPHEPT.Close;
  qryPHE.Close;
  qryJob.Close;
  qryRevenue.Close;
  QueryProduction.Close;
end;

function TReportProductionDetailDM.FindEmployee(const AEmployeeNumber: Integer):
  Boolean;
begin
  with cdsEmpl do
  begin
    if not Active then
    begin
      Filtered := False;
      Open;
    end;
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
    Result := not IsEmpty;
  end;
end;

// MR:20-01-2005 This will fill a clientdatasets with ALL
// values that are needed. This must be called only once,
// at the start of the report (Production Detail Report).
// MR:16-01-2006 Moved from ListProcsFRM.
procedure TReportProductionDetailDM.DetermineTotalPiecesForAllEmployees(
  cdsEmpPieces: TClientDataSet;
  DateFrom, DateTo: TDateTime;
  ShiftFrom, ShiftTo: Integer;
  CheckDate: Boolean;
  AAllShifts: Boolean;
  AShowShifts: Boolean;
  AProgressBar: TProgressBar);
var
  CurrentEmplTotalMinutes, TotalMinutesPQ: Integer;
  CurrentEmplTotalBreakMinutes, TotalBreakMinutesPQ: Integer;
  EmplPieces: Double;
  PrevStartDate, PrevEndDate: TDateTime;
  PQPieces: Double;
  procedure DetermineProdMins;
  var
    ProdMin, BreaksMin, PayedBreaks: Integer;
    StartDate, EndDate{, BookingDay}: TDateTime;
    EmployeeIDCard: TScannedIDCard;
  begin
    ProdMin := 0;
    BreaksMin := 0;
    PayedBreaks := 0;
    // MR:16-01-2006 Empty IDCard first!
    EmptyIDCard(EmployeeIDCard);
    EmployeeIDCard.EmployeeCode :=
      qryMain.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    EmployeeIDCard.PlantCode :=
      qryMain.FieldByName('PLANT_CODE').AsString;
    // 20015220 This is wrong!!! This must be the shift of the
    // timerecording-record of the employee! Or it will NOT find any breaks!
//    EmployeeIDCard.ShiftNumber := MyShiftNumber(AShowShifts, qryMain, 0);
    EmployeeIDCard.ShiftNumber :=
      qryMain.FieldByName('TRSHIFT_NUMBER').AsInteger;
    EmployeeIDCard.WorkSpotCode :=
      qryMain.FieldByName('WORKSPOT_CODE').AsString;
    EmployeeIDCard.DepartmentCode :=
      qryMain.FieldByName('DEPARTMENT_CODE').AsString;
    EmployeeIDCard.JobCode :=
      qryMain.FieldByName('JOB_CODE').AsString;
    EmployeeIDCard.DateIn := qryMain.FieldByName('START_DATE').AsDateTime;
    EmployeeIDCard.DateOut := qryMain.FieldByName('END_DATE').AsDateTime;
    StartDate := qryMain.FieldByName('START_DATE').AsDateTime;
    EndDate := qryMain.FieldByName('END_DATE').AsDateTime;
    if EndDate - StartDate > 0 then // 20015220.110
    begin
      if qryMain.FieldByName('DATETIME_IN').AsDateTime > StartDate then
        StartDate := qryMain.FieldByName('DATETIME_IN').AsDateTime;
      if qryMain.FieldByName('DATETIME_OUT').AsDateTime < EndDate then
        EndDate := qryMain.FieldByName('DATETIME_OUT').AsDateTime;
      EmployeeIDCard.DateIn := StartDate;
      EmployeeIDCard.DateOut := EndDate;
    // MR:19-01-2006 Use 'computebreaks' instead of 'getprodmin'
    // because the cutoff-time should not be caclulated.
    // Reason: For determination of cutoff-time, the time-rec-scannings
    // are compared to see if a cutoff is needed for the first/last scan of
    // the employee for that day,
    // but here the PQ-periods are used instead of the
    // time-rec-scannings, this means there are many small 'scans' and
    // also they cannot be matched with the time-rec-scannings!
    // This also takes lots of time to determine, and will - in most cases -
    // be irrelevant.
{    AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
      EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks); }
      AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
        ProdMin, BreaksMin, PayedBreaks);
      TotalMinutesPQ := TotalMinutesPQ + ProdMin;
      TotalBreakMinutesPQ := TotalBreakMinutesPQ + BreaksMin;
      if not((EndDate < DateFrom) or (StartDate > DateTo)) then
      begin
        if (StartDate < DateFrom) or (EndDate > DateTo) then
        begin
          if StartDate < DateFrom then
            StartDate := DateFrom;
          if EndDate > DateTo then
            EndDate := DateTo;
          EmployeeIDCard.DateIn := StartDate;
          EmployeeIDCard.DateOut := EndDate;
          // MR:19-01-2006 Use 'computebreaks' instead of 'getprodmin'
          // because the cutoff-time should not be caclulated.
{          AProdMinClass.GetProdMin(EmployeeIDCard, True, True, False,
            EmployeeIDCard, BookingDay, ProdMin, BreaksMin, PayedBreaks); }
          AProdMinClass.ComputeBreaks(EmployeeIDCard, StartDate, EndDate, 0,
            ProdMin, BreaksMin, PayedBreaks);
        end;
        CurrentEmplTotalMinutes := CurrentEmplTotalMinutes + ProdMin;
        CurrentEmplTotalBreakMinutes := CurrentEmplTotalBreakMinutes +
          BreaksMin;
      end; // if
    end; // if
  end; // DetermineProdMins;
  procedure DeterminePieces(
    ACurrentEmplTotalMinutes, ACurrentEmplTotalBreakMinutes,
    ATotalMinutesPQ, ATotalBreakMinutesPQ: Integer;
    APieces: Double;
    var AEmplPieces: Double);
  begin
    if (ATotalMinutesPQ > 0) then
      AEmplPieces := AEmplPieces +
        (ACurrentEmplTotalMinutes  / ATotalMinutesPQ) * APieces
    else
      if (ATotalBreakMinutesPQ > 0) then
        AEmplPieces := AEmplPieces +
          (ACurrentEmplTotalBreakMinutes / ATotalBreakMinutesPQ) * APieces;
  end; // DeterminePieces
  procedure HandleEmpPiecesDate(AEmpPieces: TClientDataSet);
  var
    StartDate: TDateTime;
  begin
    StartDate := Trunc(qryMain.FieldByName('START_DATE').AsDateTime);
    if not AEmpPieces.FindKey([
      qryMain.FieldByName('PLANT_CODE').AsString,
      qryMain.FieldByName('WORKSPOT_CODE').AsString,
      qryMain.FieldByName('JOB_CODE').AsString,
      qryMain.FieldByName('DEPARTMENT_CODE').AsString,
      qryMain.FieldByName('EMPLOYEE_NUMBER').AsString,
      '0', // MyShiftNumber(AShowShifts, qryMain, '0'),
      StartDate]) then
    begin
      AEmpPieces.Insert;
      AEmpPieces.FieldByName('PLANT_CODE').AsString :=
        qryMain.FieldByName('PLANT_CODE').AsString;
      AEmpPieces.FieldByName('WORKSPOT_CODE').AsString :=
       qryMain.FieldByName('WORKSPOT_CODE').AsString;
      AEmpPieces.FieldByName('JOB_CODE').AsString :=
        qryMain.FieldByName('JOB_CODE').AsString;
      // 20013478.
      AEmpPieces.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString :=
        qryMain.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString;
      AEmpPieces.FieldByName('DEPARTMENT_CODE').AsString :=
        qryMain.FieldByName('DEPARTMENT_CODE').AsString;
      AEmpPieces.FieldByName('SHIFT_NUMBER').AsInteger := 0;
        // MyShiftNumber(AShowShifts, qryMain, 0);
      AEmpPieces.FieldByName('SHIFT_NUMBER_FILTER').AsInteger :=
        MyShiftNumber(AShowShifts, qryMain, 0);
      AEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
        qryMain.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      AEmpPieces.FieldByName('START_DATE').AsDateTime :=
        Trunc(qryMain.FieldByName('START_DATE').AsDateTime);
      AEmpPieces.FieldByName('EMPPIECES').AsFloat := 0;
      AEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
        CurrentEmplTotalMinutes;
      AEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
        CurrentEmplTotalBreakMinutes;
      AEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString :=
        qryMain.FieldByName('BUSINESSUNIT_CODE').AsString;
      if UseDateTime then // 20015220
      begin
        AEmpPieces.FieldByName('PRODMINS').AsInteger := CurrentEmplTotalMinutes;
        AEmpPieces.FieldByName('BREAKMINS').AsInteger := CurrentEmplTotalBreakMinutes;
      end
      else
        AEmpPieces.FieldByName('PRODMINS').AsInteger := 0;
      AEmpPieces.FieldByName('OVERTIMEMINS').AsInteger := 0;
      AEmpPieces.FieldByName('HOURLY_WAGE').AsFloat := 0;
      AEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat := 0;
      AEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString := 'N';
      AEmpPieces.FieldByName('FOUND').AsInteger := 1;
      AEmpPieces.Post;
    end
    else
    begin
      AEmpPieces.Edit;
      AEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
        AEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger +
          CurrentEmplTotalMinutes;
      AEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
        AEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger +
          CurrentEmplTotalBreakMinutes;
      if UseDateTime then // 20015220
      begin
        AEmpPieces.FieldByName('PRODMINS').AsInteger :=
          AEmpPieces.FieldByName('PRODMINS').AsInteger +
            CurrentEmplTotalMinutes;
        AEmpPieces.FieldByName('BREAKMINS').AsInteger :=
          AEmpPieces.FieldByName('BREAKMINS').AsInteger +
            CurrentEmplTotalBreakMinutes;
      end;
      AEmpPieces.FieldByName('FOUND').AsInteger := 1;
      AEmpPieces.Post;
    end;
  end; // HandleEmpPiecesDate
  procedure HandleEmpPiecesDatePIECES(AEmpPieces: TClientDataSet);
  begin
    AEmpPieces.Filtered := False;
    AEmpPieces.Filter := 'Found = 1';
    AEmpPieces.Filtered := True;
    if not AEmpPieces.IsEmpty then
    begin
      AEmpPieces.First;
      while not AEmpPieces.Eof do
      begin
{$IFDEF DEBUG14}
  WDebugLog('Start_Date=' + DateTimeToStr(PrevStartDate) +
    ' Emp=' + AEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' PQPieces=' + Format('%.0f', [PQPieces])
    );
{$ENDIF}
        EmplPieces := 0;
        DeterminePieces(
          AEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger,
          AEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger,
          // MR:21-04-2005 Don't get them from clientdataset
          TotalMinutesPQ,
          TotalBreakMinutesPQ,
          PQPieces,
          EmplPieces);
        AEmpPieces.Edit;
        // Clear the Totalminutes
        AEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger := 0;
        AEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger := 0;
        // Store the EmplPieces
        AEmpPieces.FieldByName('EMPPIECES').AsFloat :=
          AEmpPieces.FieldByName('EMPPIECES').AsFloat + EmplPieces;
        AEmpPieces.FieldByName('FOUND').AsInteger := 0;
        AEmpPieces.Post;
        // Because a filter is being used on 'FOUND=1' and we have
        // changed this to 0 we go to the first record again
        // untill there are no 'FOUND=1'-records anymore.
        AEmpPieces.First;
      end;
    end;
    AEmpPieces.Filter := '';
    AEmpPieces.Filtered := False;
    TotalMinutesPQ := 0;
    TotalBreakMinutesPQ := 0;
  end; // HandleEmpPiecesDatePIECES
begin
  PQCount := 0;
  PQMax := 0;
  EmplPieces := 0;
  cdsEmpPieces.EmptyDataSet;
  cdsEmpPiecesDate.EmptyDataSet;
  cdsEmpPieces.Filter := '';
  cdsEmpPiecesDate.Filter := '';
  cdsEmpPieces.Filtered := False;
  cdsEmpPiecesDate.Filtered := False;
  qryMain.Filtered := False;
  if qryMain.Eof then
    Exit;
  PQMax := qryMain.RecordCount + qryPHEPT.RecordCount + qryPHE.RecordCount;
  if Assigned(AProgressBar) then // TD-26211
  begin
    AProgressBar.Position := 0;
    AProgressBar.Max := PQMax;
    AProgressBar.Min := 0;
  end;
  // Find first match of Plantcode+WorkspotCode+JobCode
  if not UseDateTime then // 20015220
  begin
    DateFrom := Trunc(DateFrom);
    DateTo := Trunc(DateTo) + 1;
  end;
  TotalMinutesPQ := 0;
  TotalBreakMinutesPQ := 0;

{$IFDEF DEBUG14}
  qryMain.First;
  while not qryMain.Eof do
  begin
    WDebugLog(
      'SD=' + DateTimeToStr(qryMain.FieldByName('START_DATE').AsDateTime) +
      ' ED=' + DateTimeToStr(qryMain.FieldByName('END_DATE').AsDateTime) +
      ' E=' + qryMain.FieldByName('EMPLOYEE_NUMBER').AsString +
      ' W=' + qryMain.FieldByName('WORKSPOT_CODE').AsString +
      ' J=' + qryMain.FieldByName('JOB_CODE').AsString +
      ' D=' + qryMain.FieldByName('DEPARTMENT_CODE').AsString
      );
    qryMain.Next;
  end;
{$ENDIF}

  qryMain.First;
  // 20015220 Related to order: When START_DATE is negative then skip it:
  //          This means it is an invalid date. Why this can happen is not clear.
  // TD-25945 This must NOTE be done! Note: During debugging this field always
  //          gives a negative value, but when logging it is correct!
//  if qryMain.FieldByName('START_DATE').AsDateTime < 0 then
//    qryMain.Next;
  while not qryMain.Eof do
  begin
    CurrentEmplTotalMinutes := 0;
    CurrentEmplTotalBreakMinutes := 0;
(*
    if not PQInShift then
    begin
      qryMain.Next;
      Continue;
    end;
*)
    // If an employee was found (there was a scanning)...
    if qryMain.FieldByName('EMPLOYEE_NUMBER').Value <> Null then
    begin
//      if PQInShift then
//      begin
        DetermineProdMins;
        if EmpPiecesPerDate then
          HandleEmpPiecesDate(cdsEmpPiecesDate)
        else
        begin
          // 20012858 Also add SHIFT_NUMBER in primary key!
          if not cdsEmpPieces.FindKey([
            qryMain.FieldByName('PLANT_CODE').AsString,
            qryMain.FieldByName('WORKSPOT_CODE').AsString,
            qryMain.FieldByName('JOB_CODE').AsString,
            qryMain.FieldByName('DEPARTMENT_CODE').AsString,
            qryMain.FieldByName('EMPLOYEE_NUMBER').AsString,
            '0'
            //MyShiftNumber(AShowShifts, qryMain, '0')
            ]) then
          begin
            cdsEmpPieces.Insert;
            cdsEmpPieces.FieldByName('PLANT_CODE').AsString :=
              qryMain.FieldByName('PLANT_CODE').AsString;
            cdsEmpPieces.FieldByName('WORKSPOT_CODE').AsString :=
              qryMain.FieldByName('WORKSPOT_CODE').AsString;
            cdsEmpPieces.FieldByName('JOB_CODE').AsString :=
              qryMain.FieldByName('JOB_CODE').AsString;
            // 20013478.
            cdsEmpPieces.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString :=
              qryMain.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString;
            cdsEmpPieces.FieldByName('DEPARTMENT_CODE').AsString :=
              qryMain.FieldByName('DEPARTMENT_CODE').AsString;
            cdsEmpPieces.FieldByName('SHIFT_NUMBER').AsInteger := 0;
              // MyShiftNumber(AShowShifts, qryMain, 0);
            cdsEmpPieces.FieldByName('SHIFT_NUMBER_FILTER').AsInteger :=
              MyShiftNumber(AShowShifts, qryMain, 0);
            cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
              qryMain.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            cdsEmpPieces.FieldByName('EMPPIECES').AsFloat := 0;
            cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
              CurrentEmplTotalMinutes;
            cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
              CurrentEmplTotalBreakMinutes;
            cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString :=
              qryMain.FieldByName('BUSINESSUNIT_CODE').AsString;
            if UseDateTime then // 20015220
            begin
              cdsEmpPieces.FieldByName('PRODMINS').AsInteger := CurrentEmplTotalMinutes;
              cdsEmpPieces.FieldByName('BREAKMINS').AsInteger := CurrentEmplTotalBreakMinutes;
            end
            else
              cdsEmpPieces.FieldByName('PRODMINS').AsInteger := 0;
            cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger := 0;
            cdsEmpPieces.FieldByName('HOURLY_WAGE').AsFloat := 0;
            cdsEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat := 0;
            cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString := 'N';
            cdsEmpPieces.FieldByName('FOUND').AsInteger := 1;
            cdsEmpPieces.Post;
          end
          else
          begin
            cdsEmpPieces.Edit;
            cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger :=
              cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger +
                CurrentEmplTotalMinutes;
            cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger :=
              cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger +
                CurrentEmplTotalBreakMinutes;
            if UseDateTime then // 20015220
            begin
              cdsEmpPieces.FieldByName('PRODMINS').AsInteger :=
                cdsEmpPieces.FieldByName('PRODMINS').AsInteger +
                  CurrentEmplTotalMinutes;
              cdsEmpPieces.FieldByName('BREAKMINS').AsInteger :=
                cdsEmpPieces.FieldByName('BREAKMINS').AsInteger +
                  CurrentEmplTotalBreakMinutes;
            end;
            cdsEmpPieces.FieldByName('FOUND').AsInteger := 1;
            cdsEmpPieces.Post;
          end;
        end; // if EmpPiecesPerDate then
//      end; // if PQInShift then
    end
    else
    begin
      if not EmpPiecesPerDate then
      begin
        // Store 'No Employee' pieces.
        // 20012858 Add SHIFT to key!
        cdsEmpPieces.Filter := '';
        cdsEmpPieces.Filtered := False;
        if not cdsEmpPieces.FindKey([
          qryMain.FieldByName('PLANT_CODE').AsString,
          qryMain.FieldByName('WORKSPOT_CODE').AsString,
          qryMain.FieldByName('JOB_CODE').AsString,
          qryMain.FieldByName('DEPARTMENT_CODE').AsString,
          NO_EMPLOYEE,
          '0'
          // MyShiftNumber(AShowShifts, qryMain, '0')
          ]) then
        begin
          cdsEmpPieces.Insert;
          cdsEmpPieces.FieldByName('PLANT_CODE').AsString :=
            qryMain.FieldByName('PLANT_CODE').AsString;
          cdsEmpPieces.FieldByName('WORKSPOT_CODE').AsString :=
            qryMain.FieldByName('WORKSPOT_CODE').AsString;
          cdsEmpPieces.FieldByName('JOB_CODE').AsString :=
            qryMain.FieldByName('JOB_CODE').AsString;
          // 20013478.
          cdsEmpPieces.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString :=
            qryMain.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString;
          cdsEmpPieces.FieldByName('DEPARTMENT_CODE').AsString :=
            qryMain.FieldByName('DEPARTMENT_CODE').AsString;
          cdsEmpPieces.FieldByName('SHIFT_NUMBER').AsInteger := 0;
            // MyShiftNumber(AShowShifts, qryMain, 0);
          cdsEmpPieces.FieldByName('SHIFT_NUMBER_FILTER').AsInteger :=
            MyShiftNumber(AShowShifts, qryMain, 0);
          cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsInteger := NO_EMPLOYEE;
          cdsEmpPieces.FieldByName('EMPPIECES').AsFloat :=
            qryMain.FieldByName('PIECES').AsFloat;
          cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString :=
            qryMain.FieldByName('BUSINESSUNIT_CODE').AsString;
          cdsEmpPieces.FieldByName('PRODMINS').AsInteger := 0;
          cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger := 0;
          cdsEmpPieces.FieldByName('HOURLY_WAGE').AsFloat := 0;
          cdsEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat := 0;
          cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString := 'N';
          cdsEmpPieces.FieldByName('FOUND').AsInteger := 0;
          cdsEmpPieces.Post;
        end
        else
        begin
          cdsEmpPieces.Edit;
          cdsEmpPieces.FieldByName('EMPPIECES').AsFloat :=
            cdsEmpPieces.FieldByName('EMPPIECES').AsFloat +
              qryMain.FieldByName('PIECES').AsFloat;
          cdsEmpPieces.Post;
        end;
      end; // if not EmpPiecesPerDate then
    end; // if qryMain.FieldByName('EMPLOYEE_NUMBER').Value <> Null then
    PrevStartDate := qryMain.FieldByName('START_DATE').AsDateTime;
    prevEndDate := qryMain.FieldByName('END_DATE').AsDateTime;
    PQPieces := qryMain.FieldByName('PIECES').AsFloat;

{$IFDEF DEBUG14}
WDebugLog(' ST=' + DateTimeToStr(PrevStartDate) +
  ' ET=' + DateTimeToStr(PrevEndDate) +
  ' E=' + qryMain.FieldByName('EMPLOYEE_NUMBER').AsString +
  ' W=' + qryMain.FieldByName('WORKSPOT_CODE').AsString +
  ' J=' + qryMain.FieldByName('JOB_CODE').AsString +
  ' D=' + qryMain.FieldByName('DEPARTMENT_CODE').AsString +
  ' S=' + MyShiftNumber(AShowShifts, qryMain, '0') +
  ' Pcs=' + IntToStr(Round(PQPieces))
  );
{$ENDIF}

    qryMain.Next;
    if ((qryMain.FieldByName('START_DATE').AsDateTime <> PrevStartDate) and
      (qryMain.FieldByName('END_DATE').AsDateTIme <> PrevEndDate)) or
      qryMain.Eof then
    begin
      if EmpPiecesPerDate then
        HandleEmpPiecesDatePIECES(cdsEmpPiecesDate)
      else
      begin
        cdsEmpPieces.Filtered := False;
        cdsEmpPieces.Filter := 'Found = 1';
        cdsEmpPieces.Filtered := True;
        if not cdsEmpPieces.IsEmpty then
        begin
          cdsEmpPieces.First;
          while not cdsEmpPieces.Eof do
          begin
            EmplPieces := 0;
            DeterminePieces(
              cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger,
              cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger,
              // MR:21-04-2005 Don't get them from clientdataset
              TotalMinutesPQ,
              TotalBreakMinutesPQ,
              PQPieces,
              EmplPieces);
{$IFDEF DEBUG14}
  WDebugLog(' E=' + cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' EmpTotMin=' + cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsString +
    ' EmpTotBrkMin=' + cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsString +
    ' TotMinPQ=' + IntToStr(TotalMinutesPQ) +
    ' TotBrkMinPQ=' + IntToStr(TotalBreakMinutesPQ) +
    ' PQPcs=' + Format('%.0f', [PQPieces]) +
    ' EmpPcs=' + Format('%.0f', [EmplPieces])
    );
{$ENDIF}
            cdsEmpPieces.Edit;
            // Clear the Totalminutes
            cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsInteger := 0;
            cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsInteger := 0;
            // Store the EmplPieces
            cdsEmpPieces.FieldByName('EMPPIECES').AsFloat :=
              cdsEmpPieces.FieldByName('EMPPIECES').AsFloat + EmplPieces;
            cdsEmpPieces.FieldByName('FOUND').AsInteger := 0;
            cdsEmpPieces.Post;
            // Because a filter is being used on 'FOUND=1' and we have
            // changed this to 0 we go to the first record again
            // untill there are no 'FOUND=1'-records anymore.
            cdsEmpPieces.First;
          end;
        end;
        cdsEmpPieces.Filter := '';
        cdsEmpPieces.Filtered := False;
        TotalMinutesPQ := 0;
        TotalBreakMinutesPQ := 0;
      end; // if EmpPiecesPerDate then
    end;
    inc(PQCount);
    // MR:09-02-2005 Prevent 'division by zero'!
    if Round(PQMax / 100) > 0 then
      if (PQCount MOD (Round(PQMax / 100)) = 0) then
      begin
        if Assigned(AProgressBar) then // TD-26211
        begin
          AProgressBar.Position := PQCount;
          Application.ProcessMessages;
        end;
      end;
  end; // while not qryMain.Eof
  Application.ProcessMessages;
  qryMain.Filtered := False;
{$IFDEF DEBUG14}
  if EmpPiecesPerDate then
  begin
    WDebugLog('cdsEmpPiecesDate:');
    cdsEmpPiecesDate.First;
    WDebugLog('Emp;Plant;Workspot;Job;Dept;Shift;EmpTotMin;EmpTotBreakMin;ProdMins;BreakMins;EmpPieces;StartDate');
    while not cdsEmpPiecesDate.Eof do
    begin
      WDebugLog(
        cdsEmpPiecesDate.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('PLANT_CODE').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('WORKSPOT_CODE').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('JOB_CODE').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('DEPARTMENT_CODE').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('SHIFT_NUMBER_FILTER').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('EMPTOTALMINUTES').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('EMPTOTALBREAKMINUTES').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('PRODMINS').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('BREAKMINS').AsString + ';' +
        cdsEmpPiecesDate.FieldByName('EMPPIECES').AsString + ';' +
        DateTimeToStr(cdsEmpPiecesDate.FieldByName('START_DATE').AsDateTime)
      );
      cdsEmpPiecesDate.Next;
    end;
  end
  else
  begin
    WDebugLog('cdsEmpPieces:');
    cdsEmpPieces.First;
    WDebugLog('Emp;Plant;Workspot;Job;Dept;Shift;EmpTotMin;EmpTotBreakMin;ProdMins;BreakMins;EmpPieces');
    while not cdsEmpPieces.Eof do
    begin
      WDebugLog(
        cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
        cdsEmpPieces.FieldByName('PLANT_CODE').AsString + ';' +
        cdsEmpPieces.FieldByName('WORKSPOT_CODE').AsString + ';' +
        cdsEmpPieces.FieldByName('JOB_CODE').AsString + ';' +
        cdsEmpPieces.FieldByName('DEPARTMENT_CODE').AsString + ';' +
        cdsEmpPieces.FieldByName('SHIFT_NUMBER_FILTER').AsString + ';' +
        cdsEmpPieces.FieldByName('EMPTOTALMINUTES').AsString + ';' +
        cdsEmpPieces.FieldByName('EMPTOTALBREAKMINUTES').AsString + ';' +
        cdsEmpPieces.FieldByName('PRODMINS').AsString + ';' +
        cdsEmpPieces.FieldByName('BREAKMINS').AsString + ';' +
        cdsEmpPieces.FieldByName('EMPPIECES').AsString
      );
      cdsEmpPieces.Next;
    end;
  end;
{$ENDIF}
end; // DetermineTotalPiecesForAllEmployees

// Determine ProductionPerEmplPerType, store it in
// cdsPieces.
procedure TReportProductionDetailDM.DeterminePHEPT(
  cdsEmpPieces: TClientDataSet;
  AShowShifts: Boolean;
  AProgressBar: TProgressBar);
begin
  if qryPHEPT.Eof then
    Exit;
  while not qryPHEPT.Eof do
  begin
    // 20012858 Add Shift to primary key!
    if not cdsEmpPieces.FindKey([
      qryPHEPT.FieldByName('PLANT_CODE').AsString,
      qryPHEPT.FieldByName('WORKSPOT_CODE').AsString,
      qryPHEPT.FieldByName('JOB_CODE').AsString,
      qryPHEPT.FieldByName('DEPARTMENT_CODE').AsString,
      qryPHEPT.FieldByName('EMPLOYEE_NUMBER').AsString,
      '0'
      // MyShiftNumber(AShowShifts, qryPHEPT, '0')
      ]) then
    begin
      // Must exist?
    end
    else
    begin
      cdsEmpPieces.Edit;
      // MRA:05-10-2007 Prodmins are stored from 'qryphe'.
{      cdsEmpPieces.FieldByName('PRODMINS').AsInteger :=
        cdsEmpPieces.FieldByName('PRODMINS').AsInteger +
          qryPHEPT.FieldByName('SUMPRODMIN').AsInteger; }
      if qryPHEPT.FieldByName('OVERTIME_YN').AsString = 'Y' then
        cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger :=
          cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger +
            qryPHEPT.FieldByName('SUMPRODMIN').AsInteger;
      cdsEmpPieces.FieldByName('HOURLY_WAGE').AsFloat :=
        qryPHEPT.FieldByName('HOURLY_WAGE').AsFloat;
      cdsEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat :=
        qryPHEPT.FieldByName('BONUS_PERCENTAGE').AsFloat;
      // RV045.1.
      if ContractGroupTFT then
        cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString :=
          qryPHEPT.FieldByName('BONUS_IN_MONEY_YN').AsString
      else
        cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString :=
          qryPHEPT.FieldByName('HT_BONUS_IN_MONEY_YN').AsString;
      cdsEmpPieces.Post;
    end;
    qryPHEPT.Next;
    inc(PQCount);
    // MR:09-02-2005 Prevent 'division by zero'!
    if Round(PQMax / 100) > 0 then
      if (PQCount MOD (Round(PQMax / 100)) = 0) then
      begin
        if Assigned(AProgressBar) then // TD-26211
        begin
          AProgressBar.Position := PQCount;
          Application.ProcessMessages;
        end;
      end;
  end; // while not qryPHEPT.Eof
  Application.ProcessMessages;
end; // DeterminePHEPT

// Determine ProductionPerEmpl, store it in
// cdsPieces. Here only PRODMINS are stored.
procedure TReportProductionDetailDM.DeterminePHE(
  cdsEmpPieces: TClientDataSet;
  AShowShifts: Boolean;
  AProgressBar: TProgressBar);
begin
  if qryPHE.Eof then
    Exit;
  if UseDateTime then // 20015220 Do not take hours from PHE
    Exit;

  while not qryPHE.Eof do
  begin
    // 20012858 Add Shift to primary key!
    if not cdsEmpPieces.FindKey([
      qryPHE.FieldByName('PLANT_CODE').AsString,
      qryPHE.FieldByName('WORKSPOT_CODE').AsString,
      qryPHE.FieldByName('JOB_CODE').AsString,
      qryPHE.FieldByName('DEPARTMENT_CODE').AsString,
      qryPHE.FieldByName('EMPLOYEE_NUMBER').AsString,
      '0'
      // MyShiftNumber(AShowShifts, qryPHE, '0')
      ]) then
    begin
      // Must exist?
    end
    else
    begin
      cdsEmpPieces.Edit;
      cdsEmpPieces.FieldByName('PRODMINS').AsInteger :=
       cdsEmpPieces.FieldByName('PRODMINS').AsInteger +
          qryPHE.FieldByName('SUMPRODMIN').AsInteger;
      cdsEmpPieces.Post;
    end;
    qryPHE.Next;
    inc(PQCount);
    // MR:09-02-2005 Prevent 'division by zero'!
    if Round(PQMax / 100) > 0 then
      if (PQCount MOD (Round(PQMax / 100)) = 0) then
      begin
        if Assigned(AProgressBar) then // TD-26211
        begin
          AProgressBar.Position := PQCount;
          Application.ProcessMessages;
        end;
      end;
  end; // while not qryPHE.Eof
  if Assigned(AProgressBar) then // TD-26211
    AProgressBar.Position := 0;
  Application.ProcessMessages;
end; // DeterminePHE

// MR:18-01-2006 Optimising
// Initialise qryMain -> gets all the data that is needed from
// ProductionQuantity and TimeRegScanning in one Query.
procedure TReportProductionDetailDM.InitMainQuery(
  const APlantFrom, APlantTo,
  ABusinessUnitFrom, ABusinessUnitTo, // TD-22296
  ADeptFrom, ADeptTo, // TD-22296
  AWorkspotFrom, AWorkspotTo,
  ATeamFrom, ATeamTo: String;
  const AShiftFrom, AShiftTo: String;
  const ADateFrom, ADateTo: TDateTime;
  const AAllPlants,
  AAllBusinessUnits, // TD-22296
  AAllDepartments, // TD-22296
  AAllWorkspots, AAllTeams,
  AAllShifts, AShowShifts: Boolean;
  AShowOnlyDownTime: Boolean=False // 20015586
  );
var
  SelectStr: String;
begin
  // TD-21167
  SelectStr := ReturnMainQuery(APlantFrom, APlantTo,
    ABusinessUnitFrom, ABusinessUnitTo, // TD-22296
    ADeptFrom, ADeptTo, // TD-22296
    AWorkspotFrom, AWorkspotTo,
    ATeamFrom, ATeamTo, AShiftFrom, AShiftTo,
    ADateFrom, ADateTo,
    AAllPlants,
    AAllBusinessUnits, // TD-22296
    AAllDepartments, // TD-22296
    AAllWorkspots, AAllTeams, AAllShifts,
    AShowShifts,
    AShowOnlyDownTime // 20015586
    );
  with qryMain do
  begin
    Close;
    Filtered := False; // 20015220
    Filter := ''; // 20015220
    SQL.Clear;
    SQL.Add(SelectStr);
// SQL.SaveToFile('c:\temp\qryMain.sql');
//    ParamByName('DATEFROM').AsDateTime := Trunc(ADateFrom); // 20015220.110
//    ParamByName('DATETO').AsDateTime := Trunc(ADateTo + 1); // 20015220.110
    Open;
  end;
end;

// Initialise the ProdHourPerEmplPerType-Query.
// This query is used as lookup-query.
procedure TReportProductionDetailDM.InitPHEPTQuery(const APlantFrom,
  APlantTo, AWorkspotFrom, AWorkspotTo: String;
  const ADateFrom, ADateTo: TDateTime;
  const AAllPlants, AAllWorkspots, AShowShifts: Boolean);
var
  SelectStr: String;
begin
  // RV045.1. Added HOURTYPE.BONUS_IN_MONEY_YN
  SelectStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER, ' + NL +
    '  E.CONTRACTGROUP_CODE, W.DEPARTMENT_CODE, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  P.SHIFT_NUMBER, ' + NL;
  SelectStr := SelectStr +
    '  CG.TIME_FOR_TIME_YN, CG.BONUS_IN_MONEY_YN, ' + NL +
    '  EC.HOURLY_WAGE, H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN, ' + NL +
    '  H.BONUS_PERCENTAGE, H.OVERTIME_YN, H.WAGE_BONUS_ONLY_YN, ' + NL +
    '  SUM(P.PRODUCTION_MINUTE) AS SUMPRODMIN ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLPERTYPE P ' + NL +
    '  INNER JOIN ' + NL +
    '    (EMPLOYEE E  ' + NL +
    '    INNER JOIN CONTRACTGROUP CG ON ' + NL +
    '      CG.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) ' + NL +
    '    ON ' + NL +
    '    E.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER ' + NL +
    '  LEFT JOIN EMPLOYEECONTRACT EC ON ' + NL +
    '    EC.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER AND ' + NL +
    '    EC.STARTDATE <= P.PRODHOUREMPLOYEE_DATE AND ' + NL +
    '    EC.ENDDATE >= P.PRODHOUREMPLOYEE_DATE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  // MR:13-03-2007 Order 550443. If GroupEmployeeEfficiency is True then
  //               use an extra where-clause.
  if GroupEmployeeEfficiency then
    SelectStr := SelectStr +
      '  AND W.GROUP_EFFICIENCY_YN = ''N'' ' + NL;
  SelectStr := SelectStr +
    '  INNER JOIN HOURTYPE H ON ' + NL +
    '    H.HOURTYPE_NUMBER = P.HOURTYPE_NUMBER ' + NL +
    'WHERE ' + NL;
  if APlantFrom = APlantTo then
  begin
    SelectStr := SelectStr +
      '  P.PLANT_CODE = ' + MyQuotes(APlantFrom) + ' AND ' + NL;
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    begin
      SelectStr := SelectStr +
        ' (' + NL +
        '   (' + NL +
        '     P.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL +
        ' AND ' + NL;
    end
    else
    begin
      if not AAllWorkspots then
        SelectStr := SelectStr +
          '  P.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + ' AND ' + NL +
          '  P.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + ' AND ' + NL;
    end;
  end
  else
    if not AAllPlants then
      SelectStr := SelectStr +
        '  P.PLANT_CODE >= ' + MyQuotes(APlantFrom) + ' AND ' + NL +
        '  P.PLANT_CODE <= ' + MyQuotes(APlantTo) + ' AND ' + NL;
  SelectStr := SelectStr +
    '  P.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE < :DATETO  ' + NL +
    'GROUP BY ' + NL +
    '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER, ' + NL +
    '  E.CONTRACTGROUP_CODE, W.DEPARTMENT_CODE, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      ' P.SHIFT_NUMBER, ' + NL;
  SelectStr := SelectStr +
    '  CG.TIME_FOR_TIME_YN, CG.BONUS_IN_MONEY_YN, ' + NL +
    '  EC.HOURLY_WAGE, H.BONUS_IN_MONEY_YN, ' + NL +
    '  H.BONUS_PERCENTAGE, H.OVERTIME_YN, H.WAGE_BONUS_ONLY_YN ' + NL +
    'ORDER BY ' + NL +
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, EMPLOYEE_NUMBER, ' + NL +
      '  CONTRACTGROUP_CODE, DEPARTMENT_CODE ';
  if AShowShifts then
    SelectStr := SelectStr +
        ' , SHIFT_NUMBER ';

  with qryPHEPT do
  begin
    Close;
    SQL.Clear;
    SQL.Add(SelectStr);
    Filtered := False;
// SQL.SaveToFile('c:\temp\qryphept.sql');
{
    if APlantFrom = APlantTo then
    begin
      ParamByName('PLANTFROM').AsString := APlantFrom;
      if not AAllWorkspots then
      begin
        ParamByName('WORKSPOTFROM').AsString := AWorkspotFrom;
        ParamByName('WORKSPOTTO').AsString := AWorkspotTo;
      end;
    end
    else
      if not AAllPlants then
      begin
        ParamByName('PLANTFROM').AsString := APlantFrom;
        ParamByName('PLANTTO').AsString := APlantTo;
      end;
}
    // 20015220
    ParamByName('DATEFROM').AsDateTime := Trunc(ADateFrom);
    ParamByName('DATETO').AsDateTime := Trunc(ADateTo + 1);
    Open;
  end;
end;

// Initialise the ProdHourPerEmpl-Query.
// This query is used as lookup-query.
procedure TReportProductionDetailDM.InitPHEQuery(const APlantFrom,
  APlantTo, AWorkspotFrom, AWorkspotTo: String;
  const ADateFrom, ADateTo: TDateTime;
  const AAllPlants, AAllWorkspots, AShowShifts: Boolean);
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, ' + NL +
    '  P.EMPLOYEE_NUMBER, W.DEPARTMENT_CODE, ';
  if AShowShifts then
    SelectStr := SelectStr +
      ' P.SHIFT_NUMBER, ' + NL;
  SelectStr := SelectStr +
    '  SUM(P.PRODUCTION_MINUTE) AS SUMPRODMIN ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  // MR:13-03-2007 Order 550443. If GroupEmployeeEfficiency is True then
  //               use an extra where-clause.
  if GroupEmployeeEfficiency then
    SelectStr := SelectStr +
      '  AND W.GROUP_EFFICIENCY_YN = ''N'' ' + NL;
  SelectStr := SelectStr +
    'WHERE ' + NL;
  if APlantFrom = APlantTo then
  begin
    SelectStr := SelectStr +
      '  P.PLANT_CODE = ' + MyQuotes(APlantFrom) + ' AND ' + NL;
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    begin
      SelectStr := SelectStr +
        ' (' + NL +
        '   (' + NL +
        '     P.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL +
        ' AND ' + NL;
    end
    else
    begin
      if not AAllWorkspots then
        SelectStr := SelectStr +
          '  P.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + ' AND ' + NL +
          '  P.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + ' AND ' + NL;
    end;
  end
  else
    if not AAllPlants then
    begin
      SelectStr := SelectStr +
        '  P.PLANT_CODE >= ' + MyQuotes(APlantFrom) + ' AND ' + NL +
        '  P.PLANT_CODE <= ' + MyQuotes(APlantTo) + ' AND ' + NL;
    end;
  SelectStr := SelectStr +
    '  P.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE < :DATETO  ' + NL +
    'GROUP BY ' + NL +
    '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, ' + NL +
    '  P.EMPLOYEE_NUMBER, W.DEPARTMENT_CODE ';
  if AShowShifts then
    SelectStr := SelectStr +
      ', P.SHIFT_NUMBER ' + NL;
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, ' + NL +
    '  EMPLOYEE_NUMBER, DEPARTMENT_CODE ';
  if AShowShifts then
    SelectStr := SelectStr +
      ', SHIFT_NUMBER';

  with qryPHE do
  begin
    Close;
    SQL.Clear;
    SQL.Add(SelectStr);
// SQL.SaveToFile('c:\temp\qryphe.sql');
    // 20015220
    ParamByName('DATEFROM').AsDateTime := Trunc(ADateFrom);
    ParamByName('DATETO').AsDateTime := Trunc(ADateTo + 1);
    Open;
  end;
end;

procedure TReportProductionDetailDM.InitAll(const APlantFrom, APlantTo,
  ABusinessUnitFrom, ABusinessUnitTo, // TD-22296
  ADeptFrom, ADeptTo, // TD-22296
  AWorkspotFrom, AWorkspotTo, ATeamFrom, ATeamTo: String;
  const AShiftFrom, AShiftTo: String;
  const ADateFrom, ADateTo: TDateTime;
  const AAllPlants,
  AAllBusinessUnits, // TD-22296
  AAllDepartments, // TD-22296
  AAllWorkspots, AAllTeams,
  AAllShifts, AShowShifts: Boolean;
  AProgressBar: TProgressBar;
  AShowOnlyDownTime: Boolean=False // 20015586
  );
begin
  // Init/Reset all Client Datasets.
  cdsEmpl.Close;
  cdsEmpl.Filtered := False;
  cdsEmpl.Open;

  InitMainQuery(
    APlantFrom, APlantTo,
    ABusinessUnitFrom, ABusinessUnitTo, // TD-22296
    ADeptFrom, ADeptTo, // TD-22296
    AWorkspotFrom, AWorkspotTo,
    ATeamFrom, ATeamTo,
    AShiftFrom, AShiftTo,
    ADateFrom, ADateTo,
    AAllPlants,
    AAllBusinessUnits, // TD-22296
    AAllDepartments, // TD-22296
    AAllWorkspots, AAllTeams, AAllShifts,
    AShowShifts,
    AShowOnlyDownTime // 20015582
    );
  InitPHEPTQuery(
    APlantFrom, APlantTo,
    AWorkspotFrom, AWorkspotTo,
    ADateFrom, ADateTo,
    AAllPlants, AAllWorkspots, AShowShifts);
  InitPHEQuery(
    APlantFrom, APlantTo,
    AWorkspotFrom, AWorkspotTo,
    ADateFrom, ADateTo,
    AAllPlants, AAllWorkspots, AShowShifts);
  if not (qryMain.IsEmpty) then
  begin
    DetermineTotalPiecesForAllEmployees(
      cdsEmpPieces,
      ADateFrom, ADateTo,
      0, 999,
      False,
      AAllShifts,
      AShowShifts,
      AProgressBar);
    DeterminePHEPT(
      cdsEmpPieces,
      AShowShifts,
      AProgressBar);
    DeterminePHE(
      cdsEmpPieces,
      AShowShifts,
      AProgressBar);
  end;
end;

procedure TReportProductionDetailDM.QueryProductionFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV067.2.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

function TReportProductionDetailDM.MyShiftNumber(AShowShifts: Boolean;
  AQuery: TQuery; ADefault: String): String;
begin
  if AShowShifts then
    Result := AQuery.FieldByName('SHIFT_NUMBER').AsString
  else
    Result := ADefault;
end;

function TReportProductionDetailDM.MyShiftNumber(AShowShifts: Boolean;
  AQuery: TQuery; ADefault: Integer): Integer;
begin
  if AShowShifts then
    Result := AQuery.FieldByName('SHIFT_NUMBER').AsInteger
  else
    Result := ADefault;
end;

// TD-21167
// TD-22296 Filtering in department was missing!
function TReportProductionDetailDM.ReturnMainQuery(const APlantFrom,
  APlantTo,
  ABusinessUnitFrom, ABusinessUnitTo, // TD-22296
  ADeptFrom, ADeptTo,  // TD-22296
  AWorkspotFrom, AWorkspotTo,
  ATeamFrom, ATeamTo, AShiftFrom,
  AShiftTo: String;
  const ADateFrom, ADateTo: TDateTime; const AAllPlants,
  AAllBusinessUnits, // TD-22296
  AAllDepartments,  // TD-22296
  AAllWorkspots, AAllTeams, AAllShifts, AShowShifts: Boolean;
  ASummary: Boolean=False;
  AShowOnlyDownTime: Boolean=False // 20015586
  ): String;
var
  SelectStr: String;
  UserName: String;
begin
  // 20013478. Addition of field JOBCODE.IGNOREQUANTITIESINREPORTS_YN
  // 20013489
  SelectStr :=
    'SELECT /*+ USE_HASH(P,T,W,J) */ ' + NL +
    '  P.PLANT_CODE, PL.DESCRIPTION AS PDESCRIPTION, ' + NL +
    '  J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESCRIPTION, ' + NL +
    '  W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESCRIPTION, ' + NL;
  if AShowShifts then
  begin
    SelectStr := SelectStr +
      '  NVL(T.SHIFT_NUMBER, -999) SHIFT_NUMBER, ' + NL;
    SelectStr := SelectStr +
      '  (SELECT S.DESCRIPTION FROM SHIFT S ' + NL +
      '   WHERE S.PLANT_CODE = PL.PLANT_CODE AND ' + NL +
      '   S.SHIFT_NUMBER = T.SHIFT_NUMBER) SDESCRIPTION, ' + NL;
  end;
  SelectStr := SelectStr +
    '  P.WORKSPOT_CODE, W.DESCRIPTION AS WDESCRIPTION, ' + NL +
    '  P.JOB_CODE, J.DESCRIPTION AS JDESCRIPTION, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '  P.START_DATE, P.END_DATE, P.QUANTITY AS PIECES, ' + NL +
    '  0 AS MINUTES, ' + NL + // 20015220
    '  T.DATETIME_IN, ' + OpenScan('T.DATETIME_OUT', True) + ', ' + NL + // 20015221
    '  T.EMPLOYEE_NUMBER, ' + NL +
    '  T.SHIFT_NUMBER TRSHIFT_NUMBER, ' + NL +
    '  B.BONUS_FACTOR, ' + NL +
    '  J.NORM_PROD_LEVEL ' + NL +
    'FROM  ' + NL +
    '  PRODUCTIONQUANTITY P ' + NL +
    '    LEFT JOIN TIMEREGSCANNING T ON ' + NL +
    '      P.PLANT_CODE = T.PLANT_CODE AND ' + NL +
    '      P.WORKSPOT_CODE = T.WORKSPOT_CODE AND ' + NL +
    '      P.JOB_CODE = T.JOB_CODE AND NOT ' + NL +
    '      (P.START_DATE >= ' + OpenScan('T.DATETIME_OUT') + ') AND NOT ' + NL + // 20015221
    '      (P.END_DATE <= T.DATETIME_IN) AND ' + NL +
    '      ' + OpenScan('T.DATETIME_OUT') + ' IS NOT NULL AND ' + NL; // 20015221
  if SystemDM.UseShiftDateSystem then
  begin
    if UseDateTime then // 20015220
    begin
      SelectStr := SelectStr +
        '      T.SHIFT_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL + // 20015220.110
        '      T.SHIFT_DATE < ' + ToDate(Trunc(ADateTo+1)) + ' AND ' + NL + // :DATETO AND ' + NL + // 20015220.110
        '      (' + OpenScan('T.DATETIME_OUT') + ' >= ' + ToDate(ADateFrom) + ' AND ' + NL + // 20015221
        '      '  + OpenScan('T.DATETIME_OUT') + ' IS NOT NULL) AND ' + NL + // 20015221
        '      T.DATETIME_IN <= ' + ToDate(ADateTo) + NL;
    end
    else
      SelectStr := SelectStr +
        // 20013489
        '      T.SHIFT_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL +  //:DATEFROM AND ' + NL + // 20015220.110
        '      T.SHIFT_DATE < ' + ToDate(Trunc(ADateTo+1)) + NL // :DATETO ' + NL // 20015220.110
  end
  else
  begin
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        '      ' + OpenScan('T.DATETIME_OUT') + ' >= ' + ToDate(ADateFrom) + ' AND ' + NL +
        '      T.DATETIME_IN <= ' + ToDate(ADateTo) + NL
    else
      SelectStr := SelectStr +
        '      ' + OpenScan('T.DATETIME_OUT') + ' >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL + // 20015220.110
        '      T.DATETIME_IN < ' + ToDate(Trunc(ADateTo+1)) + NL; // :DATETO ' + NL; // 20015220.110
  end;
  SelectStr := SelectStr +
    '    INNER JOIN WORKSPOT W ON ' + NL +
    '      P.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '      P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '    INNER JOIN JOBCODE J ON ' + NL +
    '      P.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '      P.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '      P.JOB_CODE = J.JOB_CODE ' + NL +
    '    INNER JOIN PLANT PL ON ' + NL +
    '      PL.PLANT_CODE = P.PLANT_CODE ' + NL +
    '    INNER JOIN BUSINESSUNIT B ON ' + NL +
    '      B.BUSINESSUNIT_CODE = J.BUSINESSUNIT_CODE ' + NL +
    '    INNER JOIN DEPARTMENT D ON ' + NL +
    '      W.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '      W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  // MRA:24-FEB-2009 RV022.
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      ' LEFT JOIN EMPLOYEE E ON ' + NL +
      '   T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ';
  end;
  // TD-21167 Added this part.
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    W.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    W.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND ' + NL +
      '    DT.TEAM_CODE >= ' +
      MyQuotes(ATeamFrom) + ' AND ' + NL +
      '    DT.TEAM_CODE <= ' +
      MyQuotes(ATeamTo) + ' ' + NL;

    // TD-21167
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      UserName := SystemDM.UserTeamLoginUser
    else
      UserName := '*';

    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    (' + '''' + UserName + '''' + ' = ''*'') OR ' + NL +
      '    (DT.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE ' + NL +
      '       FROM TEAMPERUSER U ' + NL +
      '       WHERE U.USER_NAME = ' + '''' + UserName + '''' + ')) ' + NL +
      '  ) ';
  end;
  // MR:13-03-2007 Order 550443. If GroupEmployeeEfficiency is True then
  //               use an extra where-clause.
  if GroupEmployeeEfficiency then
    SelectStr := SelectStr +
      '  AND W.GROUP_EFFICIENCY_YN = ''N'' ' + NL;
  SelectStr := SelectStr +
    'WHERE  ' + NL;
  if AShowOnlyDownTime then // 20015586
    SelectStr := SelectStr +
      '  ((P.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (P.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  if APlantFrom = APlantTo then
  begin
    SelectStr := SelectStr +
      '  P.PLANT_CODE = ' + MyQuotes(APlantFrom) + ' AND ' + NL;
    if AShowShifts then
      if not AAllShifts then
        SelectStr := SelectStr +
          '  P.SHIFT_NUMBER >= ' + AShiftFrom + ' AND ' + NL +
          '  P.SHIFT_NUMBER <= ' + AShiftTo + ' AND ' + NL;
    // TD-22296 Department-test was missing!
    if not AAllDepartments then
      SelectStr := SelectStr +
        '  W.DEPARTMENT_CODE >= ' + MyQuotes(ADeptFrom) + ' AND ' + NL +
        '  W.DEPARTMENT_CODE <= ' + MyQuotes(ADeptTo) + ' AND ' + NL;
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    begin
      SelectStr := SelectStr +
        ' (' + NL +
        '   (' + NL +
        '     P.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL +
        ' AND ' + NL;
    end
    else
    begin
      if not AAllWorkspots then
        SelectStr := SelectStr +
          '  P.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + ' AND ' + NL +
          '  P.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + ' AND ' + NL;
    end;
  end
  else
    if not AAllPlants then
      SelectStr := SelectStr +
        '  P.PLANT_CODE >= ' + MyQuotes(APlantFrom) + ' AND ' + NL +
        '  P.PLANT_CODE <= ' + MyQuotes(APlantTo) + ' AND ' + NL;
  // 20012858.130.3.
  if not ShowCompareJobs then
    SelectStr := SelectStr +
      '  (J.COMPARATION_JOB_YN = ' +
      '''' + UNCHECKEDVALUE + '''' + ') AND ' + NL;
  SelectStr := SelectStr +
    // 20012858.
    '  (P.JOB_CODE <> ' + '''' + NOJOB + '''' + ') AND ' + NL +
    '  P.QUANTITY <> 0 AND ' + NL;
  if SystemDM.UseShiftDateSystem then
  begin
    // 20015220 (*) -> Use < at this line, to prevent it takes 2 PQ-records when
    //                 period is something like: 9:00 to 9:05.
    if UseDateTime then // 20015220
    begin
      SelectStr := SelectStr +
        '  P.SHIFT_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL +  // :DATEFROM AND ' + NL + // 20015220.110
        '  P.SHIFT_DATE < ' + ToDate(Trunc(ADateTo+1)) + ' AND ' + NL + // :DATETO AND ' + NL + // 20015220.110
        '  DATETIME_RANGE_OVERLAP_MINS(' + ToDate(ADateFrom) + ',' + ToDate(ADateto) + ', P.START_DATE, P.END_DATE) > 0 ' + NL; // 20015220.110
//        '  P.START_DATE >= ' + ToDate(ADateFrom) + ' AND ' + NL +
//        '  P.START_DATE < ' + ToDate(ADateTo) + NL;
    end
    else
      SelectStr := SelectStr +
        // 20013489
        '  P.SHIFT_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL + // 20015220.110
        '  P.SHIFT_DATE < ' + ToDate(Trunc(ADateTo+1)) + NL // :DATETO ' + NL // 20015220.110
  end
  else
  begin
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        // 20015220.110
        '  DATETIME_RANGE_OVERLAP_MINS(' + ToDate(ADateFrom) + ',' + ToDate(ADateto) + ', P.START_DATE, P.END_DATE) > 0 ' + NL
//        '  P.START_DATE >= ' + ToDate(ADateFrom) + ' AND ' + NL +
//        '  P.START_DATE < ' + ToDate(ADateTo) + NL
    else
      SelectStr := SelectStr +
        '  P.START_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL + // 20015220.110
        '  P.START_DATE < ' + ToDate(Trunc(ADateTo+1)) + NL; // :DATETO ' + NL; // 20015220.110
  end;
  // MRA:24-FEB-2009 RV022.
  if not AAllTeams then
  begin
    SelectStr := SelectStr + ' AND ' +
      '   (E.TEAM_CODE >= ' +
      MyQuotes(ATeamFrom) + ' AND ' + NL +
      '   E.TEAM_CODE <= ' +
      MyQuotes(ATeamTo) + ' OR ' + NL +
      '   E.TEAM_CODE IS NULL) ' + NL;
  end;
  // MR:04-10-2007 To get also employees with no pieces, combine with
  // table PRODHOURPEREMPLOYEE.
  SelectStr := SelectStr +
    'UNION ' + NL +
    'SELECT ' + NL +
    '  PHE.PLANT_CODE, PL.DESCRIPTION AS PDESCRIPTION, ' + NL +
    '  J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESCRIPTION, ' + NL +
    '  W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESCRIPTION, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  PHE.SHIFT_NUMBER, ' + NL +
      '  (SELECT S.DESCRIPTION FROM SHIFT S ' + NL +
      '   WHERE S.PLANT_CODE = PHE.PLANT_CODE AND ' + NL +
      '   S.SHIFT_NUMBER = PHE.SHIFT_NUMBER) SDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  PHE.WORKSPOT_CODE, W.DESCRIPTION AS WDESCRIPTION, ' + NL +
    '  PHE.JOB_CODE, J.DESCRIPTION AS JDESCRIPTION, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    ToDate(Trunc(ADateFrom)) + ' AS START_DATE, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS START_DATE, ' + NL + // 20015220.110
    ToDate(Trunc(ADateFrom)) + ' AS END_DATE, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS END_DATE, ' + NL + // 20015220.110
    '  0 AS PIECES, ' + NL +
    '  0 AS MINUTES, ' + NL + // 20015220
    ToDate(Trunc(ADateFrom)) + ' AS DATETIME_IN, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS DATETIME_IN, ' + NL + // 20015220.110
    ToDate(Trunc(ADateFrom)) + ' AS DATETIME_OUT, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS DATETIME_OUT, ' + NL + // 20015220.110
    '  PHE.EMPLOYEE_NUMBER, ' + NL +
    '  PHE.SHIFT_NUMBER TSHIFT_NUMBER, ' + NL +
    '  B.BONUS_FACTOR, ' + NL +
    '  J.NORM_PROD_LEVEL ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE PHE INNER JOIN WORKSPOT W ON ' + NL +
    '    PHE.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    PHE.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  // MR:13-03-2007 Order 550443. If GroupEmployeeEfficiency is True then
  //               use an extra where-clause.
  if GroupEmployeeEfficiency then
    SelectStr := SelectStr +
      '  AND W.GROUP_EFFICIENCY_YN = ''N'' ' + NL;
  SelectStr := SelectStr +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    PHE.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '    PHE.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    PHE.JOB_CODE = J.JOB_CODE ' + NL +
    '  INNER JOIN PLANT PL ON ' + NL +
    '    PHE.PLANT_CODE = PL.PLANT_CODE ' + NL +
    '  INNER JOIN BUSINESSUNIT B ON ' + NL +
    '    B.BUSINESSUNIT_CODE = J.BUSINESSUNIT_CODE ' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    W.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  // MRA:24-FEB-2009 RV022.
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      ' INNER JOIN EMPLOYEE E ON ' + NL +
      '   PHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
      '   E.TEAM_CODE >= ' +
      MyQuotes(ATeamFrom) + ' AND ' + NL +
      '   E.TEAM_CODE <= ' +
      MyQuotes(ATeamTo) + ' ' + NL;

  end;
  // TD-21167 Added this part.
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    W.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    W.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND ' + NL +
      '    DT.TEAM_CODE >= ' +
      MyQuotes(ATeamFrom) + ' AND ' + NL +
      '    DT.TEAM_CODE <= ' +
      MyQuotes(ATeamTo) + ' ' + NL;
    //RV067.2.

      // TD-21167
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        UserName := SystemDM.UserTeamLoginUser
      else
        UserName := '*';

    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    (' + '''' + UserName + '''' + ' = ''*'') OR ' + NL +
      '    (DT.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE ' + NL +
      '       FROM TEAMPERUSER U ' + NL +
      '       WHERE U.USER_NAME = ' + '''' + UserName + '''' + ')) ' + NL +
      '  ) ';
  end;
  SelectStr := SelectStr +
    'WHERE ' + NL;
  if AShowOnlyDownTime then // 20015586
    SelectStr := SelectStr +
      '  ((PHE.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PHE.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  if APlantFrom = APlantTo then
  begin
    SelectStr := SelectStr +
      '  PHE.PLANT_CODE = ' + MyQuotes(APlantFrom) + ' AND ' + NL;
    if AShowShifts then
      if not AAllShifts then
        SelectStr := SelectStr +
          '  PHE.SHIFT_NUMBER >= ' + AShiftFrom + ' AND ' + NL +
          '  PHE.SHIFT_NUMBER <= ' + AShiftTo + ' AND ' + NL;
    // TD-22296 BusinessUnit-test was missing!
    if not AAllBusinessUnits then
      SelectStr := SelectStr +
        '  J.BUSINESSUNIT_CODE >= ' + MyQuotes(ABusinessUnitFrom) + ' AND ' + NL +
        '  J.BUSINESSUNIT_CODE <= ' + MyQuotes(ABusinessUnitTo) + ' AND ' + NL;
    // TD-22296 Department-test was missing!
    if not AAllDepartments then
      SelectStr := SelectStr +
        '  W.DEPARTMENT_CODE >= ' + MyQuotes(ADeptFrom) + ' AND ' + NL +
        '  W.DEPARTMENT_CODE <= ' + MyQuotes(ADeptTo) + ' AND ' + NL;
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    begin
      SelectStr := SelectStr +
        ' (' + NL +
        '   (' + NL +
        '     PHE.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL +
        ' AND ' + NL;
    end
    else
    begin
      if not AAllWorkspots then
        SelectStr := SelectStr +
          '  PHE.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + ' AND ' + NL +
          '  PHE.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + ' AND ' + NL;
    end;
  end
  else
    if not AAllPlants then
      SelectStr := SelectStr +
        '  PHE.PLANT_CODE >= ' + MyQuotes(APlantFrom) + ' AND ' + NL +
        '  PHE.PLANT_CODE <= ' + MyQuotes(APlantTo) + ' AND ' + NL;
  SelectStr := SelectStr +
    '  PHE.PRODHOUREMPLOYEE_DATE >= ' + ToDate(Trunc(ADateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL + // 20015220.110
    '  PHE.PRODHOUREMPLOYEE_DATE < ' + ToDate(Trunc(ADateTo+1)) + NL +  // :DATETO ' + NL + // 20015220.110
    'GROUP BY ' + NL +
    '  PHE.PLANT_CODE, PL.DESCRIPTION, ' + NL +
    '  J.BUSINESSUNIT_CODE, B.DESCRIPTION, ' + NL +
    '  W.DEPARTMENT_CODE, D.DESCRIPTION, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  PHE.SHIFT_NUMBER, NULL, ' + NL;
  SelectStr := SelectStr +
    '  PHE.WORKSPOT_CODE, W.DESCRIPTION, ' + NL +
    '  PHE.JOB_CODE, J.DESCRIPTION, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  NULL, ' + NL + // 20015220
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  PHE.EMPLOYEE_NUMBER, ' + NL +
    '  PHE.SHIFT_NUMBER, ' + NL +
    '  B.BONUS_FACTOR, ' + NL +
    '  J.NORM_PROD_LEVEL ' + NL;
// 20015220
// Table: Timeregscanning
  if UseDateTime or IncludeOpenScans then
  begin
    SelectStr := SelectStr +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  T.PLANT_CODE, PL.DESCRIPTION AS PDESCRIPTION, ' + NL +
      '  J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESCRIPTION, ' + NL +
      '  W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESCRIPTION, ' + NL;
    if AShowShifts then
      SelectStr := SelectStr +
        '  T.SHIFT_NUMBER, ' + NL +
        '  (SELECT S.DESCRIPTION FROM SHIFT S ' + NL +
        '   WHERE S.PLANT_CODE = T.PLANT_CODE AND ' + NL +
        '   S.SHIFT_NUMBER = T.SHIFT_NUMBER) SDESCRIPTION, ' + NL;
    SelectStr := SelectStr +
      '  T.WORKSPOT_CODE, W.DESCRIPTION AS WDESCRIPTION, ' + NL +
      '  T.JOB_CODE, J.DESCRIPTION AS JDESCRIPTION, ' + NL +
      '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
      ToDate(Trunc(ADateFrom)) + ' AS START_DATE, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS START_DATE, ' + NL + // 20015220.110
      ToDate(Trunc(ADateFrom)) + ' AS END_DATE, ' + NL + // '  TO_DATE(:DATEFROM, ''dd-mm-yyyy HH24:MI'') AS END_DATE, ' + NL + // 20015220.110
      '  -1 AS PIECES, ' + NL +
      '  DATETIME_RANGE_OVERLAP_MINS(' + ToDate(ADateFrom) +
      ',' + ToDate(ADateTo) + ', T.DATETIME_IN, ' + 
      OpenScan('T.DATETIME_OUT') + ') AS MINUTES, ' + NL + // 20015221
      '  T.DATETIME_IN, ' + NL +
      OpenScan('T.DATETIME_OUT', True) + ', ' + NL + // 20015221
      '  T.EMPLOYEE_NUMBER, ' + NL +
      '  T.SHIFT_NUMBER TSHIFT_NUMBER, ' + NL +
      '  B.BONUS_FACTOR, ' + NL +
      '  J.NORM_PROD_LEVEL ' + NL +
      '  FROM ' + NL +
      '    TIMEREGSCANNING T INNER JOIN WORKSPOT W ON ' + NL +
      '      T.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '      T.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
    if GroupEmployeeEfficiency then
      SelectStr := SelectStr +
        '  AND W.GROUP_EFFICIENCY_YN = ''N'' ' + NL;
    SelectStr := SelectStr +
      '    INNER JOIN JOBCODE J ON ' + NL +
      '      T.PLANT_CODE = J.PLANT_CODE AND ' + NL +
      '      T.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
      '      T.JOB_CODE = J.JOB_CODE ' + NL +
      '    INNER JOIN PLANT PL ON ' + NL +
      '      T.PLANT_CODE = PL.PLANT_CODE ' + NL +
      '    INNER JOIN BUSINESSUNIT B ON ' + NL +
      '      B.BUSINESSUNIT_CODE = J.BUSINESSUNIT_CODE ' + NL +
      '    INNER JOIN DEPARTMENT D ON ' + NL +
      '      W.PLANT_CODE = D.PLANT_CODE AND ' + NL +
      '      W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
    if not AAllTeams then
    begin
      SelectStr := SelectStr +
        ' INNER JOIN EMPLOYEE E ON ' + NL +
        '   T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
        '   E.TEAM_CODE >= ' +
        MyQuotes(ATeamFrom) + ' AND ' + NL +
        '   E.TEAM_CODE <= ' +
        MyQuotes(ATeamTo) + ' ' + NL;
    end;
    if not AAllTeams then
    begin
      SelectStr := SelectStr +
        '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
        '    W.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
        '    W.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND ' + NL +
        '    DT.TEAM_CODE >= ' +
        MyQuotes(ATeamFrom) + ' AND ' + NL +
        '    DT.TEAM_CODE <= ' +
        MyQuotes(ATeamTo) + ' ' + NL;
        if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
          UserName := SystemDM.UserTeamLoginUser
        else
          UserName := '*';
      SelectStr := SelectStr +
        '  AND ( ' + NL +
        '    (' + '''' + UserName + '''' + ' = ''*'') OR ' + NL +
        '    (DT.TEAM_CODE IN ' + NL +
        '      (SELECT U.TEAM_CODE ' + NL +
        '       FROM TEAMPERUSER U ' + NL +
        '       WHERE U.USER_NAME = ' + '''' + UserName + '''' + ')) ' + NL +
        '  ) ';
    end;
    SelectStr := SelectStr +
      'WHERE ' + NL;
    if AShowOnlyDownTime then // 20015586
      SelectStr := SelectStr +
        '  ((T.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
        '   (T.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
    if APlantFrom = APlantTo then
    begin
      SelectStr := SelectStr +
        '  T.PLANT_CODE = ' + MyQuotes(APlantFrom) + ' AND ' + NL;
      if AShowShifts then
        if not AAllShifts then
          SelectStr := SelectStr +
            '  T.SHIFT_NUMBER >= ' + AShiftFrom + ' AND ' + NL +
            '  T.SHIFT_NUMBER <= ' + AShiftTo + ' AND ' + NL;
      // TD-22296 BusinessUnit-test was missing!
      if not AAllBusinessUnits then
        SelectStr := SelectStr +
          '  J.BUSINESSUNIT_CODE >= ' + MyQuotes(ABusinessUnitFrom) + ' AND ' + NL +
          '  J.BUSINESSUNIT_CODE <= ' + MyQuotes(ABusinessUnitTo) + ' AND ' + NL;
      // TD-22296 Department-test was missing!
      if not AAllDepartments then
        SelectStr := SelectStr +
          '  W.DEPARTMENT_CODE >= ' + MyQuotes(ADeptFrom) + ' AND ' + NL +
          '  W.DEPARTMENT_CODE <= ' + MyQuotes(ADeptTo) + ' AND ' + NL;
      if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
      begin
        SelectStr := SelectStr +
          ' (' + NL +
          '   (' + NL +
          '     T.WORKSPOT_CODE IN ' + NL +
          '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
          '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
          '      AND ' + NL +
          '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') ' + NL +
          '   )' + NL +
          ' OR ' + NL +
          '   ( ' + NL +
          '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
          '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
          '      AND ' + NL +
          '      PW.PLANT_CODE = ' + '''' + APlantFrom + '''' + ') = 0 ' + NL +
          '   ) ' + NL +
          ' ) ' + NL +
          ' AND ' + NL;
      end
      else
      begin
        if not AAllWorkspots then
          SelectStr := SelectStr +
            '  T.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + ' AND ' + NL +
            '  T.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + ' AND ' + NL;
      end;
    end
    else
      if not AAllPlants then
        SelectStr := SelectStr +
          '  T.PLANT_CODE >= ' + MyQuotes(APlantFrom) + ' AND ' + NL +
          '  T.PLANT_CODE <= ' + MyQuotes(APlantTo) + ' AND ' + NL;
    SelectStr := SelectStr +
      OpenScan('T.DATETIME_OUT') + ' IS NOT NULL AND ' + NL + // 20015221
      OpenScan('T.DATETIME_OUT') + ' >= ' + ToDate(ADateFrom) + ' AND ' + NL + // 20015221
      '  T.DATETIME_IN < ' + ToDate(ADateTo) + NL;
    SelectStr := SelectStr +
      'GROUP BY ' + NL +
      '  T.PLANT_CODE, PL.DESCRIPTION, ' + NL +
      '  J.BUSINESSUNIT_CODE, B.DESCRIPTION, ' + NL +
      '  W.DEPARTMENT_CODE, D.DESCRIPTION, ' + NL;
    if AShowShifts then
      SelectStr := SelectStr +
        '  T.SHIFT_NUMBER, NULL, ' + NL;
    SelectStr := SelectStr +
      '  T.WORKSPOT_CODE, W.DESCRIPTION, ' + NL +
      '  T.JOB_CODE, J.DESCRIPTION, ' + NL +
      '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL +
      '  NULL, ' + NL +
      '  NULL, ' + NL +
      '  NULL, ' + NL +
      '  NULL, ' + NL +
      '  NULL, ' + NL +
      '  T.DATETIME_IN, ' + NL +
      '  T.DATETIME_OUT, ' + NL +
      '  T.EMPLOYEE_NUMBER, ' + NL +
      '  T.SHIFT_NUMBER, ' + NL +
      '  B.BONUS_FACTOR, ' + NL +
      '  J.NORM_PROD_LEVEL ' + NL;
  end; // if
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, ' + NL;
  // TD-21167 Do Not sort on Shift or the determination of pieces goes wrong!
{  if AShowShifts then
    SelectStr := SelectStr +
      '  SHIFT_NUMBER, ' + NL; }
  SelectStr := SelectStr +
    '  WORKSPOT_CODE, JOB_CODE, ' + NL +
// 20012858.130.2. Bugfix. MRA:23-JUL-2012 Add StartDate to order by
    '  START_DATE, ' + NL +
    '  DATETIME_IN ';
//    '  EMPLOYEE_NUMBER ';
  Result := SelectStr;
end; // ReturnMainQuery

// 20015221
function TReportProductionDetailDM.OpenScan(AValue: String; ASelect: Boolean=False): String;
begin
  try
    if IncludeOpenScans then
    begin
      // IMPORTANT: Do not use 'CASE T.DATETIME_OUT WHEN NULL ...' because that always gives false!!!
      if ASelect then
        Result :=
          '  CASE ' + NL +
          '    WHEN T.DATETIME_OUT IS NULL THEN OPEN_SCAN(T.DATETIME_IN, T.DATETIME_OUT) ' + NL +
          '    ELSE T.DATETIME_OUT ' + NL +
          '  END DATETIME_OUT '
      else
        Result :=
          '  CASE ' + NL +
          '    WHEN T.DATETIME_OUT IS NULL THEN OPEN_SCAN(T.DATETIME_IN, T.DATETIME_OUT) ' + NL +
          '    ELSE T.DATETIME_OUT ' + NL +
          '  END ';
    end
    else
      Result := AValue;
  except
    Result := AValue;
  end;
end;

// 20015220 + 20015221
function TReportProductionDetailDM.DetermineTimeRecProdMins(
  ADateFrom, ADateTo: TDateTime;
  APlantCode, AWorkspotCode, AJobCode, ADepartmentCode, ABusinessUnitCode: String;
  AEmployeeNumber: Integer;
  AShiftNumber: Integer;
  ADate: TDateTime;
  ADefaultValue: Integer): Integer;
var
  StartDate: TDateTime;
  MyMinutesBetween: Integer;
begin
  Result := 0;
  MyMinutesBetween := MinutesBetween(ADateFrom, ADateTo);
  if UseDateTime or IncludeOpenScans then
  begin
    qryMain.Filtered := False;
    qryMain.Filter :=
      'PLANT_CODE = ' + '''' + APlantCode + '''' +
      ' AND WORKSPOT_CODE = ' + '''' + AWorkspotCode + '''' +
      ' AND JOB_CODE = ' + '''' + AJobCode + '''';
      if ADepartmentCode <> '' then
        qryMain.Filter := qryMain.Filter +
          ' AND DEPARTMENT_CODE = ' + '''' + ADepartmentCode + '''';
      if AEmployeeNumber <> -1 then
        qryMain.Filter := qryMain.Filter +
          ' AND EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
      if ABusinessUnitCode <> '' then
        qryMain.Filter := qryMain.Filter +
          ' AND BUSINESSUNIT_CODE = ' + '''' + ABusinessUnitCode  + '''';
      if AShiftNumber > 0 then
        qryMain.Filter := qryMain.Filter +
          ' AND SHIFT_NUMBER = ' + IntToStr(AShiftNumber);
      qryMain.Filter := qryMain.Filter +
        ' AND MINUTES <> 0 ';
    qryMain.Filtered := True;
    while not qryMain.Eof do
    begin
{$IFDEF DEBUG14}
  WDebugLog('Start_Date=' + DateTimeToStr(qryMain.FieldByName('START_DATE').AsDateTime) +
    ' PlantCode=' + qryMain.FieldByName('PLANT_CODE').AsString +
    ' WorkspotCode=' + qryMain.FieldByName('WORKSPOT_CODE').AsString +
    ' JobCode=' + qryMain.FieldByName('JOB_CODE').AsString +
    ' Dept=' + qryMain.FieldByName('DEPARTMENT_CODE').AsString +
    ' Emp=' + qryMain.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' DateIn=' + DateTimeToStr(qryMain.FieldByName('DATETIME_IN').AsDateTime) +
    ' Minutes=' + IntToStr(qryMain.FieldByName('MINUTES').AsInteger)
    );
{$ENDIF}
      // Start_date has a invalid date! It shows year 0014 instead of 2014 ???
      // A temp. solution is to use 'Datetime_in' instead of 'Start_date'.
      if ADate <> NullDate then
      begin
        StartDate := Trunc(qryMain.FieldByName('DATETIME_IN').AsDateTime);
        if StartDate = ADate then
          if qryMain.FieldByName('MINUTES').AsString <> '' then
            Result := Result +
              qryMain.FieldByName('MINUTES').AsInteger;
      end
      else
      begin
        if qryMain.FieldByName('MINUTES').AsInteger <= MyMinutesBetween then
          Result := Result +
            qryMain.FieldByName('MINUTES').AsInteger
        else
          Break;
      end;
      qryMain.Next;
    end; // while
    qryMain.Filtered := False;
    if Result > MyMinutesBetween then
      Result := MyMinutesBetween;
  end // if
  else
    Result := ADefaultValue;
end; // DetermineTimeRecProdMins

// 20015220
// Convert a date to a TO_DATE-string that can be used in a query.
function TReportProductionDetailDM.ToDate(ADate: TDateTime): String;
var
  Year, Month, Day, Hours, Minutes, Seconds, MSeconds: Word;
  function FillZero(Value: String; Len: Integer): String;
  begin
    Result := Trim(Value);
    while Length(Result) < Len do
      Result := '0' + Result;
  end;
begin
  Result := '';
  try
    // Make a string like this:
    // TO_DATE('2013-09-10 08:16', 'YYYY-MM-DD HH24:MI')
    DecodeDate(ADate, Year, Month, Day);
    DecodeTime(ADate, Hours, Minutes, Seconds, MSeconds);
    Result := 'TO_DATE(''' +
      Format('%s-%s-%s %s:%s',
        [IntToStr(Year),
         FillZero(IntToStr(Month), 2),
         FillZero(IntToStr(Day), 2),
         FillZero(IntToStr(Hours), 2),
         FillZero(IntToStr(Minutes), 2)
        ]) +
      '''' + ', ''YYYY-MM-DD HH24:MI'')';
  except
    Result := '';
    WErrorLog('Error during ToDate-function!');
  end;
end; // ToDate

// 20015520 + 20015521
// This must be called first by a report where this module is used.
procedure TReportProductionDetailDM.Init;
begin
  ShowCompareJobs := False;
  EmpPiecesPerDate := False;
  GroupEmployeeEfficiency := False;
  ContractGroupTFT := GlobalDM.ContractGroupTFTExists;
  SystemDM.PlantTeamFilterEnable(QueryProduction);
  EnableWorkspotIncludeForThisReport := False;
end; // Init

// 20014450.50
// PIM-135 Use IncludeDownTime-setting.
function TReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
  const APlantFrom, APlantTo, ABusinessUnitFrom, ABusinessUnitTo,
  ADeptFrom, ADeptTo, AWorkspotFrom, AWorkspotTo, AJobFrom, AJobTo,
  ATeamFrom, ATeamTo,
  AShiftFrom, AShiftTo, AEmployeeFrom, AEmployeeTo: String;
  const ADateFrom, ADateTo: TDateTime;
  const AAllPlants, AAllBusinessUnits, AAllDepartments, AAllWorkspots,
  AAllTeams, AAllShifts, AShowShifts: Boolean;
  const AShowOnlyJob: Boolean;
  const AIncludeDownTime: Integer; // PIM-135
  const ASelectShift, ASelectDate: Boolean;
  const GroupBy: Integer=0; // 0=GroupBy BU + Dept 1=GroupBy BU 2=GroupBy Dept
  const SortByEmp: Integer=0; // 0=SortByEmp Nr 1=SortByEmp Name
  const FilterJob: Boolean=False; // Ttue=Filter on job False=not
  const TeamIncentiveReport: Boolean=False;
  const SortByJob: Integer = 0 // 0=SortByJob Code 1=SortByJob Description
  ): String;
var
  SelectStr: String;
  UserName: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  V.PLANT_CODE, V.PDESCRIPTION, V.AVERAGE_WAGE, ' + NL;
  if GroupBy = 0 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if GroupBy = 1 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL;
  if GroupBy = 2 then
    SelectStr := SelectStr +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  V.SHIFT_NUMBER, V.SDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  V.WORKSPOT_CODE, V.WDESCRIPTION, ' + NL +
    '  V.JOB_CODE, V.JDESCRIPTION, V.IGNOREQUANTITIESINREPORTS_YN, V.NORM_OUTPUT_LEVEL, ' + NL;
  if AShowOnlyJob then
    SelectStr := SelectStr +
      '  V.JOB_CODE || '' (''' + ' || V.WORKSPOT_CODE || '')'' AS JOB_CODEWK, ' + NL;
  if GroupBy = 1 then
    SelectStr := SelectStr +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if GroupBy = 2 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL;
  SelectStr := SelectStr +
    '  V.COMPARATION_JOB_YN, V.NORMLEVEL NORM_PROD_LEVEL,' + NL +
    '  V.EMPLOYEE_NUMBER, V.EDESCRIPTION, ' + NL;
  if ASelectDate then
    SelectStr := SelectStr +
      '  V.SHIFT_DATE, ' + NL;
  SelectStr := SelectStr +
    '  SUM(V.EMPLOYEESECONDSACTUAL) EMPLOYEESECONDSACTUAL, ' + NL +
    '  SUM(V.EMPLOYEEQUANTITY) EMPLOYEEQUANTITY, ' + NL +
    '  SUM(V.EMPLOYEESECONDSNORM) EMPLOYEESECONDSNORM, ' + NL +
    '  SUM(V.REVENUE_AMOUNT) REVENUE_AMOUNT, ' + NL +
    '  SUM(V.HOURLY_WAGE) HOURLY_WAGE, ' + NL +
    '  SUM(V.PRODHOURMIN) PRODHOURMIN ' + NL + // PIM-135
    'FROM ' + NL +
    '  V_PRODDETAILS V ' + NL;
// This gives multiple records when selecting multiple plants!
{
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    V.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    V.DEPARTMENT_CODE = DT.DEPARTMENT_CODE AND ' + NL +
      '    DT.TEAM_CODE >= ' +
      MyQuotes(ATeamFrom) + ' AND ' + NL +
      '    DT.TEAM_CODE <= ' +
      MyQuotes(ATeamTo) + ' ' + NL;
  end;
}
  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  V.PLANT_CODE >= ' + MyQuotes(APlantFrom) + NL +
    '  AND V.PLANT_CODE <= ' + MyQuotes(APlantTo) + NL +
    '  AND V.SHIFT_DATE >= ' + ToDate(Trunc(ADateFrom)) + NL +
    '  AND V.SHIFT_DATE <= ' + ToDate(Trunc(ADateTo)) + NL;
  // PIM-12.4 ALso filter on CompareJobs
  if not ShowCompareJobs then
    SelectStr := SelectStr +
      '  AND (V.COMPARATION_JOB_YN = ' +
      '''' + UNCHECKEDVALUE + '''' + ') ' + NL;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    UserName := SystemDM.UserTeamLoginUser
  else
    UserName := '*';
  SelectStr := SelectStr +
    '  AND ( ' + NL +
    '    (' + MyQuotes(UserName) + ' = ''*'') OR ' + NL +
    '    (V.TEAM_CODE IN ' + NL +
    '      (SELECT U.TEAM_CODE ' + NL +
    '       FROM TEAMPERUSER U ' + NL +
    '       WHERE U.USER_NAME = ' + MyQuotes(UserName) + ')) ' + NL +
    '  ) ';

  // PIM-135
  case AIncludeDownTime of
  DOWNTIME_NO:
    SelectStr := SelectStr +
      '  AND NOT (((V.JOB_CODE = ' + MyQuotes(DOWNJOB_1) + ') OR ' + NL +
      '   (V.JOB_CODE = ' + MyQuotes(DOWNJOB_2) + '))) ' + NL;
  DOWNTIME_YES:
      ;
  DOWNTIME_ONLY:
    SelectStr := SelectStr +
      '  AND ((V.JOB_CODE = ' + MyQuotes(DOWNJOB_1) + ') OR ' + NL +
      '   (V.JOB_CODE = ' + MyQuotes(DOWNJOB_2) + ')) ' + NL;
  end;

  if APlantFrom = APlantTo then
  begin
    if not AAllDepartments then
      SelectStr := SelectStr +
        '  AND V.DEPARTMENT_CODE >= ' + MyQuotes(ADeptFrom) + NL +
        '  AND V.DEPARTMENT_CODE <= ' + MyQuotes(ADeptTo) + NL;

    if (AEmployeeFrom <> '') and (AEmployeeTo <> '') then
    begin
      SelectStr := SelectStr +
        '  AND V.EMPLOYEE_NUMBER >= ' + AEmployeeFrom + NL +
        '  AND V.EMPLOYEE_NUMBER <= ' + AEmployeeTo + NL;
    end;

    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    begin
      SelectStr := SelectStr +
        ' AND ' +
        ' (' + NL +
        '   (' + NL +
        '     V.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + MyQuotes(SystemDM.CurrentComputerName) +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + MyQuotes(APlantFrom) + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + MyQuotes(SystemDM.CurrentComputerName) +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + MyQuotes(APlantFrom) + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
    begin
      if not AAllWorkspots then
      begin
        SelectStr := SelectStr +
          '  AND V.WORKSPOT_CODE >= ' + MyQuotes(AWorkspotFrom) + NL +
          '  AND V.WORKSPOT_CODE <= ' + MyQuotes(AWorkspotTo) + NL;
        if AWorkspotFrom = AWorkspotTo then
          if FilterJob then
            SelectStr := SelectStr +
              '  AND V.JOB_CODE >= ' + MyQuotes(AJobFrom) + NL +
              '  AND V.JOB_CODE <= ' + MyQuotes(AJobTo) + NL;
      end;
    end;
  end;
  if not AAllTeams then
  begin
    SelectStr := SelectStr +
      '  AND (V.TEAM_CODE >= ' + MyQuotes(ATeamFrom) + NL +
      '  AND  V.TEAM_CODE <= ' + MyQuotes(ATeamTo) + ' OR ' + NL +
      '       V.TEAM_CODE IS NULL) ' + NL;
  end;

  // GROUP BY
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  V.PLANT_CODE, V.PDESCRIPTION, V.AVERAGE_WAGE, ' + NL;
  if GroupBy = 0 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if GroupBy = 1 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL;
  if GroupBy = 2 then
    SelectStr := SelectStr +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  V.SHIFT_NUMBER, V.SDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  V.WORKSPOT_CODE, V.WDESCRIPTION, ' + NL +
    '  V.JOB_CODE, V.JDESCRIPTION, V.IGNOREQUANTITIESINREPORTS_YN, V.NORM_OUTPUT_LEVEL, ' + NL;
  if GroupBy = 1 then
    SelectStr := SelectStr +
      '  V.DEPARTMENT_CODE, V.DDESCRIPTION, ' + NL;
  if GroupBy = 2 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, V.BDESCRIPTION, V.BONUS_FACTOR, ' + NL;
  SelectStr := SelectStr +
    '  V.COMPARATION_JOB_YN, V.NORMLEVEL, ' + NL +
    '  V.EMPLOYEE_NUMBER, V.EDESCRIPTION ' + NL;
  if ASelectDate then
    SelectStr := SelectStr +
    '  , V.SHIFT_DATE ' + NL;

  // ORDER BY
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  V.PLANT_CODE, ' + NL;
  if TeamIncentiveReport then
  begin
    if SortByEmp = 0 then
      SelectStr := SelectStr +
        '  V.EMPLOYEE_NUMBER, ' + NL
    else
      SelectStr := SelectStr +
        '  V.EDESCRIPTION, ' + NL;
    if ASelectDate then
       SelectStr := SelectStr +
         '  V.SHIFT_DATE, ';
  end;
  if GroupBy = 0 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, ' + NL +
      '  V.DEPARTMENT_CODE, ' + NL;
  if GroupBy = 1 then
    SelectStr := SelectStr +
      '  V.BUSINESSUNIT_CODE, ' + NL;
  if GroupBy = 2 then
    SelectStr := SelectStr +
      '  V.DEPARTMENT_CODE, ' + NL;
  if AShowShifts then
    SelectStr := SelectStr +
      '  V.SHIFT_NUMBER, ' + NL;
  if AShowOnlyJob then
  begin
    if SortByJob = 0 then
      SelectStr := SelectStr +
        '  V.JOB_CODE, ' + NL +
        '  V.WORKSPOT_CODE ' + NL
    else
      SelectStr := SelectStr +
        '  V.JDESCRIPTION, ' + NL +
        '  V.WORKSPOT_CODE ' + NL
  end
  else
  begin
    if SortByJob = 0 then
      SelectStr := SelectStr +
        '  V.WORKSPOT_CODE, ' + NL +
        '  V.JOB_CODE ' + NL
    else
      SelectStr := SelectStr +
        '  V.WORKSPOT_CODE, ' + NL +
        '  V.JDESCRIPTION ' + NL
  end;
  if not TeamIncentiveReport then
  begin
    if SortByEmp = 0 then
      SelectStr := SelectStr +
        '  ,V.EMPLOYEE_NUMBER ' + NL
    else
      SelectStr := SelectStr +
        '  ,V.EDESCRIPTION ' + NL;
  end;
  Result := SelectStr;
end; // RealTimeEfficiencyBuildQuery

// PIM-137
function TReportProductionDetailDM.DetermineOvertime(APlantCode,
  AWorkspotCode, AJobCode, ADepartmentCode: String;
  AEmployeeNumber: Integer;
  AContractGroupTFT: Boolean;
  var ABonusInMoney: Boolean): Integer;
begin
  Result := 0;
  ABonusInMoney := False;
  with qryPHEPT do
  begin
    Filtered := False;
    Filter :=
      'PLANT_CODE=' + MyQuotes(APlantCode) +
      ' AND WORKSPOT_CODE=' + MyQuotes(AWorkspotCode) +
      ' AND JOB_CODE=' + MyQuotes(AJobCode) +
      ' AND EMPLOYEE_NUMBER=' + IntToStr(AEmployeeNumber) +
      ' AND DEPARTMENT_CODE=' + MyQuotes(ADepartmentCode) +
      ' AND OVERTIME_YN=' + '''' + 'Y' + ''''; // PIM-137 Also look for this!
    Filtered := True;
    if not Eof then
    begin
      if FieldByName('OVERTIME_YN').AsString = 'Y' then
        Result := FieldByName('SUMPRODMIN').AsInteger; // OVERTIMEMINS
      if AContractGroupTFT then
        ABonusInMoney :=
          FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y'
      else
        ABonusInMoney :=
          FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y';
    end; // if
  end; // with
end; // DetermineOvertime

end.
