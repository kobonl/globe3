inherited AbsTypePerCountryF: TAbsTypePerCountryF
  Left = 441
  Top = 253
  Width = 650
  Height = 409
  Caption = 'Absence types per country'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 642
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 640
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 640
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 141
        BandIndex = 0
        RowIndex = 0
        FieldName = 'code'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Description'
      end
      object dxMasterGridColumnExportType: TdxDBGridColumn
        Caption = 'Export Type'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Export_Type'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 283
    Width = 642
    Height = 92
    TabOrder = 3
    object LabelDescription: TLabel
      Left = 28
      Top = 7
      Width = 79
      Height = 13
      Caption = 'Type description'
    end
    object LabelExportCode: TLabel
      Left = 48
      Top = 34
      Width = 58
      Height = 13
      Caption = 'Export code'
    end
    object Label1: TLabel
      Left = 28
      Top = 34
      Width = 30
      Height = 13
      Caption = 'Active'
      Visible = False
    end
    object Label2: TLabel
      Left = 28
      Top = 61
      Width = 71
      Height = 13
      Caption = 'Counter active'
    end
    object Label3: TLabel
      Left = 248
      Top = 61
      Width = 67
      Height = 13
      Caption = 'Name counter'
    end
    object DBEditDescription: TdxDBEdit
      Tag = 1
      Left = 128
      Top = 3
      Width = 385
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'DESCRIPTION'
      DataSource = AbsTypePerCountryDM.DataSourceDetail
      ReadOnly = False
      StoredValues = 64
    end
    object dxDBCheckEditActive: TdxDBCheckEdit
      Left = 128
      Top = 31
      Width = 121
      TabOrder = 1
      Visible = False
      DataField = 'Active_YN'
      DataSource = AbsTypePerCountryDM.DataSourceDetail
      ReadOnly = False
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
      StoredValues = 64
    end
    object DBEditCounterName: TDBEdit
      Left = 327
      Top = 58
      Width = 186
      Height = 19
      Ctl3D = False
      DataField = 'COUNTER_DESCRIPTION'
      DataSource = AbsTypePerCountryDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 4
    end
    object DBEditExportCode: TDBEdit
      Left = 128
      Top = 31
      Width = 186
      Height = 19
      Ctl3D = False
      DataField = 'EXPORT_CODE'
      DataSource = AbsTypePerCountryDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 2
    end
    object dxDBCheckEditCounterActive: TdxDBCheckEdit
      Left = 125
      Top = 58
      Width = 27
      TabOrder = 3
      DataField = 'Counter_Active_YN'
      DataSource = AbsTypePerCountryDM.DataSourceDetail
      ReadOnly = False
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
      StoredValues = 64
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 642
    Height = 128
    inherited spltDetail: TSplitter
      Top = 124
      Width = 640
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 640
      Height = 123
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Absence types'
        end>
      KeyField = 'ABSENCETYPE_CODE'
      OnEnter = dxDetailGridEnter
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 141
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCETYPE_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 326
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnExportCode: TdxDBGridColumn
        Caption = 'Export code'
        Width = 146
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
      object dxDetailGridColumnCounterActive: TdxDBGridCheckColumn
        Caption = 'Counter Active'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_ACTIVE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnCounterDescription: TdxDBGridColumn
        Caption = 'Name Counter'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_DESCRIPTION'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
      OnClick = dxBarBDBNavDeleteClick
    end
  end
end
