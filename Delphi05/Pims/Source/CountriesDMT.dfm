inherited CountriesDM: TCountriesDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableDetail: TTable
    Top = 123
  end
  inherited DataSourceDetail: TDataSource
    DataSet = QueryDetail
    Top = 116
  end
  inherited DataSourceExport: TDataSource
    Left = 480
  end
  object QueryDetail: TQuery
    BeforePost = DefaultBeforePost
    AfterPost = QueryDetailAfterPost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    RequestLive = True
    SQL.Strings = (
      'SELECT'
      '  C.COUNTRY_ID,'
      '  C.CODE,'
      '  C.DESCRIPTION,'
      '  C.EXPORT_TYPE,'
      '  C.CREATIONDATE,'
      '  C.MUTATIONDATE,'
      '  C.MUTATOR,'
      '  C.TFT_HOL_AUTO_REPLACE_YN,'
      '  C.TFT_ABSENCEREASON_ID,'
      '  C.HOL_ABSENCEREASON_ID'
      'FROM'
      '  COUNTRY C'
      'ORDER BY'
      '   C.CODE'
      ' ')
    Left = 96
    Top = 176
    object QueryDetailCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Origin = 'COUNTRY.COUNTRY_ID'
    end
    object QueryDetailCODE: TStringField
      FieldName = 'CODE'
      Origin = 'COUNTRY.CODE'
      Size = 3
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'COUNTRY.DESCRIPTION'
      Size = 30
    end
    object QueryDetailEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Origin = 'COUNTRY.EXPORT_TYPE'
      Size = 6
    end
    object QueryDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Origin = 'COUNTRY.CREATIONDATE'
    end
    object QueryDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Origin = 'COUNTRY.MUTATIONDATE'
    end
    object QueryDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Origin = 'COUNTRY.MUTATOR'
    end
    object QueryDetailTFT_HOL_AUTO_REPLACE_YN: TStringField
      FieldName = 'TFT_HOL_AUTO_REPLACE_YN'
      Origin = 'COUNTRY.TFT_HOL_AUTO_REPLACE_YN'
      Size = 1
    end
    object QueryDetailTFT_ABSENCEREASON_ID: TFloatField
      FieldName = 'TFT_ABSENCEREASON_ID'
      Origin = 'COUNTRY.TFT_ABSENCEREASON_ID'
    end
    object QueryDetailHOL_ABSENCEREASON_ID: TFloatField
      FieldName = 'HOL_ABSENCEREASON_ID'
      Origin = 'COUNTRY.HOL_ABSENCEREASON_ID'
    end
  end
  object DataSourceExportPayroll: TDataSource
    DataSet = TableExportPayroll
    Left = 184
    Top = 316
  end
  object TableExportPayroll: TTable
    OnCalcFields = TableExportCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EXPORTPAYROLL'
    Left = 76
    Top = 316
    object TableExportPayrollEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Required = True
      Size = 6
    end
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT * FROM ABSENCEREASON ORDER BY DESCRIPTION'
      ' ')
    Left = 88
    Top = 240
  end
  object DataSourceAbsenceReason: TDataSource
    DataSet = qryAbsenceReason
    Left = 220
    Top = 244
  end
end
