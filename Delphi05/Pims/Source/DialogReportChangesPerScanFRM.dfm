inherited DialogReportChangesPerScanF: TDialogReportChangesPerScanF
  Left = 267
  Caption = 'Report Changes Per Scan'
  ClientHeight = 342
  ClientWidth = 637
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 637
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 341
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 637
    Height = 221
    TabOrder = 3
    object LblToWorkstation: TLabel [0]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblWorkstation: TLabel [1]
      Left = 40
      Top = 42
      Width = 58
      Height = 13
      Caption = 'Workstation'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromWorkstation: TLabel [2]
      Left = 8
      Top = 42
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [38]
      Left = 8
      Top = 66
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [39]
      Left = 40
      Top = 66
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [40]
      Left = 315
      Top = 66
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromPimsUser: TLabel [41]
      Left = 8
      Top = 42
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblPimsUser: TLabel [42]
      Left = 40
      Top = 42
      Width = 46
      Height = 13
      Caption = 'Pims User'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToPimsUser: TLabel [43]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CmbPlusWorkstationTo: TComboBoxPlus [54]
      Left = 342
      Top = 41
      Width = 180
      Height = 19
      ColCount = 158
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusWorkstationToCloseUp
    end
    object CmbPlusWorkstationFrom: TComboBoxPlus [55]
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 157
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 6
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusWorkstationFromCloseUp
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 154
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 155
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 156
      TabOrder = 12
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 157
      TabOrder = 13
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 324
      TabOrder = 30
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 155
      TabOrder = 9
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 156
      TabOrder = 10
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 292
      TabOrder = 11
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      TabOrder = 15
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      TabOrder = 17
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 156
      TabOrder = 16
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 157
      TabOrder = 25
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 32
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 31
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 18
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 522
      Top = 267
      TabOrder = 33
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 155
      TabOrder = 26
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 156
      TabOrder = 23
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 156
      TabOrder = 24
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 157
      TabOrder = 14
    end
    inherited EditWorkspots: TEdit
      TabOrder = 27
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 154
      TabOrder = 28
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 153
      TabOrder = 29
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 20
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 22
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 19
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 21
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 65
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 4
    end
    object DateTo: TDateTimePicker
      Left = 341
      Top = 64
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 5
    end
    object CmbPlusPimsUserFrom: TComboBoxPlus
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 156
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPimsUserFromCloseUp
    end
    object CmbPlusPimsUserTo: TComboBoxPlus
      Left = 342
      Top = 41
      Width = 180
      Height = 19
      ColCount = 157
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPimsUserToCloseUp
    end
    object pnlSelections: TPanel
      Left = 0
      Top = 89
      Width = 637
      Height = 134
      BevelOuter = bvNone
      TabOrder = 8
      object GroupBox1: TGroupBox
        Left = 7
        Top = 5
        Width = 204
        Height = 121
        Caption = 'Selections'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object CheckBoxShowSelection: TCheckBox
          Left = 8
          Top = 16
          Width = 169
          Height = 17
          Caption = 'Show selections'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CheckBoxExport: TCheckBox
          Left = 8
          Top = 40
          Width = 177
          Height = 17
          Caption = 'Export'
          TabOrder = 1
        end
      end
      object RadioGroupSortOn: TRadioGroup
        Left = 217
        Top = 5
        Width = 144
        Height = 121
        Caption = 'Sort On'
        ItemIndex = 0
        Items.Strings = (
          'Date'
          'Employee')
        TabOrder = 1
      end
      object RadioGroupFilterOn: TRadioGroup
        Left = 367
        Top = 5
        Width = 199
        Height = 121
        Caption = 'Filter On'
        ItemIndex = 0
        Items.Strings = (
          'Pims User'
          'Workstation'
          'None')
        TabOrder = 2
        OnClick = RadioGroupFilterOnClick
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 323
    Width = 637
  end
  inherited pnlBottom: TPanel
    Top = 282
    Width = 637
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        44040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507244469616C6F675265706F727442617365462E4461746153
        6F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A650B0E
        6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67106564
        676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E
        6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F43
        616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F4361
        6E496E73657274116564676F43616E4E617669676174696F6E116564676F436F
        6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264731065
        64676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D
        6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265720653
        6F7274656407046373557005576964746802410942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E
        554D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E5368
        6F72744E616D650743617074696F6E060A53686F7274206E616D650557696474
        6802540942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060A53484F52545F4E414D4500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557696474
        6803B4000942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741646472
        65737305576964746802450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506074144445245535300000F5464784442477269
        64436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F
        4465706172746D656E7420636F646505576964746802580942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F444550415254
        4D454E545F434F444500000F546478444247726964436F6C756D6E0A436F6C75
        6D6E5465616D0743617074696F6E06095465616D20636F64650942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507224469616C6F675265706F72
        7442617365462E44617461536F75726365456D706C546F104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802310942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D65055769647468024E0942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07436170
        74696F6E06044E616D6505576964746803CC000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E06074164647265737305576964746802650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        5245535300000F546478444247726964436F6C756D6E0E436F6C756D6E446570
        74436F64650743617074696F6E060F4465706172746D656E7420636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
  end
  object qryPimsUser: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.USER_NAME,'
      '  P.USER_NAME,'
      '  P.USER_NAME DESCRIPTION'
      'FROM'
      '  PIMSUSER P'
      'UNION'
      '  SELECT '
      '    '#39'SYSDBA'#39', '
      '    '#39'SYSDBA'#39','
      '    '#39'SYSDBA'#39
      '  FROM DUAL'
      'ORDER BY'
      '  1')
    Left = 8
    Top = 23
  end
  object qryWorkstation: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  W.COMPUTER_NAME,'
      '  W.COMPUTER_NAME,'
      '  W.COMPUTER_NAME DESCRIPTION'
      'FROM '
      '  WORKSTATION W'
      'ORDER BY '
      '  1')
    Left = 40
    Top = 23
  end
end
