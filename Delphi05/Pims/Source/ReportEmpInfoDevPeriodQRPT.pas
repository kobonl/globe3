(*
  MRA:17-NOV-2015 PIM-23
  - New Report Employee Info and Deviations (per Period).
*)
unit ReportEmpInfoDevPeriodQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls, SystemDMT, ReportEmpInfoDevPeriodDMT, DB, DBTables,
  UScannedIDCard, CalculateTotalHoursDMT, GlobalDMT;

type
  TQRParameters = class
  private
    FDepartmentFrom, FDepartmentTo, FTeamFrom, FTeamTo: String;
    FDateFrom, FDateTo: TDateTime;
    FShowAllDepartment, FShowAllTeam,
    FShowSelection, FExportToFile, FShowDetails: Boolean;
  public
    procedure SetValues(
      DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
      DateFrom, DateTo: TDateTime;
      ShowAllDepartment, ShowAllTeam,
      ShowSelection, ExportToFile, ShowDetails: Boolean
      );
  end;

type
  TTimeArray = array[1..7] of TDateTime;

type
  TTimeblock = class
  private
    TypeCode: String;
    PlantCode: String;
    DepartmentCode: String;
    ShiftNumber: String;
    EmployeeNumber: String;
    StartTime: TTimeArray;
    EndTime: TTimeArray;
  end;

type
  TReportEmpInfoDevPeriodQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLBLToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblFromEmp: TQRLabel;
    QRLblEmp: TQRLabel;
    QRLblEmpFrom: TQRLabel;
    QRLblToEmp: TQRLabel;
    QRLblEmpTo: TQRLabel;
    QRLblFromDept: TQRLabel;
    QRLblDept: TQRLabel;
    QRLblDeptFrom: TQRLabel;
    QRLblToDept: TQRLabel;
    QRLblDeptTo: TQRLabel;
    QRLblFromTeam: TQRLabel;
    QRLblTeam: TQRLabel;
    QRLblTeamFrom: TQRLabel;
    QRLblToTeam: TQRLabel;
    QRLblTeamTo: TQRLabel;
    QRLblFromDate: TQRLabel;
    QRLblDate: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLblToDate: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRGroupHDEmployee: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLblEmpName: TQRLabel;
    QRLblPlantInfo: TQRLabel;
    QRLblEmpNr: TQRLabel;
    QRLblIDCard: TQRLabel;
    QRLblBU: TQRLabel;
    QRLblContractGroup: TQRLabel;
    QRLblTeamInfo: TQRLabel;
    QRLblDepartment: TQRLabel;
    QRLblStartDate: TQRLabel;
    QRLblEndDate: TQRLabel;
    QRLblFirstAid: TQRLabel;
    QRLblFirstAidEndDate: TQRLabel;
    QRLblIsScan: TQRLabel;
    QRLblEffBonus: TQRLabel;
    QRLblCutOffTime: TQRLabel;
    QRShape3: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRBandDetail: TQRBand;
    QRLblDay: TQRLabel;
    QRLblDate1: TQRLabel;
    QRLblIn: TQRLabel;
    QRLblOut: TQRLabel;
    QRLblTotal: TQRLabel;
    QRLblAbsence: TQRLabel;
    QRLblHours: TQRLabel;
    QRBandFTEmployee: TQRBand;
    QRShape4: TQRShape;
    QRLabel25: TQRLabel;
    QRLblNormT: TQRLabel;
    QRLblNorm: TQRLabel;
    QRLblLess: TQRLabel;
    QRLblMore: TQRLabel;
    QRLblLessT: TQRLabel;
    QRLblMoreT: TQRLabel;
    QRLblTotalT: TQRLabel;
    QRLblHoursT: TQRLabel;
    QRMemoAbsCode: TQRMemo;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRShape6: TQRShape;
    QRMemoAbsDesc: TQRMemo;
    QRMemoAbsDays: TQRMemo;
    QRMemoAbsHrs: TQRMemo;
    QRChildBandBalance: TQRChildBand;
    QRLabel30: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape7: TQRShape;
    QRMemoCnt: TQRMemo;
    QRMemoLastYear: TQRMemo;
    QRMemoNorm: TQRMemo;
    QRMemoUsed: TQRMemo;
    QRMemoEarned: TQRMemo;
    QRChildBandDetail: TQRChildBand;
    QRLabel36: TQRLabel;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRLblNormSubTot: TQRLabel;
    QRLblTotalSubTotal: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRShape5: TQRShape;
    QRMemoShift: TQRMemo;
    QRMemoShiftDesc: TQRMemo;
    QRMemoShiftDays: TQRMemo;
    QRMemoShiftHrs: TQRMemo;
    QRLblPlant2: TQRLabel;
    QRMemoPlant: TQRMemo;
    QRChildBandContractInfo: TQRChildBand;
    QRLabel41: TQRLabel;
    QRShape10: TQRShape;
    QRLabel42: TQRLabel;
    QRLblOvertimePer: TQRLabel;
    QRLabel43: TQRLabel;
    QRLblRoundTrunc: TQRLabel;
    QRLabel44: TQRLabel;
    QRMemoODLineNr: TQRMemo;
    QRMemoODHT: TQRMemo;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRMemoODStart: TQRMemo;
    QRMemoODEnd: TQRMemo;
    QRShape11: TQRShape;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRShape12: TQRShape;
    QRMemoEHDay: TQRMemo;
    QRMemoEHStart: TQRMemo;
    QRMemoEHEnd: TQRMemo;
    QRMemoEHHT: TQRMemo;
    QRMemoEHHT2: TQRMemo;
    QRBandSummary: TQRBand;
    QRLblLessSubTot: TQRLabel;
    QRLblMoreSubTot: TQRLabel;
    QRLblHoursSubTot: TQRLabel;
    QRChildBandTimeblockDefs: TQRChildBand;
    QRLabel55: TQRLabel;
    QRShape13: TQRShape;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRShape14: TQRShape;
    QRLblTBDay1: TQRLabel;
    QRLblTBDay2: TQRLabel;
    QRLblTBDay3: TQRLabel;
    QRLblTBDay4: TQRLabel;
    QRLblTBDay5: TQRLabel;
    QRLblTBDay6: TQRLabel;
    QRLblTBDay7: TQRLabel;
    QRMemoTBPlant: TQRMemo;
    QRMemoTBShift: TQRMemo;
    QRMemoTBDept: TQRMemo;
    QRMemoTBEmp: TQRMemo;
    QRMemoTBDay1: TQRMemo;
    QRMemoTBDay2: TQRMemo;
    QRMemoTBDay3: TQRMemo;
    QRMemoTBDay4: TQRMemo;
    QRMemoTBDay5: TQRMemo;
    QRMemoTBDay6: TQRMemo;
    QRMemoTBDay7: TQRMemo;
    QRChildBandBreakDefs: TQRChildBand;
    QRLabel61: TQRLabel;
    QRShape15: TQRShape;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRShape16: TQRShape;
    QRLblBBDay1: TQRLabel;
    QRLblBBDay2: TQRLabel;
    QRLblBBDay3: TQRLabel;
    QRLblBBDay4: TQRLabel;
    QRLblBBDay5: TQRLabel;
    QRLblBBDay6: TQRLabel;
    QRLblBBDay7: TQRLabel;
    QRMemoBBPlant: TQRMemo;
    QRMemoBBShift: TQRMemo;
    QRMemoBBDept: TQRMemo;
    QRMemoBBEmp: TQRMemo;
    QRMemoBBDay1: TQRMemo;
    QRMemoBBDay2: TQRMemo;
    QRMemoBBDay3: TQRMemo;
    QRMemoBBDay4: TQRMemo;
    QRMemoBBDay5: TQRMemo;
    QRMemoBBDay6: TQRMemo;
    QRMemoBBDay7: TQRMemo;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRMemoTBDesc: TQRMemo;
    QRMemoBBDesc: TQRMemo;
    QRLabel56: TQRLabel;
    QRLabel62: TQRLabel;
    QRLblInEarly: TQRLabel;
    QRLabel69: TQRLabel;
    QRLblInLate: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLblOutEarly: TQRLabel;
    QRLabel73: TQRLabel;
    QRLblOutLate: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFTEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandBalanceBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandContractInfoBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandTimeblockDefsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRChildBandBreakDefsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    NormTotalMin, LessTotalMin, MoreTotalMin, TotalMin, HoursTotalMin: Integer;
    BalanceYear: Integer;
    NormSubTotalMin, TotalSubTotalMin, LessSubTotalMin, MoreSubTotalMin,
    HoursSubTotalMin: Integer;
    TimeblockList: TList;
    BreakList: TList;
    procedure InsertPivotDays;
    function TimeblockListAdd(ATypeCode, APlantCode, ADepartmentCode,
      AShiftNumber, AEmployeeNumber: String;
      AStartTime1, AEndTime1, AStartTime2, AEndTime2,
      AStartTime3, AEndTime3, AStartTime4, AEndTime4,
      AStartTime5, AEndTime5, AStartTime6, AEndTime6,
      AStartTime7, AEndTime7: TDateTime): Boolean;
    function BreakListAdd(ATypeCode, APlantCode, ADepartmentCode,
      AShiftNumber, AEmployeeNumber: String;
      AStartTime1, AEndTime1, AStartTime2, AEndTime2,
      AStartTime3, AEndTime3, AStartTime4, AEndTime4,
      AStartTime5, AEndTime5, AStartTime6, AEndTime6,
      AStartTime7, AEndTime7: TDateTime): Boolean;
    procedure TimeblockListClear;
    procedure BreakListClear;
    procedure TimeblockBreakListAddItems;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    function QRSendReportParameters(
      const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
      const DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowAllDepartment, ShowAllTeam: Boolean;
      const ShowSelection, ExportToFile, ShowDetails: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportEmpInfoDevPeriodQR: TReportEmpInfoDevPeriodQR;

implementation

{$R *.DFM}

uses
  UPimsMessageRes,
  UPimsConst,
  ListProcsFRM,
  UGlobalFunctions;

{ TReportEmpInfoDevPeriodQR }

procedure TQRParameters.SetValues(
  DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
  DateFrom, DateTo: TDateTime;
  ShowAllDepartment, ShowAllTeam,
  ShowSelection, ExportToFile, ShowDetails: Boolean);
begin
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowAllDepartment := ShowAllDepartment;
  FShowAllTeam := ShowAllTeam;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  FShowDetails := ShowDetails;
end;

function TReportEmpInfoDevPeriodQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo, DepartmentFrom, DepartmentTo,
  TeamFrom, TeamTo: String; const DateFrom, DateTo: TDateTime;
  const ShowAllDepartment, ShowAllTeam: Boolean;
  const ShowSelection, ExportToFile, ShowDetails: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DepartmentFrom, DepartmentTo, TeamFrom, TeamTo,
      DateFrom, DateTo,
      ShowAllDepartment, ShowAllTeam,
      ShowSelection, ExportToFile, ShowDetails);
  end;
  SetDataSetQueryReport(ReportEmpInfoDevPeriodDM.qryEmpInfo);
end;

function TReportEmpInfoDevPeriodQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  Year, Month, Day: Word;
  ForceShiftDate: Boolean;
begin
//  Result := False;

  InsertPivotDays;

  ForceShiftDate := True;

  with ReportEmpInfoDevPeriodDM do
  begin
    SelectStr :=
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, ' + NL +
      '  PD.DAYDATE, ' + NL +
      '  SUM(PIMSDAYOFWEEK(PD.DAYDATE)) DAYOFWEEK, ' + NL +
      '  SUM(case to_char (PD.DAYDATE, ''FmDay'', ''nls_date_language=english'') ' + NL +
      '    when ''Monday'' then 2 ' + NL +
      '    when ''Tuesday'' then 3 ' + NL +
      '    when ''Wednesday'' then 4 ' + NL +
      '    when ''Thursday'' then 5 ' + NL +
      '    when ''Friday'' then 6 ' + NL +
      '    when ''Saturday'' then 7 ' + NL +
      '    when ''Sunday'' then 1 ' + NL +
      '  end) DAYOFWEEK2, ' + NL +
      '  min(( ' + NL +
      '    select min(t.plant_code) ' + NL +
      '    from prodhourperemployee t ' + NL +
      '    where t.employee_number = E.employee_number and ' + NL +
      '    t.prodhouremployee_date = PD.DAYDATE)) PLANT_CODE, ' + NL +
      '  min(( ' + NL +
      '    select min(t.shift_number) ' + NL +
      '    from prodhourperemployee t ' + NL +
      '    where t.employee_number = E.employee_number and ' + NL +
      '    t.prodhouremployee_date = PD.DAYDATE)) SHIFT_NUMBER, ' + NL +
      '  min(( ' + NL +
      '    select min(t.datetime_in) ' + NL +
      '    from timeregscanning t ' + NL +
      '    where t.processed_yn = ''Y'' and t.employee_number = E.employee_number and ' + NL;
    if SystemDM.UseShiftDateSystem or ForceShiftDate then
      SelectStr := SelectStr +
        '    t.shift_date = PD.DAYDATE)) DATEIN, ' + NL
    else
      SelectStr := SelectStr +
        '    trunc(t.datetime_in) = PD.DAYDATE)) DATEIN, ' + NL;
    SelectStr := SelectStr +
      '  max(( ' + NL +
      '    select max(t.datetime_out) ' + NL +
      '    from timeregscanning t ' + NL +
      '    where t.processed_yn = ''Y'' and t.employee_number = E.employee_number and ' + NL;
    if SystemDM.UseShiftDateSystem or ForceShiftDate then
      SelectStr := SelectStr +
        '    t.shift_date = PD.DAYDATE)) DATEOUT, ' + NL
    else
      SelectStr := SelectStr +
        '    trunc(t.datetime_out) = PD.DAYDATE)) DATEOUT, ' + NL;
    SelectStr := SelectStr +
      '  sum(( ' + NL +
      '    select sum(s.salary_minute) ' + NL +
      '    from salaryhourperemployee s ' + NL +
      '    where s.employee_number = E.employee_number and ' + NL +
      '    s.salary_date = PD.DAYDATE)) SALMIN, ' + NL +
      '  min(( ' + NL +
      '    select min(ar.absencereason_code) ' + NL +
      '    from absencehourperemployee ae inner join absencereason ar on ' + NL +
      '      ae.absencereason_id = ar.absencereason_id ' + NL +
      '     where ae.employee_number = E.employee_number and ' + NL +
      '    ae.absencehour_date = PD.DAYDATE)) ABSCODE, ' + NL +
      '  sum(( ' + NL +
      '    select sum(a.absence_minute) ' + NL +
      '    from absencehourperemployee a ' + NL +
      '    where a.employee_number = E.employee_number and ' + NL +
      '    a.absencehour_date = PD.DAYDATE)) ABSMIN ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, ' + NL +
      '  PIVOTDAY PD ' + NL +
      'WHERE PD.DAYDATE >= TO_DATE(:DATEFROM) ' + NL +
      '  AND PD.DAYDATE <= TO_DATE(:DATETO) ' + NL +
      '  AND E.PLANT_CODE >= ' + '''' + QRBaseParameters.FPlantFrom + '''' + NL +
      '  AND E.PLANT_CODE <= ' + '''' + QRBaseParameters.FPlantTo + '''' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      if not QRParameters.FShowAllDepartment then
        SelectStr := SelectStr +
          '  AND E.DEPARTMENT_CODE >= ' + '''' + QRParameters.FDepartmentFrom + '''' + NL +
          '  AND E.DEPARTMENT_CODE <= ' + '''' + QRParameters.FDepartmentTo + '''' + NL;
      if not QRParameters.FShowAllTeam then
        SelectStr := SelectStr +
          '  AND E.TEAM_CODE >= ' + '''' + QRParameters.FTeamFrom + '''' + NL +
          '  AND E.TEAM_CODE <= ' + '''' + QRParameters.FTeamTo + '''' + NL;
      SelectStr := SelectStr +
        '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    end;
    SelectStr := SelectStr +
      'GROUP BY ' + NL +
      '  E.EMPLOYEE_NUMBER, PD.DAYDATE ' + NL +
      'ORDER BY ' + NL +
      '  1, 2';
    qryEmpInfo.Close;
    qryEmpInfo.SQL.Clear;
    qryEmpInfo.SQL.Add(SelectStr);
// qryEmpInfo.SQL.SaveToFile('c:\temp\ReportEmpInfoDevPeriod.sql');
    qryEmpInfo.ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
    qryEmpInfo.ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
    qryEmpInfo.Open;
    Result := not qryEmpInfo.Eof;
  end;
  if Result then
  begin
    DecodeDate(QRParameters.FDateTo, Year, Month, Day);
    BalanceYear := Year;
  end;
end; // ExistsRecords

procedure TReportEmpInfoDevPeriodQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblDeptFrom.Caption := '*';
  QRLblDeptTo.Caption := '*';
  QRLblTeamFrom.Caption := '*';
  QRLblTeamTo.Caption := '*';
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);;
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);;
  QRLblEmpFrom.Caption := '*';
  QRLblEmpTo.Caption := '*';
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    QRLblEmpFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmpTo.Caption := QRBaseParameters.FEmployeeTo;
    if not QRParameters.FShowAllDepartment then
    begin
      QRLblDeptFrom.Caption := QRParameters.FDepartmentFrom;
      QRLblDeptTo.Caption := QRParameters.FDepartmentTo;
    end;
  end;
  if not QRParameters.FShowAllTeam then
  begin
    QRLblTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLblTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLblTBDay1.Caption := SystemDM.GetDayWCode(1);
  QRLblTBDay2.Caption := SystemDM.GetDayWCode(2);
  QRLblTBDay3.Caption := SystemDM.GetDayWCode(3);
  QRLblTBDay4.Caption := SystemDM.GetDayWCode(4);
  QRLblTBDay5.Caption := SystemDM.GetDayWCode(5);
  QRLblTBDay6.Caption := SystemDM.GetDayWCode(6);
  QRLblTBDay7.Caption := SystemDM.GetDayWCode(7);
  QRLblBBDay1.Caption := SystemDM.GetDayWCode(1);
  QRLblBBDay2.Caption := SystemDM.GetDayWCode(2);
  QRLblBBDay3.Caption := SystemDM.GetDayWCode(3);
  QRLblBBDay4.Caption := SystemDM.GetDayWCode(4);
  QRLblBBDay5.Caption := SystemDM.GetDayWCode(5);
  QRLblBBDay6.Caption := SystemDM.GetDayWCode(6);
  QRLblBBDay7.Caption := SystemDM.GetDayWCode(7);
end;

procedure TReportEmpInfoDevPeriodQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
  TimeblockList.Free;
  BreakList.Free;
end;

procedure TReportEmpInfoDevPeriodQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption;
  TimeblockList := TList.Create;
  BreakList := TList.Create;
end;

procedure TReportEmpInfoDevPeriodQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  Printband := QRParameters.FShowSelection;
  // MR:17-09-2003
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      ExportClass.AddText(QRLblFromDept.Caption + ' ' +
        QRLblDept.Caption + QRLblDeptFrom.Caption + ' ' +
        QRLblToDept.Caption + QRLblDeptTo.Caption);
      ExportClass.AddText(QRLblFromTeam.Caption + ' ' +
        QRLblTeam.Caption + QRLblTeamFrom.Caption + ' ' +
        QRLblToTeam.Caption + QRLblTeamTo.Caption);
      ExportClass.AddText(QRLblFromEmp.Caption + ' ' +
        QRLblEmp.Caption + QRLblEmpFrom.Caption + ' ' +
        QRLblToEmp.Caption + QRLblEmpTo.Caption);
      ExportClass.AddText(QRLblFromDate.Caption + ' ' +
        QRLblDate.Caption + ' ' +
        QRLblDateFrom.Caption + ' ' +
        QRLblToDate.Caption + ' ' +
        QRLblDateTo.Caption);
    end;
  end;
end;

procedure TReportEmpInfoDevPeriodQR.InsertPivotDays;
var
  DayLoop: TDateTime;
  procedure MergePivotDay;
  begin
    with ReportEmpInfoDevPeriodDM do
    begin
      try
        qryPivotDayMerge.ParamByName('DAYDATE').AsDateTime := DayLoop;
        qryPivotDayMerge.ExecSQL;
      except
        on E:EDBEngineError do
          if not (E.Errors[0].ErrorCode = PIMS_DBIERR_KEYVIOL) then // duplicate value
            WErrorLog(E.Message);
        on E:EDatabaseError do
          WErrorLog(E.Message);
      end;
    end;
  end;
begin
  DayLoop := QRParameters.FDateFrom;
  while DayLoop <= QRParameters.FDateTo do
  begin
    MergePivotDay;
    DayLoop := DayLoop + 1;
  end;
end;

procedure TReportEmpInfoDevPeriodQR.QRGroupHDEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  NormTotalMin := 0;
  LessTotalMin := 0;
  MoreTotalMin := 0;
  TotalMin := 0;
  HoursTotalMin := 0;
  NormSubTotalMin := 0;
  LessSubTotalMin := 0;
  MoreSubTotalMin := 0;
  TotalSubTotalMin := 0;
  HoursSubTotalMin := 0;
  TimeblockListClear;
  BreakListClear;
  TimeblockBreakListAddItems;
  with ReportEmpInfoDevPeriodDM do
  begin
    with qryEmpHeader do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Open;
      if not Eof then
      begin
        QRLblEmpName.Caption := FieldByName('EDESCRIPTION').AsString;
        QRLblPlantInfo.Caption := FieldByName('PLANT_CODE').AsString +
          '-' + FieldByName('PDESCRIPTION').AsString;
        QRLblEmpNr.Caption := FieldByName('EMPLOYEE_NUMBER').AsString;
        QRLblIDCard.Caption := FieldByName('IDCARD_NUMBER').AsString;
        QRLblBU.Caption := FieldByName('BUSINESSUNIT_CODE').AsString +
          '-' + FieldByName('BUDESCRIPTION').AsString;
        QRLblContractGroup.Caption := FieldByName('CONTRACTGROUP_CODE').AsString +
          '-' + FieldByName('CGDESCRIPTION').AsString;
        QRLblTeamInfo.Caption := FieldByName('TEAM_CODE').AsString +
          '-' + FieldByName('TDESCRIPTION').AsString;
        QRLblDepartment.Caption := FieldByName('DEPARTMENT_CODE').AsString +
          '-' + FieldByName('DDESCRIPTION').AsString;
        QRLblStartDate.Caption := DateToStr(FieldByName('STARTDATE').AsDateTime);
        if FieldByName('ENDDATE').AsString <> '' then
          QRLblEndDate.Caption := DateToStr(FieldByName('ENDDATE').AsDateTime)
        else
          QRLblEndDate.Caption := '';
        if FieldByName('FIRSTAID_YN').AsString = 'Y' then
          QRLblFirstAid.Caption := SPimsReportYes
        else
          QRLblFirstAid.Caption := SPimsReportNo;
        if FieldByName('FIRSTAID_EXP_DATE').AsString <> '' then
          QRLblFirstAidEndDate.Caption :=
            DateToStr(FieldByName('FIRSTAID_EXP_DATE').AsDateTime)
        else
          QRLblFirstAidEndDate.Caption := '';
        if FieldByName('IS_SCANNING_YN').AsString = 'Y' then
          QRLblIsScan.Caption := SPimsReportYes
        else
          QRLblIsScan.Caption := SPimsReportNo;
        if FieldByName('CALC_BONUS_YN').AsString = 'Y' then
          QRLblEffBonus.Caption := SPimsReportYes
        else
          QRLblEffBonus.Caption := SPimsReportNo;
        if FieldByName('CUT_OF_TIME_YN').AsString = 'Y' then
          QRLblCutOffTime.Caption := SPimsReportYes
        else
          QRLblCutOffTime.Caption := SPimsReportNo;
        QRLblInEarly.Caption := FieldByName('INSCAN_MARGIN_EARLY').AsString;
        QRLblInLate.Caption := FieldByName('INSCAN_MARGIN_LATE').AsString;
        QRLblOutEarly.Caption := FieldByName('OUTSCAN_MARGIN_EARLY').AsString;
        QRLblOutLate.Caption := FieldByName('OUTSCAN_MARGIN_LATE').AsString;
      end;
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLblEmpName.Caption + ExportClass.Sep +
      QRLabel1.Caption + QRLblPlantInfo.Caption + ExportClass.Sep +
      QRLabel2.Caption + QRLblEmpNr.Caption + ExportClass.Sep +
      QRLabel3.Caption + QRLblIDCard.Caption
      );
    ExportClass.AddText(
      QRLabel4.Caption + QRLblBU.Caption + ExportClass.Sep +
      QRLabel5.Caption + QRLblContractGroup.Caption + ExportClass.Sep +
      QRLabel6.Caption + QRLblTeamInfo.Caption + ExportClass.Sep +
      QRLabel7.Caption + QRLblDepartment.Caption
      );
    ExportClass.AddText(
      QRLabel8.Caption + QRLblStartDate.Caption + ExportClass.Sep +
      QRLabel9.Caption + QRLblEndDate.Caption + ExportClass.Sep +
      QRLabel10.Caption + QRLblFirstAid.Caption + ExportClass.Sep +
      QRLabel12.Caption +  QRLblFirstAidEndDate.Caption
      );
    ExportClass.AddText(
      QRLabel13.Caption + QRLblIsScan.Caption + ExportClass.Sep +
      QRLabel14.Caption + QRLblEffBonus.Caption + ExportClass.Sep +
      QRLabel15.Caption + QRLblCutOffTime.Caption
      );
    ExportClass.AddText(
      QRLabel56.Caption +
      QRLabel62.Caption + QRLblInEarly.Caption + ExportClass.Sep +
      QRLabel69.Caption + QRLblInLate.Caption + ExportClass.Sep +
      QRLabel70.Caption +
      QRLabel71.Caption + QRLblOutEarly.Caption + ExportClass.Sep +
      QRLabel73.Caption + QRLblOutLate.Caption
      );
    ExportClass.AddText(
      QRLabel16.Caption + ExportClass.Sep +
      QRLabel17.Caption + ExportClass.Sep +
      QRLabel18.Caption + ExportClass.Sep +
      QRLabel19.Caption + ExportClass.Sep +
      QRLabel20.Caption + ExportClass.Sep +
      QRLabel21.Caption + ExportClass.Sep +
      QRLabel22.Caption + ExportClass.Sep +
      QRLabel23.Caption + ExportClass.Sep +
      QRLabel24.Caption
      );
  end;
end; // QRGroupHDEmployeeBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  IDCard: TScannedIDCard;
  StartDate, EndDate, TimeDiff: TDateTime;
  SalMin, NormMin, LessMin, MoreMin, AbsMin: Integer;
  Hour, Min, Sec, MSec: Word;
  ProdMin, BreaksMin, PayedBreaks: Integer;
  TotalBreaksMin: Integer;
  // 20012142.10 Round (break-) minutes to 15 minutes.
  function RoundMinutes(AMinutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := AMinutes DIV 60;
    Mins := AMinutes MOD 60;
    Mins := Round(Mins / 15) * 15;
    Result := Hours * 60 + Mins;
  end;
begin
  inherited;
  SalMin := 0;
  NormMin := 0;
  LessMin := 0;
  MoreMin := 0;
  AbsMin := 0;
  with ReportEmpInfoDevPeriodDM do
  begin
    if SystemDM.ClientDataSetDay.FindKey([qryEmpInfo.FieldByName('DAYOFWEEK2').AsInteger]) then
      QRLblDay.Caption :=  SystemDM.ClientDataSetDay.FieldByName('DAY_CODE').AsString
    else
      QRLblDay.Caption := '';
    QRLblDate1.Caption := DateToStr(qryEmpInfo.FieldByName('DAYDATE').AsDateTime);
    if qryEmpInfo.FieldByName('DATEIN').AsString <> '' then
      QRLblIn.Caption := TimeToStr(qryEmpInfo.FieldByName('DATEIN').AsDateTime)
    else
      QRLblIn.Caption := '';
    if qryEmpInfo.FieldByName('DATEOUT').AsString <> '' then
      QRLblOut.Caption := TimeToStr(qryEmpInfo.FieldByName('DATEOUT').AsDateTime)
    else
      QRLblOut.Caption := '';
    if qryEmpInfo.FieldByName('SALMIN').AsString <> '' then
    begin
      QRLblTotal.Caption :=
        IntMin2StringTime(qryEmpInfo.FieldByName('SALMIN').AsInteger);
      SalMin := qryEmpInfo.FieldByName('SALMIN').AsInteger;
      TotalMin := TotalMin + SalMin;
    end
    else
    begin
      QRLblTotal.Caption := '';
    end;
    QRLblAbsence.Caption := qryEmpInfo.FieldByName('ABSCODE').AsString;
    if qryEmpInfo.FieldByName('ABSMIN').AsString <> '' then
    begin
      QRLblHours.Caption :=
        IntMin2StringTime(qryEmpInfo.FieldByName('ABSMIN').AsInteger);
      AbsMin := qryEmpInfo.FieldByName('ABSMIN').AsInteger;
      HoursTotalMin := HoursTotalMin + AbsMin;
    end
    else
      QRLblHours.Caption := '';

    // Calculate the Norm
    QRLblNorm.Caption := '';
    if (QRLblIn.Caption <> '') and (QRLblOut.Caption <> '') then
    begin
      IDCard.EmployeeCode := qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      IDCard.ShiftNumber := qryEmpInfo.FieldByName('SHIFT_NUMBER').AsInteger;
      IDCard.PlantCode := qryEmpInfo.FieldByName('PLANT_CODE').AsString;
      StartDate := qryEmpInfo.FieldByName('DATEIN').AsDateTime;
      EndDate := qryEmpInfo.FieldByName('DATEOUT').AsDateTime;
      IDCard.DateIn := StartDate;
      IDCard.DateOut := EndDate;
      AProdMinClass.GetShiftDay(IDCard, StartDate, EndDate);
      TimeDiff := EndDate - StartDate;
      // Subtract the breaks!
      TotalBreaksMin := 0;
      AProdMinClass.ComputeBreaks(IDCard,
        IDCard.DateIn,
        IDCard.DateOut, 0,
        ProdMin, BreaksMin, PayedBreaks);
      TotalBreaksMin := TotalBreaksMin + BreaksMin - PayedBreaks;
      DecodeTime(TimeDiff, Hour, Min, Sec, MSec);
      NormMin := Hour * 60 + Min;
      if (NormMin > TotalBreaksMin) then
        NormMin := NormMin - RoundMinutes(TotalBreaksMin);
      QRLblNorm.Caption := IntMin2StringTime(NormMin);
      NormTotalMin := NormTotalMin + NormMin;
    end;
    QRLblLess.Caption := '';
    QRLblMore.Caption := '';
    if (SalMin <> 0) and (NormMin <> 0) then
    begin
      if (SalMin < NormMin) then
      begin
        LessMin := SalMin - NormMin;
        QRLblLess.Caption := IntMin2StringTime(LessMin);
        LessTotalMin := LessTotalMin + LessMin;
      end;
      if (SalMin > NormMin) then
      begin
        MoreMin := Salmin - NormMin;
        QRLblMore.Caption := IntMin2StringTime(MoreMin);
        MoreTotalMin := MoreTotalMin + MoreMin;
      end;
    end;
    NormSubTotalMin := NormSubTotalMin + NormMin;
    LessSubTotalMin := LessSubTotalMin + LessMin;
    MoreSubTotalMin := MoreSubTotalMin + MoreMin;
    TotalSubTotalMin := TotalSubTotalMin + SalMin;
    HoursSubTotalMin := HoursSubTotalMin + AbsMin;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLblDay.Caption + ' ' + QRLblDate1.Caption + ExportClass.Sep +
      QRLblIn.Caption + ExportClass.Sep +
      QRLblOut.Caption + ExportClass.Sep +
      QRLblNorm.Caption + ExportClass.Sep +
      QRLblLess.Caption + ExportClass.Sep +
      QRLblMore.Caption + ExportClass.Sep +
      QRLblTotal.Caption + ExportClass.Sep +
      QRLblAbsence.Caption + ExportClass.Sep +
      QRLblHours.Caption
    );
  end;
end; // QRBandDetailBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRChildBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  with ReportEmpInfoDevPeriodDM do
  begin
    PrintBand := (qryEmpInfo.FieldByName('DAYOFWEEK').AsInteger = 7) or
      (qryEmpInfo.FieldByName('DAYDATE').AsDateTime = QRParameters.FDateTo);
    QRLblNormSubTot.Caption := IntMin2StringTime(NormSubTotalMin);
    QRLblLessSubTot.Caption := IntMin2StringTime(LessSubTotalMin);
    QRLblMoreSubTot.Caption := IntMin2StringTime(MoreSubTotalMin);
    QRLblTotalSubTotal.Caption := IntMin2StringTime(TotalSubTotalMin);
    QRLblHoursSubTot.Caption := IntMin2StringTime(HoursSubTotalMin);
    if PrintBand then
    begin
      NormSubTotalMin := 0;
      LessSubTotalMin := 0;
      MoreSubTotalMin := 0;
      TotalSubTotalMin := 0;
      HoursSubTotalMin := 0;
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLabel36.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLblNormSubTot.Caption + ExportClass.Sep +
      QRLblLessSubTot.Caption + ExportClass.Sep +
      QRLblMoreSubTot.Caption + ExportClass.Sep +
      QRLblTotalSubTotal.Caption + ExportClass.Sep +
      QRLblHoursSubTot.Caption
      );
  end;
end; // QRChildBandDetailBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRBandFTEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalAbsDays, TotalAbsMin: Integer;
  TotalShiftDays, TotalShiftMin: Integer;
  I: Integer;
begin
  inherited;
  // Totals
  QRLblNormT.Caption := IntMin2StringTime(NormTotalMin);
  QRLblLessT.Caption := IntMin2StringTime(LessTotalMin);
  QRLblMoreT.Caption := IntMin2StringTime(MoreTotalMin);
  QRLblTotalT.Caption := IntMin2StringTime(TotalMin);
  QRLblHoursT.Caption := IntMin2StringTime(HoursTotalMin);
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLabel25.Caption + ExportClass.Sep + ExportClass.Sep +
      QRLblNormT.Caption + ExportClass.Sep +
      QRLblLessT.Caption + ExportClass.Sep +
      QRLblMoreT.Caption + ExportClass.Sep +
      QRLblTotalT.Caption + ExportClass.Sep +
      QRLblHoursT.Caption
    );
  end;
  // Shift info
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLblPlant2.Caption + ExportClass.Sep +
      QRLabel37.Caption + ExportClass.Sep +
      QRLabel38.Caption + ExportClass.Sep +
      QRLabel39.Caption + ExportClass.Sep +
      QRLabel40.Caption
      );
  end;
  TotalShiftDays := 0;
  TotalShiftMin := 0;
  QRMemoPlant.Lines.Clear;
  QRMemoShift.Lines.Clear;
  QRMemoShiftDesc.Lines.Clear;
  QRMemoShiftDays.Lines.Clear;
  QRMemoShiftHrs.Lines.Clear;
  with ReportEmpInfoDevPeriodDM do
  begin
    with qryEmpShift do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      Open;
      while not Eof do
      begin
        QRMemoPlant.Lines.Add(FieldByName('PLANT_CODE').AsString);
        QRMemoShift.Lines.Add(FieldByName('SHIFT_NUMBER').AsString);
        QRMemoShiftDesc.Lines.Add(FieldByName('DESCRIPTION').AsString);
        QRMemoShiftDays.Lines.Add(FieldByName('DAYS').AsString);
        TotalShiftDays := TotalShiftDays + FieldByName('DAYS').AsInteger;
        QRMemoShiftHrs.Lines.Add(IntMin2StringTime(FieldByName('SALMIN').AsInteger));
        TotalShiftMin := TotalShiftMin + FieldByName('SALMIN').AsInteger;
        Next;
      end;
    end;
    if QRMemoPlant.Lines.Count > 0 then
    begin
      QRMemoPlant.Lines.Add(SPimsTotals);
      QRMemoShift.Lines.Add('');
      QRMemoShiftDesc.Lines.Add('');
      QRMemoShiftDays.Lines.Add(IntToStr(TotalShiftDays));
      QRMemoShiftHrs.Lines.Add(IntMin2StringTime(TotalShiftMin));
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    for I := 0 to QRMemoPlant.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoPlant.Lines.Strings[I] + ExportClass.Sep +
        QRMemoShift.Lines.Strings[I] + ExportClass.Sep +
        QRMemoShiftDesc.Lines.Strings[I] + ExportClass.Sep +
        QRMemoShiftDays.Lines.Strings[I] + ExportClass.Sep +
        QRMemoShiftHrs.Lines.Strings[I]
      );
    end;
  end;
  // Absence reasons
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(
      QRLabel26.Caption + ExportClass.Sep +
      QRLabel27.Caption + ExportClass.Sep +
      QRLabel28.Caption + ExportClass.Sep +
      QRLabel29.Caption
      );
  end;
  TotalAbsDays := 0;
  TotalAbsMin := 0;
  QRMemoAbsCode.Lines.Clear;
  QRMemoAbsDesc.Lines.Clear;
  QRMemoAbsDays.Lines.Clear;
  QRMemoAbsHrs.Lines.Clear;
  with ReportEmpInfoDevPeriodDM do
  begin
    with qryEmpAbsence do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      Open;
      while not Eof do
      begin
        QRMemoAbsCode.Lines.Add(FieldByName('ABSENCEREASON_CODE').AsString);
        QRMemoAbsDesc.Lines.Add(FieldByName('DESCRIPTION').AsString);
        QRMemoAbsDays.Lines.Add(FieldByName('ABSDAYS').AsString);
        TotalAbsDays := TotalAbsDays + FieldByName('ABSDAYS').AsInteger;
        QRMemoAbsHrs.Lines.Add(IntMin2StringTime(FieldByName('ABSMIN').AsInteger));
        TotalAbsMin := TotalAbsMin + FieldByName('ABSMIN').AsInteger;
        Next;
      end;
    end;
    if QRMemoAbsCode.Lines.Count > 0 then
    begin
      QRMemoAbsCode.Lines.Add(SPimsTotals);
      QRMemoAbsDesc.Lines.Add('');
      QRMemoAbsDays.Lines.Add(IntToStr(TotalAbsDays));
      QRMemoAbsHrs.Lines.Add(IntMin2StringTime(TotalAbsMin));
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    for I := 0 to QRMemoAbsCode.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoAbsCode.Lines.Strings[I] + ExportClass.Sep +
        QRMemoAbsDesc.Lines.Strings[I] + ExportClass.Sep +
        QRMemoAbsDays.Lines.Strings[I] + ExportClass.Sep +
        QRMemoAbsHrs.Lines.Strings[I]
      );
    end;
  end;
end; // QRBandFTEmployeeBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRChildBandBalanceBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  ABSDescription: String;
  I: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(QRLabel30.Caption);
    ExportClass.AddText(
      QRLabel31.Caption + ExportClass.Sep +
      QRLabel33.Caption + ExportClass.Sep +
      QRLabel32.Caption + ExportClass.Sep +
      QRLabel34.Caption + ExportClass.Sep +
      QRLabel35.Caption
      );
  end;
  QRMemoCnt.Lines.Clear;
  QRMemoLastYear.Lines.Clear;
  QRMemoNorm.Lines.Clear;
  QRMemoUsed.Lines.Clear;
  QRMemoEarned.Lines.Clear;
  with ReportEmpInfoDevPeriodDM do
  begin
    with qryEmpBalance do
    begin
      Close;
      ParamByName('COUNTRY_ID').AsInteger := qryEmpHeader.FieldByName('COUNTRY_ID').AsInteger;
      ParamByName('YEAR').AsInteger := BalanceYear;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Open;
      while not Eof do
      begin
        ABSDescription := FieldByName('ABSTYPE').AsString;
        if FieldByName('ABSDESCRIPTION1').AsString <> '' then
          ABSDescription := FieldByName('ABSDESCRIPTION1').AsString
        else
          if FieldByName('ABSDESCRIPTION2').AsString <> '' then
            ABSDescription := FieldByName('ABSDESCRIPTION2').AsString;
        QRMemoCnt.Lines.Add(ABSDescription);
        QRMemoLastYear.Lines.Add(IntMin2StringTime(FieldByName('LAST_YEAR').AsInteger));
        QRMemoNorm.Lines.Add(IntMin2StringTime(FieldByName('NORM').AsInteger));
        QRMemoUsed.Lines.Add(IntMin2StringTime(FieldByName('USED').AsInteger));
        QRMemoEarned.Lines.Add(IntMin2StringTime(FieldByName('EARNED').AsInteger));
        Next;
      end;
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    for I := 0 to QRMemoCnt.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoCnt.Lines.Strings[I] + ExportClass.Sep +
        QRMemoLastYear.Lines.Strings[I] + ExportClass.Sep +
        QRMemoNorm.Lines.Strings[I] + ExportClass.Sep +
        QRMemoUsed.Lines.Strings[I] + ExportClass.Sep +
        QRMemoEarned.Lines.Strings[I]
      );
    end;
  end;
end; // QRChildBandBalanceBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRChildBandContractInfoBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  WorkingDays: String;
  ContractGroupCode: String;
  I: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportClass.AddText(QRLabel41.Caption);
  end;
  with ReportEmpInfoDevPeriodDM do
  begin
    ContractGroupCode := '';
    with qryContractInfo do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Open;
      if not Eof then
      begin
        ContractGroupCode := FieldByName('CONTRACTGROUP_CODE').AsString;
        case FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger-1 of
        0: QRLblOvertimePer.Caption := SPimsOTDay;
        1: QRLblOvertimePer.Caption := SPimsOTWeek;
        3:
          begin
            WorkingDays := '';
            if FieldByName('WORKDAY_MO_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsMO + ' ';
            if FieldByName('WORKDAY_TU_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsTU + ' ';
            if FieldByName('WORKDAY_WE_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsWE + ' ';
            if FieldByName('WORKDAY_TH_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsTH + ' ';
            if FieldByName('WORKDAY_FR_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsFR + ' ';
            if FieldByName('WORKDAY_SA_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsSA + ' ';
            if FieldByName('WORKDAY_SU_YN').AsString = 'Y' then
              WorkingDays := WorkingDays + SPimsSU + ' ';
            QRLblOvertimePer.Caption :=
              SPimsOTMonth + ' ' + SPimsOTWorkingDays + ' ' + WorkingDays +
              SPimsNormalHrs + ' ' +
              IntMin2StringTime(FieldByName('NORMALHOURSPERDAY').AsInteger);
          end;
        2:
          begin
            QRLblOvertimePer.Caption :=
              SPimsOTPeriod + ' ' + SPimsOTPeriodStarts + ' ' +
              FieldByName('PERIOD_STARTS_IN_WEEK').AsString + ' ' +
              SPimsOTWeeksInPeriod + ' ' +
              FieldByName('WEEKS_IN_PERIOD').AsString;
          end;
        end; // case
        case FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger of
        0: QRLblRoundTrunc.Caption :=
          SPimsCGRound + ' ' + SPimsCGTo + ' ' +
          FieldByName('ROUND_MINUTE').AsString + ' ' + SPimsCGMinute;
        1: QRLblRoundTrunc.Caption :=
          SPimsCGTrunc + ' ' + SPimsCGTo + ' ' +
          FieldByName('ROUND_MINUTE').AsString + ' ' + SPimsCGMinute;
        end; // case
      end;
    end; // with
    if QRParameters.FExportToFile and PrintBand then
    begin
      ExportClass.AddText(QRLabel42.Caption + ' ' + QRLblOvertimePer.Caption);
      ExportClass.AddText(QRLabel43.Caption + ' ' + QRLblRoundTrunc.Caption);
      ExportClass.AddText(QRLabel44.Caption);
    end;
    // Overtime Definitions
    if QRParameters.FExportToFile and PrintBand then
    begin
      ExportClass.AddText(QRLabel44.Caption);
      ExportClass.AddText(
        QRLabel45.Caption + ExportClass.Sep +
        QRLabel46.Caption + ExportClass.Sep +
        QRLabel47.Caption + ExportClass.Sep +
        QRLabel48.Caption
        );
    end;
    QRMemoODLineNr.Lines.Clear;
    QRMemoODHT.Lines.Clear;
    QRMemoODStart.Lines.Clear;
    QRMemoODEnd.Lines.Clear;
    with qryOvertimeDef do
    begin
      Close;
      ParamByName('CONTRACTGROUP_CODE').AsString := ContractGroupCode;
      Open;
      while not Eof do
      begin
        QRMemoODLineNr.Lines.Add(FieldByName('LINE_NUMBER').AsString);
        QRMemoODHT.Lines.Add(FieldByName('HOURTYPE_NUMBER').AsString +
          '-' + FieldByName('DESCRIPTION').AsString);
        QRMemoODStart.Lines.Add(
          IntMin2StringTime(FieldByName('STARTTIME').AsInteger, True));
        QRMemoODEnd.Lines.Add(
          IntMin2StringTime(FieldByName('ENDTIME').AsInteger, True));
        Next;
      end;
    end;
    if QRParameters.FExportToFile and PrintBand then
    begin
      for I := 0 to QRMemoODLineNr.Lines.Count - 1 do
      begin
        ExportClass.AddText(
          QRMemoODLineNr.Lines.Strings[I] + ExportClass.Sep +
          QRMemoODHT.Lines.Strings[I] + ExportClass.Sep +
          QRMemoODStart.Lines.Strings[I] + ExportClass.Sep +
          QRMemoODEnd.Lines.Strings[I]
          );
      end;
    end;
    // Exceptional Hour definitions
    if QRParameters.FExportToFile and PrintBand then
    begin
      ExportClass.AddText(QRLabel49.Caption);
      ExportClass.AddText(
        QRLabel50.Caption + ExportClass.Sep +
        QRLabel51.Caption + ExportClass.Sep +
        QRLabel52.Caption + ExportClass.Sep +
        QRLabel53.Caption + ExportClass.Sep +
        QRLabel54.Caption
        );
    end;
    QRMemoEHDay.Lines.Clear;
    QRMemoEHStart.Lines.Clear;
    QRMemoEHEnd.Lines.Clear;
    QRMemoEHHT.Lines.Clear;
    QRMemoEHHT2.Lines.Clear;
    with qryExceptHourDef do
    begin
      Close;
      ParamByName('CONTRACTGROUP_CODE').AsString := ContractGroupCode;
      Open;
      while not Eof do
      begin
        QRMemoEHDay.Lines.Add(SystemDM.GetDayWCode(FieldByName('DAY_OF_WEEK').AsInteger));
        QRMemoEHStart.Lines.Add(TimeToStr(FieldByName('STARTTIME').AsDateTime));
        QRMemoEHEnd.Lines.Add(TimeToStr(FieldByName('ENDTIME').AsDateTime));
        QRMemoEHHT.Lines.Add(FieldByName('HOURTYPE_NUMBER').AsString +
          '-' + FieldByName('DESCRIPTION').AsString);
        if FieldByName('OVERTIME_HOURTYPE_NUMBER').AsString <> '' then
          QRMemoEHHT2.Lines.Add(FieldByName('OVERTIME_HOURTYPE_NUMBER').AsString +
            '-'  + FieldByName('OVERTIME_HOURTYPE_DESCRIPTION').AsString)
        else
          QRMemoEHHT2.Lines.Add('');
        Next;
      end;
    end;
  end;
  if QRParameters.FExportToFile and PrintBand then
  begin
    for I := 0 to QRMemoEHDay.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoEHDay.Lines.Strings[I] + ExportClass.Sep +
        QRMemoEHStart.Lines.Strings[I] + ExportClass.Sep +
        QRMemoEHEnd.Lines.Strings[I] + ExportClass.Sep +
        QRMemoEHHT.Lines.Strings[I] + ExportClass.Sep +
        QRMemoEHHT2.Lines.Strings[I]
        );
    end;
  end;
end; // QRChildBandContractInfoBeforePrint

procedure TReportEmpInfoDevPeriodQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end; // QRBandSummaryBeforePrint

function TReportEmpInfoDevPeriodQR.TimeblockListAdd(ATypeCode, APlantCode,
  ADepartmentCode, AShiftNumber, AEmployeeNumber: String;
  AStartTime1, AEndTime1, AStartTime2, AEndTime2, AStartTime3, AEndTime3,
  AStartTime4, AEndTime4, AStartTime5, AEndTime5, AStartTime6, AEndTime6,
  AStartTime7, AEndTime7: TDateTime): Boolean;
var
  ATimeblock: TTimeblock;
  function TBFind: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to TimeblockList.Count - 1 do
    begin
      ATimeblock := TimeblockList.Items[I];
      if (ATimeblock.TypeCode = ATypeCode) and
        (ATimeblock.PlantCode = APlantCode) and
        (ATimeblock.DepartmentCode = ADepartmentCode) and
        (ATimeblock.ShiftNumber = AShiftNumber) and
        (ATimeblock.EmployeeNumber = AEmployeeNumber) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
begin
  Result := False;
  if not TBFind then
  begin
    ATimeblock := TTimeblock.Create;
    ATimeblock.TypeCode := ATypeCode;
    ATimeblock.PlantCode := APlantCode;
    ATimeblock.DepartmentCode := ADepartmentCode;
    ATimeblock.ShiftNumber := AShiftNumber;
    ATimeblock.EmployeeNumber := AEmployeeNumber;
    ATimeblock.StartTime[1] := AStartTime1;
    ATimeblock.EndTime[1] := AEndTime1;
    ATimeblock.StartTime[2] := AStartTime2;
    ATimeblock.EndTime[2] := AEndTime2;
    ATimeblock.StartTime[3] := AStartTime3;
    ATimeblock.EndTime[3] := AEndTime3;
    ATimeblock.StartTime[4] := AStartTime4;
    ATimeblock.EndTime[4] := AEndTime4;
    ATimeblock.StartTime[5] := AStartTime5;
    ATimeblock.EndTime[5] := AEndTime5;
    ATimeblock.StartTime[6] := AStartTime6;
    ATimeblock.EndTime[6] := AEndTime6;
    ATimeblock.StartTime[7] := AStartTime7;
    ATimeblock.EndTime[7] := AEndTime7;
    TimeblockList.Add(ATimeblock);
    Result := True;
  end;
end; // TimeblockListAdd

function TReportEmpInfoDevPeriodQR.BreakListAdd(ATypeCode, APlantCode,
  ADepartmentCode, AShiftNumber, AEmployeeNumber: String;
  AStartTime1, AEndTime1, AStartTime2, AEndTime2, AStartTime3, AEndTime3,
  AStartTime4, AEndTime4, AStartTime5, AEndTime5, AStartTime6, AEndTime6,
  AStartTime7, AEndTime7: TDateTime): Boolean;
var
  ATimeblock: TTimeblock;
  function TBFind: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to BreakList.Count - 1 do
    begin
      ATimeblock := BreakList.Items[I];
      if (ATimeblock.TypeCode = ATypeCode) and
        (ATimeblock.PlantCode = APlantCode) and
        (ATimeblock.DepartmentCode = ADepartmentCode) and
        (ATimeblock.ShiftNumber = AShiftNumber) and
        (ATimeblock.EmployeeNumber = AEmployeeNumber) then
      begin
        Result := True;
        Break;
      end;
    end;
  end;
begin
  Result := False;
  if not TBFind then
  begin
    ATimeblock := TTimeblock.Create;
    ATimeblock.TypeCode := ATypeCode;
    ATimeblock.PlantCode := APlantCode;
    ATimeblock.DepartmentCode := ADepartmentCode;
    ATimeblock.ShiftNumber := AShiftNumber;
    ATimeblock.EmployeeNumber := AEmployeeNumber;
    ATimeblock.StartTime[1] := AStartTime1;
    ATimeblock.EndTime[1] := AEndTime1;
    ATimeblock.StartTime[2] := AStartTime2;
    ATimeblock.EndTime[2] := AEndTime2;
    ATimeblock.StartTime[3] := AStartTime3;
    ATimeblock.EndTime[3] := AEndTime3;
    ATimeblock.StartTime[4] := AStartTime4;
    ATimeblock.EndTime[4] := AEndTime4;
    ATimeblock.StartTime[5] := AStartTime5;
    ATimeblock.EndTime[5] := AEndTime5;
    ATimeblock.StartTime[6] := AStartTime6;
    ATimeblock.EndTime[6] := AEndTime6;
    ATimeblock.StartTime[7] := AStartTime7;
    ATimeblock.EndTime[7] := AEndTime7;
    BreakList.Add(ATimeblock);
    Result := True;
  end;
end; // BreakListAdd

procedure TReportEmpInfoDevPeriodQR.TimeblockListClear;
var
  I: Integer;
  ATimeblock: TTimeblock;
begin
  for I := TimeblockList.Count - 1 downto 0 do
  begin
    ATimeblock := TimeblockList.Items[I];
    TimeblockList.Remove(ATimeblock);
    ATimeblock.Free;
  end;
  TimeblockList.Clear;
  // Clear QRMemoTB
//  QRMemoTBType.Lines.Clear;
  QRMemoTBDesc.Lines.Clear;
  QRMemoTBPlant.Lines.Clear;
  QRMemoTBShift.Lines.Clear;
  QRMemoTBDept.Lines.Clear;
  QRMemoTBEmp.Lines.Clear;
  QRMemoTBDay1.Lines.Clear;
  QRMemoTBDay2.Lines.Clear;
  QRMemoTBDay3.Lines.Clear;
  QRMemoTBDay4.Lines.Clear;
  QRMemoTBDay5.Lines.Clear;
  QRMemoTBDay6.Lines.Clear;
  QRMemoTBDay7.Lines.Clear;
end; // TimeblockListClear

procedure TReportEmpInfoDevPeriodQR.BreakListClear;
var
  I: Integer;
  ATimeblock: TTimeblock;
begin
  for I := BreakList.Count - 1 downto 0 do
  begin
    ATimeblock := BreakList.Items[I];
    BreakList.Remove(ATimeblock);
    ATimeblock.Free;
  end;
  BreakList.Clear;
  // Clear QRMemoBB
//  QRMemoBBType.Lines.Clear;
  QRMemoBBDesc.Lines.Clear;
  QRMemoBBPlant.Lines.Clear;
  QRMemoBBShift.Lines.Clear;
  QRMemoBBDept.Lines.Clear;
  QRMemoBBEmp.Lines.Clear;
  QRMemoBBDay1.Lines.Clear;
  QRMemoBBDay2.Lines.Clear;
  QRMemoBBDay3.Lines.Clear;
  QRMemoBBDay4.Lines.Clear;
  QRMemoBBDay5.Lines.Clear;
  QRMemoBBDay6.Lines.Clear;
  QRMemoBBDay7.Lines.Clear;
end; // BreakListClear

procedure TReportEmpInfoDevPeriodQR.TimeblockBreakListAddItems;
var
  ADataSet: TDataSet;
  Ready: Boolean;
  procedure MemoTBAdd(ATypeCode, ADesc, APlantCode, AShiftNumber,
      ADepartmentCode, AEmployeeNumber: String;
      AStartTime1, AEndTime1, AStartTime2, AEndTime2,
      AStartTime3, AEndTime3, AStartTime4, AEndTime4,
      AStartTime5, AEndTime5, AStartTime6, AEndTime6,
      AStartTime7, AEndTime7: TDateTime);
  begin
//    QRMemoTBType.Lines.Add(ATypeCode);
    QRMemoTBDesc.Lines.Add(ADesc);
    QRMemoTBPlant.Lines.Add(APlantCode);
    QRMemoTBShift.Lines.Add(AShiftNumber);
    QRMemoTBDept.Lines.Add(ADepartmentCode);
    QRMemoTBEmp.Lines.Add(AEmployeeNumber);
    QRMemoTBDay1.Lines.Add(
      DateTime2StringTime(AStartTime1) + '-' +
      DateTime2StringTime(AEndTime1));
    QRMemoTBDay2.Lines.Add(
      DateTime2StringTime(AStartTime2) + '-' +
      DateTime2StringTime(AEndTime2));
    QRMemoTBDay3.Lines.Add(
      DateTime2StringTime(AStartTime3) + '-' +
      DateTime2StringTime(AEndTime3));
    QRMemoTBDay4.Lines.Add(
      DateTime2StringTime(AStartTime4) + '-' +
      DateTime2StringTime(AEndTime4));
    QRMemoTBDay5.Lines.Add(
      DateTime2StringTime(AStartTime5) + '-' +
      DateTime2StringTime(AEndTime5));
    QRMemoTBDay6.Lines.Add(
      DateTime2StringTime(AStartTime6) + '-' +
      DateTime2StringTime(AEndTime6));
    QRMemoTBDay7.Lines.Add(
      DateTime2StringTime(AStartTime7) + '-' +
      DateTime2StringTime(AEndTime7));
  end; // MemoTBAdd
  procedure MemoBBAdd(ATypeCode, ADesc, APlantCode, AShiftNumber,
      ADepartmentCode, AEmployeeNumber: String;
      AStartTime1, AEndTime1, AStartTime2, AEndTime2,
      AStartTime3, AEndTime3, AStartTime4, AEndTime4,
      AStartTime5, AEndTime5, AStartTime6, AEndTime6,
      AStartTime7, AEndTime7: TDateTime);
  begin
//    QRMemoBBType.Lines.Add(ATypeCode);
    QRMemoBBDesc.Lines.Add(ADesc);
    QRMemoBBPlant.Lines.Add(APlantCode);
    QRMemoBBShift.Lines.Add(AShiftNumber);
    QRMemoBBDept.Lines.Add(ADepartmentCode);
    QRMemoBBEmp.Lines.Add(AEmployeeNumber);
    QRMemoBBDay1.Lines.Add(
      DateTime2StringTime(AStartTime1) + '-' +
      DateTime2StringTime(AEndTime1));
    QRMemoBBDay2.Lines.Add(
      DateTime2StringTime(AStartTime2) + '-' +
      DateTime2StringTime(AEndTime2));
    QRMemoBBDay3.Lines.Add(
      DateTime2StringTime(AStartTime3) + '-' +
      DateTime2StringTime(AEndTime3));
    QRMemoBBDay4.Lines.Add(
      DateTime2StringTime(AStartTime4) + '-' +
      DateTime2StringTime(AEndTime4));
    QRMemoBBDay5.Lines.Add(
      DateTime2StringTime(AStartTime5) + '-' +
      DateTime2StringTime(AEndTime5));
    QRMemoBBDay6.Lines.Add(
      DateTime2StringTime(AStartTime6) + '-' +
      DateTime2StringTime(AEndTime6));
    QRMemoBBDay7.Lines.Add(
      DateTime2StringTime(AStartTime7) + '-' +
      DateTime2StringTime(AEndTime7));
  end; // MemoBBAdd
begin
  with ReportEmpInfoDevPeriodDM do
  begin
    with qryPlantShiftDept do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      Open;
      while not Eof do
      begin
        // Search for Timeblock per Emp, Dept or Shift and add it when found.
        // Timeblocks per Employee
        ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) + '''' +
          ' AND ' +
          'EMPLOYEE_NUMBER = ' + qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsString +
          ' AND ' +
          'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
        ADataSet.Filtered := True;
        Ready := not ADataSet.IsEmpty;
        if Ready then
        begin
          if TimeblockListAdd('Emp',
            ADataSet.FieldByName('PLANT_CODE').AsString,
            '', // dept
            ADataSet.FieldByName('SHIFT_NUMBER').AsString,
            ADataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
            ADataSet.FieldByName('STARTTIME1').AsDateTime,
            ADataSet.FieldByName('ENDTIME1').AsDateTime,
            ADataSet.FieldByName('STARTTIME2').AsDateTime,
            ADataSet.FieldByName('ENDTIME2').AsDateTime,
            ADataSet.FieldByName('STARTTIME3').AsDateTime,
            ADataSet.FieldByName('ENDTIME3').AsDateTime,
            ADataSet.FieldByName('STARTTIME4').AsDateTime,
            ADataSet.FieldByName('ENDTIME4').AsDateTime,
            ADataSet.FieldByName('STARTTIME5').AsDateTime,
            ADataSet.FieldByName('ENDTIME5').AsDateTime,
            ADataSet.FieldByName('STARTTIME6').AsDateTime,
            ADataSet.FieldByName('ENDTIME6').AsDateTime,
            ADataSet.FieldByName('STARTTIME7').AsDateTime,
            ADataSet.FieldByName('ENDTIME7').AsDateTime) then
          begin
            with ADataSet do
            begin
              while not Eof do
              begin
                // Add to Memo
                MemoTBAdd('Emp',
                  FieldByName('DESCRIPTION').AsString,
                  FieldByName('PLANT_CODE').AsString,
                  FieldByName('SHIFT_NUMBER').AsString,
                  '',
                  FieldByName('EMPLOYEE_NUMBER').AsString,
                  FieldByName('STARTTIME1').AsDateTime,
                  FieldByName('ENDTIME1').AsDateTime,
                  FieldByName('STARTTIME2').AsDateTime,
                  FieldByName('ENDTIME2').AsDateTime,
                  FieldByName('STARTTIME3').AsDateTime,
                  FieldByName('ENDTIME3').AsDateTime,
                  FieldByName('STARTTIME4').AsDateTime,
                  FieldByName('ENDTIME4').AsDateTime,
                  FieldByName('STARTTIME5').AsDateTime,
                  FieldByName('ENDTIME5').AsDateTime,
                  FieldByName('STARTTIME6').AsDateTime,
                  FieldByName('ENDTIME6').AsDateTime,
                  FieldByName('STARTTIME7').AsDateTime,
                  FieldByName('ENDTIME7').AsDateTime);
                Next;
              end; // while
            end; // with
          end;
        end
        else
        begin
          // Timeblocks per Department
          ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
          ADataSet.Filtered := False;
          ADataSet.Filter :=
            'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) +
            '''' + ' AND ' +
            'DEPARTMENT_CODE = ''' + DoubleQuote(FieldByName('DEPARTMENT_CODE').AsString) +
            '''' + ' AND ' +
            'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
          ADataSet.Filtered := True;
          Ready := not ADataSet.IsEmpty;
          if Ready then
          begin
            if TimeblockListAdd('Dept',
              ADataSet.FieldByName('PLANT_CODE').AsString,
              ADataSet.FieldByName('DEPARTMENT_CODE').AsString,
              ADataSet.FieldByName('SHIFT_NUMBER').AsString,
              '', // emp
              ADataSet.FieldByName('STARTTIME1').AsDateTime,
              ADataSet.FieldByName('ENDTIME1').AsDateTime,
              ADataSet.FieldByName('STARTTIME2').AsDateTime,
              ADataSet.FieldByName('ENDTIME2').AsDateTime,
              ADataSet.FieldByName('STARTTIME3').AsDateTime,
              ADataSet.FieldByName('ENDTIME3').AsDateTime,
              ADataSet.FieldByName('STARTTIME4').AsDateTime,
              ADataSet.FieldByName('ENDTIME4').AsDateTime,
              ADataSet.FieldByName('STARTTIME5').AsDateTime,
              ADataSet.FieldByName('ENDTIME5').AsDateTime,
              ADataSet.FieldByName('STARTTIME6').AsDateTime,
              ADataSet.FieldByName('ENDTIME6').AsDateTime,
              ADataSet.FieldByName('STARTTIME7').AsDateTime,
              ADataSet.FieldByName('ENDTIME7').AsDateTime) then
            begin
              with ADataSet do
              begin
                while not Eof do
                begin
                  // Add to Memo
                  MemoTBAdd('Dept',
                    FieldByName('DESCRIPTION').AsString,
                    FieldByName('PLANT_CODE').AsString,
                    FieldByName('SHIFT_NUMBER').AsString,
                    FieldByName('DEPARTMENT_CODE').AsString,
                    '', // emp
                    FieldByName('STARTTIME1').AsDateTime,
                    FieldByName('ENDTIME1').AsDateTime,
                    FieldByName('STARTTIME2').AsDateTime,
                    FieldByName('ENDTIME2').AsDateTime,
                    FieldByName('STARTTIME3').AsDateTime,
                    FieldByName('ENDTIME3').AsDateTime,
                    FieldByName('STARTTIME4').AsDateTime,
                    FieldByName('ENDTIME4').AsDateTime,
                    FieldByName('STARTTIME5').AsDateTime,
                    FieldByName('ENDTIME5').AsDateTime,
                    FieldByName('STARTTIME6').AsDateTime,
                    FieldByName('ENDTIME6').AsDateTime,
                    FieldByName('STARTTIME7').AsDateTime,
                    FieldByName('ENDTIME7').AsDateTime);
                  Next;
                end; // while
              end; // with
            end;
          end
          else
          begin
            // Timeblocks per Shift
            ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
            ADataSet.Filtered := False;
            ADataSet.Filter :=
              'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) +
              '''' + ' AND ' +
              'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
            ADataSet.Filtered := True;
            Ready := not ADataSet.IsEmpty;
            if Ready then
            begin
              if TimeblockListAdd('Shift',
                ADataSet.FieldByName('PLANT_CODE').AsString,
                '', // dept
                ADataSet.FieldByName('SHIFT_NUMBER').AsString,
                '', // emp
                ADataSet.FieldByName('STARTTIME1').AsDateTime,
                ADataSet.FieldByName('ENDTIME1').AsDateTime,
                ADataSet.FieldByName('STARTTIME2').AsDateTime,
                ADataSet.FieldByName('ENDTIME2').AsDateTime,
                ADataSet.FieldByName('STARTTIME3').AsDateTime,
                ADataSet.FieldByName('ENDTIME3').AsDateTime,
                ADataSet.FieldByName('STARTTIME4').AsDateTime,
                ADataSet.FieldByName('ENDTIME4').AsDateTime,
                ADataSet.FieldByName('STARTTIME5').AsDateTime,
                ADataSet.FieldByName('ENDTIME5').AsDateTime,
                ADataSet.FieldByName('STARTTIME6').AsDateTime,
                ADataSet.FieldByName('ENDTIME6').AsDateTime,
                ADataSet.FieldByName('STARTTIME7').AsDateTime,
                ADataSet.FieldByName('ENDTIME7').AsDateTime) then
              begin
                with ADataSet do
                begin
                  while not Eof do
                  begin
                    // Add to Memo
                    MemoTBAdd('Shift',
                      FieldByName('DESCRIPTION').AsString,
                      FieldByName('PLANT_CODE').AsString,
                      FieldByName('SHIFT_NUMBER').AsString,
                      '', // dept
                      '', // emp
                      FieldByName('STARTTIME1').AsDateTime,
                      FieldByName('ENDTIME1').AsDateTime,
                      FieldByName('STARTTIME2').AsDateTime,
                      FieldByName('ENDTIME2').AsDateTime,
                      FieldByName('STARTTIME3').AsDateTime,
                      FieldByName('ENDTIME3').AsDateTime,
                      FieldByName('STARTTIME4').AsDateTime,
                      FieldByName('ENDTIME4').AsDateTime,
                      FieldByName('STARTTIME5').AsDateTime,
                      FieldByName('ENDTIME5').AsDateTime,
                      FieldByName('STARTTIME6').AsDateTime,
                      FieldByName('ENDTIME6').AsDateTime,
                      FieldByName('STARTTIME7').AsDateTime,
                      FieldByName('ENDTIME7').AsDateTime);
                    Next;
                  end; // while
                end; // with
              end;
            end;
          end;
        end;
        // Search for Breaks per Emp, Dept or Shift and add it when found.
        // Breaks per Employee
        ADataSet := CalculateTotalHoursDM.ClientDataSetBEmpl;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) + '''' + ' AND ' +
          'EMPLOYEE_NUMBER = ' + qryEmpInfo.FieldByName('EMPLOYEE_NUMBER').AsString + ' AND ' +
          'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
        ADataSet.Filtered := True;
        Ready := not ADataSet.IsEmpty;
        if Ready then
        begin
          if BreakListAdd('Emp',
            ADataSet.FieldByName('PLANT_CODE').AsString,
            '', // dept
            ADataSet.FieldByName('SHIFT_NUMBER').AsString,
            ADataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
            ADataSet.FieldByName('STARTTIME1').AsDateTime,
            ADataSet.FieldByName('ENDTIME1').AsDateTime,
            ADataSet.FieldByName('STARTTIME2').AsDateTime,
            ADataSet.FieldByName('ENDTIME2').AsDateTime,
            ADataSet.FieldByName('STARTTIME3').AsDateTime,
            ADataSet.FieldByName('ENDTIME3').AsDateTime,
            ADataSet.FieldByName('STARTTIME4').AsDateTime,
            ADataSet.FieldByName('ENDTIME4').AsDateTime,
            ADataSet.FieldByName('STARTTIME5').AsDateTime,
            ADataSet.FieldByName('ENDTIME5').AsDateTime,
            ADataSet.FieldByName('STARTTIME6').AsDateTime,
            ADataSet.FieldByName('ENDTIME6').AsDateTime,
            ADataSet.FieldByName('STARTTIME7').AsDateTime,
            ADataSet.FieldByName('ENDTIME7').AsDateTime) then
          begin
            with ADataSet do
            begin
              while not Eof do
              begin
                // Add to Memo
                MemoBBAdd('Emp',
                  FieldByName('DESCRIPTION').AsString,
                  FieldByName('PLANT_CODE').AsString,
                  FieldByName('SHIFT_NUMBER').AsString,
                  '',
                  FieldByName('EMPLOYEE_NUMBER').AsString,
                  FieldByName('STARTTIME1').AsDateTime,
                  FieldByName('ENDTIME1').AsDateTime,
                  FieldByName('STARTTIME2').AsDateTime,
                  FieldByName('ENDTIME2').AsDateTime,
                  FieldByName('STARTTIME3').AsDateTime,
                  FieldByName('ENDTIME3').AsDateTime,
                  FieldByName('STARTTIME4').AsDateTime,
                  FieldByName('ENDTIME4').AsDateTime,
                  FieldByName('STARTTIME5').AsDateTime,
                  FieldByName('ENDTIME5').AsDateTime,
                  FieldByName('STARTTIME6').AsDateTime,
                  FieldByName('ENDTIME6').AsDateTime,
                  FieldByName('STARTTIME7').AsDateTime,
                  FieldByName('ENDTIME7').AsDateTime);
                Next;
              end; // while
            end; // with
          end;
        end
        else
        begin
          // Breaks per Department
          ADataSet := CalculateTotalHoursDM.ClientDataSetBDept;
          ADataSet.Filtered := False;
          ADataSet.Filter :=
            'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) + '''' + ' AND ' +
            'DEPARTMENT_CODE = ''' + DoubleQuote(FieldByName('DEPARTMENT_CODE').AsString) + ''' AND ' +
            'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
          ADataSet.Filtered := True;
          Ready := not ADataSet.IsEmpty;
          if Ready then
          begin
            if BreakListAdd('Dept',
              ADataSet.FieldByName('PLANT_CODE').AsString,
              ADataSet.FieldByName('DEPARTMENT_CODE').AsString,
              ADataSet.FieldByName('SHIFT_NUMBER').AsString,
              '',
              ADataSet.FieldByName('STARTTIME1').AsDateTime,
              ADataSet.FieldByName('ENDTIME1').AsDateTime,
              ADataSet.FieldByName('STARTTIME2').AsDateTime,
              ADataSet.FieldByName('ENDTIME2').AsDateTime,
              ADataSet.FieldByName('STARTTIME3').AsDateTime,
              ADataSet.FieldByName('ENDTIME3').AsDateTime,
              ADataSet.FieldByName('STARTTIME4').AsDateTime,
              ADataSet.FieldByName('ENDTIME4').AsDateTime,
              ADataSet.FieldByName('STARTTIME5').AsDateTime,
              ADataSet.FieldByName('ENDTIME5').AsDateTime,
              ADataSet.FieldByName('STARTTIME6').AsDateTime,
              ADataSet.FieldByName('ENDTIME6').AsDateTime,
              ADataSet.FieldByName('STARTTIME7').AsDateTime,
              ADataSet.FieldByName('ENDTIME7').AsDateTime) then
            begin
              with ADataSet do
              begin
                while not Eof do
                begin
                  // Add to Memo
                  MemoBBAdd('Dept',
                    FieldByName('DESCRIPTION').AsString,
                    FieldByName('PLANT_CODE').AsString,
                    FieldByName('SHIFT_NUMBER').AsString,
                    FieldByName('DEPARTMENT_CODE').AsString,
                    '', // emp
                    FieldByName('STARTTIME1').AsDateTime,
                    FieldByName('ENDTIME1').AsDateTime,
                    FieldByName('STARTTIME2').AsDateTime,
                    FieldByName('ENDTIME2').AsDateTime,
                    FieldByName('STARTTIME3').AsDateTime,
                    FieldByName('ENDTIME3').AsDateTime,
                    FieldByName('STARTTIME4').AsDateTime,
                    FieldByName('ENDTIME4').AsDateTime,
                    FieldByName('STARTTIME5').AsDateTime,
                    FieldByName('ENDTIME5').AsDateTime,
                    FieldByName('STARTTIME6').AsDateTime,
                    FieldByName('ENDTIME6').AsDateTime,
                    FieldByName('STARTTIME7').AsDateTime,
                    FieldByName('ENDTIME7').AsDateTime);
                  Next;
                end; // while
              end; // with
            end;
          end
          else
          begin
            // Breaks per Shift
            ADataSet := CalculateTotalHoursDM.ClientDataSetBShift;
            ADataSet.Filtered := False;
            ADataSet.Filter :=
              'PLANT_CODE = ' + '''' + DoubleQuote(FieldByName('PLANT_CODE').AsString) + '''' + ' AND ' +
              'SHIFT_NUMBER = ' + FieldByName('SHIFT_NUMBER').AsString;
            ADataSet.Filtered := True;
            Ready := not ADataSet.IsEmpty;
            if Ready then
            begin
              if BreakListAdd('Shift',
                ADataSet.FieldByName('PLANT_CODE').AsString,
                '',
                ADataSet.FieldByName('SHIFT_NUMBER').AsString,
                '',
                ADataSet.FieldByName('STARTTIME1').AsDateTime,
                ADataSet.FieldByName('ENDTIME1').AsDateTime,
                ADataSet.FieldByName('STARTTIME2').AsDateTime,
                ADataSet.FieldByName('ENDTIME2').AsDateTime,
                ADataSet.FieldByName('STARTTIME3').AsDateTime,
                ADataSet.FieldByName('ENDTIME3').AsDateTime,
                ADataSet.FieldByName('STARTTIME4').AsDateTime,
                ADataSet.FieldByName('ENDTIME4').AsDateTime,
                ADataSet.FieldByName('STARTTIME5').AsDateTime,
                ADataSet.FieldByName('ENDTIME5').AsDateTime,
                ADataSet.FieldByName('STARTTIME6').AsDateTime,
                ADataSet.FieldByName('ENDTIME6').AsDateTime,
                ADataSet.FieldByName('STARTTIME7').AsDateTime,
                ADataSet.FieldByName('ENDTIME7').AsDateTime) then
              begin
                with ADataSet do
                begin
                  while not Eof do
                  begin
                    // Add to Memo
                    MemoBBAdd('Shift',
                      FieldByName('DESCRIPTION').AsString,
                      FieldByName('PLANT_CODE').AsString,
                      FieldByName('SHIFT_NUMBER').AsString,
                      '', // dept
                      '', // emp
                      FieldByName('STARTTIME1').AsDateTime,
                      FieldByName('ENDTIME1').AsDateTime,
                      FieldByName('STARTTIME2').AsDateTime,
                      FieldByName('ENDTIME2').AsDateTime,
                      FieldByName('STARTTIME3').AsDateTime,
                      FieldByName('ENDTIME3').AsDateTime,
                      FieldByName('STARTTIME4').AsDateTime,
                      FieldByName('ENDTIME4').AsDateTime,
                      FieldByName('STARTTIME5').AsDateTime,
                      FieldByName('ENDTIME5').AsDateTime,
                      FieldByName('STARTTIME6').AsDateTime,
                      FieldByName('ENDTIME6').AsDateTime,
                      FieldByName('STARTTIME7').AsDateTime,
                      FieldByName('ENDTIME7').AsDateTime);
                    Next;
                  end; // while
                end; // with
              end;
            end;
          end;
        end;
        Next;
      end; // while
    end;
  end;
end; // TimeblockBreakListAddItems

procedure TReportEmpInfoDevPeriodQR.QRChildBandTimeblockDefsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel55.Caption);
    ExportClass.AddText(
      QRLabel67.Caption + ExportClass.Sep +
      QRLabel57.Caption + ExportClass.Sep +
      QRLabel58.Caption + ExportClass.Sep +
      QRLabel59.Caption + ExportClass.Sep +
      QRLabel60.Caption + ExportClass.Sep +
      QRLblTBDay1.Caption + ExportClass.Sep +
      QRLblTBDay2.Caption + ExportClass.Sep +
      QRLblTBDay3.Caption + ExportClass.Sep +
      QRLblTBDay4.Caption + ExportClass.Sep +
      QRLblTBDay5.Caption + ExportClass.Sep +
      QRLblTBDay6.Caption + ExportClass.Sep +
      QRLblTBDay7.Caption + ExportClass.Sep
      );
    for I := 0 to QRMemoTBDesc.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoTBDesc.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBPlant.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBShift.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDept.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBEmp.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay1.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay2.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay3.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay4.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay5.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay6.Lines.Strings[I] + ExportClass.Sep +
        QRMemoTBDay7.Lines.Strings[I] + ExportClass.Sep
        );
    end;
  end;
end; // QRChildBandTimeblockDefsAfterPrint

procedure TReportEmpInfoDevPeriodQR.QRChildBandBreakDefsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel61.Caption);
    ExportClass.AddText(
      QRLabel68.Caption + ExportClass.Sep +
      QRLabel63.Caption + ExportClass.Sep +
      QRLabel64.Caption + ExportClass.Sep +
      QRLabel65.Caption + ExportClass.Sep +
      QRLabel66.Caption + ExportClass.Sep +
      QRLblBBDay1.Caption + ExportClass.Sep +
      QRLblBBDay2.Caption + ExportClass.Sep +
      QRLblBBDay3.Caption + ExportClass.Sep +
      QRLblBBDay4.Caption + ExportClass.Sep +
      QRLblBBDay5.Caption + ExportClass.Sep +
      QRLblBBDay6.Caption + ExportClass.Sep +
      QRLblBBDay7.Caption + ExportClass.Sep
      );
    for I := 0 to QRMemoBBDesc.Lines.Count - 1 do
    begin
      ExportClass.AddText(
        QRMemoBBDesc.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBPlant.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBShift.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDept.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBEmp.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay1.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay2.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay3.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay4.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay5.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay6.Lines.Strings[I] + ExportClass.Sep +
        QRMemoBBDay7.Lines.Strings[I] + ExportClass.Sep
        );
    end;
  end;
end; // QRChildBandBreakDefsAfterPrint

end.
