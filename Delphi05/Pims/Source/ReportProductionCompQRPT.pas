(*
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:8-MAY-2012 20012858. Production Reports and NOJOB.
  - Filter out the NOJOB-job to prevent wrong quantities when
    this job has a negative quantity.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  MRA:22-MAR-2013 SO-20013769 - reorganised header
  - Reorganize the headers so plant/business unit/dept are shown first.
  - Related to this order: Shorten workspot description
  MRA:3-DEC-2013 TD-23759
  - Report production comparison gives sometimes wrong values
    - The report gave double quantities.
    - Cause: It did a join with DEPARTMENTPERTEAM, which gave double values.
    - Parameter 'All teams' is added, based on 'all teams'-checkbox in dialog.
  MRA:18-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:19-AUG-2014 20015220.90 Rework
  - Make more room for from-to-columns in selection-part, because of
    from-to-date that can be much larger when time-part is shown.
*)
unit ReportProductionCompQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, DB,
  QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt;


type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo, FTeamFrom, FTeamTo: String;
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK, FRepDetail: Boolean;
    FCheckboxAllTeams: Boolean; // TD-23759
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo: String;
      ShowSelection,
      PagePlant, PageBU, PageDept, PageWK, RepDetail: Boolean;
      CheckBoxAllTeams: Boolean); // TD-23759
  end;

  TReportProductionCompQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupJOB: TQRGroup;
    QRGroupColumnHeader: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDescBU: TQRDBText;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRDBText2: TQRDBText;
    QRSubDetail1: TQRSubDetail;
    QRBandFooterJob: TQRBand;
    QRDBText13: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabelOtherJob: TQRLabel;
    QRLabelDevPCS: TQRLabel;
    QRLabelDevProc: TQRLabel;
    ChildBand2: TQRChildBand;
    QRMemoWK: TQRMemo;
    QRMemoJob: TQRMemo;
    QRMemoJobDesc: TQRMemo;
    QRMemoPcs: TQRMemo;
    QRMemoWKDesc: TQRMemo;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel12: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRLabelDevProc1Print(sender: TObject; var Value: String);
    procedure QRBandFooterJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    AllPlants, AllTeams, AllBusinessUnits, AllWorkspots,
    AllDepartments: Boolean;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
     PiecesJob: Double;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowSelection, PagePlant,
      PageBU, PageDept, PageWK,  RepDetail, CheckBoxAllTeams: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportProductionCompQR: TReportProductionCompQR;

implementation

{$R *.DFM}
uses
  SystemDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  ReportProductionCompDMT;


procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo: String;
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, RepDetail,
      CheckBoxAllTeams: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    FWKFrom := ReportProductionCompQR.InclExclWorkspotsResult;
    FWKTo := ReportProductionCompQR.InclExclWorkspotsResult;
  end
  else
  begin
    FWKFrom := WKFrom;
    FWKTo := WKTo;
  end;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FTeamTo := TeamTo;
  FTeamFrom := TeamFrom;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;

  FRepDetail := RepDetail;
  FCheckBoxAllTeams := CheckBoxAllTeams;
end;

function TReportProductionCompQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo, TeamFrom, TeamTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowSelection, PagePlant,
  PageBU, PageDept, PageWK , RepDetail, CheckBoxAllTeams: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo,'1', '1');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK,  RepDetail,
      CheckBoxAllTeams);
  end;
  SetDataSetQueryReport(ReportProductionCompDM.QueryProductionComp);
end;

function TReportProductionCompQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;

  // MR:27-09-2005 Order 550406 Initialize this here.
  PiecesJob := 0;

  with ReportProductionCompDM do
  begin
    AllPlants := DetermineAllPlants(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
    AllTeams := DetermineAllTeams(
      QRParameters.FTeamFrom, QRParameters.FTeamTo);
    AllBusinessUnits := False;
    AllWorkspots := False;
    AllDepartments := False;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      AllBusinessUnits := DetermineAllBusinessUnits(
        QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
      AllWorkspots := DetermineAllWorkspots(QRBaseParameters.FPlantFrom,
        QRParameters.FWKFrom, QRParameters.FWKTo);
      AllDepartments := DetermineAllDepartments(QRBaseParameters.FPlantFrom,
        QRParameters.FDeptFrom, QRParameters.FDeptTo);
    end;
  end;
  // TD-23759 This overrules this setting!
  AllTeams := QRParameters.FCheckBoxAllTeams;

  SelectStr :=
    'SELECT ' + NL +
    '  P.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION, ' + NL +
    '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION, ' + NL +
    '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '  PQ.JOB_CODE, J.DESCRIPTION AS JOBDESCRIPTION, ' + NL +
    '  SUM(PQ.QUANTITY) AS PIECES ' + NL +
    'FROM ' + NL +
    '  PRODUCTIONQUANTITY PQ INNER JOIN PLANT P ON ' + NL +
    '    PQ.PLANT_CODE = P.PLANT_CODE ' + NL +
    '    AND PQ.START_DATE >= :DATEFROM ' + NL +
    '    AND PQ.START_DATE < :DATETO ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    PQ.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND PQ.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    PQ.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND PQ.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND PQ.JOB_CODE = J.JOB_CODE ' + NL +
    '    AND J.COMPARATION_JOB_YN = ''Y'' ' + NL +
    '  INNER JOIN BUSINESSUNIT B ON ' + NL +
    '    PQ.PLANT_CODE = B.PLANT_CODE ' + NL +
    '    AND J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    PQ.PLANT_CODE = D.PLANT_CODE ' + NL +
    '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
    // TD-23759 Do not use this join! Or it will give double values!
(*
  if not AllTeams then
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENTPERTEAM T ON ' + NL +
      '    W.PLANT_CODE = T.PLANT_CODE ' + NL +
      '    AND W.DEPARTMENT_CODE = T.DEPARTMENT_CODE ' + NL +
      '    AND T.TEAM_CODE >= :TEAMFROM ' + NL +
      '    AND T.TEAM_CODE <= :TEAMTO ' + NL;
*)
  SelectStr := SelectStr +
    'WHERE ' + NL +
    // 20012858.
    '  (PQ.JOB_CODE <> ' + '''' + NOJOB + '''' + ') AND ' + NL +
    '  PQ.QUANTITY <> 0 ' + NL;
  // TD-23759 Use this instead of the join above!
  if not AllTeams then
    SelectStr := SelectStr +
      '  AND ' + NL +
      '    EXISTS ' + NL +
      '      (SELECT T.TEAM_CODE ' + NL +
      '       FROM DEPARTMENTPERTEAM T ' + NL +
      '       WHERE ' + NL +
      '         W.PLANT_CODE = T.PLANT_CODE ' + NL +
      '         AND W.DEPARTMENT_CODE = T.DEPARTMENT_CODE ' + NL +
      '         AND T.TEAM_CODE >= :TEAMFROM ' + NL +
      '         AND T.TEAM_CODE <= :TEAMTO) ' + NL;
  //RV067.2.
  // TD-23759
  SelectStr := SelectStr +
    '  AND ( ' + NL +
    '    (:USER_NAME = ''*'') OR ' + NL +
    '    ( ' + NL +
    '       EXISTS ' + NL +
    '       (SELECT T.TEAM_CODE ' + NL +
    '        FROM DEPARTMENTPERTEAM T ' + NL +
    '        WHERE ' + NL +
    '          T.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '          T.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL;
  if not AllTeams then
    SelectStr := SelectStr +
      '          AND T.TEAM_CODE >= :TEAMFROM ' + NL +
      '          AND T.TEAM_CODE <= :TEAMTO ' + NL;
  SelectStr := SelectStr +
    '          AND EXISTS ' + NL +
    '            (SELECT U.TEAM_CODE FROM TEAMPERUSER U ' + NL +
    '            WHERE U.USER_NAME = :USER_NAME ' + NL +
    '            AND U.TEAM_CODE = T.TEAM_CODE) ' + NL +
    '       ) ' + NL +
    '     ) ' + NL +
    '   ) ' + NL;
(*
      // TD-23759 Replaced (see above).
{      '    (T.TEAM_CODE IN ' + NL + }
    '    ((SELECT MIN(T.TEAM_CODE) ' + NL +
    '      FROM DEPARTMENTPERTEAM T ' + NL +
    '      WHERE ' + NL +
    '        T.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '        T.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL;
  if not AllTeams then
    SelectStr := SelectStr +
      '        AND T.TEAM_CODE >= :TEAMFROM ' + NL +
      '        AND T.TEAM_CODE <= :TEAMTO ' + NL;
  SelectStr := SelectStr +
    '      )  IN ' + NL +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U ' + NL +
    '       WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '  ) ';
*)
  if not AllPlants then
    SelectStr := SelectStr +
      '  AND PQ.PLANT_CODE >= :PLANTFROM ' + NL +
      '  AND PQ.PLANT_CODE <= :PLANTTO ' + NL;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectStr := SelectStr +
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PQ.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
      if not InclExclWorkspotsAll then
      begin
        SelectStr := SelectStr +
          '  AND PQ.JOB_CODE >= :JOBFROM ' + NL +
          '  AND PQ.JOB_CODE <= :JOBTO ' + NL;
      end;
    end
    else
      if not AllWorkspots then
      begin
        SelectStr := SelectStr +
          '  AND PQ.WORKSPOT_CODE >= :WORKSPOTFROM ' + NL +
          '  AND PQ.WORKSPOT_CODE <= :WORKSPOTTO ' + NL;
        if QRParameters.FWKFrom = QRParameters.FWKTo then
        begin
          SelectStr := SelectStr +
            '  AND PQ.JOB_CODE >= :JOBFROM ' + NL +
            '  AND PQ.JOB_CODE <= :JOBTO ' + NL;
         end;
      end; // if not AllWorkspots
    if not AllBusinessUnits then
      SelectStr := SelectStr +
        '  AND B.BUSINESSUNIT_CODE >= :BUFROM ' + NL +
        '  AND B.BUSINESSUNIT_CODE <= :BUTO ' + NL;
    if not AllDepartments then
      SelectStr := SelectStr +
        '  AND D.DEPARTMENT_CODE >= :DEPTFROM ' + NL +
        '  AND D.DEPARTMENT_CODE <= :DEPTTO ' + NL;
  end;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  P.PLANT_CODE, P.DESCRIPTION , ' + NL +
    '  B.BUSINESSUNIT_CODE, B.DESCRIPTION, ' + NL +
    '  D.DEPARTMENT_CODE, D.DESCRIPTION, ' + NL +
    '  W.WORKSPOT_CODE, W.DESCRIPTION, ' + NL +
    '  PQ.JOB_CODE, J.DESCRIPTION ' + NL;
  // TD-23759 Addition of order by
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  P.PLANT_CODE, ' + NL +
    '  B.BUSINESSUNIT_CODE, ' + NL +
    '  D.DEPARTMENT_CODE, ' + NL +
    '  W.WORKSPOT_CODE, ' + NL +
    '  PQ.JOB_CODE ' + NL;

  with ReportProductionCompDM.QueryProductionComp  do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
// SQL.SaveToFile('c:\temp\ReportProductionComp.sql');

    //RV067.2.
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      ParamByName('USER_NAME').AsString := '*';

    if not UseDateTime then // 20015220
    begin
      QRParameters.FDateFrom := Trunc(QRParameters.FDateFrom);
      QRParameters.FDateTo := Trunc(QRParameters.FDateTo) + 1;
    end;
    if not AllPlants then
    begin
      ParamByName('PLANTFROM').Value := QRBaseParameters.FPlantFrom;
      ParamByName('PLANTTO').Value := QRBaseParameters.FPlantTo;
    end;
    if not AllTeams then
    begin
      ParamByName('TEAMFROM').AsString := QRParameters.FTeamFrom;
      ParamByName('TEAMTO').AsString := QRParameters.FTeamTo;
    end;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      if SystemDM.WorkspotInclSel then // 20015223
      begin
        if not InclExclWorkspotsAll then
        begin
          ParamByName('JOBFROM').Value := QRParameters.FJobFrom;
          ParamByName('JOBTO').Value := QRParameters.FJobTo;
        end;
      end
      else
        if not AllWorkspots then
        begin
          ParamByName('WORKSPOTFROM').Value := QRParameters.FWKFrom;
          ParamByName('WORKSPOTTO').Value := QRParameters.FWKTo;
          if QRParameters.FWKFrom = QRParameters.FWKTo then
          begin
            ParamByName('JOBFROM').Value := QRParameters.FJobFrom;
            ParamByName('JOBTO').Value := QRParameters.FJobTo;
          end;
        end; // if not AllWorkspots
      if not AllBusinessUnits then
      begin
        ParamByName('BUFROM').Value := QRParameters.FBusinessFrom;
        ParamByName('BUTO').Value := QRParameters.FBusinessTo;
      end;
      if not AllDepartments then
      begin
        ParamByName('DEPTFROM').Value := QRParameters.FDeptFrom;
        ParamByName('DEPTTO').Value := QRParameters.FDeptTo;
      end;
    end;
    ParamByName('DATEFROM').Value := QRParameters.FDateFrom;
    ParamByName('DATETO').Value := QRParameters.FDateTo;

    Active := True;
    Result :=  not IsEmpty;
  end;

  if Result then
    ReportProductionCompDM.InitQueryTotalPiecesPQ(
      ReportProductionCompDM.QueryPQ,
      QRParameters.FDateFrom, QRParameters.FDateTo,
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);

  Screen.Cursor := crDefault;
end;

procedure TReportProductionCompQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := DisplayWorkspotFrom(QRParameters.FWKFrom); // 20015223
  QRLabelWKTo.Caption := DisplayWorkspotTo(QRParameters.FWKTo); // 20015223
  if UseDateTime then // 20015220
  begin
    QRLabelDateFrom.Caption := DateTimeToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateTimeToStr(QRParameters.FDateTo);
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo - 1);
  end;
  if not AllTeams then
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end
  else
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end;
end;

procedure TReportProductionCompQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportProductionCompQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
end;

procedure TReportProductionCompQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
end;

procedure TReportProductionCompQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
end;


procedure TReportProductionCompQR.QRGroupHDBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHDBU.ForceNewPage := QRParameters.FPageBU;
  inherited;
end;

procedure TReportProductionCompQR.QRGroupHDDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHDDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
end;

procedure TReportProductionCompQR.QRGroupHDWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHDWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
end;

procedure TReportProductionCompQR.QRGroupJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  PiecesJob := 0;
end;

procedure TReportProductionCompQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProductionCompQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProductionCompQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProductionCompQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProductionCompQR.QRDBText7Print(sender: TObject;
  var Value: String);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  Value:= Format('%.0f', [PiecesJob]);
  Value:= Format('%.0n', [PiecesJob]);
end;

procedure TReportProductionCompQR.QRLabelDevProc1Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Value + '%';
end;

procedure TReportProductionCompQR.QRBandFooterJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalJobPcs, TotalCompJob, DevPcs: Double;
  Plant_Code, WK_Code, JOB_CODE, Comp_WK, Comp_Job: String;
  WorkspotDesc: String;
begin
  inherited;

  TotalJobPcs := 0;

  QRMemoWK.Lines.Clear;
  QRMemoWKDesc.Lines.Clear;
  QRMemoJob.Lines.Clear;
  QRMemoJobDesc.Lines.Clear;
  QRMemoPcs.Lines.Clear;
  with ReportProductionCompDM do
  begin
    Plant_Code := QueryProductionComp.FieldByName('PLANT_CODE').AsString;
    WK_Code := QueryProductionComp.FieldByName('WORKSPOT_CODE').AsString;
    JOB_CODE := QueryProductionComp.FieldByName('JOB_CODE').AsString;

    QueryCompJob.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
      VarArrayOf([Plant_Code, WK_Code, JOB_CODE]), []);
    while not QueryCompJob.Eof and
     (QueryCompJob.FieldByName('PLANT_CODE').Value = Plant_Code) and
     (QueryCompJob.FieldByName('WORKSPOT_CODE').Value = WK_Code) and
     (QueryCompJob.FieldByName('JOB_CODE').Value = JOB_CODE) do
    begin
      Comp_wk := QueryCompJob.FieldByName('COMPARE_WORKSPOT_CODE').Value;
      Comp_Job := QueryCompJob.FieldByName('COMPARE_JOB_CODE').Value;
      QRMemoWK.Lines.Add(Comp_wk);


      // SO-20013769 Related to this order. Shorten workspot description.
      WorkspotDesc := QueryCompJob.FieldByName('WDESC').AsString;
      if Length(WorkspotDesc) > 18 then
        WorkspotDesc := Copy(WorkspotDesc, 1, 18) + '...';
      QRMemoWKDesc.Lines.Add(WorkspotDesc
        {QueryCompJob.FieldByName('WDESC').AsString} + ',');
      QRMemoJob.Lines.Add(Comp_Job);
      QRMemoJobDesc.Lines.Add(QueryCompJob.FieldByName('JDESC').AsString);
      TotalCompJob := 0;
      if QueryPQ.Locate('PLANT_CODE;WORKSPOT_CODE;JOB_CODE',
        VarArrayOf([Plant_code,  Comp_wk, Comp_Job]), []) then
        TotalCompJob := TotalCompJob + QueryPQ.FieldByNAME('QUANTITY').AsFloat;

      TotalJobPcs := TotalJobPcs + TotalCompJob;
// ROP 05-MAR-2013 TD-21527      QRMemoPcs.Lines.Add(Format('%.0f', [TotalCompJob]));
      QRMemoPcs.Lines.Add(Format('%.0n', [TotalCompJob]));
      QueryCompJob.Next;
    end;  //while

// ROP 05-MAR-2013 TD-21527    QRLabelOtherJob.Caption :=  Format('%.0f', [TotalJobPcs]);
    QRLabelOtherJob.Caption :=  Format('%.0n', [TotalJobPcs]);
    DevPcs := Round(PiecesJob) - Round(TotalJobPcs);
// ROP 05-MAR-2013 TD-21527    QRLabelDevPCS.Caption :=  Format('%.0f', [DevPcs]);
    QRLabelDevPCS.Caption :=  Format('%.0n', [DevPcs]);

    if TotalJobPcs <> 0 then
      DevPcs := (DevPcs / TotalJobPcs) * 100;
// ROP 05-MAR-2013 TD-21527    QRLabelDevProc.Caption :=  Format('%.0f', [DevPcs]);
    QRLabelDevProc.Caption :=  Format('%.0n', [DevPcs]);
  end;//with
end;

procedure TReportProductionCompQR.ChildBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FRepDetail;
end;

procedure TReportProductionCompQR.QRSubDetail1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  PiecesJob := PiecesJob +
    ReportProductionCompDM.QueryProductionComp.FieldByName('PIECES').AsFloat;
end;

end.
