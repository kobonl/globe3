(*
  Changes:
    MR:05-11-2004 Order 550349 Plan in other plants.
    MR:22-01-2008 Order 550461, Oracle 10, RV002. RECNO-Float/Integer error.
    MR:24-01-2008 Order 550461, Oracle 10, RV003. Fix query QueryAvailability.
    MRA:8-JAN-2010. RV050.6. 889967.
    - Filling standard availability does not use the shift schedule.
      When days are left empty (not shift) in shift schedule, then
      Staff Availability is still showing shift-numbers.
      Employee Availability is showing it correct.
      CAUSE: When there was no Employee Availability, it used the shift
            of the SHIFTSCHEDULE-table, but it did not look if this did exist
            in QueryAvailabilty.
      Changed: QueryAvailability, when ShiftSchedule-record does not exist, then
               return an empty string (null).
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:1-FEB-2010. RV050.4.3.
    - Do not show a -1 for a shift!
    MRA:3-FEB-2010. RV052.2. Bugfix.
    - Previous change caused ShiftSchedule-overwrite with wrong shift-values.
    MRA:22-FEB-2010. RV054.7. 889962.
    - Sort on Description for popup-menu for Absence-reasons: QueryAbsReason.
    MRA:25-MAR-2010. RV057.2.
    - Gave an error-message when trying to enter an absence-reason.
    MRA:17-SEP-2010. RV067.MRA.32 Order 550517
    - Staff Avail. gives wrong results (shiftnumbers can be wrong).
      The MAX in the query could give a wrong shiftnumber.
      Changed: Query for QueryAvailability.
    MRA:21-OCT-2010. RV073.6. Inactive employees.
    - Inactive employees are still shown in this dialog.
      Depending on selected week the employee will be shown or not:
      - Only when employee is active till that week.
      Changed: Query for QueryAvailability.
    MRA:9-MAY-2011. RV092.12. Buxfix. 550518
    - Main query was wrong! It did not show plants correct
      when they were from 'other plants'.
    - Changed query for: QueryAvailability.
    MRA:2-MAY-2013 TD-22429 Future Active Employee (REWORK)
    - Do not show the future active employee, when the STARTDATE of the
      employee is not yet within the selected week?
      - Changed: QueryAvailability
      - Added '(E.STARTDATE <= :ENDDATE) AND' at place ENDDATE is checked.
      - Added parameter ENDDATE.
    - Remark: The above is removed. Reason: When the dialog is opened and
              the employee is not active yet, it will already show the
              input as not-enabled.
    MRA:14-FEB-2014 20011800
    - Final Run System
    - Prevent it is changing availability based on last-export-date.
    - Database procedure STAFFAVAIL_QUERY has been changed.
    - Database Procedure STAFFAVAIL_FILLSTAV has been changed.
    MRA:9-FEB-2018 PIM-354
    - Memory leak in planning dialogs
    - The OnDestroy-event was not assigned!
    MRA:18-JUN-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning.
*)

(*
  Use of variables: (Since GLOB3-60)

  Front-end:
  - For each day there are 11 Tedit-fields, named Editd1 to Editd11, where
    d11 is used for shift (d = day-number) and d1 to d10 for avaiblable code.
    (this was previously d5, before GLOB3-60)
    Example: Edit11...Edit111, Edit21...Edit211, etc.
  - Columns in grid are named in a similar way (dxMasterGridColumnC).
    Example: dxMasterGridColumnC11...dxMasterGridColumnC111,
    dxMasterGridColumnC21...dxMasterGridColumnC211, etc.
  - The shift-field/column is shown at the first position, per day.

  Back-end:
  - QueryAvailability:
    - This uses names Dd1 to Dd10 for availability-code per timeblocks and day.
    - It uses names Dd11 for shift per day.
    - d = day-number
    - It uses calculated fields Dd1CALC to Dd11CALC for calculated values, where
      last one if used for shift.
*)

unit StaffAvailabilityDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, STDCtrls, Menus, Provider, DBClient, UPimsConst;

const
  Available = char('*');
  NotAvailable = char('-');
  MenuItemNotAvailable = char('_');
  Disable = ' ';
  ValidChars: set of Char = ['a'..'z','A'..'Z', Available, NotAvailable,
    #27, #8, #127];
  ValidShiftChars : set of Char = ['0'..'9','-',#27, #8, #127];

type
  TQueryStatus = (qsOpen, qsScroll);
  TAvValues = array[1..7,1..MAX_TBS+1] of String; // Last item is used for shift!
  TTBs = array [1..MAX_TBS] of String;
  TDailyAv = array [1..MAX_TBS+1] of String; // Last item is used for shift!
  TPlantArray = array[1..7] of String;
  TFilterAvb = Record
    Active: Boolean;
    //CAR 18-8-2003 add Insert_Avb - do not fill automatically staff availability
    // MR:17-02-2005 Order 550378 Team-From-To.
    TeamFrom, TeamTo, PlantCode, Insert_Avb: string;
    Year, Week: integer;
  end;

  TStaffAvailabilityDM = class(TGridBaseDM)
    QueryAvailability: TQuery;
    DataSourceAvailability: TDataSource;
    QueryWork: TQuery;
    QueryAvailabilityD11: TStringField;
    QueryAvailabilityD12: TStringField;
    QueryAvailabilityD13: TStringField;
    QueryAvailabilityD14: TStringField;
    QueryAvailabilityD15: TStringField;
    QueryAvailabilityD21: TStringField;
    QueryAvailabilityD22: TStringField;
    QueryAvailabilityD23: TStringField;
    QueryAvailabilityD24: TStringField;
    QueryAvailabilityD25: TStringField;
    QueryAvailabilityD31: TStringField;
    QueryAvailabilityD32: TStringField;
    QueryAvailabilityD33: TStringField;
    QueryAvailabilityD34: TStringField;
    QueryAvailabilityD35: TStringField;
    QueryAvailabilityD41: TStringField;
    QueryAvailabilityD42: TStringField;
    QueryAvailabilityD43: TStringField;
    QueryAvailabilityD44: TStringField;
    QueryAvailabilityD45: TStringField;
    QueryAvailabilityD51: TStringField;
    QueryAvailabilityD52: TStringField;
    QueryAvailabilityD53: TStringField;
    QueryAvailabilityD54: TStringField;
    QueryAvailabilityD55: TStringField;
    QueryAvailabilityD61: TStringField;
    QueryAvailabilityD62: TStringField;
    QueryAvailabilityD63: TStringField;
    QueryAvailabilityD64: TStringField;
    QueryAvailabilityD65: TStringField;
    QueryAvailabilityD71: TStringField;
    QueryAvailabilityD72: TStringField;
    QueryAvailabilityD73: TStringField;
    QueryAvailabilityD74: TStringField;
    QueryAvailabilityD75: TStringField;
    QueryAvailabilityEMPLOYEE_NUMBER: TIntegerField;
    QueryAvailabilityDESCRIPTION: TStringField;
    QueryWorkRestore: TQuery;
    QueryAvailabilityDEPARTMENT_CODE: TStringField;
    QueryAvailabilityRECNO: TFloatField;
    QueryAvailabilityD11CALC: TStringField;
    QueryAvailabilityD12Calc: TStringField;
    QueryAvailabilityD13Calc: TStringField;
    QueryAvailabilityD14Calc: TStringField;
    QueryAvailabilityD15CALC: TStringField;
    QueryAvailabilityD21CALC: TStringField;
    QueryAvailabilityD22CALC: TStringField;
    QueryAvailabilityD23CALC: TStringField;
    QueryAvailabilityD24CALC: TStringField;
    QueryAvailabilityD25CALC: TStringField;
    QueryAvailabilityD31CALC: TStringField;
    QueryAvailabilityD32CALC: TStringField;
    QueryAvailabilityD33CALC: TStringField;
    QueryAvailabilityD34CALC: TStringField;
    QueryAvailabilityD35CALC: TStringField;
    QueryAvailabilityD41CALC: TStringField;
    QueryAvailabilityD42CALC: TStringField;
    QueryAvailabilityD43CALC: TStringField;
    QueryAvailabilityD44CALC: TStringField;
    QueryAvailabilityD45CALC: TStringField;
    QueryAvailabilityD51CALC: TStringField;
    QueryAvailabilityD52CALC: TStringField;
    QueryAvailabilityD53CALC: TStringField;
    QueryAvailabilityD54CALC: TStringField;
    QueryAvailabilityD55CALC: TStringField;
    QueryAvailabilityD61CALC: TStringField;
    QueryAvailabilityD62CALC: TStringField;
    QueryAvailabilityD63CALC: TStringField;
    QueryAvailabilityD64CALC: TStringField;
    QueryAvailabilityD65CALC: TStringField;
    QueryAvailabilityD71CALC: TStringField;
    QueryAvailabilityD72CALC: TStringField;
    QueryAvailabilityD73CALC: TStringField;
    QueryAvailabilityD74CALC: TStringField;
    QueryAvailabilityD75CALC: TStringField;
    QueryAvailabilityFEMP: TFloatField;
    ClientDataSetAbsRsn: TClientDataSet;
    DataSetProviderAbsRsn: TDataSetProvider;
    QueryAbsReason: TQuery;
    QueryTeam: TQuery;
    QueryPlant: TQuery;
    QuerySelect: TQuery;
    QuerySelectPlanning: TQuery;
    QueryPlantTeam: TQuery;
    QueryShift: TQuery;
    ClientDataSetShift: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    QueryAvailabilityEMPSTARTDATE: TDateTimeField;
    QueryAvailabilityEMPENDDATE: TDateTimeField;
    QueryAvailabilityP1: TStringField;
    QueryAvailabilityP2: TStringField;
    QueryAvailabilityP3: TStringField;
    QueryAvailabilityP4: TStringField;
    QueryAvailabilityP5: TStringField;
    QueryAvailabilityP6: TStringField;
    QueryAvailabilityP7: TStringField;
    QueryAvailabilityP1CALC: TStringField;
    QueryAvailabilityP2CALC: TStringField;
    QueryAvailabilityP3CALC: TStringField;
    QueryAvailabilityP4CALC: TStringField;
    QueryAvailabilityP5CALC: TStringField;
    QueryAvailabilityP6CALC: TStringField;
    QueryAvailabilityP7CALC: TStringField;
    QueryAvailabilityEMPPLANT_CODE: TStringField;
    StoredProcAVAILABILITY: TStoredProc;
    QueryAvailabilityXXX: TQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    FloatField1: TFloatField;
    StringField38: TStringField;
    StringField39: TStringField;
    StringField40: TStringField;
    StringField41: TStringField;
    StringField42: TStringField;
    StringField43: TStringField;
    StringField44: TStringField;
    StringField45: TStringField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    StringField55: TStringField;
    StringField56: TStringField;
    StringField57: TStringField;
    StringField58: TStringField;
    StringField59: TStringField;
    StringField60: TStringField;
    StringField61: TStringField;
    StringField62: TStringField;
    StringField63: TStringField;
    StringField64: TStringField;
    StringField65: TStringField;
    StringField66: TStringField;
    StringField67: TStringField;
    StringField68: TStringField;
    StringField69: TStringField;
    StringField70: TStringField;
    StringField71: TStringField;
    StringField72: TStringField;
    FloatField2: TFloatField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    StringField73: TStringField;
    StringField74: TStringField;
    StringField75: TStringField;
    StringField76: TStringField;
    StringField77: TStringField;
    StringField78: TStringField;
    StringField79: TStringField;
    StringField80: TStringField;
    StringField81: TStringField;
    StringField82: TStringField;
    StringField83: TStringField;
    StringField84: TStringField;
    StringField85: TStringField;
    StringField86: TStringField;
    StringField87: TStringField;
    QueryAvailabilityXXX2: TQuery;
    IntegerField2: TIntegerField;
    StringField88: TStringField;
    StringField89: TStringField;
    StringField90: TStringField;
    StringField91: TStringField;
    StringField92: TStringField;
    StringField93: TStringField;
    StringField94: TStringField;
    StringField95: TStringField;
    StringField96: TStringField;
    StringField97: TStringField;
    StringField98: TStringField;
    StringField99: TStringField;
    StringField100: TStringField;
    StringField101: TStringField;
    StringField102: TStringField;
    StringField103: TStringField;
    StringField104: TStringField;
    StringField105: TStringField;
    StringField106: TStringField;
    StringField107: TStringField;
    StringField108: TStringField;
    StringField109: TStringField;
    StringField110: TStringField;
    StringField111: TStringField;
    StringField112: TStringField;
    StringField113: TStringField;
    StringField114: TStringField;
    StringField115: TStringField;
    StringField116: TStringField;
    StringField117: TStringField;
    StringField118: TStringField;
    StringField119: TStringField;
    StringField120: TStringField;
    StringField121: TStringField;
    StringField122: TStringField;
    StringField123: TStringField;
    StringField124: TStringField;
    FloatField3: TFloatField;
    StringField125: TStringField;
    StringField126: TStringField;
    StringField127: TStringField;
    StringField128: TStringField;
    StringField129: TStringField;
    StringField130: TStringField;
    StringField131: TStringField;
    StringField132: TStringField;
    StringField133: TStringField;
    StringField134: TStringField;
    StringField135: TStringField;
    StringField136: TStringField;
    StringField137: TStringField;
    StringField138: TStringField;
    StringField139: TStringField;
    StringField140: TStringField;
    StringField141: TStringField;
    StringField142: TStringField;
    StringField143: TStringField;
    StringField144: TStringField;
    StringField145: TStringField;
    StringField146: TStringField;
    StringField147: TStringField;
    StringField148: TStringField;
    StringField149: TStringField;
    StringField150: TStringField;
    StringField151: TStringField;
    StringField152: TStringField;
    StringField153: TStringField;
    StringField154: TStringField;
    StringField155: TStringField;
    StringField156: TStringField;
    StringField157: TStringField;
    StringField158: TStringField;
    StringField159: TStringField;
    FloatField4: TFloatField;
    DateTimeField3: TDateTimeField;
    DateTimeField4: TDateTimeField;
    StringField160: TStringField;
    StringField161: TStringField;
    StringField162: TStringField;
    StringField163: TStringField;
    StringField164: TStringField;
    StringField165: TStringField;
    StringField166: TStringField;
    StringField167: TStringField;
    StringField168: TStringField;
    StringField169: TStringField;
    StringField170: TStringField;
    StringField171: TStringField;
    StringField172: TStringField;
    StringField173: TStringField;
    StringField174: TStringField;
    QueryAvailabilityD16: TStringField;
    QueryAvailabilityD17: TStringField;
    QueryAvailabilityD18: TStringField;
    QueryAvailabilityD19: TStringField;
    QueryAvailabilityD110: TStringField;
    QueryAvailabilityD111: TStringField;
    QueryAvailabilityD26: TStringField;
    QueryAvailabilityD27: TStringField;
    QueryAvailabilityD28: TStringField;
    QueryAvailabilityD29: TStringField;
    QueryAvailabilityD210: TStringField;
    QueryAvailabilityD211: TStringField;
    QueryAvailabilityD36: TStringField;
    QueryAvailabilityD37: TStringField;
    QueryAvailabilityD38: TStringField;
    QueryAvailabilityD39: TStringField;
    QueryAvailabilityD310: TStringField;
    QueryAvailabilityD311: TStringField;
    QueryAvailabilityD46: TStringField;
    QueryAvailabilityD47: TStringField;
    QueryAvailabilityD48: TStringField;
    QueryAvailabilityD49: TStringField;
    QueryAvailabilityD410: TStringField;
    QueryAvailabilityD411: TStringField;
    QueryAvailabilityD56: TStringField;
    QueryAvailabilityD57: TStringField;
    QueryAvailabilityD58: TStringField;
    QueryAvailabilityD59: TStringField;
    QueryAvailabilityD510: TStringField;
    QueryAvailabilityD511: TStringField;
    QueryAvailabilityD66: TStringField;
    QueryAvailabilityD67: TStringField;
    QueryAvailabilityD68: TStringField;
    QueryAvailabilityD69: TStringField;
    QueryAvailabilityD610: TStringField;
    QueryAvailabilityD611: TStringField;
    QueryAvailabilityD76: TStringField;
    QueryAvailabilityD77: TStringField;
    QueryAvailabilityD78: TStringField;
    QueryAvailabilityD79: TStringField;
    QueryAvailabilityD710: TStringField;
    QueryAvailabilityD711: TStringField;
    QueryAvailabilityD16CALC: TStringField;
    QueryAvailabilityD17CALC: TStringField;
    QueryAvailabilityD18CALC: TStringField;
    QueryAvailabilityD19CALC: TStringField;
    QueryAvailabilityD110CALC: TStringField;
    QueryAvailabilityD111CALC: TStringField;
    QueryAvailabilityD26CALC: TStringField;
    QueryAvailabilityD27CALC: TStringField;
    QueryAvailabilityD28CALC: TStringField;
    QueryAvailabilityD29CALC: TStringField;
    QueryAvailabilityD210CALC: TStringField;
    QueryAvailabilityD211CALC: TStringField;
    QueryAvailabilityD36CALC: TStringField;
    QueryAvailabilityD37CALC: TStringField;
    QueryAvailabilityD38CALC: TStringField;
    QueryAvailabilityD39CALC: TStringField;
    QueryAvailabilityD310CALC: TStringField;
    QueryAvailabilityD311CALC: TStringField;
    QueryAvailabilityD46CALC: TStringField;
    QueryAvailabilityD47CALC: TStringField;
    QueryAvailabilityD48CALC: TStringField;
    QueryAvailabilityD49CALC: TStringField;
    QueryAvailabilityD410CALC: TStringField;
    QueryAvailabilityD411CALC: TStringField;
    QueryAvailabilityD56CALC: TStringField;
    QueryAvailabilityD57CALC: TStringField;
    QueryAvailabilityD58CALC: TStringField;
    QueryAvailabilityD59CALC: TStringField;
    QueryAvailabilityD510CALC: TStringField;
    QueryAvailabilityD511CALC: TStringField;
    QueryAvailabilityD66CALC: TStringField;
    QueryAvailabilityD67CALC: TStringField;
    QueryAvailabilityD68CALC: TStringField;
    QueryAvailabilityD69CALC: TStringField;
    QueryAvailabilityD610CALC: TStringField;
    QueryAvailabilityD611CALC: TStringField;
    QueryAvailabilityD76CALC: TStringField;
    QueryAvailabilityD77CALC: TStringField;
    QueryAvailabilityD78CALC: TStringField;
    QueryAvailabilityD79CALC: TStringField;
    QueryAvailabilityD710CALC: TStringField;
    QueryAvailabilityD711CALC: TStringField;
    procedure PopulateAvailability(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryAvailabilityCalcFields(DataSet: TDataSet);
    procedure QueryAvailabilityAfterScroll(DataSet: TDataSet);
    procedure QueryPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    CurrentAvailability: TAvValues;
    CurrentPlantArray: TPlantArray;

    FFilter: TFilterAvb;
    FPlanInOtherPlants: Boolean;
    procedure SetFilterData(Value: TFilterAvb);
  public
    { Public declarations }
    FStartDate, FEndDate: TDateTime;
    FEmpl, FWeek, FYear: Integer;
    FPlant: String;
    FListSave: TStringList;
    FListTBEmpl: TStringList;
    WeekDays: array[1..7] of TDateTime;
    //CAR 550276
    FDayChanged: Array[1..7] of Integer;
    QueryStatus: TQueryStatus;
    RefreshAfterSave: Boolean;
    property DataFilter: TFilterAvb read FFilter write SetFilterData;
    function SaveChanges(WasChanged: Boolean;var EmpNo: Integer): Integer;
    // This function try to save a query line
    // and return
    // 0 if any error
    // 1 if record saved
    procedure GetChangedValues(var NewAvValues: TAvValues;
      var NewPlantArray: TPlantArray);
    procedure GetOldValues(var OldAvValues: TAvValues;
      var OldPlantArray: TPlantArray);
    function  CheckForPlanning(OldPlantCode, NewPlantCode: String;
      Day, TB, ShiftNoOld, ShiftNoNew,
      EmpNo: Integer; ValuesToSave: TAvValues): Boolean;
    // check if exist TBs with value A, B or C in table EMPLOYEEPLANNING
    // on WeekDays[Day] and ShiftNo
    // if TB=0 then function return true if any of 1..4 TB has A, B or C values
    procedure UpdateStdAvailability(OldPlantCode, NewPlantCode: String;
      WasChanged: Boolean; WeekDay, EmpNo, NewSHNo, OldSHNo: integer;
      TBs: TTBs; ValuesToSave: TAvValues; ChangePlanning: Boolean);

    procedure RestoreDefault(
      var DefaultAv: TDailyAv; EmployeeNumber, Day: Integer;
        DepartmentCode: String);
    procedure GotoRecord(RecNo: Double);

    procedure InitializeDayChangedArray;
    function FormatPlantShift(PlantCode, ShiftNumber: String): String;
    function DataFilterPlantCode: String;
    procedure BuildShiftPopupMenu;
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write FPlanInOtherPlants;
  end;

var
  StaffAvailabilityDM: TStaffAvailabilityDM;

implementation

uses UGlobalFunctions, ListProcsFRM, StaffAvailabilityFRM,
  SystemDMT, UPimsMessageRes, CalculateTotalHoursDMT;

{$R *.DFM}

{ TStaffAvailabilityDM }

function TStaffAvailabilityDM.FormatPlantShift(
  PlantCode, ShiftNumber: String): String;
begin
  Result := Format('%s - %s', [PlantCode, ShiftNumber]);
end;

procedure TStaffAvailabilityDM.SetFilterData(Value: TFilterAvb);
var
  Index: Integer;
begin
  FFilter := Value;
  if not Value.Active then
    Exit;

  FListTBEmpl.Clear;
  FListSave.Clear;
  WeekDays[1] := ListProcsF.DateFromWeek(Value.Year, value.Week, 1);
  FYear := Value.Year;
  FWeek := Value.Week;
  for Index := 2 to 7 do
    WeekDays[Index] := WeekDays[1] + Index - 1;
  FStartDate := WeekDays[1];
  FEndDate := WeekDays[7];
  FPlant := Value.PlantCode;

  //car 550274 -team selection
  // Pims -> Oracle
  // Check for ('INSERT_STAV')
  // for Oracle insert and select are split
  if (Value.Insert_Avb = 'Y') then
  begin
    with StoredProcAVAILABILITY do
    begin
      if Value.PlantCode <> ALLPLANTS then
        ParamByName('EMP_PLANT_CODE').AsString :=
          Value.PlantCode
      else
        ParamByName('EMP_PLANT_CODE').AsString := '*';
      // PlantCode-selection for availability
      ParamByName('PLANT_CODE').AsString := '*';  //???

      // MR:17-02-2005 Order 550378 Team-From-To.
      if Value.TeamFrom <> ALLTEAMS then
      begin
        ParamByName('TEAMFROM').AsString := Value.TeamFrom;
        ParamByName('TEAMTO').AsString := Value.TeamTo;
      end
      else
      begin
        ParamByName('TEAMFROM').AsString := '*';
        ParamByName('TEAMTO').AsString := '*';
      end;
      ParamByName('STARTDATE').asDateTime := WeekDays[1];
      ParamByName('CREATIONDATE').asdateTime := Now;
      ParamByName('MUTATOR').asString :=
        SystemDM.CurrentProgramUser;
      ParamByName('INSERT_STAV').asString :=  Value.Insert_Avb;
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        ParamByName('USER_NAME').AsString :=
          SystemDM.UserTeamLoginUser
      else
        ParamByName('USER_NAME').AsString := '*';
{
WDebugLog('StoredProcAVAILABILITY');
WDebugLog('EMP_PLANT_CODE=' + ParamByName('EMP_PLANT_CODE').AsString +
' PLANT_CODE=' + ParamByName('PLANT_CODE').AsString +
' TEAMFROM=' + ParamByName('TEAMFROM').AsString +
' TEAMTO=' + ParamByName('TEAMTO').AsString +
' STARTDATE=' + DateTimeToStr(ParamByName('STARTDATE').asDateTime) +
' INSERT_STAV=' + ParamByName('INSERT_STAV').asString
  );
}
      if not Prepared then
        Prepare;
      ExecProc;
      // 20011800 Get status that indicates if availability could not be changed because of final-run.
      try
        if SystemDM.UseFinalRun then
          if ParamByName('STATUS').AsInteger > 0 then
            DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]);
      except
      end;
    end; // with
  end; // if

  QueryAvailability.Active := False;
  // PlantCode-selection for Employee
  if Value.PlantCode <> ALLPLANTS then
    QueryAvailability.ParamByName('EMP_PLANT_CODE').AsString :=
      Value.PlantCode
  else
    QueryAvailability.ParamByName('EMP_PLANT_CODE').AsString := '*';
  // PlantCode-selection for availability
  QueryAvailability.ParamByName('PLANT_CODE').AsString := '*';  //???
  // MR:17-02-2005 Order 550378 Team-From-To.
  if Value.TeamFrom <> ALLTEAMS then
  begin
    QueryAvailability.ParamByName('TEAMFROM').asString := Value.TeamFrom;
    QueryAvailability.ParamByName('TEAMTO').asString := Value.TeamTo;
  end
  else
  begin
    QueryAvailability.ParamByName('TEAMFROM').asString := '*';
    QueryAvailability.ParamByName('TEAMTO').asString := '*';
  end;
  QueryAvailability.ParamByName('STARTDATE').asDateTime := WeekDays[1];
//  QueryAvailability.ParamByName('ENDDATE').asDateTime := WeekDays[7]; // TD-22429
  QueryAvailability.ParamByName('CREATIONDATE').asdateTime := Now;
  QueryAvailability.ParamByName('MUTATOR').asString :=
    SystemDM.CurrentProgramUser;
  QueryAvailability.ParamByName('INSERT_STAV').asString :=  Value.Insert_Avb;
   //car 550274 -team selection
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryAvailability.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser
  else
    QueryAvailability.ParamByName('USER_NAME').AsString := '*';
  if not QueryAvailability.Prepared then
    QueryAvailability.Prepare;

  QueryStatus := qsOpen;
  QueryAvailability.Active := True;
end;

// MR:26-11-2004 Because all-plants can also be selected,
// so shift-popup-menu must be created during right-click.
procedure TStaffAvailabilityDM.BuildShiftPopupMenu;
var
  Index: Integer;
  NewMenuItem: TMenuItem;
begin
  for Index := 0 to
    (TStaffAvailabilityF(Owner).PopupMenuShift.Items.Count - 1) do
    TStaffAvailabilityF(Owner).PopupMenuShift.Items.Clear;

  NewMenuItem := TMenuItem.Create(Self);
  NewMenuItem.Caption := '_';
  NewMenuItem.OnClick := TStaffAvailabilityF(Owner).ShiftPopupSetValue;
  TStaffAvailabilityF(Owner).PopupMenuShift.Items.Add(NewMenuItem);
  //car 18-8-2003 - not necessary  for reseting filter
  if (DataFilter.PlantCode > nullstr) then
  begin
    if PlanInOtherPlants then
      ClientDataSetShift.Filter := ''
    else
      ClientDataSetShift.Filter := Format('PLANT_CODE = ''%s''',
        [DoubleQuote(DataFilterPlantCode)]);

    ClientDataSetShift.First;
    while not ClientDataSetShift.Eof do
    begin
      NewMenuItem := TMenuItem.Create(Self);
      NewMenuItem.OnClick := TStaffAvailabilityF(Owner).ShiftPopupSetValue;
      if PlanInOtherPlants then
        NewMenuItem.Caption := FormatPlantShift(
          ClientDataSetShift.FieldByName('PLANT_CODE').asString,
          ClientDataSetShift.FieldByName('SHIFT_NUMBER').asString)
      else
        NewMenuItem.Caption := Format('%s',
          [ClientDataSetShift.FieldByName('SHIFT_NUMBER').asString]);
      TStaffAvailabilityF(Owner).PopupMenuShift.Items.Add(NewMenuItem);
      ClientDataSetShift.Next;
    end;
  end;
  ClientDataSetShift.Filter := '';
end;

procedure TStaffAvailabilityDM.PopulateAvailability(DataSet: TDataSet);
var
  Day, Index: integer;
  DMOwner: TStaffAvailabilityF;
  ACmp: Tcomponent;
  PlantCode, EmpPlantCode: String;
  PlantCmp: TComponent;
  SamePlant: Boolean;
begin
  inherited;
  DMOwner := TStaffAvailabilityF(Owner);
  // Plantcode from employee.
  EmpPlantCode := DataSet.FieldByName('EMPPLANT_CODE').AsString;

  for Day := 1 to 7 do
  begin
    for Index := 1 to SystemDM.MaxTimeblocks do
      CurrentAvailability[Day, Index] := Disable;
    if True {PlanInOtherPlants} then // always show color for other plant
      CurrentPlantArray[Day] := '';
    // Shift is stored here
    CurrentAvailability[Day, ISHIFT] := Disable;
  end;
{
  for Index := 0 to DMOwner.ComponentCount - 1 do
  begin
    ACmp := DMOwner.Components[index];
    if (ACmp is TEdit) and (TEdit(ACmp).Tag = 0) then
      TEdit(ACmp).Text := Disable;
  end;
}
  for Day := 1 to 7 do
  begin
    for Index := 1 to SystemDM.MaxTimeblocks do
    begin
      ACmp := DMOwner.FindComponent('Edit'+IntToStr(Day)+IntToStr(Index));
      if Assigned(ACmp) then
        if (ACmp is TEdit) then
          TEdit(ACmp).Text := Disable;
    end;
    ACmp := DMOwner.FindComponent('Edit'+IntToStr(Day)+IntToStr(ISHIFT));
    if Assigned(ACmp) then
      if (ACmp is TEdit) then
        TEdit(ACmp).Text := Disable;
  end;
  if not(DataSet.Active and (not DataSet.isEmpty)) then
    Exit;

  for Day := 1 to 7 do
  begin
  //CAR 16-7-2003
  //CAR 550276
    SamePlant := True;
    if True {PlanInOtherPlants} then // always show color for other plant
    begin
      CurrentPlantArray[Day] :=
        DataSet.FieldByName(Format('P%dCALC', [Day])).AsString;
      PlantCode := CurrentPlantArray[Day];
      PlantCmp := DMOwner.FindComponent(Format('EditP%d', [Day]));
      if Assigned(PlantCmp) then
        if (PlantCmp is TEdit) then
        begin
          TEdit(PlantCmp).Text := PlantCode;
          if TEdit(PlantCmp).Text <> '' then
            SamePlant :=
              (TEdit(PlantCmp).Text = EmpPlantCode);
        end;
    end
    else
      PlantCode := DataFilter.PlantCode;

    if (DataSet.FieldByName(
        Format('D%d%dCALC',[Day,ISHIFT])).AsString <> '-') and
      (DataSet.FieldByName(
        Format('D%d%dCALC',[Day,ISHIFT])).AsString <> '') then
      ATBLengthClass.ValidTB(
        DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        DataSet.FieldByName(Format('D%d%dCALC',[Day,ISHIFT])).AsInteger,
        PlantCode,
        DataSet.FieldByName('DEPARTMENT_CODE').AsString);

    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        CurrentAvailability[Day, ISHIFT] :=
          DataSet.FieldByName(Format('D%d%dCALC',[Day,ISHIFT])).AsString
      else
      begin
      // CAR 21-7-2003
        if ATBLengthClass.ExistTB(Index) then
          CurrentAvailability[Day, Index] :=
            CharFromStr(DataSet.FieldByName(
              Format('D%d%dCALC',[Day,index])).AsString, 1, ' ')
        else
          CurrentAvailability[Day, Index] := '';
      end;
      if Index = 0 then
        ACmp := DMOwner.FindComponent(Format('Edit%d%d',[Day, ISHIFT]))
      else
        ACmp := DMOwner.FindComponent(Format('Edit%d%d',[Day, Index]));
      if Assigned(ACmp) then
        if (ACmp is TEdit) then
        begin
          if Index = 0 then
            TEdit(ACmp).Text := CurrentAvailability[Day, ISHIFT]
          else
            TEdit(ACmp).Text := CurrentAvailability[Day, Index];
          if (Index > 0) then
          begin
          //CAR 550276 - shift = ''
            if (not ATBLengthClass.ExistTB(Index)) or
              (DataSet.FieldByName(
                Format('D%d%dCALC',[Day,ISHIFT])).AsString = '') or
              (DataSet.FieldByName(
                Format('D%d%dCALC',[Day,ISHIFT])).AsString = '-')
               then
            begin
              TEdit(ACmp).Color := clBtnFace;
              TEdit(ACmp).Enabled := False;
              TEdit(ACmp).Visible := False;
            end
            else
            begin
              TEdit(ACmp).Enabled := True;
              TEdit(ACmp).Color := clRequired;
              TEdit(ACmp).Visible := True;
            end;
          end
          else
          begin
            if (not SamePlant) and (TEdit(ACmp).Text <> '') then
              TEdit(ACmp).Color := clOtherPlant
            else
              TEdit(ACmp).Color := clRequired;
          end;
        end; // if
    end; // for Index
  end; // for Day
end; // PopulateAvailability

function TStaffAvailabilityDM.CheckForPlanning(
  OldPlantCode, NewPlantCode: String;
  Day, TB, ShiftNoOld, ShiftNoNew, EmpNo: Integer;
  ValuesToSave: TAvValues): Boolean;
var
  Index: Integer;
  ShiftNoPlanned: Integer;
  SelectStr, WhereStr: String;
  function CheckIfDeletePerTB(Index: Integer): Boolean;
  var
    SelectStr: String;
  begin
    Result := False;
    //car 550276
    if ShiftNoNew <= -1 then
    //delete employee availability
    begin
      Result := True;
      Exit;
    end;
    // checks for update and insert an avail with:  ShiftNoNew >= 0
    if (ShiftNoNew <> ShiftNoPlanned) and (ShiftNoNew <> 0) then
    begin
      Result := True;
      Exit;
    end;
    if (ShiftNoNew = ShiftNoPlanned) and (ShiftNoNew <> 0) then
    begin
      if ValuesToSave[Day, Index] <> Available then
        Result := True;
      Exit;
    end;
    if (ShiftNoNew = 0) then
    begin
      if ValuesToSave[Day, Index] <> Available then
      begin
        Result := True;
        Exit;
      end;
      SelectStr := 'SELECT AVAILABLE_TIMEBLOCK_' + IntToStr(Index) +
        ' FROM STANDARDAVAILABILITY WHERE ' +
        ' SHIFT_NUMBER = :SHIFT AND EMPLOYEE_NUMBER = :EMPL ' +
        ' AND DAY_OF_WEEK = :DAY ' +
        ' AND AVAILABLE_TIMEBLOCK_' + IntToStr(Index) + ' = ''*''';
      QuerySelect.Close;
      QuerySelect.Sql.Clear;
      QuerySelect.Sql.Add(SelectStr);
      QuerySelect.ParamByName('EMPL').AsInteger := EmpNo;
      QuerySelect.ParamByName('SHIFT').AsInteger := ShiftNoPlanned;
      QuerySelect.ParamByName('DAY').AsInteger := Day;
      QuerySelect.Open;
      Result := QuerySelect.IsEmpty;
    end;
  end;
begin
  Result := False;
  QueryWork.Active := False;
//car 25-8-2003
  ShiftNoPlanned := ShiftNoOld;
  SelectStr :=
    'SELECT SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2, ' +
    'SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4, ' +
    'SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6, ' +
    'SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8, ' +
    'SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10, ' +
    'SHIFT_NUMBER ' +
    'FROM EMPLOYEEPLANNING';
  WhereStr :=
    '( SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_5 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_6 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_7 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_8 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_9 IN (''A'',''B'',''C'') OR ' +
    ' SCHEDULED_TIMEBLOCK_10 IN (''A'',''B'',''C'')) ';
  if (ShiftNoOld <= 0) then
  begin
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(SelectStr);
    QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE');
    QueryWork.SQL.Add('AND EMPLOYEEPLANNING_DATE=:PDATE');
    QueryWork.SQL.Add('AND EMPLOYEE_NUMBER=:EMPNO AND ');
    QueryWork.SQL.Add(WhereStr);
    QueryWork.Prepare;
    QueryWork.ParamByName('PCODE').AsString := OldPlantCode;
    QueryWork.ParamByName('PDATE').AsDateTime := WeekDays[Day];
    QueryWork.ParamByName('EMPNO').AsInteger := EmpNo;
    QueryWork.Active := True;
    ShiftNoPlanned := QueryWork.FieldByName('SHIFT_NUMBER').AsInteger;
  end
  else
  begin
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(SelectStr);
    QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE');
    QueryWork.SQL.Add('AND EMPLOYEEPLANNING_DATE=:PDATE');
    QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO AND EMPLOYEE_NUMBER=:EMPNO AND');
    QueryWork.SQL.Add(WhereStr);
    QueryWork.Prepare;
    QueryWork.ParamByName('PCODE').AsString := OldPlantCode;
    QueryWork.ParamByName('PDATE').AsDateTime := WeekDays[Day];
    QueryWork.ParamByName('SHNO').AsInteger := ShiftNoOld;
    QueryWork.ParamByName('EMPNO').AsInteger := EmpNo;
    QueryWork.Active := True;
  end;
  while not QueryWork.Eof do
  begin
    ShiftNoPlanned := QueryWork.FieldByName('SHIFT_NUMBER').AsInteger;
    if TB > 0 then
    begin
      if CharFromStr(QueryWork.FieldByName(
        Format('SCHEDULED_TIMEBLOCK_%d',[TB])).asString,1,' ') in
          ['A', 'B', 'C'] then
      begin
        Result := CheckIfDeletePerTB(TB);
      end;
    end
    else
      for index := 1 to SystemDM.MaxTimeblocks do
        if CharFromStr(QueryWork.FieldByName(
     	  Format('SCHEDULED_TIMEBLOCK_%d',[index])).asString,1,' ') in
     	  ['A', 'B', 'C'] then
        begin
          Result := CheckIfDeletePerTB(index);
          if Result then
          begin
            QueryWork.Active := False;
            Exit;
          end;
        end;
    QueryWork.Next;
  end;
  QueryWork.Active := False;
end;

function TStaffAvailabilityDM.SaveChanges(WasChanged: Boolean;
  var EmpNo: Integer): Integer;
var
  ValuesToSave: TAvValues;
  OldSHNo, NewSHNo, Day, Index, IndexList, i: integer;
  DayAv: TTBs;
  ChangePlanning: boolean;
  StrList, DepartmentCode: String;
  TBS: TTimeBlock;
  PlantArrayToSave: TPlantArray;
  OldPlantCode, NewPlantCode, PlantCode: String;
  StrValue: String;
begin
  Result := 0;
  ChangePlanning := False;
  if (not QueryAvailability.Active) or QueryAvailability.IsEmpty then
    Exit;
  EmpNo := QueryAvailability.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  DepartmentCode := QueryAvailability.FieldByName('DEPARTMENT_CODE').AsString;
  if WasChanged then
    GetChangedValues(ValuesToSave, PlantArrayToSave)
  else
    GetOldValues(ValuesToSave, PlantArrayToSave);
  for Day := 1 to 7 do
  begin
   // Shift number - Check shiftno
//CAR 550276
    NewSHNO := -1;  OldSHNo := -1;
    NewPlantCode := DataFilterPlantCode;
    OldPlantCode := DataFilterPlantCode;
    if ValuesToSave[Day, ISHIFT] = NotAvailable then
      NewSHNo := -1;   //value for  '-'
    if ValuesToSave[Day, ISHIFT] = Disable then
       NewSHNo := -2; //value for  ''
    if CurrentAvailability[Day, ISHIFT] = NotAvailable then
      OldSHNo := -1;
    if CurrentAvailability[Day, ISHIFT] = Disable then
      OldSHNo := -2;
//
//CAR 550276 - CHECK IF Timeblocks of days changed are all filled
    if (FDayChanged[Day] = 1) and
      (ValuesToSave[Day, ISHIFT] > '0') then
    begin
      PlantCode := PlantArrayToSave[Day];
      WhatTimeBlock(QueryWork, EmpNo, StrToInt(ValuesToSave[Day,ISHIFT]),
        PlantCode, DepartmentCode, TBs);
      for i := 1 to SystemDM.MaxTimeblocks do
        if ((ValuesToSave[Day, i] = '') or (ValuesToSave[Day, i] = ' ')) and
          TBS[i] then
        begin
          DisplayMessage(Format(SInvalidAvailability,[Day, i]),
            mtInformation, [mbok]);
          Exit;
        end;
      for i := 1 to SystemDM.MaxTimeblocks do
        if TBS[i] and
          (ValuesToSave[Day, i] <> '*') and (ValuesToSave[Day, i] <> '-') and
// RV057.2. Use Locate instead of Findkey, sorting was changed to DESCRIPTION
//          (not ClientDataSetAbsRsn.FindKey([ValuesToSave[Day, i]])) then
            (not ClientDataSetAbsRsn.Locate('ABSENCEREASON_CODE',
              ValuesToSave[Day, i], [])) then
        begin
          DisplayMessage(Format(SInvalidAvailability,[Day, i]),
            mtInformation,[mbok]);
          Exit;
        end;
    end;
    if ValuesToSave[Day, ISHIFT] > NotAvailable then
    begin
      PlantCode := PlantArrayToSave[Day];
      NewPlantCode := PlantCode;
      try
        NewSHNo := StrToInt(ValuesToSave[Day, ISHIFT]);
        ClientDataSetShift.Filter := '';
        if not ClientDataSetShift.Locate('PLANT_CODE;SHIFT_NUMBER',
          VarArrayOf([PlantCode, ValuesToSave[Day, ISHIFT]]),
            [loCaseInsensitive]) then
        begin
//          raise Exception.CreateFmt(SInvalidShiftNo,[Day]);
          NewPlantCode := DataFilterPlantCode;
          if ValuesToSave[Day, ISHIFT] = NotAvailable then
            NewSHNo := -1;   //value for  '-'
          if ValuesToSave[Day, ISHIFT] = Disable then
            NewSHNo := -2; //value for  ''
        end;
      except
        //car 550276
        NewPlantCode := DataFilterPlantCode;
        if ValuesToSave[Day, ISHIFT] = NotAvailable then
          NewSHNo := -1;   //value for  '-'
        if ValuesToSave[Day, ISHIFT] = Disable then
          NewSHNo := -2; //value for  ''
      end;
    end;
    if CurrentAvailability[Day, ISHIFT] > NotAvailable then
    try
      if WasChanged then
      begin
        OldSHNo := StrToInt(CurrentAvailability[Day, ISHIFT]);
        // MR:29-11-2004 Old plant could be another plant than
        // the employee's plant!
//        if PlanInOtherPlants then
        OldPlantCode := CurrentPlantArray[Day]
//        else
//          OldPlantCode := DataFilterPlantCode;
      end;
    except
      //car 550276
      if CurrentAvailability[Day, ISHIFT] = NotAvailable then
        OldSHNo := -1;
      if CurrentAvailability[Day, ISHIFT] = Disable then
        OldSHNo := -2;
    end;
    // check if shift changed
    // only for save one Query line !!
    if WasChanged and (not ChangePlanning) and
      (not ((NewSHNo = OldSHNo) and (NewPlantCode = OldPlantCode))) and
      CheckForPlanning(OldPlantCode, NewPlantCode, Day, 0,
        OldShNo, NewSHNo, EmpNo, ValuesToSave) then
    begin
      if DisplayMessage(SChangePlaned, mtInformation,[mbYes,mbNo])= mrNo then
        Exit
      else
        ChangePlanning := True;
    end;
    //car 5500276
    if not((OldSHNo = NewSHNo) and (OldSHNo <= -1)) then
    begin
      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        DayAv[Index] := ValuesToSave[Day, Index];
        // MR:02-01-2003 Use Locate because of
        // key-change to ABSENCEREASON-table.
        if DayAv[Index] <> '' then // GLOB3-60 Be sure it is not empty!
        begin
          if not (DayAv[Index][1] in [Available,NotAvailable,Disable]) and
            (not ClientDataSetAbsRsn.Locate('ABSENCEREASON_CODE',
            VarArrayOf([DayAv[Index]]), [])) then
          begin
            DisplayMessage(Format(SInvalidAvailability,[Day, Index]),
              mtInformation,[mbOk]);
            Exit;
          end;
        end;
        // check Availability no was changed
        // only for save one Query line !!
        if WasChanged and {(CurrentAvailability[day,index] = Available) and
          (DayAv[index] <> Available) and} (not ChangePlanning) and
          CheckForPlanning(OldPlantCode, NewPlantCode, Day, Index,
            OldShNo, NewSHNo, EmpNo, ValuesToSave) then
        begin
          if DisplayMessage(SChangePlaned,
            mtInformation, [mbYes, mbNo])= mrNo then
            Exit
          else
            ChangePlanning := True;
        end;
      end;
      if WasChanged then
        UpdateStdAvailability(OldPlantCode, NewPlantCode,
          WasChanged,day,EmpNo,NewSHNo,OldSHNo,DayAv,
          ValuesToSave, ChangePlanning);
    end;
  end;
  Result := 1;
  if WasChanged then
  begin
  // USE a saving list for updating only the changed values
  // Pims -> Oracle
  IndexList :=
    FListSave.IndexOf(QueryAvailability.FieldByName('RECNO').AsString);
//    IndexList := FListSave.IndexOf(QueryAvailability.FieldByName('RECNO').AsString);
//    IndexList := FListSave.IndexOf(
//      QueryAvailability.FieldByName('RECNO').AsString);
    StrList := '';
    for Day := 1 to 7 do
    begin
      // At the start, store the plant, always 6 positions long
      StrList := StrList + Format('%-6s', [PlantArrayToSave[Day]]);
      for Index := 0 to SystemDM.MaxTimeblocks do
      begin
        if Index > 0 then
        begin
          if ValuesToSave[Day, Index] = '' then
            StrValue := ' '
          else
            StrValue := ValuesToSave[Day, Index];
        end
        else
        begin
          if ValuesToSave[Day, ISHIFT] = '' then
            StrValue := ' '
          else
            StrValue := ValuesToSave[Day, ISHIFT];
        end;
        if Index > 0 then
          StrList := StrList + StrValue
        else
          // Use 2 positions for 'shift'.
          StrList := StrList + Format('%-2s', [StrValue]);
      end; // for Index
    end; // for Day
    if IndexList >= 0 then
    begin
      FListSave.Delete(IndexList + 1);
      FListSave.Insert(IndexList + 1, StrList);
    end
    else
    begin
      // Pims -> Oracle
      FListSave.Add(QueryAvailability.FieldByName('RECNO').AsString);
//        FListSave.Add(QueryAvailability.FieldByName('RECNO').AsString);
//      FListSave.Add(QueryAvailability.FieldByName('RECNO').AsString);
      FListSave.Add(StrList);
    end;
  end;
end;

procedure TStaffAvailabilityDM.GetChangedValues(
  var NewAvValues: TAvVAlues;
  var NewPlantArray: TPlantArray);
var
  ACmp: TComponent;
  Day, Index: Integer;
  OwnerFRM: TStaffAvailabilityF;
  PlantCmp: TComponent;
begin
  for Day := 1 to 7 do
  begin
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        NewAvValues[Day, ISHIFT] := Disable
      else
        NewAvValues[Day, Index] := Disable;
    end;
    NewPlantArray[Day] := Disable;
  end;
  if not QueryAvailability.Active then
    Exit;
  OwnerFRM := TStaffAvailabilityF(Owner);
  for Day := 1 to 7 do
  begin
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        ACmp := OwnerFRM.FindComponent(Format('Edit%d%d', [Day, ISHIFT]))
      else
        ACmp := OwnerFRM.FindComponent(Format('Edit%d%d', [Day, Index]));
      if Assigned(ACmp) then
        if (ACmp is TEdit) then
        begin
          if Index = 0 then
          begin
            if Length(TEdit(ACmp).Text) < 1 then
              NewAvValues[Day, ISHIFT] := Disable
            else
              NewAvValues[Day, ISHIFT] := TEdit(ACmp).text;
          end
          else
          begin
            if Length(TEdit(ACmp).Text) < 1 then
              NewAvValues[Day, Index] := Disable
            else
              NewAvValues[Day, Index] := TEdit(ACmp).text;
          end;
        end;
    end; // for Index
    PlantCmp := OwnerFRM.FindComponent(Format('EditP%d', [Day]));
    if Assigned(PlantCmp) then
      if (PlantCmp is TEdit) then
        NewPlantArray[Day] := TEdit(PlantCmp).Text;
  end; // for Day
end; // GetChangedValues

procedure TStaffAvailabilityDM.GetOldValues(var OldAvValues: TAvValues;
  var OldPlantArray: TPlantArray);
var
  Day, Index: integer;
begin
  for Day := 1 to 7 do
  begin
    for Index := 1 to SystemDM.MaxTimeblocks do
      OldAvValues[Day, Index] := Disable;
    OldAvValues[Day, ISHIFT] := Disable;
    OldPlantArray[Day] := Disable;
  end;
  if (not QueryAvailability.Active) or (QueryAvailability.IsEmpty) then
    Exit;
  for Day := 1 to 7 do
  begin
    for Index := 1 to SystemDM.MaxTimeblocks do
      OldAvValues[Day, Index] :=
        QueryAvailability.FieldByName(
          Format('D%d%dCALC',[Day, Index])).AsString;
    OldAvValues[Day, ISHIFT] :=
      QueryAvailability.FieldByName(
        Format('D%d%dCALC',[Day, ISHIFT])).AsString;
    OldPlantArray[Day] :=
      QueryAvailability.FieldByName(Format('P%dCALC',[Day])).AsString;
  end;
end;

procedure TStaffAvailabilityDM.UpdateStdAvailability(
  OldPlantCode, NewPlantCode: String;
  WasChanged: Boolean;
  WeekDay, EmpNo, NewSHNo, OldSHNo: integer; TBs: TTBs;
  ValuesToSave: TAvValues; ChangePlanning: Boolean);
var
  Index, CountNotAvailable: integer;
  Temp,TBAvailability: string;
  DateCurrent: TDateTime;
  PlannedShift: Integer;
  InsertRecord: Boolean;
  PlantCode: String;
begin
  //CAR 18-8-2003
  DateCurrent := Now;
  //Update Shift Schedule
  if WasChanged and
    (not ((NewSHNo = OldSHNo) and (OldPlantCode = NewPlantCode))) then
  begin
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add('DELETE FROM SHIFTSCHEDULE ');
    QueryWork.SQL.Add('WHERE (EMPLOYEE_NUMBER = :EMPNO) AND ');
    QueryWork.SQL.Add('(SHIFT_SCHEDULE_DATE = :SDATE) ');
    QueryWork.Prepare;
    QueryWork.ParamByName('EMPNO').asInteger := EmpNo;
    QueryWork.ParamByName('SDATE').asDateTime := WeekDays[WeekDay];
    QueryWork.ExecSQL;
    //car 550276  - insert new shift =  -1 in Shift Schedule
    if (NewShNo > -2) and (EmpNo > 0) then
    begin
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add('INSERT INTO SHIFTSCHEDULE(EMPLOYEE_NUMBER,');
      QueryWork.SQL.Add('SHIFT_SCHEDULE_DATE,PLANT_CODE,SHIFT_NUMBER, ');
      QueryWork.SQL.Add('CREATIONDATE,MUTATIONDATE,MUTATOR) VALUES(');
      QueryWork.SQL.Add(':EMPNO,:SDATE,:PCODE,:SHNO,:CDATE,:MDATE,:MUT)');
      QueryWork.Prepare;
      QueryWork.ParamByName('EMPNO').AsInteger := EmpNo;
      QueryWork.ParamByName('SDATE').AsDateTime := WeekDays[WeekDay];
      QueryWork.ParamByName('PCODE').AsString := NewPlantCode;
      QueryWork.ParamByName('SHNO').AsInteger := NewSHNo;
      QueryWork.ParamByName('CDATE').AsDateTime := DateCurrent;
      QueryWork.ParamByName('MDATE').AsDateTime := DateCurrent;
      QueryWork.ParamByName('MUT').AsString := SystemDM.CurrentProgramUser;
      QueryWork.ExecSQL;
    end;
  end;
  // EMPLOYEEAVAILABILITY
  // MR:24-11-2004 Delete 'Employeeavailability'-record without filtering
  // on Plant.
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add('DELETE FROM EMPLOYEEAVAILABILITY ');
//  QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE AND ');
  QueryWork.SQL.Add('WHERE EMPLOYEEAVAILABILITY_DATE = :ADATE AND');
  QueryWork.SQL.Add('EMPLOYEE_NUMBER = :EMPNO ');
  //car 550276
//  QueryWork.SQL.Add('AND ((SHIFT_NUMBER = :NEWSHNO) OR (SHIFT_NUMBER = :OLDSHNO))');
  QueryWork.Prepare;
(*
  if PlanInOtherPlants then
    QueryWork.ParamByName('PCODE').asString := OldPlantCode
  else
    QueryWork.ParamByName('PCODE').asString := DataFilterPlantCode;
*)
  QueryWork.ParamByName('ADATE').asDateTime := WeekDays[WeekDay];
  QueryWork.ParamByName('EMPNO').asInteger := EmpNo;
  QueryWork.ExecSQL;
    // Save New Values
  //CAR 550276
  if NewSHNo > -1 then
  begin
    //CAR 550276 - DON'T SAVE record not filled for TB values
    InsertRecord := False;
    for Index := 1 to MAX_TBS do
    begin
      if (TBs[Index] <> '') and (TBs[Index] <> ' ') then
        InsertRecord := True;
    end;
    if InsertRecord then
    begin
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add('INSERT INTO EMPLOYEEAVAILABILITY(');
      QueryWork.SQL.Add('PLANT_CODE,EMPLOYEEAVAILABILITY_DATE,SHIFT_NUMBER,');
      QueryWork.SQL.Add('EMPLOYEE_NUMBER,AVAILABLE_TIMEBLOCK_1,');
      QueryWork.SQL.Add('AVAILABLE_TIMEBLOCK_2,AVAILABLE_TIMEBLOCK_3,');
      QueryWork.SQL.Add('AVAILABLE_TIMEBLOCK_4,AVAILABLE_TIMEBLOCK_5,');
      QueryWork.SQL.Add('AVAILABLE_TIMEBLOCK_6,AVAILABLE_TIMEBLOCK_7,');
      QueryWork.SQL.Add('AVAILABLE_TIMEBLOCK_8,AVAILABLE_TIMEBLOCK_9,');
      QueryWork.SQL.Add('AVAILABLE_TIMEBLOCK_10,CREATIONDATE,');
      QueryWork.SQL.Add('MUTATIONDATE,MUTATOR)');
      QueryWork.SQL.Add('VALUES(:PCODE,:ADATE,:SHNO,:EMPNO,:ATB1,');
      QueryWork.SQL.Add(':ATB2,:ATB3,:ATB4,:ATB5,:ATB6,:ATB7,:ATB8,');
      QueryWork.SQL.Add(':ATB9,:ATB10,:CDATE,:MDATE,:MUT)');
      QueryWork.Prepare;
      QueryWork.ParamByName('PCODE').asString := NewPlantCode;
      QueryWork.ParamByName('ADATE').asDateTime := WeekDays[WeekDay];
      QueryWork.ParamByName('SHNO').asInteger := NewSHNo;
      QueryWork.ParamByName('EMPNO').asInteger := EmpNo;
      for Index := 1 to MAX_TBS do
        QueryWork.ParamByName(Format('ATB%d',[Index])).asString := TBs[Index];
      QueryWork.ParamByName('CDATE').asDateTime := DateCurrent;
      QueryWork.ParamByName('MDATE').asDateTime := DateCurrent;
      QueryWork.ParamByName('MUT').asString := SystemDM.CurrentProgramUser;
      QueryWork.ExecSQL;
    end;
  end;

  // Employee Planning

  if WasChanged and ChangePlanning then
  begin
  //Car 4-7-2004
    index := 1;
    while not CheckForPlanning(OldPlantCode, NewPlantCode, WeekDay,
      index, OldSHNo, NewSHNo, EmpNo, ValuesToSave) do
    begin
      Inc(index);
      if Index = MAX_TBS then
        Exit;
    end;
    if OldShNo <= 0 then
    begin
      QuerySelect.Close;
      QuerySelect.SQL.Clear;
      QuerySelect.SQL.Add('SELECT DISTINCT PLANT_CODE, SHIFT_NUMBER');
      QuerySelect.SQL.Add('FROM EMPLOYEEPLANNING ');
      QuerySelect.SQL.Add('WHERE PLANT_CODE=:PCODE');
      QuerySelect.SQL.Add('AND EMPLOYEEPLANNING_DATE=:EDATE');
      QuerySelect.SQL.Add('AND EMPLOYEE_NUMBER = :EMPNO');
      QuerySelect.Prepare;
      QuerySelect.ParamByName('PCODE').AsString := NewPlantCode;
      QuerySelect.ParamByName('EDATE').AsDateTime := WeekDays[WeekDay];
      QuerySelect.ParamByName('EMPNO').AsInteger := EmpNo;
      QuerySelect.Open;
    end
    else
    begin
      QuerySelect.Close;
      QuerySelect.SQL.Clear;
      QuerySelect.SQL.Add('SELECT DISTINCT PLANT_CODE, SHIFT_NUMBER');
      QuerySelect.SQL.Add('FROM EMPLOYEEPLANNING');
      QuerySelect.SQL.Add('WHERE PLANT_CODE=:PCODE');
      QuerySelect.SQL.Add('AND EMPLOYEEPLANNING_DATE=:EDATE');
      QuerySelect.SQL.Add('AND EMPLOYEE_NUMBER=:EMPNO');
      QuerySelect.SQL.Add('AND SHIFT_NUMBER=:SHIFTNO');
      QuerySelect.Prepare;
      QuerySelect.ParamByName('PCODE').AsString := OldPlantCode;
      QuerySelect.ParamByName('EDATE').AsDateTime := WeekDays[WeekDay];
      QuerySelect.ParamByName('EMPNO').AsInteger := EmpNo;
      QuerySelect.ParamByName('SHIFTNO').AsInteger := OldShNo;
      QuerySelect.Open;
    end;

    while not QuerySelect.Eof do
    begin
      PlannedShift := QuerySelect.FieldByName('SHIFT_NUMBER').AsInteger;
      PlantCode := QuerySelect.FieldByName('PLANT_CODE').AsString;

      QuerySelectPlanning.Close;
      QuerySelectPlanning.ParamByName('EMPNO').AsInteger := EmpNo;
      QuerySelectPlanning.ParamByName('DAY').AsInteger := WeekDay;
      QuerySelectPlanning.ParamByName('PCODE').asString := PlantCode;
      QuerySelectPlanning.ParamByName('SNO').asInteger := PlannedShift;
      QuerySelectPlanning.ParamByName('EDATE').asDateTime :=
        WeekDays[WeekDay];
      QuerySelectPlanning.Open;
      Temp := NullStr;
      CountNotAvailable := 0;
      if not QuerySelectPlanning.IsEmpty then
      begin
        for index := 1 to SystemDM.MaxTimeblocks do
        begin
          if Round(QuerySelectPlanning.FieldByName('RECNO').AsFloat) = 1 then
            TBAvailability :=
              QuerySelectPlanning.FieldByName('EMA_TB' +
                IntToStr(index)).AsString;
          if Round(QuerySelectPlanning.FieldByName('RECNO').AsFloat) = 2 then
            TBAvailability :=
              QuerySelectPlanning.FieldByName('STD_TB' +
                IntToStr(index)).AsString;
          if Round(QuerySelectPlanning.FieldByName('RECNO').AsFloat) = 3 then
          begin
            TBAvailability :=
              QuerySelectPlanning.FieldByName('STD_TB' +
                IntToStr(index)).AsString;
            if TBAvailability = Available then
              TBAvailability := QuerySelectPlanning.FieldByName('EMA_TB' +
                IntToStr(index)).AsString;
          end;
          if TBAvailability <> Available then
          begin
            Temp := Temp + Format('SCHEDULED_TIMEBLOCK_%d=''N'',',[index]);
            CountNotAvailable := CountNotAvailable + 1;
          end;
        end;
      end;
      if (CountNotAvailable = 0) or (CountNotAvailable = 4) then
      begin
        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add('DELETE FROM EMPLOYEEPLANNING ');
        QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE');
        QueryWork.SQL.Add('AND EMPLOYEEPLANNING_DATE=:EDATE');
        QueryWork.SQL.Add('AND SHIFT_NUMBER = :SNO ');
        QueryWork.SQL.Add('AND EMPLOYEE_NUMBER = :EMPNO');
        QueryWork.Prepare;
        QueryWork.ParamByName('PCODE').asString := PlantCode;
        QueryWork.ParamByName('EDATE').asDateTime := WeekDays[WeekDay];
        QueryWork.ParamByName('SNO').asInteger := PlannedShift;
        QueryWork.ParamByName('EMPNO').asInteger := EmpNo;
        QueryWork.ExecSQL;
      end
      else
      begin
        Temp := Copy(Temp,1,length(Temp)-1);
        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add('UPDATE EMPLOYEEPLANNING SET ');
        QueryWork.SQL.Add(Temp);
        QueryWork.SQL.Add(',MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE');
        QueryWork.SQL.Add('WHERE PLANT_CODE=:PCODE');
        QueryWork.SQL.Add('AND EMPLOYEEPLANNING_DATE=:EDATE');
        QueryWork.SQL.Add('AND SHIFT_NUMBER=:SHNO AND EMPLOYEE_NUMBER=:EMPNO');
        QueryWork.Prepare;
        QueryWork.ParamByName('PCODE').asString := PlantCode;
        QueryWork.ParamByName('EDATE').asDateTime := WeekDays[WeekDay];
        QueryWork.ParamByName('SHNO').asInteger := PlannedShift;
        QueryWork.ParamByName('EMPNO').asInteger := EmpNo;
        QueryWork.ParamByName('MUTATOR').asString :=
          SystemDM.CurrentProgramUser;
        QueryWork.ParamByName('MUTATIONDATE').asDateTime := DateCurrent;
        QueryWork.ExecSQL;
      end;
      QuerySelect.Next;
    end;//while
  end;
end; // UpdateStdAvailability

procedure TStaffAvailabilityDM.GotoRecord(Recno: Double);
begin
  QueryAvailability.First;
//  QueryAvailability.Moveby(Round(RecNo));
  QueryAvailability.Locate('RECNO', Recno, []);
end;

procedure TStaffAvailabilityDM.RestoreDefault(
  var DefaultAv: TDailyAv;
  EmployeeNumber, Day: Integer; DepartmentCode: String);
var
  PlantCode: String;
  IndexList, LastPos, P, index, ShiftNumber, i: Integer;
  AbsenceCode, StrTB: String;
//  NoTimeBlock: Boolean;
begin
  // Default values
  for index := 1 to SystemDM.MaxTimeblocks do
    DefaultAv[index] := Disable;
  // 550276
  DefaultAv[ISHIFT] :=  Disable;
  // Compute values
  if (EmployeeNumber = 0) and (DepartmentCode = NullStr) then
    Exit;
  QueryWorkRestore.Active := False;
  QueryWorkRestore.SQL.Clear;
  QueryWorkRestore.SQL.Add('SELECT PLANT_CODE, SHIFT_NUMBER');
  QueryWorkRestore.SQL.Add('FROM SHIFTSCHEDULE');
  QueryWorkRestore.SQL.Add('WHERE EMPLOYEE_NUMBER =:EMPLOYEE_NUMBER AND ');
  QueryWorkRestore.SQL.Add('SHIFT_SCHEDULE_DATE =:SDATE');
  QueryWorkRestore.Prepare;
  QueryWorkRestore.ParamByName('EMPLOYEE_NUMBER').asInteger := EmployeeNumber;
  QueryWorkRestore.ParamByName('SDATE').asDateTime := WeekDays[Day];
  QueryWorkRestore.Active := True;
  if not QueryWorkRestore.IsEmpty then
  begin
    PlantCode := QueryWorkRestore.FieldByName('PLANT_CODE').asString;
    ShiftNumber := QueryWorkRestore.FieldByName('SHIFT_NUMBER').asInteger;
  end
  else
    Exit;
  //if no shift schedulled then take the first available shift
  QueryWorkRestore.Active := False;
  //CAR 550276
  if ShiftNumber = -1 then
  begin
    DefaultAv[ISHIFT] :=  NotAvailable;
    Exit;
  end;
  // Determine valid TimeBlocks
  StrTB := '';
  IndexList := FListTBEmpl.IndexOf(IntToStr(EmployeeNumber) + Chr(Ord(254)) +
    IntToStr(ShiftNumber) + Chr(Ord(254)) + PlantCode +
    Chr(Ord(254)) + DepartmentCode);
  if IndexList < 0 then
  begin
  //CAR 18-8-2003
    ATBLengthClass.ValidTB(EmployeeNumber, ShiftNumber,
      PlantCode,  DepartmentCode);

    for i := 1 to SystemDM.MaxTimeblocks do
      if ATBLengthClass.ExistTB(i) then
        StrTB := StrTB + IntToStr(I);
    FListTBEmpl.Add(IntToStr(EmployeeNumber) + Chr(Ord(254)) +
    IntToStr(ShiftNumber) + Chr(Ord(254)) + PlantCode +
    Chr(Ord(254)) + DepartmentCode + Chr(Ord(254)) + STRTB);
  end
  else
  begin
    StrTB := FListTBEmpl.Strings[IndexList];
    P := Pos(Chr(Ord(254)), StrTB);
    LastPos := p;
    while p > 0 do
    begin
      P := Pos(Chr(Ord(254)), StrTB);
      if P > 0 then
        LastPos := p;
    end;
    StrTB := Copy(StrTB, LastPos + 1, Length(StrTB));
  end;
  DefaultAv[ISHIFT] := IntToStr(ShiftNumber);
  if DefaultAv[ISHIFT] = '' then
    DefaultAv[ISHIFT] := ' ';
  if StrTB <> '' then
  begin
    QueryWorkRestore.Active := False;
    QueryWorkRestore.SQL.Clear;
    QueryWorkRestore.SQL.Add('SELECT AVAILABLE_TIMEBLOCK_1,');
    QueryWorkRestore.SQL.Add('AVAILABLE_TIMEBLOCK_2,');
    QueryWorkRestore.SQL.Add('AVAILABLE_TIMEBLOCK_3,AVAILABLE_TIMEBLOCK_4,');
    QueryWorkRestore.SQL.Add('AVAILABLE_TIMEBLOCK_5,AVAILABLE_TIMEBLOCK_6,');
    QueryWorkRestore.SQL.Add('AVAILABLE_TIMEBLOCK_7,AVAILABLE_TIMEBLOCK_8,');
    QueryWorkRestore.SQL.Add('AVAILABLE_TIMEBLOCK_9,AVAILABLE_TIMEBLOCK_10');
    QueryWorkRestore.SQL.Add('FROM STANDARDAVAILABILITY');
    QueryWorkRestore.SQL.Add('WHERE PLANT_CODE=:PLANT_CODE');
    QueryWorkRestore.SQL.Add('AND DAY_OF_WEEK=:DW AND');
    QueryWorkRestore.SQL.Add('SHIFT_NUMBER=:SHNO AND EMPLOYEE_NUMBER=:EMPNO');
    QueryWorkRestore.Prepare;
    QueryWorkRestore.ParamByName('PLANT_CODE').asString :=
      PlantCode;
    QueryWorkRestore.ParamByName('DW').asInteger := Day;
    QueryWorkRestore.ParamByName('SHNO').asInteger := ShiftNumber;
    QueryWorkRestore.ParamByName('EMPNO').asInteger := EmployeeNumber;
    QueryWorkRestore.Active := True;
    if not QueryWorkRestore.IsEmpty then
    begin
     // NoTimeBlock := True;
      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        if QueryWorkRestore.FieldByName(
          Format('AVAILABLE_TIMEBLOCK_%d',[Index])).
            AsString <> NullStr then
        begin
          if ATBLengthClass.ExistTB(Index) then
            DefaultAv[Index] :=
              CharFromStr(QueryWorkRestore.FieldByName(
                Format('AVAILABLE_TIMEBLOCK_%d', [index])).AsString,1,' ');
        end;
      end;
    end;
  end;
  // Check for Illness Messages
  //CAR 550276
  if StrTB = '' then
    Exit;
  QueryWorkRestore.Active := False;
  QueryWorkRestore.SQL.Clear;
  QueryWorkRestore.SQL.Add('SELECT ABSENCEREASON_CODE FROM ILLNESSMESSAGE');
  QueryWorkRestore.SQL.Add('WHERE EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER AND ');
  QueryWorkRestore.SQL.Add('ILLNESSMESSAGE_STARTDATE<=:STARTDATE AND');
  QueryWorkRestore.SQL.Add('((ILLNESSMESSAGE_ENDDATE>=:ENDDATE) OR ' +
    ' (ILLNESSMESSAGE_ENDDATE IS NULL))');
  QueryWorkRestore.Prepare;
  QueryWorkRestore.ParamByName('EMPLOYEE_NUMBER').asInteger := EmployeeNumber;
  QueryWorkRestore.ParamByName('STARTDATE').asDateTime := WeekDays[day];
  QueryWorkRestore.ParamByName('ENDDATE').asDateTime := WeekDays[day];
  QueryWorkRestore.Active := True;
  if not QueryWorkRestore.IsEmpty then
  begin
    AbsenceCode :=
      QueryWorkRestore.FieldByName('ABSENCEREASON_CODE').AsString;
    for index := 1 to SystemDM.MaxTimeblocks do
      if DefaultAv[index] = Available then
        DefaultAv[index] := CharFromStr(AbsenceCode, 1, ' ');
  end;
end;

procedure TStaffAvailabilityDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := StaffAvailabilityDM.DataModuleDestroy;
//  ActiveTables(Self, True);
  ClientDataSetAbsRsn.Open;
  ClientDataSetShift.Open;
  FListTBEmpl := TStringList.Create;
  FListSave := TStringList.Create;
  QuerySelectPlanning.Prepare;
  //car 550274 -team selection
  QueryTeam.SQL.Clear;
  //Car 30.3.2004
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryTeam.Sql.Add('SELECT T.TEAM_CODE, T.DESCRIPTION ' +
      ' FROM TEAM T, TEAMPERUSER U WHERE ' +
      ' U.USER_NAME = :USER_NAME AND ' +
      ' T.TEAM_CODE = U.TEAM_CODE ORDER BY T.TEAM_CODE');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end
  else
  begin
    QueryTeam.Sql.Add('SELECT TEAM_CODE, DESCRIPTION ' +
      ' FROM TEAM ' +
      ' ORDER BY TEAM_CODE');
  end;
  QueryTeam.Prepare;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlant);
  QueryPlant.Open;
end;

procedure TStaffAvailabilityDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FListTBEmpl.Free;
  FListSave.Free;
  ClientDataSetAbsRsn.Close;
  ClientDataSetShift.Close;
  QuerySelectPlanning.Close;
  QuerySelectPlanning.UnPrepare;
  //CAR 550274
  QueryAvailability.Close;
  if QueryAvailability.Prepared then
    QueryAvailability.UnPrepare;
end;

// DXXcalc fields are entered only for improvements of saveing part
// they are display instead of original fields DXX - because when one
// value DXX is changed then only the coresponding field DXXCalc is changed
// otherwise query should be refreshed
procedure TStaffAvailabilityDM.QueryAvailabilityCalcFields(
  DataSet: TDataSet);
var
  Day, Index, K, IndexList : Integer;
  SaveTmp, FName, ShiftNumber: String;
begin
  inherited;
  DataSet.FieldByName('FLOAT_Emp').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger; //?????
  for Day := 1 to 7 do
  begin
    // for plan-in-other-plants, also store the 7 plants
    QueryAvailability.FieldByName(Format('P%dCALC', [Day])).AsString :=
      QueryAvailability.FieldByName(Format('P%d', [Day])).AsString;
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      // RV050.4.3. Filter out -1 for a shift.
      // RV052.2. Bugfix, do not do this! It results in changing of
      //          ShiftSchedule-records!
      if Index = 0 then
        QueryAvailability.
          FieldByName(Format('D%d%dCALC', [Day, ISHIFT])).AsString :=
          QueryAvailability.
            FieldByName(Format('D%d%d', [Day, ISHIFT])).AsString
      else
        QueryAvailability.
          FieldByName(Format('D%d%dCALC', [Day, Index])).AsString :=
          QueryAvailability.
            FieldByName(Format('D%d%d', [Day, Index])).AsString;
    end; // for Index
  end; // for Day
  // recalculate field if was changed - and not refresh the whole query
  // Pims -> Oracle
  IndexList := FListSave.IndexOf(DataSet.FieldByName('RECNO').AsString);
  if IndexList >= 0 then
  begin
    SaveTmp := FListSave.Strings[IndexList + 1];
    K := 1;
    for Day := 1 to 7 do
    begin
      // Plant-string-part is always 6 positions long
      FName := Format('P%dCALC', [Day]);
      QueryAvailability.FieldByName(FName).Value :=
        SaveTmp[K] + SaveTmp[K + 1] + SaveTmp[K + 2] +
          SaveTmp[K + 3] + SaveTmp[K + 4] + SaveTmp[K + 5];
      QueryAvailability.FieldByName(FName).Value :=
        Trim(QueryAvailability.FieldByName(FName).Value);
      K := K + 6;
      for Index := 0 to SystemDM.MaxTimeblocks do
      begin
        if Index > 0 then
        begin
          FName := Format('D%d%dCALC', [Day, Index]);
          if SaveTmp[K] = ' ' then
            QueryAvailability.FieldByName(FName).Value := ''
          else
            QueryAvailability.FieldByName(FName).Value := SaveTmp[K];
        end
        else
        begin
          // Shift (at index 5) is always 2 positions long
          FName := Format('D%d%dCALC', [Day, ISHIFT]);
          ShiftNumber := SaveTmp[K] + SaveTmp[K + 1];
          ShiftNumber := Trim(ShiftNumber);
          QueryAvailability.FieldByName(FName).Value := ShiftNumber;
          inc(K);
        end;
        inc(K);
      end;
    end; // for Day
  end;
//end restore calc field with last changed value
end; // QueryAvailabilityCalcFields

procedure TStaffAvailabilityDM.QueryAvailabilityAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if StaffAvailabilityF_HND <> Nil then
    if StaffAvailabilityF_HND.FSort then
      Exit;

   PopulateAvailability(DataSet);
end;

procedure TStaffAvailabilityDM.InitializeDayChangedArray;
var
  i: Integer;
begin
  for i:= 1 to 7 do
    FDayChanged[i] := 0;
end;

function TStaffAvailabilityDM.DataFilterPlantCode: String;
begin
  if DataFilter.PlantCode = ALLPLANTS then
    Result := QueryAvailability.FieldByName('EMPPLANT_CODE').AsString
  else
    Result := DataFilter.PlantCode;
end;

// RV050.8.
procedure TStaffAvailabilityDM.QueryPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
