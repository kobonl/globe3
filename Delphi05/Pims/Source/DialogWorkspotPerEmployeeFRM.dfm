inherited DialogWorkspotPerEmployeeF: TDialogWorkspotPerEmployeeF
  Left = 185
  Top = 128
  Caption = 'Dialog workspots per employee'
  ClientHeight = 364
  ClientWidth = 609
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 609
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 313
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 609
    Height = 243
    Caption = ''
    TabOrder = 4
    object RadioGroupEmpl: TRadioGroup
      Left = 8
      Top = 128
      Width = 273
      Height = 105
      Caption = 'Employee sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Number'
        'Name')
      ParentFont = False
      TabOrder = 1
    end
    object RadioGroupWKSort: TRadioGroup
      Left = 296
      Top = 128
      Width = 297
      Height = 105
      Caption = 'WorkSpot/Department sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Code'
        'Description')
      ParentFont = False
      TabOrder = 2
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 8
      Width = 585
      Height = 97
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label5: TLabel
        Left = 5
        Top = 52
        Width = 24
        Height = 13
        Caption = 'Plant'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 5
        Top = 21
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 33
        Top = 21
        Width = 26
        Height = 13
        Caption = 'Team'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 270
        Top = 21
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 72
        Top = 22
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 296
        Top = 22
        Width = 7
        Height = 13
        Caption = '*'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 64
        Top = 50
        Width = 200
        Height = 19
        ColCount = 184
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        TitleColor = clBtnFace
      end
      object CheckBoxAllTeam: TCheckBox
        Left = 496
        Top = 20
        Width = 73
        Height = 17
        Caption = 'All teams'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = CheckBoxAllTeamClick
      end
      object ComboBoxPlusTeamFrom: TComboBoxPlus
        Left = 64
        Top = 20
        Width = 200
        Height = 19
        ColCount = 206
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = ComboBoxPlusTeamFromCloseUp
      end
      object ComboBoxPlusTeamTo: TComboBoxPlus
        Left = 286
        Top = 20
        Width = 200
        Height = 19
        ColCount = 206
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = ComboBoxPlusTeamToCloseUp
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 345
    Width = 609
  end
  inherited pnlBottom: TPanel
    Top = 304
    Width = 609
    inherited btnOk: TBitBtn
      ModalResult = 0
      OnClick = btnOkClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 688
    Top = 24
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 656
    Top = 24
  end
  inherited StandardMenuActionList: TActionList
    Left = 512
    Top = 0
  end
end
