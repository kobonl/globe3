(*
  Changes:
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:20-APR-2010. RV061.1.
    - New quickreport-component used. Updated from 3.6.2 to 5.02.
    MRA:28-JUN-2010. RV066.
    - When using MEMO-fields and printing to printer or PrimoPDF, it can
      result in double lines! Possible cause: HEIGHT_MEMO.
    MRA:15-NOV-2010 RV080.1.
    - Changes for Memo-mechanism.
    - Put Memo-mechanism in a seperate class, so it can be created and freed
      when needed.
    - Memo-mechanism is disabled, because it gave double-lines.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:13-JUL-2011 RV094.9. Bugfix.
    - Reports + filename for export
      - An extra check is needed when a filename is entered
        for the export of a report, to see if the filename
        is valid.
    MRA:29-SEP-2011 RV097.1. 20012099.
    - Export-option in report-dialogs:
      - Default extension is .txt but should be .csv, with
        option to choose for .txt.
    MRA:4-SEP-2012 TODO 21095
    - Added extra 'export'-functions that can be used when a value
      must be exported. It will remove spaces at left and right of value.
    MRA:21-MAR-2013 TD-21527
    - Reformat hours-part for showing time. Show it with comma's and dots.
      - Example: 39154:35 will be shown like 3.9154:35
    MRA:22-MAR-2013 TD-22321
    - Save-to-PDF gives wrong result when report is in landscape:
      - Right part is missing!
    MRA:22-MAR-2013 TD-22323 Printing reports in duplex does not work.
    - Added routines to print reports in duplex:
      - It sets the report-duplex-setting to duplex based on default printer
        settings.
      - When printer is capable of duplex printing and it is set
        to this, then it will print report in duplex.
    MRA:7-MAY-2013 TD-22321 Save to PDF-landscape issue
    - Adjust position of page-number in footer, to get it right for
      portrait and for landscape.
    MRA:6-JUN-2014 20015223
    - Productivity reports and grouping on workspots
    MRA:18-JUN-2014 20015220
    - Productivity reports and selection on time
    MRA:24-JUN-2014 20015221
    - Include open scans
    MRA:12-SEP-2016 ABS-27052
    - Ensure quick-report is set to default printer.
*)
unit ReportBaseFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, DB, PimsFRM, DBTables, FileCtrl, QRPDFFilt,
  QRExport, QRXMLSFilt, QRWebFilt, Printers;

// RV080.1. Disabled.
{
const
  HEIGHT_MEMO = 14; // MRA:28-JUN-2010 Changed from 15 to 14. RV066.
  MAX_CHILD_BAND = 10;
  MAX_MEMO_LINES = 3;
  MAX_LINES_ON_PAGE = 35;
}
type
  TQRBaseParameters = class
    FPlantFrom,
    FPlantTo,
    FEmployeeFrom,
    FEmployeeTo: String;
    procedure SetValues(PlantFrom, PlantTo,
      EmployeeFrom, EmployeeTo: String);
    function CheckSelection: Boolean;
  end;

// MR:06-12-2002 Begin
// Export to file class and methods
type

// RV080.1. Disabled.
  // MRA:26-MAY-2010. Should be 0..MAX_CHILD_BAND-1, instead of 0..MAX_CHILD_BAND
{  TChildBandMemoArray = Array[0.. MAX_CHILD_BAND-1] of TQRChildBand; }


  TExportClass = class
  private
    StringList: TStringList;
    Init: Boolean;
    FFilename: String;
    FSep: String;
    FExtension: String;
    FExportDone: Boolean;
    procedure SetFilename(const Value: String);
    procedure SetSep(const Value: String);
    procedure SetExtension(const Value: String);
    procedure SetExportDone(const Value: Boolean);
  public
    constructor Create;
    procedure Free;
    procedure AskFilename;
    procedure AddText(Line: String);
    procedure SaveText;
    procedure ClearText;
    property Filename: String read FFilename write SetFilename;
    property Sep: String read FSep write SetSep;
    property Extension: String read FExtension write SetExtension;
    property ExportDone: Boolean read FExportDone write SetExportDone;
  end;
// MR:06-12-2002 End

  TReportBaseF = class(TPimsF)
    qrptBase: TQuickRep;
    QRBndBasePageFooter: TQRBand;
    QRBndBasePageHeader: TQRBand;
    QRLblBaseTitle: TQRLabel;
    QRLblBaseLaundry: TQRLabel;
    QRShapeBaseHeaderLine2: TQRShape;
    QRShapeBaseFooterLine: TQRShape;
    QRLblBaseDateText: TQRLabel;
    QRSysDataBaseDate: TQRSysData;
    QRSysDataBaseTime: TQRSysData;
    QRLblBasePage: TQRLabel;
    QRSysDataBasePage: TQRSysData;
    QRShapeBaseHeaderLine1: TQRShape;
    QRPDFFilter1: TQRPDFFilter;
    QRCSVFilter1: TQRCSVFilter;
    QRHTMLFilter1: TQRHTMLFilter;
    QRExcelFilter1: TQRExcelFilter;
    QRRTFFilter1: TQRRTFFilter;
    QRXMLSFilter1: TQRXMLSFilter;
    QRTextFilter1: TQRTextFilter;
    QRWMFFilter1: TQRWMFFilter;
    procedure qrptBaseEndPage(Sender: TCustomQuickRep);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure qrptBaseAfterPreview(Sender: TObject);
    procedure qrptBaseBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);

  private
    { Private declarations }
    //CAR 5-2-2004 - Update progress indicator
    FReport_RecordCount,
    FProgressIndicatorPos: Integer;

    FException: String;
    FSuppressZeroes: Boolean;
    FExtendedDuplex: Integer;
    FInclExclWorkspotsResult: String;
    FEnableWorkspotIncludeForThisReport: Boolean;
    FUseDateTime: Boolean;
    FIncludeOpenScans: Boolean;
    procedure SetException(const Value: String);
    function GetException: String;
    //
    procedure SetProgressIndicatorPos(const Value: Integer);
    procedure SetReport_RecordCount(const Value: Integer);
    function ReformatHrs(AValue: Integer; AReformat: Boolean): String; // TD-21527
//    procedure DoPrinterOrientation; // TD-22321
    procedure DeterminePrinterSettings(var ADuplex: Boolean);
    function DeterminePrinterSettingDuplex: Boolean;
    procedure DoSetPrinterDuplex;
    procedure DoSetDefaultPrinter;
  protected
// RV080.1. Disabled.
{
    //CAR  changes for memo fields
    FChildBand: TChildBandMemoArray;// array of child bands created
    FMAX_MEMO_LINES, FMax_Line_Memo_Page: Integer;
    FMemoBand: TQRCustomBand;
}
    //
    FQRBaseParameters: TQRBaseParameters;
    function ExistsRecords: Boolean; virtual; abstract;
    procedure ConfigReport; virtual; abstract;
 //   procedure OpenDataSets(Status: Boolean); virtual; abstract;
// RV080.1. Disabled.
{
//functions for printing Memo fields CAR 27-05-2003
    procedure GetMaxCountMemo(ParentBand: TQRCustomBand ;
      var MaxCountLines, MaxTopMemo: Integer) ;
    procedure FillMemoChildBand(IndexLines: Integer;
      ParentBand, ChildBand: TQRCustomBand);
    procedure SetHeightMemoFields(ParentBand: TQRCustomBand;
       MaxCountLines, MaxTopMemo: Integer);
    function GetMemo(NameMemo: String; BandParent: TQRCustomBand): TQRMemo;
}
  public
    { Public declarations }

    procedure SetDataSetQueryReport(QueryReport: TQuery);
    function FillZero(IntVal: Integer): String;

// RV080.1. Disabled.
{
//functions for printing Memo fields CAR 27-05-2003
    procedure SetHeightMemoBand(PrintBand: Boolean; ParentBand: TQRCustomBand);
    procedure SetMemoParam(Memo_Lines, Max_Lines_Memo_Page: Integer;
      BandMemo: TQRCustomBand);
}
// END CAR
    function DecodeHrsMin(HrsMin: Integer; AReformat: Boolean=False):String; // TD-21527
    function ExportHrsMin(HrsMin: Integer; AReformat: Boolean=False): String; // TD-21527
    function DecodeHrsMinL(HrsMin: Integer; L :Integer; AReformat: Boolean=False):String; // TD-21527
    function ExportHrsMinL(HrsMin: Integer; L :Integer; AReformat: Boolean=False):String; // TD-21527
    function FillSpaces(StrVal: String; MaxLength: Integer): String;
    function ProcessRecords: Boolean;
    function InclExclWorkspotsAll: Boolean; // 20015223
//    procedure ActiveReport(Status: Boolean);
    procedure FreeMemory; virtual;
    // car - update progress indicator
    procedure UpdateProgressIndicator;
    function DisplayWorkspotFrom(AValue: String): String; // 20015223
    function DisplayWorkspotTo(AValue: String): String; // 20015223
    property Report_RecordCount: Integer read FReport_RecordCount write
      SetReport_RecordCount;
    property ProgressIndicatorPos: Integer read FProgressIndicatorPos write
      SetProgressIndicatorPos;
    //CAR: 5-2-2004
    property ReportException: String read GetException write SetException;
    property QRBaseParameters : TQRBaseParameters read FQRBaseParameters;
    property SuppressZeroes: Boolean read FSuppressZeroes write FSuppressZeroes;
    property ExtendedDuplex: Integer read FExtendedDuplex write FExtendedDuplex;
    property EnableWorkspotIncludeForThisReport: Boolean
      read FEnableWorkspotIncludeForThisReport
      write FEnableWorkspotIncludeForThisReport; // 20015223
    property InclExclWorkspotsResult: String read FInclExclWorkspotsResult
      write FInclExclWorkspotsResult; // 20015223
    property UseDateTime: Boolean read FUseDateTime write FUseDateTime; // 20015220
    property IncludeOpenScans: Boolean read FIncludeOpenScans write FIncludeOpenScans; // 20015221
  end;

  TReportBaseClassQR = Class of TReportBaseF;

var
  ReportBaseF: TReportBaseF;
  ExportClass: TExportClass; // MR:06-12-2002

implementation

uses
  SystemDMT, UPimsMessageRes, UPimsConst, UGlobalFunctions,
  SideMenuUnitMaintenanceFRM;

{$R *.DFM}

var
  OldApp: TApplication;
  OldScr: TScreen;

{every time when the report is opened the parameters are send to the values of
this class}
procedure TQRBaseParameters.SetValues(PlantFrom, PlantTo,
    EmployeeFrom, EmployeeTo: String);
begin
  FPlantFrom := PlantFrom;
  FPlantTo := PlantTo;
  FEmployeeFrom := EmployeeFrom;
  FEmployeeTo := EmployeeTo;
end;

procedure TReportBaseF.SetDataSetQueryReport(QueryReport: TQuery);
begin
  if (qrptBase.DataSet = Nil) then
    qrptBase.DataSet := QueryReport;
end;

function TReportBaseF.FillSpaces(StrVal: String; MaxLength: Integer): String;
var
  k: Integer;
begin
  k := Length(StrVal);
  while (k <= MaxLength) do
  begin
    StrVal := StrVal + ' ';
    Inc(k);
  end;
  Result := StrVal;
end;

{check if the parameters of the report are valid before preview the report}
function TQRBaseParameters.CheckSelection: boolean;
begin
  if (FPlantFrom = '') or (FPlantTo = '') then
  begin
    DisplayMessage(SPimsNoPlant, mtInformation, [mbOK]);
    Result := False;
    Exit;
  end;
  if (FPlantFrom = FPlantTo) and
    ((FEmployeeFrom = '') or (FEmployeeTo = '')) then
  begin
    DisplayMessage(SPimsNoEmployee, mtInformation, [mbOK]);
    Result := False;
    Exit;
  end;
  Result := True;
end;

function TReportBaseF.FillZero(IntVal: Integer): String;
begin
  Result := IntToStr(IntVal);
  if (IntVal = 0) then
     Result := '00';
  if (length(IntToStr(IntVal)) = 1) then
    Result := '0' + IntToStr(IntVal);
end;

// TD-21527
function TReportBaseF.DecodeHrsMin(HrsMin: Integer;
  AReformat: Boolean=False):String;
var
  Hrs, Min: Integer;
begin
  // RV054.4.
  if SuppressZeroes then
    if HrsMin = 0 then
    begin
      Result := '';
      Exit;
    end;

  if HrsMin < 0 then
    Result := '-'
  else
    Result := '';
  HrsMin := Abs(HrsMin);
  Hrs := HrsMin div 60;
  Min := HrsMin mod 60;
  // TD-21527
  Result := Result + ReformatHrs(Hrs, AReformat) {FillZero(Hrs)} + ':' +
    FillZero(Min);
  if Length(Result)= 6 then
    Result := ' ' + Result;
  if Length(Result)= 5 then
    Result := '  ' + Result;
end;

// TD-21527
function TReportBaseF.DecodeHrsMinL(HrsMin: Integer; L :Integer;
  AReformat: Boolean=False):String;
var
  Hrs, Min: Integer;
begin
  // RV054.4.
  if SuppressZeroes then
    if HrsMin = 0 then
    begin
      Result := '';
      Exit;
    end;

  if HrsMin < 0 then
    Result := '-'
  else
    Result := '';
  HrsMin := Abs(HrsMin);
  Hrs := HrsMin div 60;
  Min := HrsMin mod 60;
  // TD-21527
  Result := Result +  ReformatHrs(Hrs, AReformat) {FillZero(Hrs)} +
    ':' +  FillZero(Min);
  while (Length(Result) < L ) do
    Result := ' ' + Result;
end;

{free memory used in the report and the report ifself}
procedure TReportBaseF.FreeMemory;
// RV080.1. Disabled.
{
var
  IndexBand: Integer;
}
begin
// RV080.1. Disabled.
{
// destroy child band created at runtime for memo mechanism
  if FMemoBand <> Nil then
  begin
     for IndexBand := MAX_CHILD_BAND-1 downto 0 do
       FChildBand[IndexBand].HasChild := False;
  end;
}
  qrptbase.Free;
  FreeAndNil(FQRBaseParameters);
end;

// RV080.1. Disabled.
{
//   CAR 27-05-2003 print Memo fields
// function set variable on message Create Report -
// it has to be called on each report who has band with memo fields
procedure TReportBaseF.SetMemoParam(Memo_Lines, Max_Lines_Memo_Page: Integer;
   BandMemo: TQRCustomBand);
begin
  FMemoBand := BandMemo;   // set Band who contains memo fields
  // set Number of lines into a memo field on a child band or bandmemo
  FMAX_MEMO_LINES := Memo_Lines;
  //set number of lines into a memo which had to fit in one page
  FMax_Line_Memo_Page := Max_Lines_Memo_Page;
end;
}

// RV080.1. Disabled.
{
// get the maximal number of lines over each memo fields of a parentband
procedure TReportBaseF.GetMaxCountMemo(ParentBand: TQRCustomBand;
  var MaxCountLines, MaxTopMemo: Integer);
var
  Counter: Integer;
begin
  inherited;
  MaxCountLines := 0;
  MaxTopMemo := 0;
  for Counter := 0 to ParentBand.ControlCount -1 do
    if (ParentBand.Controls[Counter] is TQRMemo) then
    begin
      (ParentBand.Controls[Counter] as TQRMemo).AutoStretch := False;
      (ParentBand.Controls[Counter] as TQRMemo).AutoSize := True;
      (ParentBand.Controls[Counter] as TQRMemo).Height := HEIGHT_MEMO;
       MaxCountLines := Max(MaxCountLines,
         (ParentBand.Controls[Counter] as TQRMemo).Lines.Count);
       MaxTopMemo :=
         Max(MaxTopMemo,(ParentBand.Controls[Counter] as TQRMemo).Top);
    end;
end;
}

// RV080.1. Disabled.
{
// set the height of memo fields who belongs to ParentBand
// set height one parent band
procedure TReportBaseF.SetHeightMemoFields(ParentBand: TQRCustomBand;
   MaxCountLines, MaxTopMemo: Integer);
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to ParentBand.ControlCount -1 do
    if (ParentBand.Controls[Counter] is TQRMemo) then
  //set height for memo of band
    begin
      TempComponent := TQRMemo(ParentBand.Controls[Counter]);
      (TempComponent as TQRMemo).Height := HEIGHT_MEMO * MaxCountLines;
    end;
  // set height of band
  ParentBand.Height :=  MaxTopMemo + HEIGHT_MEMO * MaxCountLines;
end;
}

// RV080.1. Disabled.
{
// get the memo field of BandParent with the namememo
function TReportBaseF.GetMemo(NameMemo: String; BandParent: TQRCustomBand): TQRMemo;
var
  Counter: Integer;
begin
  Result := Nil;
  if NameMemo = '' then
    Exit;
  for Counter := 0 to BandParent.ControlCount -1 do
    if (BandParent.Controls[Counter] is TQRPrintable) and
      (BandParent.Controls[Counter] is TQRMemo) then
      if TQRMemo(BandParent.Controls[Counter]).Name = NameMemo then
      begin
        Result := TQRMemo(BandParent.Controls[Counter]);
        Exit;
      end;
end;
}

// RV080.1. Disabled.
{
// fill lines of memo fields of band ChildBand with lines of coresponding memo
//  of parent band
procedure TReportBaseF.FillMemoChildBand(IndexLines: Integer;
  ParentBand, ChildBand: TQRCustomBand);
var
  PosName, Counter, CountLines: Integer;
  TmpChildMemo, TmpMemo: TQRMemo;
begin
  for Counter := 0 to ChildBand.ControlCount -1 do
    if (ChildBand.Controls[Counter] is TQRMemo) then
    begin
      TmpChildMemo := TQRMemo(ChildBand.Controls[Counter]);
      PosName := Pos('_', TmpChildMemo.Name);
      TmpMemo := Nil;
      if PosName > 0 then
      // get the name of memo corresponding to parent line
        TmpMemo := GetMemo(copy(TmpChildMemo.Name, PosName + 1, 100), ParentBand);
      if TmpMemo <> Nil then
      begin
        TmpChildMemo.Lines.Clear;
        TmpChildMemo.Left := TmpMemo.Left;
        TmpChildMemo.Width := TmpMemo.Width;
        TmpChildMemo.AutoSize := TmpMemo.AutoSize;
        for CountLines := 0 to (FMAX_MEMO_LINES - 1) do
          if(TmpMemo.Lines.Count >  IndexLines + CountLines) then
             TmpChildMemo.Lines.Add(TmpMemo.Lines.Strings[IndexLines + CountLines]);
       end;
    end;
end;
}

// RV080.1. Disabled.
{
// this function has to be called on message Before Print of band which contains memo
// - set height of parent band which contains memo fields
// - set height of all child Memo bands runtime created
// - fill all runtime created memo fields of each child band with the next lines of memo

procedure TReportBaseF.SetHeightMemoBand(PrintBand: Boolean;
  ParentBand: TQRCustomBand);
var
  Save_FMAX_MEMO_LINES, MaxTopMemo, IndexLines, Lines,
  IndexBand, MaxMemoLines: Integer;
begin
  if not PrintBand then
  begin
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
    Exit;
  end;
  //get maximal lines per memo fields of ParentBand
  GetMaxCountMemo(ParentBand, Lines, MaxTopMemo);
  //if Lines is less than the number printed with parent band then delete all child band
  // there is no need to use them
  if (Lines < FMAX_MEMO_LINES)  then
  begin
    for IndexBand := 0 to MAX_CHILD_BAND-1 do
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
    SetHeightMemoFields(ParentBand, Lines, MaxTopMemo);
    Exit;
  end;

  // check if maximal lines of memo fields is bigger then
  // the covered length by all child band runtime build
  Save_FMAX_MEMO_LINES := 0;
  if Lines > (FMAX_MEMO_LINES + 1) * MAX_CHILD_BAND then
  begin
    // if yes then set the number of lines of one memo with the
    // maximal number allowed to print lines into one page
    // - this is a parameter set in a report on create message
    Save_FMAX_MEMO_LINES := FMAX_MEMO_LINES;
    FMAX_MEMO_LINES := MAX_LINES_ON_PAGE;
  end;
  // set height of fix band of report - ParentBand
  SetHeightMemoFields(ParentBand, FMAX_MEMO_LINES, MaxTopMemo);
  // determine number of child band used further
  if (Lines mod FMAX_MEMO_LINES) <> 0 then
  begin
    Lines := (Lines div FMAX_MEMO_LINES);
    Lines := Lines + 1;
  end
  else
    Lines := (Lines div FMAX_MEMO_LINES);
  Lines := Lines - 1; // number of active band childs - one line was used for parent

   // fill memo of active child bands with Max_memo_lines
  IndexLines := FMAX_MEMO_LINES;
  for IndexBand := 0 to MAX_CHILD_BAND-1 do
  begin
    if IndexBand < Lines then
    begin
      FillMemoChildBand(IndexLines,  ParentBand, FChildBand[IndexBand]);
      GetMaxCountMemo(FChildBand[IndexBand], MaxMemoLines, MaxTopMemo) ;
      SetHeightMemoFields(FChildBand[IndexBand], MaxMemoLines, -2); // MRA:Why is last parameter -2 ?
      IndexLines := IndexLines + FMAX_MEMO_LINES;
    end
    else
      SetHeightMemoFields(FChildBand[IndexBand], 0, 0);
  end;

// restore number of lines of one memo
  if Save_FMAX_MEMO_LINES <> 0 then
    FMAX_MEMO_LINES := Save_FMAX_MEMO_LINES;
end;
// END CAR
}

{set an error string to a private variable of report
every time when the property ReportException is wrote give an error message}
procedure TReportBaseF.SetException(const Value: String);
begin
  FException := Value;
  if (Value <> '') then
  begin
    DisplayMessage(Value, mtError, [mbOk]);
    Screen.Cursor := crDefault;
  end;
end;

{every time when the property ReportException is read give an error message}
function TReportBaseF.GetException: String;
begin
  if (FException <> '') then
  begin
    DisplayMessage(FException, mtError, [mbOk]);
    Screen.Cursor := crDefault;
  end;
  Result := FException;
end;

{prepare datasets and configure settings of the report}
function TReportBaseF.ProcessRecords: Boolean;
var
  SaveTitle: String;
begin
  try
    Result := (ReportException = '');
    {give the error message, report will not be opened again
    until the error is not fixed}
    if not Result then
      Exit;
    if (qrptBase.DataSet = Nil) then
      raise Exception.Create(SExceptionDataSetReport);
    if not ExistsRecords then
    begin
      Result := False;
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
      Exit;
    end;
    ConfigReport;
    Result := True;

 //Car 26-3-2004:
  SaveTitle :=  Application.Title;
  // RV089.1. Why hide the parent-window?
{
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_Hide);
  Application.Title := SideMenuUnitMaintenanceF.Caption;
  ShowWindow(Application.Handle, SW_RESTORE);
}
// qrptBase.Preview;
  // RV061.1.
//  qrptBase.Height := Screen.Height - 30;
//  qrptBase.Width := Screen.Width - 30;
  qrptBase.PreviewModal;
  Application.ProcessMessages;
  // RV089.1. Why hide the parent-window?
{
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
  ShowWindow(Application.Handle, SW_RESTORE);
  ShowWindow(Application.Handle, SW_HIDE);
}
  Application.Title := SaveTitle;
  SetActiveWindow((self.owner as TForm).Handle);

  except
    on E:Exception do
    begin
      ReportException := E.Message;
      Result := False;
    end;
  end;
end;


{close the glass when the first page of report is displayed}
procedure TReportBaseF.qrptBaseEndPage(Sender: TCustomQuickRep);
begin
  inherited;
  if qrptBase.PageNumber = 1 then
    Screen.Cursor := crDefault;
  //CAR 29.1.2004 make progress indicator to reach 100%
  // if it's end of DataSet or it's on last record
  if Report_RecordCount <> 0 then
  begin
    if qrptBase.DataSet.Eof then
      qrptBase.QRPrinter.Progress := 100
    else
    begin
      qrptBase.DataSet.Next;
      if qrptBase.DataSet.Eof then
        qrptBase.QRPrinter.Progress := 100
      else
        qrptBase.DataSet.Prior;
    end;
  end;
end;

{create also the object for parameters used in the report}
procedure TReportBaseF.FormCreate(Sender: TObject);
// RV080.1. Disabled.
{
var
  Counter, IndexBand : Integer;
  ParentBand: TQRCustomBand;
  TmpMemo, NewTmpMemo: TQRMemo;
}
begin
  inherited;
  SuppressZeroes := False;
  FQRBaseParameters := TQRBaseParameters.Create;

// RV080.1. Disabled.
{
  // check if this mechanism is user further - if exists a band with memo fields
  if FMemoBand = nil then
    Exit;

//Create childband with memo fields on the same position as leftmemo
// maximal number of child bands is MAX_CHILD_BAND
  ParentBand := FMemoBand;
  for IndexBand := 0 to MAX_CHILD_BAND-1 do
  begin
    ParentBand.HasChild := True;
    FChildBand[IndexBand] := ParentBand.ChildBand;
    FChildBand[IndexBand].Height := HEIGHT_MEMO;
    FChildBand[IndexBand].Name := FMemoBand.Name + 'ChildBand' + IntToStr(IndexBand);
    ParentBand := FChildBand[IndexBand];
    FChildBand[IndexBand].Font.Size := 8;
    FChildBand[IndexBand].Left := FMemoBand.Left;
  end;
  // for each memo field of parent band
  //create a memo field in each childband created before
  for Counter := 0 to FMemoBand.ControlCount -1 do
    if (FMemoBand.Controls[Counter] is TQRMemo) then
    begin
      TmpMemo := TQRMemo(FMemoBand.Controls[Counter]);
      for IndexBand := 0 to MAX_CHILD_BAND-1 do
      begin
        NewTmpMemo := TQRMemo(FChildBand[IndexBand].AddPrintable(TQRMemo));
        NewTmpMemo.Left := TmpMemo.Left;
        NewTmpMemo.Font.Size := 8;
        NewTmpMemo.Top := -1;
        NewTmpMemo.Tag := -1;  // to remove any space between child bands
        NewTmpMemo.Height := HEIGHT_MEMO;
        NewTmpMemo.AutoSize := True;
        NewTmpMemo.AutoStretch := False;
        NewTmpMemo.Lines.Clear;
        NewTmpMemo.Width := TmpMemo.Width;
        NewTmpMemo.Name := 'Memo' + IntToStr(IndexBand) + '_' +  TmpMemo.Name;
        NewTmpMemo.Alignment := TmpMemo.Alignment;
        NewTmpMemo.Parent := FChildBand[IndexBand];
        NewTmpMemo.AlignToBand := TmpMemo.AlignToBand;
        NewTmpMemo.Transparent := TmpMemo.Transparent;
      end;
    end;
}
end;

procedure TReportBaseF.FormDestroy(Sender: TObject);
begin
  inherited;
  FreeMemory;
end;

// Car - update progress indicator
procedure TReportBaseF.UpdateProgressIndicator;
begin
  if ProgressIndicatorPos < Report_RecordCount then
    qrptBase.QRPrinter.Progress := (Longint(ProgressIndicatorPos) * 100) div
      Report_RecordCount;
end;

procedure TReportBaseF.SetProgressIndicatorPos(Const Value: Integer);
begin
  FProgressIndicatorPos := Value;
end;

procedure TReportBaseF.SetReport_RecordCount(Const Value: Integer);
begin
  FReport_RecordCount := Value;
end;
// MR:06-12-2002 Begin
// Export to file methods
constructor TExportClass.Create;
begin
  inherited Create;
  try
    StringList := TStringList.Create;
    Init := True;
    ExportDone := False;
    Sep := ';';
    // RV097.1.
    Extension := '.csv'; // '.txt';
    Filename := 'report'; // + Extension;
  except
    Init := False;
  end;
end;

procedure TExportClass.Free;
begin
  if Init then
    StringList.Free;
  Init := False;
  inherited Free;
end;

procedure TExportClass.SetFilename(const Value: String);
var
  ExportPath: String;
  // RV094.9. Check for valid filename.
  function ValidFileName(AFileName: String): String;
  const
    InvalidCharacters: set of Char = ['\', '/', ':', '*', '?', '"', '|'];
  var
    I, Lng: Integer;
  begin
    Result := AFileName;
    if Result <> '' then
    begin
      Lng := Length(AFileName);
      for I := 1 to Lng do
      begin
        if AFileName[I] in InvalidCharacters then
          AFileName[I] := '_';
      end;
      Result := AFileName;
    end
    else
      Result := 'Temp' + Extension; // '.txt'
  end; // ValidFileName
begin
  ExportPath := ExtractFilePath(Application.ExeName);
  ExportPath := Copy(ExportPath, 1, (Length(ExportPath) - 4)) + 'Export\';

  if not DirectoryExists(ExportPath) then
    ForceDirectories(ExportPath);
  FFilename := ExportPath + ValidFileName(Value);
  // RV097.1. Do not add the extension here, but during 'SaveDialog'.
//  FFilename := ChangeFileExt(FFilename, Extension);
end;

procedure TExportClass.AskFilename;
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(Application);
  try
    SaveDialog.Options := SaveDialog.Options + [ofOverWritePrompt];
    // RV097.1.
//    SaveDialog.Filter := 'Text files (*.txt)|*' + UpperCase(Extension);
    SaveDialog.Filter := '(*.csv)|*.csv|(*.txt)|*.txt';
    SaveDialog.DefaultExt := Copy(Extension, 2, Length(Extension)); // Skip the '.'.
    SaveDialog.FileName := FFilename;
    if SaveDialog.Execute then
    // RV097.1. Do not add extension here because it can be something else!
//      FFilename := ChangeFileExt(SaveDialog.Filename, Extension);
      FFilename := SaveDialog.Filename;
  finally
    SaveDialog.Free;
  end;
end;

procedure TExportClass.AddText(Line: String);
begin
  if Init then
    StringList.Add(Line);
end;

procedure TExportClass.SaveText;
begin
  if Init then
    StringList.SaveToFile(Filename);
end;

procedure TExportClass.ClearText;
begin
  if Init then
    StringList.Clear;
end;

procedure TExportClass.SetSep(const Value: String);
begin
  FSep := Value;
end;

procedure TExportClass.SetExtension(const Value: String);
begin
  FExtension := Value;
end;

procedure TExportClass.SetExportDone(const Value: Boolean);
begin
  FExportDone := Value;
end;
// MR:06-12-2002 End

 //CAR END
{ TfrmReportBase }
// Store the original application and screen variables
procedure TReportBaseF.qrptBaseAfterPreview(Sender: TObject);
begin
  inherited;
  // RV061.1. Do not do this: When this is done the application is not
  //          visible on the taskbar.
//  ShowWindow(Application.Handle, SW_RESTORE);
//  ShowWindow(Application.Handle, SW_HIDE);
end;

// TODO 21095 Use this when it must be exported, it will remove
// any spaces around the string.
// REMARK: When used it can give wrong values when using Excel:
// Reason: A totals-value can be higher than 24 hours, and this
// gives a wrong value (including a date).
// TD-21527
function TReportBaseF.ExportHrsMin(HrsMin: Integer;
  AReformat: Boolean=False): String;
begin
  Result := Trim(DecodeHrsMin(HrsMin, AReformat));
end;

// TODO 21095 Use this when it must be exported, it will remove
// any spaces around the string.
// See also 'ExportHrsMin'.
// TD-21527
function TReportBaseF.ExportHrsMinL(HrsMin, L: Integer;
  AReformat: Boolean=False): String;
begin
  Result := Trim(DecodeHrsMinL(HrsMin, L, AReformat));
end;

// TD-21527
function TReportBaseF.ReformatHrs(AValue: Integer; AReformat: Boolean): String;
begin
  if AReformat then
  begin
    if AValue = 0 then
      Result := FillZero(AValue)
    else
    begin
      Result := Format('%.0n', [1.0 * AValue]);
      if (Length(Result) = 1) then
        Result := '0' + Result;
    end;
  end
  else
    Result := FillZero(AValue);
end;

// TD-22321
// TD-22323 Duplex:
procedure TReportBaseF.qrptBaseBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  inherited;
  DoSetDefaultPrinter; // ABS-27052
  DoSetPrinterDuplex; // TD-22323
  // TD-22321 Calculate the left-position so it will be OK when
  //          report is printed in Portrait or in Landscape.
  QRLblBasePage.Left := Round(QRBndBasePageFooter.Width * 87 / 100);
  QRSysDatabasePage.Left := Round(QRBndBasePageFooter.Width * 92 / 100);
//  DoPrinterOrientation; // TD-22321 (this does not work).
end;

{
// TD-22321 This only makes a setting for default printer, it does not work
//          when saving to PDF-file.
procedure TReportBaseF.DoPrinterOrientation;
const
  MAXPATH=1024;
var
  Device : array[0..MAXPATH] of char;
  Driver : array[0..MAXPATH] of char;
  Port   : array[0..MAXPATH] of char;
  hDeviceMode: THandle;
  pDevMode: PDeviceMode;
  eCode: LongInt;
begin
  with Printer do
  begin
    BeginDoc;
    try
      // Get a handle to the printer DeviceMode data structure
      GetPrinter(Device, Driver, Port, hDeviceMode);
      // Get the DeviceMode data structure
      pDevMode := GlobalLock( hDevicemode );
      // Set the orientation
      with pDevMode^ do
      begin
        dmFields := dmFields or DM_ORIENTATION;
        if qrptBase.Page.Orientation = poLandscape then
          dmOrientation := DMORIENT_LANDSCAPE
        else
          dmOrientation := DMORIENT_PORTRAIT;
        // or to change trays.
        // dmFields := dmFields or DM_DEFAULTSOURCE;
        // dmDefaultSource := DM_MANUAL;
      end;
      // End the current page, otherwise get Access Denied error
      Windows.EndPage(Printer.Handle);
      // ResetDC updates the DeviceMode structure for the printer
      if ResetDC( Printer.Handle, pDevMode^ ) = 0 Then
      begin
         //ShowMessage( 'ResetDC failed');
         eCode:=GetLastError;
         // SayResult(eCode);
       end;
    // Start the next page, which will use the new orientation
    // Read the caveats for both EndPage and StartPage carefully in the
    // Win32 API reference.
    //  Windows.StartPage(Printer.Handle);
    // Refresh the canvas, otherwise it ignores the font settings
    // Printer.Canvas.Refresh;
    GlobalUnlock( hDeviceMode );
    //   Canvas.font.size := 20;
    //  Canvas.font.name := 'Arial';
    //  Canvas.TextOut( 50, 50, 'This is landscape');
    finally
      EndDoc;
    end;
  end;
end; // DoPrinterOrientation
}

// TD-22323 Duplex
procedure TReportBaseF.DeterminePrinterSettings(var ADuplex: Boolean);
var
  Device, Driver, Port: array[0..80] of Char;
  DevMode: THandle;
  pDevmode: PDeviceMode;
//  i: Integer;
  MyPrinterIndex: Integer;
{  function GetDefaultPrinterName: String;
  begin
     if (Printer.PrinterIndex > 0) then
     begin
       Result :=
         Printer.Printers[Printer.PrinterIndex];
     end
     else
     begin
       Result := '';
     end;
  end; }
begin
  MyPrinterIndex := Printer.PrinterIndex;
  ADuplex := False;
  ExtendedDuplex := 0;
{
  if APrinterName <> '' then
    for i:=0 to Printer.Printers.Count-1 do
      if APrinterName = Printer.Printers[i] then
      begin
        MyPrinterIndex := i;
        Break;
      end;
}
  Printer.PrinterIndex := MyPrinterIndex;
  // Get printer device name etc.
  Printer.GetPrinter(Device, Driver, Port, DevMode);
  // force reload of DEVMODE
  Printer.SetPrinter(Device, Driver, Port, 0) ;
  // get DEVMODE handle
  Printer.GetPrinter(Device, Driver, Port, DevMode);
  if Devmode <> 0 then
  begin
    // lock it to get pointer to DEVMODE record
    pDevMode := GlobalLock(Devmode);
    if pDevmode <> nil then
    try
//      DEBUG('Printer Name=' + pDevmode^.dmDeviceName);
//      DEBUG('Form Name=' + pDevmode^.dmFormName);
//      DEBUG('Papersize = ' + IntToStr(pDevmode^.dmPapersize));
//      DEBUG('Duplex=' + IntToStr(pDevmode^.dmDuplex));
      case pDevmode^.dmDuplex of
      DMDUP_SIMPLEX:
        begin
//          DEBUG('DuplexMode=DMDUP_SIMPLEX');
          ADuplex := False;
          ExtendedDuplex := DMDUP_SIMPLEX;
        end;
      DMDUP_HORIZONTAL:
        begin
//          DEBUG('DuplexMode=DMDUP_HORIZONTAL');
          ADuplex := True;
          ExtendedDuplex := DMDUP_HORIZONTAL;
        end;
      DMDUP_VERTICAL:
        begin
//          DEBUG('DuplexMode=DMDUP_VERTICAL');
          ADuplex := True;
          ExtendedDuplex := DMDUP_VERTICAL;
        end;
      end;


{
      // modify paper size
      DEBUG('Old dmPapersize = ' + IntToStr(pDevmode^.dmPapersize));

      if MyPaperSize = 0 then
        pDevmode^.dmPaperSize := DMPAPER_LETTER
      else
        pDevmode^.dmPapersize := DMPAPER_A4;

      DEBUG('New dmPapersize = ' + IntToStr(pDevmode^.dmPapersize));
      // tell printer driver that dmPapersize field contains
      // data it needs to inspect.
      pDevmode^.dmFields := pDevmode^.dmFields or DM_PAPERSIZE;
}
    finally
     // unlock devmode handle.
     GlobalUnlock( Devmode );
    end;
  end; { If }
end; // DeterminePrinterSettings

// TD-22323 Duplex
function TReportBaseF.DeterminePrinterSettingDuplex: Boolean;
var
  Duplex: Boolean;
begin
  try
    Duplex := False;
    DeterminePrinterSettings(Duplex);
  except
  end;
  Result := Duplex;
end; // DeterminePrinterSettingDuplex

// TD-22323 Duplex
procedure TReportBaseF.DoSetPrinterDuplex;
begin
  qrptBase.PrinterSettings.Duplex := DeterminePrinterSettingDuplex;
  qrptBase.PrinterSettings.ExtendedDuplex := ExtendedDuplex;
end;

// 20015223
function TReportBaseF.InclExclWorkspotsAll: Boolean;
begin
  Result := (InclExclWorkspotsResult = SPimsAllWorkspots) or
    (InclExclWorkspotsResult = SPimsMultipleWorkspots);
end;

// 20015223
function TReportBaseF.DisplayWorkspotFrom(AValue: String): String;
begin
  Result := AValue;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    if (AValue = SPimsAllWorkspots) or (AValue = SPimsMultipleWorkspots) then
      Result := AValue;
end;

// 20015223
function TReportBaseF.DisplayWorkspotTo(AValue: String): String;
begin
  Result := AValue;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    if (AValue = SPimsAllWorkspots) or (AValue = SPimsMultipleWorkspots) then
      Result := '...';
end;

// ABS-27052
procedure TReportBaseF.DoSetDefaultPrinter;
begin
  try
    qrptBase.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
  except
  end;
end; // DoSetDefaultPrinter;

initialization
  OldApp := Application;
  OldScr := Screen;
// Restore the original application and screen variables
finalization
  Application := OldApp;
  Screen := OldScr;
end.
