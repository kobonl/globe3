inherited MistakePerEmplF: TMistakePerEmplF
  Left = 215
  Top = 172
  Width = 785
  Height = 514
  Caption = 'Mistakes per employee'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 126
    Width = 777
    Height = 11
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 7
      Width = 775
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Width = 775
      Height = 6
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 366
    Width = 777
    Height = 121
    TabOrder = 3
    object Label4: TLabel
      Left = 24
      Top = 24
      Width = 86
      Height = 13
      Caption = 'Employee Number'
    end
    object Label5: TLabel
      Left = 24
      Top = 64
      Width = 94
      Height = 13
      Caption = 'Number of mistakes'
    end
    object DBEditEmp: TDBEdit
      Tag = 1
      Left = 128
      Top = 22
      Width = 121
      Height = 21
      DataField = 'EMPLOYEE_NUMBER'
      DataSource = MistakePerEmplDM.DataSourceDetail
      Enabled = False
      TabOrder = 0
    end
    object DBEditDesd: TDBEdit
      Tag = 1
      Left = 264
      Top = 22
      Width = 241
      Height = 21
      DataField = 'DESCRIPTION'
      DataSource = MistakePerEmplDM.DataSourceDetail
      Enabled = False
      TabOrder = 1
    end
    object EditMistake: TEdit
      Left = 128
      Top = 62
      Width = 121
      Height = 21
      TabOrder = 2
      OnKeyDown = EditMistakeKeyDown
      OnKeyPress = EditMistakeKeyPress
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 137
    Width = 777
    Height = 229
    inherited spltDetail: TSplitter
      Top = 225
      Width = 775
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 775
      Height = 224
      Bands = <
        item
          Caption = 'Employee'
          Width = 458
        end
        item
          Caption = 'Number of mistakes'
          Width = 299
        end>
      DefaultLayout = False
      KeyField = 'EMPLOYEE_NUMBER'
      OnExit = dxDetailGridExit
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Number'
        DisableEditor = True
        Width = 108
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 350
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnDate: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Date '
        DisableEditor = True
        Width = 155
        BandIndex = 1
        RowIndex = 0
        FieldName = 'NUMBER_OF_MISTAKE'
      end
      object dxDetailGridColumnWeek: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Total week'
        DisableEditor = True
        Width = 144
        BandIndex = 1
        RowIndex = 0
        FieldName = 'WEEKMISTAKE'
      end
    end
  end
  object pnlTeamPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 777
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 777
      Height = 100
      Align = alClient
      Caption = 'Team/Plant/Shift'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 26
        Height = 13
        Caption = 'Team'
      end
      object Label2: TLabel
        Left = 8
        Top = 56
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label3: TLabel
        Left = 384
        Top = 24
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object cmbPlusTeam: TComboBoxPlus
        Left = 64
        Top = 22
        Width = 201
        Height = 19
        ColCount = 45
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        ParentCtl3D = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnChange = cmbPlusTeamChange
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 64
        Top = 54
        Width = 201
        Height = 19
        ColCount = 45
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        ParentCtl3D = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnChange = cmbPlusPlantChange
      end
      object CheckBoxAllTeam: TCheckBox
        Left = 280
        Top = 24
        Width = 73
        Height = 17
        Caption = 'All teams'
        TabOrder = 2
        OnClick = CheckBoxAllTeamClick
      end
      object DateFrom: TDateTimePicker
        Left = 424
        Top = 22
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 3
        OnChange = DateFromChange
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 664
    Top = 80
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    DataSource = nil
    Left = 736
    Top = 80
  end
  inherited StandardMenuActionList: TActionList
    Left = 648
    Top = 32
  end
  inherited dsrcActive: TDataSource
    Left = 704
    Top = 40
  end
end
