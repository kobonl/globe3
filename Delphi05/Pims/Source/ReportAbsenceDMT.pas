(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportAbsenceDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, Provider, DBClient;

type
  TReportAbsenceDM = class(TReportBaseDM)
    QueryAbsence: TQuery;
    DataSourceAbsence: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryAbsenceFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportAbsenceDM: TReportAbsenceDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportAbsenceDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryAbsence);
end;

procedure TReportAbsenceDM.QueryAbsenceFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
