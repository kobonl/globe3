inherited ExtraPaymentDM: TExtraPaymentDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    Top = 16
  end
  inherited TableDetail: TTable
    OnNewRecord = TableDetailNewRecord
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    TableName = 'EXTRAPAYMENT'
    Top = 84
    object TableDetailPAYMENT_DATE: TDateTimeField
      FieldName = 'PAYMENT_DATE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailPAYMENT_EXPORT_CODE: TStringField
      FieldName = 'PAYMENT_EXPORT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailPAYMENT_AMOUNT: TFloatField
      FieldName = 'PAYMENT_AMOUNT'
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailEMPLU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLU'
      LookupDataSet = TableEmployee
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailPAYMENTEXPCODELU: TStringField
      FieldKind = fkLookup
      FieldName = 'PAYMENTEXPCODELU'
      LookupDataSet = TablePaymentExpCode
      LookupKeyFields = 'PAYMENT_EXPORT_CODE'
      LookupResultField = 'PAYMENT_EXPORT_CODE'
      KeyFields = 'PAYMENT_EXPORT_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Top = 84
  end
  object QueryDetail: TQuery
    ObjectView = True
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    RequestLive = True
    SQL.Strings = (
      'SELECT '
      '  EP.PAYMENT_DATE, '
      '  E.EMPLOYEE_NUMBER,  E.DESCRIPTION, '
      '  EP.PAYMENT_EXPORT_CODE, EP.PAYMENT_AMOUNT'
      'FROM '
      '  EXTRAPAYMENT EP, PAYMENTEXPORTCODE PE,'
      '  EMPLOYEE E'
      'WHERE '
      '  EP.PAYMENT_EXPORT_CODE  = '
      '  PE.PAYMENT_EXPORT_CODE AND'
      '  EP.EMPLOYEE_NUMBER = '
      '  E.EMPLOYEE_NUMBER AND'
      '  EP.PAYMENT_DATE = :FDATE')
    Left = 328
    Top = 96
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
    object QueryDetailPAYMENT_DATE: TDateTimeField
      FieldName = 'PAYMENT_DATE'
    end
    object QueryDetailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object QueryDetailPAYMENT_EXPORT_CODE: TStringField
      FieldName = 'PAYMENT_EXPORT_CODE'
      Size = 6
    end
    object QueryDetailPAYMENT_AMOUNT: TFloatField
      FieldName = 'PAYMENT_AMOUNT'
    end
  end
  object TableEmployee: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    TableName = 'EMPLOYEE'
    Left = 88
    Top = 136
  end
  object DataSourceEmpl: TDataSource
    DataSet = TableEmployee
    Left = 216
    Top = 140
  end
  object TablePaymentExpCode: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PAYMENTEXPORTCODE'
    Left = 80
    Top = 200
  end
  object DataSourcePaymentExpCode: TDataSource
    DataSet = TablePaymentExpCode
    Left = 240
    Top = 212
  end
end
