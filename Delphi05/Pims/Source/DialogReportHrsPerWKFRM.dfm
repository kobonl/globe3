inherited DialogReportHrsPerWKF: TDialogReportHrsPerWKF
  Left = 265
  Top = 181
  Caption = 'Report hours per workspot'
  ClientHeight = 483
  ClientWidth = 579
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 579
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 283
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 579
    Height = 362
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 152
    end
    inherited LblEmployee: TLabel
      Top = 152
    end
    inherited LblToPlant: TLabel
      Top = 153
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 156
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 156
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [15]
      Left = 16
      Top = 477
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [16]
      Left = 16
      Top = 394
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [17]
      Left = 315
      Top = 71
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [18]
      Left = 128
      Top = 153
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [19]
      Left = 336
      Top = 153
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [20]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [21]
      Left = 136
      Top = 395
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [22]
      Left = 344
      Top = 392
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [23]
      Left = 128
      Top = 477
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [24]
      Left = 336
      Top = 477
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [25]
      Left = 48
      Top = 394
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [26]
      Left = 48
      Top = 477
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [27]
      Left = 323
      Top = 395
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [28]
      Left = 315
      Top = 477
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object LblFromDate: TLabel [29]
      Left = 8
      Top = 177
      Width = 24
      Height = 13
      Caption = 'From'
    end
    object LblDate: TLabel [30]
      Left = 40
      Top = 177
      Width = 23
      Height = 13
      Caption = 'Date'
    end
    object LblToDate: TLabel [31]
      Left = 315
      Top = 183
      Width = 10
      Height = 13
      Caption = 'to'
    end
    inherited LblFromDepartment: TLabel
      Top = 69
    end
    inherited LblDepartment: TLabel
      Top = 69
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    inherited LblStarJobTo: TLabel
      Top = 125
    end
    inherited LblStarJobFrom: TLabel
      Top = 125
    end
    inherited LblJob: TLabel
      Top = 125
    end
    inherited LblFromJob: TLabel
      Top = 125
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus [69]
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 129
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus [70]
      Left = 144
      Top = 393
      Width = 180
      Height = 19
      ColCount = 133
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 33
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus [71]
      Left = 358
      Top = 393
      Width = 180
      Height = 19
      ColCount = 134
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 16
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object DateTo: TDateTimePicker [72]
      Left = 335
      Top = 179
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 15
      OnCloseUp = DateToCloseUp
    end
    object DateFrom: TDateTimePicker [73]
      Left = 120
      Top = 179
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 14
      OnCloseUp = DateFromCloseUp
    end
    object GroupBoxSelection: TGroupBox [74]
      Left = 152
      Top = 224
      Width = 369
      Height = 132
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 32
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 24
        Width = 169
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 52
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 192
        Top = 24
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 4
      end
      object CheckBoxPageWK: TCheckBox
        Left = 192
        Top = 52
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 5
      end
      object CheckBoxPageJob: TCheckBox
        Left = 192
        Top = 79
        Width = 169
        Height = 17
        Caption = 'New page per jobcode'
        TabOrder = 6
      end
      object CheckBoxPageBU: TCheckBox
        Left = 8
        Top = 79
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 2
      end
      object CheckBoxExport: TCheckBox
        Left = 192
        Top = 104
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 7
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 8
        Top = 104
        Width = 169
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 3
      end
    end
    object GroupBoxShow: TGroupBox [75]
      Left = 8
      Top = 224
      Width = 137
      Height = 132
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 31
      object CheckBoxBU: TCheckBox
        Left = 8
        Top = 20
        Width = 121
        Height = 17
        Caption = 'Business unit'
        TabOrder = 0
        OnClick = CheckBoxBUClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 102
        Width = 113
        Height = 17
        Caption = 'Employee'
        TabOrder = 4
      end
      object CheckBoxJobcode: TCheckBox
        Left = 8
        Top = 82
        Width = 121
        Height = 17
        Caption = 'Jobcode'
        TabOrder = 3
        OnClick = CheckBoxJobcodeClick
      end
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 41
        Width = 113
        Height = 17
        Caption = 'Department'
        TabOrder = 1
        OnClick = CheckBoxDeptClick
      end
      object CheckBoxWK: TCheckBox
        Left = 8
        Top = 61
        Width = 97
        Height = 17
        Caption = 'Workspot'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CheckBoxWKClick
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus [76]
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 128
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 141
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 142
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 138
      TabOrder = 17
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 139
      TabOrder = 18
    end
    inherited CheckBoxAllTeams: TCheckBox
      TabOrder = 29
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 68
      ColCount = 140
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 68
      ColCount = 141
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 71
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 152
      TabOrder = 11
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 152
      TabOrder = 12
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 138
      TabOrder = 24
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 139
      TabOrder = 25
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 26
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 203
      TabOrder = 19
      Visible = True
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 521
      Top = 153
      TabOrder = 13
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 30
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      TabOrder = 27
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      TabOrder = 28
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 96
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 96
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      TabOrder = 34
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 125
      ColCount = 154
      TabOrder = 10
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 125
      ColCount = 153
      TabOrder = 9
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 21
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 23
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 20
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 22
    end
  end
  inherited stbarBase: TStatusBar
    Top = 423
    Width = 579
  end
  inherited pnlBottom: TPanel
    Top = 442
    Width = 579
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 264
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        50040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507284469616C6F675265706F7274487273506572574B462E44
        617461536F75726365456D706C46726F6D104F7074696F6E73437573746F6D69
        7A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E
        67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A
        696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B1065
        64676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564
        676F43616E496E73657274116564676F43616E4E617669676174696F6E116564
        676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72
        6473106564676F557365426F6F6B6D61726B7300000F54647844424772696443
        6F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D62
        657206536F7274656407046373557005576964746802400942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F454D504C4F59
        45455F4E554D42455200000F546478444247726964436F6C756D6E0F436F6C75
        6D6E53686F72744E616D650743617074696F6E060A53686F7274206E616D6505
        576964746802540942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060A53484F52545F4E414D4500000F546478444247726964
        436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D6505
        576964746803B4000942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060B4445534352495054494F4E00000F54647844424772
        6964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E0607
        4164647265737305576964746802450942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D6506074144445245535300000F54647844
        4247726964436F6C756D6E0E436F6C756D6E44657074436F6465074361707469
        6F6E060F4465706172746D656E7420636F646505576964746802580942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F4445
        504152544D454E545F434F444500000F546478444247726964436F6C756D6E0A
        436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465055769
        647468023C0942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0F040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507264469616C6F675265706F72
        74487273506572574B462E44617461536F75726365456D706C546F104F707469
        6F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E656467
        6F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E6710656467
        6F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F70
        74696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F4361
        6E44656C6574650D6564676F43616E496E73657274116564676F43616E4E6176
        69676174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F
        6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F
        546478444247726964436F6C756D6E0A436F6C756D6E456D706C074361707469
        6F6E06064E756D62657206536F72746564070463735570055769647468023109
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C
        756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F
        7274206E616D65055769647468024E0942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F54
        6478444247726964436F6C756D6E11436F6C756D6E4465736372697074696F6E
        0743617074696F6E06044E616D6505576964746803CC000942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060B444553435249
        5054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E4164
        64726573730743617074696F6E06074164647265737305576964746802650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        074144445245535300000F546478444247726964436F6C756D6E0E436F6C756D
        6E44657074436F64650743617074696F6E060F4465706172746D656E7420636F
        64650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F4445504152544D454E545F434F444500000F546478444247726964
        436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20
        636F64650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited QueryEmplTo: TQuery
    Left = 368
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkSpot
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 320
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    Left = 488
    Top = 23
  end
end
