inherited ExceptHourF: TExceptHourF
  Left = 260
  Top = 180
  Height = 485
  Caption = 'Exceptional hours'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Contract groups'
          Width = 663
        end>
      KeyField = 'CONTRACTGROUP_CODE'
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 77
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACTGROUP_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 357
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnTime: TdxDBGridCheckColumn
        Caption = 'Time for time'
        Width = 81
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIME_FOR_TIME_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
      end
      object dxMasterGridColumnMoney: TdxDBGridCheckColumn
        Caption = 'Bonus in money'
        Width = 92
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_IN_MONEY_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
      end
      object dxMasterGridColumnReduction: TdxDBGridCheckColumn
        Caption = 'Work time reduction'
        Width = 108
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORK_TIME_REDUCTION_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 295
    Height = 152
    object LabelWeekDay: TLabel
      Left = 32
      Top = 16
      Width = 60
      Height = 13
      Caption = 'Day of week'
    end
    object LabelStartTime: TLabel
      Left = 32
      Top = 51
      Width = 47
      Height = 13
      Caption = 'Start time'
    end
    object LabelEndTime: TLabel
      Left = 32
      Top = 85
      Width = 41
      Height = 13
      Caption = 'End time'
    end
    object LabelHourTypeCode: TLabel
      Left = 296
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Hour type'
    end
    object Label1: TLabel
      Left = 296
      Top = 51
      Width = 193
      Height = 13
      Caption = 'Hour type when worked during overtime'
    end
    object dxDBTimeEditStartTime: TdxDBTimeEdit
      Tag = 1
      Left = 108
      Top = 47
      Width = 57
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'STARTTIME'
      DataSource = ExceptHourDM.DataSourceDetail
      TimeEditFormat = tfHourMin
      StoredValues = 4
    end
    object dxDBTimeEditEndTime: TdxDBTimeEdit
      Tag = 1
      Left = 108
      Top = 81
      Width = 57
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'ENDTIME'
      DataSource = ExceptHourDM.DataSourceDetail
      TimeEditFormat = tfHourMin
      StoredValues = 4
    end
    object dxDBLookupEditHour: TdxDBLookupEdit
      Tag = 1
      Left = 364
      Top = 12
      Width = 229
      Style.BorderStyle = xbsSingle
      TabOrder = 3
      DataField = 'HOURTYPELU'
      DataSource = ExceptHourDM.DataSourceDetail
      ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
    end
    object dxDBImgEdtDayOfWeek: TdxDBImageEdit
      Tag = 1
      Left = 109
      Top = 12
      Width = 148
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'DAY_OF_WEEK'
      DataSource = ExceptHourDM.DataSourceDetail
    end
    object dxDBLookupEditOvertimeHourtype: TdxDBLookupEdit
      Left = 364
      Top = 72
      Width = 229
      Style.BorderStyle = xbsSingle
      TabOrder = 4
      DataField = 'OVERTIME_HOURTYPELU'
      DataSource = ExceptHourDM.DataSourceDetail
      ClearKey = 46
      ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 140
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 136
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 135
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Exceptional hours'
          Width = 715
        end>
      KeyField = 'DAY_OF_WEEK'
      ShowBands = True
      object dxDetailGridColumnHourType: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 323
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxDetailGridColumnStartTime: TdxDBGridTimeColumn
        Caption = 'Start time'
        Width = 150
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTTIME'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumnEndTime: TdxDBGridTimeColumn
        Caption = 'End time'
        Width = 114
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENDTIME'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumnDay: TdxDBGridColumn
        Caption = 'Day of week'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DAYDESC'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonCopy: TdxBarButton
      Visible = ivAlways
      OnClick = mniCopyClick
    end
    inherited dxBarButtonPaste: TdxBarButton
      Enabled = False
      Visible = ivAlways
      OnClick = mniPasteClick
    end
  end
end
