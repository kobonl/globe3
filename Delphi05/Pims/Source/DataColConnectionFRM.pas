(*
  MRA:10-APR-2012 20012858 Personal Screen
  - Addition of Socket-fields: Host + Port.
  MRA:10-SEP-2012 TD-21191
  - Use check-digit for reading blackboxes.
  - An extra field 'Checkdigits' must be added for
    this purpose. It can be left empty.
    When a value is entered then it must be '1001' till
    '4200' (and always 4 positions).
  MRA:26-APR-2013 TD-22475
  - Add copy/paste buttons to make it possible to copy datacol-connections
    from one workstation to another.
*)
unit DataColConnectionFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, StdCtrls, Mask, DBCtrls,
  dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TDataColConnectionF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxDetailGridColumnCOMPORT: TdxDBGridSpinColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEditComputerName: TDBEdit;
    Label6: TLabel;
    dxDBSpinEditCOMPORT: TdxDBSpinEdit;
    dxDetailGridColumnSERIALDEVICE: TdxDBGridColumn;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    dxDetailGridColumnCOUNTER: TdxDBGridSpinColumn;
    DBRadioGroup1: TDBRadioGroup;
    Label2: TLabel;
    DBEditINTERFACE_CODE: TDBEdit;
    dxDetailGridColumnINTERFACE_CODE: TdxDBGridColumn;
    Label4: TLabel;
    dxDBSpinEditADDRESS: TdxDBSpinEdit;
    Label3: TLabel;
    DBLookupComboBoxSERIALDEVICE: TDBLookupComboBox;
    dxDetailGridColumnADDRESS: TdxDBGridSpinColumn;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label8: TLabel;
    DBEditIPADDRESS: TDBEdit;
    DBEditPORT: TDBEdit;
    dxDetailGridColumnIPADDRESS: TdxDBGridColumn;
    dxDetailGridColumnPORT: TdxDBGridColumn;
    Label9: TLabel;
    DBEditCheckDigits: TDBEdit;
    Label10: TLabel;
    dxDetailGridColumnCHECKDIGITS: TdxDBGridColumn;
    CopyAct: TAction;
    PasteAct: TAction;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure CopyActExecute(Sender: TObject);
    procedure PasteActExecute(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxGridEnter(Sender: TObject);
  private
    { Private declarations }
    FCopyComputerName: String;
    FPasteComputerName: String;
  public
    { Public declarations }
  end;

function
  DataColConnectionF: TDataColConnectionF;

implementation

{$R *.DFM}

uses
  SystemDMT, DataColConnectionDMT, UPimsMessageRes;

var
  DataColConnectionF_HND: TDataColConnectionF;

function DataColConnectionF: TDataColConnectionF;
begin
  if (DataColConnectionF_HND = nil) then
    DataColConnectionF_HND := TDataColConnectionF.Create(Application);
  Result := DataColConnectionF_HND;
end;

procedure TDataColConnectionF.FormDestroy(Sender: TObject);
begin
  inherited;
  DataColConnectionF_HND := nil;
end;

procedure TDataColConnectionF.FormCreate(Sender: TObject);
begin
  DataColConnectionDM := CreateFormDM(TDataColConnectionDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxMasterGrid.DataSource := DataColConnectionDM.DataSourceMaster;
    dxDetailGrid.DataSource := DataColConnectionDM.DataSourceDetail;
  end;
  inherited;
  dxDBSpinEditCOMPORT.DataSource := DataColConnectionDM.DataSourceDetail;
  dxDBSpinEditCOMPORT.DataField := 'COMPORT';
  dxDBSpinEditADDRESS.DataSource := DataColConnectionDM.DataSourceDetail;
  dxDBSpinEditADDRESS.DataField := 'ADDRESS';
end;

procedure TDataColConnectionF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDataColConnectionF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  if pnlDetail.Visible then
    DBLookupComboBoxSERIALDEVICE.SetFocus;
end;

procedure TDataColConnectionF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  if pnlDetail.Visible then
    DBLookupComboBoxSERIALDEVICE.SetFocus;
end;

// TD-22475
procedure TDataColConnectionF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  FPasteComputerName :=
    DataColConnectionDM.TableMaster.FieldByName('COMPUTER_NAME').AsString;
  dxBarButtonPaste.Enabled := (FCopyComputerName <> '') and
    (FCopyComputerName <> FPasteComputerName);
  if dxBarButtonPaste.Enabled then
    dxBarButtonPaste.Hint :=
      Format(SPimsPasteDatacolConnection,
        [FCopyComputerName])
  else
    dxBarButtonPaste.Hint := SPimsPaste;
end;

// TD-22475
procedure TDataColConnectionF.CopyActExecute(Sender: TObject);
begin
  inherited;
  FCopyComputerName := '';

  if (not DataColConnectionDM.TableMaster.Active) or
    DataColConnectionDM.TableMaster.Eof then Exit;

  FCopyComputerName :=
    DataColConnectionDM.TableMaster.FieldByName('COMPUTER_NAME').AsString;

  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste;
end;

// TD-22475
procedure TDataColConnectionF.PasteActExecute(Sender: TObject);
var
  DataColConnectionDefined: Boolean;
begin
  inherited;
  FPasteComputerName := '';

  if (not DataColConnectionDM.TableMaster.Active) or
    DataColConnectionDM.TableMaster.Eof then
    Exit;

  FPasteComputerName :=
    DataColConnectionDM.TableMaster.FieldByName('COMPUTER_NAME').AsString;

  DataColConnectionDefined :=
    DataColConnectionDM.DataColConnectionFor(FPasteComputerName);
  if DataColConnectionDefined then
    if DisplayMessage(SPimsDataColConnectionFound,
        mtConfirmation, [mbYes, mbNo]) = mrNo
    then
      Exit;

  DataColConnectionDM.CopyDataColConnection(FCopyComputerName,
    FPasteComputerName, DataColConnectionDefined);
end;

// TD-22475
procedure TDataColConnectionF.dxGridEnter(Sender: TObject);
begin
  inherited;
  if ActiveGrid = dxMastergrid then
  begin
    dxBarButtonCopy.Visible := ivAlways;
    dxBarButtonPaste.Visible := ivAlways;
  end
  else
  begin
    dxBarButtonCopy.Visible := ivNever;
    dxBarButtonPaste.Visible := ivNever;
  end;
end;

end.
