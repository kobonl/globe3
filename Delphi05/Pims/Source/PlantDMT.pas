(*
  Changes:
    MRA:23-SEP-2009 RV034.1.
      - Added field CUSTOMER_NUMBER (numeric) to PLANT-table.
        For use with export-payroll-ADP.
    MRA:5-JAN-2010 RV050.1. 889953.
      - Addition of field COUNTRY_ID (refers to COUNTRY-table).
        This can not be left empty.
    MRA:10-FEB-2011 RV086.1. SC-50014392.
    - Plan in other plants, related issues:
      - When teams-per-users-filtering is used, you still see
        plants/teams/department/workspots not belonging to the
        teams-per-user-selection.
    MRA:3-MAY-2011 RV092.7. Bugfix.
    - When changed the Code of the Plant, it gives an error
      during update all related tables.
      - Cause: Some tables were added later in Pims, but not added here.
    - Also: Some fields were added later in Pims, but not added here.
    - Also: Some triggers must be disabled/enabled during this action.
    MRA:20-MAY-2011 RV092.7. Bugfix.
    - See above.
    - Display message when something went wrong, instead of only logging.
    - Change MUTATIONDATE and MUTATOR for all changed records when
      PlantCode is changed.
    MRA:11-JUL-211 RV094.7. Bugfix
    - Gives different message in Dutch compared with English,
      when trying to change a record.
    - Show message when delete/modify is not allowed based
      on system setting (update plant-checkbox).
    MRA:4-FEB-2013 20013923 / TD-24853
    - Make separate settings for update/delete for plant/employee/workspot
    MRA:6-FEB-2014 20011800 Final Run System
    - Added table: TableFinalRun
    MRA:30-APR-2015 SO-20014450
    - Addition of two extra fields:
      - Cut off time shift date (hh:mm)
      - Real time current period (minutes)
    MRA:15-OCT-2015 PIM-12
    - Check for a correct value for REALTIMEINTERVAL.
    MRA:20-APR-2018 PIM-372
    - Update procedure that changes db because of plant-code-change
    - For example: Workspot has extra fields (host, port, batch)
    MRA:1-JUL-2019 PIM-377
    - Related to this order.
    - It gave 10259 Couldn't perform the edit because another user changed
      the record.
    - This happened after some tests where done with Chinese characters in
      on of the Plant-fields.
    - To solve this, change at TableDefail the UpdateMode to upWhereKeyOnly,
      instead of upWhereAll.
*)

unit PlantDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, DBClient;

type
  TPlantDM = class(TGridBaseDM)
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableMasterFAX: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterINSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterINSCAN_MARGIN_LATE: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterOUTSCAN_MARGIN_LATE: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailADDRESS: TStringField;
    TableDetailZIPCODE: TStringField;
    TableDetailCITY: TStringField;
    TableDetailSTATE: TStringField;
    TableDetailPHONE: TStringField;
    TableDetailFAX: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailINSCAN_MARGIN_EARLY: TIntegerField;
    TableDetailINSCAN_MARGIN_LATE: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableDetailOUTSCAN_MARGIN_LATE: TIntegerField;
    StoredProcDelete: TStoredProc;
    QueryWork: TQuery;
    StoredProcUpdateNewPlant: TStoredProc;
    StoredProcDelOldPlant: TStoredProc;
    QueryExec: TQuery;
    TableDetailAVERAGE_WAGE: TFloatField;
    TableDetailCUSTOMER_NUMBER: TIntegerField;
    TableDetailCOUNTRY_ID: TFloatField;
    qryCountry: TQuery;
    TableDetailCOUNTRYLU: TStringField;
    StoredProcNEWPlant: TStoredProc;
    TableFinalRun: TTable;
    TableFinalRunPLANT_CODE: TStringField;
    TableFinalRunEXPORT_TYPE: TStringField;
    TableFinalRunEXPORT_DATE: TDateTimeField;
    DataSourceFinalRun: TDataSource;
    TableFinalRunCREATIONDATE: TDateTimeField;
    TableFinalRunMUTATIONDATE: TDateTimeField;
    TableFinalRunMUTATOR: TStringField;
    TableFinalRunPLANTLU: TStringField;
    tblPlantLU: TTable;
    tblPlantLUPLANT_CODE: TStringField;
    tblPlantLUDESCRIPTION: TStringField;
    TableDetailTIMEZONELU: TStringField;
    TableDetailTIMEZONE: TMemoField;
    TableDetailTIMEZONECALC: TStringField;
    TableDetailTIMEZONEHRSDIFF: TSmallintField;
    TableDetailCUTOFFTIMESHIFTDATE: TDateTimeField;
    TableDetailREALTIMEINTERVAL: TFloatField;
    TableDetailSTAFFPLAN_SHOWTOT_YN: TStringField;
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailAfterScroll(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FOldPlant: String;
  public
    { Public declarations }
  end;

var
  PlantDM: TPlantDM;

implementation

uses SystemDMT,  UPimsConst,UPimsMessageRes, UGlobalFunctions, UTimeZone;

{$R *.DFM}

procedure TPlantDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  // 20013923
  if (SystemDM.DeletePlantYN = CHECKEDVALUE) then
  begin
    DefaultBeforeDelete(DataSet);
    if DisplayMessage(SConfirmDeleteCascadePlant, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
      Abort
    else
      if DataSet.FieldByName('PLANT_CODE').AsString <> '' then
      begin
        try
          SystemDM.Pims.StartTransaction;
          StoredProcDelete.ParamByName('PLANT_CODE').AsString :=
            DataSet.FieldByName('PLANT_CODE').AsString;
          StoredProcDelete.Prepare;
          StoredProcDelete.ExecProc;
          SystemDM.Pims.Commit;
        except on E:EDBEngineError do
          SystemDM.Pims.Rollback;
        end;
     end;
  end
  else
  begin // RV094.7.
    DisplayMessage(SPimsPlantDeleteNotAllowed, mtWarning, [mbOK]);
    Abort;
  end;
end;

procedure TPlantDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  FOldPlant := DataSet.FieldByName('PLANT_CODE').AsString;
end;

procedure TPlantDM.TableDetailBeforePost(DataSet: TDataSet);
var
  Wage, SelectSetStr: String;
  FNewPlant: String;
begin
  inherited;
  // RV094.7.
  if SystemDM.UpdatePlantYN <> CHECKEDVALUE then
  begin
    DisplayMessage(SPimsPlantUpdateNotAllowed, mtWarning, [mbOK]);
    Abort;
    Exit;
  end;
  // PIM-12 Check for a correct value here!
  try
    if TableDetailREALTIMEINTERVAL.AsString <> '' then
      if not ((TableDetailREALTIMEINTERVAL.Value >= 1) and
        (TableDetailREALTIMEINTERVAL.Value <= 30)) then
        TableDetailREALTIMEINTERVAL.Value := 5;
  except
    TableDetailREALTIMEINTERVAL.Value := 5;
  end;
  SystemDM.DefaultBeforePost(DataSet);
  //550272 - new field average wage
  Wage := Format('%*.*f', [3, 2, TableDetail.FieldByName('AVERAGE_WAGE').AsFloat]);
  TableDetail.FieldByName('AVERAGE_WAGE').AsFloat := StrToFloat(Wage);
  if (FOldPlant <> DataSet.FieldByName('PLANT_CODE').AsString)
    and (SystemDM.UpdatePlantYN = CHECKEDVALUE) and (FOldPlant <> '') then
  begin
    QueryWork.Close;
    QueryWork.ParamByName('PLANT_CODE').AsString :=
      DataSet.FieldByName('PLANT_CODE').AsString;
    QueryWork.Prepare;
    QueryWork.Open;
    if QueryWork.RecordCount = 1 then
    begin
      DisplayMessage(SPlantExist, mtWarning, [mbOK]);
      Abort;
    end;
    if DisplayMessage(SConfirmUpdateCascadePlant, mtConfirmation, [mbYes, mbNo])
      <> mrYes then
    begin
      DataSet.Cancel;
      Abort;
    end
    else
    begin // Move Plant
      try
        // Disable some triggers first.
        try
           ExecSQL(QueryExec, 'alter table TIMEREGSCANNING disable all triggers');
           ExecSQL(QueryExec, 'alter table SALARYHOURPEREMPLOYEE disable all triggers');
           ExecSQL(QueryExec, 'alter table PRODHOURPEREMPLOYEE disable all triggers');
           ExecSQL(QueryExec, 'alter table PRODHOURPEREMPLPERTYPE disable all triggers');
           ExecSQL(QueryExec, 'alter table ABSENCEHOURPEREMPLOYEE disable all triggers');
           ExecSQL(QueryExec, 'alter table MACHINE disable all triggers'); // PIM-372
        except
          // Ignore error
        end;
        try
          SystemDM.Pims.StartTransaction;
          FNewPlant :=  DataSet.FieldByName('PLANT_CODE').AsString;
          StoredProcNEWPlant.ParamByName('NEW_PLANT').AsString :=
            DataSet.FieldByName('PLANT_CODE').AsString;
          StoredProcNEWPlant.ParamByName('OLD_PLANT').AsString :=  FOldPlant;
          StoredProcNEWPlant.ParamByName('DESCRIPTION').AsString :=
            DataSet.FieldByName('DESCRIPTION').AsString;
          StoredProcNEWPlant.ParamByName('ADDRESS').AsString :=
            DataSet.FieldByName('ADDRESS').AsString;
          StoredProcNEWPlant.ParamByName('ZIPCODE').AsString :=
            DataSet.FieldByName('ZIPCODE').AsString;
          StoredProcNEWPlant.ParamByName('CITY').AsString :=
            DataSet.FieldByName('CITY').AsString;
          StoredProcNEWPlant.ParamByName('STATE').AsString :=
            DataSet.FieldByName('STATE').AsString;
          StoredProcNEWPlant.ParamByName('CREATIONDATE').AsDateTime :=
            DataSet.FieldByName('CREATIONDATE').AsDateTime;
          StoredProcNEWPlant.ParamByName('MUTATIONDATE').AsDateTime :=
            DataSet.FieldByName('MUTATIONDATE').AsDateTime;
          StoredProcNEWPlant.ParamByName('MUTATOR').AsString :=
            DataSet.FieldByName('MUTATOR').AsString;
          StoredProcNEWPlant.ParamByName('INSCAN_MARGIN_EARLY').AsInteger :=
            DataSet.FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
          StoredProcNEWPlant.ParamByName('INSCAN_MARGIN_LATE').AsInteger :=
            DataSet.FieldByName('INSCAN_MARGIN_LATE').AsInteger;
          StoredProcNEWPlant.ParamByName('OUTSCAN_MARGIN_EARLY').AsInteger :=
            DataSet.FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
          StoredProcNEWPlant.ParamByName('OUTSCAN_MARGIN_LATE').AsInteger :=
            DataSet.FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
          // RV092.7. Addition of fields. Stored proc had to be changed.
          StoredProcNEWPlant.ParamByName('AVERAGE_WAGE').AsFloat :=
            DataSet.FieldByName('AVERAGE_WAGE').AsFloat;
          StoredProcNEWPlant.ParamByName('CUSTOMER_NUMBER').AsInteger :=
            DataSet.FieldByName('CUSTOMER_NUMBER').AsInteger;
          StoredProcNEWPlant.ParamByName('COUNTRY_ID').AsInteger :=
            DataSet.FieldByName('COUNTRY_ID').AsInteger;
          // PIM-372 Addition of fields
          StoredProcNEWPlant.ParamByName('TIMEZONE').AsString :=
            DataSet.FieldByName('TIMEZONE').AsString;
          StoredProcNEWPlant.ParamByName('TIMEZONEHRSDIFF').AsInteger :=
            DataSet.FieldByName('TIMEZONEHRSDIFF').AsInteger;
          StoredProcNEWPlant.ParamByName('CUTOFFTIMESHIFTDATE').AsDateTime :=
            DataSet.FieldByName('CUTOFFTIMESHIFTDATE').AsDateTime;
          StoredProcNEWPlant.ParamByName('REALTIMEINTERVAL').AsInteger :=
            DataSet.FieldByName('REALTIMEINTERVAL').AsInteger;

          DataSet.Cancel;
          StoredProcNEWPlant.Prepare;
          StoredProcNEWPlant.ExecProc;

{         StoredProcUpdateNewPlant.ParamByName('NEW_PLANT').AsString :=  FNewPlant;
         StoredProcUpdateNewPlant.ParamByName('OLD_PLANT').AsString :=  FOldPlant;
         StoredProcUpdateNewPlant.Prepare;
         StoredProcUpdateNewPlant.ExecProc;}

          // RV092.7. This table does not have mutation-fields.
          SelectSetStr := 'SET PLANT_CODE = ''' +  DoubleQuote(FNewPlant) + '''' +
             ' WHERE PLANT_CODE = ''' + DoubleQuote(FOldPlant) + '''';
          ExecSql(QueryExec, 'UPDATE READINTODAYPLANNING    ' + SelectSetStr);
          // RV092.7. Also change MUTATIONDATE and MUTATOR to keep track
          //          of changes.
          SelectSetStr := 'SET PLANT_CODE = ''' +  DoubleQuote(FNewPlant) + '''' +
             ' , MUTATIONDATE = SYSDATE, MUTATOR = ''PIMSSYSTEM'' ' +
             ' WHERE PLANT_CODE = ''' + DoubleQuote(FOldPlant) + '''';
          ExecSql(QueryExec, 'UPDATE BREAKPEREMPLOYEE ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE BREAKPERDEPARTMENT ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE BREAKPERSHIFT   ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE TIMEBLOCKPEREMPLOYEE  ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE TIMEBLOCKPERSHIFT  ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE TIMEBLOCKPERDEPARTMENT  ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE ABSENCEHOURPEREMPLOYEE   ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE EMPLOYEEAVAILABILITY  ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE OCCUPATIONPLANNING  ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE EMPLOYEEPLANNING    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE SHIFTSCHEDULE   ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE STANDARDEMPLOYEEPLANNING   ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE STANDARDAVAILABILITY   ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE STANDARDOCCUPATION    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE PRODUCTIONQUANTITY    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE PRODHOURPEREMPLOYEE    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE PRODUCTIONSHIFTTOTAL    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE TIMEREGSCANNING     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE PRODHOURPEREMPLPERTYPE    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE WORKSPOTSPEREMPLOYEE    ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE WORKSPOTPERWORKSTATION     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE IGNOREINTERFACECODE     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE JOBCODE     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE COMPAREJOB     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE COUNTERVALUE     ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE BUSINESSUNITPERWORKSPOT      ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE EMPLOYEE      ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE DEPARTMENTPERTEAM       ' + SelectSetStr);
          ExecSql(QueryExec, 'UPDATE BUSINESSUNIT        ' + SelectSetStr);
          // RV092.7. Additional tables
          ExecSql(QueryExec, 'UPDATE TEAM ' + SelectSetStr);

          StoredProcDelOldPlant.ParamByName('OLD_PLANT').AsString :=  FOldPlant;
          StoredProcDelOldPlant.Prepare;
          StoredProcDelOldPlant.ExecProc;

          DataSet.Refresh;
          TableDetail.FindKey([DataSet.FieldByName('PLANT_CODE').AsString]);
          SystemDM.Pims.Commit;
        except on E:EDBEngineError do
          begin
            WErrorLog(E.Message);
            SystemDM.Pims.Rollback;
            // RV092.7.
            DisplayMessage(E.Message, mtError, [mbOK]);
          end;
        end; // try-except
        FOldPlant := DataSet.FieldByName('PLANT_CODE').AsString;
        Abort;
      finally
        // Enable some triggers.
        try
           ExecSQL(QueryExec, 'alter table TIMEREGSCANNING enable all triggers');
           ExecSQL(QueryExec, 'alter table SALARYHOURPEREMPLOYEE enable all triggers');
           ExecSQL(QueryExec, 'alter table PRODHOURPEREMPLOYEE enable all triggers');
           ExecSQL(QueryExec, 'alter table PRODHOURPEREMPLPERTYPE enable all triggers');
           ExecSQL(QueryExec, 'alter table ABSENCEHOURPEREMPLOYEE enable all triggers');
           ExecSQL(QueryExec, 'alter table MACHINE enable all triggers'); // PIM-372
        except
          // Ignore error
        end;
      end; // try-finally
    end; // Move Plant
  end; // if (FOldPlant <> DataSet.FieldByName('PLANT_CODE').AsString)
  FOldPlant := DataSet.FieldByName('PLANT_CODE').AsString;
end;

procedure TPlantDM.DefaultNewRecord(DataSet: TDataSet);
begin
  inherited;
  FOldPlant := '';
  // RV050.1. Set default the value from code = '-'.
  if qryCountry.Locate('CODE', '-', []) then
    TableDetailCOUNTRY_ID.Value := qryCountry.FieldByName('COUNTRY_ID').Value;
  TableDetailCUTOFFTIMESHIFTDATE.Value := EncodeTime(4, 0, 0, 0); // 20014450
  TableDetailREALTIMEINTERVAL.Value := 5; // 20014450
end;

procedure TPlantDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV086.1.
  SystemDM.PlantTeamFilterEnable(TableDetail);
  qryCountry.Open;
  tblPlantLU.Open;
end;

procedure TPlantDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  tblPlantLU.Close;
  qryCountry.Close;
end;

procedure TPlantDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.1.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TPlantDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.PlantTimezonehrsdiffWriteToDB; // 20016449
  SystemDM.DetermineWorkstationTimezonehrsdiff; // 20016449
end;

procedure TPlantDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  try
    TableDetailTIMEZONECALC.Value := Copy(TableDetailTIMEZONE.Value, 1, 80);
  except
    TableDetailTIMEZONECALC.Value := '';
  end;
end;

end.
