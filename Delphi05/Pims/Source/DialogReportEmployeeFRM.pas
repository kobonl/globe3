(*
  MRA:7-DEC-2009 RV048.2. 889945.
  - Report Employee condensed:
    - Add selection: 'Show Contract Type'
      - Permanent/Temporary/External (0/1/2) (Check-buttons).
      This is stored in employee contracts.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogReportEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxLayout, dxCntner, dxEditor,
  dxExEdtr, dxExGrEd, dxExELib, Provider, DBClient, SystemDMT, dxEdLib;

type
  TDialogReportEmplF = class(TDialogReportBaseF)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DateActiveFrom: TDateTimePicker;
    DateInactiveFrom: TDateTimePicker;
    DateActiveTo: TDateTimePicker;
    DateInactiveTo: TDateTimePicker;
    CheckBoxAllDateActive: TCheckBox;
    CheckBoxAllDateInactive: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label21: TLabel;
    Label13: TLabel;
    LblFromContractEndDate: TLabel;
    LblContractEndDate: TLabel;
    LblToContractEndDate: TLabel;
    DateContractEndDateFrom: TDateTimePicker;
    DateContractEndDateTo: TDateTimePicker;
    CheckBoxAllContractEndDate: TCheckBox;
    CheckBoxAllDepartment: TCheckBox;
    CheckBoxAllTeam: TCheckBox;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    RadioGroupSort: TRadioGroup;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    gboxShowContractType: TGroupBox;
    cBoxPermanent: TCheckBox;
    cBoxTemporary: TCheckBox;
    cBoxExternal: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxAllDateActiveClick(Sender: TObject);
    procedure CheckBoxAllDateInactiveClick(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    procedure DateVisible;
//    procedure TeamVisible(Vis: Boolean);
    procedure TeamUpdate;
//    procedure DepartmentVisible(Vis: Boolean);
//    procedure DepartmentUpdate;
  public
    { Public declarations }
  end;

var
  DialogReportEmplF: TDialogReportEmplF;

// RV089.1.
function DialogReportEmplForm: TDialogReportEmplF;

implementation

{$R *.DFM}
uses
  ReportEmplQRPT, ReportEmplDMT, UPimsMessageRes, ListProcsFRM;

// RV089.1.
var
  DialogReportEmplF_HND: TDialogReportEmplF;

// RV089.1.
function DialogReportEmplForm: TDialogReportEmplF;
begin
  if (DialogReportEmplF_HND = nil) then
  begin
    DialogReportEmplF_HND := TDialogReportEmplF.Create(Application);
    with DialogReportEmplF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportEmplF_HND;
end;

// RV089.1.
procedure TDialogReportEmplF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportEmplF_HND <> nil) then
  begin
    DialogReportEmplF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportEmplF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportEmplF.btnOkClick(Sender: TObject);
var
  DateAFrom, DateATo, DateIFrom, DateITo: TDateTime;
begin
  inherited;
  if not CheckBoxAllDateActive.Checked then
    if DateActiveFrom.DateTime > DateActiveTo.DateTime then
    begin
      DisplayMessage(SPimsStartEndActiveDate, mtInformation, [mbYes]);
      Exit;
    end;
  if not CheckBoxAllDateInactive.Checked then
    if DateInactiveFrom.DateTime > DateInactiveTo.DateTime then
    begin
      DisplayMessage(SPimsStartEndInActiveDate, mtInformation, [mbYes]);
      Exit;
    end;
  DateAFrom := GetDate(DateActiveFrom.Date);
  DateATo := GetDate(DateActiveTo.Date);
  DateIFrom := GetDate(DateInActiveFrom.Date);
  DateITo := GetDate(DateInActiveTo.Date);
  if ReportEmplQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    //550284
    DateContractEndDateFrom.Date,
    DateContractEndDateTo.Date,
    DateAFrom,
    DateATo,
    DateIFrom,
    DateITo,
    CheckBoxAllDepartments.Checked,
    CheckBoxAllTeams.Checked,
    CheckBoxAllContractEndDate.Checked,
    CheckBoxAllDateActive.Checked,
    CheckBoxAllDateInActive.Checked,
    RadioGroupSort.ItemIndex,
    CBoxShowSelection.Checked,
    CheckBoxExport.Checked,
    cBoxPermanent.Checked,
    cBoxTemporary.Checked,
    cBoxExternal.Checked) then
    ReportEmplQR.ProcessRecords;
end;

procedure TDialogReportEmplF.DateVisible;
begin
  DateActiveFrom.Visible := not CheckBoxAllDateActive.Checked;
  DateActiveTo.Visible := not CheckBoxAllDateActive.Checked;

  DateInActiveFrom.Visible := not CheckBoxAllDateInActive.Checked;
  DateInActiveTo.Visible := not CheckBoxAllDateInActive.Checked;

  DateContractEndDateFrom.Visible := not CheckBoxAllContractEndDate.Checked;
  DateContractEndDateTo.Visible := not CheckBoxAllContractEndDate.Checked;
end;

procedure TDialogReportEmplF.FormShow(Sender: TObject);
//550284
var
  DateNow: TDateTime;
begin
  InitDialog(True, True, True, True, False, False, False);
  inherited;
  DateVisible;
  DateNow := Now();
  DateActiveFrom.DateTime := DateNow;
  DateActiveTo.DateTime := DateNow;
  DateInactiveFrom.DateTime := DateNow;
  DateInactiveTo.DateTime := DateNow;

  DateContractEndDateFrom.DateTime := DateNow;
  DateContractEndDateTo.DateTime := DateNow;

//  DepartmentUpdate;
  TeamUpdate;
end;

procedure TDialogReportEmplF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportEmplDM := CreateReportDM(TReportEmplDM);
  ReportEmplQR := CreateReportQR(TReportEmplQR);
end;

procedure TDialogReportEmplF.CheckBoxAllDateActiveClick(Sender: TObject);
begin
  inherited;
  DateVisible;
end;

procedure TDialogReportEmplF.CheckBoxAllDateInactiveClick(Sender: TObject);
begin
  inherited;
  DateVisible;
end;

procedure TDialogReportEmplF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
//  DepartmentUpdate;
end;

procedure TDialogReportEmplF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
//  DepartmentUpdate;
end;

{
procedure TDialogReportEmplF.DepartmentVisible(Vis: Boolean);
begin
//  CmbPlusDeptFrom.Visible := Vis;
//  CmbPlusDeptTo.Visible := Vis;
end;
}
{
procedure TDialogReportEmplF.DepartmentUpdate;
begin
  if CheckBoxAllDepartment.Checked then
    DepartmentVisible(False)
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
      (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      DepartmentVisible(True);
      ListProcsF.FillComboBoxMasterPlant(
        QueryDept, CmbPlusDeptFrom, GetStrValue(CmbPlusPlantFrom.Value),
        'DEPARTMENT_CODE', True);
      ListProcsF.FillComboBoxMasterPlant(
        QueryDept, CmbPlusDeptTo, GetStrValue(CmbPlusPlantFrom.Value),
        'DEPARTMENT_CODE', False);
    end
    else
      DepartmentVisible(False);
  end;
end;
}
{
procedure TDialogReportEmplF.TeamVisible(Vis: Boolean);
begin
  CmbPlusTeamFrom.Visible := Vis;
  CmbPlusTeamTo.Visible := Vis;
end;
}
procedure TDialogReportEmplF.TeamUpdate;
begin
{
  if CheckBoxAllTeam.Checked then
    TeamVisible(False)
  else
  begin
    ListProcsF.FillComboBox(QueryTeam,
      CmbPlusTeamFrom, '', '', True, '', '', 'TEAM_CODE', 'DESCRIPTION');
    ListProcsF.FillComboBox(QueryTeam,
      CmbPlusTeamTo, '', '', False, '', '', 'TEAM_CODE', 'DESCRIPTION');
    TeamVisible(True);
  end;
}
end;

procedure TDialogReportEmplF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
{  TeamUpdate; }
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportEmplF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogReportEmplF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
