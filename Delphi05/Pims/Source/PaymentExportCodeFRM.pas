(*
  MRA:22-FEB-2019 GLOB3-265
  - Payment export codes dialog shows double records in grid
  - Probable cause: Use of uppercase/lowercase for codes.
  - Code is now made uppercase in the dialog.
*)
unit PaymentExportCodeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, DBTables;

type
  TPaymentExportCodeF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    DBEditCode: TDBEdit;
    DBEditName: TDBEdit;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function PaymentExportCodeF: TPaymentExportCodeF;

implementation

{$R *.DFM}

uses
  SystemDMT, PaymentExportCodeDMT;

var
  PaymentExportCodeF_HND: TPaymentExportCodeF;

function PaymentExportCodeF: TPaymentExportCodeF;
begin
  if (PaymentExportCodeF_HND = nil) then
    PaymentExportCodeF_HND := TPaymentExportCodeF.Create(Application);
  Result := PaymentExportCodeF_HND;
end;

procedure TPaymentExportCodeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

procedure TPaymentExportCodeF.FormDestroy(Sender: TObject);
begin
  inherited;
  PaymentExportCodeF_HND := nil;
end;

procedure TPaymentExportCodeF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

procedure TPaymentExportCodeF.FormCreate(Sender: TObject);
begin
  PaymentExportCodeDM := CreateFormDM(TPaymentExportCodeDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := PaymentExportCodeDM.DataSourceDetail;
  inherited;
  
end;

procedure TPaymentExportCodeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

end.
