unit DialogSelectionCopyFromOWFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib;

type
  TDialogSelectionCopyFromOWF = class(TDialogSelectionF)
    GroupBox6: TGroupBox;
    Label1: TLabel;
    dxSpinEditWeek: TdxSpinEdit;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FWeekNumber: Integer;
    FMaxWeekNumber: Integer;
  public
    { Public declarations }
    property WeekNumber: Integer read FWeekNumber write FWeekNumber;
    property MaxWeekNumber: Integer read FMaxWeekNumber write FMaxWeekNumber;
  end;

var
  DialogSelectionCopyFromOWF: TDialogSelectionCopyFromOWF;

implementation

{$R *.DFM}

procedure TDialogSelectionCopyFromOWF.FormShow(Sender: TObject);
begin
  inherited;
  dxSpinEditWeek.MaxValue := MaxWeekNumber;
  dxSpinEditWeek.MinValue := 0;
  dxSpinEditWeek.IntValue := WeekNumber;
end;

procedure TDialogSelectionCopyFromOWF.btnOkClick(Sender: TObject);
begin
  inherited;
  WeekNumber := dxSpinEditWeek.IntValue;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogSelectionCopyFromOWF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
