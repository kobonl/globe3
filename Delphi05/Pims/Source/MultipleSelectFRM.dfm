inherited DialogMultipleSelectF: TDialogMultipleSelectF
  Left = 261
  Top = 117
  Caption = 'Multiple Select'
  ClientWidth = 556
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 556
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 439
      Visible = False
    end
    object lblTitle: TLabel
      Left = 8
      Top = 6
      Width = 139
      Height = 23
      Caption = 'Select / Unselect'
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 556
    Caption = ''
    Font.Color = clBtnText
    Font.Height = -13
    TabOrder = 4
    object lblAvailable: TLabel
      Left = 24
      Top = 40
      Width = 51
      Height = 16
      Caption = 'Available'
    end
    object lblSelected: TLabel
      Left = 304
      Top = 40
      Width = 49
      Height = 16
      Caption = 'Selected'
    end
    object lblCountry: TLabel
      Left = 24
      Top = 8
      Width = 44
      Height = 16
      Caption = 'Country'
    end
    object dxTLAvailable: TdxTreeList
      Left = 24
      Top = 56
      Width = 233
      Height = 289
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      TabOrder = 0
      Options = [aoColumnSizing, aoEditing, aoTabThrough, aoRowSelect, aoAutoSort]
      OptionsEx = [aoUseBitmap, aoBandHeaderWidth, aoAutoCalcPreviewLines, aoBandSizing, aoBandMoving, aoDragScroll, aoDragExpand, aoAutoSortRefresh]
      TreeLineColor = clGrayText
      ShowGrid = True
      ShowRoot = False
      object dxTLAvailableCode: TdxTreeListColumn
        Caption = 'Code'
        Sorted = csUp
        Width = 54
        BandIndex = 0
        RowIndex = 0
      end
      object dxTLAvailableDescription: TdxTreeListColumn
        Caption = 'Description'
        Width = 147
        BandIndex = 0
        RowIndex = 0
      end
      object dxTLAvailableId: TdxTreeListColumn
        Visible = False
        Width = 20
        BandIndex = 0
        RowIndex = 0
      end
    end
    object dxTLSelected: TdxTreeList
      Left = 304
      Top = 56
      Width = 233
      Height = 289
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      TabOrder = 1
      Options = [aoColumnSizing, aoEditing, aoTabThrough, aoRowSelect, aoAutoSort]
      OptionsEx = [aoUseBitmap, aoBandHeaderWidth, aoAutoCalcPreviewLines, aoBandSizing, aoBandMoving, aoDragScroll, aoDragExpand, aoAutoSortRefresh]
      TreeLineColor = clGrayText
      ShowGrid = True
      ShowRoot = False
      object dxTLSelectedCode: TdxTreeListColumn
        Caption = 'Code'
        Sorted = csUp
        Width = 54
        BandIndex = 0
        RowIndex = 0
      end
      object dxTLSelectedDescription: TdxTreeListColumn
        Caption = 'Description'
        Width = 147
        BandIndex = 0
        RowIndex = 0
      end
      object dxTLSelectedId: TdxTreeListColumn
        Visible = False
        Width = 20
        BandIndex = 0
        RowIndex = 0
      end
    end
    object edCountry: TEdit
      Left = 104
      Top = 8
      Width = 433
      Height = 24
      TabOrder = 2
    end
    object btnSelectAll: TButton
      Left = 264
      Top = 80
      Width = 30
      Height = 25
      Caption = '>>'
      TabOrder = 3
      OnClick = btnSelectAllClick
    end
    object btnSelectOne: TButton
      Left = 264
      Top = 112
      Width = 30
      Height = 25
      Caption = '>'
      TabOrder = 4
      OnClick = btnSelectOneClick
    end
    object btnUnselectAll: TButton
      Left = 264
      Top = 144
      Width = 30
      Height = 25
      Caption = '<<'
      TabOrder = 5
      OnClick = btnUnselectAllClick
    end
    object btnUnselectOne: TButton
      Left = 264
      Top = 176
      Width = 30
      Height = 25
      Caption = '<'
      TabOrder = 6
      OnClick = btnUnselectOneClick
    end
  end
  inherited stbarBase: TStatusBar
    Width = 556
  end
  inherited pnlBottom: TPanel
    Width = 556
    inherited btnOk: TBitBtn
      Left = 169
      Caption = 'Save and Exit'
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 283
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 23
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  object Query: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.PLANT_CODE, T.DEPARTMENT_CODE, T.TEAM_CODE'
      'FROM DEPARTMENTPERTEAM T'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (T.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  )'
      'ORDER BY T.PLANT_CODE, T.DEPARTMENT_CODE'
      ' '
      ' ')
    Left = 24
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
end
