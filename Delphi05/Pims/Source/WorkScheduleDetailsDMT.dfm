inherited WorkScheduleDetailsDM: TWorkScheduleDetailsDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnCalcFields = TableMasterCalcFields
    IndexFieldNames = 'CODE'
    TableName = 'WORKSCHEDULE'
    object TableMasterCODE: TStringField
      FieldName = 'CODE'
      Required = True
      Size = 24
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
    object TableMasterREPEATS: TFloatField
      FieldName = 'REPEATS'
      Required = True
    end
    object TableMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      Required = True
    end
    object TableMasterCALCREFERENCEWEEK: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALCREFERENCEWEEK'
      Size = 30
      Calculated = True
    end
    object TableMasterWORKSCHEDULE_ID: TFloatField
      FieldName = 'WORKSCHEDULE_ID'
      Required = True
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      Size = 80
    end
  end
  inherited TableDetail: TTable
    OnCalcFields = TableDetailCalcFields
    IndexFieldNames = 'WORKSCHEDULE_ID;WORKSCHEDULELINE_ID'
    MasterFields = 'WORKSCHEDULE_ID'
    TableName = 'WORKSCHEDULELINE'
    Top = 132
    object TableDetailWORKSCHEDULE_ID: TFloatField
      FieldName = 'WORKSCHEDULE_ID'
      Required = True
    end
    object TableDetailWORKSCHEDULELINE_ID: TFloatField
      FieldName = 'WORKSCHEDULELINE_ID'
      Required = True
    end
    object TableDetailDAY1_ABSRSN_CODE: TStringField
      FieldName = 'DAY1_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY1_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY1_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY1_TYPE: TFloatField
      FieldName = 'DAY1_TYPE'
      Required = True
    end
    object TableDetailDAY1_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY1_REPEATS'
      Required = True
    end
    object TableDetailDAY2_ABSRSN_CODE: TStringField
      FieldName = 'DAY2_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY2_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY2_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY2_TYPE: TFloatField
      FieldName = 'DAY2_TYPE'
      Required = True
    end
    object TableDetailDAY2_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY2_REPEATS'
      Required = True
    end
    object TableDetailDAY3_ABSRSN_CODE: TStringField
      FieldName = 'DAY3_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY3_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY3_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY3_TYPE: TFloatField
      FieldName = 'DAY3_TYPE'
      Required = True
    end
    object TableDetailDAY3_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY3_REPEATS'
      Required = True
    end
    object TableDetailDAY4_ABSRSN_CODE: TStringField
      FieldName = 'DAY4_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY4_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY4_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY4_TYPE: TFloatField
      FieldName = 'DAY4_TYPE'
      Required = True
    end
    object TableDetailDAY4_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY4_REPEATS'
      Required = True
    end
    object TableDetailDAY5_ABSRSN_CODE: TStringField
      FieldName = 'DAY5_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY5_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY5_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY5_TYPE: TFloatField
      FieldName = 'DAY5_TYPE'
      Required = True
    end
    object TableDetailDAY5_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY5_REPEATS'
      Required = True
    end
    object TableDetailDAY6_ABSRSN_CODE: TStringField
      FieldName = 'DAY6_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY6_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY6_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY6_TYPE: TFloatField
      FieldName = 'DAY6_TYPE'
      Required = True
    end
    object TableDetailDAY6_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY6_REPEATS'
      Required = True
    end
    object TableDetailDAY7_ABSRSN_CODE: TStringField
      FieldName = 'DAY7_ABSRSN_CODE'
      Required = True
      Size = 4
    end
    object TableDetailDAY7_SHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY7_SHIFT_NUMBER'
      Required = True
    end
    object TableDetailDAY7_TYPE: TFloatField
      FieldName = 'DAY7_TYPE'
      Required = True
    end
    object TableDetailDAY7_REPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'DAY7_REPEATS'
      Required = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      Size = 80
    end
    object TableDetailMYROW: TIntegerField
      Alignment = taLeftJustify
      FieldKind = fkCalculated
      FieldName = 'MYROW'
      Calculated = True
    end
    object TableDetailDAY1_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY1_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY2_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY2_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY3_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY3_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY4_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY4_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY5_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY5_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY6_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY6_SHIFT'
      Size = 10
      Calculated = True
    end
    object TableDetailDAY7_SHIFT: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAY7_SHIFT'
      Size = 10
      Calculated = True
    end
  end
  object qryDetails: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT ROWNUM, T.WORKSCHEDULELINE_ID'
      'FROM WORKSCHEDULELINE T'
      'WHERE T.WORKSCHEDULE_ID = :WORKSCHEDULE_ID'
      'ORDER BY T.WORKSCHEDULELINE_ID'
      ' ')
    Left = 96
    Top = 200
    ParamData = <
      item
        DataType = ftInteger
        Name = 'WORKSCHEDULE_ID'
        ParamType = ptUnknown
      end>
  end
  object qryAbsRsn: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_CODE, DESCRIPTION'
      'FROM'
      '  ABSENCEREASON'
      'UNION'
      'SELECT '#39'-'#39', '#39'-'#39
      'FROM DUAL'
      'UNION'
      'SELECT '#39'*'#39', '#39'*'#39
      'FROM DUAL'
      'ORDER BY '
      '  2'
      ' ')
    Left = 96
    Top = 256
  end
  object qryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT SHIFT_NUMBER, MIN(DESCRIPTION) DESCRIPTION'
      'FROM SHIFT'
      'GROUP BY SHIFT_NUMBER'
      'ORDER BY 1'
      ''
      '')
    Left = 96
    Top = 304
  end
  object dspAbsRsn: TDataSetProvider
    DataSet = qryAbsRsn
    Constraints = True
    Left = 176
    Top = 256
  end
  object cdsAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspAbsRsn'
    Left = 256
    Top = 256
  end
  object dspShift: TDataSetProvider
    DataSet = qryShift
    Constraints = True
    Left = 176
    Top = 304
  end
  object cdsShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspShift'
    Left = 256
    Top = 304
  end
end
