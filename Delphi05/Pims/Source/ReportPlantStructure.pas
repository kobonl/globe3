(*
  MRA:3-MAY-2010. RV062.2.
  - SetMemoParam: First argument increased, to prevent
    problems with double line when printing to printer or PrimoPDF.
  MRA:17-NOV-2010 RV080.1.
  - Bug with double lines when printing to e.g. PrimoPDF.
    Preview is OK, but printing to a printer or PrimoPDF
    gives double lines.
  - Reports and Memo-mechanism.
    - Put Memo-mechanism in a seperate class, so
      it can be created and freed when needed.
      NOTE: This did not solve the problem.
  - IMPORTANT: When the Memo-mechanism is NOT used and the QRMemo-
    components are all set to AutoSize=True and AutoStretch=True,
    then it works also without the 'double lines' problem!
*)
unit ReportPlantStructure;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRPDFFilt,
  QRExport, QRXMLSFilt, QRWebFilt;

type

  TQRParameters = class
  private
     
    FDeptFrom, FDeptTo, FWKFrom, FWKTo: String;
    FShowWKDetail, FShowJobDetail,
    FIgnoreJobCodes, FCompareJobCodes,
    FShowSelection, FPagePlant,
    FPageDept, FPageWK: Boolean;
  public
    procedure SetValues(DeptFrom, DeptTo, WKFrom, WKTo: String;
      ShowWKDetail, ShowJobDetail, IgnoreJobCodes, CompareJobCodes,
      ShowSelection, PagePlant, PageDept, PageWK: Boolean);
  end;

  TReportPlantStructureQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRGroupHdPlant: TQRGroup;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRDBText4: TQRDBText;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRGroupHDJob: TQRGroup;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    ChildBandInterfaceCode: TQRChildBand;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText13: TQRDBText;
    QRGroupHDBU: TQRGroup;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText27: TQRDBText;
    ChildBandJOB: TQRChildBand;
    ChildBandBU: TQRChildBand;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelJobDetail: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabelComputer: TQRLabel;
    QRLabelComport: TQRLabel;
    QRLabelAddress: TQRLabel;
    QRLabelCounter: TQRLabel;
    QRLabelComputer_1: TQRLabel;
    QRLabelComport_1: TQRLabel;
    QRLabelAddress_1: TQRLabel;
    QRLabelCounter_1: TQRLabel;
    ChildBandJobIgnoreInterface: TQRChildBand;
    QRSubDetail1: TQRSubDetail;
    QRLabel2: TQRLabel;
    QRLabelInterface: TQRLabel;
    QRBandFooterJob: TQRBand;
    QRLabelComparisonJob: TQRLabel;
    QRMemoWK: TQRMemo;
    QRMemoWKDesc: TQRMemo;
    QRMemoJobCode: TQRMemo;
    QRMemoJobCodeDesc: TQRMemo;
    QRLabelPH: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabelUseJob: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabelMP: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabelAD: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabelAR: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabelPieces: TQRLabel;
    QRLabelWKShort: TQRLabel;
    QRBandFooterWK: TQRBand;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextWKDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextJobDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText10Print(sender: TObject; var Value: String);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure ChildBandInterfaceCodeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandDetailJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRLabelJobDetailPrint(sender: TObject; var Value: String);
    procedure ChildBandShowDetailWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure ChildBandJobIgnoreInterfaceBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelPHPrint(sender: TObject; var Value: String);
    procedure QRLabelWKShortPrint(sender: TObject; var Value: String);
  private
    FQRParameters: TQRParameters;
    FPrintChildInterface,
    FPrintChildJob,
    FPrintChildBU: Boolean;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    function QRSendReportParameters(const PlantFrom, PlantTo,
      DeptFrom, DeptTo, WKFrom, WKTo: String;
      const  ShowWKDetail, ShowJobDetail, IgnoreJobCodes,
      CompareJobCode, ShowSelection, PagePlant,
      PageDept, PageWK: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportPlantStructureQR: TReportPlantStructureQR;

implementation

{$R *.DFM}
uses
  SystemDMT, UPimsConst, Math,
  UPimsMessageRes, ReportPlantStructureDMT;

procedure TQRParameters.SetValues(
  DeptFrom, DeptTo, WKFrom, WKTo: String;
  ShowWKDetail, ShowJobDetail, IgnoreJobCodes, CompareJobCodes,
  ShowSelection, PagePlant, PageDept, PageWK: Boolean);
begin
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FWKFrom := WKFrom;
  FWKTo := WKTo;
  FShowWKDetail := ShowWKDetail;
  FShowJobDetail := ShowJobDetail;
  FIgnoreJobCodes := IgnoreJobCodes;
  FCompareJobCodes := CompareJobCodes;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageDept := PageDept;
  FPageWK := PageWK;
end;

function TReportPlantStructureQR.QRSendReportParameters(
  const PlantFrom, PlantTo, DeptFrom, DeptTo, WKFrom, WKTo: String;
  const ShowWKDetail, ShowJobDetail, IgnoreJobCodes,
    CompareJobCode, ShowSelection, PagePlant,
    PageDept, PageWK: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      DeptFrom, DeptTo, WKFrom, WKTo,
      ShowWKDetail, ShowJobDetail, IgnoreJobCodes, CompareJobCode,
      ShowSelection, PagePlant, PageDept, PageWK)
  end;
  SetDataSetQueryReport(ReportPlantStructureDM.QueryPlantStructure);
end;

function TReportPlantStructureQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;
  
  SelectStr := 
    'SELECT ' +
    '  P.PLANT_CODE, P.DESCRIPTION AS PDESC, ' +
    '  W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, ' +
    '  W.WORKSPOT_CODE, W.DESCRIPTION AS WDESC, '+
    '  W.SHORT_NAME AS WSHORTNAME, W.PRODUCTIVE_HOUR_YN, ' +
    '  W.USE_JOBCODE_YN, W.MEASURE_PRODUCTIVITY_YN, W.AUTOMATIC_DATACOL_YN, ' +
    '  W.AUTOMATIC_RESCAN_YN, W.QUANT_PIECE_YN, ' +
    '  J.JOB_CODE, J.DESCRIPTION AS JDESC, ' +
    '  J.BUSINESSUNIT_CODE AS JOB_BU, BJ.DESCRIPTION AS BJDESC, ' +
    '  J.INTERFACE_CODE, J.NORM_PROD_LEVEL, J.BONUS_LEVEL, ' +
    '  J.NORM_OUTPUT_LEVEL, J.IGNOREINTERFACECODE_YN, J.COMPARATION_JOB_YN, ' +
    '  I.INTERFACE_CODE AS IGNORE_INTERFACECODE, ' +
    '  BW.BUSINESSUNIT_CODE AS BW_BU, B.DESCRIPTION AS BDESC, BW.PERCENTAGE ' +
    'FROM ' +
    '  WORKSPOT W LEFT JOIN PLANT P ON ' +
    '    W.PLANT_CODE = P.PLANT_CODE ' +
    '  LEFT JOIN JOBCODE J ON ' +
    '    (J.PLANT_CODE = W.PLANT_CODE AND ' +
    '    J.WORKSPOT_CODE = W.WORKSPOT_CODE AND ' +
    '    (J.JOB_CODE <> ''' + DUMMYSTR + ''')) ' +
    '  LEFT JOIN DEPARTMENT D ON ' +
    '    W.PLANT_CODE = D.PLANT_CODE AND ' +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' +
    '  LEFT JOIN BUSINESSUNITPERWORKSPOT BW ON ' +
    '    W.PLANT_CODE = BW.PLANT_CODE AND '+
    '    W.WORKSPOT_CODE = BW.WORKSPOT_CODE ' +
    '  LEFT JOIN BUSINESSUNIT B ON ' +
    '    B.PLANT_CODE = BW.PLANT_CODE AND '+
    '    BW.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' +
    '  LEFT JOIN BUSINESSUNIT BJ ON ' +
    '    BJ.PLANT_CODE = J.PLANT_CODE AND '+
    '    BJ.BUSINESSUNIT_CODE = J.BUSINESSUNIT_CODE ' +
    '  LEFT JOIN IGNOREINTERFACECODE I ON ' +
    '    I.PLANT_CODE = J.PLANT_CODE AND ' +
    '    I.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' +
    '    I.JOB_CODE = J.JOB_CODE ' +
    'WHERE ' +
    '  W.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND W.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + ''''+
    '  AND (W.WORKSPOT_CODE <> ''' + DUMMYSTR + ''')';

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    SelectStr := SelectStr +
      '  AND D.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom) + '''' +
      '  AND D.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + ''''  +
      '  AND W.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWKFrom) + '''' +
      '  AND W.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWKTo) + '''';
   
  end;
  SelectStr := SelectStr + ' ' +
    'ORDER BY ' +
    '  P.PLANT_CODE, W.DEPARTMENT_CODE, '+
    '  W.WORKSPOT_CODE, J.JOB_CODE, BW.BUSINESSUNIT_CODE ';

  with ReportPlantStructureDM.QueryPlantStructure do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
    Prepare;
    Active := True;
    Result := not IsEmpty;
  end;
  Screen.Cursor := crDefault;
end;

procedure TReportPlantStructureQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := QRParameters.FWKFrom;
  QRLabelWKTo.Caption := QRParameters.FWKTo; 
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end;
  if not QRParameters.FShowWKDetail then
    QRGroupHDWK.Height := 20
  else
    QRGroupHDWK.Height := 48;
end;

procedure TReportPlantStructureQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportPlantStructureQR.FormCreate(Sender: TObject);
begin
  // RV062.2.
  // RV080.1.
//  SetMemoParam(16 {4}, MAX_LINES_ON_PAGE, QRBandFooterJob);
  inherited;
  FQRParameters := TQRParameters.Create;
end;

procedure TReportPlantStructureQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
end;
 
procedure TReportPlantStructureQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
  FPrintChildJob := False;
  FPrintChildBU := False;
  if ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('USE_JOBCODE_YN').AsString = 'Y' then
  begin
    if ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('JOB_CODE').AsString <> '' then
      FPrintChildJob := True
  end
  else
  begin
    if ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('BW_BU').AsString <> '' then
      FPrintChildBU := True;
  end;
  if Not FPrintChildJob and Not FPrintChildBU then
    PrintBand := False;
    
  QRLabelPH.Caption := '     ';
  QRLabelUseJob.Caption := '     ';
  QRLabelMP.Caption := '     ';
  QRLabelAD.Caption := '     ';
  QRLabelAR.Caption := '     ';
  QRLabelPieces.Caption := SPimsPieces;
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('PRODUCTIVE_HOUR_YN').AsString = 'Y' then
      QRLabelPH.Caption := '  X  ';
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('USE_JOBCODE_YN').AsString = 'Y' then
      QRLabelUseJob.Caption := '  X  ';
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('MEASURE_PRODUCTIVITY_YN').AsString = 'Y' then
      QRLabelMP.Caption := '  X  ';
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('AUTOMATIC_DATACOL_YN').AsString = 'Y' then
      QRLabelAD.Caption := '  X  ';
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('AUTOMATIC_RESCAN_YN').AsString = 'Y' then
      QRLabelAR.Caption := '  X  ';
  if ReportPlantStructureDM.
    QueryPlantStructure.FieldByName('QUANT_PIECE_YN').AsString = 'N' then
      QRLabelPieces.Caption := SPimsWeight;
end;

procedure TReportPlantStructureQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  PrintBand :=
    ((ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('USE_JOBCODE_YN').AsString = 'Y') and
    (ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('JOB_CODE').AsString <> ''))
    or
    ((ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('USE_JOBCODE_YN').AsString = 'N') and
    (ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('BW_BU').AsString <> '' ));
end;

procedure TReportPlantStructureQR.QRDBTextWKDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowWKDetail then
    Value := Copy(ReportPlantStructureDM.
      QueryPlantStructure.FieldByName('WDESC').AsString,0 , 15);
end;

procedure TReportPlantStructureQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
  PrintBand :=
    ((ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('USE_JOBCODE_YN').AsString = 'Y') and
    (ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('JOB_CODE').AsString <> ''))
    or
    ((ReportPlantStructureDM.QueryPlantStructure.
     FieldByName('USE_JOBCODE_YN').AsString = 'N') and
    (ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('BW_BU').AsString <> '' ));
end;

procedure TReportPlantStructureQR.QRDBTextJobDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('JDESC').AsString, 0, 12);
end;

procedure TReportPlantStructureQR.QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChildJob;
  FPrintChildInterface := False;
  if PrintBand then
  begin

    QRLabelComputer.Caption := '';
    QRLabelComport.Caption := '';
    QRLabelAddress.Caption := '';
    QRLabelCounter.Caption := '';
    if ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('INTERFACE_CODE').AsString = '' then
      Exit;
    ReportPlantStructureDM.ClientDataSetDataCol.Filter :=
      'INTERFACE_CODE = ''' +
      ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('INTERFACE_CODE').AsString + '''';

    ReportPlantStructureDM.ClientDataSetDataCol.First;
    if not ReportPlantStructureDM.ClientDataSetDataCol.IsEmpty then
    begin
      QRLabelComputer.Caption :=
        ReportPlantStructureDM.ClientDataSetDataCol.
        FieldByName('COMPUTER_NAME').AsString;
      QRLabelComport.Caption :=
         ReportPlantStructureDM.ClientDataSetDataCol.
         FieldByName('COMPORT').AsString;
      QRLabelAddress.Caption :=
        ReportPlantStructureDM.ClientDataSetDataCol.
         FieldByName('ADDRESS').AsString;
      QRLabelCounter.Caption :=
        ReportPlantStructureDM.ClientDataSetDataCol.
         FieldByName('COUNTER').AsString;
      ReportPlantStructureDM.ClientDataSetDataCol.Next;
      QRLabelComputer_1.Caption := '';
      QRLabelComport_1.Caption := '';
      QRLabelAddress_1.Caption := '';
      QRLabelCounter_1.Caption := '';
      //second line for interface code - job line
      if not ReportPlantStructureDM.ClientDataSetDataCol.Eof then
      begin
        FPrintChildInterface := True;
        QRLabelComputer_1.Caption :=
          ReportPlantStructureDM.ClientDataSetDataCol.
          FieldByName('COMPUTER_NAME').AsString;
        QRLabelComport_1.Caption :=
           ReportPlantStructureDM.ClientDataSetDataCol.
           FieldByName('COMPORT').AsString;
        QRLabelAddress_1.Caption :=
          ReportPlantStructureDM.ClientDataSetDataCol.
           FieldByName('ADDRESS').AsString;
        QRLabelCounter_1.Caption :=
          ReportPlantStructureDM.ClientDataSetDataCol.
           FieldByName('COUNTER').AsString;
      end;     
    end;
  end;
end;

procedure TReportPlantStructureQR.QRDBText10Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowJobDetail then
     Value := '';
end;

procedure TReportPlantStructureQR.QRDBText11Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowJobDetail then
     Value := '';
end;

procedure TReportPlantStructureQR.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowJobDetail then
     Value := '';
end;

procedure TReportPlantStructureQR.ChildBandInterfaceCodeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChildInterface;
end;

procedure TReportPlantStructureQR.ChildBandDetailJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //print comparison jobcodes
  PrintBand := False;
  if not FPrintChildJob then
    Exit;
  if not QRParameters.FCompareJobCodes then
    Exit;
  with ReportPlantStructureDM do
  begin
    ClientDataSetCompareJob.Filter := 'PLANT_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('PLANT_CODE').AsString) +
      ''' AND WORKSPOT_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('WORKSPOT_CODE').AsString) +
      ''' AND JOB_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('JOB_CODE').AsString) + '''';
    if not ClientDataSetCompareJob.IsEmpty then
    begin
      QRMemoWK.Lines.Clear;
      QRMemoWKDesc.Lines.Clear;
      QRMemoJobCode.Lines.Clear;
      QRMemoJobCodeDesc.Lines.Clear;
      ClientDataSetCompareJob.First;
      while not ClientDataSetCompareJob.Eof do
      begin
        QRMemoWK.Lines.Add(ClientDataSetCompareJob.
          FieldByName('COMPARE_WORKSPOT_CODE').AsString);
        QRMemoWKDesc.Lines.Add(ClientDataSetCompareJob.
          FieldByName('WDESC').AsString);
        QRMemoJobCode.Lines.Add(ClientDataSetCompareJob.
          FieldByName('JOB_CODE').AsString);
        QRMemoJobCodeDesc.Lines.Add(ClientDataSetCompareJob.
          FieldByName('JDESC').AsString);
        ClientDataSetCompareJob.Next;
      end;
    end;
  end;  //with
end;

procedure TReportPlantStructureQR.ChildBandJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChildJob;
end;

procedure TReportPlantStructureQR.ChildBandBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChildBU;
end;

procedure TReportPlantStructureQR.QRGroupHDBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChildBU;
end;

procedure TReportPlantStructureQR.QRDBText3Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWKDetail then
    Value := '';
end;

procedure TReportPlantStructureQR.QRDBText5Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWKDetail then
    Value := '';
end;

procedure TReportPlantStructureQR.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWKDetail then
    Value := '';
end;

procedure TReportPlantStructureQR.QRDBText7Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWKDetail then
    Value := '';
end;

procedure TReportPlantStructureQR.QRLabelJobDetailPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowJobDetail then
    Value := '';
end;

procedure TReportPlantStructureQR.ChildBandShowDetailWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowWKDetail;
end;

procedure TReportPlantStructureQR.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportPlantStructureDM.QueryPlantStructure.
      FieldByName('BJDESC').AsString, 0, 10);
end;

procedure TReportPlantStructureQR.ChildBandJobIgnoreInterfaceBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
// print ignore interface code
  PrintBand := False;
  if not FPrintChildJob then
    Exit;
  if not QRParameters.FIgnoreJobCodes then
    Exit;
  if (ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('IGNOREINTERFACECODE_YN').AsString = 'Y') and
     (ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('INTERFACE_CODE').AsString <> '')then
    PrintBand := True;
end;

procedure TReportPlantStructureQR.QRSubDetail1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if not FPrintChildJob then
    Exit;
  if not QRParameters.FIgnoreJobCodes then
    Exit;

  PrintBand := (ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('IGNOREINTERFACECODE_YN').AsString = 'Y') and
     (ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('INTERFACE_CODE').AsString <> '');
  if PrintBand then
    QRLabelInterface.Caption := ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('IGNORE_INTERFACECODE').AsString;
end;

procedure TReportPlantStructureQR.QRBandFooterJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if not FPrintChildJob or (not QRParameters.FCompareJobCodes) then
  begin
    // RV080.1.
//    SetHeightMemoBand(PrintBand, QRBandFooterJob);
    Exit;
  end;

  with ReportPlantStructureDM do
  begin
    ClientDataSetCompareJob.Filter := 'PLANT_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('PLANT_CODE').AsString) +
      ''' AND WORKSPOT_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('WORKSPOT_CODE').AsString) +
      ''' AND JOB_CODE = ''' +
      DoubleQuote(QueryPlantStructure.FieldByName('JOB_CODE').AsString) + '''';
    if not ClientDataSetCompareJob.IsEmpty then
    begin
      PrintBand := True;
      QRMemoWK.Lines.Clear;
      QRMemoWKDesc.Lines.Clear;
      QRMemoJobCode.Lines.Clear;
      QRMemoJobCodeDesc.Lines.Clear;
      ClientDataSetCompareJob.First;
      while not ClientDataSetCompareJob.Eof do
      begin
        QRMemoWK.Lines.Add(ClientDataSetCompareJob.
          FieldByName('COMPARE_WORKSPOT_CODE').AsString);
        QRMemoWKDesc.Lines.Add(ClientDataSetCompareJob.
          FieldByName('WDESC').AsString);
        QRMemoJobCode.Lines.Add(ClientDataSetCompareJob.
          FieldByName('JOB_CODE').AsString);
        QRMemoJobCodeDesc.Lines.Add(ClientDataSetCompareJob.
          FieldByName('JDESC').AsString);
        ClientDataSetCompareJob.Next;
      end;
    end;
  end;  //with
  // RV080.1.
//  SetHeightMemoBand(PrintBand, QRBandFooterJob);
//  if Not PrintBand then
//    SetHeightMemoBand(PrintBand, QRBandFooterJob);
end;

procedure TReportPlantStructureQR.QRLabelPHPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWKDetail then
   Value := '';
end;

procedure TReportPlantStructureQR.QRLabelWKShortPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '(' + ReportPlantStructureDM.QueryPlantStructure.
    FieldByName('WSHORTNAME').AsString + ')';
end;

end.
