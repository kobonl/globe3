inherited DialogReportEmployeePaylistF: TDialogReportEmployeePaylistF
  Left = 279
  Top = 154
  Caption = 'Report Employee Paylist'
  ClientHeight = 378
  ClientWidth = 573
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 573
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 456
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 573
    Height = 257
    TabOrder = 3
    inherited LblFromEmployee: TLabel
      Top = 72
    end
    inherited LblEmployee: TLabel
      Top = 72
    end
    inherited LblToEmployee: TLabel
      Top = 71
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 73
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 73
    end
    object Label7: TLabel [8]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [9]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [10]
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [11]
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [12]
      Left = 344
      Top = 45
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [13]
      Left = 8
      Top = 99
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel [14]
      Left = 8
      Top = 128
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object LabelDatePeriod: TLabel [15]
      Left = 192
      Top = 128
      Width = 78
      Height = 13
      Caption = 'LabelDatePeriod'
    end
    inherited LblFromDepartment: TLabel
      Top = 317
      Visible = False
    end
    inherited LblDepartment: TLabel
      Top = 317
      Visible = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 319
    end
    inherited LblStarDepartmentTo: TLabel
      Top = 319
    end
    inherited LblToDepartment: TLabel
      Top = 319
      Visible = False
    end
    inherited LblToTeam: TLabel
      Top = 292
      Visible = False
    end
    inherited LblTeam: TLabel
      Top = 290
      Visible = False
    end
    inherited LblFromTeam: TLabel
      Top = 290
      Visible = False
    end
    inherited lblFromDateBase: TLabel
      Top = 98
    end
    inherited lblDateBase: TLabel
      Top = 98
    end
    inherited lblToDateBase: TLabel
      Top = 98
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 98
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 99
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 289
      ColCount = 126
      TabOrder = 26
      Visible = False
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 289
      ColCount = 127
      TabOrder = 30
      Visible = False
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 292
      TabOrder = 33
      Visible = False
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 316
      ColCount = 125
      TabOrder = 29
      Visible = False
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 316
      ColCount = 126
      TabOrder = 21
      Visible = False
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Top = 319
      TabOrder = 22
      Visible = False
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 4
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 5
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 132
      TabOrder = 31
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 133
      TabOrder = 32
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 34
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 23
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 145
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 146
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 146
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 147
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 154
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 153
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Visible = False
    end
    inherited DatePickerFromBase: TDateTimePicker
      Top = 96
      Width = 180
    end
    inherited DatePickerToBase: TDateTimePicker
      Top = 96
      Width = 180
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 115
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 342
      Top = 41
      Width = 180
      Height = 19
      ColCount = 116
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 120
      Top = 97
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      Visible = False
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeek: TdxSpinEdit
      Left = 120
      Top = 125
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      Visible = False
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 156
      Width = 553
      Height = 93
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 40
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 1
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 64
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 2
      end
      object CheckBoxShowHourtypes: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Show hourtypes'
        TabOrder = 0
      end
    end
    object GroupBoxQualityIncentive: TGroupBox
      Left = 184
      Top = 196
      Width = 313
      Height = 93
      Caption = 'Quality Incentive Calculation'
      TabOrder = 8
      Visible = False
      object LabelStepSize: TLabel
        Left = 16
        Top = 64
        Width = 44
        Height = 13
        Caption = 'Step Size'
      end
      object RadioGroupLinearSteps: TRadioGroup
        Left = 6
        Top = 16
        Width = 302
        Height = 36
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Linear'
          'Steps')
        TabOrder = 0
        OnClick = RadioGroupLinearStepsClick
      end
      object dxSpinEditStepSize: TdxSpinEdit
        Left = 160
        Top = 59
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        MaxValue = 100
        MinValue = 1
        Value = 10
        StoredValues = 48
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 318
    Width = 573
  end
  inherited pnlBottom: TPanel
    Top = 337
    Width = 573
    inherited btnOk: TBitBtn
      Left = 165
    end
    inherited btnCancel: TBitBtn
      Left = 279
      OnClick = btnCancelClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 496
    Top = 184
  end
  inherited tblPlant: TTable
    Left = 16
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 536
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4F040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072F4469616C6F675265706F7274456D706C6F796565506179
        6C697374462E44617461536F75726365456D706C46726F6D104F7074696F6E73
        437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F4261
        6E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F
        6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F
        6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E4465
        6C6574650D6564676F43616E496E73657274116564676F43616E4E6176696761
        74696F6E116564676F436F6E6669726D44656C657465126564676F4C6F616441
        6C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F546478
        444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F
        6E06064E756D62657206536F7274656407046373557005576964746802410942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C75
        6D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F72
        74206E616D6505576964746802540942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060A53484F52545F4E414D4500000F5464
        78444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E
        06044E616D6505576964746803B4000942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060B4445534352495054494F4E00000F
        546478444247726964436F6C756D6E0D436F6C756D6E41646472657373074361
        7074696F6E06074164647265737305576964746802450942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060741444452455353
        00000F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F64
        650743617074696F6E060F4465706172746D656E7420636F6465055769647468
        02580942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F4445504152544D454E545F434F444500000F546478444247726964
        436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20
        636F64650942616E64496E646578020008526F77496E6465780200094669656C
        644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        16040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072D4469616C6F675265706F72
        74456D706C6F7965655061796C697374462E44617461536F75726365456D706C
        546F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76
        696E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76
        696E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A
        696E6700094F7074696F6E7344420B106564676F43616E63656C4F6E45786974
        0D6564676F43616E44656C6574650D6564676F43616E496E7365727411656467
        6F43616E4E617669676174696F6E116564676F436F6E6669726D44656C657465
        126564676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D
        61726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D70
        6C0743617074696F6E06064E756D62657206536F727465640704637355700557
        6964746802310942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060F454D504C4F5945455F4E554D42455200000F5464784442
        47726964436F6C756D6E0F436F6C756D6E53686F72744E616D65074361707469
        6F6E060A53686F7274206E616D65055769647468024E0942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060A53484F52545F4E
        414D4500000F546478444247726964436F6C756D6E11436F6C756D6E44657363
        72697074696F6E0743617074696F6E06044E616D6505576964746803CC000942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0B4445534352495054494F4E00000F546478444247726964436F6C756D6E0D43
        6F6C756D6E416464726573730743617074696F6E060741646472657373055769
        64746802650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506074144445245535300000F546478444247726964436F6C756D
        6E0E436F6C756D6E44657074436F64650743617074696F6E060F446570617274
        6D656E7420636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060F4445504152544D454E545F434F444500000F5464
        78444247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E
        06095465616D20636F64650942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
