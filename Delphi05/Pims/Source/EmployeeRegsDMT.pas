(*
  MRA:27-JAN-2009 RV021.
    Team selection correction for QueryEmployee, to prevent multiple
    the same records in combobox.
  MRA:25-MAR-2009 RV024.
    Changed qryScans, because it gave no results when a shift could not
    be found in Shift-table. This can happen if a shift is later removed
    from Shift-table.
  SO: 07-JUN-2010 RV065.4
    Added qryEmployeeContract to get the contract hours per week
  MRA:24-AUG-2010 RV067.MRA.6. SO-550482.
  - Give indication that employee is non-active.
  MRA:30-AUG-2010 RV067.MRA.18 Bugfix.
  - Shows wrong production hours.
    Cause: Query had wrong link with Workspots+Jobs resulting
           in multiple lines.
    Changed: qryProdHours.
  MRA:3-SEP-2010 RV067.MRA.25. Changes for 550510.
  - When using teams-per-user, it must also show employee who worked
    on the same plant based on teams-per-user, but can also be
    from other plants.
  MRA:29-SEP-2010 RV069.1. 550494. User rights. Bugfix.
  - This can show also employee that had production, but it did not
    filter on plant + department per team per user.
  MRA:29-NOV-2010 RV081.1.
  - Employee Registrations (RFL)
    - When logged in as WDB and password WDBWDB then
      when first employee 8165 is selected, and then
      a different date is selected, it is possible
      the original employee has been changed.
      Cause: It refreshes the employee-list based on date.
      Solution: After it has refreshed, always point to the
      original employee!
  MRA:1-DEC-2010 RV082.5.
  - Store ContractgroupCode for selected employee in memory,
    to use in Contractgroup-related dialogs, so it can
    be looked up when showing these dialogs.
  MRA:10-MAY-2011 RV092.13. Bugfix. 550518
  - Determine shift from 'shift schedule'.
  MRA:11-JUL-2011 RV094.8. Bugfix.
  - Extra: An employee is now inactive by comparing the end-date, but
           he can also be inactive by comparing the start-date!
  - Added STARTDATE to qryFilterEmployee.
  MRA:7-OCT-2014 20012155
  - Emp. Regs. and change of hours for employee of other team
  - When an employee of an other team (not part of teams-per-user), worked
    on a department belonging to teams-per-user, then show this employee and
    allow changes of hours.
  - Related to this order:
    - It did not refresh correct. Cause: It used a selection-date that had
      a timestamp. To solve it, a trunc is used for date-assignments.
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - cdsFilterEmployee, Filtered is set to True
  MRA:29-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)

unit EmployeeRegsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseDMT, SystemDMT, Db, DBClient, Provider, DBTables;

type
  TEmployeeRegsDM = class(TDialogBaseDM)
    qryFilterEmployee: TQuery;
    qryFilterEmployeeEMPLOYEE_NUMBER: TIntegerField;
    qryFilterEmployeeSHORT_NAME: TStringField;
    qryFilterEmployeeDESCRIPTION: TStringField;
    qryFilterEmployeePLANTLU: TStringField;
    qryFilterEmployeePLANT_CODE: TStringField;
    dspFilterEmployee: TDataSetProvider;
    cdsFilterEmployee: TClientDataSet;
    DataSourceFilterEmployee: TDataSource;
    qryPlanning: TQuery;
    qryAvailable: TQuery;
    qryAbsenceReason: TQuery;
    qryShift: TQuery;
    qrySalaryHours: TQuery;
    qryAbsenceHours: TQuery;
    qryFilterEmployeeDEPARTMENT_CODE: TStringField;
    qryScans: TQuery;
    qryScansDATETIME_IN: TDateTimeField;
    qryScansDATETIME_OUT: TDateTimeField;
    qryScansPLANT_CODE: TStringField;
    qryScansPDESCRIPTION: TStringField;
    qryScansWORKSPOT_CODE: TStringField;
    qryScansWDESCRIPTION: TStringField;
    qryScansJOB_CODE: TStringField;
    qryScansSHIFT_NUMBER: TIntegerField;
    qryScansSDESCRIPTION: TStringField;
    qryScansCALCJDESCRIPTION: TStringField;
    qryJob: TQuery;
    qryFilterEmployeeCUT_OF_TIME_YN: TStringField;
    qryPlant: TQuery;
    qryDepartment: TQuery;
    qryTeam: TQuery;
    qryFilterEmployeeTEAM_CODE: TStringField;
    dspShift: TDataSetProvider;
    cdsShift: TClientDataSet;
    dspJob: TDataSetProvider;
    cdsJob: TClientDataSet;
    dspPlant: TDataSetProvider;
    cdsPlant: TClientDataSet;
    dspDepartment: TDataSetProvider;
    cdsDepartment: TClientDataSet;
    dspTeam: TDataSetProvider;
    cdsTeam: TClientDataSet;
    dspAbsenceReason: TDataSetProvider;
    cdsAbsenceReason: TClientDataSet;
    qryEmployeeContract: TQuery;
    qryProdHours: TQuery;
    qryFilterEmployeeENDDATE: TDateTimeField;
    qryFilterEmployeeOLD: TQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    DateTimeField1: TDateTimeField;
    qryFilterEmployeeEMP_PLANT_IN_TEAM: TStringField;
    qryFilterEmployeeFILTER_PLANT_CODE: TStringField;
    qryFilterEmployeeFILTER_DEPARTMENT_CODE: TStringField;
    qryFilterEmployeeCONTRACTGROUP_CODE: TStringField;
    qryShiftSchedule: TQuery;
    qryFilterEmployeeSTARTDATE: TDateTimeField;
    qryScansDEPARTMENT_CODE: TStringField;
    procedure DataModuleDestroy(Sender: TObject);
    procedure qryScansCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure qryFilterEmployeeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure cdsFilterEmployeeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FFilterDate: TDateTime;
    FShowOnlyActive: Boolean;
    function IsEmployeeActive(ADate: TDateTime): Boolean;
  public
    { Public declarations }
    procedure UpdateSalaryHours(AEmplNo: Integer);
    procedure EmployeeFilterRefresh(ADate: TDateTime; AEmployeeNumber: Integer);
    function DetermineShiftSchedule(Adate: TDateTime; AEmployeeNumber: Integer;
      var APlantCode: String; var AShiftNumber: Integer;
      var AShiftDescription: String): Boolean;
    procedure ShowOnlyActiveSwitch; // 20013035
    property FilterDate: TDateTime read FFilterDate write FFilterDate; // 20013035
    property ShowOnlyActive: Boolean read FShowOnlyActive write FShowOnlyActive; // 20013035
  end;

var
  EmployeeRegsDM: TEmployeeRegsDM;

implementation

uses UPimsConst;

{$R *.DFM}

procedure TEmployeeRegsDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  // Close the TClientDataSets
  cdsFilterEmployee.Close;
  cdsShift.Close;
  cdsJob.Close;
  cdsPlant.Close;
  cdsDepartment.Close;
  cdsTeam.Close;
  cdsAbsenceReason.Close;
end;

procedure TEmployeeRegsDM.qryScansCalcFields(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('JOB_CODE').AsString = '0' then
    qryScansCALCJDESCRIPTION.Value := ''
  else
  begin
    qryScansCALCJDESCRIPTION.Value := '';
    if cdsJob.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('WORKSPOT_CODE').AsString,
      DataSet.FieldByName('JOB_CODE').AsString]) then
        qryScansCALCJDESCRIPTION.Value :=
          cdsJob.FieldByName('DESCRIPTION').AsString;
  end;
(*
    qryJob.Close;
    qryJob.ParamByName('PLANT_CODE').AsString :=
      DataSet.FieldByName('PLANT_CODE').AsString;
    qryJob.ParamByName('WORKSPOT_CODE').AsString :=
      DataSet.FieldByName('WORKSPOT_CODE').AsString;
    qryJob.ParamByName('JOB_CODE').AsString :=
      DataSet.FieldByName('JOB_CODE').AsString;
    qryJob.Open;
    if not qryJob.IsEmpty then
      qryScansCALCJDESCRIPTION.Value :=
        qryJob.FieldByName('DESCRIPTION').AsString
    else
      qryScansCALCJDESCRIPTION.Value := '';
  end;
*)
end;

procedure TEmployeeRegsDM.DataModuleCreate(Sender: TObject);
var
  SelectStr: String;
begin
  inherited;
  try
    cdsFilterEmployee.Open;
    cdsFilterEmployee.LogChanges := False;
    cdsFilterEmployee.Close;
    cdsShift.Open;
    cdsShift.LogChanges := False;
    cdsShift.Close;
    cdsJob.Open;
    cdsJob.LogChanges := False;
    cdsJob.Close;
    cdsPlant.Open;
    cdsPlant.LogChanges := False;
    cdsPlant.Open;
    cdsDepartment.Open;
    cdsDepartment.LogChanges := False;
    cdsDepartment.Close;
    cdsTeam.Open;
    cdsTeam.LogChanges := False;
    cdsTeam.Close;
    cdsAbsenceReason.Open;
    cdsAbsenceReason.LogChanges := False;
    cdsAbsenceReason.Close;
  except
  end;

  // RV082.5. Field CONTRACTGROUP_CODE added to qryFilterEmployee.
  // RV069.1.
  SystemDM.PlantTeamFilterEnable(qryFilterEmployee);
  // RV067.MRA.25.
  // Note: For performance reasons, only look and refresh the
  // employeefilter-query when user-team-selection is used.
  // Otherwise do not do this, or it will get too slow.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    // Query to get employees from certain teams, but also
    // who are from other teams but who made production on
    // plants from the same teams.
    // Example:
    // Plant to show is 23 (based on teams-per-user)
    // - Emp=1 Pl=23 made prod.hrs on plant 23
    // - Emp=2 Pl=21 made prod.hrs on plant 23
    // - Emp=3 Pl=22 made prod.hrs on plant 23
    // Should be refreshed based on date.
    // RV069.1. Also filter on Department when searching for production-data.
    // - Also employees must be shown that worked on workspots within the
    //   plant-department-filter for the teams-per-user.
    // - Use different filter-fields: Origin can be from
    //   Employee or from Workspot that is part of the team.
    // - The UNION must only look for employees that are NOT part of
    //   the team, but made prod. hrs. on workspots that ARE part of the
    //   team based on plant + department of the workspot.
    SelectStr :=
      ' SELECT ' + NL +
      '   E.EMPLOYEE_NUMBER, ' + NL +
      '   E.SHORT_NAME, ' + NL +
      '   E.DESCRIPTION, ' + NL +
      '   E.CUT_OF_TIME_YN, ' + NL +
      '   E.PLANT_CODE, ' + NL +
      '   E.PLANT_CODE FILTER_PLANT_CODE, ' + NL +
      '   E.DEPARTMENT_CODE, ' + NL +
      '   E.DEPARTMENT_CODE FILTER_DEPARTMENT_CODE, ' + NL +
      '   E.TEAM_CODE, ' + NL +
      '   E.STARTDATE, ' + NL +
      '   E.ENDDATE, ' + NL +
      '   E.CONTRACTGROUP_CODE, ' + NL +
      '   P.DESCRIPTION AS PLANTLU, ' + NL +
      '   ''1'' AS EMP_PLANT_IN_TEAM ' + NL +
      ' FROM ' + NL +
      '   EMPLOYEE E INNER JOIN PLANT P ON ' + NL +
      '     E.PLANT_CODE = P.PLANT_CODE ' + NL +
      ' WHERE ' + NL +
      '   ( ' + NL +
      '     (:USER_NAME = ''*'') OR ' + NL +
      '     (E.TEAM_CODE IN ' + NL +
      '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '   ) ' + NL +
      ' UNION ' + NL +
      '   SELECT ' + NL +
      '     E.EMPLOYEE_NUMBER, ' + NL +
      '     E.SHORT_NAME, ' + NL +
      '     E.DESCRIPTION, ' + NL +
      '     E.CUT_OF_TIME_YN, ' + NL +
      '     E.PLANT_CODE, ' + NL +
      '     W.PLANT_CODE, ' + NL +
      '     E.DEPARTMENT_CODE, ' + NL +
      '     W.DEPARTMENT_CODE, ' + NL +
      '     E.TEAM_CODE, ' + NL +
      '     E.STARTDATE, ' + NL +
      '     E.ENDDATE, ' + NL +
      '     E.CONTRACTGROUP_CODE, ' + NL +
      '     (SELECT P2.DESCRIPTION FROM PLANT P2 WHERE P2.PLANT_CODE = E.PLANT_CODE), ' + NL +
      '     CASE ' + NL +
      '       WHEN :USER_NAME = ''*'' THEN ''1'' ' + NL +
      '       WHEN E.PLANT_CODE IN (SELECT T.PLANT_CODE FROM TEAM T WHERE ' + NL +
      '                             T.TEAM_CODE IN (SELECT U.TEAM_CODE FROM ' + NL +
      '                             TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '                  AND E.DEPARTMENT_CODE IN ( ' + NL +
      '                                SELECT DT.DEPARTMENT_CODE ' + NL +
      '                                FROM DEPARTMENTPERTEAM DT INNER JOIN TEAM T ON ' + NL +
      '                                DT.PLANT_CODE = T.PLANT_CODE AND ' + NL +
      '                                DT.TEAM_CODE = T.TEAM_CODE ' + NL +
      '                                WHERE DT.TEAM_CODE IN (SELECT U.TEAM_CODE FROM ' + NL +
      '                                TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME) ' + NL +
      '                                           ) ' + NL +
      '         THEN ''1'' ' + NL +
      '       ELSE ' + NL +
      '         ''0'' ' + NL +
      '     END CASE ' + NL +
      ' FROM PRODHOURPEREMPLOYEE PE INNER JOIN EMPLOYEE E ON ' + NL +
      '   PE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '     INNER JOIN PLANT P ON ' + NL +
      '       PE.PLANT_CODE = P.PLANT_CODE ' + NL +
      '     INNER JOIN WORKSPOT W ON ' + NL +
      '       PE.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '       PE.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      '   WHERE  ' + NL +
      '     PE.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND ' + NL +
      '     ( ' + NL +
      '       (:USER_NAME = ''*'') OR ' + NL +
      '       (E.TEAM_CODE NOT IN ' + NL +
      '         (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '     ) AND ' + NL +
      '     P.PLANT_CODE IN  ' + NL +
      '       ( ' + NL +
      '       SELECT ' + NL +
      '         E2.PLANT_CODE ' + NL +
      '       FROM ' + NL +
      '         EMPLOYEE E2 ' + NL +
      '       WHERE ' + NL +
      '         ( ' + NL +
      '           (:USER_NAME = ''*'') OR ' + NL +
      '           (E2.TEAM_CODE IN ' + NL +
      '             (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '        ) ' + NL +
      '     ) ' + NL +
    // RV069.1. Do not do this for performance-reasons.
    //          Filtering is done using 'OnFilterRecord'.
(*
      '     AND ' + NL +
      '      W.DEPARTMENT_CODE IN ' + NL +
      '        ( ' + NL +
      '        SELECT ' + NL +
      '          E2.DEPARTMENT_CODE ' + NL +
      '        FROM ' + NL +
      '          EMPLOYEE E2 ' + NL +
      '        WHERE ' + NL +
      '          ( ' + NL +
      '            (:USER_NAME = ''*'') OR ' + NL +
      '            (E2.TEAM_CODE IN ' + NL +
      '              (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '         ) ' + NL +
      '      ) ' + NL +
*)
      ' ORDER BY ' + NL +
      '   EMPLOYEE_NUMBER ';
  end
  else
  begin
    // Query to get only employees. Should be opened only once.
    SelectStr :=
      ' SELECT ' + NL +
      '   E.EMPLOYEE_NUMBER, ' + NL +
      '   E.SHORT_NAME, ' + NL +
      '   E.DESCRIPTION, ' + NL +
      '   E.CUT_OF_TIME_YN, ' + NL +
      '   E.PLANT_CODE, ' + NL +
      '   E.PLANT_CODE FILTER_PLANT_CODE, ' + NL +
      '   E.DEPARTMENT_CODE, ' + NL +
      '   E.DEPARTMENT_CODE FILTER_DEPARTMENT_CODE, ' + NL +
      '   E.TEAM_CODE, ' + NL +
      '   E.STARTDATE, ' + NL +
      '   E.ENDDATE, ' + NL +
      '   E.CONTRACTGROUP_CODE, ' + NL +
      '   P.DESCRIPTION AS PLANTLU, ' + NL +
      '   ''1'' AS EMP_PLANT_IN_TEAM ' + NL +
      ' FROM ' + NL +
      '   EMPLOYEE E INNER JOIN PLANT P ON ' + NL +
      '     E.PLANT_CODE = P.PLANT_CODE ' + NL +
      ' WHERE ' + NL +
      '   :PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND ' + NL +
      '   ( ' + NL +
      '     (:USER_NAME = ''*'') OR ' + NL +
      '     (E.TEAM_CODE IN ' + NL +
      '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '   ) ' + NL +
      ' ORDER BY ' + NL +
      '   EMPLOYEE_NUMBER ';
  end;
  qryFilterEmployee.SQL.Clear;
  qryFilterEmployee.SQL.Add(SelectStr);
// qryFilterEmployee.SQL.SaveToFile('c:\temp\employee_regs.sql');
  qryFilterEmployee.Close;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    qryFilterEmployee.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser
  else
    qryFilterEmployee.ParamByName('USER_NAME').AsString := '*';
  qryFilterEmployee.ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := Date;
  qryFilterEmployee.Open;
{
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    qryFilterEmployee.ParamByName('USER_NAME').AsString :=
      SystemDM.LoginUser
  else
    qryFilterEmployee.ParamByName('USER_NAME').AsString := '*';
  qryFilterEmployee.Prepare;
}
end;

procedure TEmployeeRegsDM.UpdateSalaryHours(AEmplNo: Integer);
var
  CountryId: Integer;
  Sql: String;
begin
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) FROM ' +
  ' EMPLOYEE E, PLANT P ' +
  ' WHERE ' +
  '   E.PLANT_CODE = P.PLANT_CODE AND ' +
  '   E.EMPLOYEE_NUMBER = ' +
  IntToStr(AEmplNo), 0
  );

  Sql :=
  ' SELECT ' +
    ' S.HOURTYPE_NUMBER, ' +
    ' NVL((' +
    ' SELECT HC.DESCRIPTION FROM HOURTYPEPERCOUNTRY HC WHERE H.HOURTYPE_NUMBER = HC.HOURTYPE_NUMBER AND HC.COUNTRY_ID = ' + IntToStr(CountryId) +
    '), H.DESCRIPTION) AS HDESCRIPTION, ' +
    ' S.SALARY_MINUTE ' +
  ' FROM ' +
    ' SALARYHOURPEREMPLOYEE S ' +
    ' INNER JOIN HOURTYPE H ON S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' +
  ' WHERE ' +
    ' S.SALARY_DATE = :SALARY_DATE AND ' +
    ' S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
  ' ORDER BY ' +
    ' S.HOURTYPE_NUMBER ';

  qrySalaryHours.Close;
  qrySalaryHours.Params.Clear;
  qrySalaryHours.Params.CreateParam(ftDateTime, 'SALARY_DATE', ptInput);
  qrySalaryHours.Params.CreateParam(ftInteger, 'EMPLOYEE_NUMBER', ptInput);
  qrySalaryHours.Sql.Text := Sql;
  qrySalaryHours.Open;
end;

// RV067.MRA.25.
// RV081.1.
procedure TEmployeeRegsDM.EmployeeFilterRefresh(ADate: TDateTime;
  AEmployeeNumber: Integer);
begin
  // For performance reasons:
  // Only when User-Team-Selection is used refresh the
  // employee-query. Otherwise, there is no need to refresh the
  // employee-query.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    qryFilterEmployee.Close;
    qryFilterEmployee.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser;
    qryFilterEmployee.ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime :=
      Trunc(ADate);
    qryFilterEmployee.Open;
    if cdsFilterEmployee.Active then
      cdsFilterEmployee.Refresh;
    // RV081.1. Point to original employee! This prevents a different employee
    //          becomes the 'current one'.
    try
      if cdsFilterEmployee.Active then
        cdsFilterEmployee.Locate('EMPLOYEE_NUMBER',
          VarArrayOf([AEmployeeNumber]), []);
    except
    end;
  end;
end;

// RV069.1.
procedure TEmployeeRegsDM.qryFilterEmployeeFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV069.1. Use different filter-fields: Origin can be from
  //          Employee or from Workspot that is part of the team.
  Accept := SystemDM.PlantDeptTeamFilter(DataSet,
    'FILTER_PLANT_CODE', 'FILTER_DEPARTMENT_CODE');
end;

// RV092.13.
function TEmployeeRegsDM.DetermineShiftSchedule(Adate: TDateTime;
  AEmployeeNumber: Integer;
  var APlantCode: String; var AShiftNumber: Integer;
  var AShiftDescription: String): Boolean;
begin
  Result := False;
  with qryShiftSchedule do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := ADate;
    Open;
    if not Eof then
    begin
      APlantCode := FieldByName('PLANT_CODE').AsString;
      AShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
      AShiftDescription := '';
      if cdsShift.FindKey([APlantCode, AShiftNumber]) then
        AShiftDescription := cdsShift.FieldByName('DESCRIPTION').AsString;
      Result := True;
    end;
    Close;
  end;
end;

// 20013035
function TEmployeeRegsDM.IsEmployeeActive(ADate: TDateTime): Boolean;
begin
  if cdsFilterEmployee.FieldByName('ENDDATE').AsString <> '' then
  begin
    if (ADate >= cdsFilterEmployee.FieldByName('STARTDATE').AsDateTime) and
      (ADate <= cdsFilterEmployee.FieldByName('ENDDATE').AsDateTime) then
      Result := True
    else
      Result := False;
  end
  else
  begin
    if (ADate >= cdsFilterEmployee.FieldByName('STARTDATE').AsDateTime) then
      Result := True
    else
      Result := False;
  end;
end;

// 20013035
procedure TEmployeeRegsDM.ShowOnlyActiveSwitch;
begin
  cdsFilterEmployee.Refresh;
end;

// 20013035
procedure TEmployeeRegsDM.cdsFilterEmployeeFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  if ShowOnlyActive then
    Accept := IsEmployeeActive(FilterDate)
  else
    Accept := True;
end;

end.
