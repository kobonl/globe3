(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
*)
unit ShiftDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TShiftDM = class(TGridBaseDM)
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableMasterFAX: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterINSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterINSCAN_MARGIN_LATE: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterOUTSCAN_MARGIN_LATE: TIntegerField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableHourtype: TTable;
    TableDetailHOURTYPELU: TStringField;
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ShiftDM: TShiftDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TShiftDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
  SetNullValues(TableDetail);
end;

procedure TShiftDM.DefaultNewRecord(DataSet: TDataSet);
begin
  inherited;
  SetNullValues(TableDetail);
end;

// RV050.8.
procedure TShiftDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  SystemDM.PlantTeamFilterEnable(TableMaster);
end;

// RV050.8.
procedure TShiftDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
