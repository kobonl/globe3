(*
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase-form.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:1-APR-2019 PIM-377
    - Show start and end times in planning reports
*)
unit DialogReportStaffPlanDayFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT, jpeg;

type
  TDialogReportStaffPlanDayF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    CheckBoxPagePlant: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    ComboBoxPlusShiftFrom: TComboBoxPlus;
    ComboBoxPlusShiftTo: TComboBoxPlus;
    QueryShift: TQuery;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    CheckBoxPageShift: TCheckBox;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    CheckBoxShowNPlannedWorkspot: TCheckBox;
    CheckBoxSortEmp: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label2: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    LabelDeptFrom: TLabel;
    LabelDeptTo: TLabel;
    CheckBoxShowStartEndTimes: TCheckBox;
    RGrpSortOnEmployee: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillShift;
    procedure DateFromChange(Sender: TObject);
    procedure CheckBoxSortEmpClick(Sender: TObject);
    procedure CheckBoxShowNPlannedWorkspotClick(Sender: TObject);
    procedure ComboBoxPlusShiftFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftToCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private

  public
    { Public declarations }

  end;

var
  DialogReportStaffPlanDayF: TDialogReportStaffPlanDayF;

// RV089.1.
function DialogReportStaffPlanDayForm: TDialogReportStaffPlanDayF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportStaffPlanDayDMT, ReportStaffPlanDayQRPT,
  ListProcsFRM, CalculateTotalHoursDMT, UPimsMessageRes;

// RV089.1.
var
  DialogReportStaffPlanDayF_HND: TDialogReportStaffPlanDayF;

// RV089.1.
function DialogReportStaffPlanDayForm: TDialogReportStaffPlanDayF;
begin
  if (DialogReportStaffPlanDayF_HND = nil) then
  begin
    DialogReportStaffPlanDayF_HND := TDialogReportStaffPlanDayF.Create(Application);
    with DialogReportStaffPlanDayF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportStaffPlanDayF_HND;
end;

// RV089.1.
procedure TDialogReportStaffPlanDayF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportStaffPlanDayF_HND <> nil) then
  begin
    DialogReportStaffPlanDayF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportStaffPlanDayF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportStaffPlanDayF.btnOkClick(Sender: TObject);
begin
  inherited;
  DateTo.Date := DateFrom.Date; // Allways 1 day!
  if ReportStaffPlanDayQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value), GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    IntToStr(GetIntValue(ComboBoxPlusShiftFrom.Value)),
    IntToStr(GetIntValue(ComboBoxPlusShiftTO.Value)),
    GetDate(DateFrom.Date), GetDate(DateTo.Date),
    False,
    CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
    CheckBoxPageShift.Checked, CheckBoxPageWK.Checked,
    False, False,
    CheckBoxAllTeams.Checked,
    False,
    CheckBoxShowNPlannedWorkspot.Checked,
    CheckBoxSortEmp.Checked,
    CheckBoxExport.Checked,
    CheckBoxShowStartEndTimes.Checked,
    RGrpSortOnEmployee.ItemIndex)
  then
    ReportStaffPlanDayQR.ProcessRecords;
end;

procedure TDialogReportStaffPlanDayF.FillShift;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
    ComboBoxPlusShiftFrom.Visible := True;
    ComboBoxPlusShiftTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusShiftFrom.Visible := False;
    ComboBoxPlusShiftTo.Visible := False;
  end;
   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportStaffPlanDayF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, True, False, False, True, False);
  inherited;
  FillShift;
  ListProcsF.WeekUitDat(Now, Year, Week);
  DateFrom.Date := Int(Now + 1);
  DateTo.Date := InT(Now + 7);
end;

procedure TDialogReportStaffPlanDayF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportStaffPlanDayDM := CreateReportDM(TReportStaffPlanDayDM);
  ReportStaffPlanDayQR := CreateReportQR(TReportStaffPlanDayQR);
  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageShift.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxShowStartEndTimes.Checked := SystemDM.RepPlanStartEndTime; // PIM-377
  CheckBoxSortEmp.Checked := SystemDM.RepPlanStartEndTime; // PIM-377
  RGrpSortOnEmployee.Enabled := CheckBoxSortEmp.Checked;
  CheckBoxSortEmpClick(Sender);  
end;

procedure TDialogReportStaffPlanDayF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateTo.Date - DateFrom.Date >= 7 then
    DateTo.Date := DateFrom.Date + 6;
  if DateTo.Date - DateFrom.Date < 0 then
    DateTo.Date := DateFrom.Date ;
end;

procedure TDialogReportStaffPlanDayF.CheckBoxSortEmpClick(Sender: TObject);
begin
  inherited;
  if  CheckBoxSortEmp.Checked then
  begin
    // PIM-377 Show new page on employee appears not to work, so disable it.
    CheckBoxPageWK.Caption := SPimsNewPageWK;
    CheckBoxPageWK.Checked := False;
    CheckBoxPageWK.Enabled := False;
//    CheckBoxPageWK.Caption := SPimsNewPageEmpl;
    CheckBoxShowNPlannedWorkspot.Enabled := False;
    CheckBoxShowNPlannedWorkspot.Checked := False;
    RGrpSortOnEmployee.Enabled := True;
  end
  else
  begin
    CheckBoxPageWK.Enabled := True;
    CheckBoxShowNPlannedWorkspot.Enabled := True;
    CheckBoxPageWK.Caption := SPimsNewPageWK;
    RGrpSortOnEmployee.Enabled := False;
  end;
end;

procedure TDialogReportStaffPlanDayF.CheckBoxShowNPlannedWorkspotClick(
  Sender: TObject);
begin
  inherited;
  if CheckBoxShowNPlannedWorkspot.Checked then
  begin
   CheckBoxSortEmp.Checked := False;
   CheckBoxSortEmp.Enabled := False;
  end
  else
   CheckBoxSortEmp.Enabled := True;
end;

procedure TDialogReportStaffPlanDayF.ComboBoxPlusShiftFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
      GetIntValue(ComboBoxPlusShiftTo.Value) then
        ComboBoxPlusShiftTo.DisplayValue := ComboBoxPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportStaffPlanDayF.ComboBoxPlusShiftToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
       GetIntValue(ComboBoxPlusShiftTo.Value) then
         ComboBoxPlusShiftFrom.DisplayValue := ComboBoxPlusShiftTo.DisplayValue;
end;

procedure TDialogReportStaffPlanDayF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillShift;
end;

procedure TDialogReportStaffPlanDayF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillShift;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportStaffPlanDayF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportStaffPlanDayF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
