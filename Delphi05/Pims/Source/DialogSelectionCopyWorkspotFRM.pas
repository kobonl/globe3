unit DialogSelectionCopyWorkspotFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, StdCtrls, Dblup1a, ComCtrls, Buttons, ExtCtrls,
  dxLayout, dxCntner, dxEditor, dxExEdtr, dxDBEdtr, dxDBELib, dxExGrEd,
  dxExELib;

type
  TDialogSelectionCopyWorkspotF = class(TDialogSelectionF)
    LabelFrom: TLabel;
    dxDBGridLayoutListWorkspot: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    dxDBExtLookupEditComputer: TdxDBExtLookupEdit;
    LabelTo: TLabel;
    LabelName: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure dxDBExtLookupEditComputerCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogSelectionCopyWorkspotF: TDialogSelectionCopyWorkspotF;

implementation
uses WorkSpotPerStationDMT, ListProcsFRM, SystemDMT, UPimsMessageRes;
{$R *.DFM}

procedure TDialogSelectionCopyWorkspotF.FormShow(Sender: TObject);
begin
  inherited;
  WorkSpotPerStationDM.TableWorkStation.First;
end;

procedure TDialogSelectionCopyWorkspotF.btnOkClick(Sender: TObject);
begin
  if WorkSpotPerStationDM.TableMaster.FieldByName('COMPUTER_NAME').
    AsString = WorkSpotPerStationDM.TableWorkStation.FieldByName('COMPUTER_NAME').
    	AsString then
  begin
  	DisplayMessage(SPimsSameSourceAndDest, mtInformation, [mbOk]);
    ModalResult := mrNone;
    exit;
  end;
  inherited;
end;

procedure TDialogSelectionCopyWorkspotF.dxDBExtLookupEditComputerCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  WorkSpotPerStationDM.TableWorkStation.FindKey([Text]);
end;

end.
