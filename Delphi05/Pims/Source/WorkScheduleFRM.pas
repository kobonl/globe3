(*
  MRA:2-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:6-APR-2018 GLOB3-114
  - Work Schedule adjust repeats to 26 (from 12 to 26)
*)
unit WorkScheduleFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, SystemDMT, WorkScheduleDMT, StdCtrls, dxEditor,
  dxExEdtr, dxEdLib, dxDBELib, DBCtrls, Mask;

type
  TWorkScheduleF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    dxDBDateEdit1: TdxDBDateEdit;
    dxDetailGridColumnCODE: TdxDBGridColumn;
    dxDetailGridColumnDESCRIPTION: TdxDBGridColumn;
    dxDetailGridColumnREPEATS: TdxDBGridColumn;
    DBText1: TDBText;
    dxDetailGridColumnSTARTDATE: TdxDBGridColumn;
    dxDBSpinEdit1: TdxDBSpinEdit;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function WorkScheduleF: TWorkScheduleF;

var
  WorkScheduleF_HDN: TWorkScheduleF;

implementation

uses ListProcsFRM;

{$R *.DFM}

function WorkScheduleF: TWorkScheduleF;
begin
  if WorkScheduleF_HDN = nil then
  begin
    WorkScheduleF_HDN := TWorkScheduleF.Create(Application);
  end;
  Result := WorkScheduleF_HDN;
end;

procedure TWorkScheduleF.FormCreate(Sender: TObject);
begin
  WorkScheduleDM := CreateFormDM(TWorkScheduleDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := WorkScheduleDM.DataSourceDetail;
  inherited;
end;

procedure TWorkScheduleF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkScheduleF_HDN := nil;
end;

procedure TWorkScheduleF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TWorkScheduleF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

end.
