(*
  Changes:
    MRA: 08-JUL-2009 RV032.
      - Year, Week, Day From, Day To
      use this:
      - Year, Week From, Week To.
      With a maximum of 5 weeks.
      Instead of above, use this:
      - DateFrom - DateTo.
      So, it is the same as other product-reports.
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase-form.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:18-JUN-2014 20015223
    - Productivity reports and grouping on workspots
    MRA:18-JUN-2014 20015220
    - Productivity reports and selection on time
    - Use ReportProductionDetailDM3 instead of 2.
    MRA:24-JUN-2014 20015221
    - Include open scans
    MRA:22-SEP-2014 20015586
    - Include down-time in production reports
*)

unit DialogReportTeamIncentiveFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportTeamIncentiveF = class(TDialogReportBaseF)
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    QueryBU: TQuery;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label16: TLabel;
    Label13: TLabel;
    Label18: TLabel;
    Label14: TLabel;
    QueryJobCode: TQuery;
    DataSourceWorkSpot: TDataSource;
    Label2: TLabel;
    dxSpinEditDayFrom: TdxSpinEdit;
    dxSpinEditDayTo: TdxSpinEdit;
    Label4: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    CheckBoxAllTeam: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    LabelDatePeriod: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    ComboBoxPlusShiftFrom: TComboBoxPlus;
    Label33: TLabel;
    ComboBoxPlusShiftTo: TComboBoxPlus;
    QueryShift: TQuery;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    dxSpinEditWeekFrom: TdxSpinEdit;
    Label37: TLabel;
    dxSpinEditWeekTo: TdxSpinEdit;
    lblPeriod: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    DateFrom: TDateTimePicker;
    Label40: TLabel;
    DateTo: TDateTimePicker;
    Panel1: TPanel;
    RadioGroupSort: TRadioGroup;
    RadioGroupBUDept: TRadioGroup;
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageEmpl: TCheckBox;
    CheckBoxDetailRep: TCheckBox;
    CheckBoxOnlyJob: TCheckBox;
    CheckBoxIncentive: TCheckBox;
    CheckBoxExport: TCheckBox;
    ProgressBar: TProgressBar;
    RadioGroupIncludeDowntime: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxSpinEditYearChange(Sender: TObject);
    procedure FillBusiness;
{    procedure FillWorkSpot; }
//    procedure FillJobcode;
//CAR 23-10-2003
    procedure FillShift;
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxPagePlantClick(Sender: TObject);
    procedure CheckBoxPageEmplClick(Sender: TObject);
    procedure dxSpinEditWeekChange(Sender: TObject);
    procedure dxSpinEditDayToChange(Sender: TObject);
    procedure dxSpinEditDayFromChange(Sender: TObject);
    procedure CheckBoxDetailRepClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftToCloseUp(Sender: TObject);
    procedure dxSpinEditWeekFromChange(Sender: TObject);
    procedure dxSpinEditWeekToChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbPlusWorkspotFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotToCloseUp(Sender: TObject);
  private
    procedure ShowDateTimeCaption;
    procedure ShowPeriod;
    procedure ShowDateSelection(ShowYN: Boolean);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportTeamIncentiveF: TDialogReportTeamIncentiveF;

// RV089.1.
function DialogReportTeamIncentiveForm: TDialogReportTeamIncentiveF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ListProcsFRM, UPimsMessageRes,
  ReportTeamIncentiveQRPT, ReportTeamIncentiveDMT,
  ReportProductionDetailDMT;

// RV089.1.
var
  DialogReportTeamIncentiveF_HND: TDialogReportTeamIncentiveF;

// RV089.1.
function DialogReportTeamIncentiveForm: TDialogReportTeamIncentiveF;
begin
  if (DialogReportTeamIncentiveF_HND = nil) then
  begin
    DialogReportTeamIncentiveF_HND := TDialogReportTeamIncentiveF.Create(Application);
    with DialogReportTeamIncentiveF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportTeamIncentiveF_HND;
end;

// RV089.1.
procedure TDialogReportTeamIncentiveF.FormDestroy(Sender: TObject);
begin
  inherited;
//  ReportProductionDetailDM.Free;
  if (DialogReportTeamIncentiveF_HND <> nil) then
  begin
    DialogReportTeamIncentiveF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportTeamIncentiveF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportTeamIncentiveF.btnOkClick(Sender: TObject);
begin
  inherited;
  ProgressBar.Position := 0; // 20015221
(*
  if dxSpinEditDayFrom.IntValue > dxSpinEditDayTo.IntValue then
  begin
    DisplayMessage(SDaySelect,  mtInformation, [mbYes]);
    Exit;
  end;
*)
(*
  if dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value then
  begin
    DisplayMessage(SPimsWeekStartEnd, mtInformation, [mbYes]);
    Exit;
  end;
*)
  if DateTimeFrom(DateFrom.Date) > DateTimeTo(DateTo.Date) then // 20015220
  begin
    DisplayMessage( SDateFromTo, mtInformation, [mbOk]);
    Exit;
  end;
  ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := False;
  ReportTeamIncentiveDM.EnableWorkspotIncludeForThisReport := False;
  ReportTeamIncentiveQR.EnableWorkspotIncludeForThisReport := False;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
  begin
    ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := True;
    ReportTeamIncentiveDM.EnableWorkspotIncludeForThisReport := True;
    ReportTeamIncentiveQR.EnableWorkspotIncludeForThisReport := True;
    ReportTeamIncentiveQR.InclExclWorkspotsResult := EditWorkspots.Text;
    if not ReportTeamIncentiveQR.InclExclWorkspotsAll then
      ReportTeamIncentiveQR.InclExclWorkspotsResult :=
        GetStrValue(ReportTeamIncentiveQR.InclExclWorkspotsResult);
    ReportTeamIncentiveDM.InclExclWorkspotsResult :=
      ReportTeamIncentiveQR.InclExclWorkspotsResult;
  end;
  // 20015220
  ReportProductionDetailDM.UseDateTime := SystemDM.DateTimeSel;
  ReportTeamIncentiveDM.UseDateTime := SystemDM.DateTimeSel;
  ReportTeamIncentiveQR.UseDateTime := SystemDM.DateTimeSel;
  // 20015221
  ReportProductionDetailDM.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportTeamIncentiveDM.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportTeamIncentiveQR.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportTeamIncentiveQR.ProgressBar := ProgressBar;
  if ReportTeamIncentiveQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      GetStrValue(CmbPlusJobFrom.Value),
      GetStrValue(CmbPlusJobTo.Value),
 //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
//CAR 23-10-2003
      IntToStr(GetIntValue(ComboBoxPlusShiftFrom.Value)),
      IntToStr(GetIntValue(ComboBoxPlusShiftTo.Value)),
//
(*      dxSpinEditYear.IntValue,
      dxSpinEditWeek.IntValue,
      dxSpinEditDayFrom.IntValue, dxSpinEditDayTo.IntValue, *)
(*
      Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value),
      Round(dxSpinEditWeekTo.Value),
*)
      DateTimeFrom(DateFrom.Date), // 20015220
      DateTimeTo(DateTo.Date), // 20015220
      RadioGroupSort.ItemIndex, RadioGroupBUDept.ItemIndex,
      CheckBoxAllTeams.Checked,
      CheckBoxShowSelection.Checked, CheckBoxDetailRep.Checked,
      CheckBoxPagePlant.Checked, CheckBoxPageEmpl.Checked,
      CheckBoxOnlyJob.Checked,
      CheckBoxIncentive.Checked,
      CheckBoxExport.Checked,
      RadioGroupIncludeDowntime.ItemIndex // 20015586
      )
  then
    ReportTeamIncentiveQR.ProcessRecords;
  ProgressBar.Position := 0; // 20015221
end;

procedure TDialogReportTeamIncentiveF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;
{
procedure TDialogReportTeamIncentiveF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    //RV067.2.
    QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom,  GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
{
procedure TDialogReportTeamIncentiveF.FillJobCode;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
     (CmbPlusWorkspotFrom.Value <> '') and
     (CmbPlusWorkspotFrom.Value = CmbPlusWorkspotTo.Value) then
  begin
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeFrom,
       GetStrValue(CmbPlusPlantFrom.Value),
       GetStrValue(CmbPlusWorkspotFrom.Value), True);
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeTo,
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value), False);
    ComboBoxPlusJobCodeFrom.Visible := True;
    ComboBoxPlusJobCodeTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusJobCodeFrom.Visible := False;
    ComboBoxPlusJobCodeTo.Visible := False;
  end;
end;
}
//Car 23-10-2003
procedure TDialogReportTeamIncentiveF.FillShift;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
    ComboBoxPlusShiftFrom.Visible := True;
    ComboBoxPlusShiftTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusShiftFrom.Visible := False;
    ComboBoxPlusShiftTo.Visible := False;
  end;
   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportTeamIncentiveF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  if SystemDM.WorkspotInclSel then
    EnableWorkspotIncludeForThisReport := True; // 20015223
  if SystemDM.DateTimeSel then
  begin
    UseDateTime := True; // 20015220
    ShowDateSelection(False);
  end;
  InitDialog(True, True, True, True, False, True, True);
  inherited;
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}  
  ListProcsF.WeekUitDat(Now, Year, Week);
(*  dxSpinEditYear.Value := Year;
  dxSpinEditWeekFrom.Value := Week;
  dxSpinEditWeekTo.Value := Week; *)

(*  dxSpinEditWeek.Value := Week; *)

  DateFrom.Date := Date;
  DateTo.Date := Date;

  CheckBoxShowSelection.Checked := True;
  CheckBoxDetailRep.Checked := True;
//  CheckBoxAllTeam.Checked := False;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageEmpl.Checked := False;
  RadioGroupBUDept.ItemIndex := 0;
  RadioGroupSort.ItemIndex := 0;
  // CAR 10-6-2003
  CheckBoxIncentive.Checked := True;
end;

procedure TDialogReportTeamIncentiveF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
//  ReportProductionDetailDM := TReportProductionDetailDMT.Create(nil);
  ReportProductionDetailDM.Init; // 20015220 + 20015221
  ReportProductionDetailDM.EmpPiecesPerDate := True;
  ReportTeamIncentiveDM := CreateReportDM(TReportTeamIncentiveDM);
  ReportTeamIncentiveQR := CreateReportQR(TReportTeamIncentiveQR);
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditYearChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
//  ShowDateTimeCaption;
end;

procedure TDialogReportTeamIncentiveF.CheckBoxAllTeamClick(
  Sender: TObject);
begin
  inherited;
{
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
  end;
}  
end;

procedure TDialogReportTeamIncentiveF.CheckBoxPagePlantClick(
  Sender: TObject);
begin
  inherited;
  CheckBoxPageEmpl.Checked := False;
  CheckBoxPageEmpl.Enabled := not CheckBoxPagePlant.Checked;
end;

procedure TDialogReportTeamIncentiveF.CheckBoxPageEmplClick(
  Sender: TObject);
begin
  inherited;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPagePlant.Enabled := not CheckBoxPageEmpl.Checked;
end;

procedure TDialogReportTeamIncentiveF.ShowDateTimeCaption;
begin
  try
    LabelDatePeriod.Caption :=  '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
        dxSpinEditWeek.IntValue, dxSpinEditDayFrom.IntValue)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
        dxSpinEditWeek.IntValue, dxSpinEditDayTo.IntValue))
        + ')'
  except
    LabelDatePeriod.Caption := '';
  end;
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditWeekChange(
  Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowDateTimeCaption;
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditDayToChange(
  Sender: TObject);
begin
  inherited;
  ShowDateTimeCaption;
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditDayFromChange(
  Sender: TObject);
begin
  inherited;
  ShowDateTimeCaption;
end;

procedure TDialogReportTeamIncentiveF.CheckBoxDetailRepClick(
  Sender: TObject);
begin
  inherited;
  CheckBoxOnlyJob.Enabled :=   CheckBoxDetailRep.Checked;
  CheckBoxOnlyJob.Checked :=   CheckBoxDetailRep.Checked;
end;

// MR:05-12-2003
procedure TDialogReportTeamIncentiveF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
  try
    // Set max of weeks
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;
  try
    // Set max of weeks
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
  // Limit to 5 weeks!
  if (dxSpinEditWeekTo.Value - dxSpinEditWeekFrom.Value) > 4 then
  begin
    dxSpinEditWeekTo.Value := dxSpinEditWeekFrom.Value + 4;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  end;
(*
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
  except
    dxSpinEditWeek.Value := 1;
  end;
*)
end;

procedure TDialogReportTeamIncentiveF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessTo.DisplayValue :=
          ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportTeamIncentiveF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

{
procedure TDialogReportTeamIncentiveF.ComboBoxPlusJobCodeFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusJobCodeFrom.DisplayValue <> '') and
     (ComboBoxPlusJobCodeTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusJobCodeFrom.Value) >
      GetStrValue(ComboBoxPlusJobCodeTo.Value) then
        ComboBoxPlusJobCodeTo.DisplayValue :=
          ComboBoxPlusJobCodeFrom.DisplayValue;
end;
}
{
procedure TDialogReportTeamIncentiveF.ComboBoxPlusJobCodeToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusJobCodeFrom.DisplayValue <> '') and
     (ComboBoxPlusJobCodeTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusJobCodeFrom.Value) >
      GetStrValue(ComboBoxPlusJobCodeTo.Value) then
        ComboBoxPlusJobCodeFrom.DisplayValue := ComboBoxPlusJobCodeTo.DisplayValue;
end;
}
procedure TDialogReportTeamIncentiveF.ComboBoxPlusShiftFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
      GetIntValue(ComboBoxPlusShiftTo.Value) then
        ComboBoxPlusShiftTo.DisplayValue := ComboBoxPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportTeamIncentiveF.ComboBoxPlusShiftToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
       GetIntValue(ComboBoxPlusShiftTo.Value) then
      ComboBoxPlusShiftFrom.DisplayValue := ComboBoxPlusShiftTo.DisplayValue;
end;

procedure TDialogReportTeamIncentiveF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekTo.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditWeekFromChange(
  Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportTeamIncentiveF.dxSpinEditWeekToChange(
  Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportTeamIncentiveF.FillAll;
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
//  FillJobCode;
  FillShift;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportTeamIncentiveF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportTeamIncentiveF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportTeamIncentiveF.CmbPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

procedure TDialogReportTeamIncentiveF.CmbPlusWorkspotToCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

// 20015220
procedure TDialogReportTeamIncentiveF.ShowDateSelection(ShowYN: Boolean);
begin
  Label38.Visible := ShowYN;
  Label39.Visible := ShowYN;
  Label40.Visible := ShowYN;
  DateFrom.Visible := ShowYN;
  DateTo.Visible := ShowYN;
end;

end.
