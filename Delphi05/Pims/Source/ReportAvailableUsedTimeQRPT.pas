(*
  Changes
    MRA:08-08-2008 REV008.
      - Use Employee's plant during select-statement, to prevent absence is
        not shown that is booked on another plant.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:29-APR-2011. RV092.2. 890035.
    - Suppress zeroes could give different results.
    MRA:4-FEB-2019 GLOB3-208
    - Accrual Report
    - For calculation of planned it has to use max. 10 timeblocks!
    - The columns it shows must be the same as used in
      Hours-Per-Employee-dialog, for Holiday, Time for Time and Work time
      reduction.
    - For Holiday it must be:
      - Remaining Last Year | Norm | Used | Planned | Available
    - For Time for Time:
      - Remaining Last Year | Earned | Used | Planned | Available
    - For Work time Reduction:
      - Remaining Last Year | Earned | Used | Planned | Available
    MRA:22-FEB-2019     GLOB3-266
    - Report available, used and planned free time more space for name-column
    - Solved by changing setting for name-field to:
      - AutoSize=False, AutoStretch=True, WordWrap=True.
    - It also was truncated to 12 positions, this is disabled.
*)

unit ReportAvailableUsedTimeQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

const
  MAXVAL=16;

type
  TQRParameters = class
  private
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo,
    FTeamFrom, FTeamTo: String;
    FStatusEmpl: Integer;
    FShowHoliday, FShowWTR, FShowTimeForTime, FSortOnDept, FSortOnTeam: Boolean;
    FHolidayCaption, FWTRCaption, FTimeForTimeCaption: String;
    FAllTeam, FShowSelection, FSuppressZeroes,
    FExportToFile: Boolean;
  public
    procedure SetValues(
      BusinessFrom, BusinessTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo: String;
      StatusEmpl: Integer;
      ShowHoliday, ShowWTR, ShowTimeForTime, SortOnDept, SortOnTeam: Boolean;
      HolidayCaption, WTRCaption, TimeForTimeCaption: String;
      AllTeam, ShowSelection, SuppressZeroes, ExportToFile: Boolean);
  end;

  TReportAvailableUsedTimeQR = class(TReportBaseF)
    QRGrpHDPlant: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGrpHDTeam: TQRGroup;
    QRGrpHDDept: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabelDateCurrect: TQRLabel;
    QRLabelYear: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRBandFooterEmpl: TQRBand;
    QRDBTextEmpl: TQRDBText;
    QRDBTextEmplDesc: TQRDBText;
    QRBandSummary: TQRBand;
    QRLabel28: TQRLabel;
    QRLabelNormH: TQRLabel;
    QRLabelUsedH: TQRLabel;
    QRLabelAvailH: TQRLabel;
    QRLabelEarnedWTR: TQRLabel;
    QRLabelUsedWTR: TQRLabel;
    QRLabelAvailWTR: TQRLabel;
    QRLabelPlannedTWR: TQRLabel;
    QRLabelEarnedTFT: TQRLabel;
    QRLabelUsedTFT: TQRLabel;
    QRLabelAvailTFT: TQRLabel;
    QRLabelPlannedTFT: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelEmplStatus: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel2: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabelPlannedH: TQRLabel;
    QRLabelNormHT: TQRLabel;
    QRLabelUsedHT: TQRLabel;
    QRLabelAvailHT: TQRLabel;
    QRLabelPlannedHT: TQRLabel;
    QRLabelEarnedWTRT: TQRLabel;
    QRLabelUsedWTRT: TQRLabel;
    QRLabelAvailWTRT: TQRLabel;
    QRLabelPlannedTWRT: TQRLabel;
    QRLabelEarnedTFTT: TQRLabel;
    QRLabelUsedTFTT: TQRLabel;
    QRLabelAvailTFTT: TQRLabel;
    QRLabelPlannedTFTT: TQRLabel;
    QRShape1: TQRShape;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRBndGrpFooterPlant: TQRBand;
    QRLabel26: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel27: TQRLabel;
    QRLabelNormHPT: TQRLabel;
    QRLabelUsedHPT: TQRLabel;
    QRLabelAvailHPT: TQRLabel;
    QRLabelPlannedHPT: TQRLabel;
    QRLabelEarnedWTRPT: TQRLabel;
    QRLabelUsedWTRPT: TQRLabel;
    QRLabelAvailWTRPT: TQRLabel;
    QRLabelPlannedTWRPT: TQRLabel;
    QRLabelEarnedTFTPT: TQRLabel;
    QRLabelUsedTFTPT: TQRLabel;
    QRLabelAvailTFTPT: TQRLabel;
    QRLabelPlannedTFTPT: TQRLabel;
    QRShape2: TQRShape;
    QRLabel29: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRBndGrpFooterTeam: TQRBand;
    QRLabel30: TQRLabel;
    QRShape5: TQRShape;
    QRDBText6: TQRDBText;
    QRLabelNormHTT: TQRLabel;
    QRLabelUsedHTT: TQRLabel;
    QRLabelAvailHTT: TQRLabel;
    QRLabelPlannedHTT: TQRLabel;
    QRLabelEarnedWTRTT: TQRLabel;
    QRLabelUsedWTRTT: TQRLabel;
    QRLabelAvailWTRTT: TQRLabel;
    QRLabelPlannedTWRTT: TQRLabel;
    QRLabelEarnedTFTTT: TQRLabel;
    QRLabelUsedTFTTT: TQRLabel;
    QRLabelAvailTFTTT: TQRLabel;
    QRLabelPlannedTFTTT: TQRLabel;
    QRLabel31: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRBndGrpFooterDept: TQRBand;
    QRLabel32: TQRLabel;
    QRShape6: TQRShape;
    QRDBText9: TQRDBText;
    QRLabelNormHDT: TQRLabel;
    QRLabelUsedHDT: TQRLabel;
    QRLabelAvailHDT: TQRLabel;
    QRLabelPlannedHDT: TQRLabel;
    QRLabelEarnedWTRDT: TQRLabel;
    QRLabelUsedWTRDT: TQRLabel;
    QRLabelAvailWTRDT: TQRLabel;
    QRLabelPlannedTWRDT: TQRLabel;
    QRLabelEarnedTFTDT: TQRLabel;
    QRLabelUsedTFTDT: TQRLabel;
    QRLabelAvailTFTDT: TQRLabel;
    QRLabelPlannedTFTDT: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabelRemH: TQRLabel;
    QRLabelRemWTR: TQRLabel;
    QRLabelRemTFT: TQRLabel;
    QRLabelRemainHDT: TQRLabel;
    QRLabelRemainHTT: TQRLabel;
    QRLabelRemainHPT: TQRLabel;
    QRLabelRemainHT: TQRLabel;
    QRLabelRemainWTRDT: TQRLabel;
    QRLabelRemainWTRTT: TQRLabel;
    QRLabelRemainWTRPT: TQRLabel;
    QRLabelRemainWTRT: TQRLabel;
    QRLabelRemainTFTDT: TQRLabel;
    QRLabelRemainTFTTT: TQRLabel;
    QRLabelRemainTFTPT: TQRLabel;
    QRLabelRemainTFTT: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextEmplDescPrint(sender: TObject; var Value: String);
    procedure QRLabelNormHPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedHPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailHPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedWTRPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedWTRPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailWTRPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedTFTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedTFTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailTFTPrint(sender: TObject; var Value: String);
    procedure QRLabelNormHTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedHTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailHTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedWTRTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedWTRTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailWTRTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedTFTTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedTFTTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailTFTTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedHPrint(sender: TObject; var Value: String);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelPlannedTWRPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTFTPrint(sender: TObject; var Value: String);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelPlannedHTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTWRTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTFTTPrint(sender: TObject; var Value: String);
    procedure FormDestroy(Sender: TObject);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabel3Print(sender: TObject; var Value: String);
    procedure QRLabel4Print(sender: TObject; var Value: String);
    procedure QRLabel5Print(sender: TObject; var Value: String);
    procedure QRLabel6Print(sender: TObject; var Value: String);
    procedure QRLabel7Print(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
    procedure QRLabel9Print(sender: TObject; var Value: String);
    procedure QRLabel10Print(sender: TObject; var Value: String);
    procedure QRLabel12Print(sender: TObject; var Value: String);
    procedure QRLabel13Print(sender: TObject; var Value: String);
    procedure QRLabel14Print(sender: TObject; var Value: String);
    procedure QRLabel15Print(sender: TObject; var Value: String);
    procedure QRLabel16Print(sender: TObject; var Value: String);
    procedure QRLabel17Print(sender: TObject; var Value: String);
    procedure QRLabel18Print(sender: TObject; var Value: String);
    procedure QRGrpHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndGrpFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGrpHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBndGrpFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpHDTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndGrpFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndGrpFooterTeamAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndGrpFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndGrpFooterDeptAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpHDTeamAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpHDDeptAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelNormHDTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedHDTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailHDTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedHDTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedWTRDTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedWTRDTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailWTRDTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTWRDTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedTFTDTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedTFTDTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailTFTDTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTFTDTPrint(sender: TObject; var Value: String);
    procedure QRLabelNormHTTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedHTTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailHTTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedHTTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedWTRTTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedWTRTTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailWTRTTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTWRTTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedTFTTTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedTFTTTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailTFTTTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTFTTTPrint(sender: TObject; var Value: String);
    procedure QRLabelNormHPTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedHPTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailHPTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedHPTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedWTRPTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedWTRPTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailWTRPTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTWRPTPrint(sender: TObject; var Value: String);
    procedure QRLabelEarnedTFTPTPrint(sender: TObject; var Value: String);
    procedure QRLabelUsedTFTPTPrint(sender: TObject; var Value: String);
    procedure QRLabelAvailTFTPTPrint(sender: TObject; var Value: String);
    procedure QRLabelPlannedTFTPTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemHPrint(sender: TObject; var Value: String);
    procedure QRLabelRemWTRPrint(sender: TObject; var Value: String);
    procedure QRLabelRemTFTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainHDTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainHTTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainHPTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainHTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainWTRDTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainWTRTTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainWTRPTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainWTRTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainTFTDTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainTFTTTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainTFTPTPrint(sender: TObject; var Value: String);
    procedure QRLabelRemainTFTTPrint(sender: TObject; var Value: String);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FExistAvail: Boolean;
    FListAvail: TStringList;
    FYear, FDay: Word;
    FREMAIN_HOLIDAY, FNORM_HOLIDAY, FUSED_HOLIDAY, FPlannedHol,
    FREMAIN_WTR, FEARNED_WTR, FUSED_WTR, FPlannedWTR,
    FREMAIN_TFT, FEARNED_TFT, FUSED_TFT, FPlannedTFT,
    FPlannedHol_Tot, FPlannedTFT_Tot, FPlannedWTR_Tot: Integer;

    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
      const StatusEmpl: Integer;
      const ShowHoliday, ShowWTR, ShowTimeForTime, SortOnDept, SortOnTeam: Boolean;
      const HolidayCaption, WTRCaption, TimeForTimeCaption: String;
      const AllTeam, ShowSelection, SuppressZeroes,
      ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;

  function SetQueryParameters(SelectStr: String; QuerySel: TQuery): Boolean;
  end;

var
  ReportAvailableUsedTimeQR: TReportAvailableUsedTimeQR;
  ValueList: array[1..MAXVAL] of String;
  PIntList: array[1..MAXVAL] of Integer;
  TIntList: array[1..MAXVAL] of Integer;
  DIntList: array[1..MAXVAL] of Integer;
  GIntList: array[1..MAXVAL] of Integer;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAvailableUsedTimeDMT, ListProcsFRM,
  UGlobalFunctions, CalculateTotalHoursDMT, UPimsConst, UPimsMessageRes;

procedure ExportDetail;
var
  I: Integer;
  Line: String;
begin
  Line := '';
  for I := 1 to MAXVAL - 1 do
    Line := Line + ValueList[I] + ExportClass.Sep;
  Line := Line + ValueList[MAXVAL];
  ExportClass.AddText(Line);
end;

procedure TQRParameters.SetValues(
  BusinessFrom, BusinessTo, DeptFrom, DeptTo,
  TeamFrom, TeamTo: String;
  StatusEmpl: Integer;
  ShowHoliday, ShowWTR, ShowTimeForTime, SortOnDept, SortOnTeam: Boolean;
  HolidayCaption, WTRCaption, TimeForTimeCaption: String;
  AllTeam, ShowSelection, SuppressZeroes, ExportToFile: Boolean);
begin
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FStatusEmpl := StatusEmpl;
  FShowHoliday := ShowHoliday;
  FShowWTR := ShowWTR;
  FShowTimeForTime := ShowTimeForTime;
  FSortOnDept := SortOnDept;
  FSortOnTeam := SortOnTeam;
  FHolidayCaption := HolidayCaption;
  FWTRCaption := WTRCaption;
  FTimeForTimeCaption := TimeForTimeCaption;
  FAllTeam := AllTeam;
  FShowSelection := ShowSelection;
  FSuppressZeroes := SuppressZeroes;
  FExportToFile := ExportToFile;
end;

function TReportAvailableUsedTimeQR.QRSendReportParameters(
  const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo,
  TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
  const StatusEmpl: Integer;
  const ShowHoliday, ShowWTR, ShowTimeForTime, SortOnDept, SortOnTeam: Boolean;
  const HolidayCaption, WTRCaption, TimeForTimeCaption: String;
  const AllTeam, ShowSelection, SuppressZeroes, ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      BusinessFrom, BusinessTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo, StatusEmpl,
      ShowHoliday, ShowWTR, ShowTimeForTime, SortOnDept, SortOnTeam,
      HolidayCaption, WTRCaption, TimeForTimeCaption,
      AllTeam, ShowSelection,
      SuppressZeroes, ExportToFile);
  end;
  SetDataSetQueryReport(ReportAvailableUsedTimeDM.QueryEmpl);
end;

function TReportAvailableUsedTimeQR.SetQueryParameters(SelectStr: String;
  QuerySel: TQuery): Boolean;
begin
  QuerySel.Active := False;
  QuerySel.UniDirectional := False;
  QuerySel.SQL.Clear;
  QuerySel.SQL.Add(SelectStr);
// QuerySel.SQL.SaveToFile('c:\temp\report_avail_used_time1.sql');
  if (QRParameters.FStatusEmpl <> 2) then
    QuerySel.ParamByName('FDATE').AsDateTime := Trunc(Now);
  QuerySel.ParamByName('PLANTFROM').AsString :=
    QRBaseParameters.FPlantFrom;
  QuerySel.ParamByName('PLANTTO').AsString :=
    QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    QuerySel.ParamByName('BUSINESSUNITFROM').AsString :=
      QRParameters.FBusinessFrom;
    QuerySel.ParamByName('BUSINESSUNITTO').AsString :=
      QRParameters.FBusinessTo;
    QuerySel.ParamByName('DEPARTMENTFROM').AsString :=
      QRParameters.FDeptFrom;
    QuerySel.ParamByName('DEPARTMENTTO').AsString :=
      QRParameters.FDeptTo;
    QuerySel.ParamByName('EMPLOYEEFROM').AsString :=
      QRBaseParameters.FEmployeeFrom;
    QuerySel.ParamByName('EMPLOYEETO').AsString :=
      QRBaseParameters.FEmployeeTo;
  end;
  if not QRParameters.FAllTeam then
  begin
    QuerySel.ParamByName('TEAMFROM').AsString :=
      QRParameters.FTeamFrom;
    QuerySel.ParamByName('TEAMTO').AsString :=
      QRParameters.FTeamTo;
  end;
  if not QuerySel.Prepared then
    QuerySel.Prepare;
  QuerySel.Active := True;
  Result := not QuerySel.IsEmpty;
end;

{function TReportAvailableUsedTimeQR.AvailableEmpl(Empl: Integer;
  StrHours: String; var Shift: Integer; var Plant: String): Boolean;
begin
  with ReportAvailableUsedTimeDM.TableEmplAvailable do
  begin
    Result := False;
    First;
    FindNearest([Empl]);
    while (FieldByName('Employee_Number').AsInteger = Empl) and
      (not Eof) do
    begin
      if (FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime >= Now ) and
        ((FieldByName('AVAILABLE_TIMEBLOCK_1').AsString = StrHours) or
        (FieldByName('AVAILABLE_TIMEBLOCK_2').AsString = StrHours) or
        (FieldByName('AVAILABLE_TIMEBLOCK_3').AsString = StrHours) or
        (FieldByName('AVAILABLE_TIMEBLOCK_4').AsString = StrHours)) then
      begin
        Result := True;
        Shift := FieldByName('SHIFT_NUMBER').AsInteger;
        Plant := FieldByName('PLANT_CODE').AsString;
        Exit;
      end;
      Next;
    end;
  end;
end; }

function TReportAvailableUsedTimeQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  Month: Word;
  Save_Emp: Integer;
begin
  Screen.Cursor := crHourGlass;
  with ReportAvailableUsedTimeDM do
  begin
    DecodeDate(Now, FYear, Month, FDay);
    FDay := ListProcsF.DayWStartOnFromDate(Now);
    SelectStr :=
      'SELECT ' + NL +
      '  E.PLANT_CODE, P.DESCRIPTION AS PDESCRIPTION, ' + NL +
      '  E.TEAM_CODE, T.DESCRIPTION AS TDESCRIPTION, ' + NL +
      '  E.DEPARTMENT_CODE, D.DESCRIPTION AS DDESCRIPTION, ' + NL +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
      '  E.STARTDATE, E.ENDDATE, ' + NL +
      '  E.DEPARTMENT_CODE, A.USED_HOLIDAY_MINUTE, ' + NL +
      '  A.LAST_YEAR_HOLIDAY_MINUTE, '+ NL +
      '  A.NORM_HOLIDAY_MINUTE, A.LAST_YEAR_WTR_MINUTE, ' + NL +
      '  A.EARNED_WTR_MINUTE, ' + NL +
      '  A.USED_WTR_MINUTE, A.LAST_YEAR_TFT_MINUTE, A.EARNED_TFT_MINUTE, ' + NL +
      '  A.USED_TFT_MINUTE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E LEFT JOIN ABSENCETOTAL A ON ' + NL +
      '    E.EMPLOYEE_NUMBER =  A.EMPLOYEE_NUMBER AND ' + NL +
      '    A.ABSENCE_YEAR = ' + IntToStr(FYear) + NL +
      '  INNER JOIN PLANT P ON ' + NL +
      '    E.PLANT_CODE = P.PLANT_CODE ' + NL +
      '  INNER JOIN DEPARTMENT D ON ' + NL +
      '    D.PLANT_CODE = E.PLANT_CODE AND ' + NL +
      '    D.DEPARTMENT_CODE = E.DEPARTMENT_CODE ' + NL +
      '  INNER JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'WHERE ' + NL +
      '  E.PLANT_CODE >= :PLANTFROM ' + NL +
      '  AND E.PLANT_CODE <= :PLANTTO ' + NL;
    if QRParameters.FStatusEmpl = 0 then
      SelectStr := SelectStr +
        '  AND (E.STARTDATE <= :FDATE) AND ' + NL +
        '  ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) ' + NL;
    if QRParameters.FStatusEmpl = 1 then
      SelectStr := SelectStr +
        '  AND ((E.STARTDATE > :FDATE) OR (E.ENDDATE < :FDATE)) ' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      SelectStr := SelectStr +
        '  AND D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM ' + NL +
        '  AND D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO ' + NL +
        '  AND E.DEPARTMENT_CODE >= :DEPARTMENTFROM ' + NL +
        '  AND E.DEPARTMENT_CODE <= :DEPARTMENTTO ' + NL +
        '  AND E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr +
        '  AND E.TEAM_CODE >= :TEAMFROM ' +  NL +
        '  AND E.TEAM_CODE <= :TEAMTO ' + NL;
    // RV054.4.
    // RV092.2. Wrong comparisons were made!
    if QRParameters.FSuppressZeroes then
      SelectStr := SelectStr +
        ' AND (A.USED_HOLIDAY_MINUTE <> 0 OR ' + NL +
        '   A.LAST_YEAR_HOLIDAY_MINUTE <> 0 OR ' + NL +
        '   A.NORM_HOLIDAY_MINUTE <> 0 OR ' + NL +
        '   A.LAST_YEAR_WTR_MINUTE <> 0 OR ' + NL +
        '   A.EARNED_WTR_MINUTE <> 0 OR ' + NL +
        '   A.USED_WTR_MINUTE <> 0 OR ' + NL +
        '   A.LAST_YEAR_TFT_MINUTE <> 0 OR ' + NL +
        '   A.EARNED_TFT_MINUTE <> 0 OR ' + NL +
        '   A.USED_TFT_MINUTE <> 0) ' + NL;
{
        ' AND A.USED_HOLIDAY_MINUTE + ' + NL +
        '  A.LAST_YEAR_HOLIDAY_MINUTE + ' + NL +
        '  A.NORM_HOLIDAY_MINUTE + ' + NL +
        '  A.LAST_YEAR_WTR_MINUTE + ' + NL +
        '  A.EARNED_WTR_MINUTE + ' + NL +
        '  A.USED_WTR_MINUTE + ' + NL +
        '  A.LAST_YEAR_TFT_MINUTE + ' + NL +
        '  A.EARNED_TFT_MINUTE + ' + NL +
        '  A.USED_TFT_MINUTE <> 0 ' + NL;
}
    SelectStr := SelectStr +
      'ORDER BY ' + NL;
    if QRParameters.FSortOnTeam and QRParameters.FSortOnDept then
      SelectStr := SelectStr +
        '  P.PLANT_CODE, T.TEAM_CODE, E.DEPARTMENT_CODE, E.EMPLOYEE_NUMBER'
    else
      if QRParameters.FSortOnTeam then
        SelectStr := SelectStr +
          '  P.PLANT_CODE, T.TEAM_CODE, E.EMPLOYEE_NUMBER'
      else
        if QRParameters.FSortOnDept then
          SelectStr := SelectStr +
            '  P.PLANT_CODE, E.DEPARTMENT_CODE, E.EMPLOYEE_NUMBER'
        else
          SelectStr := SelectStr +
            '  P.PLANT_CODE, E.EMPLOYEE_NUMBER';

    Result := SetQueryParameters(SelectStr, QueryEmpl);
    if not Result then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
  {check if report is empty}
// SELECT all availabilities defined per absence reason :
// 'H', 'W', 'T' with date >= systemdate
    SelectStr :=
      'SELECT ' + NL +
      '  DISTINCT EA.EMPLOYEEAVAILABILITY_DATE, ' + NL +
      '  EA.EMPLOYEE_NUMBER, EA.PLANT_CODE, EA.SHIFT_NUMBER, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_1, EA.AVAILABLE_TIMEBLOCK_2, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_3, EA.AVAILABLE_TIMEBLOCK_4, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_5, EA.AVAILABLE_TIMEBLOCK_6, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_7, EA.AVAILABLE_TIMEBLOCK_8, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_9, EA.AVAILABLE_TIMEBLOCK_10 ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY EA, ABSENCEREASON A, ' + NL +
      '  EMPLOYEE E, DEPARTMENT D ' + NL +
      'WHERE ' + NL +
      '  EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  AND D.PLANT_CODE = E.PLANT_CODE ' + NL +
      '  AND D.DEPARTMENT_CODE = E.DEPARTMENT_CODE ' + NL +
      '  AND EA.EMPLOYEEAVAILABILITY_DATE >= :FDATE ' + NL +
      '  AND E.PLANT_CODE >= :PLANTFROM ' + NL +
      '  AND E.PLANT_CODE <= :PLANTTO ' + NL;
    if QRParameters.FStatusEmpl = 0 then
      SelectStr := SelectStr +
        '  AND (E.STARTDATE <= :FDATE) AND ' + NL +
        '  ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) ' + NL;
    if QRParameters.FStatusEmpl = 1 then
      SelectStr := SelectStr +
        '  AND ((E.STARTDATE > :FDATE) OR (E.ENDDATE < :FDATE)) ' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      SelectStr := SelectStr +
        '  AND D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM ' + NL +
        '  AND D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO ' + NL +
        '  AND E.DEPARTMENT_CODE >= :DEPARTMENTFROM ' + NL +
        '  AND E.DEPARTMENT_CODE <= :DEPARTMENTTO ' + NL +
        '  AND EA.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND EA.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr +
        '  AND E.TEAM_CODE >= :TEAMFROM ' + NL +
        '  AND E.TEAM_CODE <= :TEAMTO ' + NL;
    SelectStr := SelectStr +
      '  AND A.ABSENCETYPE_CODE IN (''T'', ''H'', ''W'') ' + NL +
      '  AND ( (EA.AVAILABLE_TIMEBLOCK_1 = A.ABSENCEREASON_CODE) OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_2 = A.ABSENCEREASON_CODE)  OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_3 = A.ABSENCEREASON_CODE) OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_4 = A.ABSENCEREASON_CODE)  OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_5 = A.ABSENCEREASON_CODE) OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_6 = A.ABSENCEREASON_CODE)  OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_7 = A.ABSENCEREASON_CODE) OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_8 = A.ABSENCEREASON_CODE)  OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_9 = A.ABSENCEREASON_CODE) OR ' + NL +
      '      (EA.AVAILABLE_TIMEBLOCK_10 = A.ABSENCEREASON_CODE) ) ' + NL +
      'ORDER BY ' + NL +
      '  EA.EMPLOYEE_NUMBER' + NL;
    QueryEmpAvail.Close;
    QueryEmpAvail.SQL.Clear;
    QueryEmpAvail.SQL.Add(SelectStr);
//QueryEmpAvail.SQL.SaveToFile('c:\temp\report_avail_used_time2.sql');
    QueryEmpAvail.ParamByName('FDATE').AsDateTime := Trunc(Now);
    QueryEmpAvail.ParamByName('PLANTFROM').AsString :=
      QRBaseParameters.FPlantFrom;
    QueryEmpAvail.ParamByName('PLANTTO').AsString :=
      QRBaseParameters.FPlantTo;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      QueryEmpAvail.ParamByName('BUSINESSUNITFROM').AsString :=
        QRParameters.FBusinessFrom;
      QueryEmpAvail.ParamByName('BUSINESSUNITTO').AsString :=
        QRParameters.FBusinessTo;
      QueryEmpAvail.ParamByName('DEPARTMENTFROM').AsString :=
        QRParameters.FDeptFrom;
      QueryEmpAvail.ParamByName('DEPARTMENTTO').AsString :=
        QRParameters.FDeptTo;
      QueryEmpAvail.ParamByName('EMPLOYEEFROM').AsString :=
        QRBaseParameters.FEmployeeFrom;
      QueryEmpAvail.ParamByName('EMPLOYEETO').AsString :=
        QRBaseParameters.FEmployeeTo;
    end;
    if not QRParameters.FAllTeam then
    begin
      QueryEmpAvail.ParamByName('TEAMFROM').AsString :=
        QRParameters.FTeamFrom;
      QueryEmpAvail.ParamByName('TEAMTO').AsString :=
        QRParameters.FTeamTo;
    end;
    if not QueryEmpAvail.Prepared then
      QueryEmpAvail.Prepare;
    QueryEmpAvail.Open;
    FExistAvail := not QueryEmpAvail.IsEmpty;
    if not QueryEmpAvail.IsEmpty then
    begin
      QueryEmpAvail.First;
      FListAvail.Clear;
      Save_Emp := -1;
      while not QueryEmpAvail.Eof do
      begin
         if (Save_Emp <>
           QueryEmpAvail.FieldByName('EMPLOYEE_NUMBER').AsInteger) then
         begin
           FListAvail.Add('!' +
             QueryEmpAvail.FieldByName('EMPLOYEE_NUMBER').AsString);
           Save_Emp := QueryEmpAvail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
         end;
         FListAvail.Add(QueryEmpAvail.FieldByName('SHIFT_NUMBER').AsString +
           '@'+
           QueryEmpAvail.FieldByName('PLANT_CODE').AsString + '@'+
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString  + '@' +
           QueryEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString + '@');
         QueryEmpAvail.Next;
      end;
    end;
    Screen.Cursor := crDefault;
  end;
end;

procedure TReportAvailableUsedTimeQR.ConfigReport;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes; // RV054.4.

  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

end;

procedure TReportAvailableUsedTimeQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportAvailableUsedTimeQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  FListAvail := TStringList.Create;
  // MR:08-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportAvailableUsedTimeQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  I: Integer;
begin
  inherited;
  for I := 1 to MAXVAL do
    GIntList[I] := 0;

  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FShowSelection then
  begin
    QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
    QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
      QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
      QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
      QRLabelDeptTo.Caption := QRParameters.FDeptTo;
      QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
      QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
    end
    else
    begin
      QRLabelBusinessFrom.Caption := '*';
      QRLabelBusinessTo.Caption := '*';
      QRLabelDeptFrom.Caption := '*';
      QRLabelDeptTo.Caption := '*';
      QRLabelEmployeeFrom.Caption := '*';
      QRLabelEmployeeTo.Caption := '*';
    end;
    if QRParameters.FAllTeam then
    begin
      QRLabelTeamFrom.Caption := '*';
      QRLabelTeamTo.Caption := '*';
    end
    else
    begin
      QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
      QRLabelTeamTo.Caption := QRParameters.FTeamTo;
    end;
    QRLabelYear.Caption := DateToStr(Now);
    if QRParameters.FStatusEmpl = 0 then
      QRLabelEmplStatus.Caption := SActive;
    if QRParameters.FStatusEmpl = 1 then
      QRLabelEmplStatus.Caption := SInactive;
    if QRParameters.FStatusEmpl = 2 then
      QRLabelEmplStatus.Caption := SAll;
  end;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Create a header
    if QRParameters.FShowSelection then
    begin
      ExportClass.AddText(QRLabel11.Caption);
      // From Plant PlantFrom to PlantTo
      ExportClass.AddText(
        QRLabel19.Caption + ' ' +
        QRLabel20.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption
        );
      // From BusinessUnit BUFrom to BUTo
      ExportClass.AddText(
        QRLabel25.Caption + ' ' +
        QRLabel48.Caption + ' ' +
        QRLabelBusinessFrom.Caption + ' ' +
        QRLabel21.Caption + ' ' +
        QRLabelBusinessTo.Caption
        );
      // From Department DepartmentFrom to DepartmentTo
      ExportClass.AddText(
        QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' +
        QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' +
        QRLabelDeptTo.Caption
        );
      // From Team TeamFrom to TeamTo
      ExportClass.AddText(
        QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' +
        QRLabelTeamFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' +
        QRLabelTeamTo.Caption
        );
      // From Employee EmployeeFrom to EmployeeTo
      ExportClass.AddText(
        QRLabel22.Caption + ' ' +
        QRLabel23.Caption + ' ' +
        QRLabelEmployeeFrom.Caption + ' ' +
        QRLabel24.Caption + ' ' +
        QRLabelEmployeeTo.Caption
        );
      ExportClass.AddText(QRLabelDateCurrect.Caption + ' ' +
        QRLabelYear.Caption);
      ExportClass.AddText(QRLabel1.Caption + ' ' +
        QRLabelEmplStatus.Caption);
    end;
  end;
  QRLabel3.Caption := QRParameters.FHolidayCaption;
  QRLabel8.Caption := QRParameters.FWTRCaption;
  QRLabel14.Caption := QRParameters.FTimeForTimeCaption;
  FREMAIN_HOLIDAY := 0;
  FNORM_HOLIDAY := 0;
  FUSED_HOLIDAY := 0;
  FREMAIN_WTR := 0;
  FEARNED_WTR := 0;
  FUSED_WTR := 0;
  FREMAIN_TFT := 0;
  FEARNED_TFT := 0;
  FUSED_TFT := 0;
  FPlannedHol_Tot := 0;
  FPlannedTFT_Tot := 0;
  FPlannedWTR_Tot := 0;
end;

procedure TReportAvailableUsedTimeQR.QRDBTextEmplDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
//  Value := Copy(Value,0,12);
end;

procedure TReportAvailableUsedTimeQR.QRLabelNormHPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('NORM_HOLIDAY_MINUTE').AsInteger;
    PIntList[3] := PIntList[3] + IntValue;
    TIntList[3] := TIntList[3] + IntValue;
    DIntList[3] := DIntList[3] + IntValue;
    GIntList[3] := GIntList[3] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowHoliday then
      Value := '';
    ValueList[3] := Value;
  end;
  FNORM_HOLIDAY := FNORM_HOLIDAY + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedHPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('USED_HOLIDAY_MINUTE').AsInteger;
    PIntList[4] := PIntList[4] + IntValue;
    TIntList[4] := TIntList[4] + IntValue;
    DIntList[4] := DIntList[4] + IntValue;
    GIntList[4] := GIntList[4] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowHoliday then
      Value := '';
    ValueList[4] := Value;
  end;
  FUSED_HOLIDAY := FUSED_HOLIDAY + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailHPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_HOLIDAY_MINUTE').AsInteger +
      FieldByName('NORM_HOLIDAY_MINUTE').AsInteger -
      FieldByName('USED_HOLIDAY_MINUTE').AsInteger -
      FPlannedHOL;
    PIntList[6] := PIntList[6] + IntValue;
    TIntList[6] := TIntList[6] + IntValue;
    DIntList[6] := DIntList[6] + IntValue;
    GIntList[6] := GIntList[6] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowHoliday then
      Value := '';
    ValueList[6] := Value;
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedHPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FPlannedHOL);
  PIntList[5] := PIntList[5] + FPlannedHOL;
  TIntList[5] := TIntList[5] + FPlannedHOL;
  DIntList[5] := DIntList[5] + FPlannedHOL;
  GIntList[5] := GIntList[5] + FPlannedHOL;
  if not QRParameters.FShowHoliday then
    Value := '';
  ValueList[5] := Value;
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedWTRPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('EARNED_WTR_MINUTE').AsInteger;
    PIntList[8] := PIntList[8] + IntValue;
    TIntList[8] := TIntList[8] + IntValue;
    DIntList[8] := DIntList[8] + IntValue;
    GIntList[8] := GIntList[8] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowWTR then
      Value := '';
    ValueList[8] := Value;
  end;
  FEARNED_WTR := FEARNED_WTR + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedWTRPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('USED_WTR_MINUTE').AsInteger;
    PIntList[9] := PIntList[9] + IntValue;
    TIntList[9] := TIntList[9] + IntValue;
    DIntList[9] := DIntList[9] + IntValue;
    GIntList[9] := GIntList[9] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowWTR then
      Value := '';
    ValueList[9] := Value;
  end;
  FUSED_WTR := FUSED_WTR + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailWTRPrint(
  sender: TObject; var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_WTR_MINUTE').AsInteger +
      FieldByName('EARNED_WTR_MINUTE').AsInteger -
      FieldByName('USED_WTR_MINUTE').AsInteger -
      FPlannedWTR;
    PIntList[11] := PIntList[11] + IntValue;
    TIntList[11] := TIntList[11] + IntValue;
    DIntList[11] := DIntList[11] + IntValue;
    GIntList[11] := GIntList[11] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowWTR then
      Value := '';
    ValueList[11] := Value;
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTWRPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FPlannedWTR);
  PIntList[10] := PIntList[10] + FPlannedWTR;
  TIntList[10] := TIntList[10] + FPlannedWTR;
  DIntList[10] := DIntList[10] + FPlannedWTR;
  GIntList[10] := GIntList[10] + FPlannedWTR;
  if not QRParameters.FShowWTR then
    Value := '';
  ValueList[10] := Value;
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedTFTPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('EARNED_TFT_MINUTE').AsInteger;
    PIntList[13] := PIntList[13] + IntValue;
    TIntList[13] := TIntList[13] + IntValue;
    DIntList[13] := DIntList[13] + IntValue;
    GIntList[13] := GIntList[13] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowTimeForTime then
      Value := '';
    ValueList[13] := Value;
  end;
  FEARNED_TFT := FEARNED_TFT + IntValue;
end;


procedure TReportAvailableUsedTimeQR.QRLabelUsedTFTPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('USED_TFT_MINUTE').AsInteger;
    PIntList[14] := PIntList[14] + IntValue;
    TIntList[14] := TIntList[14] + IntValue;
    DIntList[14] := DIntList[14] + IntValue;
    GIntList[14] := GIntList[14] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowTimeForTime then
      Value := '';
    ValueList[14] := Value;
  end;
  FUSED_TFT := FUSED_TFT + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailTFTPrint(
  sender: TObject; var Value: String);
var
  IntValue: Integer;
begin
  inherited;

  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_TFT_MINUTE').AsInteger +
      FieldByName('EARNED_TFT_MINUTE').AsInteger -
      FieldByName('USED_TFT_MINUTE').AsInteger -
      FPlannedTFT;
    PIntList[16] := PIntList[16] + IntValue;
    TIntList[16] := TIntList[16] + IntValue;
    DIntList[16] := DIntList[16] + IntValue;
    GIntList[16] := GIntList[16] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowTimeForTime then
      Value := '';
    ValueList[16] := Value;
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabelNormHTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedHTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailHTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedHTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedWTRTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedWTRTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailWTRTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTWRTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedTFTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedTFTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailTFTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTFTPrint(
  sender: TObject; var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  IntValue := FPlannedTFT;
  Value := DecodeHrsMin(FPlannedTFT);
  PIntList[15] := PIntList[15] + IntValue;
  TIntList[15] := TIntList[15] + IntValue;
  DIntList[15] := DIntList[15] + IntValue;
  GIntList[15] := GIntList[15] + IntValue;
  if not QRParameters.FShowTimeForTime then
    Value := '';
  ValueList[15] := Value;
end;

procedure TReportAvailableUsedTimeQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Empl, Shift, i, IndexList, Save_Shift, p: Integer;
  Dept, Plant, StrTmp, Save_Plant: String;
  TBStr: Array[1..10] of string; // GLOB3-208
  // MR:06-10-2003
  AbsenceTypeCode: String;
  //MR:06-10-2003 Search the AbsenceTypeCode by AbsenceReasonCode
  function SearchAbsenceTypeCode(AbsenceReasonCode: String): String;
  begin
    if ReportAvailableUsedTimeDM.cdsAbsenceReason.
      FindKey([AbsenceReasonCode]) then
      Result :=
        ReportAvailableUsedTimeDM.cdsAbsenceReason.
          FieldByName('ABSENCETYPE_CODE').AsString
    else
      Result :=
        AbsenceReasonCode;
  end;
begin
  inherited;
  FPlannedHol := 0;
  FPlannedTFT := 0;
  FPlannedWTR := 0;

  if not FExistAvail then
    exit;
  Empl := ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').
      AsInteger;
  Dept := ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('DEPARTMENT_CODE').
      AsString;
  IndexList := FListAvail.IndexOf('!' + IntToStr(Empl));
//CAR 9-7-2003 add extra check for indexlist
  if (IndexList < 0) or ((IndexList + 1) > (FListAvail.Count -1)) then
    exit;
  // extract the data from list availabilities
  IndexList := IndexList + 1;
  StrTmp := FListAvail.Strings[IndexList];
  Save_Shift := -1; Save_Plant := '-1';
  while (Pos('!', StrTmp) = 0 )   do
  begin
    p := Pos('@', StrTmp);
    Shift := StrToInt(Copy(strtmp, 0 , p-1));
    strtmp := copy(StrTmp,p+1, length(strtmp) + 1);  p := Pos('@', StrTmp);
    Plant := Copy(strtmp, 0 , p-1);
    strtmp := copy(StrTmp,p+1, length(strtmp) + 1); P := Pos('@', StrTmp);
    i := 1;
    while i < SystemDM.MaxTimeBlocks+1 do // GLOB3-208
    begin
      TBStr[i] := Copy(StrTmp, 0, p-1);
      strtmp := copy(StrTmp,p+1, length(strtmp) + 1); P := Pos('@', StrTmp);
      inc(i);
    end;
    if (Save_Shift <> Shift) or (Save_Plant <> Plant) then
    begin
    //CAR 23-7- 2003
      ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept, True);

      Save_Shift := Shift; Save_Plant := Plant;
    end;
    for i := 1 to SystemDM.MaxTimeblocks do // GLOB3-208
    begin
      // MR:06-10-2003 Search the AbsenceTypeCode by AbsenceReasonCode
      AbsenceTypeCode := SearchAbsenceTypeCode(TBStr[i]);
      if (AbsenceTypeCode = HOLIDAYAVAILABLETB) then
          FPlannedHol := FPlannedHol + ATBLengthClass.GetLengthTB(i, FDay);
      if (AbsenceTypeCode = TIMEFORTIMEAVAILABILITY) then
        FPlannedTFT := FPlannedTFT + ATBLengthClass.GetLengthTB(i, FDay);
      if (AbsenceTypeCode = WORKTIMEREDUCTIONAVAILABILITY) then
         FPlannedWTR := FPlannedWTR + ATBLengthClass.GetLengthTB(i, FDay);
    end;
    IndexList := IndexList + 1;
    if IndexList < FListAvail.Count then
      StrTmp := FListAvail.Strings[IndexList]
    else
      break;
  end;{while}


end;

procedure TReportAvailableUsedTimeQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  FPlannedHol_Tot := FPlannedHol_Tot + FPlannedHol;
  FPlannedTFT_Tot := FPlannedTFT_Tot + FPlannedTFT;
  FPlannedWTR_Tot := FPlannedWTR_Tot + FPlannedWTR;
  if QRParameters.FExportToFile then
  begin
    with ReportAvailableUsedTimeDM do
    begin
      ValueList[1] := QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
        QueryEmpl.FieldByName('DESCRIPTION').AsString;
    end;
    ExportDetail;
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTFTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.FormDestroy(Sender: TObject);
begin
  inherited;
  FListAvail.Free;
end;

procedure TReportAvailableUsedTimeQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
begin
  inherited;
  if BandPrinted and QRParameters.FExportToFile and
    (not ExportClass.ExportDone) then
  begin
    Line := QRLabel28.Caption + ExportClass.Sep;
    if QRParameters.FShowHoliday then
      Line := Line +
        DecodeHrsMin(GIntList[2]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[3]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[4]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[5]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[6]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowWTR then
      Line := Line +
        DecodeHrsMin(GIntList[7]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[8]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[9]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[10]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[11]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowTimeForTime then
      Line := Line +
        DecodeHrsMin(GIntList[12]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[13]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[14]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[15]) + ExportClass.Sep +
        DecodeHrsMin(GIntList[16]) + ExportClass.Sep
     else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
     ExportClass.AddText(Line);
  end;
  if QRParameters.FExportToFile then
  begin
//    ValueList[1] := QRLabel28.Caption;
//    ExportDetail;
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportAvailableUsedTimeQR.ChildBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // Column headers
    ValueList[1] := '';
    // Holiday
    ValueList[2] := QRLabel3.Caption;
    ValueList[3] := '';
    ValueList[4] := '';
    ValueList[5] := '';
    ValueList[6] := '';
    // WTR
    ValueList[7] := QRLabel8.Caption;
    ValueList[8] := '';
    ValueList[9] := '';
    ValueList[10] := '';
    ValueList[11] := '';
    // TFT
    ValueList[12] := QRLabel14.Caption;
    ValueList[13] := '';
    ValueList[14] := '';
    ValueList[15] := '';
    ValueList[16] := '';
    ExportDetail;
    ValueList[1] := '';
    // Holiday
    ValueList[2] := QRLabel33.Caption;
    ValueList[3] := QRLabel4.Caption;
    ValueList[4] := QRLabel5.Caption;
    ValueList[5] := QRLabel6.Caption;
    ValueList[6] := QRLabel7.Caption;
    // WTR
    ValueList[7] := QRLabel34.Caption;
    ValueList[8] := QRLabel9.Caption;
    ValueList[9] := QRLabel10.Caption;
    ValueList[10] := QRLabel12.Caption;
    ValueList[11] := QRLabel13.Caption;
    // TFT
    ValueList[12] := QRLabel35.Caption;
    ValueList[13] := QRLabel15.Caption;
    ValueList[14] := QRLabel16.Caption;
    ValueList[15] := QRLabel17.Caption;
    ValueList[16] := QRLabel18.Caption;
    ExportDetail;
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabel3Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel4Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel5Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel6Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel7Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel8Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel9Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel10Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel12Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel13Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel14Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel15Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel16Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel17Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabel18Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  I: Integer;
begin
  inherited;
  for I := 1 to MAXVAL do
    PIntList[I] := 0;
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelRemainHPT.Caption := DecodeHrsMin(PIntList[2]);
  QRLabelNormHPT.Caption := DecodeHrsMin(PIntList[3]);
  QRLabelUsedHPT.Caption := DecodeHrsMin(PIntList[4]);
  QRLabelPlannedHPT.Caption := DecodeHrsMin(PIntList[5]);
  QRLabelAvailHPT.Caption := DecodeHrsMin(PIntList[6]);

  QRLabelRemainWTRPT.Caption := DecodeHrsMin(PIntList[7]);
  QRLabelEarnedWTRPT.Caption := DecodeHrsMin(PIntList[8]);
  QRLabelUsedWTRPT.Caption := DecodeHrsMin(PIntList[9]);
  QRLabelPlannedTWRPT.Caption := DecodeHrsMin(PIntList[10]);
  QRLabelAvailWTRPT.Caption := DecodeHrsMin(PIntList[11]);

  QRLabelRemainTFTPT.Caption := DecodeHrsMin(PIntList[12]);
  QRLabelEarnedTFTPT.Caption := DecodeHrsMin(PIntList[13]);
  QRLabelUsedTFTPT.Caption := DecodeHrsMin(PIntList[14]);
  QRLabelPlannedTFTPT.Caption := DecodeHrsMin(PIntList[15]);
  QRLabelAvailTFTPT.Caption := DecodeHrsMin(PIntList[16]);
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted and
    QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AddText(QRLabel26.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('PDESCRIPTION').AsString
      );
  end;
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
begin
  inherited;
  if BandPrinted and QRParameters.FExportToFile and
    (not ExportClass.ExportDone) then
  begin
    Line := QRLabel27.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('PLANT_CODE').AsString +
      ExportClass.Sep;
    if QRParameters.FShowHoliday then
      Line := Line +
        DecodeHrsMin(PIntList[2]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[3]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[4]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[5]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[6]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowWTR then
      Line := Line +
        DecodeHrsMin(PIntList[7]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[8]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[9]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[10]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[11]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowTimeForTime then
      Line := Line +
        DecodeHrsMin(PIntList[12]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[13]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[14]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[15]) + ExportClass.Sep +
        DecodeHrsMin(PIntList[16]) + ExportClass.Sep
     else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
     ExportClass.AddText(Line);
  end;
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  I: Integer;
begin
  inherited;
  for I := 1 to MAXVAL do
    TIntList[I] := 0;
  PrintBand := QRParameters.FSortOnTeam;
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FSortOnTeam;

  QRLabelRemainHTT.Caption := DecodeHrsMin(TIntList[2]);
  QRLabelNormHTT.Caption := DecodeHrsMin(TIntList[3]);
  QRLabelUsedHTT.Caption := DecodeHrsMin(TIntList[4]);
  QRLabelPlannedHTT.Caption := DecodeHrsMin(TIntList[5]);
  QRLabelAvailHTT.Caption := DecodeHrsMin(TIntList[6]);

  QRLabelRemainWTRTT.Caption := DecodeHrsMin(TIntList[7]);
  QRLabelEarnedWTRTT.Caption := DecodeHrsMin(TIntList[8]);
  QRLabelUsedWTRTT.Caption := DecodeHrsMin(TIntList[9]);
  QRLabelPlannedTWRTT.Caption := DecodeHrsMin(TIntList[10]);
  QRLabelAvailWTRTT.Caption := DecodeHrsMin(TIntList[11]);

  QRLabelRemainTFTTT.Caption := DecodeHrsMin(TIntList[12]);
  QRLabelEarnedTFTTT.Caption := DecodeHrsMin(TIntList[13]);
  QRLabelUsedTFTTT.Caption := DecodeHrsMin(TIntList[14]);
  QRLabelPlannedTFTTT.Caption := DecodeHrsMin(TIntList[15]);
  QRLabelAvailTFTTT.Caption := DecodeHrsMin(TIntList[16]);
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterTeamAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
begin
  inherited;
  if BandPrinted and QRParameters.FExportToFile and
    (not ExportClass.ExportDone) then
  begin
    Line := QRLabel30.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('TEAM_CODE').AsString +
      ExportClass.Sep;
    if QRParameters.FShowHoliday then
      Line := Line +
        DecodeHrsMin(TIntList[2]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[3]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[4]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[5]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[6]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowWTR then
      Line := Line +
        DecodeHrsMin(TIntList[7]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[8]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[9]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[10]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[11]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowTimeForTime then
      Line := Line +
        DecodeHrsMin(TIntList[12]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[13]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[14]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[15]) + ExportClass.Sep +
        DecodeHrsMin(TIntList[16]) + ExportClass.Sep
     else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
     ExportClass.AddText(Line);
  end;
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  I: Integer;
begin
  inherited;
  for I := 1 to MAXVAL do
    DIntList[I] := 0;
  PrintBand := QRParameters.FSortOnDept;
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FSortOnDept;

  QRLabelRemainHDT.Caption := DecodeHrsMin(DIntList[2]);
  QRLabelNormHDT.Caption := DecodeHrsMin(DIntList[3]);
  QRLabelUsedHDT.Caption := DecodeHrsMin(DIntList[4]);
  QRLabelPlannedHDT.Caption := DecodeHrsMin(DIntList[5]);
  QRLabelAvailHDT.Caption := DecodeHrsMin(DIntList[6]);

  QRLabelRemainWTRDT.Caption := DecodeHrsMin(DIntList[7]);
  QRLabelEarnedWTRDT.Caption := DecodeHrsMin(DIntList[8]);
  QRLabelUsedWTRDT.Caption := DecodeHrsMin(DIntList[9]);
  QRLabelPlannedTWRDT.Caption := DecodeHrsMin(DIntList[10]);
  QRLabelAvailWTRDT.Caption := DecodeHrsMin(DIntList[11]);

  QRLabelRemainTFTDT.Caption := DecodeHrsMin(DIntList[12]);
  QRLabelEarnedTFTDT.Caption := DecodeHrsMin(DIntList[13]);
  QRLabelUsedTFTDT.Caption := DecodeHrsMin(DIntList[14]);
  QRLabelPlannedTFTDT.Caption := DecodeHrsMin(DIntList[15]);
  QRLabelAvailTFTDT.Caption := DecodeHrsMin(DIntList[16]);
end;

procedure TReportAvailableUsedTimeQR.QRBndGrpFooterDeptAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
begin
  inherited;
  if BandPrinted and QRParameters.FExportToFile and
    (not ExportClass.ExportDone) then
  begin
    Line := QRLabel32.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString +
      ExportClass.Sep;
    if QRParameters.FShowHoliday then
      Line := Line +
        DecodeHrsMin(DIntList[2]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[3]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[4]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[5]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[6]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowWTR then
      Line := Line +
        DecodeHrsMin(DIntList[7]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[8]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[9]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[10]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[11]) + ExportClass.Sep
    else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
    if QRParameters.FShowTimeForTime then
      Line := Line +
        DecodeHrsMin(DIntList[12]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[13]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[14]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[15]) + ExportClass.Sep +
        DecodeHrsMin(DIntList[16]) + ExportClass.Sep
     else
      Line := Line +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep;
     ExportClass.AddText(Line);
  end;
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDTeamAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted and
    QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AddText(QRLabel29.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('TEAM_CODE').AsString + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('TDESCRIPTION').AsString
      );
  end;
end;

procedure TReportAvailableUsedTimeQR.QRGrpHDDeptAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted and
    QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AddText(QRLabel31.Caption + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
      ReportAvailableUsedTimeDM.QueryEmpl.FieldByName('DDESCRIPTION').AsString
      );
  end;
end;

procedure TReportAvailableUsedTimeQR.QRLabelNormHDTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedHDTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailHDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedHDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedWTRDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedWTRDTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailWTRDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTWRDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedTFTDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedTFTDTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailTFTDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTFTDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelNormHTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedHTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailHTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedHTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedWTRTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedWTRTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailWTRTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTWRTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedTFTTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedTFTTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailTFTTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTFTTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelNormHPTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedHPTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailHPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedHPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedWTRPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedWTRPTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailWTRPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTWRPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelEarnedTFTPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelUsedTFTPTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelAvailTFTPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelPlannedTFTPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemHPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_HOLIDAY_MINUTE').AsInteger;
    PIntList[2] := PIntList[2] + IntValue;
    TIntList[2] := TIntList[2] + IntValue;
    DIntList[2] := DIntList[2] + IntValue;
    GIntList[2] := GIntList[2] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowHoliday then
      Value := '';
    ValueList[2] := Value;
  end;
  FREMAIN_HOLIDAY := FREMAIN_HOLIDAY + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemWTRPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_WTR_MINUTE').AsInteger;
    PIntList[7] := PIntList[7] + IntValue;
    TIntList[7] := TIntList[7] + IntValue;
    DIntList[7] := DIntList[7] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowWTR then
      Value := '';
    ValueList[7] := Value;
  end;
  FREMAIN_WTR := FREMAIN_WTR + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemTFTPrint(sender: TObject;
  var Value: String);
var
  IntValue: Integer;
begin
  inherited;
  with ReportAvailableUsedTimeDM.QueryEmpl do
  begin
    IntValue := FieldByName('LAST_YEAR_TFT_MINUTE').AsInteger;
    PIntList[12] := PIntList[12] + IntValue;
    TIntList[12] := TIntList[12] + IntValue;
    DIntList[12] := DIntList[12] + IntValue;
    Value := DecodeHrsMin(IntValue);
    if not QRParameters.FShowTimeForTime then
      Value := '';
    ValueList[12] := Value;
  end;
  FREMAIN_TFT := FREMAIN_TFT + IntValue;
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainHDTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainHTTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainHPTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainHTPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowHoliday then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainWTRDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainWTRTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainWTRPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainWTRTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowWTR then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainTFTDTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainTFTTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainTFTPTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRLabelRemainTFTTPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowTimeForTime then
    Value := '';
end;

procedure TReportAvailableUsedTimeQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelRemainHT.Caption := DecodeHrsMin(GIntList[2]);
  QRLabelNormHT.Caption := DecodeHrsMin(GIntList[3]);
  QRLabelUsedHT.Caption := DecodeHrsMin(GIntList[4]);
  QRLabelPlannedHT.Caption := DecodeHrsMin(GIntList[5]);
  QRLabelAvailHT.Caption := DecodeHrsMin(GIntList[6]);

  QRLabelRemainWTRT.Caption := DecodeHrsMin(GIntList[7]);
  QRLabelEarnedWTRT.Caption := DecodeHrsMin(GIntList[8]);
  QRLabelUsedWTRT.Caption := DecodeHrsMin(GIntList[9]);
  QRLabelPlannedTWRT.Caption := DecodeHrsMin(GIntList[10]);
  QRLabelAvailWTRT.Caption := DecodeHrsMin(GIntList[11]);

  QRLabelRemainTFTT.Caption := DecodeHrsMin(GIntList[12]);
  QRLabelEarnedTFTT.Caption := DecodeHrsMin(GIntList[13]);
  QRLabelUsedTFTT.Caption := DecodeHrsMin(GIntList[14]);
  QRLabelPlannedTFTT.Caption := DecodeHrsMin(GIntList[15]);
  QRLabelAvailTFTT.Caption := DecodeHrsMin(GIntList[16]);
end;

end.
