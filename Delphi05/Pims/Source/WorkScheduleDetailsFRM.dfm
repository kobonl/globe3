inherited WorkScheduleDetailsF: TWorkScheduleDetailsF
  Height = 602
  Caption = 'Work Schedule Details'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Height = 100
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 96
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 95
      Bands = <
        item
          Caption = 'Work Schedule'
        end>
      KeyField = 'WORKSCHEDULE_ID'
      DataSource = WorkScheduleDetailsDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnCODE: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CODE'
      end
      object dxMasterGridColumnDESCRIPTION: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnREPEATS: TdxDBGridColumn
        Caption = 'Repeats'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REPEATS'
      end
      object dxMasterGridColumnCALCREFERENCEWEEK: TdxDBGridColumn
        Caption = 'Reference week'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CALCREFERENCEWEEK'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 304
    Height = 259
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 257
      Align = alClient
      Caption = 'Work Schedule Details'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 40
        Width = 41
        Height = 13
        Caption = 'Absence'
      end
      object lblDay1: TLabel
        Left = 104
        Top = 40
        Width = 35
        Height = 13
        Caption = 'lblDay1'
      end
      object lblDay2: TLabel
        Left = 104
        Top = 72
        Width = 35
        Height = 13
        Caption = 'lblDay2'
      end
      object lblDay3: TLabel
        Left = 104
        Top = 104
        Width = 35
        Height = 13
        Caption = 'lblDay3'
      end
      object lblDay4: TLabel
        Left = 104
        Top = 136
        Width = 35
        Height = 13
        Caption = 'lblDay4'
      end
      object lblDay5: TLabel
        Left = 104
        Top = 168
        Width = 35
        Height = 13
        Caption = 'lblDay5'
      end
      object lblDay6: TLabel
        Left = 104
        Top = 200
        Width = 35
        Height = 13
        Caption = 'lblDay6'
      end
      object lblDay7: TLabel
        Left = 104
        Top = 232
        Width = 35
        Height = 13
        Caption = 'lblDay7'
      end
      object Label2: TLabel
        Left = 168
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object Label3: TLabel
        Left = 232
        Top = 16
        Width = 47
        Height = 13
        Caption = 'Abs. Rsn.'
      end
      object Label4: TLabel
        Left = 296
        Top = 16
        Width = 24
        Height = 13
        Caption = 'Type'
      end
      object Label5: TLabel
        Left = 488
        Top = 16
        Width = 40
        Height = 13
        Caption = 'Repeats'
        Visible = False
      end
      object Label6: TLabel
        Left = 8
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Line:'
      end
      object DBText1: TDBText
        Left = 72
        Top = 16
        Width = 65
        Height = 17
        DataField = 'MYROW'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift1: TdxDBEdit
        Left = 168
        Top = 37
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 0
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY1_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift2: TdxDBEdit
        Left = 168
        Top = 69
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 4
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY2_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift3: TdxDBEdit
        Left = 168
        Top = 100
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 8
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY3_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift4: TdxDBEdit
        Left = 168
        Top = 132
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 12
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY4_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift5: TdxDBEdit
        Left = 168
        Top = 163
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 16
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY5_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift6: TdxDBEdit
        Left = 168
        Top = 195
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 20
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY6_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditShift7: TdxDBEdit
        Left = 168
        Top = 226
        Width = 49
        PopupMenu = PopupMenuShift
        TabOrder = 24
        OnExit = dxDBEditShiftExit
        OnKeyDown = dxDBEditShiftKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY7_SHIFT'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn1: TdxDBEdit
        Left = 232
        Top = 37
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 1
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY1_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn2: TdxDBEdit
        Left = 232
        Top = 69
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 5
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY2_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn3: TdxDBEdit
        Left = 232
        Top = 100
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 9
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY3_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn4: TdxDBEdit
        Left = 232
        Top = 132
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 13
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY4_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn5: TdxDBEdit
        Left = 232
        Top = 163
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 17
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY5_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn6: TdxDBEdit
        Left = 232
        Top = 195
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 21
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY6_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object dxDBEditAbsRsn7: TdxDBEdit
        Left = 232
        Top = 226
        Width = 49
        PopupMenu = PopupMenuAbsRsn
        TabOrder = 25
        OnExit = dxDBEditAbsRsnExit
        OnKeyDown = dxDBEditAbsRsnKeyDown
        OnMouseDown = dxDBEditMouseDown
        DataField = 'DAY7_ABSRSN_CODE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
      end
      object DBRGrpType1: TDBRadioGroup
        Left = 295
        Top = 31
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY1_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 2
        Values.Strings = (
          '1'
          '2')
      end
      object DBRGrpType2: TDBRadioGroup
        Left = 295
        Top = 61
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY2_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 6
        Values.Strings = (
          '1'
          '2')
      end
      object DBRGrpType3: TDBRadioGroup
        Left = 295
        Top = 92
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY3_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 10
        Values.Strings = (
          '1'
          '2')
      end
      object dxDBSpinEditRepeats1: TdxDBSpinEdit
        Left = 488
        Top = 37
        Width = 57
        TabOrder = 3
        Visible = False
        DataField = 'DAY1_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object DBRGrpType4: TDBRadioGroup
        Left = 295
        Top = 124
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY4_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 14
        Values.Strings = (
          '1'
          '2')
      end
      object DBRGrpType5: TDBRadioGroup
        Left = 295
        Top = 156
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY5_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 18
        Values.Strings = (
          '1'
          '2')
      end
      object DBRGrpType6: TDBRadioGroup
        Left = 295
        Top = 188
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY6_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 22
        Values.Strings = (
          '1'
          '2')
      end
      object DBRGrpType7: TDBRadioGroup
        Left = 295
        Top = 220
        Width = 185
        Height = 31
        Columns = 2
        DataField = 'DAY7_TYPE'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        Items.Strings = (
          'Fixed'
          'Moving')
        TabOrder = 26
        Values.Strings = (
          '1'
          '2')
      end
      object dxDBSpinEditRepeats2: TdxDBSpinEdit
        Left = 488
        Top = 69
        Width = 57
        TabOrder = 7
        Visible = False
        DataField = 'DAY2_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object dxDBSpinEditRepeats3: TdxDBSpinEdit
        Left = 488
        Top = 101
        Width = 57
        TabOrder = 11
        Visible = False
        DataField = 'DAY3_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object dxDBSpinEditRepeats4: TdxDBSpinEdit
        Left = 488
        Top = 133
        Width = 57
        TabOrder = 15
        Visible = False
        DataField = 'DAY4_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object dxDBSpinEditRepeats5: TdxDBSpinEdit
        Left = 488
        Top = 165
        Width = 57
        TabOrder = 19
        Visible = False
        DataField = 'DAY5_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object dxDBSpinEditRepeats6: TdxDBSpinEdit
        Left = 488
        Top = 197
        Width = 57
        TabOrder = 23
        Visible = False
        DataField = 'DAY6_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
      object dxDBSpinEditRepeats7: TdxDBSpinEdit
        Left = 488
        Top = 229
        Width = 57
        TabOrder = 27
        Visible = False
        DataField = 'DAY7_REPEATS'
        DataSource = WorkScheduleDetailsDM.DataSourceDetail
        StoredValues = 48
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 126
    Height = 178
    inherited spltDetail: TSplitter
      Top = 169
      Height = 8
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 2
      Height = 168
      Bands = <
        item
          Caption = 'Work Schedule Details'
        end>
      KeyField = 'WORKSCHEDULELINE_ID'
      DataSource = WorkScheduleDetailsDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnMYROW: TdxDBGridColumn
        Caption = 'Line'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MYROW'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonEditMode: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Visible = ivNever
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Visible = ivNever
    end
  end
  object PopupMenuShift: TPopupMenu
    AutoHotkeys = maManual
    Left = 265
    Top = 35
  end
  object PopupMenuAbsRsn: TPopupMenu
    Left = 304
    Top = 68
  end
end
