(*
  Changes: MR: 1-11-2004 Order 550349
    Plan employees across plants.
  MRA:2-SEP-2010. RV067.MRA.23
  - Show week-period for year-week-component.
  MRA:1-NOV-2010. RV075.7.
  - Changed way arguments are assigned: year, week.
  MRA:21-APR-2011. RV090.1.
  - Add option to Confirm/Undo a copy-action.
  MRA:12-MAY-2011. RV092.16. Addition.
  - Shift Schedule
    - When called from Employee Regs:
      - Automatic refresh
      - Jump to employee
      - Show correct plant
  MRA:8-MAY-2013 TD-22503 Shift Schedule gives copy error
  - When shift schedule is changed (by using copy-function or by doing this
    manual) to 'non-scheduled' (no shift entered), then:
    - Delete any found Employee Planning (ask for confirmation)
    - When deletion of any found employee planning was confirmed,
      or when none was found then:
      - Delete any found Employee Availability (ask for confirmation)
        - When deletion of any found employee availability was confirmed
          or if none was found then:
          - Change the shift schedule itself.
  MRA:25-FEB-2014
  - Final Run System
  - Prevent it is changing/deleting employee availability based on
    last-export-date of final run.
  MRA:23-APR-2018 GLOB3-94
  - Problem when 2 users work in planning dialogs
  - Always allow to use Refresh-button to refresh (not only when it is red).
  - Related to this issue, use bookmark to go to last line in grid after
    doing a copy + commit, instead of going to first line.
  - If in edit-mode, then do NOT refresh when refresh-button is clicked.
*)

unit ShiftScheduleFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Dblup1a, Mask, DBCtrls, dxEditor, dxExEdtr,
  dxEdLib, Buttons, Menus;

const
  ValidShiftChars : set of Char = ['0'..'9','-',#27, #8, #127];
type
  TShiftScheduleF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    pnlTeamPlantShift: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cmbPlusPlant: TComboBoxPlus;
    CheckBoxAllPlant: TCheckBox;
    dxDetailGridColumn2: TdxDBGridColumn;
    GroupBox2: TGroupBox;
    DBEdit31: TDBEdit;
    Label7: TLabel;
    Label3: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    Label8: TLabel;
    Label9: TLabel;
    dxSpinEditWeekFrom: TdxSpinEdit;
    ButtonCopyFromEmp: TButton;
    dxDetailGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    dxDetailGridColumn8: TdxDBGridColumn;
    dxDetailGridColumn9: TdxDBGridColumn;
    dxDetailGridColumn10: TdxDBGridColumn;
    dxDetailGridColumn11: TdxDBGridColumn;
    dxDetailGridColumn12: TdxDBGridColumn;
    dxDetailGridColumn13: TdxDBGridColumn;
    dxDetailGridColumn14: TdxDBGridColumn;
    dxDetailGridColumn15: TdxDBGridColumn;
    dxDetailGridColumn16: TdxDBGridColumn;
    dxDetailGridColumn17: TdxDBGridColumn;
    dxDetailGridColumn18: TdxDBGridColumn;
    dxDetailGridColumn19: TdxDBGridColumn;
    dxDetailGridColumn20: TdxDBGridColumn;
    dxDetailGridColumn21: TdxDBGridColumn;
    dxDetailGridColumn22: TdxDBGridColumn;
    dxDetailGridColumn23: TdxDBGridColumn;
    LabelW11: TLabel;
    LabelW12: TLabel;
    LabelW13: TLabel;
    LabelW14: TLabel;
    LabelW15: TLabel;
    LabelW16: TLabel;
    LabelW17: TLabel;
    GroupBox3: TGroupBox;
    LabelW21: TLabel;
    LabelW22: TLabel;
    LabelW23: TLabel;
    LabelW24: TLabel;
    LabelW25: TLabel;
    LabelW26: TLabel;
    LabelW27: TLabel;
    EditD11: TEdit;
    EditD12: TEdit;
    EditD13: TEdit;
    EditD14: TEdit;
    EditD15: TEdit;
    EditD16: TEdit;
    EditD17: TEdit;
    EditD21: TEdit;
    EditD22: TEdit;
    EditD23: TEdit;
    EditD24: TEdit;
    EditD25: TEdit;
    EditD26: TEdit;
    EditD27: TEdit;
    EditD31: TEdit;
    EditD32: TEdit;
    EditD33: TEdit;
    EditD34: TEdit;
    EditD35: TEdit;
    EditD36: TEdit;
    EditD37: TEdit;
    GroupBox4: TGroupBox;
    LabelW31: TLabel;
    LabelW32: TLabel;
    LabelW33: TLabel;
    LabelW34: TLabel;
    LabelW35: TLabel;
    LabelW36: TLabel;
    LabelW37: TLabel;
    ButtonCopy: TButton;
    BitBtnRefresh: TBitBtn;
    CheckBoxAllTeam: TCheckBox;
    PopupMenuShift: TPopupMenu;
    dxDetailGridColumnSTARTDATE: TdxDBGridColumn;
    dxDetailGridColumnENDDATE: TdxDBGridColumn;
    CheckBoxPlanInOtherPlants: TCheckBox;
    dxDetailGridColumn26: TdxDBGridColumn;
    dxDetailGridColumn27: TdxDBGridColumn;
    dxDetailGridColumn28: TdxDBGridColumn;
    dxDetailGridColumn29: TdxDBGridColumn;
    dxDetailGridColumn30: TdxDBGridColumn;
    dxDetailGridColumn31: TdxDBGridColumn;
    dxDetailGridColumn32: TdxDBGridColumn;
    dxDetailGridColumn33: TdxDBGridColumn;
    dxDetailGridColumn34: TdxDBGridColumn;
    dxDetailGridColumn35: TdxDBGridColumn;
    dxDetailGridColumn36: TdxDBGridColumn;
    dxDetailGridColumn37: TdxDBGridColumn;
    dxDetailGridColumn38: TdxDBGridColumn;
    dxDetailGridColumn39: TdxDBGridColumn;
    dxDetailGridColumn40: TdxDBGridColumn;
    dxDetailGridColumn41: TdxDBGridColumn;
    dxDetailGridColumn42: TdxDBGridColumn;
    dxDetailGridColumn43: TdxDBGridColumn;
    dxDetailGridColumn44: TdxDBGridColumn;
    dxDetailGridColumn45: TdxDBGridColumn;
    dxDetailGridColumn46: TdxDBGridColumn;
    EditP11: TEdit;
    EditP12: TEdit;
    EditP13: TEdit;
    EditP14: TEdit;
    EditP15: TEdit;
    EditP16: TEdit;
    EditP17: TEdit;
    EditP21: TEdit;
    EditP22: TEdit;
    EditP23: TEdit;
    EditP24: TEdit;
    EditP25: TEdit;
    EditP26: TEdit;
    EditP27: TEdit;
    EditP31: TEdit;
    EditP32: TEdit;
    EditP33: TEdit;
    EditP34: TEdit;
    EditP35: TEdit;
    EditP36: TEdit;
    EditP37: TEdit;
    Label12: TLabel;
    cmbPlusTeamFrom: TComboBoxPlus;
    Label14: TLabel;
    cmbPlusTeamTo: TComboBoxPlus;
    lblPeriod: TLabel;
    btnConfirm: TBitBtn;
    btnUndo: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxAllPlantClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure pnlDetailExit(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure EditD11Exit(Sender: TObject);
    procedure dxSpinEditWeekFromChange(Sender: TObject);
    procedure dxSpinEditYearChange(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure ButtonCopyClick(Sender: TObject);
    procedure ButtonCopyFromEmpClick(Sender: TObject);
    procedure cmbPlusPlantChange(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure EditD11ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure EditD11MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EditD11KeyPress(Sender: TObject; var Key: Char);
    procedure dxDetailGridClick(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure CheckBoxPlanInOtherPlantsClick(Sender: TObject);
    procedure cmbPlusTeamFromCloseUp(Sender: TObject);
    procedure cmbPlusTeamToCloseUp(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    procedure btnUndoClick(Sender: TObject);
  private
    { Private declarations }
    FClickGrid: Boolean;
    FEmpl: Integer;
    FEditField: TEdit;
    FEditPlantField: TEdit;
    FMyYear: Integer;
    FMyWeek: Integer;
    FMyPlant: String;
    FMyTeam: String;
    FPlanInOtherPlants: Boolean;
    procedure FillPlants;
    procedure BuildPopupShift(Plant: String; Day_Nr: Integer);
    procedure PopupItemClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure SetMyPlant(const Value: String);
    procedure SetMyWeek(const Value: Integer);
    procedure SetMyYear(const Value: Integer);
    procedure SetMyTeam(const Value: String);
    procedure SetPlanInOtherPlants(const Value: Boolean);
    function FormatPlantShift(PlantCode, ShiftNumber: String): String;
    procedure ShowPeriod;
  public
    { Public declarations }
    procedure FillEditDetail(OnOff: Boolean);
    procedure EnabledNavButtons(Active: Boolean);
    procedure AskForChanges;
    procedure FillCaptionDayOfWeek;
    procedure ChangeLabel(Week: Integer);
    procedure FillArraySHS;
    function CheckShiftNumber(PlantCode, Shift_Number: String;
      Empl, Week, Day: Integer): Boolean;
    procedure RefreshGrid(OnOff: Boolean);
    procedure SetRefreshStatus(OnOff: Boolean);
    procedure GotoCurrentRecord(EmplMax, EmplCurrent: Integer);
    function CheckArraySHS: Boolean;
    property MyPlant: String read FMyPlant write SetMyPlant;
    property MyTeam: String read FMyTeam write SetMyTeam;
    property MyYear: Integer read FMyYear write SetMyYear;
    property MyWeek: Integer read FMyWeek write SetMyWeek;
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write SetPlanInOtherPlants;
  end;

function ShiftScheduleF(APlantCode: String=''; ATeamCode: String='';
  AYear: Word=0; AWeek: Word=0): TShiftScheduleF;

var
  ShiftScheduleF_HDN: TShiftScheduleF;

implementation

{$R *.DFM}

uses
  SystemDMT, ListProcsFRM, UPimsConst, UPimsMessageRes,
  ShiftScheduleDMT, DialogCopySelectionSHSFRM,
  DialogCopyFromSHSFRM ;

// RV075.7.
function ShiftScheduleF(APlantCode: String=''; ATeamCode: String='';
  AYear: Word=0; AWeek: Word=0): TShiftScheduleF;
begin
  if ShiftScheduleF_HDN = nil then
  begin
    ShiftScheduleF_HDN := TShiftScheduleF.Create(Application);
    // RV075.7.
    ShiftScheduleF_HDN.MyYear := AYear;
    ShiftScheduleF_HDN.MyWeek := AWeek;
    ShiftScheduleF_HDN.MyPlant := APlantCode;
    ShiftScheduleF_HDN.MyTeam := ATeamCode;
  end;
  Result := ShiftScheduleF_HDN;
end;

procedure TShiftScheduleF.FormCreate(Sender: TObject);
begin
  ShiftScheduleDM := CreateFormDM(TShiftScheduleDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := ShiftScheduleDM.DataSourceDetail;
  inherited;
  MyPlant := '';
  MyTeam := '';
  MyYear := 0;
  MyWeek := 0;
  CheckBoxPlanInOtherPlants.Checked := False;
  CheckBoxPlanInOtherPlants.Visible :=
    SystemDM.TablePIMSettingsEMPLOYEES_ACROSS_PLANTS_YN.AsString = 'Y';
  PlanInOtherPlants := False;
  ShiftScheduleDM.PlanInOtherPlants := PlanInOtherPlants;
  // RV090.1.
  btnConfirm.Enabled := False;
  btnUndo.Enabled := False;
end;

procedure TShiftScheduleF.FormDestroy(Sender: TObject);
begin
  inherited;
  ShiftScheduleF_HDN := nil;
end;

procedure TShiftScheduleF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // RV090.1.
  if SystemDM.Pims.InTransaction then
  begin
    if DisplayMessage(SPimsConfirmLastTransactionFirst, mtConfirmation,
      [mbYes, mbNo]) = mrYes then
      btnConfirmClick(nil)
    else
      btnUndoClick(nil);
  end;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SUndoChanges, mtInformation, [mbOk]);
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end;
  // RV050.8.
  // Turn filtering off to prevent a problem during OnRecordFilter!
  ShiftScheduleDM.QueryPlant.Filtered := False;
  ShiftScheduleDM.QueryPlant.Active := False;
  Action := caFree;
end;

function TShiftScheduleF.FormatPlantShift(
  PlantCode, ShiftNumber: String): String;
begin
  Result := Format('%s - %s', [PlantCode, ShiftNumber]);
end;

procedure TShiftScheduleF.FillPlants;
begin
  // MR:17-02-2005 Order 550378 Team-From-To.
  if CheckBoxAllTeam.Checked then
    ListProcsF.FillComboBoxPlant(ShiftScheduleDM.QueryPlant,
      True, CmbPlusPlant)
  else
 //Car 26-3-2004
  begin
    if cmbPlusTeamFrom.DisplayValue = cmbPlusTeamTo.DisplayValue then
    begin
      ShiftScheduleDM.QueryPlantTeam.Close;
      ShiftScheduleDM.QueryPlantTeam.ParamByName('TEAM_CODE').Value :=
        GetStrValue(cmbPlusTeamFrom.Value);
      ShiftScheduleDM.QueryPlantTeam.Open;
      if ShiftScheduleDM.QueryPlantTeam.IsEmpty then
        ListProcsF.FillComboBoxPlant(ShiftScheduleDM.QueryPlant,
          True, CmbPlusPlant)
      else
        ListProcsF.FillComboBox(ShiftScheduleDM.QueryPlantTeam,
          CmbPlusPlant, GetStrValue(cmbPlusTeamFrom.Value), '', True,
            'TEAM_CODE', '', 'PLANT_CODE', 'DESCRIPTION');
    end
    else
      ListProcsF.FillComboBoxPlant(ShiftScheduleDM.QueryPlant,
        True, CmbPlusPlant);
  end;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
//  inherited;
 // car 15-9-2003 - user changes
  SetNotEditable;
  cmbPlusTeamFrom.ShowSpeedButton := False;
  cmbPlusTeamFrom.ShowSpeedButton := True;
  cmbPlusTeamTo.ShowSpeedButton := False;
  cmbPlusTeamTo.ShowSpeedButton := True;
  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;
  ListProcsF.WeekUitDat(Now, Year, Week);
  // MR:09-12-2003
  if MyYear <> 0 then
    Year := MyYear;
  if MyWeek <> 0 then
    Week := MyWeek;

  dxSpinEditYear.Value := Year;
  dxSpinEditWeekFrom.Value := Week;
  ChangeLabel(Week);
  // MR:17-02-2005 Order 550378 Team-From-To.
  ListProcsF.FillComboBoxMaster(ShiftScheduleDM.QueryTeam, 'TEAM_CODE',
    True, cmbPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(ShiftScheduleDM.QueryTeam, 'TEAM_CODE',
    False, cmbPlusTeamTo);
  FillPlants;
  // MR:09-12-2003
  if MyPlant <> '' then
    cmbPlusPlant.DisplayValue := MyPlant;
  if MyTeam <> '' then
  begin
    cmbPlusTeamFrom.DisplayValue := MyTeam;
    cmbPlusTeamTo.DisplayValue := MyTeam;
  end;

  dxDetailGrid.SetFocus;
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
  FillCaptionDayOfWeek;
  SetRefreshStatus(False);
  // 550276
  dxSpinEditWeekFrom.MaxValue :=
    ListProcsF.WeeksInYear(Round(dxSpinEditYear.Value)) - 2;
  ShowPeriod;
  // RV092.16. Refresh and jump to employee. (called from Employee-Regs).
  if MyYear <> 0 then
  begin
    try
      BitBtnRefreshClick(Sender);
      ShiftScheduleDM.QueryDetail.Locate('EMPLOYEE_NUMBER',
        SystemDM.ASaveTimeRecScanning.Employee, []);
    except
      // Ignore error
    end;
  end;
end;

procedure TShiftScheduleF.ChangeLabel(Week: Integer);
var
  Index: Integer;
begin
  for Index := 1 to 3 do
    dxDetailGrid.Bands[Index ].Caption := SHeaderWeekAbsRsn + ' ' +
      IntToStr(Round(dxSpinEditWeekFrom.Value) + Index - 1);
  GroupBox2.Caption :=SHeaderWeekAbsRsn + ' ' +
    IntToStr(Round(dxSpinEditWeekFrom.Value));
  GroupBox2.Tag := Round(dxSpinEditWeekFrom.Value);
  GroupBox3.Caption := SHeaderWeekAbsRsn + ' ' +
    IntToStr(Round(dxSpinEditWeekFrom.Value) + 1);
  GroupBox3.Tag := Round(dxSpinEditWeekFrom.Value) + 1;
  GroupBox4.Caption := SHeaderWeekAbsRsn + ' ' +
    IntToStr(Round(dxSpinEditWeekFrom.Value) + 2);
  GroupBox4.Tag := Round(dxSpinEditWeekFrom.Value) + 2;
end;

procedure TShiftScheduleF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
  // MR:17-02-2005 Order 550378 Team-from-to
  cmbPlusTeamFrom.Enabled := not CheckBoxAllTeam.Checked;
  cmbPlusTeamTo.Enabled := not CheckBoxAllTeam.Checked;
  FillPlants;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.CheckBoxAllPlantClick(Sender: TObject);
begin
  inherited;
  cmbPlusPlant.Enabled := not CheckBoxAllPlant.Checked;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.FillEditDetail(OnOff: Boolean);
var
  Nr, Day: Integer;
  EdShift, EdPlant: TComponent;
  SamePlant: Boolean;
begin
  SamePlant := True;
  for Nr := 1 to 3 do
  begin
    for Day := 1 to 7 do
    begin
      // Always show another color for different plants.
      // Even if 'PlanInOtherPlants' is False.
      if True {PlanInOtherPlants} then
      begin
        EdPlant := FindComponent(Format('EditP%d%d', [Nr, Day]));
        if (EdPlant <> nil) then
        begin
          if OnOff then
            (EdPlant as TEdit).Text :=
             ShiftScheduleDM.QueryDetail.FieldByName(
               Format('P%d%d', [Nr, Day])).AsString
          else
            (EdPlant as TEdit).Text := '';
          // If the shift is of another plant than the details-plant,
          // show another color.
          SamePlant := True;
          if (EdPlant as TEdit).Text <> '' then
            SamePlant := ((EdPlant as TEdit).Text =
              ShiftScheduleDM.QueryDetail.FieldByName('PLANT').AsString);
          if not SamePlant then
            (EdPlant as TEdit).Color := clOtherPlant
          else
            (EdPlant as TEdit).Color := clRequired;
        end;
      end;
      EdShift := FindComponent(Format('EditD%d%d', [Nr, Day]));
      if (EdShift <> nil) then
      begin
        if OnOff then
          (EdShift as TEdit).Text :=
             ShiftScheduleDM.QueryDetail.FieldByName(
               Format('D%d%d', [Nr, Day])).AsString
        else
          (EdShift as TEdit).Text := '';
        // If the shift is of another plant than the details-plant,
        // show another color.
        if not SamePlant then
          (EdShift as TEdit).Color := clOtherPlant
        else
          (EdShift as TEdit).Color := clRequired;
      end;
    end;
  end;
end;

procedure TShiftScheduleF.FillCaptionDayOfWeek;
var
  TempComponent: TComponent;
  Counter, Index, WeekNr, DayNr: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TdxDBGridColumn) then
    begin
      Index := (TempComponent as TdxDBGridColumn).Tag;
      if Index <> 0 then
      begin
        WeekNr := Index div 10;
        DayNr := Index - (WeekNr * 10);
        (TempComponent as TdxDBGridColumn).Caption :=
          SystemDM.GetDayWCode(DayNr);
      end;
    end;
    if (TempComponent is TLabel) then
    begin
      Index := (TempComponent as TLabel).Tag;
      if Index <> 0 then
      begin
        WeekNr := Index div 10;
        DayNr := Index - (WeekNr * 10);
        (TempComponent as TLabel).Caption := SystemDM.GetDayWCode(DayNr);
      end;
    end;
  end;
end;

procedure TShiftScheduleF.FillArraySHS;
var
  Nr, Day: Integer;
  EdShift, EdPlant: TComponent;
begin
  for Nr := 1 to 3 do
  begin
    for Day := 1 to 7 do
    begin
      EdShift := FindComponent(Format('EditD%d%d', [Nr, Day]));
      if (EdShift <> nil) then
        ShiftScheduleDM.FSHSArray[Nr, Day] :=
         (EdShift as TEdit).Text;
      // Always fill the plant-array, even if PlanInOtherPlants is False.
      if True {PlanInOtherPlants} then
      begin
        EdPlant := FindComponent(Format('EditP%d%d', [Nr, Day]));
        if (EdPlant <> nil) then
          ShiftScheduleDM.FPlantSHSArray[Nr, Day] :=
           (EdPlant as TEdit).Text;
      end;
    end;
  end;
end;

procedure TShiftScheduleF.dxBarBDBNavPostClick(Sender: TObject);
var
  EmplMax: Integer;
begin
  inherited;
  if not FClickGrid then
    FillArraySHS;
  ShiftScheduleDM.FillOldArraySHS;
  if CheckArraySHS then
  begin
    ShiftScheduleDM.UpdateShiftSchedule( FEmpl, Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value));
    ShiftScheduleDM.FEmployee :=
      ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    EmplMax := dxDetailGrid.Items[dxDetailGrid.Count-1].Values[0];
    GotoCurrentRecord(EmplMax, ShiftScheduleDM.FEmployee);
    ShiftScheduleDM.FEmployee := 0;
  end;
end;

procedure TShiftScheduleF.GotoCurrentRecord(EmplMax, EmplCurrent: Integer);
var
  Count :Integer;
begin
  RefreshGrid(True);
  if dxBarButtonSort.down then
  begin
    ShiftScheduleDM.GotoRecord(EmplCurrent);
    Exit;
  end;
  ShiftScheduleDM.GotoRecord(EmplMax);
  Count := dxDetailGrid.Count-1 ;
  while (Count >= 0) do
  begin
    if (dxDetailGrid.Items[Count].Values[0] = EmplCurrent) then
       break;
    dxDetailGrid.GotoPrev(True);
    dec(Count);
  end
end;
procedure TShiftScheduleF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
  ButtonCopyFromEmp.Enabled := not Active;
  ButtonCopy.Enabled := not Active;
end;

procedure TShiftScheduleF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  FillEditDetail(True);
  EnabledNavButtons(False);
  if dxDetailGrid.CanFocus then
    dxDetailGrid.SetFocus;
end;

function TShiftScheduleF.CheckArraySHS: Boolean;
var
  Week, Day: Integer;
begin
  Result := True;
  if not dxBarBDBNavPost.Enabled then
    Exit;

  for Week := 1 to 3 do
    for Day := 1 to 7 do
      if not CheckShiftNumber(
        ShiftScheduleDM.FPlantSHSArray[Week, Day],
        ShiftScheduleDM.FSHSArray[Week, Day], FEmpl,
        Week, Day) then
      begin
        Result := False;
        Exit;
      end;
end;

procedure TShiftScheduleF.pnlDetailExit(Sender: TObject);
begin
  inherited;
  FillArraySHS;
end;

procedure TShiftScheduleF.AskForChanges;
var
  QuestionResult: Integer;
begin
  QuestionResult := 0;
  FClickGrid := True;
  if dxBarBDBNavPost.Enabled then
     QuestionResult := DisplayMessage(SPimsSaveChanges, mtConfirmation,
       [mbYes, mbNo]);
  case QuestionResult of
    mrYes: dxBarBDBNavPost.OnClick(dxBarDBNavigator);
    mrNo: dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end; { case }
  if dxBarBDBNavPost.Enabled then
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  FClickGrid := False;
end;

procedure TShiftScheduleF.dxGridEnter(Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

function TShiftScheduleF.CheckShiftNumber(PlantCode,
  Shift_Number: String;
  Empl, Week, Day: Integer): Boolean;
var
  OldShiftNr: String;
  OldPlantCode: String;
  SamePlant: Boolean;
  PlannedDeleted: Boolean;
  NewPlantCode: String;
  NewShiftNumber: Integer; // TD-22503
begin
  Result := True;
  PlannedDeleted := False;

  // TD-22503
  if (Shift_Number = '-') or (Shift_Number = '') then
    NewShiftNumber := -1
  else
    NewShiftNumber := StrToInt(Shift_Number);

  if not PlanInOtherPlants then
    PlantCode := ShiftScheduleDM.QueryDetail.FieldByName('PLANT').AsString;
  NewPlantCode := PlantCode;
  if (Shift_Number <> '-') and (Shift_Number <> '') then
    if not ShiftScheduleDM.ValidShift(PlantCode,
      StrToInt(Shift_Number), Day, NewPlantCode) then
    begin
      DisplayMessage(SInvalidShiftSchedule, mtInformation, [mbOk]);
      Result := False;
      Exit;
    end;
  // The shift is of another plant, so change the array-item!
  // This can happen if the shift has been entered by keyboard.
  if NewPlantCode <> PlantCode then
    ShiftScheduleDM.FPlantSHSArray[Week, Day] := NewPlantCode;
  OldShiftNr := ShiftScheduleDM.FOldSHSArray[Week, Day];
  OldPlantCode := ShiftScheduleDM.FOldPlantSHSArray[Week, Day];
  SamePlant := True;
  if PlanInOtherPlants then
    SamePlant := (OldPlantCode = ShiftScheduleDM.FPlantSHSArray[Week, Day]);
  //The old shift number or plantcode is changed
  if (OldShiftNr <> '') and (OldShiftNr <> '-') then
    if (OldShiftNr <> ShiftScheduleDM.FSHSArray[Week, Day]) or
      (not SamePlant) then
      if ShiftScheduleDM.ValidEmplAvail(NewShiftNumber, OldPlantCode, Empl,
        StrToInt(OldShiftNr),
        Round(dxSpinEditYear.Value), Round(dxSpinEditWeekFrom.Value) +
        Week - 1, Day, PlannedDeleted) then
      begin
        // TD-22503 When 'NewShiftNumber <> -1' it means the
        //          shift schedule was changed to a different shift, but
        //          there was still availability.
//          if not PlannedDeleted then
        if (NewShiftNumber <> -1) then // TD-22503
        begin
          DisplayMessage(SEmployeeAvail_0 + IntToStr(Empl) + SEmployeeAvail_1 +
            IntToStr(Round(dxSpinEditYear.Value)) + ', ' +
            IntToStr(Week + Round(dxSpinEditWeekFrom.Value) - 1) + ', ' +
            SystemDM.GetDayWCode(Day) + SEmployeeAvail_3,
            mtInformation, [mbOk]);
          Result := False;
        end
        else
          Result := True;
      end;
end;

procedure TShiftScheduleF.EditD11Exit(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;
  FillArraySHS;
end;

procedure TShiftScheduleF.dxSpinEditWeekFromChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ChangeLabel(Round(dxSpinEditWeekFrom.Value));
  SetRefreshStatus(False);
  ShowPeriod;
end;

procedure TShiftScheduleF.dxSpinEditYearChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  SetRefreshStatus(False);
  // 550276
  dxSpinEditWeekFrom.MaxValue :=
    ListProcsF.WeeksInYear(Round(dxSpinEditYear.Value)) - 2;
  ShowPeriod;
end;

procedure TShiftScheduleF.FormHide(Sender: TObject);
begin
   EnabledNavButtons(False);
   inherited;
end;

procedure TShiftScheduleF.FormDeactivate(Sender: TObject);
begin
  EnabledNavButtons(False);
  inherited;
end;

procedure TShiftScheduleF.ButtonCopyClick(Sender: TObject);
var
  SaveEmp, EmplMax: Integer;
begin
  inherited;
  // RV090.1.
  if SystemDM.Pims.InTransaction then
  begin
    DisplayMessage(SPimsFinishLastTransactionFirst, mtInformation, [mbOK]);
    Exit;
  end;
//  if (BitBtnRefresh.Font.Color = clRed)  then
  if not ShiftScheduleDM.QueryDetail.Active then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbOK]);
    Exit;
  end;
  if(ShiftScheduleDM.QueryDetail.RecordCount = 0){ or
    (BitBtnRefresh.Font.Color = clRed) } then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbOK]);
    Exit;
  end;
  SaveEmp :=
    ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  EmplMax := dxDetailGrid.Items[dxDetailGrid.Count-1].Values[0];
  DialogCopySelectionSHSF := TDialogCopySelectionSHSF.Create(Application);
//  if SystemDM.EmbedDialogs then
//    DialogCopySelectionSHSF.FormStyle := fsStayOnTop;
  DialogCopySelectionSHSF.FStartWeek := Round(dxSpinEditWeekFrom.Value);
  DialogCopySelectionSHSF.FYear := Round(dxSpinEditYear.Value);
  try
    ShiftScheduleDM.FillQueryEmpl(CheckBoxAllTeam.Checked,
      CheckBoxAllPlant.Checked,
      GetStrValue(cmbPlusTeamFrom.Value),
      GetStrValue(cmbPlusTeamTo.Value),
      GetStrValue(cmbPlusPlant.Value), True, True);
    DialogCopySelectionSHSF.ShowModal;
  finally
    if DialogCopySelectionSHSF.FCopy then
      GotoCurrentRecord(EmplMax, SaveEmp);
    DialogCopySelectionSHSF.Free;
    // RV090.1. Handling of buttons for Confirm/Undo
    if SystemDM.Pims.InTransaction then
    begin
      btnConfirm.Enabled := True;
      btnUndo.Enabled := True;
    end;
  end;
end;

procedure TShiftScheduleF.ButtonCopyFromEmpClick(Sender: TObject);
var
  EmplMax: Integer;
begin
  inherited;
  // RV090.1.
  if SystemDM.Pims.InTransaction then
  begin
    DisplayMessage(SPimsFinishLastTransactionFirst, mtInformation, [mbOK]);
    Exit;
  end;
{  if (BitBtnRefresh.Font.Color = clRed) then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbOK]);
    Exit;
  end; }
  if not ShiftScheduleDM.QueryDetail.Active then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbOK]);
    Exit;
  end;
  if (ShiftScheduleDM.QueryDetail.RecordCount = 0) then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbOK]);
    Exit;
  end;
  DialogCopyFromSHSF := TDialogCopyFromSHSF.Create(Application);
//  if SystemDM.EmbedDialogs then
//    DialogCopyFromSHSF.FormStyle := fsStayOnTop;
  DialogCopyFromSHSF.FEmp :=
   ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  // MR:23-07-2004 Order 550323
  // Don't copy to days the employee is not-active
  DialogCopyFromSHSF.FStartDate :=
    ShiftScheduleDM.QueryDetail.FieldByName('STARTDATE').AsDateTime;
  DialogCopyFromSHSF.FEndDate :=
    ShiftScheduleDM.QueryDetail.FieldByName('ENDDATE').AsDateTime;

  EmplMax := dxDetailGrid.Items[dxDetailGrid.Count-1].Values[0];

  DialogCopyFromSHSF.FStartWeek := Round(dxSpinEditWeekFrom.Value);
  DialogCopyFromSHSF.FYear := Round(dxSpinEditYear.Value);
  try
    ShiftScheduleDM.FillQueryEmpl(CheckBoxAllTeam.Checked,
      CheckBoxAllPlant.Checked,
      GetStrValue(cmbPlusTeamFrom.Value),
      GetStrValue(cmbPlusTeamTo.Value),
      GetStrValue(cmbPlusPlant.Value), False, False);
    DialogCopyFromSHSF.ShowModal;
  finally
    if DialogCopyFromSHSF.FCopy then
    begin
      ShiftScheduleDM.FEmployee := DialogCopyFromSHSF.FEmp;
      GotoCurrentRecord(EmplMax, ShiftScheduleDM.FEmployee);
      ShiftScheduleDM.FEmployee := 0;
    end;
    DialogCopyFromSHSF.Free;
    // RV090.1. Handling of buttons for Confirm/Undo
    if SystemDM.Pims.InTransaction then
    begin
      btnConfirm.Enabled := True;
      btnUndo.Enabled := True;
    end;
  end;
end;

procedure TShiftScheduleF.RefreshGrid(OnOff: Boolean);
var
  Bookmark: TBookmark;
begin
  BookMark := nil;
  if ShiftScheduleDM.QueryDetail.Active and
    (not ShiftScheduleDM.QueryDetail.Eof) then
    BookMark := ShiftScheduleDM.QueryDetail.GetBookmark;
  if Not OnOff then
  begin
    ShiftScheduleDM.QueryDetail.Active := False;
    Exit;
  end;
  if ShiftScheduleDM <> Nil then
  begin
    ShiftScheduleDM.FillQueryDetail(CheckBoxAllTeam.Checked,
       CheckBoxAllPlant.Checked,
       GetStrValue(cmbPlusTeamFrom.Value),
       GetStrValue(cmbPlusTeamTo.Value),
       GetStrValue(cmbPlusPlant.Value),
       Round(dxSpinEditYear.Value), Round(dxSpinEditWeekFrom.Value));

    EnabledNavButtons(False);
    FillEditDetail(True);
  end;
  if Assigned(BookMark) then
  begin
    ShiftScheduleDM.QueryDetail.GotoBookmark(Bookmark);
    ShiftScheduleDM.QueryDetail.FreeBookmark(Bookmark);
    FillEditDetail(True);
  end;
end;

procedure TShiftScheduleF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.BitBtnRefreshClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then // GLOB3-94
    Exit;

  // GLOB3-94 Always allow to refresh
//  if (BitBtnRefresh.Font.Color = clRed) then
  begin
    ShiftScheduleF.FSort := True;
    SetRefreshStatus(True);
    ShiftScheduleF.FSort := False;
  end;
end;

procedure TShiftScheduleF.SetRefreshStatus(OnOff: Boolean);
begin
  if OnOff then
  begin
    BitBtnRefresh.Font.Color := clWindowText;
    RefreshGrid(True);
  end
  else
  begin
//    BitBtnRefresh.Font.Color := clRed;
    RefreshGrid(False);
    FillEditDetail(False);
  end;

end;

procedure TShiftScheduleF.BuildPopupShift(Plant: String; Day_Nr: Integer);
var
  i: Integer;
  NewItem: TMenuItem;
begin
  for i := 0 to (PopupMenuShift.Items.Count - 1) do
    PopupMenuShift.Items.Clear;

  with ShiftScheduleDM.ClientDataSetShift do
  begin
    Filtered := False;
    if not PlanInOtherPlants then
    begin
      Filter := 'PLANT_CODE = ' + Plant;
      Filtered := True;
    end;
    First;
    while not Eof do
    begin
      if (FieldByName('STARTTIME' + IntToStr(Day_Nr)).AsDateTime <> 0) or
        (FieldByName('ENDTIME' + IntToStr(Day_Nr)).AsDateTime <> 0) then
      begin
        NewItem := TMenuItem.Create(Self);
        if PlanInOtherPlants then
          NewItem.Caption :=
            FormatPlantShift(FieldByName('PLANT_CODE').AsString,
            IntToStr(FieldByName('SHIFT_NUMBER').AsInteger))
        else
          NewItem.Caption := IntToStr(FieldByName('SHIFT_NUMBER').AsInteger);
        PopupMenuShift.Items.Add(NewItem);
        NewItem.OnClick := PopupItemClick;
      end;
      Next;
    end;
    Filtered := False;
    NewItem := TMenuItem.Create(Self);
    NewItem.Caption := '_';
    PopupMenuShift.Items.Add(NewItem);
    NewItem.OnClick := PopupItemClick;
  end;
end;

procedure TShiftScheduleF.PopupItemClick(Sender: TObject);
var
  Absence: String;
  PlantCode: String;
  function FilterPlantShift(AField: String; var APlantCode: String): String;
  var
    I: Integer;
  begin
    Result := AField;
    APlantCode := '';
    if PlanInOtherPlants then
      if AField <> '' then
      begin
        I := Pos('-', AField);
        try
          if I > 0 then
          begin
            APlantCode := Trim(Copy(AField, 1, I - 1));
            Result := Trim(Copy(AField, I + 1, Length(AField)));
          end;
        except
          Result := '';
          APlantCode := '';
        end;
      end;
  end;
begin
  inherited;
  Absence := FilterPlantShift((Sender as TMenuItem).Caption, PlantCode);
  if Absence = '_' then
    Absence := '-';
  FEditField.Text := Absence;
  if PlanInOtherPlants then
    FEditPlantField.Text := PlantCode;
  EnabledNavButtons(True);
  FillArraySHS;
  ShiftScheduleDM.FillOldArraySHS;
  FEmpl := ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TShiftScheduleF.EditD11ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
  Day: Integer;
  MenuLine: String;
  MenuItem: TMenuItem;
begin
  inherited;
  if (ShiftScheduleDM.QueryDetail.FieldByName('PLANT').AsString <> '') then
  begin
    Day := (Sender as TEdit).Tag;
    Day := Day - ((Day div 10) * 10);
    BuildPopupShift(
      ShiftScheduleDM.QueryDetail.FieldByName('PLANT').AsString, Day);
    if PlanInOtherPlants then
    begin
      MenuLine := FormatPlantShift(FEditPlantField.Text, FEditField.Text);
      MenuItem := PopupMenuShift.Items.Find(MenuLine);
      if MenuItem <> nil then
      begin
        MenuItem.Checked := True;
      end;
    end;
  end;
end;

procedure TShiftScheduleF.EditD11MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  (Sender as TEdit).SelectAll;
  if PlanInOtherPlants then
    FEditPlantField := (FindComponent('EditP' +
      IntToStr((Sender as TEdit).Tag)) as TEdit);
  FEditField := (Sender as TEdit);
end;

procedure TShiftScheduleF.EditD11KeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EnabledNavButtons(True);

  ShiftScheduleDM.FillOldArraySHS;
  FEmpl := ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if not (Key in ValidShiftChars) then
  begin
    key := #0;
    Exit;
  end      
end;

procedure TShiftScheduleF.dxDetailGridClick(Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

procedure TShiftScheduleF.dxBarButtonExportGridClick(Sender: TObject);
begin
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;
end;

procedure TShiftScheduleF.EditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  EnabledNavButtons(True);
  FEmpl := ShiftScheduleDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

// MR:05-12-2003
procedure TShiftScheduleF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    // NOTE: Use '-2' because the first 3 weeks are shown in grid, where
    //       Otherwise if week is '53', also weeks '54' and '55' are shown.
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks - 2;
    if dxSpinEditWeekFrom.Value > (MaxWeeks - 2) then
      dxSpinEditWeekFrom.Value := MaxWeeks - 2;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;
end;

procedure TShiftScheduleF.SetMyPlant(const Value: String);
begin
  FMyPlant := Value;
end;

procedure TShiftScheduleF.SetMyWeek(const Value: Integer);
begin
  FMyWeek := Value;
end;

procedure TShiftScheduleF.SetMyYear(const Value: Integer);
begin
  FMyYear := Value;
end;

procedure TShiftScheduleF.SetMyTeam(const Value: String);
begin
  FMyTeam := Value;
end;

// MR:16-07-2004: Order 550232
// Set Week/Day Edit-boxes to enabled or not, depending on
// availability of Employee (by checking StartDate and EndDate).
// NOTE: For dxDetailGridColumns StartDate and Enddate, the
// Disable...-properties are set to False, so they cannot be
// seen/selected by user, and will not appear visible in grid.
procedure TShiftScheduleF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  StartDate, EndDate: TDateTime;
  Year, Week: Integer;
  FinalRunExportDate: TDateTime; // 20011800
  procedure EditEnable(Year, Week: Integer);
  var
    Ed: TComponent;
    Nr, Day: Integer;
    DayDate: TDateTime;
  begin
    for Nr := 1 to 3 do
    begin
      for Day := 1 to 7 do
      begin
        DayDate := ListProcsF.DateFromWeek(Year, Week, Day);
        Ed := FindComponent(Format('EditD%d%d', [Nr, Day]));
        if (Ed <> nil) then
        begin
          (Ed as TEdit).Enabled := (DayDate >= StartDate) and
            ((DayDate <= EndDate) or (EndDate = 0));

          // 20011800
          if (Ed as TEdit).Enabled then
            if SystemDM.UseFinalRun then
            begin
              FinalRunExportDate :=
                SystemDM.FinalRunExportDateByPlant(
                  dxDetailGrid.DataSource.DataSet.FieldByName('PLANT').AsString
                  );
              if FinalRunExportDate <> NullDate then
                if DayDate <= FinalRunExportDate then
                  (Ed as TEdit).Enabled := False;
            end;
          (Ed as TEdit).Visible := (Ed as TEdit).Enabled;
        end;
      end;
      inc(Week); // next week
    end;
  end;
begin
  inherited;
  if AFocused or ASelected then
    if AColumn = dxDetailGridColumn1 then
    begin
      StartDate := dxDetailGridColumnSTARTDATE.Field.AsDateTime;
      EndDate := dxDetailGridColumnENDDATE.Field.AsDateTime;
      Year := dxSpinEditYear.IntValue;
      Week := GroupBox2.Tag; // Week 1
      EditEnable(Year, Week);
    end;
end;

procedure TShiftScheduleF.CheckBoxPlanInOtherPlantsClick(Sender: TObject);
begin
  inherited;
  SetRefreshStatus(False);
  PlanInOtherPlants := CheckBoxPlanInOtherPlants.Checked;
  ShiftScheduleDM.PlanInOtherPlants := CheckBoxPlanInOtherPlants.Checked;
end;

procedure TShiftScheduleF.SetPlanInOtherPlants(const Value: Boolean);
begin
  FPlanInOtherPlants := Value;
end;

procedure TShiftScheduleF.cmbPlusTeamFromCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamTo.DisplayValue := cmbPlusTeamFrom.DisplayValue;
  FillPlants;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.cmbPlusTeamToCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamFrom.DisplayValue := cmbPlusTeamTo.DisplayValue;
  FillPlants;
  SetRefreshStatus(False);
end;

procedure TShiftScheduleF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

// RV090.1.
procedure TShiftScheduleF.btnConfirmClick(Sender: TObject);
  function ConfirmResult: Boolean;
  begin
    if not Assigned(Sender) then // Called during Close?
      Result := True
    else
      Result := (DisplayMessage(SPimsConfirmLastTransaction, mtConfirmation,
        [mbYes, mbNo]) = mrYes);
  end;
begin
  inherited;
  try
    if SystemDM.Pims.InTransaction then
      if ConfirmResult then
      begin
        SystemDM.Pims.Commit;
        btnConfirm.Enabled := False;
        btnUndo.Enabled := False;
        RefreshGrid(True);
      end;
  finally
    //
  end;
end;

// RV090.1.
procedure TShiftScheduleF.btnUndoClick(Sender: TObject);
  function ConfirmResult: Boolean;
  begin
    if not Assigned(Sender) then // Called during Close?
      Result := True
    else
      Result := (DisplayMessage(SPimsUndoLastTransaction, mtConfirmation,
        [mbYes, mbNo]) = mrYes);
  end;
begin
  inherited;
  try
    if SystemDM.Pims.InTransaction then
      if ConfirmResult then
      begin
        SystemDM.Pims.Rollback;
        btnConfirm.Enabled := False;
        btnUndo.Enabled := False;
        RefreshGrid(True);
      end;
  finally
    //
  end;
end;

end.
