inherited PTODefDM: TPTODefDM
  OldCreateOrder = True
  Left = 407
  Top = 276
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnCalcFields = TableMasterCalcFields
    TableName = 'CONTRACTGROUP'
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 24
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
    object TableMasterTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Required = True
      Size = 1
    end
    object TableMasterBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Required = True
      Size = 1
    end
    object TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableMasterWEEKS_IN_PERIOD: TIntegerField
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableMasterWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Required = True
      Size = 1
    end
    object TableMasterHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableMasterMAX_PTO_MINUTE: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'MAX_PTO_MINUTE'
    end
    object TableMasterMAX_PTO_MINUTE_CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'MAX_PTO_MINUTE_CALC'
      Size = 24
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    OnCalcFields = TableDetailCalcFields
    IndexFieldNames = 'CONTRACTGROUP_CODE'
    MasterFields = 'CONTRACTGROUP_CODE'
    TableName = 'PTODEFINITION'
    Top = 144
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 24
    end
    object TableDetailLINE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'LINE_NUMBER'
      Required = True
    end
    object TableDetailFROMEMPYEARS: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'FROMEMPYEARS'
      Required = True
    end
    object TableDetailTILLEMPYEARS: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'TILLEMPYEARS'
      Required = True
    end
    object TableDetailPTOMINUTE: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PTOMINUTE'
      Required = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      Size = 80
    end
    object TableDetailPTOHOURMINCALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'PTOHOURMINCALC'
      Size = 25
      Calculated = True
    end
  end
end
