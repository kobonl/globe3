object DialogMonthlyEmployeeEfficiencyDM: TDialogMonthlyEmployeeEfficiencyDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 191
  Top = 107
  Height = 491
  Width = 830
  object qryEmployeeEfficiencyInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEEFFICIENCY'
      '(YEAR_NUMBER,'
      'MONTH_NUMBER,'
      'EMPLOYEE_NUMBER,'
      'PLANT_CODE,'
      'WORKSPOT_CODE,'
      'JOB_CODE,'
      'EFFICIENCY,'
      'CREATIONDATE,'
      'MUTATIONDATE,'
      'MUTATOR)'
      'VALUES'
      '(:YEAR_NUMBER,'
      ':MONTH_NUMBER,'
      ':EMPLOYEE_NUMBER,'
      ':PLANT_CODE,'
      ':WORKSPOT_CODE,'
      ':JOB_CODE,'
      ':EFFICIENCY,'
      ':CREATIONDATE,'
      ':MUTATIONDATE,'
      ':MUTATOR)'
      ''
      ' '
      ' ')
    Left = 80
    Top = 20
    ParamData = <
      item
        DataType = ftInteger
        Name = 'YEAR_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MONTH_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'EFFICIENCY'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeeEfficiencyDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEEFFICIENCY'
      'WHERE YEAR_NUMBER = :YEAR_NUMBER AND'
      'MONTH_NUMBER = :MONTH_NUMBER AND'
      'PLANT_CODE >= :PLANTFROM AND'
      'PLANT_CODE <= :PLANTTO AND'
      'WORKSPOT_CODE >= :WORKSPOTFROM AND'
      'WORKSPOT_CODE <= :WORKSPOTTO')
    Left = 80
    Top = 76
    ParamData = <
      item
        DataType = ftInteger
        Name = 'YEAR_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MONTH_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end>
  end
  object qryJob: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  J.PLANT_CODE,'
      '  J.WORKSPOT_CODE,'
      '  J.JOB_CODE,'
      '  J.NORM_PROD_LEVEL'
      'FROM'
      '  JOBCODE J'
      'ORDER BY'
      '  J.PLANT_CODE,'
      '  J.WORKSPOT_CODE,'
      '  J.JOB_CODE'
      '   ')
    Left = 80
    Top = 136
  end
  object dspJob: TDataSetProvider
    DataSet = qryJob
    Constraints = True
    Left = 160
    Top = 136
  end
  object cdsJob: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspJob'
    Left = 240
    Top = 136
  end
end
