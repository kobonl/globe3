inherited DialogReportEmplF: TDialogReportEmplF
  Left = 250
  Caption = 'Report employees'
  ClientHeight = 416
  ClientWidth = 590
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 590
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 294
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 590
    Height = 295
    TabOrder = 3
    inherited LblFromEmployee: TLabel
      Top = 94
    end
    inherited LblEmployee: TLabel
      Top = 94
    end
    inherited LblToEmployee: TLabel
      Top = 94
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 95
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 95
    end
    object Label1: TLabel [8]
      Left = 8
      Top = 164
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [9]
      Left = 8
      Top = 188
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [10]
      Left = 40
      Top = 164
      Width = 56
      Height = 13
      Caption = 'Date Active'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [11]
      Left = 40
      Top = 188
      Width = 65
      Height = 13
      Caption = 'Date Inactive'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel [12]
      Left = 315
      Top = 166
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [13]
      Left = 315
      Top = 190
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [14]
      Left = 128
      Top = 174
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel [15]
      Left = 344
      Top = 174
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel [16]
      Left = 128
      Top = 198
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel [17]
      Left = 344
      Top = 198
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 47
    end
    object Label21: TLabel [21]
      Left = 344
      Top = 46
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [22]
      Left = 128
      Top = 47
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object LblFromContractEndDate: TLabel [26]
      Left = 8
      Top = 120
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblContractEndDate: TLabel [27]
      Left = 40
      Top = 120
      Width = 74
      Height = 13
      Caption = 'Contr. enddate'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToContractEndDate: TLabel [28]
      Left = 315
      Top = 120
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel [29]
      Left = 128
      Top = 383
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [30]
      Left = 128
      Top = 122
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel [31]
      Left = 344
      Top = 382
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [32]
      Left = 344
      Top = 121
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblToTeam: TLabel
      Top = 69
    end
    inherited LblTeam: TLabel
      Top = 67
    end
    inherited LblFromTeam: TLabel
      Top = 67
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 109
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 110
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 66
      ColCount = 130
      TabOrder = 5
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 66
      ColCount = 131
      TabOrder = 6
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 528
      TabOrder = 7
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 132
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 133
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 528
      Top = 42
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 92
      TabOrder = 8
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 92
      TabOrder = 9
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 135
      TabOrder = 27
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 25
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 26
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 24
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 28
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 29
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 146
      TabOrder = 32
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 147
      TabOrder = 21
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 147
      TabOrder = 22
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 148
      TabOrder = 23
    end
    inherited EditWorkspots: TEdit
      TabOrder = 31
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 153
      TabOrder = 34
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 152
      TabOrder = 33
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 36
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 38
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 35
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 37
    end
    object DateActiveFrom: TDateTimePicker
      Left = 120
      Top = 164
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
    end
    object DateInactiveFrom: TDateTimePicker
      Left = 120
      Top = 188
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 16
    end
    object DateActiveTo: TDateTimePicker
      Left = 342
      Top = 164
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 14
    end
    object DateInactiveTo: TDateTimePicker
      Left = 342
      Top = 188
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 17
    end
    object CheckBoxAllDateActive: TCheckBox
      Left = 528
      Top = 166
      Width = 41
      Height = 17
      Caption = 'All '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 15
      OnClick = CheckBoxAllDateActiveClick
    end
    object CheckBoxAllDateInactive: TCheckBox
      Left = 528
      Top = 188
      Width = 41
      Height = 17
      Caption = 'All '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 18
      OnClick = CheckBoxAllDateInactiveClick
    end
    object DateContractEndDateFrom: TDateTimePicker
      Left = 120
      Top = 118
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 10
    end
    object DateContractEndDateTo: TDateTimePicker
      Left = 342
      Top = 118
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 11
    end
    object CheckBoxAllContractEndDate: TCheckBox
      Left = 528
      Top = 120
      Width = 57
      Height = 17
      Caption = 'All '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 12
      OnClick = CheckBoxAllDateActiveClick
    end
    object CheckBoxAllDepartment: TCheckBox
      Left = 528
      Top = 42
      Width = 65
      Height = 17
      Caption = 'All '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 20
      Visible = False
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 528
      Top = 382
      Width = 57
      Height = 17
      Caption = 'All '
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 30
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object Panel1: TPanel
      Left = 0
      Top = 213
      Width = 590
      Height = 82
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 19
      object GroupBox1: TGroupBox
        Left = 376
        Top = 0
        Width = 214
        Height = 82
        Align = alRight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object CBoxShowSelection: TCheckBox
          Left = 10
          Top = 15
          Width = 97
          Height = 17
          Caption = 'Show selection'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object CheckBoxExport: TCheckBox
          Left = 10
          Top = 41
          Width = 97
          Height = 17
          Caption = 'Export'
          TabOrder = 1
        end
      end
      object RadioGroupSort: TRadioGroup
        Left = 0
        Top = 0
        Width = 177
        Height = 82
        Align = alLeft
        Caption = 'Sort on'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          'Name'
          'Short name'
          'Number')
        ParentFont = False
        TabOrder = 0
      end
      object gboxShowContractType: TGroupBox
        Left = 177
        Top = 0
        Width = 199
        Height = 82
        Align = alClient
        Caption = 'Show Contract Type'
        TabOrder = 1
        object cBoxPermanent: TCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Permanent'
          Checked = True
          State = cbChecked
          TabOrder = 0
        end
        object cBoxTemporary: TCheckBox
          Left = 8
          Top = 39
          Width = 97
          Height = 17
          Caption = 'Temporary'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object cBoxExternal: TCheckBox
          Left = 8
          Top = 62
          Width = 97
          Height = 17
          Caption = 'External'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 356
    Width = 590
  end
  inherited pnlBottom: TPanel
    Top = 375
    Width = 590
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 552
    Top = 72
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited QueryEmplFrom: TQuery
    Left = 184
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        44040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507244469616C6F675265706F7274456D706C462E4461746153
        6F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A650B0E
        6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67106564
        676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E
        6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F43
        616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F4361
        6E496E73657274116564676F43616E4E617669676174696F6E116564676F436F
        6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264731065
        64676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D
        6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265720653
        6F7274656407046373557005576964746802410942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E
        554D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E5368
        6F72744E616D650743617074696F6E060A53686F7274206E616D650557696474
        6802540942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060A53484F52545F4E414D4500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557696474
        6803B4000942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741646472
        65737305576964746802450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506074144445245535300000F5464784442477269
        64436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F
        4465706172746D656E7420636F646505576964746802580942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F444550415254
        4D454E545F434F444500000F546478444247726964436F6C756D6E0A436F6C75
        6D6E5465616D0743617074696F6E06095465616D20636F64650942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507224469616C6F675265706F72
        74456D706C462E44617461536F75726365456D706C546F104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802310942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D65055769647468024E0942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07436170
        74696F6E06044E616D6505576964746803CC000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E06074164647265737305576964746802650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        5245535300000F546478444247726964436F6C756D6E0E436F6C756D6E446570
        74436F64650743617074696F6E060F4465706172746D656E7420636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
  end
end
