unit IgnoreInterfaceCodeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEdLib, dxDBELib, dxEditor, dxExEdtr, DBCtrls,
  StdCtrls, Mask, dxDBTLCl, dxGrClms, dxDBEdtr;

type
  TIgnoreInterfaceCodeF = class(TGridBaseF)
    GroupBox2: TGroupBox;
    Label6: TLabel;
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridLookupColumn;
    dxMasterGridColumn4: TdxDBGridLookupColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn5: TdxDBGridCheckColumn;
    dxMasterGridColumn6: TdxDBGridCheckColumn;
    dxMasterGridColumn7: TdxDBGridCheckColumn;
    dxMasterGridColumn8: TdxDBGridDateColumn;
    dxMasterGridColumn9: TdxDBGridCheckColumn;
    dxMasterGridColumn10: TdxDBGridCheckColumn;
    dxMasterGridColumn11: TdxDBGridCheckColumn;
    dxMasterGridColumn12: TdxDBGridLookupColumn;
    ComboBoxInterface: TComboBox;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure ComboBoxInterfaceClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxDetailGridClick(Sender: TObject);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    FListInterface: TStringList;
    FInterfaceDefined, FJob, FInterfaceCode: String;
    FInsertMode: Boolean;
//    procedure  GetStrings(SelString: String; var Job, InterfaceCode: String);
    procedure  GetStrings( SelString: String;
      var Plant, PlantDesc, WK, WKDesc, Job, JobDesc, InterfaceCode: String);
    procedure SetValuesBasedOnCombo;
  end;

function IgnoreInterfaceCodeF: TIgnoreInterfaceCodeF;

implementation

{$R *.DFM}

uses
   SystemDMT, WorkSpotDMT, UPimsMessageRes, UPimsConst;

var
  IgnoreInterfaceCodeF_HDN: TIgnoreInterfaceCodeF;

function IgnoreInterfaceCodeF: TIgnoreInterfaceCodeF;
begin
  if (IgnoreInterfaceCodeF_HDN = nil) then
    IgnoreInterfaceCodeF_HDN := TIgnoreInterfaceCodeF.Create(Application);
  Result := IgnoreInterfaceCodeF_HDN;
end;

procedure TIgnoreInterfaceCodeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  FInsertMode := True;
//  EditJobDesc.Text := '';
  ComboBoxInterface.Text := '';
  ComboBoxInterface.SetFocus;
end;

procedure TIgnoreInterfaceCodeF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  ComboBoxInterface.SetFocus;
end;

procedure TIgnoreInterfaceCodeF.FormDestroy(Sender: TObject);
begin
  inherited;
  IgnoreInterfaceCodeF_HDN := Nil;
  FListInterface.Free;
end;

procedure TIgnoreInterfaceCodeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
  if (dxDetailGrid.Items[0].Values[0] = NULL ) and
   (WORKSPOTDM.TableJobForInterface.RecordCount > 0) then
  begin
    DisplayMessage(SPimsInterfaceCode, mtError, [mbOk]);
    Abort;
  end;
  inherited;
  IgnoreInterfaceCodeF_HDN := Nil;
end;

procedure TIgnoreInterfaceCodeF.FormShow(Sender: TObject);
var
  Plant, PlantDesc, InterfaceCode{, SelectStr}: String;
  InterfaceIndex: Integer;
begin
  inherited;
  FListInterface := TStringList.Create;
  ComboBoxInterface.Clear;
  WORKSPOTDM.TableIgnoreInterface.Close;
  WORKSPOTDM.TableIgnoreInterface.Open;
  WorkSpotDM.QueryInterface.Active := True;
  Plant := WORKSPOTDM.TableMaster.FieldByName('PLANT_CODE').AsString;
  PlantDesc := WORKSPOTDM.TableMaster.FieldByName('DESCRIPTION').AsString;
  FListInterface.Clear;
  with WorkspotDM.QueryInterface do
  begin
   while not Eof do
   begin
     if FListInterface.IndexOf(FieldByName('INTERFACE_CODE').AsString) < 0 then
     begin
       FListInterface.Add(FieldByName('INTERFACE_CODE').AsString);
       ComboBoxInterface.Items.Add(Plant  + '  ' +
         PlantDesc + ' | ' +
         FieldByNAME('WORKSPOT_CODE').AsString + '  ' +
         FieldByNAME('DESCRIPTION').AsString + ' | ' +
         FieldByName('JOB_CODE').AsString + '  ' +
         FieldByName('DESCRIPTION_1').AsString + ' | ' +
         FieldByName('INTERFACE_CODE').AsString);
     end;
     Next;
    end;
  end;
  if WorkspotDM <> Nil then
    if WorkspotDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString <> '' then
    begin
      InterfaceIndex :=
        FListInterface.IndexOf(WorkspotDM.TableIgnoreInterface.
          FieldByName('INTERFACE_CODE').AsString);
      if InterfaceIndex >= 0 then
        ComboBoxInterface.ItemIndex := InterfaceIndex;
    end;
//  CheckInterface;
  WORKSPOTDM.TableIgnoreInterface.First;
  while not WORKSPOTDM.TableIgnoreInterface.Eof do
  begin
    InterfaceCode :=
      WORKSPOTDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString;
    if FListInterface.IndexOf(InterfaceCode) < 0 then
      begin
        if WORKSPOTDM.TableIgnoreTmp.FindKey([
          WorkSpotDM.TableIgnoreInterface.FieldByName('PLANT_CODE').Value,
          WorkSpotDM.TableIgnoreInterface.FieldByName('WORKSPOT_CODE').Value,
          WorkSpotDM.TableIgnoreInterface.FieldByName('JOB_CODE').Value,
          InterfaceCode]) then
          WORKSPOTDM.TableIgnoreTmp.Delete;
      end;
    WORKSPOTDM.TableIgnoreInterface.Next;
  end;
  WORKSPOTDM.TableIgnoreInterface.First;
end;

procedure  TIgnoreInterfaceCodeF.GetStrings( SelString: String;
  var Plant, PlantDesc, WK, WKDesc, Job, JobDesc, InterfaceCode: String);
var
  p: Integer;
  StrTmp: String;
begin
  Plant := '';
  PlantDesc := '';
  WK := '';
  WKDesc := '';
  Job := '';
  JobDesc := '';
  InterfaceCode := '';
  StrTmp := SelString;
  p := Pos('  ', StrTmp);
  if p > 0 then
  begin
    Plant := Copy(StrTmp, 0 , p - 1);
    StrTmp := Copy(StrTmp,p+2, 100);
    p := Pos(' | ', StrTmp);
    PlantDesc := Copy(StrTmp, 0, p - 1);
    StrTmp := Copy(StrTmp,p+3 , 100);
    P := Pos('  ' ,StrTmp);
    WK := Copy(StrTmp, 0 , p - 1);
    StrTmp := Copy(StrTmp,p +2, 100);
    p := Pos(' | ', StrTmp);
    WKDesc := Copy(StrTmp, 0, p - 1);
    StrTmp := Copy(StrTmp,p+3 , 100);

    P := Pos('  ' ,StrTmp);
    Job := Copy(StrTmp, 0 , p - 1);
    StrTmp := Copy(StrTmp,p+2 , 100);
    p := Pos(' | ', StrTmp);
    JobDesc := Copy(StrTmp, 0, p - 1);
    StrTmp := Copy(StrTmp,p+3 , 100);
    InterfaceCode := StrTmp;
  end;
end;

procedure TIgnoreInterfaceCodeF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
var
  InterfaceIndex: Integer;
begin
  inherited;
  if WorkspotDM <> Nil then
    if WorkspotDM.TableIgnoreInterface.Active then
    if (WorkspotDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString <> '') then
    begin
      InterfaceIndex :=
        FListInterface.IndexOf(WorkspotDM.TableIgnoreInterface.
          FieldByName('INTERFACE_CODE').AsString);
      if InterfaceIndex >= 0 then
        ComboBoxInterface.ItemIndex := InterfaceIndex;
    end;
end;

procedure TIgnoreInterfaceCodeF.ComboBoxInterfaceClick(Sender: TObject);
begin
  inherited;
  SetValuesBasedOnCombo;
  if FInsertMode then
    WorkspotDM.DataSourceIgnoreInterface.DataSet.Append
  else
    WorkspotDM.DataSourceIgnoreInterface.DataSET.Edit;
end;

procedure TIgnoreInterfaceCodeF.SetValuesBasedOnCombo;
var
  Plant{, PlantDesc,} ,WK, WKDesc, Job, JobDesc: String;
begin
  GetStrings(ComboBoxInterface.Text, Plant, Plant, WK, WKDesc,
    Job,JobDesc, FInterfaceCODE);
//  WORKSPOTDM.FJob := FJob;
  WORKSPOTDM.FInterfaceCode := FInterfaceCode;
end;

procedure TIgnoreInterfaceCodeF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  WorkspotDM.TableIgnoreInterface.Cancel;
  FInsertMode := False;
  ComboBoxInterface.Text := '';
end;

procedure TIgnoreInterfaceCodeF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  WorkspotDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString :=
    FInterfaceCode;
  WorkspotDM.TableIgnoreInterface.Post;
  FInsertMode := False;
end;

procedure TIgnoreInterfaceCodeF.dxDetailGridClick(Sender: TObject);
var
  InterfaceIndex: Integer;
begin
  inherited;
  if WorkspotDM <> Nil then
    if WorkspotDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString <> '' then
    begin
      InterfaceIndex :=
        FListInterface.IndexOf(WorkspotDM.TableIgnoreInterface.
          FieldByName('INTERFACE_CODE').AsString);
      if InterfaceIndex >= 0 then
        ComboBoxInterface.ItemIndex := InterfaceIndex;
    end;
end;

procedure TIgnoreInterfaceCodeF.dxBarBDBNavDeleteClick(Sender: TObject);
var
  InterfaceIndex: Integer;
begin
  inherited;
  WorkspotDM.TableIgnoreInterface.Delete;
  ComboBoxInterface.text := '';
  if WorkspotDM <> Nil then
    if WorkspotDM.TableIgnoreInterface.FieldByName('INTERFACE_CODE').AsString <> '' then
    begin
      InterfaceIndex :=
        FListInterface.IndexOf(WorkspotDM.TableIgnoreInterface.
          FieldByName('INTERFACE_CODE').AsString);
      if InterfaceIndex >= 0 then
        ComboBoxInterface.ItemIndex := InterfaceIndex;
    end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TIgnoreInterfaceCodeF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
