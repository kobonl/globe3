(*
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machines-dialog.
  MRA:11-JUN-2014 SO-20014450
  - Addition of field Department.
  MRA:19-MAY-2016 PIM-180
  - When HybridMode=Hybrid then
    - When 'edit' is allowed:
      - Do not allow add or delete, but only edit.
*)
unit MachineFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxEdLib, dxDBELib, StdCtrls, DBCtrls,
  dxDBTLCl, dxGrClms;

type
  TMachineF = class(TGridBaseF)
    lblCode: TLabel;
    lblDescription: TLabel;
    DBEditCode: TdxDBEdit;
    DBEditDescription: TdxDBEdit;
    dxMasterGridColumnPLANT_CODE: TdxDBGridColumn;
    dxMasterGridColumnDESCRIPTION: TdxDBGridColumn;
    dxMasterGridColumnCITY: TdxDBGridColumn;
    dxMasterGridColumnPHONE: TdxDBGridColumn;
    dxDetailGridMACHINE_CODE: TdxDBGridMaskColumn;
    dxDetailGridDESCRIPTION: TdxDBGridMaskColumn;
    btnJobCodes: TButton;
    btnShowWorkspots: TButton;
    dxDetailGridColumnSHORT_NAME: TdxDBGridColumn;
    DBEditShortName: TdxDBEdit;
    lblShortName: TLabel;
    lblDepartment: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    dxDetailGridColumnDEPARTMENT: TdxDBGridLookupColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure btnJobCodesClick(Sender: TObject);
    procedure btnShowWorkspotsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
     InsertForm: TForm;
  public
    { Public declarations }
  end;

function MachineF: TMachineF;

var
  MachineF_HND: TMachineF;

implementation

uses
  SystemDMT, MachineDMT, MachineJobCodeFRM, MachineWorkspotFRM,
  UPimsMessageRes, UPimsConst;

{$R *.DFM}

function MachineF: TMachineF;
begin
  if (MachineF_HND = nil) then
  begin
    MachineF_HND := TMachineF.Create(Application);
  end;
  Result := MachineF_HND;
end;

procedure TMachineF.FormCreate(Sender: TObject);
begin
  MachineDM := CreateFormDM(TMachineDM);
  if (dxDetailGrid.DataSource = nil) or (dxMasterGrid.DataSource = nil) then
  begin
    dxDetailGrid.DataSource := MachineDM.DataSourceDetail;
    dxMasterGrid.DataSource := MachineDM.DataSourceMaster;
  end;
  inherited;
end;

procedure TMachineF.FormDestroy(Sender: TObject);
begin
  inherited;
  MachineF_HND := nil;
end;

procedure TMachineF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

procedure TMachineF.btnJobCodesClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  if MachineDM.TableDetail.FieldByName('MACHINE_CODE').AsString = '' then
    Exit;
  InsertForm := MachineJobCodeF;
  InsertForm.Show;
end;

procedure TMachineF.btnShowWorkspotsClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  if MachineDM.TableDetail.FieldByName('MACHINE_CODE').AsString = '' then
    Exit;
  InsertForm := MachineWorkspotF;
  InsertForm.Show;
end;

procedure TMachineF.FormShow(Sender: TObject);
begin
  inherited;
  if SystemDM.HybridMode = hmHybrid then // PIM-180
  begin
    if Form_Edit_YN = ITEM_VISIBIL_EDIT then
    begin
      dxDetailGrid.Tag := 2; // No add or delete, but edit is allowed.
      ActiveGridRefresh;
    end;
  end;
end;

end.
