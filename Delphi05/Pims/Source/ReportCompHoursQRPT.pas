(*
  Changes:
    MRA:12-MAR-2008 Order 550463, RV005.
      Add plant-from-to selection to report.
    MRA:05-MAY-2008 RV006.
      This report never gave work-hours. Cause: there was
      a newline too much in a select-statement.
    MRA:28-JAN-2010. RV050.4.2.
    - Bugfix for FDATECURRENT-argument-error.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:23-JUL-2010. RV067.MRA.3. 550507.
    - Report does not look at timeblocks-per-department, for availability.
      Cause: The query did not look at the employee-department.
    MRA:21-OCT-2010. RV073.5. Access rights.
    - Report shows employees not belonging to the teams according
      to teams-per-user.
    MRA:4-JUL-2016 PIM-197
    - Addition of Team-selection.
    MRA:26-APR-2019 GLOB3-296
    - When all columns give 0 then it should not show the line
    - Also at some parts it still used a max. of 4 timeblocks.
*)

unit ReportCompHoursQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, DBClient,
  UGlobalFunctions, UPimsConst, UScannedIDCard, QRExport, QRXMLSFilt,
  QRWebFilt, QRPDFFilt;

// RV050.4.2. Values used for 'FShowEmployee'
const
  EMPLOYEE_ACTIVE = 0;
  EMPLOYEE_NONACTIVE = 1;
  EMPLOYEE_ALL = 2;

type
  TTBDates = Array[1..4] of TDateTime;
  TQRParameters = class
  private
    FTeamFrom, FTeamTo: String;
    FYear, FWeekFrom, FWeekTo: Integer;
    FDateFrom, FDateTo: TDateTime;
    FShowEmployee, FContractPlanOrAvail: Integer;
    FShowTotal, FShowSelection, FShowOnlyDev, FSuppressZeroes: Boolean;
    FShowAllTeam: Boolean;
    FExportToFile: Boolean; // MR:18-09-2003
  public
    procedure SetValues(
      TeamFrom, TeamTo: String;
      Year, WeekFrom, WeekTo: Integer;
      DateFrom, DateTo: TDateTime;
      ShowEmployee: Integer;
      ContractPlanOrAvail: Integer;
      ShowTotal, ShowSelection, ShowOnlyDev, SuppressZeroes,
      ShowAllTeam,
      ExportToFile: Boolean);
  end;

  TReportCompHoursQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelYear: TQRLabel;
    QRLabelShowEmpl: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRDBTextEmpl: TQRDBText;
    QRDBTextEmplDesc: TQRDBText;
    QRBandTitleEmpl: TQRBand;
    QRShape5: TQRShape;
    QRLabel34: TQRLabel;
    QRShape6: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblWeekFrom: TQRLabel;
    QRLabel5: TQRLabel;
    QRLblWeekTo: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabelWkHours: TQRLabel;
    QRLabelIllness: TQRLabel;
    QRLabelHoliday: TQRLabel;
    QRLabelPaidTot: TQRLabel;
    QRLabelUnpaid: TQRLabel;
    QRLabelTotUnpaid: TQRLabel;
    QRLabelContract: TQRLabel;
    QRLabelDifference: TQRLabel;
    QRLabelPaid: TQRLabel;
    QRLabelTotWKHours: TQRLabel;
    QRLabelTotIll: TQRLabel;
    QRLabelTotHol: TQRLabel;
    QRLabelTotPaid: TQRLabel;
    QRLabelTotEmplPaid: TQRLabel;
    QRLabelTotEmplUnpaid: TQRLabel;
    QRLabelTotEmpl: TQRLabel;
    QRLabelTotContr: TQRLabel;
    QRLabelTotDif: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel39: TQRLabel;
    QRLblTeamFrom: TQRLabel;
    QRLabel38: TQRLabel;
    QRLblTeamTo: TQRLabel;
    QRGroupTeamHD: TQRGroup;
    QRBandTeamGrpFooter: TQRBand;
    QRLabel21: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel22: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabelTeamWkHours: TQRLabel;
    QRLabelTeamIllness: TQRLabel;
    QRLabelTeamHoliday: TQRLabel;
    QRLabelTeamPaid: TQRLabel;
    QRLabelTeamPaidTot: TQRLabel;
    QRLabelTeamUnpaid: TQRLabel;
    QRLabelTeamTotUnpaid: TQRLabel;
    QRLabelTeamContract: TQRLabel;
    QRLabelTeamDifference: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelWkHoursPrint(sender: TObject; var Value: String);
    procedure QRLabelIllnessPrint(sender: TObject; var Value: String);
    procedure QRLabelHolidayPrint(sender: TObject; var Value: String);
    procedure QRLabelPaidPrint(sender: TObject; var Value: String);
    procedure QRLabelPaidTotPrint(sender: TObject; var Value: String);
    procedure QRLabelUnpaidPrint(sender: TObject; var Value: String);
    procedure QRLabelTotUnpaidPrint(sender: TObject; var Value: String);
    procedure QRLabelTotWKHoursPrint(sender: TObject; var Value: String);
    procedure QRLabelTotIllPrint(sender: TObject; var Value: String);
    procedure QRLabelTotHolPrint(sender: TObject; var Value: String);
    procedure QRLabelTotPaidPrint(sender: TObject; var Value: String);
    procedure QRLabelTotEmplPaidPrint(sender: TObject; var Value: String);
    procedure QRLabelTotEmplUnpaidPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotEmplPrint(sender: TObject; var Value: String);
    procedure QRLabelTotContrPrint(sender: TObject; var Value: String);
    procedure QRLabelTotDifPrint(sender: TObject; var Value: String);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandTitleEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupTeamHDBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTeamGrpFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTeamGrpFooterAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupTeamHDAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
    function DetermineEmployeePlanning(
      const APlantCode: String;
      const AEmployeeNumber: Integer;
      const APlanningDateTimeMin: TDateTime;
      const APlanningDateTimeMax: TDateTime;
      var VStartTime, VEndTime: TDateTime;
      var VShiftNumber: Integer;
      var VEmpLevel: String;
      var VWorkspotCode: String;
      var VPlanMinutes: Integer): Boolean;
    function DetermineEmployeeAvailable(
      const APlantCode: String;
      const AEmployeeNumber: Integer;
      const AAvailableDateTimeMin: TDateTime;
      const AAvailableDateTimeMax: TDateTime;
      var VStartTime, VEndTime: TDateTime;
      var VShiftNumber: Integer;
      var VAvailableMinutes: Integer): Boolean;
  protected
    FEmptyEmplReq: Boolean;
    FMinDate, FMaxDate: TDateTime;
    FWK_Hrs,
    FIll_Hrs,
    FHol_Hrs,
    FPaid_Hrs,
    FUnpaid_Hrs,
    FAbsIllMin,
    FAbsHolMin,
    FAbsPaidMin,
    FAbsUnpaidMin,
    FSalMin: Integer;
    FContractPlanOrAvail_Hrs,
    FContrPlanOrAvailEmpl_Hrs: Double;
    TeamWKHrs, TeamIllness, TeamHol,
    TeamPaid, TeamPaidTot, TeamUnpaid, TeamUnpaidTot,
    TeamContract, TeamDiff: Double;
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
    function DecodeHrsDouble(Hrs: Double): String;
  public
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const EmployeeFrom, EmployeeTo: String;
      const TeamFrom, TeamTo: String;
      const Year, WeekFrom, WeekTo: Integer;
      const DateFrom, DateTo: TDateTime;
      const ShowEmployee: Integer;
      const ContractPlanOrAvail: Integer;
      const ShowTotal, ShowSelection, ShowOnlyDev, SuppressZeroes, ShowAllTeam,
      ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure GetAbsence_Minutes(Absence_Reason_Code: String;
      QuerySel: TQuery; ClientDataSet: TClientDataSet);
    function SetQueryParameters(SelectStr: String;
      QuerySel: TQuery;
      SetCurrentDate, SetIntervDate, SetActive: Boolean;
      SetUser: Boolean = False): Boolean;
  end;

var
  ReportCompHoursQR: TReportCompHoursQR;
  ExportList: array[1..9] of String;

implementation

{$R *.DFM}

uses
  SystemDMT, ReportCompHoursDMT, ListProcsFRM, UPimsMessageRes,
  CalculateTotalHoursDMT;

procedure TQRParameters.SetValues(
  TeamFrom, TeamTo: String;
  Year, WeekFrom, WeekTo: Integer;
  DateFrom, DateTo: TDateTime;
  ShowEmployee: Integer;
  ContractPlanOrAvail: Integer;
  ShowTotal, ShowSelection, ShowOnlyDev, SuppressZeroes, ShowAllTeam,
  ExportToFile: Boolean);
begin
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FYear := Year;
  FWeekFrom := WeekFrom;
  FWeekTo := WeekTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowTotal := ShowTotal;
  FShowSelection := ShowSelection;
  FShowEmployee := ShowEmployee;
  FShowOnlyDev := ShowOnlyDev;
  FContractPlanOrAvail := ContractPlanOrAvail;
  FSuppressZeroes := SuppressZeroes;
  FShowAllTeam := ShowAllTeam;
  FExportToFile := ExportToFile;
end;

function TReportCompHoursQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const EmployeeFrom, EmployeeTo: String;
  const TeamFrom, TeamTo: String;
  const Year, WeekFrom, WeekTo: Integer;
  const DateFrom, DateTo: TDateTime;
  const ShowEmployee: Integer;
  const ContractPlanOrAvail: Integer;
  const ShowTotal, ShowSelection, ShowOnlyDev, SuppressZeroes, ShowAllTeam,
  ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(TeamFrom, TeamTo, Year, WeekFrom, WeekTo,
      DateFrom, DateTo,
      ShowEmployee, ContractPlanOrAvail,
      ShowTotal, ShowSelection, ShowOnlyDev, SuppressZeroes, ShowAllTeam,
      ExportToFile);
  end;
  SetDataSetQueryReport(ReportCompHoursDM.QueryEmpl);
end;

function TReportCompHoursQR.SetQueryParameters(SelectStr: String;
  QuerySel: TQuery; SetCurrentDate, SetIntervDate, SetActive: Boolean;
  SetUser: Boolean = False): Boolean;
begin
  Result := False;
  QuerySel.Active := False;
  QuerySel.UniDirectional := False;
  QuerySel.SQL.Clear;
  QuerySel.SQL.Add(UpperCase(SelectStr));
  if SetIntervDate then
  begin
    QuerySel.ParamByName('FSTARTDATE').Value := FMinDate;
    QuerySel.ParamByName('FENDDATE').Value :=  FMaxDate;
  end;
  if (SetCurrentDate) and (QRParameters.FShowEmployee <> EMPLOYEE_ALL) then
  begin
    QuerySel.ParamByName('FDATE').AsDateTime := Now;
  end;
  // RV073.5. Access rights.
  if SetUser then
    QuerySel.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  if not QuerySel.Prepared then
    QuerySel.Prepare;
  if SetActive then
  begin
    QuerySel.Active := True;
    Result := not (QuerySel.IsEmpty);
  end;
end;

procedure TReportCompHoursQR.GetAbsence_Minutes(Absence_Reason_Code: String;
  QuerySel: TQuery; ClientDataSet: TClientDataSet);
var
  SelectStr: String;
begin
  ClientDataSet.Close;
  SelectStr :=
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER, SUM(A.ABSENCE_MINUTE) AS ABS_MIN ' + NL +
    'FROM ' + NL +
    '   ABSENCEHOURPEREMPLOYEE A, ABSENCEREASON R, EMPLOYEE E ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  AND A.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllTeam then
    SelectStr := SelectStr +
     ' AND ((E.TEAM_CODE >= ''' + QRParameters.FTeamFrom +
     ''') AND (E.TEAM_CODE <= ''' + QRParameters.FTeamTo + ''')) ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    SelectStr := SelectStr +
      '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
      '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  SelectStr := SelectStr +
    '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
    '  AND A.ABSENCEHOUR_DATE <= :FENDDATE ' + NL +
    '  AND R.ABSENCETYPE_CODE IN ' +  Absence_Reason_Code + NL;
  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL +
    'ORDER BY ' + NL +
    '  A.EMPLOYEE_NUMBER' + NL;
  SetQueryParameters(SelectStr, QuerySel, False, True, False);
  ClientDataSet.Open;
end;

function TReportCompHoursQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin

  Screen.Cursor := crHourGlass;
  with ReportCompHoursDM do
  begin
    if QRParameters.FContractPlanOrAvail = 0 then
    begin
      FMinDate :=
        ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekFrom, 1);
      FMaxDate :=
        ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekTo, 7);
    end
    else
    begin
      FMinDate := Trunc(QRParameters.FDateFrom) + Frac(EncodeTime(0, 0, 0, 0));
      FMaxDate := Trunc(QRParameters.FDateTo) + Frac(EncodeTime(23, 59, 59, 0));
    end;
    SelectStr :=
      'SELECT ' + NL +
      '  E.TEAM_CODE, T.DESCRIPTION TDESCRIPTION, ' + NL +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, E.STARTDATE, E.ENDDATE, ' + NL +
      '  E.PLANT_CODE, E.CUT_OF_TIME_YN, ' + NL +
      '  P.INSCAN_MARGIN_EARLY, P.INSCAN_MARGIN_LATE, ' + NL +
      '  P.OUTSCAN_MARGIN_EARLY, P.OUTSCAN_MARGIN_LATE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E LEFT JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL +
      '  INNER JOIN PLANT P ON ' + NL +
      '    E.PLANT_CODE = P.PLANT_CODE ' + NL +
      'WHERE ' + NL +
      // RV073.5. Access rights.
      '  ( ' + NL +
      '    (:USER_NAME = ''*'') OR ' + NL +
      '    (E.TEAM_CODE IN ' + NL +
      '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
      '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
      '  ) ' + NL +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
    if not QRParameters.FShowAllTeam then
         SelectStr := SelectStr +
         ' AND ((E.TEAM_CODE >= ''' + QRParameters.FTeamFrom +
         ''') AND (E.TEAM_CODE <= ''' + QRParameters.FTeamTo + ''')) ' + NL;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
      SelectStr := SelectStr +
        '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    if QRParameters.FShowEmployee = EMPLOYEE_ACTIVE then
      SelectStr := SelectStr +
        ' AND (E.STARTDATE <= :FDATE) ' + NL +
        ' AND ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) ' + NL
    else
      if QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE then
        SelectStr := SelectStr +
          ' AND (E.STARTDATE > :FDATE OR E.ENDDATE < :FDATE) ' + NL;
    SelectStr := SelectStr + ' ' +
      'ORDER BY ' + NL +
      '  E.TEAM_CODE, E.EMPLOYEE_NUMBER ' + NL;
    Result := SetQueryParameters(SelectStr,
      ReportCompHoursDM.QueryEmpl, True, False,
      True, True);
    if not Result then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;

    // MRA:05-MAY-2008 RV006.
    // There was a newline too much, causing this query to never give
    // a result.
    SelectStr :=
      'SELECT ' + NL +
      '  S.EMPLOYEE_NUMBER, SUM(S.SALARY_MINUTE) AS ABS_MIN ' + NL +
      'FROM ' + NL +
      '  SALARYHOURPEREMPLOYEE S, EMPLOYEE E ' + NL +
      'WHERE ' + NL +
      '  S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
    if not QRParameters.FShowAllTeam then
         SelectStr := SelectStr +
         ' AND ((E.TEAM_CODE >= ''' + QRParameters.FTeamFrom +
         ''') AND (E.TEAM_CODE <= ''' + QRParameters.FTeamTo + '''))' + NL;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
      SelectStr := SelectStr +
        '  AND S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE <= :FENDDATE ' + NL +
      'GROUP BY ' + NL +
      '  S.EMPLOYEE_NUMBER ' + NL +
      'ORDER BY ' + NL +
      '  S.EMPLOYEE_NUMBER ';
    ClientDataSetSal.Close;
    SetQueryParameters(SelectStr, QuerySalaryMinute, False, True, False);
    ClientDataSetSal.Open;
    GetAbsence_Minutes('(''' + ILLNESS + ''')',
      QueryAbsMinute_Ill, ClientDataSetAbsIll);
    GetAbsence_Minutes('(''' + HOLIDAYAVAILABLETB + ''')',
      QueryAbsMinute_Hol, ClientDataSetAbsHol);
    GetAbsence_Minutes('(''' + PAID + ''', ''' + RSNB + ''', ''' +
      TIMEFORTIMEAVAILABILITY + ''',' + '''' +
      WORKTIMEREDUCTIONAVAILABILITY + ''')', QueryAbsMinute_Paid,
      ClientDataSetAbsPaid);
    GetAbsence_Minutes('(''' + UNPAID + ''')', QueryAbsMinute_UnPaid,
      ClientDataSetAbsUnpaid);

    // Get EmployeePlanning-data
    case QRParameters.FContractPlanOrAvail of
//    0: // Contract, do nothing here
    1: // Planned
      begin
        with qryEmployeePlanning do
        begin
          ClientDataSetEmpPln.Close;
          Close;
          ParamByName('EMPFROM').AsInteger :=
            StrToInt(QRBaseParameters.FEmployeeFrom);
          ParamByName('EMPTO').AsInteger :=
            StrToInt(QRBaseParameters.FEmployeeTo);
          ParamByName('DATEFROM').AsDateTime := FMinDate;
          ParamByName('DATETO').AsDateTime := FMaxDate;
          if not Prepared then
            Prepare;
           ClientDataSetEmpPln.Open;
        end;
      end;
    2: // Available
      begin
        with qryEmployeeAvailable do
        begin
          ClientDataSetEmpAV.Close;
          Close;
          ParamByName('EMPFROM').AsInteger :=
            StrToInt(QRBaseParameters.FEmployeeFrom);
          ParamByName('EMPTO').AsInteger :=
            StrToInt(QRBaseParameters.FEmployeeTo);
          ParamByName('DATEFROM').AsDateTime := FMinDate;
          ParamByName('DATETO').AsDateTime := FMaxDate;
          if not Prepared then
            Prepare;

          ClientDataSetEmpAV.Open;
        end;
      end;
    end;
    FWK_Hrs := 0;
    FIll_Hrs := 0;
    FHol_Hrs := 0;
    FPaid_Hrs := 0;
    FUnpaid_Hrs := 0;
    FContractPlanOrAvail_Hrs := 0;

  {check if report is empty}
    Screen.Cursor := crDefault;
  end;
end;

procedure TReportCompHoursQR.ConfigReport;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes; // RV054.4.

  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  end
  else
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end;
  if (not QRParameters.FShowAllTeam) then
  begin
    QRLblTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLblTeamTo.Caption := QRParameters.FTeamTo;
  end
  else
  begin
    QRLblTeamFrom.Caption := '*';
    QRLblTeamTo.Caption := '*';
  end;
  if QRParameters.FShowEmployee = EMPLOYEE_ACTIVE then
    QRLabelShowEmpl.Caption := SRepHrsPerEmplActiveEmpl;
  if QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE then
    QRLabelShowEmpl.Caption := SRepHrsPerEmplInActiveEmpl;
  if QRParameters.FShowEmployee = EMPLOYEE_ALL then
    QRLabelShowEmpl.Caption := '';
  case QRParameters.FContractPlanOrAvail of
  0: QRLabel14.Caption := SPimsContract;
  1: QRLabel14.Caption := SPimsPlanned;
  2: QRLabel14.Caption := SPimsAvailable;
  end;

  if QRParameters.FContractPlanOrAvail = 0 then
  begin
    QRLabelDate.Caption := SPimsYear;
    QRLabel2.Caption := SPimsWeek;
    QRLabelYear.Caption := IntToStr(QRParameters.FYear);
    QRLblWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
    QRLblWeekTo.Caption := IntToStr(QRParameters.FWeekTo) + ' ' +
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
      QRParameters.FWeekFrom, 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
      QRParameters.FWeekTo, 7)) + ')';
  end
  else
  begin
    QRLabelDate.Caption := '';
    QRLabelYear.Caption := '';
    QRLabel2.Caption := SPimsDate;
    QRLblWeekFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLblWeekTo.Caption := DateToStr(QRParameters.FDateTo);
  end;
end;

procedure TReportCompHoursQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportCompHoursQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

function TReportCompHoursQR.DecodeHrsDouble(Hrs: Double): String;
begin
  // RV054.4.
  if QRParameters.FSuppressZeroes then
    if Trunc(Hrs) = 0 then
    begin
      Result := '';
      Exit;
    end;
    
  Result := FillZero(Trunc(Hrs)) + ':' +
    FillZero(Round(60 * (Hrs - Trunc(Hrs))));
end;

procedure TReportCompHoursQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Selections
    if QRParameters.FShowSelection then
    begin
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' + QRLabel19.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel20.Caption + ' ' +
        QRLblEmployeeFrom.Caption + ' ' + QRLabel16.Caption + ' ' +
        QRLblEmployeeTo.Caption);
      ExportClass.AddText(QRLabel40.Caption + ' ' + QRLabel39.Caption + ' ' +
        QRLblTeamFrom.Caption + ' ' + QRLabel38.Caption + ' ' +
        QRLblTeamTo.Caption);
      ExportClass.AddText(QRLabelDate.Caption + ' ' + QRLabelYear.Caption);
      ExportClass.AddText(QRLabel1.Caption + ' ' + QRLabel2.Caption + ' ' +
        QRLblWeekFrom.Caption + ' ' + QRLabel5.Caption + ' ' +
        QRLblWeekTo.Caption);
      ExportClass.AddText(QRLabelShowEmpl.Caption);
    end;
  end;
  FWK_Hrs := 0;
  FIll_Hrs := 0;
  FHol_Hrs := 0;
  FPaid_Hrs := 0;
  FUnpaid_Hrs := 0;
  FContractPlanOrAvail_Hrs := 0;
end;

procedure TReportCompHoursQR.QRLabelWkHoursPrint(sender: TObject;
  var Value: String);
var
  Worked_Min: Integer;
begin
  inherited;
  Worked_Min := FSalMin;

  FWK_Hrs := FWK_Hrs + Worked_Min;
  TeamWKHrs := TeamWKHrs + Worked_Min;
  Value := DecodeHrsMin(Worked_Min);
  ExportList[1] := Value;
end;

procedure TReportCompHoursQR.QRLabelIllnessPrint(sender: TObject;
  var Value: String);
var
  Ill_Min: Integer;
begin
  inherited;
  Ill_Min := FAbsIllMin;

  FIll_Hrs := FIll_Hrs + Ill_Min;
  TeamIllness := TeamIllness + Ill_Min;
  Value := DecodeHrsMin(Ill_Min);
  ExportList[2] := Value;
end;

procedure TReportCompHoursQR.QRLabelHolidayPrint(sender: TObject;
  var Value: String);
var
  Hol_Min: Integer;
begin
  inherited;
  Hol_Min := FAbsHolMin;
  FHol_Hrs := FHol_Hrs + Hol_Min;
  TeamHol := TeamHol + Hol_Min;
  Value := DecodeHrsMin(Hol_Min);
  ExportList[3] := Value;
end;

procedure TReportCompHoursQR.QRLabelPaidPrint(sender: TObject;
  var Value: String);
var
  Paid_Min: Integer;
begin
  inherited;
  Paid_Min := FAbsPaidMin;
  FPaid_Hrs := FPaid_Hrs + Paid_Min;
  TeamPaid := TeamPaid + Paid_Min;
  Value := DecodeHrsMin(Paid_Min);
  ExportList[4] := Value;
end;

procedure TReportCompHoursQR.QRLabelPaidTotPrint(sender: TObject;
  var Value: String);
var
  PaidTotal: Integer;
begin
  inherited;
  PaidTotal := FAbsPaidMin + FAbsHolMin + FAbsIllMin + FSalMin;
  TeamPaidTot := TeamPaidTot + PaidTotal;
  Value := DecodeHrsMin(PaidTotal);
  ExportList[5] := Value;
end;

procedure TReportCompHoursQR.QRLabelUnpaidPrint(sender: TObject;
  var Value: String);
var
  UnPaid_Min: Integer;
begin
  inherited;
  UnPaid_Min := FAbsUnPaidMin;
  FUnPaid_Hrs := FUnPaid_Hrs + UnPaid_Min;
  TeamUnpaid := TeamUnpaid + UnPaid_Min;
  Value := DecodeHrsMin(UnPaid_Min);
  ExportList[6] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotUnpaidPrint(sender: TObject;
  var Value: String);
var
  Total: Integer;
begin
  inherited;
  Total := FAbsIllMin + FAbsHolMin + FAbsPaidMin + FAbsUnPaidMin + FSalMin;
  TeamUnpaidTot := TeamUnpaidTot + Total;
  Value := DecodeHrsMin(Total);
  ExportList[7] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotWKHoursPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FWK_Hrs);
  ExportList[1] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotIllPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FIll_Hrs);
  ExportList[2] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotHolPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FHol_Hrs);
  ExportList[3] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotPaidPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FPaid_Hrs);
  ExportList[4] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotEmplPaidPrint(sender: TObject;
  var Value: String);
var
  Total: Integer;
begin
  inherited;
  Total := FWK_Hrs + FIll_Hrs + FHol_Hrs + FPaid_Hrs;
  Value := DecodeHrsMin(Total);
  ExportList[5] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotEmplUnpaidPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FUnpaid_Hrs);
  ExportList[6] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotEmplPrint(sender: TObject;
  var Value: String);
var
  Total: Integer;
begin
  inherited;
  Total := FWK_Hrs + FIll_Hrs + FHOL_Hrs + FPaid_Hrs + FUnPaid_Hrs;
  Value := DecodeHrsMin(Total);
  ExportList[7] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotContrPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // MR:22-11-2004 variable is a Double.
  Value := DecodeHrsDouble(FContractPlanOrAvail_Hrs);
  ExportList[8] := Value;
end;

procedure TReportCompHoursQR.QRLabelTotDifPrint(sender: TObject;
  var Value: String);
var
  Diff: Integer;
begin
  inherited;
  Diff := FWK_Hrs + FIll_Hrs + FHol_Hrs + FPaid_Hrs + FUnpaid_Hrs -
    Round(FContractPlanOrAvail_Hrs * 60);
  Value := DecodeHrsMin(Diff);
  ExportList[9] := Value;
end;

procedure TReportCompHoursQR.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowTotal;
end;

procedure TReportCompHoursQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Empl, WeekNr: Integer;
  StartYear, EndYear, StartWeek, EndWeek: Word;
  LastDateOnEndWeek, FirstDateOnStartWeek: TDateTime;
  Result: Boolean;
  VStartTime, VEndTime: TDateTime;
  VShiftNumber: Integer;
  VEmpLevel: String;
  VWorkspotCode: String;
  PlanningMinutes: Integer;
  ProdMin: Integer;
  AvailableMinutes: Integer;
  function PrintLabelDiff: Boolean;
  var
    Total: Integer;
  begin
    Total := FAbsIllMin + FAbsHolMin + FAbsPaidMin + FAbsUnpaidMin + FSalMin;
    Total := Total - Round(FContrPlanOrAvailEmpl_Hrs * 60);
    Result := (Total <> 0);
    QRLabelDifference.Caption := DecodeHrsMin(Total);
    TeamDiff := TeamDiff + Total;
    ExportList[9] := QRLabelDifference.Caption;
  end;
  function DetermineTotal: Integer; // RV054.4.
  begin
    Result := FAbsIllMin + FAbsHolMin + FAbsPaidMin + FAbsUnpaidMin + FSalMin +
      Round(FContrPlanOrAvailEmpl_Hrs); // GLOB3-296
  end;
begin
  inherited;
//
  with ReportCompHoursDM do
  begin
    Empl := ReportCompHoursDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    SetParamEmpl(ClientDataSetAbsIll, Empl, FAbsIllMin);
    SetParamEmpl(ClientDataSetAbsHol, Empl, FAbsHolMin);
    SetParamEmpl(ClientDataSetAbsPaid, Empl, FAbsPaidMin);
    SetParamEmpl(ClientDataSetAbsUnpaid, Empl, FAbsUnpaidMin);
    SetParamEmpl(ClientDataSetSal, Empl, FSalMin);
    FEmptyEmplReq := Not ReportCompHoursDM.ClientDataSetEmpReq.Locate(
      'EMPLOYEE_NUMBER', VarArrayOf([Empl]), []);
  end;

  // GLOB3-296
{
  // RV054.4.
  if QRParameters.FSuppressZeroes then
    if DetermineTotal = 0 then
    begin
      PrintBand := False;
      Exit;
    end;
}
  FContrPlanOrAvailEmpl_Hrs := 0;

  case QRParameters.FContractPlanOrAvail of
  0:  // Contract hours
    begin

      with ReportCompHoursDM do
      begin
        Empl := QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ClientDataSetEmplContr.First;
        ClientDataSetEmplContr.FindNearest([Empl]);
        while (not ClientDataSetEmplContr.Eof) and
          (ClientDataSetEmplContr.FieldByName('EMPLOYEE_NUMBER').AsInteger = Empl) do
        begin
          if ClientDataSetEmplContr.FieldByName('STARTDATE').AsDateTime > 0 then
           ListProcsF.WeekUitDat(ClientDataSetEmplContr.FieldByName('STARTDATE').AsDateTime,
             StartYear, StartWeek)
          else
          begin
            StartYear := 0;
            StartWeek := 0;
          end;
          if ClientDataSetEmplContr.FieldByName('ENDDATE').AsDateTime > 0 then
            ListProcsF.WeekUitDat(ClientDataSetEmplContr.FieldByName('ENDDATE').AsDateTime,
              EndYear, EndWeek)
          else
          begin
            EndYear := 0;
            EndWeek := 0;
          end;
          if ((StartWeek > QRParameters.FWeekTo) and
            (StartYear = QRParameters.FYear)) or
            (StartYear > QRParameters.FYear) then
          begin
            FContractPlanOrAvail_Hrs := FContractPlanOrAvail_Hrs +
              FContrPlanOrAvailEmpl_Hrs;
            QRLabelContract.Caption :=
              DecodeHrsDouble(FContrPlanOrAvailEmpl_Hrs);
            TeamContract := TeamContract + FContrPlanOrAvailEmpl_Hrs;
            ExportList[8] := QRLabelContract.Caption;
            PrintBand := True;
            Result := PrintLabelDiff;
            if QRParameters.FShowOnlyDev then
               PrintBand := Result;
            Exit;
          end;
          if (StartYear < QRParameters.FYear) or
            ((StartYear = QRParameters.FYear) and (StartWeek < QRParameters.FWeekFrom)) then
          begin
            StartWeek := QRParameters.FWeekFrom;
            StartYear := QRParameters.FYear
          end;
          if (EndYear < QRParameters.FYear) or
             ((EndYear = QRParameters.FYear) and (EndWeek < QRParameters.FWeekFrom)) then
    // not valid contract
          else
          begin
            if (EndYear > QRParameters.FYear) or
              ((EndYear = QRParameters.FYear) and (EndWeek > QRParameters.FWeekTo)) then
            begin
              EndYear := QRParameters.FYear;
              EndWeek := QRParameters.FWeekTo
            end;
            FirstDateOnStartWeek := ListProcsF.DateFromWeek(StartYear, StartWeek, 1);
            LastDateOnEndWeek := ListProcsF.DateFromWeek(EndYear, EndWeek, 7);
            if (StartYear = EndYear) then
              WeekNr := (EndWeek - StartWeek)
            else
              WeekNr := (ListProcsF.WeeksInYear(StartYear) - StartWeek + 1 + EndWeek);
            if (ClientDataSetEmplContr.FieldByName('STARTDATE').AsDateTime > FirstDateOnStartWeek) then
              WeekNr := WeekNr - 1;
            if (ClientDataSetEmplContr.FieldByName('ENDDATE').AsDateTime >= LastDateOnEndWeek) then
              WeekNr := WeekNr + 1;

            // MR:22-11-2004 'CONTRACT_HOUR_PER_WEEK' is a Double!
            FContrPlanOrAvailEmpl_Hrs :=
              ClientDataSetEmplContr.
                FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat *
                  WeekNr + FContrPlanOrAvailEmpl_Hrs
          end;
          ClientDataSetEmplContr.Next;
        end;
      end;
    end;
  1:  // Planning Hours
    begin
      // MR:03-03-2003
      // Get EmployeePlanning-information
      //CAR :07-07-2003 improvements for speed
      PlanningMinutes := 0;
      if DetermineEmployeePlanning(
        ReportCompHoursDM.QueryEmpl.FieldByName('PLANT_CODE').AsString,
        ReportCompHoursDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        FMinDate, FMaxDate,
        VStartTime, VEndTime,
        VShiftNumber,
        VEmpLevel,
        VWorkspotCode,
        ProdMin) then
        PlanningMinutes := PlanningMinutes + ProdMin;

      FContrPlanOrAvailEmpl_Hrs := PlanningMinutes / 60;
    end;
  2:  // Available hours
    begin
      // MR:03-03-2003
      // Get EmployeeAvailability-information

      AvailableMinutes := 0;
      if DetermineEmployeeAvailable(
        ReportCompHoursDM.QueryEmpl.FieldByName('PLANT_CODE').AsString,
        ReportCompHoursDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        FMinDate, FMaxDate,
        VStartTime, VEndTime,
        VShiftNumber,
        ProdMin) then
        AvailableMinutes := AvailableMinutes + ProdMin;
        
      FContrPlanOrAvailEmpl_Hrs := AvailableMinutes / 60;
    end;
  end; // end case

  FContractPlanOrAvail_Hrs := FContractPlanOrAvail_Hrs +
    FContrPlanOrAvailEmpl_Hrs;
  QRLabelContract.Caption := DecodeHrsDouble(FContrPlanOrAvailEmpl_Hrs);
  TeamContract := TeamContract + FContrPlanOrAvailEmpl_Hrs;
  ExportList[8] := QRLabelContract.Caption;
  Result := PrintLabelDiff;
  if QRParameters.FShowOnlyDev then
    PrintBand := Result;
  // GLOB3-296
  if DetermineTotal = 0 then
  begin
    PrintBand := False;
  end;
end;

//CAR 7-7-2003 - improvements

function TReportCompHoursQR.DetermineEmployeePlanning(
  const APlantCode: String;
  const AEmployeeNumber: Integer;
  const APlanningDateTimeMin: TDateTime;
  const APlanningDateTimeMax: TDateTime;
  var VStartTime, VEndTime: TDateTime;
  var VShiftNumber: Integer;
  var VEmpLevel: String;
  var VWorkspotCode: String;
  var VPlanMinutes: Integer): Boolean;
var

  StartTime, EndTime: TDateTime;
  Accept, PlanningFound: Boolean;
  Block: Integer;
  EmployeeData: TScannedIDcard;
  ProdMin, BreaksMin, PayedBreaks: Integer;
  DayPlanning: Integer;
  PlanningDateTime, BookingDay{,StartDate, EndDate}: TDateTime;

begin
  PlanningFound := False;
  VPlanMinutes := 0;
  with ReportCompHoursDM.ClientDataSetEmpPln do
  begin
    // MR:13-10-2004
    // Use Filter instead of 'locate', no need to look for dates, because
    // query is already selected with 'datemin' and 'datemax'.
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
(*
    Locate('EMPLOYEE_NUMBER;EMPLOYEEPLANNING_DATE',
      VarArrayOf([AEmployeeNumber, APlanningDateTimeMin]), []);
    while not Eof and
      (FieldByName('EMPLOYEE_NUMBER').Value = AEmployeeNumber) and
      (FieldByName('EMPLOYEEPLANNING_DATE').Value <= APlanningDateTimeMax) do
*)
    while not Eof do
    begin
      PlanningDateTime := FieldByName('EMPLOYEEPLANNING_DATE').Value;
      DayPlanning :=  ListProcsF.DayWStartOnFromDate(PlanningDateTime);
      ATBLengthClass.FillTBArray(AEmployeeNumber,
        FieldByName('SHIFT_NUMBER').Value,
        FieldByName('PLANT_CODE').Value,
        FieldByName('DEPARTMENT_CODE').Value);
      for Block := 1 to SystemDM.MaxTimeblocks do // GLOB3-296
      begin
        if (FieldByName('SCHEDULED_TIMEBLOCK_' +
           IntToStr(Block)).AsString <> '') then
          if FieldByName('SCHEDULED_TIMEBLOCK_' +
            IntToStr(Block)).AsString[1] in ['A', 'B', 'C'] then
          begin
            Accept := ATBLengthClass.ExistTB(Block);
            if Accept then
            begin
              StartTime := ATBLengthClass.GetStartTime(Block,
                DayPlanning);
              EndTime := ATBLengthClass.GetEndTime(Block,
                DayPlanning);;
            end;
            Accept := (StartTime  > 0) and ( EndTime  > 0) and
              (StartTime <> EndTime);
            if Accept then
            begin
              PlanningFound := True;
              VShiftNumber := FieldByName('SHIFT_NUMBER').Value;
              VWorkspotCode := FieldByName('WORKSPOT_CODE').Value;
              VEmpLevel := FieldByName('SCHEDULED_TIMEBLOCK_'
                + IntToStr(Block)).AsString;
              ReplaceDate(StartTime, PlanningDateTime);
              ReplaceDate(EndTime, PlanningDateTime);
              VStartTime := StartTime;
              VEndTime := EndTime;
              EmployeeData.CutOfTime :=
                ReportCompHoursDM.QueryEmpl.FieldByName('CUT_OF_TIME_YN').AsString;
              EmployeeData.EmployeeCode := AEmployeeNumber;
              EmployeeData.InscanEarly := ReportCompHoursDM.QueryEmpl.
                FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
              EmployeeData.InscanLate := ReportCompHoursDM.QueryEmpl.
                FieldByName('INSCAN_MARGIN_LATE').AsInteger;
              EmployeeData.OutscanEarly := ReportCompHoursDM.QueryEmpl.
                FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
              EmployeeData.OutscanLate := ReportCompHoursDM.QueryEmpl.
                FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
              EmployeeData.PlantCode := APlantCode;
              EmployeeData.DepartmentCode :=
                ReportCompHoursDM.ClientDataSetEmpPln.
                FieldByName('DEPARTMENT_CODE').Value;
              EmployeeData.WorkSpotCode := VWorkspotCode;
              EmployeeData.ShiftNumber := VShiftNumber;
              EmployeeData.DateIn := VStartTime;
              EmployeeData.DateOut := VEndTime;
              //CAR 23-7-2003
//              ProdMinClass.GetProdMin(EmployeeData,
//                FEmptyEmplReq, BookingDay,
//                ProdMin, BreaksMin, PayedBreaks);
              // MR:25-09-2003 Function is changed
              AProdMinClass.GetProdMin(EmployeeData,
                True, False, False, EmployeeData, BookingDay,
                ProdMin, BreaksMin, PayedBreaks);
              VPlanMinutes := VPlanMinutes + ProdMin + PayedBreaks;
            end;  // if Accept then
          end;  // if FieldByName('SCHEDULED_TIMEBLOCK_' +
      end; // for Block := 1 to
      Next;
    end;  // while not Eof
  end;
  Result := PlanningFound;
end;

function TReportCompHoursQR.DetermineEmployeeAvailable(
  const APlantCode: String;
  const AEmployeeNumber: Integer;
  const AAvailableDateTimeMin: TDateTime;
  const AAvailableDateTimeMax: TDateTime;
  var VStartTime, VEndTime: TDateTime;
  var VShiftNumber: Integer;
  var VAvailableMinutes: Integer): Boolean;
var
  SavePlant: String;
  SaveShift: Integer;
  StartTime, EndTime: TDateTime;
  Accept, AvailableFound: Boolean;
  Block: Integer;
  DayAvail: Integer;
  EmployeeData: TScannedIDcard;
  AvailDateTime, BookingDay: TDateTime;
  ProdMin, BreaksMin, PayedBreaks: Integer;

begin
  AvailableFound := False;
  VAvailableMinutes := 0;
  with ReportCompHoursDM.ClientDataSetEmpAv do
  begin
    // An employee can have more than 1 availability-records on a specific day
    // MR:13-10-2004
    // Use Filter instead of 'locate', no need to look for dates, because
    // query is already selected with 'datemin' and 'datemax'.
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
(*
     Locate('EMPLOYEE_NUMBER;EMPLOYEEAVAILABILITY_DATE',
     VarArrayOf([AEmployeeNumber, AAvailableDateTimeMin]), []);
*)
    SavePlant := '';
    SaveShift := -1;
(*
    while not Eof and
      (FieldByName('EMPLOYEE_NUMBER').Value = AEmployeeNumber) and
      (FieldByName('EMPLOYEEAVAILABILITY_DATE').Value <= AAvailableDateTimeMax) do
*)
    while not Eof do
    begin
      AvailDateTime := FieldByName('EMPLOYEEAVAILABILITY_DATE').Value;
      DayAvail :=  ListProcsF.DayWStartOnFromDate(AvailDateTime);
      if (SavePlant <> FieldByName('PLANT_CODE').Value) or
        (SaveShift <> FieldByName('SHIFT_NUMBER').Value) then
      begin
        // RV067.MRA.3. 550507. Also look at DEPARTMENT!
        ATBLengthClass.FillTBArray(FieldByName('EMPLOYEE_NUMBER').Value,
          FieldByName('SHIFT_NUMBER').Value,
          FieldByName('PLANT_CODE').Value,
          FieldByName('DEPARTMENT_CODE').Value);
        SavePlant := FieldByName('PLANT_CODE').Value;
        SaveShift := FieldByName('SHIFT_NUMBER').Value;
      end;


      for Block := 1 to SystemDM.MaxTimeblocks do // GLOB3-296
        if FieldByName('AVAILABLE_TIMEBLOCK_' +
          IntToStr(Block)).AsString <> '' then
          if (FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Block)).AsString[1] in ['*']) then
          begin
            Accept :=  ATBLengthClass.ExistTB(Block);
            if Accept then
            begin
              StartTime := ATBLengthClass.GetStartTime(Block, DayAvail);
              EndTime := ATBLengthClass.GetEndTime(Block, DayAvail);
            end;

            Accept := (StartTime  > 0) and (EndTime > 0) and (StartTime <> EndTime);
            if Accept then
            begin
              AvailableFound := True;
              VShiftNumber := FieldByName('SHIFT_NUMBER').Value;
              ReplaceDate(StartTime, AvailDateTime);
              ReplaceDate(EndTime, AvailDateTime);
              VStartTime := StartTime;
              VEndTime := EndTime;
              EmployeeData.EmployeeCode := AEmployeeNumber;
              EmployeeData.PlantCode := APlantCode;
              EmployeeData.WorkSpotCode := '';
              EmployeeData.ShiftNumber := VShiftNumber;
              EmployeeData.DateIn := VStartTime;
              EmployeeData.DateOut := VEndTime;
              EmployeeData.DepartmentCode := '';
              EmployeeData.InscanEarly := ReportCompHoursDM.QueryEmpl.
                FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
              EmployeeData.InscanLate := ReportCompHoursDM.QueryEmpl.
                FieldByName('INSCAN_MARGIN_LATE').AsInteger;
              EmployeeData.OutscanEarly := ReportCompHoursDM.QueryEmpl.
                FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
              EmployeeData.OutscanLate := ReportCompHoursDM.QueryEmpl.
                FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
              EmployeeData.CutOfTime :=
                ReportCompHoursDM.QueryEmpl.FieldByName('CUT_OF_TIME_YN').AsString;
               //CAR 23-7-2003
//              ProdMinClass.GetProdMin(EmployeeData,
//                FEmptyEmplReq, BookingDay,
//                ProdMin, BreaksMin, PayedBreaks);
              // MR:25-09-2003 Function is changed
              AProdMinClass.GetProdMin(EmployeeData,
                True, False, False, EmployeeData, BookingDay,
                ProdMin, BreaksMin, PayedBreaks);

              VAvailableMinutes := VAvailableMinutes + ProdMin + PayedBreaks;
            end;  // if Accept then
          end;  // if FieldByName('SCHEDULED_TIMEBLOCK_' +
        Next;
    end;  // while not Eof
  end;
  Result := AvailableFound;
end;

procedure TReportCompHoursQR.QRBandTitleEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel34.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel17.Caption + ' ' + QRLabel3.Caption + ExportClass.Sep +
      QRLabel6.Caption + ExportClass.Sep +
      QRLabel7.Caption + ExportClass.Sep +
      QRLabel8.Caption + ExportClass.Sep +
      QRLabel9.Caption + ExportClass.Sep +
      QRLabel10.Caption + ExportClass.Sep +
      QRLabel12.Caption + ExportClass.Sep +
      QRLabel14.Caption + ExportClass.Sep +
      QRLabel15.Caption
      );
  end;
end;

procedure TReportCompHoursQR.QRBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // Detail
    with ReportCompHoursDM do
    begin
      ExportClass.AddText(
        QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('DESCRIPTION').AsString + ExportClass.Sep +
        ExportList[1] + ExportClass.Sep +
        ExportList[2] + ExportClass.Sep +
        ExportList[3] + ExportClass.Sep +
        ExportList[4] + ExportClass.Sep +
        ExportList[5] + ExportClass.Sep +
        ExportList[6] + ExportClass.Sep +
        ExportList[7] + ExportClass.Sep +
        ExportList[8] + ExportClass.Sep +
        ExportList[9]
        );
    end;
  end;
end;

procedure TReportCompHoursQR.QRBand1AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if QRParameters.FShowTotal then
    begin
      // Totals
      ExportClass.AddText(
        QRLabel4.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportList[1] + ExportClass.Sep +
        ExportList[2] + ExportClass.Sep +
        ExportList[3] + ExportClass.Sep +
        ExportList[4] + ExportClass.Sep +
        ExportList[5] + ExportClass.Sep +
        ExportList[6] + ExportClass.Sep +
        ExportList[7] + ExportClass.Sep +
        ExportList[8] + ExportClass.Sep +
        ExportList[9]
        );
    end;
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportCompHoursQR.QRGroupTeamHDBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  TeamWKHrs := 0;
  TeamIllness := 0;
  TeamHol := 0;
  TeamPaid := 0;
  TeamPaidTot := 0;
  TeamUnpaid := 0;
  TeamUnpaidTot := 0;
  TeamContract := 0;
  TeamDiff := 0;
end;

procedure TReportCompHoursQR.QRBandTeamGrpFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelTeamWkHours.Caption := DecodeHrsMin(Round(TeamWKHrs));
  QRLabelTeamIllness.Caption := DecodeHrsMin(Round(TeamIllness));
  QRLabelTeamHoliday.Caption := DecodeHrsMin(Round(TeamHol));
  QRLabelTeamPaid.Caption := DecodeHrsMin(Round(TeamPaid));
  QRLabelTeamPaidTot.Caption := DecodeHrsMin(Round(TeamPaidTot));
  QRLabelTeamUnpaid.Caption := DecodeHrsMin(Round(TeamUnpaid));
  QRLabelTeamTotUnpaid.Caption := DecodeHrsMin(Round(TeamUnpaidTot));
  QRLabelTeamContract.Caption := DecodeHrsDouble(TeamContract);
  QRLabelTeamDifference.Caption := DecodeHrsMin(Round(TeamDiff));
end;

procedure TReportCompHoursQR.QRBandTeamGrpFooterAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // Detail
    with ReportCompHoursDM do
    begin
      ExportClass.AddText(
        QRLabel22.Caption + ' ' +
        QueryEmpl.FieldByName('TEAM_CODE').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('TDESCRIPTION').AsString + ExportClass.Sep +
        QRLabelTeamWkHours.Caption + ExportClass.Sep +
        QRLabelTeamIllness.Caption + ExportClass.Sep +
        QRLabelTeamHoliday.Caption + ExportClass.Sep +
        QRLabelTeamPaid.Caption + ExportClass.Sep +
        QRLabelTeamPaidTot.Caption + ExportClass.Sep +
        QRLabelTeamUnpaid.Caption + ExportClass.Sep +
        QRLabelTeamTotUnpaid.Caption + ExportClass.Sep +
        QRLabelTeamContract.Caption + ExportClass.Sep +
        QRLabelTeamDifference.Caption
        );
    end;
  end;
end;

procedure TReportCompHoursQR.QRGroupTeamHDAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    with ReportCompHoursDM do
    begin
      ExportClass.AddText(
        QRLabel21.Caption + ' ' +
        QueryEmpl.FieldByName('TEAM_CODE').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('TDESCRIPTION').AsString);
    end;
  end;
end;

end.
