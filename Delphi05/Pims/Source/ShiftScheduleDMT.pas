(*
  MRA:27-JAN-2009 RV021.
    Team selection correction, to prevent multiple
    the same records.
  MRA:3-DEC-2009 RV047.1.
  - Use constants instead of numbers for result of question-dialog,
    to make more clear what happens.
  - Abort loop when answer is NoToAll during copy.
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  MRA:21-APR-2011. RV089.1.
  - Use other function for DisplayMessage with 4 buttons: Yes, No, All, NoToAll.
  MRA:2-MAY-2013 TD-22429 Future Active Employee (REWORK)
  - Changes for employee who are not active yet (startdate is set in future).
    - These must still be shown. Only employee who are not active anymore
      (enddate in past) should not be shown.
  MRA:8-MAY-2013 TD-22503 Shift Schedule gives copy error
  - When shift schedule is changed (by using copy-function or by doing this
    manual) to 'non-scheduled' (no shift entered), then:
    - Delete any found Employee Planning (ask for confirmation)
    - When deletion of any found employee planning was confirmed,
      or when none was found then:
      - Delete any found Employee Availability (ask for confirmation)
        - When deletion of any found employee availability was confirmed
          or if none was found then:
          - Change the shift schedule itself.
  MRA:14-MAY-2013 TD-22503 Shift Schedule gives copy error
  - When shift for day of shift schedule that must be copied is empty then it
    means the target must also be removed (non-available).
    - Problem: It now gives a message the target shift cannot be changed
      to a different shift.
  MRA:9-FEB-2018 PIM-354
  - Memory leak in planning dialogs
  - The OnDestroy-event was not assigned!
*)
unit ShiftScheduleDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, Dblup1a, ShiftScheduleFRM, Provider, DBClient;

const
  NoToAll: Integer=9;

type
  TArrayTB = Array[1..4] of Integer;

  TShiftScheduleDM = class(TGridBaseDM)
    TableDetailPLANT_CODE: TStringField;
    TableDetailDAY_OF_WEEK: TIntegerField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailAVAILABLE_TIMEBLOCK_1: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_2: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_3: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_4: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    QueryDetail: TQuery;
    QueryDetailD11: TStringField;
    QueryDetailD21: TStringField;
    QueryDetailD31: TStringField;
    QueryDetailD15: TStringField;
    QueryDetailD16: TStringField;
    QueryDetailD17: TStringField;
    QueryDetailEMPLOYEE_NUMBER: TIntegerField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailPLANT: TStringField;
    QueryDetailD12: TStringField;
    QueryDetailD13: TStringField;
    QueryDetailD14: TStringField;
    QueryDetailD22: TStringField;
    QueryDetailD23: TStringField;
    QueryDetailD24: TStringField;
    QueryDetailD32: TStringField;
    QueryDetailD33: TStringField;
    QueryDetailD34: TStringField;
    QueryDetailD25: TStringField;
    QueryDetailD35: TStringField;
    QueryDetailD26: TStringField;
    QueryDetailD36: TStringField;
    QueryDetailD27: TStringField;
    QueryDetailD37: TStringField;
    QueryDetailTEAM: TStringField;
    StoredProcSHS: TStoredProc;
    QueryDetailFLOAT_EMP: TFloatField;
    ClientDataSetShift: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    QueryShift: TQuery;
    QueryTeam: TQuery;
    QueryEmpl: TQuery;
    QueryCheckIsPlanned: TQuery;
    QueryDeletePlanning: TQuery;
    QueryPlantTeam: TQuery;
    QueryPlant: TQuery;
    DataSourceEmpl: TDataSource;
    QueryEmplTo: TQuery;
    DataSourceEmplTo: TDataSource;
    QueryDetailSTARTDATE: TDateTimeField;
    QueryDetailENDDATE: TDateTimeField;
    QueryDetailDEPARTMENT: TStringField;
    QueryDetailP11: TStringField;
    QueryDetailP12: TStringField;
    QueryDetailP13: TStringField;
    QueryDetailP14: TStringField;
    QueryDetailP15: TStringField;
    QueryDetailP16: TStringField;
    QueryDetailP17: TStringField;
    QueryDetailP21: TStringField;
    QueryDetailP22: TStringField;
    QueryDetailP23: TStringField;
    QueryDetailP24: TStringField;
    QueryDetailP25: TStringField;
    QueryDetailP26: TStringField;
    QueryDetailP27: TStringField;
    QueryDetailP31: TStringField;
    QueryDetailP32: TStringField;
    QueryDetailP33: TStringField;
    QueryDetailP34: TStringField;
    QueryDetailP35: TStringField;
    QueryDetailP36: TStringField;
    QueryDetailP37: TStringField;
    qryEmplAvail: TQuery;
    qrySHSFind: TQuery;
    qrySHSDelete: TQuery;
    qrySHSInsert: TQuery;
    qrySHSUpdate: TQuery;
    qryEmplAvailDelete: TQuery;
    procedure QueryDetailAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryDetailCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FPlanInOtherPlants: Boolean;
    procedure UpdateValues(TempTable: TTable);
    procedure DeletePlanning(NewShift: Integer; // TD-22503
      Plant: String; Shift, Employee: Integer;
      DateShift: TDateTime);
    function SHSFind(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime): Boolean;
    procedure SHSDelete(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime);
    procedure SHSInsert(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime;
      APlantCode: String;
      AShiftNumber: Integer);
    procedure SHSUpdate(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime;
      APlantCode: String;
      AShiftNumber: Integer);
    procedure DeleteAvailability(APlantCode: String;
      AShiftNumber, AEmployeeNumber: Integer;
      ADate: TDateTime);
  public
    { Public declarations }
    FEmployee: Integer;
    FSHSArray, FOldSHSArray: Array[1..3, 1..7] of String;
    FPlantSHSArray, FOldPlantSHSArray: Array[1..3, 1..7] of String;
    FAllTeam, FAllPlant: Boolean;
    FTeam, FPlant: String;
    FYear, FWeek: Integer;
    procedure FillQueryDetail(AllTeam, AllPlant: Boolean;
      TeamFrom, TeamTo, Plant: String; Year, Week: Integer);
    procedure UpdateShiftSchedule(Empl, Year, Week: Integer);
    function ValidShift(PlantCode: String; Shift_Nr, Day_Nr: Integer; var
      NewPlantCode: String): Boolean;
    function ValidEmplAvail(NewShift_Number: Integer; // TD-22503
      PlantCode: String;
      Empl, Shift_Number, Year, Week, Day: Integer;
      var PlannedDeleted: Boolean): Boolean;
    procedure CopyFunction({Source, }Dest: TTable; EmplSource, EmplDest: Integer;
      DateSHS, DateCopySHS: TDateTime; var Result: Integer);
    procedure FillQueryEmpl(AllTeam, AllPlant: Boolean;
      TeamFrom, TeamTo, Plant: String;
      FillEmplTo, AllEmpl: Boolean);
    procedure GotoRecord(Empl:Integer);
    procedure FillOldArraySHS;
    function DefaultPlant(const APlantCode: String): String;
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write FPlanInOtherPlants;
  end;

var
  ShiftScheduleDM: TShiftScheduleDM;

implementation

{$R *.DFM}

uses
  SystemDMT, CalculateTotalHoursDMT, ListProcsFRM, UPimsMessageRes,
  UPimsConst;

procedure TShiftScheduleDM.FillOldArraySHS;
var
  Nr, Day: Integer;
begin
  for Nr := 1 to 3 do
    for Day := 1 to 7 do
    begin
      FOldSHSArray[Nr, Day] :=
        QueryDetail.FieldByName(Format('D%d%d', [Nr, Day])).AsString;
      FOldPlantSHSArray[Nr, Day] :=
        QueryDetail.FieldByName(Format('P%d%d', [Nr, Day])).AsString;
    end;
end;

procedure TShiftScheduleDM.FillQueryDetail(AllTeam, AllPlant: Boolean;
  TeamFrom, TeamTo, Plant: String; Year, Week: Integer);
begin
  QueryDetail.Active := False;
  QueryDetail.UniDirectional := False;
  QueryDetail.SQL.Clear;
  // Pims -> Oracle
//    QueryDetailRECNO.Free;
  QueryDetail.SQL.Add(
    'SELECT ' + NL +
    '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
    '  E.PLANT_CODE AS PLANT, E.TEAM_CODE AS TEAM, ' + NL +
    '  E.STARTDATE, E.ENDDATE, ' + NL +
    '   ''NULL'' AS DEPARTMENT, ' + NL +
//    '  CAST(MAX(ROWNUM) AS NUMBER(10)) AS RECNO2, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE    = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D11, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-1  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D12, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-2  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D13, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-3  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D14, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-4  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D15, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-5  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D16, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-6  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D17, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-7  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D21, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-8  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D22, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-9  = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D23, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-10 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D24, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-11 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D25, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-12 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D26, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-13 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D27, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-14 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D31, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-15 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D32, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-16 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D33, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-17 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D34, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-18 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D35, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-19 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D36, ' + NL +
    '  TO_CHAR(MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-20 = :D THEN REPLACE(TO_CHAR(S.SHIFT_NUMBER),''-1'',''-'') ELSE NULL END)) D37, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE    = :D THEN S.PLANT_CODE   ELSE NULL END) P11, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-1  = :D THEN S.PLANT_CODE   ELSE NULL END) P12, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-2  = :D THEN S.PLANT_CODE   ELSE NULL END) P13, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-3  = :D THEN S.PLANT_CODE   ELSE NULL END) P14, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-4  = :D THEN S.PLANT_CODE   ELSE NULL END) P15, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-5  = :D THEN S.PLANT_CODE   ELSE NULL END) P16, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-6  = :D THEN S.PLANT_CODE   ELSE NULL END) P17, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-7  = :D THEN S.PLANT_CODE   ELSE NULL END) P21, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-8  = :D THEN S.PLANT_CODE   ELSE NULL END) P22, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-9  = :D THEN S.PLANT_CODE   ELSE NULL END) P23, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-10 = :D THEN S.PLANT_CODE   ELSE NULL END) P24, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-11 = :D THEN S.PLANT_CODE   ELSE NULL END) P25, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-12 = :D THEN S.PLANT_CODE   ELSE NULL END) P26, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-13 = :D THEN S.PLANT_CODE   ELSE NULL END) P27, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-14 = :D THEN S.PLANT_CODE   ELSE NULL END) P31, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-15 = :D THEN S.PLANT_CODE   ELSE NULL END) P32, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-16 = :D THEN S.PLANT_CODE   ELSE NULL END) P33, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-17 = :D THEN S.PLANT_CODE   ELSE NULL END) P34, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-18 = :D THEN S.PLANT_CODE   ELSE NULL END) P35, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-19 = :D THEN S.PLANT_CODE   ELSE NULL END) P36, ' + NL +
    '  MAX(CASE WHEN S.SHIFT_SCHEDULE_DATE-20 = :D THEN S.PLANT_CODE   ELSE NULL END) P37 ' + NL +
    'FROM ' +
    '  EMPLOYEE E LEFT OUTER JOIN SHIFTSCHEDULE S ON ' + NL +
    '    E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER AND ' + NL +
    '    S.SHIFT_SCHEDULE_DATE >= :D AND ' + NL +
    '    (S.SHIFT_SCHEDULE_DATE - 21 <= :D) ' + NL +
{
    '  LEFT OUTER JOIN TEAMPERUSER TPU ON ' + NL +
    '    E.TEAM_CODE = TPU.TEAM_CODE ' + NL +
}
    'WHERE ' + NL +
// MR:24-02-2006 Order 550417
    '   ( ' + NL +
{    '      (E.STARTDATE <= :DATEMAX) AND ' + NL + } // TD-22429
    '      ( (E.ENDDATE >= :DATEMIN) OR (E.ENDDATE IS NULL) ) ' + NL +
    '   ) AND ' + NL +
    '  ((:PLANT_CODE = ''*'') OR (E.PLANT_CODE = :PLANT_CODE)) AND ' + NL +
// MR:13-02-2006 Order 550418
    '  ( ' + NL +
    '    ( ' + NL +
    '       (:USER_NAME <> ''*'') AND  ' + NL +
    '       E.TEAM_CODE IN  ' + NL +
    '         ( ' + NL +
    '           SELECT T.TEAM_CODE  ' + NL +
    '           FROM TEAMPERUSER T ' + NL +
    '           WHERE  ' + NL +
    '             T.USER_NAME = :USER_NAME AND ' + NL +
    '             ( ' + NL +
    '               (:TEAMFROM = ''*'') OR  ' + NL +
    '               (T.TEAM_CODE >= :TEAMFROM AND ' + NL +
    '                T.TEAM_CODE <= :TEAMTO) ' + NL +
    '             ) ' + NL +
    '         ) ' + NL +
    '     ) ' + NL +
    '     OR ' + NL +
    '     ( ' + NL +
    '        (:USER_NAME = ''*'') AND ' + NL +
    '        (  ' + NL +
    '          (:TEAMFROM = ''*'') OR ' + NL +
    '          (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO)  ' + NL +
    '        ) ' + NL +
    '    ) ' + NL +
    '  ) ' + NL +
{
    '  (' + NL +
    '    ((:TEAMFROM = ''*'') OR ' + NL +
    '     (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO)) AND ' + NL +
    '    ((:USER_NAME = ''*'') OR (TPU.USER_NAME = :USER_NAME)) ' + NL +
    '  ) ' + NL +
}
    'GROUP BY ' + NL +
    '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
    '  E.PLANT_CODE, E.TEAM_CODE, E.STARTDATE, E.ENDDATE, ''NULL''' + ' ' + NL +
    'ORDER BY 1');
  QueryDetail.ParamByName('D').AsDateTime :=
    ListProcsF.DateFromWeek(Year, Week, 1);
  if AllTeam then
  begin
    QueryDetail.ParamByName('TEAMFROM').AsString := '*';
    QueryDetail.ParamByName('TEAMTO').AsString := '*';
  end
  else
  begin
    QueryDetail.ParamByName('TEAMFROM').AsString := TeamFrom;
    QueryDetail.ParamByName('TEAMTO').AsString := TeamTo;
  end;
  if AllPlant then
    QueryDetail.ParamByName('PLANT_CODE').AsString := '*'
  else
    QueryDetail.ParamByName('PLANT_CODE').AsString := Plant;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryDetail.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryDetail.ParamByName('USER_NAME').AsString := '*';
  QueryDetail.ParamByName('DATEMIN').AsDateTime :=
    ListProcsF.DateFromWeek(Year, Week, 1);
{  QueryDetail.ParamByName('DATEMAX').AsDateTime :=
    ListProcsF.DateFromWeek(Year, Week + 2, 7); } // TD-22429
// QueryDetail.SQL.SaveToFile('c:\temp\shiftschedule.sql');
  if not QueryDetail.Prepared then
    QueryDetail.Prepare;
  QueryDetail.Active := True;
  QueryDetail.First;
end;

procedure TShiftScheduleDM.GotoRecord(Empl:Integer);
begin
  QueryDetail.First;
  while not QueryDetail.Eof do
  begin
    if (QueryDetail.FieldByName('EMPLOYEE_NUMBER').asInteger = Empl) then
      Exit;
    QueryDetail.Next;
  end;
end;

procedure TShiftScheduleDM.FillQueryEmpl(AllTeam, AllPlant: Boolean;
  TeamFrom, TeamTo, Plant: String;
  FillEmplTo, AllEmpl: Boolean);
var
  SelectStr: String;
begin
//car 2-4-2004
  SelectStr :=
    'SELECT ' +
    '  E.PLANT_CODE, E.EMPLOYEE_NUMBER, '+
    '  E.EMPLOYEE_NUMBER || '' | '' || E.DESCRIPTION AS VALUEDISPLAY, ' +
    '  E.DESCRIPTION, E.SHORT_NAME, E.DEPARTMENT_CODE, E.TEAM_CODE, E.ADDRESS ' +
    'FROM ' +
(* RV021.
    '  EMPLOYEE E LEFT JOIN TEAMPERUSER TPU ON ' +
    '    E.TEAM_CODE = TPU.TEAM_CODE ' +
*)
    '  EMPLOYEE E ' +
    'WHERE ' +
(* RV021.
    '  ((:USER_NAME = ''*'') OR (TPU.USER_NAME = :USER_NAME)) ';
*)
    '  ( ' +
    '    (:USER_NAME = ''*'') OR ' +
    '    (E.TEAM_CODE IN ' +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
    '  ) ';
  // MR:17-02-2005 Order 550378 Team-From-To.
  if (not AllTeam) and (TeamFrom <> '') then
    SelectStr := SelectStr + ' AND ' +
      ' ((E.TEAM_CODE >= ''' + DoubleQuote(TeamFrom) + ''') AND ' +
      '  (E.TEAM_CODE <= ''' + DoubleQuote(TeamTo) + ''')) ';

  if (not AllPlant) and (Plant <> '')  then
    SelectStr := SelectStr + ' AND ' +
      ' (E.PLANT_CODE = ''' + DoubleQuote(Plant) + ''') ';

  // MR:23-07-2004 Order 550323
  // Get all customers (also not-active) or not.
  if not AllEmpl then
    SelectStr := SelectStr + ' AND ' +
{      ' (E.STARTDATE <= :FDATE) AND ' + } // TD-22429
      ' ((E.ENDDATE >= :FDATE) OR (E.ENDDATE IS NULL)) ';

  SelectStr := SelectStr + ' ORDER BY E.EMPLOYEE_NUMBER';
  QueryEmpl.Active := False;
  QueryEmpl.UniDirectional := False;
  QueryEmpl.SQL.Clear;
  QueryEmpl.SQL.Add(UpperCase(SelectStr));

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmpl.ParamByName('USER_NAME').AsString := '*';
  if not AllEmpl then
    QueryEmpl.ParamByName('FDATE').AsDateTime := Now;
  if not QueryEmpl.Prepared then
     QueryEmpl.Prepare;
  QueryEmpl.Active := True;

  QueryEmplTo.Active := False;
  QueryEmplTo.UniDirectional := False;
  QueryEmplTo.SQL.Clear;
  QueryEmplTo.SQL.Add(UpperCase(SelectStr));
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmplTo.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmplTo.ParamByName('USER_NAME').AsString := '*';
  if not AllEmpl then
    QueryEmplTo.ParamByName('FDATE').Value := Now;
  if not QueryEmplTo.Prepared then
     QueryEmplTo.Prepare;
  QueryEmplTo.Active := True;
end;

procedure TShiftScheduleDM.QueryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  // during the refresh of sort is done don't fill the detail panel
  if ShiftScheduleF.FSort then
    Exit;
  ShiftScheduleF.FillEditDetail(True);
end;

procedure TShiftScheduleDM.UpdateValues(TempTable: TTable);
var
  CurrentDate: TDateTime;
begin
  CurrentDate := Now;
  TempTable.FieldByName('CREATIONDATE').Value := CurrentDate;
  TempTable.FieldByName('MUTATIONDATE').Value := CurrentDate;
  TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
end;

function TShiftScheduleDM.ValidShift(PlantCode: String;
  Shift_Nr, Day_Nr: Integer; var NewPlantCode: String): Boolean;
var
  Found: Boolean;
begin
  Result := False;
  NewPlantCode := PlantCode;
  if Shift_Nr < 0 then
    Exit;
  Found := ClientDataSetShift.FindKey([PlantCode, Shift_Nr]);
  // If not found, look in plant that belongs to the selection.
  if not Found then
  begin
    Found := ClientDataSetShift.FindKey([
      QueryDetail.FieldByName('PLANT').AsString, Shift_Nr]);
    if Found then
      NewPlantCode := QueryDetail.FieldByName('PLANT').AsString;
  end;
  // If shift was not found, look if it's a unique shift from
  // another plant. Only for 'PlanInOtherPlants'.
  // MR:23-11-2004 Do NOT look for a unique shift.
(*
  if (not Found) and PlanInOtherPlants then
  begin
    ClientDataSetShift.Filtered := False;
    ClientDataSetShift.Filter := 'SHIFT_NUMBER=' + IntToStr(Shift_Nr);
    ClientDataSetShift.Filtered := True;
    if ClientDataSetShift.RecordCount = 1 then
    begin
      NewPlantCode := ClientDataSetShift.FieldByName('PLANT_CODE').AsString;
      Found := True;
    end;
  end;
*)
  if Found then
  begin
    if (ClientDataSetShift.FieldByName(
        Format('STARTTIME%d', [Day_Nr])).AsDateTime <> 0) or
      (ClientDataSetShift.FieldByName(
       Format('ENDTIME%d', [Day_Nr])).AsDateTime <> 0) then
      Result := True;
  end;
//  ClientDataSetShift.Filtered := False;
end;

// MR:24-03-2005 Order 550392
// Add 'plantcode' to parameters, because of 'planinotherplants',
// the plant can be different from the employee-plant.
function TShiftScheduleDM.ValidEmplAvail(
  NewShift_Number: Integer; // TD-22503
  PlantCode: String;
  Empl, Shift_Number, Year,
  Week, Day: Integer;
  var PlannedDeleted: Boolean): Boolean;
var
  DateEmpl: TDateTime;
  DialogResult: Integer;
  EmpAvailFound: Boolean; // TD-22503
  PlannedFound: Boolean; // TD-22503
begin
  Result := False;
  PlannedDeleted := False;
  PlannedFound := False;
  if Shift_Number < 0 then
    Exit;
  DateEmpl := ListProcsF.DateFromWeek(Year, Week , Day);
  // MR:20-01-2006 Use Query instead of Table for Optimising.
  qryEmplAvail.Close;
  qryEmplAvail.ParamByName('PLANT_CODE').AsString := PlantCode;
  qryEmplAvail.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEmpl;
  qryEmplAvail.ParamByName('SHIFT_NUMBER').AsInteger := Shift_Number;
  qryEmplAvail.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
  qryEmplAvail.Open;
  Result := (qryEmplAvail.FieldByName('RECCOUNT').AsInteger > 0);
  EmpAvailFound := Result; // TD-22503
  qryEmplAvail.Close;

//  if not Result then // TD-22503 Always look for employee planning
    if (Shift_Number = 0) or
      (NewShift_Number = -1) // TD-22503 When making non-available
      then
    begin
      QueryCheckIsPlanned.Active := False;
      QueryCheckIsPlanned.ParamByName('PCODE').asString :=
        PlantCode {QueryDetail.FieldByName('PLANT').AsString};
      QueryCheckIsPlanned.ParamByName('PDATE').asDateTime := DateEmpl;
      QueryCheckIsPlanned.ParamByName('EMPNO').asInteger := Empl;
      QueryCheckIsPlanned.Active := True;
      if not QueryCheckIsPlanned.IsEmpty then
      begin
        PlannedFound := True;
        PlannedDeleted := True;
        DialogResult := DisplayMessage(SChangePlaned, mtInformation,
          [mbYes, mbNo]);
        if DialogResult = mrNo then
          Result := True;
        if DialogResult = mrYes then
          Result := False;
      end;
    end;

  // TD-22503 When making non-available then delete availability
  if (PlannedDeleted and (not Result)) or ((not PlannedFound) and Result) then
  begin
    if EmpAvailFound and (NewShift_Number = -1) then
    begin
      if DisplayMessage(SChangeEmplAvail, mtInformation,
        [mbYes, mbNo]) = mrYes then
      begin
        DeleteAvailability(PlantCode, Shift_Number, Empl, DateEmpl);
        Result := False; // Any found planning/avail. can be deleted.
      end
      else
        Result := True; // Any found planning/avail. should not be deleted.
    end;
  end;
end;

procedure TShiftScheduleDM.DeletePlanning(NewShift: Integer; // TD-22503
  Plant: String; Shift, Employee: Integer;
  DateShift: TDateTime);
begin
  // TD-22503 Use other criterium, only delete planning when made non-available
  if {Shift <> 0} NewShift <> -1 then
    Exit;
  QueryCheckIsPlanned.Active := False;
  QueryCheckIsPlanned.ParamByName('PCODE').asString := Plant;
  QueryCheckIsPlanned.ParamByName('PDATE').asDateTime := DateShift;
  QueryCheckIsPlanned.ParamByName('EMPNO').asInteger := Employee;
  QueryCheckIsPlanned.Active := True;
  if not QueryCheckIsPlanned.IsEmpty then
  begin
    QueryDeletePlanning.Close;
    QueryDeletePlanning.ParamByName('PCODE').asString := Plant;
    QueryDeletePlanning.ParamByName('PDATE').asDateTime := DateShift;
    QueryDeletePlanning.ParamByName('EMPNO').asInteger := Employee;
    QueryDeletePlanning.ExecSQL;
  end;
end;

// MR:20-1-2006 BEGIN Use query for finding/updating SHS (optimising)
function TShiftScheduleDM.SHSFind(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime): Boolean;
begin
  with qrySHSFind do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    Open;
    Result := not IsEmpty;
  end;
end;

procedure TShiftScheduleDM.SHSDelete(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime);
begin
  with qrySHSDelete do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ExecSQL;
  end;
end;

procedure TShiftScheduleDM.SHSInsert(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime;
  APlantCode: String;
  AShiftNumber: Integer);
begin
  with qrySHSInsert do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    ParamByName('CREATIONDATE').AsDateTime := Now;
    ParamByName('MUTATIONDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end;

procedure TShiftScheduleDM.SHSUpdate(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime;
  APlantCode: String;
  AShiftNumber: Integer);
begin
  with qrySHSUpdate do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    ParamByName('MUTATIONDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end;

procedure TShiftScheduleDM.UpdateShiftSchedule(Empl, Year, Week: Integer);
var
  Index, Day: Integer;
  SamePlant: Boolean;
  EmplShiftDate: TDateTime;
  function NewShiftNumber: Integer;
  begin
    if FSHSArray[Index,Day] = '-' then
      Result := -1
    else
      try
        Result := StrToInt(FSHSArray[Index,Day]);
      except
        Result := 1;
      end;
  end;
  function NewPlantCode: String;
  begin
    if PlanInOtherPlants then
      Result := DefaultPlant(FPlantSHSArray[Index,Day])
    else
      Result := QueryDetail.FieldByName('PLANT').AsString;
  end;
begin
  inherited;
  for Index := 1 to 3 do
    for Day := 1 to 7 do
    begin
      EmplShiftDate := ListProcsF.DateFromWeek(Year, Week + Index - 1, Day);
      if SHSFind(Empl, EmplShiftDate) then
      begin
        SamePlant := True;
        if PlanInOtherPlants then
          SamePlant :=
            (FPlantSHSArray[Index,Day] =
              qrySHSFind.FieldByName('PLANT_CODE').AsString);
        if (FSHSArray[Index,Day] <>
          qrySHSFind.FieldByName('SHIFT_NUMBER').AsString) or
          (not SamePlant) then
          DeletePlanning(
            NewShiftNumber, // TD-22503
            qrySHSFind.FieldByName('PLANT_CODE').AsString,
            qrySHSFind.FieldByName('SHIFT_NUMBER').AsInteger,
            qrySHSFind.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            qrySHSFind.FieldByName('SHIFT_SCHEDULE_DATE').AsDateTime);
        if (FSHSArray[Index,Day] = '') then
          SHSDelete(Empl, EmplShiftDate)
        else
          if (qrySHSFind.FieldByName('SHIFT_NUMBER').AsString <>
            FSHSArray[Index,Day]) or (not SamePlant) then
            SHSUpdate(Empl, EmplShiftDate, NewPlantCode, NewShiftNumber);
      end
      else
        if (FSHSArray[Index,Day] <> '') then
          SHSInsert(Empl, EmplShiftDate, NewPlantCode, NewShiftNumber);
    end; // for
end;

{
procedure TShiftScheduleDM.UpdateShiftSchedule(Empl, Year, Week: Integer);
var
  index, Day: Integer;
  SamePlant: Boolean;
begin
  inherited;
  if not TableSHS.Active then
    TableSHS.Open;
  for Index := 1 to 3 do
    for Day := 1 to 7 do
    begin
      if TableSHS.FindKey([Empl,
        ListProcsF.DateFromWeek(Year, Week + Index - 1, Day)]) then
      begin
        SamePlant := True;
        if PlanInOtherPlants then
          SamePlant :=
            (FPlantSHSArray[Index,Day] =
              TableSHS.FieldByName('PLANT_CODE').AsString);
        if (FSHSArray[Index,Day] <>
          IntToStr(TableSHS.FieldByName('SHIFT_NUMBER').AsInteger)) or
          (not SamePlant) then
          DeletePlanning(TableSHS.FieldByName('PLANT_CODE').AsString,
            TableSHS.FieldByName('SHIFT_NUMBER').AsInteger,
            TableSHS.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            TableSHS.FieldByName('SHIFT_SCHEDULE_DATE').AsDateTime);
//CAR 550276
        if FSHSArray[Index,Day] = '' then
          TableSHS.Delete
        else
          if (TableSHS.FieldByName('SHIFT_NUMBER').AsString <>
            FSHSArray[Index,Day]) or (not SamePlant) then
          begin
            TableSHS.Edit;
            //CAR 550276
            if FSHSArray[Index,Day] = '-' then
              TableSHS.FieldByName('SHIFT_NUMBER').Value := -1
            else
              TableSHS.FieldByName('SHIFT_NUMBER').Value :=
                FSHSArray[Index,Day];
            if PlanInOtherPlants then
              TableSHS.FieldByName('PLANT_CODE').Value :=
                DefaultPlant(FPlantSHSArray[Index,Day])
            else
              TableSHS.FieldByName('PLANT_CODE').Value :=
                QueryDetail.FieldByName('PLANT').AsString;
            UpdateValues(TableSHS);
            TableSHS.Post;
          end;
      end
      else
        if FSHSArray[Index,Day] <> '' then
        begin
          TableSHS.Insert;
          TableSHS.FieldByName('EMPLOYEE_NUMBER').Value := Empl;
          TableSHS.FieldByName('SHIFT_SCHEDULE_DATE').Value :=
            ListProcsF.DateFromWeek(Year, Week + Index - 1, Day);
            //CAR 550276
            if FSHSArray[Index,Day] = '-' then
              TableSHS.FieldByName('SHIFT_NUMBER').Value := -1
            else
              TableSHS.FieldByName('SHIFT_NUMBER').Value :=
                FSHSArray[Index,Day];
          //TableSHS.FieldByName('SHIFT_NUMBER').Value := FSHSArray[Index,Day];
          if PlanInOtherPlants then
            TableSHS.FieldByName('PLANT_CODE').Value :=
              DefaultPlant(FPlantSHSArray[Index,Day])
          else
            TableSHS.FieldByName('PLANT_CODE').Value :=
              QueryDetail.FieldByName('PLANT').AsString;
          UpdateValues(TableSHS);
          TableSHS.Post;
        end;
    end; //FOR
end;
}
// MR:22-03-2005 Order 550392
// Also keep track of 'oldplantcode' and assign the 'newplantcode'
// during copy.
// MR:20-1-2006 Disable 'Source' that relates to 'TableSHS',
// because Queryies are used for optimisation.
// MRA:3-DEC-2009 RV047.1.
// Use constants instead of numbers for result of question-dialog,
// to make more clear what happens. Also, abort loop when answer
// is NoToAll.
procedure TShiftScheduleDM.CopyFunction({Source, }Dest: TTable;
  EmplSource, EmplDest: Integer;
  DateSHS, DateCopySHS: TDateTime; var Result: Integer);
var
  OldPlantCode, NewPlantCode: String;
  OldShiftNr, NewShiftNr: Integer;
  Year, Week, Day: Word;
  OK: Boolean;
//  PlantCode: String;
  PlannedDeleted: Boolean;
  NewPlantCodeDummy: String;
begin
//  if Source.FindKey([EmplSource, DateSHS]) then
//  NewShiftNr := 0; // TD-22503
  // This (-1) indicates there is no shift filled-in for source
  NewShiftNr := -1; // TD-22503
  // When there is no shift filled in for source (-1), then it will find no
  // shift schedule as source (not available: no record).
  if SHSFind(EmplSource, DateSHS) then
  begin
    NewShiftNr := qrySHSFind.FieldByName('SHIFT_NUMBER').AsInteger;
    NewPlantCode := qrySHSFind.FieldByName('PLANT_CODE').AsString;
    if (Dest.FindKey([EmplDest, DateCopySHS])) then
    begin
      if (Result <> mrAll) and (Result <> NoToAll) then
// RV089.1. Use other function!
{         Result := DisplayMessage(SCopySelectionShiftSchedule_1 +
           IntToStr(EmplDest) + ' ,' +
           DateToStr(DateCopySHS) + SCopySelectionShiftSchedule_2,
           mtConfirmation, [mbYes, mbNo, mbAll, mbNoToAll]); }
         Result := DisplayMessage4Buttons(SCopySelectionShiftSchedule_1 +
           IntToStr(EmplDest) + ' ,' +
           DateToStr(DateCopySHS) + SCopySelectionShiftSchedule_2);
      if (Result = mrAll) or (Result = mrYes) then
      begin
        //copy - yes or all
        OK := True;
        OldPlantCode := Dest.FieldByName('PLANT_CODE').AsString;
        OldShiftNr := Dest.FieldByName('SHIFT_NUMBER').AsInteger;
        ListProcsF.WeekUitDat(DateCopySHS, Year, Week);
        Day := ListProcsF.DayWStartOnFromDate(DateCopySHS);
        if (NewShiftNr <> OldShiftNr) or (NewPlantCode <> OldPlantCode) then
        begin
          if (NewShiftNr <> -1) then
            if not ValidShift(NewPlantCode, NewShiftNr,
              Day, NewPlantCodeDummy) then
            begin
              DisplayMessage(SInvalidShiftSchedule, mtInformation, [mbOk]);
              OK := False;
            end;
          if ValidEmplAvail(NewShiftNr, // TD-22503
            OldPlantCode, EmplDest, OldShiftNr, Year, Week, Day,
            PlannedDeleted) then
          begin
            // TD-22503 When 'NewShifNr <> -1' it means the
            //          shift schedule was changed to a different shift, but
            //          there was still availability.
//          if not PlannedDeleted then
            if (NewShiftNr <> -1) then // TD-22503
            begin
              DisplayMessage(SEmployeeAvail_0 + IntToStr(EmplDest) +
                SEmployeeAvail_1 + IntToStr(Year) + ', ' +
                IntToStr(Week ) + ', '+
                SystemDM.GetDayWCode(Day) + SEmployeeAvail_3,
                mtInformation, [mbOk]);
              OK := False;
            end
            else
              OK := True;
          end;
          if OK then
          begin
            if PlannedDeleted then
              DeletePlanning(
                NewShiftNr, // TD-22503
                Dest.FieldByName('PLANT_CODE').AsString,
                Dest.FieldByName('SHIFT_NUMBER').AsInteger,
                Dest.FieldByName('EMPLOYEE_NUMBER').AsInteger,
                Dest.FieldByName('SHIFT_SCHEDULE_DATE').AsDateTime);
            Dest.Edit;
            Dest.FieldByName('PLANT_CODE').AsString := NewPlantCode;
            Dest.FieldByName('SHIFT_NUMBER').AsInteger := NewShiftNr;
            UpdateValues(Dest);
            Dest.Post;
          end;
        end;
      end;// if (Result = mrAll) or (Result = mrYes) then
    end // if (Dest.FindKey([EmplDest, DateCopySHS])) then
    else
    begin  // if dest record does not exist
//car 550276
   //   if NewShiftNr <> -1 then
    // begin
       Dest.Insert;
       Dest.FieldByName('EMPLOYEE_NUMBER').Value := EmplDest;
       Dest.FieldByName('SHIFT_SCHEDULE_DATE').Value :=  DateCopySHS;
       Dest.FieldByName('SHIFT_NUMBER').Value := NewShiftNr;
       Dest.FieldByName('PLANT_CODE').Value := NewPlantCode;
       UpdateValues(Dest);
       Dest.Post;
    //  end;
    end;
  end //if Source.FindKey([EmplSource, DateSHS]) then
  else
  begin // if empl source does not exist, meaning non-available.
    if (Dest.FindKey([EmplDest, DateCopySHS])) then
    begin
      // delete the dest shift only if overwrite is 'Y' or 'YesAll'
      if Result = 0 then
        Result := DisplayMessage(SCopySelectionShiftSchedule_1 +
          IntToStr(EmplDest) + ' ,' +
          DateToStr(DateCopySHS) + SCopySelectionShiftSchedule_2,
          mtConfirmation, [mbYes, mbNo, mbAll, mbNoToAll]);
      if (Result <> NoToAll) and (Result <> mrNo) then
      begin
        OldShiftNr := Dest.FieldByName('SHIFT_NUMBER').AsInteger;
        OldPlantCode := Dest.FieldByName('PLANT_CODE').AsString;
        ListProcsF.WeekUitDat(DateCopySHS, Year, Week);
        Day := ListProcsF.DayWStartOnFromDate(DateCopySHS);
        // Test for employee availability-record
        if ValidEmplAvail(NewShiftNr, // TD-22503
          OldPlantCode, EmplDest, OldShiftNr, Year, Week, Day,
          PlannedDeleted) then
        begin
          // TD-22503 When 'NewShifNr <> -1' it means the
          //          shift schedule was changed to a different shift, but
          //          there was still availability.
//          if not PlannedDeleted then
          if (NewShiftNr <> -1) then // TD-22503
            DisplayMessage( SEmployeeAvail_0 + IntToStr(EmplDest) +
              SEmployeeAvail_1 +  IntToStr(Year ) + ', ' +
              IntToStr(Week ) + ', '+
              SystemDM.GetDayWCode(Day) + SEmployeeAvail_3,
              mtInformation, [mbOk]);
        end
        else
        begin
          if PlannedDeleted then
            DeletePlanning(
              NewShiftNr, // TD-22503
              Dest.FieldByName('PLANT_CODE').AsString,
              Dest.FieldByName('SHIFT_NUMBER').AsInteger,
              Dest.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              Dest.FieldByName('SHIFT_SCHEDULE_DATE').AsDateTime);
          Dest.Delete;
        end;
      end;
    end;
  end;
end;

procedure TShiftScheduleDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := ShiftScheduleDM.DataModuleDestroy;
  FEmployee := 0;
  //car 550274 -team selection
  QueryTeam.SQL.Clear;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
  //car 26-3-2004
    QueryTeam.Sql.Add(
      'SELECT ' +
      '  T.TEAM_CODE, T.DESCRIPTION  ' +
      'FROM ' +
      '  TEAM T, TEAMPERUSER U ' +
      'WHERE ' +
      '  U.USER_NAME = :USER_NAME AND ' +
      '  T.TEAM_CODE = U.TEAM_CODE ' +
      'ORDER BY ' +
      '  T.TEAM_CODE');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end
  else
  begin
    QueryTeam.Sql.Add(
      'SELECT ' +
      '  TEAM_CODE, DESCRIPTION ' +
      'FROM ' +
      '  TEAM  ' +
      'ORDER BY ' +
      '  TEAM_CODE');
  end;
  QueryTeam.Prepare;

  // Pims -> Oracle
//  QueryDetailRECNO2.Free;

//  QueryDetail.Prepare;
  // is used to check if Shift is valid - read table Shift
  ClientDataSetShift.Open;

  // MR:20-1-2006 Use Query instead of Table.
//  TableEmplAvail.Open;
  // MR:20-1-2006 Open table at a later time, when it is really needed.
//  TableSHS.Open;
  QueryCheckIsPlanned.Prepare;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlant);
  QueryPlant.Open;
end;

procedure TShiftScheduleDM.QueryDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  //used for export big employee number
  DataSet.FieldByName('FLOAT_EMP').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TShiftScheduleDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  ClientDataSetShift.Close;

  QueryCheckIsPlanned.Close;
  QueryCheckIsPlanned.UnPrepare;
  //CAR 550274
  QueryDetail.Close;
  QueryDetail.UnPrepare;
  //Car 2-1-2004
  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
  QueryEmplTo.Close;
  QueryEmplTo.UnPrepare;
end;

// MR:09-03-2005 Be sure the plant is not empty, otherwise
// it is stored in 'shiftschedule'.
function TShiftScheduleDM.DefaultPlant(const APlantCode: String): String;
begin
  Result := APlantCode;
  if APlantCode = '' then
    Result := QueryDetail.FieldByName('PLANT').AsString;
end;

// RV050.8.
procedure TShiftScheduleDM.QueryPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// TD-22503 Delete Employee Availability when the shift of shift schedule is
//          set to '-' (non-available)
procedure TShiftScheduleDM.DeleteAvailability(APlantCode: String;
  AShiftNumber, AEmployeeNumber: Integer; ADate: TDateTime);
begin
  with qryEmplAvailDelete do
  begin
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := ADate;
    ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ExecSQL;
  end;
end; // DeleteAvailability

end.

