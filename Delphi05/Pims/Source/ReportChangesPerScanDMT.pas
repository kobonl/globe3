(*
  MRA:12-MAR-2013 New report Changes per Scan
  - This report shows information based on a table that stores logging-info
    during changes made to scans.
*)
unit ReportChangesPerScanDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportChangesPerScanDM = class(TReportBaseDM)
    QueryChangesPerScan: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportChangesPerScanDM: TReportChangesPerScanDM;

implementation

uses
  SystemDMT;

{$R *.DFM}

end.
