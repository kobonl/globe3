(*
  Changes:
    MRA:12 FEB 2008, REV003:
      Added Shift_Number to Employee-component (selected employee).
      Together with Plant_Code from this component, this
      must be used when saving AbsenceHours.
      This to prevent AbsenceHours-records are stored with
      wrong plant+shift.
    SO: 07-JUN-2010 REV065.2
      added first, prev, next, last employee buttons
    SO: 07-JUN-2010 REV065.5
      added the possibility to manually enter an hour type
    SO: 09-JUN-2010 REV065.9
    - the salary hours created from manually entered production hours are shown in
    different color and read only
    SO:08-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
    - new holiday counters
    SO:27-AUG-2010 RV067.10. 550509
    Remove the Refresh-button and retrieve the needed data when:
    x	The dialog is opened and shown
    x	Another employee is selected from employee-drop-down-box, or by using the navigation-buttons
    x	Another year is selected from year-field
    x	Another week is selected from week-field
    MRA:6-SEP-2010 RV067.MRA.29 Changes for 550497
    - Some counters are always visible. This must be determined
      by AbsenceTypesPerCountry.
    - When switching employees, the counter-groupbox-captions can be wrong.
      Store the original captions and use these as defaults.
    MRA:4-OCT-2010 RV071.3. Changes for 550497
    - Bugfix. When changing a norm, the action for Save/Cancel was not
      always correct.
    - When changing the 'Norm Bank Holiday Reserve Minute'-field,
      then store this in a flag and use a different color to indicate
      it has been manually changed.
    MRA:6-OCT-2010 RV071.7. 550497
    - Sequence-problem when showing holiday counters: Solved by
      first making invisible of all groupboxes in reverse order.
    MRA:8-OCT-2010 RV071.10. 550497 Additions for Saturday Credit.
    - Addition of Sat. Credit handling.
    MRA:15-OCT-2010 RV071.16. Bugfixing.
    - When entering nothing in Hourtype-field, then
      it gives an error when openening Hourtype-combobox.
  MRA:22-OCT-2010 RV073.7. Bugfix for REV065.9. 550487. (CLF)
  - Manual prod hours bug.
    When adding manual prod. hours, it creates salary hours. These are
    indicated as 'from prod hours'.
    When adding negative manual prod. hours this gives a bug when these
    are deleted: The manual salary hours are kept!
    Problems:
    - Because these are flagged as 'from prod. hours'
      they cannot removed anymore!
    - You also cannot add a manual sal. record to
      correct the hours, because this is not accepted!
  MRA:2-NOV-2010 RV076.1. RV077.1. Tailor made non-scanners. RFL.
  - Changes only for RFL:
    - Hours per Employee
      - For scanners:
        - Disable edit on salary hours
        - Only edit on production hours is allowed.
      - For non-scanners + book prod. hrs:
        - Disable edit on production hours.
        - Only edit on salary hours is allowed.
  MRA:12-NOV-2010 RV079.4.
  - Balances
    - GlobalDM-routines are now used for Balance-information, because
      it is needed in several places.
  MRA:12-NOV-2010 RV079.5. Bugfix.
  - When Hours Per Employee-dialog is called from
    Employee Regs, it still shows the details-panel when
    switching from first to last tab-page. It must not do this!
  - When doing this without Employee-Regs, it does not
    do this.
  MRA:15-NOV-2010 RV079.6. Bugfix.
  - When Hours Per Employee-dialog is called from
    Employee Regs, and absence-grid is entered,
    it still shows the detail-fields for salary!
  - This does not happen when you call Hours Per Employee-
    dialog without Employee-Regs.
  MRA:25-NOV-2010 RV082.1.
  - When employee is of type 'prod. hrs. based on standard planning',
    then allow changing of prod.hrs. (manual).
  MRA:30-NOV-2010 RV082.3.
  - When 3 tables must be synchronized (PHE, SHE and PHEPT),
    then have some restrictions for changing manual salary hours:
    - Do not allow adding/deleting of manual salary hours.
    - Only allow changing of the hourtype for manual salary hours.
  MRA:3-DEC-2010 RV082.9.
  - When non-manual production-hours are shown
    in Production Hours-Tab, it looks like workspot
    and job can be changed, but it gives an error-message
    when you try to do this.
  - All fields in detail-panel must be disabled when the shown
    line is a non-manual line.
  MRA:3-FEB-2012. 2.0.162.215.2 Small change
  - In absence-grid: Column absencereason-description was too small,
    changed from 20 to 30 (which is the real max. length).
  - Also hourtype-description was too small, in salary-grid, changed
    from 20 to 30.
  MRA:16-MAY-2012 20013183. Extra counter Travel Time.
  - Hours Per Employee
    - This must show an extra counter named Travel Time in
      balance-tab.
      This should be visible when it is defined as
      COUNTER_ACTIVE_YN = 'Y' in Absence type per country-table,
      linked to the country of the plant of the current employee.
    - When manual hours are added/changed or deleted, then if
      this is linked to an hour type with TRAVELTIME_YN equals 'Y'
      then these hours must be booked in balance as 'earned travel time'.
    - When manual absence hours are added/changed or deleted,
      then when the absence reason connects to a absence type 'N',
      the balance (ABSENCETOTAL) for 'used travel time' must be
      updated.
  MRA:25-MAR-2013 20013035 Do not show inactive employees
  - Added IsEmployeeActive-function + DoActiveAction-function that determines
    if an employee is active or not.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:10-FEB-2014 20011800
  - Final Run System
  - Do not allow changes (only for MANUAL hours!) for days
    that were already exported.
  MRA:16-APR-2014 20011800.70
  - Final Run System - Rework
  MRA:21-JUL-2014 20013438
  - Any Norm-fields can only be changed by a user that is defined as
    system administrator.
  - Also do this for 'ADV Earned'-field.
  - Related to this order: When set to 'not-edible' (by admin-tool) then disable
    some extra field in balance-tab.
  MRA:11-NOV-2014 20013438.70 Rework
  - Any Norm-fields can only be changed by a user that is defined as
    system administrator.
  - It did enable the holiday-norm after using arrow-keys to go to another
    employee.
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - Add 'Show only active' to show only active employees
  - Added cdsFilterEmployee that links to QueryEmployee
  MRA:25-JUN-2015 20014450.50
  - Real Time Eff.
  MRA:2-MAY-2016 ABS-27382
  - Show only active (employees)-checkbox must be default checked.
  MRA:17-MAY-2016 ABS-28216
  - Show only active problem:
    - When swithing between TimeRecScanning-dialog and HoursPerEmployee-dialog
      it changes the employee.
  MRA:31-DEC-2018 PIM-417
  - Problem with active/inactive employees. It used a wrong year, resulting
    in showing less employees. Reason: It opened the cdsFilterEmployee
    before setting the correct year+week.
*)

unit HoursPerEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxLayout, dxExEdtr, dxEdLib, dxDBELib, DBCtrls, Mask,
  StdCtrls, dxDBTLCl, dxGrClms, ComCtrls, Buttons, dxEditor, dxExGrEd,
  dxExELib, UPimsConst;

const
  LongDayDescription = 'LongDay';
  ProdTimeColWidth = 45;
  SalTimeColWidth = 55;
  ScrollBarWidth = 20;
  NegativeTimeColor = clRed;
  PositiveTimeColor = clWindow;
  ProductionHourColName = 'dxDBGridProductionHourColumnDay';
  SalaryHourColName = 'dxDBGridSalaryHourColumnDay';
  AbsenceHourColName = 'dxDBGridAbsenceHourColumnDay';
  AbsenceHourSColName = 'dxDBGridAbsenceHourSColumnDay';
  EditTimeObjNameMaskProd = 'dxDBTimeEditProdDay';
  EditTimeObjNameMaskSal = 'dxDBTimeEditSalary';
  EditTimeObjNameMaskAbs = 'dxDBTimeEditAbsence';

type
  TGridsMode = (gdBrowse, gdReadOnly, gdEdit, gdNone);
  THoursPerEmployeeF = class(TGridBaseF)
    GroupBoxEmployee: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LabelStartWeekDate: TLabel;
    LabelEndWeekDate: TLabel;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dxDBGridSalaryHour: TdxDBGrid;
    dxDBGridSalaryHourColumnHourType: TdxDBGridLookupColumn;
    dxDBGridAbsenceHourS: TdxDBGrid;
    dxDBGridAbsenceHourSColumnReason: TdxDBGridLookupColumn;
    TabSheet3: TTabSheet;
    Employee: TGroupBox;
    Label4: TLabel;
    EditStartDate: TEdit;
    GroupBoxTravelTime: TGroupBox;
    Label49: TLabel;
    Label66: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label76: TLabel;
    EditTravelTimeRemaining: TEdit;
    EditTravelTimeEarned: TEdit;
    EditTravelTimeUsed: TEdit;
    EditTravelTimePlanned: TEdit;
    EditTravelTimeAvailable: TEdit;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    EditIllness: TEdit;
    EditIllnessUntil: TEdit;
    GroupBoxAbsenceHour: TGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    DBLookupComboBoxAbsenceReason: TDBLookupComboBox;
    dxDBTimeEditAbsence1: TdxDBTimeEdit;
    dxDBTimeEditAbsence2: TdxDBTimeEdit;
    dxDBTimeEditAbsence3: TdxDBTimeEdit;
    dxDBTimeEditAbsence4: TdxDBTimeEdit;
    dxDBTimeEditAbsence5: TdxDBTimeEdit;
    dxDBTimeEditAbsence6: TdxDBTimeEdit;
    dxDBTimeEditAbsence7: TdxDBTimeEdit;
    GroupBoxSalaryHour: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    DBLookupComboBoxDetailSalary: TDBLookupComboBox;
    dxDBTimeEditSalary1: TdxDBTimeEdit;
    dxDBTimeEditSalary2: TdxDBTimeEdit;
    dxDBTimeEditSalary3: TdxDBTimeEdit;
    dxDBTimeEditSalary4: TdxDBTimeEdit;
    dxDBTimeEditSalary5: TdxDBTimeEdit;
    dxDBTimeEditSalary6: TdxDBTimeEdit;
    dxDBTimeEditSalary7: TdxDBTimeEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    dxDBGridSalaryHourColumnTotal: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnTotal: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay1: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay2: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay3: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay4: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay5: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay6: TdxDBGridColumn;
    dxDBGridSalaryHourColumnDay7: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay1: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay2: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay3: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay4: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay5: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay6: TdxDBGridColumn;
    dxDBGridAbsenceHourSColumnDay7: TdxDBGridColumn;
    LabelEmplDesc: TLabel;
    dxDBGridSTotal: TdxDBGrid;
    dxDBGridLookupColumn2: TdxDBGridLookupColumn;
    dxDBGridColumn9: TdxDBGridColumn;
    dxDBGridColumn10: TdxDBGridColumn;
    dxDBGridColumn11: TdxDBGridColumn;
    dxDBGridColumn12: TdxDBGridColumn;
    dxDBGridColumn13: TdxDBGridColumn;
    dxDBGridColumn14: TdxDBGridColumn;
    dxDBGridColumn15: TdxDBGridColumn;
    dxDBGridColumn16: TdxDBGridColumn;
    GroupBoxProductionHour: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label50: TLabel;
    DBLookupComboBoxProdShift: TDBLookupComboBox;
    DBLookupComboBoxProdWorkspot: TDBLookupComboBox;
    DBLookupComboBoxPlant: TDBLookupComboBox;
    DBLookupComboBoxProdJob: TDBLookupComboBox;
    dxDBTimeEditProdDay1: TdxDBTimeEdit;
    dxDBTimeEditProdDay2: TdxDBTimeEdit;
    dxDBTimeEditProdDay3: TdxDBTimeEdit;
    dxDBTimeEditProdDay4: TdxDBTimeEdit;
    dxDBTimeEditProdDay5: TdxDBTimeEdit;
    dxDBTimeEditProdDay6: TdxDBTimeEdit;
    dxDBTimeEditProdDay7: TdxDBTimeEdit;
    dxDBGridProductionHour: TdxDBGrid;
    dxDBGridProductionHourColumnPlant: TdxDBGridLookupColumn;
    dxDBGridProductionHourColumnWorkspot: TdxDBGridLookupColumn;
    dxDBGridProductionHourColumnShift: TdxDBGridLookupColumn;
    dxDBGridProductionHourColumnJob: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay1: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay2: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay3: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay4: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay5: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay6: TdxDBGridColumn;
    dxDBGridProductionHourColumnDay7: TdxDBGridColumn;
    dxDBGridProductionHourColumnTotal: TdxDBGridColumn;
    dxDBGridAbsenceHour: TdxDBGrid;
    dxDBGridAbsenceHourColumnAbsenceReason: TdxDBGridLookupColumn;
    dxDBGridAbsenceHourColumnDay1: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay2: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay3: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay4: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay5: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay6: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnDay7: TdxDBGridColumn;
    dxDBGridAbsenceHourColumnTotal: TdxDBGridColumn;
    dxDBGridPTotal: TdxDBGrid;
    dxDBGridLookupColumn1: TdxDBGridLookupColumn;
    dxDBGridColumn1: TdxDBGridColumn;
    dxDBGridColumn2: TdxDBGridColumn;
    dxDBGridColumn3: TdxDBGridColumn;
    dxDBGridColumn4: TdxDBGridColumn;
    dxDBGridColumn5: TdxDBGridColumn;
    dxDBGridColumn6: TdxDBGridColumn;
    dxDBGridColumn7: TdxDBGridColumn;
    dxDBGridColumn8: TdxDBGridColumn;
    dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit;
    dxDBGridLayoutListWorkspot: TdxDBGridLayoutList;
    dxDBGridLayoutListWorkspotItem: TdxDBGridLayout;
    lblWorkspotDescription: TLabel;
    Label48: TLabel;
    MaskEditMaxSalaryBalance: TMaskEdit;
    MaskEditMaxSalaryBalanceBackup: TMaskEdit;
    BitBtnFirst: TBitBtn;
    BitBtnPrev: TBitBtn;
    BitBtnNext: TBitBtn;
    BitBtnLast: TBitBtn;
    edtHourType: TdxDBEdit;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    LabelHolidayRemaining: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    EditHolidayRemaining: TEdit;
    EditHolidayUsed: TEdit;
    EditHolidayPlanned: TEdit;
    EditHolidayAvailable: TEdit;
    MaskEditHolidayNorm: TMaskEdit;
    MaskEditHolidayNormBackup: TMaskEdit;
    GroupBox5: TGroupBox;
    Label17: TLabel;
    Label19: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    EditTimeRemaining: TEdit;
    EditTimeUsed: TEdit;
    EditTimePlanned: TEdit;
    EditTimeAvailable: TEdit;
    GroupBoxSeniorityHld: TGroupBox;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    EditSeniorityHldUsed: TEdit;
    EditSeniorityHldPlanned: TEdit;
    EditSeniorityHldAvailable: TEdit;
    GroupBoxHldPrevYear: TGroupBox;
    Label55: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    EditHldPrevYearRemaining: TEdit;
    EditHldPrevYearUsed: TEdit;
    EditHldPrevYearPlanned: TEdit;
    EditHldPrevYearAvailable: TEdit;
    GroupBoxShorterWeek: TGroupBox;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    EditShorterWeekRemaining: TEdit;
    EditShorterWeekUsed: TEdit;
    EditShorterWeekPlanned: TEdit;
    EditShorterWeekAvailable: TEdit;
    GroupBoxAddBnkHoliday: TGroupBox;
    Label65: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    EditAddBnkHolidayRemaining: TEdit;
    EditAddBnkHolidayUsed: TEdit;
    EditAddBnkHolidayPlanned: TEdit;
    EditAddBnkHolidayAvailable: TEdit;
    GroupBoxRsvBnkHoliday: TGroupBox;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    EditRsvBnkHolidayUsed: TEdit;
    EditRsvBnkHolidayPlanned: TEdit;
    EditRsvBnkHolidayAvailable: TEdit;
    GroupBoxSatCredit: TGroupBox;
    Label75: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    EditSatCreditRemaining: TEdit;
    EditSatCreditUsed: TEdit;
    EditSatCreditPlanned: TEdit;
    EditSatCreditAvailable: TEdit;
    Label51: TLabel;
    MaskEditSeniorityHldNorm: TMaskEdit;
    Label56: TLabel;
    MaskEditRsvBnkHolidayNorm: TMaskEdit;
    Label61: TLabel;
    EditTimeEarned: TEdit;
    MaskEditSeniorityHldNormBackup: TMaskEdit;
    MaskEditRsvBnkHolidayNormBackup: TMaskEdit;
    EditNormBankHldResMinManYN: TEdit;
    Label6: TLabel;
    MaskEditShorterWeekEarned: TMaskEdit;
    MaskEditShorterWeekEarnedBackup: TMaskEdit;
    Label18: TLabel;
    EditSatCreditEarned: TEdit;
    EditWorkRemaining: TEdit;
    EditWorkEarned: TEdit;
    EditWorkUsed: TEdit;
    EditWorkPlanned: TEdit;
    EditWorkAvailable: TEdit;
    cBoxShowOnlyActive: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DefaultSetFilterData(Sender: TObject);
    procedure DefaultTimeEditDblClick(Sender: TObject);
    procedure DefaultProdTimeEditChange(Sender: TObject);
    procedure DefaultChangeDate(Sender: TObject);
    procedure DefaultGridEnter(Sender: TObject);
    procedure DefaultGridClick(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxDBExtLookupEditEmployeeCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure dxDBGridDefaultCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure DefaultFormatTotalCol(Sender: TObject; ANode: TdxTreeListNode;
    	var AText: String);
    procedure dxDBGridProductionHourClick(Sender: TObject);
    procedure GridChangeRecord(Sender: TObject; OldNode, Node: TdxTreeListNode);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure pnlDetailExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl1Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure dxDBExtLookupEditEmployeeEnter(Sender: TObject);
    procedure dxDBExtLookupEditEmployeeClick(Sender: TObject);
    procedure dxBarButtonNavigationClick(Sender: TObject);
    procedure dxDBGridPTotalCustomDrawColumnHeader(Sender: TObject;
      AColumn: TdxTreeListColumn; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
      var ADone: Boolean);
    procedure dxBarButtonResetColumnsClick(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotPopup(Sender: TObject;
      const EditText: String);
    procedure dxDBExtLookupEditWorkspotKeyPress(Sender: TObject;
      var Key: Char);
    procedure dxDBExtLookupEditWorkspotEnter(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotExit(Sender: TObject);
    procedure BalancesChange(Sender: TObject);
    procedure MaskEditKeyChange(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtnFirstClick(Sender: TObject);
    procedure BitBtnPrevClick(Sender: TObject);
    procedure BitBtnNextClick(Sender: TObject);
    procedure BitBtnLastClick(Sender: TObject);
    procedure edtHourTypeExit(Sender: TObject);
    procedure RefreshData;
    procedure cBoxShowOnlyActiveClick(Sender: TObject);
  private
    { Private declarations }
    FRefreshData: Boolean;
    WorkTimeReductionCaption, HolidayCaption, TimeForTimeCaption,
    HolidayPreviousYearCaption, SeniorityHolidayCaption, AddBankHolidayCaption,
    SatCreditCaption, BankHolToRsvCaption, ShorterWorkingWeekCaption,
    TravelTimeCaption: String;
    procedure NormAskForSaveChanges;
    procedure CorrectWeekComponent;
    procedure GoToRecord(Which: Integer);
    procedure EmployeeChanged(Sender: TObject);
    procedure DoActiveAction;
    procedure FinalRunEnableEditComponents;
    function FinalRunWeekCheck: Boolean;
    function FinalRunYearCheck: Boolean;
    procedure NormEditSysAdminEnabler; // 20013438
    procedure MySetNotEditable; // 20013438
  public
    { Public declarations }
    FEmptyDate: Boolean;
    procedure EnableDetailEdit(AEnable: Boolean; AEnableHourtype: Boolean);
    procedure ResizeProductionHour;
    procedure ResizeSalaryHour;
    procedure SetActiveGridMode(GridMode: TGridsMode);
    procedure ShowButtons(OnOff: Boolean);
    procedure ShowWeekDates;
    procedure ActivateGridDataSource(OnOff: Boolean);
    procedure SetBalances;
    procedure ResetBalances;
    procedure ResetTimeEditColor;
    procedure ResetTotals;
    procedure ReturnTotals;
    procedure SetTotals(TotalValues: TDailyTime;
      TotalDay, TotalPDay, TotalSDay: TWeekDaysTotal);
    procedure NotSetTotal;
    procedure SetEditTimeSign(WeekTimeSigns: TWeekDaysTotal);
  end;

function HoursPerEmployeeF: THoursPerEmployeeF;

var
  HoursPerEmployeeF_HND: THoursPerEmployeeF;
  DefaultYear,DefaultWeek: word;

implementation

uses
  ListProcsFRM, SystemDMT, UGlobalFunctions,
  UPimsMessageRes, HoursPerEmployeeDMT, CalculateTotalHoursDMT,
  GlobalDMT;

{$R *.DFM}

function HoursPerEmployeeF: THoursPerEmployeeF;
begin
  if (HoursPerEmployeeF_HND = nil) then
    HoursPerEmployeeF_HND := THoursPerEmployeeF.Create(Application);
  Result := HoursPerEmployeeF_HND;
end;

procedure THoursPerEmployeeF.DefaultSetFilterData(Sender: TObject);
var
  AFilter: TFilterData;
  // RV077.1. Enable/Disable grid-editable.
  procedure ActiveGridEnableAction;
  begin
    if SystemDM.IsRFL then
    begin
      AFilter := HoursPerEmployeeDM.FilterData;
      case TActiveTab(PageControl1.ActivePageIndex) of
      ProductionHours:
        begin
          // RV082.1. Disabled.
{
          // Non-scanners and BookProdHrs-employees cannot edit prod. hrs.
          if (not AFilter.IsScanning) and (AFilter.BookProdHrs) then
            dxDBGridProductionHour.Tag := 0
          else
            dxDBGridProductionHour.Tag := 1;

          ActiveGridRefresh;
}
        end;
      SalaryHours:
        begin
          // RV082.3.
          if not HoursPerEmployeeDM.Sync3Tables then
            dxDBGridSalaryHour.Tag := 1 // All changes allowed.
          else
            dxDBGridSalaryHour.Tag := 2; // Only edit allowed for some fields.
{
          // Scanners and Non-BookProdHrs-employee cannot edit sal. hrs.
          if (AFilter.IsScanning) and (not AFilter.BookProdHrs) then
            dxDBGridSalaryHour.Tag := 0 // No changes allowed.
          else
          begin
            if not HoursPerEmployeeDM.Sync3Tables then
              dxDBGridSalaryHour.Tag := 1 // All changes allowed.
            else
              dxDBGridSalaryHour.Tag := 2; // Only edit allowed for some fields.
          end;
}
          try
            ActiveGridRefresh;
          except
          end;
        end;
      end;
    end; // if SystemDM.IsRFL then
  end; // ActiveGridEnableAction;
begin
  inherited;
  if HoursPerEmployeeDM = nil then
    Exit;
  AFilter := HoursPerEmployeeDM.FilterData;
  if Sender <> PageControl1 then
    AFilter.Active := False;
  try
    Afilter.Year := dxSpinEditYear.IntValue;
    AFilter.Week := dxSpinEditWeek.IntValue;
  except
    dxSpinEditYear.Value := DefaultYear;
    dxSpinEditWeek.Value := DefaultWeek;
    CorrectWeekComponent;
    Afilter.Year := DefaultYear;
    AFilter.Week := DefaultWeek;
  end;
  // RV077.1. Not here! Or it will not load any records.
//  ActiveGridEnableAction; // RV077.1.  
  case TActiveTab(PageControl1.ActivePageIndex) of
  ProductionHours:
    begin
      AFilter.ActiveTab := ProductionHours;
      ActiveGrid := dxDBGridProductionHour;
      ResizeProductionHour;
      ResetTotals;
    end;
  SalaryHours:
    begin
      AFilter.ActiveTab := SalaryHours;
      ActiveGrid := dxDBGridSalaryHour;
      ResizeSalaryHour;
      ResetTotals;
    end;
  Balances:
    begin
      AFilter.ActiveTab := Balances;
      if AFilter.Active then
        SetBalances;
    end;
  //RV067.4.
  HolidayCounters:
    begin
      AFilter.ActiveTab := HolidayCounters;
      if AFilter.Active then
        SetBalances;
    end;
  end;
  try
    ActiveGrid.BeginUpdate;
    if (HoursPerEmployeeDM.FilterData.Employee_Number <>
      HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').asInteger)or
      (HoursPerEmployeeDM.FilterData.Year<>AFilter.Year)or
      (HoursPerEmployeeDM.FilterData.Week<>AFilter.Week) then
    begin
      AFilter.Active := False;
      ActivateGridDataSource(False);
      ResetBalances;
    end;
    HoursPerEmployeeDM.FilterData := AFilter;
    if AFilter.Active then ReturnTotals;
    case TActiveTab(PageControl1.ActivePageIndex) of
    ProductionHours: SetActiveGridMode(gdReadOnly);
    SalaryHours    : SetActiveGridMode(gdReadOnly);
    Balances       : SetActiveGridMode(gdNone);
    HolidayCounters: SetActiveGridMode(gdNone);
    end;
  finally
    ActiveGrid.EndUpdate;
    //car 15-1-2004
    if (TActiveTab(PageControl1.ActivePageIndex) = ProductionHours) or
      (TActiveTab(PageControl1.ActivePageIndex) = SalaryHours) then
      if (Sender.ClassName <> 'TdxSpinEdit') then // MR:02-04-2004
        ActiveGrid.SetFocus;
    ActiveGridEnableAction; // RV077.1.
  end;
end;

procedure THoursPerEmployeeF.EnableDetailEdit(AEnable: Boolean;
  AEnableHourtype: Boolean);
var
  AEnableBackup: Boolean;
begin
  AEnableBackup := AEnable;
  if AEnable and AEnableHourtype then
  begin
    AEnable := FinalRunWeekCheck; // 20011800
    AEnableHourtype := AEnable; // 20011800.70
  end;
  if ActiveGrid=dxDBGridProductionHour then
  begin
    dxDBTimeEditProdDay1.Enabled := AEnable;
    dxDBTimeEditProdDay2.Enabled := AEnable;
    dxDBTimeEditProdDay3.Enabled := AEnable;
    dxDBTimeEditProdDay4.Enabled := AEnable;
    dxDBTimeEditProdDay5.Enabled := AEnable;
    dxDBTimeEditProdDay6.Enabled := AEnable;
    dxDBTimeEditProdDay7.Enabled := AEnable;
    DBLookupComboBoxPlant.Enabled := AEnable;
    DBLookupComboBoxProdShift.Enabled := AEnable;
    DBLookupComboBoxProdWorkspot.Enabled := AEnable;
    dxDBExtLookupEditWorkspot.Enabled := AEnable; // RV082.9.
    DBLookupComboBoxProdJob.Enabled := AEnable;
  end;
  // Active Grid = AbsenceHours
  if (ActiveGrid=dxDBGridAbsenceHour) or (ActiveGrid=dxDBGridAbsenceHourS) then
  begin
    dxDBTimeEditAbsence1.Enabled := AEnable;
    dxDBTimeEditAbsence2.Enabled := AEnable;
    dxDBTimeEditAbsence3.Enabled := AEnable;
    dxDBTimeEditAbsence4.Enabled := AEnable;
    dxDBTimeEditAbsence5.Enabled := AEnable;
    dxDBTimeEditAbsence6.Enabled := AEnable;
    dxDBTimeEditAbsence7.Enabled := AEnable;
    DBLookupComboBoxAbsenceReason.Enabled := AEnable;
  end;
  // Active Grid = Salary Hours
  if (ActiveGrid=dxDBGridSalaryHour) then
  begin
    dxDBTimeEditSalary1.Enabled := AEnable;
    dxDBTimeEditSalary2.Enabled := AEnable;
    dxDBTimeEditSalary3.Enabled := AEnable;
    dxDBTimeEditSalary4.Enabled := AEnable;
    dxDBTimeEditSalary5.Enabled := AEnable;
    dxDBTimeEditSalary6.Enabled := AEnable;
    dxDBTimeEditSalary7.Enabled := AEnable;
    DBLookupComboBoxDetailSalary.Enabled := {AEnable} AEnableHourtype; // RV082.3.
    edtHourType.Enabled := {AEnable} AEnableHourtype; //REV065.5 // RV082.3.
  end;
  if AEnableBackup then
    if AEnable then
      FinalRunEnableEditComponents; // 20011800
end;

procedure THoursPerEmployeeF.FormCreate(Sender: TObject);
  // ABS-28216 Set this property here based on checkbox.
var
  Week: word;
  procedure InitFilter;
  var
    AFilter: TFilterData;
    Year, Month, Day: Word;
  begin
    AFilter := HoursPerEmployeeDM.FilterData;
    DecodeDate(Date, Year, Month, Day);
    AFilter.Year := Year;
    AFilter.Week := ListProcsF.WeekOfYear(Date);
    HoursPerEmployeeDM.ShowOnlyActive := cBoxShowOnlyActive.Checked;
    HoursPerEmployeeDM.FilterData := AFilter;
  end; // InitFilter
begin
  inherited;
  HoursPerEmployeeDM := CreateFormDM(THoursPerEmployeeDM);
  // ABS-28216 Set this property here based on checkbox.
  InitFilter;
  // 20014450.50
  BitBtnFirst.Color := clBtnFace;
  BitBtnPrev.Color := clBtnFace;
  BitBtnNext.Color := clBtnFace;
  BitBtnLast.Color := clBtnFace;

  // PIM-417
  // We need to know the correct year and week here, before the
  // cdsFilterEmployee is opened, or it will use a wrong year/week to filter on,
  // which can result in showing less employees, because of the check on
  // active employees.
  FEmptyDate := False;
  if SystemDM.ASaveTimeRecScanning.DateScanning = EncodeDate(1900,1,1) then
  begin
    SystemDM.ASaveTimeRecScanning.DateScanning := Now;
    FEmptyDate := True;
  end;
  ListProcsF.WeekUitDat(SystemDM.ASaveTimeRecScanning.DateScanning, DefaultYear,
    Week);
  try
    dxSpinEditYear.IntValue := DefaultYear;
    DefaultWeek :=
      ListProcsF.WeekOfYear(SystemDM.ASaveTimeRecScanning.DateScanning);
    dxSpinEditWeek.IntValue := DefaultWeek;
  finally
  end;
  HoursPerEmployeeDM.FilterDataYear := DefaultYear;
  HoursPerEmployeeDM.FilterDataWeek := DefaultWeek;

  // Open the Queries here, otherwise they are not open!
  with HoursPerEmployeeDM do
  begin
    QueryWorkspotLU.Open;
    QueryShiftLU.Open;
    QueryJobLU.Open;
    //Car 550274
    cdsFilterEmployee.Open;
  end;
  // MR:08-07-2003
  // Create the TClientDataSets here!
  // If you do this in 'HoursPerEmployeeDM.DatamoduleCreate
  // it fails to create.
  HoursPerEmployeeDM.cdsHREPRODUCTION.CreateDataSet;
  HoursPerEmployeeDM.cdsHRESALARY.CreateDataSet;
  HoursPerEmployeeDM.cdsHREABSENCE.CreateDataSet;
  lblWorkspotDescription.Caption := '';
  // RV067.MRA.29. Save captions for counter-groupboxes here.
  WorkTimeReductionCaption := GroupBox3.Caption;
  HolidayCaption := GroupBox2.Caption;
  TimeForTimeCaption := GroupBox5.Caption;
  HolidayPreviousYearCaption := GroupBoxHldPrevYear.Caption;
  SeniorityHolidayCaption := GroupBoxSeniorityHld.Caption;
  AddBankHolidayCaption := GroupBoxAddBnkHoliday.Caption;
  SatCreditCaption := GroupBoxSatCredit.Caption;
  BankHolToRsvCaption := GroupBoxRsvBnkHoliday.Caption;
  ShorterWorkingWeekCaption := GroupBoxShorterWeek.Caption;
  // 20013183
  TravelTimeCaption := GroupBoxTravelTime.Caption;
  // 20014289
  dxDBGridSalaryHour.BandFont.Color := clWhite;
  dxDBGridSalaryHour.BandFont.Style := [fsBold];
  dxDBGridAbsenceHourS.BandFont.Color := dxDBGridSalaryHour.BandFont.Color;
  dxDBGridAbsenceHourS.BandFont.Style := dxDBGridSalaryHour.BandFont.Style;
  dxDBGridSTotal.BandFont.Color := dxDBGridSalaryHour.BandFont.Color;
  dxDBGridSTotal.BandFont.Style := dxDBGridSalaryHour.BandFont.Style;
  dxDBGridProductionHour.BandFont.Color := dxDBGridSalaryHour.BandFont.Color;
  dxDBGridProductionHour.BandFont.Style := dxDBGridSalaryHour.BandFont.Style;
  dxDBGridAbsenceHour.BandFont.Color := dxDBGridSalaryHour.BandFont.Color;
  dxDBGridAbsenceHour.BandFont.Style := dxDBGridSalaryHour.BandFont.Style;
  dxDBGridPTotal.BandFont.Color := dxDBGridSalaryHour.BandFont.Color;
  dxDBGridPTotal.BandFont.Style := dxDBGridSalaryHour.BandFont.Style;
end;

procedure THoursPerEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  HoursPerEmployeeF_HND := Nil;
end;

procedure THoursPerEmployeeF.FormResize(Sender: TObject);
begin
  inherited;
  ResizeProductionHour;
  ResizeSalaryHour;
//  if (BitBtnRefresh.Font.Color  = clWindowText) then
//    ReturnTotals;
end;

procedure THoursPerEmployeeF.FormShow(Sender: TObject);
var
  {month, day, }Week: word;
  obj, index: integer;
  DayDescript, DayCode: String;
  AFilter: TFilterData;
  DefaultTimeSigns: TWeekDaysTotal;
begin
  inherited;
  // Be sure the correct tab-page is set during design time!
//  PageControl1.ActivePageIndex := Integer(ProductionHours);
  FRefreshData := True;
  GroupBoxSalaryHour.Visible := False;
  GroupBoxAbsenceHour.Visible := False;
  GroupBoxProductionHour.Visible := True;
  if dxDBGridProductionHour.DataSource = nil then
    dxDBGridProductionHour.DataSource :=
      HoursPerEmployeeDM.DataSourceHREPRODUCTION;
  if dxDBGridAbsenceHour.DataSource = nil then
    dxDBGridAbsenceHour.DataSource :=
      HoursPerEmployeeDM.DataSourceHREABSENCE;
  if dxDBGridSalaryHour.DataSource = nil then
    dxDBGridSalaryHour.DataSource :=
      HoursPerEmployeeDM.DataSourceHRESALARY;
  ActiveGrid := dxDBGridProductionHour;
  // RV075.8.
//  SetActiveGridMode(gdBrowse);

// CAR 7-5-2003 initialize saved values
//CAR 17-10-2003 : 550262
  FEmptyDate := False;
  if SystemDM.ASaveTimeRecScanning.DateScanning = EncodeDate(1900,1,1) then
  begin
    SystemDM.ASaveTimeRecScanning.DateScanning := Now;
    FEmptyDate := True;
  end;
  if SystemDM.ASaveTimeRecScanning.AEmployeeNumber = -1 then
  begin
    HoursPerEmployeeDM.cdsFilterEmployee.First;
    if not HoursPerEmployeeDM.cdsFilterEmployee.IsEmpty then
      SystemDM.ASaveTimeRecScanning.Employee :=
        HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    HoursPerEmployeeDM.TableShift.First;
    if not HoursPerEmployeeDM.TableShift.IsEmpty then
      SystemDM.ASaveTimeRecScanning.AShiftNumber :=
        HoursPerEmployeeDM.TableShift.FieldByName('SHIFT_NUMBER').AsInteger;
  end
  else
    HoursPerEmployeeDM.cdsFilterEmployee.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee, []);
//
  // MR:06-01-2004 Get year through weeknumber
//  DecodeDate(SystemDM.ASaveTimeRecScanning.DateScanning, DefaultYear, month,day);
  ListProcsF.WeekUitDat(SystemDM.ASaveTimeRecScanning.DateScanning, DefaultYear,
    Week);

  FRefreshData := False;
  try
    dxSpinEditYear.IntValue := DefaultYear;

    DefaultWeek :=
      ListProcsF.WeekOfYear(SystemDM.ASaveTimeRecScanning.DateScanning);

    dxSpinEditWeek.IntValue := DefaultWeek;
  finally
    FRefreshData := True;
  end;
  CorrectWeekComponent;
  HoursPerEmployeeDM.SetWeekDates(0,0);
  AFilter.Year := DefaultYear;
  AFilter.Week := DefaultWeek;
  AFilter.ActiveTab := ProductionHours;
  AFilter.Active := False;
  AFilter.Employee_Number := -1;
  AFilter.IsScanning := False; // MR:23-1-2004
  AFilter.ExportDate := NullDate; // 20011800
  HoursPerEmployeeDM.FilterData := AFilter;
  //Days Descriptions
  for index:=1 to 7 do
  begin
    DayCode := SystemDM.GetDayWCode(Index);
    DayDescript := SystemDM.GetDayWDescription(Index);
    dxDBGridProductionHour.Columns[index+3].Caption := DayCode;
    dxDBGridAbsenceHour.Columns[index].Caption := DayCode;
    dxDBGridSalaryHour.Columns[index].Caption := DayCode;
    dxDBGridAbsenceHourS.Columns[index].Caption := DayCode;
    for obj:=0 to ComponentCount-1 do
    begin
      if (Components[obj] is TLabel) then
      begin
      	if (UpperCase(TLabel(Components[obj]).Caption) =
          Format(UpperCase(TimeFieldMask)+'%d',[index])) then
            TLabel(Components[obj]).Caption := DayCode;
        if (TLabel(Components[obj]).Caption =
          Format(LongDayDescription+'%d',[index])) then
            TLabel(Components[obj]).Caption := DayDescript;
      end;
      if Components[obj] is TdxTimeEdit then
        TdxTimeEdit(Components[obj]).Hint := SPimsNegativeTime;
    end;
    DefaultTimeSigns[index] := 1;
  end;
  ResizeProductionHour;
  ResizeSalaryHour;
  ResetTotals;
  ShowWeekDates;
  SetEditTimeSign(DefaultTimeSigns);
  LabelEmplDesc.Caption :=
    HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
//car 7-4-2003
  NotSetTotal;
// CAR 7-5-2003 initialize saved values
//CAR 17-10-2003 : 550262
  if SystemDM.ASaveTimeRecScanning.DateScanning = EncodeDate(1900,1,1) then
     SystemDM.ASaveTimeRecScanning.DateScanning := Now;
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
  begin
    HoursPerEmployeeDM.cdsFilterEmployee.First;
    if not HoursPerEmployeeDM.cdsFilterEmployee.IsEmpty then
      SystemDM.ASaveTimeRecScanning.Employee :=
        HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    HoursPerEmployeeDM.TableShift.First;
    if not HoursPerEmployeeDM.TableShift.IsEmpty then
      SystemDM.ASaveTimeRecScanning.Shift :=
        HoursPerEmployeeDM.TableShift.FieldByName('SHIFT_NUMBER').AsInteger;
  end
  else
    HoursPerEmployeeDM.cdsFilterEmployee.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee,[]);

  // Switch tabpages to be sure the dxDBGridPTotal is aligned correct.
  PageControl1.ActivePageIndex := Integer(SalaryHours);
  PageControl1.ActivePageIndex := Integer(ProductionHours);
  //CAR 21-7-2003
//   TBLenghtClass := TTBLengthClass.Create;
  EmployeeChanged(Sender);
  FinalRunEnableEditComponents; // 20011800
  RefreshData;
  // ABS-28216 Do this during FormCreate 
//  cBoxShowOnlyActiveClick(cBoxShowOnlyActive); // ABS-27382
end;

procedure THoursPerEmployeeF.ResizeProductionHour;
var
  Index, FirstColWidth: integer;
begin
  FirstColWidth := dxDBGridProductionHour.Width - (8 * ProdTimeColWidth) -
    ScrollBarWidth;
  FirstColWidth := Round(FirstColWidth / 4) ;
  FirstColWidth := Max(FirstColWidth,5);
  for Index := 0 to dxDBGridProductionHour.columncount - 1 do
    if Index < 4 then
      dxDBGridProductionHour.Columns[index].Width := FirstColWidth
    else
      dxDBGridProductionHour.Columns[index].Width := ProdTimeColWidth;

  dxDBGridAbsenceHour.Columns[0].Width := FirstColWidth *4;
  for index := 1 to dxDBGridAbsenceHour.columncount -1 do
    dxDBGridAbsenceHour.Columns[index].Width := ProdTimeColWidth;
// resize total hours
  dxDBGridPTotal.BandS[0].Width := FirstColWidth *4;
  dxDBGridPTotal.Columns[0].Width := FirstColWidth *4;

  for index := 1 to dxDBGridPTotal.Bands.Count -1 do
    if Index = dxDBGridPTotal.Bands.count -1 then
      dxDBGridPTotal.Bands[index].Width := ProdTimeColWidth + ScrollBarWidth
    else
     dxDBGridPTotal.Bands[index].Width := ProdTimeColWidth;
  for index := 1 to dxDBGridPTotal.Bands.count - 2 do
    dxDBGridPTotal.Bands[index].Caption := SystemDM.GetDayWCode(index);
  for index := 1 to dxDBGridPTotal.ColumnCount -1 do
    if Index = dxDBGridPTotal.Columncount -1 then
       dxDBGridPTotal.Columns[Index].Width :=ProdTimeColWidth + ScrollBarWidth
    else
      dxDBGridPTotal.Columns[index].Width := ProdTimeColWidth;
  dxDBGridPTotal.Height := 37;
//  ResetTotals;
// end
end;

procedure THoursPerEmployeeF.ResizeSalaryHour;
var
  index, FirstColWidth: integer;
begin
  FirstColWidth := dxDBGridSalaryHour.Width - (8 * SalTimeColWidth) -
    ScrollBarWidth;
  FirstColWidth := Max(FirstColWidth,5);
  dxDBGridSalaryHour.Columns[0].Width := FirstColWidth;
  for index := 1 to dxDBGridSalaryHour.columncount - 1 do
    dxDBGridSalaryHour.Columns[index].Width := SalTimeColWidth;
  dxDBGridAbsenceHourS.Columns[0].Width := FirstColWidth;
  for index := 1 to dxDBGridAbsenceHourS.columncount -1 do
    dxDBGridAbsenceHourS.Columns[index].Width := SalTimeColWidth;
// resize total hours    car 24.02.2003
  dxDBGridSTotal.Bands[0].Width := FirstColWidth ;
  dxDBGridSTotal.Columns[0].Width := FirstColWidth ;
  for index := 1 to dxDBGridSTotal.Bands.Count -1 do
    if Index = dxDBGridSTotal.Bands.count -1 then
      dxDBGridSTotal.Bands[index].Width := SalTimeColWidth + ScrollBarWidth
    else
      dxDBGridSTotal.Bands[index].Width := SalTimeColWidth;
  for index := 1 to dxDBGridSTotal.Bands.Count -2 do
    dxDBGridSTotal.Bands[index].Caption := SystemDM.GetDayWCode(index);
  for index := 1 to dxDBGridSTotal.ColumnCount -1 do
    if Index = dxDBGridSTotal.Columncount -1 then
      dxDBGridSTotal.Columns[Index].Width :=SalTimeColWidth + ScrollBarWidth
    else
      dxDBGridSTotal.Columns[index].Width := SalTimeColWidth;

  dxDBGridSTotal.Height := 37;
//  ResetTotals;
 // end
end;

procedure THoursPerEmployeeF.ShowButtons(OnOff: Boolean);
begin
  if OnOff then
  begin
    dxBarButtonFirst.visible := ivAlways;
    dxBarButtonPrior.visible := ivAlways;
    dxBarButtonNext.visible := ivAlways;
    dxBarButtonLast.visible := ivAlways;
    // car 15-9-2003 changes rights user
    if Form_Edit_YN = ITEM_VISIBIL_EDIT then
      SetEditable
    else
      MySetNotEditable; // 20013438
    dxBarButtonExportAllHTML.Visible := ivAlways;
    dxBarButtonExportAllXLS.Visible := ivAlways;
  end
  else
  begin
    dxBarButtonFirst.visible := ivNever;
    dxBarButtonPrior.visible := ivNever;
    dxBarButtonNext.visible := ivNever;
    dxBarButtonLast.visible := ivNever;
    dxBarBDBNavInsert.visible := ivNever;
    dxBarBDBNavDelete.visible := ivNever;
    dxBarButtonEditMode.visible := ivNever;
    dxBarButtonExportAllHTML.Visible := ivNever;
    dxBarButtonExportAllXLS.Visible := ivNever;
    // MR:31-12-2002 Show cancel/save buttons
//    dxBarBDBNavCancel.Visible := ivNever;
//    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivAlways;
    dxBarBDBNavPost.Visible := ivAlways;
  end;
  NormEditSysAdminEnabler; // 20013438
end;

procedure THoursPerEmployeeF.ShowWeekDates;
begin
  LabelStartWeekDate.Caption := SRepExpPayroll+' '+
    DateToStr(HoursPerEmployeeDM.WeekDates[1]);
  LabelEndWeekDate.Caption := SPimsTo+' '+
    DateToStr(HoursPerEmployeeDM.WeekDates[7]);
end;

procedure THoursPerEmployeeF.DefaultTimeEditDblClick(Sender: TObject);
begin
  if (not (TdxDBTimeEdit(Sender).DataSource.State in [dsEdit,dsInsert,dsBrowse])) and
  	(TdxDBTimeEdit(Sender).Time = 0) then exit;
  if (TdxDBTimeEdit(Sender).color = PositiveTimeColor) then
  begin
    if ActiveGrid = dxDBGridProductionHour then
    begin
      HoursPerEmployeeDM.cdsHREPRODUCTION.Edit;
      HoursPerEmployeeDM.cdsHREPRODUCTION.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := UNCHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := NegativeTimeColor;
    end;
    if ActiveGrid = dxDBGridSalaryHour then
    begin
      HoursPerEmployeeDM.cdsHRESALARY.Edit;
      HoursPerEmployeeDM.cdsHRESALARY.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := UNCHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := NegativeTimeColor;
    end;
    if (ActiveGrid = dxDBGridAbsenceHour) or
    	(ActiveGrid = dxDBGridAbsenceHourS) then
    begin
      HoursPerEmployeeDM.cdsHREABSENCE.Edit;
      HoursPerEmployeeDM.cdsHREABSENCE.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := UNCHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := NegativeTimeColor;
    end;
  end
  else
  begin
    if ActiveGrid = dxDBGridProductionHour then
    begin
      HoursPerEmployeeDM.cdsHREPRODUCTION.Edit;
      HoursPerEmployeeDM.cdsHREPRODUCTION.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := CHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := PositiveTimeColor;
    end;
    if ActiveGrid = dxDBGridSalaryHour then
    begin
      HoursPerEmployeeDM.cdsHRESALARY.Edit;
      HoursPerEmployeeDM.cdsHRESALARY.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := CHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := PositiveTimeColor;
    end;
    if (ActiveGrid = dxDBGridAbsenceHour) or
    	(ActiveGrid = dxDBGridAbsenceHourS) then
    begin
      HoursPerEmployeeDM.cdsHREABSENCE.Edit;
      HoursPerEmployeeDM.cdsHREABSENCE.FieldByName(Format(SignFieldMask+'%d',
        [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).asString := CHECKEDVALUE;
      TdxDBTimeEdit(Sender).color := PositiveTimeColor;
    end;
  end;
end;

procedure THoursPerEmployeeF.DefaultProdTimeEditChange(Sender: TObject);
begin
  if (HoursPerEmployeeDM.cdsHREPRODUCTION.FieldByName(Format(SignFieldMask+'%d',
    [GetNoFromStr(TdxDBTimeEdit(Sender).Name)])).AsString = CHECKEDVALUE) then
    TdxDBTimeEdit(Sender).Color := PositiveTimeColor
  else
    TdxDBTimeEdit(Sender).Color := NegativeTimeColor;
end;

procedure THoursPerEmployeeF.SetActiveGridMode(GridMode: TGridsMode);
var
  CanAction: boolean;
  ADataSet: TDataSet;
  // RV077.1.
  function ActiveGridModeEnableAction(ACanAction: Boolean): Boolean;
  begin
    Result := False;
    if SystemDM.IsRFL then
    begin
      case ActiveGrid.Tag of
      0: // Do not allow changes at all.
        begin
          dxBarBDBNavInsert.Visible := ivNever;
          dxBarBDBNavDelete.Visible := ivNever;
          dxBarBDBNavPost.Visible := ivNever;
          dxBarBDBNavCancel.Visible := ivNever;
          dxBarButtonEditMode.Visible := ivNever;
          EnableDetailEdit(False, False);
          Result := True;
        end;
      2: // Only allow changes, but not add/delete.
        begin
          dxBarBDBNavInsert.Visible := ivNever;
          dxBarBDBNavDelete.Visible := ivNever;
          if ACanAction then
          begin
            dxBarBDBNavPost.Visible := ivAlways;
            dxBarBDBNavCancel.Visible := ivAlways;
            dxBarButtonEditMode.Visible := ivAlways;
            EnableDetailEdit(False, True);
          end
          else
          begin
            dxBarBDBNavPost.Visible := ivNever;
            dxBarBDBNavCancel.Visible := ivNever;
            dxBarButtonEditMode.Visible := ivNever;
            EnableDetailEdit(False, False);
          end;
          Result := True;
        end;
      end;
    end;
  end;
begin
  // RV079.5. Original.
  // When this is done:
  // - When called from emp.regs. it exits when
  //   4th Tab is opened. As a result, the detail-panel
  //   is still visible.
{  if TdxDBGrid(ActiveGrid).DataSource = Nil then
    Exit; }
  // RV079.5. New.
  // When this is done (see above), it is OK.
  CanAction := False;
  if TdxDBGrid(ActiveGrid).DataSource <> Nil then
  begin
    AdataSet := TdxDBGrid(ActiveGrid).DataSource.DataSet;
    CanAction := (HoursPerEmployeeDM.FilterData.Active and
      (not AdataSet.IsEmpty) and
      (ADataSet.FieldByName('MANUAL_YN').asString = CHECKEDVALUE));
  end;
  case GridMode of
  gdBrowse:
    begin
      ShowButtons(True);
      pnlDetail.Visible := True;
      if not ActiveGridModeEnableAction(CanAction) then // RV077.1.
      begin
        dxBarBDBNavInsert.Enabled := HoursPerEmployeeDM.FilterData.Active;
        dxBarBDBNavDelete.Enabled := CanAction;
        dxBarBDBNavPost.Enabled := False;
        dxBarBDBNavCancel.Enabled := False;
        dxBarButtonEditMode.Down := False;
        dxBarButtonEditMode.Enabled := CanAction;
        EnableDetailEdit(True, CanAction);
      end;
    end;
  gdReadOnly:
    begin
      ShowButtons(True);
      pnlDetail.Visible := True;
      if not ActiveGridModeEnableAction(CanAction) then // RV077.1.
      begin
        dxBarBDBNavInsert.Enabled := HoursPerEmployeeDM.FilterData.Active;
        dxBarBDBNavDelete.Enabled := False;
        dxBarBDBNavPost.Enabled := False;
        dxBarBDBNavCancel.Enabled := False;
        dxBarButtonEditMode.Down := False;
        dxBarButtonEditMode.Enabled := False;
        EnableDetailEdit(False, False);
      end;
    end;
  gdEdit:
    begin
      ShowButtons(True);
      pnlDetail.Visible := True;
      if not ActiveGridModeEnableAction(CanAction) then // RV077.1.
      begin
        dxBarBDBNavInsert.Enabled := False;
        dxBarBDBNavDelete.Enabled := False;
        dxBarBDBNavPost.Enabled := True;
        dxBarBDBNavCancel.Enabled := True;
        dxBarButtonEditMode.Enabled := False;
      end;
    end;
  gdNone:
    begin
      ShowButtons(False);
      pnlDetail.Visible := False;
    end;
  end;
end;

procedure THoursPerEmployeeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  SetActiveGridMode(gdEdit);
  EnableDetailEdit(True, True);
  ResetTimeEditColor;
end;

procedure THoursPerEmployeeF.dxBarBDBNavPostClick(Sender: TObject);
var
  Remaining, Normal, Earned, Used, Planned, Available: String;
begin
  inherited;
  // MR:31-12-2002 Norm in Total-page has changed?
  if
    (TActiveTab(PageControl1.ActivePageIndex) = Balances) or
    (TActiveTab(PageControl1.ActivePageIndex) = HolidayCounters)
  then
  begin
    // Save norm-total
    if not HoursPerEmployeeDM.SetNormHoliday(MaskEditHolidayNorm.Text) then
    begin
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeHoliday(Remaining, Normal, Used, Planned,
        Available);
      MaskeditHolidayNorm.Text := Normal;
      MaskEditHolidayNormBackup.Text := Normal;
      DisplayMessage(SPimsInvalidTime, mtInformation, [mbOk]);
    end;
    MaskEditHolidayNormBackup.Text := MaskEditHolidayNorm.Text;
    // Save norm seniority
    if not HoursPerEmployeeDM.SetNormSeniority(MaskEditSeniorityHldNorm.Text) then
    begin
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeSeniorityHld(Normal, Used, Planned,
        Available);
      MaskEditSeniorityHldNorm.Text := Normal;
      MaskEditSeniorityHldNormBackup.Text := Normal;
      DisplayMessage(SPimsInvalidTime, mtInformation, [mbOk]);
    end;
    MaskEditSeniorityHldNormBackup.Text := MaskEditSeniorityHldNorm.Text;
    // Save norm bank holiday reserve
    if not HoursPerEmployeeDM.SetNormRsvBnkHoliday(MaskEditRsvBnkHolidayNorm.Text) then
    begin
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeRsvBnkHoliday(Normal, Used, Planned, Available);
      MaskEditRsvBnkHolidayNorm.Text := Normal;
      MaskEditRsvBnkHolidayNormBackup.Text := Normal;
      DisplayMessage(SPimsInvalidTime, mtInformation, [mbOk]);
    end;
    MaskEditRsvBnkHolidayNormBackup.Text := MaskEditRsvBnkHolidayNorm.Text;
    // RV071.5.
    // Save earned shorter working week
    if not HoursPerEmployeeDM.SetEarnedShorterWorkerWeek(MaskEditShorterWeekEarned.Text) then
    begin
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeShorterWeek(Remaining, Earned, Used, Planned, Available);
      EditShorterWeekRemaining.Text := Remaining;
      MaskEditShorterWeekEarned.Text := Earned;
      MaskEditShorterWeekEarnedBackup.Text := Earned;
      EditShorterWeekUsed.Text := Used;
      EditShorterWeekPlanned.Text := Planned;
      EditShorterWeekAvailable.Text := Available;
    end;
    MaskEditShorterWeekEarnedBackup.Text := MaskEditShorterWeekEarned.Text;

    // Save Max Salary Balance
    if MaskEditMaxSalaryBalance.Text <>
      MaskEditMaxSalaryBalanceBackup.Text then
    begin
      HoursPerEmployeeDM.UpdateMaxSalaryBalanceMinutes(
        MaskEditMaxSalaryBalance.Text);
      MaskEditMaxSalaryBalanceBackup.Text := MaskEditMaxSalaryBalance.Text;
    end;
    dxBarBDBNavPost.Enabled := False;
    dxBarBDBNavCancel.Enabled := False;
  end
  else
  begin
    HoursPerEmployeeDM.SaveChanges;
    ReturnTotals;
    SetActiveGridMode(gdReadOnly);
    HoursPerEmployeeDM.FindCurrentRecord(TdxDBGrid(ActiveGrid).DataSource.DataSet);
  end;
  // MR:02-10-2003 Refresh is needed here
  // RV071.3. Also refresh for HolidayCounters.
  if (TActiveTab(PageControl1.ActivePageIndex) = SalaryHours) or
    (TActiveTab(PageControl1.ActivePageIndex) = HolidayCounters) then
    //RV067.10.
    RefreshData;
end;

procedure THoursPerEmployeeF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  // MR:31-12-2002 Norm in Totals-page has changed?
  if TActiveTab(PageControl1.ActivePageIndex) = Balances then
  begin
    // No save
    MaskEditHolidayNorm.Text := MaskEditHolidayNormBackup.Text;
    MaskEditMaxSalaryBalance.Text := MaskEditMaxSalaryBalanceBackup.Text;
    dxBarBDBNavPost.Enabled := False;
    dxBarBDBNavCancel.Enabled := False;
  end
  else
    if TActiveTab(PageControl1.ActivePageIndex) = HolidayCounters then
    begin
      // RV071.3. This is wrong! It must copy '.Text'.
      MaskEditSeniorityHldNorm.Text := MaskEditSeniorityHldNormBackup.Text;
      MaskEditRsvBnkHolidayNorm.Text := MaskEditRsvBnkHolidayNormBackup.Text;
      MaskEditShorterWeekEarned.Text := MaskEditShorterWeekEarnedBackup.Text;
//      MaskEditSeniorityHldNorm := MaskEditSeniorityHldNormBackup;
//      MaskEditRsvBnkHolidayNorm := MaskEditRsvBnkHolidayNormBackup;
      dxBarBDBNavPost.Enabled := False;
      dxBarBDBNavCancel.Enabled := False;
    end
    else
    begin
      TdxDbGrid(ActiveGrid).DataSource.DataSet.Cancel;
      SetActiveGridMode(gdReadOnly);
    end;
  // MR:02-10-2003 Refresh if in this mode.
  if TActiveTab(PageControl1.ActivePageIndex) = SalaryHours then
    //RV067.10.
    RefreshData;
end;
//REV065.2 added the function to be able to use it also with navigation buttons
procedure THoursPerEmployeeF.EmployeeChanged(Sender: TObject);
var
  CountryId, EmplNo: Integer;
begin
  EmplNo := HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(EmplNo), 0
  );
  if EmplNo <> HoursPerEmployeeDM.FilterData.Employee_Number then
  begin
    DefaultSetFilterData(Sender);
//RV067.10.
    RefreshData;
  end;
  LabelEmplDesc.Caption :=
    HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
  SystemDM.UpdateHourTypePerCountry(CountryId, HoursPerEmployeeDM.QueryHourType);
  HoursPerEmployeeDM.UpdateHourTypePerCountryLU(CountryId);
  SystemDM.UpdateAbsenceReasonPerCountry(CountryId, HoursPerEmployeeDM.QueryAbsenceReason);

  NotSetTotal;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditEmployeeCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
//REV065.2
  inherited;
  EmployeeChanged(Sender);
end;

//RV067.10.
procedure THoursPerEmployeeF.RefreshData;
var
  AFilter: TFilterData;
  DefaultYear,DefaultWeek: Word;
  AllowChange: Boolean;
begin
  if not FRefreshData then Exit;
  AFilter := HoursPerEmployeeDM.FilterData;
  AFilter.Active := True;
  HoursPerEmployeeDM.FilterData := AFilter;
  ReturnTotals;
  ActivateGridDataSource(True);
  if TActiveTab(PageControl1.ActivePageIndex) < Balances then
    SetActiveGridMode(gdReadOnly)
  else
  begin
    SetActiveGridMode(gdNone);
    SetBalances;
    PageControl1Changing(PageControl1, AllowChange);  // 20011800
  end;
//car 15-9-2003 user rights changes
  if Form_Edit_YN = ITEM_VISIBIL then
    MySetNotEditable; // 20013438

  // MR:08-01-2003
  ResetTimeEditColor;
  //Car 7-5-2003 save last values
  //CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;

  ListProcsF.WeekUitDat(SystemDM.ASaveTimeRecScanning.DateScanning,
   DefaultYear,DefaultWeek);
  if FEmptyDate or (DefaultYear <> dxSpinEditYear.IntValue) or
    (DefaultWeek <> dxSpinEditWeek.IntValue) then
    SystemDM.ASaveTimeRecScanning.DateScanning :=
      ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
        dxSpinEditWeek.IntValue,1);
  DoActiveAction; // 20013035        
end;

procedure THoursPerEmployeeF.ActivateGridDataSource(OnOff: boolean);
begin
  dxDBGridProductionHour.DataSource.Enabled := OnOff;
  dxDBGridAbsenceHour.DataSource.Enabled := OnOff;
  dxDBGridSalaryHour.DataSource.Enabled := OnOff;
  dxDBGridAbsenceHourS.DataSource.Enabled := OnOff;
  // MR:08-07-2003 - Don't do this with TDataClientSets!!!
//  dxDBGridSalaryHour.DataSource.DataSet.Active := OnOff;
//  dxDBGridProductionHour.DataSource.DataSet.Active := OnOff;
//  dxDBGridAbsenceHour.DataSource.DataSet.Active := OnOff;
end;

procedure THoursPerEmployeeF.DefaultChangeDate(Sender: TObject);
begin
  // MR:31-12-2002
  NormAskForSaveChanges;

  CorrectWeekComponent;

  HoursPerEmployeeDM.SetWeekDates(dxSpinEditYear.IntValue,
    dxSpinEditWeek.IntValue);
  ShowWeekDates;

  DefaultSetFilterData(Sender);
  //RV067.10.
  RefreshData;
end;

procedure THoursPerEmployeeF.DefaultGridEnter(Sender: TObject);
begin
  inherited;
  ActiveGrid := TdxDBGrid(Sender);
  if Sender=dxDBGridProductionHour then
  begin
    GroupBoxAbsenceHour.Visible := False;
    GroupBoxSalaryHour.Visible := False;
    GroupBoxProductionHour.Visible := True;
  end;
  if (Sender=dxDBGridAbsenceHour) or (Sender=dxDBGridAbsenceHourS) then
  begin
    // RV079.6. Do this first, otherwise it is not refreshed correct,
    //          when called from Employee Regs.
    GroupBoxProductionHour.Visible := False;
    GroupBoxAbsenceHour.Visible := False;
    GroupBoxSalaryHour.Visible := True;

    GroupBoxSalaryHour.Visible := False;
    GroupBoxProductionHour.Visible := False;
    GroupBoxAbsenceHour.Visible := True;
  end;
  if (Sender=dxDBGridSalaryHour) then
  begin
    GroupBoxProductionHour.Visible := False;
    GroupBoxAbsenceHour.Visible := False;
    GroupBoxSalaryHour.Visible := True;
  end;
// car 15-9-2003 - user rights changes
  if Form_Edit_YN = ITEM_VISIBIL then
    SetNotEnabled;
end;

procedure THoursPerEmployeeF.SetBalances;
var
  Remaining, Normal, Earned, Used, Planned, Available, CounterDesc: String;
  GroupVisible: Boolean;
  DefaultVisible: Boolean;
  AFilter: TFilterData;
begin
  AFilter := HoursPerEmployeeDM.FilterData;
  TabSheet4.Visible := False;
  try
    if HoursPerEmployeeDM.cdsFilterEmployee.Active and
      (not HoursPerEmployeeDM.cdsFilterEmployee.IsEmpty) then
      EditStartDate.Text := FormatDateTime('dd/mm/yyyy',
        HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('STARTDATE').AsDateTime)
    else
      EditStartDate.Text := '';

    //RV067.4.
    //Get all counters here
//    HoursPerEmployeeDM.GetCounters;

    ABalanceCounters.EmployeeNumber := AFilter.Employee_Number;
    ABalanceCounters.Year := AFilter.Year;
    ABalanceCounters.DateFrom := Trunc(Now);

    // RV079.4. Do this AFTER initializing the parameters!
    DefaultVisible := not //HoursPerEmployeeDM.
      ABalanceCounters.AbsenceTypePerCountryExist;

    ABalanceCounters.GetCounters;

    // RV071.5.
    // Make all groupboxes invisible in reverse order.
    GroupBoxShorterWeek.Visible := False;
    GroupBoxRsvBnkHoliday.Visible := False;
    GroupBoxSatCredit.Visible := False;
    GroupBoxAddBnkHoliday.Visible := False;
    GroupBoxSeniorityHld.Visible := False;
    GroupBoxHldPrevYear.Visible := False;
    GroupBox5.Visible := False;
    GroupBox2.Visible := False;
    GroupBoxTravelTime.Visible := False; // 20013183

    // RV067.MRA.30 - Make some extra visible or not.
    // Holiday
    CounterDesc := HolidayCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible := //HoursPerEmployeeDM.
        ABalanceCounters.GetCounterActivated(HOLIDAYAVAILABLETB, CounterDesc);
    GroupBox2.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox2.Caption := CounterDesc;
      // If absence type 'R' is activated as counter then 'remaining last year'
      // must not be shown for Holiday
      EditHolidayRemaining.Visible :=
        not //HoursPerEmployeeDM.
          ABalanceCounters.GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc);
      LabelHolidayRemaining.Visible := EditHolidayRemaining.Visible;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeHoliday(Remaining, Normal, Used, Planned,
        Available, LabelHolidayRemaining.Visible);
      EditHolidayRemaining.Text := Remaining;
      MaskEditHolidayNorm.Text := Normal;
      // MR:31-12-2002 Keep current value for comparison later
      MaskEditHolidayNormBackup.Text := Normal;

      EditHolidayUsed.Text := Used;
      EditHolidayPlanned.Text := Planned;
      EditHolidayAvailable.Text := Available;
    end;

    // RV067.MRA.30
    // Time for Time
    CounterDesc := TimeForTimeCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible := //HoursPerEmployeeDM.
        ABalanceCounters.GetCounterActivated(TIMEFORTIMEAVAILABILITY, CounterDesc);
    GroupBox5.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox5.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeTimeForTime(Remaining, Normal, Used, Planned,
        Available);
      EditTimeRemaining.Text := Remaining;
      EditTimeEarned.Text := Normal;
      EditTimeUsed.Text := Used;
      EditTimePlanned.Text := Planned;
      EditTimeAvailable.Text := Available;
    end;

    // RV067.MRA.30
    // Work time reduction
    CounterDesc := WorkTimeReductionCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible := //HoursPerEmployeeDM.
        ABalanceCounters.GetCounterActivated(WORKTIMEREDUCTIONAVAILABILITY, CounterDesc);
    GroupBox3.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox3.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeWorkTime(Remaining, Normal, Used, Planned,
        Available);
      EditWorkRemaining.Text := Remaining;
      EditWorkEarned.Text := Normal;
      EditWorkUsed.Text := Used;
      EditWorkPlanned.Text := Planned;
      EditWorkAvailable.Text := Available;
    end;

    //Holiday previous year
    CounterDesc := HolidayPreviousYearCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc);
    GroupBoxHldPrevYear.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxHldPrevYear.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeHldPrevYear(Remaining, Used, Planned,
        Available);
      EditHldPrevYearRemaining.Text := Remaining;
      EditHldPrevYearUsed.Text := Used;
      EditHldPrevYearPlanned.Text := Planned;
      EditHldPrevYearAvailable.Text := Available;
    end;

    //Seniority Holiday
    CounterDesc := SeniorityHolidayCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(SENIORITYHOLIDAYAVAILABILITY, CounterDesc);
    GroupBoxSeniorityHld.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxSeniorityHld.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeSeniorityHld(Normal, Used, Planned,
        Available);
      MaskEditSeniorityHldNorm.Text := Normal;
      MaskEditSeniorityHldNormBackup.Text := Normal;
      EditSeniorityHldUsed.Text := Used;
      EditSeniorityHldPlanned.Text := Planned;
      EditSeniorityHldAvailable.Text := Available;
    end;

    //Additional Bank Holiday
    CounterDesc := AddBankHolidayCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(ADDBANKHOLIDAYAVAILABILITY, CounterDesc);
    GroupBoxAddBnkHoliday.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxAddBnkHoliday.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeAddBnkHoliday(Remaining, Used, Planned,
        Available);
      EditAddBnkHolidayRemaining.Text := Remaining;
      EditAddBnkHolidayUsed.Text := Used;
      EditAddBnkHolidayPlanned.Text := Planned;
      EditAddBnkHolidayAvailable.Text := Available;
    end;

    //Saturday credit
    CounterDesc := SatCreditCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(SATCREDITAVAILABILITY, CounterDesc);
    GroupBoxSatCredit.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxSatCredit.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeSatCredit(Remaining, Earned, Used, Planned,
        Available);
      // RV071.10. Do not show remaining.
//      EditSatCreditRemaining.Text := Remaining;
      // RV071.10. Addition of earned.
      EditSatCreditEarned.Text := Earned;
      EditSatCreditUsed.Text := Used;
      EditSatCreditPlanned.Text := Planned;
      EditSatCreditAvailable.Text := Available;
    end;

    // 20013183
    // Travel Time
    CounterDesc := TravelTimeCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(TRAVELTIMEAVAILABILITY, CounterDesc);
    GroupBoxTravelTime.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxTravelTime.Caption := CounterDesc;
      ABalanceCounters.ComputeTravelTime(Remaining, Earned, Used, Planned,
        Available);

      EditTravelTimeRemaining.Text := Remaining;
      EditTravelTimeEarned.Text := Earned;
      EditTravelTimeUsed.Text := Used;
      EditTravelTimePlanned.Text := Planned;
      EditTravelTimeAvailable.Text := Available;
    end;

    //Bank holiday to reserve
    CounterDesc := BankHolToRsvCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(BANKHOLIDAYRESERVEAVAILABILITY, CounterDesc);
    GroupBoxRsvBnkHoliday.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxRsvBnkHoliday.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeRsvBnkHoliday(Normal, Used, Planned,
        Available);
      EditRsvBnkHolidayUsed.Text := Used;
      EditRsvBnkHolidayPlanned.Text := Planned;
      EditRsvBnkHolidayAvailable.Text := Available;
      MaskEditRsvBnkHolidayNorm.Text := Normal;
      MaskEditRsvBnkHolidayNormBackup.Text := Normal;
      // RV071.3.
      EditNormBankHldResMinManYN.Text :=
        //HoursPerEmployeeDM.
        ABalanceCounters.DetermineNormBankHldResMinManYN;
      if EditNormBankHldResMinManYN.Text = 'Y' then
        MaskEditRsvBnkHolidayNorm.Color := clRed
      else
        MaskEditRsvBnkHolidayNorm.Color := clWindow;
    end;

    //Shorter working week
    CounterDesc := ShorterWorkingWeekCaption;
    GroupVisible := //HoursPerEmployeeDM.
      ABalanceCounters.GetCounterActivated(SHORTERWORKWEEKAVAILABILITY, CounterDesc);
    GroupBoxShorterWeek.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxShorterWeek.Caption := CounterDesc;
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeShorterWeek(Remaining, Earned, Used, Planned,
        Available);
      EditShorterWeekRemaining.Text := Remaining;
      MaskEditShorterWeekEarned.Text := Earned;
      MaskEditShorterWeekEarnedBackup.Text := Earned;
      EditShorterWeekUsed.Text := Used;
      EditShorterWeekPlanned.Text := Planned;
      EditShorterWeekAvailable.Text := Available;
    end;

    // Illness
    //HoursPerEmployeeDM.
    ABalanceCounters.ComputeIllness(Remaining, Normal);
    EditIllness.Text := Remaining;   // currentyear
    EditIllnessUntil.Text := Normal; // previousyears
    // MR:09-08-2004 Order 550333
    // Get 'Max Salary Balance Minutes' in hours:minutes
    MaskEditMaxSalaryBalance.Text :=
      //HoursPerEmployeeDM.
      ABalanceCounters.ComputeMaxSalaryBalanceMinutes;
    MaskEditMaxSalaryBalanceBackup.Text :=
      MaskEditMaxSalaryBalance.Text;
  finally
    TabSheet4.Visible := True;
  end;
end; // SetBalances

procedure THoursPerEmployeeF.ResetBalances;
  procedure ClearEdit(AEdit: TCustomEdit);
  begin
    AEdit.Text := NullTime;
  end;
begin
  EditStartDate.Text := '';
  ClearEdit(EditHolidayRemaining);ClearEdit(MaskEditHolidayNorm);
  // MR:31-12-2002
  ClearEdit(MaskEditHolidayNormBackup);
  ClearEdit(EditHolidayUsed);  ClearEdit(EditHolidayPlanned);
  ClearEdit(EditHolidayAvailable);

  ClearEdit(EditWorkRemaining); ClearEdit(EditWorkEarned);
  ClearEdit(EditWorkUsed); ClearEdit(EditWorkPlanned);
  ClearEdit(EditWorkAvailable);

  ClearEdit(EditTimeRemaining); ClearEdit(EditTimeEarned);
  ClearEdit(EditTimeUsed); ClearEdit(EditTimePlanned);
  ClearEdit(EditTimeAvailable);

  ClearEdit(EditIllness); ClearEdit(EditIllnessUntil);

  ClearEdit(EditHldPrevYearRemaining); ClearEdit(EditHldPrevYearUsed);
  ClearEdit(EditHldPrevYearPlanned); ClearEdit(EditHldPrevYearAvailable);

  ClearEdit(MaskEditSeniorityHldNorm); ClearEdit(EditSeniorityHldUsed);
  ClearEdit(EditSeniorityHldPlanned); ClearEdit(EditSeniorityHldAvailable);
  ClearEdit(MaskEditSeniorityHldNormBackup);

  ClearEdit(EditAddBnkHolidayRemaining); ClearEdit(EditAddBnkHolidayUsed);
  ClearEdit(EditAddBnkHolidayPlanned); ClearEdit(EditAddBnkHolidayAvailable);

  ClearEdit(EditSatCreditRemaining); ClearEdit(EditSatCreditUsed);
  ClearEdit(EditSatCreditPlanned); ClearEdit(EditSatCreditAvailable);
  ClearEdit(EditSatCreditEarned);

  ClearEdit(EditRsvBnkHolidayUsed);
  ClearEdit(EditRsvBnkHolidayPlanned); ClearEdit(EditRsvBnkHolidayAvailable);
  ClearEdit(MaskEditRsvBnkHolidayNorm); ClearEdit(MaskEditRsvBnkHolidayNormBackup);

  ClearEdit(EditShorterWeekRemaining); ClearEdit(MaskEditShorterWeekEarned);
  ClearEdit(EditShorterWeekUsed); ClearEdit(EditShorterWeekPlanned);
  ClearEdit(EditShorterWeekAvailable); ClearEdit(MaskEditShorterWeekEarnedBackup);

  ClearEdit(MaskEditMaxSalaryBalance); // MR:23-09-2004
  ClearEdit(MaskEditMaxSalaryBalanceBackup); // MR:23-09-2004

  // 20013183
  ClearEdit(EditTravelTimeRemaining); ClearEdit(EditTravelTimeUsed);
  ClearEdit(EditTravelTimePlanned); ClearEdit(EditTravelTimeAvailable);
  ClearEdit(EditTravelTimeEarned);
end;

procedure THoursPerEmployeeF.ResetTimeEditColor;
begin
  // Reset all signs from Production tab
  if ActiveGrid = dxDBGridProductionHour then
  begin
    dxDBTimeEditProdDay1.Color := PositiveTimeColor;
    dxDBTimeEditProdDay2.Color := PositiveTimeColor;
    dxDBTimeEditProdDay3.Color := PositiveTimeColor;
    dxDBTimeEditProdDay4.Color := PositiveTimeColor;
    dxDBTimeEditProdDay5.Color := PositiveTimeColor;
    dxDBTimeEditProdDay6.Color := PositiveTimeColor;
    dxDBTimeEditProdDay7.Color := PositiveTimeColor;
    // MR:08-01-2003 Also do this, because on tabpage
    // both grids are shown. (prod + absence)
    dxDBTimeEditAbsence1.Color := PositiveTimeColor;
    dxDBTimeEditAbsence2.Color := PositiveTimeColor;
    dxDBTimeEditAbsence3.Color := PositiveTimeColor;
    dxDBTimeEditAbsence4.Color := PositiveTimeColor;
    dxDBTimeEditAbsence5.Color := PositiveTimeColor;
    dxDBTimeEditAbsence6.Color := PositiveTimeColor;
    dxDBTimeEditAbsence7.Color := PositiveTimeColor;
  end;
  if ActiveGrid = dxDBGridSalaryHour then
  begin
    dxDBTimeEditSalary1.Color := PositiveTimeColor;
    dxDBTimeEditSalary2.Color := PositiveTimeColor;
    dxDBTimeEditSalary3.Color := PositiveTimeColor;
    dxDBTimeEditSalary4.Color := PositiveTimeColor;
    dxDBTimeEditSalary5.Color := PositiveTimeColor;
    dxDBTimeEditSalary6.Color := PositiveTimeColor;
    dxDBTimeEditSalary7.Color := PositiveTimeColor;
    // MR:08-01-2003 Also do this, because on this tabpage
    // both grids are shown. (sal + absence)
    dxDBTimeEditAbsence1.Color := PositiveTimeColor;
    dxDBTimeEditAbsence2.Color := PositiveTimeColor;
    dxDBTimeEditAbsence3.Color := PositiveTimeColor;
    dxDBTimeEditAbsence4.Color := PositiveTimeColor;
    dxDBTimeEditAbsence5.Color := PositiveTimeColor;
    dxDBTimeEditAbsence6.Color := PositiveTimeColor;
    dxDBTimeEditAbsence7.Color := PositiveTimeColor;
  end;
  if (ActiveGrid = dxDBGridAbsenceHour) or
    (ActiveGrid = dxDBGridAbsenceHourS) then
  begin
    dxDBTimeEditAbsence1.Color := PositiveTimeColor;
    dxDBTimeEditAbsence2.Color := PositiveTimeColor;
    dxDBTimeEditAbsence3.Color := PositiveTimeColor;
    dxDBTimeEditAbsence4.Color := PositiveTimeColor;
    dxDBTimeEditAbsence5.Color := PositiveTimeColor;
    dxDBTimeEditAbsence6.Color := PositiveTimeColor;
    dxDBTimeEditAbsence7.Color := PositiveTimeColor;
  end;
end;

procedure THoursPerEmployeeF.ReturnTotals;
var
  TotalValues: TDailyTime;
  TotalDay, TotalPDay, TotalSDay: TWeekDaysTotal;
begin
  if
    (HoursPerEmployeeDM.FilterData.ActiveTab = Balances) or
    (HoursPerEmployeeDM.FilterData.ActiveTab = HolidayCounters)
  then exit;
  HoursPerEmployeeDM.ComputeTotal(dxSpinEditYear.IntValue,
    dxSpinEditWeek.IntValue,
    TotalValues, TotalDay, TotalPDay, TotalSDay);
  SetTotals(TotalValues, TotalDay, TotalPDay, TotalSDay);
end;

procedure THoursPerEmployeeF.ResetTotals;
var
  Index: Integer;
begin
  for Index := 1 to dxDBGridPTotal.columncount - 1 do
    dxDBGridPTotal.Columns[Index].Caption := NullTime;

  for Index := 1 to dxDBGridSTotal.columncount - 1 do
    dxDBGridSTotal.Columns[Index].Caption := NullTime;
end;

procedure THoursPerEmployeeF.SetEditTimeSign(WeekTimeSigns: TWeekDaysTotal);
var
  Index: integer;
  CurrentCmp: TComponent;
  ObjectNameMask: string;
begin
  if ActiveGrid = dxDBGridProductionHour then
    ObjectNameMask := EditTimeObjNameMaskProd;
  if ActiveGrid = dxDBGridSalaryHour then
    ObjectNameMask := EditTimeObjNameMaskSal;
  if (ActiveGrid = dxDBGridAbsenceHour) or
    (ActiveGrid = dxDBGridAbsenceHourS)then
    ObjectNameMask := EditTimeObjNameMaskAbs;

  for Index:=1 to 7 do
  begin
    CurrentCmp := FindComponent(Format(ObjectNameMask+'%d',[Index]));
    if Assigned(CurrentCmp) then
    begin
      if WeekTimeSigns[index] >= 0 then
        TdxDBTimeEdit(CurrentCmp).Color := PositiveTimeColor
      else
        TdxDBTimeEdit(CurrentCmp).Color := NegativeTimeColor;
    end; // if
  end; // for
end;

procedure THoursPerEmployeeF.dxDBGridDefaultCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  index: integer;
  DataSet: TDataSet;
  Grd: TdxDBGrid;
begin
  inherited;
  //REV065.9
  Grd := TdxDBGrid(Sender);
  DataSet := Grd.DataSource.DataSet;
  if ASelected and (not AFocused) then
  begin
    AColor := clGridNoFocus;
    AFont.Color := clBlack;
  end;
  if
    (DataSet.FieldByName('MANUAL_YN').asString = UNCHECKEDVALUE) and
    (not ASelected)
  then
    AColor := clDisabled;

  // RV073.7. Disabled this part.
{
  //REV065.9
  if
    Assigned(DataSet.FindField('FROMMANUALPROD_YN')) and
    (DataSet.FieldByName('FROMMANUALPROD_YN').AsString = CHECKEDVALUE) and
    (not ASelected)
  then
    AColor := clOrange;
}    
  for index := 1 to 7 do
  if ((AColumn.Name = Format(ProductionHourColName+'%d',[index])) or
    (AColumn.Name = Format(SalaryHourColName + '%d',[index])) or
    (AColumn.Name = Format(AbsenceHourColName + '%d',[index])) or
    (AColumn.Name = Format(AbsenceHourSColName + '%d',[index]))) and
    (HoursPerEmployeeDM.GetDailyMinutes(DataSet, index) < 0) then
    AText := Format('-%s',[AText]) ;
  if ASelected then
    if AColumn = dxDBGridProductionHourColumnWorkspot then
      lblWorkspotDescription.Caption := AText;
end;

procedure THoursPerEmployeeF.DefaultFormatTotalCol(Sender: TObject;
  ANode: TdxTreeListNode; var AText: String);
begin
  if length(AText) = 0 then exit;
  try
    AText := IntMin2StringTime(StrToInt(AText),true);
  except
    Atext := NullTime;
  end;
end;

procedure THoursPerEmployeeF.dxDBGridProductionHourClick(Sender: TObject);
begin
  inherited;
  HoursPerEmployeeDM.DefaultAfterScroll(HoursPerEmployeeDM.cdsHREPRODUCTION);
end;

procedure THoursPerEmployeeF.DefaultGridClick(Sender: TObject);
var
  Index: integer;
  DataSet: TDataSet;
  DailyTime: TWeekDaysTotal;
begin
  DataSet := TdxDBGrid(Sender).DataSource.DataSet;
  dsrcActive.DataSet := TdxDBGrid(Sender).DataSource.DataSet;
  for index:=1 to 7 do
    DailyTime[index] := HoursPerEmployeeDM.GetDailyMinutes(DataSet,index);
  SetEditTimeSign(DailyTime);
  if dxBarBDBNavPost.Enabled and (DisplayMessage(SPimsSaveChanges,
    mtConfirmation, [mbYes, mbNo])=mrYes) then
  begin
    HoursPerEmployeeDM.SaveChanges;
    ReturnTotals;
    HoursPerEmployeeDM.FindCurrentRecord(TdxDBGrid(ActiveGrid).
      DataSource.DataSet);
  end;
  HoursPerEmployeeDM.DefaultAfterScroll(DataSet);
  GridChangeRecord(Sender,Nil,Nil);

  // car 15-9-2003 - user rights changes
  if Form_Edit_YN = ITEM_VISIBIL then
    SetNotEnabled
  else
    FinalRunEnableEditComponents; // 20011800
end;

procedure THoursPerEmployeeF.NotSetTotal;
var
  Index: Integer;
begin
  if HoursPerEmployeeDM.FilterData.ActiveTab = ProductionHours then
  begin
    for Index := 1 to dxDBGridPTotal.columncount - 1 do
    begin
      if (Index in [1.. 7]) then
        dxDBGridPTotal.Columns[Index].Tag := 1;
      if Index = 8 then
        dxDBGridPTotal.Columns[Index].Tag := 1;
    end;
  end
  else
  begin
    for Index := 1 to dxDBGridSTotal.columncount - 1 do
    begin
      if (Index in [1.. 7]) then
        dxDBGridSTotal.Columns[Index].Tag := 1;
      if Index = 8 then
        dxDBGridSTotal.Columns[Index].Tag := 1;
    end;
  end;
end;

procedure THoursPerEmployeeF.SetTotals(TotalValues: TDailyTime;
  TotalDay, TotalPDay, TotalSDay: TWeekDaysTotal);
var
  Index, TotalP, TotalS, TotalEmpl: Integer;
begin
  if HoursPerEmployeeDM.FilterData.ActiveTab = ProductionHours then
  begin
    TotalP := 0; TotalEmpl := 0;
    for Index := 1 to 7 do
    begin
      TotalP := TotalP +  TotalPDay[Index];
      TotalEmpl := TotalEmpl + TotalDay[Index];
    end;
    for index := 1 to dxDBGridPTotal.columncount - 1 do
    begin
      dxDBGridPTotal.Columns[Index].Caption := TotalValues[Index];
      if (Index in [1.. 7]) then
      begin
        if TotalPDay[Index] <> TotalDay[Index] then
          dxDBGridPTotal.Columns[Index].Tag := 1
        else
          dxDBGridPTotal.Columns[Index].Tag := 2;
      end;
      if Index = 8 then
        if TotalP <> TotalEmpl then
          dxDBGridPTotal.Columns[Index].Tag := 1
        else
          dxDBGridPTotal.Columns[Index].Tag := 2;
    end;
  end
  else
  begin
    TotalS := 0;  TotalEmpl := 0;
    for Index := 1 to 7 do
    begin
      TotalS := TotalS +  TotalSDay[Index];
      TotalEmpl := TotalEmpl + TotalDay[Index];
    end;
    for index := 1 to dxDBGridSTotal.columncount - 1 do
    begin
      dxDBGridSTotal.Columns[Index].Caption := TotalValues[Index];
      if (Index in [1.. 7]) then
      begin
        if TotalSDay[Index] <> TotalDay[Index] then
          dxDBGridSTotal.Columns[Index].Tag := 1
        else
          dxDBGridSTotal.Columns[Index].Tag := 2;
      end;
      if Index = 8 then
        if TotalS <> TotalEmpl then
          dxDBGridSTotal.Columns[Index].Tag := 1
        else
          dxDBGridSTotal.Columns[Index].Tag := 2;
    end;
  end;
end;

procedure THoursPerEmployeeF.GridChangeRecord(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
var
  ADataSet: TDataSet;
begin
  if not (Sender is TdxDBGrid) then
    Exit;
  ADataSet := TdxDBGrid(Sender).DataSource.DataSet;
  if ADataSet.FieldByName('MANUAL_YN').asString = CHECKEDVALUE then
    SetActiveGridMode(gdBrowse)
  else
    SetActiveGridMode(gdReadOnly);

  // RV073.7. Disabled this part.
{
  //REV065.9 if the salary record comes from a manually entered production record then
  //its read only
  if
    Assigned(ADataSet.FindField('FROMMANUALPROD_YN')) and
    (ADataSet.FieldByName('FROMMANUALPROD_YN').AsString = CHECKEDVALUE)
  then
    SetActiveGridMode(gdReadOnly);
}    
end;

procedure THoursPerEmployeeF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  index: integer;
  DataSet: TDataSet;
begin
//  inherited;
  DataSet := TdxDBGrid(Sender).DataSource.DataSet;
  if ASelected and (not AFocused) then
  begin
    AColor := clGridNoFocus;
    AFont.Color := clBlack;
  end;
  if (DataSet.FieldByName('MANUAL_YN').asString = UNCHECKEDVALUE) and
    (not ASelected) then
    AColor := clDisabled;
  for index := 1 to 7 do
    if ((AColumn.Name = Format(ProductionHourColName+'%d',[index])) or
      (AColumn.Name = Format(SalaryHourColName + '%d',[index])) or
      (AColumn.Name = Format(AbsenceHourColName + '%d',[index])) or
      (AColumn.Name = Format(AbsenceHourSColName + '%d',[index]))) and
      (HoursPerEmployeeDM.GetDailyMinutes(DataSet, index) < 0) then
      AText := Format('-%s',[AText]) ;
end;

procedure THoursPerEmployeeF.pnlDetailExit(Sender: TObject);
begin
  inherited;
  NormAskForSaveChanges;
  ReturnTotals;
end;

procedure THoursPerEmployeeF.NormAskForSaveChanges;
begin
  if
    (TActiveTab(PageControl1.ActivePageIndex) = Balances) or
    (TActiveTab(PageControl1.ActivePageIndex) = HolidayCounters)
    then
  begin
    if dxBarBDBNavPost.Enabled then // 20011800
      if (MaskEditHolidayNorm.Text <> MaskEditHolidayNormBackup.Text) or
        (MaskEditMaxSalaryBalance.Text <> MaskEditMaxSalaryBalanceBackup.Text) or
        (MaskEditSeniorityHldNorm.Text <> MaskEditSeniorityHldNormBackup.Text) or
        (MaskEditRsvBnkHolidayNorm.Text <> MaskEditRsvBnkHolidayNormBackup.Text) or
        (MaskEditShorterWeekEarned.Text <> MaskEditShorterWeekEarnedBackup.Text)
        then
      begin
        if (DisplayMessage(SPimsSaveChanges, mtConfirmation,
          [mbYes, mbNo])= mrYes) then
          dxBarBDBNavPostClick(nil)
        else
          dxBarBDBNavCancelClick(nil);
      end;
  end
  else
  begin
    if dxBarBDBNavPost.Enabled then
      if (DisplayMessage(SPimsSaveChanges, mtConfirmation,
        [mbYes, mbNo])= mrYes) then
        dxBarBDBNavPostClick(nil)
      else
        dxBarBDBNavCancelClick(nil);
  end;
end;

procedure THoursPerEmployeeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // MR:31-12-2002
  NormAskForSaveChanges;
  //CAR 21-7-2003
//  TBLenghtClass.Free;
end;

procedure THoursPerEmployeeF.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  inherited;
  // MR:31-12-2002
  NormAskForSaveChanges;
  // MR:15-12-2004
  MaskEditMaxSalaryBalance.Enabled := not (Form_Edit_YN = ITEM_VISIBIL);
  if MaskEditMaxSalaryBalance.Enabled then
    MaskEditMaxSalaryBalance.Enabled := FinalRunYearCheck; // 20011800
  MaskEditHolidayNorm.Enabled := not (Form_Edit_YN = ITEM_VISIBIL);
  if MaskEditHolidayNorm.Enabled then
    MaskEditHolidayNorm.Enabled := FinalRunYearCheck; // 20011800
  if (Form_Edit_YN = ITEM_VISIBIL) then
  begin
    MaskEditMaxSalaryBalance.Color := clDisabled;
    MaskEditHolidayNorm.Color := clDisabled;
  end
  else
    if (not FinalRunYearCheck) then // 20011800
    begin
      MaskEditMaxSalaryBalance.Color := clDisabled;
      MaskEditHolidayNorm.Color := clDisabled;
    end
    else
    begin
      MaskEditMaxSalaryBalance.Color := clWindow;
      MaskEditHolidayNorm.Color := clWindow;
    end;
  NormEditSysAdminEnabler; // 20013438.70
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditEmployeeEnter(
  Sender: TObject);
begin
  inherited;
  // MR:31-12-2002
  NormAskForSaveChanges;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditEmployeeClick(
  Sender: TObject);
begin
  inherited;
  dsrcActive.DataSet := dxDBExtLookupEditEmployee.DataSource.DataSet;
end;

procedure THoursPerEmployeeF.dxBarButtonNavigationClick(Sender: TObject);
  procedure GotoMasterTable (Which: Integer);
  begin
    case Which of
      NAV_FIRST : HoursPerEmployeeDM.cdsFilterEmployee.First;
      NAV_PRIOR : HoursPerEmployeeDM.cdsFilterEmployee.Prior;
      NAV_NEXT  : HoursPerEmployeeDM.cdsFilterEmployee.Next;
      NAV_LAST  : HoursPerEmployeeDM.cdsFilterEmployee.Last;
    end;
    LabelEmplDesc.Caption :=
      HoursPerEmployeeDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
    //RV067.10.
    RefreshData;
  end;
begin
  if dxBarDBNavigator.DataSource.DataSet.Name = 'cdsFilterEmployee' then
  begin
    GotoMasterTable((Sender as TdxBarButton).Tag);
    Exit;
  end
  else
    inherited;
end;

procedure THoursPerEmployeeF.dxDBGridPTotalCustomDrawColumnHeader(
  Sender: TObject; AColumn: TdxTreeListColumn; ACanvas: TCanvas;
  ARect: TRect; var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
  var ADone: Boolean);
begin
  inherited;
  if AColumn.Tag = 1  then
    AColor := clScanScrap
  else
    AColor := clScanRouteP1;
end;

procedure THoursPerEmployeeF.dxBarButtonResetColumnsClick(Sender: TObject);
begin
  Exit;
  inherited;
end;

// MR:04-12-2003 Set max of weeks and correct the component that handles
//               the week-selection
procedure THoursPerEmployeeF.CorrectWeekComponent;
begin
  try
    // Set max of weeks
    dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    // Correct the week-selection-component
    if dxSpinEditWeek.Value > dxSpinEditWeek.MaxValue then
      dxSpinEditWeek.Value := dxSpinEditWeek.MaxValue;
  except
    dxSpinEditWeek.Value := DefaultWeek;
  end;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditWorkspotPopup(
  Sender: TObject; const EditText: String);
begin
  inherited;
  with HoursPerEmployeeDM do
  begin
    if not TableWorkspot.FindKey([
      cdsHREPRODUCTION.FieldByName('PLANT_CODE').AsString,
      cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').AsString]) then
      TableWorkspot.Locate('PLANT_CODE',
        cdsHREPRODUCTION.FieldByName('PLANT_CODE').AsString, []);
  end;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditWorkspotKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditWorkspotEnter(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure THoursPerEmployeeF.dxDBExtLookupEditWorkspotExit(
  Sender: TObject);
begin
  inherited;
  // MR:10-05-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure THoursPerEmployeeF.BalancesChange(
  Sender: TObject);
begin
  inherited;
  if
    (TActiveTab(PageControl1.ActivePageIndex) = Balances) or
    (TActiveTab(PageControl1.ActivePageIndex) = HolidayCounters)
  then
    if (MaskEditHolidayNorm.Text <> MaskEditHolidayNormBackup.Text) or
      (MaskEditMaxSalaryBalance.Text <> MaskEditMaxSalaryBalanceBackup.Text) or
      (MaskEditSeniorityHldNorm.Text <> MaskEditSeniorityHldNormBackup.Text) or
      (MaskEditRsvBnkHolidayNorm.Text <> MaskEditRsvBnkHolidayNormBackup.Text) or
      (MaskEditShorterWeekEarned.Text <> MaskEditShorterWeekEarnedBackup.Text)
    then
    begin
      dxBarBDBNavPost.Enabled := True;
      dxBarBDBNavCancel.Enabled := True;
    end;
end;

procedure THoursPerEmployeeF.MaskEditKeyChange(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  BalancesChange(Sender);
end;

//REV065.2 BEGIN navigation button functions
procedure THoursPerEmployeeF.GoToRecord(Which: Integer);
begin
  case Which of
    NAV_FIRST : HoursPerEmployeeDM.cdsFilterEmployee.First;
    NAV_PRIOR : HoursPerEmployeeDM.cdsFilterEmployee.Prior;
    NAV_NEXT  : HoursPerEmployeeDM.cdsFilterEmployee.Next;
    NAV_LAST  : HoursPerEmployeeDM.cdsFilterEmployee.Last;
  end;
  EmployeeChanged(Self);
end;

procedure THoursPerEmployeeF.BitBtnFirstClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_FIRST);
  BitBtnFirst.Enabled := False;
  BitBtnPrev.Enabled := False;
  BitBtnNext.Enabled := True;
  BitBtnLast.Enabled := True;
end;

procedure THoursPerEmployeeF.BitBtnPrevClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_PRIOR);
  BitBtnFirst.Enabled := True;
  BitBtnNext.Enabled := True;
  BitBtnLast.Enabled := True;
end;

procedure THoursPerEmployeeF.BitBtnNextClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_NEXT);
  BitBtnFirst.Enabled := True;
  BitBtnPrev.Enabled := True;
  BitBtnLast.Enabled := True;
end;

procedure THoursPerEmployeeF.BitBtnLastClick(Sender: TObject);
begin
  inherited;
  GoToRecord(NAV_LAST);
  BitBtnFirst.Enabled := True;
  BitBtnPrev.Enabled := True;
  BitBtnNext.Enabled := False;
  BitBtnLast.Enabled := False;
end;
//REV065.2 END navigation button functions

//REV065.5
procedure THoursPerEmployeeF.edtHourTypeExit(Sender: TObject);
begin
  inherited;
  // RV071.16. Bugfixing.
  // Check if entered value is empty!
  // Use try-except to trap other possible problems.
  try
    if edtHourType.Text <> '' then
      if HoursPerEmployeeDM.QueryHourType.Locate('HOURTYPE_NUMBER',
        StrToInt(edtHourType.Text), []) then
        DBLookupComboBoxDetailSalary.KeyValue := StrToInt(edtHourType.Text);
  except
    // Ignore error.
  end;
end;

// 20013035
procedure THoursPerEmployeeF.DoActiveAction;
begin
  if HoursPerEmployeeDM.IsEmployeeActive(HoursPerEmployeeDM.cdsFilterEmployee,
    ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
        dxSpinEditWeek.IntValue,1)) then
  begin
    dxDBExtLookupEditEmployee.Color := clWindow;
    LabelEmplDesc.Enabled := True;
  end
  else
  begin
    dxDBExtLookupEditEmployee.Color := clScrollBar;
    LabelEmplDesc.Enabled := False;
  end;
end;

// 20011800
// For Final Run System:
// - Set Edit Components to Enabled or Disabled based on
//   Last-Export-Date.
// - Do this only for MANUAL hours!
procedure THoursPerEmployeeF.FinalRunEnableEditComponents;
var
  Index: Integer;
  CurrentCmp: TComponent;
  ObjectNameMask: String;
  DataSet: TDataSet;
  CanAction: Boolean;
  DisableCount: Integer;
begin
  if (Form_Edit_YN = ITEM_VISIBIL) then
    Exit;
  if not (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
    Exit;
  if SystemDM.UseFinalRun then
  begin
    if not Assigned(ActiveGrid) then
      Exit;
    try
      DataSet := TdxDBGrid(ActiveGrid).DataSource.DataSet;
    except
      Exit;
    end;
    if not Assigned(DataSet) then
      Exit;
    CanAction := (HoursPerEmployeeDM.FilterData.Active and
      (not DataSet.IsEmpty) and
      (DataSet.FieldByName('MANUAL_YN').asString = CHECKEDVALUE));
    if not CanAction then
      Exit;
    if HoursPerEmployeeDM.FilterData.ExportDate <> NullDate then
    begin
      if ActiveGrid = dxDBGridProductionHour then
        ObjectNameMask := EditTimeObjNameMaskProd;
      if ActiveGrid = dxDBGridSalaryHour then
        ObjectNameMask := EditTimeObjNameMaskSal;
      if (ActiveGrid = dxDBGridAbsenceHour) or
        (ActiveGrid = dxDBGridAbsenceHourS)then
        ObjectNameMask := EditTimeObjNameMaskAbs;

      DisableCount := 0;
      for Index := 1 to 7 do
      begin
        CurrentCmp := FindComponent(Format(ObjectNameMask+'%d', [Index]));
        if Assigned(CurrentCmp) then
        begin
          // Is the Day <= ExportDate?
          // -> disable the day-edit-component
          if ListProcsF.DateFromWeek(
            HoursPerEmployeeDM.FilterData.Year,
            HoursPerEmployeeDM.FilterData.Week, Index) <=
            HoursPerEmployeeDM.FilterData.ExportDate then
          begin
            TdxDBTimeEdit(CurrentCmp).Enabled := False;
            inc(DisableCount);
          end
          else
            TdxDBTimeEdit(CurrentCmp).Enabled := True;
        end; // if
      end; // for
      // 20011800.70 When not whole week disabled...
      if (DisableCount > 0) and (DisableCount < 7) then
        if ActiveGrid = dxDBGridSalaryHour then
        begin
          if not (dxDBGridSalaryHour.DataSource.State in [dsInsert]) then
          begin
            DBLookupComboBoxDetailSalary.Enabled := False;
            edtHourType.Enabled := False;
          end;
        end;
    end; // if
  end; // if
  NormEditSysAdminEnabler; // 20013438
end; // FinalRunEnableEditComponents

// 20011800
// Must the whole week be disabled because of the last-export-date?
function THoursPerEmployeeF.FinalRunWeekCheck: Boolean;
begin
  Result := True;
  if SystemDM.UseFinalRun then
  begin
    if HoursPerEmployeeDM.FilterData.ExportDate <> NullDate then
      if ListProcsF.DateFromWeek(
        HoursPerEmployeeDM.FilterData.Year,
        HoursPerEmployeeDM.FilterData.Week, 7) <=
        HoursPerEmployeeDM.FilterData.ExportDate then
        Result := False;
  end;
end; // FinalRunWeekCheck

// 20011800
// Must the balance be disabled because of the last-export-date?
function THoursPerEmployeeF.FinalRunYearCheck: Boolean;
var
  Year, Month, Day: Word;
begin
  Result := True;
  if SystemDM.UseFinalRun then
  begin
    if HoursPerEmployeeDM.FilterData.ExportDate <> NullDate then
    begin
      DecodeDate(HoursPerEmployeeDM.FilterData.ExportDate, Year, Month, Day);
      if HoursPerEmployeeDM.FilterData.Year < Year then
        Result := False;
    end;
  end;
end; // FinalRunYearCheck

// 20013438 Allow change of norm-fields only by system-admin.
procedure THoursPerEmployeeF.NormEditSysAdminEnabler;
begin
  if Form_Edit_YN = ITEM_VISIBIL_EDIT then
  begin
    MaskEditHolidayNorm.Enabled := SystemDM.SysAdmin;
    MaskEditSeniorityHldNorm.Enabled := SystemDM.SysAdmin;
    MaskEditRsvBnkHolidayNorm.Enabled := SystemDM.SysAdmin;
    MaskEditShorterWeekEarned.Enabled := SystemDM.SysAdmin;
  end;
end;

// 20013438 Related to this order:
// Set Norm-fields disabled!
procedure THoursPerEmployeeF.MySetNotEditable;
begin
  MaskEditHolidayNorm.Enabled := False;
  MaskEditSeniorityHldNorm.Enabled := False;
  MaskEditRsvBnkHolidayNorm.Enabled := False;
  MaskEditShorterWeekEarned.Enabled := False;
  SetNotEditable;
end;

// 20013035
procedure THoursPerEmployeeF.cBoxShowOnlyActiveClick(Sender: TObject);
begin
  inherited;
  with HoursPerEmployeeDM do
  begin
    ShowOnlyActive := cBoxShowOnlyActive.Checked;
    ShowOnlyActiveSwitch;
    DefaultChangeDate(Sender);
  end;
end;

end.
