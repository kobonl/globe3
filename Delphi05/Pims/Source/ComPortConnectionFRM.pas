unit ComPortConnectionFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, StdCtrls, Mask, DBCtrls,
  dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TComPortConnectionF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxDetailGridColumnCOMPORT: TdxDBGridSpinColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEditComputerName: TDBEdit;
    Label6: TLabel;
    dxDBSpinEditCOMPORT: TdxDBSpinEdit;
    dxDetailGridColumnSERIALDEVICE: TdxDBGridColumn;
    Label3: TLabel;
    DBLookupComboBoxSERIALDEVICE: TDBLookupComboBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function
  ComPortConnectionF: TComPortConnectionF;

implementation

{$R *.DFM}

uses
  SystemDMT, ComPortConnectionDMT;

var
  ComPortConnectionF_HND: TComPortConnectionF;

function ComPortConnectionF: TComPortConnectionF;
begin
  if (ComPortConnectionF_HND = nil) then
    ComPortConnectionF_HND := TComPortConnectionF.Create(Application);
  Result := ComPortConnectionF_HND;
end;

procedure TComPortConnectionF.FormDestroy(Sender: TObject);
begin
  inherited;
  ComPortConnectionF_HND := nil;
end;

procedure TComPortConnectionF.FormCreate(Sender: TObject);
begin
  ComPortConnectionDM := CreateFormDM(TComPortConnectionDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxMasterGrid.DataSource := ComPortConnectionDM.DataSourceMaster;
    dxDetailGrid.DataSource := ComPortConnectionDM.DataSourceDetail;
  end;
  inherited;
  dxDBSpinEditCOMPORT.DataSource := ComPortConnectionDM.DataSourceDetail;
  dxDBSpinEditCOMPORT.DataField := 'COMPORT';

end;

procedure TComPortConnectionF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TComPortConnectionF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  if pnlDetail.Visible then
    DBLookupComboBoxSERIALDEVICE.SetFocus;
end;

procedure TComPortConnectionF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  if pnlDetail.Visible then
    DBLookupComboBoxSERIALDEVICE.SetFocus;
end;

end.
