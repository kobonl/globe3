(*
  MRA:27-NOV-2009 RV045.1.
  - Check for TFT-settings on Hourtype-level.
    If this is found, then disable these settings on
    ContractGroup-level.
  MRA:2-DEC-2009 RV046.01. Order 550476.
  - Add field 'Raise Percentage' in Contract group.
    This is only stored in database, but not used anywhere else.
  MRA:5-MAR-2010. RV055.2.
  - Field RAISE_PERCENTAGE must not be mandatory!
  MRA:26-AUG-2010. RV067.MRA.10. Bugfix.
  - Set 2 extra boolean-fields to 'N'.
  - NOTE: When a Contract Group is first added and then deleted, without
          leaving the dialog, error 10259 (Couldn�t perform the edit
          because another user changed the record.) occurs.
          This problems also occurs when you want to change the record
          (without reopening the dialog).
  - SOLVED: Solved by setting TableDetail.UpdateMode to 'upWhereKeyOnly',
            instead of 'upWhereAll'
  MRA:8-OCT-2010. RV071.10. 550497 Additions for Saturday Credit.
  - Addition of field 'Max. hrs. Saturday Credit'.
  MRA:24-MAR-2011. RV088.1. SO-20011510.20 Export payroll AFAS.
  - Addition of field EXPORT_NORMAL_HRS_YN to contractgroup.
    Only visible for export payroll AFAS.
    Default: 'N'.
  MRA:11-NOV-2011. RV101.1. 20012331. Export AFAS.
  - Addition of checkbox 'Export worked days'.
  - Should only be visible for 'Export AFAS'.
  - Default: 'Y'.
  MRA:2-DEC-2011. RV103.1. SO-20011799. Worked hours during bank holiday.
  - Addition of hourtype for 'worked hours during bank holiday'.
  - Can be NULL.
  MRA:14-JAN-2019 GLOB3-204
  - Personal Time Off USA
*)

unit ContractGroupDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TContractGroupDM = class(TGridBaseDM)
    TableHourtype: TTable;
    TableHourtypeHOURTYPE_NUMBER: TIntegerField;
    TableHourtypeDESCRIPTION: TStringField;
    TableHourtypeCREATIONDATE: TDateTimeField;
    TableHourtypeOVERTIME_YN: TStringField;
    TableHourtypeMUTATIONDATE: TDateTimeField;
    TableHourtypeMUTATOR: TStringField;
    TableHourtypeCOUNT_DAY_YN: TStringField;
    TableHourtypeEXPORT_CODE: TStringField;
    TableHourtypeBONUS_PERCENTAGE: TFloatField;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterTIME_FOR_TIME_YN: TStringField;
    TableMasterBONUS_IN_MONEY_YN: TStringField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableMasterMUTATOR: TStringField;
    TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableMasterWEEKS_IN_PERIOD: TIntegerField;
    TableMasterWORK_TIME_REDUCTION_YN: TStringField;
    TableMasterHOURTYPE_NUMBER: TIntegerField;
    TableMasterHOURTYPELU: TStringField;
    TableMasterOVERTIMETYPE: TStringField;
    TableMasterGUARANTEED_HOURS: TIntegerField;
    TableMasterGuaranteedHrs: TStringField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailTIME_FOR_TIME_YN: TStringField;
    TableDetailBONUS_IN_MONEY_YN: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableDetailMUTATOR: TStringField;
    TableDetailPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableDetailWEEKS_IN_PERIOD: TIntegerField;
    TableDetailWORK_TIME_REDUCTION_YN: TStringField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailHOURTYPELU: TStringField;
    TableDetailOVERTIMETYPE: TStringField;
    TableDetailGUARANTEED_HOURS: TIntegerField;
    TableDetailGuaranteedHrs: TStringField;
    TableDetailEXCEPTIONAL_BEFORE_OVERTIME: TStringField;
    TableDetailROUND_MINUTE: TIntegerField;
    TableDetailROUND_TRUNCATE_HRS: TStringField;
    TableDetailROUND_TRUNC_SALARY_HOURS: TIntegerField;
    TableDetailMAXIMUM_SALARY: TFloatField;
    TableDetailPAID_ILLNESS_PERIOD: TIntegerField;
    TableAbsenceReason: TTable;
    TableAbsenceReasonABSENCEREASON_ID: TIntegerField;
    TableAbsenceReasonABSENCETYPE_CODE: TStringField;
    TableAbsenceReasonABSENCEREASON_CODE: TStringField;
    TableAbsenceReasonDESCRIPTION: TStringField;
    TableAbsenceReasonCREATIONDATE: TDateTimeField;
    TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField;
    TableAbsenceReasonMUTATIONDATE: TDateTimeField;
    TableAbsenceReasonMUTATOR: TStringField;
    TableAbsenceReasonPAYED_YN: TStringField;
    TableDetailUNPAID_ILLN_ABSENCEREASONLU: TStringField;
    TableDetailBANKHOLIDAY_OVERRULES_AVAIL_YN: TStringField;
    TableDetailEXPORT_SR_HOURS_YN: TStringField;
    TableDetailUNPAID_ILLN_ABSENCEREASON_ID: TIntegerField;
    TableDetailWORKDAY_MO_YN: TStringField;
    TableDetailWORKDAY_TU_YN: TStringField;
    TableDetailWORKDAY_WE_YN: TStringField;
    TableDetailWORKDAY_TH_YN: TStringField;
    TableDetailWORKDAY_FR_YN: TStringField;
    TableDetailWORKDAY_SA_YN: TStringField;
    TableDetailWORKDAY_SU_YN: TStringField;
    TableDetailNORMALHOURSPERDAY: TFloatField;
    qryCheckHourTypeTFT: TQuery;
    TableDetailRAISE_PERCENTAGE: TFloatField;
    TableDetailMAX_SAT_CREDIT_MINUTE: TFloatField;
    TableDetailEXPORT_NORMAL_HRS_YN: TStringField;
    TableDetailEXPORT_WORKED_DAYS_YN: TStringField;
    TableDetailHOURTYPE_WRK_ON_BANKHOL: TIntegerField;
    TableHourtypeBankHol: TTable;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DateTimeField1: TDateTimeField;
    StringField2: TStringField;
    DateTimeField2: TDateTimeField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    FloatField1: TFloatField;
    TableDetailHOURTYPEBANKHOLLU: TStringField;
    TableDetailMAX_PTO_MINUTE: TIntegerField;
    TableDetailMAX_PTO_MINUTE_CALC: TStringField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FGuaranteedHours: Integer;
    FNormalHoursPerDay: Integer;
    FHourTypeTFT: Boolean;
    FMaxSatCreditMinute: Integer;
    FMaxPTOMinute: Integer;
  public
    { Public declarations }
    FTFT_YN, FBonus_YN: Boolean;
    function GetValueYN(ValueBool: Boolean): String;
    function CheckHourTypeTFT: Boolean;
    property GuaranteedHours: Integer read FGuaranteedHours
      write FGuaranteedHours;
    property NormalHoursPerDay: Integer read FNormalHoursPerDay
      write FNormalHoursPerDay;
    property HourTypeTFT: Boolean read FHourTypeTFT write FHourTypeTFT;
    property MaxSatCreditMinute: Integer read FMaxSatCreditMinute
      write FMaxSatCreditMinute;
    property MaxPTOMinute: Integer read FMaxPTOMinute write FMaxPTOMinute;
  end;

var
  ContractGroupDM: TContractGroupDM;

implementation

{$R *.DFM}

uses SystemDMT, UPimsConst, UPimsMessageRes, GridBaseFRM;

function TContractGroupDM.GetValueYN(ValueBool: Boolean): String;
begin
  if ValueBool then
    Result := 'Y'
  else
    Result := 'N';
end;

procedure TContractGroupDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetailTIME_FOR_TIME_YN.Value := UNCHECKEDVALUE;
  TableDetailBONUS_IN_MONEY_YN.Value := UNCHECKEDVALUE;
  TableDetailWORK_TIME_REDUCTION_YN.Value := UNCHECKEDVALUE;
  TableDetailOVERTIME_PER_DAY_WEEK_PERIOD.Value := Integer(OTDay);
  //car 550287
  TableDetailROUND_MINUTE.Value := 1;
  TableDetailROUND_TRUNC_SALARY_HOURS.Value := 1;
  TableDetailPAID_ILLNESS_PERIOD.Value := 0;
  TableDetailUNPAID_ILLN_ABSENCEREASON_ID.Value := 0;
  TableDetailWORKDAY_MO_YN.Value := 'Y';
  TableDetailWORKDAY_TU_YN.Value := 'Y';
  TableDetailWORKDAY_WE_YN.Value := 'Y';
  TableDetailWORKDAY_TH_YN.Value := 'Y';
  TableDetailWORKDAY_FR_YN.Value := 'Y';
  TableDetailWORKDAY_SA_YN.Value := 'N';
  TableDetailWORKDAY_SU_YN.Value := 'N';
  TableDetailNORMALHOURSPERDAY.Value := 0;
  TableDetailRAISE_PERCENTAGE.Value := 0; // Set value here! RV055.1.
  // RV067.MRA.10.
  TableDetailEXCEPTIONAL_BEFORE_OVERTIME.Value := UNCHECKEDVALUE;
  TableDetailBANKHOLIDAY_OVERRULES_AVAIL_YN.Value := UNCHECKEDVALUE;
  // RV088.1.
  TableDetailEXPORT_NORMAL_HRS_YN.Value := UNCHECKEDVALUE;
  // RV101.1.
  TableDetailEXPORT_WORKED_DAYS_YN.Value := CHECKEDVALUE;
end;

procedure TContractGroupDM.TableDetailCalcFields(DataSet: TDataSet);
var
  HoursStr, MinStr: String;
begin
  inherited;
  HoursStr := IntToStr(TableDetail.FieldByName('GUARANTEED_HOURS').asInteger div 60);
  HoursStr := Copy('     ', 0 , 5 - Length(HoursStr)) + HoursStr;
  MinStr := IntToStr(TableDetail.FieldByName('GUARANTEED_HOURS').asInteger mod 60);
  MinStr := Copy('00', 0, 2 - Length(MinStr)) + MinStr;
  TableDetail.FieldByName('GuaranteedHrs').asString := HoursStr +  ':' + MinStr;
  case TOvertime(TableDetail.
    FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger) of
  OTDay: TableDetail.FieldByName('OVERTIMETYPE').AsString := SOvertimeDay;
  OTWeek: TableDetail.FieldByName('OVERTIMETYPE').AsString := SOvertimeWeek;
  OTMonth: TableDetail.FieldByName('OVERTIMETYPE').AsString := SOvertimeMonth;
  OTPeriod: TableDetail.FieldByName('OVERTIMETYPE').AsString := SPeriodReport;
  end;
  if TableDetail.FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger = 0 then
    TableDetail.FieldByName('Round_Truncate_Hrs').AsString := STruncateHrs;
  if TableDetail.FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger = 1 then
    TableDetail.FieldByName('Round_Truncate_Hrs').AsString := SRoundHrs;
  // GLOB3-204
  if TableDetail.FieldByName('MAX_PTO_MINUTE').AsString <> '' then
    TableDetail.FieldByName('MAX_PTO_MINUTE_CALC').AsString :=
      Format('%.2d:%.2d',[(TableDetail.FieldByName('MAX_PTO_MINUTE').AsInteger div 60),
      (TableDetail.FieldByName('MAX_PTO_MINUTE').AsInteger mod 60)]);
end;

procedure TContractGroupDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);

  TableDetail.FieldByName('TIME_FOR_TIME_YN').AsString :=
    GetValueYN(FTFT_YN);
  TableDetail.FieldByName('BONUS_IN_MONEY_YN').AsString :=
    GetValueYN(FBonus_YN);
  TableDetail.FieldByName('GUARANTEED_HOURS').AsInteger := GuaranteedHours;
  TableDetail.FieldByName('NORMALHOURSPERDAY').AsInteger := NormalHoursPerDay;
  if TOvertime(TableDetail.
    FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger) <> OTPeriod then
  begin
    TableDetail.FieldByName('PERIOD_STARTS_IN_WEEK').AsString := '';
    TableDetail.FieldByName('WEEKS_IN_PERIOD').AsString := '';
  end;
  // RV055.2.
  if TableDetail.FieldByName('RAISE_PERCENTAGE').AsString = '' then
    TableDetail.FieldByName('RAISE_PERCENTAGE').AsFloat := 0;
  // RV071.10.
  TableDetail.FieldByName('MAX_SAT_CREDIT_MINUTE').AsInteger :=
    MaxSatCreditMinute;
  // GLOB3-204
  TableDetail.FieldByName('MAX_PTO_MINUTE').AsInteger :=
    MaxPTOMinute;
end;

// RV045.1.
function TContractGroupDM.CheckHourTypeTFT: Boolean;
begin
  Result := False;
  with qryCheckHourTypeTFT do
  begin
    Open;
    if not Eof then
      Result := True;
    Close;
  end;
end;

end.
