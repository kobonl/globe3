(*
  Changes:
    MR:31-03-2006:
      In ReportCheckListScanDMT the main-query that is used in the report
      has now a default-query, to be sure the fields are known in the report
      for TQRDBText-components.
    MRA:12-MAR-2008 Order 550463, RV005.
      Add plant-from-to selection to report.
    MRA:15-JAN-2009 RV020. Revision 020.
      When there are overnight-scans, this report gave wrong results.
      To solve this, a special procedure is made and used:
      - GetProdMinOvernight.
    MRA:10-FEB-2010 RV054.1. Bugfix.
    - This did not show all availability-info because of filtering on
      plants (part of teams), for team-per-user.
    MRA:13-JAN-2011 RV084.5. Bugfix.
    - The following is done:
      - Teams-per-user is used with teams, for example:
        - User: 31ABS (test-user)
          - Team (31) CLF Transport
          - Team (34) Blanco Transport
        - Two scans are made, each for an employee of the 2 teams.
    - Then when report checklist scans is run for plant 31 to 34, then
      it shows nothing!
    MRA:4-FEB-2011 RV085.18. SC-50016412.
    - This must be changed, so it can show scans made in other
      plants. This will be done with an additional checkbox
      named 'Include scans made in other plants'.
      When this checkbox is checked, it will not filter on
      plant+workspots but only on plant+employees. It will
      also show the plantcode-info for each scan.
      When this checkbox is not checked, it works like before.
    MRA:18-JUL-2011 RV095.1. SO-20011798.
    - It should be possible to select scans on plant-department-level and
      plant-workspot-level. This can be done by using 'all employees'-checkbox.
    MRA:3-OCT-2012 20013489
    - Overnight-Shift-System. Show scans based on SHIFT_DATE instead of on
      scans-date+times.
    - Selections-part: Changed text 'Date' to 'Shift Date'
    MRA:19-OCT-2012 20013489 Overnight-Shift-System
    - Show Shiftdate in report.
    MRA:23-NOV-2012 20013489
    - Use NVL in query for comparison with SHIFT_DATE:
      - If SHIFT_DATE is NULL, then select on DATETIME_IN.
    - Print SHIFT_DATE using DateToStr.
    MRA:23-NOV-2012 20013288
    - Show MUTATIONDATE and MUTATOR in report.
      - Call them 'Timestamp' and 'User'.
    MRA:10-OCT-2013 20014327
    - Make use of Overnight-Shift-System optional.
    MRA:11-JUL-2014 TD-25174
    - Also export Timestamp and User
    MRA:1-SEP-2014 TD-23699
    - Show optionally Shift, Dept and Job in report.
    MRA:5-JAN-2015 20015995
    - Add checkbox to show only SANA-employees.
    - Custom-made, only usable for VOS
    MRA:16-MAR-2015 20015346
    - Store scans in seconds
    - DATETIME_IN/DATETIME_OUT-fields must be rounded with RoundTime() to prevent
      problems with display + calculation.
    MRA:7-MAY-2015 20014450.50
    - Real Time Efficiency
    - Show the times for date-in and date-out including seconds.
    MRA:10-MAR-2016 ABS-24733
    - Also filter on team!
    - This means TEAM_CODE had to be added in query.
    MRA:15-JAN-2018 PIM-348
    - Calculation of Early/Late Margins gives a problem
    - Problem has to do with PIM-326.
    - It should always determine the first/last scan to determine if the
      current scan should be rounded because of early/late in/out-margins!
*)

unit ReportCheckListScanQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  UScannedIDCard, QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FDepartmentFrom, FDepartmentTo: String; // RV095.1.
    FWorkSpotFrom, FWorkSpotTo: String;
    FDateFrom,FDateTo: TDateTime;
    FShowAllDate, FShowTotal, FShowSelection: Boolean;
    FShowProcessed: Integer;
    FExportToFile, FShowAvail,
    FIncludeScansOtherPlants, // RV085.18.
    FAllDepartments, // RV095.1.
    FAllPlants, // RV095.1.
    FAllEmployees, // RV095.1.
    FShowDetails: Boolean; // TD-23699
    FOnlySANAEmps: Boolean; // 20015995
  public
    procedure SetValues(DepartmentFrom, DepartmentTo,
      WorkSpotFrom, WorkSpotTo: String;
      DateFrom, DateTo: TDateTime;
      ShowAllDate, ShowTotal, ShowSelection: Boolean;
      ShowProcessed: Integer;
      ExportToFile, ShowAvail,
      IncludeScansOtherPlants, // RV085.18.
      AllDepartments,
      AllPlants,
      AllEmployees,
      ShowDetails: Boolean; // TD-23699
      OnlySANAEmps: Boolean); // 20015995
  end;

  TReportCheckListScanQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelShowProcessed: TQRLabel;
    QRLabel1: TQRLabel;
    QRLblWorkSpotFrom: TQRLabel;
    QRLblWorkSpotTo: TQRLabel;
    QRBandGrpFooterEmpl: TQRBand;
    QRBandDetailEmployee: TQRBand;
    QRGroupHDEmpl: TQRGroup;
    QRBandTitleEmpl: TQRBand;
    QRShape5: TQRShape;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape6: TQRShape;
    QRLabel4: TQRLabel;
    QRLabelTotHours: TQRLabel;
    QRLabelDateTime: TQRLabel;
    QRLabelTimeIn: TQRLabel;
    LabelCalcHours: TQRLabel;
    QRLabelDateTimeOut: TQRLabel;
    QRDBTextCardIn: TQRDBText;
    QRDBTextCardOut: TQRDBText;
    QRDBTextWK: TQRDBText;
    QRDBTextWKDesc: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRLabelEmplAvail: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabelPlantTo: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblWPlant: TQRLabel;
    QRDBTextWPlant: TQRDBText;
    QRLabelFromDept: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLblDeptFrom: TQRLabel;
    QRLabelToDept: TQRLabel;
    QRLblDeptTo: TQRLabel;
    QRLabelShiftDate: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabelMutationDate: TQRLabel;
    QRDBTextMutator: TQRDBText;
    QRChildBandDetailEmployee: TQRChildBand;
    QRDBTextShift: TQRDBText;
    QRDBTextDepartment: TQRDBText;
    ChildBandTitleEmpl: TQRChildBand;
    QRLblShift: TQRLabel;
    QRLblDepartment: TQRLabel;
    QRShape1: TQRShape;
    QRLblJob: TQRLabel;
    QRDBTextJob: TQRDBText;
    QRLblOnlySANAEmps: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelDateTimePrint(sender: TObject; var Value: String);
    procedure QRLabelTimeInPrint(sender: TObject; var Value: String);
    procedure QRDBTextWKDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextCardOutPrint(sender: TObject; var Value: String);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotHoursPrint(sender: TObject; var Value: String);
    procedure LabelCalcHoursPrint(sender: TObject; var Value: String);
    procedure QRLabelDateTimeOutPrint(sender: TObject; var Value: String);
    procedure QRBandGrpFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelShiftDatePrint(sender: TObject; var Value: String);
    procedure QRLabelMutationDatePrint(sender: TObject; var Value: String);
    procedure ChildBandTitleEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextShiftPrint(sender: TObject; var Value: String);
    procedure QRDBTextDepartmentPrint(sender: TObject; var Value: String);
    procedure ChildBandTitleEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRChildBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBTextJobPrint(sender: TObject; var Value: String);
  private
    FQRParameters: TQRParameters;
    MyEmployee_Tot_Min: Integer;
    FEmptyReq, FEmptyEmplReq: Boolean;

  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    Total_Hours_Per_Empl: Integer;
    ExistTR,
    FPrintBandEmpl: Boolean;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const EmployeeFrom, EmployeeTo, DepartmentFrom, DepartmentTo,
      WorkSpotFrom, WorkSpotTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowAllDate, ShowTotal, ShowSelection: Boolean;
       ShowProcessed: Integer;
       ExportToFile, ShowAvail, IncludeScansOtherPlants,
       AllDepartments, AllPlants, AllEmployees, ShowDetails: Boolean;
       OnlySANAEmps: Boolean): Boolean; // 20015995
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;

  end;

var
  ReportCheckListScanQR: TReportCheckListScanQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportCheckListScanDMT, UGlobalFunctions, UPimsMessageRes, UPimsConst,
  CalculateTotalHoursDMT;

// MR:06-12-2002
// RV085.18. Addition of WPlant
procedure ExportDetail(
  EmployeeNumber, EmployeeName, ShiftDate, ScanDate, TimeIn, TimeOut,
  CalculatedHours, WPlant, Workspot, Completed, IDCardIn, IDCardOut,
  Timestamp, User: String); // TD-25174
var
  MyShiftDate: String;
begin
  if SystemDM.UseShiftDateSystem then // 20014327
    MyShiftDate := ShiftDate + ExportClass.Sep // 20013489
  else
    MyShiftDate := '';
  ExportClass.AddText(
    EmployeeNumber + ExportClass.Sep +
    EmployeeName + ExportClass.Sep +
    MyShiftDate + // ShiftDate + ExportClass.Sep + // 20013489
    ScanDate + ExportClass.Sep +
    TimeIn + ExportClass.Sep +
    TimeOut + ExportClass.Sep +
    CalculatedHours + ExportClass.Sep +
    WPlant + ExportClass.Sep +
    Workspot + ExportClass.Sep +
    Completed + ExportClass.Sep +
    IDCardIn + ExportClass.Sep +
    IDCardOut + ExportClass.Sep +
    Timestamp + ExportClass.Sep + // TD-25174
    User // TD-25174
    );
end;

procedure TQRParameters.SetValues(DepartmentFrom, DepartmentTo,
      WorkSpotFrom, WorkSpotTo: String;
      DateFrom, DateTo: TDateTime;
      ShowAllDate, ShowTotal, ShowSelection: Boolean;
      ShowProcessed: Integer;
      ExportToFile, ShowAvail, IncludeScansOtherPlants, AllDepartments,
      AllPlants,
      AllEmployees, ShowDetails: Boolean;
      OnlySANAEmps: Boolean); // 20015995
begin
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FWorkSpotFrom := WorkSpotFrom;
  FWorkSpotTo := WorkSpotTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowAllDate := ShowAllDate;
  FShowTotal := ShowTotal;
  FShowSelection := ShowSelection;
  FShowProcessed := ShowProcessed;
  FExportToFile := ExportToFile;
  FShowAvail := ShowAvail;
  FIncludeScansOtherPlants := IncludeScansOtherPlants;
  FAllDepartments := AllDepartments;
  FAllPlants := AllPlants;
  FAllEmployees := AllEmployees;
  FShowDetails := ShowDetails; // TD-23699
  FOnlySANAEmps := OnlySANAEmps; // 20015995
end;

function TReportCheckListScanQR.QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const EmployeeFrom, EmployeeTo, DepartmentFrom, DepartmentTo,
      WorkSpotFrom, WorkSpotTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowAllDate, ShowTotal, ShowSelection: Boolean;
       ShowProcessed: Integer;
       ExportToFile, ShowAvail, IncludeScansOtherPlants, AllDepartments,
       AllPlants,
       AllEmployees, ShowDetails: Boolean;
       OnlySANAEmps: Boolean): Boolean; // 20015995
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DepartmentFrom, DepartmentTo,
      WorkSpotFrom, WorkSpotTo, DateFrom, DateTo,
      ShowAllDate, ShowTotal, ShowSelection, ShowProcessed, ExportToFile,
      ShowAvail, IncludeScansOtherPlants, AllDepartments, AllPlants,
      AllEmployees, ShowDetails,
      OnlySANAEmps); // 20015995
  end;
  SetDataSetQueryReport(ReportCheckListScanDM.QueryTimeRegScan);
end;

function TReportCheckListScanQR.ExistsRecords: Boolean;
var
  SelectStr, ProcessedYN: String;
begin
  Screen.Cursor := crHourGlass;

  {open all linked datasets}
  ProcessedYN := '';
  if QRParameters.FShowProcessed = 0 then
    ProcessedYN := 'Y';
  if QRParameters.FShowProcessed = 1 then
    ProcessedYN := 'N';
  // 20013489 Select scans based on SHIFT_DATE.
  SelectStr :=
    'SELECT ' + NL +
    '  T.EMPLOYEE_NUMBER, E.DESCRIPTION, T.DATETIME_IN, ' + NL +
    '  W.PLANT_CODE AS WPLANT_CODE, ' + NL + // RV085.18.
    '  E.PLANT_CODE AS EPLANT_CODE, ' + NL + // RV085.18.
    '  E.DEPARTMENT_CODE AS EDEPARTMENT_CODE, ' + NL + // RV085.18.
    '  E.TEAM_CODE, ' + NL + // ABS-24733
    '  T.WORKSPOT_CODE, W.DESCRIPTION AS WDESCRIPTION, ' + NL +
    '  T.DATETIME_OUT, T.PROCESSED_YN, ' + NL +
    '  T.IDCARD_IN, T.IDCARD_OUT, T.SHIFT_NUMBER, ' + NL +
    '  T.JOB_CODE, J.DESCRIPTION AS JOBDESC, ' + NL +
    '  T.PLANT_CODE, P.DESCRIPTION AS PDESCRIPTION, ' + NL +
    '  W.DEPARTMENT_CODE, E.CUT_OF_TIME_YN , ' + NL +
    '  P.INSCAN_MARGIN_EARLY, P.OUTSCAN_MARGIN_LATE, ' + NL +
    '  P.INSCAN_MARGIN_LATE, P.OUTSCAN_MARGIN_EARLY, '  + NL +
    '  T.SHIFT_DATE, ' + NL; // 20013489 Also show shiftdate in report
  if QRParameters.FShowDetails then // TD-23699
    SelectStr := SelectStr +
      '  S.DESCRIPTION AS SDESCRIPTION, ' + NL +
      '  D.DESCRIPTION AS DDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  T.MUTATIONDATE, T.MUTATOR ' + NL + // 20013288
    'FROM ' + NL +
    '  TIMEREGSCANNING T INNER JOIN EMPLOYEE E ON ' + NL +
    '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  LEFT JOIN PLANT P ON ' + NL +
    '    T.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  LEFT JOIN WORKSPOT W ON ' + NL +
    '    T.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    T.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  if QRParameters.FShowDetails then // TD-23699
    SelectStr := SelectStr +
      '  LEFT JOIN DEPARTMENT D ON ' + NL +
      '    T.PLANT_CODE = D.PLANT_CODE AND ' + NL +
      '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
      '  LEFT JOIN SHIFT S ON ' + NL +
      '    T.PLANT_CODE = S.PLANT_CODE AND ' + NL +
      '    T.SHIFT_NUMBER = S.SHIFT_NUMBER ' + NL;
  SelectStr := SelectStr +
    '  LEFT JOIN JOBCODE J ON ' + NL +
    '    T.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '    T.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    T.JOB_CODE = J.JOB_CODE ' + NL +
    'WHERE ' + NL;
  // RV095.1.
  if not QRParameters.FAllPlants then
    SelectStr := SelectStr +
      '  E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL
  else
    SelectStr := SelectStr +
      '  W.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND W.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    // RV095.1.
    if not QRParameters.FAllPlants then
      SelectStr := SelectStr +
        '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    // RV095.1.
    if not QRParameters.FAllDepartments then
      SelectStr := SelectStr +
        '  AND W.DEPARTMENT_CODE >= '''  +
        DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
        '  AND W.DEPARTMENT_CODE <= ''' +
        DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL;
    // RV084.5. This was always done! Only do this when 1 plant is selected!
    if (not QRParameters.FIncludeScansOtherPlants) then // RV085.18.
      SelectStr := SelectStr +
        '  AND T.WORKSPOT_CODE >= ''' +
        DoubleQuote(QRParameters.FWorkSpotFrom) + '''' + NL +
        '  AND T.WORKSPOT_CODE <= ''' +
        DoubleQuote(QRParameters.FWorkSpotTo) + '''' + NL;
  end;
  if (ProcessedYN <> '') then
    SelectStr := SelectStr +
      '  AND T.PROCESSED_YN = :PROCESSEDYN ' + NL;
  // 20013489 Instead of on DATETIME_IN, filter on SHIFT_DATE
  if not QRParameters.FShowAllDate then
  begin
    if SystemDM.UseShiftDateSystem then
      SelectStr := SelectStr +
        '  AND NVL(T.SHIFT_DATE, T.DATETIME_IN) >= :DATEFROM ' +
        '  AND NVL(T.SHIFT_DATE, T.DATETIME_IN) < :DATETO '
    else
      SelectStr := SelectStr +
        '  AND T.DATETIME_IN >= :DATEFROM ' +
        '  AND T.DATETIME_IN < :DATETO ';
  end;
  if SystemDM.IsVOS then // 20015995
  begin
    if QRParameters.FOnlySANAEmps then
      SelectStr := SelectStr +
        '  AND E.FREETEXT = ' + '''' + '1' + '''' + ' ' + NL
    else
      SelectStr := SelectStr +
        '  AND E.FREETEXT IS NULL ' + NL;
  end;
  if QRParameters.FShowAvail then
  begin
    SelectStr := SelectStr  + ' ' + NL + ' ' +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
      '  TO_DATE(''1-1-1900 12:00'',''DD-MM-YYYY HH24:MI''), ' + NL +
      '  CAST ('''' AS VARCHAR(6)), ' + NL + // RV085.18.
      '  E.PLANT_CODE AS EPLANT_CODE, ' + NL + // RV085.18.
      '  E.DEPARTMENT_CODE AS EDEPARTMENT_CODE, ' + NL + // RV085.18.
      '  E.TEAM_CODE, ' + NL + // ABS-24733
      '  CAST ('''' AS VARCHAR(6)), ' + NL +
      '  CAST ('''' AS VARCHAR(30)), ' + NL +
      '  TO_DATE(''1-1-1900 12:00'',''DD-MM-YYYY HH24:MI''), ' + NL +
      '  CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(15)), CAST('''' AS VARCHAR(15)), ' + NL +
      '  A.SHIFT_NUMBER, ' + NL +
      '  CAST ('''' AS VARCHAR(6)), CAST ('''' AS VARCHAR(30)),  ' + NL +
// RV054.1. Bugfix. Plant must be included!
//      '  CAST ('''' AS VARCHAR(6)), CAST ('''' AS VARCHAR(30)), ' + NL +
      '  E.PLANT_CODE, CAST ('''' AS VARCHAR(30)), ' + NL +
      '  CAST ('''' AS VARCHAR(6)), CAST('''' AS VARCHAR(1)),  ' + NL +
      '  1,1,1,1, ' + NL +
      '  TO_DATE(''1-1-1900'',''DD-MM-YYYY''), ' + NL; // 20013489 Dummy field
    if QRParameters.FShowDetails then // TD-23699
      SelectStr := SelectStr +
        '  CAST ('''' AS VARCHAR(30)), ' + NL +
        '  CAST ('''' AS VARCHAR(30)), ' + NL;
    SelectStr := SelectStr +
      '  A.MUTATIONDATE, A.MUTATOR ' + NL + // 20013288
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY A INNER JOIN EMPLOYEE E ON ' + NL +
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      'WHERE  ' + NL;
  // RV095.1.
  if not QRParameters.FAllPlants then
    SelectStr := SelectStr +
      '  E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL
  else
    SelectStr := SelectStr +
      '  A.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND A.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    // RV095.1.
    if not QRParameters.FAllPlants then
      SelectStr := SelectStr +
        '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  SelectStr := SelectStr +
      '  AND A.EMPLOYEEAVAILABILITY_DATE = :DATEFROM AND ' + NL +
      '  A.EMPLOYEE_NUMBER NOT IN ' + NL +
      '  (SELECT ' + NL +
      '    S.EMPLOYEE_NUMBER ' + NL +
      '  FROM ' + NL +
      '    TIMEREGSCANNING S INNER JOIN EMPLOYEE E ON ' + NL +
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  WHERE ' + NL +
      '    E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '    AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      // RV095.1.
      if not QRParameters.FAllPlants then
        SelectStr := SelectStr +
          '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
          '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
      // RV095.1.
      if not QRParameters.FAllDepartments then
        SelectStr := SelectStr +
          '  AND E.DEPARTMENT_CODE >= '''  +
          DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
          '  AND E.DEPARTMENT_CODE <= ''' +
          DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL;
      // RV084.5. This was always done! Only do this when 1 plant is selected!
      if (not QRParameters.FIncludeScansOtherPlants) then // RV085.18.
        SelectStr := SelectStr +
          '  AND S.WORKSPOT_CODE >= ''' +
          DoubleQuote(QRParameters.FWorkSpotFrom) + '''' + NL +
          '    AND S.WORKSPOT_CODE <= ''' +
          DoubleQuote(QRParameters.FWorkSpotTo) + '''' + NL;
    end;
    if (ProcessedYN <> '') then
      SelectStr := SelectStr +
        '  AND S.PROCESSED_YN = :PROCESSEDYN ' + NL;
    SelectStr := SelectStr +
      '  AND S.DATETIME_IN >= :DATEFROM ' +
      '  AND S.DATETIME_IN < :DATETO ' +
      '  )';
    if SystemDM.IsVOS then // 20015995
    begin
      if QRParameters.FOnlySANAEmps then
        SelectStr := SelectStr +
          '  AND E.FREETEXT = ' + '''' + '1' + '''' + ' ' + NL
      else
        SelectStr := SelectStr +
          '  AND E.FREETEXT IS NULL ' + NL;
    end;

      ReportCheckListScanDM.BuildListAvail(QRBaseParameters.FEmployeeFrom,
        QRBaseParameters.FEmployeeTo, QRParameters.FDateFrom);
  end;
  SelectStr := SelectStr +
    ' ORDER BY 1, 3';

  ReportCheckListScanDM.QueryTimeRegScan.Active := False;
  ReportCheckListScanDM.QueryTimeRegScan.UniDirectional := False;
  ReportCheckListScanDM.QueryTimeRegScan.SQL.Clear;
  ReportCheckListScanDM.QueryTimeRegScan.SQL.Add(SelectStr);
// ReportCheckListScanDM.QueryTimeRegScan.SQL.SaveToFile('c:\temp\checklistscan.sql');
  if PROCESSEDYN <> '' then
    ReportCheckListScanDM.QueryTimeRegScan.
      ParamByName('PROCESSEDYN').AsString := PROCESSEDYN;

  if not QRParameters.FShowAllDate then
  begin
    ReportCheckListScanDM.QueryTimeRegScan.
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
    ReportCheckListScanDM.QueryTimeRegScan.
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
  end;

  if not ReportCheckListScanDM.QueryTimeRegScan.Prepared then
     ReportCheckListScanDM.QueryTimeRegScan.Prepare;
  ReportCheckListScanDM.QueryTimeRegScan.Active := True;

  Result := not ReportCheckListScanDM.QueryTimeRegScan.IsEmpty;
  if Result then
    FEmptyReq :=  ReportCheckListScanDM.CheckEmptyRequest;
  Screen.Cursor := crDefault;
end;

procedure TReportCheckListScanQR.ConfigReport;
begin
  // MR:06-12-2002
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    // RV095.1.
    if not QRParameters.FAllPlants then
    begin
      QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
      QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
    end
    else
    begin
      QRLblEmployeeFrom.Caption := '*';
      QRLblEmployeeTo.Caption := '*';
    end;
    // RV095.1.
    if not QRParameters.FAllDepartments then
    begin
      QRLblDeptFrom.Caption := QRParameters.FDepartmentFrom;
      QRLblDeptTo.Caption := QRParameters.FDepartmentTo;
    end
    else
    begin
      QRLblDeptFrom.Caption := '*';
      QRLblDeptTo.Caption := '*';
    end;
    // RV085.18.
    if (not QRParameters.FIncludeScansOtherPlants) then
    begin
      // RV084.5.
      QRLblWorkSpotFrom.Caption := QRParameters.FWorkSpotFrom;
      QRLblWorkSpotTo.Caption := QRParameters.FWorkSpotTo;
    end
    else
    begin
      QRLblWorkSpotFrom.Caption := '*';
      QRLblWorkSpotTo.Caption := '*';
    end;
  end
  else
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
    // RV095.1.
    QRLblDeptFrom.Caption := '*';
    QRLblDeptTo.Caption := '*';
    // RV084.5.
    QRLblWorkSpotFrom.Caption := '*';
    QRLblWorkSpotTo.Caption := '*';
  end;
  if QRParameters.FShowProcessed = 0 then
    QRLabelShowProcessed.Caption := SShowProcessed;
  if QRParameters.FShowProcessed = 1 then
    QRLabelShowProcessed.Caption := SShowNotProcessed;
  if QRParameters.FShowProcessed = 2 then
  QRLabelShowProcessed.Caption := '';
  if QRParameters.FShowAllDate then
  begin
    QRLabelDateFrom.Caption := '*';
    QRLabelDateTo.Caption := '*';
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo - 1);
  end;
  // Do not show ShiftDate-column in report (optionally).
  if not SystemDM.UseShiftDateSystem then // 20014327
  begin
    QRLabelDate.Caption := SDate;
    QRLabel3.Caption := '';
  end;
  if SystemDM.IsVOS then // 20015995
  begin
    if QRParameters.FOnlySANAEmps then
       QRLblOnlySANAEmps.Caption := SPimsOnlySANAEmps
    else
       QRLblOnlySANAEmps.Caption := '';
  end
  else
    QRLblOnlySANAEmps.Caption := '';
end;

procedure TReportCheckListScanQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportCheckListScanQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportCheckListScanQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Show selections
    if QRParameters.FShowSelection then
    begin
      // Selections
      ExportClass.AddText(QRLabel11.Caption);
      // From Plant PlantCode to PlantCode
      ExportClass.AddText(QRLabel2.Caption + ' ' +
        QRLabel19.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' + QRLabelPlantTo.Caption);
      // From Employee EmployeeNumber to EmployeeNumber
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLblEmployeeFrom.Caption + ' ' +
        QRLabel16.Caption + ' ' + QRLblEmployeeTo.Caption);
      // RV095.1.
      // From Department DepartmentCode to DepartmentCode
      ExportClass.AddText(QRLabelFromDept.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLblDeptFrom.Caption + ' ' +
        QRLabelToDept.Caption + ' ' + QRLblDeptTo.Caption);
      // From Workspot WorkspotCode to WorkspotCode
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel1.Caption + ' ' + QRLblWorkspotFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLblWorkspotTo.Caption);
      // From Scandate date to date
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' +
        QRLabelDate.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabelToDate.Caption + ' ' + QRLabelDateTo.Caption);
      // Show processed
      QRLabelShowProcessed.Caption;
      if SystemDM.IsVOS then // 20015995
        ExportClass.AddText(QRLblOnlySANAEmps.Caption);
    end;

    // Show columns
    ExportDetail(
      QRLabel34.Caption,                           // Employee number
      '',                                          // Employee name
      QRLabel3.Caption,                            // Shiftdate 20013489
      QRLabel35.Caption,                           // Date
      QRLabel36.Caption + ' ' + QRLabel37.Caption, // Date In
      QRLabel36.Caption + ' ' + QRLabel38.Caption, // Date Out
      QRLabel40.Caption + ' ' + QRLabel39.Caption, // Calculated Hours
      QRLblWPlant.Caption,                         // W-Plant RV085.18.
      QRLabel41.Caption,                           // Workspot
      QRLabel42.Caption,                           // Completed
      QRLabel43.Caption + ' ' + QRLabel44.Caption, // IDCard In
      QRLabel43.Caption + ' ' + QRLabel45.Caption, // IDCard Out
      QRLabel5.Caption,                            // Timestamp // TD-25174
      QRLabel6.Caption                             // User // TD-25174
      );
  end;
end;

procedure TReportCheckListScanQR.QRLabelDateTimePrint(sender: TObject;
  var Value: String);
var
  Year, Month, Day: Word;
begin
  inherited;
  DecodeDate(RoundTime(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_IN').AsDateTime,1), Year, Month, Day);
  Value := DateToStr(EncodeDate(Year, Month, Day));
end;

procedure TReportCheckListScanQR.QRLabelTimeInPrint(sender: TObject; var Value: String);
var
  Hour, Min, Sec, MSec: Word;
begin
  inherited;
  // 20014450.50
//  DecodeTime(RoundTime(ReportCheckListScanDM.QueryTimeRegScan.
//    FieldByName('DATETIME_IN').AsDateTime,1), Hour, Min, Sec, MSec);
  DecodeTime(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_IN').AsDateTime, Hour, Min, Sec, MSec);
  Value := FillZero(Hour) + ':' + FillZero(Min) + ':' + FillZero(Sec);
end;

procedure TReportCheckListScanQR.QRDBTextWKDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
   Value := Copy(Value, 0, 15);
end;

procedure TReportCheckListScanQR.QRDBTextCardOutPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013288 Related to this order.
  // Why do this?
//  Value := Copy(Value, 0, 6);
end;

procedure TReportCheckListScanQR.QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  Total_Hours_Per_Empl := 0;
  QRLabelEmplAvail.Caption := '';
  if QRPARAMETERS.FShowAvail then
    QRLabelEmplAvail.Caption := SPimsAvailability +
      ReportCheckListScanDM.GetAvail(ReportCheckListScanDM.QueryTimeRegScan.
        FieldByName('EMPLOYEE_NUMBER').AsString);

  if PrintBand then
    FEmptyEmplReq := ReportCheckListScanDM.CheckEmptyEmplRequest(FEmptyReq,
      ReportCheckListScanDM.QueryTimeRegScan.FieldByName('EMPLOYEE_NUMBER').Value);
end;

procedure TReportCheckListScanQR.QRLabelTotHoursPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Total_Hours_Per_Empl);
end;

procedure TReportCheckListScanQR.LabelCalcHoursPrint(sender: TObject;
  var Value: String);
var
  ScannedIDCard: TScannedIDCard;
  BookingDay{, StartDate, EndDate}: TDateTime;
  ProdMin, BreaksMin, PayedBreaks, Employee_Tot_Min: Integer;

begin
  inherited;
  if ReportCheckListScanDM.QueryTimeRegScan.FieldByName('PROCESSED_YN').AsString = '' then
    Exit;
  MyEmployee_Tot_Min := 0;
  if (RoundTime(ReportCheckListScanDM.QueryTimeRegScan.FieldByName('DATETIME_IN').AsDateTime,1) >
     RoundTime(ReportCheckListScanDM.QueryTimeRegScan.FieldByName('DATETIME_OUT').AsDateTime,1)) then
  begin
    Value := DecodeHrsMin(0);
    Exit;
  end;

  ScannedIDCard.EmployeeCode := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('EMPLOYEE_NUMBER').AsInteger;
   ScannedIDCard.EmplName := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DESCRIPTION').AsString;
  ScannedIDCard.ShiftNumber := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('SHIFT_NUMBER').AsInteger;
  ScannedIDCard.IDCard := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('IDCARD_IN').AsString;
  ScannedIDCard.InscanEarly := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('INSCAN_MARGIN_EARLY').AsInteger;
  ScannedIDCard.InscanLate := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('INSCAN_MARGIN_LATE').AsInteger;
   ScannedIDCard.OutscanEarly := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('OUTSCAN_MARGIN_EARLY').AsInteger;
  ScannedIDCard.OutscanLate := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('OUTSCAN_MARGIN_LATE').AsInteger;
  ScannedIDCard.WorkSpotCode := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('WORKSPOT_CODE').AsString;
  ScannedIDCard.WorkSpot := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('WDESCRIPTION').AsString;
  ScannedIDCard.JobCode := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('JOB_CODE').AsString;
  ScannedIDCard.Job := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('JOBDESC').AsString;
  ScannedIDCard.PlantCode := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('PLANT_CODE').AsString;
  ScannedIDCard.Plant := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('PDESCRIPTION').AsString;
  ScannedIDCard.DepartmentCode :=
   ReportCheckListScanDM.QueryTimeRegScan.
      FieldByName('DEPARTMENT_CODE').AsString;
  ScannedIDCard.CutOfTime :=
   ReportCheckListScanDM.QueryTimeRegScan.
     FieldByName('CUT_OF_TIME_YN').AsString;
  ScannedIDCard.DateIn := RoundTime(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_IN').AsDateTime,1);
  ScannedIDCard.DateOut :=  RoundTime(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_OUT').AsDateTime,1);
  // PIM-348 Also store Original-dates or the margin-rounding does not work!
  ScannedIDCard.DateInOriginal := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_IN').AsDateTime;
  ScannedIDCard.DateOutOriginal := ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_OUT').AsDateTime;


  // MRA:15-JAN-2009 RV020. Calculate in different way.
  // GetProdMin gives wrong result when a scan is an overnight scan!
  // Break such a scan into 2 parts here ?
  // Use an extended version of 'GetProdMin' that is doing this.

  AProdMinClass.GetProdMinOvernight(ScannedIDCard,
      True, False, False, ScannedIDCard, BookingDay,
      ProdMin, BreaksMin, PayedBreaks);

  Employee_Tot_Min := ProdMin + PayedBreaks;

{$IFDEF DEBUG1}
  WDebugLog('In=' + DateTimeToStr(ScannedIDCard.DateIn) +
    ' Out=' + DateTimeToStr(ScannedIDCard.DateOut) +
    ' Prodmin=' + IntToStr(ProdMin) +
    ' BreaksMin=' + IntToStr(BreaksMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks)
    );
{$ENDIF}


  MyEmployee_Tot_Min := Employee_Tot_Min;
  Value := DecodeHrsMin(Employee_Tot_Min);
  Total_Hours_Per_Empl := Total_Hours_Per_Empl + Employee_Tot_Min;
end;

procedure TReportCheckListScanQR.QRLabelDateTimeOutPrint(sender: TObject;
  var Value: String);
var
  Hour, Min, Sec, MSec: Word;
begin
  inherited;
  // 20014450.50
//  DecodeTime(RoundTime(ReportCheckListScanDM.QueryTimeRegScan.
//    FieldByName('DATETIME_OUT').AsDateTime,1), Hour, Min, Sec, MSec);
  DecodeTime(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('DATETIME_OUT').AsDateTime, Hour, Min, Sec, MSec);
  Value := FillZero(Hour) + ':' + FillZero(Min) + ':' + FillZero(Sec);
end;

procedure TReportCheckListScanQR.QRBandGrpFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowTotal;
  if PrintBand then
    if Not QRPARAMETERS.FShowAvail then
       QRLabelEmplAvail.Caption := '';

   if PrintBand then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportClass.AddText(QRLabel4.Caption + ' ' +
        IntToStr(Total_Hours_Per_Empl));
end;

procedure TReportCheckListScanQR.QRBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  // MR:06-12-2002
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportCheckListScanQR.QRBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  HourIn, MinIn, HourOut, MinOut, Sec, MSec: Word;
begin
  inherited;
  with ReportCheckListScanDM do
  begin
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    begin
      HourIn := 0;
      MinIn := 0;
      if QueryTimeRegScan.FieldByName('DATETIME_IN').AsString <> '' then
        DecodeTime(RoundTime(QueryTimeRegScan.FieldByName('DATETIME_IN').AsDateTime,1),
          HourIn, MinIn, Sec, MSec);
      HourOut := 0;
      MinOut := 0;
      if QueryTimeRegScan.FieldByName('DATETIME_OUT').AsString <> '' then
        DecodeTime(Roundtime(QueryTimeRegScan.FieldByName('DATETIME_OUT').AsDateTime,1),
          HourOut, MinOut, Sec, MSec);
      ExportDetail(
        QueryTimeRegScan.FieldByName('EMPLOYEE_NUMBER').AsString,
        QueryTimeRegScan.FieldByName('DESCRIPTION').AsString,
        DateToStr(QueryTimeRegScan.FieldByName('SHIFT_DATE').AsDateTime), // 20013489
        DateToStr(QueryTimeRegScan.FieldByName('DATETIME_IN').AsDateTime),
        FillZero(HourIn) + ':' + FillZero(MinIn),
        FillZero(HourOut) + ':' + FillZero(MinOut),
        IntToStr(MyEmployee_Tot_Min),
        QueryTimeRegScan.FieldByName('WPLANT_CODE').AsString, // RV085.18.
        QueryTimeRegScan.FieldByName('WORKSPOT_CODE').AsString + ' ' +
          QueryTimeRegScan.FieldByName('WDESCRIPTION').AsString,
        QueryTimeRegScan.FieldByName('PROCESSED_YN').AsString,
        QueryTimeRegScan.FieldByName('IDCARD_IN').AsString,
        QueryTimeRegScan.FieldByName('IDCARD_OUT').AsString,
        FormatDateTime('dd-mm-yyyy hh:nn', QueryTimeRegScan.FieldByName('MUTATIONDATE').AsDateTime), // TD-25174
        QueryTimeRegScan.FieldByName('MUTATOR').AsString // TD-25174
        );
    end;
  end;
end;

procedure TReportCheckListScanQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);

begin
  inherited;
  if (ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('PROCESSED_YN').AsString = '') then
    PrintBand := False;
end;

// 20013489
procedure TReportCheckListScanQR.QRLabelShiftDatePrint(sender: TObject;
  var Value: String);
{var
  Year, Month, Day: Word;}
begin
  inherited;
{  DecodeDate(ReportCheckListScanDM.QueryTimeRegScan.
    FieldByName('SHIFT_DATE').AsDateTime, Year, Month, Day);
  if (Year = 1900) and (Month = 1) and (Day = 1) then
    Value := ''
  else
    Value := DateToStr(EncodeDate(Year, Month, Day));
}
  if not SystemDM.UseShiftDateSystem then // 20014327
    Value := ''
  else
  begin
    // 20013489
    if ReportCheckListScanDM.QueryTimeRegScan.
      FieldByName('SHIFT_DATE').AsDateTime = 0 then
      Value := ''
    else
      Value := DateToStr(Trunc(ReportCheckListScanDM.QueryTimeRegScan.
        FieldByName('SHIFT_DATE').AsDateTime));
  end;
end;

// 20013288
procedure TReportCheckListScanQR.QRLabelMutationDatePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FormatDateTime('dd-mm-yyyy hh:nn',
    ReportCheckListScanDM.QueryTimeRegScan.
      FieldByName('MUTATIONDATE').AsDateTime);
end;

procedure TReportCheckListScanQR.ChildBandTitleEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetails; // TD-23699
end;

procedure TReportCheckListScanQR.QRChildBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetails; // TD-23699
end;

// TD-23699
procedure TReportCheckListScanQR.QRDBTextShiftPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportCheckListScanDM do
  begin
    Value := QueryTimeRegScan.FieldByName('SHIFT_NUMBER').AsString + ' - ' +
      QueryTimeRegScan.FieldByName('SDESCRIPTION').AsString;
  end;
end;

// TD-23699
procedure TReportCheckListScanQR.QRDBTextDepartmentPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportCheckListScanDM do
  begin
    Value := QueryTimeRegScan.FieldByName('DEPARTMENT_CODE').AsString + ' - ' +
      QueryTimeRegScan.FieldByName('DDESCRIPTION').AsString;
  end;
end;

// TD-23699
procedure TReportCheckListScanQR.ChildBandTitleEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
       ExportDetail('', '', '', QRLblShift.Caption, '', '',
      '', QRLblDepartment.Caption, QRLblJob.Caption, '', '', '',
      '', '');
end;

// TD-23699
procedure TReportCheckListScanQR.QRChildBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  with ReportCheckListScanDM do
  begin
    if BandPrinted then
      if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
        ExportDetail('', '', '',
        QueryTimeRegScan.FieldByName('SHIFT_NUMBER').AsString + ' - ' +
        QueryTimeRegScan.FieldByName('SDESCRIPTION').AsString, '', '',
        '',
        QueryTimeRegScan.FieldByName('DEPARTMENT_CODE').AsString + ' - ' +
        QueryTimeRegScan.FieldByName('DDESCRIPTION').AsString,
        QueryTimeRegScan.FieldByName('JOB_CODE').AsString + ' - ' +
        QueryTimeRegScan.FieldByName('JOBDESC').AsString,
        '', '', '',
        '', '');
  end;
end;

// TD-23699
procedure TReportCheckListScanQR.QRDBTextJobPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportCheckListScanDM do
  begin
    Value := QueryTimeRegScan.FieldByName('JOB_CODE').AsString + ' - ' +
      QueryTimeRegScan.FieldByName('JOBDESC').AsString;
  end;
end;

end.
