(*
  Changes:
    MR:05-11-2004 Order 550349 Plan in other plants.
    MRA: 14-SEP-2009 Order 550470 - Prevent selecting year 2099.
      Do not allow to select year 2099.
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    - Bugfix when showing calling this form from EmployeeAvailability-form.
  SO:19-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
  MRA:2-SEP-2010. RV067.MRA.23
  - Show week-period for year-week-component.
  MRA:14-FEB-2014 20011800
  - Final Run System
  - Prevent changing availability based on last-export-date of final run.
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:23-APR-2018 GLOB3-94
  - Problem when 2 users work in planning dialogs
  - Always allow to use Refresh-button to refresh (not only when it is red).
  - If in edit-mode, then do NOT refresh when refresh-button is clicked.
  MRA:18-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:26-JUL-2018 GLOB3-60 Bugfix.
  - It can give an access violation when the grid is adjusted per line because
    of the number of timeblocks. Because of that we determine the
    max. number of timeblocks based on selections (one plant or for all plants).
    This means only the details-panel is showing the correct number of
    timeblocks, and the grid shows always the maximum.
  MRA:24-JAN-2019 GLOB3-225
  - Changes for extension 4 to 10 blocks for planning
  - When MaxTimeblocks=4 do not show extra blocks in grid
*)

unit StaffAvailabilityFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, StdCtrls, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL,
  dxDBCtrl, dxDBGrid, ExtCtrls, Mask, dxEditor, dxExEdtr, dxEdLib, DBCtrls,
  Buttons, dxDBTLCl, dxGrClms, Menus, Dblup1a, SideMenuUnitMaintenanceFRM;

type
  TStaffAvailabilityF = class(TGridBaseF)
    GroupBox15: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    GroupBoxDays: TGroupBox;
    dxMasterGridColumnNo: TdxDBGridColumn;
    dxMasterGridColumnName: TdxDBGridColumn;
    dxMasterGridColumnC11: TdxDBGridColumn;
    dxMasterGridColumnC12: TdxDBGridColumn;
    dxMasterGridColumnC13: TdxDBGridColumn;
    dxMasterGridColumnC14: TdxDBGridColumn;
    dxMasterGridColumnC111: TdxDBGridColumn;
    dxMasterGridColumnC21: TdxDBGridColumn;
    dxMasterGridColumnC22: TdxDBGridColumn;
    dxMasterGridColumnC23: TdxDBGridColumn;
    dxMasterGridColumnC24: TdxDBGridColumn;
    dxMasterGridColumnC211: TdxDBGridColumn;
    dxMasterGridColumnC31: TdxDBGridColumn;
    dxMasterGridColumnC32: TdxDBGridColumn;
    dxMasterGridColumnC33: TdxDBGridColumn;
    dxMasterGridColumnC34: TdxDBGridColumn;
    dxMasterGridColumnC311: TdxDBGridColumn;
    dxMasterGridColumnC41: TdxDBGridColumn;
    dxMasterGridColumnC42: TdxDBGridColumn;
    dxMasterGridColumnC43: TdxDBGridColumn;
    dxMasterGridColumnC44: TdxDBGridColumn;
    dxMasterGridColumnC411: TdxDBGridColumn;
    dxMasterGridColumnC51: TdxDBGridColumn;
    dxMasterGridColumnC52: TdxDBGridColumn;
    dxMasterGridColumnC53: TdxDBGridColumn;
    dxMasterGridColumnC54: TdxDBGridColumn;
    dxMasterGridColumnC511: TdxDBGridColumn;
    dxMasterGridColumnC61: TdxDBGridColumn;
    dxMasterGridColumnC62: TdxDBGridColumn;
    dxMasterGridColumnC63: TdxDBGridColumn;
    dxMasterGridColumnC64: TdxDBGridColumn;
    dxMasterGridColumnC611: TdxDBGridColumn;
    dxMasterGridColumnC71: TdxDBGridColumn;
    dxMasterGridColumnC72: TdxDBGridColumn;
    dxMasterGridColumnC73: TdxDBGridColumn;
    dxMasterGridColumnC74: TdxDBGridColumn;
    dxMasterGridColumnC711: TdxDBGridColumn;
    PopupMenuTB: TPopupMenu;
    PopupMenuShift: TPopupMenu;
    CheckBoxAllTeams: TCheckBox;
    ComboBoxPlusPlants: TComboBoxPlus;
    BitBtnRefresh: TBitBtn;
    dxMasterGridColumn38: TdxDBGridColumn;
    BitBtnEmployeeAv: TBitBtn;
    BitBtnStandard: TBitBtn;
    BitBtnFillStav: TBitBtn;
    BitBtnReport: TBitBtn;
    dxMasterGridColumnSTARTDATE: TdxDBGridColumn;
    dxMasterGridColumnENDDATE: TdxDBGridColumn;
    CheckBoxPlanInOtherPlants: TCheckBox;
    dxMasterGridColumnP1: TdxDBGridColumn;
    dxMasterGridColumnP2: TdxDBGridColumn;
    dxMasterGridColumnP3: TdxDBGridColumn;
    dxMasterGridColumnP4: TdxDBGridColumn;
    dxMasterGridColumnP5: TdxDBGridColumn;
    dxMasterGridColumnP6: TdxDBGridColumn;
    dxMasterGridColumnP7: TdxDBGridColumn;
    EditP1: TEdit;
    EditP2: TEdit;
    EditP3: TEdit;
    EditP4: TEdit;
    EditP5: TEdit;
    EditP6: TEdit;
    EditP7: TEdit;
    CheckBoxAllPlant: TCheckBox;
    cmbPlusTeamFrom: TComboBoxPlus;
    cmbPlusTeamTo: TComboBoxPlus;
    Label14: TLabel;
    Label12: TLabel;
    lblPeriod: TLabel;
    dxMasterGridColumnEMPPLANT_CODE: TdxDBGridColumn;
    BitBtnWorkSchedule: TBitBtn;
    ScrollBox1: TScrollBox;
    GroupBoxDay1: TGroupBox;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit111: TEdit;
    GroupBoxDay2: TGroupBox;
    Edit21: TEdit;
    Edit22: TEdit;
    Edit23: TEdit;
    Edit24: TEdit;
    Edit211: TEdit;
    GroupBoxDay3: TGroupBox;
    Edit31: TEdit;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    Edit311: TEdit;
    GroupBoxDay4: TGroupBox;
    Edit41: TEdit;
    Edit42: TEdit;
    Edit43: TEdit;
    Edit44: TEdit;
    Edit411: TEdit;
    GroupBoxDay5: TGroupBox;
    Edit51: TEdit;
    Edit52: TEdit;
    Edit53: TEdit;
    Edit54: TEdit;
    Edit511: TEdit;
    GroupBoxDay6: TGroupBox;
    Edit61: TEdit;
    Edit62: TEdit;
    Edit63: TEdit;
    Edit64: TEdit;
    Edit611: TEdit;
    GroupBoxDay7: TGroupBox;
    Edit71: TEdit;
    Edit72: TEdit;
    Edit73: TEdit;
    Edit74: TEdit;
    Edit711: TEdit;
    Edit15: TEdit;
    Edit16: TEdit;
    Edit17: TEdit;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit110: TEdit;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Edit28: TEdit;
    Edit29: TEdit;
    Edit210: TEdit;
    Edit35: TEdit;
    Edit36: TEdit;
    Edit37: TEdit;
    Edit38: TEdit;
    Edit39: TEdit;
    Edit310: TEdit;
    Edit45: TEdit;
    Edit46: TEdit;
    Edit47: TEdit;
    Edit48: TEdit;
    Edit49: TEdit;
    Edit410: TEdit;
    Edit55: TEdit;
    Edit56: TEdit;
    Edit57: TEdit;
    Edit58: TEdit;
    Edit59: TEdit;
    Edit510: TEdit;
    Edit65: TEdit;
    Edit66: TEdit;
    Edit67: TEdit;
    Edit68: TEdit;
    Edit69: TEdit;
    Edit610: TEdit;
    Edit75: TEdit;
    Edit76: TEdit;
    Edit77: TEdit;
    Edit78: TEdit;
    Edit79: TEdit;
    Edit710: TEdit;
    dxMasterGridColumnC15: TdxDBGridColumn;
    dxMasterGridColumnC16: TdxDBGridColumn;
    dxMasterGridColumnC17: TdxDBGridColumn;
    dxMasterGridColumnC18: TdxDBGridColumn;
    dxMasterGridColumnC19: TdxDBGridColumn;
    dxMasterGridColumnC110: TdxDBGridColumn;

    dxMasterGridColumnC25: TdxDBGridColumn;
    dxMasterGridColumnC26: TdxDBGridColumn;
    dxMasterGridColumnC27: TdxDBGridColumn;
    dxMasterGridColumnC28: TdxDBGridColumn;
    dxMasterGridColumnC29: TdxDBGridColumn;
    dxMasterGridColumnC210: TdxDBGridColumn;

    dxMasterGridColumnC35: TdxDBGridColumn;
    dxMasterGridColumnC36: TdxDBGridColumn;
    dxMasterGridColumnC37: TdxDBGridColumn;
    dxMasterGridColumnC38: TdxDBGridColumn;
    dxMasterGridColumnC39: TdxDBGridColumn;
    dxMasterGridColumnC310: TdxDBGridColumn;

    dxMasterGridColumnC45: TdxDBGridColumn;
    dxMasterGridColumnC46: TdxDBGridColumn;
    dxMasterGridColumnC47: TdxDBGridColumn;
    dxMasterGridColumnC48: TdxDBGridColumn;
    dxMasterGridColumnC49: TdxDBGridColumn;
    dxMasterGridColumnC410: TdxDBGridColumn;

    dxMasterGridColumnC55: TdxDBGridColumn;
    dxMasterGridColumnC56: TdxDBGridColumn;
    dxMasterGridColumnC57: TdxDBGridColumn;
    dxMasterGridColumnC58: TdxDBGridColumn;
    dxMasterGridColumnC59: TdxDBGridColumn;
    dxMasterGridColumnC510: TdxDBGridColumn;

    dxMasterGridColumnC65: TdxDBGridColumn;
    dxMasterGridColumnC66: TdxDBGridColumn;
    dxMasterGridColumnC67: TdxDBGridColumn;
    dxMasterGridColumnC68: TdxDBGridColumn;
    dxMasterGridColumnC69: TdxDBGridColumn;
    dxMasterGridColumnC610: TdxDBGridColumn;

    dxMasterGridColumnC75: TdxDBGridColumn;
    dxMasterGridColumnC76: TdxDBGridColumn;
    dxMasterGridColumnC77: TdxDBGridColumn;
    dxMasterGridColumnC78: TdxDBGridColumn;
    dxMasterGridColumnC79: TdxDBGridColumn;
    dxMasterGridColumnC710: TdxDBGridColumn;
    procedure DefaultEditValidation(Sender: TObject; var Key: Char);
    procedure DefaultShiftEditValidation(Sender: TObject; var Key: Char);
    procedure DefaultEditChange(Sender: TObject);
    procedure DefaultShiftEditChange(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure TBPopupSetValue(Sender: TObject);
    procedure ShiftPopupSetValue(Sender: TObject);
    procedure BitBtnStandardClick(Sender: TObject);
    procedure DefaultChangeShiftNo(Sender: TObject; var Key: Word;
  		Shift: TShiftState);
    procedure BitBtnReportClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BitBtnEmployeeAvClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChangeFilterItem(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure DefaultEditPopUp(Sender: TObject; MousePos: TPoint;
    	var Handled: Boolean);
    procedure dxBarButtonRecordDetailsClick(Sender: TObject);
    procedure EditKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit11MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PopupMenuTBPopup(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure BitBtnFillStavClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure CheckBoxPlanInOtherPlantsClick(Sender: TObject);
    procedure CheckBoxAllPlantClick(Sender: TObject);
    procedure cmbPlusTeamFromCloseUp(Sender: TObject);
    procedure cmbPlusTeamToCloseUp(Sender: TObject);
    procedure BitBtnWorkScheduleClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
     FCtrlState, FPopupOpen: Boolean;
     FVisible_Empl_Avail, FVisible_Rep_Avail: Boolean;
    FPlanInOtherPlants: Boolean;
    FMaxTBCount: Integer;
    procedure SetPlanInOtherPlants(const Value: Boolean);
    procedure CreatePopupMenu;
    procedure ShowPeriod;
    procedure PopulatePlant(var TeamCode: string);
    procedure SetDayDescription;
    procedure SetWeekDates;
    procedure RestoreValues;
    procedure ChangeSaveCancelState(NewState: Boolean);
    procedure SetDMFilter(Active: Boolean; Insert_Avb: String);
{    function NewValuesOk: String; }
    procedure  ResetAvailabilities;
    procedure SetDisableBtn(Status: Boolean);
    procedure GotoCurrentRecord(
      RecNoMax, RecNo: Double; IndexColumn: Integer);
    procedure SetValueForAllEditFields(Value, ParentName: String; Tag: Integer);
    procedure SetVisibleButtons;
    procedure GroupBoxResize(ADay, ATBCount: Integer);
    procedure GridColumnVisible(ADay, ATBCount: Integer);
    procedure GridColumnSetCaption;
    procedure HideColumns;
  public
    { Public declarations }
    constructor Create(Sender: TComponent; Team, Plant: String;
    	 AYear, AWeek: Integer); reintroduce; overload;
    property Visible_Empl_Avail: Boolean  read FVisible_Empl_Avail
      write FVisible_Empl_Avail;
     property Visible_Rep_Avail: Boolean  read FVisible_Rep_Avail
      write FVisible_Rep_Avail;
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write SetPlanInOtherPlants;
    property MaxTBCount: Integer read FMaxTBCount write FMaxTBCount;
  end;

function StaffAvailabilityF(Team,Plant: String; Year,
  Week: Integer): TStaffAvailabilityF;

var
  StaffAvailabilityF_HND: TStaffAvailabilityF;

implementation

{$R *.DFM}

uses
  StaffAvailabilityDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  SystemDMT, UPimsMessageRes, DialogReportStaffAvailabilityFRM,
  CalculateTotalHoursDMT, DialogProcessWorkScheduleFRM;

function StaffAvailabilityF(Team,Plant: String; Year,
  Week: Integer): TStaffAvailabilityF;
begin
  if (StaffAvailabilityF_HND = nil) then
    StaffAvailabilityF_HND :=
      TStaffAvailabilityF.Create(Application, Team, Plant, Year, Week);
  Result := StaffAvailabilityF_HND;
end;

procedure TStaffAvailabilityF.FormShow(Sender: TObject);
begin
  if dxMasterGrid.DataSource = nil then
    dxMasterGrid.DataSource := StaffAvailabilityDM.DataSourceAvailability;
   //CAR 21-7-2003
//  TBLenghtClass := TTBLengthClass.Create;
//  inherited;
//CAR 15-9-2003
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    dxBarBDBNavInsert.Visible := ivNever;
    dxBarBDBNavDelete.Visible := ivNever;
    dxBarBDBNavPost.Visible := ivNever;
    dxBarBDBNavCancel.Visible := ivNever;
    dxBarButtonEditMode.Visible := ivNever;
    GroupBoxDays.Enabled := False;
  end;
  SetVisibleButtons;
  // MR:25-11-2004 Get rid of ugly buttons.
  cmbPlusTeamFrom.ShowSpeedButton := False;
  cmbPlusTeamFrom.ShowSpeedButton := True;
  cmbPlusTeamTo.ShowSpeedButton := False;
  cmbPlusTeamTo.ShowSpeedButton := True;
  ComboBoxPlusPlants.ShowSpeedButton := False;
  ComboBoxPlusPlants.ShowSpeedButton := True;
  ShowPeriod;
end;

procedure TStaffAvailabilityF.SetDayDescription;
var
  Day, Index: integer;
  C: TComponent;
begin
  for Day := 1 to 7 do
  begin
    C := FindComponent(Format('GroupBoxDay%d',[Day]));
    if Assigned(C) then
      if (C is TGroupBox) then
        TGroupBox(C).Caption := SystemDM.GetDayWDescription(Day);
  end;
  for Index := 1 to dxMasterGrid.Bands.Count -1 do
    dxMasterGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
end;

procedure TStaffAvailabilityF.SetWeekDates;
var
  Index: integer;
begin
  StaffAvailabilityDM.WeekDays[1] :=
    ListProcsF.DateFromWeek(
      Round(dxSpinEditYear.Value),Round(dxSpinEditWeek.Value), 1);
  for Index := 2 to 7 do
    StaffAvailabilityDM.WeekDays[1] :=
      StaffAvailabilityDM.WeekDays[1] + Index;
end;

procedure TStaffAvailabilityF.DefaultEditValidation(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (Key in ValidChars) then
  begin
    key := #0;
    exit;
  end
  else
    ChangeSaveCancelState(True);
end;

procedure TStaffAvailabilityF.DefaultEditChange(Sender: TObject);
var
  TxtValue: string;
begin
  TxtValue := TEdit(Sender).Text;
  if TxtValue = Disable then
  begin
    TEdit(Sender).Enabled := False;
    TEdit(Sender).Color := clBtnFace;
  end
  else
  begin
    TEdit(Sender).Enabled := True;
    TEdit(Sender).Color := clRequired;
  end;
end;

procedure TStaffAvailabilityF.DefaultShiftEditValidation(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if not (Key in ValidShiftChars) then
  begin
    key := #0;
    Exit;
  end
  else
    ChangeSaveCancelState(True);
end;

procedure TStaffAvailabilityF.DefaultShiftEditChange(Sender: TObject);
begin
  if (TEdit(Sender).Text = Disable) or (TEdit(Sender).Text = NotAvailable) then
  begin
    TEdit(Sender).Enabled := False;
    TEdit(Sender).Color := clDisabled;
  end
  else
  begin
    TEdit(Sender).Enabled := True;
    TEdit(Sender).Color := clWindow;
  end;
end;

// function used for restore position of the screen after save and refresh grid
// after save
procedure TStaffAvailabilityF.GotoCurrentRecord(
  RecNoMax, RecNo: Double; IndexColumn: Integer);
(*
var
  Count :Integer;
*)
begin
  // MR:10-03-2005 Pims -> Oracle
  // Use 'locate' to go to current record.
(*
  if dxBarButtonSort.down then
  begin
    StaffAvailabilityDM.GotoRecord(Recno);
    Exit;
  end;
  StaffAvailabilityDM.GotoRecord(RecNoMax - 1);

  Count := dxMasterGrid.Count-1 ;
  while (Count >= 0) do
  begin
    if (dxMasterGrid.Items[Count].Values[IndexColumn] = Recno) then
       break;
    dxMasterGrid.GotoPrev(True);
    dec(Count);
  end;
*)
  try
    if StaffAvailabilityDM.QueryAvailability.Active then
      if not StaffAvailabilityDM.QueryAvailability.Eof then
        StaffAvailabilityDM.QueryAvailability.Locate('RECNO', Recno, []);
  except
    on E:Exception do
      WErrorLog(E.Message);
  end;
end;

procedure TStaffAvailabilityF.dxBarBDBNavPostClick(Sender: TObject);
var
  Emp, i, IndexColumn: Integer;
  RecNoMax: Double;
begin
  StaffAvailabilityDM.SaveChanges(True, Emp);
  StaffAvailabilityDM.FEmpl :=  Emp;
  // get index of column with EMPLOYEE_NUMBER field
  IndexColumn := 0;
  for i := 0 to dxMasterGrid.columncount -1 do
    if dxMasterGrid.Columns[i].FieldName = 'RECNO' then
    begin
      IndexColumn := i;
      break;
    end;
  RecNoMax := dxMasterGrid.Items[dxMasterGrid.Count-1].Values[IndexColumn];
  GotoCurrentRecord(RecNoMax,
    StaffAvailabilityDM.QueryAvailability.FieldByName('RECNO').AsFloat,
    IndexColumn);
  ChangeSaveCancelState(False);
  //car 550276 - initialize array for saving days changed
  StaffAvailabilityDM.InitializeDayChangedArray;
end;

procedure TStaffAvailabilityF.ChangeSaveCancelState(NewState: Boolean);
begin
  dxBarBDBNavPost.Enabled := NewState;
  dxBarBDBNavCancel.Enabled := NewState;
end;

procedure TStaffAvailabilityF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  RestoreValues;

end;

procedure TStaffAvailabilityF.RestoreValues;
begin
  StaffAvailabilityDM.PopulateAvailability(
    StaffAvailabilityDM.QueryAvailability);
  ChangeSaveCancelState(False);
  //car 550276 - initialize array for saving days changed
  StaffAvailabilityDM.InitializeDayChangedArray;
end;

procedure TStaffAvailabilityF.TBPopupSetValue(Sender: TObject);
var
  AEdit: TEdit;
begin
  AEdit := TEdit(PopupMenuTB.PopUpComponent);
  if TMenuItem(Sender).Caption = MenuItemNotAvailable then
    AEdit.Text := NotAvailable
  else
    AEdit.Text := CharFromStr(TMenuItem(Sender).Caption,1,' ');
  ChangeSaveCancelState(True);
  //car 550276
  StaffAvailabilityDM.FDayChanged[StrToInt(Copy(AEdit.Name,5,1))] := 1;
  SetValueForAllEditFields(AEdit.Text,AEdit.Parent.Name, 1);
  FPopupOpen := False;
end;

procedure TStaffAvailabilityF.ShiftPopupSetValue(Sender: TObject);
var
  AEdit: TEdit;
//  Index: integer;
  key: Word;
  Shift: TShiftState;
  PlantCode: String;
  PlantCmp: TComponent;
  Day: Integer;
  function FilterPlantShift(AField: String; var APlantCode: String): String;
  var
    I: Integer;
  begin
    Result := AField;
    APlantCode := '';
    if PlanInOtherPlants then
      if AField <> '' then
      begin
        I := Pos('-', AField);
        try
          if I > 0 then
          begin
            APlantCode := Trim(Copy(AField, 1, I - 1));
            Result := Trim(Copy(AField, I + 1, Length(AField)));
          end;
        except
          Result := '';
          APlantCode := '';
        end;
      end;
  end;
begin
  AEdit := TEdit(PopupMenuShift.PopUpComponent);
  if (TMenuItem(Sender).Caption = '_') then
    AEdit.Text := '-'
  else
  begin
    Day := GetNoFromStr(AEdit.Name) div 10;
    if PlanInOtherPlants then
      AEdit.Text := FilterPlantShift(TMenuItem(Sender).Caption, PlantCode)
    else
    begin
      AEdit.Text := TMenuItem(Sender).Caption;
      PlantCode := StaffAvailabilityDM.DataFilterPlantCode;
    end;
    // Always fill 'EditP' with the PlantCode.
    PlantCmp := FindComponent(Format('EditP%d', [Day]));
    if Assigned(PlantCmp) then
      if (PlantCmp is TEdit) then
        TEdit(PlantCmp).Text := PlantCode;
    // GLOB3-60 This can give an error when PlantCmp is NULL, so leave it out.
//    else
//      (PlantCmp as TEdit).Text := '';
  end;

  AEdit.OnChange(AEdit);
  ChangeSaveCancelState(True);
  //car 550276
  StaffAvailabilityDM.FDayChanged[StrToInt(Copy(AEdit.Name,5,1))] := 1;
  DefaultChangeShiftNo(AEdit, Key, Shift);

  SetValueForAllEditFields(AEdit.Text,AEdit.Parent.Name, 5);
  FPopupOpen := False;
end; // ShiftPopupSetValue

procedure TStaffAvailabilityF.BitBtnStandardClick(Sender: TObject);
var
  DailyAv: TDailyAv;
  day, index, tbindex, EmployeeNumber: integer;
  Cmp: TComponent;
  DepartmentCode: String;
  FinalRunExportDate: TDateTime; // 20011800
  GoOn: Boolean; // 20011800
  SkipCount: Integer; // 20011800
begin
  inherited;
  // Default Values
//  EmployeeNumber := 0;
  DepartmentCode := NullStr;
  SkipCount := 0; // 20011800
  if not StaffAvailabilityDM.DataFilter.Active then
    Exit;
  if StaffAvailabilityDM.QueryAvailability.Active and
    (not StaffAvailabilityDM.QueryAvailability.IsEmpty) then
  begin
    DepartmentCode := StaffAvailabilityDM.QueryAvailability.
      FieldByName('DEPARTMENT_CODE').asString;
    EmployeeNumber := StaffAvailabilityDM.QueryAvailability.
    	FieldByName('EMPLOYEE_NUMBER').asInteger;
    // 20011800
    FinalRunExportDate := NullDate;
    if SystemDM.UseFinalRun then
    begin
      FinalRunExportDate := SystemDM.FinalRunExportDateByPlant(
        StaffAvailabilityDM.QueryAvailability.
        FieldByName('EMPPLANT_CODE').AsString);
    end;
    for day := 1 to 7 do
    begin
      // 20011800
      GoOn := True;
      if SystemDM.UseFinalRun then
      begin
        if FinalRunExportDate <> NullDate then
        begin
          GoOn :=
            StaffAvailabilityDM.WeekDays[Day] > FinalRunExportDate;
          if not GoOn then
            inc(SkipCount);
        end;
      end;
      if GoOn then
      begin
        StaffAvailabilityDM.RestoreDefault(DailyAv, EmployeeNumber, Day,
          DepartmentCode);
        for index := 0 to SystemDM.MaxTimeblocks do
        begin
          if index = 0 then
            tbindex := ISHIFT
          else
            tbindex := index;
          Cmp := FindComponent(Format('Edit%d%d',[day,tbindex]));
          if Assigned(Cmp) then
            if (Cmp is TEdit) then
              TEdit(Cmp).Text := DailyAv[tbindex];
        end; // for index
      end; // if GoOn
    end; // for day
  end; // if
  dxBarBDBNavPost.OnClick(dxBarBDBNavPost);
  if SkipCount > 0 then // 20011800
    DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]);
  BitBtnRefreshClick(Sender);
end;

procedure TStaffAvailabilityF.PopulatePlant(var TeamCode: string);
var
  TblFilter: string;
begin
  if TeamCode=NullStr then
    TeamCode := ALLTEAMS;
  TeamCode := DoubleQuote(TeamCode);
  TblFilter := Nullstr;
  if TeamCode = ALLTEAMS then
    ListProcsF.FillComboBoxPlant(StaffAvailabilityDM.QueryPlant, True,
      ComboBoxPlusPlants)
  else
  begin
    // MR:17-02-2005 Order 550378 Team-From-To.
    if cmbPlusTeamFrom.DisplayValue = cmbPlusTeamTo.DisplayValue then
    begin
      StaffAvailabilityDM.QueryPlantTeam.Close;
      StaffAvailabilityDM.QueryPlantTeam.ParamByName('TEAM_CODE').Value :=
        GetStrValue(cmbPlusTeamFrom.Value);
      StaffAvailabilityDM.QueryPlantTeam.Open;
      if StaffAvailabilityDM.QueryPlantTeam.IsEmpty then
        ListProcsF.FillComboBoxPlant(StaffAvailabilityDM.QueryPlant, True,
        ComboBoxPlusPlants)
      else
        ListProcsF.FillComboBox(StaffAvailabilityDM.QueryPlantTeam,
          ComboBoxPlusPlants, GetStrValue(cmbPlusTeamFrom.Value),'', True,
          'TEAM_CODE',  '', 'PLANT_CODE', 'DESCRIPTION');
    end
    else
      ListProcsF.FillComboBoxPlant(StaffAvailabilityDM.QueryPlant, True,
        ComboBoxPlusPlants);
  end;
  //RV067.4.
  CreatePopupMenu;
end;

procedure TStaffAvailabilityF.DefaultChangeShiftNo(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Index, Day, Tb, EmployeeNumber, ShiftNumber: Integer;
  PlantCode, DepartmentCode: String;
  TBS: TTimeBlock;
  PlantCmp, Cmp: TComponent;
  TBResult: String;
  TBCount: Integer;
begin
  // Disable not available timeblocks
  // Edit-name has format: Editdbb where d=day (1 to 7) and bb=timeblock (1 to 11)
  // When bb=ISHIFT it stores a shift.
  try
//  day := GetNoFromStr(TEdit(Sender).Name) div 10;
//  tb := GetNoFromStr(TEdit(Sender).Name) mod 10;
    day := StrToInt(Copy(TEdit(Sender).Name, 5, 1));
    tb := StrToInt(Copy(TEdit(Sender).Name, 6, 2));
  except
    day := 0;
    tb := 0;
  end;
  if tb = ISHIFT then
  begin
    for index := 1 to SystemDM.MaxTimeblocks do
    	TBS[index] := False;
    if StaffAvailabilityDM.QueryAvailability.Active and
    	(not StaffAvailabilityDM.QueryAvailability.isEmpty) then
    begin
      ShiftNumber := -1;
      //car 550276
      if TEdit(Sender).Text <= NotAvailable then
        ShiftNumber := -1;
      if TEdit(Sender).Text > NotAvailable then
        ShiftNumber := StrToInt(TEdit(Sender).Text);

      DepartmentCode := StaffAvailabilityDM.QueryAvailability.
        FieldByName('DEPARTMENT_CODE').asString;
      EmployeeNumber := StaffAvailabilityDM.QueryAvailability.
        FieldByName('EMPLOYEE_NUMBER').asInteger;
      PlantCode := StaffAvailabilityDM.DataFilterPlantCode;
      if PlanInOtherPlants then
      begin
        PlantCmp := FindComponent(Format('EditP%d', [day]));
        if Assigned(PlantCmp) then
          if (PlantCmp is TEdit) then
            PlantCode := TEdit(PlantCmp).Text;
      end
      else
      begin
        // If not PlanInOtherPlants, fill this component 'EditP' with
        // the employee's plantcode.
        PlantCmp := FindComponent(Format('EditP%d', [day]));
        if Assigned(PlantCmp) then
          if (PlantCmp is TEdit) then
            TEdit(PlantCmp).Text := PlantCode;
      end;
      TBResult := WhatTimeBlock(StaffAvailabilityDM.QueryWork, EmployeeNumber,
        ShiftNumber, PlantCode, DepartmentCode, TBs);
      if PlanInOtherPlants and (TBResult = NullStr) then
      begin
        PlantCode := StaffAvailabilityDM.DataFilterPlantCode;
        TBResult := WhatTimeBlock(StaffAvailabilityDM.QueryWork, EmployeeNumber,
          ShiftNumber, PlantCode, DepartmentCode, TBs);
      end;
    end;
    TBCount := 0;
    for index := 1 to SystemDM.MaxTimeblocks do
    begin
      Cmp := FindComponent(Format('Edit%d%d',[day,index]));
      if Assigned(Cmp) then
        if (Cmp is TEdit) then
        begin
          //CAR 550276
          if (TEdit(Sender).Text = NotAvailable) or
            (TEdit(Sender).Text = '') then
            TEdit(Cmp).Text := Disable;
          if TBs[index] then
          begin
            if TEdit(Cmp).Text = Disable then
              TEdit(Cmp).Text := '';
            TEdit(Cmp).Enabled := True;
            TEdit(Cmp).Color := clRequired;
            TEdit(Cmp).Visible := True;
            inc(TBCount);
          end
          else
          begin
            TEdit(Cmp).Text := Disable;
            TEdit(Cmp).Color := clBtnFace;
            TEdit(Cmp).Enabled := False;
            TEdit(Cmp).Visible := False;
          end;
        end;
    end; // for index
    GroupBoxResize(Day, TBCount+1);
  end; // if tb
end; // DefaultChangeShiftNo

//RV067.4.
procedure TStaffAvailabilityF.CreatePopupMenu;
var
  NewMenuItem: TMenuItem;
  SaveSql: String;
begin
  PopupMenuTB.Items.Clear;
  NewMenuItem := TMenuItem.Create(Self);
  NewMenuItem.Caption := Available;
  NewMenuItem.OnClick := TBPopupSetValue;
  PopupMenuTB.Items.Add(NewMenuItem);
  NewMenuItem := TMenuItem.Create(Self);
  NewMenuItem.OnClick := TBPopupSetValue;
  NewMenuItem.Caption := '_';
  PopupMenuTB.Items.Add(NewMenuItem);

  SaveSql := StaffAvailabilityDM.QueryAbsReason.Sql.Text;
  if
    StaffAvailabilityDM.QueryPlant.Active and
    (not StaffAvailabilityDM.QueryPlant.Eof)
  then
  begin
    StaffAvailabilityDM.QueryPlant.First;
    StaffAvailabilityDM.QueryPlant.Locate('PLANT_CODE', GetStrValue(ComboBoxPlusPlants.Value), []);
    SystemDM.UpdateAbsenceReasonPerCountry(
      StaffAvailabilityDM.QueryPlant.FieldByName('Country_Id').AsInteger,
      StaffAvailabilityDM.QueryAbsReason
    );
  end;
  if not StaffAvailabilityDM.QueryAbsReason.Active then
    StaffAvailabilityDM.QueryAbsReason.Open;
  StaffAvailabilityDM.ClientDataSetAbsRsn.Close;
  StaffAvailabilityDM.ClientDataSetAbsRsn.Open;
  if StaffAvailabilityDM.ClientDataSetAbsRsn.Active then
  begin
    StaffAvailabilityDM.ClientDataSetAbsRsn.First;
    While not StaffAvailabilityDM.ClientDataSetAbsRsn.Eof do
      begin
        NewMenuItem := TMenuItem.Create(Self);
        NewMenuItem.OnClick := TBPopupSetValue;
        NewMenuItem.Caption := StaffAvailabilityDM.ClientDataSetAbsRsn.
          FieldByName('ABSENCEREASON_CODE').asString + ' - ' +
            StaffAvailabilityDM.ClientDataSetAbsRsn.FieldByName(
              'DESCRIPTION').asString;
        PopupMenuTB.Items.Add(NewMenuItem);
        StaffAvailabilityDM.ClientDataSetAbsRsn.Next;
      end;
  end;
  StaffAvailabilityDM.QueryAbsReason.Close;
  StaffAvailabilityDM.QueryAbsReason.Sql.Text := SaveSql;
end;

procedure TStaffAvailabilityF.BitBtnReportClick(Sender: TObject);
var
  TeamFrom, TeamTo, PlantCode: String;
  Year, Week: Integer;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtInformation, [mbOk]);
    Exit;
  end;
  // MR:17-02-2005 Order 550378 Team-From-To.
  TeamFrom := TStaffAvailabilityDM(GridFormDM).DataFilter.TeamFrom;
  TeamTo := TStaffAvailabilityDM(GridFormDM).DataFilter.TeamTo;
  PlantCode := TStaffAvailabilityDM(GridFormDM).DataFilter.PlantCode;
  Year := TStaffAvailabilityDM(GridFormDM).DataFilter.Year ;
  Week := TStaffAvailabilityDM(GridFormDM).DataFilter.Week;
  try
    if TeamFrom = ALLTEAMS then
    begin
      TeamFrom := NullStr;
      TeamTo := NullStr;
    end;
    if PlantCode = ALLPLANTS then
      PlantCode := NullStr;
    DialogReportStaffAvailabilityF :=
      TDialogReportStaffAvailabilityF.Create(Application, TeamFrom, TeamTo,
        PlantCode, Year, Week);
    DialogReportStaffAvailabilityF.ShowModal;
  finally
    DialogReportStaffAvailabilityF.Free;
//    TBLenghtClass := TTBLengthClass.Create;
  //  TBLenghtClass.OpenDataTBLength;
  end;
end;

constructor TStaffAvailabilityF.Create(Sender: TComponent;
  Team, Plant: String; AYear, AWeek: Integer);
var
  year, month, day, Week: word;
  TeamCode: String;
begin
  inherited Create(Sender);
  StaffAvailabilityDM := CreateFormDM(TStaffAvailabilityDM);

  CheckBoxPlanInOtherPlants.Checked := False;
  CheckBoxPlanInOtherPlants.Visible :=
    SystemDM.TablePIMSettingsEMPLOYEES_ACROSS_PLANTS_YN.AsString = 'Y';
  PlanInOtherPlants := False;
  StaffAvailabilityDM.PlanInOtherPlants := PlanInOtherPlants;

  // MR:17-02-2005 Order 550378 Team-From-To.
  // Populate Team combobox.
  ListProcsF.FillComboBoxMaster(StaffAvailabilityDM.QueryTeam, 'TEAM_CODE',
    True, cmbPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(StaffAvailabilityDM.QueryTeam, 'TEAM_CODE',
    False, cmbPlusTeamTo);

  if Ayear = -1 then
  begin
    DecodeDate(Now, year, month, day);
    ListProcsF.WeekUitDat(Now, Year, Week);
    dxSpinEditYear.IntValue := year;
    dxSpinEditWeek.IntValue := Week;
    TeamCode := GetStrValue(cmbPlusTeamFrom.Value);
    PopulatePlant(TeamCode);
  end
  else
  begin
    dxSpinEditYear.Value := AYear;
    dxSpinEditWeek.Value := AWeek;
    if Team <> NullStr then
    begin
      StaffAvailabilityDM.QueryTeam.Locate('TEAM_CODE', Team, []);
      cmbPlusTeamFrom.Text :=
        StaffAvailabilityDM.QueryTeam.
          FieldByName('TEAM_CODE').AsString + Str_Sep +
          StaffAvailabilityDM.QueryTeam.
            FieldByName('DESCRIPTION').AsString;
      cmbPlusTeamTo.Text := cmbPlusTeamFrom.Text;
      // RV050.8. Also assign displayvalue! Or 'Value' will be empty!
      cmbPlusTeamFrom.DisplayValue := cmbPlusTeamFrom.Text;
      cmbPlusTeamTo.DisplayValue := cmbPlusTeamTo.Text;
    end
    else
      CheckBoxAllTeams.Checked := True;
    ComboBoxPlusPlants.Value := Plant;
    TeamCode := GetStrValue(cmbPlusTeamFrom.Value);
    PopulatePlant(TeamCode);
  end;
  SetDmFilter(False, 'N');
  SetWeekDates;
  dxBarBDBNavPost.visible := ivAlways;
  dxBarBDBNavCancel.visible := ivAlways;
  SetDayDescription;
  ChangeSaveCancelState(False);
  dsrcActive := StaffAvailabilityDM.DataSourceAvailability;
  ActiveGrid := dxMasterGrid;
end;

procedure TStaffAvailabilityF.FormDestroy(Sender: TObject);
begin
  inherited;
  StaffAvailabilityF_HND := Nil;
end;

procedure TStaffAvailabilityF.BitBtnEmployeeAvClick(Sender: TObject);
var
  Employee, Year, Week: Integer;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtInformation, [mbOk]);
    Exit;
  end;
   if StaffAvailabilityDM.QueryAvailability.Active and
  	(not StaffAvailabilityDM.QueryAvailability.isEmpty) then
  begin
    Year := TStaffAvailabilityDM(GridFormDM).DataFilter.Year;
    Week := TStaffAvailabilityDM(GridFormDM).DataFilter.Week;
    Employee := 0;
    if StaffAvailabilityDM <> nil then
      Employee := StaffAvailabilityDM.QueryAvailability.
    	FieldByName('EMPLOYEE_NUMBER').asInteger;
    SideMenuUnitMaintenanceF.SwitchEmplAvailForm(Employee, Year, Week);
  end;  
end;

procedure TStaffAvailabilityF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // RV050.8.
  // Turn filtering off to prevent a problem during OnRecordFilter!
  StaffAvailabilityDM.QueryPlant.Filtered := False;
  StaffAvailabilityDM.QueryPlant.Active := False;
  Action := caFree;
  //CAR 21-7-2003
//  TBLenghtClass.Free;
end;

procedure TStaffAvailabilityF.ChangeFilterItem(Sender: TObject);
var
  TeamCode: String;
  Year, Month, Day: Word;
begin
  if CheckBoxAllPlant.Checked then
    MaxTBCount := ShiftMaxTBCount
  else
    MaxTBCount := PlantShiftMaxTBCount(GetStrValue(ComboBoxPlusPlants.Value));

  // MRA:14-SEP-2009 Order 550470.
  if (dxSpinEditYear.Value = PIMS_TEMPLATE_YEAR) then
  begin
    DisplayMessage(SPimsTemplateYearSelectNotAllowed, mtInformation, [mbOK]);
    DecodeDate(Date, Year, Month, Day);
    dxSpinEditYear.Value := Year;
  end;

  ChangeDate(Sender);
  // MR:14-03-2007 Order 550427. Also look at 'cmbPlusTeamTo'.
  if (Sender = cmbPlusTeamFrom) or (Sender = cmbPlusTeamTo) then
  begin
    TeamCode := GetStrValue(TComboBoxPlus(Sender).Value);
    PopulatePlant(TeamCode);
  end;
  if Sender=CheckBoxAllTeams then
  begin
    if TCheckBox(Sender).Checked then
      TeamCode := NullStr
    else
      TeamCode := GetStrValue(cmbPlusTeamFrom.Value);
    cmbPlusTeamFrom.Enabled := not (TCheckBox(Sender).Checked);
    cmbPlusTeamTo.Enabled := not (TCheckBox(Sender).Checked);
     //Car 29-3-2004
    ComboBoxPlusPlants.Enabled := True;
    // TCheckBox(Sender).Checked;
    PopulatePlant(TeamCode);
  end;
  if Sender=CheckBoxAllPlant then
  begin
    if CheckBoxAllTeams.Checked then
      TeamCode := NullStr
    else
      TeamCode := GetStrValue(cmbPlusTeamFrom.Value);
    PopulatePlant(TeamCode);
  end;
  //RV067.4.
  CreatePopupMenu;
  SetDMFilter(False, 'N');
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
//  BitBtnRefresh.Font.Color := clred;
  //car 18-8-2003
  BitBtnFillStav.Font.Color := clred;
  SetDisableBtn(True);
  ResetAvailabilities;
  TStaffAvailabilityDM(GridFormDM).DataSourceAvailability.Enabled := False;
  ShowPeriod;
end; // ChangeFilterItem

procedure TStaffAvailabilityF.SetDisableBtn(Status: Boolean);
begin
  BitBtnEmployeeAv.Enabled := not Status;
  BitBtnStandard.Enabled := not Status;
  BitBtnFillStav.Enabled := not Status;
  BitBtnReport.Enabled := not Status;
  BitBtnWorkSchedule.Enabled := not Status;
end;

procedure TStaffAvailabilityF.BitBtnRefreshClick(Sender: TObject);
var
  Bookmark: TBookmark;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then // GLOB3-94
    Exit;

  Bookmark := nil;
  if StaffAvailabilityDM.QueryAvailability.Active then
    Bookmark := StaffAvailabilityDM.QueryAvailability.GetBookmark;
//  if (BitBtnRefresh.Font.Color = clRed) then
  begin
    StaffAvailabilityF_HND.FSort := True;
    SetDMFilter(True, 'N');
    ResetAvailabilities;
    TBitBtn(Sender).Font.Color := clBlack;
    TStaffAvailabilityDM(GridFormDM).DataSourceAvailability.Enabled := True;
    ActiveGrid :=  dxMasterGrid;
    SetDisableBtn(False);
    StaffAvailabilityF_HND.FSort := False;
// car 15-9-2003
    SetVisibleButtons;
    if not Assigned(BookMark) then
      dxGridEnter(ActiveGrid); // Activate first record in ActiveGrid
  end;
  if Assigned(Bookmark) then
  begin
    StaffAvailabilityDM.QueryAvailability.GotoBookmark(Bookmark);
    StaffAvailabilityDM.QueryAvailability.FreeBookmark(Bookmark);
  end;
end;

procedure TStaffAvailabilityF.SetDMFilter(Active: Boolean; Insert_Avb: String);
var
  AFilter: TFilterAvb;
begin
  AFilter.Active := Active;
  AFilter.Year := dxSpinEditYear.IntValue;
  AFilter.Week := dxSpinEditWeek.IntValue;
  if CheckBoxAllTeams.Checked then
    AFilter.TeamFrom := ALLTEAMS
  else
  begin
    AFilter.TeamFrom := GetStrValue(cmbPlusTeamFrom.Value);
    AFilter.TeamTo := GetStrValue(cmbPlusTeamTo.Value);
  end;
  if CheckBoxAllPlant.Checked then
    AFilter.PlantCode := ALLPLANTS
  else
    AFilter.PlantCode := GetStrValue(ComboBoxPlusPlants.Value);
  //CAR 18-8-2003
  AFilter.Insert_Avb := Insert_Avb;
  TStaffAvailabilityDM(GridFormDM).DataFilter := AFilter;
end;

procedure TStaffAvailabilityF.dxGridEnter(Sender: TObject);
var
  Emp, EmplMax, IndexColumn, i: Integer;
begin
  inherited;
  try
    if dxBarBDBNavPost.Enabled and (DisplayMessage(SPimsSaveChanges,
      mtConfirmation, [mbYes, mbNo]) = mrYes) then
      try
        dxBarBDBNavPost.Enabled := False;
        dxBarBDBNavCancel.Enabled := False;
        SystemDM.Pims.StartTransaction;
        if StaffAvailabilityDM.SaveChanges(True, Emp) < 0 then
          raise Exception.Create('');
        SystemDM.Pims.Commit;
        StaffAvailabilityDM.FEmpl := Emp;
  // get index of column with EMPLOYEE_NUMBER field
       IndexColumn := 0;
       for i := 0 to dxMasterGrid.columncount -1 do
        if dxMasterGrid.Columns[i].FieldName = 'RECNO' then
        begin
           IndexColumn := i;
           break;
        end;
  // restore position on screen and refresh grid
       EmplMax := dxMasterGrid.Items[dxMasterGrid.Count-1].Values[IndexColumn];
       StaffAvailabilityDM.RefreshAfterSave := True;
       GotoCurrentRecord(EmplMax,
         StaffAvailabilityDM.QueryAvailability.FieldByNAME('RECNO').AsFloat,
         IndexColumn);
       StaffAvailabilityDM.RefreshAfterSave := False;
       //car 550276 - initialize array for saving days changed
       StaffAvailabilityDM.InitializeDayChangedArray;
      except
      begin
        RestoreValues;
        SystemDM.Pims.RollBack;
      end;
    end
    else
      RestoreValues;
  except
    on E:Exception do
      WErrorLog(E.Message);
  end;
end;

procedure TStaffAvailabilityF.SetValueForAllEditFields(
  Value, ParentName: String; Tag: Integer);
var
  TempComponent: TComponent;
  Counter: Integer;
  key: Word;
  shift: TShiftState;
 // Day, Index, TBIndex: Integer;
//  Cmp: TComponent;
begin
  if (not FCtrlState)  or (Value = '') then
    Exit;
  if not FPopupOpen then
    Exit;
{  for Day := 1 to 7 do
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        TBIndex := ISHIFT
      else
        TBIndex := Index;
      Cmp := FindComponent(Format('Edit%d%d', [Day, TBIndex]));
      if Assigned(Cmp) then
        if (Cmp is TEdit) then
          TEdit(Cmp).Text := Value;
    end;

}
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TEdit) then
      if ((TempComponent as TEdit).Tag = Tag) then
        if (TempComponent as TEdit).Enabled then
        begin
          if (Tag = 1 ) and
            ((TempComponent as TEdit).Parent.Name = ParentName) then
            (TempComponent as TEdit).Text := Value;
          if (Tag = 5) then // Shift
            DefaultChangeShiftNo((TempComponent as TEdit), Key, Shift);
        end;
  end;
end;

procedure TStaffAvailabilityF.DefaultEditPopUp(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
  MenuLine: String;
  MenuItem: TMenuItem;
  Day: Integer;
  PlantCmp: TComponent;
  I: Integer;
begin
  if TEdit(Sender).CanFocus then
    TEdit(Sender).SetFocus;
  StaffAvailabilityDM.BuildShiftPopupMenu;
  FPopupOpen := False;
  if PlanInOtherPlants then
  begin
    Day := GetNoFromStr((Sender as TEdit).Name) div 10;
    PlantCmp := FindComponent(Format('EditP%d', [Day]));
    if Assigned(PlantCmp) then
      if (PlantCmp is TEdit) then
      begin
        for I := 0 to PopupMenuShift.Items.Count - 1 do
        begin
          MenuItem := PopupMenuShift.Items[I];
          MenuItem.Checked := False;
        end;
        MenuLine :=
          StaffAvailabilityDM.FormatPlantShift(
            TEdit(PlantCmp).Text, (Sender as TEdit).Text);
        MenuItem := PopupMenuShift.Items.Find(MenuLine);
        if Assigned(MenuItem) then
          MenuItem.Checked := True;
      end; // if
  end; // if
end; // DefaultEditPopUp

(*
function TStaffAvailabilityF.NewValuesOk: String;
var
  index: integer;
  cmp: TComponent;
begin
  Result := nullstr;
  for index := 0 to ComponentCount-1 do
  begin
    cmp := Components[index];
    if (cmp is TEdit) and TEdit(cmp).Enabled and
      (TEdit(cmp).Text = nullstr) then
    begin
      if TEdit(cmp).CanFocus then
        TEdit(cmp).SetFocus;
//      Result := Copy(TEdit(cmp).Name,5,2);
      //CAR 550276
      if TEdit(cmp).Tag = 5 then // Shift has tag=5
        Result := nullstr
      else
        Exit;
    end;
  end;
end;
*)
procedure TStaffAvailabilityF.ResetAvailabilities;
var
  Cmp: TComponent;
  Day, TBIndex, Index: Integer;
begin
  for Day := 1 to 7 do
  begin
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        TBIndex := ISHIFT
      else
        TBIndex := Index;
      Cmp := FindComponent(Format('Edit%d%d', [Day, TBIndex]));
      if Assigned(Cmp) then
        if (Cmp is TEdit) then
          TEdit(Cmp).Text := Disable;
    end;
  end;
{  for index := 0 to ComponentCount-1 do
  begin
    cmp := Components[index];
    if (cmp is TEdit) then
      TEdit(cmp).Text := Disable;
  end; }
end;

procedure TStaffAvailabilityF.dxBarButtonRecordDetailsClick(
  Sender: TObject);
begin
  inherited;
  ActiveGrid := dxMasterGrid;
end;

procedure TStaffAvailabilityF.EditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  FPopupOpen := False;
  ChangeSaveCancelState(True);
  //car 550276
  if (Sender is TEdit) then
    if (Length(TEdit(Sender).Name) >= 5) then
      StaffAvailabilityDM.FDayChanged[
        StrToInt(Copy(TEdit(Sender).Name,5,1))] := 1;
end;

procedure TStaffAvailabilityF.Edit11MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FCtrlState := (ssCtrl in Shift);
  (Sender as TEdit).SelectAll;
end;

procedure TStaffAvailabilityF.PopupMenuTBPopup(Sender: TObject);
begin
  inherited;
  FPopupOpen := True;
end;

procedure TStaffAvailabilityF.dxBarButtonExportGridClick(Sender: TObject);
begin
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;

end;

procedure TStaffAvailabilityF.SetVisibleButtons;
begin
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    GroupBoxDays.Enabled := False;
    BitBtnStandard.Enabled := False;
    BitBtnFillStav.Enabled := False;
  end;
  if not Visible_Empl_Avail then
    BitBtnEmployeeAv.Enabled := False;
  if not Visible_Rep_Avail then
    BitBtnReport.Enabled := False;
end;

procedure TStaffAvailabilityF.BitBtnFillStavClick(Sender: TObject);
begin
  inherited;
  if (BitBtnFillStav.Font.Color = clRed) then
  begin
    SetDMFilter(True, 'Y');
    ResetAvailabilities;
    TBitBtn(Sender).Font.Color := clBlack;
    ActiveGrid :=  dxMasterGrid;
    SetDisableBtn(False);
    // car 15-9-2003;
    SetVisibleButtons;
  end;
end;

// MR:05-12-2003
procedure TStaffAvailabilityF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

// MR:16-07-2004 Order 550232
// Employees who are not available (by checking Start+EndDate),
// will be shown in grid, but some or all information cannot be
// entered, depending on StartDate and Enddate.
procedure TStaffAvailabilityF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  StartDate, EndDate: TDateTime;
  Year, Week: Integer;
  procedure EditEnable(Year, Week: Integer);
  var
    Day: Integer;
    DayDate: TDateTime;
    MyEnable: Boolean;
    FinalRunExportDate: TDateTime; // 20011800
    Cmp: TComponent;
    Index, TBIndex: Integer;
    TBCount: Integer;
  begin
    for Day := 1 to 7 do
    begin
      DayDate := ListProcsF.DateFromWeek(Year, Week, Day);
      MyEnable := (DayDate >= StartDate) and
        ((DayDate <= EndDate) or (EndDate = 0));
      // 20011800 Final Run System - set 'enabled' based on last-export-date
      if MyEnable then
        if SystemDM.UseFinalRun then
        begin
          FinalRunExportDate :=
            SystemDM.FinalRunExportDateByPlant(
              dxMasterGridColumnEMPPLANT_CODE.Field.AsString);
          if FinalRunExportDate <> NullDate then
            if DayDate <= FinalRunExportDate then
              MyEnable := False;
        end;
      TBCount := 0;
      for Index := 0 to SystemDM.MaxTimeblocks do
      begin
        if Index = 0 then
          TBIndex := ISHIFT
        else
          TBIndex := Index;
        Cmp := FindComponent(Format('Edit%d%d', [Day, TBIndex]));
        if Assigned(Cmp) then
          if (Cmp is TEdit) then
          begin
            // Also set to invisible when not required
            if MyEnable then
              if TEdit(Cmp).Color <> clRequired then
                MyEnable := False;
            // MR:21-09-2004 Only set to (IN)VISIBLE.
            // Because colors are also set in another part,
            // depending on other things, we make the component also
            // invisible.
            TEdit(Cmp).Visible := MyEnable;
            if MyEnable then
              inc(TBCount);
{
            // This results in freezing of the dialog!
            // Make grid-column also (in-)visible
            if TBCount > MIN_TBS then
            begin
              Cmp := FindComponent(Format('dxMasterGridColumnC%d%d', [Day, TBIndex]));
              if Assigned(Cmp) then
                if (Cmp is TdxDBGridColumn) then
                  TdxDBGridColumn(Cmp).Visible := MyEnable;
            end;
}
          end;
      end; // for Index
      GroupBoxResize(Day, TBCount);
    end; // for Day
  end; // EditEnable
begin
  inherited;
  try
    if AFocused or ASelected then
    begin
      if AColumn = dxMasterGridColumnNo then
      begin
        StartDate := dxMasterGridColumnSTARTDATE.Field.AsDateTime;
        EndDate := dxMasterGridColumnENDDATE.Field.AsDateTime;
        Year := dxSpinEditYear.IntValue;
        Week := dxSpinEditWeek.IntValue;
        EditEnable(Year, Week);
      end;
    end;
  except
    on E:Exception do
      WErrorLog(E.Message);
  end;
end;

procedure TStaffAvailabilityF.SetPlanInOtherPlants(const Value: Boolean);
begin
  FPlanInOtherPlants := Value;
end;

procedure TStaffAvailabilityF.CheckBoxPlanInOtherPlantsClick(
  Sender: TObject);
begin
  inherited;
  PlanInOtherPlants := CheckBoxPlanInOtherPlants.Checked;
  StaffAvailabilityDM.PlanInOtherPlants := PlanInOtherPlants;
  ChangeFilterItem(Sender);
end;

procedure TStaffAvailabilityF.CheckBoxAllPlantClick(Sender: TObject);
begin
  inherited;
  ComboBoxPlusPlants.Enabled := not CheckBoxAllPlant.Checked;
  ChangeFilterItem(Sender);
end;

procedure TStaffAvailabilityF.cmbPlusTeamFromCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamTo.DisplayValue := cmbPlusTeamFrom.DisplayValue;
  ChangeFilterItem(Sender);
end;

procedure TStaffAvailabilityF.cmbPlusTeamToCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamFrom.DisplayValue := cmbPlusTeamTo.DisplayValue;
  ChangeFilterItem(Sender);
end;

procedure TStaffAvailabilityF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeek.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeek.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

// PIM-52
procedure TStaffAvailabilityF.BitBtnWorkScheduleClick(Sender: TObject);
begin
  inherited;
  try
    DialogProcessWorkScheduleF := TDialogProcessWorkScheduleF.Create(SideMenuUnitMaintenanceF);
    try
      DialogProcessWorkScheduleF.Year := dxSpinEditYear.IntValue;
      DialogProcessWorkScheduleF.FromWeek := dxSpinEditWeek.IntValue;
      DialogProcessWorkScheduleF.ToWeek := dxSpinEditWeek.IntValue;
      DialogProcessWorkScheduleF.MyEmployee := -1;
      DialogProcessWorkScheduleF.qryEmployee := StaffAvailabilityDM.QueryAvailability;
      DialogProcessWorkScheduleF.ShowModal;
    except
      on E:Exception do
        WErrorLog(E.Message);
    end;
  finally
    DialogProcessWorkScheduleF.Free;
    // Refresh grid
//    BitBtnRefresh.Font.Color := clRed;
    BitBtnRefreshClick(Sender);
  end;
end;

// GLOB3-60
procedure TStaffAvailabilityF.GroupBoxResize(ADay, ATBCount: Integer);
var
  Cmp: TComponent;
begin
  try
    if ATBCount < MIN_TBS then
      ATBCount := MIN_TBS;
    Cmp := FindComponent(Format('GroupBoxDay%d', [ADay]));
    if Assigned(Cmp) then
      if (Cmp is TGroupBox) then
        TGroupBox(Cmp).Width :=
          3 + (Edit111.Width+1) + ((Edit11.Width+1) * (ATBCount-1));
    GridColumnVisible(ADay, MaxTBCount);
  except
    on E:Exception do
      WErrorLog(E.Message);
  end;
end; // GroupBoxResize

// GLOB3-60
procedure TStaffAvailabilityF.GridColumnVisible(ADay, ATBCount: Integer);
var
  Cmp: TComponent;
  TBIndex: Integer;
begin
  try
    if ATBCount < MIN_TBS then
      ATBCount := MIN_TBS;
    for TBIndex := 1 to SystemDM.MaxTimeblocks do
    begin
      Cmp := FindComponent(Format('dxMasterGridColumnC%d%d', [ADay, TBIndex]));
      if Assigned(Cmp) then
        if (Cmp is TdxDBGridColumn) then
        begin
          if TBIndex <= ATBCount then
            TdxDBGridColumn(Cmp).Visible := True
          else
            TdxDBGridColumn(Cmp).Visible := False;
        end;
    end;
  except
    on E:Exception do
      WErrorLog(E.Message);
  end;
end; // GridColumnVisible

// GLOB3-60
procedure TStaffAvailabilityF.FormCreate(Sender: TObject);
begin
  inherited;
  MaxTBCount := SystemDM.MaxTimeblocks;
  // GLOB3-60 Leave this empty
  GroupBoxDays.Caption := '';
  // GLOB3-60 Do this once.
  GridColumnSetCaption;
  HideColumns; // GLOB3-225
end;

// GLOB3-60
// Set caption, because to prevent the translation-tool changes it.
procedure TStaffAvailabilityF.GridColumnSetCaption;
var
  Cmp: TComponent;
  Day, TBIndex: Integer;
begin
  for Day := 1 to 7 do
    for TBIndex := 1 to SystemDM.MaxTimeblocks do
    begin
      Cmp := FindComponent(Format('dxMasterGridColumnC%d%d', [Day, TBIndex]));
      if Assigned(Cmp) then
        if (Cmp is TdxDBGridColumn) then
          TdxDBGridColumn(Cmp).Caption := IntToStr(TBIndex);
    end;
end; // GridColumnSetCaption;

// GLOB3-225
procedure TStaffAvailabilityF.HideColumns;
var
  Day, I: Integer;
  C: TComponent;
begin
  for Day := 1 to 7 do
    for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
    begin
      C := FindComponent('dxMasterGridColumnC' + IntToStr(Day) +
        IntToStr(I));
      if Assigned(C) then
        if (C is TdxDBGridColumn) then
        begin
          TdxDBGridColumn(C).Visible := False;
          TdxDBGridColumn(C).DisableGrouping := True;
        end;
    end;
end; // HideColumns

end.
