inherited OvertimeDefDM: TOvertimeDefDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    AfterScroll = TableMasterAfterScroll
    OnCalcFields = TableMasterCalcFields
    TableName = 'CONTRACTGROUP'
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Size = 1
    end
    object TableMasterBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Size = 1
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableMasterWEEKS_IN_PERIOD: TIntegerField
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableMasterWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Size = 1
    end
    object TableMasterCALC_OVERTIMETYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALC_OVERTIMETYPE'
      Size = 30
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    AfterScroll = TableDetailAfterScroll
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'CONTRACTGROUP_CODE'
    MasterFields = 'CONTRACTGROUP_CODE'
    TableName = 'OVERTIMEDEFINITION'
    Left = 100
    Top = 132
    object TableDetailLINE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      DefaultExpression = '0'
      FieldName = 'LINE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailSTARTTIME: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'STARTTIME'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailENDTIME: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'ENDTIME'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailSTARTHOURMIN: TStringField
      FieldKind = fkCalculated
      FieldName = 'STARTHOURMIN'
      OnValidate = DefaultNotEmptyValidate
      Calculated = True
    end
    object TableDetailENDHOURMIN: TStringField
      FieldKind = fkCalculated
      FieldName = 'ENDHOURMIN'
      OnValidate = DefaultNotEmptyValidate
      Calculated = True
    end
    object TableDetailFROMTIME: TDateTimeField
      FieldName = 'FROMTIME'
    end
    object TableDetailTOTIME: TDateTimeField
      FieldName = 'TOTIME'
    end
    object TableDetailDAY_OF_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
    end
    object TableDetailDAYDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAYDESC'
      Size = 30
      Calculated = True
    end
    object TableDetailFROMTIMESTRING: TStringField
      FieldKind = fkCalculated
      FieldName = 'FROMTIMESTRING'
      Size = 30
      Calculated = True
    end
    object TableDetailTOTIMESTRING: TStringField
      FieldKind = fkCalculated
      FieldName = 'TOTIMESTRING'
      Size = 30
      Calculated = True
    end
  end
  object TableHourType: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = TableHourTypeFilterRecord
    TableName = 'HOURTYPE'
    Left = 100
    Top = 196
    object TableHourTypeHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableHourTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableHourTypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableHourTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableHourTypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableHourTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableHourTypeBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
    object TableHourTypeIGNORE_FOR_OVERTIME_YN: TStringField
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeMINIMUM_WAGE: TFloatField
      FieldName = 'MINIMUM_WAGE'
    end
    object TableHourTypeWAGE_BONUS_ONLY_YN: TStringField
      FieldName = 'WAGE_BONUS_ONLY_YN'
      Size = 1
    end
  end
  object DataSourceHourType: TDataSource
    DataSet = TableHourType
    Left = 204
    Top = 192
  end
  object qryOvertimeDef: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  STARTTIME, ENDTIME'
      'FROM '
      '  OVERTIMEDEFINITION'
      'WHERE'
      '  (:NEW_CONTRACTGROUP_CODE = CONTRACTGROUP_CODE)'
      '  AND (:NEW_LINE_NUMBER <> LINE_NUMBER)')
    Left = 96
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'NEW_LINE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
