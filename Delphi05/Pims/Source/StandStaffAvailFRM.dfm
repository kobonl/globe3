inherited StandStaffAvailF: TStandStaffAvailF
  Left = 420
  Top = 213
  Width = 832
  Height = 660
  Caption = 'Standard Staff Availability'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlDetail: TPanel [1]
    Top = 500
    Width = 816
    Height = 121
    TabOrder = 3
    object GroupBoxDetails: TGroupBox
      Left = 1
      Top = 1
      Width = 814
      Height = 119
      Align = alClient
      Caption = 'Standard Staff Availability'
      TabOrder = 0
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 161
        Height = 102
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label4: TLabel
          Left = 8
          Top = 5
          Width = 26
          Height = 13
          Caption = 'Team'
        end
        object Label5: TLabel
          Left = 8
          Top = 31
          Width = 24
          Height = 13
          Caption = 'Plant'
        end
        object Label6: TLabel
          Left = 8
          Top = 55
          Width = 22
          Height = 13
          Caption = 'Shift'
        end
        object Label7: TLabel
          Left = 8
          Top = 78
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object DBEdit32: TDBEdit
          Left = 64
          Top = 5
          Width = 89
          Height = 21
          DataField = 'TEAM'
          DataSource = StandStaffAvailDM.DataSourceDetail
          Enabled = False
          TabOrder = 0
        end
        object DBEdit29: TDBEdit
          Left = 64
          Top = 28
          Width = 89
          Height = 21
          DataField = 'PLANT'
          DataSource = StandStaffAvailDM.DataSourceDetail
          Enabled = False
          TabOrder = 1
        end
        object DBEdit30: TDBEdit
          Left = 64
          Top = 50
          Width = 89
          Height = 21
          DataField = 'SHIFT'
          DataSource = StandStaffAvailDM.DataSourceDetail
          Enabled = False
          TabOrder = 2
        end
        object DBEdit31: TDBEdit
          Left = 64
          Top = 75
          Width = 89
          Height = 21
          DataField = 'EMPLOYEE_NUMBER'
          DataSource = StandStaffAvailDM.DataSourceDetail
          Enabled = False
          TabOrder = 3
        end
      end
      object Panel2: TPanel
        Left = 163
        Top = 15
        Width = 649
        Height = 102
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 25
          Width = 649
          Height = 77
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 0
          object GroupBoxDay1: TGroupBox
            Left = 0
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 0
            object Panel11: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 33
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit11: TEdit
                Tag = 11
                Left = 2
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit12: TEdit
                Tag = 12
                Left = 18
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit13: TEdit
                Tag = 13
                Left = 34
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit14: TEdit
                Tag = 14
                Left = 49
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit15: TEdit
                Tag = 15
                Left = 65
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit16: TEdit
                Tag = 16
                Left = 81
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit17: TEdit
                Tag = 17
                Left = 97
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit18: TEdit
                Tag = 18
                Left = 113
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit19: TEdit
                Tag = 19
                Left = 129
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit110: TEdit
                Tag = 110
                Left = 145
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay2: TGroupBox
            Left = 166
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 1
            object Panel22: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit21: TEdit
                Tag = 21
                Left = 1
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit22: TEdit
                Tag = 22
                Left = 17
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit23: TEdit
                Tag = 23
                Left = 33
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit24: TEdit
                Tag = 24
                Left = 48
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit25: TEdit
                Tag = 25
                Left = 64
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit26: TEdit
                Tag = 26
                Left = 80
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit27: TEdit
                Tag = 27
                Left = 96
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit28: TEdit
                Tag = 28
                Left = 112
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit29: TEdit
                Tag = 29
                Left = 128
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit210: TEdit
                Tag = 210
                Left = 144
                Top = 4
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay3: TGroupBox
            Left = 332
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 2
            object Panel33: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit31: TEdit
                Tag = 31
                Left = 1
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit32: TEdit
                Tag = 32
                Left = 17
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit33: TEdit
                Tag = 33
                Left = 33
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit34: TEdit
                Tag = 34
                Left = 48
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit35: TEdit
                Tag = 35
                Left = 64
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit36: TEdit
                Tag = 36
                Left = 80
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit37: TEdit
                Tag = 37
                Left = 96
                Top = 5
                Width = 17
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit38: TEdit
                Tag = 38
                Left = 112
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit39: TEdit
                Tag = 39
                Left = 128
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit310: TEdit
                Tag = 310
                Left = 144
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay4: TGroupBox
            Left = 498
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 3
            object Panel44: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit41: TEdit
                Tag = 41
                Left = 1
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit42: TEdit
                Tag = 42
                Left = 17
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit43: TEdit
                Tag = 43
                Left = 33
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit44: TEdit
                Tag = 44
                Left = 48
                Top = 5
                Width = 17
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit45: TEdit
                Tag = 45
                Left = 64
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit46: TEdit
                Tag = 46
                Left = 80
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit47: TEdit
                Tag = 47
                Left = 96
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit48: TEdit
                Tag = 48
                Left = 112
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit49: TEdit
                Tag = 49
                Left = 128
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit410: TEdit
                Tag = 410
                Left = 144
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay5: TGroupBox
            Left = 664
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 4
            object Panel55: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit51: TEdit
                Tag = 51
                Left = 0
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit52: TEdit
                Tag = 52
                Left = 16
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit53: TEdit
                Tag = 53
                Left = 32
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit54: TEdit
                Tag = 54
                Left = 47
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit55: TEdit
                Tag = 55
                Left = 63
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit56: TEdit
                Tag = 56
                Left = 79
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit57: TEdit
                Tag = 57
                Left = 95
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit58: TEdit
                Tag = 58
                Left = 111
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit59: TEdit
                Tag = 59
                Left = 127
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit510: TEdit
                Tag = 510
                Left = 143
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay6: TGroupBox
            Left = 830
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 5
            object Panel66: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit61: TEdit
                Tag = 61
                Left = 0
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit62: TEdit
                Tag = 62
                Left = 16
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit63: TEdit
                Tag = 63
                Left = 32
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit64: TEdit
                Tag = 64
                Left = 47
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit65: TEdit
                Tag = 65
                Left = 63
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit66: TEdit
                Tag = 66
                Left = 79
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit67: TEdit
                Tag = 67
                Left = 95
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit68: TEdit
                Tag = 68
                Left = 111
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit69: TEdit
                Tag = 69
                Left = 127
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit610: TEdit
                Tag = 610
                Left = 143
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
          object GroupBoxDay7: TGroupBox
            Left = 996
            Top = 0
            Width = 166
            Height = 60
            Align = alLeft
            Caption = 'GroupBox'
            TabOrder = 6
            object Panel77: TPanel
              Left = 2
              Top = 15
              Width = 162
              Height = 34
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Edit71: TEdit
                Tag = 71
                Left = 1
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 0
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit72: TEdit
                Tag = 72
                Left = 17
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 1
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit73: TEdit
                Tag = 73
                Left = 33
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 2
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit74: TEdit
                Tag = 74
                Left = 48
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 3
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit75: TEdit
                Tag = 75
                Left = 64
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 4
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit76: TEdit
                Tag = 76
                Left = 80
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 5
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit77: TEdit
                Tag = 77
                Left = 96
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 6
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit78: TEdit
                Tag = 78
                Left = 112
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 7
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit79: TEdit
                Tag = 79
                Left = 128
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 8
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
              object Edit710: TEdit
                Tag = 710
                Left = 144
                Top = 5
                Width = 15
                Height = 21
                MaxLength = 1
                PopupMenu = PopupMenuStdAvail
                TabOrder = 9
                OnChange = Edit11Change
                OnContextPopup = Edit11ContextPopup
                OnExit = Edit11Exit
                OnKeyDown = Edit11KeyDown
                OnMouseDown = Edit11MouseDown
              end
            end
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 649
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object ButtonCopy: TButton
            Left = 2
            Top = 0
            Width = 68
            Height = 25
            Caption = 'Copy'
            TabOrder = 0
            OnClick = ButtonCopyClick
          end
        end
      end
    end
  end
  inherited pnlMasterGrid: TPanel [2]
    Top = 126
    Width = 816
    Height = 40
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 36
      Width = 814
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Width = 814
      Height = 35
      Visible = False
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 166
    Width = 816
    Height = 334
    inherited spltDetail: TSplitter
      Top = 330
      Width = 814
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 814
      Height = 329
      Bands = <
        item
          Caption = 'Employee'
          Width = 88
        end
        item
          Caption = 'Plant'
          Width = 47
        end
        item
          Caption = 'Shift'
          Width = 47
        end
        item
          Caption = 'Monday'
          Width = 152
        end
        item
          Caption = 'Tuesday'
          Width = 152
        end
        item
          Caption = 'Wednesday'
          Width = 160
        end
        item
          Caption = 'Thursday'
          Width = 57
        end
        item
          Caption = 'Friday'
          Width = 57
        end
        item
          Caption = 'Saturday'
          Width = 57
        end
        item
          Caption = 'Sunday'
          Width = 57
        end
        item
          Caption = 'Hours'
          Width = 57
        end>
      DefaultLayout = False
      KeyField = 'EMPLOYEE_NUMBER'
      PartialLoadBufferCount = 1000
      OnClick = dxDetailGridClick
      DataSource = StandStaffAvailDM.DataSourceDetail
      OptionsView = [edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      object dxDetailGridColumnRECO: TdxDBGridColumn
        DisableEditor = True
        Sizing = False
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'RECNO'
      end
      object dxDetailGridColumnEMPLOYEE_NUMBER: TdxDBGridColumn
        Caption = 'Nr'
        DisableEditor = True
        MinWidth = 0
        Width = 31
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumnDESCRIPTION: TdxDBGridColumn
        Caption = 'Name'
        DisableEditor = True
        Width = 57
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnPLANT: TdxDBGridColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 47
        BandIndex = 1
        RowIndex = 0
        FieldName = 'PLANT'
      end
      object dxDetailGridColumnSHIFT: TdxDBGridColumn
        Caption = 'Shift'
        DisableEditor = True
        Width = 47
        BandIndex = 2
        RowIndex = 0
        FieldName = 'SHIFT'
      end
      object dxDetailGridColumnD11CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D11CALC'
      end
      object dxDetailGridColumnD12CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D12CALC'
      end
      object dxDetailGridColumnD13CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D13CALC'
      end
      object dxDetailGridColumnD14CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D14CALC'
      end
      object dxDetailGridColumnD15CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D15CALC'
      end
      object dxDetailGridColumnD16CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D16CALC'
      end
      object dxDetailGridColumnD17CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D17CALC'
      end
      object dxDetailGridColumnD18CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D18CALC'
      end
      object dxDetailGridColumnD19CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D19CALC'
      end
      object dxDetailGridColumnD110CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 20
        BandIndex = 3
        RowIndex = 0
        FieldName = 'D110CALC'
      end
      object dxDetailGridColumnD21CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D21CALC'
      end
      object dxDetailGridColumnD22CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D22CALC'
      end
      object dxDetailGridColumnD23CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D23CALC'
      end
      object dxDetailGridColumnD24CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D24CALC'
      end
      object dxDetailGridColumnD25CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D25CALC'
      end
      object dxDetailGridColumnD26CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D26CALC'
      end
      object dxDetailGridColumnD27CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D27CALC'
      end
      object dxDetailGridColumnD28CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D28CALC'
      end
      object dxDetailGridColumnD29CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D29CALC'
      end
      object dxDetailGridColumnD210CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 20
        BandIndex = 4
        RowIndex = 0
        FieldName = 'D210CALC'
      end
      object dxDetailGridColumnD31CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D31CALC'
      end
      object dxDetailGridColumnD32CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D32CALC'
      end
      object dxDetailGridColumnD33CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D33CALC'
      end
      object dxDetailGridColumnD34CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D34CALC'
      end
      object dxDetailGridColumnD35CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D35CALC'
      end
      object dxDetailGridColumnD36CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D36CALC'
      end
      object dxDetailGridColumnD37CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D37CALC'
      end
      object dxDetailGridColumnD38CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D38CALC'
      end
      object dxDetailGridColumnD39CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 16
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D39CALC'
      end
      object dxDetailGridColumnD310CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 5
        RowIndex = 0
        FieldName = 'D310CALC'
      end
      object dxDetailGridColumnD41CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D41CALC'
      end
      object dxDetailGridColumnD42CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D42CALC'
      end
      object dxDetailGridColumnD43CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D43CALC'
      end
      object dxDetailGridColumnD44CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D44CALC'
      end
      object dxDetailGridColumnD45CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D45CALC'
      end
      object dxDetailGridColumnD46CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D46CALC'
      end
      object dxDetailGridColumnD47CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D47CALC'
      end
      object dxDetailGridColumnD48CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D48CALC'
      end
      object dxDetailGridColumnD49CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D49CALC'
      end
      object dxDetailGridColumnD410CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 6
        RowIndex = 0
        FieldName = 'D410CALC'
      end
      object dxDetailGridColumnD51CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D51CALC'
      end
      object dxDetailGridColumnD52CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D52CALC'
      end
      object dxDetailGridColumnD53CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D53CALC'
      end
      object dxDetailGridColumnD54CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D54CALC'
      end
      object dxDetailGridColumnD55CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D55CALC'
      end
      object dxDetailGridColumnD56CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D56CALC'
      end
      object dxDetailGridColumnD57CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D57CALC'
      end
      object dxDetailGridColumnD58CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D58CALC'
      end
      object dxDetailGridColumnD59CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D59CALC'
      end
      object dxDetailGridColumnD510CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 7
        RowIndex = 0
        FieldName = 'D510CALC'
      end
      object dxDetailGridColumnD61CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D61CALC'
      end
      object dxDetailGridColumnD62CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D62CALC'
      end
      object dxDetailGridColumnD63CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D63CALC'
      end
      object dxDetailGridColumnD64CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D64CALC'
      end
      object dxDetailGridColumnD65CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D65CALC'
      end
      object dxDetailGridColumnD66CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D66CALC'
      end
      object dxDetailGridColumnD67CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D67CALC'
      end
      object dxDetailGridColumnD68CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D68CALC'
      end
      object dxDetailGridColumnD69CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D69CALC'
      end
      object dxDetailGridColumnD610CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 8
        RowIndex = 0
        FieldName = 'D610CALC'
      end
      object dxDetailGridColumnD71CALC: TdxDBGridColumn
        Caption = '1'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D71CALC'
      end
      object dxDetailGridColumnD72CALC: TdxDBGridColumn
        Caption = '2'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D72CALC'
      end
      object dxDetailGridColumnD73CALC: TdxDBGridColumn
        Caption = '3'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D73CALC'
      end
      object dxDetailGridColumnD74CALC: TdxDBGridColumn
        Caption = '4'
        DisableEditor = True
        MinWidth = 15
        Width = 15
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D74CALC'
      end
      object dxDetailGridColumnD75CALC: TdxDBGridColumn
        Caption = '5'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D75CALC'
      end
      object dxDetailGridColumnD76CALC: TdxDBGridColumn
        Caption = '6'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D76CALC'
      end
      object dxDetailGridColumnD77CALC: TdxDBGridColumn
        Caption = '7'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D77CALC'
      end
      object dxDetailGridColumnD78CALC: TdxDBGridColumn
        Caption = '8'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D78CALC'
      end
      object dxDetailGridColumnD79CALC: TdxDBGridColumn
        Caption = '9'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D79CALC'
      end
      object dxDetailGridColumnD710CALC: TdxDBGridColumn
        Caption = '10'
        DisableEditor = True
        MinWidth = 15
        Width = 18
        BandIndex = 9
        RowIndex = 0
        FieldName = 'D710CALC'
      end
      object dxDetailGridColumnTOTALHRS: TdxDBGridColumn
        Caption = 'Total'
        DisableEditor = True
        Width = 36
        BandIndex = 10
        RowIndex = 0
        FieldName = 'TOTALHRS'
      end
      object dxDetailGridColumnCONTRACTHOURS: TdxDBGridColumn
        Caption = 'Contract'
        DisableEditor = True
        BandIndex = 10
        RowIndex = 0
        FieldName = 'CONTRACTHOURS'
      end
    end
  end
  object pnlTeamPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 816
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 816
      Height = 100
      Align = alClient
      Caption = 'Team/Plant/Shift'
      TabOrder = 0
      object Label1: TLabel
        Left = 35
        Top = 24
        Width = 26
        Height = 13
        Caption = 'Team'
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label3: TLabel
        Left = 8
        Top = 72
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object Label12: TLabel
        Left = 8
        Top = 24
        Width = 24
        Height = 13
        Caption = 'From'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 270
        Top = 21
        Width = 10
        Height = 13
        Caption = 'to'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 64
        Top = 46
        Width = 201
        Height = 19
        ColCount = 198
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        TabOrder = 3
        TitleColor = clBtnFace
        OnChange = cmbPlusPlantChange
      end
      object cmbPlusShift: TComboBoxPlus
        Left = 64
        Top = 70
        Width = 201
        Height = 19
        ColCount = 198
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        TabOrder = 5
        TitleColor = clBtnFace
        OnChange = cmbPlusShiftChange
      end
      object CheckBoxPaidYN: TCheckBox
        Left = 402
        Top = 72
        Width = 167
        Height = 17
        Caption = 'Include Paid Breaks'
        TabOrder = 8
        OnClick = CheckBoxPaidYNClick
      end
      object CheckBoxAllTeam: TCheckBox
        Left = 496
        Top = 22
        Width = 73
        Height = 17
        Caption = 'All teams'
        TabOrder = 2
        OnClick = CheckBoxAllTeamClick
      end
      object CheckBoxAllPlant: TCheckBox
        Left = 277
        Top = 48
        Width = 73
        Height = 17
        Caption = 'All plants'
        TabOrder = 4
        OnClick = CheckBoxAllPlantClick
      end
      object CheckBoxAllShift: TCheckBox
        Left = 277
        Top = 72
        Width = 73
        Height = 17
        Caption = 'All shifts'
        TabOrder = 6
        OnClick = CheckBoxAllShiftClick
      end
      object BitBtnRefresh: TBitBtn
        Left = 573
        Top = 68
        Width = 80
        Height = 25
        Caption = '&Refresh '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        OnClick = BitBtnRefreshClick
      end
      object CheckBoxOnlyActiveEmp: TCheckBox
        Left = 402
        Top = 48
        Width = 207
        Height = 17
        Caption = 'Show only active employees'
        Checked = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        State = cbChecked
        TabOrder = 7
        OnClick = CheckBoxOnlyActiveEmpClick
      end
      object cmbPlusTeamFrom: TComboBoxPlus
        Left = 64
        Top = 20
        Width = 201
        Height = 19
        ColCount = 251
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamFromCloseUp
      end
      object cmbPlusTeamTo: TComboBoxPlus
        Left = 286
        Top = 20
        Width = 201
        Height = 19
        ColCount = 251
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        DropDownWidth = 200
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = ANSI_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnCloseUp = cmbPlusTeamToCloseUp
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 696
    Top = 80
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    DataSource = nil
    Left = 736
    Top = 80
  end
  inherited StandardMenuActionList: TActionList
    Left = 696
    Top = 32
  end
  inherited dsrcActive: TDataSource
    Left = 616
    Top = 32
  end
  object PopupMenuStdAvail: TPopupMenu
    AutoHotkeys = maManual
    OnPopup = PopupMenuStdAvailPopup
    Left = 577
    Top = 35
  end
end
