inherited DialogCalculateEarnedWTRF: TDialogCalculateEarnedWTRF
  Left = 244
  Top = 197
  Caption = 'Calculate earned worktime reduction'
  ClientHeight = 264
  ClientWidth = 626
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 626
    inherited imgOrbit: TImage
      Left = 330
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 626
    Height = 143
    inherited LblFromEmployee: TLabel
      Top = 68
    end
    inherited LblEmployee: TLabel
      Top = 68
    end
    inherited LblToEmployee: TLabel
      Top = 68
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 68
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 349
      Top = 68
    end
    object Label14: TLabel [8]
      Left = 8
      Top = 42
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [9]
      Left = 40
      Top = 42
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [10]
      Left = 128
      Top = 45
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [11]
      Left = 315
      Top = 42
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label27: TLabel [12]
      Left = 349
      Top = 45
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [13]
      Left = 8
      Top = 94
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [14]
      Left = 8
      Top = 121
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 349
    end
    inherited LblStarTeamTo: TLabel
      Left = 352
      Top = 332
      Visible = False
    end
    inherited LblToTeam: TLabel
      Top = 332
      Visible = False
    end
    inherited LblStarTeamFrom: TLabel
      Left = 136
      Top = 332
      Visible = False
    end
    inherited LblTeam: TLabel
      Top = 330
      Visible = False
    end
    inherited LblFromTeam: TLabel
      Top = 330
      Visible = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 131
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 132
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 329
      ColCount = 133
      TabOrder = 14
      Visible = False
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 329
      ColCount = 134
      TabOrder = 13
      Visible = False
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 332
      TabOrder = 12
      Visible = False
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 132
      TabOrder = 3
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 133
      TabOrder = 4
    end
    inherited CheckBoxAllDepartments: TCheckBox
      TabOrder = 5
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 6
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 66
      TabOrder = 7
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 135
      TabOrder = 18
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 16
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 17
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 15
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 144
      TabOrder = 19
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 145
      TabOrder = 20
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 145
      TabOrder = 21
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 146
      TabOrder = 22
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 152
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 151
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 208
      Width = 625
      Height = 33
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      Visible = False
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 120
      Top = 91
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeek: TdxSpinEdit
      Left = 120
      Top = 120
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
  end
  inherited stbarBase: TStatusBar
    Top = 245
    Width = 626
  end
  inherited pnlBottom: TPanel
    Top = 204
    Width = 626
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 640
    Top = 160
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 648
    Top = 64
  end
  inherited StandardMenuActionList: TActionList
    Left = 640
    Top = 112
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4C040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072C4469616C6F6743616C63756C6174654561726E65645754
        52462E44617461536F75726365456D706C46726F6D104F7074696F6E73437573
        746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453
        697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D
        6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344
        420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C6574
        650D6564676F43616E496E73657274116564676F43616E4E617669676174696F
        6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C52
        65636F726473106564676F557365426F6F6B6D61726B7300000F546478444247
        726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802410942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D6505576964746802540942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E
        616D6505576964746803B4000942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060B4445534352495054494F4E00000F546478
        444247726964436F6C756D6E0D436F6C756D6E41646472657373074361707469
        6F6E06074164647265737305576964746802450942616E64496E646578020008
        526F77496E6465780200094669656C644E616D6506074144445245535300000F
        546478444247726964436F6C756D6E0E436F6C756D6E44657074436F64650743
        617074696F6E060F4465706172746D656E7420636F6465055769647468025809
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060F4445504152544D454E545F434F444500000F546478444247726964436F6C
        756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F64
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        13040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072A4469616C6F6743616C6375
        6C6174654561726E6564575452462E44617461536F75726365456D706C546F10
        4F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E67
        0E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67
        106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E67
        00094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564
        676F43616E44656C6574650D6564676F43616E496E73657274116564676F4361
        6E4E617669676174696F6E116564676F436F6E6669726D44656C657465126564
        676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B
        7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C0743
        617074696F6E06064E756D62657206536F727465640704637355700557696474
        6802310942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F454D504C4F5945455F4E554D42455200000F5464784442477269
        64436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E06
        0A53686F7274206E616D65055769647468024E0942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D45
        00000F546478444247726964436F6C756D6E11436F6C756D6E44657363726970
        74696F6E0743617074696F6E06044E616D6505576964746803CC000942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060B4445
        534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C75
        6D6E416464726573730743617074696F6E060741646472657373055769647468
        02650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506074144445245535300000F546478444247726964436F6C756D6E0E43
        6F6C756D6E44657074436F64650743617074696F6E060F4465706172746D656E
        7420636F64650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060F4445504152544D454E545F434F444500000F5464784442
        47726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E060954
        65616D20636F64650942616E64496E646578020008526F77496E646578020009
        4669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  object TableEarned: TTable
    OnNewRecord = TableEarnedNewRecord
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EARNEDWTRPERWEEK'
    Left = 344
    Top = 23
    object TableEarnedEARNED_WTR_YEAR: TIntegerField
      FieldName = 'EARNED_WTR_YEAR'
    end
    object TableEarnedEARNED_WTR_WEEK: TIntegerField
      FieldName = 'EARNED_WTR_WEEK'
    end
    object TableEarnedEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableEarnedCONTRACT_MINUTES: TIntegerField
      FieldName = 'CONTRACT_MINUTES'
    end
    object TableEarnedEARNED_WTR_MINUTES: TIntegerField
      FieldName = 'EARNED_WTR_MINUTES'
    end
    object TableEarnedCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableEarnedMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableEarnedMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object TableAbsenceTot: TTable
    OnNewRecord = TableAbsenceTotNewRecord
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'ABSENCETOTAL'
    Left = 392
    Top = 23
    object TableAbsenceTotEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableAbsenceTotABSENCE_YEAR: TIntegerField
      FieldName = 'ABSENCE_YEAR'
    end
    object TableAbsenceTotLAST_YEAR_HOLIDAY_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_HOLIDAY_MINUTE'
    end
    object TableAbsenceTotNORM_HOLIDAY_MINUTE: TFloatField
      FieldName = 'NORM_HOLIDAY_MINUTE'
    end
    object TableAbsenceTotUSED_HOLIDAY_MINUTE: TFloatField
      FieldName = 'USED_HOLIDAY_MINUTE'
    end
    object TableAbsenceTotLAST_YEAR_TFT_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_TFT_MINUTE'
    end
    object TableAbsenceTotEARNED_TFT_MINUTE: TFloatField
      FieldName = 'EARNED_TFT_MINUTE'
    end
    object TableAbsenceTotUSED_TFT_MINUTE: TFloatField
      FieldName = 'USED_TFT_MINUTE'
    end
    object TableAbsenceTotLAST_YEAR_WTR_MINUTE: TFloatField
      FieldName = 'LAST_YEAR_WTR_MINUTE'
    end
    object TableAbsenceTotEARNED_WTR_MINUTE: TFloatField
      FieldName = 'EARNED_WTR_MINUTE'
    end
    object TableAbsenceTotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableAbsenceTotUSED_WTR_MINUTE: TFloatField
      FieldName = 'USED_WTR_MINUTE'
    end
    object TableAbsenceTotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableAbsenceTotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableAbsenceTotILLNESS_MINUTE: TFloatField
      FieldName = 'ILLNESS_MINUTE'
    end
  end
  object TableEmplContract: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    MasterFields = 'EMPLOYEE_NUMBER'
    TableName = 'EMPLOYEECONTRACT'
    Left = 432
    Top = 23
  end
  object QuerySel: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.EMPLOYEE_NUMBER, SUM(S.SALARY_MINUTE) AS SUMMIN  '
      'FROM '
      '  SALARYHOURPEREMPLOYEE S '
      'WHERE '
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND '
      '  S.SALARY_DATE >= :FSTARTDATE AND '
      '  S.SALARY_DATE <= :FENDDATE AND '
      '  S.HOURTYPE_NUMBER = 1 '
      'GROUP BY '
      '  S.EMPLOYEE_NUMBER')
    Left = 472
    Top = 23
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 304
    Top = 24
  end
end
