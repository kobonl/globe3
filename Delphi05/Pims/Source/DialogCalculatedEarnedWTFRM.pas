unit DialogCalculatedEarnedWTFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables;

type
  TDialogTransferFreeTimeF = class(TDialogBaseF)
    GroupBox1: TGroupBox;
    GroupBoxTimeForTime: TGroupBox;
    Label9: TLabel;
    Label12: TLabel;
    ComboBoxPlusPlantFrom: TComboBoxPlus;
    Label13: TLabel;
    ComboBoxPlusPlantTo: TComboBoxPlus;
    Label14: TLabel;
    Label15: TLabel;
    ComboBoxPlusDeptFrom: TComboBoxPlus;
    Label16: TLabel;
    ComboBoxPlusDeptTo: TComboBoxPlus;
    ComboBoxPlusEmplTo: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusEmplFrom: TComboBoxPlus;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dxSpinEditYearFrom: TdxSpinEdit;
    Label23: TLabel;
    Label24: TLabel;
    dxSpinEditYearTo: TdxSpinEdit;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label1: TLabel;
    CheckBoxHoliday: TCheckBox;
    CheckBoxWTR: TCheckBox;
    CheckBoxTimeForTime: TCheckBox;
    tblPlant: TTable;
    tblPlantPLANT_CODE: TStringField;
    tblPlantDESCRIPTION: TStringField;
    tblPlantADDRESS: TStringField;
    tblPlantZIPCODE: TStringField;
    tblPlantCITY: TStringField;
    tblPlantSTATE: TStringField;
    tblPlantPHONE: TStringField;
    tblPlantFAX: TStringField;
    tblPlantCREATIONDATE: TDateTimeField;
    tblPlantINSCAN_MARGIN_EARLY: TIntegerField;
    tblPlantINSCAN_MARGIN_LATE: TIntegerField;
    tblPlantMUTATIONDATE: TDateTimeField;
    tblPlantMUTATOR: TStringField;
    tblPlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    tblPlantOUTSCAN_MARGIN_LATE: TIntegerField;
    qryEmployeeByPlant: TQuery;
    QueryDept: TQuery;
    DataSourcePlant: TDataSource;
    TableAbsTotTarget: TTable;
    TableAbsTotSource: TTable;
    TableEmpl: TTable;
    procedure ComboBoxPlusPlantFromChange(Sender: TObject);
    procedure ComboBoxPlusPlantToChange(Sender: TObject);
    procedure ComboBoxPlusDeptFromChange(Sender: TObject);
    procedure ComboBoxPlusDeptToChange(Sender: TObject);
    procedure ComboBoxPlusEmplFromChange(Sender: TObject);
    procedure ComboBoxPlusEmplToChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FillEmpl;
    procedure FillDept;
  end;

var
  DialogTransferFreeTimeF: TDialogTransferFreeTimeF;

implementation

uses ListProcsFRM, SystemDMT;

{$R *.DFM}

procedure TDialogTransferFreeTimeF.FillEmpl;
begin
  if (ComboBoxPlusPlantFrom.Value <> '') and
     (ComboBoxPlusPlantFrom.Value = ComboBoxPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      qryEmployeeByPlant, ComboBoxPlusEmplFrom, StrToInt(ComboBoxPlusPlantFrom.Value),
      'EMPLOYEE_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(
      qryEmployeeByPlant, ComboBoxPlusEmplTo,StrToInt(ComboBoxPlusPlantFrom.Value),
      'EMPLOYEE_NUMBER', False);
    ComboBoxPlusEmplFrom.Visible := True;
    ComboBoxPlusEmplTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusEmplFrom.Visible := False;
    ComboBoxPlusEmplTo.Visible := False;
  end;
end;

procedure TDialogTransferFreeTimeF.FillDept;
begin
  if (ComboBoxPlusPlantFrom.Value <> '') and
     (ComboBoxPlusPlantFrom.Value = ComboBoxPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptFrom, StrToInt(ComboBoxPlusPlantFrom.Value),
      'DEPARTMENT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryDept, ComboBoxPlusDeptTo, StrToInt(ComboBoxPlusPlantFrom.Value),
      'DEPARTMENT_CODE', False);
    ComboBoxPlusDeptFrom.Visible := True;
    ComboBoxPlusDeptTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusDeptFrom.Visible := False;
    ComboBoxPlusDeptTo.Visible := False;
  end;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusPlantFromChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusPlantFrom.DisplayValue <> '') and
    (ComboBoxPlusPlantTo.DisplayValue <> '') then
    if StrToInt(ComboBoxPlusPlantFrom.Value) >  StrToInt(ComboBoxPlusPlantTo.Value) then
      ComboBoxPlusPlantTo.DisplayValue := ComboBoxPlusPlantFrom.DisplayValue;
  FillEmpl;
  FillDept;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusPlantToChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusPlantFrom.DisplayValue <> '') and
    (ComboBoxPlusPlantTo.DisplayValue <> '') then
    if StrToInt(ComboBoxPlusPlantFrom.Value) >  StrToInt(ComboBoxPlusPlantTo.Value) then
    ComboBoxPlusPlantTo.DisplayValue := ComboBoxPlusPlantFrom.DisplayValue;
  FillEmpl;
  FillDept;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusDeptFromChange(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusDeptFrom.DisplayValue <> '') and
     (ComboBoxPlusDeptTo.DisplayValue <> '') then
    if ComboBoxPlusDeptFrom.Value > ComboBoxPlusDeptTo.Value then
      ComboBoxPlusDeptTo.DisplayValue := ComboBoxPlusDeptFrom.DisplayValue;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusDeptToChange(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusDeptFrom.DisplayValue <> '') and
     (ComboBoxPlusDeptTo.DisplayValue <> '') then
    if StrToInt(ComboBoxPlusDeptFrom.Value) >
       StrToInt(ComboBoxPlusDeptTo.Value) then
      ComboBoxPlusDeptFrom.DisplayValue := ComboBoxPlusDeptTo.DisplayValue;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusEmplFromChange(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusEmplFrom.DisplayValue <> '') and
     (ComboBoxPlusEmplTo.DisplayValue <> '') then
    if ComboBoxPlusEmplFrom.Value > ComboBoxPlusEmplTo.Value then
    ComboBoxPlusEmplTo.DisplayValue := ComboBoxPlusEmplFrom.DisplayValue;
end;

procedure TDialogTransferFreeTimeF.ComboBoxPlusEmplToChange(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusEmplFrom.DisplayValue <> '') and
     (ComboBoxPlusEmplTo.DisplayValue <> '') then
    if ComboBoxPlusEmplFrom.Value > ComboBoxPlusEmplTo.Value then
      ComboBoxPlusEmplFrom.DisplayValue := ComboBoxPlusEmplTo.DisplayValue;
end;

procedure TDialogTransferFreeTimeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  ComboBoxPlusPlantFrom.ClearGridData;
  ComboBoxPlusPlantTo.ClearGridData;
  ComboBoxPlusDeptFrom.ClearGridData;
  ComboBoxPlusDeptTo.ClearGridData;
  ComboBoxPlusEmplFrom.ClearGridData;
  ComboBoxPlusEmplTo.ClearGridData;
  TblPlant.Close;
  QryEmployeeByPlant.Close;
  QueryDept.Close;
  TableAbsTotSource.Close;
  TableAbsTotTarget.Close;
  TableEmpl.Close;
end;

procedure TDialogTransferFreeTimeF.FormShow(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  inherited;
  Update;
  Screen.Cursor := crHourGlass;
  ComboBoxPlusPlantFrom.ShowSpeedButton := False;
  ComboBoxPlusPlantFrom.ShowSpeedButton := True;
  ComboBoxPlusPlantTo.ShowSpeedButton := False;
  ComboBoxPlusPlantTo.ShowSpeedButton := True;
  ListProcsF.FillComboBoxPlant(tblPlant, True, ComboBoxPlusPlantFrom);
  ListProcsF.FillComboBoxPlant(tblPlant, False, ComboBoxPlusPlantTo);

  ComboBoxPlusDeptFrom.ShowSpeedButton := False;
  ComboBoxPlusDeptFrom.ShowSpeedButton := True;
  ComboBoxPlusDeptTo.ShowSpeedButton := False;
  ComboBoxPlusDeptTo.ShowSpeedButton := True;
  QueryDept.Open;
  FillDept;

  ComboBoxPlusEmplFrom.ShowSpeedButton := False;
  ComboBoxPlusEmplFrom.ShowSpeedButton := True;
  ComboBoxPlusEmplTo.ShowSpeedButton := False;
  ComboBoxPlusEmplTo.ShowSpeedButton := True;
  qryEmployeeByPlant.Open;
  FillEmpl;

  DecodeDate(Now(), Year, Month, Day);
  dxSpinEditYearFrom.Value := Year;
  dxSpinEditYearTo.Value := Year + 1;
  CheckBoxHoliday.Checked := False;
  CheckBoxWTR.Checked := False;
  CheckBoxTimeForTime.Checked := False;
  Update;
  Screen.Cursor := crDefault;
end;

procedure TDialogTransferFreeTimeF.btnOkClick(Sender: TObject);
var
  PlantFrom, PlantTo, DeptFrom, DeptTo, MessageStr: String;
  EmplFrom, EmplTo, Empl, Year: Integer;
function ActiveEmployee(Empl: Integer): Boolean;
begin
  Result := False;
  if TableEmpl.FindKey([Empl]) then
    if (PlantFrom <> PlantTo) then
      Result := (TableEmpl.FieldByName('PLANT_CODE').AsString >= PlantFrom) and
         (TableEmpl.FieldByName('PLANT_CODE').AsString <= PlantTo)
    else
      Result :=
        (TableEmpl.FieldByName('PLANT_CODE').AsString = PlantFrom) and
        (TableEmpl.FieldByName('DEPARTMENT_CODE').AsString >= DeptFrom) and
        (TableEmpl.FieldByName('DEPARTMENT_CODE').AsString <= DeptTo) and
        (TableEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger >= EmplFrom) and
        (TableEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger <= EmplTo);
end;

procedure FillValues;
begin
  TableAbsTotTarget.FieldByName('CREATIONDATE').Value := Now;
  TableAbsTotTarget.FieldByName('MUTATIONDATE').Value := Now;
  TableAbsTotTarget.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
  if CheckBoxHoliday.Checked then
    TableAbsTotTarget.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value :=
      TableAbsTotSource.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value +
      TableAbsTotSource.FieldByName('NORM_HOLIDAY_MINUTE').Value -
      TableAbsTotSource.FieldByName('USED_HOLIDAY_MINUTE').Value;
  if CheckBoxWTR.Checked then
    TableAbsTotTarget.FieldByName('LAST_YEAR_WTR_MINUTE').Value :=
      TableAbsTotSource.FieldByName('LAST_YEAR_WTR_MINUTE').Value +
      TableAbsTotSource.FieldByName('EARNED_WTR_MINUTE').Value -
      TableAbsTotSource.FieldByName('USED_WTR_MINUTE').Value;
  if CheckBoxTimeForTime.Checked then
    TableAbsTotTarget.FieldByName('LAST_YEAR_TFT_MINUTE').Value :=
      TableAbsTotSource.FieldByName('LAST_YEAR_TFT_MINUTE').Value +
      TableAbsTotSource.FieldByName('EARNED_TFT_MINUTE').Value -
      TableAbsTotSource.FieldByName('USED_TFT_MINUTE').Value;
end;
procedure InitializeValues;
begin
  TableAbsTotTarget.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('NORM_HOLIDAY_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('USED_HOLIDAY_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('LAST_YEAR_TFT_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('EARNED_TFT_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('USED_TFT_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('LAST_YEAR_WTR_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('EARNED_WTR_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('USED_WTR_MINUTE').Value := 0;
  TableAbsTotTarget.FieldByName('ILLNESS_MINUTE').Value := 0;
end;

begin
  inherited;
  if (dxSpinEditYearFrom.Value >= dxSpinEditYearTo.Value) then
  begin
    DisplayMessage('Target year should be bigger than source year!',
      mtInformation, [mbYes]);

    Exit;
  end;
  if ((not CheckBoxHoliday.Checked) and (not CheckBoxWTR.Checked) and
    (not CheckBoxTimeForTime.Checked)) then
  begin
    MessageStr := 'At least one of check boxes: holiday, work time reduction, ' +
      'time for time, should be selected !';
    DisplayMessage(MessageStr, mtInformation, [mbYes]);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  PlantFrom := ComboBoxPlusPlantFrom.Value;
  PlantTo := ComboBoxPlusPlantTo.Value;
  DeptFrom := ComboBoxPlusDeptFrom.Value;
  DeptTo := ComboBoxPlusDeptTo.Value;
  if ComboBoxPlusEmplFrom.Value <> '' then
    EmplFrom :=  StrToInt(ComboBoxPlusEmplFrom.Value);
  if ComboBoxPlusEmplTo.Value <> '' then
    EmplTo :=  StrToInt(ComboBoxPlusEmplTo.Value);

  TableAbsTotSource.Filtered := True;
  TableAbsTotSource.Filter := 'ABSENCE_YEAR = ' +
    IntToStr(Round(dxSpinEditYearFrom.Value));

  TableAbsTotSource.First;
  while not TableAbsTotSource.Eof do
  begin
    Empl := TableAbsTotSource.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if ActiveEmployee(Empl) then
    begin
      Year := Round(dxSpinEditYearTo.Value);
      if TableAbsTotTarget.FindKey([ Empl, Year]) then
      begin
// rewrite
        TableAbsTotTarget.Edit;
        FillValues;
        TableAbsTotTarget.Post;
      end
      else
      begin
// write
        TableAbsTotTarget.Insert;
        TableAbsTotTarget.FieldByName('EMPLOYEE_NUMBER').Value := Empl;
        TableAbsTotTarget.FieldByName('ABSENCE_YEAR').Value := dxSpinEditYearTo.Value;
        InitializeValues;
        FillValues;
        TableAbsTotTarget.Post;
      end;
    end;{active employee}
    TableAbsTotSource.Next;
  end;{while}

  TableAbsTotSource.Filtered := False;
  TableAbsTotSource.Refresh;
  Screen.Cursor := crDefault;
end;

procedure TDialogTransferFreeTimeF.FormCreate(Sender: TObject);
begin
  inherited;
  TableEmpl.Open;
  TableAbsTotSource.Open;
  TableAbsTotTarget.Open;
end;

end.
