// MR:14-07-2003
// Be sure you re-create the lookup-fields when converting a TTable
// to a TClientDataSet!
// Otherwise you get Access Violations in 'midas.dll'.
//
// For the Grid 3 extra tables are used for lookup-fields.
// These are needed, because the tables/queries that are used
// for DBLookupCombobox cannot be used for this, because the keys are
// different. If more than 1 key is needed to lookup a field, this fails.
(*
  Changes:
  MR:23-1-2004 If employee has 'Is_scanning_yn' = 'N' then salary
               should not be recalculated.
  MRA:27-JAN-2009 RV021.
    Team selection correction for QueryEmployee, to prevent multiple
    the same records in combobox.
  MRA:10-NOV-2009 RV040.
    - Check on AbsenceReason! It was possible to enter absence-hours
      without an absence reason! Added a check on absence reason.
  MRA:13-NOV-2009 RV041.
    - Check on AbsenceReason! Because of an error, it always did cancel
      adding the absencereason.
  MRA:2-DEC-2009 RV045.1.
    - Addition of RecalcEarnedTFTHours. Call this always when manual hours
      are changed to be sure the TFT-hours in balance are correct.
  MRA:3-MAY-2010. RV062.3.
  -  When adding manual hours like absence hours,
     it only recalculates overtime for that week! It should
     use the overtime-period!
  MRA:4-MAY-2010. RV062.4.
  - When manual salary hours are added/changed/deleted, it does not
    create/change/delete records for PRODHOURPEREMPLPERTYPE.
  SO: 07-JUN-2010 REV065.5
  - added an hour type check
  SO: 09-JUN-2010 REV065.9
  - the salary hours created from manually entered production hours are shown in
  different color and read only
  MRA:23-JUL-2010 RV067.MRA.4. 550509.
  - Do not cancel the record when during entering absence-hours the absence
    reason was not entered.
  SO:27-AUG-2010 RV067.10. 550509
    Remove the Refresh-button and retrieve the needed data
  MRA:6-SEP-2010 RV067.MRA.29 Changes for 550497
  - Getting counters (from absencetotal) was changed, but not for all
    counters, like Holiday. This has been fixed.
  MRA:27-SEP-2010 RV068.1. Bugfix.
  - When adding prod.hours and workspot is not entered,
    it gives a message about this, but it saves the record!
  MRA:4-OCT-2010 RV071.3. Changes for 550497
  - When changing the 'Norm Bank Holiday Reserve Minute'-field,
    then store this in a flag and use a different color to indicate
    it has been manually changed.
  MRA:5-OCT-2010 RV071.5. 550497
  - The ADV-fields had to be renamed, because they became too long:
    - USED_SHORTER_WWEEK_MINUTE -> USED_SHORTWWEEK_MINUTE
    - LAST_YEAR_SHORTER_WWEEK_MINUTE -> LAST_YEAR_SHORTWWEEK_MINUTE
  - Added ADV-field:
    - EARNED_SHORTWWEEK_MINUTE
    - This field can manually be changed in balance-dialog.
  MRA:6-OCT-2010 RV071.6. 550497
  - Added call to: RecalcEarnedSWWHours
  MRA:7-OCT-2010 RV071.7. 550497
  - ComputePlanned is always determined based on current date!
    This is wrong, because it must be based on the current
    selected year and week (end-of-week).
  MRA:8-OCT-2010 RV071.10. 550497 Additions for Saturday Credit.
  - Addition of Sat. Credit handling.
    - Check for manual salary 'sat. credit hrs.' added.
    - If it reaches a maximum (contr. group) then give an error-message.
    - Update Absence Total-records for 'sat. credit hrs':
      - When inserting/updating/deleting (manual) salary records.
  MRA:11-OCT-2010 RV071.13. 550497 Bugfixing.
  - Added sorting on hourtypes.
  MRA:11-OCT-2010 RV071.13. 550497 Bugfixing.
  - When previous holiday is not shown, do not calculate with it for
    ComputeHoliday.
  - This datamodule is also used by DialogExportPayrollFRM to get
    Planned Absence for the Export Counters Attentia.
  MRA:14-OCT-2010 RV071.15. Bugfixing/Changes.
  - Compute-Planned was originally based on current date,
    then it was changed to be based on 1 day after current
    selected week.
    Changed back to be based on current day (the current day of the
    computer).
  MRA:21-OCT-2010 RV073.3.
  - The dialog shows always all workspots. It does not filter on teams-per-user
    (departments linked to workspots).
  MRA:22-OCT-2010 RV073.7. Bugfix for REV065.9. 550487. (CLF)
  - Manual prod hours bug.
    When adding manual prod. hours, it creates salary hours. These are
    indicated as 'from prod hours'.
    When adding negative manual prod. hours this gives a bug when these
    are deleted: The manual salary hours are kept!
    Problems:
    - Because these are flagged as 'from prod. hours'
      they cannot removed anymore!
    - You also cannot add a manual sal. record to
      correct the hours, because this is not accepted!
  MRA:27-OCT-2010 RV075.2. Bugfix.
  - It only recalculates balances like TFT when an employee
    is of type Is-Scanning. This is wrong, it must do this for every employee,
    when a change has been made in salary-hours.
  MRA:28-OCT-2010 RV075.4. Bugfix.
  - When manual salary hours are add/modified/deleted the prodhrs-peremp-pertype
    are also changed. But this can give problems, because they are also
    based on prodhrs-peremp-records!
  - Prod-hrs-peremp-pertype were always deleted, but must only be done
    when this is set in PIMSSETTING.
  - The problem is: When a manual salary hour-record exists
    that was created during the addition of a manual
    prod-hour-record, then when this manual salary hour-record
    is modified, it creates a new prodhrs-peremp-pertype record!
    This results in too many hours in PHEPT-records.
  MRA:1-NOV-2010 RV076.1. Tailor made non-scanners. RFL.
  - Changes only for RFL:
    - Hours per Employee
      - For scanners:
        - Disable edit on salary hours
        - Only edit on production hours is allowed.
      - For non-scanners + book prod. hrs:
        - Disable edit on production hours.
        - Only edit on salary hours is allowed.
  MRA:2-NOV-2010 RV076.2. Changes for PHEPT-records.
  - When storing manual salary-records and PHEPT should be created or
    updated, then take the Plant/Workspot/Job/Shift-fields from the first
    PHE-record that could be found. If nothing was found then search
    defaults for these fields.
  MRA:11-NOV-2010 RV079.4. 550497. Addition.
  - The information that is shown for 'balances' should be changed.
    It must show the same information that is shown in
    Hours Per Employee-dialog.
  - The same routines for HoursPerEmployee must be used! For this purpose
    some routines must be rewritten.
  - A GlobalDMT-class is now made and used for this purpose.
  MRA:25-NOV-2010 RV082.1.
  - When employee is of type 'prod. hrs. based on standard planning',
    then allow changing of prod.hrs. (manual).
    But not of sal.hrs. (manual).
  MRA:25-NOV-2010 RV082.2.
  - When manual prod.hrs. are changed, then related tables 'sal.hrs'
    and 'prod.hrs.per type' must be in sync.
  MRA:30-NOV-2010 RV082.3.
  - When 3 tables must be synchronized (PHE, SHE and PHEPT),
    then have some restrictions for changing manual salary hours:
    - Do not allow adding/deleting of manual salary hours.
    - Only allow changing of the hourtype for manual salary hours.
  MRA:3-DEC-2010 RV082.10.
  - For sync of 3 tables:
    - When manual salary hours are removed,
      it did not always remove the PHEPT.
  CAR:27-DEC-2010 RV083.4. Bugfix.
  - When manually deleting TFT-hours, the balance
    is not updated.
  MRA:1-NOV-2011. RV100.1.
  - QueryEmployee: Added Contractgroup.
  MRA:3-FEB-2012. 2.0.162.215.1 Bugfix
  - When absence hours were added it recalculated overtime hours, but
    not after the absence hours were changed, it only looked if the
    absence reason was different then before. This can give wrong results
    when the hourtype was changed of the absence reason.
  MRA:16-MAY-2012 20013183. Extra counter Travel Time.
  - Hours Per Employee
    - This must show an extra counter named Travel Time in
      balance-tab.
      This should be visible when it is defined as
      COUNTER_ACTIVE_YN = 'Y' in Absence type per country-table,
      linked to the country of the plant of the current employee.
    - When manual hours are added/changed or deleted, then if
      this is linked to an hour type with TRAVELTIME_YN equals 'Y'
      then these hours must be booked in balance as 'earned travel time'.
    - When manual absence hours are added/changed or deleted,
      then when the absence reason connects to a absence type 'N',
      the balance (ABSENCETOTAL) for 'used travel time' must be
      updated.
  MRA:25-MAR-2013 20013035 Do not show inactive employees
  - Added ENDDATE to QueryEmployee
  - Added IsEmployeeActive-function that determines if an employee is
    active or not.
  MRA:10-FEB-2014 20011800
  - Final Run System
  - Keep track of (Employee-)PlantCode and (Last-)ExportDate.
  - Handling of delete: Only delete when not yet exported.
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - QueryEmployee, Filtered is set to True
  - Added cdsFilterEmployee that links to QueryEmployee
  MRA:22-DEC-2015 PIM-12 Bugfix 
  - It gave error 'No user transaction'.
  - Cause: A commit was done without checking on 'InTransaction',
    this has been added for Commit and Rollback.
  MRA:18-FEB-2019 GLOB3-253
  - Issue with Hours per employees
  - When employee contract starts or ends in selected week, then it is an
    active employee.
*)

unit HoursPerEmployeeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, HoursPerEmployeeFRM, UGlobalFunctions, Math,
  UPimsConst, UScannedIDCard, DBClient, CalculateTotalHoursDMT, GlobalDMT,
  Provider;

const
  TimeFieldMask = 'DAY';
  SignFieldMask = 'SIGNDAY';
type
  TActiveTab = (ProductionHours, SalaryHours, Balances, HolidayCounters);
  TOldProductionRec = record
    Plant, Workspot, Job: String;
    Shift: integer;
  end;
  TOldSalaryRec = record
    HourType: Integer;
  end;
  TOldAbsenceRec = record
    AbsenceReason: String;
  end;

  TFilterData = record
    Employee_Number,Year,Week: Integer;
    IsScanning: Boolean; // MR:23-1-2004
    ActiveTab: TActiveTab;
    OverTimePeriod: Integer;
    // 0 nil
    // 1 per day
    // 2 per week
    // 3 period
    Active: Boolean;
    MaxSatCreditMinute: Integer;
    BookProdHrs: Boolean; // RV076.1.
    PlantCode: String; // 20011800
    ExportDate: TDateTime; // 20011800
  end;
  THoursPerEmployeeDM = class(TGridBaseDM)
    TablePlant: TTable;
    DataSourcePlant: TDataSource;
    DataSourceShift: TDataSource;
    DataSourceWorkspotLU: TDataSource;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    QueryWork: TQuery;
    DataSourceJob: TDataSource;
    TableMasterEMPLOYEE_NUMBER: TIntegerField;
    TableMasterSHORT_NAME: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterDEPARTMENT_CODE: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterLANGUAGE_CODE: TStringField;
    TableMasterPHONE_NUMBER: TStringField;
    TableMasterEMAIL_ADDRESS: TStringField;
    TableMasterDATE_OF_BIRTH: TDateTimeField;
    TableMasterSEX: TStringField;
    TableMasterSOCIAL_SECURITY_NUMBER: TStringField;
    TableMasterSTARTDATE: TDateTimeField;
    TableMasterTEAM_CODE: TStringField;
    TableMasterENDDATE: TDateTimeField;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterIS_SCANNING_YN: TStringField;
    TableMasterCUT_OF_TIME_YN: TStringField;
    TableMasterREMARK: TStringField;
    TableMasterCALC_BONUS_YN: TStringField;
    TableMasterFIRSTAID_YN: TStringField;
    TableMasterFIRSTAID_EXP_DATE: TDateTimeField;
    TableAbsenceType: TTable;
    TableAbsenceTypeABSENCETYPE_CODE: TStringField;
    TableAbsenceTypeABSENCEREASON_CODE: TStringField;
    TableAbsenceTypeDESCRIPTION: TStringField;
    DataSourceHREPRODUCTION: TDataSource;
    DataSourceHRESALARY: TDataSource;
    DataSourceHREABSENCE: TDataSource;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailBONUS_PERCENTAGE: TFloatField;
    TableDetailOVERTIME_YN: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailCOUNT_DAY_YN: TStringField;
    TableDetailEXPORT_CODE: TStringField;
    QueryDelete: TQuery;
    TableMasterPLANTLU: TStringField;
    QueryCheckIfExist: TQuery;
    TableWK: TTable;
    StringField6: TStringField;
    TableTmpJob: TTable;
    AQuery: TQuery;
    TableTmpJobDESCRIPTION: TStringField;
    TableWKPLANT_CODE: TStringField;
    TableWKDESCRIPTION: TStringField;
    TableWKWORKSPOT_CODE: TStringField;
    TableAbsenceTypeHOURTYPE_NUMBER: TIntegerField;
    TableTmpJobPLANT_CODE: TStringField;
    TableTmpJobWORKSPOT_CODE: TStringField;
    TableTmpJobJOB_CODE: TStringField;
    cdsHRESALARY: TClientDataSet;
    cdsHREABSENCE: TClientDataSet;
    cdsHRESALARYHOURTYPE_NUMBER: TIntegerField;
    cdsHRESALARYMANUAL_YN: TStringField;
    cdsHRESALARYDAY1: TDateTimeField;
    cdsHRESALARYDAY2: TDateTimeField;
    cdsHRESALARYDAY3: TDateTimeField;
    cdsHRESALARYDAY4: TDateTimeField;
    cdsHRESALARYDAY5: TDateTimeField;
    cdsHRESALARYDAY6: TDateTimeField;
    cdsHRESALARYDAY7: TDateTimeField;
    cdsHRESALARYTOTAL: TIntegerField;
    cdsHRESALARYCREATIONDATE: TDateTimeField;
    cdsHRESALARYMUTATIONDATE: TDateTimeField;
    cdsHRESALARYMUTATOR: TStringField;
    cdsHRESALARYSIGNDAY1: TStringField;
    cdsHRESALARYSIGNDAY2: TStringField;
    cdsHRESALARYSIGNDAY3: TStringField;
    cdsHRESALARYSIGNDAY4: TStringField;
    cdsHRESALARYSIGNDAY5: TStringField;
    cdsHRESALARYSIGNDAY6: TStringField;
    cdsHRESALARYSIGNDAY7: TStringField;
    cdsHREABSENCEABSENCEREASON_CODE: TStringField;
    cdsHREABSENCEMANUAL_YN: TStringField;
    cdsHREABSENCEDAY1: TDateTimeField;
    cdsHREABSENCEDAY2: TDateTimeField;
    cdsHREABSENCEDAY3: TDateTimeField;
    cdsHREABSENCEDAY4: TDateTimeField;
    cdsHREABSENCEDAY5: TDateTimeField;
    cdsHREABSENCEDAY6: TDateTimeField;
    cdsHREABSENCEDAY7: TDateTimeField;
    cdsHREABSENCETOTAL: TIntegerField;
    cdsHREABSENCECREATIONDATE: TDateTimeField;
    cdsHREABSENCEMUTATIONDATE: TDateTimeField;
    cdsHREABSENCEMUTATOR: TStringField;
    cdsHREABSENCESIGNDAY1: TStringField;
    cdsHREABSENCESIGNDAY2: TStringField;
    cdsHREABSENCESIGNDAY3: TStringField;
    cdsHREABSENCESIGNDAY4: TStringField;
    cdsHREABSENCESIGNDAY5: TStringField;
    cdsHREABSENCESIGNDAY6: TStringField;
    cdsHREABSENCESIGNDAY7: TStringField;
    cdsHREPRODUCTION: TClientDataSet;
    cdsHREPRODUCTIONPLANT_CODE: TStringField;
    cdsHREPRODUCTIONSHIFT_NUMBER: TIntegerField;
    cdsHREPRODUCTIONWORKSPOT_CODE: TStringField;
    cdsHREPRODUCTIONJOB_CODE: TStringField;
    cdsHREPRODUCTIONDAY1: TDateTimeField;
    cdsHREPRODUCTIONDAY2: TDateTimeField;
    cdsHREPRODUCTIONDAY3: TDateTimeField;
    cdsHREPRODUCTIONDAY4: TDateTimeField;
    cdsHREPRODUCTIONDAY5: TDateTimeField;
    cdsHREPRODUCTIONDAY6: TDateTimeField;
    cdsHREPRODUCTIONDAY7: TDateTimeField;
    cdsHREPRODUCTIONTOTAL: TIntegerField;
    cdsHREPRODUCTIONMANUAL_YN: TStringField;
    cdsHREPRODUCTIONCREATIONDATE: TDateTimeField;
    cdsHREPRODUCTIONMUTATIONDATE: TDateTimeField;
    cdsHREPRODUCTIONMUTATOR: TStringField;
    cdsHREPRODUCTIONSIGNDAY1: TStringField;
    cdsHREPRODUCTIONSIGNDAY2: TStringField;
    cdsHREPRODUCTIONSIGNDAY3: TStringField;
    cdsHREPRODUCTIONSIGNDAY4: TStringField;
    cdsHREPRODUCTIONSIGNDAY5: TStringField;
    cdsHREPRODUCTIONSIGNDAY6: TStringField;
    cdsHREPRODUCTIONSIGNDAY7: TStringField;
    DataSourceAbsenceReason: TDataSource;
    DataSourceHourtype: TDataSource;
    cdsHREPRODUCTIONPLANTLU: TStringField;
    cdsHREPRODUCTIONSHIFTLU: TStringField;
    cdsHREPRODUCTIONWORKSPOTLU: TStringField;
    cdsHREPRODUCTIONJOBLU: TStringField;
    cdsHRESALARYHOURTYPELU: TStringField;
    cdsHREABSENCEABSENCEREASONLU: TStringField;
    TableShift: TTable;
    TableJob: TTable;
    QueryWorkspotLU: TQuery;
    QueryJobLU: TQuery;
    QueryShiftLU: TQuery;
    QueryEmployee: TQuery;
    TableWorkspot: TTable;
    TableWorkspotPLANT_CODE: TStringField;
    TableWorkspotDESCRIPTION: TStringField;
    TableWorkspotWORKSPOT_CODE: TStringField;
    TableWorkspotCREATIONDATE: TDateTimeField;
    TableWorkspotDEPARTMENT_CODE: TStringField;
    TableWorkspotMUTATIONDATE: TDateTimeField;
    TableWorkspotMUTATOR: TStringField;
    TableWorkspotUSE_JOBCODE_YN: TStringField;
    TableWorkspotHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotQUANT_PIECE_YN: TStringField;
    TableWorkspotAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotCOUNTER_VALUE_YN: TStringField;
    TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotDATE_INACTIVE: TDateTimeField;
    DataSourceWorkspot: TDataSource;
    TableWorkspotLU: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    DateTimeField2: TDateTimeField;
    StringField5: TStringField;
    StringField7: TStringField;
    IntegerField1: TIntegerField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    DateTimeField3: TDateTimeField;
    QueryMaxSalaryBalance: TQuery;
    QueryMaxSalaryBalanceUpdate: TQuery;
    TablePlantLU: TTable;
    TablePlantLUPLANT_CODE: TStringField;
    TablePlantLUDESCRIPTION: TStringField;
    DataSourcePlantLU: TDataSource;
    QueryMaxSalaryBalanceInsert: TQuery;
    qryWorkspotJobByEmp: TQuery;
    cdsHRESALARYFROMMANUALPROD_YN: TStringField;
    QueryHourType: TQuery;
    QueryAbsenceReason: TQuery;
    QueryHourTypeLU: TQuery;
    DataSourceHourTypeLU: TDataSource;
    qryAbsenceTypePerCountryExist: TQuery;
    qryHourtypeCheck: TQuery;
    qryAbsenceTotalSatCred: TQuery;
    qryPHE: TQuery;
    qryPHEByEmpDate: TQuery;
    qryPHEPT: TQuery;
    qryPHESHE: TQuery;
    cdsFilterEmployee: TClientDataSet;
    dspFilterEmployee: TDataSetProvider;
    procedure ComputeTotal(Year, Weeknr: Integer;
      var TotalStr: TDailyTime;
      var Total, TotalPHrs, TotalSHrs: TWeekDaysTotal);
    procedure DataModuleCreate(Sender: TObject);
    procedure DefaultAfterDelete(DataSet: TDataSet);
    procedure DefaultAfterScroll(DataSet: TDataSet);
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultBeforeDeleteRecord(dataSet: TDataSet);
    procedure DeleteProductionManualRecs(AEmployeeNumber: Integer;
      ADate: TDateTime);
    procedure DeleteProdPerTypeManualRecs(AEmployeeNumber: Integer;
      ADate: TDateTime);
    procedure DeleteProductionRec(Plant, Workspot, Job: string;
      Shift: integer; StartProdDate, EndProdDate: TDateTime);
    procedure DeleteSalaryRec(HourType: integer; StartSalDate,
      EndSalDate: TDateTime);
    procedure DeleteAbsenceRec(Absencereason: string; StartAbsDate,
      EndAbsDate: TDateTime);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsHREPRODUCTIONNewRecord(DataSet: TDataSet);
    procedure cdsHREPRODUCTIONWORKSPOT_CODEChange(Sender: TField);
    procedure cdsHREPRODUCTIONPLANT_CODEChange(Sender: TField);
    procedure cdsHRESALARYNewRecord(DataSet: TDataSet);
    procedure cdsHREABSENCENewRecord(DataSet: TDataSet);
    procedure TableWorkspotFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure cdsHREPRODUCTIONBeforeEdit(DataSet: TDataSet);
    procedure cdsFilterEmployeeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FFilterData: TFilterData;
    FSync3Tables: Boolean; // RV082.3.
    FShowOnlyActive: Boolean;
    function SetNormEarnedField(AFieldName, ANormEarned: String): Boolean;
    // RV079.4. Not needed anymore.
(*    function MyDateFrom: TDateTime; *)
    function SaturdayCreditHourtypeCheck(ADataSet: TDataSet): Boolean;
    function HourtypeCheck(AField: String; ADataSet: TDataSet): Boolean;
    function TravelTimeHourtypeCheck(ADataSet: TDataSet): Boolean;
    procedure SaturdayCreditAbsenceTotalUpdate(DataSet: TDataSet;
      DeleteAction: Boolean=False);
    procedure TravelTimeAbsenceTotalUpdate(DataSet: TDataSet;
      DeleteAction: Boolean=False);
    function GetFilterDataEmployee: Integer;
    procedure SetFilterDataEmployee(const Value: Integer);
    function GetFilterDataYear: Integer;
    procedure SetFilterDataYear(const Value: Integer);
    function GetFilterDataWeek: Integer;
    procedure SetFilterDataWeek(const Value: Integer);
  public
    { Public declarations }
    WeekDates: TWeekDates;
    Total: TWeekDaysTotal;
    OldProductionRec: TOldProductionRec;
    CurrentRecordProd: TOldProductionRec;
    OldSalaryRec: TOldSalaryRec;
    CurrentRecordSal: TOldSalaryRec;
    OldAbsenceRec: TOldAbsenceRec;
    CurrentRecordAbs: TOldAbsenceRec;
    OldMinutes: array[1..7] of integer;
    // depends of the ActiveTab contain the total
    // Production hours + Absence Hours
    procedure SetFilterData(Value: TFilterData);
    property FilterData: TFilterData read FFilterData write SetFilterData;
    // RV079.4. Not needed anymore.
(*
    function DetermineCounterValue(const Fieldname: String): String;
    function DetermineNormBankHldResMinManYN: String;
*)
    // RV079.4. Not needed anymore.
(*
    procedure GetCounters;
    procedure ComputeHoliday(var Remaining, Normal, Used, Planned,
      Available: string; RemainingVisible: Boolean=True);
    procedure ComputeWorkTime(var Remaining, Earned, Used, Planned,
      Available: string);
    procedure ComputeTimeForTime(var Remaining, Earned, Used, Planned,
      Available: string);
    procedure ComputeIllness(var CurrentYear, PreviousYears: string);
    procedure ComputeAddBnkHoliday(var Remaining, Used, Planned,
    Available: String);
    procedure ComputeHldPrevYear(var Remaining, Used, Planned,
    Available: String);
    procedure ComputeRsvBnkHoliday(var Normal, Used, Planned,
    Available: String);
    procedure ComputeSatCredit(var Remaining, Earned, Used, Planned,
    Available: String);
    procedure ComputeSeniorityHld(var Normal, Used, Planned,
    Available: String);
    procedure ComputeShorterWeek(var Remaining, Earned, Used, Planned,
    Available: String);
*)
    function SetNormHoliday(NormHoliday: String): Boolean;
    function SetNormRsvBnkHoliday(NormHoliday: String): Boolean;
    function SetNormSeniority(NormHoliday: String): Boolean;
    function SetEarnedShorterWorkerWeek(Earned: String): Boolean;
    // Return False if NormHoliday does not represent a valid
    // hour : minuites value.
    procedure PopulateAbsenceIDCard(AShiftNumber: Integer; var EmployeeIDCard:
      TScannedIDCard; ShiftNumber: integer; WeekDay: TDateTime);
    procedure SetWeekDates(Year,WeekNo: integer);
    // if WeekNo = 0 then will construct weekdates from curentdate
    // else use the WeekNo
    //REV065.9 Added FromManualProd parameter 'Y' for the salary records created from
    //manually entered production records
    procedure CorrectSalaryHour(SalDate: TDateTime; EmployeeNumber,
      Minutes: Integer; PlantCode, WorkspotCode, JobCode, Manual, FromManualProd: string;
      Shift: Integer);
    // RV082.3. Disabled.
(*    function UpdateSalaryHour2(ASalaryDate: TDateTime;
      AEmployeeNumber: Integer; AHourTypeNumber: Integer;
      AOldMinutes, AMinutes: Integer; AManual: String): Boolean; // RV062.4. *)
    function GetDailyMinutes(CurrentDataSet: TDataSet;
    	index: integer): integer;
    // this function compute the amount of minutes for index week day
    // including the sign of minutes amount
    // dataset TableHREPRODUCTION or TableHRESALARY or TableHREABSENCE
    // index values [1..7] - day index
    // if any error return 0 !!
    procedure RecalculateOvertimePeriod(AShiftNumber: Integer;
      ADate: TDateTime);
    procedure InsertProductionMinutes(ProdDate:TDateTime;Plant,WorkSpot,
      Job: String; Shift, Minutes: integer);
    procedure InsertSalaryMinutes(SalDate: TDateTime; AOldMinutes, Minutes,
      HourType: integer);
    procedure InsertAbsenceMinutes(Minutes: integer; AbsDate: TDateTime;
      AbsenceReason: string);
    procedure DeactivateTablesEvents(Enable: boolean);
    procedure SaveChanges;
    function FindCurrentRecord(DataSet: TDataSet): Boolean;
    function CheckIfExistRecord(DataSet: TDataSet): Boolean;
    //car 25.02.2003 - improve appearance
    procedure CalculateNormalHoursPerEmpl(Year, Weeknr: Integer;
      Empl: Integer;
      Dept: String; var TotalPHrs, TotalSHrs: TWeekDaysTotal);
    // RV079.4. Not needed anymore.
(*    function ComputeMaxSalaryBalanceMinutes: String; *)
    procedure UpdateMaxSalaryBalanceMinutes(MinutesText: String);
    // RV079.4. Not needed anymore.
(*    function GetCounterActivated(AAbsenceType: Char; var ACounterDescription: String): Boolean; *)
    procedure UpdateHourTypePerCountryLU(CountryId: Integer);
    // RV079.4. Not needed anymore.
(*    function AbsenceTypePerCountryExist: Boolean; *)
    procedure RecalcPHEPT(AProdDate: TDateTime); // RV082.2.
    function IsEmployeeActive(ADataSet: TDataSet; ADate: TDateTime): Boolean;
    procedure ShowOnlyActiveSwitch; // 20013035
    property FilterDataEmployee: Integer read GetFilterDataEmployee write SetFilterDataEmployee;
    property FilterDataYear: Integer read GetFilterDataYear write SetFilterDataYear;
    property FilterDataWeek: Integer read GetFilterDataWeek write SetFilterDataWeek;
    property Sync3Tables: Boolean read FSync3Tables write FSync3Tables;
    property ShowOnlyActive: Boolean read FShowOnlyActive write FShowOnlyActive; // 20013035
  end;

var
  HoursPerEmployeeDM: THoursPerEmployeeDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, SystemDMT, UPimsMessageRes;

function THoursPerEmployeeDM.CheckIfExistRecord(DataSet: TDataSet): Boolean;
var
  SelectStr: String;
begin
  Result := False;
  //RV067.4.
  if DataSet = TableMaster then
    Exit;
  if DataSet = cdsHREPRODUCTION then
  begin
    if (DataSet.State <> dsInsert) and
     (OldProductionRec.Plant = cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString) and
     (OldProductionRec.Workspot = DataSet.FieldByName('WORKSPOT_CODE').asString) and
     (OldProductionRec.Job = DataSet.FieldByName('JOB_CODE').asString) and
     (OldProductionRec.Shift = DataSet.FieldByName('SHIFT_NUMBER').asInteger) then
    begin
      Result := False;
      Exit;
    end;
    // Warning: If you switch to 'Locate' the current record will be posted!
(*
    Result := cdsHREPRODUCTION.Locate(
      'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;SHIFT_NUMBER;MANUAL_YN',
      VarArrayOf([
        cdsHREPRODUCTION.FieldByName('PLANT_CODE').AsString,
        cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').AsString,
        cdsHREPRODUCTION.FieldByName('JOB_CODE').AsString,
        cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').AsInteger,
        CHECKEDVALUE]), []);
*)
    SelectStr :=
      'SELECT ' + NL +
      '  EMPLOYEE_NUMBER ' + NL +
      'FROM ' + NL +
      '  PRODHOURPEREMPLOYEE ' + NL +
      'WHERE ' + NL +
      '  PRODHOUREMPLOYEE_DATE >= :STARTDATE ' + NL +
      '  AND PRODHOUREMPLOYEE_DATE <= :ENDDATE ' + NL +
      '  AND EMPLOYEE_NUMBER = :EMPNO ' + NL +
      '  AND PLANT_CODE = ''' +
      cdsHREPRODUCTION.FieldByName('PLANT_CODE').AsString + '''' + NL +
      '  AND WORKSPOT_CODE = ''' +
      cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').AsString +  '''' + NL +
      '  AND JOB_CODE = ''' +
      cdsHREPRODUCTION.FieldByName('JOB_CODE').AsString +  '''' + NL +
      '  AND SHIFT_NUMBER = ' +
      cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').AsString + NL +
      '  AND MANUAL_YN = ''' + CHECKEDVALUE + '''';
  end
  else
  begin
    if DataSet = cdsHRESALARY then
    begin
      if (DataSet.State <> dsInsert) and
        (OldSalaryRec.HourType =
          cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').AsInteger) then
      begin
        Result := False;
        Exit;
      end;
      SelectStr := 
        'SELECT ' + NL +
        '  EMPLOYEE_NUMBER ' + NL +
        'FROM ' + NL +
        '  SALARYHOURPEREMPLOYEE ' + NL +
        'WHERE ' + NL +
        '  SALARY_DATE >= :STARTDATE ' + NL +
        '  AND SALARY_DATE <= :ENDDATE ' + NL +
        '  AND EMPLOYEE_NUMBER = :EMPNO ' + NL +
        '  AND HOURTYPE_NUMBER = ' +
        cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').AsString +
        '  AND MANUAL_YN = ''' + CHECKEDVALUE + '''';
    end
    else
    begin
      if DataSet = cdsHREABSENCE then
      begin
        if (DataSet.State <> dsInsert) and
          (OldAbsenceRec.AbsenceReason =
            DataSet.FieldByName('ABSENCEREASON_CODE').asString) then
        begin
          Result := False;
          Exit;
        end;
        SelectStr :=
          'SELECT ' + NL +
          '  EMPLOYEE_NUMBER ' + NL +
          'FROM ' + NL +
          '  ABSENCEHOURPEREMPLOYEE, ABSENCEREASON ' + NL +
          'WHERE ' + NL +
          '  ABSENCEHOURPEREMPLOYEE.ABSENCEREASON_ID = ' +
          '  ABSENCEREASON.ABSENCEREASON_ID ' + NL +
          '  AND ABSENCEHOUR_DATE >= :STARTDATE ' +
          '  AND ABSENCEHOUR_DATE <= :ENDDATE ' + NL +
          '  AND EMPLOYEE_NUMBER = :EMPNO ' + NL +
          '  AND ABSENCEREASON.ABSENCEREASON_CODE = ''' +
          cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').AsString +  '''' +
          '  AND MANUAL_YN = ''' + CHECKEDVALUE + '''';
      end;
    end;
  end;
  QueryCheckIfExist.Close;
  QueryCheckIfExist.SQL.Clear;
// MR:20-01-2003 Don't use 'uppercase' because 'absencereason_code' can
// also be lowercase!
//  QueryCheckIfExist.SQL.Add(UpperCase(SelectStr));
  QueryCheckIfExist.SQL.Add(SelectStr);
  QueryCheckIfExist.ParamByName('EMPNO').AsInteger :=
     FilterData.Employee_Number;
  QueryCheckIfExist.ParamByName('STARTDATE').AsDateTime :=
    WeekDates[1];
  QueryCheckIfExist.ParamByName('ENDDATE').AsDateTime :=
    WeekDates[7];
  QueryCheckIfExist.Open;
  Result := (QueryCheckIfExist.RecordCount > 0);
  QueryCheckIfExist.Close;
end;

procedure THoursPerEmployeeDM.SetFilterData(Value: TFilterData);
var
  Index, Shift, HourType, Minutes: integer;
  Plant, Workspot, Job, AbsenceRreasonCode, Manual: string;
  // RV076.1.
  procedure ProductionReadOnlySwitch(AMakeReadOnly: Boolean);
  begin
    Exit; // RV082.1. Disable this functionality.
    if SystemDM.IsRFL then
    begin
      if AMakeReadOnly then
      begin
        // When employee is of type NonScanner and BookProdHrs, then
        // edit of production hrs is not allowed.
        if (not FFilterData.IsScanning) and (FFilterData.BookProdHrs) then
          cdsHREPRODUCTION.ReadOnly := True
        else
          cdsHREPRODUCTION.ReadOnly := False;
      end
      else
        cdsHREPRODUCTION.ReadOnly := False;
    end;
  end;
  // RV076.1.
  procedure SalaryReadOnlySwitch(AMakeReadOnly: Boolean);
  begin
    Exit; // RV082.12. Disable this.
    if SystemDM.IsRFL then
    begin
      if AMakeReadOnly then
      begin
        // When employee is of type Scanner and Not-BookProdHrs, then
        // edit of production hrs is not allowed.
//        if (FFilterData.IsScanning) and (not FFilterData.BookProdHrs) then
        // RV082.1.
        // Scanners OR BookProdHrs-employee cannot edit sal.hrs.
        if (FFilterData.IsScanning) and (not FFilterData.BookProdHrs) then
//        if (FFilterData.IsScanning) or (FFilterData.BookProdHrs) then
          cdsHRESALARY.ReadOnly := True
        else
          cdsHRESALARY.ReadOnly := False;
      end
      else
        cdsHRESALARY.ReadOnly := False;
    end;
  end;
begin
//car 550274
  if cdsFilterEmployee.Active and (not cdsFilterEmployee.IsEmpty) then
  begin
    Value.Employee_Number :=
      cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').asInteger;
    // MR:23-1-2004
    Value.IsScanning :=
      (cdsFilterEmployee.FieldByName('IS_SCANNING_YN').AsString = 'Y');
    // RV076.1.
    Value.BookProdHrs :=
      (cdsFilterEmployee.FieldByName('BOOK_PROD_HRS_YN').AsString = 'Y');
    // 20011800
    Value.PlantCode :=
      cdsFilterEmployee.FieldByName('PLANT_CODE').AsString;
    Value.ExportDate :=
      SystemDM.FinalRunExportDateByPlant(Value.PlantCode);
  end;
  if (Value.Year > 0) or (Value.Week > 0) then
    SetWeekDates(Value.Year, Value.Week)
  else
    SetWeekDates(0,0);
  FFilterData := Value;
  // Overtimeperiod
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  // RV071.10 Get 'max_sat_credit_minute'.
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    '  A.OVERTIME_PER_DAY_WEEK_PERIOD, A.MAX_SAT_CREDIT_MINUTE ' + NL +
    'FROM ' + NL +
    '  CONTRACTGROUP A, EMPLOYEE B ' + NL +
    'WHERE ' + NL +
    '  (A.CONTRACTGROUP_CODE=B.CONTRACTGROUP_CODE) AND ' + NL +
    '  (B.EMPLOYEE_NUMBER=:EMPNO)'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger := Value.Employee_Number;
  QueryWork.Active := True;
  FFilterData.OverTimePeriod := 0;
  FFilterData.MaxSatCreditMinute := 0;
  if QueryWork.Active and (not QueryWork.IsEmpty) then
  begin
    FFilterData.OverTimePeriod :=
      QueryWork.FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger;
    // RV071.10 Get 'max_sat_credit_minute'
    FFilterData.MaxSatCreditMinute :=
      QueryWork.FieldByName('MAX_SAT_CREDIT_MINUTE').AsInteger;
  end;
  if (not Value.Active) then
    Exit;
  // Deactivate the BeforePost events
  DeactivateTablesEvents(True);
  // ProductionHours:
  if FFilterData.ActiveTab = ProductionHours then
  begin
    ProductionReadOnlySwitch(False); // RV076.1.
    DataSourceHREPRODUCTION.Enabled := False;
    cdsHREPRODUCTION.EmptyDataSet;
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' + NL +
      '  PLANT_CODE, SHIFT_NUMBER, WORKSPOT_CODE, ' + NL +
      '  JOB_CODE, MANUAL_YN, PRODHOUREMPLOYEE_DATE, ' + NL +
      '  PRODUCTION_MINUTE ' + NL +
      'FROM ' + NL +
      '  PRODHOURPEREMPLOYEE ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER=:EMPNO AND ' + NL +
      '  PRODHOUREMPLOYEE_DATE >= :STARTDATE AND ' + NL +
      '  PRODHOUREMPLOYEE_DATE <= :ENDDATE ' + NL +
      'ORDER BY ' + NL +
      '  PLANT_CODE, SHIFT_NUMBER,WORKSPOT_CODE, ' + NL +
      '  JOB_CODE, MANUAL_YN, PRODHOUREMPLOYEE_DATE'
      );
    QueryWork.ParamByName('EMPNO').asInteger :=
      FilterData.Employee_Number;
    QueryWork.ParamByName('STARTDATE').asDateTime :=
      WeekDates[1];
    QueryWork.ParamByName('ENDDATE').asDateTime :=
      WeekDates[7];
    QueryWork.Prepare;
    QueryWork.Active := True;
    Plant:=NullStr;Shift:=0;Workspot:=Nullstr;Job:=Nullstr;Manual:=NullStr;
    QueryWork.First;
    while not QueryWork.Eof do
    begin
      if (QueryWork.FieldByName('PLANT_CODE').asString <> Plant) or
        (QueryWork.FieldByName('SHIFT_NUMBER').asInteger <> Shift) or
        (QueryWork.FieldByName('WORKSPOT_CODE').asString <> Workspot) or
        (QueryWork.FieldByName('JOB_CODE').asString <> Job) or
        (QueryWork.FieldByName('MANUAL_YN').asString <> Manual) then
      begin
        if cdsHREPRODUCTION.State in [dsEdit, dsInsert] then
        begin
          Minutes := 0;
          for index:=1 to 7 do
            Minutes := Minutes + GetDailyMinutes(cdsHREPRODUCTION,index);
          cdsHREPRODUCTION.FieldByName('TOTAL').asInteger := Minutes;
          // MR:11-07-2003 Post was missing!
          cdsHREPRODUCTION.Post;
        end;
        cdsHREPRODUCTION.Insert;
        cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString :=
        QueryWork.FieldByName('PLANT_CODE').asString;
        cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger :=
          QueryWork.FieldByName('SHIFT_NUMBER').asInteger;
        cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString :=
          QueryWork.FieldByName('WORKSPOT_CODE').asString;
        cdsHREPRODUCTION.FieldByName('JOB_CODE').asString :=
          QueryWork.FieldByName('JOB_CODE').asString;
        cdsHREPRODUCTION.FieldByName('MANUAL_YN').asString :=
          QueryWork.FieldByName('MANUAL_YN').asString;
        Plant := QueryWork.FieldByName('PLANT_CODE').asString;
        Shift := QueryWork.FieldByName('SHIFT_NUMBER').asInteger;
        Workspot := QueryWork.FieldByName('WORKSPOT_CODE').asString;
        Job := QueryWork.FieldByName('JOB_CODE').asString;
        Manual := QueryWork.FieldByName('MANUAL_YN').asString;
      end;
      for index := 1 to 7 do
      begin
        if QueryWork.FieldByName('PRODHOUREMPLOYEE_DATE').asDateTime =
          WeekDates[index] then
        begin
          Minutes := QueryWork.FieldByName('PRODUCTION_MINUTE').asInteger;
          if Minutes < 0 then
          begin
            cdsHREPRODUCTION.FieldByName(Format(SignFieldMask+'%d',[index])).
              AsString := UNCHECKEDVALUE;
            Minutes := (-1)*Minutes;
          end;
          cdsHREPRODUCTION.FieldByName(Format(TimeFieldMask+'%d',[index])).
            AsDateTime := IntMin2TimeMin(Minutes);
          Break;
        end;
      end;
      QueryWork.Next;
    end;
    if cdsHREPRODUCTION.State in [dsInsert,dsEdit] then
    begin
      Minutes := 0;
      for index := 1 to 7 do
        Minutes := Minutes + GetDailyMinutes(cdsHREPRODUCTION,index);
      cdsHREPRODUCTION.FieldByName('TOTAL').asInteger := Minutes;
      cdsHREPRODUCTION.Post;
    end;
    DataSourceHREPRODUCTION.Enabled := True;
    ProductionReadOnlySwitch(True); // RV076.1.
  end; // ProductionHours
  // Salary Hours:
  if FFilterData.ActiveTab = SalaryHours then
  begin
    SalaryReadOnlySwitch(False); // RV076.1.
    DataSourceHRESALARY.Enabled := False;
    cdsHRESALARY.EmptyDataSet;
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' + NL +
      '  HOURTYPE_NUMBER, MANUAL_YN, FROMMANUALPROD_YN, SALARY_DATE, ' + NL +
      '  SALARY_MINUTE ' + NL +
      'FROM ' + NL +
      '  SALARYHOURPEREMPLOYEE ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER=:EMPNO AND ' + NL +
      '  SALARY_DATE >= :STARTDATE AND ' + NL +
      '  SALARY_DATE <= :ENDDATE ' + NL +
      'ORDER BY ' + NL +
      '  HOURTYPE_NUMBER, MANUAL_YN, SALARY_DATE'
      );
    QueryWork.Prepare;
    QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
    QueryWork.ParamByName('STARTDATE').asDateTime := WeekDates[1];
    QueryWork.ParamByName('ENDDATE').asDateTime := WeekDates[7];
    QueryWork.Active := True;
    HourType:=0;Manual:=NullStr;
    while not QueryWork.Eof do
    begin
      if (QueryWork.FieldByName('HOURTYPE_NUMBER').asInteger <> HourType) or
        (QueryWork.FieldByName('MANUAL_YN').asString <> Manual) then
      begin
        if cdsHRESALARY.State in [dsEdit, dsInsert] then
        begin
          Minutes := 0;
          for index := 1 to 7 do
            Minutes := Minutes + GetDailyMinutes(cdsHRESALARY,index);
          cdsHRESALARY.FieldByName('TOTAL').asinteger := Minutes;
          // MR:11-07-2003 Post was missing!
          cdsHRESALARY.Post;
        end;
        cdsHRESALARY.Insert;
        cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').asInteger :=
          QueryWork.FieldByName('HOURTYPE_NUMBER').asInteger;
        cdsHRESALARY.FieldByName('MANUAL_YN').asString :=
          QueryWork.FieldByName('MANUAL_YN').asString;
        cdsHRESALARY.FieldByName('FROMMANUALPROD_YN').asString :=
          QueryWork.FieldByName('FROMMANUALPROD_YN').asString;
        HourType := QueryWork.FieldByName('HOURTYPE_NUMBER').asInteger;
        Manual := QueryWork.FieldByName('MANUAL_YN').asString;
      end;
      for index:=1 to 7 do
      begin
        if QueryWork.FieldByName('SALARY_DATE').asDateTime =
          WeekDates[index] then
        begin
          Minutes := QueryWork.FieldByName('SALARY_MINUTE').asInteger;
          if Minutes < 0 then
          begin
            cdsHRESALARY.FieldByName(Format(SignFieldMask+'%d',[index])).
              AsString := UNCHECKEDVALUE;
            Minutes := (-1)*Minutes;
          end;
          cdsHRESALARY.FieldByName(Format(TimeFieldMask+'%d',[index])).
            AsDateTime := IntMin2TimeMin(Minutes);
          break;
        end;
      end;
      QueryWork.Next;
    end;
    DataSourceHRESALARY.Enabled := True;
    if cdsHRESALARY.State in [dsInsert,dsEdit] then
    begin
      Minutes := 0;
      for index := 1 to 7 do
        Minutes := Minutes + GetDailyMinutes(cdsHRESALARY,index);
      cdsHRESALARY.FieldByName('TOTAL').asInteger := Minutes;
      cdsHRESALARY.Post;
    end;
    SalaryReadOnlySwitch(True); // RV076.1.
  end; // SalaryHours
  // Absence Hours:
  if (FFilterData.ActiveTab <> Balances) and (FFilterData.ActiveTab <> HolidayCounters) then
  begin
    DataSourceHREABSENCE.Enabled := False;
    cdsHREABSENCE.EmptyDataSet;
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' + NL +
      '  AHE.ABSENCEHOUR_DATE, AR.ABSENCEREASON_CODE, ' + NL +
      '  AHE.MANUAL_YN, AHE.ABSENCE_MINUTE ' + NL +
      'FROM ' + NL +
      '  ABSENCEHOURPEREMPLOYEE AHE, ABSENCEREASON AR ' + NL +
      'WHERE ' + NL +
      '  AHE.EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID AND ' + NL +
      '  AHE.ABSENCEHOUR_DATE >= :STARTDATE AND ' + NL +
      '  AHE.ABSENCEHOUR_DATE <= :ENDDATE ' + NL
    );
    QueryWork.SQL.Add(
      'ORDER BY ' + NL +
      '  AR.ABSENCEREASON_CODE, AHE.MANUAL_YN, ' + NL +
      '  AHE.ABSENCEHOUR_DATE'
      );

    QueryWork.Prepare;
    //RV067.4.
    QueryWork.ParamByName('EMPNO').asInteger :=
      FilterData.Employee_Number;
    QueryWork.ParamByName('STARTDATE').asDateTime :=
      WeekDates[1];
    QueryWork.ParamByName('ENDDATE').asDateTime :=
      WeekDates[7];
    QueryWork.Active := True;
    AbsenceRreasonCode:=NullStr;Manual:=NullStr;
    while not QueryWork.Eof do
    begin
      if (QueryWork.FieldByName('ABSENCEREASON_CODE').asString <>
        AbsenceRreasonCode) or (QueryWork.FieldByName('MANUAL_YN').asString <>
          Manual) then
      begin
        if cdsHREABSENCE.State in [dsEdit, dsInsert] then
        begin
          Minutes := 0;
          for index := 1 to 7 do
            Minutes := Minutes + GetDailyMinutes(cdsHREABSENCE,index);
          cdsHREABSENCE.FieldByName('TOTAL').asinteger := Minutes;
          // MR:11-07-2003 Post was missing!
          cdsHREABSENCE.Post;
        end;
        cdsHREABSENCE.Insert;
        cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').asString :=
          QueryWork.FieldByName('ABSENCEREASON_CODE').asString;
        cdsHREABSENCE.FieldByName('MANUAL_YN').asString :=
          QueryWork.FieldByName('MANUAL_YN').asString;
        AbsenceRreasonCode := QueryWork.FieldByName('ABSENCEREASON_CODE').asString;
        Manual := QueryWork.FieldByName('MANUAL_YN').asString;
      end;
      for index := 1 to 7 do
      begin
        if QueryWork.FieldByName('ABSENCEHOUR_DATE').asDateTime =
          WeekDates[index] then
        begin
          Minutes := QueryWork.FieldByName('ABSENCE_MINUTE').asInteger;
          if Minutes < 0 then
          begin
            Minutes := Minutes * (-1);
            cdsHREABSENCE.FieldByName(Format(SignFieldMask+'%d',[index])).
              AsString := UNCHECKEDVALUE;
          end;
          cdsHREABSENCE.FieldByName(Format(TimeFieldMask+'%d',[index])).
            AsDateTime := IntMin2TimeMin(Minutes);
          break;
        end;
      end;
      QueryWork.Next;
    end;
    DataSourceHREABSENCE.Enabled := True;
    if cdsHREABSENCE.State in [dsInsert,dsEdit] then
    begin
      Minutes := 0;
      for index := 1 to 7 do
        Minutes := Minutes + GetDailyMinutes(cdsHREABSENCE,index);
      cdsHREABSENCE.FieldByName('TOTAL').asInteger := Minutes;
      cdsHREABSENCE.Post;
    end;
  end;
  // Activate BeforePost event
  DeactivateTablesEvents(False);
end;

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeHoliday(var Remaining, Normal, Used,
  Planned, Available: string; RemainingVisible: Boolean=True);
begin
  Remaining := NullTime; Normal := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  // Retreive all data from ABSENCETOTAL
  // RV067.MRA.29
{
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    '  NORM_HOLIDAY_MINUTE, USED_HOLIDAY_MINUTE, ' + NL +
    '  LAST_YEAR_HOLIDAY_MINUTE ' + NL +
    'FROM ' + NL +
    '  ABSENCETOTAL ' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER = :EMPNO AND ABSENCE_YEAR = :AYEAR'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
  QueryWork.Active := True;
}
  // RV071.13. 550497 Bugfixing.
  if not QueryWork.IsEmpty then
  begin
    if RemainingVisible then
      Remaining := IntMin2StringTime(
        Round(QueryWork.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').asFloat),True)
    else
      Remaining := IntMin2StringTime(0,True);
    Normal := IntMin2StrDigTime(
      Round(QueryWork.FieldByName('NORM_HOLIDAY_MINUTE').asFloat),3,True);
    Used := IntMin2StringTime(
      Round(QueryWork.FieldByName('USED_HOLIDAY_MINUTE').asFloat),True);
  end;
  // Compute Planned
  Planned := IntMin2StringTime(ComputePlanned(QueryWork,
    FilterData.Employee_Number, HOLIDAYAVAILABLETB, MyDateFrom), True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining) + StrTime2IntMin(Normal) -
    StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeIllness(var CurrentYear,
	PreviousYears: string);
begin
  // Previous years
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    '  SUM(ILLNESS_MINUTE) TOT ' + NL +
    'FROM ' + NL +
    '  ABSENCETOTAL' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR<=:AYEAR'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').AsInteger := FilterData.Employee_Number;
  QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
  QueryWork.Active := True;
  if not QueryWork.IsEmpty then
    PreviousYears :=
      IntMin2StringTime(QueryWork.FieldByName('TOT').asInteger,True);
  // Current year
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    '  ILLNESS_MINUTE ' + NL +
    'FROM ' + NL +
    '  ABSENCETOTAL ' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR=:AYEAR'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').AsInteger := FilterData.Employee_Number;
  QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
  QueryWork.Active := True;
  if not QueryWork.IsEmpty then
    CurrentYear := IntMin2StringTime(
      QueryWork.FieldByName('ILLNESS_MINUTE').asInteger,True);
end;
*)

// RV079.4. Note needed anymore.
(*
procedure THoursPerEmployeeDM.GetCounters;
begin
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' + NL +
    //holiday - RV067.MRA.29
    '  NORM_HOLIDAY_MINUTE, USED_HOLIDAY_MINUTE, ' + NL +
    '  LAST_YEAR_HOLIDAY_MINUTE, ' + NL +
    //time for time
    '  LAST_YEAR_TFT_MINUTE, EARNED_TFT_MINUTE, ' + NL +
    '  USED_TFT_MINUTE, ' + NL +
    //work time
    '  LAST_YEAR_WTR_MINUTE, EARNED_WTR_MINUTE, ' + NL +
    '  USED_WTR_MINUTE, ' + NL +
    //Previous year holiday
    '  LAST_YEAR_HLD_PREV_YEAR_MINUTE, USED_HLD_PREV_YEAR_MINUTE, ' + NL +
    //Seniority holiday
    '  NORM_SENIORITY_HOLIDAY_MINUTE, USED_SENIORITY_HOLIDAY_MINUTE, ' + NL +
    //Add. bank holiday
    '  LAST_YEAR_ADD_BANK_HLD_MINUTE, USED_ADD_BANK_HLD_MINUTE, ' + NL +
    //Saturday credit
    '  LAST_YEAR_SAT_CREDIT_MINUTE, USED_SAT_CREDIT_MINUTE, ' + NL +
    //Bank holiday to reserve
    '  NORM_BANK_HLD_RESERVE_MINUTE, USED_BANK_HLD_RESERVE_MINUTE, ' + NL +
    //Shorter week RV071.5. Fields are renamed! Extra field added for earned.
    '  LAST_YEAR_SHORTWWEEK_MINUTE, USED_SHORTWWEEK_MINUTE, ' + NL +
    '  EARNED_SHORTWWEEK_MINUTE, ' + NL +
    // RV071.3 Extra flag when to keep track if
    //         NORM_BANK_HLD_RESERVE_MINUTE was manually changed.
    '  NORM_BANK_HLD_RES_MIN_MAN_YN, ' + NL +
    // RV071.10.
    '  EARNED_SAT_CREDIT_MINUTE ' + NL +
    'FROM ' + NL +
    '  ABSENCETOTAL ' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR=:AYEAR'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
  QueryWork.Active := True;
end;
*)

// RV079.4. Not needed anymore.
(*
// RV071.3.
function THoursPerEmployeeDM.DetermineCounterValue(
  const Fieldname: String): String;
begin
  Result := QueryWork.FieldByName(FieldName).AsString;
end;
*)

// RV079.4. Not needed anymore.
(*
// RV071.3.
function THoursPerEmployeeDM.DetermineNormBankHldResMinManYN: String;
begin
  Result := DetermineCounterValue('NORM_BANK_HLD_RES_MIN_MAN_YN');
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeTimeForTime(var Remaining, Earned,
  Used, Planned, Available: string);
begin
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Remaining := IntMin2StringTime(
      Round(QueryWork.FieldByName('LAST_YEAR_TFT_MINUTE').asFloat),True);
    Earned := IntMin2StringTime(
      Round(QueryWork.FieldByName('EARNED_TFT_MINUTE').asFloat),True);
    Used := IntMin2StringTime(
      Round(QueryWork.FieldByName('USED_TFT_MINUTE').asFloat),True);
  end;
  Planned := IntMin2StringTime(ComputePlanned(QueryWork,
    FilterData.Employee_Number, TIMEFORTIMEAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
    StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
      StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeWorkTime(var Remaining, Earned, Used,
  Planned, Available: string);
begin
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Remaining := IntMin2StringTime(Round(
      QueryWork.FieldByName('LAST_YEAR_WTR_MINUTE').asFloat),True);
    Earned := IntMin2StringTime(Round(
     QueryWork.FieldByName('EARNED_WTR_MINUTE').asFloat),True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_WTR_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      WORKTIMEREDUCTIONAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
    StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
      StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeHldPrevYear(var Remaining, Used, Planned,
    Available: String);
begin
    //Previous year holiday
    //'  LAST_YEAR_HLD_PREV_YEAR_MINUTE, USED_HOLIDAY_PREV_YEAR_MINUTE, ' + NL +
  Remaining := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Remaining := IntMin2StringTime(Round(
      QueryWork.FieldByName('LAST_YEAR_HLD_PREV_YEAR_MINUTE').asFloat),True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_HLD_PREV_YEAR_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      HOLIDAYPREVIOUSYEARAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining)
    - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeSeniorityHld(var Normal, Used, Planned,
    Available: String);
begin
    //Seniority holiday
    //'  NORM_SENIORITY_HOLIDAY_MINUTE, USED_SENIORITY_HOLIDAY_MINUTE, ' + NL +
  Normal := NullTime; Used := NullTime; Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Normal := IntMin2StrDigTime(Round(
      QueryWork.FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').asFloat), 3, True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_SENIORITY_HOLIDAY_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      SENIORITYHOLIDAYAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Normal)
    - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeAddBnkHoliday(var Remaining, Used, Planned,
    Available: String);
begin
    //Add. bank holiday
    //'  LAST_YEAR_ADD_BANK_HOLIDAY_MINUTE, USED_ADD_BANK_HOLIDAY_MINUTE, ' + NL +
  Remaining := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Remaining := IntMin2StringTime(Round(
      QueryWork.FieldByName('LAST_YEAR_ADD_BANK_HLD_MINUTE').asFloat),True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_ADD_BANK_HLD_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      ADDBANKHOLIDAYAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
    - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
// RV071.10 Addition of Earned. Remove of Remaining.
procedure THoursPerEmployeeDM.ComputeSatCredit(var Remaining, Earned, Used, Planned,
    Available: String);
begin
    //Saturday credit
    //'  LAST_YEAR_SAT_CREDIT_MINUTE, USED_SAT_CREDIT_MINUTE, ' + NL +
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    // RV071.10 Do not get Remaining.
{    Remaining := IntMin2StringTime(Round(
      QueryWork.FieldByName('LAST_YEAR_SAT_CREDIT_MINUTE').asFloat),True); }
    Earned := IntMin2StringTime(Round(
      QueryWork.FieldByName('EARNED_SAT_CREDIT_MINUTE').asFloat),True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_SAT_CREDIT_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      SATCREDITAVAILABILITY, MyDateFrom),True);
  // RV071.10 Addition of Earned.
  Available := IntMin2StringTime( { (StrTime2IntMin(Remaining) + }
    (StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
    StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeRsvBnkHoliday(var Normal, Used, Planned,
    Available: String);
begin
    //Bank holiday to reserve
    //'  NORM_BANK_HOLIDAY_RESERVE_MINUTE, USED_BANK_HOLIDAY_RESERVE_MINUTE, ' + NL +
  Normal := NullTime; Used := NullTime; Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Normal := IntMin2StrDigTime(Round(
      QueryWork.FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').asFloat), 3, True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_BANK_HLD_RESERVE_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      BANKHOLIDAYRESERVEAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Normal)
    - StrTime2IntMin(Used) - StrTime2IntMin(Planned)),True);
end;
*)

// RV079.4. Not needed anymore.
(*
procedure THoursPerEmployeeDM.ComputeShorterWeek(var Remaining, Earned,
  Used, Planned, Available: String);
begin
    //Shorter week
    //'  LAST_YEAR_SHORTWWEEK_MINUTE, USED_SHORTWWEEK_MINUTE ' + NL +
    // RV071.5. Addition of: EARNED_SHORTWWEEK_MINUTE
  Remaining := NullTime; Earned := NullTime; Used := NullTime;
  Planned := NullTime; Available := NullTime;
  if not QueryWork.IsEmpty then
  begin
    Remaining := IntMin2StringTime(Round(
      QueryWork.FieldByName('LAST_YEAR_SHORTWWEEK_MINUTE').asFloat),True);
    Earned := IntMin2StrDigTime(Round(
      QueryWork.FieldByName('EARNED_SHORTWWEEK_MINUTE').asFloat), 3, True);
    Used := IntMin2StringTime(Round(
      QueryWork.FieldByName('USED_SHORTWWEEK_MINUTE').asFloat),True);
  end;
  Planned :=
    IntMin2StringTime(ComputePlanned(QueryWork, FilterData.Employee_Number,
      SHORTERWORKWEEKAVAILABILITY, MyDateFrom),True);
  Available := IntMin2StringTime((StrTime2IntMin(Remaining) +
    StrTime2IntMin(Earned) - StrTime2IntMin(Used) -
      StrTime2IntMin(Planned)),True);
end;
*)

// RV071.3. When changing the 'Norm Bank Holiday Reserve Minute'-field,
// then store this in a flag and use a different color to indicate
// it has been manually changed.
function THoursPerEmployeeDM.SetNormEarnedField(AFieldName, ANormEarned: String): Boolean;
var
  index: Integer;
  TotMinutes: Double;
  Temp: String;
  OldMinutes: Double;
  SelectFlag1, FlagFieldName1, UpdateFlag1: String;
begin
  Temp := ANormEarned;
  SelectFlag1 := '';
  FlagFieldName1 := '';
  UpdateFlag1 := '';
  for index := 1 to length(Temp) do
    if Temp[index]=' ' then Temp[index] := '0';
  TotMinutes := StrTime2IntMin(Temp);
  try
    if AFieldName = 'NORM_BANK_HLD_RESERVE_MINUTE' then
    begin
      FlagFieldName1 := 'NORM_BANK_HLD_RES_MIN_MAN_YN';
      SelectFlag1 := ', ' + FlagFieldName1 + ' ';
    end;
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(
      'SELECT ' + NL +
      '  ' + AFieldName + ' ' + NL +
      SelectFlag1 + NL +
      'FROM ' + NL +
      '  ABSENCETOTAL ' + NL +
      'WHERE ' + NL +
      '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR=:AYEAR'
      );
    QueryWork.Prepare;
    QueryWork.ParamByName('EMPNO').AsInteger := FilterData.Employee_Number;
    QueryWork.ParamByName('AYEAR').AsInteger := FilterData.Year;
    QueryWork.Active := True;
    if not QueryWork.IsEmpty then
    begin
      OldMinutes := QueryWork.FieldByName(AFieldName).AsInteger;
      if (OldMinutes <> TotMinutes) then
      begin
        if AFieldName = 'NORM_BANK_HLD_RESERVE_MINUTE' then
        begin
          if (QueryWork.
            FieldByName(FlagFieldName1).AsString <>
              CHECKEDVALUE) then
            UpdateFlag1 := ', ' + FlagFieldName1 + ' = :FLAG1 ';
        end;
        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add(
          'UPDATE ABSENCETOTAL ' + NL +
          'SET ' + AFieldName + '=:MIN ' + NL +
          UpdateFlag1 + NL +
          ', MUTATOR =:MUTATOR , MUTATIONDATE = :MUTATIONDATE ' + NL +
          'WHERE EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR=:AYEAR'
          );
        QueryWork.Prepare;
        QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
        QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
        QueryWork.ParamByName('MIN').asFloat := TotMinutes;
        if UpdateFlag1 <> '' then
          QueryWork.ParamByName('FLAG1').AsString := CHECKEDVALUE;
        QueryWork.ParamByName('MUTATOR').asString := SystemDM.CurrentProgramUser;
        QueryWork.ParamByName('MUTATIONDATE').asDateTime := Now;
        QueryWork.ExecSQL;
      end; // if (OldMinutes <> TotMinutes) then
    end
    else
    begin
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'INSERT INTO ABSENCETOTAL ' + NL +
        '(EMPLOYEE_NUMBER,ABSENCE_YEAR,' + AFieldName + ', ' + NL +
        'CREATIONDATE,MUTATIONDATE,MUTATOR) ' + NL +
        'VALUES(:EMPNO,:AYEAR,:MIN,:CDATE,:CDATE,:AUTOR)'
        );
      QueryWork.Prepare;
      QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
      QueryWork.ParamByName('AYEAR').asInteger := FilterData.Year;
      QueryWork.ParamByName('MIN').asFloat := TotMinutes;
      QueryWork.ParamByName('CDATE').asDateTime := now;
      QueryWork.ParamByName('AUTOR').asString := SystemDM.CurrentProgramUser;
      QueryWork.ExecSQL;
    end;
    Result := True;
  except
    QueryWork.Active := False;
    Result := False;
  end;
  QueryWork.Active := False;
end;

function THoursPerEmployeeDM.SetNormHoliday(NormHoliday: String): Boolean;
begin
  Result := SetNormEarnedField('NORM_HOLIDAY_MINUTE', NormHoliday);
end;

function THoursPerEmployeeDM.SetNormSeniority(NormHoliday: String): Boolean;
begin
  Result := SetNormEarnedField('NORM_SENIORITY_HOLIDAY_MINUTE', NormHoliday);
end;

function THoursPerEmployeeDM.SetNormRsvBnkHoliday(NormHoliday: String): Boolean;
begin
  Result := SetNormEarnedField('NORM_BANK_HLD_RESERVE_MINUTE', NormHoliday);
end;

// RV071.5.
function THoursPerEmployeeDM.SetEarnedShorterWorkerWeek(
  Earned: String): Boolean;
begin
  Result := SetNormEarnedField('EARNED_SHORTWWEEK_MINUTE', Earned);
end;

procedure  THoursPerEmployeeDM.CalculateNormalHoursPerEmpl(Year,
  Weeknr: Integer; Empl: Integer;
  Dept: String; var TotalPHrs, TotalSHrs: TWeekDaysTotal);
var
  TBWeek: Array[1..7,1..4] of String;
  IndexDay, IndexTB: Integer;
  SavePlant, SelectStr : String;
  SaveShift: Integer;
 // DateWeek: TDateTime;
  SHS_SHIFT: TWeekDaysTotal;
  procedure ProdSalaryBreaks( PayedYN: Boolean;  var TotalHrs: TWeekDaysTotal);
  var
    IndexDay, IndexTB: Integer;
  begin
  //CAR 22-7-2003
    ATBLengthClass.FillBreaks(Empl, SaveShift, SavePlant, Dept, PayedYN);
    for indexDay := 1 to 7 do
      for IndexTB := 1 to 4 do
        if (TBWeek[indexDay][IndexTB] = '*') then
        //CAR 21-7-2003
          TotalHrs[IndexDay] := TotalHrs[IndexDay] +
            ATBLengthClass.GetLengthTB(IndexTB, IndexDay);
  end;
begin
  if Year = 1950 then // Initial value of component!
    Exit;
  for IndexDay := 1 to 7 do
  begin
    TotalPHrs[IndexDay] := 0;
    TotalSHrs[IndexDay] := 0;
    SHS_SHIFT[INDEXDAY] := -1;
  end;
  for indexDay:= 1 to 7 do
    for IndexTB := 1 to 4 do
      TBWeek[indexDay][IndexTB] := '';

  SelectStr :=
    'SELECT ' + NL +
    '  SHIFT_SCHEDULE_DATE, SHIFT_NUMBER ' + NL +
    'FROM ' + NL +
    '  SHIFTSCHEDULE ' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER = '+ IntToStr(Empl) + NL +
    '  AND SHIFT_SCHEDULE_DATE >= :DATESTART AND ' + NL +
    //CAR 550276
    '  SHIFT_SCHEDULE_DATE <= :DATEEND AND SHIFT_NUMBER <> -1 ' + NL +
    'ORDER BY ' + NL +
    '  SHIFT_SCHEDULE_DATE';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  QueryWork.Prepare;
  QueryWork.ParamByName('DATESTART').asDateTime :=
    ListProcsF.DateFromWeek(Year, Weeknr,1);
  QueryWork.ParamByName('DATEEND').asDateTime :=
    ListProcsF.DateFromWeek(Year, Weeknr,7);
  QueryWork.Active := True;
  QueryWork.First;
  if QueryWork.IsEmpty then
  begin
    QueryWork.Close;
    Exit;
  end;
  while not QueryWork.Eof do
  begin
    SHS_SHIFT[ListProcsF.DayWStartOnFromDate(
      QueryWork.FieldByName('SHIFT_SCHEDULE_DATE').AsDateTime)] :=
      QueryWork.FieldByName('SHIFT_NUMBER').AsInteger;
    QueryWork.Next;
  end;

  SelectStr := 
    'SELECT ' + NL +
    '  PLANT_CODE, SHIFT_NUMBER, DAY_OF_WEEK, ' + NL +
    '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2, ' + NL +
    '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4  ' + NL +
    'FROM ' + NL +
    '  STANDARDAVAILABILITY ' + NL +
    'WHERE ' + NL +
    '  EMPLOYEE_NUMBER = :EMPL AND ' + NL +
    '  ((AVAILABLE_TIMEBLOCK_1 = ''*'' ) OR ' + NL +
    '   (AVAILABLE_TIMEBLOCK_2 = ''*'') OR ' + NL +
    '   (AVAILABLE_TIMEBLOCK_3 = ''*'' ) OR ' + NL +
    '   (AVAILABLE_TIMEBLOCK_4 = ''*'')) ' + NL +
    'ORDER BY ' + NL +
    '  PLANT_CODE, SHIFT_NUMBER, DAY_OF_WEEK ';
    QueryWork.Active := False;
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(SelectStr);
    QueryWork.Prepare;
    QueryWork.ParamByName('EMPL').asInteger := Empl;
    QueryWork.Active := True;
    QueryWork.First;

    SavePlant := ''; SaveShift := -1;
    if QueryWork.IsEmpty then
    begin
      QueryWork.Close;
      Exit;
    end;
    while not QueryWork.Eof do
    begin
      if SavePlant = '' then
      begin
        SavePlant := QueryWork.FieldByNAME('PLANT_CODE').AsString;
        SaveShift := QueryWork.FieldByNAME('SHIFT_NUMBER').AsInteger;
      end;
      if (SavePlant = QueryWork.FieldByNAME('PLANT_CODE').AsString) and
        (SaveShift = QueryWork.FieldByNAME('SHIFT_NUMBER').AsInteger) and
        (SHS_SHIFT[QueryWork.FieldByNAME('DAY_OF_WEEK').AsInteger] = SaveShift) then
        for IndexTB := 1 to 4 do
          TBWeek[QueryWork.FieldByNAME('DAY_OF_WEEK').AsInteger][IndexTB] :=
            QueryWork.FieldByNAME('AVAILABLE_TIMEBLOCK_' +
              IntToStr(IndexTB)).AsString;

      if (SavePlant <> QueryWork.FieldByNAME('PLANT_CODE').AsString) or
        (SaveShift <> QueryWork.FieldByNAME('SHIFT_NUMBER').AsInteger) then
      begin
        ATBLengthClass.FillTBArray(Empl, SaveShift, SavePlant, Dept);
        ProdSalaryBreaks(False, TotalPHrs);
        ProdSalaryBreaks(True, TotalSHrs);
        for indexDay:= 1 to 7 do
          for IndexTB := 1 to 4 do
            TBWeek[indexDay][IndexTB] := '';
        SavePlant := QueryWork.FieldByNAME('PLANT_CODE').AsString;
        SaveShift := QueryWork.FieldByNAME('SHIFT_NUMBER').AsInteger;
        if (SHS_SHIFT[QueryWork.FieldByNAME('DAY_OF_WEEK').AsInteger] = SaveShift) then
          for IndexTB := 1 to 4 do
            TBWeek[QueryWork.FieldByNAME('DAY_OF_WEEK').AsInteger][IndexTB] :=
               QueryWork.FieldByNAME('AVAILABLE_TIMEBLOCK_' +
                 IntToStr(IndexTB)).AsString;
      end;
      QueryWork.Next;
    end;

  if SaveShift <> -1 then
  begin
    //CAR 22-7-2003
    ATBLengthClass.FillTBArray(Empl, SaveShift, SavePlant, Dept);
    ProdSalaryBreaks(False, TotalPHrs);
    ProdSalaryBreaks(True, TotalSHrs);
  end;
  QueryWork.Close;
end;

procedure THoursPerEmployeeDM.ComputeTotal(Year, Weeknr: Integer;
  var TotalStr: TDailyTime;
  var Total, TotalPHrs, TotalSHrs: TWeekDaysTotal);
var
 TotalMin, index: integer;
begin
  DeactivateTablesEvents(True);
  for index := 1 to 7 do
    Total[index] := 0;
  case FilterData.ActiveTab of
  ProductionHours:
    begin
      DataSourceHREPRODUCTION.Enabled := False;
      cdsHREPRODUCTION.First;
      while not cdsHREPRODUCTION.Eof do
      begin
        for index := 1 to 7 do
          Total[index] := Total[index] +
            GetDailyMinutes(cdsHREPRODUCTION,index);
        cdsHREPRODUCTION.Next;
      end;
      cdsHREPRODUCTION.First;
      DataSourceHREPRODUCTION.Enabled := True;
    end;
  SalaryHours:
    begin
      DataSourceHRESALARY.Enabled := False;
      cdsHRESALARY.First;
      while not cdsHRESALARY.Eof do
      begin
        for index:=1 to 7 do
          Total[index] := Total[index] +
            GetDailyMinutes(cdsHRESALARY,index);
        cdsHRESALARY.Next;
      end;
      cdsHRESALARY.First;
      DataSourceHRESALARY.Enabled := True;
    end;
  end;
  DataSourceHREABSENCE.Enabled := False;
  cdsHREABSENCE.First;
  while not cdsHREABSENCE.Eof do
  begin
    for index := 1 to 7 do
      Total[index] := Total[index] +
        GetDailyMinutes(cdsHREABSENCE,index);
    cdsHREABSENCE.Next;
  end;
  cdsHREABSENCE.First;
  DataSourceHREABSENCE.Enabled := True;
  TotalMin := 0;
  for index := 1 to 7 do
  begin
    TotalStr[index] := IntMin2StringTime(Total[index],True);
    TotalMin := TotalMin + Total[index];
  end;
  TotalStr[8]:=IntMin2StringTime(TotalMin,True);
  DeactivateTablesEvents(False);
  CalculateNormalHoursPerEmpl(Year, WeekNr,
    cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    cdsFilterEmployee.FieldByName('DEPARTMENT_CODE').AsString,
    TotalPHrs, TotalSHrs);
end;

procedure THoursPerEmployeeDM.PopulateAbsenceIDCard(AShiftNumber: Integer;
  var EmployeeIDCard: TScannedIDCard; ShiftNumber: integer; WeekDay: TDateTime);
begin
  if (not cdsFilterEmployee.Active) or cdsFilterEmployee.IsEmpty or
    (not TablePlant.Active) or TablePlant.IsEmpty then
    Exit;
  EmployeeIDCard.EmployeeCode :=
    cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  // RV100.1. Added Contractgroup.
  EmployeeIDCard.ContractgroupCode :=
    cdsFilterEmployee.FieldByName('CONTRACTGROUP_CODE').AsString;
  EmployeeIDCard.PlantCode := cdsFilterEmployee.FieldByName('PLANT_CODE').asString;
  EmployeeIDCard.DepartmentCode := cdsFilterEmployee.FieldByName('DEPARTMENT_CODE').
    AsString;
  EmployeeIDCard.CutOfTime :=
    cdsFilterEmployee.FieldByName('CUT_OF_TIME_YN').asString;
  EmployeeIDCard.InscanEarly :=
    TablePlant.FieldByName('INSCAN_MARGIN_EARLY').asInteger;
  EmployeeIDCard.InscanLate := TablePlant.FieldByName('INSCAN_MARGIN_LATE').
    asInteger;
  EmployeeIDCard.OutScanEarly :=
    TablePlant.FieldByName('OUTSCAN_MARGIN_EARLY').asInteger;
  EmployeeIDCard.OutScanLate :=
    TablePlant.FieldByName('OUTSCAN_MARGIN_LATE').asInteger;
  // MR:01-10-2003 Needed for 'GetShiftDay'!
  EmployeeIDCard.ShiftNumber := AShiftNumber;
  EmployeeIDCard.DateIn := WeekDay + 0.5;
  EmployeeIDCard.DateOut := WeekDay + 0.5;
end;

procedure THoursPerEmployeeDM.SetWeekDates(Year,WeekNo: integer);
var
  StartWeekDate: TDateTime;
  index: integer;
begin
  if (Year = 0) and (WeekNo = 0) then
  begin
    StartWeekDate := Now - DayInWeek(SystemDM.WeekStartsOn, Now) + 1;
    ReplaceTime(StartWeekDate,EncodeTime(0,0,0,0));
  end
  else
    StartWeekDate := ListProcsF.DateFromWeek(Year,WeekNo,1);
  for index := 1 to 7 do
    HoursPerEmployeeDM.WeekDates[index] := StartWeekDate + index - 1;
end;

procedure THoursPerEmployeeDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  try
    cdsFilterEmployee.Open;
    cdsFilterEmployee.LogChanges := False;
    cdsFilterEmployee.Close;
  except
  end;

  // RV082.3. Should 3 tables (PHE, SHE + PHEPT) be in sync or not?
  Sync3Tables := (SystemDM.GetFillProdHourPerHourTypeYN = CHECKEDVALUE);

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmployee.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmployee.ParamByName('USER_NAME').AsString := '*';
  QueryEmployee.Prepare;
  // MRA:12 FEB 2008, REV003: END

  // RV073.3.
  SystemDM.PlantTeamFilterEnable(TableWorkspot);
end;

procedure THoursPerEmployeeDM.DefaultBeforePost(DataSet: TDataSet);
var
  TotalMin, index: integer;
  AllTimeFieldsNull, SalaryChanged, OvertimeRecalculated: boolean;
  DailyMinutes: array[1..7]of integer;
  AbsenceReason, AbsenceType: string;
  year, month, day: word;
  MyShiftNumber: Integer;
  // RV071.10. 550497
  function SaturdayCreditAction: Boolean;
  var
    Index, OldSalMinutes, NewSalMinutes, SalMinutes: Integer;
  begin
    Result := False;
    // Is hourtype of type 'Saturday Credit?'
    try
      if SaturdayCreditHourtypeCheck(DataSet) then
      begin
        // Get MaxSatCreditMinute from contractgroup of employee.
        if FilterData.MaxSatCreditMinute > 0 then
        begin
          //
          // Check if MaxSatCreditMinute has been reached.
          //
          // Get salary minute that are entered.
          OldSalMinutes := 0;
          NewSalMinutes := 0;
          for Index := 1 to 7 do
          begin
            OldSalMinutes := OldSalMinutes + OldMinutes[Index];
            NewSalMinutes := NewSalMinutes + GetDailyMinutes(cdsHRESALARY, Index);
          end;
          SalMinutes := NewSalMinutes - OldSalMinutes;
          // Get total max sat credit minute till now.
          qryAbsenceTotalSatCred.Close;
          qryAbsenceTotalSatCred.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            FilterData.Employee_Number;
          qryAbsenceTotalSatCred.ParamByName('ABSENCE_YEAR').AsInteger :=
            FilterData.Year;
          qryAbsenceTotalSatCred.Open;
          if not qryAbsenceTotalSatCred.Eof then
            if qryAbsenceTotalSatCred.
              FieldByName('EARNED_SAT_CREDIT_MINUTE').AsInteger + SalMinutes >
                FilterData.MaxSatCreditMinute then
            begin
              // Max. Sat Credit has been reached!
              Result := True;
            end;
        end;
      end;
    finally
      qryAbsenceTotalSatCred.Close;
      qryHourtypeCheck.Close;
    end;
  end; // SaturdayCreditAction
  procedure ProductionHourAction;
  var
    Index: Integer;
  begin
    CurrentRecordProd.Plant :=
      cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString;
    CurrentRecordProd.Workspot :=
      cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString;
    CurrentRecordProd.Job :=
      cdsHREPRODUCTION.FieldByName('JOB_CODE').asString;
    CurrentRecordProd.Shift :=
      cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger;
    // unprocess old values TTable
    SalaryChanged := False;
    for Index := 1 to 7 do
    begin
      DailyMinutes[Index] := GetDailyMinutes(cdsHREPRODUCTION, Index);
      // RV082.2. Keep track of old sal. record.
      //          Init here.
      OldSalaryRec.HourType := -1;
      if (OldProductionRec.Plant <> DataSet.FieldByName('PLANT_CODE').AsString) or
        (OldProductionRec.Workspot <> DataSet.FieldByName('WORKSPOT_CODE').AsString) or
        (OldProductionRec.Job <> DataSet.FieldByName('JOB_CODE').AsString) or
        (OldProductionRec.Shift <> DataSet.FieldByName('SHIFT_NUMBER').AsInteger) or
        (DailyMinutes[Index] <> OldMinutes[Index]) then
          // need to substract oldsalaryminutes
      begin
        OvertimeRecalculated := False;
        if OldMinutes[Index] <> 0 then
        begin
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
            CorrectSalaryHour(WeekDates[Index], FilterData.Employee_Number,
              (-1)*OldMinutes[Index], OldProductionRec.Plant,
            OldProductionRec.Workspot,OldProductionRec.Job,
              CHECKEDVALUE, CHECKEDVALUE, OldProductionRec.Shift);

          DeleteProductionRec(OldProductionRec.Plant,
            OldProductionRec.WorkSpot,OldProductionRec.Job,
            OldProductionRec.Shift,WeekDates[Index],WeekDates[Index]);
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            if FilterData.OverTimePeriod = 1 then
            begin
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[Index]);
              OvertimeRecalculated := True;
            end;
            SalaryChanged := True;
          end;
        end;
        // insert new minutes in ProdHourPerEmployee
        if DailyMinutes[Index] <> 0 then
        begin
          InsertProductionMinutes(WeekDates[Index],
            cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString,
            cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString,
            cdsHREPRODUCTION.FieldByName('JOB_CODE').asString,
            cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger,
            DailyMinutes[Index]);
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            CorrectSalaryHour(WeekDates[Index], FilterData.Employee_Number,
              DailyMinutes[Index],
              cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString,
              cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString,
              cdsHREPRODUCTION.FieldByName('JOB_CODE').asString,
              //REV065.9
              CHECKEDVALUE, CHECKEDVALUE,
              cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger);
            // recalculate salaryhours only if header differ from old record
            if (FilterData.OverTimePeriod = 1) and
              (not OvertimeRecalculated)then
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[Index]);
            SalaryChanged := True;
          end;
        end;
        // RV082.2.
        RecalcPHEPT(WeekDates[Index]);
      end; // if (OldProductionRec.Plant <> DataSet.FieldByName('PLANT_CODE').AsString) or
    end; // for Index := 1 to 7 do
    if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
      if (FilterData.OverTimePeriod in [2,3]) and SalaryChanged then
        RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
    // RV075.2.
    GlobalDM.RecalculateBalances(FilterData.Employee_Number, WeekDates[1],
      WeekDates[7]);
  end; // ProductionHourAction
  procedure SalaryHourAction;
  var
    Index: Integer;
  begin
    CurrentRecordSal.HourType :=
      DataSet.FieldByName('HOURTYPE_NUMBER').asInteger;
    // unprocess old values
    SalaryChanged := False;
    for Index := 1 to 7 do
    begin
      OvertimeRecalculated := False;
      DailyMinutes[index] := GetDailyMinutes(cdsHRESALARY,index);
      if (OldSalaryRec.HourType <>
        DataSet.FieldByName('HOURTYPE_NUMBER').AsInteger) or
        (OldMinutes[index] <> DailyMinutes[index]) then
      begin
        //Delete Old SalaryMinutes
        if OldMinutes[index] <> 0 then
        begin
          DeleteSalaryRec(OldSalaryRec.HourType,
            WeekDates[index], WeekDates[index]);
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            if FilterData.OverTimePeriod = 1 then
            begin
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
              OvertimeRecalculated := True;
            end;
            SalaryChanged := True;
          end;
        end;
        // Insert New Values
        if DailyMinutes[index] <> 0 then
        begin
          InsertSalaryMinutes(WeekDates[index], OldMinutes[Index],
            DailyMinutes[index],
            cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').asInteger);
         { if (not OvertimeRecalculated) or
          	((OldSalaryRec.HourType <> DataSet.FieldByName('HOURTYPE_NUMBER').
          	asInteger)and(FilterData.OverTimePeriod = 1)) then}
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            if (FilterData.OverTimePeriod = 1) then
              if (not OvertimeRecalculated) or
                (OldSalaryRec.HourType <>
                DataSet.FieldByName('HOURTYPE_NUMBER').asInteger) then
                RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
            SalaryChanged := True;
          end;
        end;
        // RV082.2.
        RecalcPHEPT(WeekDates[Index]);
      end; // if (OldSalaryRec.HourType <>
    end; // for Index := 1 to 7 do
    if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
      if (FilterData.OverTimePeriod in [2,3]) and SalaryChanged then
        RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
    // RV071.10.
    SaturdayCreditAbsenceTotalUpdate(DataSet);
    // 20013183
    TravelTimeAbsenceTotalUpdate(DataSet);
    // RV075.2.
    GlobalDM.RecalculateBalances(FilterData.Employee_Number, WeekDates[1],
      WeekDates[7]);;
  end; // SalaryHourAction
  procedure AbsenceHourAction;
  var
    Index: Integer;
    MyAbsenceType: Char;
  begin
    CurrentRecordAbs.AbsenceReason :=
      DataSet.FieldByName('ABSENCEREASON_CODE').asString;
    // unprocess old values TTable
    SalaryChanged := False;
    for Index := 1 to 7 do
    begin
      OvertimeRecalculated := False;
      DailyMinutes[index] := GetDailyMinutes(cdsHREABSENCE,index);
      if (OldAbsenceRec.AbsenceReason <>
        cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').asString) or
        (OldMinutes[index] <> DailyMinutes[index]) then
      // Substract old minutes from AbsenceTotal
      begin
        if OldMinutes[index] <> 0 then
        begin
          AbsenceReason := OldAbsenceRec.AbsenceReason;
          if TableAbsenceType.FindKey([AbsenceReason]) then
          AbsenceType:=TableAbsenceType.FieldByName('ABSENCETYPE_CODE').asString;
          DecodeDate(WeekDates[index],year,month,day);
          // 200113183 Travel time. Book this as 'used travel time' in balance.
          MyAbsenceType := CharFromStr(AbsenceType,1,' ');
          if MyAbsenceType = TRAVELTIME then
            MyAbsenceType := USED_TRAVELTIME; // Used
          UpdateAbsenceTotalMode(QueryWork,FilterData.Employee_Number,year,
            ((-1)*OldMinutes[index]), MyAbsenceType);
          DeleteAbsenceRec(AbsenceReason,WeekDates[index],WeekDates[index]);
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            if FilterData.OverTimePeriod = 1 then
            begin
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
              OvertimeRecalculated := True;
            end;
            SalaryChanged := True;
          end;
        end;
        // insert new Abs. Minutes
        if DailyMinutes[index] <> 0 then
        begin
          AbsenceReason := cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').asString;
          InsertAbsenceMinutes(DailyMinutes[index],WeekDates[index],AbsenceReason);
          if TableAbsenceType.FindKey([AbsenceReason]) then
          AbsenceType:=TableAbsenceType.FieldByName('ABSENCETYPE_CODE').asString;
          DecodeDate(WeekDates[index],year,month,day);
          // 200113183 Travel time. Book this as 'used travel time' in balance.
          MyAbsenceType := CharFromStr(AbsenceType,1,' ');
          if MyAbsenceType = TRAVELTIME then
            MyAbsenceType := USED_TRAVELTIME; // Used
          UpdateAbsenceTotalMode(QueryWork,FilterData.Employee_Number,year,
            ({(-1)*}DailyMinutes[index]), MyAbsenceType);
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
          begin
            // 2.0.162.215.1 Bugfix: Also look if oldminutes <> dailyminutes (newminutes) !
            // Always recalculate: It is possible the hourtype-settings were changed,
            // which can give a different result, even when the hours are the same.
            if (FilterData.OverTimePeriod = 1) then
{               if (not OvertimeRecalculated) or
                 (AbsenceReason <> OldAbsenceRec.AbsenceReason) or
                 (OldMinutes[index] <> DailyMinutes[index]) then // 2.0.162.215.1 }
                 RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
            SalaryChanged := True;
          end;
        end;
      end;
    end;
    if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
      if (FilterData.OverTimePeriod in [2,3]) and SalaryChanged then
        RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
  end; // AbsenceHourAction
begin
  MyShiftNumber := -1; // If DataSet is 'Absence' or 'Salary'

  // MR:17-01-2003 Check required fields FIRST!
  // To ensure all required fields have been filled in.
  // Do this HERE, otherwise it will be too late if it's done later!
  // which results in a saved record that has empty fields.
  SystemDM.DefaultbeforePost(DataSet);
  // MR:17-01-2003 Check if 'workspot' or 'shift' is not empty ('')
  if DataSet = QueryEmployee then
    Exit;
  if DataSet = cdsFilterEmployee then
    Exit;
  if (DataSet = cdsHREPRODUCTION) then
  begin
    if DataSet.FieldByName('SHIFT_NUMBER').AsString = '' then
    begin
      DisplayMessage(SPimsUseShift, mtInformation, [mbOk]);
      // RV068.1. Abort added.
      Abort;
      Exit;
    end;
    if DataSet.FieldByName('WORKSPOT_CODE').AsString = '' then
    begin
      DisplayMessage(SPimsUseWorkspot, mtInformation, [mbOk]);
      // RV068.1. Abort added.
      Abort;
      Exit;
    end;
    MyShiftNumber :=
      DataSet.FieldByName('SHIFT_NUMBER').AsInteger;
  end;
  if HoursPerEmployeeDM.CheckIfExistRecord(DataSet) then
  begin
    DisplayMessage(SPimsKeyViolation, mtInformation, [mbOK]);
    DataSet.Cancel;
    Abort;
  end;
  if (DataSet = cdsHREPRODUCTION) then
  begin
    if TableWk.FindKey([
      DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('WORKSPOT_CODE').AsString]) then
      if (TableWK.FieldByName('USE_JOBCODE_YN').AsString = CHECKEDVALUE) and
        ((DataSet.FieldByName('JOB_CODE').AsString = '') OR
        (DataSet.FieldByName('JOB_CODE').AsString = DUMMYSTR)) then
      begin
        DisplayMessage(SPimsUseJobCode, mtInformation, [mbOk]);
        // RV068.1. Do not cancel the record!
        // DataSet.Cancel;
        Abort;
      end;
    // MR:18-02-2005 Order 550375 Force job_code to '0'.
    if (TableWK.FieldByName('USE_JOBCODE_YN').AsString = UNCHECKEDVALUE) then
      DataSet.FieldByName('JOB_CODE').AsString := DUMMYSTR;
  end;
  AllTimeFieldsNull := True;
  for Index := 1 to 7 do
    if GetDailyMinutes(DataSet, Index) = 0 then
      DataSet.FieldByName(Format(SignFieldMask+'%d', [Index])).asString :=
        CHECKEDVALUE
    else
      AllTimeFieldsNull := False;
  if AllTimeFieldsNull then
  begin
    DisplayMessage(SPimsNoTimeValue, mtInformation, [mbOk]);
    // RV068.1. Do not cancel the record!
//    DataSet.Cancel;
    Abort;
    exit;
  end;
  //REV065.5
  if
    Assigned(Dataset.FindField('HOURTYPE_NUMBER')) and
    (Trim(Dataset.FieldByName('HOURTYPE_NUMBER').AsString) <> '') and
    (not HoursPerEmployeeDM.QueryHourType.Locate('HOURTYPE_NUMBER', Dataset.FieldByName('HOURTYPE_NUMBER').Value, []))
  then
  begin
    DisplayMessage(SPimsIncorrectHourType, mtInformation, [mbOk]);
    // RV067.MRA.4. Do not cancel the record!
//    DataSet.Cancel;
    Abort;
    exit;
  end;
  if DataSet.FieldByName('MANUAL_YN').asString = UNCHECKEDVALUE then
  begin
    DisplayMessage(SPimsAutomaticRecord, mtInformation, [mbOk]);
    // RV067.MRA.4. Do not cancel the record!
//    DataSet.Cancel;
    Abort;
    Exit;
  end;
  // RV040. Check if there was an absencereason entered. This is mandatory!
  // RV041. Only 'displaymessage' was in the if! All should be part of if!
  if DataSet = cdsHREABSENCE then
  begin
    if (DataSet.FieldByName('ABSENCEREASON_CODE').AsString = '') then
    begin
      DisplayMessage(SPimsEmptyAbsReason, mtInformation, [mbOK]);
      // RV067.MRA.4. Do not cancel the record!
//      DataSet.Cancel;
      Abort;
      Exit;
    end;
  end;
  // RV071.10. 550497.
  // Salary Hours
  if DataSet = cdsHRESALARY then
  begin
    // RV071.10. 550497
    if SaturdayCreditAction then
    begin
      DisplayMessage(SPimMaxSatCreditReached, mtInformation, [mbOk]);
      Abort;
      Exit;
    end;
  end;
  SystemDM.Pims.StartTransaction;
  try
    // Production Hours
    if DataSet = cdsHREPRODUCTION then
    begin
      ProductionHourAction;
    end;
    // Salary Hours
    if DataSet = cdsHRESALARY then
    begin
      SalaryHourAction;
    end;
    // Absence Hours
    if DataSet = cdsHREABSENCE then
    begin
      AbsenceHourAction;
    end;
    // Compute Total field.
    TotalMin := 0;
    for index := 1 to 7 do
      TotalMin := TotalMin + GetDailyMinutes(DataSet,index);
    DataSet.FieldByName('TOTAL').asInteger := TotalMin;
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Rollback;
    DisplayMessage(SPimsExistingRecord, mtInformation, [mbOk]);
    DataSet.Cancel;
    Abort;
  end;
  inherited;
  // MR:17-01-2003 Moved to higher place!
//  SystemDM.DefaultbeforePost(DataSet);
end;

procedure THoursPerEmployeeDM.DefaultAfterDelete(DataSet: TDataSet);
begin
  THoursPerEmployeeF(Owner).ResetTimeEditColor;
  THoursPerEmployeeF(Owner).ReturnTotals;
  // MR:02-10-2003 Refresh is needed
  if (DataSet = cdsHRESALARY) or (DataSet = cdsHREABSENCE) then
  //RV067.10.
    THoursPerEmployeeF(Owner).RefreshData;
end;

procedure THoursPerEmployeeDM.DefaultBeforeDelete(DataSet: TDataSet);
var
  Index, Minutes: Integer;
  SalaryHourChanged: Boolean;
  AbsenceReasonCode, AbsenceTypeCode: string;
  year,month,day: word;
  MyShiftNumber: Integer;
  MyAbsenceType: Char;
  WeekStartIndex, WeekEndIndex: Integer;
begin
  inherited;
  MyShiftNumber := -1; // If DataSet is 'Absence' or 'Salary'
  // 20011800 Handling of Final Run System and delete-action.
  // Only allow delete for records on dates that come AFTER the last-
  // export-date!
  WeekStartIndex := 1;
  WeekEndIndex := 7;
  if SystemDM.UseFinalRun then
  begin
    WeekStartIndex := -1;
    WeekEndIndex := 7;
    if FilterData.ExportDate <> NullDate then
    begin
      for Index := 1 to 7 do
      begin
        if (WeekDates[Index] > FilterData.ExportDate) and (WeekStartIndex = -1) then
          WeekStartIndex := Index;
      end;
    end
    else
      WeekStartIndex := 1;
    if WeekStartIndex = -1 then
    begin
      DisplayMessage(SPimsFinalRunDeleteNotAllowed, mtInformation, [mbOk]);
      DataSet.Cancel;
      Exit;
    end
    else
      if WeekStartIndex > 1 then
        DisplayMessage(SPimsFinalRunOnlyPartDelete, mtInformation, [mbOK]);
  end;
  try
    SystemDM.Pims.StartTransaction;
    if DataSet = cdsHREPRODUCTION then
    begin
      MyShiftNumber := cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger;
      SalaryHourChanged := False;
      //Delete from ProdHourPerEmployee.
      DeleteProductionRec(cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString,
      	cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString,
        cdsHREPRODUCTION.FieldByName('JOB_CODE').asString,
        cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger,
        WeekDates[WeekStartIndex],
        WeekDates[WeekEndIndex]);
// MR:22-1-2004
// Depends on 'Employee.Is_scanning_yn'
      if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
        for index := 1 to 7 do
        begin
          // RV082.2. Keep track of old sal. record.
          //          Init here.
          OldSalaryRec.HourType := -1;
          // Daily minutes
          Minutes := GetDailyMinutes(cdsHREPRODUCTION,index);
          if Minutes <> 0 then
          begin
            //Delete minutes from SalaryHourPerEmployee.
            CorrectSalaryHour(WeekDates[index],FilterData.Employee_Number,
              (-1)*Minutes,
              cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString,
              cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString,
              cdsHREPRODUCTION.FieldByName('JOB_CODE').asString,
              cdsHREPRODUCTION.FieldByName('MANUAL_YN').asString,
              //REV065.9
              CHECKEDVALUE,
              cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger);
            if FilterData.OverTimePeriod <= 1 then  //overtime per day
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
            SalaryHourChanged := True;
            // RV082.2.
            RecalcPHEPT(WeekDates[Index]);
          end;
        end;
// MR:22-1-2004
// Depends on 'Employee.Is_scanning_yn'
      if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
        if (FilterData.OverTimePeriod > 1) and SalaryHourChanged then
          RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
    end;
    // SalaryHours
    if DataSet = cdsHRESALARY then
    begin
      //Delete minutes from SalaryHours
      DeleteSalaryRec(cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').asInteger,
      	 WeekDates[WeekStartIndex], WeekDates[WeekEndIndex]);
      // RV082.2. + RV082.10.
      for Index := 1 to 7 do
        RecalcPHEPT(WeekDates[Index]);
   /// CAR 27.12.2010 - RV083.4. Bugfix.

    GlobalDM.RecalculateBalances(FilterData.Employee_Number, WeekDates[1],
      WeekDates[7]);
   /// END
// MR:22-1-2004
// Depends on 'Employee.Is_scanning_yn'
      if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
      begin
        // overtime per day
        if FilterData.OverTimePeriod <= 1 then
          for index := 1 to 7 do
          begin
            if TimeMin2IntMin(cdsHRESALARY.FieldByName(Format(TimeFieldMask+'%d',
              [index])).asDateTime) <> 0 then
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
            // RV082.2.
            RecalcPHEPT(WeekDates[Index]);
          end
        // overtime per week or period
        else
          RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
      end;
      // RV071.10.
      SaturdayCreditAbsenceTotalUpdate(DataSet, True);
      // 20013183
      TravelTimeAbsenceTotalUpdate(DataSet, True);
    end; // if DataSet = cdsHRESALARY
    if DataSet = cdsHREABSENCE then
    begin
      // delete coresponding records from salaryhoursperemployee
      AbsenceReasonCode :=
        cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').asString;
      DeleteAbsenceRec(AbsenceReasonCode,WeekDates[WeekStartIndex],WeekDates[WeekEndIndex]);
      if TableAbsenceType.Findkey([AbsenceReasonCode]) then
      	AbsenceTypeCode :=
          TableAbsenceType.FieldByName('ABSENCETYPE_CODE').asString;
      // update AbsenceTotal
      SalaryHourChanged := False;
      for index := 1 to 7 do
      begin
        Minutes := GetDailyMinutes(cdsHREABSENCE,index);
        if Minutes <> 0 then
        begin
          DecodeDate(WeekDates[index],year,month,day);
          // 200113183 Travel time. Book this as 'used travel time' in balance.
          MyAbsenceType := CharFromStr(AbsenceTypeCode,1,' ');
          if MyAbsenceType = TRAVELTIME then
            MyAbsenceType := USED_TRAVELTIME; // Used
          UpdateAbsenceTotalMode(QueryWork,FilterData.Employee_Number,year,
            ((-1)*Minutes), MyAbsenceType);
          SalaryHourChanged := True;
          // recalculate SalaryHours.
// MR:22-1-2004
// Depends on 'Employee.Is_scanning_yn'
          if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
            if (FilterData.OverTimePeriod <= 1) then
              RecalculateOvertimePeriod(MyShiftNumber, WeekDates[index]);
        end;
      end;
// MR:22-1-2004
// Depends on 'Employee.Is_scanning_yn'
      if FilterData.IsScanning or FilterData.BookProdHrs then // RV082.2.
        if (FilterData.OverTimePeriod > 1) and SalaryHourChanged then
          RecalculateOvertimePeriod(MyShiftNumber, WeekDates[1]);
    end; // if DataSet = cdsHREABSENCE then
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Rollback;
    DataSet.Cancel;
  end;
end;

procedure THoursPerEmployeeDM.CorrectSalaryHour(SalDate: TDateTime;
  EmployeeNumber, Minutes: Integer; PlantCode, WorkspotCode, JobCode,
  Manual, FromManualProd: string; Shift: Integer);
var
  HourType: integer;
  EmployeeData: TScannedIDCard;
  // RV082.2.
  function DetermineOldSalaryRecHourtypeNumber: Integer;
  begin
    Result := 1;
    if OldSalaryRec.HourType <> -1 then
    begin
      Result := OldSalaryRec.HourType;
      Exit;
    end;
    with QueryWork do
    begin
      Close;
      SQL.Clear;
      SQL.Add(
        'SELECT HOURTYPE_NUMBER ' + NL +
        'FROM SALARYHOURPEREMPLOYEE ' + NL +
        'WHERE ' + NL +
        '  SALARY_DATE = :SDATE AND ' + NL +
        '  EMPLOYEE_NUMBER = :EMPNO AND ' + NL +
        '  MANUAL_YN = :MAN ' + NL +
        'ORDER BY HOURTYPE_NUMBER '
        );
      ParamByName('SDATE').AsDateTime := SalDate;
      ParamByName('EMPNO').AsInteger := EmployeeNumber;
      ParamByName('MAN').AsString := Manual;
      Open;
      if not Eof then
      begin
        Result := FieldByName('HOURTYPE_NUMBER').AsInteger;
        OldSalaryRec.HourType := Result;
      end
      else
        OldSalaryRec.HourType := -1;
      Close;
    end;
  end;
  function DetermineWorkspotHourtypeNumber(
    ADefaultHourtypeNumber: Integer): Integer;
  begin
    Result := ADefaultHourtypeNumber;
    with QueryWork do
    begin
      Close;
      SQL.Clear;
      SQL.Add(
        'SELECT ' + NL +
        '  HOURTYPE_NUMBER ' + NL +
        'FROM ' + NL +
        '  WORKSPOT ' + NL +
        'WHERE ' + NL +
        '  PLANT_CODE = :PCODE AND ' + NL +
        '  WORKSPOT_CODE = :WCODE '
        );
      ParamByName('PCODE').AsString := PlantCode;
      ParamByName('WCODE').AsString := WorkspotCode;
      Open;
      if (not Eof) and
        (not FieldByName('HOURTYPE_NUMBER').IsNull) then
        Result := FieldByName('HOURTYPE_NUMBER').AsInteger;
      Close;
    end;
  end;
begin
  // RV082.2. This is wrong! The hourtype can also be something different!
  //          It looks only for hourtype 1 or from the workspot. But
  //          when a manual change was made to a salary-record, the
  //          hourtype can be something else!
  //          First it must look in salary-records to get the first
  //          existing salary-record (sorted on hourtype).
  HourType := DetermineOldSalaryRecHourtypeNumber;

  HourType := DetermineWorkspotHourtypeNumber(HourType);

  EmployeeData.EmployeeCode := FilterData.Employee_Number;
  EmployeeData.PlantCode := PlantCode;
  EmployeeData.WorkspotCode := WorkspotCode;
  EmployeeData.JobCode := JobCode;
  EmployeeData.ShiftNumber := Shift;
  //REV065.9
  GlobalDM.UpdateSalaryHour(SalDate, EmployeeData,
    HourType,Minutes,Manual,False,True, FromManualProd);
end;

function THoursPerEmployeeDM.GetDailyMinutes(CurrentDataSet: TDataSet;
  index: integer): integer;
var
  Minutes: integer;
begin
  Result := 0;
  if not CurrentDataSet.Active or (CurrentDataSet.IsEmpty) then
    Exit;
  Minutes := TimeMin2IntMin(CurrentDataSet.FieldByName(Format(
    TimeFieldMask+'%d', [index])).asDateTime);
  Result := 1-2*Integer(CurrentDataSet.FieldByName(Format(SignFieldMask+'%d',
    [index])).asString=UNCHECKEDVALUE);
  Result := Result * Minutes;
end;

procedure THoursPerEmployeeDM.DefaultAfterScroll(DataSet: TDataSet);
var
  index: integer;
begin
  if DataSet = cdsHREPRODUCTION then
  begin
    OldProductionRec.Plant := DataSet.FieldByName('PLANT_CODE').asString;
    OldProductionRec.Workspot := DataSet.FieldByName('WORKSPOT_CODE').asString;
    OldProductionRec.Job := DataSet.FieldByName('JOB_CODE').asString;
    OldProductionRec.Shift := DataSet.FieldByName('SHIFT_NUMBER').asInteger;
  end;
  if DataSet = cdsHRESALARY then
    OldSalaryRec.HourType := DataSet.FieldByName('HOURTYPE_NUMBER').asInteger;
  if DataSet = cdsHREABSENCE then
    OldAbsenceRec.AbsenceReason :=
      DataSet.FieldByName('ABSENCEREASON_CODE').asString;
  for index:=1 to 7 do
    OldMinutes[index] := GetDailyMinutes(DataSet,index);

  if DataSet.FieldByName('MANUAL_YN').asString = CHECKEDVALUE then
    THoursPerEmployeeF(Owner).SetActiveGridMode(gdBrowse)
  else
    THoursPerEmployeeF(Owner).SetActiveGridMode(gdReadOnly);
  // RV073.7. Disabled this part.
{
  //REV065.9 if the salary record comes from a manually entered production record then
  //its read only
  if
    Assigned(DataSet.FindField('FROMMANUALPROD_YN')) and
    (DataSet.FieldByName('FROMMANUALPROD_YN').AsString = CHECKEDVALUE)
  then
    THoursPerEmployeeF(Owner).SetActiveGridMode(gdReadOnly);
}
end;

procedure THoursPerEmployeeDM.RecalculateOvertimePeriod(AShiftNumber: Integer;
  ADate: TDateTime);
var
  EmployeeIDCard: TScannedIDCard;
  StartDate, EndDate: TDateTime; // RV062.3.
begin
  // RV082.2.
//  if FFilterData.BookProdHrs then
//    Exit;

  EmptyIDCard(EmployeeIDCard);
  PopulateAbsenceIDCard(AShiftNumber, EmployeeIDCard, 0, ADate);
{$IFDEF DEBUG}
  WDebugLog('Start HoursPerEmployee - RecalculateOvertimePeriod');
{$ENDIF}

  // RV062.3.
  // Determine Over Time Period, to ensure the complete overtime-period
  // is taken into account.
  StartDate := ADate;
  EndDate := ADAte;
  GlobalDM.ComputeOvertimePeriod(StartDate, EndDate, EmployeeIDCard);
  ADate := StartDate;
{$IFDEF DEBUG}
  WDebugLog('OverTimePeriod=' + DateTimeToStr(StartDate) + ' - ' +
    DateTimeToStr(EndDate));
{$ENDIF}

  GlobalDM.UnprocessProcessScans(QueryWork, EmployeeIDCard,
    ADate, NullDate, NullDate, NullDate, NullDate,
    True, False,
    False,
    False,
    True // 2001349 AManual: Must be TRUE to indicate it is about manual prod. hrs.!
    );
  // RV075.2. Do this at a different place!

{
  // RV045.1. Always do this!
  GlobalDM.RecalcEarnedTFTHours(EmployeeIDCard.EmployeeCode,
    EmployeeIDCard.DateIn, EmployeeIDCard.DateOut);
  // RV071.6. Also do this!
  GlobalDM.RecalcEarnedSWWHours(EmployeeIDCard.EmployeeCode,
    EmployeeIDCard.DateIn, EmployeeIDCard.DateOut);
}
{$IFDEF DEBUG}
  WDebugLog('End HoursPerEmployee - RecalculateOvertimePeriod');
{$ENDIF}
end;

// RV082.2. Delete all MANUAL production records for
//          an employee and date.
procedure THoursPerEmployeeDM.DeleteProductionManualRecs(
  AEmployeeNumber: Integer; ADate: TDateTime);
begin
  with QueryDelete do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'DELETE FROM PRODHOURPEREMPLOYEE ' +
      'WHERE ' +
      'EMPLOYEE_NUMBER = :EMPNO AND ' +
      'MANUAL_YN = :MAN AND ' +
      'PRODHOUREMPLOYEE_DATE = :PRODDATE'
      );
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    ParamByName('MAN').AsString := CHECKEDVALUE;
    ParamByName('PRODDATE').AsDateTime := ADate;
    ExecSQL;
  end;
end;

// RV082.2. Delete all MANUAL production-per-type records for
//          an employee and date.
procedure THoursPerEmployeeDM.DeleteProdPerTypeManualRecs(
  AEmployeeNumber: Integer; ADate: TDateTime);
begin
  with QueryDelete do
  begin
    Close;
    SQL.Clear;
    SQL.Add(
      'DELETE FROM PRODHOURPEREMPLPERTYPE ' +
      'WHERE ' +
      'EMPLOYEE_NUMBER = :EMPNO AND ' +
      'MANUAL_YN = :MAN AND ' +
      'PRODHOUREMPLOYEE_DATE = :PRODDATE'
      );
    ParamByName('EMPNO').AsInteger := AEmployeeNumber;
    ParamByName('MAN').AsString := CHECKEDVALUE;
    ParamByName('PRODDATE').AsDateTime := ADate;
    ExecSQL;
  end;
end;

procedure THoursPerEmployeeDM.DeleteProductionRec(Plant, Workspot, Job: String;
  Shift: Integer; StartProdDate, EndProdDate: TDateTime);
begin
  QueryDelete.Active := False;
  QueryDelete.SQL.Clear;
  QueryDelete.SQL.Add(
    'DELETE FROM PRODHOURPEREMPLOYEE ' +
    'WHERE PLANT_CODE=:PCODE AND SHIFT_NUMBER=:SHNO AND ' +
    'EMPLOYEE_NUMBER=:EMPNO AND WORKSPOT_CODE=:WCODE ' +
    'AND JOB_CODE=:JCODE AND MANUAL_YN=:MAN AND ' +
    'PRODHOUREMPLOYEE_DATE>=:DSTART AND ' +
    'PRODHOUREMPLOYEE_DATE<=:DSTOP'
    );
  QueryDelete.Prepare;
  QueryDelete.ParamByName('PCODE').asString :=Plant;
  QueryDelete.ParamByName('SHNO').asInteger :=Shift;
  QueryDelete.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryDelete.ParamByName('WCODE').asString := Workspot;
  QueryDelete.ParamByName('JCODE').asString := Job;
  QueryDelete.ParamByName('MAN').asString := CHECKEDVALUE;
  QueryDelete.ParamByName('DSTART').asDateTime := StartProdDate;
  QueryDelete.ParamByName('DSTOP').asDateTime := EndProdDate;
  QueryDelete.ExecSQL;
end;

procedure THoursPerEmployeeDM.DeleteAbsenceRec(Absencereason: string;
  StartAbsDate, EndAbsDate: TDateTime);
begin
  QueryDelete.Active := False;
  QueryDelete.SQL.Clear;
  QueryDelete.SQL.Add(
    'DELETE FROM ABSENCEHOURPEREMPLOYEE ' +
    'WHERE EMPLOYEE_NUMBER=:EMPNO AND ' +
    'ABSENCEREASON_ID=:AID AND MANUAL_YN=:MAN AND ' +
    'ABSENCEHOUR_DATE>=:DSTART AND ' +
    'ABSENCEHOUR_DATE<=:DSTOP'
    );
  QueryDelete.Prepare;
  QueryDelete.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryDelete.ParamByName('AID').asInteger :=
    SearchAbsenceReasonID(AQuery, Absencereason);
  QueryDelete.ParamByName('MAN').asString := CHECKEDVALUE;
  QueryDelete.ParamByName('DSTART').asDateTime := StartAbsDate;
  QueryDelete.ParamByName('DSTOP').asDateTime := EndAbsDate;
  QueryDelete.ExecSQL;
end;

procedure THoursPerEmployeeDM.DeleteSalaryRec(HourType: integer;
  StartSalDate, EndSalDate: TDateTime);
{var
  SalDate: TDateTime; }
begin
  QueryDelete.Active := False;
  QueryDelete.SQL.Clear;
  QueryDelete.SQL.Add(
    'DELETE FROM SALARYHOURPEREMPLOYEE ' +
    'WHERE EMPLOYEE_NUMBER=:EMPNO AND ' +
    'HOURTYPE_NUMBER=:HNO AND MANUAL_YN=:MAN AND ' +
    'SALARY_DATE>=:DSTART AND SALARY_DATE<=:DSTOP'
    );
  QueryDelete.Prepare;
  QueryDelete.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryDelete.ParamByName('HNO').asInteger := HourType;
  QueryDelete.ParamByName('MAN').asString := CHECKEDVALUE;
  QueryDelete.ParamByName('DSTART').asDateTime := StartSalDate;
  QueryDelete.ParamByName('DSTOP').asDateTime := EndSalDate;
  QueryDelete.ExecSQL;
  // RV082.3. Disabled.
{
  // RV062.4.
  SalDate := StartSalDate;
  while SalDate <= EndSalDate do
  begin
    UpdateSalaryHour2(SalDate, FilterData.Employee_Number,
      HourType, 0, 0, CHECKEDVALUE);
    SalDate := SalDate + 1;
  end;
}  
end;

procedure THoursPerEmployeeDM.InsertAbsenceMinutes(Minutes: integer;
  AbsDate: TDateTime; AbsenceReason: string);
begin
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'INSERT INTO ABSENCEHOURPEREMPLOYEE( ' +
    'ABSENCEHOUR_DATE,EMPLOYEE_NUMBER,ABSENCEREASON_ID, ' +
    'PLANT_CODE,MANUAL_YN,SHIFT_NUMBER,ABSENCE_MINUTE, ' +
    'CREATIONDATE,MUTATIONDATE,MUTATOR) ' +
    'VALUES(:MYDATE,:EMPNO,:AID,:PCODE,:MAN,:SHNO, ' +
    ':MIN,:CDATE,:CDATE,:MUTATOR)'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('MYDATE').asDateTime := AbsDate;
  QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
  QueryWork.ParamByName('AID').asInteger :=
    SearchAbsenceReasonID(AQuery, AbsenceReason);
// MRA:12 FEB 2008 REV003:
// Use Plant_code from QueryEmployee instead of TableShift.
  QueryWork.ParamByName('PCODE').asString :=
    cdsFilterEmployee.FieldByName('PLANT_CODE').AsString;
//    TableShift.FieldByName('PLANT_CODE').asString;
  QueryWork.ParamByName('MAN').asString := CHECKEDVALUE;
// MRA:12 FEB 2008 REV003:
// Use Shift_number from QueryEmployee instead of TableShift.
  QueryWork.ParamByName('SHNO').asInteger :=
    cdsFilterEmployee.FieldByName('SHIFT_NUMBER').AsInteger;
//    TableShift.FieldByName('SHIFT_NUMBER').asInteger;
  QueryWork.ParamByName('MIN').asInteger := Minutes;
  QueryWork.ParamByName('CDATE').asDateTime := now;
  QueryWork.ParamByName('MUTATOR').asString := SystemDM.CurrentProgramUser;
  QueryWork.ExecSQL;
end;

procedure THoursPerEmployeeDM.InsertProductionMinutes(ProdDate: TDateTime;
  Plant, WorkSpot, Job: String; Shift, Minutes: integer);
begin
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'INSERT INTO PRODHOURPEREMPLOYEE( ' +
    'PRODHOUREMPLOYEE_DATE,PLANT_CODE,SHIFT_NUMBER, ' +
    'EMPLOYEE_NUMBER,WORKSPOT_CODE,JOB_CODE, ' +
    'PRODUCTION_MINUTE,PAYED_BREAK_MINUTE,MANUAL_YN, ' +
    'CREATIONDATE,MUTATIONDATE,MUTATOR) ' +
    'VALUES(:DAYDATE,:PCODE,:SHNO,:EMPNO,:WCODE,:JCODE, ' +
    ':PRODMIN,:PBMIN,:MN,:CDATE,:MDATE,:M)'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('DAYDATE').asDateTime := ProdDate;
  QueryWork.ParamByName('PCODE').asString := Plant;
  QueryWork.ParamByName('SHNO').asInteger := Shift;
  QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number ;
  QueryWork.ParamByName('WCODE').asString := WorkSpot;
  QueryWork.ParamByName('JCODE').asString := Job;
  QueryWork.ParamByName('PRODMIN').asInteger := Minutes;
  QueryWork.ParamByName('PBMIN').asInteger := 0; // MR:7-12-2004 Added
  QueryWork.ParamByName('MN').asString := CHECKEDVALUE; //Manual;
  QueryWork.ParamByName('CDATE').asDateTime := now;
  QueryWork.ParamByName('MDATE').asDateTime := now;
  QueryWork.ParamByName('M').asString := SystemDM.CurrentProgramUser;
  QueryWork.ExecSQL;
end;

procedure THoursPerEmployeeDM.InsertSalaryMinutes(SalDate: TDateTime;
  AOldMinutes, Minutes, HourType: integer);
begin
  // RV062.4.
  // RV082.3. Disabled.
{
  if not UpdateSalaryHour2(SalDate, FilterData.Employee_Number, HourType,
    AOldMinutes, Minutes, CHECKEDVALUE) then }
  begin
    try
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'INSERT INTO SALARYHOURPEREMPLOYEE( ' +
        'SALARY_DATE,EMPLOYEE_NUMBER,HOURTYPE_NUMBER, ' +
        'SALARY_MINUTE,MANUAL_YN, ' +
        'CREATIONDATE,MUTATIONDATE,EXPORTED_YN,MUTATOR) ' +
        'VALUES(:MYDATE,:EMPNO,:HNO, ' +
        ':MIN,:MAN,:CDATE,:CDATE,:EXP,:MUTATOR)'
        );
      QueryWork.Prepare;
      QueryWork.ParamByName('MYDATE').asDateTime := SalDate;
      QueryWork.ParamByName('EMPNO').asInteger := FilterData.Employee_Number;
      QueryWork.ParamByName('HNO').asInteger := HourType;
      QueryWork.ParamByName('MIN').asInteger := Minutes;
      QueryWork.ParamByName('MAN').asString := CHECKEDVALUE;
      QueryWork.ParamByName('CDATE').asDateTime := now;
      QueryWork.ParamByName('EXP').asString := UNCHECKEDVALUE;
      QueryWork.ParamByName('MUTATOR').asString := SystemDM.CurrentProgramUser;
      QueryWork.ExecSQL;
    except
      on E:Exception do
      begin
        DisplayMessage(E.Message, mtInformation, [mbOk]);
      end;
    end;
  end;
end;

procedure THoursPerEmployeeDM.DeactivateTablesEvents(Enable: boolean);
begin
  if Enable then
  begin
    cdsHREPRODUCTION.BeforeDelete := nil;
    cdsHREPRODUCTION.AfterScroll := nil;
    cdsHREPRODUCTION.BeforePost := nil;
    cdsHREPRODUCTIONWORKSPOT_CODE.OnChange := nil;
    cdsHRESALARY.BeforeDelete := nil;
    cdsHRESALARY.AfterScroll := nil;
    cdsHRESALARY.BeforePost := nil;
    cdsHREABSENCE.BeforeDelete := nil;
    cdsHREABSENCE.AfterScroll := nil;
    cdsHREABSENCE.BeforePost := nil;
  end
  else
  begin
    cdsHREPRODUCTION.BeforeDelete := DefaultBeforeDelete;
    cdsHREPRODUCTION.AfterScroll := DefaultAfterScroll;
    cdsHREPRODUCTION.BeforePost := DefaultBeforePost;
    cdsHREPRODUCTIONWORKSPOT_CODE.OnChange :=
      cdsHREPRODUCTIONWORKSPOT_CODEChange;
    cdsHRESALARY.BeforeDelete := DefaultBeforeDelete;
    cdsHRESALARY.AfterScroll := DefaultAfterScroll;
    cdsHRESalary.BeforePost := DefaultBeforePost;
    cdsHREABSENCE.BeforeDelete := DefaultBeforeDelete;
    cdsHREABSENCE.AfterScroll := DefaultAfterScroll;
    cdsHREABSENCE.BeforePost := DefaultBeforePost;
  end;
end;

procedure THoursPerEmployeeDM.SaveChanges;
begin
  if cdsHREPRODUCTION.State in [dsInsert,dsEdit] then
    cdsHREPRODUCTION.Post;
  if cdsHRESALARY.State in [dsInsert,dsEdit] then
    cdsHRESALARY.Post;
  if cdsHREABSENCE.State in [dsInsert,dsEdit] then
    cdsHREABSENCE.Post;
end;

procedure THoursPerEmployeeDM.DefaultBeforeDeleteRecord(dataSet: TDataSet);
begin
  if DataSet.FieldByName('MANUAL_YN').asString = UNCHECKEDVALUE then
  begin
    Abort;
    Exit;
  end;
  DefaultBeforeDelete(DataSet);
end;

function THoursPerEmployeeDM.FindCurrentRecord(DataSet: TDataSet): Boolean;
begin
  Result := False;
  if DataSet = cdsHREPRODUCTION then
    Result := cdsHREPRODUCTION.Locate(
      'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;SHIFT_NUMBER;MANUAL_YN',
      VarArrayOf([CurrentRecordProd.Plant,CurrentRecordProd.Workspot,
      CurrentRecordProd.Job,CurrentRecordProd.Shift,CHECKEDVALUE]),
      [loCaseInsensitive]);
  if DataSet = cdsHRESALARY then
    Result := cdsHRESALARY.Locate('HOURTYPE_NUMBER;MANUAL_YN',VarArrayOf([
      CurrentRecordSal.HourType,CHECKEDVALUE]),[loCaseInsensitive]);
  if DataSet = cdsHREABSENCE then
  begin
    cdsHREABSENCE.First;
    Result := cdsHREABSENCE.Locate('ABSENCEREASON_CODE;MANUAL_YN',
      VarArrayOf([CurrentRecordAbs.AbsenceReason,CHECKEDVALUE]),
      [loCaseInsensitive]);
  end;
end;

procedure THoursPerEmployeeDM.DataModuleDestroy(Sender: TObject);
begin
  try
    cdsFilterEmployee.Close;
    QueryWorkspotLU.Close;
    QueryShiftLU.Close;
    QueryJobLU.Close;
    if cdsHREPRODUCTION.Active then
      cdsHREPRODUCTION.EmptyDataSet;
    if cdsHRESALARY.Active then
      cdsHRESALARY.EmptyDataSet;
    if cdsHREABSENCE.Active then
      cdsHREABSENCE.EmptyDataSet;
    //CAR 550274  Team selection
    QueryEmployee.Close;
    QueryEmployee.Unprepare;
  except
    // Ignore error
  end;
  inherited;
end;

procedure THoursPerEmployeeDM.cdsHREPRODUCTIONNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsHREPRODUCTION.FieldByName('MANUAL_YN').asString := CHECKEDVALUE;
  DefaultNewRecord(DataSet);
  if TablePlant.RecordCount = 1 then
    cdsHREPRODUCTION.FieldByName('PLANT_CODE').asString :=
      TablePlant.FieldByName('PLANT_CODE').asString;
end;

procedure THoursPerEmployeeDM.cdsHREPRODUCTIONWORKSPOT_CODEChange(
  Sender: TField);
begin
  inherited;
  if not QueryJobLU.IsEmpty then
  begin
    cdsHREPRODUCTION.FieldByName('JOB_CODE').asString := NullStr;
    if QueryJobLU.RecordCount = 1 then
      cdsHREPRODUCTION.FieldByName('JOB_CODE').asString :=
        QueryJobLU.FieldByName('JOB_CODE').asString;
  end
  else
    cdsHREPRODUCTION.FieldBYName('JOB_CODE').asString := DUMMYSTR;
end;

procedure THoursPerEmployeeDM.cdsHREPRODUCTIONPLANT_CODEChange(
  Sender: TField);
begin
  inherited;
  if QueryShiftLU.RecordCount = 1 then
    cdsHREPRODUCTION.FieldByName('SHIFT_NUMBER').asInteger :=
      QueryShiftLU.FieldByName('SHIFT_NUMBER').asInteger;
  if QueryWorkspotLU.RecordCount = 1 then
    cdsHREPRODUCTION.FieldByName('WORKSPOT_CODE').asString :=
      QueryWorkSpotLU.FieldByName('WORKSPOT_CODE').asString;
end;

procedure THoursPerEmployeeDM.cdsHRESALARYNewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsHRESALARY.FieldByName('MANUAL_YN').asString := CHECKEDVALUE;
  if TableDetail.RecordCount = 1 then
    cdsHRESALARY.FieldByName('HOURTYPE_NUMBER').asInteger :=
      TableDetail.FieldByName('HOURTYPE_NUMBER').asInteger;
  DefaultNewRecord(DataSet);
end;

procedure THoursPerEmployeeDM.cdsHREABSENCENewRecord(DataSet: TDataSet);
begin
  inherited;
  cdsHREABSENCE.FieldByName('MANUAL_YN').AsString := CHECKEDVALUE;
  if QueryAbsenceReason.RecordCount = 1 then
  begin
    cdsHREABSENCE.FieldByName('ABSENCEREASON_CODE').asString :=
      QueryAbsenceReason.FieldByName('ABSENCEREASON_CODE').asString;
  end;
  DefaultNewRecord(DataSet);
end;

// RV079.4. Not needed anymore.
(*
// MR:09-08-2004 Order 550333
function THoursPerEmployeeDM.ComputeMaxSalaryBalanceMinutes: String;
var
  Len: Integer;
begin
  Result := IntMin2StringTime(0, True);
  with QueryMaxSalaryBalance do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      FilterData.Employee_Number;
    Open;
    if not IsEmpty then
    begin
      First;
      // MR:04-01-2005
      // If value is negative the length should be 3, otherwise it should
      // be 4! Otherwise the values will be converted wrong.
      if FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger < 0 then
        Len := 3
      else
        Len := 4;
      Result := IntMin2StrDigTime(
        FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger, Len, True);
    end;
    Close;
  end;
end;
*)

// MR:26-11-2004
procedure THoursPerEmployeeDM.UpdateMaxSalaryBalanceMinutes(
  MinutesText: String);
var
  Minutes, Index: Integer;
  Temp: String;
  Exists: Boolean;
begin
  Temp := MinutesText;
  for Index := 1 to Length(Temp) do
    if Temp[Index]=' ' then Temp[Index] := '0';
  Minutes := StrTime2IntMin(Temp);
  with QueryMaxSalaryBalance do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      FilterData.Employee_Number;
    Open;
    Exists := not IsEmpty;
    Close;
  end;
  if not Exists then
  begin
    with QueryMaxSalaryBalanceInsert do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := FilterData.Employee_Number;
      ParamByName('MAX_SALARY_BALANCE_MINUTES').AsInteger := Minutes;
      ParamByName('CREATIONDATE').AsDateTime := Now;
      ParamByName('MUTATIONDATE').AsDateTime := Now;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
    end;
  end
  else
  begin
    with QueryMaxSalaryBalanceUpdate do
    begin
      Close;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := FilterData.Employee_Number;
      ParamByName('MAX_SALARY_BALANCE_MINUTES').AsInteger := Minutes;
      ParamByName('MUTATIONDATE').AsDateTime := Now;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
    end;
  end;
end;

// RV082.3. By making restrictions on 'Edit Manual Salary Hours', this
//          function is not needed anymore.
(*
// RV062.4.
function THoursPerEmployeeDM.UpdateSalaryHour2(ASalaryDate: TDateTime;
  AEmployeeNumber, AHourTypeNumber, AOldMinutes, AMinutes: Integer;
  AManual: String): Boolean;
var
  EmployeeData: TScannedIDCard;
//  ProdMin: Integer; // RV082.2.
  PHEFound: Boolean;
  // RV082.2. Sync PHE with PHEPT table.
  //          When manual salary-hours are made, it should also
  //          create PHE-records and PHEPT-records, and they must be in sync!
  procedure SyncPHE_PHEPT;
  var
    PHEMin, PHEPTMin: Integer;
  begin
    // Only look for Manual records!
    PHEMin := 0;
    PHEPTMin := 0;
    with qryPHE do
    begin
      // Determine the total PHE-minutes for all manual records for that day
      // and employee.
      Close;
      ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := ASalaryDate;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('MANUAL_YN').AsString := CHECKEDVALUE;
      Open;
      if not Eof then
        PHEMin := FieldByName('PRODMIN').AsInteger;
      Close;
    end;
    with qryPHEPT do
    begin
      // Determine the total PHEPT-minutes for all manual records for that day
      // and employee
      Close;
      ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := ASalaryDate;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
      ParamByName('MANUAL_YN').AsString := CHECKEDVALUE;
      Open;
      if not Eof then
        PHEPTMin := FieldByName('PRODMIN').AsInteger;
      Close;
    end;
    // Is there a difference?
    if PHEMin <> PHEPTMin then
    begin
      // Delete ALL manual PHE-records for that day and employee!
      DeleteProductionManualRecs(EmployeeData.EmployeeCode, ASalaryDate);
      // Now insert 1 manual record with the correct minutes for that day.
      if (PHEPTMin <> 0) then
        InsertProductionMinutes(ASalaryDate,
          EmployeeData.PlantCode, EmployeeData.WorkspotCode,
          EmployeeData.JobCode, EmployeeData.ShiftNumber,
          PHEPTMin)
      else
        // Sum of PHEPT-minutes was 0:
        // Delete ALL manual PHEPT-records for that day and employee!
        // Note: This is needed to get rid of records where the sum is 0.
        DeleteProdPerTypeManualRecs(EmployeeData.EmployeeCode, ASalaryDate);
    end;
  end;
begin
  Result := True;
  try
    EmployeeData.EmployeeCode := AEmployeeNumber;
    // RV076.2.
    // First look if there are existing Prodhour-records for that
    // date en employee. If found, then take fields from the first found
    // record.
    PHEFound := False;
    with qryPHEByEmpDate do
    begin
      Close;
      ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := ASalaryDate;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
      Open;
      if not Eof then
      begin
        EmployeeData.PlantCode := FieldByName('PLANT_CODE').AsString;
        EmployeeData.WorkspotCode := FieldByName('WORKSPOT_CODE').AsString;
        EmployeeData.JobCode := FieldByName('JOB_CODE').AsString;
        EmployeeData.ShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
        PHEFound := True;
      end;
      Close;
    end;
    // RV076.2.
    // If not Prodhour-record was found, then search for defaults.
    if not PHEFound then
    begin
      // First determine plant/workspot/jobcode/shift
      with qryWorkspotJobByEmp do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        Open;
        if not Eof then
        begin
          EmployeeData.PlantCode := FieldByName('PLANT_CODE').AsString;
          EmployeeData.WorkspotCode := FieldByName('WORKSPOT_CODE').AsString;
          EmployeeData.JobCode := FieldByName('JOB_CODE').AsString;
          EmployeeData.ShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
        end;
        Close;
      end;
    end;
    // Always delete the PHPEPT-record first
    if Sync3Tables then
    begin
      with GlobalDM.qryProdHourPEPTDeleteOneRec do
      begin
        try
          Close;
          ParamByName('PDATE').AsDateTime := ASalaryDate;
          ParamByName('PCODE').AsString := EmployeeData.PlantCode;
          ParamByName('SHNO').AsInteger := EmployeeData.ShiftNumber;
          ParamByName('EMPNO').AsInteger := EmployeeData.EmployeeCode;
          ParamByName('WCODE').AsString := EmployeeData.WorkSpotCode;
          ParamByName('JCODE').AsString := EmployeeData.JobCode;
          ParamByName('MAN').AsString := AManual;
          ParamByName('HOURTYPE').AsInteger := AHourTypeNumber;
          ExecSQL;
          Close;
        except
          // Ignore when record did not exist.
        end;
      end;
    end;
    // RV075.4. Update PHEPT here.
    if Sync3Tables then
    begin
      // RV082.2. This is WRONG! It gives wrong results in PHEPT-table!
{
      ProdMin := 0;
      with qryPHE do
      begin
        Close;
        ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := ASalaryDate;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeData.EmployeeCode;
        ParamByName('MANUAL_YN').AsString := AManual;
        Open;
        if not Eof then
          ProdMin := FieldByName('PRODMIN').AsInteger;
        Close;
      end;
}
      // RV075.4. Only update PHEPT here.
      GlobalDM.UpdatePHEPT(EmployeeData, ASalaryDate, AHourTypeNumber,
        AMinutes {- ProdMin}, AManual, True);
    end;

    // RV075.4. Do not update PHEPT here and do NOT round here!
    GlobalDM.UpdateSalaryHour(ASalaryDate, EmployeeData,
      AHourTypeNumber, AMinutes, AManual, False, False, 'N', False);

    // RV082.2.
    if Sync3Tables and
      SystemDM.IsRFL and FFilterData.BookProdHrs then
      SyncPHE_PHEPT;
  except
    Result := False;
  end;
end; // UpdateSalaryHour2
*)

// RV079.4. Not needed anymore.
(*
function THoursPerEmployeeDM.GetCounterActivated(
  AAbsenceType: Char; var ACounterDescription: String): Boolean;
var
  CountryId: Integer;
  StrValue: String;
begin
  CountryId := SystemDM.GetDBValue(
  ' SELECT NVL(P.COUNTRY_ID, 0) FROM ' +
  ' EMPLOYEE E, PLANT P ' +
  ' WHERE ' +
  '   E.PLANT_CODE = P.PLANT_CODE AND ' +
  '   E.EMPLOYEE_NUMBER = ' +
  IntToStr(FilterData.Employee_Number), 0
  );

  Result := SystemDM.GetCounterActivated(CountryId, AAbsenceType);

  if Result then
  begin
    StrValue := SystemDM.GetDBValue(
      Format(
      'SELECT AC.COUNTER_DESCRIPTION FROM ABSENCETYPEPERCOUNTRY AC ' +
      'WHERE COUNTRY_ID = %d AND ABSENCETYPE_CODE = ''%s''',
      [CountryId, AAbsenceType]
      ), '*'
    );
    if StrValue <> '*' then
      ACounterDescription := StrValue;
  end;
end;
*)

procedure THoursPerEmployeeDM.UpdateHourTypePerCountryLU(CountryId: Integer);
begin
  // RV071.13. 550497 Bugfixing.
  QueryHourTypeLU.Close;
  QueryHourTypeLU.Sql.Text :=
    'SELECT H.*, NVL(' +
    ' (SELECT HC.DESCRIPTION FROM  HOURTYPEPERCOUNTRY HC ' +
    '  WHERE HC.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' +
    '  AND HC.COUNTRY_ID =' + IntToStr(CountryId) +
    '  ), H.DESCRIPTION) HDESCRIPTION ' +
    'FROM HOURTYPE H ' +
    'ORDER BY H.HOURTYPE_NUMBER';
  QueryHourTypeLU.Open;
end;

// RV079.4. Not needed anymore.
(*
// RV067.MRA.29 Are there any absencetype-per-country defined?
function THoursPerEmployeeDM.AbsenceTypePerCountryExist: Boolean;
var
  CountryId: Integer;
begin
  Result := False;
  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(FilterData.Employee_Number), 0
  );
  if CountryID <> 0 then
  begin
    with qryAbsenceTypePerCountryExist do
    begin
      Close;
      ParamByName('COUNTRY_ID').AsInteger :=
        CountryID;
      Open;
      Result := not Eof;
      Close;
    end;
  end;
end;
*)

// RV079.4. Not needed anymore.
(*
// RV071.7. Get date based on current selection in dialog.
// RV071.15. Changed back to 'Now'.
function THoursPerEmployeeDM.MyDateFrom: TDateTime;
begin
  Result := Now;
{
  Result := ListProcsF.DateFromWeek(FilterData.Year, FilterData.Week, 7);
  Result := Result + 1; // Add 1 day to get AFTER the week.
}
end;
*)

// RV071.10. 550497.
// 20013183 Restructured so it can be used for more fields to check.
function THoursPerEmployeeDM.SaturdayCreditHourtypeCheck(
  ADataSet: TDataSet): Boolean;
begin
  Result := HourtypeCheck('SATURDAY_CREDIT_YN', ADataSet);
{
  Result := False;
  qryHourtypeCheck.Close;
  qryHourtypeCheck.ParamByName('HOURTYPE_NUMBER').AsInteger :=
    DataSet.FieldByName('HOURTYPE_NUMBER').AsInteger;
  qryHourtypeCheck.Open;
  if not qryHourtypeCheck.Eof then
     if qryHourtypeCheck.FieldByName('SATURDAY_CREDIT_YN').AsString =
       CHECKEDVALUE then
       Result := True;
  qryHourtypeCheck.Close;
}
end;

// RV071.10. 550497.
procedure THoursPerEmployeeDM.SaturdayCreditAbsenceTotalUpdate(
  DataSet: TDataSet; DeleteAction: Boolean=False);
var
  OldSalMinutes, NewSalMinutes, Index: Integer;
begin
  if SaturdayCreditHourtypeCheck(DataSet) then
  begin
    OldSalMinutes := 0;
    NewSalMinutes := 0;
    for Index := 1 to 7 do
    begin
      OldSalMinutes := OldSalMinutes + OldMinutes[Index];
      NewSalMinutes := NewSalMinutes + GetDailyMinutes(cdsHRESALARY, Index);
    end;
    // Update Absence Total for Earned Saturday Credit Minutes.
    // First book off old minutes.
    UpdateAbsenceTotalMode(QueryWork, FilterData.Employee_Number,
      FilterData.year, ((-1) * OldSalMinutes), 'Y');
    if not DeleteAction then
      // Then book new minutes.
      UpdateAbsenceTotalMode(QueryWork, FilterData.Employee_Number,
        FilterData.year, NewSalMinutes, 'Y');
  end;
end;

// RV071.13. 550497. These are used by external sources (DialogExportPayroll).
function THoursPerEmployeeDM.GetFilterDataEmployee: Integer;
begin
  Result := FFilterData.Employee_Number;
end;

procedure THoursPerEmployeeDM.SetFilterDataEmployee(const Value: Integer);
begin
  FFilterData.Employee_Number := Value;
end;

function THoursPerEmployeeDM.GetFilterDataYear: Integer;
begin
  Result := FFilterData.Year;
end;

procedure THoursPerEmployeeDM.SetFilterDataYear(const Value: Integer);
begin
  FFilterData.Year := Value;
end;

function THoursPerEmployeeDM.GetFilterDataWeek: Integer;
begin
  Result := FFilterData.Week;
end;

procedure THoursPerEmployeeDM.SetFilterDataWeek(const Value: Integer);
begin
  FFilterData.Week := Value;
end;

// RV073.3.
procedure THoursPerEmployeeDM.TableWorkspotFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantDeptTeamFilter(DataSet,
    'PLANT_CODE', 'DEPARTMENT_CODE');
end;

procedure THoursPerEmployeeDM.cdsHREPRODUCTIONBeforeEdit(
  DataSet: TDataSet);
begin
  inherited;
  if not DataSet.CanModify then
    DisplayMessage('Edit is not allowed!', mtInformation, [mbOk]);
end;

// RV082.2. Recalc PHEPT records based on PHE and SHE records.
// TD-21132 NOTE:
// This only recalculates PHEPT based on PHE records that are manual!
// Not when salary hours are manually changed!
// This routine only works for RFL and for Sync3Tables.
procedure THoursPerEmployeeDM.RecalcPHEPT(AProdDate: TDateTime);
var
  EmpData: TScannedIDCard;
begin
  if not SystemDM.IsRFL then
    Exit;
  if not Sync3Tables then
    Exit;

  try
    // First delete existing PHEPT-records.
    DeleteProdPerTypeManualRecs(FilterData.Employee_Number, AProdDate);
    // Now recalculate them based on PHE and SHE records.
    with qryPHESHE do
    begin
      Close;
      ParamByName('PRODHOUREMPLOYEE_DATE').AsDate := AProdDate;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := FilterData.Employee_Number;
      ParamByName('MANUAL_YN').AsString := CHECKEDVALUE;
      Open;
      while not Eof do
      begin
        // Insert PHEPT-record based on PHE and SHE
        EmpData.EmployeeCode := FieldByName('EMPLOYEE_NUMBER').AsInteger;
        EmpData.ShiftNumber := FieldByName('SHIFT_NUMBER').AsInteger;
        EmpData.WorkSpotCode := FieldByName('WORKSPOT_CODE').AsString;
        EmpData.JobCode := FieldByName('JOB_CODE').AsString;
        EmpData.PlantCode := FieldByName('PLANT_CODE').AsString;
        GlobalDM.UpdatePHEPT(EmpData,
          AProdDate,
          FieldByName('HOURTYPE_NUMBER').AsInteger,
          FieldByName('PRODUCTION_MINUTE').AsInteger,
          CHECKEDVALUE,
          True);
        Next;
      end;
      Close;
    end;
  except
    // Ignore error.
  end;
end; // RecalcPHEPT

// 20013183 This check for a certain field.
function THoursPerEmployeeDM.HourtypeCheck(AField: String;
  ADataSet: TDataSet): Boolean;
begin
  Result := False;
  qryHourtypeCheck.Close;
  qryHourtypeCheck.ParamByName('HOURTYPE_NUMBER').AsInteger :=
    ADataSet.FieldByName('HOURTYPE_NUMBER').AsInteger;
  qryHourtypeCheck.Open;
  if not qryHourtypeCheck.Eof then
     if qryHourtypeCheck.FieldByName(AField).AsString =
       CHECKEDVALUE then
       Result := True;
  qryHourtypeCheck.Close;
end;

function THoursPerEmployeeDM.TravelTimeHourtypeCheck(
  ADataSet: TDataSet): Boolean;
begin
  Result := HourtypeCheck('TRAVELTIME_YN', ADataSet);
end;

// 20013183 Update the 'earned travel time minute'  in balance-table,
// when salary is added/updated or deleted.
procedure THoursPerEmployeeDM.TravelTimeAbsenceTotalUpdate(
  DataSet: TDataSet; DeleteAction: Boolean);
var
  OldSalMinutes, NewSalMinutes, Index: Integer;
begin
  if TravelTimeHourtypeCheck(DataSet) then
  begin
    OldSalMinutes := 0;
    NewSalMinutes := 0;
    for Index := 1 to 7 do
    begin
      OldSalMinutes := OldSalMinutes + OldMinutes[Index];
      NewSalMinutes := NewSalMinutes + GetDailyMinutes(cdsHRESALARY, Index);
    end;
    // Update Absence Total for Travel Time Minutes.
    // First book off old minutes.
    UpdateAbsenceTotalMode(QueryWork, FilterData.Employee_Number,
      FilterData.year, ((-1) * OldSalMinutes), TRAVELTIME);
    if not DeleteAction then
      // Then book new minutes.
      UpdateAbsenceTotalMode(QueryWork, FilterData.Employee_Number,
        FilterData.year, NewSalMinutes, TRAVELTIME);
  end;
end;

// 20013035
function THoursPerEmployeeDM.IsEmployeeActive(ADataSet: TDataSet; ADate: TDateTime): Boolean;
begin
  if ADataSet.FieldByName('ENDDATE').AsString <> '' then
  begin
    if (ADate >= ADataSet.FieldByName('STARTDATE').AsDateTime) and
      (ADate <= ADataSet.FieldByName('ENDDATE').AsDateTime) then
      Result := True
    else
      // GLOB3-253 Is contract starting or ending in selected week?
      if
        (
          (ADataSet.FieldByName('STARTDATE').AsDateTime >= ADate) and
          (ADataSet.FieldByName('STARTDATE').AsDateTime <= ADate+6)
        )
        or
        (
          (ADataSet.FieldByName('ENDDATE').AsDateTime >= ADate) and
          (ADataSet.FieldByName('ENDDATE').AsDateTime <= ADate+6)
        ) then
          Result := True
        else
          Result := False;
  end
  else
  begin
    if (ADate >= ADataSet.FieldByName('STARTDATE').AsDateTime) then
      Result := True
    else
      // GLOB3-253 Is Contract starting in selected week?
      if
        (
          (ADataSet.FieldByName('STARTDATE').AsDateTime >= ADate) and
          (ADataSet.FieldByName('STARTDATE').AsDateTime <= ADate+6)
        )
      then
        Result := True
      else
        Result := False;
  end;
end;

// 20013035
procedure THoursPerEmployeeDM.ShowOnlyActiveSwitch;
begin
  cdsFilterEmployee.Refresh;
end;

// 20013035
procedure THoursPerEmployeeDM.cdsFilterEmployeeFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  if ShowOnlyActive then
    Accept := IsEmployeeActive(DataSet, ListProcsF.DateFromWeek(FilterDataYear,
        FilterDataWeek,1))
  else
    Accept := True;
end;

end.

