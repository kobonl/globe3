(*
  MRA:15-JUL-2015 PIM-53
  - Add Holiday functionality
  - Added cdsAbsRsn to search for absence-reason when entered by keyboard
*)
unit DialogAddHolidayDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseDMT, SystemDMT, Db, DBTables, DBClient, Provider;

type
  TDialogAddHolidayDM = class(TDialogBaseDM)
    dsrcEmployee: TDataSource;
    qryShift: TQuery;
    tblEmployee: TTable;
    tblEmployeeEMPLOYEE_NUMBER: TIntegerField;
    tblEmployeePLANT_CODE: TStringField;
    tblEmployeeSHIFT_NUMBER: TIntegerField;
    tblEmployeeSHIFTLU: TStringField;
    tblEmployeeDESCRIPTION: TStringField;
    qryAbsReason: TQuery;
    qryPlant: TQuery;
    tblEmployeeCOUNTRY_ID: TIntegerField;
    qryAbsRsn: TQuery;
    dspAbsRsn: TDataSetProvider;
    cdsAbsRsn: TClientDataSet;
  private
    { Private declarations }
    FEmployeeNumber: Integer;
  public
    { Public declarations }
    procedure Init;
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
  end;

var
  DialogAddHolidayDM: TDialogAddHolidayDM;

implementation

{$R *.DFM}

{ TDialogAddHolidayDM }

procedure TDialogAddHolidayDM.Init;
begin
  tblEmployee.Locate('EMPLOYEE_NUMBER', EmployeeNumber, []);
  qryPlant.Open;
  qryShift.Open;
end;

end.
