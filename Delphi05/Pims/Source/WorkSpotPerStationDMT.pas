(*
  MRA:21-OCT-2010 RV073.4. Addition.
  - When adding new workspot, the sequence-number must always be entered.
    Changed so it will automatically set the sequence-number to the
    next number.
*)
unit WorkSpotPerStationDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TWorkSpotPerStationDM = class(TGridBaseDM)
    TableDetailCOMPUTER_NAME: TStringField;
    TableDetailSEQUENCE_NUMBER: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailWORKSPOT_CODE: TStringField;
    TableDetailMUTATOR: TStringField;
    TableMasterCOMPUTER_NAME: TStringField;
    TableMasterLICENSEVERSION: TIntegerField;
    TableDetailPLANTLU: TStringField;
    TablePlant: TTable;
    TableWorkspot: TTable;
    DataSourcePlant: TDataSource;
    DataSourceWorkspot: TDataSource;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableWorkspotPLANT_CODE: TStringField;
    TableWorkspotDESCRIPTION: TStringField;
    TableWorkspotWORKSPOT_CODE: TStringField;
    TableWorkspotCREATIONDATE: TDateTimeField;
    TableWorkspotDEPARTMENT_CODE: TStringField;
    TableWorkspotMUTATIONDATE: TDateTimeField;
    TableWorkspotMUTATOR: TStringField;
    TableWorkspotUSE_JOBCODE_YN: TStringField;
    TableWorkspotHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotQUANT_PIECE_YN: TStringField;
    TableWorkspotAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotCOUNTER_VALUE_YN: TStringField;
    TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotDATE_INACTIVE: TDateTimeField;
    TableWorkspotDsc: TTable;
    TableWorkspotDscPLANT_CODE: TStringField;
    TableWorkspotDscDESCRIPTION: TStringField;
    TableWorkspotDscWORKSPOT_CODE: TStringField;
    TableWorkspotDscCREATIONDATE: TDateTimeField;
    TableWorkspotDscDEPARTMENT_CODE: TStringField;
    TableWorkspotDscMUTATIONDATE: TDateTimeField;
    TableWorkspotDscMUTATOR: TStringField;
    TableWorkspotDscUSE_JOBCODE_YN: TStringField;
    TableWorkspotDscHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotDscMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotDscPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotDscQUANT_PIECE_YN: TStringField;
    TableWorkspotDscAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotDscCOUNTER_VALUE_YN: TStringField;
    TableWorkspotDscENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotDscDATE_INACTIVE: TDateTimeField;
    TableDetailWORKSPOTCAL: TStringField;
    TableTmpPlant: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StoredProcWork: TStoredProc;
    TableWorkStation: TTable;
    DataSourceWorkStation: TDataSource;
    TableWorkStationCOMPUTER_NAME: TStringField;
    TableWorkStationLICENSEVERSION: TIntegerField;
    TableWorkStationTIMERECORDING_YN: TStringField;
    TableWorkspotLU: TTable;
    TableWorkspotLUPLANT_CODE: TStringField;
    TableWorkspotLUDESCRIPTION: TStringField;
    TableWorkspotLUWORKSPOT_CODE: TStringField;
    TableWorkspotLUCREATIONDATE: TDateTimeField;
    TableWorkspotLUDEPARTMENT_CODE: TStringField;
    TableWorkspotLUMUTATIONDATE: TDateTimeField;
    TableWorkspotLUMUTATOR: TStringField;
    TableWorkspotLUUSE_JOBCODE_YN: TStringField;
    TableWorkspotLUHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotLUMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotLUPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotLUQUANT_PIECE_YN: TStringField;
    TableWorkspotLUAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotLUCOUNTER_VALUE_YN: TStringField;
    TableWorkspotLUENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotLUDATE_INACTIVE: TDateTimeField;
    TableWorkspotLUAUTOMATIC_RESCAN_YN: TStringField;
    TableWorkspotLUSHORT_NAME: TStringField;
    TableWorkspotLUGRADE: TIntegerField;
    TableDetailWORKSPOTLU: TStringField;
    qryWSPerWSCount: TQuery;
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableExportCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WorkSpotPerStationDM: TWorkSpotPerStationDM;

implementation

uses SystemDMT, UPimsConst;
{$R *.DFM}

procedure TWorkSpotPerStationDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if DataSet.State in [dsEdit, dsInsert] then
    Exit;
  if not TableWorkspotDsc.Active then
    TableWorkspotDsc.Active := True;
    if (TableDetail.FieldByName('WORKSPOT_CODE').asString <> '') then
      if (TableWorkspotDsc.FindKey([TableDetail.FieldByName('PLANT_CODE').asString,
        TableDetail.FieldByName('WORKSPOT_CODE').asString])) then
          TableDetailWORKSPOTCAL.Value :=
            TableWorkspotDsc.FieldByName('DESCRIPTION').asString;
end;

procedure TWorkSpotPerStationDM.TableDetailNewRecord(DataSet: TDataSet);
var
  WSCount: Integer;
  function NextSequenceNumber: Integer;
  begin
    Result := 0;
    with qryWSPerWSCount do
    begin
      try
        if TableMaster.RecordCount > 0 then
        begin
          Close;
          ParamByName('COMPUTER_NAME').AsString :=
            TableMaster.FieldByName('COMPUTER_NAME').AsString;
          Open;
          Result := FieldByName('WSCOUNT').AsInteger;
        end;
      except
        Result := 0;
      end;
    end;
  end; // NextSequenceNumber
begin
  inherited;
  // RV073.4. Determine next sequence-number here.
  WSCount := NextSequenceNumber;
  inc(WSCount);

  SystemDM.DefaultNewRecord(DataSet);
  TableDetailSEQUENCE_NUMBER.Value := WSCount; // // RV073.4.
  if not TablePlant.IsEmpty then
  begin
    TablePlant.First;
    TableDetail.FieldByName('PLANT_CODE').Value :=
      TablePlant.FieldByName('PLANT_CODE').Value;
  end;
end;

procedure TWorkSpotPerStationDM.TableExportCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not TableWorkspotDsc.Active then
    TableWorkspotDsc.Active := True;
  if (TableExport.FieldByName('WORKSPOT_CODE').asString <> '') then
     if (TableWorkspotDsc.FindKey([TableExport.FieldByName('PLANT_CODE').asString,
       TableExport.FieldByName('WORKSPOT_CODE').asString])) then
       TableExport.FieldByName('WORKSPOTCAL').Value :=
         TableWorkspotDsc.FieldByName('DESCRIPTION').asString;
end;

end.
