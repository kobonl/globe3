(*
  Changes:
    MRA:10-JUN-2011. RV093.6. 20011796
    - New report Holiday Card. Shows holiday and other types in card-report.
      With totals at the end and totals of availability (when possible).
    MRA:2-SEP-2011. RV096.1. Rework
    - Report Holiday Card
      - The selection on BU is not correct: It must look for BU
        connected to department that belongs to the employee.
    MRA:28-OCT-2011. RV100.2. 20011796. Update.
    - Report Holiday Hours
      - Determine TFT-Earned based on worked hours
        booked for TFT-hourtype.
      - Show the earned TFT also in calendar! (based on salary-table)
      - Show a kind of matrix for the total/available
        hours at end of report.
    MRA:7-DEC-2011. RV103.2. SO-20011796. Addition.
    - Report Holiday Card
      - Show a matrix with absence reasons, with columns
        about 'Norm/Earned', 'Used' and 'Available'.
    MRA:10-JAN-2012. RV103.4. Bugfix.
    - Some changes made for texts.
    - Compare date with year, month, day from query, instead
      of using 'ADateFtm', because that gives a problem
      when the windows-date-time-settings are for
      example 'yyyy-mm-dd'.
    MRA:11-JAN-2012. RV103.4. Changes.
    - Some fonts are made smaller.
    - First column (for month) is made smaller.
    - Month-description is truncated to first 3 positions.
    MRA:12-JAN-2012. RV103.4. Changes.
    - Divide matrix in 2 parts at the bottom.
    MRA:16-JAN-2012. RV103.4. Changes.
    - Changes to get more on 1 page.
    MRA:15-FEB-2012. 2.0.161.214.1. 20011796.2. Changes.
    - When only Holiday and TFT (H + T) is selected:
      - Change headers for these 2.
      - Add a legenda (on 1 line).
      - Add 'total available' at the right of shown
        Holiday + TFT, which is the total available for these 2.
      - Based on example from Floron.
    - Note: Legenda + Total available is always shown.
    - Bugfix: It did not export more than 1 value per day.
    - Bugfix: When multiple absence reasons exist for time-for-time,
              resulting in mulitple lines for totals, it went wrong:
              - For each line it calculated TFT-Earned again. This
                must be done only once!
    - Small change: Exclude employees who are out-of-service.
  MRA:29-MAR-2012. 20011796.3. Rework.
  - Employee has quit:
    - Rule 1:
      - When employee has emp-end-date on or before date-to of
        selection: Do not show.
      - When employee has emp-end-date after date-to of selection:
        Do show.
    - Rule 2:
      - When only 1 employee is selected (from-to-employee), then
        always show the card (rule 1 is ignored).
      - When 2 or more employees are selected, then only show
        employees according to rule 1.
  MRA:6-MAY-2013 TD-22321 Save to PDF + landscape
  - Report Holiday Card
    - This report is saved in portrait when stored
      as PDF.
    - Technical Reason:
      - The report-settings (in the source) had to be changed.
        See also the Landscape-settings in report Staff Planning Per Day.
*)

unit ReportHolidayCardQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

const
  MAXDAY=31;
  EARNED_TFT_MARKER='ET';

type
  TQRParameters = class
  private
    FBusinessUnitFrom, FBusinessUnitTo: String;
    FDepartmentFrom, FDepartmentTo: String;
    FTeamFrom, FTeamTo: String;
    FDateFrom, FDateTo: TDateTime;
    FAllTeams, FAllDepartments,
    FShowSelection: Boolean;
    FHoliday, FWorkTimeReduction,
    FTimeForTime, FBankHoliday,
    FAddBankHoliday, FRsvBankHoliday,
    FShorterWWeek, FSeniorityHoliday: Boolean;
    FHolidayDesc, FWorkTimeReductionDesc,
    FTimeForTimeDesc, FBankHolidayDesc,
    FAddBankHolidayDesc, FRsvBankHolidayDesc,
    FShorterWWeekDesc, FSeniorityHolidayDesc: String;
    FExportToFile: Boolean;
    FHolidayTFT: Boolean;
  public
    procedure SetValues(
      BusinessUnitFrom, BusinessUnitTo: String;
      DepartmentFrom, DepartmentTo: String;
      TeamFrom, TeamTo: String;
      DateFrom, DateTo: TDateTime;
      AllTeams, AllDepartments: Boolean;
      ShowSelection: Boolean;
      Holiday, WorkTimeReduction,
      TimeForTime, BankHoliday,
      AddBankHoliday, RsvBankHoliday,
      ShorterWWeek, SeniorityHoliday: Boolean;
      HolidayDesc, WorkTimeReductionDesc,
      TimeForTimeDesc, BankHolidayDesc,
      AddBankHolidayDesc, RsvBankHolidayDesc,
      ShorterWWeekDesc, SeniorityHolidayDesc: String;
      ExportToFile: Boolean);
  end;

  TIntArray20 = Array[1..20] of Integer;

  TReportHolidayCardQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGroupHDDate: TQRGroup;
    QRLabelMonth: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    ChildBandEmpl: TQRChildBand;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    QR1: TQRMemo;
    QR2: TQRMemo;
    QR3: TQRMemo;
    QR4: TQRMemo;
    QR5: TQRMemo;
    QR6: TQRMemo;
    QR7: TQRMemo;
    QR8: TQRMemo;
    QR9: TQRMemo;
    QR10: TQRMemo;
    QR11: TQRMemo;
    QR12: TQRMemo;
    QR13: TQRMemo;
    QR14: TQRMemo;
    QR15: TQRMemo;
    QR16: TQRMemo;
    QR17: TQRMemo;
    QR18: TQRMemo;
    QR19: TQRMemo;
    QR20: TQRMemo;
    QR21: TQRMemo;
    QR22: TQRMemo;
    QR23: TQRMemo;
    QR24: TQRMemo;
    QR25: TQRMemo;
    QR28: TQRMemo;
    QR29: TQRMemo;
    QR30: TQRMemo;
    QR26: TQRMemo;
    QR27: TQRMemo;
    QR31: TQRMemo;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShapeDATE: TQRShape;
    QRShape3: TQRShape;
    QRShape68: TQRShape;
    QRBandFTEmployee: TQRBand;
    QRLabel3: TQRLabel;
    QRMemo1: TQRMemo;
    QRMemo2: TQRMemo;
    QRLabel1: TQRLabel;
    QRDBText6: TQRDBText;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRBandSummary: TQRBand;
    QRShape69: TQRShape;
    QRLabel45: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel46: TQRLabel;
    QRMemo3: TQRMemo;
    QRMemo4: TQRMemo;
    QRLabel51: TQRLabel;
    QRMemo6: TQRMemo;
    QRLabel52: TQRLabel;
    QRMemo7: TQRMemo;
    QRLabel53: TQRLabel;
    QRMemo8: TQRMemo;
    QRMemo5: TQRMemo;
    QRLabel54: TQRLabel;
    QRShape70: TQRShape;
    QRShape71: TQRShape;
    QRLabelTitle: TQRLabel;
    QRLabel55: TQRLabel;
    QRShape72: TQRShape;
    QRLabelTotAvail: TQRLabel;
    procedure QRGroupHDDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QRBandFTEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFTEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndBasePageHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate, FMaxDate: TDateTime;
    FYear: Word;
    FCurYear, FPrevYear: Word;
    VQRGroupHDDateHeight: Integer;
    VMemoHeight: Integer;
    VShapeHeight: Integer;
    VQRShapeDATETop: Integer;
    VQRShape72Width: Integer;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const BusinessUnitFrom, BusinessUnitTo: String;
      const DepartmentFrom, DepartmentTo: String;
      const TeamFrom, TeamTo: String;
      const EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const AllTeams, AllDepartments: Boolean;
      const ShowSelection: Boolean;
      const Holiday, WorkTimeReduction,
      TimeForTime, BankHoliday,
      AddBankHoliday, RsvBankHoliday,
      ShorterWWeek, SeniorityHoliday: Boolean;
      const HolidayDesc, WorkTimeReductionDesc,
      TimeForTimeDesc, BankHolidayDesc,
      AddBankHolidayDesc, RsvBankHolidayDesc,
      ShorterWWeekDesc, SeniorityHolidayDesc: String;
      const ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure ClearMemoField;
    procedure FillMemoField(Day: Integer; SpaceFill: Boolean);
    function GetMonth(MonthInt: Integer): String;
    procedure SetMemoHeight(HeightMemo: Integer );
    procedure SetVertShapeHeight(HeightShape: Integer );
  end;

var
  ReportHolidayCardQR: TReportHolidayCardQR;
  ValueList: array[1..MAXDAY] of String;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAbsenceSheduleDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes, ReportHolidayCardDMT, GlobalDMT;

procedure ClearValueList;
var
  I: Integer;
begin
  for I := 1 to MAXDAY do
    ValueList[I] := '';
end;

procedure ExportDetail(Month: String);
var
  I: Integer;
  Line: String;
begin
  Line := Month + ExportClass.Sep;
  for I := 1 to MAXDAY - 1 do
    Line := Line + ValueList[I] + ExportClass.Sep;
  Line := Line + ValueList[MAXDAY];
  ExportClass.AddText(Line);
end;

procedure TQRParameters.SetValues(
  BusinessUnitFrom, BusinessUnitTo: String;
  DepartmentFrom, DepartmentTo: String;
  TeamFrom, TeamTo: String;
  DateFrom, DateTo: TDateTime;
  AllTeams, AllDepartments: Boolean;
  ShowSelection: Boolean;
  Holiday, WorkTimeReduction,
  TimeForTime, BankHoliday,
  AddBankHoliday, RsvBankHoliday,
  ShorterWWeek, SeniorityHoliday: Boolean;
  HolidayDesc, WorkTimeReductionDesc,
  TimeForTimeDesc, BankHolidayDesc,
  AddBankHolidayDesc, RsvBankHolidayDesc,
  ShorterWWeekDesc, SeniorityHolidayDesc: String;
  ExportToFile: Boolean);
begin
  FBusinessUnitFrom := BusinessUnitFrom;
  FBusinessUnitTo := BusinessUnitTo;
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FAllTeams := AllTeams;
  FAllDepartments := AllDepartments;
  FShowSelection := ShowSelection;
  FHoliday := Holiday;
  FWorkTimeReduction := WorkTimeReduction;
  FTimeForTime := TimeForTime;
  FBankHoliday := BankHoliday;
  FAddBankHoliday := AddBankHoliday;
  FRsvBankHoliday := RsvBankHoliday;
  FShorterWWeek := ShorterWWeek;
  FSeniorityHoliday := SeniorityHoliday;
  FHolidayDesc := HolidayDesc;
  FWorkTimeReductionDesc := WorkTimeReductionDesc;
  FTimeForTimeDesc := TimeForTimeDesc;
  FBankHolidayDesc := BankHolidayDesc;
  FAddBankHolidayDesc := AddBankHolidayDesc;
  FRsvBankHolidayDesc := RsvBankHolidayDesc;
  FShorterWWeekDesc := ShorterWWeekDesc;
  FSeniorityHolidayDesc := SeniorityHolidayDesc;
  FExportToFile := ExportToFile;
  // 20011796.2. Determine if only Holiday and TFT are selected.
  FHolidayTFT :=
    FHoliday and
    (not FWorkTimeReduction) and
    FTimeForTime and
    (not FBankHoliday) and
    (not FAddBankHoliday) and
    (not FRsvBankHoliday) and
    (not FShorterWWeek) and
    (not FSeniorityHoliday);
end;

function TReportHolidayCardQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const BusinessUnitFrom, BusinessUnitTo: String;
  const DepartmentFrom, DepartmentTo: String;
  const TeamFrom, TeamTo: String;
  const EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const AllTeams, AllDepartments: Boolean;
  const ShowSelection: Boolean;
  const Holiday, WorkTimeReduction,
  TimeForTime, BankHoliday,
  AddBankHoliday, RsvBankHoliday,
  ShorterWWeek, SeniorityHoliday: Boolean;
  const HolidayDesc, WorkTimeReductionDesc,
  TimeForTimeDesc, BankHolidayDesc,
  AddBankHolidayDesc, RsvBankHolidayDesc,
  ShorterWWeekDesc, SeniorityHolidayDesc: String;
  const ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      BusinessUnitFrom, BusinessUnitTo,
      DepartmentFrom, DepartmentTo,
      TeamFrom, TeamTo,
      DateFrom, DateTo,
      AllTeams, AllDepartments,
      ShowSelection,
      Holiday, WorkTimeReduction,
      TimeForTime, BankHoliday,
      AddBankHoliday, RsvBankHoliday,
      ShorterWWeek, SeniorityHoliday,
      HolidayDesc, WorkTimeReductionDesc,
      TimeForTimeDesc, BankHolidayDesc,
      AddBankHolidayDesc, RsvBankHolidayDesc,
      ShorterWWeekDesc, SeniorityHolidayDesc,
      ExportToFile);
  end;
  SetDataSetQueryReport(ReportHolidayCardDM.QueryAbsence);
end;


function TReportHolidayCardQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  Month: Word;
  FromStr, WhereStr: String;
  AbsenceStr: String;
  SelectStr2: String;
  procedure AddAbsence(AValue: String);
  begin
    if AbsenceStr = '' then
      AbsenceStr := AbsenceStr + '''' + AValue + ''''
    else
      AbsenceStr := AbsenceStr + ',' + '''' + AValue + '''';
  end;
  procedure DetermineYear;
  var
    MinYear, MinMonth, MinDay: Word;
    MaxYear, MaxMonth, MaxDay: Word;
  begin
    DecodeDate(FMinDate, MinYear, MinMonth, MinDay);
    // What is year of max?
    DecodeDate(FMaxDate, MaxYear, MaxMonth, MaxDay);
    // Some logic to decide what the current year is.
    FCurYear := MinYear;
    if MinYear <> MaxYear then
    begin
      if MaxYear - MinYear = 1 then
        FCurYear := MaxYear;
      if MaxYear - MinYear = 2 then
        FCurYear := MaxYear - 1;
    end;
    FPrevYear := FCurYear - 1;
    FYear := FCurYear;
  end;
  // 20011796.3
  function EmployeeQuitRule: String;
  begin
    Result := '';
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
      (QRBaseParameters.FEmployeeFrom = QRBaseParameters.FEmployeeTo) then
      Result := ''
    else
      Result :=
        '  AND ' + NL +
        '  ( ' + NL +
        '    (E.ENDDATE IS NULL) OR ' + NL +
        '    (E.ENDDATE > :FDATETO) ' + NL +
        '  ) ' + NL;
  end;
begin
  // NOTE: What if datefrom starts at previous year?
//  DecodeDate(QRParameters.FDateFrom, FYear, Month, Day);
  Screen.Cursor := crHourGlass;
  {open all linked datasets}
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;
  DetermineYear;
  SelectStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE, ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE, ' + NL +
    '  R.ABSENCEREASON_CODE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMMIN ' + NL;
  FromStr :=
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON ' + NL +
    '    A.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER ' + NL;
  SelectStr := SelectStr +
    FromStr;
  WhereStr :=
    'WHERE ' + NL +
    '   ( ' + NL +
    '     (:USER_NAME = ''*'') OR ' + NL +
    '     (E.TEAM_CODE IN ' + NL +
    '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '   ) ' + NL +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) +  '''' + NL +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) +  '''' + NL +
    '  AND A.ABSENCEHOUR_DATE >= :FDATEFROM ' + NL +
    '  AND A.ABSENCEHOUR_DATE <= :FDATETO ' + NL;
  // Exclude employees out-of-service - 20011796.3
  WhereStr := WhereStr + EmployeeQuitRule;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    WhereStr := WhereStr +
    // RV096.1. Use other selection for BU-filtering.
{
      '  AND E.PLANT_CODE IN ' + NL +
      '    (SELECT BU.PLANT_CODE FROM BUSINESSUNIT BU ' + NL +
      '     WHERE ' + NL +
      '       BU.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessUnitFrom) + '''' + NL +
      '       AND BU.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessUnitTo) + '''' + NL +
      '      ) ' + NL +
}
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '   (SELECT E2.EMPLOYEE_NUMBER ' + NL +
      '    FROM EMPLOYEE E2 INNER JOIN DEPARTMENT D ON ' + NL +
      '      E2.PLANT_CODE = D.PLANT_CODE ' + NL +
      '      AND E2.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
      '    WHERE ' + NL +
      '      D.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessUnitFrom) + '''' + NL +
      '      AND D.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessUnitTo) + '''' + NL +
      '     ) ' + NL +
      '  AND E.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
      '  AND E.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL +
      '  AND E.TEAM_CODE >= ''' +
      DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
      '  AND E.TEAM_CODE <= ''' +
      DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
      '  AND E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
      '  AND E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
  end;
{ Possible options:

  const Holiday, WorkTimeReduction,
  TimeForTime, BankHoliday,
  AddBankHoliday, RsvBankHoliday,
  ShorterWWeek, SeniorityHoliday: Boolean;
}

  // Make a list of possible absence types.
  AbsenceStr := '';
  if QRParameters.FHoliday then
    AddAbsence(HOLIDAY);
  if QRParameters.FWorkTimeReduction then
    AddAbsence(WTR);
  if QRParameters.FTimeForTime then
    AddAbsence(TIMEFORTIMEAVAILABILITY);
  if QRParameters.FBankHoliday then
    AddAbsence(RSNB);
  if QRParameters.FAddBankHoliday then
    AddAbsence(ADDBANKHOLIDAYAVAILABILITY);
  if QRParameters.FRsvBankHoliday then
    AddAbsence(BANKHOLIDAYRESERVEAVAILABILITY);
  if QRParameters.FShorterWWeek then
    AddAbsence(SHORTERWORKWEEKAVAILABILITY);
  if QRParameters.FSeniorityHoliday then
    AddAbsence(SENIORITYHOLIDAYAVAILABILITY);
  if AbsenceStr <> '' then
    WhereStr := WhereStr +
      '  AND R.ABSENCETYPE_CODE IN (' + AbsenceStr + ')  ' + NL;

  SelectStr := SelectStr +
    WhereStr;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.PLANT_CODE, A.EMPLOYEE_NUMBER, A.ABSENCEHOUR_DATE, ' + NL +
    '  R.ABSENCEREASON_CODE ' + NL;

  // Create a union to get the months.
  for Month := 1 to 12 do
  begin
    SelectStr := SelectStr + ' ' +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  A.PLANT_CODE, ' + NL +
      '  A.EMPLOYEE_NUMBER, ' + NL +
      Format('  TO_DATE(''%d-%d-%d'',''dd-mm-yyyy''), ', [1, Month, FYear]) + NL +
      '  CAST ('''' AS VARCHAR(1)), ' + NL +
      '  CAST(-1 AS NUMBER(10)) ' + NL +
      FromStr +
      WhereStr +
      'GROUP BY ' + NL +
      '  A.PLANT_CODE, A.EMPLOYEE_NUMBER ' + NL;
  end; // for

  // RV100.2. Get Earned TFT from Salary-table
  if QRParameters.FTimeForTime then
  begin
    SelectStr := SelectStr +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  E.PLANT_CODE, ' + NL +
      '  E.EMPLOYEE_NUMBER, ' + NL +
      '  SHE.SALARY_DATE, ' + NL +
      '  ' + ''''  + EARNED_TFT_MARKER + '''' + ', ' + NL +
      '  SUM(SHE.SALARY_MINUTE) ' + NL;
    FromStr :=
      'FROM ' + NL +
      '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE H ON ' + NL +
      '    SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' + NL +
      '  INNER JOIN EMPLOYEE E ON ' + NL +
      '    SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  INNER JOIN CONTRACTGROUP C ON ' + NL +
      '    E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE ' + NL;
    SelectStr := SelectStr +
      FromStr;
    WhereStr :=
      'WHERE ' + NL +
      '   ( ' + NL +
      '     (:USER_NAME = ''*'') OR ' + NL +
      '     (E.TEAM_CODE IN ' + NL +
      '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      '   ) ' + NL +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) +  '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) +  '''' + NL;
    // Exclude employees out-of-service - 20011796.3
    WhereStr := WhereStr + EmployeeQuitRule;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    begin
      WhereStr := WhereStr +
        '  AND E.EMPLOYEE_NUMBER IN ' + NL +
        '   (SELECT E2.EMPLOYEE_NUMBER ' + NL +
        '    FROM EMPLOYEE E2 INNER JOIN DEPARTMENT D ON ' + NL +
        '      E2.PLANT_CODE = D.PLANT_CODE ' + NL +
        '      AND E2.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
        '    WHERE ' + NL +
        '      D.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessUnitFrom) + '''' + NL +
        '      AND D.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessUnitTo) + '''' + NL +
        '     ) ' + NL +
        '  AND E.DEPARTMENT_CODE >= ''' +
        DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
        '  AND E.DEPARTMENT_CODE <= ''' +
        DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL +
        '  AND E.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        '  AND E.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
        '  AND E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
    end;
    WhereStr := WhereStr +
      '  AND SHE.SALARY_DATE >= :FDATEFROM AND ' + NL +
      '  SHE.SALARY_DATE <= :FDATETO AND ' + NL +
      '  (H.OVERTIME_YN = ''Y'') AND ' + NL +
      '  ( ' + NL +
      '    (C.TIME_FOR_TIME_YN = ''Y'' AND C.TIME_FOR_TIME_YN = ''Y'') OR ' + NL +
      '    (H.TIME_FOR_TIME_YN = ''Y'' AND C.TIME_FOR_TIME_YN = ''N'') ' + NL +
      '   ) ' + NL;
    SelectStr := SelectStr +
      WhereStr;
    SelectStr := SelectStr +
      'GROUP BY ' + NL +
      '  E.PLANT_CODE, ' + NL +
      '  E.EMPLOYEE_NUMBER, ' + NL +
      '  SHE.SALARY_DATE ' + NL;
  end; // if QRParameters.FTimeForTime then

  // RV100.2. Create a union to get the months.
  for Month := 1 to 12 do
  begin
    SelectStr := SelectStr + ' ' +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  E.PLANT_CODE, ' + NL +
      '  E.EMPLOYEE_NUMBER, ' + NL +
      Format('  TO_DATE(''%d-%d-%d'',''dd-mm-yyyy''), ', [1, Month, FYear]) + NL +
      '  CAST ('''' AS VARCHAR(1)), ' + NL +
      '  CAST(-1 AS NUMBER(10)) ' + NL +
      FromStr +
      WhereStr +
      'GROUP BY ' + NL +
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER ' + NL;
  end; // for

  // Begin: At least 1 record must be found for the totals.
  // JVL 28-12-2011
  for Month := 1 to 12 do
  begin
  SelectStr := SelectStr + ' ' +
    'UNION ' + NL +
    'SELECT ' + NL +
    '  E.PLANT_CODE, ' + NL +
    '  E.EMPLOYEE_NUMBER, ' + NL +
//    '  TO_DATE(''1-1-2011'',''dd-mm-yyyy''), ' + NL +
    Format('  TO_DATE(''%d-%d-%d'',''dd-mm-yyyy''), ', [1, Month, FYear]) + NL +
    '  CAST ('''' AS VARCHAR(1)), ' + NL +
    '  CAST(-1 AS NUMBER(10)) ' + NL +
    'FROM EMPLOYEE E ' + NL +
    'WHERE ' + NL +
    '   ( ' + NL +
    '     (:USER_NAME = ''*'') OR ' + NL +
    '     (E.TEAM_CODE IN ' + NL +
    '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '   ) ' + NL +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) +  '''' + NL +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) +  '''' + NL;
  // Exclude employees out-of-service - 20011796.3
  SelectStr := SelectStr + EmployeeQuitRule;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '   (SELECT E2.EMPLOYEE_NUMBER ' + NL +
      '    FROM EMPLOYEE E2 INNER JOIN DEPARTMENT D ON ' + NL +
      '      E2.PLANT_CODE = D.PLANT_CODE ' + NL +
      '      AND E2.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
      '    WHERE ' + NL +
      '      D.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessUnitFrom) + '''' + NL +
      '      AND D.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessUnitTo) + '''' + NL +
      '     ) ' + NL +
      '  AND E.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
      '  AND E.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL +
      '  AND E.TEAM_CODE >= ''' +
      DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
      '  AND E.TEAM_CODE <= ''' +
      DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
      '  AND E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
      '  AND E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
  end;
  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + NL +
    '  E.PLANT_CODE, E.EMPLOYEE_NUMBER ' + NL;
  // End: At least 1 record must be found for the totals.
  end;

  SelectStr := SelectStr + ' ' +
    'ORDER BY ' +
    '  1, 2, 3 ';

  ReportHolidayCardDM.QueryAbsence.Active := False;
  ReportHolidayCardDM.QueryAbsence.UniDirectional := False;
  ReportHolidayCardDM.QueryAbsence.SQL.Clear;
  ReportHolidayCardDM.QueryAbsence.SQL.Add(SelectStr);
// ReportHolidayCardDM.QueryAbsence.SQL.SaveToFile('c:\temp\reportHolidayCard.sql');
  ReportHolidayCardDM.QueryAbsence.ParamByName('USER_NAME').AsString :=
    SystemDM.UserTeamLoginUser;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    ReportHolidayCardDM.QueryAbsence.ParamByName('EMPLOYEEFROM').AsInteger :=
      StrToInt(QRBaseParameters.FEmployeeFrom);
    ReportHolidayCardDM.QueryAbsence.ParamByName('EMPLOYEETO').AsInteger :=
      StrToInt(QRBaseParameters.FEmployeeTo);
  end;
  ReportHolidayCardDM.QueryAbsence.
    ParamByName('FDATEFROM').AsDateTime := FMinDate;
  ReportHolidayCardDM.QueryAbsence.
    ParamByName('FDATETO').AsDateTime := FMaxDate;
  ReportHolidayCardDM.QueryAbsence.Prepare;
  ReportHolidayCardDM.QueryAbsence.Active := True;
  Result := (not ReportHolidayCardDM.QueryAbsence.IsEmpty);

  // Query to get the totals per absence reason
  if Result then
  begin
    with ReportHolidayCardDM.qryAbsenceReasons do
    begin
      SelectStr :=
        'SELECT ' + NL +
        '  A.EMPLOYEE_NUMBER, R.ABSENCEREASON_CODE, R.DESCRIPTION, ' + NL +
        '  R.ABSENCETYPE_CODE, ' + NL +
        '  SUM(A.ABSENCE_MINUTE) ABSMIN ' + NL +
        'FROM ' + NL +
        '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON ' + NL +
        '    R.ABSENCEREASON_ID = A.ABSENCEREASON_ID ' + NL +
        '  INNER JOIN EMPLOYEE E ON ' + NL +
        '    E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER ' + NL +
        'WHERE ' + NL +
        '   ( ' + NL +
        '     (:USER_NAME = ''*'') OR ' + NL +
        '     (E.TEAM_CODE IN ' + NL +
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
        '   ) ' + NL +
        '  AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL +
        '  AND A.ABSENCEHOUR_DATE >= :FDATEFROM ' + NL +
        '  AND A.ABSENCEHOUR_DATE <= :FDATETO ' + NL;
      if AbsenceStr <> '' then
        SelectStr := SelectStr +
          '  AND R.ABSENCETYPE_CODE IN (' + AbsenceStr + ')  ' + NL;
      // Exclude employees out-of-service - 20011796.3
      SelectStr := SelectStr + EmployeeQuitRule;
      SelectStr := SelectStr +
        'GROUP BY ' + NL +
        '  A.EMPLOYEE_NUMBER, R.ABSENCEREASON_CODE, R.DESCRIPTION, ' + NL +
        '  R.ABSENCETYPE_CODE ' + NL +
        'ORDER BY ' + NL +
        '  1, 2 ';
      Close;
      SQL.Clear;
      SQL.Add(SelectStr);
// ReportHolidayCardDM.qryAbsenceReasons.SQL.SaveToFile('c:\temp\reportHolidayCard2.sql');
      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        ParamByName('EMPLOYEEFROM').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeFrom);
        ParamByName('EMPLOYEETO').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeTo);
      end
      else
      begin
        ParamByName('EMPLOYEEFROM').AsInteger := 1;
        ParamByName('EMPLOYEETO').AsInteger := 999999999;
      end;
      ParamByName('FDATEFROM').AsDateTime := FMinDate;
      ParamByName('FDATETO').AsDateTime := FMaxDate;
      Open;
    end;
  end;

  // RV103.2. Build second query that always get all absence types
  if Result then
  begin
    with ReportHolidayCardDM.qryAbsenceReasonsEmp do
    begin
      SelectStr2 :=
        'SELECT ABST.ABSENCETYPE_CODE, B.* ' + NL +
        'FROM ABSENCETYPE ABST LEFT OUTER JOIN ' + NL +
        '( ' + NL +
        SelectStr +
        ') B ON ' + NL +
        'ABST.ABSENCETYPE_CODE = B.ABSENCETYPE_CODE ' + NL +
        'WHERE ABST.ABSENCETYPE_CODE IN (' + AbsenceStr + ')  ' + NL +
        'ORDER BY 1 ';
      SQL.Clear;
      SQL.Add(SelectStr2);
    end;
  end;

  // Query to get the totals from ABSENCETOTAL to determine availability
  if Result then
  begin
    // Totals for current year
    with ReportHolidayCardDM.qryAbsenceTotalCur do
    begin
      Close;
      ParamByName('ABSENCE_YEAR').AsInteger := FCurYear;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        ParamByName('EMPLOYEEFROM').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeFrom);
        ParamByName('EMPLOYEETO').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeTo);
      end
      else
      begin
        ParamByName('EMPLOYEEFROM').AsInteger := 1;
        ParamByName('EMPLOYEETO').AsInteger := 999999999;
      end;
      Open;
    end;
    // Totals for previous year
    with ReportHolidayCardDM.qryAbsenceTotalPrev do
    begin
      Close;
      ParamByName('ABSENCE_YEAR').AsInteger := FPrevYear;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        ParamByName('EMPLOYEEFROM').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeFrom);
        ParamByName('EMPLOYEETO').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeTo);
      end
      else
      begin
        ParamByName('EMPLOYEEFROM').AsInteger := 1;
        ParamByName('EMPLOYEETO').AsInteger := 999999999;
      end;
      Open;
    end;
  end;

  // RV100.2. Get TFT-Earned details for calculations.
  if Result and QRParameters.FTimeForTime then
  begin
    with ReportHolidayCardDM.qryGetTFTHours do
    begin
      // RV103.4. Get year, month, day from salary-date to compare in filter.
      SelectStr :=
        'SELECT ' + NL +
        '  C.TIME_FOR_TIME_YN AS CGTFT_YN, ' + NL +
        '  H.HOURTYPE_NUMBER, ' + NL +
        '  H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN, ' + NL +
        '  C.BONUS_IN_MONEY_YN, ' + NL +
        '  H.BONUS_PERCENTAGE, ' + NL +
        '  E.EMPLOYEE_NUMBER, ' + NL +
        '  SHE.SALARY_DATE, ' + NL +
        '  EXTRACT(YEAR FROM SHE.SALARY_DATE) AS AYEAR, ' + NL +
        '  EXTRACT(MONTH FROM SHE.SALARY_DATE) AS AMONTH, ' + NL +
        '  EXTRACT(DAY FROM SHE.SALARY_DATE) AS ADAY, ' + NL +
        '  SUM(SHE.SALARY_MINUTE) AS SUMMINUTE ' + NL +
        'FROM ' + NL +
        '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE H ON ' + NL +
        '    SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' + NL +
        '  INNER JOIN EMPLOYEE E ON ' + NL +
        '    SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
        '  INNER JOIN CONTRACTGROUP C ON ' + NL +
        '    E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE ' + NL +
        'WHERE ' + NL +
        '  E.PLANT_CODE >= :PLANTFROM AND ' + NL +
        '  E.PLANT_CODE <= :PLANTTO ' + NL;
      // Exclude employees out-of-service - 20011796.3
      SelectStr := SelectStr + EmployeeQuitRule;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        SelectStr := SelectStr +
          '  AND E.EMPLOYEE_NUMBER IN ' + NL +
          '   (SELECT E2.EMPLOYEE_NUMBER ' + NL +
          '    FROM EMPLOYEE E2 INNER JOIN DEPARTMENT D ON ' + NL +
          '      E2.PLANT_CODE = D.PLANT_CODE ' + NL +
          '      AND E2.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
          '    WHERE ' + NL +
          '      D.BUSINESSUNIT_CODE >= ''' +
          DoubleQuote(QRParameters.FBusinessUnitFrom) + '''' + NL +
          '      AND D.BUSINESSUNIT_CODE <= ''' +
          DoubleQuote(QRParameters.FBusinessUnitTo) + '''' + NL +
          '     ) ' + NL +
          '  AND E.DEPARTMENT_CODE >= ''' +
          DoubleQuote(QRParameters.FDepartmentFrom) + '''' + NL +
          '  AND E.DEPARTMENT_CODE <= ''' +
          DoubleQuote(QRParameters.FDepartmentTo) + '''' + NL +
          '  AND E.TEAM_CODE >= ''' +
          DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
          '  AND E.TEAM_CODE <= ''' +
          DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
          '  AND E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
          '  AND E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
      end;
      SelectStr := SelectStr +
        '  AND SHE.SALARY_DATE >= :FDATEFROM AND ' + NL +
        '  SHE.SALARY_DATE <= :FDATETO AND ' + NL +
        '  (H.OVERTIME_YN = ''Y'') AND ' + NL +
        '  ( ' + NL +
        '    (C.TIME_FOR_TIME_YN = ''Y'' AND C.TIME_FOR_TIME_YN = ''Y'') OR ' + NL +
        '    (H.TIME_FOR_TIME_YN = ''Y'' AND C.TIME_FOR_TIME_YN = ''N'') ' + NL +
        '   ) ' + NL +
        'GROUP BY ' + NL +
        '  C.TIME_FOR_TIME_YN, ' + NL +
        '  H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN, ' + NL +
        '  C.BONUS_IN_MONEY_YN, H.BONUS_PERCENTAGE, ' + NL +
        '  E.EMPLOYEE_NUMBER, ' + NL +
        '  SHE.SALARY_DATE, ' + NL +
        '  NULL, ' + NL +
        '  NULL, ' + NL +
        '  NULL ' + NL +
        'ORDER BY ' + NL +
        '  E.EMPLOYEE_NUMBER, ' + NL +
        '  SHE.SALARY_DATE ';
      Close;
      Filtered := False;
      SQL.Clear;
      SQL.Add(SelectStr);
      ParamByName('PLANTFROM').AsString := QRBaseParameters.FPlantFrom;
      ParamByName('PLANTTO').AsString := QRBaseParameters.FPlantTo;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        ParamByName('EMPLOYEEFROM').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeFrom);
        ParamByName('EMPLOYEETO').AsInteger :=
          StrToInt(QRBaseParameters.FEmployeeTo);
      end;
      ParamByName('FDATEFROM').AsDateTime := FMinDate;
      ParamByName('FDATETO').AsDateTime := FMaxDate;
      Open;
    end;
  end;

  {check if report is empty}
  Screen.Cursor := crDefault;
end;

procedure TReportHolidayCardQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  // RV103.4.
  QRLblBaseTitle.Caption := SPimsHolidayCardTitle
    {Caption + ' - ' + SPimsYear }
    + ' ' + IntToStr(FYear);

  // RV103.4.
  QRLabelTitle.Caption := QRLblBaseTitle.Caption;

  // RV103.4. Right side of matrix: Column-names.
  QRLabel51.Caption := QRLabel45.Caption;
  QRLabel52.Caption := QRLabel2.Caption;
  QRLabel53.Caption := QRLabel46.Caption;

  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRGroupHDPlant.ForceNewPage := True;
  QRGroupHDPlant.Height := 0;
  
//  QRBandTitle.ForceNewPage := True;
  QRGroupHDEmpl.ForceNewPage := True;

//  QRBandTitle.Enabled := QRParameters.FShowSelection;

  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := QRParameters.FBusinessUnitFrom;
    QRLabelBusinessTo.Caption := QRParameters.FBusinessUnitTo;
    if QRParameters.FAllDepartments then
    begin
      QRLabelDeptFrom.Caption := '*';
      QRLabelDeptTo.Caption := '*';
    end
    else
    begin
      QRLabelDeptFrom.Caption := QRParameters.FDepartmentFrom;
      QRLabelDeptTo.Caption := QRParameters.FDepartmentTo;
    end;
    if QRParameters.FAllTeams then
    begin
      QRLabelTeamFrom.Caption := '*';
      QRLabelTeamTo.Caption := '*';
    end
    else
    begin
      QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
      QRLabelTeamTo.Caption := QRParameters.FTeamTo;
    end;
    QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  end
  else
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);

  // 20011796 Do not show these:
  QRLabel3.Caption := '';
  QRLabel54.Caption := '';

  // 20011796.2.
  // Total Available - show always
  QRLabel55.Caption := SPimsRepHolCardTotAvail;
  QRShape72.Width := VQRShape72Width;
  QRLabelTotAvail.Caption := '';

  // 20011796.2.
//  if QRParameters.FHolidayTFT then
  if QRParameters.FTimeForTime then
  begin
    // Holiday-header
    QRLabel45.Caption := SPimsRepHolCardOpeningBalance;
    QRLabel2.Caption  := SPimsRepHolCardUsed;
    QRLabel46.Caption := SPimsRepHolCardAvailable;
    // TFT-header
    QRLabel51.Caption := SPimsRepHolCardTFTEarned;
    QRLabel52.Caption := SPimsRepHolCardTUsed;
    QRLabel53.Caption := SPimsRepHolCardAvailable;
    // Total Available
//    QRLabel55.Caption := SPimsRepHolCardTotAvail;
//    QRShape72.Width := VQRShape72Width;
//    QRLabelTotAvail.Caption := '';
  end
  else
  begin
//    QRLabel55.Caption := '';
//    QRShape72.Width := 0;
//    QRLabelTotAvail.Caption := '';
  end;
end;

procedure TReportHolidayCardQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportHolidayCardQR.ClearMemoField;
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
      if (TempComponent as TQRMemo).Tag >= 1 then
      (TempComponent as TQRMemo).Lines.Clear;
  end;
end;


procedure TReportHolidayCardQR.SetMemoHeight(HeightMemo: Integer );
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
      if (TempComponent as TQRMemo).Tag >= 1 then
      begin
        (TempComponent as TQRMemo).Height := HeightMemo;
        (TempComponent as TQRMemo).Top := 3;
      end;
  end;
end;

procedure TReportHolidayCardQR.SetVertShapeHeight(HeightShape: Integer );
 var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRShape) then
      if (TempComponent as TQRShape).Tag = 1 then
      begin
        (TempComponent as TQRShape).Height := HeightShape;
        (TempComponent as TQRShape).Top := -3;
      end;
  end;
end;

procedure TReportHolidayCardQR.FillMemoField(Day : Integer; SpaceFill: Boolean);
var
  TempComponent: TComponent;
  Counter: Integer;
  SumMin: Double;
  AbsRsn: String;
  CYear, CMonth, CDay: Word;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
    begin
      if (TempComponent as TQRMemo).Tag = Day then
      begin
        if SpaceFill then
          (TempComponent as TQRMemo).Lines.Add( '    ')
        else
        begin
          with ReportHolidayCardDM do
          begin
            if (QRParameters.FTimeForTime and
              (QueryAbsence.
              FieldByName('ABSENCEREASON_CODE').AsString = EARNED_TFT_MARKER)
              ) then
            begin
              qryGetTFTHours.Close;
              qryGetTFTHours.Filtered := False;
              DecodeDate(QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
                CYear, CMonth, CDay);
              // RV103.4. Bugfix.
              //          Compare date with year, month, day from query, instead
              //          of using 'ADateFtm', because that gives a problem
              //          when the windows-date-time-settings are for
              //          example 'yyyy-mm-dd'.
{
              SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
              qryGetTFTHours.Filter := 'EMPLOYEE_NUMBER = ' +
                IntToStr(QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger) +
                ' AND SALARY_DATE = ' +
                SystemDM.ADateFmt.Quote +
                FormatDateTime(SystemDM.ADateFmt.DateTimeFmt,
//                    Trunc(QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime)) +
                  QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime) +
                SystemDM.ADateFmt.Quote;
              SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
}
              qryGetTFTHours.Filter := 'EMPLOYEE_NUMBER = ' +
                IntToStr(QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger) +
                ' AND AYEAR = ' + IntToStr(CYear) +
                ' AND AMONTH = ' + IntToStr(CMonth) +
                ' AND ADAY = ' + IntToStr(CDay);

              qryGetTFTHours.Filtered := True;
              qryGetTFTHours.Open;
              SumMin := GlobalDM.CalcEarnedTFTHours(qryGetTFTHours);
              AbsRsn := SPimsEarnedTFTSign;
            end
            else
            begin
              SumMin := QueryAbsence.FieldByName('SUMMIN').AsFloat;
              AbsRsn := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
            end;
            if AbsRsn = SPimsEarnedTFTSign then
              (TempComponent as TQRMemo).Lines.Add(
                AbsRsn)
            else
              (TempComponent as TQRMemo).Lines.Add(
                '    ' + AbsRsn + '  ');
            (TempComponent as TQRMemo).Lines.Add(
              DecodeHrsMinL(Round(SumMin), 5));
            // 20011796.2. Bugfix, export of multiple values was wrong:
            //             - It only exported last value.
            if QRParameters.FExportToFile then
{              ValueList[Day] := AbsRsn + ' ' +
                DecodeHrsMinL(Round(SumMin), 5); }
              ValueList[Day] := ValueList[Day] + AbsRsn + ' ' +
                DecodeHrsMinL(Round(SumMin), 5) + ' ';
          end;
        end;
        Exit;
      end;
    end;
  end;
end;

function TReportHolidayCardQR.GetMonth(MonthInt: Integer): String;
begin
  Result := '';
  case MonthInt of
   1: Result := SJanuaryS;
   2: Result := SFebruaryS;
   3: Result := SMarchS;
   4: Result := SAprilS;
   5: Result := SMayS;
   6: Result := SJuneS;
   7: Result := SJulyS;
   8: Result := SAugustS;
   9: Result := SSeptemberS;
  10: Result := SOctoberS;
  11: Result := SNovemberS;
  12: Result := SDecemberS;
  end;
end;

procedure TReportHolidayCardQR.QRGroupHDDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
{const
  AH=35;  // 35
  SH=25;  // 25
  SVH=35; // 35
  QSD=31; // 31
}
var
  Year, Month,  PrevMonth, Day: Word;
  SAVEEmpl, SaveDay: Integer;
begin
  inherited;
  ClearValueList;


  // RV103.4. Do not use fixed values in source!
{
  QRGroupHDDate.Height := 40;
  SetMemoHeight(30);
  SetVertShapeHeight(40);
  QRShapeDATE.Top := 36;
}

  // RV103.4. Do not use fixed values in source!
//  QRGroupHDDate.Height := AH; // 35
//  SetMemoHeight(SH);          // 25
//  SetVertShapeHeight(SVH);     // 35
//  QRShapeDATE.Top := QSD;      // 31

  // RV103.4.
  QRGroupHDDate.Height := VQRGroupHDDateHeight;
  SetMemoHeight(VMemoHeight);
  SetVertShapeHeight(VShapeHeight);
  QRShapeDATE.Top := VQRShapeDATETop;

  DecodeDate(ReportHolidayCardDM.QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
     Year, PrevMonth, Day);

  Month := PrevMonth;
  // RV103.4. Truncate month to 3 positions.
//  QRLabelMonth.Caption := Copy(GetMonth(Month), 1, 3);
  // RV103.4. The months are now abbr. in UPimsMessageRes.
  QRLabelMonth.Caption := GetMonth(Month);

  ClearMemoField;
  SaveEmpl := ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  SaveDay := 0;
  while (Month = PrevMonth) and (not ReportHolidayCardDM.QueryAbsence.Eof) and
  (SaveEmpl = ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger) do
  begin
    // Skip when -1 or when 0 !
    if (Round(ReportHolidayCardDM.QueryAbsence.FieldByName('SUMMIN').AsFloat) = -1) or
      (Round(ReportHolidayCardDM.QueryAbsence.FieldByName('SUMMIN').AsFloat) = 0) then
    else
    begin
      if SaveDay = Day then
      begin
  // RV103.4. Do not use fixed values in source!
{
        QRGroupHDDate.Height := 70;
        SetMemoHeight(58);
        SetVertShapeHeight(80);
        QRShapeDATE.Top := 65;
}

  // RV103.4. Do not use fixed values in source!
//        QRGroupHDDate.Height := Round(AH * 1.75); // 60
//        SetMemoHeight(Round(SH * 1.933));          // 48
//        SetVertShapeHeight(Round(SVH * 2));     // 70
//        QRShapeDATE.Top := Round(QSD * 1.805);      // 55

        // RV103.4.
        QRGroupHDDate.Height := VQRGroupHDDateHeight * 2;
        SetMemoHeight(VMemoHeight * 2);
        SetVertShapeHeight(VShapeHeight * 2);
        QRShapeDATE.Top := VQRShapeDATETop * 2;
      end;

      if Day - SaveDay > 1 then
        FillMemoField(SaveDay + 1, True);
      FillMemoField(Day, False);
      SaveDay := Day;
    end;
    ReportHolidayCardDM.QueryAbsence.Next;
    DecodeDate(ReportHolidayCardDM.QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
     Year, Month, Day);
  end;
  if not ReportHolidayCardDM.QueryAbsence.Eof then
    ReportHolidayCardDM.QueryAbsence.Prior;

  if QRParameters.FExportToFile then
    ExportDetail(QRLabelMonth.Caption);
end;

procedure TReportHolidayCardQR.FormCreate(Sender: TObject);
const // Use this to get more on 1 page: Lower=Smaller.
  SizePerc = 85; // RV103.4.
begin
  inherited;
  // RV103.4.
  VQRGroupHDDateHeight := Round(QRGroupHDDate.Height / 100 * SizePerc);
  VMemoHeight := Round(QR1.Height / 100 * SizePerc);
  VShapeHeight := Round(QRShape4.Height / 100 * SizePerc);
  VQRShapeDATETop := Round(QRShapeDATE.Top / 100 * SizePerc);

  // 20011796.2.
  VQRShape72Width := QRShape72.Width;

  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportHolidayCardQR.QRBandFTEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
  Line: String;
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // 20011796.2.
//    if QRParameters.FHolidayTFT then
    if QRParameters.FTimeForTime then
    begin
      // Header - Holiday
      Line := QRLabel3.Caption + ExportClass.Sep +
        QRLabel45.Caption + ExportClass.Sep +
        QRlabel2.Caption + ExportClass.Sep +
        QRLabel46.Caption + ExportClass.Sep;
      ExportClass.AddText(Line);
      // Details for Holiday
      // Left Side
      for I := 0 to QRMemo1.Lines.Count - 1 do
      begin
        if (QRMemo1.Lines[I] <> '') and
          (QRMemo1.Lines[I] <> SPimsRepHolCardLegenda) and
          (QRMemo1.Lines[I] <> SPimsRepHolCardLegendaTFTEarned) then
        begin
          Line := QRMemo1.Lines[I] + ExportClass.Sep +
            QRMemo2.Lines[I] + ExportClass.Sep +
            QRMemo3.Lines[I] + ExportClass.Sep +
            QRMemo4.Lines[I] + ExportClass.Sep;
          ExportClass.AddText(Line);
        end;
      end;
      // Header - TFT
      Line := QRLabel54.Caption + ExportClass.Sep +
        QRLabel51.Caption + ExportClass.Sep +
        QRlabel52.Caption + ExportClass.Sep +
        QRLabel53.Caption + ExportClass.Sep +
        QRLabel55.Caption + ExportClass.Sep;
      ExportClass.AddText(Line);
      // Details for TFT
      // Right Side
      for I := 0 to QRMemo5.Lines.Count - 1 do
      begin
        Line := QRMemo5.Lines[I] + ExportClass.Sep +
          QRMemo6.Lines[I] + ExportClass.Sep +
          QRMemo7.Lines[I] + ExportClass.Sep +
          QRMemo8.Lines[I] + ExportClass.Sep;
        if I = 0 then
          Line := Line + QRLabelTotAvail.Caption + ExportClass.Sep;
        ExportClass.AddText(Line);
      end;
      // Legenda
      for I := 0 to QRMemo1.Lines.Count - 1 do
      begin
        if (QRMemo1.Lines[I] = '') or
          (QRMemo1.Lines[I] = SPimsRepHolCardLegenda) or
          (QRMemo1.Lines[I] = SPimsRepHolCardLegendaTFTEarned) then
        begin
          Line := QRMemo1.Lines[I];
          ExportClass.AddText(Line);
        end;
      end;
    end // if QRParameters.FTimeForTime then
    else
    begin // if QRParameters.FTimeForTime then
      // Header
      Line := QRLabel3.Caption + ExportClass.Sep +
        QRLabel45.Caption + ExportClass.Sep +
        QRlabel2.Caption + ExportClass.Sep +
        QRLabel46.Caption + ExportClass.Sep;
      ExportClass.AddText(Line);
      // Details
      // Left Side
      for I := 0 to QRMemo1.Lines.Count - 1 do
      begin
        if (QRMemo1.Lines[I] <> '') and
          (QRMemo1.Lines[I] <> SPimsRepHolCardLegenda) and
          (QRMemo1.Lines[I] <> SPimsRepHolCardLegendaTFTEarned) then
        begin
          Line := QRMemo1.Lines[I] + ExportClass.Sep +
            QRMemo2.Lines[I] + ExportClass.Sep +
            QRMemo3.Lines[I] + ExportClass.Sep +
            QRMemo4.Lines[I] + ExportClass.Sep;
          ExportClass.AddText(Line);
        end;
      end;
      // RV103.4.
      // Right Side
      for I := 0 to QRMemo5.Lines.Count - 1 do
      begin
        Line := QRMemo5.Lines[I] + ExportClass.Sep +
          QRMemo6.Lines[I] + ExportClass.Sep +
          QRMemo7.Lines[I] + ExportClass.Sep +
          QRMemo8.Lines[I] + ExportClass.Sep;
        if I = 0 then
          Line := Line + QRLabelTotAvail.Caption + ExportClass.Sep;
        ExportClass.AddText(Line);
      end;
      // 20011796.2. Legenda
      for I := 0 to QRMemo1.Lines.Count - 1 do
      begin
        if (QRMemo1.Lines[I] = '') or
          (QRMemo1.Lines[I] = SPimsRepHolCardLegenda) or
          (QRMemo1.Lines[I] = SPimsRepHolCardLegendaTFTEarned) then
        begin
          Line := QRMemo1.Lines[I];
          ExportClass.AddText(Line);
        end;
      end;
    end; // if QRParameters.FTimeForTime then
{
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
}
  end; // if QRParameters.FExportToFile then
end;

procedure TReportHolidayCardQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
{var
  I: Integer; }
begin
  inherited;
{  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.Filename := Caption + '-' +
      IntToStr(FYear) + '-' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + '-' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + '-' +
      '.txt';
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Create a header
    ExportClass.AddText(IntToStr(FYear));
    ExportClass.AddText(QRLabel4.Caption + ' ' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportHolidayCardDM.TablePlant.FieldByName('DESCRIPTION').AsString + ' ' +
      QRLabel1.Caption + ' ' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
      ReportHolidayCardDM.TableEmpl.FieldByName('DESCRIPTION').AsString);
    ClearValueList;
    for I := 1 to MAXDAY do
      ValueList[I] := IntToStr(I);
    ExportDetail('');
  end; }
end;

procedure TReportHolidayCardQR.QRGroupHDEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
{
    ExportClass.Filename := Caption + '-' +
      IntToStr(FYear) + '-' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + '-' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + '-' +
      '.txt';
    ExportClass.AskFilename;
    ExportClass.ClearText;
}
    // Create a header
//    ExportClass.AddText(IntToStr(FYear));
    ExportClass.AddText(QRLabel4.Caption + ' ' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportHolidayCardDM.TablePlant.FieldByName('DESCRIPTION').AsString + ' ' +
      QRLabel5.Caption + ' ' +
      ReportHolidayCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
      ReportHolidayCardDM.TableEmpl.FieldByName('DESCRIPTION').AsString);
    ClearValueList;
    for I := 1 to MAXDAY do
      ValueList[I] := IntToStr(I);
    ExportDetail('');
  end;
end;

procedure TReportHolidayCardQR.QRBandFTEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
//  IndexCount: Integer;
  LeftSide: Boolean;
  AbsReason: String;
  NormEarned, Used, Available: Double;
  TotalAvailable: Double; // 20011796.2.
  DetermineEarnedTFTDone: Boolean; // 20011792.2
  function DetermineAvailable(AbsenceTypeCode: String;
    CurrentYearUsed: Double): Boolean;
  var
    Total: Double;
    CurFound{, PrevFound }: Boolean;
    PrevYearAvail, CurYearNorm, CurYearUsed: Double;
  begin
    Result := False;
    Total := 0;
    PrevYearAvail := 0;
    CurYearNorm := 0;
//    CurYearUsed := 0;
    with ReportHolidayCardDM do
    begin
      // Get totals for current and previous year
      CurFound := qryAbsenceTotalCur.Locate('EMPLOYEE_NUMBER;ABSENCE_YEAR',
        VarArrayOf([QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          FCurYear]), []);
{      PrevFound := qryAbsenceTotalPrev.Locate('EMPLOYEE_NUMBER;ABSENCE_YEAR',
        VarArrayOf([QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          FPrevYear]), []); }
      // Determine availabilty
      case AbsenceTypeCode[1] of
      HOLIDAY:
        begin
          if QRParameters.FHoliday then
            Result := True;
          // a
{          if PrevFound then
            PrevYearAvail :=
              qryAbsenceTotalPrev.FieldByName('NORM_HOLIDAY_MINUTE').AsFloat -
              qryAbsenceTotalPrev.FieldByName('USED_HOLIDAY_MINUTE').AsFloat; }
          if CurFound then
            PrevYearAvail :=
              qryAbsenceTotalCur.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').AsFloat;
          // b
          if CurFound then
            CurYearNorm :=
              qryAbsenceTotalCur.FieldByName('NORM_HOLIDAY_MINUTE').AsFloat;
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          // The Norm must include the PrevYearAvail in report.
          CurYearNorm := PrevYearAvail + CurYearNorm;
          AbsReason := QRParameters.FHolidayDesc;
        end;
      WTR:
        begin
          if QRParameters.FWorkTimeReduction then
            Result := True;
          // a
          if CurFound then
            PrevYearAvail :=
              qryAbsenceTotalCur.FieldByName('LAST_YEAR_WTR_MINUTE').AsFloat;
          // b
          // How to determine 'Earned WTR' for the period? (see: DialogCalculatedEarnedWTR)
          if CurFound then
            CurYearNorm :=
              qryAbsenceTotalCur.FieldByName('EARNED_WTR_MINUTE').AsFloat;
          // c
          CurYearUsed := CurrentYearUsed;
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          // The Norm must include the PrevYearAvail in report.
          CurYearNorm := PrevYearAvail + CurYearNorm;
          AbsReason := QRParameters.FWorkTimeReductionDesc;
        end;
      TIMEFORTIMEAVAILABILITY:
        begin
          if QRParameters.FTimeForTime then
            Result := True;
          // RV103.2.
          // a
          if CurFound then
            PrevYearAvail :=
              qryAbsenceTotalCur.FieldByName('LAST_YEAR_TFT_MINUTE').AsFloat;
          // b
          // 20011796.2. This must only determined once!
          if not DetermineEarnedTFTDone then
          begin
            CurYearNorm := GlobalDM.DetermineEarnedTFTHours(
              QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              FMinDate, FMaxDate, True);
            DetermineEarnedTFTDone := True;
          end;
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          // The Norm must include the PrevYearAvail in report.
          CurYearNorm := PrevYearAvail + CurYearNorm;
          AbsReason := QRParameters.FTimeForTimeDesc;
        end;
      RSNB: // What is this?
        begin
          if QRParameters.FBankHoliday then
            Result := True;
          // a
          // b
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          AbsReason := QRParameters.FBankHolidayDesc;
        end;
      ADDBANKHOLIDAYAVAILABILITY:
        begin
         if QRParameters.FAddBankHoliday then
           Result := True;
          // a
          if CurFound then
            PrevYearAvail :=
              qryAbsenceTotalCur.FieldByName('LAST_YEAR_ADD_BANK_HLD_MINUTE').AsFloat;
          // b
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          // The Norm must include the PrevYearAvail in report.
          CurYearNorm := PrevYearAvail + CurYearNorm;
          AbsReason := QRParameters.FAddBankHolidayDesc;
        end;
      BANKHOLIDAYRESERVEAVAILABILITY:
        begin
          if QRParameters.FRsvBankHoliday then
            Result := True;
          // a
{
          if PrevFound then
            PrevYearAvail :=
              qryAbsenceTotalPrev.FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').AsFloat -
              qryAbsenceTotalPrev.FieldByName('USED_BANK_HLD_RESERVE_MINUTE').AsFloat;
}
          // b
          if CurFound then
            CurYearNorm :=
              qryAbsenceTotalCur.FieldByName('NORM_BANK_HLD_RESERVE_MINUTE').AsFloat;
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          AbsReason := QRParameters.FRsvBankHolidayDesc;
        end;
      SHORTERWORKWEEKAVAILABILITY:
        begin
          if QRParameters.FShorterWWeek then
            Result := True;
          // a
          if CurFound then
            PrevYearAvail :=
              qryAbsenceTotalCur.FieldByName('LAST_YEAR_SHORTWWEEK_MINUTE').AsFloat;
          // b
          CurYearNorm := GlobalDM.DetermineEarnedSWWHours(
            QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            FMinDate, FMaxDate, True);
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          // The Norm must include the PrevYearAvail in report.
          CurYearNorm := PrevYearAvail + CurYearNorm;
          AbsReason := QRParameters.FShorterWWeekDesc;
        end;
      SENIORITYHOLIDAYAVAILABILITY:
        begin
          if QRParameters.FSeniorityHoliday then
            Result := True;
          // a
{
          if PrevFound then
            PrevYearAvail :=
              qryAbsenceTotalPrev.FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').AsFloat -
              qryAbsenceTotalPrev.FieldByName('USED_SENIORITY_HOLIDAY_MINUTE').AsFloat;
}
          // b
          if CurFound then
            CurYearNorm :=
              qryAbsenceTotalCur.FieldByName('NORM_SENIORITY_HOLIDAY_MINUTE').AsFloat;
          // c
          CurYearUsed := CurrentYearUsed;
          // a + b - c
          Total := PrevYearAvail + CurYearNorm - CurYearUsed;
          AbsReason := QRParameters.FSeniorityHolidayDesc;
        end;
      end; // case
      NormEarned := CurYearNorm;
      Available := Total;
{      if Total <> 0 then
        Result := ' / ' +
          DecodeHrsMinL(Round(Total),7); }
    end;
  end;
begin
  inherited;
  // 20011796.2.
  DetermineEarnedTFTDone := False;
  // This prints the explanation of the absence reasons.
  // And should also show the totals.
  // Must be done for each employee.
  // RV103.2. Show a matrix of the absence reasons with the values in columns.
  if True then
  begin
    // 20011796.2.
    TotalAvailable := 0;
    // RV103.4. Left side
    QRMemo1.Lines.Clear; // Absence reason
    QRMemo2.Lines.Clear; // Norm/Earned
    QRMemo3.Lines.Clear; // Used
    QRMemo4.Lines.Clear; // Available
    // RV103.4. Right side
    QRMemo5.Lines.Clear; // Absence reason
    QRMemo6.Lines.Clear; // Norm/Earned
    QRMemo7.Lines.Clear; // Used
    QRMemo8.Lines.Clear; // Available
    with ReportHolidayCardDM do
    begin
{
      qryAbsenceReasons.Filtered := False;
      qryAbsenceReasons.Filter := 'EMPLOYEE_NUMBER = ' +
        QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString;
      qryAbsenceReasons.Filtered := True;
      qryAbsenceReasons.First;
      IndexCount := 1;
}
      LeftSide := True;
      with qryAbsenceReasonsEmp do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('EMPLOYEEFROM').AsInteger :=
          QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('EMPLOYEETO').AsInteger :=
          QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('FDATEFROM').AsDateTime := FMinDate;
        ParamByName('FDATETO').AsDateTime := FMaxDate;
        Open;
      end;
      while not qryAbsenceReasonsEmp.Eof do
      begin
        AbsReason := '';
        NormEarned := 0;
        Used := 0;
        Available := 0;
        // if qryAbsenceReasons.FieldByName('ABSMIN').AsFloat <> 0 then
//        begin
{
          AbsReason := qryAbsenceReasons.
              FieldByName('ABSENCEREASON_CODE').AsString + '  ' +
              qryAbsenceReasons.FieldByName('DESCRIPTION').AsString +
              ': ' +
              DecodeHrsMinL(
                Round(qryAbsenceReasons.FieldByName('ABSMIN').AsFloat),7) +
              DetermineAvailable(
                qryAbsenceReasons.FieldByName('ABSENCETYPE_CODE').AsString,
                qryAbsenceReasons.FieldByName('ABSMIN').AsFloat);
}
        if qryAbsenceReasonsEmp.FieldByName('ABSMIN').AsString <> '' then
        begin
          DetermineAvailable(
            qryAbsenceReasonsEmp.FieldByName('ABSENCETYPE_CODE').AsString,
            qryAbsenceReasonsEmp.FieldByName('ABSMIN').AsFloat);
          AbsReason :=
            qryAbsenceReasonsEmp.FieldByName('ABSENCEREASON_CODE').AsString + '  ' +
            qryAbsenceReasonsEmp.FieldByName('DESCRIPTION').AsString;
          Used := qryAbsenceReasonsEmp.FieldByName('ABSMIN').AsFloat;
        end
        else
        begin
          DetermineAvailable(
            qryAbsenceReasonsEmp.FieldByName('ABSENCETYPE_CODE').AsString,
            0);
        end;
        // 20011796.2.
        TotalAvailable := TotalAvailable + Available;
        // 20011796.2. Truncate this to prevent it overwrites other parts
        AbsReason := Copy(AbsReason, 1, 25);
        // 20011796.2. Always show TFT at the right, all other at the left
        if QRParameters.FTimeForTime then
        begin
          if (qryAbsenceReasonsEmp.FieldByName('ABSENCETYPE_CODE').AsString =
            TIMEFORTIMEAVAILABILITY) then
            LeftSide := False
          else
            LeftSide := True;
        end;
        if LeftSide then
        begin
          // RV103.4. Left
          // Absence Reason
          QRMemo1.Lines.Add(AbsReason);
          // Norm/Earned
          QRMemo2.Lines.Add(Trim(DecodeHrsMinL(Round(NormEarned), 7)));
          // Used
          QRMemo3.Lines.Add(Trim(DecodeHrsMinL(Round(Used), 7)));
          // Available
          QRMemo4.Lines.Add(Trim(DecodeHrsMinL(Round(Available), 7)));
        end
        else
        begin
          // RV103.4. Right
          // Absence Reason
          QRMemo5.Lines.Add(AbsReason);
          // Norm/Earned
          QRMemo6.Lines.Add(Trim(DecodeHrsMinL(Round(NormEarned), 7)));
          // Used
          QRMemo7.Lines.Add(Trim(DecodeHrsMinL(Round(Used), 7)));
          // Available
          QRMemo8.Lines.Add(Trim(DecodeHrsMinL(Round(Available), 7)));
        end;

        // RV103.4.
        LeftSide := not LeftSide;
{
        if IndexCount > 2 then
          IndexCount := 1;
          case IndexCount of
          1: QRMemo1.Lines.Add(AbsReason);
          2: QRMemo2.Lines.Add(AbsReason);
          end;
        Inc(IndexCount);
}

//        end;
        qryAbsenceReasonsEmp.Next;
      end; // while not qryAbsenceReasons.Eof do
      // RV100.2. Also determine and show Earned-TFT
{
      TFTEarned := GlobalDM.DetermineEarnedTFTHours(
        QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        FMinDate, FMaxDate, True);
      if Round(TFTEarned) <> 0 then
      begin
        AbsReason := SPimsEarnedTFT + ': ' + DecodeHrsMinL(Round(TFTEarned),7);
        if IndexCount > 2 then
          IndexCount := 1;
        case IndexCount of
        1: QRMemo1.Lines.Add(AbsReason);
        2: QRMemo2.Lines.Add(AbsReason);
        end;
      end;
}

      // 20011796.2. Show total available.
      // Show this always
//      if QRParameters.FHolidayTFT then
      begin
        // Total Available
        QRLabelTotAvail.Caption :=
          Trim(DecodeHrsMinL(Round(TotalAvailable), 7));
      end;
      // 20011796.2. Show extra line with legenda.
      // Show this always
//      if QRParameters.FHolidayTFT then
      begin
        // Add empty line
        QRMemo1.Lines.Add('');
        // Add legenda
        if QRParameters.FHolidayTFT then
          QRMemo1.Lines.Add(SPimsRepHolCardLegenda)
        else
          if QRParameters.FTimeForTime then
            QRMemo1.Lines.Add(SPimsRepHolCardLegendaTFTEarned);
      end;
    end; // with ReportHolidayCardDM do
  end; // if Result then
end;

procedure TReportHolidayCardQR.QRGroupHDPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
//  PrintBand := False;
end;

procedure TReportHolidayCardQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;

  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(
          QRLabel11.Caption
          );
        // From Plant PlantFrom to PlantTo
        ExportClass.AddText(
          QRLabel13.Caption + ' ' +
          QRLabel16.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' +
          QRLabel49.Caption + ' ' +
          QRLabelPlantTo.Caption
          );
        // From BusinessUnit BUFrom to BUTo
        ExportClass.AddText(
          QRLabel20.Caption + ' ' +
          QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' +
          QRLabel18.Caption + ' ' +
          QRLabelBusinessTo.Caption
          );
        // From Department DepartmentFrom to DepartmentTo
        ExportClass.AddText(
          QRLabel47.Caption + ' ' +
          QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' +
          QRLabel50.Caption + ' ' +
          QRLabelDeptTo.Caption
          );
        // From Team TeamFrom to TeamTo
        ExportClass.AddText(
          QRLabel87.Caption + ' ' +
          QRLabel88.Caption + ' ' +
          QRLabelTeamFrom.Caption + ' ' +
          QRLabel90.Caption + ' ' +
          QRLabelTeamTo.Caption
          );
        // From Employee EmployeeFrom to EmployeeTo
        ExportClass.AddText(
          QRLabel22.Caption + ' ' +
          QRLabel42.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' +
          QRLabel44.Caption + ' ' +
          QRLabelEmployeeTo.Caption
          );
        // From Date to Date
        ExportClass.AddText(
          QRLabelFromDate.Caption + ' ' +
          QRLabelDate.Caption + ' ' +
          QRLabelDateFrom.Caption + ' ' +
          QRLabelToDate.Caption + ' ' +
          QRLabelDateTo.Caption
          );
      end;
      ExportClass.AddText(QRLblBaseTitle.Caption);
    end;
end;

procedure TReportHolidayCardQR.QRBandSummaryBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHolidayCardQR.QRBndBasePageHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // RV103.4.
end;

end.


