unit RevenueDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TRevenueDM = class(TGridBaseDM)
    TableBU: TTable;
    TableRevenue: TTable;
    TableDetailBUSINESSUNIT_CODE: TStringField;
    TableDetailREVENUE_DATE: TDateTimeField;
    TableDetailREVENUE_AMOUNT: TFloatField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    QueryWork: TQuery;

    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FDateMin, FDateMax, FOldDate: TDateTime;
    FBusiness: String;
    FInsertMode: Boolean;
    procedure SetValues(Business: String; Year, Week: Integer);
    function SaveRecord: Boolean;
    function DeleteRecord: Boolean;
    procedure UpdateValues(TempTable: TTable);
  end;

var
  RevenueDM: TRevenueDM;

implementation

uses RevenueFRM, ListProcsFRM, SystemDMT, UPimsMessageRes;

{$R *.DFM}
procedure TRevenueDM.SetValues(Business: String; Year, Week: Integer);
begin
  if (Business = '') or (Year = 0) or ( Week = 0) then
    Exit;
  FDateMax := ListProcsF.DateFromWeek(Year, Week, 7);
  FDateMin := ListProcsF.DateFromWeek(Year, Week, 1);
  Fbusiness := Business;
end;


function TRevenueDM.DeleteRecord: Boolean;
var
  Business: String;
  DateRevenue: TDateTime;
begin
  Business := GetStrValue(RevenueF.cmbPlusBU.Value);
  DateRevenue := GetDate(RevenueF.DateRevenue.Date);

  if TableRevenue.FindKey([Business, DateRevenue]) then
  begin
    Result := True;
    TableRevenue.Delete;
  end
  else
    Result := False;
end;

function TRevenueDM.SaveRecord: Boolean;
var
  Business, Selectstr: String;
  DateRevenue: TDateTime;
  Amount: Real;
begin
  Business := RevenueF.cmbPlusBU.Value;
  DateRevenue := GetDate(RevenueF.DateRevenue.Date);
  if RevenueF.AmountRevenue.Text = '' then
    Amount := 0
  else
    Amount := StrToFloat(RevenueF.AmountRevenue.Text);
  Result := True;
  if FInsertMode then
  begin
    Selectstr := 
      'INSERT INTO REVENUE (BUSINESSUNIT_CODE,REVENUE_DATE,REVENUE_AMOUNT, ' +
      '  CREATIONDATE, MUTATIONDATE, MUTATOR) ' +
      '  VALUES(:BUSINESSUNIT_CODE,:REVENUE_DATE,:AMOUNT_REVENUE, ' +
      '  :CREATIONDATE,:MUTATIONDATE,:MUTATOR)';
    QueryWork.SQL.Clear;
    QueryWork.Sql.ADD(SelectStr);
    QueryWork.ParamByName('BUSINESSUNIT_CODE').Value := FBusiness;
    QueryWork.ParamByName('REVENUE_DATE').Value := DateRevenue;
    QueryWork.ParamByName('AMOUNT_REVENUE').Value := Amount;
    QueryWork.ParamByName('CREATIONDATE').Value := Now;
    QueryWork.ParamByName('MUTATIONDATE').Value := Now;
    QueryWork.ParamByName('MUTATOR').Value :=  SystemDM.CurrentProgramUser;
    try
      QueryWork.ExecSql;
    except
      Result := False;
      DisplayMessage(SPimsKeyViolation, mtInformation, [mbOk]);
      Exit;
    end;
  end
  else
  begin
    Selectstr := 
      'UPDATE REVENUE SET REVENUE_DATE = :REVENUE_DATE, ' +
      '  REVENUE_AMOUNT = :AMOUNT_REVENUE , CREATIONDATE = :CREATIONDATE, ' +
      '  MUTATIONDATE = :MUTATIONDATE, MUTATOR = :MUTATOR ' +
      'WHERE ' +
      '  BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE AND ' +
      '  REVENUE_DATE = :FDATE'  ;
    QueryWork.SQL.Clear;
    QueryWork.Sql.ADD(SelectStr);

    QueryWork.Prepare;
    QueryWork.ParamByName('REVENUE_DATE').Value := DateRevenue;
    QueryWork.ParamByName('BUSINESSUNIT_CODE').Value :=
            TableDetail.FieldByName('BUSINESSUNIT_CODE').VALUE;
    QueryWork.ParamByName('FDATE').Value := FOldDate;
    QueryWork.ParamByName('AMOUNT_REVENUE').Value := Amount;
    QueryWork.ParamByName('CREATIONDATE').Value := Now;
    QueryWork.ParamByName('MUTATIONDATE').Value := Now;
    QueryWork.ParamByName('MUTATOR').Value :=  SystemDM.CurrentProgramUser;
    try
      QueryWork.ExecSql;
    except
      Result := False;
      DisplayMessage(SPimsKeyViolation, mtInformation, [mbOk]);
      Exit;
    end;
  end;
end;

procedure TRevenueDM.UpdateValues(TempTable: TTable);
begin
  TempTable.FieldByName('CREATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
end;

procedure TRevenueDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := (TableDetail.FieldByName('REVENUE_DATE').Value >= FDateMin) and
    (TableDetail.FieldByName('REVENUE_DATE').Value <= FDateMax) and
    (TableDetail.FieldByName('BUSINESSUNIT_CODE').Value = FBusiness);
end;

procedure TRevenueDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  DefaultNewRecord(DataSet);
  TableDetail.FieldByName('BUSINESSUNIT_CODE').ASSTRING := FBusiness;
  TableDetail.FieldByName('REVENUE_DATE').ASDATETIME := FDATEMIN;
end;

procedure TRevenueDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if TableDetail.RecordCount > 0 then
    FOldDate := TableDetail.FieldByName('REVENUE_DATE').VALUE;
end;

end.
