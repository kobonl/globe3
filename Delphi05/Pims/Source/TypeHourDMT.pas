(*
  MRA:20-NOV-2009 RV045.1. SR-889942.
  - Addition of 2 fields per hourtype related to Overtime:
    - Time for Time
    - Bonus in Money
    This can be used to use instead of setting on Contract Group-level.
    But only one level must be allowed!
  SO:08-JUL-2010 RV067.4. 550497
    - saturday credit and ADV fields added
  MRA:16-MAY-2012 20013183. Extra counter Travel Time.
  - Type of Hours
    - Show an extra checkbox for 'Travel Time'. Default 'N'.
      This makes it possible to book hours in balance for
      earned travel time.
      NOTE: This does not fall under 'overtime'.
  MRA:2-NOV-2012 20013723
  - Addition of 2 fields: EXPORT_OVERTIME_YN and EXPORT_PAYROLL_YN.
  - Test on Hourtypes-per-country for export type ADP
*)

unit TypeHourDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTypeHourDM = class(TGridBaseDM)
    TableMasterHOURTYPE_NUMBER: TIntegerField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterBONUS_PERCENTAGE: TFloatField;
    TableMasterOVERTIME_YN: TStringField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterCOUNT_DAY_YN: TStringField;
    TableMasterEXPORT_CODE: TStringField;
    TableMasterMINIMUM_WAGE: TFloatField;
    TableMasterIGNORE_FOR_OVERTIME_YN: TStringField;
    TableMasterWAGE_BONUS_ONLY_YN: TStringField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailBONUS_PERCENTAGE: TFloatField;
    TableDetailOVERTIME_YN: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailCOUNT_DAY_YN: TStringField;
    TableDetailMINIMUM_WAGE: TFloatField;
    TableDetailIGNORE_FOR_OVERTIME_YN: TStringField;
    TableDetailWAGE_BONUS_ONLY_YN: TStringField;
    TableDetailTIME_FOR_TIME_YN: TStringField;
    TableDetailBONUS_IN_MONEY_YN: TStringField;
    TableDetailEXPORTVL_NUMBER: TIntegerField;
    TableDetailADV_YN: TStringField;
    TableDetailSATURDAY_CREDIT_YN: TStringField;
    TableDetailEXPORT_CODE: TStringField;
    TableDetailTRAVELTIME_YN: TStringField;
    TableDetailEXPORT_OVERTIME_YN: TStringField;
    TableDetailEXPORT_PAYROLL_YN: TStringField;
    qryHTPerCountryADP: TQuery;
    procedure TableMasterNewRecord(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function TestOnHourtypePerCountryADP: Boolean;
  end;

var
  TypeHourDM: TTypeHourDM;

implementation

uses SystemDMT, UPimsConst;

{$R *.DFM}

procedure TTypeHourDM.TableMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableMaster.FieldByName('WAGE_BONUS_ONLY_YN').AsString := UNCHECKEDVALUE;
  TableMaster.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString := UNCHECKEDVALUE;
  TableMasterOVERTIME_YN.Value := UNCHECKEDVALUE;
  TableMasterCOUNT_DAY_YN.Value := UNCHECKEDVALUE;
  TableMasterCOUNT_DAY_YN.Value := UNCHECKEDVALUE;
end;

procedure TTypeHourDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetail.FieldByName('WAGE_BONUS_ONLY_YN').AsString := UNCHECKEDVALUE;
  TableDetail.FieldByName('IGNORE_FOR_OVERTIME_YN').AsString := UNCHECKEDVALUE;
  TableDetailOVERTIME_YN.Value := UNCHECKEDVALUE;
  TableDetailCOUNT_DAY_YN.Value := UNCHECKEDVALUE;
  TableDetailCOUNT_DAY_YN.Value := UNCHECKEDVALUE;
  TableDetailTIME_FOR_TIME_YN.Value := UNCHECKEDVALUE;
  TableDetailBONUS_IN_MONEY_YN.Value := UNCHECKEDVALUE;
  //RV067.4.
  TableDetailADV_YN.Value := UNCHECKEDVALUE;
  TableDetailSATURDAY_CREDIT_YN.Value := UNCHECKEDVALUE;
  // 20013183.
  TableDetailTRAVELTIME_YN.Value := UNCHECKEDVALUE;
  // 20013723
  TableDetailEXPORT_OVERTIME_YN.Value := UNCHECKEDVALUE;
  TableDetailEXPORT_PAYROLL_YN.Value := UNCHECKEDVALUE;
end;

// RV045.1.
procedure TTypeHourDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if TableDetailOVERTIME_YN.Value = UNCHECKEDVALUE then
  begin
    TableDetailTIME_FOR_TIME_YN.Value := UNCHECKEDVALUE;
    TableDetailBONUS_IN_MONEY_YN.Value := UNCHECKEDVALUE;
    //RV067.4.
    TableDetailADV_YN.Value := UNCHECKEDVALUE;
  end;
end;

// 20013723
function TTypeHourDM.TestOnHourtypePerCountryADP: Boolean;
begin
  Result := False;
  with qryHTPerCountryADP do
  begin
    Close;
    Open;
    if not Eof then
      Result := True;
    Close;
  end;
end; // TestOnHourtypePerCountryADP

end.
