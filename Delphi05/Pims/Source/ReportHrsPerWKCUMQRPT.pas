(*
  Changes:
    MR:17-10-2005 Order 550411
      Select should give results for 3 situations:
      1. USE_JOBCODE_YN = 'Y' AND JOB_CODE <> '0'
      2. USE_JOBCODE_YN = 'N' (for any jobcode; '0' or not '0')
      3. USE_JOBCODE_YN = 'Y' AND JOB_CODE = '0'
      This can happen if user switches these settings in Pims on Workspot-level.
      The hours from PRODHOURPEREMPLOYEE should always be shown, regardless
      of these settings.
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
    MRA:21-JUL-2010 RV067.MRA.2. 550496.
    - Report gives wrong results when using 'show BU' and 'show JOB'.
      Cause: When showing on JOB and job is zero ('0'), then it gets the
             BusinessunitPerWorkspot (BW). This can give multiple records for
             BW, resulting in a multiplication of the production-minutes.
             To solve it the calculation with BU-percentage is done in query.
    MRA:24-OCT-2012 Bugfix. 20013489 (Only related to this order).
    - It shows wrong working days when showing employees and/or jobs.
    - Show working days ONLY for levels:
      - Plant, Business Unit, Department, Workspot
    - Do NOT show working days for levels:
      - Employee, Job
    - Use a different way to determine worked days.
*)

unit ReportHrsPerWKCUMQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo: String; FShowBU, FShowDept, FShowWK, FShowJob, FShowEmpl,
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK, FPageJob: Boolean;
    FExportToFile, FAllEmployees, FIncludeNotConnectedEmp: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
      ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean);
  end;

  TAmountsPerDay = Array[1..7] of Integer;

  TReportHrsPerWKCUMQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDJob: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelJobCodeFrom: TQRLabel;
    QRLabelJobCodeTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRBandFooterWK: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBandFooterDEPT: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRBandFooterJOB: TQRBand;
    QRLabel60: TQRLabel;
    QRDBText2: TQRDBText;
    QRBandFooterEmpl: TQRBand;
    QRLabel106: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRLabelTotJob: TQRLabel;
    QRDBTextJobCode: TQRDBText;
    QRDBTextJobDesc: TQRDBText;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabelEmplHrs: TQRLabel;
    QRLabelAvgEmpl: TQRLabel;
    QRLabelEmplDay: TQRLabel;
    QRLabelJobHrs: TQRLabel;
    QRLabelAvgJob: TQRLabel;
    QRLabelJobDay: TQRLabel;
    QRLabelDeptHrs: TQRLabel;
    QRLabelAvgDept: TQRLabel;
    QRLabelDeptDay: TQRLabel;
    QRLabelBUHrs: TQRLabel;
    QRLabelAvgBU: TQRLabel;
    QRLabelBUDay: TQRLabel;
    QRLabelPlantHrs: TQRLabel;
    QRLabelAvgPlant: TQRLabel;
    QRLabelPlantDay: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRBand1: TQRBand;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRBandSummary: TQRBand;
    QRDBText18: TQRDBText;
    QRDBText7: TQRDBText;
    ChildBandFooterWK: TQRChildBand;
    QRLabelTotWK: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelWKHrs: TQRLabel;
    QRLabelAvgWK: TQRLabel;
    QRLabelWKDay: TQRLabel;
    QRLabelWKUnknownBU: TQRLabel;
    QRLabelWKHrsUN: TQRLabel;
    QRLabelAvgWKUN: TQRLabel;
    QRLabelWKDayUN: TQRLabel;
    ChildBandFooterWK2: TQRChildBand;
    QRLabelUnknownBU2: TQRLabel;
    QRLabelWKHrsUN2: TQRLabel;
    QRLabelAvgWKUN2: TQRLabel;
    QRLabelWKDayUN2: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJOBAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDJobAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText13Print(sender: TObject; var Value: String);
    procedure ChildBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandFooterWK2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterWK2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    Average: Integer; // MR:17-12-2002
    AverageUnknown: Integer;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    LevelPlant, LevelBU, LevelDept,
    LevelWK, LevelJob, LevelEmpl, WKHasJobDummy: Boolean;
    LastLevel: Char;
    TotalEmpl, TotalBU, TotalWK, TotalJob, TotalDept, TotalPlant,
    FDaysBU, FDaysDept, FDaysPlant, FDaysWK, FDaysJob, FDaysEmpl: Integer;
    AllPlants, AllBusinessUnits: Boolean;
    TotalWKUnknown, TotalJobUnknown: Integer;
    FDaysWKUnknown, FDaysJobUnknown: Integer;
    WorkedDaysSelectStr: String; // 20013489
    procedure SetLevelReport;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob: Boolean;
      const ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure AddAmounts(Amount: Integer; BU, Dept, WK, Job, Empl: Boolean);
    function DetermineSUMMIN: Integer;
    function DetermineWorkedDays(ALevel: String): Integer;
  end;

var
  ReportHrsPerWKCUMQR: TReportHrsPerWKCUMQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportHrsPerWKCUMDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes;

// MR:17-12-2002
procedure ExportDetail(
  Workspot, WorkspotCode, WorkspotDescription, TotalHours,
  AveragePerDay, WorkingDay: String);
begin
  ExportClass.AddText(
    Workspot + ExportClass.Sep +
    WorkspotCode + ExportClass.Sep +
    WorkspotDescription + ExportClass.Sep +
    TotalHours + ExportClass.Sep +
    AveragePerDay + ExportClass.Sep +
    WorkingDay
    );
end;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo: String;
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob: Boolean;
      ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FWKFrom := WKFrom;
  FWKTo := WKTo;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FShowBU := ShowBU;
  FShowDept := ShowDept;
  FShowWK := ShowWK;
  FShowJob := ShowJob;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FPageJob := PageJob;
  FExportToFile := ExportToFile;
  FAllEmployees := AllEmployees;
  FIncludeNotConnectedEmp := IncludeNotConnectedEmp;
end;

function TReportHrsPerWKCUMQR.QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl, ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob: Boolean;
      const ExportToFile, AllEmployees, IncludeNotConnectedEmp: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
      ExportToFile, AllEmployees, IncludeNotConnectedEmp);
  end;
  SetDataSetQueryReport(ReportHrsPerWKCUMDM.QueryProdHour);
end;

function TReportHrsPerWKCUMQR.ExistsRecords: Boolean;
var
  SelectStr, SelectStr1, SelectStr2, SelectStr3, TempStr: String;
  SelectEmployeeStr: String;
//  DateTmp: TDateTime;
  // 20013489 Bugfix.
  function BuildSelectStatement(AOnlyWorkspot: Boolean): String;
  begin
    TempStr :=
      'SELECT ' + NL +
      '  P.PLANT_CODE, PL.DESCRIPTION AS PDESC ' + NL;
    SelectStr1 := TempStr;
    SelectStr2 := TempStr;
    SelectStr3 := TempStr;
    if QRParameters.FShowBU then
    begin
    //
      TempStr :=
        ', J.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      TempStr :=
        ', BW.BUSINESSUNIT_CODE, B.DESCRIPTION AS BDESC ' + NL;
      SelectStr2 := SelectStr2 + TempStr;
      TempStr :=
        ', J.BUSINESSUNIT_CODE, CAST ('''' AS VARCHAR(30)) as BDESC ' + NL;
      SelectStr3 := SelectStr3 + TempStr;
    //
    end;
    if QRParameters.FShowDept then
    begin
      TempStr :=
        ', W.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if QRParameters.FShowWK then
    begin
      TempStr :=
        ', P.WORKSPOT_CODE, W.DESCRIPTION AS WDESC ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if (not AOnlyWorkspot) and QRParameters.FShowJob then
    begin
    //
      TempStr :=
        ', P.JOB_CODE, J.DESCRIPTION AS JDESC ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      TempStr :=
        ', P.JOB_CODE, CAST ('''' AS VARCHAR(30)) AS JDESC ' + NL;
      SelectStr2 := SelectStr2 + TempStr;
      TempStr :=
        ', P.JOB_CODE, CAST ('''' AS VARCHAR(30)) AS JDESC ' + NL;
      SelectStr3 := SelectStr3 + TempStr;
    //
    end;
    if (not AOnlyWorkspot) and QRParameters.FShowEmpl then
    begin
      TempStr :=
        ', P.EMPLOYEE_NUMBER ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    // MR:06-10-2005 The 'businessunit' is always needed for determination
    // of the Businessunitperworkspot.percentage.
    // But if it is put in the 'SELECT' (when FShowBU is False),
    // this will trigger the Workspot, even if the plant+workspot is the same,
    // if there are 2 same workspots for 2 different businessunits.
    // By putting the businessunitcode as a 'max'-value this can be solved.
    TempStr :=
      ', P.PRODHOUREMPLOYEE_DATE ' + NL +
      ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    // RV067.MRA.2. 550496.
    // MRA:21-JUL-2010 Calculate the total minutes here!
    TempStr :=
      ', P.PRODHOUREMPLOYEE_DATE ' + NL +
      ', SUM(P.PRODUCTION_MINUTE * BW.PERCENTAGE / 100) AS SUMMIN ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      ', P.PRODHOUREMPLOYEE_DATE ' + NL +
      ', SUM(P.PRODUCTION_MINUTE) AS SUMMIN ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
    //
    TempStr :=
      ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
      ', MAX(1) AS LISTCODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      ', MAX(BW.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
      ', MAX(2) AS LISTCODE ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
      ', MAX(3) AS LISTCODE ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
    //
    // MR:21-1-2005 should be '< :FENDDATE' instead of '<= :FENDDATE'!,
    // because of 'enddate + 1'.
    TempStr :=
      'FROM ' + NL +
      '  PRODHOURPEREMPLOYEE P INNER JOIN PLANT PL ON ' + NL +
      '    P.PLANT_CODE = PL.PLANT_CODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    if not AllPlants then
    begin
      TempStr :=
        '    AND P.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '    AND P.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    TempStr :=
      '    AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '    AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL +
      '  INNER JOIN WORKSPOT W ON ' + NL +
      '    P.PLANT_CODE = W.PLANT_CODE ' + NL +
      '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    //
    TempStr :=
      '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
      '    AND (P.JOB_CODE <> ''' + DUMMYSTR + ''')' + NL +
      '  INNER JOIN JOBCODE J ON ' + NL +
      '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
      '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
      '    AND P.JOB_CODE = J.JOB_CODE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      '    AND W.USE_JOBCODE_YN = ''N'' ' + NL +
      '  INNER JOIN BUSINESSUNITPERWORKSPOT BW ON' + NL +
      '    BW.PLANT_CODE = P.PLANT_CODE ' + NL +
      '    AND BW.WORKSPOT_CODE = P.WORKSPOT_CODE ' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    TempStr :=
      '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
      '    AND (P.JOB_CODE = ''' + DUMMYSTR + ''')' + NL +
      '  LEFT JOIN JOBCODE J ON ' + NL + // Left Join -> Jobs do not exist
      '    P.PLANT_CODE = J.PLANT_CODE ' + NL +
      '    AND P.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
      '    AND P.JOB_CODE = J.JOB_CODE ' + NL;
    SelectStr3 := SelectStr3 + TempStr;
    //
    if QRParameters.FShowDept then
    begin
      TempStr :=
        '  INNER JOIN DEPARTMENT D ON ' + NL +
        '    W.PLANT_CODE = D.PLANT_CODE ' + NL +
        '    AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if QRParameters.FShowBU then
    begin
    //
      TempStr :=
        '  INNER JOIN BUSINESSUNIT B ON ' + NL +
        '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      TempStr :=
        '  INNER JOIN BUSINESSUNIT B ON ' + NL +
        '    BW.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
      SelectStr2 := SelectStr2 + TempStr;
      TempStr :=
        '  LEFT JOIN BUSINESSUNIT B ON ' + NL + // Left Join -> Jobs do not exist
        '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
      SelectStr3 := SelectStr3 + TempStr;
    //
    end;
    // This join gives an 'endless loop' (?)
  (*
    if QRParameters.FShowEmpl then
    begin
      TempStr :=
        '  INNER JOIN EMPLOYEE E ON ' + NL +
        '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
  *)
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
    //
      TempStr :=
        'WHERE ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
      if not AllBusinessUnits then
      begin
        TempStr :=
          '  J.BUSINESSUNIT_CODE >= ''' +
          DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
          '  AND J.BUSINESSUNIT_CODE <= ''' +
          DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
        SelectStr1 := SelectStr1 + TempStr;
      end;
      if not AllBusinessUnits then
      begin
        TempStr :=
          '  BW.BUSINESSUNIT_CODE >= ''' +
          DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
          '  AND BW.BUSINESSUNIT_CODE <= ''' +
          DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
        SelectStr2 := SelectStr2 + TempStr;
      end;
      // SelectStr3 -> There are no jobs / no businesunits.
      //
      if not AllBusinessUnits then
      begin
        TempStr := ' AND ';
        SelectStr1 := SelectStr1 + TempStr;
        SelectStr2 := SelectStr2 + TempStr;
        // SelectStr3 -> There are no jobs / no businessunits.
      end;
      // RV050.4.
      if QRParameters.FAllEmployees then
        SelectEmployeeStr := ''
      else
        SelectEmployeeStr :=
          '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
          '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
          '  AND ';
      TempStr := SelectEmployeeStr +
        ' W.DEPARTMENT_CODE >= ''' +
        DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
        ' AND W.DEPARTMENT_CODE <= ''' +
        DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
        ' AND P.WORKSPOT_CODE >= ''' +
        DoubleQuote(QRParameters.FWKFrom) + '''' + NL +
        ' AND P.WORKSPOT_CODE <= ''' +
        DoubleQuote(QRParameters.FWKTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
      if (QRParameters.FWKFrom = QRParameters.FWKTo) then
      begin
        TempStr :=
          ' AND P.JOB_CODE >= ''' +
          DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
          ' AND P.JOB_CODE <= ''' +
          DoubleQuote(QRParameters.FJobTo) + '''' + NL;
        SelectStr1 := SelectStr1 + TempStr;
        if (QRParameters.FJobTo <> '') and (QRParameters.FJobFrom <> '') then
        begin
          SelectStr2 := SelectStr2 + TempStr;
          SelectStr3 := SelectStr3 + TempStr;
        end;
      end;
    end;
    TempStr := ' ' +
      'GROUP BY ' + NL +
      '  P.PLANT_CODE, PL.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    if QRParameters.FShowBU then
    begin
    //
      TempStr :=
        ', J.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      TempStr :=
        ', BW.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
      SelectStr2 := SelectStr2 + TempStr;
      TempStr :=
        ', J.BUSINESSUNIT_CODE, B.DESCRIPTION ' + NL;
      SelectStr3 := SelectStr3 + TempStr;
    //
    end;
    if QRParameters.FShowDept then
    begin
      TempStr :=
        ', W.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if QRParameters.FShowWK then
    begin
      TempStr :=
        ', P.WORKSPOT_CODE, W.DESCRIPTION ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if (not AOnlyWorkspot) and QRParameters.FShowJob then
    begin
      TempStr :=
        ', P.JOB_CODE, J.DESCRIPTION ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      TempStr :=
        ', P.JOB_CODE ' + NL; // Oracle->Leave out J.DESCRIPTION
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    if (not AOnlyWorkspot) and QRParameters.FShowEmpl then
    begin
      TempStr :=
        ', P.EMPLOYEE_NUMBER ' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
    TempStr :=
      ', P.PRODHOUREMPLOYEE_DATE ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
    SelectStr := SelectStr1 + ' UNION ' + NL +
      SelectStr2 + ' UNION ' + NL +
      SelectStr3 + NL;
    Result := SelectStr;
  end; // BuildSelectStatement
begin
  Screen.Cursor := crHourGlass;
  SetLevelReport;

  AllPlants := ReportHrsPerWKCUMDM.DetermineAllPlants(
    QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllBusinessUnits := ReportHrsPerWKCUMDM.DetermineAllBusinessUnits(
    QRParameters.FBusinessFrom, QRParameters.FBusinessTo);

  // 20013489 Build a select statement with only workspots (and b.u. and dept.)
  //          This is used to count the working days.
  SelectStr := BuildSelectStatement(True);
  WorkedDaysSelectStr := SelectStr;
  with ReportHrsPerWKCumDM.QueryWSWorkingDays do
  begin
    Active := False;
    Unidirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
    ParamByName('FSTARTDATE').Value := GetDate(QRParameters.FDateFrom);
    ParamByName('FENDDATE').Value := GetDate(QRParameters.FDateTo + 1);
    if not Prepared then
      Prepare;
    Active := True;
//    Result := (not IsEmpty);
  end;

  // 20013489 Build a select statement with all
  SelectStr := BuildSelectStatement(False);
  with ReportHrsPerWKCumDM.QueryProdHour do
  begin
    Active := False;
    Unidirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
// !!!TESTING!!! START
{
if QRParameters.FShowJob and QRParameters.FShowEmpl then
  SQL.SaveToFile('c:\temp\rephrsperwkcum-job-emp.sql')
else
  if QRParameters.FShowJob then
    SQL.SaveToFile('c:\temp\rephrsperwkcum-job.sql')
  else
    if QRParameters.FShowEmpl then
      SQL.SaveToFile('c:\temp\rephrsperwkcum-emp.sql')
    else
      SQL.SaveToFile('c:\temp\rephrsperwkcum.sql');
}      
// !!!TESTING!!! END
//CAR 12.12.2002
    ParamByName('FSTARTDATE').Value := GetDate(QRParameters.FDateFrom);
    ParamByName('FENDDATE').Value := GetDate(QRParameters.FDateTo + 1);
    if not Prepared then
      Prepare;
    Active := True;
    Result := (not IsEmpty);
  end;
  if Result then
  begin
    ReportHrsPerWKCumDM.cdsBUPerWK.Close;
    ReportHrsPerWKCumDM.cdsBUPerWK.Open;
    ReportHrsPerWKCumDM.cdsEmpl.Close;
    ReportHrsPerWKCumDM.cdsEmpl.Open;
  end;
  Screen.Cursor := crDefault;
end;

function TReportHrsPerWKCUMQR.DetermineSUMMIN: Integer;
begin
  Result := ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('SUMMIN').AsInteger;
end;

procedure TReportHrsPerWKCUMQR.SetLevelReport;
begin
  LevelPlant := True;
  LevelBU := False;
  LevelDept := False;
  LevelWK := False;
  LevelJob := False;
  LevelEmpl := False;
  LastLevel := 'P';
  if QRParameters.FShowBU then
  begin
    LevelBU := True;
    LastLevel := 'B';
  end;
  if QRParameters.FShowDept then
  begin
    LevelDept := True;
    LastLevel := 'D';
  end;
  if QRParameters.FShowWK then
  begin
    LevelWK := True;
    LastLevel := 'W';
  end;
  if QRParameters.FShowJob then
  begin
    LevelJob := True;
    LastLevel := 'J';
  end;
  if QRParameters.FShowEmpl then
  begin
    LevelEmpl := True;
    LastLevel := 'E';
  end;
end;

procedure TReportHrsPerWKCUMQR.ConfigReport;
begin
  // MR:17-12-2002
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := QRParameters.FWKFrom;
  QRLabelWKTo.Caption := QRParameters.FWKTo;
  QRLabelJobCodeFrom.Caption := QRParameters.FJobFrom;
  QRLabelJobCodeTo.Caption := QRParameters.FJobTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;

  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
  if (QRParameters.FWKFrom <> QRParameters.FWKTo) then
  begin
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
  end;
  // RV050.4.
  if QRParameters.FAllEmployees then
  begin
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;
(*
  QRGroupHDBU.Enabled := False;
  QRBandFooterBU.Enabled := False;
  QRGroupHDDept.Enabled := False;
  QRBandFooterDept.Enabled := False;
  QRGroupHDWK.Enabled := False;
  QRBandFooterWK.Enabled := False;
  QRGroupHDJob.Enabled := False;
  QRBandFooterJob.Enabled := False;
  QRGroupHDEmpl.Enabled := False;
  QRBandFooterEmpl.Enabled := False;

  if QRParameters.FShowBU then
  begin
    if LastLevel <> 'B' then
      QRGroupHDBU.Enabled := True;
    QRBandFooterBU.Enabled := True;
  end;
  if QRParameters.FShowDept then
  begin
    if LastLevel <> 'D' then
      QRGroupHDDept.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FShowWK then
  begin
    if LastLevel <> WORKTIMEREDUCTIONAVAILABILITY then
      QRGroupHDWK.Enabled := True;
    QRBandFooterWK.Enabled := True;
  end;
  if QRParameters.FShowJob then
  begin
    if LastLevel <> 'J' then
      QRGroupHDJob.Enabled := True;
    QRBandFooterJob.Enabled := True;
  end;
  if QRParameters.FShowEmpl then
    QRBandFooterEmpl.Enabled := True;
*)
end;

procedure TReportHrsPerWKCUMQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportHrsPerWKCUMQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:17-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportHrsPerWKCUMQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  TotalPlant := 0;
  TotalEmpl := 0;
  TotalWK := 0;
  TotalJob := 0;
  TotalDept := 0;
  TotalBU := 0;
  FDaysPlant := 0;
  FDaysBU := 0;
  FDaysDept := 0;
  FDaysWK := 0;
  FDaysJob := 0;
  FDaysEmpl := 0;
  TotalWKUnknown := 0;
  TotalJobUnknown := 0;
  FDaysWKUnknown := 0;
  FDaysJobUnknown := 0;

  // MR:17-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(QRLabel11.Caption);
        // From Plant to Plant
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelPlantTo.Caption
          );
        // From BU to BU
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelBusinessTo.Caption
          );
        // From Dept to Dept
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelDeptTo.Caption
          );
        // From Workspot to Workspot
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel88.Caption + ' ' +
          QRLabelWKFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelWKTo.Caption
          );
        // From Jobcode to Jobcode
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelAbsenceRsn.Caption + ' ' +
          QRLabelJobCodeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelJobCodeTo.Caption
          );
        // From Employee to Employee
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' + QRLabel49.Caption + ' ' + { To }
          QRLabelEmployeeTo.Caption
          );
        // From date to date
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabelDate.Caption + ' ' +
          QRLabelDateFrom.Caption + ' ' + QRLabel2.Caption + ' ' + { To }
          QRLabelDateTo.Caption
          );
        // Column-headers
        ExportDetail('', '', '', QRLabel8.Caption, QRLabel9.Caption,
          QRLabel10.Caption);
      end;
    end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TotalWK := 0;
  FDaysWK := 0;
  TotalWKUnknown := 0;
  FDaysWKUnknown := 0;
  WKHasJobDummy := False;
  if LastLevel = 'W' then
    PrintBand := False;
  QRGroupHdWK.ForceNewPage := QRParameters.FPageWK;
(*
//  WKHasJobDummy := False;
  if LevelWK then
 // if (ReportHrsPerWKCUMDM.QueryBUPerWK.FieldByName('SUMMin').AsFloat <> 0) then
 //   WKHasJobDummy := True;
*)
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // UNKNOWN (WORKSPOT)
  PrintBand := False;
  if (LastLevel <> 'W')  then
    if (TotalWKUnknown <> 0) then
    begin
      PrintBand := True;
      QRLabelWKHrsUN.Caption := DecodeHrsMin(TotalWKUnknown);
      QRLabelAvgWKUN.Caption := '00:00';
      QRLabelWKDayUN.Caption := IntToStr(FDaysWKUnknown);
      AverageUnknown := 0;
      if FDaysWKUnknown <> 0 then
      begin
        AverageUnknown := Round(TotalWKUnknown / FDaysWKUnknown);
        QRLabelAvgWKUN.Caption := DecodeHrsMin(AverageUnknown);
      end;
    end;
end;

// MR: 10-11-2005 Workspot - Unknown Business Unit
procedure TReportHrsPerWKCUMQR.ChildBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // WORKSPOT
  if LastLevel = 'W'  then
    QRLabelTotWK.Caption := SRepHrsCUMWK
  else
  begin
    QRLabelTotWK.Caption := SRepHrsTotalCUM;
    // Also add unknown to total workspot!
    TotalWK := TotalWK + TotalWKUnknown;
  end;
(*
  if WKHasJobDummy then
    AddAmounts(DetermineSUMMIN, True, True, True,False, False);
*)
  QRLabelWKHrs.Caption := DecodeHrsMin(TotalWK);
  QRLabelAvgWK.Caption := '00:00';
  // 20013489
  FDaysWK := DetermineWorkedDays('W');
  QRLabelWKDay.Caption := IntToStr(FDaysWK);
  Average := 0;
  if FDaysWK <> 0 then
  begin
    Average := Round(TotalWK / FDaysWK);
    QRLabelAvgWK.Caption := DecodeHrsMin(Average);
  end;
end;

// MR: 10-11-2005 Workspot - Unknown Business Unit
procedure TReportHrsPerWKCUMQR.ChildBandFooterWK2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // UNKNOWN (WORKSPOT)
  PrintBand := False;
  if (LastLevel = 'W')  then
    if (TotalWKUnknown <> 0) then
    begin
      PrintBand := True;
      QRLabelWKHrsUN2.Caption := DecodeHrsMin(TotalWKUnknown);
      QRLabelAvgWKUN2.Caption := '00:00';
      QRLabelWKDayUN2.Caption := IntToStr(FDaysWKUnknown);
      AverageUnknown := 0;
      if FDaysWKUnknown <> 0 then
      begin
        AverageUnknown := Round(TotalWKUnknown / FDaysWKUnknown);
        QRLabelAvgWKUN2.Caption := DecodeHrsMin(AverageUnknown);
      end;
    end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  TotalPlant := 0;
  FDaysPlant := 0;
  QRGroupHDPlant.ForceNewPage := QRParameters.FPagePlant;
//  WKHasJobDummy := False;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if (LastLevel = 'P') then
    QRLabelTotPlant.Caption := SRepHrsCUMWKPlant
  else
    QRLabelTotPlant.Caption := SRepHrsTotalCUMPlant;
  QRLabelPlantHrs.Caption := DecodeHrsMin(TotalPlant);
  QRLabelAvgPlant.Caption := '00:00';
  // 20013489
  FDaysPlant := DetermineWorkedDays('P');
  QRLabelPlantDay.Caption := IntToStr(FDaysPlant);
  Average := 0;
  if FDaysPlant <> 0 then
  begin
    Average := Round(TotalPlant / FDaysPlant);
    QRLabelAvgPlant.Caption := DecodeHrsMin(Average);
  end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowBU;
  if PrintBand then
  begin
    TotalBU := 0;
    FDaysBU := 0;
    PrintBand := QRParameters.FShowBU;
    QRGroupHdBU.ForceNewPage := PrintBand and QRParameters.FPageBU;
  end;
(*  if LastLevel = 'B' then
    PrintBand := False; *)
//  WKHasJobDummy := False;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowBU;
  if PrintBand then
  begin
    if ( LastLevel = 'B') then
      QRLabelTotBU.Caption := SRepHrsCUMWKBU
    else
      QRLabelTotBU.Caption := SRepHrsTotalCUMBU;
    QRLabelBUHrs.Caption := DecodeHrsMin(TotalBU);
    QRLabelAvgBU.Caption := '00:00';
    // 20013489
    FDaysBU := DetermineWorkedDays('B');
    QRLabelBUDay.Caption := IntToStr(FDaysBU);
    Average := 0;
    if FDaysBU <> 0 then
    begin
      Average := Round(TotalBU / FDaysBU);
      QRLabelAvgBU.Caption := DecodeHrsMin(Average);
    end;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDept;
  if PrintBand then
  begin
    TotalDept := 0;
    FDaysDept := 0;
(*  if LastLevel = 'D' then
    PrintBand := False; *)
    QRGroupHdDept.ForceNewPage := PrintBand and QRParameters.FPageDept;
//  WKHasJobDummy := False;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDept;
  if PrintBand then
  begin
    if (LastLevel = 'D') then
      QRLabelTotDept.Caption := SRepHrsCUMWKDept
    else
      QRLabelTotDept.Caption := SRepHrsTotalCUMDept;
    QRLabelDeptHrs.Caption := DecodeHrsMin(TotalDept);
    QRLabelAvgDept.Caption := '00:00';
    // 20013489
    FDaysDept := DetermineWorkedDays('D');
    QRLabelDeptDay.Caption := IntToStr(FDaysDept);
    Average := 0;
    if FDaysDept <> 0 then
    begin
      Average := Round(TotalDept / FDaysDept);
      QRLabelAvgDept.Caption := DecodeHrsMin(Average);
    end;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  PrintBand := QRParameters.FShowJob;
  if PrintBand then
  begin
    TotalJob := 0;
    FDaysJob := 0;
    TotalJobUnknown := 0;
    FDaysJobUnknown := 0;
    if Lastlevel = 'J' then
      PrintBand := False;
    if PrintBand then
      if (ReportHrsPerWKCUMDM.
        QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
        (ReportHrsPerWKCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
      begin
        PrintBand := False;
        if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
          (not AllBusinessUnits)) then
          PrintBand := True;
      end;
    QRGroupHDJob.ForceNewPage := PrintBand and QRParameters.FPageJob;
  end;
  inherited;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowJob;
  if PrintBand then
  begin
    if (ReportHrsPerWKCUMDM.
      QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
      (ReportHrsPerWKCUMDM.
      QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
    begin
      PrintBand := False;
      if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
        (not AllBusinessUnits)) then
        PrintBand := True;
    end;
    if PrintBand then
    begin
      if LastLevel = 'J' then
        QRLabelTotJob.Caption := SRepJobCode
      else
        QRLabelTotJob.Caption := SRepTotJobCode;
      QRLabelJobHrs.Caption := DecodeHrsMin(TotalJob);
      // 20013489 Do not show/calculate average/working days on job-level!
//      QRLabelAvgJob.Caption := '00:00';
//      QRLabelJobDay.Caption := IntToStr(FDaysJob);
      QRLabelAvgJob.Caption := '';
      QRLabelJobDay.Caption := '';
{
      Average := 0;
      if FDaysJob <> 0 then
      begin
        Average := Round(TotalJob / FDaysJob);
        QRLabelAvgEmpl.Caption := DecodeHrsMin(Average);
      end;
}
    end;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // Never show this group!
  TotalEmpl := 0;
  FDaysEmpl := 0;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowEmpl;
  if PrintBand then
  begin
    if QRParameters.FShowJob then
    begin
      if (ReportHrsPerWKCUMDM.
        QueryProdHour.FieldByName('JOB_CODE').AsString = DummyStr) and
        (ReportHrsPerWKCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) then
      begin
        PrintBand := False;
        if not ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
          (not AllBusinessUnits)) then
          PrintBand := True;
      end;
    end;
    if PrintBand then
    begin
      QRLabelEmplHrs.Caption := DecodeHrsMin(TotalEmpl);
      // 20013489 Do not show average and working-days on employee-level!
//      QRLabelAvgEmpl.Caption := '00:00';
//      QRLabelEmplDay.Caption := IntToStr(FDaysEmpl);
      QRLabelAvgEmpl.Caption := '';
      QRLabelEmplDay.Caption := '';
      // 20013489 Do not calculate average on employee-level!
{
      Average := 0;
      if FDaysEmpl <> 0 then
      begin
        Average := Round(TotalEmpl / FDaysEmpl);
        QRLabelAvgEmpl.Caption := DecodeHrsMin(Average);
      end;
}      
    end;
  end;
end;

procedure TReportHrsPerWKCUMQR.AddAmounts(Amount: Integer; BU, Dept, WK, Job,
  Empl: Boolean);
var
  Unknown: Boolean;
begin
  // MR: If ONE Plant and NOT ALL BusinessUnits are selected,
  //     determine 'unknown', and show this on workspot-level.
  //     Because this can be of another BusinessUnit.
  Unknown :=
    (
      (ReportHrsPerWKCUMDM.
        QueryProdHour.FieldByName('LISTCODE').AsInteger = 3) and
         ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
           (not AllBusinessUnits))
    );
  if BU then
  begin
    TotalBU := TotalBU + Amount;
    Inc(FDaysBU);
  end;
  if Dept then
  begin
    TotalDept:= TotalDept + Amount;
    Inc(FDaysDept);
  end;
  if WK then
  begin
    if not Unknown then
    begin
      TotalWK := TotalWK + Amount;
      Inc(FDaysWK);
    end
    else
    begin
      TotalWKUnknown := TotalWKUnknown + Amount;
      Inc(FDaysWKUnknown);
    end;
  end;
  if Job then
  begin
    if not Unknown then
    begin
      TotalJob := TotalJob + Amount;
      Inc(FDaysJob);
    end
    else
    begin
      TotalJobUnknown := TotalJobUnknown + Amount;
      TotalJob := TotalJob + Amount;
      Inc(FDaysJobUnknown);
    end;
  end;
  if Empl then
  begin
    if (not Unknown) then
    begin
      TotalEmpl := TotalEmpl + Amount;
      Inc(FDaysEmpl);
    end;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRBandDetailBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  SumMin: Integer;
begin
  inherited;
  PrintBand := False;
  SumMin := DetermineSUMMIN;
  TotalPlant := TotalPlant + SumMin;
  Inc(FDaysPlant);
  // 20013489 Do not AddAmounts on employee- and/or job-level
  case LastLevel of
    'B': AddAmounts(SumMin, True, False, False, False, False);
    'D': AddAmounts(SumMin, True, True, False, False, False);
    'W': AddAmounts(SumMin, True, True, True, False, False);
    // 20013489
//    'J',
//    'E': if TotalWK = 0 then
//           AddAmounts(SumMin, True, True, True, False, False);
    'J':  AddAmounts(SumMin, True, True, True, True, False);
    'E':  AddAmounts(SumMin, True, True, True, True, True);
   end;
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotBU.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('BDESC').AsString,
        IntToStr(TotalBU),
        IntToSTr(Average),
        IntToSTr(FDaysBU)
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotDept.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('DDESC').AsString,
        IntToStr(TotalDept),
        IntToStr(Average),
        IntToStr(FDaysDept)
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // UNKNOWN (WORKSPOT)
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelWKUnknownBU.Caption,
        '',
        '',
        IntToStr(TotalWKUnknown),
        IntToStr(AverageUnknown),
        IntToStr(FDaysWKUnknown)
        );
end;

// MR:10-11-2005 Workspot Unknown
procedure TReportHrsPerWKCUMQR.ChildBandFooterWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // WORKSPOT
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotWK.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('WDESC').AsString,
        IntToStr(TotalWK),
        IntToStr(Average),
        IntToStr(FDaysWK)
        );
end;

procedure TReportHrsPerWKCUMQR.ChildBandFooterWK2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelWKUnknownBU.Caption,
        '',
        '',
        IntToStr(TotalWKUnknown),
        IntToStr(AverageUnknown),
        IntToStr(FDaysWKUnknown)
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterJOBAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotJob.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('JDESC').AsString,
        IntToStr(TotalJob),
        // 20013489 Do not export avarage and worked days on job-level
        '', // IntToStr(Average),
        ''  // IntToStr(FDaysJob)
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel106.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsString,
        ReportHrsPerWKCUMDM.cdsEmpl.FieldByName('DESCRIPTION').AsString,
        IntToStr(TotalEmpl),
        // 20013489 Do not export average and worked days on employee-level
        '', // IntToStr(Average),
        ''  // IntToStr(FDaysEmpl)
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandSummaryAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel55.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('BDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel54.Caption,
      ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
      ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('PDESC').AsString,
      '',
      '',
      ''
      );
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel35.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('DDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel3.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('WDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerWKCUMQR.QRGroupHDJobAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabel6.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('JDESC').AsString,
        '',
        '',
        ''
        );
end;

procedure TReportHrsPerWKCUMQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-12-2002
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        QRLabelTotPlant.Caption,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
        ReportHrsPerWKCUMDM.QueryProdHour.FieldByName('PDESC').AsString,
        IntToStr(TotalPlant),
        IntToStr(Average),
        IntToStr(FDaysPlant)
        );
end;

procedure TReportHrsPerWKCUMQR.QRDBText13Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  with ReportHrsPerWKCUMDM do
  begin
    if cdsEmpl.Locate('EMPLOYEE_NUMBER',
      VarArrayOf([QueryProdHour.
      FieldByName('EMPLOYEE_NUMBER').AsInteger]), []) then
        Value := Copy(cdsEmpl.FieldByName('DESCRIPTION').AsString, 0,25);
  end;
end;

// 20013489 Determine worked days based on a special query that only
//          contains data on plant/b.u./dept/workspot-level.
//          Reason: When using employee/job-level this is WRONG!
function TReportHrsPerWKCUMQR.DetermineWorkedDays(ALevel: String): Integer;
var
  FilterStr: String;
  SelectStr: String;
begin
  Result := 0;
  try
    with ReportHrsPerWKCUMDM do
    begin
      with QueryWSWorkingDays do
      begin
        Close;
        SQL.Clear;
        // First add original query-string.
        SQL.Add(WorkedDaysSelectStr);
        // Make a new query to get
        // only unique combination of level + prod-date
        case ALevel[1] of
        'P':
          SelectStr :=
            'SELECT PLANT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
            'FROM ' + NL +
            '(' + NL +
            SQL.Text + NL +
            ') ' +
            'GROUP BY PLANT_CODE, PRODHOUREMPLOYEE_DATE ' +
            'ORDER BY PLANT_CODE, PRODHOUREMPLOYEE_DATE';
        'B':
          SelectStr :=
            'SELECT PLANT_CODE, BUSINESSUNIT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
            'FROM ' + NL +
            '(' + NL +
            SQL.Text + NL +
            ') ' +
            'GROUP BY PLANT_CODE, BUSINESSUNIT_CODE, PRODHOUREMPLOYEE_DATE ' +
            'ORDER BY PLANT_CODE, BUSINESSUNIT_CODE, PRODHOUREMPLOYEE_DATE';
        'D':
          if QRParameters.FShowBU and QRParameters.FShowDept then
            SelectStr :=
              'SELECT PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
              'FROM ' + NL +
              '(' + NL +
              SQL.Text + NL +
              ') ' +
              'GROUP BY PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE ' +
              'ORDER BY PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE'
          else
            SelectStr :=
              'SELECT PLANT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
              'FROM ' + NL +
              '(' + NL +
              SQL.Text + NL +
              ') ' +
              'GROUP BY PLANT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE ' +
              'ORDER BY PLANT_CODE, DEPARTMENT_CODE, PRODHOUREMPLOYEE_DATE';
        'W':
          if QRParameters.FShowBU and QRParameters.FShowDept then
            SelectStr :=
              'SELECT PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
              'FROM ' + NL +
              '(' + NL +
              SQL.Text + NL +
              ') ' +
              'GROUP BY PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' +
              'ORDER BY PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE'
          else
            if QRParameters.FShowBU and (not QRParameters.FShowDept) then
              SelectStr :=
                'SELECT PLANT_CODE, BUSINESSUNIT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
                'FROM ' + NL +
                '(' + NL +
                SQL.Text + NL +
                ') ' +
                'GROUP BY PLANT_CODE, BUSINESSUNIT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' +
                'ORDER BY PLANT_CODE, BUSINESSUNIT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE'
            else
              if (not QRParameters.FShowBU) and (QRParameters.FShowDept) then
                SelectStr :=
                  'SELECT PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
                  'FROM ' + NL +
                  '(' + NL +
                  SQL.Text + NL +
                  ') ' +
                  'GROUP BY PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' +
                  'ORDER BY PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE'
              else
                SelectStr :=
                  'SELECT PLANT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' + NL +
                  'FROM ' + NL +
                  '(' + NL +
                  SQL.Text + NL +
                  ') ' +
                  'GROUP BY PLANT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE ' +
                  'ORDER BY PLANT_CODE, WORKSPOT_CODE, PRODHOUREMPLOYEE_DATE'
        end;
        SQL.Clear;
        SQL.Add(SelectStr);

        // Now filter on the query
        case ALevel[1] of
        'P': // Plant
          FilterStr :=
            'PLANT_CODE = ' +
            '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''';
        'B': // B.U.
          FilterStr :=
            'PLANT_CODE = ' +
            '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
            ' AND BUSINESSUNIT_CODE = ' +
            '''' + QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString + '''';
        'D': // Dept
          if QRParameters.FShowBU and QRParameters.FShowDept then
            FilterStr :=
              'PLANT_CODE = ' +
              '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
              ' AND BUSINESSUNIT_CODE = ' +
              '''' + QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString + '''' +
              ' AND DEPARTMENT_CODE = ' +
              '''' + QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString + ''''
          else
            FilterStr :=
              'PLANT_CODE = ' +
              '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
              ' AND DEPARTMENT_CODE = ' +
              '''' + QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString + '''';
        'W': // Workspot
          if QRParameters.FShowBU and QRParameters.FShowDept then
            FilterStr :=
              'PLANT_CODE = ' +
              '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
              ' AND BUSINESSUNIT_CODE = ' +
              '''' + QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString + '''' +
              ' AND DEPARTMENT_CODE = ' +
              '''' + QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString + '''' +
              ' AND WORKSPOT_CODE = ' +
              '''' + QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + ''''
          else
            if QRParameters.FShowBU and (not QRParameters.FShowDept) then
              FilterStr :=
                'PLANT_CODE = ' +
                '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
                ' AND BUSINESSUNIT_CODE = ' +
                '''' + QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString + '''' +
                ' AND WORKSPOT_CODE = ' +
                '''' + QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + ''''
            else
              if (not QRParameters.FShowBU) and QRParameters.FShowDept then
                FilterStr :=
                  'PLANT_CODE = ' +
                  '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
                  ' AND DEPARTMENT_CODE = ' +
                  '''' + QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString + '''' +
                  ' AND WORKSPOT_CODE = ' +
                  '''' + QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + ''''
            else
              if (not QRParameters.FShowBU) and (not QRParameters.FShowDept) then
                FilterStr :=
                  'PLANT_CODE = ' +
                  '''' + QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' +
                  ' AND WORKSPOT_CODE = ' +
                  '''' + QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + '''';
        end;
        // !!!TESTING!!! START
  //      SQL.SaveToFile('c:\temp\queryWSWorkedDays_' + ALevel + '.sql');
        // !!!TESTING!!! END
        Close;
        ParamByName('FSTARTDATE').Value := GetDate(QRParameters.FDateFrom);
        ParamByName('FENDDATE').Value := GetDate(QRParameters.FDateTo + 1);
        Filtered := False;
        Filter := FilterStr;
        Filtered := True;
        Open;
        if not Eof then
          Result := RecordCount;
        Close;
      end;
    end;
  except
    Result := 0;
  end;
end; // DetermineWorkedDays

end.
