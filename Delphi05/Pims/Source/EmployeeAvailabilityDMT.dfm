inherited EmployeeAvailabilityDM: TEmployeeAvailabilityDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 326
  Top = 120
  Height = 638
  Width = 794
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    Left = 32
    Top = 8
  end
  inherited TableDetail: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnEditError = nil
    OnNewRecord = nil
    MasterSource = nil
    Left = 344
    Top = 12
  end
  inherited DataSourceMaster: TDataSource
    DataSet = QueryEmpl
    Left = 128
    Top = 8
  end
  inherited DataSourceDetail: TDataSource
    DataSet = TablePivot
    Left = 128
    Top = 60
  end
  inherited TableExport: TTable
    Left = 260
    Top = 436
  end
  inherited DataSourceExport: TDataSource
    Left = 336
    Top = 436
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 336
    Top = 288
  end
  object TableTempPivot: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 32
    Top = 248
  end
  object StoredProcEMA: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'EMPLAVAILABLE_FILLPIVOTEMA'
    Left = 124
    Top = 248
    ParamData = <
      item
        DataType = ftString
        Name = 'PIVOTCOMPUTERNAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'YEAR_NR'
        ParamType = ptInput
      end>
  end
  object TablePivot: TTable
    BeforeDelete = DefaultBeforeDelete
    AfterDelete = TableEMAPIVOTAfterDelete
    AfterScroll = TableEMAPIVOTAfterScroll
    OnNewRecord = TableEMAPIVOTNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PIVOTCOMPUTERNAME;EMPLOYEE_NR;YEAR_NR;WEEK_NR'
    TableName = 'PIVOTEMPAVAILABLE'
    Left = 32
    Top = 64
    object TablePivotPIVOTCOMPUTERNAME: TStringField
      FieldName = 'PIVOTCOMPUTERNAME'
      Required = True
      Size = 30
    end
    object TablePivotEMPLOYEE_NR: TIntegerField
      FieldName = 'EMPLOYEE_NR'
      Required = True
    end
    object TablePivotYEAR_NR: TIntegerField
      FieldName = 'YEAR_NR'
      Required = True
    end
    object TablePivotWEEK_NR: TIntegerField
      FieldName = 'WEEK_NR'
      Required = True
    end
    object TablePivotDAY11: TStringField
      DisplayWidth = 1
      FieldName = 'DAY11'
      Size = 1
    end
    object TablePivotDAY12: TStringField
      FieldName = 'DAY12'
      Size = 1
    end
    object TablePivotDAY13: TStringField
      FieldName = 'DAY13'
      Size = 1
    end
    object TablePivotDAY14: TStringField
      FieldName = 'DAY14'
      Size = 1
    end
    object TablePivotDAY21: TStringField
      FieldName = 'DAY21'
      Size = 1
    end
    object TablePivotDAY22: TStringField
      FieldName = 'DAY22'
      Size = 1
    end
    object TablePivotDAY23: TStringField
      FieldName = 'DAY23'
      Size = 1
    end
    object TablePivotDAY24: TStringField
      FieldName = 'DAY24'
      Size = 1
    end
    object TablePivotDAY31: TStringField
      FieldName = 'DAY31'
      Size = 1
    end
    object TablePivotDAY32: TStringField
      FieldName = 'DAY32'
      Size = 1
    end
    object TablePivotDAY33: TStringField
      FieldName = 'DAY33'
      Size = 1
    end
    object TablePivotDAY34: TStringField
      FieldName = 'DAY34'
      Size = 1
    end
    object TablePivotDAY41: TStringField
      FieldName = 'DAY41'
      Size = 1
    end
    object TablePivotDAY42: TStringField
      FieldName = 'DAY42'
      Size = 1
    end
    object TablePivotDAY43: TStringField
      FieldName = 'DAY43'
      Size = 1
    end
    object TablePivotDAY44: TStringField
      FieldName = 'DAY44'
      Size = 1
    end
    object TablePivotDAY51: TStringField
      FieldName = 'DAY51'
      Size = 1
    end
    object TablePivotDAY52: TStringField
      FieldName = 'DAY52'
      Size = 1
    end
    object TablePivotDAY53: TStringField
      FieldName = 'DAY53'
      Size = 1
    end
    object TablePivotDAY54: TStringField
      FieldName = 'DAY54'
      Size = 1
    end
    object TablePivotDAY61: TStringField
      FieldName = 'DAY61'
      Size = 1
    end
    object TablePivotDAY62: TStringField
      FieldName = 'DAY62'
      Size = 1
    end
    object TablePivotDAY63: TStringField
      FieldName = 'DAY63'
      Size = 1
    end
    object TablePivotDAY64: TStringField
      FieldName = 'DAY64'
      Size = 1
    end
    object TablePivotDAY71: TStringField
      FieldName = 'DAY71'
      Size = 1
    end
    object TablePivotDAY72: TStringField
      FieldName = 'DAY72'
      Size = 1
    end
    object TablePivotDAY73: TStringField
      FieldName = 'DAY73'
      Size = 1
    end
    object TablePivotDAY74: TStringField
      FieldName = 'DAY74'
      Size = 1
    end
    object TablePivotDAY15: TStringField
      FieldName = 'DAY15'
      Size = 2
    end
    object TablePivotDAY25: TStringField
      FieldName = 'DAY25'
      Size = 2
    end
    object TablePivotDAY35: TStringField
      FieldName = 'DAY35'
      Size = 2
    end
    object TablePivotDAY45: TStringField
      FieldName = 'DAY45'
      Size = 2
    end
    object TablePivotDAY55: TStringField
      FieldName = 'DAY55'
      Size = 2
    end
    object TablePivotDAY65: TStringField
      FieldName = 'DAY65'
      Size = 2
    end
    object TablePivotDAY75: TStringField
      FieldName = 'DAY75'
      Size = 2
    end
    object TablePivotPLANT1: TStringField
      FieldName = 'PLANT1'
      Size = 6
    end
    object TablePivotPLANT2: TStringField
      FieldName = 'PLANT2'
      Size = 6
    end
    object TablePivotPLANT3: TStringField
      FieldName = 'PLANT3'
      Size = 6
    end
    object TablePivotPLANT4: TStringField
      FieldName = 'PLANT4'
      Size = 6
    end
    object TablePivotPLANT5: TStringField
      FieldName = 'PLANT5'
      Size = 6
    end
    object TablePivotPLANT6: TStringField
      FieldName = 'PLANT6'
      Size = 6
    end
    object TablePivotPLANT7: TStringField
      FieldName = 'PLANT7'
      Size = 6
    end
    object TablePivotDAY16: TStringField
      FieldName = 'DAY16'
      Size = 4
    end
    object TablePivotDAY17: TStringField
      FieldName = 'DAY17'
      Size = 4
    end
    object TablePivotDAY18: TStringField
      FieldName = 'DAY18'
      Size = 4
    end
    object TablePivotDAY19: TStringField
      FieldName = 'DAY19'
      Size = 4
    end
    object TablePivotDAY110: TStringField
      FieldName = 'DAY110'
      Size = 4
    end
    object TablePivotDAY111: TStringField
      FieldName = 'DAY111'
      Size = 8
    end
    object TablePivotDAY26: TStringField
      FieldName = 'DAY26'
      Size = 4
    end
    object TablePivotDAY27: TStringField
      FieldName = 'DAY27'
      Size = 4
    end
    object TablePivotDAY28: TStringField
      FieldName = 'DAY28'
      Size = 4
    end
    object TablePivotDAY29: TStringField
      FieldName = 'DAY29'
      Size = 4
    end
    object TablePivotDAY210: TStringField
      FieldName = 'DAY210'
      Size = 4
    end
    object TablePivotDAY211: TStringField
      FieldName = 'DAY211'
      Size = 8
    end
    object TablePivotDAY36: TStringField
      FieldName = 'DAY36'
      Size = 4
    end
    object TablePivotDAY37: TStringField
      FieldName = 'DAY37'
      Size = 4
    end
    object TablePivotDAY38: TStringField
      FieldName = 'DAY38'
      Size = 4
    end
    object TablePivotDAY39: TStringField
      FieldName = 'DAY39'
      Size = 4
    end
    object TablePivotDAY310: TStringField
      FieldName = 'DAY310'
      Size = 4
    end
    object TablePivotDAY311: TStringField
      FieldName = 'DAY311'
      Size = 8
    end
    object TablePivotDAY46: TStringField
      FieldName = 'DAY46'
      Size = 4
    end
    object TablePivotDAY47: TStringField
      FieldName = 'DAY47'
      Size = 4
    end
    object TablePivotDAY48: TStringField
      FieldName = 'DAY48'
      Size = 4
    end
    object TablePivotDAY49: TStringField
      FieldName = 'DAY49'
      Size = 4
    end
    object TablePivotDAY410: TStringField
      FieldName = 'DAY410'
      Size = 4
    end
    object TablePivotDAY411: TStringField
      FieldName = 'DAY411'
      Size = 8
    end
    object TablePivotDAY56: TStringField
      FieldName = 'DAY56'
      Size = 4
    end
    object TablePivotDAY57: TStringField
      FieldName = 'DAY57'
      Size = 4
    end
    object TablePivotDAY58: TStringField
      FieldName = 'DAY58'
      Size = 4
    end
    object TablePivotDAY59: TStringField
      FieldName = 'DAY59'
      Size = 4
    end
    object TablePivotDAY510: TStringField
      FieldName = 'DAY510'
      Size = 4
    end
    object TablePivotDAY511: TStringField
      FieldName = 'DAY511'
      Size = 8
    end
    object TablePivotDAY66: TStringField
      FieldName = 'DAY66'
      Size = 4
    end
    object TablePivotDAY67: TStringField
      FieldName = 'DAY67'
      Size = 4
    end
    object TablePivotDAY68: TStringField
      FieldName = 'DAY68'
      Size = 4
    end
    object TablePivotDAY69: TStringField
      FieldName = 'DAY69'
      Size = 4
    end
    object TablePivotDAY610: TStringField
      FieldName = 'DAY610'
      Size = 4
    end
    object TablePivotDAY611: TStringField
      FieldName = 'DAY611'
      Size = 8
    end
    object TablePivotDAY76: TStringField
      FieldName = 'DAY76'
      Size = 4
    end
    object TablePivotDAY77: TStringField
      FieldName = 'DAY77'
      Size = 4
    end
    object TablePivotDAY78: TStringField
      FieldName = 'DAY78'
      Size = 4
    end
    object TablePivotDAY79: TStringField
      FieldName = 'DAY79'
      Size = 4
    end
    object TablePivotDAY710: TStringField
      FieldName = 'DAY710'
      Size = 4
    end
    object TablePivotDAY711: TStringField
      FieldName = 'DAY711'
      Size = 8
    end
  end
  object QueryCheckIsPlanned: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4,'
      '  SCHEDULED_TIMEBLOCK_5, SCHEDULED_TIMEBLOCK_6,'
      '  SCHEDULED_TIMEBLOCK_7, SCHEDULED_TIMEBLOCK_8,'
      '  SCHEDULED_TIMEBLOCK_9, SCHEDULED_TIMEBLOCK_10,'
      '  SHIFT_NUMBER'
      'FROM'
      '  EMPLOYEEPLANNING'
      'WHERE'
      '  PLANT_CODE=:PCODE AND EMPLOYEEPLANNING_DATE=:PDATE'
      '  AND EMPLOYEE_NUMBER=:EMPNO AND'
      '  (SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39','#39'B'#39', '#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39','#39'B'#39', '#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_5 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_6 IN ('#39'A'#39','#39'B'#39', '#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_7 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_8 IN ('#39'A'#39','#39'B'#39', '#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_9 IN ('#39'A'#39','#39'B'#39','#39'C'#39') OR'
      '   SCHEDULED_TIMEBLOCK_10 IN ('#39'A'#39','#39'B'#39','#39'C'#39'))'
      ''
      ' ')
    Left = 40
    Top = 368
    ParamData = <
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end>
  end
  object qryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER '
      'FROM '
      '  SHIFT'
      'ORDER BY '
      '  PLANT_CODE, SHIFT_NUMBER'
      '')
    Left = 256
    Top = 64
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE, P.DESCRIPTION AS PDESC, P.COUNTRY_ID,'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, E.SHORT_NAME,'
      '  E.TEAM_CODE , T.DESCRIPTION AS TDESC,'
      '  E.STARTDATE, E.ENDDATE,'
      '  E.DEPARTMENT_CODE'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN TEAM T ON'
      '    E.TEAM_CODE = T.TEAM_CODE'
      'WHERE'
      '  ((E.ENDDATE >= :FCURRENTDATE) OR (E.ENDDATE IS NULL))'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ''
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 256
    Top = 8
    ParamData = <
      item
        DataType = ftDate
        Name = 'FCURRENTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object QueryIlnessMsg: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MIN(ABSENCEREASON_CODE)  AS ILL '
      'FROM '
      '  ILLNESSMESSAGE '
      'WHERE'
      ' (EMPLOYEE_NUMBER=:EMPLOYEE_NUMBER) AND'
      ' (ILLNESSMESSAGE_STARTDATE <= :CURRENTDATE) AND '
      ' ((ILLNESSMESSAGE_ENDDATE >= :CURRENTDATE) OR '
      ' (ILLNESSMESSAGE_ENDDATE IS NULL))  ')
    Left = 248
    Top = 288
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CURRENTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CURRENTDATE'
        ParamType = ptUnknown
      end>
  end
  object QuerySTA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER,'
      '  DAY_OF_WEEK, AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY '
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND '
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  DAY_OF_WEEK = :DAY_OF_WEEK'
      ''
      ' ')
    Left = 128
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end>
  end
  object cdsAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspAbsRsn'
    Left = 432
    Top = 120
  end
  object dspAbsRsn: TDataSetProvider
    DataSet = qryAbsRsn
    Constraints = True
    Left = 344
    Top = 120
  end
  object qryAbsRsn: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      
        '  ABSENCEREASON_ID, ABSENCEREASON_CODE, DESCRIPTION, ABSENCETYPE' +
        '_CODE'
      'FROM '
      '  ABSENCEREASON '
      'ORDER BY '
      '  DESCRIPTION')
    Left = 256
    Top = 120
  end
  object cdsShift: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspShift'
    Left = 432
    Top = 64
  end
  object dspShift: TDataSetProvider
    DataSet = qryShift
    Constraints = True
    Left = 344
    Top = 64
  end
  object QuerySelect: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 424
    Top = 288
  end
  object QuerySelectPlanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT 1 AS RECNO,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB1,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB2,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB3,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB4,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB5,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB6,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB7,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB8,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB9,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB10,'
      '  EMA.AVAILABLE_TIMEBLOCK_1 AS EMA_TB1,'
      '  EMA.AVAILABLE_TIMEBLOCK_2 AS EMA_TB2,'
      '  EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB3,'
      '  EMA.AVAILABLE_TIMEBLOCK_4 AS EMA_TB4,'
      '  EMA.AVAILABLE_TIMEBLOCK_5 AS EMA_TB5,'
      '  EMA.AVAILABLE_TIMEBLOCK_6 AS EMA_TB6,'
      '  EMA.AVAILABLE_TIMEBLOCK_7 AS EMA_TB7,'
      '  EMA.AVAILABLE_TIMEBLOCK_8 AS EMA_TB8,'
      '  EMA.AVAILABLE_TIMEBLOCK_9 AS EMA_TB9,'
      '  EMA.AVAILABLE_TIMEBLOCK_10 AS EMA_TB10'
      'FROM'
      '  EMPLOYEEAVAILABILITY EMA'
      'WHERE'
      '  EMA.EMPLOYEE_NUMBER = :EMPNO AND'
      '  EMA.EMPLOYEEAVAILABILITY_DATE = :EDATE AND'
      '  ((EMA.PLANT_CODE = :PCODE) OR (:PCODE = '#39'*'#39')) AND'
      '  EMA.SHIFT_NUMBER = :SNO'
      'UNION'
      'SELECT 2 AS RECNO,'
      '  STD.AVAILABLE_TIMEBLOCK_1 AS STD_TB1,'
      '  STD.AVAILABLE_TIMEBLOCK_2 AS STD_TB2,'
      '  STD.AVAILABLE_TIMEBLOCK_3 AS STD_TB3,'
      '  STD.AVAILABLE_TIMEBLOCK_4 AS STD_TB4,'
      '  STD.AVAILABLE_TIMEBLOCK_5 AS STD_TB5,'
      '  STD.AVAILABLE_TIMEBLOCK_6 AS STD_TB6,'
      '  STD.AVAILABLE_TIMEBLOCK_7 AS STD_TB7,'
      '  STD.AVAILABLE_TIMEBLOCK_8 AS STD_TB8,'
      '  STD.AVAILABLE_TIMEBLOCK_9 AS STD_TB9,'
      '  STD.AVAILABLE_TIMEBLOCK_10 AS STD_TB10,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB1,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB2,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB3,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB4,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB5,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB6,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB7,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB8,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB9,'
      '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB10'
      'FROM'
      '  STANDARDAVAILABILITY STD, SHIFTSCHEDULE SHS'
      'WHERE'
      '  STD.EMPLOYEE_NUMBER = :EMPNO AND STD.DAY_OF_WEEK = :DAY AND'
      '  ((STD.PLANT_CODE = :PCODE) OR (:PCODE = '#39'*'#39')) AND'
      '  STD.SHIFT_NUMBER = :SNO AND'
      '  SHS.EMPLOYEE_NUMBER = :EMPNO AND'
      '  SHS.SHIFT_NUMBER = 0 AND SHS.SHIFT_SCHEDULE_DATE = :EDATE AND'
      '  (STD.EMPLOYEE_NUMBER  NOT IN'
      '    (SELECT'
      '       EMPLOYEE_NUMBER'
      '     FROM'
      '       EMPLOYEEAVAILABILITY'
      '     WHERE'
      '       EMPLOYEEAVAILABILITY_DATE = :EDATE AND'
      '       SHIFT_NUMBER = 0 AND'
      '       EMPLOYEE_NUMBER = :EMPNO'
      '    )'
      '  )'
      'UNION SELECT 3 AS RECNO,'
      '  STD.AVAILABLE_TIMEBLOCK_1 AS STD_TB1,'
      '  STD.AVAILABLE_TIMEBLOCK_2 AS STD_TB2,'
      '  STD.AVAILABLE_TIMEBLOCK_3 AS STD_TB3,'
      '  STD.AVAILABLE_TIMEBLOCK_4 AS STD_TB4,'
      '  STD.AVAILABLE_TIMEBLOCK_5 AS STD_TB5,'
      '  STD.AVAILABLE_TIMEBLOCK_6 AS STD_TB6,'
      '  STD.AVAILABLE_TIMEBLOCK_7 AS STD_TB7,'
      '  STD.AVAILABLE_TIMEBLOCK_8 AS STD_TB8,'
      '  STD.AVAILABLE_TIMEBLOCK_9 AS STD_TB9,'
      '  STD.AVAILABLE_TIMEBLOCK_10 AS STD_TB10,'
      '  EMA.AVAILABLE_TIMEBLOCK_1 AS EMA_TB1,'
      '  EMA.AVAILABLE_TIMEBLOCK_2 AS EMA_TB2,'
      '  EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB3,'
      '  EMA.AVAILABLE_TIMEBLOCK_4 AS EMA_TB4,'
      '  EMA.AVAILABLE_TIMEBLOCK_1 AS EMA_TB5,'
      '  EMA.AVAILABLE_TIMEBLOCK_2 AS EMA_TB6,'
      '  EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB7,'
      '  EMA.AVAILABLE_TIMEBLOCK_4 AS EMA_TB8,'
      '  EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB9,'
      '  EMA.AVAILABLE_TIMEBLOCK_4 AS EMA_TB10'
      'FROM'
      '  STANDARDAVAILABILITY STD, SHIFTSCHEDULE SHS,'
      '  EMPLOYEEAVAILABILITY EMA'
      'WHERE'
      '  STD.EMPLOYEE_NUMBER = :EMPNO AND'
      '  STD.DAY_OF_WEEK = :DAY AND'
      '  ((STD.PLANT_CODE = :PCODE) OR (:PCODE = '#39'*'#39')) AND'
      '  STD.SHIFT_NUMBER = :SNO AND'
      '  SHS.EMPLOYEE_NUMBER = :EMPNO AND'
      '  SHS.SHIFT_NUMBER = 0 AND'
      '  SHS.SHIFT_SCHEDULE_DATE = :EDATE AND'
      '  (EMA.EMPLOYEE_NUMBER = :EMPNO AND'
      '  EMA.EMPLOYEEAVAILABILITY_DATE = :EDATE AND'
      '  EMA.SHIFT_NUMBER = 0)'
      '')
    Left = 40
    Top = 424
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end>
  end
  object qryEMAInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EMPLOYEEAVAILABILITY'
      '(PLANT_CODE, EMPLOYEEAVAILABILITY_DATE, SHIFT_NUMBER,'
      'EMPLOYEE_NUMBER, AVAILABLE_TIMEBLOCK_1,'
      'AVAILABLE_TIMEBLOCK_2, AVAILABLE_TIMEBLOCK_3,'
      'AVAILABLE_TIMEBLOCK_4, AVAILABLE_TIMEBLOCK_5,'
      'AVAILABLE_TIMEBLOCK_6, AVAILABLE_TIMEBLOCK_7,'
      'AVAILABLE_TIMEBLOCK_8, AVAILABLE_TIMEBLOCK_9,'
      'AVAILABLE_TIMEBLOCK_10, CREATIONDATE, MUTATIONDATE, MUTATOR)'
      'VALUES(:PLANT_CODE, :EMPLOYEEAVAILABILITY_DATE, :SHIFT_NUMBER,'
      ':EMPLOYEE_NUMBER, :AVAILABLE_TIMEBLOCK_1,'
      ':AVAILABLE_TIMEBLOCK_2, :AVAILABLE_TIMEBLOCK_3,'
      ':AVAILABLE_TIMEBLOCK_4, :AVAILABLE_TIMEBLOCK_5,'
      ':AVAILABLE_TIMEBLOCK_6, :AVAILABLE_TIMEBLOCK_7,'
      ':AVAILABLE_TIMEBLOCK_8, :AVAILABLE_TIMEBLOCK_9,'
      ':AVAILABLE_TIMEBLOCK_10, :CREATIONDATE, :MUTATIONDATE, :MUTATOR)'
      ''
      ' '
      ' ')
    Left = 576
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryEMADelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEAVAILABILITY'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      'EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 576
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEMAUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET PLANT_CODE = :NEW_PLANT_CODE,'
      'SHIFT_NUMBER = :NEW_SHIFT_NUMBER,'
      'AVAILABLE_TIMEBLOCK_1 = :NEW_AVAILABLE_TIMEBLOCK_1,'
      'AVAILABLE_TIMEBLOCK_2 = :NEW_AVAILABLE_TIMEBLOCK_2,'
      'AVAILABLE_TIMEBLOCK_3 = :NEW_AVAILABLE_TIMEBLOCK_3,'
      'AVAILABLE_TIMEBLOCK_4 = :NEW_AVAILABLE_TIMEBLOCK_4,'
      'AVAILABLE_TIMEBLOCK_5 = :NEW_AVAILABLE_TIMEBLOCK_5,'
      'AVAILABLE_TIMEBLOCK_6 = :NEW_AVAILABLE_TIMEBLOCK_6,'
      'AVAILABLE_TIMEBLOCK_7 = :NEW_AVAILABLE_TIMEBLOCK_7,'
      'AVAILABLE_TIMEBLOCK_8 = :NEW_AVAILABLE_TIMEBLOCK_8,'
      'AVAILABLE_TIMEBLOCK_9 = :NEW_AVAILABLE_TIMEBLOCK_9,'
      'AVAILABLE_TIMEBLOCK_10 = :NEW_AVAILABLE_TIMEBLOCK_10,'
      'MUTATIONDATE = :NEW_MUTATIONDATE,'
      'MUTATOR = :NEW_MUTATOR'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      'EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 576
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'NEW_SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_AVAILABLE_TIMEBLOCK_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_AVAILABLE_TIMEBLOCK_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_AVAILABLE_TIMEBLOCK_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_AVAILABLE_TIMEBLOCK_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'NEW_AVAILABLE_TIMEBLOCK_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'NEW_MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEMADeleteRange: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EMPLOYEEAVAILABILITY'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'EMPLOYEEAVAILABILITY_DATE >= :DATEMIN AND'
      'EMPLOYEEAVAILABILITY_DATE <= :DATEMAX'
      ' '
      ' ')
    Left = 576
    Top = 352
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object qryEMA: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  EMPLOYEEAVAILABILITY_DATE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ' ')
    Left = 496
    Top = 448
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPivotEMP: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  DAY11, DAY12, DAY13, DAY14, DAY15, DAY16, DAY17, DAY18, DAY19,' +
        ' DAY110, DAY111,'
      
        '  DAY21, DAY22, DAY23, DAY24, DAY25, DAY26, DAY27, DAY28, DAY29,' +
        ' DAY210, DAY211,'
      
        '  DAY31, DAY32, DAY33, DAY34, DAY35, DAY36, DAY37, DAY38, DAY39,' +
        ' DAY310, DAY311,'
      
        '  DAY41, DAY42, DAY43, DAY44, DAY45, DAY46, DAY47, DAY48, DAY49,' +
        ' DAY410, DAY411,'
      
        '  DAY51, DAY52, DAY53, DAY54, DAY55, DAY56, DAY57, DAY58, DAY59,' +
        ' DAY510, DAY511,'
      
        '  DAY61, DAY62, DAY63, DAY64, DAY65, DAY66, DAY67, DAY68, DAY69,' +
        ' DAY610, DAY611,'
      
        '  DAY71, DAY72, DAY73, DAY74, DAY75, DAY76, DAY77, DAY78, DAY79,' +
        ' DAY710, DAY711,'
      '  PLANT1, PLANT2, PLANT3, PLANT4, PLANT5, PLANT6, PLANT7'
      'FROM'
      '  PIVOTEMPAVAILABLE'
      'WHERE'
      '  EMPLOYEE_NR = :EMPLOYEE_NR AND'
      '  YEAR_NR = :YEAR_NR AND'
      '  WEEK_NR = :WEEK_NR AND'
      '  PIVOTCOMPUTERNAME = :PIVOTCOMPUTERNAME'
      ' '
      ' ')
    Left = 32
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEAR_NR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'WEEK_NR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PIVOTCOMPUTERNAME'
        ParamType = ptUnknown
      end>
  end
  object qryEMAPlannedHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE,'
      '  A.SHIFT_NUMBER,'
      '  A.EMPLOYEEAVAILABILITY_DATE,'
      '  A.AVAILABLE_TIMEBLOCK_1,'
      '  A.AVAILABLE_TIMEBLOCK_2,'
      '  A.AVAILABLE_TIMEBLOCK_3,'
      '  A.AVAILABLE_TIMEBLOCK_4,'
      '  A.AVAILABLE_TIMEBLOCK_5,'
      '  A.AVAILABLE_TIMEBLOCK_6,'
      '  A.AVAILABLE_TIMEBLOCK_7,'
      '  A.AVAILABLE_TIMEBLOCK_8,'
      '  A.AVAILABLE_TIMEBLOCK_9,'
      '  A.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY A'
      'WHERE'
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  A.EMPLOYEEAVAILABILITY_DATE >= :EMPLOYEEAVAILABILITY_DATE AND'
      '  NOT'
      
        '    ((A.AVAILABLE_TIMEBLOCK_1 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_1 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_1 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_2 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_2 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_2 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_3 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_3 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_3 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_4 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_4 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_4 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_5 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_5 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_5 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_6 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_6 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_6 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_7 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_7 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_7 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_8 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_8 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_8 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_9 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_9 =' +
        ' '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_9 IS NULL) AND'
      
        '     (A.AVAILABLE_TIMEBLOCK_10 = '#39'*'#39' OR A.AVAILABLE_TIMEBLOCK_10' +
        ' = '#39'-'#39' OR'
      '      A.AVAILABLE_TIMEBLOCK_10 IS NULL))'
      '  '
      ' ')
    Left = 128
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end>
  end
  object qrySHSFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER,'
      '  SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER'
      'FROM'
      '  SHIFTSCHEDULE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 576
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qrySHSDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SHIFTSCHEDULE'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 576
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qrySHSInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTSCHEDULE'
      '('
      '  EMPLOYEE_NUMBER, SHIFT_SCHEDULE_DATE,'
      '  PLANT_CODE, SHIFT_NUMBER, CREATIONDATE,'
      '  MUTATIONDATE, MUTATOR'
      ')'
      'VALUES'
      '('
      '  :EMPLOYEE_NUMBER, :SHIFT_SCHEDULE_DATE,'
      '  :PLANT_CODE, :SHIFT_NUMBER, :CREATIONDATE,'
      '  :MUTATIONDATE, :MUTATOR'
      ')'
      '')
    Left = 576
    Top = 104
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qrySHSUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE SHIFTSCHEDULE'
      'SET PLANT_CODE = :PLANT_CODE,'
      'SHIFT_NUMBER = :SHIFT_NUMBER,'
      'MUTATIONDATE = :MUTATIONDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'SHIFT_SCHEDULE_DATE = :SHIFT_SCHEDULE_DATE')
    Left = 576
    Top = 152
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_SCHEDULE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryEMAFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ''
      ' ')
    Left = 576
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEMAUpdate2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY'
      'SET PLANT_CODE = :NEW_PLANT_CODE,'
      'SHIFT_NUMBER = :NEW_SHIFT_NUMBER,'
      'AVAILABLE_TIMEBLOCK_1 = :AVAILABLE_TIMEBLOCK_1,'
      'AVAILABLE_TIMEBLOCK_2 = :AVAILABLE_TIMEBLOCK_2,'
      'AVAILABLE_TIMEBLOCK_3 = :AVAILABLE_TIMEBLOCK_3,'
      'AVAILABLE_TIMEBLOCK_4 = :AVAILABLE_TIMEBLOCK_4,'
      'AVAILABLE_TIMEBLOCK_5 = :AVAILABLE_TIMEBLOCK_5,'
      'AVAILABLE_TIMEBLOCK_6 = :AVAILABLE_TIMEBLOCK_6,'
      'AVAILABLE_TIMEBLOCK_7 = :AVAILABLE_TIMEBLOCK_7,'
      'AVAILABLE_TIMEBLOCK_8 = :AVAILABLE_TIMEBLOCK_8,'
      'AVAILABLE_TIMEBLOCK_9 = :AVAILABLE_TIMEBLOCK_9,'
      'AVAILABLE_TIMEBLOCK_10 = :AVAILABLE_TIMEBLOCK_10,'
      'MUTATIONDATE = :MUTATIONDATE,'
      'MUTATOR = :MUTATOR'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      'EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      'SHIFT_NUMBER = :SHIFT_NUMBER AND'
      'EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ''
      ' '
      ' '
      ' ')
    Left = 576
    Top = 448
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'NEW_SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_1'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_2'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'AVAILABLE_TIMEBLOCK_4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_7'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_8'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_9'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'AVAILABLE_TIMEBLOCK_10'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qrySTA: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, EMPLOYEE_NUMBER, SHIFT_NUMBER,'
      '  DAY_OF_WEEK, AVAILABLE_TIMEBLOCK_1,'
      '  AVAILABLE_TIMEBLOCK_2,  AVAILABLE_TIMEBLOCK_3,'
      '  AVAILABLE_TIMEBLOCK_4,  AVAILABLE_TIMEBLOCK_5,'
      '  AVAILABLE_TIMEBLOCK_6,  AVAILABLE_TIMEBLOCK_7,'
      '  AVAILABLE_TIMEBLOCK_8,  AVAILABLE_TIMEBLOCK_9,'
      '  AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER'
      'ORDER BY'
      '  DAY_OF_WEEK  '
      ''
      ' '
      ' ')
    Left = 576
    Top = 496
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
