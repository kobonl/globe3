(*
  MRA:30-JAN-2013 TD-22045
  - In second grid: Show plant-shift-combination, to make
    it possible to also show timeblocks-per-employee for
    employee where the plant was changed.
  MRA:25-MAR-2013 TD-22311 Related to this order
  - Also filter on teams-per-user
  - Added TablePlant for looking up plant description for TableMaster.
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
*)
unit TimeBlockPerEmplDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTimeBlockPerEmplDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailTIMEBLOCK_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TableMasterPLANTLU: TStringField;
    DataSourceEmpl: TDataSource;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableTempTBperEmpl: TTable;
    TableDetailPLANTLU: TStringField;
    TableExportEMPLLU: TStringField;
    QueryEmpl: TQuery;
    QueryEmplFLOAT_EMP: TFloatField;
    QueryEmplEMPLOYEE_NUMBER: TIntegerField;
    QueryEmplDESCRIPTION: TStringField;
    QueryEmplSHORT_NAME: TStringField;
    QueryEmplDEPARTMENT_CODE: TStringField;
    QueryEmplADDRESS: TStringField;
    QueryEmplPLANT_CODE: TStringField;
    QueryEmplPLANTLU: TStringField;
    QueryEmplSHIFT_NUMBER: TIntegerField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailDESCRIPTIONValidate(Sender: TField);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEmplCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetEmployee;
  end;

var
  TimeBlockPerEmplDM: TTimeBlockPerEmplDM;

implementation

uses SystemDMT, UPimsMessageRes, UPimsConst;

{$R *.DFM}

procedure TTimeBlockPerEmplDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;
  if (CheckBlocks(TimeBlockPerEmplDM.Handle_Grid, TableMaster, False) <> 0 ) then
  begin
    DisplayMessage(SPIMSTimeIntervalError, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TTimeBlockPerEmplDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  SetNullValues(TableDetail);
  TableDetail.FieldByName('EMPLOYEE_NUMBER').AsString :=
    QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString;
  TableDetail.FieldByName('PLANT_CODE').AsString :=
    TableMaster.FieldByName('PLANT_CODE').AsString;
  TableDetail.FieldByName('SHIFT_NUMBER').AsInteger :=
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger;

  if not CheckNrOfTimeblocks(TableDetail.RecordCount) then // GLOB3-60
    Abort;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('TIMEBLOCK_NUMBER').AsInteger := Index + 1;

  if TableTempTBPerEmpl.FindKey([QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    TableMaster.FieldByName('PLANT_CODE').AsString,
      TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
    for Index := 1 to 7 do
    begin
      DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
        TableTempTBPerEmpl.FieldByName('ENDTIME' + IntToStr(Index)).Value;
      DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
        TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
    end
    else
      InitializeTimeValues(TableDetail, TableMaster);
end;

procedure TTimeBlockPerEmplDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailTIMEBLOCK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

procedure TTimeBlockPerEmplDM.SetEmployee;
var
  SubStr: String;
begin
//CAR 550279
  SubStr := 'EMPLOYEE_NUMBER = ' +
    QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' AND PLANT_CODE = ' + '''' +
    DoubleQuote(TableMaster.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND SHIFT_NUMBER = ' + TableMaster.FieldByName('SHIFT_NUMBER').AsString;
  if SubStr = TableDetail.Filter then
    Exit;
  TableDetail.Filter := SubStr;
end;

procedure TTimeBlockPerEmplDM.TableDetailDESCRIPTIONValidate(
  Sender: TField);
begin
  inherited;
  SystemDM.DefaultNotEmptyValidate(Sender);
end;

procedure TTimeBlockPerEmplDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  SetEmployee;
end;

procedure TTimeBlockPerEmplDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //CAR: 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    // TD-22045 Added SHIFT_NUMBER in QueryEmpl.
    QueryEmpl.Sql.Clear;
    QueryEmpl.Sql.Add(
      'SELECT ' +NL +
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME, ' + NL +
      '  E.DESCRIPTION, E.PLANT_CODE, P.DESCRIPTION AS PLANTLU, ' + NL +
      '  E.DEPARTMENT_CODE, E.ADDRESS, ' + NL +
      '  E.SHIFT_NUMBER ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E, PLANT P, TEAMPERUSER T ' + NL +
      'WHERE ' + NL +
      '  E.PLANT_CODE = P.PLANT_CODE AND ' + NL +
      '  T.USER_NAME = :USER_NAME AND ' + NL +
      '  E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'ORDER BY ' + NL +
      '  E.EMPLOYEE_NUMBER');
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryEmpl.Prepare;
  end;
  QueryEmpl.Open;
  // TD-22311 Also filter on team
  SystemDM.PlantTeamFilterEnable(Tablemaster);
end;

procedure TTimeBlockPerEmplDM.QueryEmplCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('FLOAT_EMP').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TTimeBlockPerEmplDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //car 550279
  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
end;

// TD-22311 Also filter on teams-per-user
procedure TTimeBlockPerEmplDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
