inherited ReportProductionDM: TReportProductionDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 581
  Width = 741
  inherited qryPlantMinMax: TQuery
    Left = 184
    Top = 200
  end
  inherited qryTeamMinMax: TQuery
    Left = 184
    Top = 256
  end
  inherited qryEmplMinMax: TQuery
    Left = 536
    Top = 88
  end
  inherited qryBUMinMax: TQuery
    Left = 536
    Top = 144
  end
  inherited qryWeekDelete: TQuery
    Left = 536
    Top = 256
  end
  inherited qryWeekInsert: TQuery
    Left = 536
    Top = 200
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 176
    Top = 352
  end
  object TableEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EMPLOYEE'
    Left = 72
    Top = 432
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceProduction
    TableName = 'PLANT'
    Left = 72
    Top = 80
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableDept: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'DEPARTMENT'
    Left = 72
    Top = 136
    object TableDeptDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDeptDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDeptBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDeptCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDeptDIRECT_HOUR_YN: TStringField
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDeptMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDeptMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDeptPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDeptREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
  end
  object QueryProduction: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProductionFilterRecord
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  PQ.SHIFT_NUMBER, PQ.SHIFT_DATE,'
      '  PQ.QUANTITY AS PIECES'
      'FROM'
      '  PRODUCTIONQUANTITY PQ,'
      '  PLANT P,'
      '  JOBCODE J,'
      '  WORKSPOT W,'
      '  BUSINESSUNIT B,'
      '  DEPARTMENT D,'
      '  PRODHOURPEREMPLOYEE PE'
      'WHERE'
      '  PQ.PLANT_CODE >= :PLANTFROM AND'
      '  PQ.PLANT_CODE <= :PLANTTO AND'
      '  B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  B.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND'
      '  PQ.WORKSPOT_CODE <= :WORKSPOTTO AND'
      '  PQ.START_DATE >= :DATEFROM AND'
      '  PQ.START_DATE <= :DATETO AND'
      '  PQ.SHIFT_DATE >= :SHIFTDATEFROM AND'
      '  PQ.SHIFT_DATE <= :SHIFTDATETO AND'
      '  PQ.PLANT_CODE = J.PLANT_CODE AND'
      '  PQ.PLANT_CODE = P.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = J.JOB_CODE AND'
      '  J.PLANT_CODE = B.PLANT_CODE AND'
      '  J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE AND'
      '  PQ.PLANT_CODE = W.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  J.BUSINESSUNIT_CODE = D.BUSINESSUNIT_CODE AND'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  PQ.PLANT_CODE = PE.PLANT_CODE AND'
      '  PQ.SHIFT_NUMBER = PE.SHIFT_NUMBER AND'
      '  PQ.WORKSPOT_CODE = PE.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = PE.JOB_CODE'
      'ORDER BY'
      '  P.PLANT_CODE, P.DESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFTDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFTDATETO'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProduction: TDataSource
    DataSet = QueryProduction
    Left = 184
    Top = 80
  end
  object TableWK: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 72
    Top = 368
  end
  object TableJob: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'JOBCODE'
    Left = 72
    Top = 192
  end
  object QueryBUPerWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE, '
      '  SUM(PERCENTAGE)'
      'FROM '
      '  BUSINESSUNITPERWORKSPOT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE  AND'
      '  BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE '
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE'
      '')
    Left = 176
    Top = 424
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableBU: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BUSINESSUNIT'
    Left = 184
    Top = 144
    object TableBUBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableBUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableBUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableBUCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableBUBONUS_FACTOR: TFloatField
      FieldName = 'BONUS_FACTOR'
    end
    object TableBUNORM_ILL_VS_DIRECT_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
    end
    object TableBUMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableBUMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableBUNORM_ILL_VS_INDIRECT_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
    end
    object TableBUNORM_ILL_VS_TOTAL_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
    end
  end
  object TableEmployeeContract: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EMPLOYEECONTRACT'
    Left = 72
    Top = 256
  end
  object QueryProdHourPerEmployee: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT'
      '  PRODHOUREMPLOYEE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE,'
      '  MANUAL_YN,'
      '  PRODUCTION_MINUTE,'
      '  PAYED_BREAK_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLOYEE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE AND'
      '  PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  PRODHOUREMPLOYEE_DATE <= :DATETO AND'
      '  MANUAL_YN = '#39'N'#39
      'ORDER BY'
      '  EMPLOYEE_NUMBER,'
      '  PRODHOUREMPLOYEE_DATE'
      ''
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 352
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object QuerySalaryHourPerEmployee: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT'
      '  S.SALARY_DATE,'
      '  S.EMPLOYEE_NUMBER,'
      '  S.HOURTYPE_NUMBER,'
      '  S.MANUAL_YN,'
      '  S.SALARY_MINUTE,'
      '  H.BONUS_PERCENTAGE,'
      '  H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN,'
      '  O.LINE_NUMBER'
      'FROM'
      '  EMPLOYEE E,'
      '  OVERTIMEDEFINITION O,'
      '  SALARYHOURPEREMPLOYEE S,'
      '  HOURTYPE H'
      'WHERE'
      '  E.CONTRACTGROUP_CODE = O.CONTRACTGROUP_CODE AND'
      '  H.HOURTYPE_NUMBER = O.HOURTYPE_NUMBER AND'
      '  E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER AND'
      '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  H.OVERTIME_YN = '#39'Y'#39' AND'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  S.SALARY_DATE >= :DATEFROM AND'
      '  S.SALARY_DATE <= :DATETO AND'
      '  S.MANUAL_YN = '#39'N'#39
      'ORDER BY'
      '  O.LINE_NUMBER DESC'
      ' ')
    Left = 352
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object QueryTimeRegScanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT'
      '  DATETIME_IN,'
      '  PLANT_CODE,'
      '  EMPLOYEE_NUMBER,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE,'
      '  IDCARD_IN,'
      '  SHIFT_NUMBER,'
      '  DATETIME_OUT,'
      '  SHIFT_DATE'
      'FROM'
      '  TIMEREGSCANNING'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ('
      '    (:ONSHIFT = 1 AND'
      '    SHIFT_DATE >= :DATEFROM AND'
      '    SHIFT_DATE < :DATETO)'
      '    OR'
      '    (:ONSHIFT = 0 AND'
      '    DATETIME_IN >= :DATEFROM AND'
      '    DATETIME_OUT < :DATETO)'
      '  )'
      'ORDER BY'
      '  DATETIME_OUT DESC'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 352
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ONSHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object TableBreakPerEmployee: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BREAKPEREMPLOYEE'
    Left = 352
    Top = 192
  end
  object TableBreakPerDepartment: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BREAKPERDEPARTMENT'
    Left = 352
    Top = 248
  end
  object TableBreakPerShift: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BREAKPERSHIFT'
    Left = 352
    Top = 304
  end
  object QueryEmployeePlanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT'
      '  PRODHOUREMPLOYEE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE,'
      '  MANUAL_YN,'
      '  PRODUCTION_MINUTE,'
      '  PAYED_BREAK_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLOYEE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  PRODHOUREMPLOYEE_DATE <= :DATETO AND'
      '  MANUAL_YN = '#39'N'#39)
    Left = 352
    Top = 360
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object TableContractgroup: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'CONTRACTGROUP'
    Left = 352
    Top = 424
  end
  object QueryRevenue: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SUM(REVENUE_AMOUNT) AS REVENUESUM'
      'FROM'
      '  REVENUE'
      'WHERE'
      '  BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE AND'
      '  REVENUE_DATE >= :DATEFROM AND'
      '  REVENUE_DATE <= :DATETO')
    Left = 72
    Top = 312
    ParamData = <
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 536
    Top = 24
  end
end
