(*
  MRA:10-NOV-2010. RV079.2.
  - When adding records, then first record for Monday is ok.
    But when adding additional records, it fills TB4, when it should
    be empty.
  - Some grid-columns were coupled to wrong fields!
  MRA:20-JAN-2010 RV085.7. Bugfix.
  - The Standard Occupation shows a wrong letter in grid for
    timeblock 3. It shows 'C' where it should be 'A'.
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:23-JAN-2019 GLOB3-225
  - Changes for extension 4 to 10 blocks for planning
  - When MaxTimeblocks=4 do not show extra blocks in grid
*)
unit StandardOccupationFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxDBTLCl, dxGrClms, StdCtrls, DBCtrls, dxEditor,
  dxExEdtr, dxEdLib, dxDBELib, Dblup1a, Menus, DBTables;

type
  TStandardOccupationF = class(TGridBaseF)
    dxDetailGridColumnFIXED: TdxDBGridCheckColumn;
    dxDetailGridColumnTIMEBLOCK1A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK1B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK1C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK2A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK2B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK2C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK3B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK3C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK4A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK4B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK4C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK3A: TdxDBGridSpinColumn;
    GroupBox2: TGroupBox;
    pnlPlantShift: TPanel;
    GroupBox6: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    cmbPlusPlant: TComboBoxPlus;
    cmbPlusShift: TComboBoxPlus;
    dxMasterGridColumnPLAN_ON_WORKSPOT_YN: TdxDBGridColumn;
    dxMasterGridColumnDEPARTMENT_CODE: TdxDBGridColumn;
    dxMasterGridColumnDEPARTMENT_DESCRIPTION: TdxDBGridColumn;
    dxMasterGridColumnWORKSPOT_DESCRIPTION: TdxDBGridColumn;
    PopupMenuCopyPaste: TPopupMenu;
    CopyRecord: TMenuItem;
    PasteRecord: TMenuItem;
    dxDetailGridColumnDayDesc: TdxDBGridColumn;
    dxMasterGridColumnWORKSPOT_CODE: TdxDBGridColumn;
    Panel2: TPanel;
    ScrollBox1: TScrollBox;
    grpBxTIMEBLOCK1: TGroupBox;
    grpBxTIMEBLOCK2: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    dxDBSpinEdit2A: TdxDBSpinEdit;
    dxDBSpinEdit2B: TdxDBSpinEdit;
    dxDBSpinEdit2C: TdxDBSpinEdit;
    grpBxTIMEBLOCK3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    dxDBSpinEdit3A: TdxDBSpinEdit;
    dxDBSpinEdit3B: TdxDBSpinEdit;
    dxDBSpinEdit3C: TdxDBSpinEdit;
    grpBxTIMEBLOCK4: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dxDBSpinEdit4A: TdxDBSpinEdit;
    dxDBSpinEdit4B: TdxDBSpinEdit;
    dxDBSpinEdit4C: TdxDBSpinEdit;
    grpBxTIMEBLOCK5: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    dxDBSpinEdit5A: TdxDBSpinEdit;
    dxDBSpinEdit5B: TdxDBSpinEdit;
    dxDBSpinEdit5C: TdxDBSpinEdit;
    grpBxTIMEBLOCK6: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    dxDBSpinEdit6A: TdxDBSpinEdit;
    dxDBSpinEdit6B: TdxDBSpinEdit;
    dxDBSpinEdit6C: TdxDBSpinEdit;
    grpBxTIMEBLOCK7: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    dxDBSpinEdit7A: TdxDBSpinEdit;
    dxDBSpinEdit7B: TdxDBSpinEdit;
    dxDBSpinEdit7C: TdxDBSpinEdit;
    grpBxTIMEBLOCK8: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    dxDBSpinEdit8A: TdxDBSpinEdit;
    dxDBSpinEdit8B: TdxDBSpinEdit;
    dxDBSpinEdit8C: TdxDBSpinEdit;
    grpBxTIMEBLOCK9: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    dxDBSpinEdit9A: TdxDBSpinEdit;
    dxDBSpinEdit9B: TdxDBSpinEdit;
    dxDBSpinEdit9C: TdxDBSpinEdit;
    grpBxTIMEBLOCK10: TGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    dxDBSpinEdit10A: TdxDBSpinEdit;
    dxDBSpinEdit10B: TdxDBSpinEdit;
    dxDBSpinEdit10C: TdxDBSpinEdit;
    dxDetailGridColumnTIMEBLOCK5A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK5B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK5C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK6A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK6B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK6C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK7A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK7B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK7C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK8A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK8B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK8C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK9A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK9B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK9C: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK10A: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK10B: TdxDBGridSpinColumn;
    dxDetailGridColumnTIMEBLOCK10C: TdxDBGridSpinColumn;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DaySelection: TComboBox;
    DBCheckBoxFixedYN: TDBCheckBox;
    Label35: TLabel;
    DBCheckBoxPlantOCC_A: TDBCheckBox;
    Label36: TLabel;
    DBCheckBoxPlantOCC_B: TDBCheckBox;
    Label37: TLabel;
    DBCheckBoxPlantOCC_C: TDBCheckBox;
    Label38: TLabel;
    Panel1A: TPanel;
    Label6: TLabel;
    dxDBSpinEdit1A: TdxDBSpinEdit;
    Panel1B: TPanel;
    dxDBSpinEdit1B: TdxDBSpinEdit;
    Label7: TLabel;
    Panel1C: TPanel;
    dxDBSpinEdit1C: TdxDBSpinEdit;
    Label8: TLabel;
    Panel2A: TPanel;
    Panel2B: TPanel;
    Panel2C: TPanel;
    Panel3A: TPanel;
    Panel3B: TPanel;
    Panel3C: TPanel;
    Panel4A: TPanel;
    Panel4B: TPanel;
    Panel4C: TPanel;
    Panel5A: TPanel;
    Panel5B: TPanel;
    Panel5C: TPanel;
    Panel6A: TPanel;
    Panel6B: TPanel;
    Panel6C: TPanel;
    Panel7A: TPanel;
    Panel7B: TPanel;
    Panel7C: TPanel;
    Panel8A: TPanel;
    Panel8B: TPanel;
    Panel8C: TPanel;
    Panel9A: TPanel;
    Panel9B: TPanel;
    Panel9C: TPanel;
    Panel10A: TPanel;
    Panel10B: TPanel;
    Panel10C: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cmbPlusPlantChange(Sender: TObject);
    procedure cmbPlusShiftChange(Sender: TObject);
    procedure dxMasterGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure CopyRecordClick(Sender: TObject);
    procedure PasteRecordClick(Sender: TObject);
    procedure DaySelectionChange(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxMasterGridClick(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure DBCheckBoxPlantOCC_Click(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillShifts;
    procedure DetailSetFilter;
    procedure TimeBlockSwitch(ABlockNumber: Integer; AOnOff: Boolean);
    procedure InitGroupBoxTimeblocks;
    procedure DisplayGroupBoxTimeblocks;
    procedure DisplayTimeBlocksABC;
    procedure PlantAskSaveChanges;
  public
    { Public declarations }
  end;

function StandardOccupationF: TStandardOccupationF;

implementation

{$R *.DFM}

uses
  SystemDMT, ListProcsFRM, StandardOccupationDMT, UPimsMessageRes, UPimsConst;

var
  StandardOccupationF_HND: TStandardOccupationF;

function StandardOccupationF: TStandardOccupationF;
begin
  if StandardOccupationF_HND = nil then
    StandardOccupationF_HND := TStandardOccupationF.Create(Application);
  Result := StandardOccupationF_HND;
end;

procedure TStandardOccupationF.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  StandardOccupationDM := CreateFormDM(TStandardOccupationDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil)then
  begin
    dxDetailGrid.DataSource := StandardOccupationDM.DataSourceDetail;
    dxMasterGrid.DataSource := StandardOccupationDM.DataSourceDepartmentMASTER;
  end;
  inherited;
  DaySelection.Clear;
  for i:= 1 to 7 do
    DaySelection.Items.Add(SystemDM.GetDayWDescription(i));
  i := ListProcsF.DayWStartOnFromDate(Now);
  DaySelection.ItemIndex := i - 1;
  // GLOB3-60
  // Some properties must be set to other caption here, to prevent
  // the translator tool will overwrite them.
  dxDetailGrid.Bands[1].Caption := '1';
  dxDetailGrid.Bands[2].Caption := '2';
  dxDetailGrid.Bands[3].Caption := '3';
  dxDetailGrid.Bands[4].Caption := '4';
  grpBxTIMEBLOCK1.Caption := '1';
  grpBxTIMEBLOCK2.Caption := '2';
  grpBxTIMEBLOCK3.Caption := '3';
  grpBxTIMEBLOCK4.Caption := '4';
end;

procedure TStandardOccupationF.FormDestroy(Sender: TObject);
begin
  inherited;
  StandardOccupationF_HND := nil;
end;

procedure TStandardOccupationF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  PlantAskSaveChanges;
  // RV050.8.
  // Turn filtering off to prevent a problem during OnRecordFilter!
  StandardOccupationDM.QueryPlantShift.Filtered := False;
  StandardOccupationDM.QueryPlantShift.Active := False;
  Action := caFree;
end;

procedure TStandardOccupationF.DetailSetFilter;
begin
  StandardOccupationDM.TableDetail.Active := False;
  StandardOccupationDM.TableDetail.Filtered := True;
  if GetIntValue(CmbPlusShift.Value) <> 0 then
    StandardOccupationDM.TableDetail.Filter := 'SHIFT_NUMBER=' +
      IntToStr(GetIntValue(CmbPlusShift.Value));
  StandardOccupationDM.TableDetail.Active := True;

 if CmbPlusShift.Value <> '' then
 // Remember Shiftnumber for New-TimeBlock-Record.
  try
    StandardOccupationDM.ShiftNumber := GetIntValue(CmbPlusShift.Value);
  except
    StandardOccupationDM.ShiftNumber := 0;
  end;
   //set filter to export table
  StandardOccupationDM.TableExport.Active := False;
  StandardOccupationDM.TableExport.Filtered := True;
  if GetIntValue(CmbPlusShift.Value) <> 0 then
    StandardOccupationDM.TableExport.Filter := 'SHIFT_NUMBER=' +
      IntToStr(GetIntValue(CmbPlusShift.Value)) +
      ' AND PLANT_CODE = ''' +
      DoubleQuote(GetStrValue(StandardOccupationF.CmbPlusPlant.Value)) + '''';
  StandardOccupationDM.TableExport.Active := True;
end;

procedure TStandardOccupationF.FillShifts;
begin
  ListProcsF.FillComboBoxMasterPlant(StandardOccupationDM.QueryShift,
    CmbPlusShift, GetStrValue(CmbPlusPlant.Value), 'SHIFT_NUMBER', True);
  cmbPlusShift.IsSorted := True; // GLOB3-60 Do this for numerical sort!

  StandardOccupationDM.QueryDepartmentMASTER.Close;
  StandardOccupationDM.QueryDepartmentMASTER.ParamByName('PLANT_CODE').Value :=
    GetStrValue(CmbPlusPlant.Value);
  StandardOccupationDM.QueryDepartmentMASTER.Open;
end;

procedure TStandardOccupationF.FormShow(Sender: TObject);
begin
  inherited;
  InitGroupBoxTimeblocks;
  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;
  cmbPlusShift.ShowSpeedButton := False;
  cmbPlusShift.ShowSpeedButton := True;
  ListProcsF.FillComboBoxMaster(StandardOccupationDM.QueryPlantShift,
    'PLANT_CODE', True, CmbPlusPlant);
  FillShifts;
  DetailSetFilter;
  if StandardOccupationDM <> Nil then
  begin
    DaySelection.ItemIndex :=
      StandardOccupationDM.TableDetail.FieldByName('DAY_OF_WEEK').ASInteger - 1;
    if DaySelection.ItemIndex < 0 then
      DaySelection.ItemIndex := 0;
  end;
  //CAR 24-10-2003 - 550109
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    dxMasterGrid.PopupMenu := Nil;
    dxDetailGrid.PopupMenu := Nil;    
  end;
  DisplayTimeBlocksABC;
end;

procedure TStandardOccupationF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  FillShifts;
  DetailSetFilter;
  DisplayTimeBlocksABC;
end;

procedure TStandardOccupationF.cmbPlusShiftChange(Sender: TObject);
begin
  inherited;
  DetailSetFilter;
end;

procedure TStandardOccupationF.TimeBlockSwitch(ABlockNumber: Integer;
  AOnOff: Boolean);
  procedure GridColumnVisible(ALetter: String);
  var
    C: TComponent;
  begin
    C := FindComponent('dxDetailGridColumnTIMEBLOCK' +
      IntToStr(ABlockNumber) + ALetter);
    if Assigned(C) then
      if (C is TdxDBGridSpinColumn) then
      begin
        TdxDBGridSpinColumn(C).ReadOnly := not AOnOff;
//        TdxDBGridSpinColumn(C).Visible := AOnOff;
      end;
  end; // GridColumnVisible
  procedure SpinEditVisible(ALetter: String);
  var
    C: TComponent;
  begin
    C := FindComponent('dxDBSpinEdit' + IntToStr(ABlockNumber) + ALetter);
    if Assigned(C) then
      if (C is TdxDBSpinEdit) then
      begin
        TdxDBSpinEdit(C).Enabled := AOnOff;
        TdxDBSpinEdit(C).Visible := AOnOff;
      end;
  end; // SpinEditVisible
  procedure grpBoxVisible;
  var
    C: TComponent;
  begin
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(ABlockNumber));
    if Assigned(C) then
      if (C is TGroupBox) then
      begin
        TGroupBox(C).Enabled := AOnOff;
//        TGroupBox(C).Visible := AOnOff;
      end;
  end; // grpBoxVisible
begin
  GridColumnVisible('A');
  GridColumnVisible('B');
  GridColumnVisible('C');
  SpinEditVisible('A');
  SpinEditVisible('B');
  SpinEditVisible('C');
  grpBoxVisible;
(*
  case BlockNumber of
  1:
    begin
      dxDetailGridColumnTIMEBLOCK1A.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK1B.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK1C.ReadOnly := not OnOff;
      dxDBSpinEdit1A.Enabled := OnOff;
      dxDBSpinEdit1B.Enabled := OnOff;
      dxDBSpinEdit1C.Enabled := OnOff;
    end;
  2:
    begin
      dxDetailGridColumnTIMEBLOCK2A.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK2B.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK2C.ReadOnly := not OnOff;
      dxDBSpinEdit2A.Enabled := OnOff;
      dxDBSpinEdit2B.Enabled := OnOff;
      dxDBSpinEdit2C.Enabled := OnOff;
    end;
  3:
    begin
      dxDetailGridColumnTIMEBLOCK3A.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK3B.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK3C.ReadOnly := not OnOff;
      dxDBSpinEdit3A.Enabled := OnOff;
      dxDBSpinEdit3B.Enabled := OnOff;
      dxDBSpinEdit3C.Enabled := OnOff;
    end;
  4:
    begin
      dxDetailGridColumnTIMEBLOCK4A.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK4B.ReadOnly := not OnOff;
      dxDetailGridColumnTIMEBLOCK4C.ReadOnly := not OnOff;
      dxDBSpinEdit4A.Enabled := OnOff;
      dxDBSpinEdit4B.Enabled := OnOff;
      dxDBSpinEdit4C.Enabled := OnOff;
    end;
  end;
*)
end;

procedure TStandardOccupationF.dxMasterGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  Value: Variant;
  BlockNumber: Integer;
begin
  inherited;
  GridBaseFRM.GridBaseF.dxGridCustomDrawCell(Sender, ACanvas, ARect, ANode,
    AColumn, ASelected, AFocused, ANewItemRow, AText, AColor, AFont,
    AAlignment, ADone);

  if ANode.HasChildren then
    Exit;

  if (AColumn = dxMasterGridColumnWORKSPOT_CODE) then
  begin
    Value := ANode.Values[dxMasterGridColumnPLAN_ON_WORKSPOT_YN.Index];
    if VarIsNull(Value) then
      AText := ''
    else
      if Value = 'N' then
        AText := ''
  end;
  if (AColumn = dxMasterGridColumnWORKSPOT_DESCRIPTION) then
  begin
    Value := ANode.Values[dxMasterGridColumnPLAN_ON_WORKSPOT_YN.Index];
    if VarIsNull(Value) then
      AText := ''
    else
      if Value = 'N' then
        AText := ''
  end;

  if ASelected then
    if (AColumn = dxMasterGridColumnDEPARTMENT_CODE) then
    begin
      for BlockNumber := 1 to SystemDM.MaxTimeblocks do
        TimeBlockSwitch(BlockNumber, False);
      with StandardOccupationDM.QueryTimeBlockPerDepartment do
      begin
        Close;
        ParamByName('PLANT_CODE').Value := GetStrValue(CmbPlusPlant.Value);
        ParamByName('DEPARTMENT_CODE').Value :=
          dxMasterGridColumnDEPARTMENT_CODE.Field.Value;
        ParamByName('SHIFT_NUMBER').Value := GetIntValue(CmbPlusShift.Value);
        Open;
        if RecordCount > 0 then
        begin
          First;
          while not Eof do
          begin
            if (FieldByName('TIMEBLOCK_NUMBER').AsInteger >= 1) and
              (FieldByName('TIMEBLOCK_NUMBER').AsInteger <= SystemDM.MaxTimeblocks) then
              TimeBlockSwitch(FieldByName('TIMEBLOCK_NUMBER').AsInteger, True);
            Next;
          end;
        end
        else
        begin
          with StandardOccupationDM.QueryTimeBlockPerShift do
          begin
            Close;
            ParamByName('PLANT_CODE').Value := GetStrValue(CmbPlusPlant.Value);
            ParamByName('SHIFT_NUMBER').Value := GetIntValue(CmbPlusShift.Value);
            Open;
            if RecordCount > 0 then
            begin
              First;
              while not Eof do
              begin
                if (FieldByName('TIMEBLOCK_NUMBER').AsInteger >= 1) and
                  (FieldByName('TIMEBLOCK_NUMBER').AsInteger <= SystemDM.MaxTimeblocks) then
                  TimeBlockSwitch(FieldByName('TIMEBLOCK_NUMBER').AsInteger, True);
                Next;
              end;
            end;
          end;
        end;
      end;
      DisplayGroupBoxTimeblocks;
    end;
end;

procedure TStandardOccupationF.CopyRecordClick(Sender: TObject);
begin
  inherited;
  // Remember current selection
  StandardOccupationDM.SetCopyFromValues(
    GetIntValue(cmbPlusShift.Value),
    dxMasterGridColumnPLAN_ON_WORKSPOT_YN.Field.Value,
    GetStrValue(cmbPlusPlant.Value),
    dxMasterGridColumnDEPARTMENT_CODE.Field.Value,
    dxMasterGridColumnWORKSPOT_CODE.Field.Value);
end;

procedure TStandardOccupationF.PasteRecordClick(Sender: TObject);
begin
  inherited;
  StandardOccupationDM.PasteExecute;
end;

procedure TStandardOccupationF.DaySelectionChange(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
  begin
    StandardOccupationDM.TableDetail.Edit;
    dxBarBDBNavPost.Enabled := True;
  end;
  StandardOccupationDM.TableDetail.FieldByName('DAY_OF_WEEK').ASInteger :=
    DaySelection.ItemIndex + 1;
end;

procedure TStandardOccupationF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  DaySelection.ItemIndex :=
    StandardOccupationDM.TableDetail.FieldByName('DAY_OF_WEEK').ASInteger - 1;
end;

procedure TStandardOccupationF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  DaySelection.ItemIndex :=
    StandardOccupationDM.TableDetail.FieldByName('DAY_OF_WEEK').ASInteger - 1;
  if DaySelection.ItemIndex < 0 then
     DaySelection.ItemIndex := 0;
end;

procedure TStandardOccupationF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
//  DaySelection.ItemIndex := 0;
end;

procedure TStandardOccupationF.dxMasterGridClick(Sender: TObject);
begin
//  inherited;
  
end;

procedure TStandardOccupationF.dxBarButtonExportGridClick(Sender: TObject);
begin
  CreateExportColumns(dxDetailGrid, '', 'PLANT_CODE');
  CreateExportColumnsDesc(SExportDescPlant, 'PLANTLU');
  CreateExportColumns(dxDetailGrid, '', 'SHIFT_NUMBER');
  CreateExportColumnsDesc(SExportDescShift, 'SHIFTLU');
  CreateExportColumns(dxDetailGrid, '', 'SHIFTLU');
  CreateExportColumns(dxMasterGrid, '', 'WORKSPOT_CODE');
  CreateExportColumns(dxMasterGrid, '', 'DEPARTMENT_CODE');
  //car 550279 - 6-1-2004- set here for speed reasons
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    StandardOccupationDM.TableExport.Filtered := True;

  inherited;

end;

procedure TStandardOccupationF.DBCheckBoxPlantOCC_Click(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
    dxBarBDBNavPost.Enabled := True;
end;

procedure TStandardOccupationF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  if (ActiveGrid  as TdxDBGrid).DataSource.
    DataSet.State in [dsEdit, dsInsert] then
   (ActiveGrid  as TdxDBGrid).DataSource.
    DataSet.Post;
  with StandardOccupationDM do
  begin
    if TablePlant.State in [dsEdit] then
    begin
      TablePlant.Post;
      dxBarBDBNavPost.Enabled := False;
      DisplayTimeBlocksABC;
    end;
  end;
end;

procedure TStandardOccupationF.DisplayTimeBlocksABC;
var
  TB_A, TB_B, TB_C: Boolean;
  I: Integer;
  procedure DisplayComponent(AIndex: Integer; ALetter: String; AYN: Boolean);
  var
    C: TComponent;
  begin
    C := FindComponent('Panel' + IntToStr(AIndex) + ALetter);
    if Assigned(C) then
      if (C is TPanel) then
        TPanel(C).Visible := AYN;
    C := FindComponent('dxDetailGridColumnTIMEBLOCK' + IntToStr(AIndex) + ALetter);
    if Assigned(C) then
      if (C is TdxDBGridSpinColumn) then
        TdxDBGridSpinColumn(C).Visible := AYN;
  end;
  procedure ResizeGroupBox(AIndex: Integer);
  var
    C: TComponent;
    Nr: Integer;
    procedure RePositionPanel(ALetter: String);
    begin
      C := FindComponent('Panel' + IntToStr(AIndex) + ALetter);
      if Assigned(C) then
        if (C is TPanel) then
          TPanel(C).Left := (Nr-1) * TPanel(C).Width + 2;
    end;
  begin
    Nr := 0;
    if TB_A then
    begin
      inc(Nr);
      RePositionPanel('A');
    end;
    if TB_B then
    begin
      inc(Nr);
      RePositionPanel('B');
    end;
    if TB_C then
    begin
      inc(Nr);
      RePositionPanel('C');
    end;
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(AIndex));
    if Assigned(C) then
      if (C is TGroupBox) then
      begin
        TGroupBox(C).Width := TGroupBox(C).Tag - (3 * Panel1A.Width) + (Nr * Panel1A.Width);
        TGroupBox(C).Left := Round((TGroupBox(C).Tag / 3 * Nr) * (AIndex-1));
      end;
  end;
begin
  with StandardOccupationDM do
  begin
    TB_A := TablePlant.FieldByName('STDOCC_A_YN').AsString = 'Y';
    TB_B := TablePlant.FieldByName('STDOCC_B_YN').AsString = 'Y';
    TB_C := TablePlant.FieldByName('STDOCC_C_YN').AsString = 'Y';
    // Prevent nothing is true.
    if not (TB_A or TB_B or TB_C) then
      TB_C := True;
  end;
  for I := 1 to SystemDM.MaxTimeblocks do
  begin
    DisplayComponent(I, 'A', TB_A);
    DisplayComponent(I, 'B', TB_B);
    DisplayComponent(I, 'C', TB_C);
    ResizeGroupBox(I);
  end;
end; // DisplayTimeBlocksABC

procedure TStandardOccupationF.InitGroupBoxTimeblocks;
var
  I: Integer;
  C: TComponent;
begin
  for I := 1 to SystemDM.MaxTimeblocks do
  begin
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(I));
    if Assigned(C) then
      if (C is TGroupBox) then
        TGroupBox(C).Tag := TGroupBox(C).Width;
  end;
end; // InitGroupBoxTimeblocks

procedure TStandardOccupationF.DisplayGroupBoxTimeblocks;
var
  I: Integer;
  C: TComponent;
  procedure ListBandVisible(AI: Integer; AVisible: Boolean);
  begin
    dxDetailGrid.Bands.Items[AI].Visible := AVisible;
  end;
begin
  for I := 1 to SystemDM.MaxTimeblocks do
  begin
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(I));
    if Assigned(C) then
      if (C is TGroupBox) then
      begin
        TGroupBox(C).Align := alNone;
        TGroupBox(C).Visible := False;
      end;
  end;
  for I := 1 to SystemDM.MaxTimeblocks do
  begin
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(I));
    if Assigned(C) then
      if (C is TGroupBox) then
      begin
        if TGroupBox(C).Enabled then
          TGroupBox(C).Visible := True;
        ListBandVisible(I, TGroupBox(C).Visible);
      end;
  end;
  // GLOB3-225
  for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
  begin
    dxDetailGrid.Bands[I].Visible := False;
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(I));
    if Assigned(C) then
      if (C is TGroupBox) then
      begin
        TGroupBox(C).Visible := False;
      end;
  end;
  // When using alLeft, the sequence goes wrong!
{  for I := 1 to SystemDM.MaxTimeblocks do
  begin
    C := FindComponent('grpBxTIMEBLOCK' + IntToStr(I));
    if Assigned(C) then
      if (C is TGroupBox) then
        TGroupBox(C).Align := alLeft;
  end; }
end; // DisplayGroupBoxTimeblocks

procedure TStandardOccupationF.PlantAskSaveChanges;
var
  QuestionResult: Integer;
begin
  with StandardOccupationDM do
  begin
    if TablePlant.State in [dsEdit] then
    begin
      QuestionResult := DisplayMessage(SPimsSaveChanges,
        mtConfirmation, [mbYes, mbNo]);
      case QuestionResult of
        mrYes :
          begin
            try
              TablePlant.Post;
              dxBarBDBNavPost.Enabled := False;
             except
              FGiveError := False;
            end
          end;
        mrNo: TablePlant.Cancel;
      end; { case }
    end;
  end;
end; // PlantAskSaveChanges

end.
