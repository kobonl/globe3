unit WorkspotEmployeeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, Mask, dxEditor, dxExEdtr, dxEdLib,
  dxDBELib, dxDBTLCl, dxGrClms, ComCtrls, DBPicker, dxDBEdtr;

type
  TWorkspotEmployeeF = class(TGridBaseF)
    LabelWorkspot: TLabel;
    LabelStartDate: TLabel;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnName: TdxDBGridColumn;
    dxMasterGridColumnPlant: TdxDBGridLookupColumn;
    dxMasterGridColumnSecurityNumber: TdxDBGridColumn;
    dxMasterGridColumnAddress: TdxDBGridColumn;
    dxDetailGridColumnStartLevel: TdxDBGridDateColumn;
    dxMasterGridColumnDepartment: TdxDBGridLookupColumn;
    Label1: TLabel;
    DBEditDescription: TDBEdit;
    dxDetailGridColumnLevel: TdxDBGridColumn;
    DBPicker1: TDBPicker;
    dxDetailGridColumnDept: TdxDBGridLookupColumn;
    Label2: TLabel;
    dxDBLookupEditWK: TdxDBLookupEdit;
    dxDBLookupEditDept: TdxDBLookupEdit;
    dxDetailGridColumnWorkspot: TdxDBGridColumn;
    dxDetailGridColumnPlant: TdxDBGridLookupColumn;
    Label3: TLabel;
    dxDBLookupEditPlant: TdxDBLookupEdit;
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBEditDescriptionKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure dxMasterGridClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);

    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxGridCustomDrawBand(Sender: TObject; ABand: TdxTreeListBand;
      ACanvas: TCanvas; ARect: TRect; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxDBLookupEditDeptSelectionChange(Sender: TObject);
    procedure dxDBLookupEditDeptCloseUp(Sender: TObject;
      var Value: Variant; var Accept: Boolean);
    procedure dxDBLookupEditWKCloseUp(Sender: TObject; var Value: Variant;
      var Accept: Boolean);
    procedure dxDBLookupEditPlantCloseUp(Sender: TObject;
      var Value: Variant; var Accept: Boolean);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CheckWK;
  end;

function WorkspotEmployeeF: TWorkspotEmployeeF;

implementation
uses WorkspotEmployeeDMT, UPimsConst, UPimsMessageRes, SystemDMT;
{$R *.DFM}
var
  WorkspotEmployeeF_HND: TWorkspotEmployeeF;

function WorkspotEmployeeF: TWorkspotEmployeeF;
begin
  if (WorkspotEmployeeF_HND = nil) then
  begin
    WorkspotEmployeeF_HND := TWorkspotEmployeeF.Create(Application);
  end;
  Result := WorkspotEmployeeF_HND;
end;

procedure TWorkspotEmployeeF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkspotEmployeeF_HND := Nil;
end;

procedure TWorkspotEmployeeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditDescription.SetFocus;
end;

procedure TWorkspotEmployeeF.FormCreate(Sender: TObject);
begin
  WorkspotEmployeeDM := CreateFormDM(TWorkspotEmployeeDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil)then
  begin
    dxDetailGrid.DataSource := WorkspotEmployeeDM.DataSourceDetail;
    dxMasterGrid.DataSource := WorkspotEmployeeDM.DataSourceMaster;
  end;
  inherited;
end;

procedure TWorkspotEmployeeF.DBEditDescriptionKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if (Ord(Key) >= 97 ) and (Ord(Key) <= 122 )then
    Key := Chr(Ord(Key) - 32);
end;

procedure TWorkspotEmployeeF.CheckWK;
begin
  with WorkspotEmployeeDM do
  begin
    if not TableDetail.Active then
      Exit;
    if TableDetail.RecordCount = 0 then
     exit;
    dxDBLookUpEditDept.Text := TableDetail.FieldByName('DEPTDESC').AsString;
    if IsPlannedOnWK(WorkspotEmployeeDM.TableDetail.FieldByName('DEPARTMENT_CODE').AsString )then
    begin
      if dxDBLookupEditWK.Text = '' then
      begin
        if FindWK(TableDetail.FieldByName('PLANT_CODE').AsString,
          TableDetail.FieldByName('DEPARTMENT_CODE').AsString,
          TableDetail.FieldByName('WORKSPOT_CODE').AsString) then
        begin
          dxDBLookupEditWK.Text := TableDetail.FieldByName('WKDESC').AsString;
        end;
      end
      else
      if not FindWK(TableDetail.FieldByName('PLANT_CODE').AsString,
        TableDetail.FieldByName('DEPARTMENT_CODE').AsString,
        TableDetail.FieldByName('WORKSPOT_CODE').AsString) then
        dxDBLookupEditWK.Text := '';
  end
  else
      dxDBLookupEditWK.Text := DummyWKStr;
  end;
end;

procedure TWorkspotEmployeeF.FormShow(Sender: TObject);
begin
  inherited;
  // MR:30-01-2003 Set active grid. Otherwise, it's set to detailgrid
  // and behaviour of mastergrid-scrollbar is wrong.
  ActiveGrid := dxMasterGrid;
  CheckWK;
  //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      WorkspotEmployeeDM.TableMaster.FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
    WorkspotEmployeeDM.TableMaster.FindKey([SystemDM.ASaveTimeRecScanning.Employee]);
  //
end;

procedure TWorkspotEmployeeF.dxMasterGridClick(Sender: TObject);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    dxBarBDBNavCancel.Enabled;
    WorkspotEmployeeDM.TableDetail.Edit;
    DisplayMessage(SPimsSaveBefore, mtWarning, [mbOK]);
    Exit;
  end;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxBarBDBNavPostClick(Sender: TObject);
begin
  inherited;
  if not(WorkspotEmployeeDM.TableDetail.State in [dsEdit, dsInsert]) then
    WorkspotEmployeeDM.TableDetail.Edit;
  if not WorkspotEmployeeDM.IsPlannedOnWK(WorkspotEmployeeDM.TableDetail.
    FieldByName('DEPARTMENT_CODE').AsString) then
    WorkspotEmployeeDM.TableDetail.FieldByName('WORKSPOT_CODE').Value := DummyStr
  else
    if (dxDBLookupEditWK.Text = '') then
    begin
      DisplayMessage(SPimsFieldRequired, mtWarning, [mbOK]);
      Exit;
    end;
  WorkspotEmployeeDM.TableDetail.Post;
  dxBarBDBNavPost.Enabled := False;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxGridEnter(Sender: TObject);
begin
inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    WorkspotEmployeeDM.TableDetail.Edit;
    dxBarBDBNavCancel.Enabled;
    DisplayMessage(SPimsSaveBefore, mtWarning, [mbOK]);
    Exit;
  end;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  WorkspotEmployeeDM.TableDetail.Cancel;
  CheckWK;
  dxBarBDBNavCancel.Enabled := False;
end;

procedure TWorkspotEmployeeF.dxGridCustomDrawBand(Sender: TObject;
  ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxDBLookupEditDeptSelectionChange(
  Sender: TObject);
begin
  inherited;
  CheckWK;
end;

procedure TWorkspotEmployeeF.dxDBLookupEditDeptCloseUp(Sender: TObject;
  var Value: Variant; var Accept: Boolean);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;

end;

procedure TWorkspotEmployeeF.dxDBLookupEditWKCloseUp(Sender: TObject;
  var Value: Variant; var Accept: Boolean);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;
end;

procedure TWorkspotEmployeeF.dxDBLookupEditPlantCloseUp(Sender: TObject;
  var Value: Variant; var Accept: Boolean);
begin
  inherited;
  dxBarBDBNavPost.Enabled := True;
end;

procedure TWorkspotEmployeeF.dxBarButtonExportGridClick(Sender: TObject);
begin
//CAR 27.02.2003
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;

end;

procedure TWorkspotEmployeeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  //Car 17-10-2003: 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    WorkspotEmployeeDM.TableMaster.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

end.
