(*
  MRA:7-DEC-2009 RV048.2. 889945.
  - Report Employee condensed:
    - Add selection: 'Show Contract Type'
      - Permanent/Temporary/External (0/1/2) (Check-buttons).
      This is stored in employee contracts.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportEmplQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, QRExport, QRXMLSFilt,
  QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FSort: Integer;
    FDepartmentFrom, FDepartmentTo,
    FTeamFrom, FTeamTo: String;
    FContractEndDateFrom, FContractEndDateTo: TDateTime;
    FDateActiveFrom, FDateActiveTo,
    FDateInactiveFrom, FDateInactiveTo: TDateTime;
    FShowAllDepartment, FShowAllTeam, FShowAllContractEndDate,
    FShowAllDateActive, FShowAllDateInactive, FShowSelection,
    FExportToFile: Boolean; // MR:17-09-2003
    FShowPermanent, FShowTemporary, FShowExternal: Boolean; // RV048.2.
  public
    procedure SetValues(
      DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
      ContractEndDateFrom, ContractEndDateTo: TDateTime;
      DateActiveFrom, DateActiveTo,
      DateInactiveFrom, DateInactiveTo: TDateTime;
      ShowAllDepartment, ShowAllTeam, ShowAllContractEndDate,
      ShowAllDateActive, ShowAllDateInactive: Boolean;
      Sort: Integer; ShowSelection, ExportToFile: Boolean;
      ShowPermanent, ShowTemporary, ShowExternal: Boolean);
  end;

  TReportEmplQR = class(TReportBaseF)
    QRBandDetailEmployee: TQRBand;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRDBText2: TQRDBText;
    QRDBTextName: TQRDBText;
    QRDBTextAdress: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBTextCity: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBTextSocialNr: TQRDBText;
    QRDBText9: TQRDBText;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLabel16: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelDateActive: TQRLabel;
    QRLabelToDateActive: TQRLabel;
    QRLabelDateActiveTo: TQRLabel;
    QRLabelDateInactiveTo: TQRLabel;
    QRLabelToDateInactive: TQRLabel;
    QRLabelDateInActiveFrom: TQRLabel;
    QRLabelDateInactive: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabelSort: TQRLabel;
    QRLabelFromDateActive: TQRLabel;
    QRLabelDateActiveFrom: TQRLabel;
    QRLabelFromDateInActive: TQRLabel;
    QRLabel5: TQRLabel;
    QRBandSummary: TQRBand;
    QRLblCEndDate: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLblDepartmentFrom: TQRLabel;
    QRLabel23: TQRLabel;
    QRLblDepartmentTo: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLblTeamFrom: TQRLabel;
    QRLabel28: TQRLabel;
    QRLblTeamTo: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLblContrEndDateFrom: TQRLabel;
    QRLabel33: TQRLabel;
    QRLblContrEndDateTo: TQRLabel;
    QRLblMaxSalBalMin: TQRLabel;
    QRLblHDMaxSalBalMin: TQRLabel;
    QRLabel21: TQRLabel;
    QRLblContractType: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextNamePrint(sender: TObject; var Value: String);
    procedure QRDBTextAdressPrint(sender: TObject; var Value: String);
    procedure QRDBTextCityPrint(sender: TObject; var Value: String);
    procedure QRDBTextSocialNrPrint(sender: TObject; var Value: String);
    procedure QRBandHDAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLblCEndDatePrint(sender: TObject; var Value: String);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    ShowMaxSalaryBalance: Boolean;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    function QRSendReportParameters(
      const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
      const DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
      const ContractEndDateFrom, ContractEndDateTo: TDateTime;
      const DateActiveFrom, DateActiveTo, DateInactiveFrom,
      DateInactiveTo: TDateTime;
      const ShowAllDepartment, ShowAllTeam, ShowAllContractEndDate: Boolean;
      const ShowAllDateActive, ShowAllDateInactive: Boolean;
      const Sort: Integer; const ShowSelection, ExportToFile: Boolean;
      const ShowPermanent, ShowTemporary, ShowExternal: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportEmplQR: TReportEmplQR;

implementation

{$R *.DFM}

uses
  SystemDMT,
  UPimsMessageRes,
  UPimsConst,
  ListProcsFRM, ReportEmplDMT,
  UGlobalFunctions;

procedure TQRParameters.SetValues(
  DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
  ContractEndDateFrom, ContractEndDateTo: TDateTime;
  DateActiveFrom, DateActiveTo,
  DateInactiveFrom, DateInactiveTo: TDateTime;
  ShowAllDepartment, ShowAllTeam, ShowAllContractEndDate,
  ShowAllDateActive, ShowAllDateInactive: Boolean;
  Sort: Integer; ShowSelection, ExportToFile: Boolean;
  ShowPermanent, ShowTemporary, ShowExternal: Boolean);
begin
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FContractEndDateFrom := ContractEndDateFrom;
  FContractEndDateTo := ContractEndDateTo;
  FDateActiveFrom := DateActiveFrom;
  FDateActiveTo := DateActiveTo;
  FDateInactiveFrom := DateInactiveFrom;
  FDateInactiveTo := DateInactiveTo;
  FShowAllDepartment := ShowAllDepartment;
  FShowAllTeam := ShowAllTeam;
  FShowAllContractEndDate := ShowAllContractEndDate;
  FShowAllDateActive := ShowAllDateActive;
  FShowAllDateInactive := ShowAllDateInactive;
  FSort := Sort;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  FShowPermanent := ShowPermanent;
  FShowTemporary := ShowTemporary;
  FShowExternal := ShowExternal;
end;

function TReportEmplQR.QRSendReportParameters(
  const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
  const DepartmentFrom, DepartmentTo, TeamFrom, TeamTo: String;
  const ContractEndDateFrom, ContractEndDateTo: TDateTime;
  const DateActiveFrom, DateActiveTo, DateInactiveFrom,
  DateInactiveTo: TDateTime;
  const ShowAllDepartment, ShowAllTeam, ShowAllContractEndDate: Boolean;
  const ShowAllDateActive, ShowAllDateInactive: Boolean;
  const Sort: Integer; const ShowSelection, ExportToFile: Boolean;
  const ShowPermanent, ShowTemporary, ShowExternal: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DepartmentFrom, DepartmentTo, TeamFrom, TeamTo,
      ContractEndDateFrom, ContractEndDateTo,
      DateActiveFrom, DateActiveTo, DateInactiveFrom, DateInactiveTo,
      ShowAllDepartment, ShowAllTeam, ShowAllContractEndDate,
      ShowAllDateActive, ShowAllDateInactive, Sort,
      ShowSelection, ExportToFile,
      ShowPermanent, ShowTemporary, ShowExternal);
  end;
  SetDataSetQueryReport(ReportEmplDM.QueryEmpl);
end;

function TReportEmplQR.ExistsRecords: Boolean;
var
  SelectStr, OrderStr: String;
  Coding: String;
  function ShowPermanent: String;
  begin
    Result := '';
    if QRParameters.FShowPermanent then
      Result := '0'
  end;
  function ShowTemporary: String;
  begin
    Result := '';
    if QRParameters.FShowTemporary then
      Result := '1'
  end;
  function ShowExternal: String;
  begin
    Result := '';
    if QRParameters.FShowExternal then
      Result := '2'
  end;
  function ShowContractType: String;
  begin
    Result := '';
    Coding := '';
    if ShowPermanent <> '' then
      Coding := Coding + ShowPermanent;
    if ShowTemporary <> '' then
    begin
      if Coding <> '' then
        Coding := Coding + ',';
      Coding := Coding + ShowTemporary;
    end;
    if ShowExternal <> '' then
    begin
      if Coding <> '' then
        Coding := Coding + ',';
      Coding := Coding + ShowExternal;
    end;
    if Coding <> '' then
      Result := ' AND (EC.CONTRACT_TYPE IN (' + Coding + ')) ';
  end;
begin
  Screen.Cursor := crHourGlass;

  // MR:22-03-2005 Order 550385
  // Show MaxSalaryBalanceMinutes if 'Export_Type' is 'SR'.
  if SystemDM.GetExportPayroll then
    ShowMaxSalaryBalance :=
      (SystemDM.TableExportPayroll.
        FieldByName('EXPORT_TYPE').AsString = 'SR');
  if ShowMaxSalaryBalance then
    QRLblHDMaxSalBalMin.Caption := SPimsMaxSalBalMin
  else
    QRLblHDMaxSalBalMin.Caption := '';


{initialize variables}
  with ReportEmplDM.QueryEmpl do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    // RV050.8. Added PLANT_CODE for filtering.
    SelectStr :=
      'SELECT ' +
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER, E.DESCRIPTION, E.SHORT_NAME,' +
      '  E.ADDRESS, E.ZIPCODE, E.CITY, E.SOCIAL_SECURITY_NUMBER, ' +
      '  E.PHONE_NUMBER, E.CONTRACTGROUP_CODE, EC.ENDDATE ';
    if ShowMaxSalaryBalance then
      SelectStr := SelectStr +
        ', M.MAX_SALARY_BALANCE_MINUTES ';
    SelectStr := SelectStr +
      'FROM ' +
      '  EMPLOYEE E LEFT OUTER JOIN EMPLOYEECONTRACT EC ON ' +
      '    E.EMPLOYEE_NUMBER = EC.EMPLOYEE_NUMBER ';
    if ShowMaxSalaryBalance then
      SelectStr := SelectStr +
        '  LEFT OUTER JOIN MAXSALARYBALANCE M ON ' +
        '    E.EMPLOYEE_NUMBER = M.EMPLOYEE_NUMBER ';
    SelectStr := SelectStr +
      'WHERE ';
    if not QRParameters.FShowAllContractEndDate then
      SelectStr := SelectStr +
      '  EC.ENDDATE >= :CONTRACTENDDATEFROM AND ' +
      '  EC.ENDDATE <= :CONTRACTENDDATETO AND ';
    SelectStr := SelectStr +
      '  E.PLANT_CODE  >= :PLANTFROM AND E.PLANT_CODE <= :PLANTTO ';
    // RV048.2.
    if not (QRParameters.FShowPermanent and
      QRParameters.FShowTemporary and
      QRParameters.FShowExternal) then
    begin
      SelectStr := SelectStr + ShowContractType;
    end;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      SelectStr := SelectStr +
        ' AND E.EMPLOYEE_NUMBER  >= :EmployeeFrom and '+
        ' E.EMPLOYEE_NUMBER <= :EmployeeTo ';
    end;

    if not QRParameters.FShowAllDateActive then
      SelectStr := SelectStr + ' AND E.STARTDATE >= :STARTACTIVEDATE ' +
        ' AND E.STARTDATE <= :ENDACTIVEDATE ';

    if not QRParameters.FShowAllDateInActive then
      SelectStr := SelectStr + ' AND E.ENDDATE >= :STARTINACTIVEDATE ' +
        ' AND E.ENDDATE <= :ENDINACTIVEDATE ';

    if not QRParameters.FShowAllDepartment then
      if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
        SelectStr := SelectStr + ' AND E.DEPARTMENT_CODE >= :DEPARTMENTFROM ' +
          ' AND E.DEPARTMENT_CODE <= :DEPARTMENTTO ';

    if not QRParameters.FShowAllTeam then
      SelectStr := SelectStr + ' AND E.TEAM_CODE >= :TEAMFROM ' +
        ' AND E.TEAM_CODE <= :TEAMTO ';

    case QRParameters.FSort of
    0: OrderStr := ' ORDER BY E.DESCRIPTION';
    1: OrderStr := ' ORDER BY E.SHORT_NAME';
    2: OrderStr := ' ORDER BY E.EMPLOYEE_NUMBER';
    end;
    SelectStr := SelectStr + OrderStr;

    SQL.Add(UpperCase(SelectStr));
//SQL.SaveToFile('c:\temp\reportempcondensed.txt');
    ParamByName('PLANTFROM').AsString := QRBaseParameters.FPlantFrom;
    ParamByName('PLANTTO').AsString := QRBaseParameters.FPlantTo;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      ParamByName('EMPLOYEEFROM').AsInteger :=
        StrToInt(QRBaseParameters.FEmployeeFrom);
      ParamByName('EMPLOYEETO').AsInteger :=
        StrToInt(QRBaseParameters.FEmployeeTo);
    end;
    if not QRParameters.FShowAllDateActive then
    begin
      ParamByName('STARTACTIVEDATE').AsDateTime := QRParameters.FDateActiveFrom;
      ParamByName('ENDACTIVEDATE').AsDateTime := QRParameters.FDateActiveTo;
    end;
    if not QRParameters.FShowAllDateInActive then
    begin
      ParamByName('STARTINACTIVEDATE').AsDateTime :=
        QRParameters.FDateInActiveFrom;
      ParamByName('ENDINACTIVEDATE').AsDateTime := QRParameters.FDateInActiveTo;
    end;
    if not QRParameters.FShowAllDepartment then
      if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
      begin
        ParamByName('DEPARTMENTFROM').AsString := QRParameters.FDepartmentFrom;
        ParamByName('DEPARTMENTTO').AsString := QRParameters.FDepartmentTo;
      end;
    if not QRParameters.FShowAllTeam then
    begin
      ParamByName('TEAMFROM').AsString := QRParameters.FTeamFrom;
      ParamByName('TEAMTO').AsString := QRParameters.FTeamTo;
    end;
    if not QRParameters.FShowAllContractEndDate then
    begin
      ParamByName('CONTRACTENDDATEFROM').AsDateTime :=
        QRParameters.FContractEndDateFrom;
      ParamByName('CONTRACTENDDATETO').AsDateTime :=
        QRParameters.FContractEndDateTo;
    end;

    if not Prepared then
      Prepare;
    Active := True;
    {open all linked datasets}
    Result := True;
    if RecordCount = 0 then
      Result := False;
    {check if report is empty}
  end;
  Screen.Cursor := crDefault;
end;

procedure TReportEmplQR.ConfigReport;
begin
  // MR:17-09-2003
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblDepartmentFrom.Caption := '*';
  QRLblDepartmentTo.Caption := '*';
  QRLblTeamFrom.Caption := '*';
  QRLblTeamTo.Caption := '*';
  QRLblContrEndDateFrom.Caption := '*';
  QRLblContrEndDateTo.Caption := '*';
  QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end;
  if not QRParameters.FShowAllDepartment then
  begin
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      QRLblDepartmentFrom.Caption := QRParameters.FDepartmentFrom;
      QRLblDepartmentTo.Caption := QRParameters.FDepartmentTo;
    end;
  end;
  if not QRParameters.FShowAllTeam then
  begin
    QRLblTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLblTeamTo.Caption := QRParameters.FTeamTo;
  end;
  if not QRParameters.FShowAllContractEndDate then
  begin
    QRLblContrEndDateFrom.Caption := DateToStr(QRParameters.FContractEndDateFrom);
    QRLblContrEndDateTo.Caption := DateToStr(QRParameters.FContractEndDateTo);
  end;
  case QRParameters.Fsort of
  0: QRLabelSort.Caption := SName;
  1: QRLabelSort.Caption := SShortName;
  2: QRLabelSort.Caption := SNumber;
  end;
  
  // RV048.1.
  QRLblContractType.Caption := '';
  if QRParameters.FShowPermanent then
    QRLblContractType.Caption := SPIMSContractTypePermanent;
  if QRParameters.FShowTemporary then
  begin
    if QRLblContractType.Caption <> '' then
      QRLblContractType.Caption := QRLblContractType.Caption + '/';
    QRLblContractType.Caption := QRLblContractType.Caption +
      SPIMSContractTypeTemporary;
  end;
  if QRParameters.FShowExternal then
  begin
    if QRLblContractType.Caption <> '' then
      QRLblContractType.Caption := QRLblContractType.Caption + '/';
    QRLblContractType.Caption := QRLblContractType.Caption +
      SPIMSContractTypePartTime;
  end;

  if QRParameters.FShowAllDateActive then
  begin
    QRLabelDateActiveFrom.Caption := '*';
    QRLabelDateActiveTo.Caption := '*';
  end
  else
  begin
    QRLabelDateActiveFrom.Caption := DateToStr(QRParameters.FDateActiveFrom);
    QRLabelDateActiveTo.Caption := DateToStr(QRParameters.FDateActiveTo);
  end;
  if QRParameters.FShowAllDateInActive then
  begin
    QRLabelDateInActiveFrom.Caption := '*';
    QRLabelDateInActiveTo.Caption := '*';
  end
  else
  begin
    QRLabelDateInactiveFrom.Caption := DateToStr(QRParameters.FDateInactiveFrom);
    QRLabelDateInactiveTo.Caption := DateToStr(QRParameters.FDateInactiveTo);
  end;
end;

procedure TReportEmplQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportEmplQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:17-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportEmplQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  Printband := QRParameters.FShowSelection;
  // MR:17-09-2003
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel14.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLabel16.Caption + ' ' + QRLblPlantTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel20.Caption + QRLblEmployeeFrom.Caption + ' ' +
        QRLabel22.Caption + QRLblEmployeeTo.Caption);
      ExportClass.AddText(QRLabelFromDateActive.Caption + ' ' +
        QRLabelDateActive.Caption + ' ' +
        QRLabelDateActiveFrom.Caption + ' ' +
        QRLabelToDateActive.Caption + ' ' +
        QRLabelDateActiveTo.Caption);
      ExportClass.AddText(QRLabelFromDateInActive.Caption + ' ' +
        QRLabelDateInActive.Caption + ' ' +
        QRLabelDateInActiveFrom.Caption + ' ' +
        QRLabelToDateInActive.Caption + ' ' +
        QRLabelDateInactiveTo.Caption);
      ExportClass.AddText(QRLabel35.Caption + ' ' +
        QRLabelSort.Caption);
    end;
  end;
end;

procedure TReportEmplQR.QRDBTextNamePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value, 0 , 28);
end;

procedure TReportEmplQR.QRDBTextAdressPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value, 0 , 26);
end;

procedure TReportEmplQR.QRDBTextCityPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value, 0 , 19);
end;

procedure TReportEmplQR.QRDBTextSocialNrPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(Value, 0 , 20);
end;

procedure TReportEmplQR.QRBandHDAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:17-09-2003
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel1.Caption + ExportClass.Sep + // Number
      QRLabel2.Caption + ExportClass.Sep + // Shortname
      QRLabel3.Caption + ExportClass.Sep + // Name
      QRLabel4.Caption + ExportClass.Sep + // Address
      QRLabel6.Caption + ExportClass.Sep + // Zipcode
      QRLabel7.Caption + ExportClass.Sep + // City
      QRLabel8.Caption + ExportClass.Sep + // Telephone
      QRLabel9.Caption + ExportClass.Sep + // Social Sec. Number
      QRLabel5.Caption + QRLabel10.Caption); // Contract Group
  end;
end;

procedure TReportEmplQR.QRBandSummaryBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  // MR:17-09-2003
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportEmplQR.QRBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-09-2003
  if QRParameters.FExportToFile then
  begin
    // Detail
    with ReportEmplDM do
    begin
      ExportClass.AddText(
        QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('SHORT_NAME').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('DESCRIPTION').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('ADDRESS').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('ZIPCODE').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('CITY').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('PHONE_NUMBER').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('SOCIAL_SECURITY_NUMBER').AsString + ExportClass.Sep +
        QueryEmpl.FieldByName('CONTRACTGROUP_CODE').AsString);
    end;
  end;
end;

procedure TReportEmplQR.QRLblCEndDatePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if ReportEmplDM.QueryEmpl.FieldByName('ENDDATE').AsDateTime = 0 then
    Value := ''
  else
    Value := DateToStr(
      ReportEmplDM.QueryEmpl.FieldByName('ENDDATE').AsDateTime);
end;

procedure TReportEmplQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // MR:22-03-2005 Order 550385
  if ShowMaxSalaryBalance and (ReportEmplDM.QueryEmpl.
    FieldByName('MAX_SALARY_BALANCE_MINUTES').AsString <> '') then
    QRLblMaxSalBalMin.Caption :=
      UGlobalFunctions.IntMin2StringTime(
        ReportEmplDM.QueryEmpl.
          FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger, False)
  else
    QRLblMaxSalBalMin.Caption := '';
end;

end.


