(*
  Changes:
  MR:12-07-2004 Order 550330, added to 'employee-days': Bank-holiday and
                TFT days. Also making use of 'clientdataset' instead of
                TStringList to store 'employee-days'.
  MR:06-09-2004 Change to procedure EmplDaysRecordUPDATE. Only insert
                record, no edit.
  MRA:09-JAN-2009 RV019. (ExportParis: Order 550466, addition).
    Select-statement for QueryPHETYPE_PARIS changed.
  MRA:04-MAR-2009 RV023. (ExportParis: Order 550466)
    Select-statement for QueryPHETYPE_PARIS changed.
  MRA:19-MAR-2009 RV024. (ExportParis: Order 550466)
    Select-statement for QueryPHETYPE_PARIS changed, because it only showed
    SUM_EOMIN for the E-record (Exceptional) instead of also for the
    other records on the same date (Regular and Overtime).
  MRA:2-APR-2009 RV025. (ExportParis: Order 550466)
    The calculation of the combined hours is now completely done during
    the hour-calculation. So no extra calculation is needed during the
    export payroll.
  MRA:6-MAY-2009 RV026.
    Add export payroll for REINO.
  MRA:23-SEP-2009 RV034.1.
    - Added field CUSTOMER_NUMBER (numeric) to PLANT-table.
      For use with export-payroll-ADP. Field PLANT.CUSTOMER_NUMBER must be
      exported for ADP for each PK-line instead of
      EXPORTPAYROLL.CUSTOMER_NUMBER, for the employee's plant.
      QueryEmpl has been extended for this purpose.
  MRA:25-SEP-2009 RV035.1.
    - Added 2 absence type for export payroll ADP;
      'M' = Maternity leave (zwangerschapsverlof)
      'A' = Lay days (wachtdagen)
  MRA:7-OCT-2009 RV037.
    - SQL-statement of QueryAHE is now not filled anymore during run
      of program, because this was not necessary.
  MRA:23-NOV-2009 RV045.1.
    - TimeForTime-settings are now possible on hourtype-level, instead
      of only contractgroup-level.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict information by user (team-per-user).
  SO:04-AUG-2010 RV067.5. 550497
    Export Attentia
  MRA:3-SEP-2010. RV067.MRA.28 Bugfix.
  - New counters made for export attentia are not initialized.
  MRA:7-SEP-2010 RV067.MRA.30 Changes for 550497
  - QueryAHE: Also look for export-codes coming linked by hourtypepercountry.
  - QuerySHE: Also look for export-codes coming linked by hourtypepercountry.
  - Also add export code to AddHourTypeMins-procedure and store it in
    cdsEmplMins.
  - Add and store export code to cdsEmplDays.
  MRA:7-OCT-2010 RV071.9. 550497 Export Attentia
  - Sequence number problem. Because of export attentia, there can now be
    more than 1 ExportPayroll-record.
  MRA:13-OCT-2010 RV071.14. 550497 Bugfixing/Changes.
  - Not all hours export exported for Attentia.
    Reason: Problem with HourTypes-Per-Country.
  MRA:22-FEB-2011 RV087.2. Export Payroll Select/AFAS (SO-20011509/20011510)
  - Export Payroll Select / AFAS
    - An export payroll is needed for Select/AFAS
  MRA:25-MAR-2011 RV088.1. SO-20011510.20 Export payroll AFAS.
  - Export all hours and all days. Use methods from ADP to determine days.
  - Query 'qryAFAS' changed. It should only get hours (worked/absence).
    - The export-code for absence should be taken from 'hourtype'.
  MRA:18-JUL-2011. RV095.2. SO-20011797
  - Export Payroll Select
    - Add option to select on either combination of:
      - Plant - employee + all related selections
      - OR:
      - Plant - department (not on employee)
    - qrySELECT and qrySELECT_FILTER changed.
  MRA:11-OCT-2011. RV098.3. Bugfix.
  - Removed reference DialogExportPayrollF. Instead of
    that property CurrentExportType is used.
  MRA:1-JUN-2012. SO-20013169 Travel time
  - Addition for travel time (day counter) with
    absence type code = 'N'.
  MRA:30-JUL-2012 20013430. Split export in 2 parts.
  - Export payroll SR
    - Split export in 2 parts (file + report)
      based on field EMPLOYEE.FREETEXT.
      Contents of field is 1 or 2.
      Based on this criteria create 2 files/reports.
  - Added field EXPORTGROUP to cdsEmpExportLine that is used
    by this exportSR.
  MRA:4-SEP-2012 TODO 21132 Problem in export payroll 'extern personeel'.
  - Export Payroll - extern personeel (LTB).
    - It does not export manual hours (corrections).
  - Solved by adding the manual-salary-hours to the queries (qrySELECT and
    qrySELECT_FILTER).
  MRA:24-SEP-2012 TODO 21132 Problem in export payroll 'extern personeel'
  - REWORK: Some hours were not shown correct, because of
    grouping on departments. Reason: It uses ProdHour-PerEmp-PerType
    for getting the hours per department/hourtype and manual corrections
    are done in salary hours-table.
  - Changed queries (qrySELECT and qrySELECT_FILTER).
  - qrySELECT_FILTER:
    - Extra problem in Oracle 10g and 11g, when last order by is used:
      - ORDER BY NAME, DEPARTMENT, HOURTYPE
      then in Oracle 10g it is OK, but in Oracle 11g it gives NULL-values
      for department.
      To solve it, this last ORDER BY is removed.
  MRA:17-OCT-2012 TODO 21132 Problem in export payroll 'extern personeel'
  - REWORK: It did not show the departments correct. Solved by changing
    the qrySELECT and qrySELECT_FILTER: Instead of 'max(department)' it
    now uses 'department'. This can give more lines when employee worked
    on different departments (linked to workspots).
  MRA:2-NOV-2012 20013723 Combine Month/Period export payroll ADP
  - During export ADP: Use 2 settings based on checkboxes per hourtype/
    hourtype-per-country to decide
    if something must be exported for 'export overtime' (period) or for
    'export payroll' (month).
  - NOTE: ONLY DO THIS FOR CUSTOMER CLF (custom-made).
  - Extra: It did not check for the export-code defined at
           absencetype-per-country.
  MRA:7-NOV-2012 TODO 21576 Export Payroll Select.
  - It sometimes did not export all hours. For example
    when there were 2 salary records 'regular hours' on 1 day,
    but with different hour-type-numbers, it only exported
    1 line (based on 1 record).
  - Solved by adding ALL to UNION ALL for qrySELECT and qrySELECT_FILTER.
  MRA:7-NOV-2012 20013723.70 Rework.
  - It shows multiple lines in report for same hourtypes.
    - AddHourTypeList had to be changed: Add extra field in key or it will
      not find it.
  MRA:8-NOV-2012 TODO 21132 Problem in export payroll 'extern personeel'
  - Sometimes it shows broken numbers for minutes.
    Reason: PHEPT-table can have broken numbers after rounding based on salary.
    Solution: Based on rounding-settings on contractgroup-level, round/trunc
              the minutes before they are exported.
  - Changes qrySELECT and qrySELECT_FILTER: Add Contractgroup_code.
  MRA:15-NOV-2012 20013723 Combine Month/Period export payroll ADP
  - Rework
    - For 'export period (overtime)' never export worked days
    - For 'export month (payroll)' always export worked days
  - Reason: Otherwise it is possible they both export days,
    leading to double days.
  MRA:19-NOV-2012 20013288
  - Export Payroll Easy Labor (ELA)
  MRA:4-DEC-2012 20013288 Rework
  - Export of Swipe-information is not done by whole shift, reason:
    It must filter on SHIFT_DATE instead of on DATETIME_IN.
    - Changed: qryUpdateSwipe
  MRA:22-JAN-2013 20013288.130 Interface Easy Labor - addition
  - Export Easy Labor
    - Needed changes:
      - Add 2 checkboxes to export-dialog: Swipes and Schedule.
      - Default Swipes checkbox is checked.
      - Based on these 2 checkboxes, export Swipes or Schedule or both
      - When both are NOT checked, give a message about �Please make
        a choice about what to export�.
      - Use a second EXPORTPAYROLL-record with EXPORT_TYPE = 'ELA2' to
        keep track of last-export-date and sequence-number.
  MRA:22-MAY-2013 TD-22543 Error during export
  - Sometimes it gives 'insufficient memory'-error.
    - Do not use 'Prepare': This takes extra memory
  MRA:18-OCT-2013 20014714
  - Addition of export payroll for WMU.
  MRA:6-APR-2018 GLOB3-110
  - Export payroll (SELECT) do not export BSN but employee number
  - Changed: qrySELECT + qrySELECT_FILTER
  MRA:8-FEB-2019 GLOB3-234
  - Export Payroll Quickbooks
  MRA:15-APR-2019 GLOB3-271
  - Export payroll to Paychex
  MRA:26-APR-2019 GLOB3-297
  - Export payroll Navision
*)

unit DialogExportPayrollDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseDMT, Db, DBTables, DBClient, Provider;

type
//CAR: 550299
  TExportType = (etNone, etADP, etADPUS, etPARIS, etSR, etREINO, etATTENT,
                 etAFAS, etSELECT, etELA, etWMU, etNTS, etQBOOKS, etPCHEX,
                 etNAVIS, etBAMBOO);

type
  TDialogExportPayrollDM = class(TDialogBaseDM)
    QueryWork: TQuery;
    TableEP: TTable;
    QueryEmpl: TQuery;
    QueryLastContract: TQuery;
    TableEPEXPORT_TYPE: TStringField;
    TableEPSEQUENCE_NUMBER: TIntegerField;
    TableEPCUSTOMER_NUMBER: TIntegerField;
    TableEPMEDIUM: TStringField;
    TableEPOPERATING_SYSTEM: TStringField;
    TableEPRELEASE_NUMBER: TIntegerField;
    TableEPPROCESS_CODE: TStringField;
    TableEPMUTATION_CODE: TStringField;
    TableEPEXPORT_CODE_DAYS: TStringField;
    TableEPTOTAL_PK_RECORDS: TIntegerField;
    TableEPWEEK_PERIOD_MONTH: TStringField;
    TableEPEXPORT_WAGE_YN: TStringField;
    QuerySHE: TQuery;
    QueryAHE: TQuery;
    QueryExtraPayment: TQuery;
    TableEPWAGE_SEPARATOR: TStringField;
    QueryPRODHOURPEPT: TQuery;
    TableEPCORPORATE_CODE: TStringField;
    TableEPBATCH_ID: TStringField;
    QueryAbsenceType: TQuery;
    cdsEmployeeADPUSData: TClientDataSet;
    cdsEmployeeADPUSDataEMPLOYEE_NUMBER: TIntegerField;
    cdsEmployeeADPUSDataWKDAYS: TIntegerField;
    cdsEmployeeADPUSDataTOTALDAYS: TIntegerField;
    QueryABSType_ADP: TQuery;
    ClientDataSetAbsType: TClientDataSet;
    DataSetProviderAbsType: TDataSetProvider;
    QueryHourType: TQuery;
    ClientDataSetHourType: TClientDataSet;
    DataSetProviderHourType: TDataSetProvider;
    QueryContract: TQuery;
    ClientDataSetContract: TClientDataSet;
    DataSetProviderContract: TDataSetProvider;
    QueryEmpContract: TQuery;
    ClientDataSetEmpl: TClientDataSet;
    DataSetProviderEmpl: TDataSetProvider;
    TableEPDAYS_PAY: TFloatField;
    QueryPHETYPE_PARIS: TQuery;
    QueryAHE_PARIS: TQuery;
    QueryWK: TQuery;
    ClientDataSetWK: TClientDataSet;
    DataSetProviderWK: TDataSetProvider;
    ClientDataSetGradeExpCode: TClientDataSet;
    DataSetProviderGradeExpCode: TDataSetProvider;
    QueryGradeExpCode: TQuery;
    ClientDataSetEmployeePARIS: TClientDataSet;
    ClientDataSetEmployeePARISEMPLOYEE_NUMBER: TIntegerField;
    ClientDataSetEmployeePARISEXPORT_CODE: TStringField;
    ClientDataSetEmployeePARISAMOUNT_HRS: TIntegerField;
    cdsEmplDays: TClientDataSet;
    cdsEmplDaysEMPLOYEE_NUMBER: TIntegerField;
    cdsEmplDaysEDATE: TDateTimeField;
    cdsEmplDaysWORK: TIntegerField;
    cdsEmplDaysTFT: TIntegerField;
    cdsEmplDaysBANK: TIntegerField;
    cdsEmplDaysILL: TIntegerField;
    cdsEmplDaysHOL: TIntegerField;
    cdsEmplDaysPAID: TIntegerField;
    cdsEmplDaysUNPAID: TIntegerField;
    cdsEmplDaysWTR: TIntegerField;
    cdsEmplDaysLABOUR: TIntegerField;
    qryEmpContractHourlyWage: TQuery;
    qryWork: TQuery;
    cdsEmpSalary: TClientDataSet;
    cdsEmpSalaryEMPLOYEE_NUMBER: TIntegerField;
    cdsEmpSalarySALARY_MINUTE: TIntegerField;
    cdsEmpSalarySALARY: TFloatField;
    cdsEmpExportLine: TClientDataSet;
    cdsEmpExportLineEMPLOYEE_NUMBER: TIntegerField;
    cdsEmpExportLineEXPORT_CODE: TStringField;
    cdsEmpExportLineWORKSPOT_CODE: TStringField;
    cdsEmpExportLineEFFICIENCY: TFloatField;
    cdsEmpExportLineREMARK: TStringField;
    cdsGroupEfficiency: TClientDataSet;
    cdsGroupEfficiencyWORKSPOT_CODE: TStringField;
    cdsGroupEfficiencyEFFICIENCY: TFloatField;
    cdsHoliday: TClientDataSet;
    cdsHolidayEMPLOYEEAVAILABILITY_DATE: TDateTimeField;
    cdsHolidayPLANT_CODE: TStringField;
    cdsHolidaySHIFT_NUMBER: TIntegerField;
    cdsHolidayEMPLOYEE_NUMBER: TIntegerField;
    cdsHolidayEXPORT_CODE: TStringField;
    cdsHolidayDEPARTMENT_CODE: TStringField;
    cdsHolidayAVAILABLE_TIMEBLOCK_1: TStringField;
    cdsHolidayAVAILABLE_TIMEBLOCK_2: TStringField;
    cdsHolidayAVAILABLE_TIMEBLOCK_3: TStringField;
    cdsHolidayAVAILABLE_TIMEBLOCK_4: TStringField;
    cdsAbsencetype: TClientDataSet;
    cdsAbsencetypeABSENCETYPE_CODE: TStringField;
    cdsAbsencetypeEXPORT_CODE: TStringField;
    tblMaxSalaryEarning: TTable;
    tblMaxSalaryBalance: TTable;
    cdsGroupEfficiencyPLANT_CODE: TStringField;
    TableEPMONTH_GROUP_EFFICIENCY_YN: TStringField;
    TableEPMULTIPLY_FACTOR_HOURS: TIntegerField;
    TableEPMULTIPLY_FACTOR_DAYS: TIntegerField;
    cdsAbsenceReason: TClientDataSet;
    cdsAbsenceReasonABSENCEREASON_CODE: TStringField;
    cdsAbsenceReasonABSENCETYPE_CODE: TStringField;
    cdsEmpExportLineDAYS_YN: TStringField;
    cdsEmpExportLineAMOUNT: TFloatField;
    cdsEmpExportLineBONUS_YN: TStringField;
    qryMaxSalaryBalance: TQuery;
    TableEPEFFICIENCY100MIN_YN: TStringField;
    cdsGroupEmployeeEfficiency: TClientDataSet;
    cdsGroupEmployeeEfficiencyPLANT_CODE: TStringField;
    cdsGroupEmployeeEfficiencyWORKSPOT_CODE: TStringField;
    cdsGroupEmployeeEfficiencyJOB_CODE: TStringField;
    cdsGroupEmployeeEfficiencyEMPLOYEE_NUMBER: TIntegerField;
    cdsGroupEmployeeEfficiencyEFFICIENCY: TFloatField;
    ClientDataSetEmployeePARISBUSINESSUNIT_CODE: TStringField;
    ClientDataSetEmployeePARISDEPARTMENT_CODE: TStringField;
    qryDeleteEPayroll: TQuery;
    qryInsertEPayroll: TQuery;
    qryExportREINO: TQuery;
    cdsEmplDaysMAT: TIntegerField; // RV035.1.
    cdsEmplDaysLAY: TIntegerField;
    cdsEmplDaysPREV: TIntegerField;
    cdsEmplDaysSEN: TIntegerField;
    cdsEmplDaysADDBANK: TIntegerField;
    cdsEmplDaysSAT: TIntegerField;
    cdsEmplDaysRSVBANK: TIntegerField;
    cdsEmplDaysSHWEEK: TIntegerField;
    cdsEmplMins: TClientDataSet;
    cdsEmplMinsEMPLOYEE_NUMBER: TIntegerField;
    cdsEmplMinsHOURTYPE_NUMBER: TIntegerField;
    cdsEmplMinsMINS: TIntegerField;
    cdsEmplCounters: TClientDataSet;
    cdsEmplCountersEMPLOYEE_NUMBER: TIntegerField;
    cdsEmplCountersHOURTYPE_NUMBER: TIntegerField;
    cdsEmplCountersMINS: TIntegerField;
    cdsEmplMinsEDATE: TDateField;
    qryExportTypeAttent: TQuery;
    qryEP_ADP_ATTENT: TQuery;
    cdsEmplMinsEXPORT_CODE: TStringField;
    cdsEmplDaysEXPORT_CODE: TStringField;
    qryExpCodeCounter: TQuery;
    QueryHourTypeXXX: TQuery;
    qryEP_AFAS_SELECT: TQuery;
    qrySELECT: TQuery;
    qrySELECT_FILTER: TQuery;
    qryAFAS: TQuery;
    cdsEmplDaysTRA: TIntegerField;
    cdsEmpExportLineEXPORTGROUP: TStringField;
    qryExportGroupSR: TQuery;
    qryEmpIsExternal: TQuery;
    qryUpdateSwipe: TQuery;
    qryUpdateSchedule: TQuery;
    qryReadLastExportDate: TQuery;
    qryUpdateLastExportDate: TQuery;
    qryReadLastSequenceNr: TQuery;
    qryUpdateLastSequenceNr: TQuery;
    qryWMU: TQuery;
    qryQBOOKS: TQuery;
    qryPCHEX: TQuery;
    qryNAVIS: TQuery;
    qryBAMBOO: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
    FIsAFAS_SELECT: Boolean;
    FCurrentExportType: TExportType;
    FPeriodMonthSign: String;
    procedure AddHourTypeMins(ADate: TDateTime; Empl, HourType,
      Min: Integer; AExportCode: String);
    procedure AddCounterMins(Empl, HourType, Min: Integer);
  public
    { Public declarations }
    FEmplSort: TStringList;
    FEmplHourType, FEmpQuaranteedHrs, FEmpHourTypeWage, FEmpHourSick,
    FEmpExtraPayment: TStringList;
    FEmplHourTypeAbsence: TStringList; // MR: For ADPUS
    procedure FillListOfEmployeeSHE(DateMin, DateMax: TDateTime;
      PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
    function ValidContr(Empl: Integer; DateSHE: TDateTime): Boolean;
    function IncreaseDay(TFTMin, BankMin, IllMin, HolMin,
      PaidMin, UnPaidMin, WTRMin, LabourMin, MatMin, LayMin, TravelMin,
      HolPrevMin, HolSenMin, HolAddBankMin, SatCredMin, HolBnkRsvMin, ShorterWeekMin: Integer;//RV067.5.
      var TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay, WTRDay,
      LabourDay, MatDay, LayDay, TravelDay,
      HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay, ShorterWeekDay //RV067.5.
      : Integer): Boolean;
    procedure FillListOfEmployeeAHE(DateMin, DateMax: TDateTime;
      PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
    function AddPerHourType(HourType, Empl: Integer):  Boolean;
    procedure AddHourTypeList(Empl, HourType, Min: Integer;
      Wage_Bonus_YN: String; Export_Code: String);
    procedure AddHourTypeAbsenceList(Empl, HourType, Min: Integer ;
      Wage_Bonus_YN: String);
    procedure AddEmplSort(Empl: Integer);
    procedure SetExportToY(Empl: Integer; DateMin, DateMax: TDateTime);
    procedure UpdateExportPayrollTable(TotalPK, Seq: Integer;
      AExportType: String = 'ADP');
    procedure GetGuaranteedHrs(Empl: Integer;
      var HourType, GuaranteedMin: Integer; var  GuaranteedExportCode: String);
    procedure AddPerEmplQuaranteedContract(Empl, HourType: Integer;
      MinExported: Integer);
    procedure GetValidEmpl(PlantFrom, PlantTo : String;
      EmployeeFrom, EmployeeTo: String);
    function CheckIfStorePeriod(Year, Period_Number: Integer;
      PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer): Boolean;
    procedure StorePeriod(Year, Period_Number: Integer;
      PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
    procedure GetWage(Empl, HourType: Integer;
      DateMin, DateMax: TDateTime; var Wage: Double);
    procedure LastValidContr(Empl: Integer;
      DateMin, DateMax: TDateTime; var Wage: Real;
      var GuaranteedDays: Integer; var GuaranteedDaysExportCode: String;
      var Worker: String);
    procedure GetHourSickPay(YearSelection, Empl: Integer;
      DateMin, DateMax, StartDate: TDateTime;
      var HourType: Integer; var Hours: Integer);
    procedure FillQueryExtraPayment(Empl: Integer; DateMin, DateMax: TDateTime);
    function DetermineExportType: TExportType;
    procedure DetermineDays(
      Empl: Integer;
      var WKDays, IllDays, HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays,
        BankDays, TFTDays, MatDays, LayDays, TravelDays,
        HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,  //RV067.5.
        ShorterWeekDay: Integer;
      var AExportCode: String);
    procedure EmplDaysRecordUPDATE(
      const Empl: Integer; const EDate: TDateTime;
      const
        WKDay, TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay,
        WTRDay, LabourDay, MatDay, LayDay, TravelDay,
        HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,  //RV067.5.
        ShorterWeekDay: Integer;
        AExportCode: String);
    procedure GetGuaranteedDays(Empl: Integer;
      DateMin, DateMax: TDateTime; var GuaranteedDays: Integer;
      var GuaranteedDaysExportCode: String);
    procedure DeleteEPayroll(ADateFrom, ADateTo: TDateTime);
    procedure InsertEPayroll(ADateFrom, ADateTo: TDateTime;
      AEmployeeNumber: Integer;
      ABusinessUnitCode, ADepartmentCode, AExportCode: String;
      AMinute: Integer);
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
    procedure FillCounters;
    function StringToExportType(AValue: String): TExportType;
    function ExportTypeToString(AValue: TExportType): String;
    function GetCountryId(APlantFrom, APlantTo: String): Integer;
    procedure UpdateHourTypePerCountry(ACountryId: Integer);
    function DetermineADP_ATTENT: Boolean;
    function DetermineExportATTENTByPlant(APlantCode: String): Boolean;
    function DetermineCounterExportCodeByEmpAbsType(AEmployeeNumber: Integer;
      AAbsenceTypeCode: String): String;
    function HourTypeFind(AHourtypeNumber: Integer;
      ACountryID: Integer=-1): Boolean;
    function AbsenceTypeFind(AAbsenceTypeCode: String;
      ACountryID: Integer=-1): Boolean;
    function DetermineAFAS_SELECT: Boolean;
    function IsPeriod: Boolean;
    property IsAFAS_SELECT: Boolean read FIsAFAS_SELECT write FIsAFAS_SELECT;
    property CurrentExportType: TExportType read FCurrentExportType
      write FCurrentExportType;
    property PeriodMonthSign: String read FPeriodMonthSign
      write FPeriodMonthSign;
  end;

var
  DialogExportPayrollDM: TDialogExportPayrollDM;

implementation

{$R *.DFM}

uses UPimsConst, SystemDMT, GlobalDMT;

procedure TDialogExportPayrollDM.FillQueryExtraPayment(Empl: Integer;
  DateMin, DateMax: TDateTime);
begin
  QueryExtraPayment.Close;
  QueryExtraPayment.ParamByName('EMPLOYEE').AsInteger := Empl;
  QueryExtraPayment.ParamByName('DATEMIN').AsDateTime := DateMin;
  QueryExtraPayment.ParamByName('DATEMAX').AsDateTime := DateMax;
  // TD-22543 Memory issue: Do not prepare!
{  if not QueryExtraPayment.Prepared then
      QueryExtraPayment.Prepare; }
  QueryExtraPayment.Open;
  if not QueryExtraPayment.Eof then
    FEmpExtraPayment.Add(IntToStr(Empl));
  while not QueryExtraPayment.Eof do
  begin
    FEmpExtraPayment.Add(
      QueryExtraPayment.FieldByName('PAYMENT_EXPORT_CODE').AsString + CHR_SEP +
      FloatToStr(QueryExtraPayment.FieldByName('SUMAMOUNT').AsFloat));
    QueryExtraPayment.Next;
  end;
end;

function TDialogExportPayrollDM.CheckIfStorePeriod(Year, Period_Number: Integer;
  PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer): Boolean;
var
  SelectStr: String;
begin
  Result := False;
  if PlantFrom <> PlantTo then
  begin
    EmployeeFrom := 1;
    EmployeeTo := 999999;
  end;
  SelectStr := 
    'SELECT ' +
    '  * ' +
    'FROM ' +
    '  EXPORTEDTOPAYROLL ' +
    'WHERE ' +
    '  YEAR_NUMBER = ' +
    IntToStr(Year) + ' AND PERIOD_NUMBER = ' + IntToStr(Period_Number) +
    '  AND FROM_PLANT_CODE = ''' + DoubleQuote(PlantFrom) +
    '''  AND TO_PLANT_CODE = ''' + DoubleQuote(PlantTo) +  '''' +
    '  AND FROM_EMPLOYEE_NUMBER = ' + IntToStr(EmployeeFrom) +
    '  AND TO_EMPLOYEE_NUMBER = ' + IntToStr(EmployeeTo);
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(UpperCase(SelectStr));
  QueryWork.Open;
  if QueryWork.RecordCount = 1 then
  begin
    Result := True;
    Exit;
  end;
end;

procedure TDialogExportPayrollDM.StorePeriod(Year, Period_Number: Integer;
  PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
var
  SelectStr: String;
begin
  if PlantFrom <> PlantTo then
  begin
    EmployeeFrom := 1;
    EmployeeTo := 999999;
  end;
  SelectStr := 
    'SELECT ' +
    '  * ' +
    'FROM ' +
    '  EXPORTEDTOPAYROLL ' +
    'WHERE ' +
    '  YEAR_NUMBER = ' +  IntToStr(Year) +
    '  AND PERIOD_NUMBER = ' + IntToStr(Period_Number) +
    '  AND FROM_PLANT_CODE = ''' +  DoubleQuote(PlantFrom) + '''' +
    '  AND TO_PLANT_CODE = ''' + DoubleQuote(PlantTo) +  '''' +
    '  AND FROM_EMPLOYEE_NUMBER = ' + IntToStr(EmployeeFrom) +
    '  AND TO_EMPLOYEE_NUMBER = ' + IntToStr(EmployeeTo);
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  QueryWork.Open;
  if not QueryWork.IsEmpty then
    Exit;
  SelectStr :=
    'INSERT INTO ' +
    '  EXPORTEDTOPAYROLL ( YEAR_NUMBER, PERIOD_NUMBER, ' +
    '  FROM_PLANT_CODE, TO_PLANT_CODE, FROM_EMPLOYEE_NUMBER, TO_EMPLOYEE_NUMBER) ' +
    '  VALUES (' + IntToStr(Year) + ', ' + IntToStr(Period_Number) +
    //Car 20.2.2004 - add DoubleQuote function
    ', ''' + DoubleQuote(PlantFrom) + ''', ''' + DoubleQuote(PlantTo) + ''', ' +
    IntToStr(EmployeeFrom) + ', ' + IntToStr(EmployeeTo) + ')';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  try
    QueryWork.ExecSQL;
  except
  end;
end;

procedure TDialogExportPayrollDM.GetGuaranteedHrs(Empl: Integer;
  var HourType, GuaranteedMin: Integer;  var GuaranteedExportCode: String);
begin
  GuaranteedMin := 0;
  GuaranteedExportCode := '';
  HourType := 0;
  if not ClientDataSetContract.
    FindKey([ClientDataSetEmpl.FieldByName('CONTRACTGROUP_CODE').AsString]) then
    Exit;
  if not HourTypeFind(ClientDataSetContract.
    FieldByName('HOURTYPE_NUMBER').AsInteger)
{  ClientDataSetHourType.
      FindKey([ClientDataSetContract.FieldByName('HOURTYPE_NUMBER').AsInteger])  }
      then
    Exit;
//CAR 11-7-2003
  if ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString = '' then
    Exit;
  HourType := ClientDataSetContract.FieldByName('HOURTYPE_NUMBER').AsInteger;
  GuaranteedExportCode := ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;
  GuaranteedMin := ClientDataSetContract.FieldByName('GUARANTEED_HOURS').AsInteger;
end;

procedure TDialogExportPayrollDM.AddPerEmplQuaranteedContract(Empl, HourType: Integer;
  MinExported: Integer);
begin
  FEmpQuaranteedHrs.Add(IntToStr(Empl));
  FEmpQuaranteedHrs.Add(IntToStr(HourType) + ' ' + IntToStr(MinExported));
end;

function TDialogExportPayrollDM.AddPerHourType(HourType, Empl: Integer):  Boolean;
begin
  Result := True;
  HourTypeFind(HourType);  { ClientDataSetHourType.FindKey([HourType]); }
  if (ClientDataSetHourType.FieldByName('OVERTIME_YN').AsString = 'Y') and
    (ClientDataSetEmpl.FindKey([Empl])) then
  begin
    // RV045.1.
    if ContractGroupTFT then
    begin
      // RV045.1. Contract Group Settings.
      if ClientDataSetContract.FindKey([
        ClientDataSetEmpl.FieldByName('CONTRACTGROUP_CODE').AsString]) then
        if (ClientDataSetContract.FieldByName('TIME_FOR_TIME_YN').AsString = 'Y') and
          (ClientDataSetContract.FieldByName('BONUS_IN_MONEY_YN').AsString = 'N') then
            Result := False;
    end
    else
    begin
      // RV045.1. Hourtype Settings.
        if (ClientDataSetHourType.FieldByName('TIME_FOR_TIME_YN').AsString = 'Y') and
          (ClientDataSetHourType.FieldByName('BONUS_IN_MONEY_YN').AsString = 'N') then
            Result := False;
    end;
  end;
end;

// list with sort empl
procedure TDialogExportPayrollDM.AddEmplSort(Empl: Integer);
var
  Index: Integer;
begin
  Index := FEmplSort.IndexOf(IntToStr(Empl));
  if Index < 0 then
     FEmplSort.Add(IntToStr(Empl));
end;

procedure TDialogExportPayrollDM.GetHourSickPay(YearSelection, Empl: Integer;
  DateMin, DateMax, StartDate: TDateTime;
  var HourType: Integer; var Hours: Integer);
var
  YearEmpl, MonthEmpl, DayEmpl, YearMax, MonthMax, DayMax: Word;
  DateEmpl: TDateTime;
  MaxMinutesSick, MinutesSick, YearSick, HourTypeSick, YearDiff: Integer;
begin
  HourType := 0;
  Hours := 0;

  YearSick := SystemDM.TablePIMSettings.FieldByName('YEARS_FOR_SICK_PAY').AsInteger;
  HourTypeSick :=
    SystemDM.TablePIMSettings.FieldByName('HOURTYPE_SICK_PAY').AsInteger;
  MinutesSick :=
    SystemDM.TablePIMSettings.FieldByName('MINUTES_SICK_PAY').AsInteger;
  MaxMinutesSick :=
    SystemDM.TablePIMSettings.FieldByName('MAX_MINUTES_SICK_PAY').AsInteger;
  if (YearSick = 0 ) or (HourTypeSick = 0 ) or (MinutesSick = 0 ) then
   Exit;
  DecodeDate(StartDate, YearEmpl, MonthEmpl, DayEmpl);
  DateEmpl := EncodeDate(YearSelection, MonthEmpl, DayEmpl);
  if (DateEmpl >= DateMin) and (DateEmpl <= DateMax) then
  begin
    DateEmpl := EncodeDate(YearEmpl + YearSick, MonthEmpl, DayEmpl);
    if (DateEmpl <= DateMax) then
    begin
      HourType := HourTypeSick;
      DecodeDate(DateMax, YearMax, MonthMax, DayMax);
// diff years
      if (MonthEmpl = 1) and (DayEmpl = 1) then
      begin
        YearDiff := YearMax - YearEmpl;
        if (MonthMax = 12) and (DayMax = 31) then
          YearDiff := YearDiff + 1;
      end
      else
      begin
        YearDiff := YearMax - YearEmpl - 1;
        if (MonthEmpl < MonthMax) or
          ( (MonthEmpl = MonthMax) and (DayEmpl <=DayMax) ) then
          YearDiff := YearDiff + 1;
      end;
// end
      Hours := Trunc(YearDiff div YearSick) * MinutesSick;
      if (Hours > MaxMinutesSick) then
        Hours := MaxMinutesSick;
      if  FEmpHourSick.IndexOf(IntToStr(Empl)) < 0 then
      begin
        FEmpHourSick.Add(IntToStr(Empl));
        FEmpHourSick.Add( IntToStr(HourTypeSick)+ CHR_SEP + IntToStr(Hours));
      end;
    end;
  end;
end;

procedure TDialogExportPayrollDM.GetValidEmpl(PlantFrom, PlantTo : String;
  EmployeeFrom, EmployeeTo: String);
begin
  QueryEmpl.Active := False;
  ClientDataSetEmpl.Close;
  QueryEmpl.ParamByName('PLANTFROM').AsString := PlantFrom;
  QueryEmpl.ParamByName('PLANTTO').AsString := PlantTo;
  QueryEmpl.ParamByName('EMPLOYEEFROM').AsInteger := StrToInt(EmployeeFrom);
  QueryEmpl.ParamByName('EMPLOYEETO').AsInteger := StrToInt(EmployeeTo);
  ClientDataSetEmpl.Open;
  ClientDataSetEmpl.LogChanges := False; // TD-22543
end;

procedure TDialogExportPayrollDM.GetWage(Empl, HourType: Integer;
   DateMin, DateMax: TDateTime; var Wage: Double);
var
  BonusPerc: Real;
  IndexWage: Integer;
  WageContract, WageReal: Real;
  GuaranteedDaysDummy: Integer;
  GuaranteedDaysExportCodeDummy, Worker: String;
begin
  Wage := 0;
  IndexWage :=
    FEmpHourTypeWage.IndexOf(IntToStr(Empl) + CHR_SEP + IntToStr(HourType) +
      CHR_SEP + ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString);
//CAR 9-7-2003 add extra check for indexWage
  if (IndexWage >= 0) and ((IndexWage + 1 ) <= (FEmpHourTypeWage.Count -1)) then
  begin
    Wage := StrToFloat(FEmpHourTypeWage.Strings[IndexWage + 1]);
    Exit;
  end;
  if HourTypeFind(HourType) { ClientDataSetHourType.FindKey([HourType]) } then
  begin
    WageReal := ClientDataSetHourType.FieldByName('MINIMUM_WAGE').AsFloat;
    //RV067.5.
    LastValidContr(Empl, DateMin, DateMax, WageContract, GuaranteedDaysDummy,
      GuaranteedDaysExportCodeDummy, Worker);
//CAR 11-8-2003
    if ((WageReal = 0) or (WageReal < WageContract)) and
      (CurrentExportType = etADP) then
    begin
      BonusPerc := ClientDataSetHourType.FieldByName('BONUS_PERCENTAGE').AsFloat;
      if (ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString =
        CHECKEDVALUE) then
        WageReal := (BonusPerc /100)* WageContract
      else
        WageReal := ((BonusPerc + 100)/100)* WageContract
    end;
    if (CurrentExportType = etADPUS) and
      (WageReal <= WageContract) then
      WageReal := 0;

    if (CurrentExportType = etADP) then
      Wage := Round(WageReal * 100)
    else
      Wage := WageReal;

    if Wage <> 0 then
    begin
      FEmpHourTypeWage.Add(IntToStr(Empl) + CHR_SEP + IntToStr(HourType) +
        CHR_SEP + ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString );
      FEmpHourTypeWage.Add(FloatToStr(Wage))
    end;
  end
end;

procedure TDialogExportPayrollDM.GetGuaranteedDays(Empl: Integer;
  DateMin, DateMax: TDateTime; var GuaranteedDays: Integer;
  var GuaranteedDaysExportCode: String);
var
  Worker: String;
  WageDummy: Real;
begin
  //RV067.5.
  LastValidContr(Empl, DateMin, DateMax, WageDummy, GuaranteedDays,
    GuaranteedDaysExportCode, Worker);
end;

procedure TDialogExportPayrollDM.LastValidContr(Empl: Integer;
  DateMin, DateMax: TDateTime; var Wage: Real;
  var GuaranteedDays: Integer;
  var GuaranteedDaysExportCode: String;
  //RV067.5.
  var Worker: String);
begin
  Wage := 0;
  GuaranteedDays := 0;
  GuaranteedDaysExportCode := '';
  QueryLastContract.Close;
  QueryLastContract.ParamByName('EMPLOYEE').AsInteger := Empl;
  // MR:28-07-2004
  // Don't use DataMin for comparing, otherwise, if there are more
  // then one contract in the period, it will find nothing.
//  QueryLastContract.ParamByName('DateMin').AsDateTime := DateMin;
  QueryLastContract.ParamByName('DateMax').AsDateTime := DateMax;
  // TD-22543 Memory issue: Do not prepare!
{  if not QueryLastContract.Prepared then
    QueryLastContract.Prepare; }
  QueryLastContract.Open;
  if not QueryLastContract.IsEmpty then
  begin
    QueryLastContract.Last;
    Wage := QueryLastContract.FieldByName('HOURLY_WAGE').AsFloat;
    GuaranteedDays := QueryLastContract.FieldByName('GUARANTEED_DAYS').AsInteger;
    GuaranteedDaysExportCode :=
      QueryLastContract.FieldByName('EXPORT_CODE').AsString;
    //RV067.5.
    Worker := QueryLastContract.FieldByName('WORKER_YN').AsString;
  end;
end;

function TDialogExportPayrollDM.ValidContr(Empl: Integer; DateSHE: TDateTime): Boolean;
begin
  QueryEmpContract.Close;
  QueryEmpContract.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
  QueryEmpContract.ParamByName('FDATE').AsDateTime := Trunc(DATESHE);
  // Remark: This query looks at a contract_type <> 2 ! (2=External)
  QueryEmpContract.Open;
  Result := not QueryEmpContract.IsEmpty;
end;

procedure TDialogExportPayrollDM.FillListOfEmployeeSHE(DateMin, DateMax: TDateTime;
  PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
var
  Empl, WorkedMin,HourType, SHSMin: Integer;
  DateSHE: TDateTime;
  CountryID: Integer; // RV071.14.
  Include: Boolean; // 20013723
  CountDays: Boolean; // 20013723
begin
  QuerySHE.Close;
  QuerySHE.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
  QuerySHE.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
  QuerySHE.ParamByName('PLANTFROM').AsString := PlantFrom;
  QuerySHE.ParamByName('PLANTTO').AsString := PlantTo;
  QuerySHE.ParamByName('DATEFROM').AsDateTime := DateMin;
  QuerySHE.ParamByName('DATETO').AsDateTime := DateMax;
  // TD-22543 Memory issue: Do not prepare!
{  if not QuerySHE.Prepared then
    QuerySHE.Prepare; }
  QuerySHE.Open;
  QuerySHE.First;
  while not QuerySHE.Eof do
  begin
    Empl := QuerySHE.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    CountryID := QuerySHE.FieldByName('COUNTRY_ID').AsInteger;
    DateSHE := QuerySHE.FieldByName('SALARY_DATE').AsDateTime;
    if  ValidContr(Empl, DateSHE) then
    begin
      WorkedMin := 0;
      while (Not QuerySHE.Eof) and
        (DateSHE = QuerySHE.FieldByName('SALARY_DATE').AsDateTime) and
        (Empl = QuerySHE.FieldByName('EMPLOYEE_NUMBER').AsInteger) do
      begin
        HourType := QuerySHE.FieldByName('HOURTYPE_NUMBER').AsInteger;
        SHSMin := Round(QuerySHE.FieldByName('SUMMIN').AsFloat);
        if HourTypeFind(HourType, CountryID) {ClientDataSetHourType.FindKey([HourType])} then
        begin
          // 20013723
          Include := True;
          CountDays := (ClientDataSetHourType.
            FieldByName('COUNT_DAY_YN').AsString = 'Y');
          if SystemDM.IsCLF and (CurrentExportType = etADP) then
          begin
            if IsPeriod then
            begin
              Include :=
                ClientDataSetHourType.
                  FieldByName('EXPORT_OVERTIME_YN').AsString = 'Y';
              // 20013723 Do NOT always count-days for 'IsPeriod'.
              // if not Include then
                CountDays := False;
            end
            else
            begin
              Include :=
                ClientDataSetHourType.
                  FieldByName('EXPORT_PAYROLL_YN').AsString = 'Y';
              // 20013723 Always count-days for 'not IsPeriod'.
              // if not Include then
                CountDays := True;
            end;
          end;
          // 20013723
          if CountDays then
            WorkedMin := WorkedMin + SHSMin;
          if Include then
          begin
            if DetermineExportType <> etSR then
            begin
              // 20013723 Also add Export_Code to this list.
              AddHourTypeList(Empl, HourType, SHSMin,
                ClientDataSetHourType.FieldByName('WAGE_BONUS_ONLY_YN').AsString,
                QuerySHE.FieldByName('EXPORT_CODE').AsString);
              AddHourTypeMins(DateSHE, Empl, HourType, SHSMin,
                QuerySHE.FieldByName('EXPORT_CODE').AsString);
            end;
          end;
        end;
        QuerySHE.Next;
        if not QuerySHE.Eof and
          ((DateSHE <> QuerySHE.FieldByName('SALARY_DATE').AsDateTime) or
          (Empl <> QuerySHE.FieldByName('EMPLOYEE_NUMBER').AsInteger)) then
        begin
           QuerySHE.Prior;
           Break;
        end;
      end;
      if (WorkedMin > 0) then
        EmplDaysRecordUPDATE(Empl, DateSHE,
          1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
          QuerySHE.FieldByName('EXPORT_CODE').AsString);
    end;
    if not QuerySHE.Eof then
      QuerySHE.Next;
  end;
end;
//RV067.5.
function TDialogExportPayrollDM.IncreaseDay(TFTMin, BankMin, IllMin, HolMin,
  PaidMin, UnPaidMin, WTRMin, LabourMin, MatMin, LayMin, TravelMin,
  HolPrevMin, HolSenMin, HolAddBankMin, SatCredMin, HolBnkRsvMin, ShorterWeekMin: Integer;//RV067.5.
  var TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay, WTRDay,
  LabourDay, MatDay, LayDay, TravelDay,
  HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay, ShorterWeekDay //RV067.5.
  : Integer): Boolean;
begin
  TFTDay := 0; BankDay := 0; IllDay := 0; HolDay := 0; PaidDay := 0;
  UnpaidDay := 0; WTRDay := 0; Labourday := 0; MatDay := 0; LayDay := 0;
  TravelDay := 0;
  HolPrevDay := 0; HolSenDay := 0; HolAddBankDay := 0; SatCredDay := 0;
  HolBnkRsvDay := 0; ShorterWeekDay := 0;
  if (TFTMin >= BankMin + IllMin + HolMin + PaidMin + UnPaidMin + WTRMin +
      LabourMin + MatMin + LayMin + HolPrevMin +
      HolSenMin + HolAddBankMin + SatCredMin +
      HolBnkRsvMin + ShorterWeekMin + TravelMin) and (TFTMin <> 0) then
    TFTDay := 1
  else
    if (BankMin >= IllMin + HolMin + PaidMin + UnPaidMin + WTRMin + LabourMin +
      MatMin + LayMin + HolPrevMin + HolSenMin + HolAddBankMin + SatCredMin +
      HolBnkRsvMin + ShorterWeekMin + TravelMin) and (BankMin <> 0) then
      BankDay := 1
    else
      if (IllMin >= HolMin + PaidMin + UnPaidMin + WTRMin + LabourMin +
        MatMin + LayMin + HolPrevMin + HolSenMin + HolAddBankMin + SatCredMin +
        HolBnkRsvMin + ShorterWeekMin + TravelMin) and (IllMin <> 0 )then
        IllDay := 1
      else
        if (HolMin >= PaidMin + UnPaidMin + WTRMin + LabourMin +
          MatMin + LayMin + HolPrevMin + HolSenMin + HolAddBankMin + SatCredMin +
            HolBnkRsvMin + ShorterWeekMin + TravelMin) and (HolMin <> 0) then
          HolDay := 1
        else
          if (PaidMin >= UnPaidMin + WTRMin + LabourMin + MatMin + LayMin
            + HolPrevMin + HolSenMin + HolAddBankMin + SatCredMin + HolBnkRsvMin +
            ShorterWeekMin + TravelMin) and
            (PaidMin <> 0) then
            PaidDay := 1
          else
            if (UnPaidMin >= WTRMin + LabourMin + MatMin + LayMin + HolPrevMin +
              HolSenMin + HolAddBankMin + SatCredMin + HolBnkRsvMin +
              ShorterWeekMin + TravelMin) and
              (UnPaidMin <> 0 ) then
              UnpaidDay := 1
            else
              if (WTRMin >= LabourMin + MatMin + LayMin + HolPrevMin +
                HolSenMin + HolAddBankMin + SatCredMin + HolBnkRsvMin +
                ShorterWeekMin + TravelMin) and (WTRMin <> 0) then
                WTRDay := 1
              else
                if (LabourMin >= MatMin + LayMin + HolPrevMin +
                  HolSenMin + HolAddBankMin + SatCredMin + HolBnkRsvMin +
                  ShorterWeekMin + TravelMin) and (LabourMin <> 0) then
                  LabourDay := 1
                else
                  if (MatMin >= LayMin  + HolPrevMin + HolSenMin + HolAddBankMin +
                    SatCredMin + HolBnkRsvMin + ShorterWeekMin + TravelMin)
                    and (MatMin <> 0) then
                    MatDay := 1
                  else
                    if (LayMin >= HolPrevMin +
                      HolSenMin + HolAddBankMin + SatCredMin + HolBnkRsvMin +
                      ShorterWeekMin + TravelMin) and (LayMin <> 0) then
                      LayDay := 1
                    else
                      if (HolPrevMin >= HolSenMin + HolAddBankMin + SatCredMin +
                        HolBnkRsvMin + ShorterWeekMin + TravelMin)
                        and (HolPrevMin <> 0) then
                        HolPrevDay := 1
                      else
                        if (HolSenMin >= HolAddBankMin + SatCredMin +
                          HolBnkRsvMin + ShorterWeekMin + TravelMin)
                          and (HolSenMin <> 0) then
                          HolSenDay := 1
                        else
                          if (HolAddBankMin >= SatCredMin +
                            HolBnkRsvMin + ShorterWeekMin + TravelMin)
                            and (HolAddBankMin <> 0) then
                            HolAddBankDay := 1
                          else
                            if (SatCredMin >= HolBnkRsvMin + ShorterWeekMin +
                              TravelMin) and (SatCredMin <> 0) then
                              SatCredDay := 1
                            else
                              if (HolBnkRsvMin >= ShorterWeekMin + TravelMin) and
                                (HolBnkRsvMin <> 0) then
                                HolBnkRsvDay := 1
                              else
                                if (ShorterWeekMin >= TravelMin) and
                                  (ShorterWeekMin <> 0) then
                                  ShorterWeekDay := 1
                                else
                                  if (TravelMin <> 0) then
                                    TravelDay := 1;

                        //!!complete for attentia
  Result :=
    (TFTDay + BankDay + IllDay + HolDay + PaidDay + UnpaidDay + WTRDay +
     LabourDay + MatDay + LayDay + HolPrevDay + HolSenDay + HolAddBankDay +
     SatCredDay + HolBnkRsvDay + ShorterWeekDay + TravelDay) > 0;
end;

//RV067.5.
// RV067.MRA.30 Also add and store exportcode in cdsEmplMins.
procedure TDialogExportPayrollDM.AddHourTypeMins(ADate: TDateTime; Empl,
  HourType, Min: Integer; AExportCode: String);
begin
  if cdsEmplMins.FindKey([ADate, Empl, Hourtype]) then
  begin
    cdsEmplMins.Edit;
    cdsEmplMins.FieldByName('MINS').AsInteger :=
      cdsEmplMins.FieldByName('MINS').AsInteger + Min;
    cdsEmplMins.Post;
  end
  else
  begin
    cdsEmplMins.Insert;
    cdsEmplMins.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    cdsEmplMins.FieldByName('EDATE').AsDateTime := ADate;
    cdsEmplMins.FieldByName('HOURTYPE_NUMBER').AsInteger := Hourtype;
    cdsEmplMins.FieldByName('MINS').AsInteger := Min;
    cdsEmplMins.FieldByName('EXPORT_CODE').AsString := AExportCode;
    cdsEmplMins.Post;
  end;
end;

procedure TDialogExportPayrollDM.AddCounterMins(Empl, HourType, Min: Integer);
begin
  if cdsEmplCounters.FindKey([Empl, Hourtype]) then
  begin
    cdsEmplCounters.Edit;
    cdsEmplCounters.FieldByName('MINS').AsInteger :=
      cdsEmplCounters.FieldByName('MINS').AsInteger + Min;
    cdsEmplCounters.Post;
  end
  else
  begin
    cdsEmplCounters.Insert;
    cdsEmplCounters.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    cdsEmplCounters.FieldByName('HOURTYPE_NUMBER').AsInteger := Hourtype;
    cdsEmplCounters.FieldByName('MINS').AsInteger := Min;
    cdsEmplCounters.Post;
  end;
end;

procedure TDialogExportPayrollDM.FillCounters;
begin
  cdsEmplCounters.EmptyDataset;
  cdsEmplMins.First;
  while not cdsEmplMins.Eof do
  begin
    AddCounterMins(
      cdsEmplMins.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      cdsEmplMins.FieldByName('HOURTYPE_NUMBER').AsInteger,
      cdsEmplMins.FieldByName('MINS').AsInteger
    );
    cdsEmplMins.Next;
  end;
end;

// 20013723 Also store export-code in this list.
procedure TDialogExportPayrollDM.AddHourTypeList(Empl, HourType, Min: Integer;
  Wage_Bonus_YN: String; Export_Code: String);
var
  IndexEmpl, Index, HourTypeMin: Integer;
  Key: String;
begin
  if AddPerHourType(HourType, Empl) then
  begin
    AddEmplSort(Empl);
    // 20013723.70 Add Export_Code to key or it will not find it!
    Key := IntToStr(Empl) + ' ' + IntToStr(HourType) + ' ' +
      WAGE_Bonus_YN + ' ' + Export_Code;
    IndexEmpl := FEmplHourType.IndexOf(Key);
    if (IndexEmpl < 0) then
    begin
      Index := FEmplSort.IndexOf(IntToStr(Empl));
      if Index < 0 then
         FEmplSort.Add(IntToStr(Empl));
      FEmplHourType.Add(Key);
      FEmplHourType.Add(IntToStr(Min));
    end
    else
    //car 9-7-2003 add extra check for indexempl
      if (IndexEmpl + 1 ) <= (FEmplHourType.Count -1) then
      begin
        HourTypeMin := StrToInt(FEmplHourType.Strings[IndexEmpl + 1]);
        FEmplHourType.Delete(IndexEmpl + 1);
        FEmplHourType.Insert(IndexEmpl + 1, IntToStr(HourTypeMin + Min));
      end;
  end;
end;
// store in a seperate list for ADPUS - because only absence minutes are store
// in this list
procedure TDialogExportPayrollDM.AddHourTypeAbsenceList(Empl, HourType, Min: Integer;
  Wage_Bonus_YN: String);
var
  IndexEmpl, Index, HourTypeMin: Integer;
begin
  if AddPerHourType(HourType, Empl) then
  begin
    AddEmplSort(Empl);
    IndexEmpl := FEmplHourTypeAbsence.IndexOf(IntToStr(Empl) + ' ' + IntToStr(HourType) +
      ' ' +   WAGE_Bonus_YN);
    if (IndexEmpl < 0) then
    begin
      Index := FEmplSort.IndexOf(IntToStr(Empl));
      if Index < 0 then
         FEmplSort.Add(IntToStr(Empl));
      FEmplHourTypeAbsence.Add(IntToStr(Empl) + ' ' + IntToStr(HourType) + ' ' +
      Wage_Bonus_YN);
      FEmplHourTypeAbsence.Add(IntToStr(Min));
    end
    else
    //car 9-7-2003 add extra check for indexempl
      if (IndexEmpl + 1 ) <= (FEmplHourTypeAbsence.Count -1) then
      begin
        HourTypeMin := StrToInt(FEmplHourTypeAbsence.Strings[IndexEmpl + 1]);
        FEmplHourTypeAbsence.Delete(IndexEmpl + 1);
        FEmplHourTypeAbsence.Insert(IndexEmpl + 1, IntToStr(HourTypeMin + Min));
      end;
  end;
end;
//RV067.5.
procedure TDialogExportPayrollDM.FillListOfEmployeeAHE(DateMin, DateMax: TDateTime;
  PlantFrom, PlantTo: String; EmployeeFrom, EmployeeTo: Integer);
var
  Empl, HourType, AHEMin, IllMin, HolMin, PaidMin, UnPaidMin, WtrMin,
  LabourMin, BankMin, TFTMin, HolPrevMin, HolSenMin, HolAddBankMin, SatCredMin,
  HolBnkRsvMin, ShorterWeekMin: Integer;
  MatMin, LayMin, TravelMin: Integer; // RV035.1.
  DateAHE: TDateTime;
  ABSType: String;
  AbsTypeDay: Boolean;
  TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay, WTRDay,
  MatDay, LayDay, TravelDay,
  LabourDay, HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,
  ShorterWeekDay: Integer;
  CountryID: Integer; // 20013723
  Include: Boolean; // 20013723
begin
  QueryAHE.Close;
  QueryAHE.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
  QueryAHE.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
  QueryAHE.ParamByName('PLANTFROM').AsString := PlantFrom;
  QueryAHE.ParamByName('PLANTTO').AsString := PlantTo;
  QueryAHE.ParamByName('DATEFROM').AsDateTime := DateMin;
  QueryAHE.ParamByName('DATETO').AsDateTime := DateMax;
  // TD-22543 Memory issue: Do not prepare!
{  if not QueryAHE.Prepared then
    QueryAHE.Prepare; }
  QueryAHE.Open;
  QueryAHE.First;

  while not QueryAHE.Eof do
  begin
    Empl := QueryAHE.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    DateAHE := QueryAHE.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
    if  ValidContr(Empl, DateAHE) then
    begin
      IllMin := 0; HolMin := 0; PaidMin := 0; UnPaidMin := 0; WtrMin := 0;
      LabourMin := 0; BankMin := 0; TFTMin := 0;
      //RV067.5.
      HolPrevMin := 0; HolSenMin := 0;
      HolAddBankMin := 0;  SatCredMin := 0; HolBnkRsvMin := 0;
      ShorterWeekMin := 0;
      MatMin := 0; LayMin := 0; // RV035.1.
      TravelMin := 0;
      while (not QueryAHE.Eof) and
        (DateAHE = QueryAHE.FieldByName('ABSENCEHOUR_DATE').AsDateTime) and
        (Empl = QueryAHE.FieldByName('EMPLOYEE_NUMBER').AsInteger) do
      begin
        CountryID := QueryAHE.FieldByName('COUNTRY_ID').AsInteger;
        AHEMin := QueryAHE.FieldByName('ABSENCE_MINUTE').AsInteger;
        AbsType := QueryAHE.FieldByName('ABSENCETYPE_CODE').AsString;
        HourType := QueryAHE.FieldByName('HOURTYPE_NUMBER').AsInteger;
        Include := True;
        if HourTypeFind(HourType, CountryID) then
        begin
          if SystemDM.IsCLF and (CurrentExportType = etADP) then
          begin
            if IsPeriod then
              Include :=
              ClientDataSetHourType.
                FieldByName('EXPORT_OVERTIME_YN').AsString = 'Y'
            else
              Include :=
                ClientDataSetHourType.
                  FieldByName('EXPORT_PAYROLL_YN').AsString = 'Y'
          end; // if
          if Include then
          begin
            case AbsType[1] of
              ILLNESS                 : IllMin := IllMin + AHEMin;
              HOLIDAY                 : // MR:6-12-2004 If 'SR' ignore holiday
                                        if DetermineExportType <> etSR then
                                          HolMin := HolMin + AHEMin
                                        else
                                          HolMin := 0;
              PAID                    : PaidMin := PaidMin + AHEMin;
              UNPAID                  : UnPaidMin := UnPaidMin + AHEMin;
              WTR                     : WTRMin := WTRMin + AHEMin;
              LABOURTHERAPY           : LabourMin := LabourMin + AHEMin;
              RSNB                    : BankMin := BankMin + AHEMin;
              TIMEFORTIMEAVAILABILITY : TFTMin := TFTMin + AHEMin;
              // RV035.1. Two extra types:
              MATERNITYLEAVE          : MatMin := Matmin + AHEMin;
              LAY_DAYS                : LayMin := LayMin + AHEMin;
              // SO-20013169
              TRAVELTIME              : TravelMin := TravelMin + AHEMin;

              HOLIDAYPREVIOUSYEARAVAILABILITY : HolPrevMin :=
                                                  HolPrevMin + AHEMin;
              SENIORITYHOLIDAYAVAILABILITY    : HolSenMin :=
                                                  HolSenMin + AHEMin;
              ADDBANKHOLIDAYAVAILABILITY      : HolAddBankMin :=
                                                  HolAddBankMin + AHEMin;
              SATCREDITAVAILABILITY           : SatCredMin :=
                                                  SatCredMin + AHEMin;
              BANKHOLIDAYRESERVEAVAILABILITY  : HolBnkRsvMin :=
                                                  HolBnkRsvMin + AHEMin;
              SHORTERWORKWEEKAVAILABILITY     : ShorterWeekMin :=
                                                  ShorterWeekMin + AHEMin;
            end; // case
            // 20013723 Also add Export_Code to this list.
            AddHourTypeList(Empl, HourType, AHEMin,
              QueryAHE.FieldByName('WAGE_BONUS_ONLY_YN').AsString,
              QueryAHE.FieldByName('EXPORT_CODE').AsString);
            AddHourTypeMins(DateAHE, Empl, HourType, AHEMin,
              QueryAHE.FieldByName('EXPORT_CODE').AsString);
            // MR: Store in separate list for ADPUS!
            if DetermineExportType = etADPUS then
              AddHourTypeAbsenceList(Empl, HourType, AHEMin,
                QueryAHE.FieldByName('WAGE_BONUS_ONLY_YN').AsString);
          end; // if Include
          QueryAHE.Next;
          if not QueryAHE.Eof and
            ((DateAHE <> QueryAHE.FieldByName('ABSENCEHOUR_DATE').AsDateTime) or
            (Empl <> QueryAHE.FieldByName('EMPLOYEE_NUMBER').AsInteger)) then
          begin
             QueryAHE.Prior;
             Break;
          end; // if
        end; // if HourTypeFind
      end; // while (2)
      AbsTypeDay := IncreaseDay(TFTMin, BankMin, IllMin, HolMin, PaidMin,
        UnPaidMin, WTRMin, LabourMin,
        MatMin, LayMin, // RV035.1.
        TravelMin,
        HolPrevMin, HolSenMin, HolAddBankMin, SatCredMin, HolBnkRsvMin, ShorterWeekMin, //RV067.5.
        TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay, WTRDay, LabourDay,
        MatDay, LayDay, TravelDay,
        //RV067.5.
        HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay, ShorterWeekDay
        ); // RV035.1.
      if AbsTypeDay then
        EmplDaysRecordUPDATE(Empl, DateAHE, 0, TFTDay, BankDay, IllDay, HolDay,
          PaidDay, UnpaidDay, WTRDay, LabourDay, MatDay, LayDay, TravelDay,
          //RV067.5.
          HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,
          ShorterWeekDay, QueryAHE.FieldByName('EXPORT_CODE').AsString
          ); // RV035.1.
    end; // if ValidContr
    if not QueryAHE.Eof then
      QueryAHE.Next;
  end; // while (1)
end; // FillListOfEmployeeAHE

procedure TDialogExportPayrollDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  CurrentExportType := etNone; // RV098.3.
  FEmplSort := TStringList.Create;
  FEmplHourType := TStringList.Create;
  FEmplHourTypeAbsence := TStringList.Create;
  FEmpQuaranteedHrs := TStringList.Create;
  FEmpHourTypeWage := TStringList.Create;
  FEmpHourSick :=  TStringList.Create;
  FEmpExtraPayment := TStringList.Create;
  // MR:08-08-2003
  cdsEmployeeADPUSData.CreateDataSet;
  cdsEmployeeADPUSData.LogChanges := False;
  cdsEmplDays.CreateDataSet;
  cdsEmplDays.LogChanges := False;
  //RV067.5.
  cdsEmplMins.CreateDataset;
  cdsEmplMins.LogChanges := False;
  cdsEmplCounters.CreateDataset;
  cdsEmplCounters.LogChanges := False;

  //CAR 26-8-2003
  // TD-22543 Memory issue: Do not prepare!
{  QuerySHE.Prepare;
  QueryAHE.Prepare;
  QueryExtraPayment.Prepare; }
  ClientDataSetAbsType.Open;
  ClientDataSetAbsType.LogChanges := False; // TD-22543
  ClientDataSetHourType.Open;
  ClientDataSetHourType.LogChanges := False; // TD-22543
  ClientDataSetContract.Open;
  ClientDataSetContract.LogChanges := False; // TD-22543
  // TD-22543 Memory issue: Do not prepare!
{  QueryEmpContract.Prepare;
  QueryEmpl.Prepare; }
  //550299
  ClientDataSetEmployeePARIS.CreateDataSet;
  // TD-22543 Memory issue: Do not prepare!
{  QueryAHE_PARIS.Prepare;
  QueryPHETYPE_PARIS.Prepare; }
  // RV045.1.
  ContractGroupTFT := GlobalDM.ContractGroupTFTExists;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
end;

procedure TDialogExportPayrollDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FEmplHourType.Free;
  FEmplHourTypeAbsence.Free;
  FEmplSort.Free;
  FEmpQuaranteedHrs.Free;
  FEmpHourTypeWage.Free;
  FEmpHourSick.Free;
  FEmpExtraPayment.Free;
  // MR:08-08-2003
  cdsEmployeeADPUSData.EmptyDataSet;
  // MR:09-07-2004
  cdsEmplDays.EmptyDataSet;
  //RV067.5.
  cdsEmplMins.EmptyDataset;
  cdsEmplCounters.EmptyDataset;

    //
  QuerySHE.Close;
  QuerySHE.UnPrepare;
  QueryAHE.Close;
  QueryAHE.Unprepare;
  QueryExtraPayment.Close;
  QueryExtraPayment.UnPrepare;
  ClientDataSetAbsType.Close;
  ClientDataSetHourType.Close;
  ClientDataSetContract.Close;
  QueryEmpContract.Close;
  QueryEmpContract.UnPrepare;
  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
   //550299
  ClientDataSetEmployeePARIS.EmptyDataSet;
  QueryAHE_PARIS.Close;
  QueryAHE_PARIS.UnPrepare;
  QueryPHETYPE_PARIS.Close;
  QueryPHETYPE_PARIS.UnPrepare;
end;

procedure TDialogExportPayrollDM.SetExportToY(Empl: Integer;
  DateMin, DateMax: TDateTime);
var
  SelectStr: String;
begin
  SelectStr := 
    'UPDATE ' +
    '  SALARYHOURPEREMPLOYEE ' +
    'SET ' +
    '  EXPORTED_YN = ''Y''' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER = ' + IntToStr(Empl) +
    '  AND SALARY_DATE >= :FDateMin AND SALARY_DATE <= :FDATEMAX AND ' +
    '  SALARY_MINUTE <> 0';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(Selectstr);
  QueryWork.ParamByName('FDATEMIN').Value := DateMin;
  QueryWork.ParamByName('FDATEMAX').Value := DateMax;
  QueryWork.ExecSQL;
end;

procedure TDialogExportPayrollDM.UpdateExportPayrollTable(
  TotalPK, Seq: Integer; AExportType: String = 'ADP');
var
  SelectStr: String;
begin
  SelectStr :=
    'UPDATE EXPORTPAYROLL ' +
    'SET SEQUENCE_NUMBER = ' + IntToStr(SEQ) + ' ' +
    ' , TOTAL_PK_RECORDS = ' + IntToStr(TOTALPK) + ' ' +
    'WHERE EXPORT_TYPE = ' + '''' + AExportType + '''';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(Selectstr);
  QueryWork.ExecSQL;
  TableEP.Refresh;
end;

// MR:For 'ADPUS'-export
//RV067.5.
function TDialogExportPayrollDM.StringToExportType(AValue: String): TExportType;
begin
  Result := etNone;
  if AValue = 'ADP' then
    Result := etADP
  else
    if AValue = 'ADPUS' then
      Result := etADPUS
    else
      if AValue = 'PARIS' then
        Result := etPARIS
      else
        if AValue = 'SR' then
          Result := etSR
        else
          if AValue = 'REINO' then
            Result := etREINO
          else
            if AValue = 'ATTENT' then
              Result := etATTENT
            else
              if AValue = 'AFAS' then
                Result := etAFAS
              else
                if AValue = 'ELA' then
                  Result := etELA
                else
                  if AValue = 'WMU' then
                    Result := etWMU
                  else
                    if AValue = 'NTS' then
                      Result := etNTS
                    else
                      if AValue = 'QBOOKS' then
                        Result := etQBOOKS
                      else
                        if AValue = 'PCHEX' then
                          Result := etPCHEX
                        else
                          if AValue = 'NAVIS' then
                            Result := etNAVIS
                          else
                            if AValue = 'BAMBOO' then
                              Result := etBAMBOO
end;

// RV071.9. 550497 Export Attentia
function TDialogExportPayrollDM.ExportTypeToString(
  AValue: TExportType): String;
begin
  case AValue of
  etADP:    Result := 'ADP';
  etADPUS:  Result := 'ADPUS';
  etPARIS:  Result := 'PARIS';
  etSR:     Result := 'SR';
  etREINO:  Result := 'REINO';
  etATTENT: Result := 'ATTENT';
  etAFAS:   Result := 'AFAS';
  etELA:    Result := 'ELA';
  etWMU:    Result := 'WMU';
  etNTS:    Result := 'NTS';
  etQBOOKS: Result := 'QBOOKS';
  etPCHEX:  Result := 'PCHEX';
  etNAVIS:  Result := 'NAVIS';
  etBAMBOO: Result := 'BAMBOO';
  else
    Result := '';
  end;
end;

function TDialogExportPayrollDM.DetermineExportType: TExportType;
begin
  Result := StringToExportType(TableEPEXPORT_TYPE.AsString);
end;

// MR:12-07-2004
procedure TDialogExportPayrollDM.DetermineDays(
 Empl: Integer;
 var WKDays, IllDays, HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays,
   BankDays, TFTDays, MatDays, LayDays, TravelDays,
   HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,  //RV067.5.
   ShorterWeekDay: Integer;
 var AExportCode: String);
begin
  WKDays := 0; IllDays := 0; PaidDays := 0; UnpaidDays := 0; HolDays := 0;
  WTRDays := 0; LabourDays := 0; BankDays := 0; TFTDays := 0; MatDays := 0;
  LayDays := 0; TravelDays := 0;
  // RV067.MRA.28 Bugfix. Initialise new variables.
  HolPrevDay := 0; HolSenDay := 0; HolAddBankDay := 0; SatCredDay := 0;
  HolBnkRsvDay := 0; ShorterWeekDay := 0; // RV067.5.
  cdsEmplDays.Filtered := False;
  cdsEmplDays.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(Empl);
  cdsEmplDays.Filtered := True;
  if not cdsEmplDays.IsEmpty then
  begin
    cdsEmplDays.First;
    while not cdsEmplDays.Eof do
    begin
      WKDays := WKDays + cdsEmplDays.FieldByName('WORK').AsInteger;
      TFTDays := TFTDays + cdsEmplDays.FieldByName('TFT').AsInteger;
      BankDays := BankDays + cdsEmplDays.FieldByName('BANK').AsInteger;
      IllDays := IllDays + cdsEmplDays.FieldByName('ILL').AsInteger;
      HolDays := HolDays + cdsEmplDays.FieldByName('HOL').AsInteger;
      PaidDays := PaidDays + cdsEmplDays.FieldByName('PAID').AsInteger;
      UnPaidDays := UnPaidDays + cdsEmplDays.FieldByName('UNPAID').AsInteger;
      WTRDays := WTRDays + cdsEmplDays.FieldByName('WTR').AsInteger;
      LabourDays := LabourDays + cdsEmplDays.FieldByName('LABOUR').AsInteger;
      MatDays := MatDays + cdsEmplDays.FieldByName('MAT').AsInteger;
      LayDays := LayDays + cdsEmplDays.FieldByName('LAY').AsInteger;
      //RV067.5.
      HolPrevDay := HolPrevDay + cdsEmplDays.FieldByName('PREV').Value;
      HolSenDay := HolSenDay + cdsEmplDays.FieldByName('SEN').Value;
      HolAddBankDay := HolAddBankDay + cdsEmplDays.FieldByName('ADDBANK').Value;
      SatCredDay := SatCredDay + cdsEmplDays.FieldByName('SAT').Value;
      HolBnkRsvDay := HolBnkRsvDay + cdsEmplDays.FieldByName('RSVBANK').Value;
      ShorterWeekDay := ShorterWeekDay + cdsEmplDays.FieldByName('SHWEEK').Value;
      // SO-20013169
      TravelDays := TravelDays + cdsEmplDays.FieldByName('TRA').AsInteger;

      AExportCode := cdsEmplDays.FieldByName('EXPORT_CODE').AsString;

      cdsEmplDays.Next;
    end;
  end;
  cdsEmplDays.Filtered := False;
end;

// MR:12-07-2004
procedure TDialogExportPayrollDM.EmplDaysRecordUPDATE(
  const Empl: Integer; const EDate: TDateTime;
  const WKDay, TFTDay, BankDay, IllDay, HolDay, PaidDay, UnpaidDay,
    WTRDay, LabourDay, MatDay, LayDay, TravelDay,
    HolPrevDay, HolSenDay, HolAddBankDay, SatCredDay, HolBnkRsvDay,
    ShorterWeekDay: Integer;
    AExportCode: String);
begin
  // MR:06-09-2004
  // Only if record does not exist, add it.
  if not cdsEmplDays.FindKey([Empl, EDate]) then
  begin
    cdsEmplDays.Insert;
    cdsEmplDays.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    cdsEmplDays.FieldByName('EDATE').AsDateTime := EDate;
    cdsEmplDays.FieldByName('WORK').Value := WKDay;
    cdsEmplDays.FieldByName('TFT').Value := TFTDay;
    cdsEmplDays.FieldByName('BANK').Value := BankDay;
    cdsEmplDays.FieldByName('ILL').Value := IllDay;
    cdsEmplDays.FieldByName('HOL').Value := HolDay;
    cdsEmplDays.FieldByName('PAID').Value := PaidDay;
    cdsEmplDays.FieldByName('UNPAID').Value := UnpaidDay;
    cdsEmplDays.FieldByName('WTR').Value := WTRDay;
    cdsEmplDays.FieldByName('LABOUR').Value := LabourDay;
    cdsEmplDays.FieldByName('MAT').Value := MatDay;
    cdsEmplDays.FieldByName('LAY').Value := LayDay;
    //RV067.5.
    cdsEmplDays.FieldByName('PREV').Value := HolPrevDay;
    cdsEmplDays.FieldByName('SEN').Value := HolSenDay;
    cdsEmplDays.FieldByName('ADDBANK').Value := HolAddBankDay;
    cdsEmplDays.FieldByName('SAT').Value := SatCredDay;
    cdsEmplDays.FieldByName('RSVBANK').Value := HolBnkRsvDay;
    cdsEmplDays.FieldByName('SHWEEK').Value := ShorterWeekDay;
    cdsEmplDays.FieldByName('EXPORT_CODE').Value := AExportCode;
    // SO-20013169
    cdsEmplDays.FieldByName('TRA').Value := TravelDay;

    cdsEmplDays.Post;
  end;
end;

// MRA:04-MAR-2009 RV023. Store results for ExportPayroll for in database,
// for easy comparison: Used for ExportParis.
procedure TDialogExportPayrollDM.DeleteEPayroll(ADateFrom,
  ADateTo: TDateTime);
begin
  with qryDeleteEPayroll do
  begin
    try
      ParamByName('DATEFROM').AsDateTime := ADateFrom;
      ParamByName('DATETO').AsDateTime := ADateTo;
      ExecSQL;
    except
      // Ignore any errors.
    end;
  end;
end;

// MRA:04-MAR-2009 RV023. Store results for ExportPayroll for in database,
// for easy comparison. Used for ExportParis.
procedure TDialogExportPayrollDM.InsertEPayroll(ADateFrom,
  ADateTo: TDateTime; AEmployeeNumber: Integer; ABusinessUnitCode,
  ADepartmentCode, AExportCode: String; AMinute: Integer);
begin
  with qryInsertEPayroll do
  begin
    try
      ParamByName('DATEFROM').AsDateTime := ADateFrom;
      ParamByName('DATETO').AsDateTime := ADateTo;
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
      ParamByName('BUSINESSUNIT_CODE').AsString := ABusinessUnitCode;
      ParamByName('DEPARTMENT_CODE').AsString := ADepartmentCode;
      ParamByName('EXPORT_CODE').AsString := AExportCode;
      ParamByName('MINUTE').AsInteger := AMinute;
      ParamByName('CREATIONDATE').AsDateTime := Now;
      ParamByName('MUTATIONDATE').AsDateTime := Now;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
    except
      // Ingore any errors.
    end;
  end;
end;

procedure TDialogExportPayrollDM.QueryEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

function TDialogExportPayRollDM.GetCountryId(APlantFrom, APlantTo: String): Integer;
var
  sqlPlants: String;
  CountCountries: Integer;
begin
  Result := -1;
  sqlPlants := Format('select distinct p.country_id from plant p where p.plant_code >= ''%s'' and  p.plant_code <= ''%s''', [APlantFrom, APlantTo]);
  CountCountries := SystemDM.GetDBValue('select count(*) from (' + sqlPlants + ')', 0);
  if CountCountries <> 1 then Exit;

  Result := SystemDM.GetDBValue(sqlPlants, -1);
end;

procedure TDialogExportPayRollDM.UpdateHourTypePerCountry(ACountryId: Integer);
begin
  SystemDM.UpdateHourTypePerCountry(ACountryId, QueryHourType);
end;

// RV067.MRA.30.
// Determine if export type is a combination of ADP and ATTENT.
// This means 2 records  should exist in export-payroll-table.
function TDialogExportPayrollDM.DetermineADP_ATTENT: Boolean;
begin
  with qryEP_ADP_ATTENT do
  begin
    Close;
    Open;
    Result := not Eof;
    Close;
  end;
end;

// RV067.MRA.30.
// Determine if export type is ATTENT based on Plant-level
// by looking for export_type linked to Plant's Country.
function TDialogExportPayrollDM.DetermineExportATTENTByPlant(
  APlantCode: String): Boolean;
begin
  Result := False;
  with qryExportTypeAttent do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    Open;
    if not Eof then
      Result := (FieldByName('EXPORT_TYPE').AsString =
        ExportTypeToString(etAttent));
    Close;
  end;
end;

function TDialogExportPayrollDM.DetermineCounterExportCodeByEmpAbsType(
  AEmployeeNumber: Integer; AAbsenceTypeCode: String): String;
begin
  Result := '';
  with qryExpCodeCounter do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('ABSENCETYPE_CODE').AsString := AAbsenceTypeCode;
    Open;
    if not Eof then
      Result := FieldByName('EXPORT_CODE').AsString;
    Close;
  end;
end;

// RV071.14. 550497
// Also take Country into account when searching for the hourtype.
function TDialogExportPayrollDM.HourTypeFind(AHourtypeNumber: Integer;
  ACountryID: Integer=-1): Boolean;
begin
  Result := False;
  // First test with country-id
  if ClientDataSetHourType.
    Locate('COUNTRY_ID;HOURTYPE_NUMBER',
      VarArrayOf([ACountryID, AHourtypeNumber]),
      []) then
    Result := True;
  if not Result then // Now test for -1.
    if ClientDataSetHourType.
      Locate('COUNTRY_ID;HOURTYPE_NUMBER',
        VarArrayOf([-1, AHourtypeNumber]),
        []) then
      Result := True;
end;

// RV087.2.
function TDialogExportPayrollDM.DetermineAFAS_SELECT: Boolean;
begin
  with qryEP_AFAS_SELECT do
  begin
    Close;
    Open;
    Result := not Eof;
    IsAFAS_SELECT := Result;
    Close;
  end;
end;

// 20013723
function TDialogExportPayrollDM.AbsenceTypeFind(AAbsenceTypeCode: String;
  ACountryID: Integer=-1): Boolean;
begin
  Result := False;
  // First test with country-id
  if ClientDataSetAbsType.Locate('COUNTRY_ID;ABSENCETYPE_CODE',
    VarArrayOf([ACountryID, AAbsenceTypeCode]), []) then
    Result := True;
  if not Result then // Now test for -1. (not-country-related).
    if ClientDataSetAbsType.Locate('COUNTRY_ID;ABSENCETYPE_CODE',
      VarArrayOf([-1, AAbsenceTypeCode]), []) then
      Result := True;
end; // AbsenceTypeFind

// 20013723
function TDialogExportPayrollDM.IsPeriod: Boolean;
begin
  Result := PeriodMonthSign <> EXPORTPAYROLL_MONTH;
end;

end.
