(*
  Changes
    MRA:30-OCT-2009 RV040.
      - When this report is run several times after each other
        with a selection for 1 week, it gives a memory problem.
        - Clear all lists at start of action.
        - Do not use 'Prepare' for TQuery, this costs lots of memory!
          Note: Because 'UnPrepare' is not used, the memory is never released!
                Even when 'UnPrepare' is used, it costs memory!
    MRA:1-DEC-2009 RV045.2.
      - Report gave double absence-hours when multiple weeks
        were selected. Cause: Wrong join with department.
        It was only joined by DEPARTMENT_CODE, not by PLANT_CODE.
      - Also: When the sum of minutes is 0, it can lead to double hours!
        This can happen when manual changes are made where on 1 day
        hours are corrected leading to 0 hours, example:
      - Truncate description of hourtype-description.
      - Make more space between team-code and team-description.
      - Total week: Use UPimsMessageRes-string instead of literal.
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:28-JAN-2010. RV050.4.1.
    - When 'AllEmployees' is False, then not only select on Employee-From-To,
      but ALSO on the plant from the employee! Because then only 1 plant is
      selected.
    MRA:28-JAN-2010. RV050.4.2.
    - Bugfix for FDATECURRENT-argument-error.
    MRA:15-FEB-2010. RV054.3. 889944.
    - Shows some wrong texts in the report.
    - Make more room for employee.
    - Make more room for teams.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:29-APR-2010. RV062.1.
    - Limit size of hourtype-description.
    MRA:29-APR-2010. RV062.2.
    - SetMemoParam: Set this from 4 to higher position!
      Otherwise it can lead to one double line after the 4th line, when
      report is printed or printed to PrimoPDF. When only a preview is
      done then it is OK.
    SO: 07-JUN-2010. RV065.1
    - replaced the export numbers shown in minutes with hh:mm format
    MRA:6-SEP-2010. RV067.MRA.29 Bugfix.
    - When using 'Include employees not connected to teams', it gives
      wrong hours (multiplied).
    - It must be possible to show hours made by employees made on other
      plants.
    - When using 'include employees not connected to teams', it must be
      possible to show salary-hours made by employees from other plants.
    MRA:17-SEP-2010 RV067.MRA.33 550480
    - Add option to show plant info for production hours. This
      can be necessary to see employee's production made
      in other plants.
    MRA:15-OCT-2010 RV071.17. Bugfixing.
    - Set Memo Height to higher value.
    MRA:19-OCT-2010 RV072.3. Bugfixing.
    - Report hours per employee - salary
      - Sometimes it does not show the absence hours
        when 'show hourtypes' is unchecked.
  MRA:17-NOV-2010 RV080.1.
  - Bug with double lines when printing to e.g. PrimoPDF.
    Preview is OK, but printing to a printer or PrimoPDF
    gives double lines.
  - Reports and Memo-mechanism.
    - Put Memo-mechanism in a seperate class, so
      it can be created and freed when needed.
      NOTE: This did not solve the problem.
  - IMPORTANT: When the Memo-mechanism is NOT used and the QRMemo-
    components are all set to AutoSize=True and AutoStretch=True,
    then it works also without the 'double lines' problem!
  MRA:6-DEC-2010 RV082.11.
  - Hourtype description was shorter than before,
    resulting in showing not complete description.
  - Cause: Component QRMemoHTDesc was used for this,
    but setting for AutoSize was False, this should be
    True.
  MRA:28-JAN-2011 RV085.19. SC-50015401. SR-20011412. CVL (tailor made).
  - Report Hours Per Employee
    - For CVL a different salary-hour-output is needed.
      - All hourtypes must be shown, even if they are 0.
    Additional Business Rules:
      - Normal hours in salary report must be based on exceptional hours
        (per contract group per employee) with hourtype 1 (normal hours), but
         when the real normal hours are less then show the real normal hours.
      - Show always all possible hour types with correct hours, even if
        they are 0.
      - Totals per day are the shown normal hours + overtime hours. The
        'overtime hours' are based on overtime definitions per contract group
         of the employee.
      - The Totals shown at the right are (also) the totals of the shown normal
        hours + overtime hours.
      - The Total shown for 'Contract'-column is based on exceptional hours
        (per contract group per employee) with hourtype 1 (normal hours).
    - Only show this different salary-hour-output when:
      - PIMSSETTING.CODE = 'CVL'.
      - Export-checkbox is checked.
    MRA:11-MAR-2011 RV087.5. Correction SC-50015401. SR-20011412. CVL (tailor made).
    - Look for total hours made on one day (not only for hourtype 1) and if
      that is less than Except-hrs-normal-hrs-def, then show that in report.
      - Query changed.
    MRA:1-JUL-2011. RV093.7. Bugfix.
    - Hours Per Employee (Salary) Report
      - With LTB-database: Gives sometimes a (last)
        line with only the hourtype-number without
        the hours.
      - Reason: Hour type descriptions are truncated to 16 positions, during
        the addition of them to TMemo-lines this can go wrong, when
        they are similar for first 16 positions!
    MRA:5-OCT-2011. RV098.1. Debug info added.
    - For same issue about RV093.7:
      - It happened again, but the cause was a description of a hour type,
        that was the same as an existing one:
        - Hour type 1 with description 'Normal hours'
        - Hour type 200 with description 'Normal hours'
      - This results in a missing line for 200.
      - To solve this, the 2nd description must be changed by the user!
    MRA:25-OCT-2011 RV099.2. Bugfix.
    - Report Hours per Employee (Salary)
      - When there are hourtype-descriptions with the same
        name (but with different hourtype-numbers), then the
        report is wrong. This results in rows that are moved 1
        row up.
        - Reason: In the report it tries to put
          hourtype-rows with the same description on 1 row,
          but because of the similar names this goes wrong.
        - Solution:
          - During processing the report, put the hourtype-number
            before the hourtype-description separated by ';',
            to make them unique. At the moment it should be printed,
            removed this part.
    MRA:2-NOV-2011 RV100.1. 20012124. Tailor made CVL.
    - Report Salary hours
      - When hours were booked on hour types that are linked to
        a workspot (field: overruled contract group), then these
        hours must be shown as one extra line at the end of
        the report.
      - What it should show:
        - First a total of salary hours must be shown (soil + non-soil),
          per hour type.
        - Last line shows all total hours made in soil.
      - Additional Business Rules to make this work:
        - The 'soil' hourtype-numbers must be based on the 'non-soil'
          hourtype-numbers plus 100.
        - Example:
          Hourtype-number 12 has a soil-hourtype-number: 112.
          Hourtype-number 15 has a soil-hourtype-number: 115.
        - Further:
          - A fixed hourtype-number must be used to show the last soil-line,
            see const-definitions for CVL in this report.
            The description of this hourtype-number will be used to show as
            description in the report.
          - A fixed contract group must be used that is linked to the
            workspot for 'soil', see const-definitions for CVL in this report.
          - It only works for exceptional-hour-definitions linked to the
            soil-contract group.
    MRA:4-SEP-2012 TODO 21095
    - Problem in exportfile report -> not editable
    - When doing an export of this report the hours
      are not editable. Reason: They have additional spaces
      around the hours. These must be removed.
    - NOTE: I tried to change this by leaving out spaces, but this gives other
      problems when looking at the file with Excel:
      - It adds a ':00' to the times, instead of 05:00 you see 05:00:00
      - It adds a date when a time is larger than 24 hours which can happen
        for totals. For example: 30:00 is shown as 1-1-1900 30:00:00
    - For totals: it was showing columns wrong: 1 column was shifted to the
      left. This has been solved.
    MRA:1-OCT-2012 TODO 21095
    - Remove leading spaces during export.
    MRA:17-NOV-2015 ABS-5675
    - Always show contract hours, this is always based on weeks.
    MRA:23-FEB-2018 GLOB3-86
    - Change for hours per employee report
    - For salary hours:
      - Higlight a line when hourtype is linked to a workspot.
    MRA:27-AUG-2018 PIM-387
    - Report salary hours not always right
    - When not all hourtypes were selected, do not show contract-hours+diff.
    MRA:16-OCT-2018 PIM-399
    - Salary hours report shows hours 6 times too high
    - The check for a workspot with an hourtype gave this problem, to
      solve it the query had to be changed.
    MRA:28-JAN-2019 GLOB3-205
    - Time Card Report
    - Show hourly wage when found in employee contract
    - Show total hourly payment. When it is overtime, calculate more via bonus.
    - Show extra employee payments as extra block after totals per employee.
    MRA:4-FEB-2019 GLOB3-207
    - Employee Hours Summary
    - Add Department selection
    MRA:27-MAY-2019 GLOB3-311
    - Report hours per employee salary gives an error
    - When using pimssetting.code = 'NTS'
    - Added restriction for above: Only when hourtypes are shown, or it gives
      also an error about not a group by expression.
    - For BuildSalary: Use field-names instead of numbers for order by to
      prevent mistakes.
*)

unit ReportHoursPerEmplQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, DBClient;
  {UReportMemoClass} // RV080.1. Do not use this!

// RV050.4.2. Values used for 'FShowEmployee'
const
  EMPLOYEE_ACTIVE = 0;
  EMPLOYEE_NONACTIVE = 1;
  EMPLOYEE_ALL = 2;
  REPORTTYPE_PRODUCTION = 0;
  REPORTTYPE_SALARY = 1;
  CVL_SOIL_CONTRACTGROUP_CODE = '100'; // RV100.1.
  CVL_SOIL_HOURTYPE_NUMBER = 999;    // RV100.1.

type
  PTPHERecord = ^TPHERecord;
  TPHERecord = record
    Employee: Integer;
    PlantCode: String;
    PlantDesc: String;
    DatePHE: TDateTime;
    MinSum: Integer;
    MinPayed : Integer;
    BreakMin: Integer;
    HourType: String; // GLOB3-205
    AddHrs: Boolean; // RV085.19.
    IsWorkspotHourType: Boolean; // GLOB3-86
    RecordType: Integer; // GLOB3-205
  end;

  // RV085.19. Keeps track of Hourtypes that must be added for totals.
  PTADDHTRecord = ^TADDHTRecord;
  TADDHTRecord = record
    Employee: Integer;
    Hourtype: String;
  end;

  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FReportType, FShowEmployee, FHourTypeFrom, FHourTypeTo: Integer;
    FDeptFrom, FDeptTo: String;
    FTeamFrom, FTeamTo: String;
    FShowAllDate, FShowSelection, FAllHourType, FShowHourType,
    FShowAllTeam, FShowEmpNoTeam, FShowWeek, FPageTeam: Boolean;
    FExportToFile, FAllEmployees, FSuppressZeroes, FIncludeNotConnectedEmp,
    FShowPlantInfo: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      ReportType, ShowEmployee, HourTypeFrom, HourTypeTo: Integer;
      DeptFrom, DeptTo: String;
      TeamFrom, TeamTo: String;
      ShowAllDate, ShowSelection, AllHourType, ShowHourType,
      ShowAllTeam, ShowEmpNoTeam, ShowWeek, PageTeam: Boolean;
      ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp,
      ShowPlantInfo: Boolean);
  end;
  TWorkedHours = Array[1..7] of Integer;

  TReportHoursPerEmplQR = class(TReportBaseF)
    QRGroupTeam: TQRGroup;
    QRGroupEmp: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelShowEmpl: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLabel5: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRBandSumarry: TQRBand;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabelDirMO: TQRLabel;
    QRLabelDirTU: TQRLabel;
    QRLabelDirWE: TQRLabel;
    QRLabelDirTH: TQRLabel;
    QRLabelDirFR: TQRLabel;
    QRLabelDirSA: TQRLabel;
    QRLabelDirSU: TQRLabel;
    QRLabelTotDIR: TQRLabel;
    QRLabelIndMO: TQRLabel;
    QRLabelIndTU: TQRLabel;
    QRLabelIndWE: TQRLabel;
    QRLabelIndTH: TQRLabel;
    QRLabelIndFR: TQRLabel;
    QRLabelIndSA: TQRLabel;
    QRLabelIndSU: TQRLabel;
    QRLabelTotInd: TQRLabel;
    QRBandFooterEmpl: TQRBand;
    QRLabelEmplMO: TQRLabel;
    QRLabelEmplTU: TQRLabel;
    QRLabelEmplWE: TQRLabel;
    QRLabelEmplTUF: TQRLabel;
    QRLabelEmplFR: TQRLabel;
    QRLabelEmplSA: TQRLabel;
    QRLabelEmplSU: TQRLabel;
    QRLabelEmplTotWK: TQRLabel;
    QRLabelEmplTotABS: TQRLabel;
    QRLabelEmplTot: TQRLabel;
    QRLabelProdHrs: TQRLabel;
    QRLabelNProdHrs: TQRLabel;
    QRLabelProdMO: TQRLabel;
    QRLabelNProdMO: TQRLabel;
    QRLabelProdTU: TQRLabel;
    QRLabelNProdTU: TQRLabel;
    QRLabelProdWE: TQRLabel;
    QRLabelNProdWE: TQRLabel;
    QRLabelProdTH: TQRLabel;
    QRLabelNProdTH: TQRLabel;
    QRLabelProdFR: TQRLabel;
    QRLabelNProdFR: TQRLabel;
    QRLabelProdSA: TQRLabel;
    QRLabelNProdSA: TQRLabel;
    QRLabelProdSU: TQRLabel;
    QRLabelNProdSU: TQRLabel;
    QRLabelTotProd: TQRLabel;
    QRLabelTotNProd: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRLabelTotMO: TQRLabel;
    QRLabelTotTU: TQRLabel;
    QRLabelTotWE: TQRLabel;
    QRLabelTotTH: TQRLabel;
    QRLabelTotFR: TQRLabel;
    QRLabelTotSA: TQRLabel;
    QRLabelTotSU: TQRLabel;
    QRLabelTotWK: TQRLabel;
    QRLabelTotABS: TQRLabel;
    QRLabelTotal: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText1: TQRDBText;
    QRMemoHT: TQRMemo;
    MO: TQRMemo;
    TU: TQRMemo;
    WE: TQRMemo;
    TH: TQRMemo;
    FR: TQRMemo;
    SA: TQRMemo;
    SU: TQRMemo;
    QRMemoTOT: TQRMemo;
    QRMemoHTDesc: TQRMemo;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRBandTitleEmp: TQRBand;
    QRLabel6: TQRLabel;
    QRLabelHourType: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabelAbsence: TQRLabel;
    QRLabelTot: TQRLabel;
    QRShape1: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel7: TQRLabel;
    QRBandFooterTeam: TQRBand;
    QRLabelTeamDesc: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel37: TQRLabel;
    QRMemoAbsTOT: TQRMemo;
    QRLabelManualCor: TQRLabel;
    QRLabelCORMO: TQRLabel;
    QRLabelCorTU: TQRLabel;
    QRLabelCORWE: TQRLabel;
    QRLabelCORTH: TQRLabel;
    QRLabelCORFR: TQRLabel;
    QRLabelCorSA: TQRLabel;
    QRLabelcORSU: TQRLabel;
    QRLabelTotCor: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelPlantTo: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLblTeamTo: TQRLabel;
    QRLabel38: TQRLabel;
    QRLblTeamFrom: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLblHourlyWageTitle: TQRLabel;
    QRLblHourlyWage: TQRLabel;
    QRLblHourlyWageTitle2: TQRLabel;
    QRLblHourlyWage2: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    ChildBandHourPayments: TQRChildBand;
    ChildBandExtraPayments: TQRChildBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRMemoPED: TQRMemo;
    QRMemoPEC: TQRMemo;
    QRLabelExtraPayments: TQRLabel;
    QRMemoPETot: TQRMemo;
    QRLabel42: TQRLabel;
    QRLblTotalHourlyPayment: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelEmplMOPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplTUPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplWEPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplTUFPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplFRPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplSAPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplSUPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplTotWKPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplTotABSPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplTotPrint(sender: TObject; var Value: String);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSumarryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRLabelW1Print(sender: TObject; var Value: String);
    procedure QRLabelW2Print(sender: TObject; var Value: String);
    procedure QRLabelW3Print(sender: TObject; var Value: String);
    procedure QRLabelW4Print(sender: TObject; var Value: String);
    procedure QRLabelW5Print(sender: TObject; var Value: String);
    procedure QRLabelW6Print(sender: TObject; var Value: String);
    procedure QRLabelW7Print(sender: TObject; var Value: String);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel23Print(sender: TObject; var Value: String);
    procedure QRDBText1Print(sender: TObject; var Value: String);
    procedure QRBandTitleEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure QRLabelEmplPrint(sender: TObject; var Value: String);
    procedure QRGroupEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTeamDescPrint(sender: TObject; var Value: String);
    procedure QRLabel10Print(sender: TObject; var Value: String);
    procedure QRLabel12Print(sender: TObject; var Value: String);
    procedure QRLabel14Print(sender: TObject; var Value: String);
    procedure QRLabel22Print(sender: TObject; var Value: String);
    procedure QRLabel24Print(sender: TObject; var Value: String);
    procedure QRLabel25Print(sender: TObject; var Value: String);
    procedure QRLabel26Print(sender: TObject; var Value: String);
    procedure QRLabel27Print(sender: TObject; var Value: String);
    procedure QRLabel28Print(sender: TObject; var Value: String);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRBandFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSumarryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabel34Print(sender: TObject; var Value: String);
    procedure FormDestroy(Sender: TObject);
    procedure ChildBandProdBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel41Print(sender: TObject; var Value: String);
    procedure QRLabel15Print(sender: TObject; var Value: String);
    procedure QRGroupPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandExtraPaymentsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandHourPaymentsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandHourPaymentsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandExtraPaymentsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
    //
    APHERecord : PTPHERecord;
    ADDHTRecord: PTADDHTRecord; // RV085.19.
    EmplSumABSMin: Integer;
    FDayDiff: Integer;
    FLastHourType: Integer;
    FFirstHourType: Integer;
//    function BuildSalarySelectionCVL: String; // RV085.19.
    function BuildSalarySelectionCVL2: String; // RV100.1.
    function BuildSalarySelectionDefault: String;
    function IsTailorMadeCVL: Boolean; // RV085.19.
    function IsTailorMadeCVL2: Boolean; // RV100.1.
    procedure AddListHT(AEmployee: Integer;
      AHourtype: String); // RV085.19.
    function ListHTExists(AEmployee: Integer;
      AHourtype: String): Boolean; // RV085.19.
//    AMemoClass: TMemoClass; // RV080.1. Do not use this!
    function DecodeHrsMin(HrsMin: Integer):String;
    function AllHourTypes: Boolean; // PIM-387
    function DetermineHourlyWage(AEmployeeNumber: Integer): String;
    procedure InitExtraPayments;
    function DetermineExtraPayments(AEmployeeNumber: Integer): Boolean;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate, FMaxDate: TDateTime;
    ProdHourWeek,
    TeamProdHour,
    ProdHour,
    AbsenceHour,
    HourTypeHours,
    TotProdHour,
    TotDirHour,
    TotIndHour,
    TotProductiveHour,
    TotNProductiveHour: TWorkedHours;
    EmployeeHourlyWage: Double; // GLOB3-205
    EmployeeTotalPayment: Double; // GLOB3-205
    FEmplPrint: Boolean;
    FEmplContrMin, TotAbsence, TotalEmpl, TeamTotAbsence: Integer;
    FPrintBandChild, FTeamPrintBand: Boolean;
//    WorkedHoursLabelCaption: String;
    //  performance lists Car 20-6-2003
    FListTeamPHE,
    FListTeamABS,
    FListTeamSAL: TStringList;

    FListPHE,
    FListABS,
    FListDirHrs,
    FListIndHrs,
    FListNonProdHrs,
    FListProdHrs,
    FListABSSAL,
    FListIndHrsSAL,
    FListDirHrsSAL: TList;

    FListHT: TList; // RV085.19.
    TotalEmplExceptHrDefMinute: Integer; // RV085.19.
    //
    FQRBandDetailEmployeeHeight: Integer;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const HourTypeFrom, HourTypeTo: Integer;
      const DeptFrom, DeptTo: String;
      const TeamFrom, TeamTo: String;
      const ReportType, ShowEmployee: Integer;
      const ShowAllDate, ShowSelection, AllHourType, ShowHourType,
      ShowAllTeam, ShowEmpNoTeam, ShowWeek, PageTeam: Boolean;
      ExportToFile, AllEmployees, SuppressZeroes,
      IncludeNotConnectedEmp, ShowPlantInfo: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure FillWeekPerEmployee(FList:TList;
      Employee: Integer; SetDirHrs, FillHourType: Boolean;
      var ProdWeek: TWorkedHours; SetAbsenceHrs, FillWeek: Boolean);

    function SetProduction: Boolean;
    function SetSalary: Boolean;
    function GetRecordQueryEmpl: Boolean;
    procedure FillMemoHTPerDays(Employee: Integer; HourType: String; WorkedHours: Boolean);
    function CheckEmptyTeamForPHE(Team_Code: String): Boolean;
    procedure SetQueryTeamParameters(SelectStr: String);
    function CheckEmptyTeamForSAL(Team_Code: String): Boolean;
    function CheckEmptyTeamForABS(Team_Code: String): Boolean;

    //performance CAR 20-6-2003
    procedure BuildListTeamForPHE;
    procedure BuildListTeamForABS;
    procedure BuildListTeamForSAL;

    procedure BuildListABS;
    function SetQueryParametersNoEmpl(SelectStr: String;
      QuerySel: TQuery): Boolean;
    procedure BuildDirIndHours;
    procedure BuildProdNonProdHrs;
    procedure FreeItemList(TmpList: TList);
    procedure ClearLists;
    procedure FreeList(TmpList: TList);
    procedure FillList(QuerySelect: TQuery;
      TmpList: TList; FillHT, FillBreak, FillPlant: Boolean);

   // 550292
    procedure FillHrsPerWeek(Min_Day: Integer;  WeekProdDate: TDateTime);
    procedure FillHrsPerHT(WorkedHours: Boolean;
      Hour_Type, Minute_Days: Integer; Hour_Type_Description: String;
      WeekProdDate: TDateTime; IsWorkspotHourtype: Boolean);
    procedure FillHrsPerPlantWeek(APlantCode, APlantDesc: String;
      AMin_Day: Integer;  AProdDate: TDateTime);
    procedure FillHrsPerPlantDay(APlantCode, APlantDesc: String;
      AMin_Day: Integer;  AProdDate: TDateTime);
    procedure FillMemoWeekPerDays;
    procedure FillMemoWeekPerDays_HT;
    procedure FillMemoPlantWeekPerDays;
    procedure FillMemoPlantPerDays;
    property FirstHourType: Integer read FFirstHourType write FFirstHourType; // PIM-387
    property LastHourType: Integer read FLastHourType write FLastHourType; // PIM-387
  end;

var
  ReportHoursPerEmplQR: TReportHoursPerEmplQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportHoursPerEmplDMT, ListProcsFRM, UPimsMessageRes,
  UGlobalFunctions, UPimsConst;

// Can be used to save the query.
{$IFDEF DEBUG5}
function MyFile: String;
var
  Nr: Integer;
begin
  Nr := 1;
  repeat
    Result := 'c:\temp\' + Format('Rphe_%d', [Nr]) + '.sql';
    inc(Nr);
  until not FileExists(Result);
end;
{$ENDIF}

// RV085.19.
// RV100.1. Not used anymore.
function TReportHoursPerEmplQR.IsTailorMadeCVL: Boolean;
begin
  Result := False;
{
  if (SystemDM.IsCVL and QRParameters.FShowHourType
    and QRParameters.FExportToFile) then
    Result := True
  else
    Result := False;
}
end;

// MR:06-12-2002
procedure ExportDetail(
  TeamCode, EmployeeNumber, EmployeeName, Hourtype, HourtypeDesc,
  D1, D2, D3, D4, D5, D6, D7, TotalWorked, Absence, Total: String);
begin
  ExportClass.AddText(
    TeamCode + ExportClass.Sep +
    EmployeeNumber + ExportClass.Sep +
    EmployeeName + ExportClass.Sep +
    Hourtype + ExportClass.Sep +
    HourtypeDesc + ExportClass.Sep +
    D1 + ExportClass.Sep +
    D2 + ExportClass.Sep +
    D3 + ExportClass.Sep +
    D4 + ExportClass.Sep +
    D5 + ExportClass.Sep +
    D6 + ExportClass.Sep +
    D7 + ExportClass.Sep +
    TotalWorked + ExportClass.Sep +
    Absence + ExportClass.Sep +
    Total
    );
end;

// MR:10-12-2002
function HrMinToMin(HrMin: String): String;
var
  Hr: String;
  Min: String;
begin
 Result := '0';
  try
    Hr  := Copy(HrMin, 1, Pos(':', HrMin) - 1);
    Min := Copy(HrMin, Pos(':', HrMin) + 1, Length(HrMin));
    if Hr <> '' then
      Result := IntToStr(StrToInt(Hr) * 60 + StrToInt(Min));
  except
    Result := '0';
  end;
end;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  ReportType, ShowEmployee, HourTypeFrom, HourTypeTo: Integer;
  DeptFrom, DeptTo: String;
  TeamFrom, TeamTo: String;
  ShowAllDate, ShowSelection, AllHourType, ShowHourType,
  ShowAllTeam, ShowEmpNoTeam, ShowWeek, PageTeam: Boolean;
  ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp,
  ShowPlantInfo: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FHourTypeFrom := HourTypeFrom;
  FHourTypeTo := HourTypeTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FReportType := ReportType;
  FShowAllDate := ShowAllDate;
  FShowSelection := ShowSelection;
  FShowEmployee := ShowEmployee;
  FAllHourType := AllHourType;
  FShowHourType := ShowHourType;
  FShowAllTeam := ShowAllTeam;
  FShowEmpNoTeam := ShowEmpNoTeam;
  //Car: 550292
  FShowWeek := ShowWeek;
  FPageTeam := PageTeam;
  FExportToFile := ExportToFile;
  FAllEmployees := AllEmployees;
  FSuppressZeroes := SuppressZeroes;
  FIncludeNotConnectedEmp := IncludeNotConnectedEmp;
  FShowPlantInfo := ShowPlantInfo;
end;

function TReportHoursPerEmplQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const HourTypeFrom, HourTypeTo: Integer;
  const DeptFrom, DeptTo: String;
  const TeamFrom, TeamTo: String;
  const ReportType, ShowEmployee: Integer;
  const ShowAllDate, ShowSelection, AllHourType, ShowHourType,
    ShowAllTeam, ShowEmpNoTeam, ShowWeek, PageTeam: Boolean;
    ExportToFile, AllEmployees, SuppressZeroes, IncludeNotConnectedEmp,
    ShowPlantInfo: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      ReportType, ShowEmployee, HourTypeFrom, HourTypeTo,
      DeptFrom, DeptTo,
      TeamFrom, TeamTo,
      ShowAllDate, ShowSelection,  AllHourType, ShowHourType,
      ShowAllTeam, ShowEmpNoTeam, ShowWeek, PageTeam, ExportToFile,
      AllEmployees, SuppressZeroes, IncludeNotConnectedEmp, ShowPlantInfo);
  end;
  SetDataSetQueryReport(ReportHoursPerEmplDM.QueryEmpl);
end;

function TReportHoursPerEmplQR.SetQueryParametersNoEmpl(SelectStr: String;
  QuerySel: TQuery): Boolean;
begin
  QuerySel.Active := False;
  QuerySel.UniDirectional := False;
  QuerySel.SQL.Clear;
//  QuerySel.SQL.Add(UpperCase(SelectStr));
  QuerySel.SQL.Add(SelectStr);
// ReportHoursPerEmplDM.QueryProductionMinute.SQL.SaveToFile('c:\temp\rephrsperemp_salary.sql');
{$IFDEF DEBUG5}
  QuerySel.SQL.SaveToFile(MyFile);
{$ENDIF}
  if not QRParameters.FShowAllDate then
  begin
    QuerySel.ParamByName('FSTARTDATE').Value := QRParameters.FDateFrom;
    QuerySel.ParamByName('FENDDATE').Value :=  QRParameters.FDateTo;
  end;
//  QuerySel.UnPrepare;
//  if not QuerySel.Prepared then // RV040.
//    QuerySel.Prepare;
  QuerySel.Active := True;
  if (QuerySel.IsEmpty) then
    Result := False
  else
    Result := True;
end;

function TReportHoursPerEmplQR.GetRecordQueryEmpl: Boolean;
var
  SelectStr: String;
//  WeekDayMin, WeekDayMax: Word; // ABS-5675
begin
  with ReportHoursPerEmplDM do
  begin
    QueryEmpl.Active := False;
    QueryEmpl.UniDirectional := False;
    QueryEmpl.SQL.Clear;
    SelectStr :=
      'SELECT ' + NL +
      '  E.PLANT_CODE, E.TEAM_CODE, E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
      '  E.STARTDATE, E.ENDDATE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E ' + NL;
      if QRParameters.FIncludeNotConnectedEmp then
      begin
         SelectStr :=  SelectStr + ',PRODHOURPEREMPLOYEE P' + NL;
      end;
      SelectStr :=  SelectStr + 'WHERE ' + NL +
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
    // RV050.4.1. Select also on employees plant!
    //RV065.10.
    if (not QRParameters.FAllEmployees) then
    begin
      if QRParameters.FIncludeNotConnectedEmp then
      begin
        SelectStr := SelectStr + '  AND ( P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ) ' + NL;
        SelectStr := SelectStr +
        '  AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
        '  AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
      end;
      SelectStr := SelectStr + '  AND ( ' + NL +
      '(E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
      if QRParameters.FIncludeNotConnectedEmp then
      begin
        SelectStr := SelectStr + ' OR ' + NL +
        '(P.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
        '  AND P.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
      end;
      SelectStr := SelectStr + '  )' + NL;
  (*
      SelectStr := SelectStr +
        '  AND E.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '  AND E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  *)
    end;

    if (QRParameters.FShowEmployee = EMPLOYEE_ACTIVE) then
      SelectStr := SelectStr +
        ' AND (E.STARTDATE <= :FDATECURRENT) AND ' + NL +
        ' ((E.ENDDATE >= :FDATECURRENT) OR (E.ENDDATE IS NULL))' + NL
    else
      if (QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE) then
        SelectStr := SelectStr +
          '  AND (E.STARTDATE > :FDATECURRENT ' + NL +
          '  OR E.ENDDATE < :FDATECURRENT) ' + NL;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
         SelectStr := SelectStr +
           ' AND ( ((E.DEPARTMENT_CODE >= ''' + QRParameters.FDeptFrom +
           ''') AND (E.DEPARTMENT_CODE <= ''' + QRParameters.FDeptTo + '''))) ' + NL;
    if not QRParameters.FShowAllTeam then
         SelectStr := SelectStr +
         ' AND ( ((E.TEAM_CODE >= ''' + QRParameters.FTeamFrom +
         ''') AND (E.TEAM_CODE <= ''' + QRParameters.FTeamTo + ''')) ' + NL;

    if (QRParameters.FShowEMPNoTeam) and (not QRParameters.FShowAllTeam) then
      SelectStr := SelectStr + ' OR (E.TEAM_CODE IS NULL)' + NL;

    if (not QRParameters.FShowAllTeam) then
      SelectStr := SelectStr + ')' + NL;

    // RV067.MRA.29 Bugfix.
    SelectStr := SelectStr +
      'GROUP BY ' + NL +
      '  E.PLANT_CODE, E.TEAM_CODE, E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
      '  E.STARTDATE, E.ENDDATE ' + NL +
      'ORDER BY ' + NL +
      '  E.TEAM_CODE, E.EMPLOYEE_NUMBER ' + NL;

    QueryEmpl.SQL.Add(UpperCase(SelectStr));
// QueryEmpl.SQL.SaveToFile('c:\temp\rhpe_qry_emp.sql');
    QueryEmpl.ParamByName('EMPLOYEEFROM').Value := QRBaseParameters.FEmployeeFrom;
    QueryEmpl.ParamByName('EMPLOYEETO').Value := QRBaseParameters.FEmployeeTo;
    //RV065.10.
    if QRParameters.FIncludeNotConnectedEmp then
    begin
      QueryEmpl.ParamByName('FSTARTDATE').Value := QRParameters.FDateFrom;
      QueryEmpl.ParamByName('FENDDATE').Value :=  QRParameters.FDateTo;
    end;
    // RV050.4.2.
    if (QRParameters.FShowEmployee <> EMPLOYEE_ALL) then
      QueryEmpl.ParamByName('FDATECURRENT').Value := Now;
//    QueryEmpl.UnPrepare;
//    if not QueryEmpl.Prepared then // RV040.
//      QueryEmpl.Prepare;
    QueryEmpl.Active := True;
    if (QueryEmpl.IsEmpty) then
      Result := False
    else
      Result := True;
    if Result then
    begin
//CAR 21-8-2003
      FDayDiff := ListProcsF.DateDifferenceDays(QRParameters.FDateFrom,
        QRParameters.FDateTo);
      // MR:15-12-2004 Check for whole weeks.
      // Because FDateFrom and FDateTo can span more than 1 week,
      // look if the first and last day start/end with a whole week.
      // ABS-5678
{
      WeekDayMin := ListProcsF.PimsDayOfWeek(QRParameters.FDateFrom);
      WeekDayMax := ListProcsF.PimsDayOfWeek(QRParameters.FDateTo - 1);
      if not ((WeekDayMin = 1) and (WeekDayMax = 7)) then
        FDayDiff := 1;
}
      // ABS-5675
//      if (FDayDiff >= 7) then
      begin
        SelectStr :=
          'SELECT ' + NL +
          '  EMPLOYEE_NUMBER, STARTDATE, ENDDATE, CONTRACT_HOUR_PER_WEEK, NVL(HOURLY_WAGE,0) HOURLY_WAGE ' + NL +
          'FROM ' + NL +
          '  EMPLOYEECONTRACT ' + NL +
          'WHERE ' + NL +
          '  EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
          '  AND EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
        if not QRParameters.FShowAllDate then
          SelectStr := SelectStr +
            '  AND STARTDATE <= :FMAXDATE AND ENDDATE >= :FMINDATE ' + NL;
        SelectStr := SelectStr +
          'ORDER BY ' + NL +
          '  EMPLOYEE_NUMBER, STARTDATE DESC ' + NL;
        QueryEmplContract.Active := False;
        QueryEmplContract.UniDirectional := False;
        QueryEmplContract.SQL.Clear;
        QueryEmplContract.SQL.Add(UpperCase(SelectStr));
        QueryEmplContract.ParamByName('EMPLOYEEFROM').Value := QRBaseParameters.FEmployeeFrom;
        QueryEmplContract.ParamByName('EMPLOYEETO').Value := QRBaseParameters.FEmployeeTo;
        if not QRParameters.FShowAllDate then
        begin
          QueryEmplContract.ParamByName('FMAXDATE').AsDateTime := QRParameters.FDateTo;
          QueryEmplContract.ParamByName('FMINDATE').AsDateTime := QRParameters.FDateFrom;
        end;
//        QueryEmplContract.UnPrepare;
//        if not QueryEmplContract.Prepared then // RV040.
//          QueryEmplContract.Prepare;
        QueryEmplContract.Active := True;
      end;
    end;
  end;
end;

function TReportHoursPerEmplQR.SetProduction: Boolean;
var
  SelectStr: String;
begin
  // RV067.MRA.33 550480 Addition of plant-info, enabled by 'FShowPlantInfo'.
  FreeItemList(FListPHE);
  SelectStr :=
    'SELECT ' + NL +
    '  P.EMPLOYEE_NUMBER, ' + NL;
  if QRParameters.FShowPlantInfo then
    SelectStr := SelectStr +
      '  P.PLANT_CODE, PL.DESCRIPTION AS PLANTDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  P.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(P.PRODUCTION_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowPlantInfo then
    SelectStr := SelectStr +
      '  INNER JOIN PLANT PL ON ' + NL +
      '    P.PLANT_CODE = PL.PLANT_CODE ' + NL;
  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    // RV067.MRA.29 Look at E.PLANT_CODE instead of P.PLANT_CODE
    //              But only on certain condition.
    if not QRParameters.FIncludeNotConnectedEmp then
      SelectStr := SelectStr +
        '  AND E.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
        '  AND E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
  //RV065.10.
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr + '  AND ( ' + NL +
    '(E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
    if QRParameters.FIncludeNotConnectedEmp then
    begin
      SelectStr := SelectStr + ' OR ' + NL +
      '(P.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
    end;
    SelectStr := SelectStr + '  )' + NL;
(*
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
*)
  end;
  if not QRParameters.FShowAllDate then
     SelectStr := SelectStr +
     '  AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
     '  AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  P.EMPLOYEE_NUMBER, ' + NL;
  if QRParameters.FShowPlantInfo then
    SelectStr := SelectStr +
      '  P.PLANT_CODE, PL.DESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  P.PRODHOUREMPLOYEE_DATE ' + NL +
    'ORDER BY ' + NL +
    '  P.EMPLOYEE_NUMBER, ' + NL;
  if QRParameters.FShowPlantInfo then
    SelectStr := SelectStr +
      '  P.PLANT_CODE, ' + NL;
  SelectStr := SelectStr +
    '  P.PRODHOUREMPLOYEE_DATE';
  Result :=
    SetQueryParametersNoEmpl(SelectStr,
      ReportHoursPerEmplDM.QueryProductionMinute);
// ReportHoursPerEmplDM.QueryProductionMinute.SQL.SaveToFile('c:\temp\rhpe_qry_prodmin.sql');
  FillList(ReportHoursPerEmplDM.QueryProductionMinute, FListPHE,
    False, False, True);
end;

 // Check if exists phe minutes for a team - is called on before print message
procedure TReportHoursPerEmplQR.SetQueryTeamParameters(SelectStr: String);
begin
  with ReportHoursPerEmplDM.QueryCheckTeam do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
{$IFDEF DEBUG5}
  SQL.SaveToFile(MyFile);
{$ENDIF}
    if not QRParameters.FShowAllDate then
    begin
      ParamByName('FSTARTDATE').Value := QRParameters.FDateFrom;
      ParamByName('FENDDATE').Value :=  QRParameters.FDateTo;
    end;
    // RV050.4.2.
    if (QRParameters.FShowEmployee <> EMPLOYEE_ALL) then
      ParamByName('FDATECURRENT').Value := Now;
//    UnPrepare;
//    if not Prepared then // RV040.
//      Prepare;
    // RV050.8.
//    SystemDM.PlantTeamFilterEnable(ReportHoursPerEmplDM.QueryCheckTeam);
    Active := True;
  end;
end;

procedure TReportHoursPerEmplQR.BuildListTeamForPHE;
var
  SelectStr: String;
begin
  FListTeamPHE.Clear;
  SelectStr :=
    'SELECT ' + NL +
    '  E.TEAM_CODE, SUM(P.PRODUCTION_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    // RV067.MRA.29 Look at E.PLANT_CODE instead of P.PLANT_CODE
    //              But only on certain condition.
    if not QRParameters.FIncludeNotConnectedEmp then
      SelectStr := SelectStr +
        '  AND E.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
        '  AND E.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
  //RV065.10.
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr + '  AND ( ' + NL +
    '(E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
    if QRParameters.FIncludeNotConnectedEmp then
    begin
      SelectStr := SelectStr + ' OR ' + NL +
      '(P.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
    end;
    SelectStr := SelectStr + '  )' + NL;
(*
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
*)
  end;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '  AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
  if (QRParameters.FShowEmployee = EMPLOYEE_ACTIVE) then
    SelectStr := SelectStr +
      '  AND (E.STARTDATE <= :FDATECURRENT) AND ' + NL +
      '  ((E.ENDDATE >= :FDATECURRENT) OR (E.ENDDATE IS NULL))' + NL
  else
    if (QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE) then
      SelectStr := SelectStr +
        '  AND ((E.STARTDATE > :FDATECURRENT) ' + NL +
        '  OR (E.ENDDATE < :FDATECURRENT)) ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  E.TEAM_CODE ' + NL +
    'ORDER BY ' + NL +
    '  E.TEAM_CODE ' + NL;
  SetQueryTeamParameters(SelectStr);
  while not ReportHoursPerEmplDM.QueryCheckTeam.Eof do
  begin
    if Round(ReportHoursPerEmplDM.QueryCheckTeam.
      FieldByName('SUMWK_HRS').AsFloat) <> 0 then
      FListTeamPHE.Add( CHR_SEP1 +
        ReportHoursPerEmplDM.QueryCheckTeam.FieldByName('TEAM_CODE').AsString +
        CHR_SEP  +
        IntToStr(
          Round(ReportHoursPerEmplDM.QueryCheckTeam.
            FieldByName('SUMWK_HRS').AsFloat)));
    ReportHoursPerEmplDM.QueryCheckTeam.Next;
  end;
end;

function TReportHoursPerEmplQR.CheckEmptyTeamForPHE(Team_Code: String): Boolean;
var
  P,
  I: Integer;
begin
  Result := False;
  for i := 0 to FListTeamPHE.Count -1 do
  begin
    p:= Pos(CHR_SEP1 + Team_Code + CHR_SEP, FListTeamPHE.Strings[i]);
    if p > 0 then
    begin
      Result := (p > 0);
      Exit;
    end;
  end;
  if (Result = False) then
    Result := CheckEmptyTeamForABS(Team_Code);
end;

procedure TReportHoursPerEmplQR.BuildListTeamForSAL;
var
  SelectStr: String;
begin
  FListTeamSAL.Clear;
  // RV050.4.
  // NOTE: When all-employees was selected, then only filter on teams,
  //       because the salaryhourperemployee does not store the plant.
  //       This means, filtering on plants, it can only be done on
  //       employees plant, but then it is not possible to show
  //       employees that worked in other plants.
  //       -> For salary do not show employees worked in other plants!
  SelectStr :=
    'SELECT ' + NL +
    '  E.TEAM_CODE, SUM(S.SALARY_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON ' + NL +
    '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV067.MRA.29 Bugfix.
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  else
    // RV050.4.1. Always select on plant for salary.
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end; }
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  if (QRParameters.FShowEmployee = EMPLOYEE_ACTIVE) then
    SelectStr := SelectStr +
      ' AND (E.STARTDATE <= :FDATECURRENT) AND ' + NL +
      ' ((E.ENDDATE >= :FDATECURRENT) OR (E.ENDDATE IS NULL)) ' + NL
  else
    if (QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE) then
      SelectStr := SelectStr +
        ' AND ((E.STARTDATE > :FDATECURRENT) ' + NL +
        ' OR (E.ENDDATE < :FDATECURRENT)) ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  E.TEAM_CODE ' + NL +
    'ORDER BY ' + NL +
    '  E.TEAM_CODE ' + NL;
  SetQueryTeamParameters(SelectStr);
  while not ReportHoursPerEmplDM.QueryCheckTeam.Eof do
  begin
    if Round(ReportHoursPerEmplDM.QueryCheckTeam.
      FieldByName('SUMWK_HRS').AsFloat) <> 0 then
      FListTeamSAL.Add(chr_sep1 +
        ReportHoursPerEmplDM.QueryCheckTeam.FieldByName('TEAM_CODE').AsString +
        CHR_SEP +
        IntToStr(Round(ReportHoursPerEmplDM.QueryCheckTeam.
          FieldByName('SUMWK_HRS').AsFloat)));
    ReportHoursPerEmplDM.QueryCheckTeam.Next;
  end;
end;

function TReportHoursPerEmplQR.CheckEmptyTeamForSAL(Team_Code: String): Boolean;
var
  P,
  I: Integer;
begin
  Result := False;
  for i := 0 to FListTeamSAL.Count -1 do
  begin
    p:= Pos(CHR_SEP1 + Team_Code + CHR_SEP, FListTeamSAL.Strings[i]);
    if p > 0 then
    begin
      Result := (p > 0);
      Exit;
    end;
  end;
  if (Result = False) then
    Result := CheckEmptyTeamForABS(Team_Code);
end;

procedure  TReportHoursPerEmplQR.BuildListTeamForABS;
var
  SelectStr: String;
begin
  {if FListTeamABS.Count > 0 then
    Exit;}
  FListTeamABS.Clear;
  SelectStr :=
    'SELECT ' + NL +
    '  E.TEAM_CODE, SUM(ABSENCE_MINUTE) AS SUMABS_MIN ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  A.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND A.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Select also on employees plant!
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if (QRParameters.FShowEmployee = EMPLOYEE_ACTIVE) then
    SelectStr := SelectStr +
      '  AND (E.STARTDATE <= :FDATECURRENT) AND ' + NL +
      '  ((E.ENDDATE >= :FDATECURRENT) OR (E.ENDDATE IS NULL))' + NL
  else
    if (QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE) then
      SelectStr := SelectStr +
        '  AND ((E.STARTDATE > :FDATECURRENT) ' + NL +
        '  OR (E.ENDDATE < :FDATECURRENT)) ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  E.TEAM_CODE ' + NL +
    'ORDER BY ' + NL +
    '  E.TEAM_CODE ';
  SetQueryTeamParameters(SelectStr);
  while not ReportHoursPerEmplDM.QueryCheckTeam.Eof do
  begin
    if (Round(ReportHoursPerEmplDM.QueryCheckTeam.
      FieldByName('SUMABS_MIN').AsFloat) <> 0) then
      FListTeamABS.Add(CHR_SEP1 +
      ReportHoursPerEmplDM.QueryCheckTeam.FieldByName('TEAM_CODE').AsString +
      chr_sep +
      IntToStr(Round(ReportHoursPerEmplDM.QueryCheckTeam.
        FieldByName('SUMABS_MIN').AsFloat)));
    ReportHoursPerEmplDM.QueryCheckTeam.Next;
  end;
end;

function TReportHoursPerEmplQR.CheckEmptyTeamForABS(Team_Code: String): Boolean;
var
  P,
  I: Integer;
begin
  Result := False;
  for i := 0 to FListTeamABS.Count -1 do
  begin
    p:= Pos(CHR_SEP1 + Team_Code + CHR_SEP, FListTeamABS.Strings[i]);
    if P > 0 then
    begin
      Result := (p > 0);
      Exit;
    end;
  end;
end;

// RV085.19.
function TReportHoursPerEmplQR.ListHTExists(AEmployee: Integer;
  AHourtype: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to FListHT.Count - 1 do
  begin
    ADDHTRecord := FListHT.Items[I];
    if (ADDHTRecord.Employee = AEmployee) and
      (ADDHTRecord.Hourtype = AHourtype) then
      Result := True;
  end;
end;

// RV085.19.
procedure TReportHoursPerEmplQR.AddListHT(AEmployee: Integer;
  AHourtype: String);
begin
  if not ListHTExists(AEmployee, AHourtype) then
  begin
    new(ADDHTRecord);
    ADDHTRecord.Employee := AEmployee;
    ADDHTRecord.Hourtype := AHourtype;
    FListHT.Add(ADDHTREcord);
  end;
end;

procedure TReportHoursPerEmplQR.FillList(QuerySelect: TQuery;
  TmpList: TList; FillHT, FillBreak, FillPlant: Boolean);
begin
  while not QuerySelect.Eof do
  begin
    APHERecord := new(PTPHERecord);
    if Assigned(QuerySelect.FindField('RECORDTYPE')) then // GLOB3-205
      APHERecord.RecordType := QuerySelect.FieldByName('RECORDTYPE').AsInteger
    else
      APHERecord.RecordType := 1;
    APHERecord.Employee :=
      QuerySelect.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if QRParameters.FShowHourType and FillHT then
    begin
      APHERecord.HourType:=
        QuerySelect.FieldByName('HOURTYPE_NUMBER').AsString;
      if IsTailorMadeCVL then // RV085.19.
      begin
        if QuerySelect.FieldByName('ADD_HRS').AsString = 'Y' then
        begin
          APHERecord.AddHrs := True;
          AddListHT(APHERecord.Employee, APHERecord.HourType);
        end
        else
          APHERecord.AddHrs := False;
      end;
      // GLOB3-86
      // PIM-399
      if SystemDM.IsNTS and QRParameters.FShowHourType then
        APHERecord.IsWorkspotHourType :=
          QuerySelect.FieldByName('WS_HT').AsString <> '0';
    end;
    if QRParameters.FShowPlantInfo and FillPlant then
    begin
      APHERecord.PlantCode :=
        QuerySelect.FieldByName('PLANT_CODE').AsString;
      APHERecord.PlantDesc :=
        QuerySelect.FieldByName('PLANTDESCRIPTION').AsString;
    end;
    APHERecord.DatePHE:=
      QuerySelect.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
    if APHERecord.RecordType = 1 then // GLOB3-205
    begin
      APHERecord.MinSum :=
        Round(QuerySelect.FieldByName('SUMWK_HRS').AsFloat);
      if FillBreak then
        APHERecord.BreakMin :=
          Round(QuerySelect.FieldByName('BREAK_MIN').AsFloat);
    end
    else
    begin
      APHERecord.MinSum := 0;
      APHERecord.BreakMin := 0;
    end;
    TmpList.Add(APHERecord);
    QuerySelect.Next;
  end;
end;

(*
// RV085.19.
// CVL-Selection to determine SALARY hours (+ Absence hours).
// Only when HOURTYPES should be shown.
function TReportHoursPerEmplQR.BuildSalarySelectionCVL: String;
var
  SelectStr: String;
begin
  // MRA: 02-02-2007 Order 550440
  //                 Combine select-statement for salary+absence hours to
  //                 ensure the hours will be put on the report, even if the
  //                 hourtype of an absence-record is 'normal hours'.
  // IMPORTANT: There is a special Oracle-function made and used to
  //            determine the correct 'PimsDayOfWeek'.
  //            This function must be added to Pims-DB with an update-script.

  // Start - Salary Hours - Select statement
  SelectStr :=
    'SELECT ' + NL +
    '  SREP.HOURTYPE_NUMBER, ' + NL +
    '  SREP.EMPLOYEE_NUMBER, ' + NL +
    '  SREP.SHOURTYPE_NUMBER, ' + NL +
    '  SREP.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SREP.DAY_OF_WEEK, ' + NL +
    // RV087.5.
    '  CASE ' + NL +
    '     WHEN (SREP.SHOURTYPE_NUMBER = 1) AND (SREP.EH_MIN IS NOT NULL) AND (SREP.SUMWK_HRS <> 0) AND (SREP.SUMWK_HRS2 < SREP.EH_MIN) THEN SREP.SUMWK_HRS ' + NL +
    '    WHEN (SREP.SHOURTYPE_NUMBER = 1) AND (SREP.SUMWK_HRS <> 0) AND (SREP.EH_MIN IS NOT NULL) THEN SREP.EH_MIN ' + NL +
    '    ELSE SREP.SUMWK_HRS ' + NL +
    '  END SUMWK_HRS, ' + NL +
    // RV087.5.
{
    '  CASE ' + NL +
    '    WHEN (SREP.EH_MIN IS NOT NULL) AND (SREP.SUMWK_HRS < SREP.EH_MIN) THEN SREP.SUMWK_HRS ' + NL +
    '    WHEN SREP.EH_MIN IS NOT NULL THEN SREP.EH_MIN ' + NL +
    '  END SUMWK_HRS, ' + NL +
}
    '  SREP.EH_MIN, ' + NL +
    '  SREP.ADD_HRS ' + NL +
    'FROM ' + NL +
    '( ' + NL +
    'SELECT ' + NL +
    '  H.HOURTYPE_NUMBER, ' + NL +
    '  S.EMPLOYEE_NUMBER ' + NL +
    ',  S.HOURTYPE_NUMBER SHOURTYPE_NUMBER ' + NL +
    ',  S.SALARY_DATE AS PRODHOUREMPLOYEE_DATE ' + NL +
    ',  EH.DAY_OF_WEEK, ' + NL +
    '   EH.STARTTIME, ' + NL +
    '   EH.ENDTIME, ' + NL +
    '   E.CONTRACTGROUP_CODE ' + NL +
    ',  CASE ' + NL +
    '     WHEN H.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER THEN SUM(S.SALARY_MINUTE) ' + NL +
    '     WHEN H.HOURTYPE_NUMBER <> S.HOURTYPE_NUMBER THEN SUM(0) ' + NL +
    '   END SUMWK_HRS ' + NL +
    ',  CASE ' + NL +
    '     WHEN PIMSDAYOFWEEK(S.SALARY_DATE) = EH.DAY_OF_WEEK THEN ' + NL +
    '       60*trunc(((86400*(EH.ENDTIME-EH.STARTTIME))/60)/60)-24*(trunc((((86400*(EH.ENDTIME-EH.STARTTIME))/60)/60)/24)) + ' + NL +
    '       trunc((86400*(EH.ENDTIME-EH.STARTTIME))/60)-60*(trunc(((86400*(EH.ENDTIME-EH.STARTTIME))/60)/60)) ' + NL +
    '   END EH_MIN, ' + NL +
    '   CASE ' + NL +
    '     WHEN H.HOURTYPE_NUMBER = 1 THEN ''Y'' ' + NL +
    '     WHEN H.HOURTYPE_NUMBER IN ' + NL +
    '       (SELECT OD.HOURTYPE_NUMBER FROM OVERTIMEDEFINITION OD WHERE OD.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) THEN ''Y'' ' + NL +
    '     ELSE ''N'' ' + NL +
    '   END ADD_HRS, ' + NL +
    // RV087.5.
    '   ( ' + NL +
    '     SELECT SUM(S2.SALARY_MINUTE) ' + NL +
    '      FROM SALARYHOURPEREMPLOYEE S2 ' + NL +
    '      WHERE S2.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER AND ' + NL +
    '     S2.SALARY_DATE = S.SALARY_DATE ' + NL +
    '   ) ' + NL +
    '   SUMWK_HRS2 ' + NL +
    'FROM ' + NL +
    '  HOURTYPE H CROSS JOIN SALARYHOURPEREMPLOYEE S ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  LEFT JOIN EXCEPTIONALHOURDEF EH ON ' + NL +
    '    E.CONTRACTGROUP_CODE = EH.CONTRACTGROUP_CODE AND ' + NL +
    '    PIMSDAYOFWEEK(S.SALARY_DATE) = EH.DAY_OF_WEEK AND ' + NL +
    '    EH.HOURTYPE_NUMBER = 1 ' + NL +
    'WHERE ' + NL +
    '  S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  else
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  H.HOURTYPE_NUMBER, ' + NL +
    '  S.EMPLOYEE_NUMBER ' + NL +
    ',  S.HOURTYPE_NUMBER ' + NL +
    ',  S.SALARY_DATE ' + NL +
    ',  EH.DAY_OF_WEEK, ' + NL +
    '   EH.STARTTIME, ' + NL +
    '   EH.ENDTIME, ' + NL +
    '   E.CONTRACTGROUP_CODE ' + NL;

  SelectStr := SelectStr +
    'UNION ALL ' + NL;

  // Start - Absence Hours - Select statement
  SelectStr := SelectStr +
    'SELECT ' + NL +
    '  NULL, ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL +
    ',  AR.HOURTYPE_NUMBER ' + NL +
    ',  A.ABSENCEHOUR_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  NULL, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS, ' + NL +
    '  NULL, ' + NL +
    '  ''Y'', ' + NL +
    // RV087.5.
    '  NULL ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON AR ON ' + NL +
    '    AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND AR.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND AR.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL +
    ',  AR.HOURTYPE_NUMBER ' + NL +
    ',  A.ABSENCEHOUR_DATE ' + NL +
    ' ORDER BY 2, 1, 4 ' + NL + // 1, 2, 3
    ') SREP ';

  Result := SelectStr;
end; // BuildSalarySelectionCVL
*)

// RV100.1.
// Show a report where first soil+non-soil is shown.
// As last line only soil is shown.
function TReportHoursPerEmplQR.BuildSalarySelectionCVL2: String;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  C.EMPLOYEE_NUMBER, ' + NL +
    '  C.HNR AS HOURTYPE_NUMBER, ' + NL +
    '  C.SALARY_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  C.SALARY_MINUTE AS SUMWK_HRS ' + NL +
    '  FROM ' + NL +
    '  ( ' + NL +
    '    SELECT A.EMPLOYEE_NUMBER, A.HNR, A.SALARY_DATE, ' + NL +
    '           SUM(A.SALARY_MINUTE) SALARY_MINUTE ' + NL +
    '    FROM ' + NL +
    '    ( ' + NL +
    '      SELECT S.EMPLOYEE_NUMBER, S.HOURTYPE_NUMBER, S.SALARY_DATE, ' + NL +
    '             S.SALARY_MINUTE, ' + NL +
    '        CASE ' + NL +
    '          WHEN S.HOURTYPE_NUMBER IN ' + NL +
    '            (SELECT ED.HOURTYPE_NUMBER FROM EXCEPTIONALHOURDEF ED ' + NL +
    '             WHERE ED.CONTRACTGROUP_CODE = ' +
    '''' + CVL_SOIL_CONTRACTGROUP_CODE + '''' +  ') ' + NL +
    '            THEN S.HOURTYPE_NUMBER - 100 ' + NL +
    '          ELSE S.HOURTYPE_NUMBER ' + NL +
    '        END HNR ' + NL +
    '      FROM ' + NL +
    '        SALARYHOURPEREMPLOYEE S INNER JOIN HOURTYPE H ON ' + NL +
    '          S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' + NL +
    '        INNER JOIN EMPLOYEE E ON ' + NL +
    '          S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '      WHERE ' + NL +
    '        S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '        AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  ELSE
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    '      ORDER BY ' + NL +
    '        S.HOURTYPE_NUMBER ' + NL +
    '  ) A ' + NL +
    '  GROUP BY A.EMPLOYEE_NUMBER, A.HNR, A.SALARY_DATE, A.SALARY_MINUTE ' + NL +
    '    UNION ' + NL +
    '  SELECT B.EMPLOYEE_NUMBER, B.HOURTYPE_NUMBER, B.SALARY_DATE, ' + NL +
    '         SUM(B.SALARY_MINUTE) SALARY_MINUTE ' + NL +
    '  FROM ' + NL +
    '  ( ' + NL +
    '    SELECT S.EMPLOYEE_NUMBER, ' + IntToStr(CVL_SOIL_HOURTYPE_NUMBER) +
    '      AS HOURTYPE_NUMBER, S.SALARY_DATE, S.SALARY_MINUTE ' + NL +
    '    FROM SALARYHOURPEREMPLOYEE S INNER JOIN HOURTYPE H ON ' + NL +
    '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' + NL +
    '      INNER JOIN EMPLOYEE E ON ' + NL +
    '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '    WHERE ' + NL +
    '        S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '        AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  else
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    '    AND S.HOURTYPE_NUMBER IN (SELECT ED.HOURTYPE_NUMBER ' + NL +
    '      FROM EXCEPTIONALHOURDEF ED ' + NL +
    '      WHERE ED.CONTRACTGROUP_CODE = ' +
    '''' + CVL_SOIL_CONTRACTGROUP_CODE + '''' + ') ' + NL +
    '  ) B ' + NL +
    '  GROUP BY B.EMPLOYEE_NUMBER, B.HOURTYPE_NUMBER, B.SALARY_DATE ' + NL +
    '  ) C ' + NL;
//    '  order by 3 ' + NL;

  // Start - Salary Hours - Select statement
(*
  SelectStr :=
    'SELECT ' + NL +
    '  S.EMPLOYEE_NUMBER ' + NL +
    ',  S.HOURTYPE_NUMBER ' + NL +
    ',  S.SALARY_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(S.SALARY_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON ' + NL +
    '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  else
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  S.EMPLOYEE_NUMBER ' + NL +
    ',  S.HOURTYPE_NUMBER ' + NL +
    ',  S.SALARY_DATE ' + NL;
  // End - Salary Hours - Select statement
*)
  // Use UNION to combine the two select-statements
  // RV072.3. This must be 'UNION ALL'! Or it will not show absence
  //          when the combination in select is the same for absence compared
  //          with salary! (When show hour-types is unchecked).
  SelectStr := SelectStr +
    'UNION ALL ' + NL;

  // Start - Absence Hours - Select statement
  SelectStr := SelectStr +
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  AR.HOURTYPE_NUMBER ' + NL;
  SelectStr := SelectStr +
    ',  A.ABSENCEHOUR_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON AR ON ' + NL +
    '    AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Always select on plant for absence hours!
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end;
}
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND AR.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND AR.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  AR.HOURTYPE_NUMBER ' + NL;
  SelectStr := SelectStr +
    ',  A.ABSENCEHOUR_DATE ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ' ORDER BY 1, 2, 3'
  else
    SelectStr := SelectStr +
      ' ORDER BY 1, 2';

  Result := SelectStr;
end; // BuildSalarySelectionCVL2

// RV085.19.
// Default selection to determine SALARY hours (+ Absence hours).
function TReportHoursPerEmplQR.BuildSalarySelectionDefault: String;
var
  SelectStr: String;
begin
  // MRA: 02-02-2007 Order 550440
  //                 Combine select-statement for salary+absence hours to
  //                 ensure the hours will be put on the report, even if the
  //                 hourtype of an absence-record is 'normal hours'.

  // Start - Salary Hours - Select statement
  SelectStr :=
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ', A.HOURTYPE_NUMBER ' + NL;
  SelectStr := SelectStr +
      ', A.PRODHOUREMPLOYEE_DATE ' + NL;
  if SystemDM.IsNTS and QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ', A.WS_HT ' + NL;
  SelectStr := SelectStr +
    ',  A.SUMWK_HRS ' + NL +
    ',  A.RECORDTYPE  ' + NL +
    'FROM ' + NL +
    '( ' + NL;
    
  SelectStr := SelectStr +
    'SELECT ' + NL +
    '  S.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  TO_CHAR(S.HOURTYPE_NUMBER) AS HOURTYPE_NUMBER ' + NL; // GLOB3-205
  SelectStr := SelectStr +
    ',  S.SALARY_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL;
  // GLOB3-86
  // PIM-399 Bugfix
(*  if SystemDM.IsNTS then
    SelectStr := SelectStr +
      '  SUM(CASE ' + NL +
      '    WHEN W.WORKSPOT_CODE IS NULL THEN 0 ' + NL +
      '    ELSE 1 ' + NL +
      '  END) WS_HT, ' + NL; *)
  if SystemDM.IsNTS and QRParameters.FShowHourType then
    SelectStr := SelectStr +
      '         NVL((SELECT MIN(W.HOURTYPE_NUMBER) ' + NL +
      '          FROM WORKSPOT W INNER JOIN PRODHOURPEREMPLPERTYPE P ON ' + NL +
      '          P.PRODHOUREMPLOYEE_DATE = S.SALARY_DATE AND ' + NL +
      '          P.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER AND ' + NL +
      '          P.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER AND ' + NL +
      '          W.PLANT_CODE = P.PLANT_CODE AND ' + NL +
      '          W.WORKSPOT_CODE = P.WORKSPOT_CODE AND ' + NL +
      '          W.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER),0) WS_HT, ' + NL;

  SelectStr := SelectStr +
    '  SUM(S.SALARY_MINUTE) AS SUMWK_HRS ' + NL +
    '  , 1 RECORDTYPE ' + NL + // GLOB3-205
    'FROM ' + NL +
    '  SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON ' + NL +
    '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;

  // GLOB3-86
  // PIM-399 Bugfix
(*  if SystemDM.IsNTS then
    SelectStr := SelectStr +
      '  LEFT JOIN PRODHOURPEREMPLPERTYPE P ON ' + NL +
      '    P.PRODHOUREMPLOYEE_DATE = S.SALARY_DATE AND ' + NL +
      '    P.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER AND ' + NL +
      '    P.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER ' + NL +
      '  LEFT JOIN WORKSPOT W ON ' + NL +
      '    W.PLANT_CODE = P.PLANT_CODE AND ' + NL +
      '    W.WORKSPOT_CODE = P.WORKSPOT_CODE AND ' + NL +
      '    W.HOURTYPE_NUMBER = S.HOURTYPE_NUMBER ' + NL; *)

  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  S.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND S.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV067.MRA.29 Bugfix.
  if QRParameters.FIncludeNotConnectedEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER IN ' + NL +
      '    (SELECT P.EMPLOYEE_NUMBER FROM PRODHOURPEREMPLOYEE P ' + NL +
      '     WHERE P.PLANT_CODE >=  ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '     AND P.PLANT_CODE <=  ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      '     AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE) '
  else
    // RV050.4.1. Always select on plant for salary!
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end; }
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND S.SALARY_DATE >= :FSTARTDATE ' + NL +
      '  AND S.SALARY_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND S.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) +
      '  AND S.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  S.EMPLOYEE_NUMBER ';
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  S.HOURTYPE_NUMBER ' + NL;
  SelectStr := SelectStr +
    ',  S.SALARY_DATE ' + NL;
{  Result := SetQueryParametersNoEmpl(SelectStr,
    ReportHoursPerEmplDM.QueryProductionMinute); }
  // End - Salary Hours - Select statement

  // Use UNION to combine the two select-statements
  // RV072.3. This must be 'UNION ALL'! Or it will not show absence
  //          when the combination in select is the same for absence compared
  //          with salary! (When show hour-types is unchecked).
  SelectStr := SelectStr +
    'UNION ALL ' + NL;

  // Start - Absence Hours - Select statement
  SelectStr := SelectStr +
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  TO_CHAR(AR.HOURTYPE_NUMBER) AS HOURTYPE_NUMBER ' + NL; // GLOB3-205
  SelectStr := SelectStr +
    ',  A.ABSENCEHOUR_DATE AS PRODHOUREMPLOYEE_DATE ' + NL;

  // GLOB3-86
  if SystemDM.IsNTS and QRParameters.FShowHourType then
    SelectStr := SelectStr +
    ',   0 ' + NL;

  SelectStr := SelectStr +
    ',  SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS ' + NL +
    '  , 1 ' + NL + // GLOB3-205
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON AR ON ' + NL +
    '    AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Always select on plant for absence hours!
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end;
}
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND AR.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND AR.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ',  AR.HOURTYPE_NUMBER ' + NL;
  SelectStr := SelectStr +
    ',  A.ABSENCEHOUR_DATE ' + NL;

  // GLOB3-205
  SelectStr := SelectStr +
     'UNION ALL ' + NL +
     'SELECT ' + NL +
     '  EP.EMPLOYEE_NUMBER ' + NL;
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
     ', EP.PAYMENT_EXPORT_CODE ';
    SelectStr := SelectStr +
     ', EP.PAYMENT_DATE ' + NL;
  if SystemDM.IsNTS and QRParameters.FShowHourType then
    SelectStr := SelectStr +
    ',   0 ' + NL;
  SelectStr := SelectStr +
     ', SUM(EP.PAYMENT_AMOUNT) PAYMENT_AMOUNT ' + NL +
     ', 2 ' + NL +
     ' FROM EXTRAPAYMENT EP ' + NL +
     'WHERE ' + NL +
    '  EP.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND EP.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
     ' AND EP.PAYMENT_DATE >= :FSTARTDATE AND EP.PAYMENT_DATE <= :FENDDATE ' + NL +
     'GROUP BY ' + NL +
     '  EP.EMPLOYEE_NUMBER ';
  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      '  ,EP.PAYMENT_EXPORT_CODE ' + NL;
    SelectStr := SelectStr +
      '  ,EP.PAYMENT_DATE ' ;
  SelectStr := SelectStr +
    ' ) A ';

  if QRParameters.FShowHourType then
    SelectStr := SelectStr +
      ' ORDER BY EMPLOYEE_NUMBER, RECORDTYPE, HOURTYPE_NUMBER, PRODHOUREMPLOYEE_DATE' // GLOB3-205 1, 5, 2, 3
  else
    SelectStr := SelectStr +
      ' ORDER BY EMPLOYEE_NUMBER, RECORDTYPE, PRODHOUREMPLOYEE_DATE'; // GLOB3-205 1, 4, 2

  Result := SelectStr;
end; // BuildSalarySelectionDefault

function TReportHoursPerEmplQR.SetSalary: Boolean;
var
  SelectStr: String;
begin
  Result := True;

  FreeItemList(FListPHE);
  FreeItemList(FListABSSAL);
  FreeItemList(FListDirHrsSAL);
  FreeItemList(FListIndHrsSAL);

//  if IsTailorMadeCVL then // RV085.19.
//    SelectStr := BuildSalarySelectionCVL // RV085.19.
  if IsTailorMadeCVL2 then // RV100.1.
    SelectStr := BuildSalarySelectionCVL2 // RV100.1.
  else
    SelectStr := BuildSalarySelectionDefault;

  SetQueryParametersNoEmpl(SelectStr,
    ReportHoursPerEmplDM.QueryProductionMinute);

// TESTING
// ReportHoursPerEmplDM.QueryProductionMinute.SQL.SaveToFile('c:\temp\rephrsperemp_salary.sql');
{  SetQueryParametersNoEmpl(SelectStr,
    ReportHoursPerEmplDM.QueryAbsenceMinuteHourtype); }
  // End - Absence Hours - Select statement

  // Start - Direct Hours - Select statement
  // RV045.2. Link department also to plant.
  SelectStr :=
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON AR ON ' + NL +
    '    A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    E.DEPARTMENT_CODE = D.DEPARTMENT_CODE  ' + NL +
    '    AND E.PLANT_CODE = D.PLANT_CODE ' + NL +
    'WHERE  '  + NL +
    '  D.DIRECT_HOUR_YN = ''Y''' + NL +
    '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Always select on plant for absence hours!
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end; }
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND AR.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND AR.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE ' + NL +
    'ORDER BY ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE ' + NL;
  SetQueryParametersNoEmpl(SelectStr, ReportHoursPerEmplDM.QueryDirSalary);
  // End - Direct Hours - Select statement

  // Start - Indirect Hours - Select statement
  // RV045.2. Link department also to plant.
  SelectStr :=
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE AS PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN ABSENCEREASON AR ON ' + NL +
    '    A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    E.DEPARTMENT_CODE = D.DEPARTMENT_CODE  ' + NL +
    '    AND E.PLANT_CODE = D.PLANT_CODE ' + NL +
    'WHERE  '  + NL +
    '  D.DIRECT_HOUR_YN = ''N''' + NL +
    '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Always select on plant for absence hours!
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end; }
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL;
  if not QRParameters.FAllHourType then
    SelectStr := SelectStr +
      '  AND AR.HOURTYPE_NUMBER >= ' + IntToStr(QRParameters.FHourTypeFrom) + NL +
      '  AND AR.HOURTYPE_NUMBER <= ' + IntToStr(QRParameters.FHourTypeTo) + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE ' + NL +
    'ORDER BY ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE ' + NL;
  SetQueryParametersNoEmpl(SelectStr, ReportHoursPerEmplDM.QueryIndSalary);
  // End - Indirect Hours - Select statement

  FillList(ReportHoursPerEmplDM.QueryProductionMinute, FListPHE, True, False, False);
  FillList(ReportHoursPerEmplDM.QueryDirSalary, FListDirHrsSAL, False, False, False);
  FillList(ReportHoursPerEmplDM.QueryIndSalary, FListIndHrsSAL, False, False, False);
{  FillList(ReportHoursPerEmplDM.QueryAbsenceMinuteHourtype, FListABSSAL, True,
    False); }
end; // SetSalary

procedure TReportHoursPerEmplQR.BuildListAbs;
var
  SelectStr: String;
begin
  FreeItemList(FListABS);
  SelectStr :=
    'SELECT ' + NL +
    '  A.EMPLOYEE_NUMBER, SUM(A.ABSENCE_MINUTE) AS SUMABS_MIN ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A, EMPLOYEE E ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      '  AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
      '  AND A.ABSENCEHOUR_DATE < :FENDDATE ' + NL +
      '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
      '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Always select on plant for absence hours!
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV050.4.1. Select also on employees plant!
{  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  end; }
  SelectStr := SelectStr  +
    'GROUP BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL +
    'ORDER BY ' + NL +
    '  A.EMPLOYEE_NUMBER ' + NL;
  SetQueryParametersNoEmpl(SelectStr, ReportHoursPerEmplDM.QueryAbsenceMinute);
  while not ReportHoursPerEmplDM.QueryAbsenceMinute.Eof do
  begin
    APHERecord := new(PTPHERecord);
    APHERecord.RecordType := 1;
    APHERecord.Employee :=
      ReportHoursPerEmplDM.QueryAbsenceMinute.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    APHERecord.MinSum :=
      Round(ReportHoursPerEmplDM.QueryAbsenceMinute.FieldByName('SUMABS_MIN').AsFloat);
    FListABS.Add(APHERecord);
    ReportHoursPerEmplDM.QueryAbsenceMinute.Next;
  end;
end;

procedure TReportHoursPerEmplQR.BuildDirIndHours;
var
  SelectStr, SelectDir, SelectInd: String;
begin
  FreeItemList(FListDirHrs);
  FreeItemList(FListIndHrs);

  SelectStr :=
    'SELECT ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(P.PRODUCTION_MINUTE) AS SUMWK_HRS, ' + NL +
    '  SUM(P.PAYED_BREAK_MINUTE) AS BREAK_MIN ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
    '    AND W.PLANT_CODE = D.PLANT_CODE ' + NL +
    'WHERE ' + NL +
    '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Select also on employees plant!
  //RV065.10.
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr + '  AND ( ' + NL +
    '(E.PLANT_CODE >= ''' + NL +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
    if QRParameters.FIncludeNotConnectedEmp then
    begin
      SelectStr := SelectStr + ' OR ' + NL +
      '(P.PLANT_CODE >= ''' + NL +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
    end;
    SelectStr := SelectStr + '  )' + NL;
(*
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
*)
  end;
  SelectDir := SelectStr + '  AND D.DIRECT_HOUR_YN = ''Y''' + NL;
  SelectInd := SelectStr + '  AND D.DIRECT_HOUR_YN = ''N''' + NL;
  SelectStr := '';
  if not QRParameters.FShowAllDate then
     SelectStr :=
       '  AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
       '  AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE ' + NL +
    'ORDER BY ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE' + NL;
  SelectInd := SelectInd + SelectStr;
  SelectDir := SelectDir + SelectStr;

  SetQueryParametersNoEmpl(SelectDir, ReportHoursPerEmplDM.QueryDirProduction);
  SetQueryParametersNoEmpl(SelectInd, ReportHoursPerEmplDM.QueryIndProduction);
  FillList(ReportHoursPerEmplDM.QueryDirProduction, FListDirHrs, False, True, False);
  FillList(ReportHoursPerEmplDM.QueryIndProduction, FListIndHrs, False, True, False);
end;

procedure TReportHoursPerEmplQR.BuildProdNonProdHrs;
var
  SelectStr, SelectDir, SelectInd: String;
begin
  FreeItemList(FListProdHrs);
  FreeItemList(FListNonProdHrs);

  SelectStr :=
    'SELECT ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  SUM(P.PRODUCTION_MINUTE) AS SUMWK_HRS ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P INNER JOIN WORKSPOT W ON ' + NL +
    '    P.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  P.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
    '  AND P.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  // RV050.4.1. Select also on employees plant!
  //RV065.10.
  if (not QRParameters.FAllEmployees) then
  begin
    SelectStr := SelectStr + '  AND ( ' + NL +
    '(E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
    '  AND E.PLANT_CODE <= ''' +
    DoubleQuote(QRBaseParameters.FPlantTo) + ''')' + NL;
    if QRParameters.FIncludeNotConnectedEmp then
    begin
      SelectStr := SelectStr + ' OR ' + NL +
      '(P.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND P.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + ''') ';
    end;
    SelectStr := SelectStr + '  )' + NL;
(*
    SelectStr := SelectStr +
      '  AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      '  AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
*)
  end;

  SelectDir := SelectStr + '  AND W.PRODUCTIVE_HOUR_YN = ''Y''' + NL;
  SelectInd := SelectStr + '  AND W.PRODUCTIVE_HOUR_YN = ''N''' + NL;
  SelectStr := '';
  if not QRParameters.FShowAllDate then
    SelectStr := SelectStr +
      ' AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
      ' AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE ' + NL +
    'ORDER BY ' + NL +
    '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE ' + NL;
  SelectInd := SelectInd + SelectStr;
  SelectDir := SelectDir + SelectStr;

  SetQueryParametersNoEmpl(SelectDir, ReportHoursPerEmplDM.QueryProductive);
  SetQueryParametersNoEmpl(SelectInd, ReportHoursPerEmplDM.QueryNonProductive);
  FillList(ReportHoursPerEmplDM.QueryProductive, FListProdHrs, False, False, False);
  FillList(ReportHoursPerEmplDM.QueryNonProductive, FListNonProdHrs, False, False, False);
end;

function TReportHoursPerEmplQR.ExistsRecords: Boolean;
var
  Count: Integer;
begin
  Screen.Cursor := crHourGlass;

  // GLOB3-205
  ReportHoursPerEmplDM.FillWeekTable(QRParameters.FDateFrom, QRParameters.FDateTo);
  InitExtraPayments;

  if QRParameters.FAllEmployees then
  begin
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999999';
  end;

  ClearLists; // RV040.
  Result := True;
  if not GetRecordQueryEmpl then
  begin
    Result := false;
    Screen.Cursor := crDefault;
    Exit;
  end;
  BuildListTeamForABS;
  if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
  begin
    BuildListTeamForPHE;
    SetProduction;
  end
  else
  begin
    BuildListTeamForSAL;
    SetSalary;
  end;
// absence minutes
  BuildListAbs;

// direct hours
  BuildDirIndHours;

// productive - non productive
  if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
    BuildProdNonProdHrs;
//
  {check if report is empty}
  for Count := 1 to 7 do
  begin
    TotProdHour[Count] := 0;
    TotDirHour[Count] := 0;
    TotIndHour[Count] := 0;
    TotProductiveHour[Count] := 0;
    TotNProductiveHour[Count] := 0;
  end;
  TotAbsence := 0;
  TotalEmpl := 0;
  TotalEmplExceptHrDefMinute := 0; // RV085.19.
  Screen.Cursor := crDefault;

//  ReportHoursPerEmplDM.QueryEmpl.SQL.SaveToFile('c:\temp\hours_per_emp_salary.sql');
end;

procedure TReportHoursPerEmplQR.ConfigReport;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes; // RV054.4.
  
  // MR:06-12-2002
  ExportClass.ExportDone := False;

  if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
  begin
    Caption := SCaptionProdRepHrs;
//    QRLabel15.Caption := WorkedHoursLabelCaption;
  end
  else
  begin
    Caption := SCaptionSalRepHrs;
//    QRLabel15.Caption := '';
  end;
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) or
    QRParameters.FAllEmployees then
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end
  else
  begin
    QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  end;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
  end
  else
  begin
    QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
    QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  end;
  if QRParameters.FShowAllTeam then
  begin
    QRLblTeamFrom.Caption := '*';
    QRLblTeamTo.Caption := '*';
  end
  else
  begin
    QRLblTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLblTeamTo.Caption := QRparameters.FTeamTo;
  end;
  if QRParameters.FShowEmployee = EMPLOYEE_ACTIVE then
    QRLabelShowEmpl.Caption := SRepHrsPerEmplActiveEmpl;
  if QRParameters.FShowEmployee = EMPLOYEE_NONACTIVE then
    QRLabelShowEmpl.Caption := SRepHrsPerEmplInActiveEmpl;
  if QRParameters.FShowEmployee = EMPLOYEE_ALL then
    QRLabelShowEmpl.Caption := '';

  if QRParameters.FShowAllDate then
  begin
    QRLblDateFrom.Caption := '*';
    QRLblDateTo.Caption := '*';
  end
  else
  begin
    QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo - 1);
  end;

  if QRParameters.FReportType = REPORTTYPE_SALARY then
  begin
    // ABS-5675
//    if FDayDiff >= 7 then
    begin
      QRLabelAbsence.Caption := SPimsContract;
      QRLabelTot.Caption := SPimsDifference;
    end;
{    else
    begin
      QRLabelAbsence.Caption := '';
      QRLabelTot.Caption := '';
    end; }
  end
  else
  begin
    QRLabelAbsence.Caption := SPimsAbsence;
    QRLabelTot.Caption := '      ' + SRepHrsPerEmplTotal;
  end;
end;

procedure TReportHoursPerEmplQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
//  AMemoClass.Free; // RV080.1.
end;

procedure TReportHoursPerEmplQR.FormCreate(Sender: TObject);
begin
  // RV062.2. Set this from 4 to higher position!
  // Otherwise it can lead to a double line after the 4th line.
  // RV071.17. Set to higher value, from 12 to 24!
//  SetMemoParam(24 {12} {4}, MAX_LINES_ON_PAGE, QRBandDetailEmployee);

  // MRA:27-MAY-2010. 26-MAY-2010. Store the original Height, because
  // it will change during the run.
  FQRBandDetailEmployeeHeight := QRBandDetailEmployee.Height;

  inherited;
  FQRParameters := TQRParameters.Create;
//  WorkedHoursLabelCaption := QRLabel15.Caption;
  // MR:06-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  //performance   CAR 20-6-2003
  FListTeamPHE := TStringList.Create;
  FListTeamABS := TStringList.Create;
  FListTeamSAL := TStringList.Create;

  FListPHE := TList.Create;
  FListABS := TList.Create;
  FListIndHrs := TList.Create;
  FListDirHrs := TList.Create;

  FListIndHrsSAL := TList.Create;
  FListDirHrsSAL := TList.Create;

  FListProdHrs := TList.Create;
  FListNonProdHrs := TList.Create;
  FListABSSAL :=  TList.Create;

  FListHT := TList.Create; // RV085.19.

  // RV080.1.
//  AMemoClass := TMemoClass.Create(24, MAX_LINES_ON_PAGE, QRBandDetailEmployee);
end;

procedure TReportHoursPerEmplQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Count: Integer;
  ExportHourType, ExportWorkedHrs: String;
begin
  inherited;
  // RV080.1.
//  AMemoClass.Init;
  //  SetMemoParam(24 {12} {4}, MAX_LINES_ON_PAGE, QRBandDetailEmployee);

  // MRA:27-MAY-2010. Restore original height, because it will be
  //                  changed during run.
  QRBandDetailEmployee.Height := FQRBandDetailEmployeeHeight;

  PrintBand := QRParameters.FShowSelection;
  for Count := 1 to 7 do
  begin
    TotProdHour[Count] := 0;
    TotDirHour[Count] := 0;
    TotIndHour[Count] := 0;
    TotProductiveHour[Count] := 0;
    TotNProductiveHour[Count] := 0;
  end;
  TotAbsence := 0;
  TotalEmpl := 0;
  // MR:09-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if not ExportClass.ExportDone then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      // Show Selections
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(QRLabel11.Caption);
        // From Employee to Employee
        ExportClass.AddText(
          QRLabel13.Caption + ' ' + { From } QRLabel20.Caption + ' ' +
          QRLblEmployeeFrom.Caption + ' ' + QRLabel16.Caption + ' ' + { To }
          QRLblEmployeeTo.Caption
          );
        // From Dept to Dept
        ExportClass.AddText(
          QRLabel47.Caption + ' ' + { From } QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' + QRLabel50.Caption + ' ' + { To }
          QRLabelDeptTo.Caption
          );
        // From Team to Team
        ExportClass.AddText(
          QRLabel40.Caption + ' ' + { From } QRLabel39.Caption + ' ' +
          QRLblTeamFrom.Caption + ' ' + QRLabel38.Caption + ' ' + { To }
          QRLblTeamTo.Caption
          );
        // From Date to Date
        ExportClass.AddText(
          QRLabel1.Caption + ' ' + { From } QRLabel2.Caption + ' ' +
          QRLblDateFrom.Caption + ' ' + QRLabel5.Caption + ' ' + { To }
          QRLblDateTo.Caption
          );

        ExportClass.AddText(QRLabelShowEmpl.Caption);
      end;
      // Show Column-names
//CAR 5-5-2003
      if QRParameters.FShowHourType then
        ExportHourType := QRLabelHourType.Caption
      else
        ExportHourType := '';
      if QRParameters.FReportType = REPORTTYPE_SALARY then
        ExportWorkedHrs := SRepHrsPerEmplTotal
      else
        ExportWorkedHrs := QRLabel41.Caption + ' ' + QRLabel34.Caption;
      ExportDetail(
        QRLabel3.Caption,
        QRLabel6.Caption,
        '',  
        ExportHourType, '',
        SystemDM.GetDayWCode(1), SystemDM.GetDayWCode(2), SystemDM.GetDayWCode(3),
        SystemDM.GetDayWCode(4), SystemDM.GetDayWCode(5), SystemDM.GetDayWCode(6),
        SystemDM.GetDayWCode(7),
        ExportWorkedHrs, QRLabelAbsence.Caption, Trim(QRLabelTot.Caption));
   end;
end;

procedure TReportHoursPerEmplQR.FillMemoHTPerDays(Employee: Integer; HourType: String;
  WorkedHours: Boolean);
var
  TotHTHours, Count: Integer;
  GoOn: Boolean;
begin
  MO.Lines.Add(DecodeHrsMin(HourTypeHours[1]));
  TU.Lines.Add(DecodeHrsMin(HourTypeHours[2]));
  WE.Lines.Add(DecodeHrsMin(HourTypeHours[3]));
  TH.Lines.Add(DecodeHrsMin(HourTypeHours[4]));
  FR.Lines.Add(DecodeHrsMin(HourTypeHours[5]));
  SA.Lines.Add(DecodeHrsMin(HourTypeHours[6]));
  SU.Lines.Add(DecodeHrsMin(HourTypeHours[7]));
  TotHTHours := 0;
  // RV085.19.
  GoOn := True;
  if IsTailorMadeCVL then
    if (not ListHTExists(Employee, HourType)) then
      GoOn := False;
  if GoOn then
    for Count := 1 to 7 do
      TotHTHours := TotHTHours + HourTypeHours[Count];
  if WorkedHours then
  begin
    QRMemoTOT.Lines.Add(DecodeHrsMin(TotHTHours));
    QRMemoAbsTOT.Lines.Add('');
  end
  else
  begin
    QRMemoTOT.Lines.Add('');
    QRMemoAbsTOT.Lines.Add(DecodeHrsMin(TotHTHours));
  end;
end;

//550292
procedure TReportHoursPerEmplQR.FillHrsPerHT(WorkedHours: Boolean;
  Hour_Type, Minute_Days: Integer; Hour_Type_Description: String;
  WeekProdDate: TDateTime; IsWorkspotHourtype: Boolean);
var
  YearNr, WeekNr, DayNr: Word;
  Cds: TClientDataSet;
  // RV085.19. Accept all hourtypes for 'CVL'.
  function MinuteDaysTest: Boolean;
  begin
    if IsTailorMadeCVL and (not QRParameters.FSuppressZeroes) then
      Result := True
    else
      Result := (Minute_Days <> 0);
  end;
begin
  // RV085.19.
//  if Minute_Days = 0 then
//    Exit;
  if not MinuteDaysTest then
    Exit;
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerHT;
  ListProcsF.WeekUitDat(WeekProdDate, YearNr, WeekNr);
  DayNr := ListProcsF.DayWStartOnFromDate(WeekProdDate);
  if Cds.FindKey([WeekNr, Hour_Type]) then
  begin
    Cds.Edit;
    Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger :=
      Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger + Minute_Days;
    Cds.Post;
  end
  else
  begin
    Cds.Insert;
    Cds.FieldByName('WEEK_NR').AsInteger := WeekNr;
    Cds.FieldByName('HOUR_TYPE').AsInteger := Hour_Type;
    Cds.FieldByName('WORKED_HOURS').AsBoolean := WorkedHours;
    Cds.FieldByName('HOUR_TYPE_DESCRIPTION').AsString := Hour_Type_Description;
    Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger := Minute_Days;
    if IsWorkspotHourType then
      Cds.FieldByName('ISWORKSPOTHOURTYPE').AsString := '1'
    else
      Cds.FieldByName('ISWORKSPOTHOURTYPE').AsString := '0';
    Cds.Post;
  end;
end;

procedure TReportHoursPerEmplQR.FillHrsPerWeek(Min_Day: Integer;
  WeekProdDate: TDateTime);
var
  YearNr, WeekNr, DayNr: Word;
  Cds: TClientDataSet;
begin
  if Min_Day = 0 then
    Exit;
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerWeek;
  ListProcsF.WeekUitDat(WeekProdDate, YearNr, WeekNr);
  DayNr := ListProcsF.DayWStartOnFromDate(WeekProdDate);
  if Cds.FindKey([WeekNr, DayNr]) then
  begin
    Cds.Edit;
    Cds.FieldByName('MINUTE_DAY').AsInteger :=
      Cds.FieldByName('MINUTE_DAY').AsInteger + Min_Day;
    Cds.Post;
  end
  else
  begin
    Cds.Insert;
    Cds.FieldByName('WEEK_NR').AsInteger := WeekNr;
    Cds.FieldByName('DAY_NR').AsInteger := DayNr;
    Cds.FieldByName('MINUTE_DAY').AsInteger := Min_Day;
    Cds.Post;
  end;
end;

//550292
procedure TReportHoursPerEmplQR.FillMemoWeekPerDays;
var
  TotHours, DayNr, CountDay, LastDayFilled, MinuteNr: Integer;
  Cds: TClientDataSet;
  procedure FillDaysMemo(DayNr, MinuteDay: Integer);
  begin
    case DayNr of
      1: MO.Lines.Add(DecodeHrsMin(MinuteDay));
      2: TU.Lines.Add(DecodeHrsMin(MinuteDay));
      3: WE.Lines.Add(DecodeHrsMin(MinuteDay));
      4: TH.Lines.Add(DecodeHrsMin(MinuteDay));
      5: FR.Lines.Add(DecodeHrsMin(MinuteDay));
      6: SA.Lines.Add(DecodeHrsMin(MinuteDay));
      7: SU.Lines.Add(DecodeHrsMin(MinuteDay));
    end;
  end;
begin
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerWeek;
  Cds.First;
  TotHours := 0;
  LastDayFilled := -1;
  while not Cds.Eof do
  begin
    if QRMemoHT.Lines.IndexOf(SPimsWeek + '  ' +
      Cds.FieldByName('WEEK_NR').AsString) < 0 then
    begin
      // fill only if the week is filled
      if LastDayFilled >= 0 then
        for CountDay := LastDayFilled + 1 to 7 do
          FillDaysMemo(CountDay, 0);
      if TotHours <> 0 then
      begin
        QRMemoTot.Lines.Add(DecodeHrsMin(TotHours));
        QRMemoAbsTOT.Lines.Add('');
      end;
      //fill next week
      QRMemoHT.Lines.Add(SPimsWeek + '  ' +
        Cds.FieldByName('WEEK_NR').AsString);
      QRMemoHTDesc.Lines.Add('');
      LastDayFilled := 0;
      TotHours := 0;
    end;
    DayNr := Cds.FieldByName('DAY_NR').AsInteger;
    MinuteNr := Cds.FieldByName('MINUTE_DAY').AsInteger;
    // fill with empty minutes
    for CountDay := LastDayFilled + 1 to DayNr - 1 do
      FillDaysMemo(CountDay, 0);
    // fill minutes
    FillDaysMemo(DayNr, MinuteNr);

    TotHours := TotHours + MinuteNr;
    LastDayFilled := DayNr;
    Cds.Next;
  end;

  // before last week fill the rest of memo fields
  if LastDayFilled >= 0 then
    for CountDay := LastDayFilled + 1 to 7 do
      FillDaysMemo(CountDay, 0);
  if TotHours <> 0 then
  begin
    QRMemoTot.Lines.Add(DecodeHrsMin(TotHours));
    QRMemoAbsTOT.Lines.Add('');
  end;
end;

procedure TReportHoursPerEmplQR.FillMemoWeekPerDays_HT;
var
  TotHours, i, HourType, SaveWeek: Integer;
  ProdWeek: Array[1..7] of Integer;
  HourTypeDesc, RecordKey: String;
  WorkedHours: Boolean;
  Cds: TClientDataSet;
  GoOn: Boolean;
begin
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerHT;
  Cds.First;
  SaveWeek := 0;
  while not Cds.Eof do
  begin
    RecordKey := SPimsWeek + '  ' + Cds.FieldByName('WEEK_NR').AsString;
    if QRMemoHT.Lines.IndexOf(RecordKey) < 0 then
    begin
      //fill next week
      QRMemoHT.Lines.Add(RecordKey);
      QRMemoHTDesc.Lines.Add(' ');
      MO.Lines.Add('');
      TU.Lines.Add('');
      WE.Lines.Add('');
      TH.Lines.Add('');
      FR.Lines.Add('');
      SA.Lines.Add('');
      SU.Lines.Add('');
      QRMemoAbsTOT.Lines.Add('');
      QRMemoTOT.Lines.Add('');
      for i := 1 to 7 do
        ProdWeek[i] := 0;
      SaveWeek := Cds.FieldByName('WEEK_NR').AsInteger;
    end;

    HourType := Cds.FieldByName('HOUR_TYPE').AsInteger;
    HourTypeDesc := Cds.FieldByName('HOUR_TYPE_DESCRIPTION').AsString;
    WorkedHours := Cds.FieldByName('WORKED_HOURS').AsBoolean;

    // GLOB3-86
    // PIM-399
    if SystemDM.IsNTS and QRParameters.FShowHourType then
    begin
      if Cds.FieldByName('ISWORKSPOTHOURTYPE').AsString <> '0' then
      begin
        QRMemoHT.Lines.Add('  ' + IntToStr(HourType) + ' (X)');
      end
      else
      begin
        QRMemoHT.Lines.Add('  ' + IntToStr(HourType));
      end;
    end
    else
      QRMemoHT.Lines.Add('  ' + IntToStr(HourType));
    // RV045.2. Truncate description of hourtype-description
    QRMemoHTDesc.Lines.Add(Copy(HourTypeDesc, 1, 16));

    MO.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_1').AsInteger));
    TU.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_2').AsInteger));
    WE.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_3').AsInteger));
    TH.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_4').AsInteger));
    FR.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_5').AsInteger));
    SA.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_6').AsInteger));
    SU.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_7').AsInteger));
    TotHours := 0;
    // RV085.19.
    GoOn := True;
    if IsTailorMadeCVL then
      if (not ListHTExists(ReportHoursPerEmplDM.QueryEmpl.
        FieldByName('EMPLOYEE_NUMBER').AsInteger, IntToStr(HourType))) then
        GoOn := False;
    // RV100.1. Do not add the total-hours made for fixed soil-hourtypenumber
    if IsTailorMadeCVL2 then
      if HourType = CVL_SOIL_HOURTYPE_NUMBER then
        GoOn := False;
    if GoOn then
      for i := 1 to 7 do
        TotHours := TotHours +
          Cds.FieldByName('MINUTE_DAY_' + IntToStr(i)).AsInteger;
    if WorkedHours then
    begin
      QRMemoTOT.Lines.Add(DecodeHrsMin(TotHours));
      QRMemoAbsTOT.Lines.Add('');
    end
    else
    begin
      QRMemoAbsTOT.Lines.Add(DecodeHrsMin(TotHours));
      QRMemoTOT.Lines.Add('');
    end;
    // RV085.19.
    GoOn := True;
    if IsTailorMadeCVL then
      if (not ListHTExists(ReportHoursPerEmplDM.QueryEmpl.
        FieldByName('EMPLOYEE_NUMBER').AsInteger, IntToStr(HourType))) then
        GoOn := False;
    // RV100.1. Do not add the total-hours made for fixed soil-hourtypenumber
    if IsTailorMadeCVL2 then
      if HourType = CVL_SOIL_HOURTYPE_NUMBER then
        GoOn := False;
    if GoOn then
      for i := 1 to 7 do
        ProdWeek[i] := ProdWeek[i] +
          Cds.FieldByName('MINUTE_DAY_' + IntToStr(i)).AsInteger;
    Cds.Next;
    // fill total per week
    if (SaveWeek <> Cds.FieldByName('WEEK_NR').AsInteger) or Cds.Eof then
    begin
      // RV045.2. Total Week: Use UPimsMessageRes-string instead of literal.
      QRMemoHT.Lines.Add(STotalWeek);
      QRMemoHTDesc.Lines.Add(IntToStr(SaveWeek));
      MO.Lines.Add(DecodeHrsMin(ProdWeek[1]));
      TU.Lines.Add(DecodeHrsMin(ProdWeek[2]));
      WE.Lines.Add(DecodeHrsMin(ProdWeek[3]));
      TH.Lines.Add(DecodeHrsMin(ProdWeek[4]));
      FR.Lines.Add(DecodeHrsMin(ProdWeek[5]));
      SA.Lines.Add(DecodeHrsMin(ProdWeek[6]));
      SU.Lines.Add(DecodeHrsMin(ProdWeek[7]));
      TotHours := 0;
      for i := 1 to 7 do
        TotHours := TotHours + ProdWeek[i];
      QRMemoAbsTOT.Lines.Add('');
      QRMemoTOT.Lines.Add(DecodeHrsMin(TotHours));
      SaveWeek := Cds.FieldByName('WEEK_NR').AsInteger;
    end;
  end;

end;

procedure TReportHoursPerEmplQR.FillWeekPerEmployee(FList: TList;
  Employee: Integer; SetDirHrs, FillHourType: Boolean;
  var ProdWeek: TWorkedHours; SetAbsenceHrs, FillWeek: Boolean);
var
  ProdDate: TDateTime;
  Count: Integer;
  HourType: String;
  HourTypeDesc: String;
  EmployeeList, MinList, BreakMinList: Integer;
  HourTypeList: String;
  i, j: Integer;
  //550292
  YearNr, WeekNr: Word;
  HourTypeKey: String; // GLOB3-86
  BonusPercentage: Double; // GLOB3-205
  // RV085.19. Accept all hourtypes for 'CVL'.
  function MinListTest(MinList: Integer): Boolean;
  begin
    if IsTailorMadeCVL and (not QRParameters.FSuppressZeroes) then
      Result := True
    else
      Result := (MinList <> 0);
  end;
  // RV085.19.
  function ComputePlannedMinutes(AEmpNo: Integer;
    AFromDate: TDateTime): Integer;
  begin
    Result := ComputePlanned(
      ReportHoursPerEmplDM.WorkQuery, AEmpNo, '*', AFromDate);
  end;
  // RV099.2.
  function HourtypeNumberDescription(AHourtypeNumber: Integer;
    AHourtypeDescription: String): String;
  begin
    Result := IntToStr(AHourtypeNumber) + ';' + AHourtypeDescription;
  end;
  // RV099.2.
  function DetermineHourtypeDescription(AValue: String): String;
  var
    IPos: Integer;
  begin
    Result := AValue;
    IPos := Pos(';', AValue);
    if IPos > 0 then
      Result := Copy(AValue, IPos + 1, Length(AValue));
  end;
begin
  for i := 0 to FList.Count - 1 do
  begin
    APHERecord := FList.Items[i];
    EmployeeList := APHERecord.Employee;
    HourTypeList := APHERecord.HourType;
    MinList := APHERecord.MinSum;
    BreakMinList := APHERecord.BreakMin;
    // RV045.2. Also test for MinList <> 0 ! Or it can lead to double hours!
    // RV085.19.
    if (EmployeeList = Employee) and MinListTest(MinList) then
    begin
      ProdDate := APHERecord.DatePHE;
      Count := ListProcsF.DayWStartOnFromDate(ProdDate);
      if (QRParameters.FShowHourType) and FillHourType then
      begin
        for j := 1 to 7 do
          HourTypeHours[j] := 0;
        HourType := HourTypeList;
        HourTypeDesc := '';
        BonusPercentage := 0; // GLOB3-205
        ReportHoursPerEmplDM.UpdateHourTypePerCountry(APHERecord.Employee);
        ReportHoursPerEmplDM.ClientDataSetHT.Close;
        ReportHoursPerEmplDM.ClientDataSetHT.Open;
// RV098.1.
{$IFDEF DEBUG}
//  WDebugLog('- Hourtype: ' + IntToStr(HourType));
{$ENDIF}

        // RV093.7.
        // Problem: Hour type descriptions are truncated to 16 positions, during
        // the addition of them to TMemo-lines this can go wrong, when
        // they are similar for first 16 positions!
        // Solution: Do not truncate them here, but afterwards!
        if ReportHoursPerEmplDM.ClientDataSetHT.FindKey([StrToInt(HourType)]) then
        begin
          BonusPercentage := ReportHoursPerEmplDM.ClientDataSetHT.
            FieldByName('BONUS_PERCENTAGE').AsFloat; // GLOB3-205
          if (Self.EmployeeHourlyWage > 0) and (MinList > 0) then // GLOB3-205
          begin
            if (BonusPercentage > 0) then
              Self.EmployeeTotalPayment :=
                Self.EmployeeTotalPayment +
                (
                  ((BonusPercentage + 100) / 100) * MinList *
                    Self.EmployeeHourlyWage / 60.0
                )
            else
              Self.EmployeeTotalPayment :=
                Self.EmployeeTotalPayment +
                  (MinList * Self.EmployeeHourlyWage / 60.0);
          end;
          HourTypeDesc :=
            // RV099.2.
            HourtypeNumberDescription(StrToInt(HourType),
            // RV093.7. Do not truncate here, but afterwards!
              Trim(ReportHoursPerEmplDM.ClientDataSetHT.
                FieldByName('HDESCRIPTION').AsString)
              );
{            Trim(Copy(ReportHoursPerEmplDM.ClientDataSetHT.
              FieldByName('HDESCRIPTION').AsString, 1, 16)); // RV062.1. }
        end;
        //550292 -
        // GLOB3-86
        if SystemDM.IsNTS and QRParameters.FShowHourType then
        begin
          if APHERecord.IsWorkspotHourType then
            HourTypeKey := HourType + ' (X)'
          else
            HourTypeKey := HourType;
        end
        else
          HourTypeKey := HourType;
        if (QRMemoHT.Lines.IndexOf(HourTypeKey) < 0 ) and
          (Not QRParameters.FShowWeek) then
          QRMemoHT.Lines.Add(HourTypeKey);
        if QRParameters.FShowWeek then
          ListProcsF.WeekUitDat(ProdDate, YearNr, WeekNr);
        if (HourTypeDesc <> '') and
          ( ((QRMemoHTDesc.Lines.IndexOf(HourTypeDesc) < 0) and
             (Not QRParameters.FShowWeek)) or
            (QRParameters.FShowWeek and
             Not ReportHoursPerEmplDM.ClientDataSetHrsPerHT.
             FindKey([WeekNr, HourType]))) then
          begin
            if (Not QRParameters.FShowWeek) then
            begin
              QRMemoHTDesc.Lines.Add(HourTypeDesc);
{$IFDEF DEBUG}
  WDebugLog('- 2Hourtypedesc: ' + HourTypeDesc);
{$ENDIF}
            end;
            for j := 0 to FList.Count - 1 do
            begin
              APHERecord := FList.Items[j];
              EmployeeList  := APHERecord.Employee;
              HourTypeList := APHERecord.HourType;
              MinList :=  APHERecord.MinSum;
              if (EmployeeList = Employee) and (HourTypeList = HourType) then
              begin
                ProdDate := APHERecord.DatePHE;
                Count := ListProcsF.DayWStartOnFromDate(ProdDate);
                if IsTailorMadeCVL then // RV085.19.
                begin
                  if ListHTExists(Employee, HourType) then
                    ProdWeek[Count] := ProdWeek[Count] + MinList;
                end
                else
                  if IsTailorMadeCVL2 then // RV100.1.
                  begin
                    // Do not add hours this hourtype to the totals.
                    if StrToInt(HourType) <> CVL_SOIL_HOURTYPE_NUMBER then
                      ProdWeek[Count] := ProdWeek[Count] + MinList;
                  end
                  else
                    ProdWeek[Count] := ProdWeek[Count] + MinList;
                HourTypeHours[Count] := HourTypeHours[Count] + MinList;
                if QRParameters.FShowWeek then
                  FillHrsPerHT(not SetAbsenceHrs, StrToInt(HourTypeList), MinList,
                    DetermineHourtypeDescription(HourTypeDesc), ProdDate,
                    APHERecord.IsWorkspotHourType);
              end; //if empl = empl & ht
            end;  //for j
            if Not QRParameters.FShowWeek then
              FillMemoHTPerDays(Employee, HourType, not SetAbsenceHrs);

         end;// if < 0
      end // End Show Hour_Type
      else
      begin
        ProdWeek[Count] := ProdWeek[Count] +  MinList;
        //CAR 550292
        if FillWeek then
          if (not QRParameters.FShowPlantInfo) and
            (QRParameters.FShowWeek and not (QRParameters.FShowHourType)) then
            FillHrsPerWeek(MinList, ProdDate);
        // RV067.MRA.33 550480 Plant-info per week or not per week
        if QRParameters.FShowPlantInfo and not (QRParameters.FShowHourType) then
        begin
          if QRParameters.FShowWeek then
            FillHrsPerPlantWeek(APHERecord.PlantCode, APHERecord.PlantDesc,
              MinList, ProdDate)
          else
            FillHrsPerPlantDay(APHERecord.PlantCode, APHERecord.PlantDesc,
              MinList, ProdDate)
        end;
      end;

      if SetDirHrs then
        if QRParameters.FReportType = REPORTTYPE_SALARY then
        begin
          if SetAbsenceHrs then
            ProdWeek[Count] := ProdWeek[Count] + MinList
          else
            ProdWeek[Count] := ProdWeek[Count] + BreakMinList;
        end;
    end;//employee
  end;
  // RV093.7. Truncate descriptions afterwards!
  for i := 0 to QRMemoHTDesc.Lines.Count - 1 do
  begin
    QRMemoHTDesc.Lines.Strings[i] :=
      Copy(DetermineHourtypeDescription(QRMemoHTDesc.Lines.Strings[i]), 1, 16);
  end;
end;

procedure TReportHoursPerEmplQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Employee, Count: Integer;
  ProdDirSalWeek, ProdIndSalWeek: TWorkedHours;
  I: Integer; // MR:09-12-2002
//CAR 5-5-2003
  ExportTot: String;
begin
  inherited;
  EmployeeTotalPayment := 0;
  // GLOB3-205 Print Hourly_wage for employee
  QRLblHourlyWageTitle2.Caption := '';
  QRLblHourlyWage2.Caption := '';
  if (QRParameters.FReportType = REPORTTYPE_SALARY) then
  begin
    QRLblHourlyWage2.Caption := DetermineHourlyWage(
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger);
    if QRLblHourlyWage2.Caption <> '' then
      QRLblHourlyWageTitle2.Caption := SPimsHourlyWage;
  end; // if
  PrintBand := FTeamPrintBand;
  if not PrintBand then
  begin
    Exit;
  end;
  PrintBand := QRParameters.FShowHourType or QRParameters.FShowWeek or
    QRParameters.FShowPlantInfo;

  FPrintBandChild := PrintBand;

  QRMemoHT.Lines.Clear;
  QRMemoHTDesc.Lines.Clear;
  MO.Lines.Clear;
  TU.Lines.Clear;
  WE.Lines.Clear;
  TH.Lines.Clear;
  FR.Lines.Clear;
  SA.Lines.Clear;
  SU.Lines.Clear;
  QRMemoTOT.Lines.Clear;
  QRMemoAbsTOT.Lines.Clear;

  Employee :=
    ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;

  FillWeekPerEmployee(FListPHE, Employee, False, True, ProdHour, False, True );
  if (QRParameters.FReportType = REPORTTYPE_SALARY) then
    FillWeekPerEmployee(FListABSSAL, Employee,  False, True, AbsenceHour, True,
      True)
  else
    FillWeekPerEmployee(FListABSSAL, Employee,  False, True, AbsenceHour, True,
      False);

   //CAR: 550292 - for show week
  if QRParameters.FShowWeek then
    FillMemoWeekPerDays;
  if (QRParameters.FReportType = REPORTTYPE_SALARY) and
      QRParameters.FShowWeek and QRParameters.FShowHourType then
    FillMemoWeekPerDays_HT;
  // RV067.MRA.33 550480
  if QRParameters.FShowPlantInfo then
  begin
    if QRParameters.FShowWeek then
      FillMemoPlantWeekPerDays
    else
      FillMemoPlantPerDays;
  end;

  if PrintBand then
    if QRMemoHT.Lines.Count = 0 then
      PrintBand := False;

  FillWeekPerEmployee(FListDirHrs, Employee, True, False, TotDirHour, False, False);
  FillWeekPerEmployee(FListIndHrs, Employee, True, False,  TotIndHour, False,
    False);
  if (QRParameters.FReportType = REPORTTYPE_SALARY) then
  begin
    for count := 1 to 7 do
      ProdHour[count] := ProdHour[count]  + AbsenceHour[Count];
    for count := 1 to 7 do
    begin
      ProdDirSalWeek[Count] := 0;
      ProdIndSalWeek[Count] := 0;
    end;
    FillWeekPerEmployee(FListIndHrsSal, Employee, False,
      False, ProdIndSalWeek, False, False);
    FillWeekPerEmployee(FListDirHrsSal, Employee, False,
      False, ProdDirSalWeek, False, False );
    for Count:= 1 to 7 do
    begin
      TotDirHour[Count] := TotDirHour[Count] + ProdDirSalWeek[Count];
      TotIndHour[Count] := TotIndHour[Count] + ProdIndSalWeek[Count];
    end;
  end
  else
  begin
    FillWeekPerEmployee(FListProdHrs,
      Employee, False, False, TotProductiveHour, False, False);
    FillWeekPerEmployee(FListNonProdHrs,
      Employee, False, False, TotNProductiveHour, False, False);
  end;
  FPrintBandChild := PrintBand;

  // MR:09-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    for I:=0 to QRMemoHT.Lines.Count - 1 do
    begin
    // CAR 9-7-2003
      ExportTot := '';
      if (I <= QRMemoTOT.Lines.Count - 1)  then
        ExportTot := QRMemoTOT.Lines.Strings[I];
      if (ExportTot = '') and (I <= QRMemoAbsTOT.Lines.Count - 1) then
        ExportTot := QRMemoAbsTOT.Lines.Strings[I];

      if (I <= QRMemoHTDesc.Lines.Count - 1) and
         (I <= MO.Lines.Count -1) and (I <= TU.Lines.Count -1) and
         (I <= FR.Lines.Count -1) and (I <= SA.Lines.Count -1) and
         (I <= SU.Lines.Count -1) then
        ExportDetail(
          QRDBText3.DataSet.FieldByName('TEAM_CODE').AsString,
          QRDBText6.DataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
          QRDBText5.DataSet.FieldByName('DESCRIPTION').AsString, //RV065.1.
          QRMemoHT.Lines.Strings[I],
          QRMemoHTDesc.Lines.Strings[I],
          MO.Lines.Strings[I],
          TU.Lines.Strings[I],
          WE.Lines.Strings[I],
          TH.Lines.Strings[I],
          FR.Lines.Strings[I],
          SA.Lines.Strings[I],
          SU.Lines.Strings[I],
          ExportTot,
          '',
          '');
    end;
end; // QRBandDetailEmployeeBeforePrint

procedure TReportHoursPerEmplQR.QRLabelEmplMOPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[1]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplTUPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[2]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplWEPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[3]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplTUFPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[4]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplFRPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[5]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplSAPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[6]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplSUPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(ProdHour[7]);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplTotWKPrint(sender: TObject;
  var Value: String);
var
  TotalWK,
  Count: Integer;
begin
  inherited;
  TotalWK := 0;
  for Count := 1 to 7 do
    TotalWK := TotalWK + ProdHour[Count];
  Value := DecodeHrsMin(TotalWK);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplTotABSPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  TeamTotAbsence := TeamTotAbsence + EmplSumABSMin;
  Value :=  DecodeHrsMin(EmplSumABSMin);
//18.02.2003 car
//CAR 21-8-2003

  // ABS-5675
  if (QRParameters.FReportType = REPORTTYPE_SALARY) {and (FDayDiff >= 7)} then
  begin
    // PIM-387
    if AllHourTypes then
      Value :=  DecodeHrsMin(FEmplContrMin)
    else
      Value := '';
  end;

  // ABS-5675
//  if (QRParameters.FReportType = REPORTTYPE_SALARY) and (FDayDiff < 7) then
//    Value := '';
//
  if IsTailorMadeCVL then // RV085.19.
    Value := DecodeHrsMin(TotalEmplExceptHrDefMinute);
end;

procedure TReportHoursPerEmplQR.QRLabelEmplTotPrint(sender: TObject;
  var Value: String);
var
  TotalMin, Count: Integer;
begin
  inherited;
  TotalMin := EmplSumABSMin;
  for Count := 1 to 7 do
    TotalMin := TotalMin + ProdHour[Count];
  //CAR 5-5-2003
   if QRParameters.FReportType = REPORTTYPE_SALARY then
   begin
     TotalMin := 0;
     for Count := 1 to 7 do
       TotalMin := TotalMin + ProdHour[Count];
     if IsTailorMadeCVL then // RV085.19.
       Value := DecodeHrsMin(TotalMin - TotalEmplExceptHrDefMinute)
     else
     begin
       // PIM-387
      if AllHourTypes then
        Value := DecodeHrsMin(TotalMin - FEmplContrMin)
      else
        Value := '';
     end;
//CAR 18-8-2003
    // ABS-5675
//     if FDayDiff < 7 then
//       Value := '';
   end
   else
     Value := DecodeHrsMin(TotalMin);
end;

procedure TReportHoursPerEmplQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Count: Integer;
begin
  inherited;
  for Count := 1 to 7 do
  begin
    TotProdHour[Count] := TotProdHour[Count] + ProdHour[Count];
   // TotAbsence := TotAbsence + AbsenceHour[Count];
  end;

  for Count := 1 to 7 do
    TeamProdHour[Count] := TeamProdHour[Count] + ProdHour[Count];
end;

procedure TReportHoursPerEmplQR.QRBandSumarryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotDirect,
  TotIndirect,
  TotProd,
  TotProductive,
  TotNProductive,
  Count: Integer;
  procedure SetProductiveToEmpty;
  begin
    QRLabelProdMO.Caption := '';
    QRLabelProdTU.Caption := '';
    QRLabelProdWE.Caption := '';
    QRLabelProdTH.Caption := '';
    QRLabelProdFR.Caption := '';
    QRLabelProdSA.Caption := '';
    QRLabelProdSU.Caption := '';

    QRLabelNProdMO.Caption := '';
    QRLabelNProdTU.Caption := '';
    QRLabelNProdWE.Caption := '';
    QRLabelNProdTH.Caption := '';
    QRLabelNProdFR.Caption := '';
    QRLabelNProdSA.Caption := '';
    QRLabelNProdSU.Caption := '';
    QRLabelProdHrs.Caption := '';
    QRLabelNProdHrs.Caption := '';
    QRLabelTotProd.Caption  := '';
    QRLabelTotNProd.Caption := '';
  end;
  procedure SetManualCorToEmpty;
  begin
    QRLabelCorMO.Caption := '';
    QRLabelCorTU.Caption := '';
    QRLabelCorWE.Caption := '';
    QRLabelCorTH.Caption := '';
    QRLabelCorFR.Caption := '';
    QRLabelCorSA.Caption := '';
    QRLabelCorSU.Caption := '';
    QRLabelTotCor.Caption  := '';
    QRLabelManualCor.Caption := '';
  end;
begin
  //RV065.1. everywhere where the number of minutes were shown in this function replaced with hh:mm format
  inherited;
  TotDirect := 0;
  TotIndirect := 0;
  TotProd := 0;
  TotProductive := 0;
  TotNProductive := 0;

  for Count := 1 to 7 do
  begin
    TotDirect := TotDirect + TotDirHour[Count];
    TotIndirect := TotIndirect + TotIndHour[Count];

    TotProductive :=  TotProductive + TotProductiveHour[Count];
    TotNProductive := TotNProductive + TotNProductiveHour[Count];
    TotProd := TotProd + TotProdHour[Count];
  end;

  QRLabelDirMO.Caption :=   DecodeHrsMin(TotDirHour[1]);
  QRLabelDirTU.Caption :=   DecodeHrsMin(TotDirHour[2]);
  QRLabelDirWE.Caption :=   DecodeHrsMin(TotDirHour[3]);
  QRLabelDirTH.Caption :=   DecodeHrsMin(TotDirHour[4]);
  QRLabelDirFR.Caption :=   DecodeHrsMin(TotDirHour[5]);
  QRLabelDirSA.Caption :=   DecodeHrsMin(TotDirHour[6]);
  QRLabelDirSU.Caption :=   DecodeHrsMin(TotDirHour[7]);

  QRLabelIndMO.Caption :=   DecodeHrsMin(TotIndHour[1]);
  QRLabelIndTU.Caption :=   DecodeHrsMin(TotIndHour[2]);
  QRLabelIndWE.Caption :=   DecodeHrsMin(TotIndHour[3]);
  QRLabelIndTH.Caption :=   DecodeHrsMin(TotIndHour[4]);
  QRLabelIndFR.Caption :=   DecodeHrsMin(TotIndHour[5]);
  QRLabelIndSA.Caption :=   DecodeHrsMin(TotIndHour[6]);
  QRLabelIndSU.Caption :=   DecodeHrsMin(TotIndHour[7]);

//show productive & non productive values
  if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
  begin
    SetManualCorToEmpty;
    QRLabelProdHrs.Caption := SRepProdHrs;
    QRLabelNProdHrs.Caption := SRepNonProdHrs;
    QRLabelProdMO.Caption := DecodeHrsMin(TotProductiveHour[1]);
    QRLabelProdTU.Caption := DecodeHrsMin(TotProductiveHour[2]);
    QRLabelProdWE.Caption := DecodeHrsMin(TotProductiveHour[3]);
    QRLabelProdTH.Caption := DecodeHrsMin(TotProductiveHour[4]);
    QRLabelProdFR.Caption := DecodeHrsMin(TotProductiveHour[5]);
    QRLabelProdSA.Caption := DecodeHrsMin(TotProductiveHour[6]);
    QRLabelProdSU.Caption := DecodeHrsMin(TotProductiveHour[7]);

    QRLabelNProdMO.Caption := DecodeHrsMin(TotNProductiveHour[1]);
    QRLabelNProdTU.Caption := DecodeHrsMin(TotNProductiveHour[2]);
    QRLabelNProdWE.Caption := DecodeHrsMin(TotNProductiveHour[3]);
    QRLabelNProdTH.Caption := DecodeHrsMin(TotNProductiveHour[4]);
    QRLabelNProdFR.Caption := DecodeHrsMin(TotNProductiveHour[5]);
    QRLabelNProdSA.Caption := DecodeHrsMin(TotNProductiveHour[6]);
    QRLabelNProdSU.Caption := DecodeHrsMin(TotNProductiveHour[7]);
    QRLabelTotProd.Caption := DecodeHrsMin(TotProductive);
    QRLabelTotNProd.Caption := DecodeHrsMin(TotNProductive);
  end
  else
  begin
    SetProductiveToEmpty;
    if (TotProd - (TotDirect + TotIndirect)) <> 0 then
    begin
      QRLabelCorMO.Caption :=
        DecodeHrsMin(TotProdHour[1]-(TotDirHour[1] + TotIndHour[1]));
      QRLabelCorTU.Caption :=
        DecodeHrsMin(TotProdHour[2]-(TotDirHour[2] + TotIndHour[2]));
      QRLabelCorWE.Caption :=
        DecodeHrsMin(TotProdHour[3]-(TotDirHour[3] + TotIndHour[3]));
      QRLabelCorTH.Caption :=
        DecodeHrsMin(TotProdHour[4]-(TotDirHour[4] + TotIndHour[4]));
      QRLabelCorFR.Caption :=
        DecodeHrsMin(TotProdHour[5]-(TotDirHour[5] + TotIndHour[5]));
      QRLabelCorSA.Caption :=
        DecodeHrsMin(TotProdHour[6]-(TotDirHour[6] + TotIndHour[6]));
      QRLabelCorSU.Caption :=
        DecodeHrsMin(TotProdHour[7]-(TotDirHour[7] + TotIndHour[7]));
      QRLabelTotCor.Caption :=
        DecodeHrsMin(TotProd - (TotDirect + TotIndirect));
      QRLabelManualCor.Caption := SRepHRSPerEmplCorrection;
    end
    else
      SetManualCorToEmpty
  end;
  QRLabelTotDIR.Caption := DecodeHrsMin(TotDirect);
  QRLabelTotInd.Caption := DecodeHrsMin(TotIndirect);
  QRLabelTotWK.Caption  := DecodeHrsMin(TotProd);

  QRLabelTotMO.Caption :=  DecodeHrsMin(TotProdHour[1]);
  QRLabelTotTU.Caption :=  DecodeHrsMin(TotProdHour[2]);
  QRLabelTotWE.Caption :=  DecodeHrsMin(TotProdHour[3]);
  QRLabelTotTH.Caption :=  DecodeHrsMin(TotProdHour[4]);
  QRLabelTotFR.Caption :=  DecodeHrsMin(TotProdHour[5]);
  QRLabelTotSA.Caption :=  DecodeHrsMin(TotProdHour[6]);
  QRLabelTotSU.Caption :=  DecodeHrsMin(TotProdHour[7]);

  QRLabelTotABS.Caption := DecodeHrsMin(TotAbsence);
  //Car 02.19.2003 remove total worked hours for salary report
  if QRParameters.FReportType = REPORTTYPE_SALARY then
  begin
    QRLabelTotal.Caption := '';
    QRLabelTotABS.Caption := '';
  end
  else
    QRLabelTotal.Caption  :=  DecodeHrsMin(TotAbsence + TotProd);

  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    // MR:10-12-2002 Export values
    // MR:10-12-2002 Grand Totals
    // TODO 21095 This was exported wrong (1 column diff.). Use ExportDetail.
    if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
      ExportDetail(
        QRLabel4.Caption, '', '', '', '',
        DecodeHrsMin(TotProdHour[1]),
        DecodeHrsMin(TotProdHour[2]),
        DecodeHrsMin(TotProdHour[3]),
        DecodeHrsMin(TotProdHour[4]),
        DecodeHrsMin(TotProdHour[5]),
        DecodeHrsMin(TotProdHour[6]),
        DecodeHrsMin(TotProdHour[7]),
        DecodeHrsMin(TotProd),
        DecodeHrsMin(TotAbsence),
        DecodeHrsMin(TotAbsence + TotProd))
    else
      ExportDetail(
        QRLabel4.Caption, '', '', '', '',
        DecodeHrsMin(TotProdHour[1]),
        DecodeHrsMin(TotProdHour[2]),
        DecodeHrsMin(TotProdHour[3]),
        DecodeHrsMin(TotProdHour[4]),
        DecodeHrsMin(TotProdHour[5]),
        DecodeHrsMin(TotProdHour[6]),
        DecodeHrsMin(TotProdHour[7]),
        DecodeHrsMin(TotProd),
        '',
        '');

    // MR:10-12-2002 Direct Hours
    // TODO 21095 This was exported wrong (1 column diff.). Use ExportDetail.
    ExportDetail(
      QRLabel21.Caption, '', '', '', '',
      DecodeHrsMin(TotDirHour[1]),
      DecodeHrsMin(TotDirHour[2]),
      DecodeHrsMin(TotDirHour[3]),
      DecodeHrsMin(TotDirHour[4]),
      DecodeHrsMin(TotDirHour[5]),
      DecodeHrsMin(TotDirHour[6]),
      DecodeHrsMin(TotDirHour[7]),
      DecodeHrsMin(TotDirect),
      '',
      ''
      );
    // MR:10-12-2002 Indirect Hours
    // TODO 21095 This was exported wrong (1 column diff.). Use ExportDetail.
    ExportDetail(
      QRLabel19.Caption, '', '', '', '',
      DecodeHrsMin(TotIndHour[1]),
      DecodeHrsMin(TotIndHour[2]),
      DecodeHrsMin(TotIndHour[3]),
      DecodeHrsMin(TotIndHour[4]),
      DecodeHrsMin(TotIndHour[5]),
      DecodeHrsMin(TotIndHour[6]),
      DecodeHrsMin(TotIndHour[7]),
      DecodeHrsMin(TotIndirect),
      '',
      ''
      );
  //show productive & non productive values
    if QRParameters.FReportType = REPORTTYPE_PRODUCTION then
    begin
      // MR:10-12-2002 Productive Hours
      // TODO 21095 This was exported wrong (1 column diff.). Use ExportDetail.
      ExportDetail(
        QRLabelProdHrs.Caption, '', '', '', '',
        DecodeHrsMin(TotProductiveHour[1]),
        DecodeHrsMin(TotProductiveHour[2]),
        DecodeHrsMin(TotProductiveHour[3]),
        DecodeHrsMin(TotProductiveHour[4]),
        DecodeHrsMin(TotProductiveHour[5]),
        DecodeHrsMin(TotProductiveHour[6]),
        DecodeHrsMin(TotProductiveHour[7]),
        DecodeHrsMin(TotProductive),
        '',
        ''
        );
      // MR:10-12-2002 Non-Productive Hours
      // TODO 21095 This was exported wrong (1 column diff.). Use ExportDetail.
      ExportDetail(
        QRLabelNProdHrs.Caption, '', '', '', '',
        DecodeHrsMin(TotNProductiveHour[1]),
        DecodeHrsMin(TotNProductiveHour[2]),
        DecodeHrsMin(TotNProductiveHour[3]),
        DecodeHrsMin(TotNProductiveHour[4]),
        DecodeHrsMin(TotNProductiveHour[5]),
        DecodeHrsMin(TotNProductiveHour[6]),
        DecodeHrsMin(TotNProductiveHour[7]),
        DecodeHrsMin(TotNProductive),
        '',
        ''
        );
    end;
  end;
end;

procedure TReportHoursPerEmplQR.QRDBText2Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FPrintBandChild then
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRLabelW1Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(1);
end;

procedure TReportHoursPerEmplQR.QRLabelW2Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(2);
end;

procedure TReportHoursPerEmplQR.QRLabelW3Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(3);
end;

procedure TReportHoursPerEmplQR.QRLabelW4Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(4);
end;

procedure TReportHoursPerEmplQR.QRLabelW5Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(5);
end;

procedure TReportHoursPerEmplQR.QRLabelW6Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(6);
end;

procedure TReportHoursPerEmplQR.QRLabelW7Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := SystemDM.GetDayWCode(7);
end;

procedure TReportHoursPerEmplQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count, TotalWK, TotalMin: Integer;
  EmployeeList, i : Integer;
  EmpContr, EmpDiff: String;
begin
  inherited;
  if not FTeamPrintBand then
  begin
    PrintBand := False;
    Exit;
  end;
  EmplSumABSMin := 0;
  TotalMin := 0;
  for i := 0 to FListABS.Count - 1 do
  begin
    APHERecord := FListABS.Items[i];
    EmployeeList  := APHERecord.Employee;
    if  (EmployeeList =
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger) then
    begin
      EmplSumABSMin := APHERecord.MinSum;
      TotalMin := APHERecord.MinSum;
      Break;
    end;
  end;


  for Count := 1 to 7 do
    TotalMin := TotalMin + ProdHour[Count];
  PrintBand := (TotalMin <> 0);
  //RV065.1.
  // MR:09-12-2002
  if PrintBand then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    begin
      TotalWK := 0;
      for Count := 1 to 7 do
        TotalWK := TotalWK + ProdHour[Count];

      if QRParameters.FReportType = REPORTTYPE_SALARY then
      begin
       // PIM-387
       if AllHourTypes then
       begin
         EmpContr := DecodeHrsMin(FEmplContrMin);
         EmpDiff := DecodeHrsMin(TotalWK - FEmplContrMin);
       end
       else
       begin
         EmpContr := '';
         EmpDiff := '';
       end;
       ExportDetail(
        QRLabel23.Caption,
        QRDBText6.DataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
        QRDBText5.DataSet.FieldByName('DESCRIPTION').AsString, //RV065.1.
        '', '',
        DecodeHrsMin(ProdHour[1]),
        DecodeHrsMin(ProdHour[2]),
        DecodeHrsMin(ProdHour[3]),
        DecodeHrsMin(ProdHour[4]),
        DecodeHrsMin(ProdHour[5]),
        DecodeHrsMin(ProdHour[6]),
        DecodeHrsMin(ProdHour[7]),
        DecodeHrsMin(TotalWK),
        EmpContr, // PIM-387
        EmpDiff); // PIM-387
      end
      else
        ExportDetail(
          QRLabel23.Caption,
          QRDBText6.DataSet.FieldByName('EMPLOYEE_NUMBER').AsString,
          QRDBText5.DataSet.FieldByName('DESCRIPTION').AsString, //RV065.1.
          '', '', //RV065.1.
          DecodeHrsMin(ProdHour[1]),
          DecodeHrsMin(ProdHour[2]),
          DecodeHrsMin(ProdHour[3]),
          DecodeHrsMin(ProdHour[4]),
          DecodeHrsMin(ProdHour[5]),
          DecodeHrsMin(ProdHour[6]),
          DecodeHrsMin(ProdHour[7]),
          DecodeHrsMin(TotalWK),
          DecodeHrsMin(EmplSumABSMin),
          DecodeHrsMin(TotalMin));
    end;
end;

procedure TReportHoursPerEmplQR.QRLabel23Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not FPrintBandChild then
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRDBText1Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not FPrintBandChild then
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRBandTitleEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FShowHourType then
  begin
    QRLabelHourType.VISIBLE := True;
    QRBandTitleEmp.Height := 72;
  end
  else
  begin
    QRLabelHourType.VISIBLE := False;
    QRBandTitleEmp.Height := 48;
  end;
end;

procedure TReportHoursPerEmplQR.QRDBText5Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if  FPrintBandChild then
    Value := ''
  else
  // RV054.3.
//    Value := Copy(Value, 1, 15);
    Value := Copy(Value, 1, 30);

end;

procedure TReportHoursPerEmplQR.QRLabelEmplPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not FEmplPrint then
  begin
    FEmplPrint := True;
    Value :=
      IntToStr(ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger) +
      '  ' +
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('DESCRIPTION').AsString;
  end
  else
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRGroupEmpBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Count, WeekNr, ModNr: Integer;
begin
  inherited;
  EmployeeTotalPayment := 0;
  for Count := 1 to 7 do
  begin
    ProdHour[Count] := 0;
    AbsenceHour[Count] := 0;
    //550292
    ProdHourWeek[Count] := 0;
  end;
  FEmplPrint := False;
  PrintBand := FTeamPrintBand;
  // don't print employee number on a separate line
//  if (not QRParameters.FShowWeek) and (not QRParameters.FShowHourType) then
  PrintBand := False;
  ReportHoursPerEmplDM.ClientDataSetHrsPerWeek.EmptyDataSet;
  ReportHoursPerEmplDM.ClientDataSetHrsPerHT.EmptyDataSet;
  ReportHoursPerEmplDM.ClientDataSetHrsPerPlantWeek.EmptyDataSet;
  ReportHoursPerEmplDM.ClientDataSetHrsPerPlant.EmptyDataSet;
//CAR 21-18-2003
  if FTeamPrintBand then
  begin
    FEmplContrMin := 0;
    // ABS-5675 Calculate contract-hours based of involved number-of-weeks
//    if (FDayDiff >= 7) then
    begin
      if not ReportHoursPerEmplDM.QueryEmplContract.IsEmpty then
      begin
        if ReportHoursPerEmplDM.QueryEmplContract.Locate('EMPLOYEE_NUMBER',
          ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger, []) then
        begin
          // ABS-5675 Calculate nr-of-week based of DayDiff
          WeekNr := FDayDiff div 7;
          ModNr := FDayDiff mod 7;
          if (ModNr >= 1) and (ModNr <= 6) then
            WeekNr := WeekNr + 1;
          // ABS-5675
          if WeekNr <= 0 then
            WeekNr := 1;
          FEmplContrMin := Trunc(ReportHoursPerEmplDM.QueryEmplContract.
             FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat * 60 * WeekNr);
        end;
      end;
    end;
  end;
  // RV085.19. Determine 'Total for Contract' here, per employee.
  //           For CVL this is determined in a different way.
  TotalEmplExceptHrDefMinute := 0;
  if IsTailorMadeCVL then
  begin
    if (FDayDiff >= 7) then
    begin
      WeekNr := FDayDiff div 7;
      TotalEmplExceptHrDefMinute := WeekNr *
        ReportHoursPerEmplDM.DetermineExceptionalHourDefMin(
          ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger);
    end;
  end;
(*
  // GLOB3-205 Print Hourly_wage for employee
  QRLblHourlyWageTitle.Caption := '';
  QRLblHourlyWage.Caption := '';
  if QRParameters.FShowWeek and
    (QRParameters.FReportType = REPORTTYPE_SALARY) then
  begin
    QRLblHourlyWage.Caption := DetermineHourlyWage(
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger);
    if QRLblHourlyWage.Caption <> '' then
      QRLblHourlyWageTitle.Caption := SPimsHourlyWage;
  end; // if
*)
end; // QRGroupEmpBeforePrint

procedure TReportHoursPerEmplQR.QRBandTitleEmpBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FShowHourType then
  begin
    QRLabelHourType.VISIBLE := True;
    QRBandTitleEmp.Height := 72;
  end
  else
  begin
    QRLabelHourType.VISIBLE := False;
    QRBandTitleEmp.Height := 48;
  end;
end;

procedure TReportHoursPerEmplQR.QRGroupTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
  TeamCode: String;
begin
  inherited;
  QRGroupTeam.ForceNewPage := QRParameters.FPageTeam;
  TeamCode :=
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('TEAM_CODE').AsString;

  if (TeamCode <> '') then
    QRLabelTeamDesc.Left := 112 // From 60 to 112
  else
  begin
    QRLabelTeamDesc.Caption := SNoTeam;
    QRLabelTeamDesc.Left := 0;
  end;
  if (QRParameters.FReportType = REPORTTYPE_PRODUCTION) then
    FTeamPrintBand := CheckEmptyTeamForPHE(TeamCode)
  else
    FTeamPrintBand := CheckEmptyTeamForSAL(TeamCode);
  for Count := 1 to 7 do
    TeamProdHour[Count] := 0;
  TeamTotAbsence := 0;
  PrintBand := FTeamPrintBand;
end;


procedure TReportHoursPerEmplQR.QRLabelTeamDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if (ReportHoursPerEmplDM.QueryEmpl.FieldByName('TEAM_CODE').AsString <> '') then
  begin
    ReportHoursPerEmplDM.TableTeam.
      FindKey([ReportHoursPerEmplDM.QueryEmpl.FieldByName('TEAM_CODE').AsString]);
    Value := ReportHoursPerEmplDM.TableTeam.FieldByName('DESCRIPTION').AsString;
  end
  else
    Value := SNoTeam;
end;

procedure TReportHoursPerEmplQR.QRLabel10Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[1]);
end;

procedure TReportHoursPerEmplQR.QRLabel12Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[2]);
end;

procedure TReportHoursPerEmplQR.QRLabel14Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[3]);
end;

procedure TReportHoursPerEmplQR.QRLabel22Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[4]);
end;

procedure TReportHoursPerEmplQR.QRLabel24Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[5]);
end;

procedure TReportHoursPerEmplQR.QRLabel25Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[6]);
end;

procedure TReportHoursPerEmplQR.QRLabel26Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamProdHour[7]);
end;

procedure TReportHoursPerEmplQR.QRLabel27Print(sender: TObject;
  var Value: String);
var
  TotalWK,
  Count: Integer;
begin
  inherited;
  TotalWK := 0;
  for Count := 1 to 7 do
    TotalWK := TotalWK + TeamProdHour[Count];
  Value := DecodeHrsMin(TotalWK);
end;



procedure TReportHoursPerEmplQR.QRLabel28Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(TeamTotAbsence);
  //Car 02.19.2003 remove total worked hours for salary report
  if QRParameters.FReportType = REPORTTYPE_SALARY then
    Value := ''
end;

procedure TReportHoursPerEmplQR.QRLabel37Print(sender: TObject;
  var Value: String);
var
  TotalMin, Count: Integer;
begin
  inherited;
  TotalMin := TeamTotAbsence;
  for Count := 1 to 7 do
    TotalMin := TotalMin + TeamProdHour[Count];
   //Car 02.19.2003 remove total worked hours for salary report
  if QRParameters.FReportType = REPORTTYPE_SALARY then
    Value := ''
  else
    Value := DecodeHrsMin(TotalMin);
end;

procedure TReportHoursPerEmplQR.QRBandFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count, TotalWK, TotalMin: Integer;
begin
  inherited;
  PrintBand := FTeamPrintBand;
  TotAbsence := TotAbsence + TeamTotAbsence;
  // MR:10-12-2002
  if PrintBand and QRParameters.FExportToFile and
    (not ExportClass.ExportDone) then
  begin
    TotalWK := 0;
    for Count := 1 to 7 do
      TotalWK := TotalWK + TeamProdHour[Count];
    TotalMin := TeamTotAbsence;
    for Count := 1 to 7 do
      TotalMin := TotalMin + TeamProdHour[Count];
    if QRParameters.FReportType = REPORTTYPE_SALARY then
      ExportDetail(
        QRLabel8.Caption + QRDBText4.DataSet.FieldByName('TEAM_CODE').AsString,
        '', '', '', '', //RV065.1.
        DecodeHrsMin(TeamProdHour[1]),
        DecodeHrsMin(TeamProdHour[2]),
        DecodeHrsMin(TeamProdHour[3]),
        DecodeHrsMin(TeamProdHour[4]),
        DecodeHrsMin(TeamProdHour[5]),
        DecodeHrsMin(TeamProdHour[6]),
        DecodeHrsMin(TeamProdHour[7]),
        DecodeHrsMin(TotalWK),
        '',
        '')
    else
      ExportDetail(
        QRLabel8.Caption + QRDBText4.DataSet.FieldByName('TEAM_CODE').AsString,
        '', '', '', '',
        DecodeHrsMin(TeamProdHour[1]),
        DecodeHrsMin(TeamProdHour[2]),
        DecodeHrsMin(TeamProdHour[3]),
        DecodeHrsMin(TeamProdHour[4]),
        DecodeHrsMin(TeamProdHour[5]),
        DecodeHrsMin(TeamProdHour[6]),
        DecodeHrsMin(TeamProdHour[7]),
        DecodeHrsMin(TotalWK),
        DecodeHrsMin(TeamTotAbsence),
        DecodeHrsMin(TotalMin));
  end;
end;

procedure TReportHoursPerEmplQR.QRBandSumarryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:06-12-2002
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHoursPerEmplQR.QRLabel34Print(sender: TObject;
  var Value: String);
begin
  inherited;
  //CAR - Remove string for salary hours report 19.02.2003
  if QRParameters.FReportType = REPORTTYPE_SALARY then
// RV054.3.
//    Value := '            '+ SRepHrsPerEmplTotal;
    Value := SRepHrsPerEmplTotal;
end;

procedure TReportHoursPerEmplQR.FreeItemList(TmpList: TList);
var
  I: Integer;
begin
  if TmpList <> nil then
  begin
    for I := TmpList.Count - 1  downto 0 do
    begin
      Dispose(TmpList.Items[I]);
      TmpList.Remove(TmpList.Items[I]);
    end;
  end;
end;

procedure TReportHoursPerEmplQR.FreeList(TmpList: TList);
begin
  if TmpList <> nil then
  begin
    FreeItemList(TmpList);
    TmpList.Free;
  end;
end;

procedure TReportHoursPerEmplQR.FormDestroy(Sender: TObject);
begin
  inherited;
  FListTeamPHE.Free;
  FListTeamABS.Free;
  FListTeamSAL.Free;

  FreeList(FListPHE);
  FreeList(FListABS);
  FreeList(FListDirHrs);
  FreeList(FListIndHrs);
  FreeList(FListDirHrsSAL);
  FreeList(FListIndHrsSAL);
  FreeList(FListProdHrs);
  FreeList(FListNonProdHrs);
  FreeList(FListABSSAL);
  FreeList(FListHT); // RV085.19.
end;

procedure TReportHoursPerEmplQR.ChildBandProdBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FTeamPrintBand and
    QRParameters.FShowWeek and (Not QRParameters.FShowHourType);

//  SetHeightMemoBand(PrintBand, QRBandDetailEmployee);
  // RV080.1.
//  AMemoClass.SetHeightMemoBand(PrintBand, QRBandDetailEmployee);
end;

// RV040.
procedure TReportHoursPerEmplQR.ClearLists;
begin
  FListTeamPHE.Clear;
  FListTeamABS.Clear;
  FListTeamSAL.Clear;

  FreeItemList(FListPHE);
  FreeItemList(FListABS);
  FreeItemList(FListDirHrs);
  FreeItemList(FListIndHrs);
  FreeItemList(FListNonProdHrs);
  FreeItemList(FListProdHrs);
  FreeItemList(FListABSSAL);
  FreeItemList(FListIndHrsSAL);
  FreeItemList(FListDirHrsSAL);
  FreeItemList(FListHT); // RV085.19.
end;

procedure TReportHoursPerEmplQR.QRLabel41Print(sender: TObject;
  var Value: String);
begin
  inherited;
// RV054.3. When reporttype = REPORTTYPE_PRODUCTION is will use the original text
//          as defined in report.
  if QRParameters.FReportType = REPORTTYPE_SALARY then
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRLabel15Print(sender: TObject;
  var Value: String);
begin
  inherited;
// RV054.3. When reporttype = REPORTTYPE_PRODUCTION is will use the original text
//          as defined in report.
  if QRParameters.FReportType = REPORTTYPE_SALARY then
    Value := '';
end;

procedure TReportHoursPerEmplQR.QRGroupPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

// RV067.MRA.33 550480 Prod. Hrs. Per Plant.
procedure TReportHoursPerEmplQR.FillHrsPerPlantDay(APlantCode,
  APlantDesc: String; AMin_Day: Integer; AProdDate: TDateTime);
var
  DayNr: Word;
  Cds: TClientDataSet;
begin
  if AMin_Day = 0 then
    Exit;
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerPlant;
  DayNr := ListProcsF.DayWStartOnFromDate(AProdDate);
  if Cds.FindKey([APlantCode, DayNr]) then
  begin
    Cds.Edit;
    Cds.FieldByName('MINUTE_DAY').AsInteger :=
      Cds.FieldByName('MINUTE_DAY').AsInteger + AMin_Day;
    Cds.Post;
  end
  else
  begin
    Cds.Insert;
    Cds.FieldByName('PLANT_CODE').AsString := APlantCode;
    Cds.FieldByName('DAY_NR').AsInteger := DayNr;
    Cds.FieldByName('MINUTE_DAY').AsInteger := AMin_Day;
    Cds.FieldByName('DESCRIPTION').AsString := APlantDesc;
    Cds.Post;
  end;
end;

// RV067.MRA.33 550480 Prod. Hrs. Per Plant and Week.
procedure TReportHoursPerEmplQR.FillHrsPerPlantWeek(APlantCode,
  APlantDesc: String; AMin_Day: Integer; AProdDate: TDateTime);
var
  YearNr, WeekNr, DayNr: Word;
  Cds: TClientDataSet;
begin
  if AMin_Day = 0 then
    Exit;
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerPlantWeek;
  ListProcsF.WeekUitDat(AProdDate, YearNr, WeekNr);
  DayNr := ListProcsF.DayWStartOnFromDate(AProdDate);
  if Cds.FindKey([WeekNr, APlantCode]) then
  begin
    Cds.Edit;
    Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger :=
      Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger + AMin_Day;
    Cds.Post;
  end
  else
  begin
    Cds.Insert;
    Cds.FieldByName('WEEK_NR').AsInteger := WeekNr;
    Cds.FieldByName('PLANT_CODE').AsString := APlantCode;
    Cds.FieldByName('MINUTE_DAY_' + IntToStr(DayNr)).AsInteger := AMin_Day;
    Cds.FieldByName('DESCRIPTION').AsString := APlantDesc;
    Cds.Post;
  end;
end;

// RV067.MRA.33 550480 Prod. Hrs. per plant and per day, do not show weeks
procedure TReportHoursPerEmplQR.FillMemoPlantPerDays;
var
  TotHours, DayNr, CountDay, LastDayFilled, MinuteNr: Integer;
  Cds: TClientDataSet;
  RecordKey: String;
  procedure FillDaysMemo(DayNr, MinuteDay: Integer);
  begin
    case DayNr of
      1: MO.Lines.Add(DecodeHrsMin(MinuteDay));
      2: TU.Lines.Add(DecodeHrsMin(MinuteDay));
      3: WE.Lines.Add(DecodeHrsMin(MinuteDay));
      4: TH.Lines.Add(DecodeHrsMin(MinuteDay));
      5: FR.Lines.Add(DecodeHrsMin(MinuteDay));
      6: SA.Lines.Add(DecodeHrsMin(MinuteDay));
      7: SU.Lines.Add(DecodeHrsMin(MinuteDay));
    end;
  end;
begin
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerPlant;
  Cds.First;
  TotHours := 0;
  LastDayFilled := -1;
  while not Cds.Eof do
  begin
    RecordKey := Cds.FieldByName('PLANT_CODE').AsString;
    if QRMemoHT.Lines.IndexOf(RecordKey) < 0 then
    begin
      // fill only if the week is filled
      if LastDayFilled >= 0 then
        for CountDay := LastDayFilled + 1 to 7 do
          FillDaysMemo(CountDay, 0);
      if TotHours <> 0 then
      begin
        QRMemoTot.Lines.Add(DecodeHrsMin(TotHours));
        QRMemoAbsTOT.Lines.Add('');
      end;
      //fill next week
      QRMemoHT.Lines.Add(RecordKey);
      QRMemoHTDesc.Lines.Add(
        Copy(Cds.FieldByName('DESCRIPTION').AsString, 1, 16));
      LastDayFilled := 0;
      TotHours := 0;
    end; // if QRMemoHT.Lines.IndexOf(RecordKey) < 0 then
    DayNr := Cds.FieldByName('DAY_NR').AsInteger;
    MinuteNr := Cds.FieldByName('MINUTE_DAY').AsInteger;
    // fill with empty minutes
    for CountDay := LastDayFilled + 1 to DayNr - 1 do
      FillDaysMemo(CountDay, 0);
    // fill minutes
    FillDaysMemo(DayNr, MinuteNr);

    TotHours := TotHours + MinuteNr;
    LastDayFilled := DayNr;
    Cds.Next;
  end; // while not Cds.Eof do

  // before last week fill the rest of memo fields
  if LastDayFilled >= 0 then
    for CountDay := LastDayFilled + 1 to 7 do
      FillDaysMemo(CountDay, 0);
  if TotHours <> 0 then
  begin
    QRMemoTot.Lines.Add(DecodeHrsMin(TotHours));
    QRMemoAbsTOT.Lines.Add('');
  end;
end;

// RV067.MRA.33 550480 Prod. Hrs. per plant and per day, show weeks.
procedure TReportHoursPerEmplQR.FillMemoPlantWeekPerDays;
var
  TotHours, i, SaveWeek: Integer;
  PlantCode, PlantDescription, RecordKey: String;
  ProdWeek: Array[1..7] of Integer;
  Cds: TClientDataSet;
begin
  Cds := ReportHoursPerEmplDM.ClientDataSetHrsPerPlantWeek;
  Cds.First;
  SaveWeek := 0;
  while not Cds.Eof do
  begin
    RecordKey := SPimsWeek + '  ' + Cds.FieldByName('WEEK_NR').AsString;
    if QRMemoHT.Lines.IndexOf(RecordKey) < 0 then
    begin
      //fill next week
      QRMemoHT.Lines.Add(RecordKey);
      QRMemoHTDesc.Lines.Add(' ');
      MO.Lines.Add('');
      TU.Lines.Add('');
      WE.Lines.Add('');
      TH.Lines.Add('');
      FR.Lines.Add('');
      SA.Lines.Add('');
      SU.Lines.Add('');
      QRMemoAbsTOT.Lines.Add('');
      QRMemoTOT.Lines.Add('');
      for i := 1 to 7 do
        ProdWeek[i] := 0;
      SaveWeek := Cds.FieldByName('WEEK_NR').AsInteger;
    end;

    PlantCode := Cds.FieldByName('PLANT_CODE').AsString;
    PlantDescription := Cds.FieldByName('DESCRIPTION').AsString;

    QRMemoHT.Lines.Add('  ' + PlantCode);
    // RV045.2. Truncate description of hourtype-description
    QRMemoHTDesc.Lines.Add(Copy(PlantDescription, 1, 16));

    MO.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_1').AsInteger));
    TU.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_2').AsInteger));
    WE.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_3').AsInteger));
    TH.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_4').AsInteger));
    FR.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_5').AsInteger));
    SA.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_6').AsInteger));
    SU.Lines.Add(DecodeHrsMin(Cds.FieldByName('MINUTE_DAY_7').AsInteger));
    TotHours := 0;
    for i := 1 to 7 do
      TotHours := TotHours +
        Cds.FieldByName('MINUTE_DAY_' + IntToStr(i)).AsInteger;
    QRMemoTOT.Lines.Add(DecodeHrsMin(TotHours));
    QRMemoAbsTOT.Lines.Add('');
    for i := 1 to 7 do
      ProdWeek[i] := ProdWeek[i] +
        Cds.FieldByName('MINUTE_DAY_' + IntToStr(i)).AsInteger;
    Cds.Next;
    // fill total per week
    if (SaveWeek <> Cds.FieldByName('WEEK_NR').AsInteger) or Cds.Eof then
    begin
      // RV045.2. Total Week: Use UPimsMessageRes-string instead of literal.
      QRMemoHT.Lines.Add(STotalWeek);
      QRMemoHTDesc.Lines.Add(IntToStr(SaveWeek));
      MO.Lines.Add(DecodeHrsMin(ProdWeek[1]));
      TU.Lines.Add(DecodeHrsMin(ProdWeek[2]));
      WE.Lines.Add(DecodeHrsMin(ProdWeek[3]));
      TH.Lines.Add(DecodeHrsMin(ProdWeek[4]));
      FR.Lines.Add(DecodeHrsMin(ProdWeek[5]));
      SA.Lines.Add(DecodeHrsMin(ProdWeek[6]));
      SU.Lines.Add(DecodeHrsMin(ProdWeek[7]));
      TotHours := 0;
      for i := 1 to 7 do
        TotHours := TotHours + ProdWeek[i];
      QRMemoAbsTOT.Lines.Add('');
      QRMemoTOT.Lines.Add(DecodeHrsMin(TotHours));
      SaveWeek := Cds.FieldByName('WEEK_NR').AsInteger;
    end;
  end; // while not Cds.Eof do
end;

// RV100.1.
function TReportHoursPerEmplQR.IsTailorMadeCVL2: Boolean;
begin
  if (SystemDM.IsCVL and QRParameters.FShowHourType
    and QRParameters.FExportToFile) then
    Result := True
  else
    Result := False;
end;

// TODO 21095
// NOTE: This overrules the function in ReportBase.
function TReportHoursPerEmplQR.DecodeHrsMin(HrsMin: Integer): String;
begin
  Result := ExportHrsMin(HrsMin);
end;

// PIM-387
function TReportHoursPerEmplQR.AllHourTypes: Boolean;
begin
  Result := QRParameters.FAllHourType or
    ((QRParameters.FHourTypeFrom = Self.FirstHourType) and
     (QRParameters.FHourTypeTo = Self.LastHourType));
end; // HourTypeAll

// GLOB3-205
function TReportHoursPerEmplQR.DetermineHourlyWage(
  AEmployeeNumber: Integer): String;
begin
  Result := '';
  EmployeeHourlyWage := 0;
  with ReportHoursPerEmplDM do
  begin
    if not QueryEmplContract.IsEmpty then
    begin
      if QueryEmplContract.Locate('EMPLOYEE_NUMBER', AEmployeeNumber, []) then
      begin
        if QueryEmplContract.FieldByName('HOURLY_WAGE').AsFloat <> 0 then
        begin
          EmployeeHourlyWage := QueryEmplContract.FieldByName('HOURLY_WAGE').AsFloat;
          Result := Format('%.2n', [EmployeeHourlyWage]);
        end;
      end; // if
    end; // if
  end; // with
end; // DetermineHourlyWage

// GLOB3-205
procedure TReportHoursPerEmplQR.InitExtraPayments;
begin
  with ReportHoursPerEmplDM do
  begin
    qryExtraPaymentsWeek.Filtered := False;
    qryExtraPaymentsWeek.Close;
    qryExtraPaymentsWeek.ParamByName('DATEFROM').AsDateTime :=
      QRParameters.FDateFrom;
    qryExtraPaymentsWeek.ParamByName('DATETO').AsDateTime :=
      QRParameters.FDateTo;
    qryExtraPaymentsWeek.Open;
  end;
end; // InitExtraPayments

// GLOB3-205
function TReportHoursPerEmplQR.DetermineExtraPayments(
  AEmployeeNumber: Integer): Boolean;
var
  YearNr, WeekNr: Word;
  PaymentAmountTotal: Double;
begin
  Result := False;

  QRMemoPEC.Lines.Clear;
  QRMemoPED.Lines.Clear;
  QRMemoPETot.Lines.Clear;
  PaymentAmountTotal := 0;
  with ReportHoursPerEmplDM do
  begin
    ListProcsF.WeekUitDat(
      QueryProductionMinute.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime,
        YearNr, WeekNr);
    with qryExtraPaymentsWeek do
    begin
      Filtered := False;
      Filter := 'EMPLOYEE_NUMBER=' + IntToStr(AEmployeeNumber);
      { +
        ' AND YEARNUMBER=' + IntToStr(YearNr) +
        ' AND WEEKNUMBER=' + IntToStr(WeekNr); }
      Filtered := True;
      while not Eof do
      begin
        Result := True;
        QRMemoPEC.Lines.Add(FieldByName('PAYMENT_EXPORT_CODE').AsString);
        QRMemoPED.Lines.Add(FieldByName('DESCRIPTION').AsString);
        QRMemoPETot.Lines.Add(
          Format('%.2n', [FieldByName('PAYMENT_AMOUNT').AsFloat]));
        PaymentAmountTotal := PaymentAmountTotal +
          FieldByName('PAYMENT_AMOUNT').AsFloat;
{WDebugLog('EP: Emp=' + IntToStr(AEmployeeNumber) +
  ' Y=' + IntToStr(YearNr) +
  ' W=' + IntToStr(WeekNr) +
  ' PEC=' + FieldByName('PAYMENT_EXPORT_CODE').AsString +
  ' PTOT=' + Format('%.2n', [FieldByName('PAYMENT_AMOUNT').AsFloat])
  ); }
        Next;
      end; // while
      if Result then
      begin
        QRMemoPEC.Lines.Add(SPimsTotalExtraPayments);
        QRMemoPED.Lines.Add('');
        QRMemoPETot.Lines.Add(Format('%.2n', [PaymentAmountTotal]));
      end;
    end; // with
  end; // with
end; // DetermineExtraPayments

// GLOB3-205
procedure TReportHoursPerEmplQR.ChildBandExtraPaymentsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := (QRParameters.FReportType = REPORTTYPE_SALARY);
  if PrintBand then
    PrintBand := DetermineExtraPayments(
      ReportHoursPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger);
end;

procedure TReportHoursPerEmplQR.ChildBandHourPaymentsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if EmployeeTotalPayment > 0 then
  begin
    PrintBand := True;
    QRLblTotalHourlyPayment.Caption := Format('%.2n', [EmployeeTotalPayment]);
  end;
end;

// GLOB3-205
procedure TReportHoursPerEmplQR.ChildBandHourPaymentsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    begin
      ExportClass.AddText(QRLabel42.Caption + ' ' +
        QRLblTotalHourlyPayment.Caption);
    end;
end;

// GLOB3-205
procedure TReportHoursPerEmplQR.ChildBandExtraPaymentsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  if BandPrinted then
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    begin
      if QRMemoPEC.Lines.Count > 0 then
      begin
        ExportClass.AddText(
            QRDBText10.DataSet.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
            QRDBText11.DataSet.FieldByName('DESCRIPTION').AsString);
        ExportClass.AddText(QRLabelExtraPayments.Caption);
        for I := 0 to QRMemoPEC.Lines.Count - 1 do
        begin
          ExportClass.AddText(
            QRMemoPEC.Lines.Strings[I] + ExportClass.Sep +
            QRMemoPED.Lines.Strings[I] + ExportClass.Sep +
            QRMemoPETot.Lines.Strings[I]
            );
        end;
      end;
    end;
end;

end.
