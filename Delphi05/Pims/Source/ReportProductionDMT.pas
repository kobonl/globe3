(*
  MRA: 30-NOV-2009 RV045.1.
    - Time For Time and Bonus In Money can be set on ContractGroup-level or
      HourType-level.
  MRA:1-OCT-2012 20013489 Overnight-Shift-System
  - For 'PopulateIDCard' query QueryTimeRegScanning must also get
    SHIFT_DATE-field.
  - Filter on SHIFT_DATE for QueryTimeRegScanning.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
    - Addition of field SHIFTDATESYSTEM_YN to PIMSSETTING-table.
    - Filter on SHIFT_DATE for QueryTimeRegScanning is optional.
*)

unit ReportProductionDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, CalculateTotalHoursDMT, DBClient;

type
  TReportProductionDM = class(TReportBaseDM)
    TableEmpl: TTable;
    TablePlant: TTable;
    TableDept: TTable;
    QueryProduction: TQuery;
    DataSourceProduction: TDataSource;
    TableDeptDEPARTMENT_CODE: TStringField;
    TableDeptPLANT_CODE: TStringField;
    TableDeptDESCRIPTION: TStringField;
    TableDeptBUSINESSUNIT_CODE: TStringField;
    TableDeptCREATIONDATE: TDateTimeField;
    TableDeptDIRECT_HOUR_YN: TStringField;
    TableDeptMUTATIONDATE: TDateTimeField;
    TableDeptMUTATOR: TStringField;
    TableDeptPLAN_ON_WORKSPOT_YN: TStringField;
    TableDeptREMARK: TStringField;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableWK: TTable;
    TableJob: TTable;
    QueryBUPerWK: TQuery;
    TableBU: TTable;
    TableBUBUSINESSUNIT_CODE: TStringField;
    TableBUDESCRIPTION: TStringField;
    TableBUPLANT_CODE: TStringField;
    TableBUCREATIONDATE: TDateTimeField;
    TableBUBONUS_FACTOR: TFloatField;
    TableBUNORM_ILL_VS_DIRECT_HOURS: TFloatField;
    TableBUMUTATIONDATE: TDateTimeField;
    TableBUMUTATOR: TStringField;
    TableBUNORM_ILL_VS_INDIRECT_HOURS: TFloatField;
    TableBUNORM_ILL_VS_TOTAL_HOURS: TFloatField;
    TableEmployeeContract: TTable;
    QueryProdHourPerEmployee: TQuery;
    QuerySalaryHourPerEmployee: TQuery;
    QueryTimeRegScanning: TQuery;
    TableBreakPerEmployee: TTable;
    TableBreakPerDepartment: TTable;
    TableBreakPerShift: TTable;
    QueryEmployeePlanning: TQuery;
    TableContractgroup: TTable;
    QueryRevenue: TQuery;
    AQuery: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProductionFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
  public
    { Public declarations }
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
  end;

var
  ReportProductionDM: TReportProductionDM;

implementation

{$R *.DFM}

uses
  GlobalDMT, SystemDMT;

procedure TReportProductionDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  ContractGroupTFT := GlobalDM.ContractGroupTFTExists;
  // RV067.2.
  SystemDM.PlantTeamFilterEnable(QueryProduction);
end;

procedure TReportProductionDM.QueryProductionFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV067.2.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
