inherited ContractGroupDM: TContractGroupDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Size = 1
    end
    object TableMasterBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Size = 1
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableMasterWEEKS_IN_PERIOD: TIntegerField
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableMasterWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Size = 1
    end
    object TableMasterHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableMasterHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourtype
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableMasterOVERTIMETYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OVERTIMETYPE'
      Calculated = True
    end
    object TableMasterGUARANTEED_HOURS: TIntegerField
      FieldName = 'GUARANTEED_HOURS'
    end
    object TableMasterGuaranteedHrs: TStringField
      FieldKind = fkCalculated
      FieldName = 'GUARANTEEDHRS'
      Size = 8
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    TableName = 'CONTRACTGROUP'
    UpdateMode = upWhereKeyOnly
    Top = 116
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Size = 1
    end
    object TableDetailBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Size = 1
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailPERIOD_STARTS_IN_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableDetailWEEKS_IN_PERIOD: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableDetailWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Size = 1
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourtype
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailOVERTIMETYPE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OVERTIMETYPE'
      Calculated = True
    end
    object TableDetailGUARANTEED_HOURS: TIntegerField
      FieldName = 'GUARANTEED_HOURS'
    end
    object TableDetailGuaranteedHrs: TStringField
      FieldKind = fkCalculated
      FieldName = 'GUARANTEEDHRS'
      Size = 8
      Calculated = True
    end
    object TableDetailEXCEPTIONAL_BEFORE_OVERTIME: TStringField
      FieldName = 'EXCEPTIONAL_BEFORE_OVERTIME'
      Size = 1
    end
    object TableDetailROUND_MINUTE: TIntegerField
      FieldName = 'ROUND_MINUTE'
      Required = True
    end
    object TableDetailROUND_TRUNCATE_HRS: TStringField
      FieldKind = fkCalculated
      FieldName = 'ROUND_TRUNCATE_HRS'
      Size = 30
      Calculated = True
    end
    object TableDetailROUND_TRUNC_SALARY_HOURS: TIntegerField
      FieldName = 'ROUND_TRUNC_SALARY_HOURS'
    end
    object TableDetailMAXIMUM_SALARY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'MAXIMUM_SALARY'
    end
    object TableDetailPAID_ILLNESS_PERIOD: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'PAID_ILLNESS_PERIOD'
      Required = True
    end
    object TableDetailUNPAID_ILLN_ABSENCEREASONLU: TStringField
      FieldKind = fkLookup
      FieldName = 'UNPAID_ILLN_ABSENCEREASONLU'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'UNPAID_ILLN_ABSENCEREASON_ID'
      Size = 30
      Lookup = True
    end
    object TableDetailBANKHOLIDAY_OVERRULES_AVAIL_YN: TStringField
      FieldName = 'BANKHOLIDAY_OVERRULES_AVAIL_YN'
      Size = 1
    end
    object TableDetailEXPORT_SR_HOURS_YN: TStringField
      FieldName = 'EXPORT_SR_HOURS_YN'
      Size = 1
    end
    object TableDetailUNPAID_ILLN_ABSENCEREASON_ID: TIntegerField
      FieldName = 'UNPAID_ILLN_ABSENCEREASON_ID'
      Required = True
    end
    object TableDetailWORKDAY_MO_YN: TStringField
      FieldName = 'WORKDAY_MO_YN'
      Size = 1
    end
    object TableDetailWORKDAY_TU_YN: TStringField
      FieldName = 'WORKDAY_TU_YN'
      Size = 1
    end
    object TableDetailWORKDAY_WE_YN: TStringField
      FieldName = 'WORKDAY_WE_YN'
      Size = 1
    end
    object TableDetailWORKDAY_TH_YN: TStringField
      FieldName = 'WORKDAY_TH_YN'
      Size = 1
    end
    object TableDetailWORKDAY_FR_YN: TStringField
      FieldName = 'WORKDAY_FR_YN'
      Size = 1
    end
    object TableDetailWORKDAY_SA_YN: TStringField
      FieldName = 'WORKDAY_SA_YN'
      Size = 1
    end
    object TableDetailWORKDAY_SU_YN: TStringField
      FieldName = 'WORKDAY_SU_YN'
      Size = 1
    end
    object TableDetailNORMALHOURSPERDAY: TFloatField
      FieldName = 'NORMALHOURSPERDAY'
    end
    object TableDetailRAISE_PERCENTAGE: TFloatField
      FieldName = 'RAISE_PERCENTAGE'
    end
    object TableDetailMAX_SAT_CREDIT_MINUTE: TFloatField
      FieldName = 'MAX_SAT_CREDIT_MINUTE'
    end
    object TableDetailEXPORT_NORMAL_HRS_YN: TStringField
      FieldName = 'EXPORT_NORMAL_HRS_YN'
      Size = 1
    end
    object TableDetailEXPORT_WORKED_DAYS_YN: TStringField
      FieldName = 'EXPORT_WORKED_DAYS_YN'
      Size = 1
    end
    object TableDetailHOURTYPE_WRK_ON_BANKHOL: TIntegerField
      FieldName = 'HOURTYPE_WRK_ON_BANKHOL'
    end
    object TableDetailHOURTYPEBANKHOLLU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPEBANKHOLLU'
      LookupDataSet = TableHourtypeBankHol
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_WRK_ON_BANKHOL'
      Size = 30
      Lookup = True
    end
    object TableDetailMAX_PTO_MINUTE: TIntegerField
      FieldName = 'MAX_PTO_MINUTE'
    end
    object TableDetailMAX_PTO_MINUTE_CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'MAX_PTO_MINUTE_CALC'
      Size = 25
      Calculated = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Top = 116
  end
  object TableHourtype: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 96
    Top = 192
    object TableHourtypeHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableHourtypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourtypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableHourtypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableHourtypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableHourtypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableHourtypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableHourtypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableHourtypeBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
  object TableAbsenceReason: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'ABSENCEREASON'
    Left = 96
    Top = 248
    object TableAbsenceReasonABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
      Required = True
    end
    object TableAbsenceReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceReasonCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableAbsenceReasonMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableAbsenceReasonMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableAbsenceReasonPAYED_YN: TStringField
      FieldName = 'PAYED_YN'
      Required = True
      Size = 1
    end
  end
  object qryCheckHourTypeTFT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.HOURTYPE_NUMBER'
      'FROM HOURTYPE T'
      'WHERE T.TIME_FOR_TIME_YN = '#39'Y'#39' OR'
      'T.BONUS_IN_MONEY_YN = '#39'Y'#39
      ' ')
    Left = 96
    Top = 312
  end
  object TableHourtypeBankHol: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 208
    Top = 192
    object IntegerField1: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField1: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField2: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField3: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField4: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object StringField5: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object FloatField1: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
end
