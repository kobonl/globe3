inherited ReportStaffPlanningCondDM: TReportStaffPlanningCondDM
  OldCreateOrder = True
  Left = 266
  Top = 193
  Height = 479
  Width = 741
  object QueryEMP: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEMPFilterRecord
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE,'
      '  A.SHIFT_NUMBER,'
      '  A.WORKSPOT_CODE,'
      '  A.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  '#39'000'#39' AS TB,'
      '  '#39'000000000000000000000000000000'#39' AS TBDESC,'
      '  '#39'0'#39' AS FLAG'
      'FROM'
      '  EMPLOYEEPLANNING A INNER JOIN EMPLOYEE E ON'
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      ''
      ''
      ' '
      ' ')
    Left = 56
    Top = 32
  end
  object TableSTO: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'STANDARDOCCUPATION'
    Left = 232
    Top = 120
  end
  object QueryExec: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 464
    Top = 248
  end
  object QueryDept: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DEPARTMENT_CODE, '
      '  DESCRIPTION '
      'FROM '
      '  DEPARTMENT'
      'ORDER BY '
      '  PLANT_CODE, DEPARTMENT_CODE')
    Left = 296
    Top = 32
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER,  DESCRIPTION '
      'FROM '
      '  SHIFT'
      'ORDER BY '
      '  PLANT_CODE, SHIFT_NUMBER ')
    Left = 376
    Top = 32
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 456
    Top = 32
  end
  object QueryWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, '
      '  DESCRIPTION '
      'FROM '
      '  WORKSPOT'
      'ORDER BY '
      '  PLANT_CODE, WORKSPOT_CODE')
    Left = 232
    Top = 32
  end
  object QueryEMPPLN: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER, EMPLOYEEPLANNING_DATE, EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4 '
      'FROM '
      '  EMPLOYEEPLANNING '
      'WHERE'
      '  EMPLOYEEPLANNING_DATE >= :DATEMIN AND '
      '  EMPLOYEEPLANNING_DATE <= :DATEMAX  AND'
      '( (SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) )'
      'ORDER BY '
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER')
    Left = 64
    Top = 200
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object cdsEmpPln: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'CDSNUMBER'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 304
    object cdsEmpPlnCDSNumber: TIntegerField
      FieldName = 'CDSNUMBER'
    end
    object cdsEmpPlnNumber: TIntegerField
      FieldName = 'NUMBER'
    end
    object cdsEmpPlnDay1EmpNr: TStringField
      FieldName = 'DAY1EMPNR'
    end
    object cdsEmpPlnDay1EmpName: TStringField
      FieldName = 'DAY1EMPNAME'
    end
    object cdsEmpPlnDay2EmpName: TStringField
      FieldName = 'DAY2EMPNAME'
    end
    object cdsEmpPlnDay2EmpNr: TStringField
      FieldName = 'DAY2EMPNR'
    end
    object cdsEmpPlnDay3EmpNr: TStringField
      FieldName = 'DAY3EMPNR'
    end
    object cdsEmpPlnDay3EmpName: TStringField
      FieldName = 'DAY3EMPNAME'
    end
    object cdsEmpPlnDay4EmpNr: TStringField
      FieldName = 'DAY4EMPNR'
    end
    object cdsEmpPlnDay4EmpName: TStringField
      FieldName = 'DAY4EMPNAME'
    end
    object cdsEmpPlnDay5EmpNr: TStringField
      FieldName = 'DAY5EMPNR'
    end
    object cdsEmpPlnDay5EmpName: TStringField
      FieldName = 'DAY5EMPNAME'
    end
    object cdsEmpPlnDay6EmpNr: TStringField
      FieldName = 'DAY6EMPNR'
    end
    object cdsEmpPlnDay6EmpName: TStringField
      FieldName = 'DAY6EMPNAME'
    end
    object cdsEmpPlnDay7EmpNr: TStringField
      FieldName = 'DAY7EMPNR'
    end
    object cdsEmpPlnDay7EmpName: TStringField
      FieldName = 'DAY7EMPNAME'
    end
    object cdsEmpPlnTB: TStringField
      FieldName = 'TB'
      Size = 30
    end
    object cdsEmpPlnTBDESC: TStringField
      FieldName = 'TBDESC'
      Size = 30
    end
  end
end
