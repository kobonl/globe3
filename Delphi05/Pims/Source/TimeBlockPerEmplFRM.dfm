inherited TimeBlockPerEmplF: TTimeBlockPerEmplF
  Left = 213
  Top = 174
  Width = 788
  Height = 602
  Caption = 'Timeblocks per employee'
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 209
    Width = 780
    Height = 120
    Align = alBottom
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 117
      Width = 778
      Height = 2
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 778
      Height = 116
      Bands = <
        item
          Caption = 'Plants'
        end
        item
          Caption = 'Shifts'
          Width = 243
        end
        item
          Caption = 'Monday'
          Width = 74
        end
        item
          Caption = 'Tuesday'
          Width = 76
        end
        item
          Caption = 'Wednesday'
          Width = 74
        end
        item
          Caption = 'Thursday'
          Width = 74
        end
        item
          Caption = 'Friday'
          Width = 74
        end
        item
          Caption = 'Sunday'
          Width = 74
        end
        item
          Caption = 'Saturday'
          Width = 71
        end>
      DefaultLayout = False
      KeyField = 'SHIFT_NUMBER'
      DataSource = TimeBlockPerEmplDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnPLANT_CODE: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumnPLANTLU: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Shift number'
        DisableEditor = True
        Width = 73
        BandIndex = 1
        RowIndex = 0
        FieldName = 'SHIFT_NUMBER'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Shift name'
        DisableEditor = True
        Width = 170
        BandIndex = 1
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 38
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 38
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 37
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 36
        BandIndex = 8
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 35
        BandIndex = 8
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 441
    Width = 780
    Height = 127
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 778
      Height = 125
      Align = alClient
      Caption = 'Times per Day'
      TabOrder = 0
      object Label4: TLabel
        Left = 16
        Top = 63
        Width = 24
        Height = 13
        Caption = 'Start'
      end
      object Label5: TLabel
        Left = 16
        Top = 90
        Width = 18
        Height = 13
        Caption = 'End'
      end
      object LabelMO: TLabel
        Left = 83
        Top = 44
        Width = 38
        Height = 13
        Caption = 'Monday'
      end
      object LabelTU: TLabel
        Left = 157
        Top = 44
        Width = 41
        Height = 13
        Caption = 'Tuesday'
      end
      object LabelWE: TLabel
        Left = 226
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Wednesday'
      end
      object LabelTH: TLabel
        Left = 300
        Top = 44
        Width = 45
        Height = 13
        Caption = 'Thursday'
      end
      object LabelFR: TLabel
        Left = 378
        Top = 44
        Width = 30
        Height = 13
        Caption = 'Friday'
      end
      object LabelSA: TLabel
        Left = 448
        Top = 44
        Width = 44
        Height = 13
        Caption = 'Saterday'
      end
      object LabelSU: TLabel
        Left = 520
        Top = 44
        Width = 36
        Height = 13
        Caption = 'Sunday'
      end
      object Label1: TLabel
        Left = 16
        Top = 19
        Width = 54
        Height = 13
        Caption = 'Timeblocks '
      end
      object dxDBTimeEditST1: TdxDBTimeEdit
        Left = 80
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'STARTTIME1'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET1: TdxDBTimeEdit
        Left = 80
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'ENDTIME1'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST2: TdxDBTimeEdit
        Left = 152
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'STARTTIME2'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET2: TdxDBTimeEdit
        Left = 152
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 5
        DataField = 'ENDTIME2'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST3: TdxDBTimeEdit
        Left = 224
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 6
        DataField = 'STARTTIME3'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET3: TdxDBTimeEdit
        Left = 224
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 7
        DataField = 'ENDTIME3'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST4: TdxDBTimeEdit
        Left = 296
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 8
        DataField = 'STARTTIME4'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET4: TdxDBTimeEdit
        Left = 296
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 9
        DataField = 'ENDTIME4'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST5: TdxDBTimeEdit
        Left = 368
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 10
        DataField = 'STARTTIME5'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET5: TdxDBTimeEdit
        Left = 368
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 11
        DataField = 'ENDTIME5'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST6: TdxDBTimeEdit
        Left = 440
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 12
        DataField = 'STARTTIME6'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET6: TdxDBTimeEdit
        Left = 440
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 13
        DataField = 'ENDTIME6'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST7: TdxDBTimeEdit
        Left = 512
        Top = 64
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 14
        DataField = 'STARTTIME7'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET7: TdxDBTimeEdit
        Left = 512
        Top = 88
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 15
        DataField = 'ENDTIME7'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object DBEditTimeBlock: TDBEdit
        Tag = 1
        Left = 80
        Top = 16
        Width = 65
        Height = 19
        Ctl3D = False
        DataField = 'TIMEBLOCK_NUMBER'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditDesc: TDBEdit
        Tag = 1
        Left = 160
        Top = 16
        Width = 153
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = TimeBlockPerEmplDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 1
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 329
    Width = 780
    Height = 112
    Align = alBottom
    inherited spltDetail: TSplitter
      Top = 108
      Width = 778
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 778
      Height = 107
      Bands = <
        item
          Caption = 'Timeblocks per employee'
        end
        item
          Caption = 'Monday'
          Width = 66
        end
        item
          Caption = 'Tuesday'
          Width = 80
        end
        item
          Caption = 'Wednesday'
          Width = 70
        end
        item
          Caption = 'Thursday'
          Width = 75
        end
        item
          Caption = 'Friday'
          Width = 67
        end
        item
          Caption = 'Saturday'
          Width = 69
        end
        item
          Caption = 'Sunday'
          Width = 74
        end>
      DefaultLayout = False
      KeyField = 'TIMEBLOCK_NUMBER'
      DataSource = TimeBlockPerEmplDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMEBLOCK_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
  end
  object PanelEmpl: TPanel [4]
    Left = 0
    Top = 26
    Width = 780
    Height = 183
    Align = alClient
    Caption = 'PanelEmpl'
    TabOrder = 7
    object Splitter1: TSplitter
      Left = 1
      Top = 180
      Width = 778
      Height = 2
      Cursor = crVSplit
      Align = alBottom
      AutoSnap = False
      MinSize = 100
    end
    object dxDBGridEmpl: TdxDBGrid
      Left = 1
      Top = 1
      Width = 778
      Height = 179
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Employees'
        end>
      DefaultLayout = False
      HeaderPanelRowCount = 1
      KeyField = 'EMPLOYEE_NUMBER'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      OnClick = dxDBGridEmplClick
      OnEnter = dxGridEnter
      DataSource = TimeBlockPerEmplDM.DataSourceEmpl
      HideSelectionTextColor = clWindowText
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCanNavigation, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
      ShowBands = True
      OnBackgroundDrawEvent = dxGridBackgroundDrawEvent
      OnChangeNode = dxDBGridEmplChangeNode
      OnCustomDrawBand = dxGridCustomDrawBand
      OnCustomDrawCell = dxGridCustomDrawCell
      OnCustomDrawColumnHeader = dxGridCustomDrawColumnHeader
      OnEndColumnsCustomizing = dxGridEndColumnsCustomizing
      object dxDBGridEmplColumn4: TdxDBGridLookupColumn
        Caption = 'Employee number'
        DisableEditor = True
        Width = 78
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDBGridEmplColumn5: TdxDBGridColumn
        Caption = 'Employee name'
        DisableEditor = True
        Width = 203
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDBGridEmplColumn8: TdxDBGridColumn
        Caption = 'Plant code'
        Width = 61
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxDBGridEmplColumn6: TdxDBGridColumn
        Caption = 'Plant  description'
        Width = 178
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDBGridEmplColumn7: TdxDBGridColumn
        Caption = 'Department'
        Width = 84
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENT_CODE'
      end
      object dxDBGridColumn3: TdxDBGridColumn
        Caption = 'Address'
        DisableEditor = True
        Width = 156
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 520
    Top = 72
  end
end
