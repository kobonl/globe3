(*
  MRA:21-OCT-2013 20014715
  - New Report Incentive Program (WMU)
  MRA:27-NOV-2013 20014715 Rework
  - It goes wrong when on 1 day scans are made on different departments,
    because it uses the department to see what first/last scan is.
    To solve this, do not look at department when looking for first/last scan.
  - query 'qryFirstLastScans' changed (do not look for plant/department).
*)
unit ReportIncentiveProgramDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, SystemDMT;

type
  TReportIncentiveProgramDM = class(TReportBaseDM)
    qryEmployee: TQuery;
    qryAbsence: TQuery;
    qryEarlyLateXXX: TQuery;
    qryEarlyLateOLD: TQuery;
    qryShiftXXX: TQuery;
    qryShiftLoop: TQuery;
    qryFirstLastScans: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportIncentiveProgramDM: TReportIncentiveProgramDM;

implementation

{$R *.DFM}

end.
