inherited PlantF: TPlantF
  Left = 307
  Top = 207
  Width = 636
  Height = 507
  Caption = 'Plants'
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 620
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 618
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 618
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 273
    Width = 620
    Height = 195
    OnEnter = pnlDetailEnter
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 618
      Height = 193
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'General'
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 318
          Height = 165
          Align = alLeft
          Caption = 'Plants'
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 20
            Width = 25
            Height = 13
            Caption = 'Code'
          end
          object Label3: TLabel
            Left = 8
            Top = 44
            Width = 27
            Height = 13
            Caption = 'Name'
          end
          object Label4: TLabel
            Left = 8
            Top = 140
            Width = 26
            Height = 13
            Caption = 'State'
            Visible = False
          end
          object Label5: TLabel
            Left = 8
            Top = 92
            Width = 62
            Height = 13
            Caption = 'ZipCode/City'
          end
          object Label2: TLabel
            Left = 8
            Top = 68
            Width = 39
            Height = 13
            Caption = 'Address'
          end
          object LabelCountry: TLabel
            Left = 8
            Top = 115
            Width = 47
            Height = 13
            Caption = 'Count(r)y'
          end
          object Label10: TLabel
            Left = 8
            Top = 140
            Width = 49
            Height = 13
            Caption = 'Time Zone'
          end
          object DBEditPlant: TDBEdit
            Tag = 1
            Left = 83
            Top = 15
            Width = 86
            Height = 19
            Ctl3D = False
            DataField = 'PLANT_CODE'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
          end
          object DBEditName: TDBEdit
            Tag = 1
            Left = 83
            Top = 39
            Width = 230
            Height = 19
            Ctl3D = False
            DataField = 'DESCRIPTION'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 1
          end
          object DBEditAddress: TDBEdit
            Left = 83
            Top = 64
            Width = 230
            Height = 19
            Ctl3D = False
            DataField = 'ADDRESS'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 2
          end
          object DBEditCity: TDBEdit
            Left = 152
            Top = 88
            Width = 161
            Height = 19
            Ctl3D = False
            DataField = 'CITY'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 4
          end
          object DBEditState: TDBEdit
            Left = 83
            Top = 136
            Width = 126
            Height = 19
            Ctl3D = False
            DataField = 'STATE'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 6
            Visible = False
          end
          object DBEditZip: TDBEdit
            Left = 83
            Top = 88
            Width = 62
            Height = 19
            Ctl3D = False
            DataField = 'ZIPCODE'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 3
          end
          object dxDBLookupEditCountry: TdxDBLookupEdit
            Tag = 1
            Left = 82
            Top = 111
            Width = 127
            Style.BorderStyle = xbsSingle
            TabOrder = 5
            DataField = 'COUNTRYLU'
            DataSource = PlantDM.DataSourceDetail
            DropDownRows = 3
            DropDownWidth = 230
            ListFieldName = 'DESCRIPTION;CODE'
          end
          object dxDBLookupEditTimeZone: TdxDBLookupEdit
            Left = 82
            Top = 135
            Width = 230
            Style.BorderStyle = xbsSingle
            TabOrder = 7
            DataField = 'TIMEZONELU'
            DataSource = PlantDM.DataSourceDetail
            DropDownRows = 6
            DropDownWidth = 400
            ListFieldName = 'TIMEZONE'
            CanDeleteText = True
          end
        end
        object GroupBox2: TGroupBox
          Left = 318
          Top = 0
          Width = 292
          Height = 165
          Align = alClient
          TabOrder = 1
          object Label6: TLabel
            Left = 8
            Top = 20
            Width = 30
            Height = 13
            Caption = 'Phone'
          end
          object Label7: TLabel
            Left = 8
            Top = 40
            Width = 18
            Height = 13
            Caption = 'Fax'
          end
          object DBEditPhone: TDBEdit
            Left = 59
            Top = 15
            Width = 118
            Height = 19
            Ctl3D = False
            DataField = 'PHONE'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
          end
          object DBEditFax: TDBEdit
            Left = 59
            Top = 39
            Width = 118
            Height = 19
            Ctl3D = False
            DataField = 'FAX'
            DataSource = PlantDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 1
          end
          object GroupBox3: TGroupBox
            Left = 8
            Top = 60
            Width = 137
            Height = 65
            Caption = 'Inscan margin minutes'
            TabOrder = 2
            object Label11: TLabel
              Left = 8
              Top = 16
              Width = 24
              Height = 13
              Caption = 'Early'
            end
            object Label12: TLabel
              Left = 8
              Top = 40
              Width = 21
              Height = 13
              Caption = 'Late'
            end
            object DBEditInscanEarly: TDBEdit
              Tag = 1
              Left = 51
              Top = 14
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'INSCAN_MARGIN_EARLY'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 0
            end
            object DBEditInscanLate: TDBEdit
              Tag = 1
              Left = 51
              Top = 38
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'INSCAN_MARGIN_LATE'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 1
            end
          end
          object GroupBox4: TGroupBox
            Left = 152
            Top = 60
            Width = 145
            Height = 65
            Caption = 'Outscan margin minutes'
            TabOrder = 3
            object Label8: TLabel
              Left = 16
              Top = 16
              Width = 24
              Height = 13
              Caption = 'Early'
            end
            object Label9: TLabel
              Left = 16
              Top = 40
              Width = 21
              Height = 13
              Caption = 'Late'
            end
            object DBEditOutscanEarly: TDBEdit
              Tag = 1
              Left = 59
              Top = 14
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'OUTSCAN_MARGIN_EARLY'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 0
            end
            object DBEditOutscanLate: TDBEdit
              Tag = 1
              Left = 59
              Top = 38
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'OUTSCAN_MARGIN_LATE'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 1
            end
          end
          object GroupBox5: TGroupBox
            Left = 8
            Top = 124
            Width = 137
            Height = 36
            Caption = 'Average wage'
            TabOrder = 4
            object DBEdit1: TDBEdit
              Left = 51
              Top = 13
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'AVERAGE_WAGE'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 0
            end
          end
          object GroupBox6: TGroupBox
            Left = 152
            Top = 124
            Width = 146
            Height = 35
            Caption = 'Customer Number'
            TabOrder = 5
            object DBEdit2: TDBEdit
              Left = 59
              Top = 13
              Width = 70
              Height = 19
              Ctl3D = False
              DataField = 'CUSTOMER_NUMBER'
              DataSource = PlantDM.DataSourceDetail
              MaxLength = 6
              ParentCtl3D = False
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Additional'
        ImageIndex = 1
        object GroupBox7: TGroupBox
          Left = 0
          Top = 0
          Width = 321
          Height = 165
          Align = alLeft
          TabOrder = 0
          object GroupBox8: TGroupBox
            Left = 2
            Top = 144
            Width = 317
            Height = 19
            Caption = 'Final Run System'
            TabOrder = 0
            object btnFinalRunInfo: TButton
              Left = 8
              Top = 17
              Width = 300
              Height = 25
              Caption = 'Final Run Info'
              TabOrder = 0
              OnClick = btnFinalRunInfoClick
            end
          end
          object GroupBox9: TGroupBox
            Left = 2
            Top = 15
            Width = 317
            Height = 82
            Align = alTop
            Caption = 'Real Time Efficiency'
            TabOrder = 1
            object Label13: TLabel
              Left = 8
              Top = 24
              Width = 106
              Height = 13
              Caption = 'Cut off time shift date'
            end
            object Label14: TLabel
              Left = 8
              Top = 48
              Width = 115
              Height = 13
              Caption = 'Real time current period'
            end
            object Label15: TLabel
              Left = 256
              Top = 24
              Width = 32
              Height = 13
              Caption = 'hh:mm'
            end
            object Label16: TLabel
              Left = 256
              Top = 48
              Width = 37
              Height = 13
              Caption = 'minutes'
            end
            object dxDBTimeEdit1: TdxDBTimeEdit
              Left = 176
              Top = 21
              Width = 73
              TabOrder = 0
              DataField = 'CUTOFFTIMESHIFTDATE'
              DataSource = PlantDM.DataSourceDetail
              TimeEditFormat = tfHourMin
              StoredValues = 4
            end
            object dxDBSpinEdit1: TdxDBSpinEdit
              Left = 176
              Top = 46
              Width = 73
              TabOrder = 1
              DataField = 'REALTIMEINTERVAL'
              DataSource = PlantDM.DataSourceDetail
              StoredValues = 48
            end
          end
          object GroupBox10: TGroupBox
            Left = 2
            Top = 97
            Width = 317
            Height = 66
            Align = alClient
            Caption = 'Staff Planning Dialog'
            TabOrder = 2
            object Label17: TLabel
              Left = 8
              Top = 24
              Width = 58
              Height = 13
              Caption = 'Show Totals'
            end
            object DBCheckBox1: TDBCheckBox
              Left = 176
              Top = 24
              Width = 97
              Height = 17
              DataField = 'STAFFPLAN_SHOWTOT_YN'
              DataSource = PlantDM.DataSourceDetail
              TabOrder = 0
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
          end
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Tag = 1
    Width = 620
    Height = 118
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 115
      Width = 618
      Height = 2
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 618
      Height = 114
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants '
        end>
      DefaultLayout = False
      KeyField = 'PLANT_CODE'
      Color = clWhite
      BandColor = clInactiveBorder
      DataSource = PlantDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        Width = 63
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridColumn
        Caption = 'Address'
        Width = 108
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Zipcode'
        Width = 58
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ZIPCODE'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Caption = 'City'
        Width = 261
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Caption = 'Fax'
        MinWidth = 0
        Visible = False
        Width = 68
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FAX'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Caption = 'Phone'
        MinWidth = 0
        Visible = False
        Width = 68
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
      object dxDetailGridColumn8: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Inscan margin early'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        Width = 59
        BandIndex = 0
        RowIndex = 0
        FieldName = 'INSCAN_MARGIN_EARLY'
      end
      object dxDetailGridColumn9: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Inscan margin late'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        Width = 59
        BandIndex = 0
        RowIndex = 0
        FieldName = 'INSCAN_MARGIN_LATE'
      end
      object dxDetailGridColumn10: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Outscan margin early'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        Width = 59
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OUTSCAN_MARGIN_EARLY'
      end
      object dxDetailGridColumn11: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Outscan margin late'
        MaxLength = 10
        MinWidth = 0
        Visible = False
        Width = 59
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OUTSCAN_MARGIN_LATE'
      end
      object dxDetailGridColumn12: TdxDBGridColumn
        Caption = 'Average wage'
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AVERAGE_WAGE'
      end
      object dxDetailGridColumn13: TdxDBGridColumn
        Caption = 'Timezone'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMEZONECALC'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
  end
end
