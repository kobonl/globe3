(*
  MRA:17-NOV-2015 PIM-23
  - New Report Employee Info and Deviations (per Period).
*)
unit ReportEmpInfoDevPeriodDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportEmpInfoDevPeriodDM = class(TReportBaseDM)
    qryEmpInfo: TQuery;
    qryPivotDayMerge: TQuery;
    qryEmpHeader: TQuery;
    qryEmpAbsence: TQuery;
    qryEmpBalance: TQuery;
    qryEmpShift: TQuery;
    qryContractInfo: TQuery;
    qryOvertimeDef: TQuery;
    qryExceptHourDef: TQuery;
    qryPlantShiftDept: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportEmpInfoDevPeriodDM: TReportEmpInfoDevPeriodDM;

implementation

{$R *.DFM}

end.
