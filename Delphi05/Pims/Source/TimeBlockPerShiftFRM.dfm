inherited TimeBlockPerShiftF: TTimeBlockPerShiftF
  Left = 232
  Top = 151
  Height = 510
  Caption = 'Timeblocks per shift'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Height = 147
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 143
    end
    inherited dxMasterGrid: TdxDBGrid
      Height = 142
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plant - Shifts'
          Width = 224
        end
        item
          Caption = 'Monday'
          Width = 55
        end
        item
          Caption = 'Tuesday'
          Width = 65
        end
        item
          Caption = 'Wednesday'
          Width = 69
        end
        item
          Caption = 'Thursday'
          Width = 59
        end
        item
          Caption = 'Friday'
          Width = 52
        end
        item
          Caption = 'Sunday'
          Width = 64
        end
        item
          Caption = 'Saturday'
          Width = 70
        end>
      DefaultLayout = False
      KeyField = 'SHIFT_NUMBER'
      ShowBands = True
      object dxMasterGridColumn17: TdxDBGridColumn
        Caption = 'Plant code'
        DisableEditor = True
        Width = 80
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumn18: TdxDBGridLookupColumn
        Caption = 'Plant name'
        DisableEditor = True
        Width = 130
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Shift number'
        DisableEditor = True
        Width = 79
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHIFT_NUMBER'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Shift name'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 34
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 34
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 34
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 35
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 31
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 38
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 34
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 36
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 32
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 37
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 34
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 35
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 31
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxMasterGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 53
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 307
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 69
      Align = alTop
      Caption = 'Shifts'
      TabOrder = 0
      object Label1: TLabel
        Left = 24
        Top = 20
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label2: TLabel
        Left = 24
        Top = 43
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object Label3: TLabel
        Left = 312
        Top = 43
        Width = 54
        Height = 13
        Caption = 'Timeblocks '
      end
      object DBEditPlant: TDBEdit
        Tag = 1
        Left = 65
        Top = 17
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = TimeBlockPerShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditPlantName: TDBEdit
        Tag = 1
        Left = 137
        Top = 17
        Width = 160
        Height = 19
        Ctl3D = False
        DataField = 'PLANTLU'
        DataSource = TimeBlockPerShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 1
      end
      object DBEditShift: TDBEdit
        Tag = 1
        Left = 65
        Top = 41
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'SHIFT_NUMBER'
        DataSource = TimeBlockPerShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 2
      end
      object DBEditShiftName: TDBEdit
        Tag = 1
        Left = 137
        Top = 41
        Width = 160
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = TimeBlockPerShiftDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 3
      end
      object DBEditTimeBlockPerShift: TDBEdit
        Tag = 1
        Left = 377
        Top = 41
        Width = 62
        Height = 19
        Ctl3D = False
        DataField = 'TIMEBLOCK_NUMBER'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 4
      end
      object DBEditTimeDesc: TDBEdit
        Tag = 1
        Left = 449
        Top = 41
        Width = 160
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 5
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 70
      Width = 640
      Height = 94
      Align = alClient
      Caption = 'Times per Day'
      TabOrder = 1
      object Label4: TLabel
        Left = 24
        Top = 34
        Width = 24
        Height = 13
        Caption = 'Start'
      end
      object Label5: TLabel
        Left = 24
        Top = 58
        Width = 18
        Height = 13
        Caption = 'End'
      end
      object LabelMO: TLabel
        Left = 67
        Top = 12
        Width = 38
        Height = 13
        Caption = 'Monday'
      end
      object LabelTU: TLabel
        Left = 141
        Top = 12
        Width = 41
        Height = 13
        Caption = 'Tuesday'
      end
      object LabelWE: TLabel
        Left = 210
        Top = 12
        Width = 57
        Height = 13
        Caption = 'Wednesday'
      end
      object LabelTH: TLabel
        Left = 292
        Top = 12
        Width = 45
        Height = 13
        Caption = 'Thursday'
      end
      object LabelFR: TLabel
        Left = 362
        Top = 12
        Width = 30
        Height = 13
        Caption = 'Friday'
      end
      object LabelSA: TLabel
        Left = 432
        Top = 12
        Width = 44
        Height = 13
        Caption = 'Saterday'
      end
      object LabelSU: TLabel
        Left = 504
        Top = 12
        Width = 36
        Height = 13
        Caption = 'Sunday'
      end
      object dxDBTimeEditST1: TdxDBTimeEdit
        Left = 64
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'STARTTIME1'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET1: TdxDBTimeEdit
        Left = 64
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'ENDTIME1'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST2: TdxDBTimeEdit
        Left = 136
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'STARTTIME2'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET2: TdxDBTimeEdit
        Left = 136
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'ENDTIME2'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST3: TdxDBTimeEdit
        Left = 208
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'STARTTIME3'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET3: TdxDBTimeEdit
        Left = 208
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 5
        DataField = 'ENDTIME3'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST4: TdxDBTimeEdit
        Left = 280
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 6
        DataField = 'STARTTIME4'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET4: TdxDBTimeEdit
        Left = 280
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 7
        DataField = 'ENDTIME4'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST5: TdxDBTimeEdit
        Left = 352
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 8
        DataField = 'STARTTIME5'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET5: TdxDBTimeEdit
        Left = 352
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 9
        DataField = 'ENDTIME5'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST6: TdxDBTimeEdit
        Left = 424
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 10
        DataField = 'STARTTIME6'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET6: TdxDBTimeEdit
        Left = 424
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 11
        DataField = 'ENDTIME6'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditST7: TdxDBTimeEdit
        Left = 496
        Top = 32
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 12
        DataField = 'STARTTIME7'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditET7: TdxDBTimeEdit
        Left = 496
        Top = 56
        Width = 65
        Style.BorderStyle = xbsSingle
        TabOrder = 13
        DataField = 'ENDTIME7'
        DataSource = TimeBlockPerShiftDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 173
    Height = 134
    Align = alBottom
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 130
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 129
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Timeblocks per shift'
        end
        item
          Caption = 'Monday'
          Width = 66
        end
        item
          Caption = 'Tuesday'
          Width = 80
        end
        item
          Caption = 'Wednesday'
          Width = 70
        end
        item
          Caption = 'Thursday'
          Width = 75
        end
        item
          Caption = 'Friday'
          Width = 67
        end
        item
          Caption = 'Saturday'
          Width = 69
        end
        item
          Caption = 'Sunday'
          Width = 74
        end>
      DefaultLayout = False
      KeyField = 'TIMEBLOCK_NUMBER'
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMEBLOCK_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Name'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'STARTTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn4: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 1
        RowIndex = 0
        FieldName = 'ENDTIME1'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn5: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'STARTTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn6: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 2
        RowIndex = 0
        FieldName = 'ENDTIME2'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn7: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'STARTTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn8: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 3
        RowIndex = 0
        FieldName = 'ENDTIME3'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn9: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'STARTTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn10: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 4
        RowIndex = 0
        FieldName = 'ENDTIME4'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn11: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'STARTTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn12: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 5
        RowIndex = 0
        FieldName = 'ENDTIME5'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn13: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'STARTTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn14: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 6
        RowIndex = 0
        FieldName = 'ENDTIME6'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn15: TdxDBGridTimeColumn
        Caption = 'Start'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'STARTTIME7'
        TimeEditFormat = tfHourMin
      end
      object dxDetailGridColumn16: TdxDBGridTimeColumn
        Caption = 'End'
        Width = 39
        BandIndex = 7
        RowIndex = 0
        FieldName = 'ENDTIME7'
        TimeEditFormat = tfHourMin
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
