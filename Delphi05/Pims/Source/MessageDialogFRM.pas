(*
  MRA:21-APR-2011. Creation. RV089.1.
  - Shows a MessageDialog with 4 buttons: Yes, No, All, No To All.
*)
unit MessageDialogFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TMessageDialogF = class(TForm)
    btnYes: TButton;
    lblMessage: TLabel;
    btnNo: TButton;
    btnAll: TButton;
    btnNoToAll: TButton;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FMsg: String;
  public
    { Public declarations }
    property Msg: String read FMsg write FMsg;
  end;

var
  MessageDialogF: TMessageDialogF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes;

procedure TMessageDialogF.FormShow(Sender: TObject);
begin
  lblMessage.Caption := Msg;
end;

procedure TMessageDialogF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TMessageDialogF.FormCreate(Sender: TObject);
begin
  btnYes.Caption := SPimsYes;
  btnNo.Caption := SPimsNo;
  btnAll.Caption := SPimsAll;
  btnNoToAll.Caption := SPimsNoToAll;
end;

end.
