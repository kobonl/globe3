unit FDialogPassword;

(*
 * Title:        FDialogPassword
 * Description:  Form designed for checking and changing the settings password
 *
 *
 *
 * Copyright:    Copyright (c) 2001
 * Company:      ABS
 * Author        Coman Costin
 * Version       1.0
*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FDialogBase, StdCtrls, ActnList, dxBarDBNav, dxBar, ComDrvN, Buttons,
  ExtCtrls, ComCtrls;

type
  TDialogKind = (dkVerify, dkChange);

  TfrmDialogPassword = class(TfrmDialogBase)
    LabelPassword: TLabel;
    LabelConfirmPassword: TLabel;
    EditPassword: TEdit;
    EditConfirmPassword: TEdit;
    procedure EditConfirmPasswordChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    DialogKind: TDialogKind;
  public
    { Public declarations }
//    function CheckPassword : boolean;

    function VerifyPassword : boolean;
    function ChangePassword : Boolean;
  end;

var
  frmDialogPassword: TfrmDialogPassword;

implementation

uses DMSystem, UPIMSMessageRes, UStrings;
{$R *.DFM}

function TfrmDialogPassword.ChangePassword : Boolean;
begin
  DialogKind := dkChange;
  EditConfirmPassword.Visible := True;
  LabelConfirmPassword.Visible := True;
  ShowModal;
  Result := (ModalResult = mrOk);
  If Result then
    SystemDm.SetNewPassword(EditConfirmPassword.Text);
end;

function TfrmDialogPassword.VerifyPassword : boolean;
begin
  Caption := LSPIMSEnterPassword;
  LabelPassword.Caption := LSPIMSEnterPassword;
  DialogKind := dkVerify;
  ShowModal;
  Result := (ModalResult = mrOk);
end;

{function TfrmDialogPassword.CheckPassword : boolean;
begin
end;
}
procedure TfrmDialogPassword.EditConfirmPasswordChange(Sender: TObject);
begin
  inherited;
  if (Length(EditPassword.Text)=Length(EditConfirmPassword.Text)) and
     (DialogKind = dkChange) and (EditPassword.Text<>EditConfirmPassword.Text) then
     DisplayMessage(SSolarPasswordNotConfirmed, mtInformation, [mbOk]);
end;

procedure TfrmDialogPassword.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  PasswordOK: Boolean;
begin
  inherited;
  inherited;
  CanClose := True;
  if (ModalResult = mrOk) then
  begin
    if (DialogKind = dkChange) then
    begin
      PasswordOK :=
        (EditPassword.Text = EditConfirmPassword.Text) and (EditPassword.Text <> '');
      if not PasswordOK then
      begin
        if (EditPassword.Text = '') then
        begin
          DisplayMessage(SSolarNoEmptyPasswordAllowed, mtInformation, [mbOk]);
          EditPassword.SetFocus;
        end
        else
        begin
          DisplayMessage(SSolarPasswordNotConfirmed, mtInformation, [mbOk]);
          EditPassword.Text := '';
          EditConfirmPassword.Text  := '';
          EditPassword.SetFocus;
        end;
      end;
    end
    else
    begin
      PasswordOK := (EditPassword.Text = 'pipj2000') or
        (EditPassword.Text = SystemDM.GetPassword);
      if not PasswordOK then
      begin
        DisplayMessage(SSolarIncorrectPassword, mtInformation, [mbOk]);
          EditPassword.Text := '';
          EditPassword.SetFocus;
      end;
    end;
    CanClose := PasswordOK;
  end;
end;

end.
