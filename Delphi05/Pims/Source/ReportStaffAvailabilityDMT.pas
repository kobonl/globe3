(*
  Changes:
    MRA:7-JAN-2010. RV050.3. 889954.
    - Totals in Report Staff Availability are wrong.
      Reason: Wrong join in QueryEMP with departments.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
*)

unit ReportStaffAvailabilityDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, Provider, DBClient;

type
  TReportStaffAvailabilityDM = class(TReportBaseDM)
    QueryEMA: TQuery;
    QueryEMP: TQuery;
    QueryEMPDEPARTMENT_CODE: TStringField;
    QueryEMPDIRECT_HOUR_YN: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_1: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_2: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_3: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_4: TStringField;
    ClientDataSetSHIFT: TClientDataSet;
    DataSetProviderShift: TDataSetProvider;
    QueryShift: TQuery;
    QuerySHS: TQuery;
    ClientDataSetSHS: TClientDataSet;
    DataSetProviderSHS: TDataSetProvider;
    QueryTBPerEmpl: TQuery;
    ClientDataSetTBPerEmpl: TClientDataSet;
    DataSetProviderTBPerEmpl: TDataSetProvider;
    QueryABS: TQuery;
    ClientDataSetAbs: TClientDataSet;
    DataSetProviderAbs: TDataSetProvider;
    QueryEMPEMPLOYEEPLANNING_DATE: TDateTimeField;
    QueryEMPEMPLOYEE_NUMBER: TIntegerField;
    QueryEMPSHIFT_NUMBER: TIntegerField;
    QueryEMPPLANT_CODE: TStringField;
    TableDept: TTable;
    QueryEMPSCHEDULED_TIMEBLOCK_5: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_6: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_7: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_8: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_9: TStringField;
    QueryEMPSCHEDULED_TIMEBLOCK_10: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEMAFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportStaffAvailabilityDM: TReportStaffAvailabilityDM;

implementation

uses CalculateTotalHoursDMT, SystemDMT;

{$R *.DFM}

procedure TReportStaffAvailabilityDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryEMA.Prepare;
  QuerySHS.Prepare;
  QueryEMP.Prepare;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEMA);
end;

procedure TReportStaffAvailabilityDM.QueryEMAFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  if SystemDM.qryPlantDeptTeam.Active and DataSet.Active then
    Accept := SystemDM.qryPlantDeptTeam.Locate('PLANT_CODE',
      DataSet.FieldByName('EMAPLANT_CODE').AsString, [])
  else
    Accept := True;

end;

end.
