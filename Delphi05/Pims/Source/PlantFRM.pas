(*
  Changes:
    MRA:23-SEP-2009 RV034.1.
      - Added field CUSTOMER_NUMBER (numeric) to PLANT-dialog.
        Only visible/usable with export-payroll-ADP.
    MRA:5-JAN-2010 RV050.1. 889953.
      - Addition of field COUNTRY_ID (refers to COUNTRY-table).
        This can not be left empty.
      - Do not show 'state' anymore. Instead of that show 'country'.
    MRA:11-OCT-2010 RV071.13. 550497 Bugfixing/Changes.
    - Expand Customer Number from 5 to 6 positions (MaxLength of
      edit-box changed).
    MRA:22-FEB-2011 RV087.2. Export Payroll Select/AFAS (SO-20011509/20011510)
    - Export Payroll Select / AFAS
      - An export payroll is needed for Select/AFAS
      - For export type Select / AFAS it is also necessary to show the
        Customernumber-field.
    JVL:14-APR-2011. RV089. SO-550532
    - Memorize selected plant
    MRA:6-FEB-2014 20011800 Final Run System
    - Addition of Final Run Info
    - Added tab-page Additional that can show Final Run Info.
    MRA:28-APR-2014 20011800.70 Rework
    - Final Run System: Use Form_Edit_YN-value from Plant (parent) for
      PlantFinalRun-form.
    MRA:31-OCT-2014 20011800.80
    - Disable final run system
    MRA:7-APR-2015 SO-20016449
    - Time Zone Implementation
    MRA:30-APR-2015 SO-20014450
    - Addition of two extra fields:
      - Cut off time shift date (hh:mm)
      - Real time current period (minutes)
*)

unit PlantFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Mask, DBCtrls, DBTables, dxEditor,
  dxExEdtr, dxDBEdtr, dxDBELib, ComCtrls, dxEdLib;

const
  ValidKeys  : set of Char = [#08, #13] + [#48..#57];
type

  TPlantF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    dxDetailGridColumn8: TdxDBGridColumn;
    dxDetailGridColumn9: TdxDBGridColumn;
    dxDetailGridColumn10: TdxDBGridColumn;
    dxDetailGridColumn11: TdxDBGridColumn;
    dxDetailGridColumn12: TdxDBGridColumn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    LabelCountry: TLabel;
    DBEditPlant: TDBEdit;
    DBEditName: TDBEdit;
    DBEditAddress: TDBEdit;
    DBEditCity: TDBEdit;
    DBEditState: TDBEdit;
    DBEditZip: TDBEdit;
    dxDBLookupEditCountry: TdxDBLookupEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    DBEditPhone: TDBEdit;
    DBEditFax: TDBEdit;
    GroupBox3: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    DBEditInscanEarly: TDBEdit;
    DBEditInscanLate: TDBEdit;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    DBEditOutscanEarly: TDBEdit;
    DBEditOutscanLate: TDBEdit;
    GroupBox5: TGroupBox;
    DBEdit1: TDBEdit;
    GroupBox6: TGroupBox;
    DBEdit2: TDBEdit;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    btnFinalRunInfo: TButton;
    Label10: TLabel;
    dxDBLookupEditTimeZone: TdxDBLookupEdit;
    dxDetailGridColumn13: TdxDBGridColumn;
    GroupBox9: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    dxDBTimeEdit1: TdxDBTimeEdit;
    dxDBSpinEdit1: TdxDBSpinEdit;
    Label15: TLabel;
    Label16: TLabel;
    GroupBox10: TGroupBox;
    Label17: TLabel;
    DBCheckBox1: TDBCheckBox;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure btnFinalRunInfoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    InsertForm: TForm;
  public
    { Public declarations }
  end;

function PlantF: TPlantF;

implementation

{$R *.DFM}

uses
  SystemDMT, PlantDMT, PlantFinalRunFRM;

var
  PlantF_HND: TPlantF;

function PlantF: TPlantF;
begin
  if (PlantF_HND = nil) then
    PlantF_HND := TPlantF.Create(Application);
  Result := PlantF_HND;
end;

procedure TPlantF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePageIndex = 0 then
    DBEditPlant.SetFocus;
end;

procedure TPlantF.FormDestroy(Sender: TObject);
begin
  inherited;
  PlantF_HND := nil;
end;

procedure TPlantF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  if PageControl1.ActivePageIndex = 0 then
    DBEditPlant.SetFocus;
end;

procedure TPlantF.FormCreate(Sender: TObject);
begin
  PlantDM := CreateFormDM(TPlantDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := PlantDM.DataSourceDetail;
  inherited;
  // RV034.1.
  GroupBox6.Visible := (SystemDM.ExportPayrollType = 'ADP') or
    (SystemDM.ExportPayrollType = 'AFAS') or
    (SystemDM.ExportPayrollType = 'SELECT');
  PageControl1.ActivePageIndex := 0; // 20011800
  btnFinalRunInfo.Enabled := SystemDM.UseFinalRun; // 20011800
  // 20011800.80
  if not SystemDM.UseFinalRun then
  begin
    // PageControl1.Pages[1].TabVisible := False; // Make tab invisible
    GroupBox8.Visible := False;
  end;
  // 20016449 Optionally disable time zone part
  if SystemDM.DatabaseTimeZone = '' then
  begin
    Label10.Visible := False;
    dxDBLookupEditTimeZone.Visible := False;
  end;
end;

procedure TPlantF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;

  SystemDM.ASaveTimeRecScanning.Plant :=
      PlantDM.TableDetail.FieldByName('PLANT_CODE').AsString;
end;

procedure TPlantF.FormShow(Sender: TObject);
begin
  inherited;

  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      PlantDM.TableDetail.FieldByName('PLANT_CODE').AsString
  else
    PlantDM.TableDetail.FindKey([SystemDM.ASaveTimeRecScanning.Plant]);
  // 20011800 Related to this order
  // Make it possible to go to the tab-pages.
  if not pnlDetail.Enabled then
  begin
    pnlDetail.Enabled := True;
    GroupBox1.Enabled := False;
    GroupBox2.Enabled := False;
  end;
end;

procedure TPlantF.btnFinalRunInfoClick(Sender: TObject);
begin
  inherited;
  InsertForm := PlantFinalRunF;
  PlantFinalRunF.Form_Edit_YN := Form_Edit_YN; // 20011800.70 Use this from parent.
  InsertForm.Show;
end;

procedure TPlantF.FormActivate(Sender: TObject);
begin
  inherited;
  if InsertForm <> Nil then
    if (InsertForm.Visible) then
      InsertForm.Show;
end;

end.
