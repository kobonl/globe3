inherited IllnessMessagesF: TIllnessMessagesF
  Left = 258
  Top = 140
  Width = 730
  Caption = 'Illness messages'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 105
    Width = 722
    Height = 304
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 300
      Width = 720
    end
    inherited dxMasterGrid: TdxDBGrid
      Top = 28
      Height = 267
      DefaultLayout = False
      Align = alNone
      ShowBands = True
    end
  end
  inherited pnlDetailGrid: TPanel [2]
    Top = 105
    Width = 722
    Height = 304
    inherited spltDetail: TSplitter
      Top = 300
      Width = 720
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 720
      Height = 299
      KeyField = 'ILLNESSMESSAGE_STARTDATE'
      OnClick = dxDetailGridClick
      DataSource = IllnessMessagesDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnStartdate: TdxDBGridDateColumn
        Caption = 'Start date'
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ILLNESSMESSAGE_STARTDATE'
        SaveTime = False
        UseEditMask = True
      end
      object dxDetailGridColumnEndDate: TdxDBGridDateColumn
        Caption = 'End Date'
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ILLNESSMESSAGE_ENDDATE'
        SaveTime = False
        UseEditMask = True
      end
      object dxDetailGridColumnEmployee: TdxDBGridLookupColumn
        Caption = 'Employee'
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEELU'
        ListFieldName = 'EMPLOYEE_NUMBER;DESCRIPTION'
      end
      object dxDetailGridColumnAbsencereason: TdxDBGridLookupColumn
        Caption = 'Absence reason'
        Width = 124
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCEREASONLU'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnLineNumber: TdxDBGridColumn
        Caption = 'Number'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LINE_NUMBER'
      end
    end
  end
  inherited pnlDetail: TPanel [3]
    Top = 409
    Width = 722
    Height = 155
    TabOrder = 3
    object GroupBoxDetailIllness: TGroupBox
      Left = 1
      Top = 1
      Width = 720
      Height = 153
      Align = alClient
      Caption = 'Illness messages'
      TabOrder = 0
      object GroupBoxDetailEmployee: TGroupBox
        Left = 2
        Top = 15
        Width = 716
        Height = 40
        Align = alTop
        TabOrder = 0
        object LabelDetailEmployee: TLabel
          Left = 8
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Employee'
        end
        object DBLookupComboBoxEmployee: TDBLookupComboBox
          Tag = 1
          Left = 76
          Top = 12
          Width = 245
          Height = 21
          DataField = 'EMPLOYEELU'
          DataSource = IllnessMessagesDM.DataSourceDetail
          ListField = 'EMPLOYEE_NUMBER;DESCRIPTION'
          TabOrder = 0
          OnCloseUp = DBLookupComboBoxEmployeeCloseUp
        end
      end
      object GroupBoxDetailDate: TGroupBox
        Left = 2
        Top = 55
        Width = 235
        Height = 96
        Align = alLeft
        Caption = 'Date'
        TabOrder = 1
        object LabelStartDate: TLabel
          Left = 12
          Top = 16
          Width = 50
          Height = 13
          Caption = 'Start Date'
        end
        object LabelEndDate: TLabel
          Left = 12
          Top = 44
          Width = 44
          Height = 13
          Caption = 'End Date'
        end
        object dxDBDateEditStartdate: TdxDBDateEdit
          Tag = 1
          Left = 76
          Top = 12
          Width = 121
          TabOrder = 0
          DataField = 'ILLNESSMESSAGE_STARTDATE'
          DataSource = IllnessMessagesDM.DataSourceDetail
          DateButtons = [btnToday]
          SaveTime = False
        end
        object dxDBDateEditEndDate: TdxDBDateEdit
          Left = 76
          Top = 40
          Width = 121
          TabOrder = 1
          DataField = 'ILLNESSMESSAGE_ENDDATE'
          DataSource = IllnessMessagesDM.DataSourceDetail
          SaveTime = False
        end
      end
      object GroupBoxDetail: TGroupBox
        Left = 237
        Top = 55
        Width = 481
        Height = 96
        Align = alClient
        TabOrder = 2
        object LabelAbsenceReason: TLabel
          Left = 16
          Top = 16
          Width = 77
          Height = 13
          Caption = 'Absence reason'
        end
        object LabelDescription: TLabel
          Left = 16
          Top = 42
          Width = 53
          Height = 13
          Caption = 'Description'
        end
        object Label1: TLabel
          Left = 16
          Top = 69
          Width = 37
          Height = 13
          Caption = 'Number'
        end
        object DBEditDescription: TDBEdit
          Left = 104
          Top = 38
          Width = 277
          Height = 21
          DataField = 'DESCRIPTION'
          DataSource = IllnessMessagesDM.DataSourceDetail
          TabOrder = 1
        end
        object DBLookupComboBoxAbsenceReason: TDBLookupComboBox
          Tag = 1
          Left = 104
          Top = 12
          Width = 277
          Height = 21
          DataField = 'ABSENCEREASON_CODE'
          DataSource = IllnessMessagesDM.DataSourceDetail
          DropDownRows = 4
          KeyField = 'ABSENCEREASON_CODE'
          ListField = 'ABSENCEREASON_CODE;DESCRIPTION'
          ListSource = IllnessMessagesDM.DataSourceAbsenceReason
          TabOrder = 0
        end
        object dxDBSpinEditLINE_NUMBER: TdxDBSpinEdit
          Left = 104
          Top = 64
          Width = 121
          TabOrder = 2
          DataField = 'LINE_NUMBER'
          DataSource = IllnessMessagesDM.DataSourceDetail
          StoredValues = 16
        end
      end
    end
  end
  object PanelTabs: TPanel [4]
    Left = 0
    Top = 26
    Width = 722
    Height = 79
    Align = alTop
    Caption = 'PanelTabs'
    TabOrder = 7
    object PageControlIllness: TPageControl
      Left = 1
      Top = 1
      Width = 720
      Height = 77
      ActivePage = TabSheetIllnessMessages
      Align = alClient
      TabOrder = 0
      OnChange = PageControlIllnessChange
      object TabSheetOutstanding: TTabSheet
        Caption = 'Outstanding illness messages'
      end
      object TabSheetIllnessMessages: TTabSheet
        Caption = 'Illness messages per employee'
        ImageIndex = 1
        object GroupBoxSelectEmployee: TGroupBox
          Left = 0
          Top = 0
          Width = 712
          Height = 49
          Align = alClient
          Caption = 'Employee / date'
          TabOrder = 0
          object LabelEmployee: TLabel
            Left = 8
            Top = 24
            Width = 46
            Height = 13
            Caption = 'Employee'
          end
          object LabelIllnessMessages: TLabel
            Left = 312
            Top = 24
            Width = 158
            Height = 13
            Caption = 'Illness messages since start date'
          end
          object LabelEmplDesc: TLabel
            Left = 168
            Top = 24
            Width = 70
            Height = 13
            Caption = 'LabelEmplDesc'
          end
          object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
            Left = 64
            Top = 22
            Width = 97
            TabOrder = 0
            DataField = 'EMPLOYEE_NUMBER'
            DataSource = IllnessMessagesDM.DataSourceMaster
            PopupWidth = 500
            OnCloseUp = dxDBExtLookupEditEmployeeCloseUp
            DBGridLayout = dxDBGridLayoutListEmployeeItem
          end
          object DateEditStart: TDateTimePicker
            Left = 480
            Top = 20
            Width = 121
            Height = 21
            CalAlignment = dtaLeft
            Date = 0.544151967595099
            Time = 0.544151967595099
            DateFormat = dfShort
            DateMode = dmComboBox
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBtnShadow
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkDate
            ParseInput = False
            ParentFont = False
            TabOrder = 1
            OnCloseUp = DateEditStartCloseUp
            OnChange = DateEditStartChange
          end
        end
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 652
    Top = 4
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 680
    Top = 4
  end
  inherited dsrcActive: TDataSource
    Left = 4
    Top = 108
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 573
    Top = 7
    object dxDBGridLayoutListEmployeeItem: TdxDBGridLayout
      Data = {
        53030000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070E746152696768744A75737469667907436170
        74696F6E0608456D706C6F79656500000D44656661756C744C61796F75740913
        48656164657250616E656C526F77436F756E740201084B65794669656C64060F
        454D504C4F5945455F4E554D4245520D53756D6D61727947726F7570730E0010
        53756D6D617279536570617261746F7206022C200A44617461536F7572636507
        22496C6C6E6573734D65737361676573444D2E44617461536F757263654D6173
        7465720F4F7074696F6E734265686176696F720B0C6564676F4175746F536F72
        740E6564676F447261675363726F6C6C136564676F456E74657253686F774564
        69746F720E6564676F5461625468726F7567680F6564676F566572745468726F
        75676800094F7074696F6E7344420B106564676F43616E63656C4F6E45786974
        116564676F43616E4E617669676174696F6E126564676F4C6F6164416C6C5265
        636F726473106564676F557365426F6F6B6D61726B7300000F54647844424772
        6964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E
        756D62657208526561644F6E6C790905576964746802450942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F454D504C4F59
        45455F4E554D42455200000F546478444247726964436F6C756D6E0B436F6C75
        6D6E53686F72740743617074696F6E060A53686F7274206E616D650852656164
        4F6E6C7909055769647468024A0942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060A53484F52545F4E414D4500000F546478
        444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06
        044E616D6508526561644F6E6C790905576964746803C4000942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060B4445534352
        495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E41
        6464726573730743617074696F6E06074164647265737308526561644F6E6C79
        0905576964746803F4000942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060741444452455353000000}
    end
  end
end
