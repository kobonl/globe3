inherited DialogReportHoursPerEmplF: TDialogReportHoursPerEmplF
  Left = 245
  Top = 111
  Caption = 'Report hours per employee'
  ClientHeight = 468
  ClientWidth = 612
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 612
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 495
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 612
    Height = 347
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 18
    end
    inherited LblFromEmployee: TLabel
      Top = 42
    end
    inherited LblEmployee: TLabel
      Top = 42
    end
    inherited LblToPlant: TLabel
      Top = 18
    end
    inherited LblToEmployee: TLabel
      Left = 307
      Top = 257
    end
    object Label2: TLabel [6]
      Left = 8
      Top = 120
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [7]
      Left = 40
      Top = 120
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [8]
      Left = 315
      Top = 120
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 122
      Top = 44
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 44
    end
    object Label5: TLabel [11]
      Left = 122
      Top = 98
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [12]
      Left = 336
      Top = 96
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelFromHT: TLabel [13]
      Left = 8
      Top = 144
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelHT: TLabel [14]
      Left = 40
      Top = 144
      Width = 45
      Height = 13
      Caption = 'Hourtype'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelToHT: TLabel [15]
      Left = 315
      Top = 144
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelHTFrom: TLabel [16]
      Left = 122
      Top = 127
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelHTTo: TLabel [17]
      Left = 336
      Top = 127
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel [18]
      Left = 56
      Top = 260
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [19]
      Left = 72
      Top = 260
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [20]
      Left = 59
      Top = 252
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [21]
      Left = 58
      Top = 263
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [22]
      Left = 56
      Top = 263
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label8: TLabel [23]
      Left = 315
      Top = 42
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 68
    end
    inherited LblDepartment: TLabel
      Top = 68
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 122
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 97
    end
    inherited LblToTeam: TLabel
      Top = 94
    end
    inherited LblStarTeamFrom: TLabel
      Left = 122
      Top = 97
    end
    inherited LblTeam: TLabel
      Top = 94
    end
    inherited LblFromTeam: TLabel
      Top = 94
    end
    object CheckBoxAllTeam: TCheckBox [64]
      Left = 35
      Top = 255
      Width = 36
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 18
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 125
      Visible = False
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 126
      Visible = False
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 93
      ColCount = 146
      TabOrder = 5
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 93
      ColCount = 147
      TabOrder = 6
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 523
      Top = 100
      TabOrder = 7
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 67
      ColCount = 147
      TabOrder = 37
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 67
      ColCount = 148
      TabOrder = 30
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 266
      Top = 242
      TabOrder = 38
      Visible = False
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 41
      TabOrder = 2
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 41
      TabOrder = 3
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 145
      TabOrder = 26
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 146
      TabOrder = 24
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 25
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 168
      TabOrder = 13
      Visible = True
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 522
      Top = 42
      TabOrder = 4
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 36
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 153
      TabOrder = 29
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 154
      TabOrder = 21
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 154
      TabOrder = 22
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 155
      TabOrder = 23
    end
    inherited EditWorkspots: TEdit
      TabOrder = 31
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 157
      TabOrder = 33
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 156
      TabOrder = 32
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 35
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 34
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 119
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 8
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 119
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 9
    end
    object GroupBox1: TGroupBox
      Left = 256
      Top = 192
      Width = 297
      Height = 145
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 17
      object CheckBoxShowSelection: TCheckBox
        Left = 16
        Top = 24
        Width = 113
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxShowHourType: TCheckBox
        Left = 16
        Top = 88
        Width = 113
        Height = 17
        Caption = 'Show hour types'
        TabOrder = 2
      end
      object CheckBoxPageTeam: TCheckBox
        Left = 144
        Top = 24
        Width = 145
        Height = 17
        Caption = 'New page per team'
        TabOrder = 4
      end
      object CheckBoxExport: TCheckBox
        Left = 144
        Top = 92
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 6
      end
      object CheckBoxWeek: TCheckBox
        Left = 16
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Show per week'
        TabOrder = 1
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 144
        Top = 58
        Width = 148
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 5
      end
      object CheckBoxShowPlantInfo: TCheckBox
        Left = 16
        Top = 120
        Width = 121
        Height = 17
        Caption = 'Show plant info'
        TabOrder = 3
      end
    end
    object CheckBoxAllDate: TCheckBox
      Left = 19
      Top = 255
      Width = 38
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      Visible = False
      OnClick = CheckBoxAllDateClick
    end
    object RadioGroupProduction: TRadioGroup
      Left = 8
      Top = 192
      Width = 113
      Height = 145
      Caption = 'Hours'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Production '
        'Salary ')
      ParentFont = False
      TabOrder = 15
      OnClick = RadioGroupProductionClick
    end
    object RadioGroupStatusEmpl: TRadioGroup
      Left = 128
      Top = 192
      Width = 121
      Height = 145
      Caption = 'Status employee'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Active'
        'Not active'
        'All')
      ParentFont = False
      TabOrder = 16
    end
    object ComboBoxPlusHourTypeFrom: TComboBoxPlus
      Tag = 1
      Left = 120
      Top = 145
      Width = 180
      Height = 19
      ColCount = 142
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusHourTypeFromCloseUp
    end
    object ComboBoxPlusHourTypeTo: TComboBoxPlus
      Tag = 1
      Left = 334
      Top = 145
      Width = 180
      Height = 19
      ColCount = 143
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusHourTypeToCloseUp
    end
    object CheckBoxAllHourType: TCheckBox
      Left = 523
      Top = 145
      Width = 38
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      OnClick = CheckBoxAllHourTypeClick
    end
  end
  inherited stbarBase: TStatusBar
    Top = 408
    Width = 612
  end
  inherited pnlBottom: TPanel
    Top = 427
    Width = 612
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 496
    Top = 24
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 536
    Top = 24
  end
  inherited tblPlant: TTable
    Left = 80
  end
  inherited QueryEmplFrom: TQuery
    Left = 200
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 296
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 584
    Top = 176
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4C040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072C4469616C6F675265706F7274486F757273506572456D70
        6C462E44617461536F75726365456D706C46726F6D104F7074696F6E73437573
        746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453
        697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D
        6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344
        420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C6574
        650D6564676F43616E496E73657274116564676F43616E4E617669676174696F
        6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C52
        65636F726473106564676F557365426F6F6B6D61726B7300000F546478444247
        726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802410942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D6505576964746802540942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E
        616D6505576964746803B4000942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060B4445534352495054494F4E00000F546478
        444247726964436F6C756D6E0D436F6C756D6E41646472657373074361707469
        6F6E06074164647265737305576964746802450942616E64496E646578020008
        526F77496E6465780200094669656C644E616D6506074144445245535300000F
        546478444247726964436F6C756D6E0E436F6C756D6E44657074436F64650743
        617074696F6E060F4465706172746D656E7420636F6465055769647468025809
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060F4445504152544D454E545F434F444500000F546478444247726964436F6C
        756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F64
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        13040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072A4469616C6F675265706F72
        74486F757273506572456D706C462E44617461536F75726365456D706C546F10
        4F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E67
        0E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67
        106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E67
        00094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564
        676F43616E44656C6574650D6564676F43616E496E73657274116564676F4361
        6E4E617669676174696F6E116564676F436F6E6669726D44656C657465126564
        676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B
        7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C0743
        617074696F6E06064E756D62657206536F727465640704637355700557696474
        6802310942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F454D504C4F5945455F4E554D42455200000F5464784442477269
        64436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E06
        0A53686F7274206E616D65055769647468024E0942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D45
        00000F546478444247726964436F6C756D6E11436F6C756D6E44657363726970
        74696F6E0743617074696F6E06044E616D6505576964746803CC000942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060B4445
        534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C75
        6D6E416464726573730743617074696F6E060741646472657373055769647468
        02650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506074144445245535300000F546478444247726964436F6C756D6E0E43
        6F6C756D6E44657074436F64650743617074696F6E060F4465706172746D656E
        7420636F64650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060F4445504152544D454E545F434F444500000F5464784442
        47726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E060954
        65616D20636F64650942616E64496E646578020008526F77496E646578020009
        4669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  object TableHourType: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'HOURTYPE_NUMBER'
    TableName = 'HOURTYPE'
    Left = 16
    Top = 28
  end
end
