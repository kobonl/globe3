(*
  MR:09-11-2005
    Added queries and functions for determination of
      All plants, teams, employees...
    Added queries and procedure for deleting and adding of week-records,
      that can be used in a query where weeks are needed.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
*)

unit ReportBaseDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, DBClient, Db;

type

  TReportBaseDM = class(TDataModule)
    qryPlantMinMax: TQuery;
    qryTeamMinMax: TQuery;
    qryEmplMinMax: TQuery;
    qryBUMinMax: TQuery;
    qryWeekDelete: TQuery;
    qryWeekInsert: TQuery;
    qryWorkspotMinMax: TQuery;
    qryDepartmentMinMax: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FEnableWorkspotIncludeForThisReport: Boolean;
    FInclExclWorkspotsResult: String;
    FUseDateTime: Boolean;
    FIncludeOpenScans: Boolean;
  public
    { Public declarations }
    procedure ActiveTables(Table_Active: Boolean);
    procedure FillWeekTable(const ADateFrom, ADateTo: TDateTime);
    function DetermineAllPlants(const APlantFrom, APlantTo: String): Boolean;
    function DetermineAllTeams(const ATeamFrom, ATeamTo: String): Boolean;
    function DetermineAllEmployees(const AEmplFrom, AEmplTo: String): Boolean;
    function DetermineAllBusinessUnits(
      const ABUFrom, ABUTo: String): Boolean;
    function DetermineAllWorkspots(
      const APlant, AWorkspotFrom, AWorkspotTo: String): Boolean;
    function DetermineAllDepartments(
      const APlant, ADepartmentFrom, ADepartmentTo: String): Boolean;
    function MyQuotes(AValue: String): String;
    function InclExclWorkspotsAll: Boolean; // 20015223
    property EnableWorkspotIncludeForThisReport: Boolean
      read FEnableWorkspotIncludeForThisReport
      write FEnableWorkspotIncludeForThisReport; // 20015223
    property InclExclWorkspotsResult: String read FInclExclWorkspotsResult
      write FInclExclWorkspotsResult; // 20015223
    property UseDateTime: Boolean read FUseDateTime write FUseDateTime; // 20015220
    property IncludeOpenScans: Boolean read FIncludeOpenScans write FIncludeOpenScans; // 20015221
  end;

  TReportBaseClassDM = Class of TReportBaseDM;

var
  ReportBaseDM: TReportBaseDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, SystemDMT, UPimsMessageRes;

procedure TReportBaseDM.ActiveTables(Table_Active: Boolean);
var
  DataSetCounter: Integer;
  Temp: TComponent;
begin
  if Self <> nil then
    for DataSetCounter := 0 to Self.ComponentCount - 1 do
    begin
      Temp := Self.Components[DataSetCounter];
      if (Temp is TTable) then
      begin
        if ((Temp as TTable).TableName <> '') then
        begin
        // !!!TESTING!!!
//          ShowMessage((Temp as TTable).TableName);
          (Temp as TTable).Active := Table_Active;
        end;
      end;
//CAR 8-7-2003 -  ClentDataSet are assign to TQuery components
      if (Temp is TClientDataSet) then
      begin
        if ((Temp as TClientDataSet).ProviderName <> '') then
          (Temp as TClientDataSet).Active := Table_Active;
      end;

      if not Table_Active then
        if (Temp is TQuery) then
        begin
          (Temp as TQuery).Close;
          (Temp as TQuery).Unprepare;
        end;
    end;
end;

procedure TReportBaseDM.DataModuleCreate(Sender: TObject);
begin
   ActiveTables(True);
end;

procedure TReportBaseDM.DataModuleDestroy(Sender: TObject);
begin
   ActiveTables(False);
end;

// The table 'WEEK' is filled here, so it can be used in a Query to
// get the WEEK.WEEKNUMBER by comparing WEEK.DATEFROM and WEEK.DATETO.
// Also 'computername' must be used in the query!
procedure TReportBaseDM.FillWeekTable(
  const ADateFrom, ADateTo: TDateTime);
var
  ADate: TDateTime;
  Year, Week: Word;
  PreviousWeek: Integer;
begin
  SystemDM.Pims.StartTransaction;
  try
    qryWeekDelete.Close;
    qryWeekDelete.ParamByName('COMPUTERNAME').AsString :=
      SystemDM.CurrentComputerName;
    qryWeekDelete.ExecSQL;
    PreviousWeek := 0;
    ADate := ADateFrom;
    while ADate <= ADateTo do
    begin
      ListProcsF.WeekUitDat(ADate, Year, Week);
      if PreviousWeek <> Week then
      begin
        qryWeekInsert.Close;
        qryWeekInsert.ParamByName('COMPUTERNAME').AsString :=
          SystemDM.CurrentComputerName;
        qryWeekInsert.ParamByName('YEARNUMBER').AsInteger := Year;
        qryWeekInsert.ParamByName('WEEKNUMBER').AsInteger := Week;
        qryWeekInsert.ParamByName('DATEFROM').AsDateTime :=
          ListProcsF.LastDayOfWeek(ADate) - 6;
        qryWeekInsert.ParamByName('DATETO').AsDateTime :=
          ListProcsF.LastDayOfWeek(ADate);
        qryWeekInsert.ExecSQL;
      end;
      ADate := ADate + 1;
      PreviousWeek := Week;
    end;
    qryWeekDelete.Close;
    qryWeekInsert.Close;
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Rollback;
  end;
end;

function TReportBaseDM.DetermineAllPlants(
  const APlantFrom, APlantTo: String): Boolean;
begin
  Result := False;
  qryPlantMinMax.Close;
  qryPlantMinMax.Open;
  if (qryPlantMinMax.FieldByName('MINPLANT').AsString = APlantFrom) and
    (qryPlantMinMax.FieldByName('MAXPLANT').AsString = APlantTo) then
    Result := True;
  qryPlantMinMax.Close;
end;

function TReportBaseDM.DetermineAllTeams(
  const ATeamFrom, ATeamTo: String): Boolean;
begin
  Result := False;
  qryTeamMinMax.Close;
  qryTeamMinMax.Open;
  if (qryTeamMinMax.FieldByName('MINTEAM').AsString = ATeamFrom) and
    (qryTeamMinMax.FieldByName('MAXTEAM').AsString = ATeamTo) then
    Result := True;
  qryTeamMinMax.Close;
end;

function TReportBaseDM.DetermineAllEmployees(
  const AEmplFrom, AEmplTo: String): Boolean;
begin
  Result := False;
  qryEmplMinMax.Close;
  qryEmplMinMax.Open;
  if (qryEmplMinMax.FieldByName('MINEMPLOYEE').AsString = AEmplFrom) and
    (qryEmplMinMax.FieldByName('MAXEMPLOYEE').AsString = AEmplTo) then
    Result := True;
  qryEmplMinMax.Close;
end;

function TReportBaseDM.DetermineAllBusinessUnits(
  const ABUFrom, ABUTo: String): Boolean;
begin
  Result := False;
  qryBUMinMax.Close;
  qryBUMinMax.Open;
  if (qryBUMinMax.FieldByName('MINBU').AsString = ABUFrom) and
    (qryBUMinMax.FieldByName('MAXBU').AsString = ABUTo) then
    Result := True;
  qryBUMinMax.Close;
end;

function TReportBaseDM.DetermineAllWorkspots(const APlant, AWorkspotFrom,
  AWorkspotTo: String): Boolean;
begin
  Result := False;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
    Result := InclExclWorkspotsAll
  else
  begin
    qryWorkspotMinMax.Close;
    qryWorkspotMinMax.ParamByName('PLANT_CODE').AsString := APlant;
    qryWorkspotMinMax.Open;
    if (qryWorkspotMinMax.FieldByName('MINWORKSPOT').AsString = AWorkspotFrom) and
      (qryWorkspotMinMax.FieldByName('MAXWORKSPOT').AsString = AWorkspotTo) then
      Result := True;
    qryWorkspotMinMax.Close;
  end;
end;

function TReportBaseDM.DetermineAllDepartments(const APlant,
  ADepartmentFrom, ADepartmentTo: String): Boolean;
begin
  Result := False;
  qryDepartmentMinMax.Close;
  qryDepartmentMinMax.ParamByName('PLANT_CODE').AsString := APlant;
  qryDepartmentMinMax.Open;
  if (qryDepartmentMinMax.FieldByName('MINDEPARTMENT').AsString =
    ADepartmentFrom) and
    (qryDepartmentMinMax.FieldByName('MAXDEPARTMENT').AsString =
    ADepartmentTo) then
    Result := True;
  qryDepartmentMinMax.Close;
end;

// Check if string is numeric, if not put quotes around it.
// Changes: Function only trims the value and always put quotes around it. 
function TReportBaseDM.MyQuotes(AValue: String): String;
{const                      // 0..9
  ValidKeys: set of Char = [#48..#57];
var
  I: Integer;
  Key: Char;}
begin
  Result := '''' + Trim(AValue) + '''';
{
  Result := Trim(AValue);
  if Result <> '' then
  begin
    for I := 1 to Length(Result) do
    begin
      Key := Result[I];
      if not(Key in ValidKeys) then
      begin
        Result := '''' + Result + '''';
        Break;
      end;
    end;
  end;
}
end;

// 20015223
function TReportBaseDM.InclExclWorkspotsAll: Boolean;
begin
  Result := (InclExclWorkspotsResult = SPimsAllWorkspots) or
    (InclExclWorkspotsResult = SPimsMultipleWorkspots);
end;

end.
