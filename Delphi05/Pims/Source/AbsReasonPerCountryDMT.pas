(*
  SO:08-JUL-2010 RV067.4. 550497
  MRA:4-JUN-2012. 20377. ToDo.
  - The description of absence reason is not completely
    displayed.
  MRA:19-FEB-2018 Glob3-72.
  - Add Export_code to AbsenceReason
*)

unit AbsReasonPerCountryDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TAbsReasonPerCountryDM = class(TGridBaseDM)
    TableAbsenceType: TTable;
    TableHourType: TTable;
    DataSourceAbsenceType: TDataSource;
    DataSourceHourType: TDataSource;
    TableAbsenceTypeABSENCETYPE_CODE: TStringField;
    TableAbsenceTypeDESCRIPTION: TStringField;
    TableAbsenceTypeCREATIONDATE: TDateTimeField;
    TableAbsenceTypeEXPORT_CODE: TStringField;
    TableAbsenceTypeMUTATIONDATE: TDateTimeField;
    TableAbsenceTypeMUTATOR: TStringField;
    TableHourTypeHOURTYPE_NUMBER: TIntegerField;
    TableHourTypeDESCRIPTION: TStringField;
    TableHourTypeCREATIONDATE: TDateTimeField;
    TableHourTypeOVERTIME_YN: TStringField;
    TableHourTypeMUTATIONDATE: TDateTimeField;
    TableHourTypeMUTATOR: TStringField;
    TableHourTypeCOUNT_DAY_YN: TStringField;
    TableHourTypeEXPORT_CODE: TStringField;
    TableHourTypeBONUS_PERCENTAGE: TFloatField;
    TableMasterCOUNTRY_ID: TFloatField;
    TableMasterEXPORT_TYPE: TStringField;
    TableMasterCODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableDetailCOUNTRY_ID: TIntegerField;
    TableDetailABSENCEREASON_ID: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableAbsenceReason: TTable;
    TableAbsenceReasonABSENCEREASON_ID: TIntegerField;
    TableAbsenceReasonABSENCETYPE_CODE: TStringField;
    TableAbsenceReasonABSENCEREASON_CODE: TStringField;
    TableAbsenceReasonDESCRIPTION: TStringField;
    TableAbsenceReasonCREATIONDATE: TDateTimeField;
    TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField;
    TableAbsenceReasonMUTATIONDATE: TDateTimeField;
    TableAbsenceReasonMUTATOR: TStringField;
    TableAbsenceReasonPAYED_YN: TStringField;
    TableAbsenceReasonOVERRULE_WITH_ILLNESS_YN: TStringField;
    TableAbsenceReasonabsencetype: TStringField;
    TableDetailabsencetype: TStringField;
    TableDetailabsencereason_code: TStringField;
    TableDetaildescription: TStringField;
    TableAbsenceReasonhourtype: TStringField;
    TableDetailpayed_yn: TStringField;
    TableDetailoverrule_with_illness_yn: TStringField;
    TableDetailhourtype: TStringField;
    TableAbsenceReasonEXPORT_CODE: TStringField;
    TableDetailexport_code: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AbsReasonPerCountryDM: TAbsReasonPerCountryDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TAbsReasonPerCountryDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableDetail.Open;
end;

procedure TAbsReasonPerCountryDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  TableDetail.Close;
end;

end.
