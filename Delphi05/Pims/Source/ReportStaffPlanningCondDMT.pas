(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:6-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
*)
unit ReportStaffPlanningCondDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, ReportStaffPlanningCondQRPT, DBClient,
  UPimsConst;

type
  TReportStaffPlanningCondDM = class(TReportBaseDM)
    QueryEMP: TQuery;
    TableSTO: TTable;
    QueryExec: TQuery;
    QueryDept: TQuery;
    QueryShift: TQuery;
    TablePlant: TTable;
    QueryWK: TQuery;
    cdsEmpPln: TClientDataSet;
    cdsEmpPlnDay1EmpNr: TStringField;
    cdsEmpPlnDay1EmpName: TStringField;
    cdsEmpPlnDay2EmpNr: TStringField;
    cdsEmpPlnDay2EmpName: TStringField;
    cdsEmpPlnDay3EmpNr: TStringField;
    cdsEmpPlnDay4EmpNr: TStringField;
    cdsEmpPlnDay4EmpName: TStringField;
    cdsEmpPlnDay5EmpNr: TStringField;
    cdsEmpPlnDay5EmpName: TStringField;
    cdsEmpPlnDay6EmpNr: TStringField;
    cdsEmpPlnDay6EmpName: TStringField;
    cdsEmpPlnDay7EmpNr: TStringField;
    cdsEmpPlnDay7EmpName: TStringField;
    cdsEmpPlnDay3EmpName: TStringField;
    cdsEmpPlnNumber: TIntegerField;
    cdsEmpPlnCDSNumber: TIntegerField;
    cdsEmpPlnTB: TStringField;
    cdsEmpPlnTBDESC: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryEMPFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function ISNotDeptValid(Dept,Plant, TeamFrom, TeamTo: String) : Boolean;
    procedure GetTBFromEMP(Empl, Shift: Integer;
      Plant, Workspot, Department: String;
      DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE);
  end;

var
  ReportStaffPlanningCondDM: TReportStaffPlanningCondDM;

implementation

uses CalculateTotalHoursDMT, ListProcsFRM, SystemDMT;

{$R *.DFM}
procedure TReportStaffPlanningCondDM.GetTBFromEMP(Empl, Shift: Integer;
  Plant, Workspot, Department: String;
  DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE);
var
  i: Integer;
begin
  for i:= 1 to MAX_TBS do
    Schedule_TB[i] := '';
  for i := 1 to MAX_TBS do
    Schedule_TB[I] :=
      QueryEmp.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
end;

function TReportStaffPlanningCondDM.ISNotDeptValid(Dept,Plant, TeamFrom,
  TeamTo: String) : Boolean;
var
  SelectStr: String;
begin
  SelectStr := 
    'SELECT ' +
    '  COUNT(TEAM_CODE) AS COUNTTEAM ' +
    'FROM ' +
    '  DEPARTMENTPERTEAM ' +
  //Car 20.2.2004 add doublequote function
    'WHERE ' +
    '  PLANT_CODE = ''' + DoubleQuote(Plant) + '''' +
    '  AND DEPARTMENT_CODE = ''' + DoubleQuote(DEPT) + '''' +
    '  AND TEAM_CODE >= ''' + DoubleQuote(TEAMFrom) + '''' +
    '  AND TEAM_CODE <= ''' + DoubleQuote(TeamTo) + '''';
  QueryExec.Active := False;
  QueryExec.SQL.Clear;
  QueryExec.SQL.Add(Selectstr);
  QueryExec.ExecSQL;
  QueryExec.Active := True;
  // Pims -> Oracle
  Result := QueryExec.FieldByName('COUNTTEAM').AsFloat > 0;
//  Result := QueryExec.RecordCount > 0 ;
end;

procedure TReportStaffPlanningCondDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  cdsEmpPln.CreateDataSet;
  QueryShift.Open;
  QueryDept.Open;
  QueryWK.Open;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEMP);
end;

procedure TReportStaffPlanningCondDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  cdsEmpPln.EmptyDataSet;
  cdsEmpPln.Close;
  QueryShift.Close;
  QueryDept.Close;
  QueryWK.Close;
end;

procedure TReportStaffPlanningCondDM.QueryEMPFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
