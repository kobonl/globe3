(*
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:19-JUL-2011 RV095.1.
  - Workspot moved to DialogReportBase-form.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:3-DEC-2013 TD-23759
  - Report production comparison gives sometimes wrong values
    - The report gave double quantities.
    - Cause: It did a join with DEPARTMENTPERTEAM, which gave double values.
    - Here parameter 'All teams' is added.
  MRA:18-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
*)
unit DialogReportProductionCompFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportProductionCompF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    QueryBU: TQuery;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    QueryJobCode: TQuery;
    DataSourceWorkSpot: TDataSource;
    CheckBoxPageBU: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxDetailRep: TCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label27: TLabel;
    Label6: TLabel;
    Label19: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
{    procedure FillWorkSpot; }
//    procedure FillJobcode;
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbPlusWorkspotFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotToCloseUp(Sender: TObject);
  private
    procedure ShowDateSelection(ShowYN: Boolean);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportProductionCompF: TDialogReportProductionCompF;

// RV089.1.
function DialogReportProductionCompForm: TDialogReportProductionCompF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ListProcsFRM,
  ReportProductionCompDMT, ReportProductionCompQRPT, UPimsMessageRes;

// RV089.1.
var
  DialogReportProductionCompF_HND: TDialogReportProductionCompF;

// RV089.1.
function DialogReportProductionCompForm: TDialogReportProductionCompF;
begin
  if (DialogReportProductionCompF_HND = nil) then
  begin
    DialogReportProductionCompF_HND := TDialogReportProductionCompF.Create(Application);
    with DialogReportProductionCompF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportProductionCompF_HND;
end;

// RV089.1.
procedure TDialogReportProductionCompF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportProductionCompF_HND <> nil) then
  begin
    DialogReportProductionCompF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportProductionCompF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportProductionCompF.btnOkClick(Sender: TObject);
begin
  inherited;
  if DateTimeFrom(DateFrom.Date) > DateTimeTo(DateTo.Date) then // 20015220
  begin
    DisplayMessage( SDateFromTo, mtInformation, [mbOk]);
    Exit;
  end;
  ReportProductionCompDM.EnableWorkspotIncludeForThisReport := False;
  ReportProductionCompQR.EnableWorkspotIncludeForThisReport := False;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
  begin
    ReportProductionCompDM.EnableWorkspotIncludeForThisReport := True;
    ReportProductionCompQR.EnableWorkspotIncludeForThisReport := True;
    ReportProductionCompQR.InclExclWorkspotsResult := EditWorkspots.Text;
    if not ReportProductionCompQR.InclExclWorkspotsAll then
      ReportProductionCompQR.InclExclWorkspotsResult :=
        GetStrValue(ReportProductionCompQR.InclExclWorkspotsResult);
    ReportProductionCompDM.InclExclWorkspotsResult :=
      ReportProductionCompQR.InclExclWorkspotsResult;
  end;
  // 20015220
  ReportProductionCompDM.UseDateTime := SystemDM.DateTimeSel;
  ReportProductionCompQR.UseDateTime := SystemDM.DateTimeSel;
  if ReportProductionCompQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(ComboBoxPlusBusinessFrom.Value),
    GetStrValue(ComboBoxPlusBusinessTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    GetStrValue(CmbPlusJobFrom.Value),
    GetStrValue(CmbPlusJobTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    DateTimeFrom(DateFrom.Date), // 20015220
    DateTimeTo(DateTo.Date), // 20015220
    CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
    CheckBoxPageBU.Checked, CheckBoxPageDept.Checked,
    CheckBoxPageWK.Checked, CheckBoxDetailRep.Checked,
    CheckBoxAllTeams.Checked) // TD-23759
  then
    ReportProductionCompQR.ProcessRecords;
end;

procedure TDialogReportProductionCompF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;
{
procedure TDialogReportProductionCompF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    //RV067.2.
    QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
{
procedure TDialogReportProductionCompF.FillJobCode;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
     (CmbPlusWorkspotFrom.Value <> '') and
     (CmbPlusWorkspotFrom.Value = CmbPlusWorkspotTo.Value) then
  begin
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeFrom,
       GetStrValue(CmbPlusPlantFrom.Value),
       GetStrValue(CmbPlusWorkspotFrom.Value), True);
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeTo,
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value), False);
    ComboBoxPlusJobCodeFrom.Visible := True;
    ComboBoxPlusJobCodeTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusJobCodeFrom.Visible := False;
    ComboBoxPlusJobCodeTo.Visible := False;
  end;
end;
}
procedure TDialogReportProductionCompF.FormShow(Sender: TObject);
begin
  if SystemDM.WorkspotInclSel then
    EnableWorkspotIncludeForThisReport := True; // 20015223
  if SystemDM.DateTimeSel then // 20015220
  begin
    UseDateTime := True;
    ShowDateSelection(False);
  end;
  InitDialog(True, True, True, False, False, True, True);
  inherited;
  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
  CheckBoxShowSelection.Checked := True;
{  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    False, ComboBoxPlusTeamTo); }

end;

procedure TDialogReportProductionCompF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportProductionCompDM := CreateReportDM(TReportProductionCompDM);
  ReportProductionCompQR := CreateReportQR(TReportProductionCompQR);
end;

procedure TDialogReportProductionCompF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
     GetStrValue(ComboBoxPlusBusinessTo.Value) then
       ComboBoxPlusBusinessTo.DisplayValue :=
         ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportProductionCompF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportProductionCompF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
       ComboBoxPlusWorkSpotTo.DisplayValue :=
         ComboBoxPlusWorkSpotFrom.DisplayValue;
  FillJobCode; }
end;

procedure TDialogReportProductionCompF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue;
   FillJobCode; }
end;

procedure TDialogReportProductionCompF.FillAll;
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
//  FillJobCode;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportProductionCompF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportProductionCompF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportProductionCompF.CmbPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

procedure TDialogReportProductionCompF.CmbPlusWorkspotToCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

// 20015220
procedure TDialogReportProductionCompF.ShowDateSelection(ShowYN: Boolean);
begin
  Label2.Visible := ShowYN;
  Label4.Visible := ShowYN;
  Label26.Visible := ShowYN;
  DateFrom.Visible := ShowYN;
  DateTo.Visible := ShowYN;
end;

end.
