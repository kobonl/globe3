(*
  MRA:30-NOV-2009 RV045.1.
    - Time For Time and Bonus in Money can be set on Contractgroup-
      or Hourtype-level.
    - Also changed qrySalHrPerEmp because of wrong link with
      B.U. When there are multiple B.U.'s per plant, then
      it gave multiple records.
  MRA:1-FEB-2019 GLOB3-206
  - Disbursement details report
  - Also show employee extra payments
*)

unit ReportEmployeePaylistDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, ReportBaseDMT, Db, DBTables;

type
  TReportEmployeePaylistDM = class(TReportBaseDM)
    tblTaylorMade: TTable;
    qryOvertimeDefinition: TQuery;
    tblHourtype: TTable;
    qryEmployeeContract: TQuery;
    tblContractgroup: TTable;
    qrySalHrPerEmp: TQuery;
    tblTaylorMadeQUALITY_INCENTIVE_YN: TStringField;
    qryBonusPerHour: TQuery;
    qryMistakesPerEmp: TQuery;
    QueryProdHour: TQuery;
    QueryPQ: TQuery;
    QueryTRSCNT: TQuery;
    QueryTRS: TQuery;
    AQuery: TQuery;
    TableWK: TTable;
    TableBU: TTable;
    TableJob: TTable;
    qryEmployeeExtraPayments: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
  public
    { Public declarations }
    function BonusCalculation(const Mistakes,
      SalaryMinutes: Integer; Overtime: Double): Double;
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
  end;

var
  ReportEmployeePaylistDM: TReportEmployeePaylistDM;

implementation

{$R *.DFM}

uses
  GlobalDMT;

procedure TReportEmployeePaylistDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //CAR 29/1/2004 - it is not necessary they are open in class report
{  tblTaylorMade.Active := True;
  tblContractgroup.Active := True;
  tblHourtype.Active := True;
  TableWK.Active := True;
  TableJob.Active := True;
  TableBU.Active := True; }
  qryBonusPerHour.Open;
  // RV045.1.
  ContractGroupTFT := GlobalDM.ContractGroupTFTExists;
end;

procedure TReportEmployeePaylistDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //CAR 29/1/2004 - it is not necessary they are closed in class report
{ tblTaylorMade.Active := False;
  tblContractgroup.Active := False;
  tblHourtype.Active := False;
  TableWK.Active := False;
  TableJob.Active := False;
  TableBU.Active := False;
  qryBonusPerHour.Close;  }
end;

function TReportEmployeePaylistDM.BonusCalculation(const Mistakes,
  SalaryMinutes: Integer; Overtime: Double): Double;
var
  BonusPerHr: Double;
begin
  BonusPerHr := 0;
  // MR:06-04-2005
  // Pims -> Oracle
  qryBonusPerHour.Close;
  qryBonusPerHour.ParamByName('MISTAKE').AsInteger := Mistakes;
  qryBonusPerHour.Open;
  if not qryBonusPerHour.IsEmpty then
    BonusPerHr := qryBonusPerHour.FieldByName('BONUS_PER_HOUR').AsFloat;
  qryBonusPerHour.Close;
(*
  qryBonusPerHour.First;
  while not qryBonusPerHour.Eof do
  begin
    if Mistakes < qryBonusPerHour.FieldByName('MISTAKE').AsInteger then
      Break;
    BonusPerHr := qryBonusPerHour.FieldByName('BONUS_PER_HOUR').AsFloat;
    qryBonusPerHour.Next;
  end;
*)
  Result := BonusPerHr * ((SalaryMinutes + Overtime) / 60);
end;

end.
