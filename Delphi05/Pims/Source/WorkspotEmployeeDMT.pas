unit WorkspotEmployeeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TWorkspotEmployeeDM = class(TGridBaseDM)
    TablePlant: TTable;
    TableWorkspot: TTable;
    DataSourcePlant: TDataSource;
    DataSourceWorkSpot: TDataSource;
    TableMasterEMPLOYEE_NUMBER: TIntegerField;
    TableMasterSHORT_NAME: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterDEPARTMENT_CODE: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterLANGUAGE_CODE: TStringField;
    TableMasterPHONE_NUMBER: TStringField;
    TableMasterEMAIL_ADDRESS: TStringField;
    TableMasterDATE_OF_BIRTH: TDateTimeField;
    TableMasterSEX: TStringField;
    TableMasterSOCIAL_SECURITY_NUMBER: TStringField;
    TableMasterSTARTDATE: TDateTimeField;
    TableMasterTEAM_CODE: TStringField;
    TableMasterENDDATE: TDateTimeField;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterIS_SCANNING_YN: TStringField;
    TableMasterCUT_OF_TIME_YN: TStringField;
    TableMasterREMARK: TStringField;
    TableMasterCALC_BONUS_YN: TStringField;
    TableMasterFIRSTAID_YN: TStringField;
    TableMasterFIRSTAID_EXP_DATE: TDateTimeField;
    TableMasterPLANTLU: TStringField;
    TableMasterDEPARTMENTLU: TStringField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailWORKSPOT_CODE: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailSTARTDATE_LEVEL: TDateTimeField;
    TableDetailWORKSPOTLU: TStringField;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableWorkspotPLANT_CODE: TStringField;
    TableWorkspotDESCRIPTION: TStringField;
    TableWorkspotWORKSPOT_CODE: TStringField;
    TableWorkspotCREATIONDATE: TDateTimeField;
    TableWorkspotDEPARTMENT_CODE: TStringField;
    TableWorkspotMUTATIONDATE: TDateTimeField;
    TableWorkspotMUTATOR: TStringField;
    TableWorkspotUSE_JOBCODE_YN: TStringField;
    TableWorkspotHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotQUANT_PIECE_YN: TStringField;
    TableWorkspotAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotCOUNTER_VALUE_YN: TStringField;
    TableWorkspotENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotDATE_INACTIVE: TDateTimeField;
    TableDetailEMPLOYEE_LEVEL: TStringField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDept: TTable;
    TableDetailDEPTLU: TStringField;
    DataSourceDept: TDataSource;
    TableDeptDEPARTMENT_CODE: TStringField;
    TableDeptPLANT_CODE: TStringField;
    TableDeptDESCRIPTION: TStringField;
    TableDeptBUSINESSUNIT_CODE: TStringField;
    TableDeptCREATIONDATE: TDateTimeField;
    TableDeptDIRECT_HOUR_YN: TStringField;
    TableDeptMUTATIONDATE: TDateTimeField;
    TableDeptMUTATOR: TStringField;
    TableDeptPLAN_ON_WORKSPOT_YN: TStringField;
    TableDeptREMARK: TStringField;
    Tablewk: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    DateTimeField2: TDateTimeField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField1: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    DateTimeField3: TDateTimeField;
    TableDetailWKDESC: TStringField;
    TableDetailPLANTLU: TStringField;
    TablewkAUTOMATIC_RESCAN_YN: TStringField;
    TablewkSHORT_NAME: TStringField;
    TableDepartment: TTable;
    TableDepartmentDEPARTMENT_CODE: TStringField;
    TableDepartmentPLANT_CODE: TStringField;
    TableDepartmentDESCRIPTION: TStringField;
    TableDepartmentBUSINESSUNIT_CODE: TStringField;
    TableDepartmentCREATIONDATE: TDateTimeField;
    TableDepartmentDIRECT_HOUR_YN: TStringField;
    TableDepartmentMUTATIONDATE: TDateTimeField;
    TableDepartmentMUTATOR: TStringField;
    TableDepartmentPLAN_ON_WORKSPOT_YN: TStringField;
    TableDepartmentREMARK: TStringField;
    TableDetailDEPTDESC: TStringField;
    TableDeptLU: TTable;
    StringField13: TStringField;
    StringField14: TStringField;
    StringField15: TStringField;
    StringField16: TStringField;
    DateTimeField4: TDateTimeField;
    StringField17: TStringField;
    DateTimeField5: TDateTimeField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    TableMasterEMP: TFloatField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableWorkspotFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableMasterCalcFields(DataSet: TDataSet);
    procedure TableExportCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function IsPlannedOnWK(Dept: String): Boolean;
    procedure UpdateValues(TempTable: TTable);
    function FindWK(Plant, Dept, WK: String): Boolean;
  end;

var
  WorkspotEmployeeDM: TWorkspotEmployeeDM;

implementation

uses SystemDMT, UPimsMessageRes, UPimsConst;

{$R *.DFM}

function TWorkspotEmployeeDM.FindWK(Plant, Dept, WK: String): Boolean;
begin
  Result := False;
  if Tablewk.FindKey([Plant, WK]) then
    Result := (Tablewk.FieldByName('DEPARTMENT_CODE').AsString = Dept);
end;

function TWorkspotEmployeeDM.IsPlannedOnWK(Dept: String): Boolean;
begin
  Result := False;
  if (Dept <> '') and (TableDetail.FieldByName('PLANT_CODE').AsString <> '') then
  begin
    TableDept.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString, Dept]);
    Result := (TableDept.FieldByName('PLAN_ON_WORKSPOT_YN').AsString = CHECKEDVALUE);
  end;
end;

procedure TWorkspotEmployeeDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetail.FieldByName('STARTDATE_LEVEL').Value := Now;
  if not TableWorkSpot.IsEmpty then
  begin
    TableWorkSpot.First;
    TableDetail.FieldByName('WORKSPOT_CODE').Value :=
      TableWorkSpot.FieldByName('WORKSPOT_CODE').Value;
  end; 
end;

procedure TWorkspotEmployeeDM.TableDetailBeforePost(DataSet: TDataSet);
var
  Plant: String;
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if (TableDept.FieldByName('PLAN_ON_WORKSPOT_YN').AsString = UNCHECKEDVALUE) then
    WorkspotEmployeeDM.TableDetail.FieldByName('WORKSPOT_CODE').Value := DummyStr;
  if TableDetail.FieldByNAME('STARTDATE_LEVEL').Value <> Null then
    TableDetail.FieldByNAME('STARTDATE_LEVEL').Value :=
      Int(TableDetail.FieldByNAME('STARTDATE_LEVEL').Value);
  if (TableDetail.FieldByName('EMPLOYEE_LEVEL').AsString <> 'A') and
    (TableDetail.FieldByName('EMPLOYEE_LEVEL').AsString <> 'B') and
    (TableDetail.FieldByName('EMPLOYEE_LEVEL').AsString <> 'C') then
  begin
    DisplayMessage(SLEVELWK,   mtWarning, [mbOK]);
    Abort;
  end;
  if not(TableDept.FieldByName('PLAN_ON_WORKSPOT_YN').AsString = CHECKEDVALUE) then
  begin
    Plant := TableDetail.FieldByName('PLANT_CODE').AsString;
    if not TableWk.FindKey([Plant, DummyStr]) then
    begin
      TableWk.Insert;
      TableWk.FieldByName('PLANT_CODE').Value :=  Plant;
      TableWk.FieldByName('DEPARTMENT_CODE').Value :=
        TableDept.FieldByName('DEPARTMENT_CODE').Value;
      TableWk.FieldByName('WORKSPOT_CODE').Value :=  DummyStr;
      TableWk.FieldByName('SHORT_NAME').Value :=  DummyWKStr;
      TableWk.FieldByName('DESCRIPTION').Value :=  DummyWKStr;
      TableWk.FieldByName('USE_JOBCODE_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('MEASURE_PRODUCTIVITY_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('PRODUCTIVE_HOUR_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('QUANT_PIECE_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('QUANT_PIECE_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('COUNTER_VALUE_YN').Value := UNCHECKEDVALUE;
      TableWk.FieldByName('ENTER_COUNTER_AT_SCAN_YN').Value := UNCHECKEDVALUE;
      UpdateValues(TableWk);
      TableWk.Post;
      TableWk.Close;
      TableWk.Open;
    end;
  end;
end;

procedure TWorkspotEmployeeDM.UpdateValues(TempTable: TTable);
begin
  TempTable.FieldByName('CREATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
end;

procedure TWorkspotEmployeeDM.TableWorkspotFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := (TableWorkspot.FieldByName('WORKSPOT_CODE').AsString <> DummyStr); 
end;


procedure TWorkspotEmployeeDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not TableDetail.Active then
    TableDetail.Active := True;
  if not Tablewk.Active then
    Tablewk.Active := True;
  if not TableDept.Active then
    TableDept.Open;
  if TableDetail.RecordCount = 0 then
    Exit;

  if (TableDetail.FieldByName('WORKSPOT_CODE').AsString <> '') and
    (TableDetail.FieldByName('PLANT_CODE').AsString <> '')
   then
    if TableWK.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('WORKSPOT_CODE').AsString]) then
    begin
      TableDetailWKDESC.Value :=  Tablewk.FieldByName('DESCRIPTION').AsString;
      if TableDetailWKDESC.Value = DummyWKStr then
        TableDetailWKDESC.Value := '';
    end;
    if TableDept.FindKey([TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('DEPARTMENT_CODE').AsString]) then
      TableDetailDEPTDESC.Value := TableDept.FieldByName('DESCRIPTION').AsString;
end;

procedure TWorkspotEmployeeDM.TableMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByNAME('FLOAT_EMP').AsFLOAT :=
    DataSet.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TWorkspotEmployeeDM.TableExportCalcFields(DataSet: TDataSet);
begin
  inherited;
  if not TableExport.Active then
    TableExport.Active := True;
  if not Tablewk.Active then
    Tablewk.Active := True;
  if not TableDept.Active then
    TableDept.Open;
  if TableExport.RecordCount = 0 then
    Exit;
   if (TableExport.FieldByName('WORKSPOT_CODE').AsString <> '') and
     (TableExport.FieldByName('PLANT_CODE').AsString <> '')
   then
    if TableWK.FindKey([TableExport.FieldByName('PLANT_CODE').AsString,
     TableExport.FieldByName('WORKSPOT_CODE').AsString]) then
    begin
      TableExport.FieldByName('WKDESC').AsString :=
         Tablewk.FieldByName('DESCRIPTION').AsString;
      if TableExport.FieldByName('WKDESC').AsString = DummyWKStr then
         TableExport.FieldByName('WKDESC').AsString := '';
    end;
    if TableDept.FindKey([TableExport.FieldByName('PLANT_CODE').AsString,
      TableExport.FieldByName('DEPARTMENT_CODE').AsString]) then
        TableExport.FieldByName('DEPTDESC').AsString :=
          TableDept.FieldByName('DESCRIPTION').AsString;
   
end;

end.
