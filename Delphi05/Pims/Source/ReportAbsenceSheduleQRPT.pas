(*
  Changes:
    MRA:05-AUG-2008 RV008.
      - An order-by was missing, so the group-by went wrong. As a result
        the hours were not added together according to the group-by. (ORA10).
      - Link employee to plant instead of absence during select, to
        prevent absence is not shown if it has a different (employee-)plant.
      - Old bug solved. There are 13 week-values (1 to 13) that have to be
        filled, on several grouping-levels. This is done by using field-tags.
        But because these started at 1, 10, 20 and 30, there was an overlap,
        resulting in wrong values. This is solved by changing the tags and
        start-points to 1, 20, 40 and 60.
      - Show period based on year+weeks of selection.
    MRA:6-OCT-2009 RV037.
      - Selection plant/employee was wrong.
*)

unit ReportAbsenceSheduleQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

const
  MAXVAL=15;

type
  TQRParameters = class
  private
    FYear, FWeekFrom, FWeekTo, FEmployeeStatus: Integer;
    FShowTotal, FShowSelection, FShowWTR, FShowIll, FShowHol,
    FExportToFile: Boolean;
  public
    procedure SetValues(Year, WeekFrom, WeekTo, EmployeeStatus: Integer;
      ShowTotal, ShowSelection, ShowWTR, ShowIll, ShowHol,
      ExportToFile: Boolean);
  end;

  TWeekAmount = Array[1..13] of Integer;

  TReportAbsenceSheduleQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRBandGrpFooterEmpl: TQRBand;
    QRBandDetailEmployee: TQRBand;
    QRGroupHDAbs: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabelYear: TQRLabel;
    QRLabelQRShowEmpl: TQRLabel;
    QRBandTitleEmpl: TQRBand;
    QRShape5: TQRShape;
    QRLabel34: TQRLabel;
    QRShape6: TQRShape;
    QRBandSummary: TQRBand;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabelW1: TQRLabel;
    QRLabelW3: TQRLabel;
    QRLabelW4: TQRLabel;
    QRLabelW5: TQRLabel;
    QRLabelW6: TQRLabel;
    QRLabelW7: TQRLabel;
    QRLabelW8: TQRLabel;
    QRLabelW9: TQRLabel;
    QRLabelW10: TQRLabel;
    QRLabelW11: TQRLabel;
    QRLabelW12: TQRLabel;
    QRLabelW13: TQRLabel;
    QRLabelW2: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRGroupHDEmpl: TQRGroup;
    QRBandFooterAbs: TQRBand;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabelTotEmpl: TQRLabel;
    QRLabelTotAbs: TQRLabel;
    QRLabelTot: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape1: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDAbsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterAbsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGrpFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterAbsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDAbsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandGrpFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate, FMaxDate: TDateTime;
    FEmplWeek,
    FAbsWeek,
    FTotWeek, FEarnedWTRWeek: TWeekAmount;
    procedure InitArray(var WeekAmount: TWeekAmount);
    procedure FillCaptionWeeks(IndexMin: Integer; WeekAmount: TWeekAmount);
    function QRSendReportParameters(const PlantFrom, PlantTo, EmployeeFrom,
      EmployeeTo: String;
      const Year, WeekFrom, WeekTo,EmployeeStatus: Integer;
      const ShowTotal, ShowSelection, ShowWTR, ShowIll,
        ShowHol, ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportAbsenceSheduleQR: TReportAbsenceSheduleQR;
  // MR:08-09-2003
  ValueList: array[1..MAXVAL] of String;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAbsenceSheduleDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes;

procedure ExportDetail;
var
  I: Integer;
  Line: String;
begin
  Line := '';
  for I := 1 to MAXVAL - 1 do
    Line := Line + ValueList[I] + ExportClass.Sep;
  Line := Line + ValueList[MAXVAL];
  ExportClass.AddText(Line);
end;

procedure TQRParameters.SetValues(Year, WeekFrom, WeekTo,
  EmployeeStatus: Integer; ShowTotal, ShowSelection, ShowWTR, ShowIll,
  ShowHol, ExportToFile: Boolean);
begin
  FYear := Year;
  FWeekFrom := WeekFrom;
  FWeekTo := WeekTo;
  FEmployeeStatus := EmployeeStatus;
  FShowTotal := ShowTotal;
  FShowSelection := ShowSelection;
  FShowWTR := ShowWTR;
  FShowIll := ShowIll;
  FShowHol := ShowHol;
  FExportToFile := ExportToFile;
end;

function TReportAbsenceSheduleQR.QRSendReportParameters(const PlantFrom, PlantTo,
  EmployeeFrom, EmployeeTo: String;
  const Year, WeekFrom, WeekTo, EmployeeStatus: Integer;
  const ShowTotal, ShowSelection, ShowWTR, ShowIll, ShowHol,
    ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues( PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, WeekFrom, WeekTo, EmployeeStatus,
      ShowTotal, ShowSelection, ShowWTR, ShowIll, ShowHol, ExportToFile);
  end;
  SetDataSetQueryReport(ReportAbsenceSheduleDM.QueryAbsence);
end;

procedure TReportAbsenceSheduleQR.FillCaptionWeeks(IndexMin: Integer;
  WeekAmount: TWeekAmount);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 12) then
      begin
        if (Index <= IndexMin + (QRParameters.FWeekTo - QRParameters.FWeekFrom )) then
          if (IndexMin = 1) then
            (TempComponent as TQRLabel).Caption :=
              IntToStr(WeekAmount[Index - IndexMin + 1])
          else
            (TempComponent as TQRLabel).Caption :=
              DecodeHrsMin(WeekAmount[Index - IndexMin + 1])

        else
          (TempComponent as TQRLabel).Caption := '';
      end;
    end;
  end;
end;

function TReportAbsenceSheduleQR.ExistsRecords: Boolean;
var
  SelectStr, AbsStr: String;
begin
  Screen.Cursor := crHourGlass;
  {open all linked datasets}
//  Result := True;
  FMinDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekFrom, 1);
  FMaxDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekTo, 7);

  SelectStr := 
    'SELECT ' + NL +
    '  R.ABSENCETYPE_CODE, A.EMPLOYEE_NUMBER, A.ABSENCEHOUR_DATE, ' + NL +
    '  MAX(E.PLANT_CODE) AS PLANT_CODE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMMIN ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A, EMPLOYEE E, ' + NL +
    '  ABSENCEREASON R ' + NL +
    'WHERE ' + NL +
    '  R.ABSENCEREASON_ID = A.ABSENCEREASON_ID AND ' + NL +
    '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
    // RV037. Always select on plant (of employee)!
//  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
    SelectStr := SelectStr +
      ' AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' +
      ' AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  // RV037. Only select on employee when ONE plant has been selected.
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    SelectStr := SelectStr +
      ' AND '+ NL +
      '   ( ' + NL +
      '      E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom +
      '      AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo +
      '   ) ' + NL;

//absence reason - begin
  AbsStr := '';
  if QRParameters.FShowWTR then
    AbsStr := '''' + WORKTIMEREDUCTIONAVAILABILITY + '''';
  if QRParameters.FShowIll then
  begin
    if QRParameters.FShowWTR then
      AbsStr := AbsStr + ', ' ;
    AbsStr := AbsStr + '''' + ILLNESS + '''';
   end;
  if QRParameters.FShowHol then
  begin
    if (QRParameters.FShowWTR) or (QRParameters.FShowIll) then
      AbsStr := AbsStr + ', ' ;
    AbsStr := AbsStr + '''' + HOLIDAYAVAILABLETB + '''';
  end;
// - end
//active/notactive employee
  if (QRParameters.FEmployeeStatus = 0) then
    SelectStr := SelectStr + ' AND (E.STARTDATE <= :FDATECURRENT) AND ' +
      ' ((E.ENDDATE >= :FDATECURRENT) OR (ENDDATE IS NULL))';
  if (QRParameters.FEmployeeStatus = 1) then
    SelectStr := SelectStr + '  AND ((E.STARTDATE > :FDATECURRENT) ' +
      '  OR (E.ENDDATE < :FDATECURRENT)) ';
  SelectStr := SelectStr  +
    '  AND A.ABSENCEHOUR_DATE >= :FDATEFROM AND ' +
    '  A.ABSENCEHOUR_DATE <= :FDATETO ' +
    '  AND R.ABSENCETYPE_CODE IN (' + AbsStr + ') ' + NL +
    'GROUP BY ' + NL +
    '  R.ABSENCETYPE_CODE, A.EMPLOYEE_NUMBER, A.ABSENCEHOUR_DATE ' + NL +
    'ORDER BY ' + NL +
    '  R.ABSENCETYPE_CODE, A.EMPLOYEE_NUMBER, A.ABSENCEHOUR_DATE' + NL;
//- end
  ReportAbsenceSheduleDM.QueryAbsence.Active := False;
  ReportAbsenceSheduleDM.QueryAbsence.UniDirectional := False;
  ReportAbsenceSheduleDM.QueryAbsence.SQL.Clear;
  ReportAbsenceSheduleDM.QueryAbsence.SQL.Add(UpperCase(SelectStr));
  ReportAbsenceSheduleDM.QueryAbsence.ParamByName('FDATEFROM').Value := FMinDate;
  ReportAbsenceSheduleDM.QueryAbsence.ParamByName('FDATETO').Value :=  FMaxDate;
// ReportAbsenceSheduleDM.QueryAbsence.SQL.SaveToFile('c:\temp\reportabsenceschedule.sql');
  if (QRParameters.FEmployeeStatus <> 2) then
  begin
    ReportAbsenceSheduleDM.QueryAbsence.ParamByName('FDATECURRENT').Value := Now();
  end;
  if not ReportAbsenceSheduleDM.QueryAbsence.Prepared then
    ReportAbsenceSheduleDM.QueryAbsence.Prepare;
  ReportAbsenceSheduleDM.QueryAbsence.Active := True;
  if (ReportAbsenceSheduleDM.QueryAbsence.RecordCount = 0) then
    Result := False
  else
    Result := True;
  InitArray(FAbsWeek);
  InitArray(FEmplWeek);
  InitArray(FTotWeek);
  InitArray(FEarnedWTRWeek);
  {check if report is empty}
  Screen.Cursor := crDefault;
end;

procedure TReportAbsenceSheduleQR.ConfigReport;
var
  Count: Integer;
  WeekAmount: TWeekAmount;
begin
  // MR:08-09-2003
  ExportClass.ExportDone := False;

  Caption := SReport;
  if QRParameters.FShowWTR then
    Caption := Caption + SReportWorkTimeReduction;
  if QRParameters.FShowIll then
  begin
    if QRParameters.FShowWTR then
      Caption := Caption + ', ';
    Caption := Caption + SReportIllness;
  end;
  if QRParameters.FShowHol then
  begin
    if (QRParameters.FShowWTR or QRParameters.FShowIll) then
      Caption := Caption + ', ';
    Caption := Caption + SReportHoliday;
  end;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end
  else
  begin
    QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  end;
  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
  QRLabelWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
//  QRLabelWeekTo.Caption := IntToStr(QRParameters.FWeekTo);
  QRLabelWeekTo.Caption := IntToStr(QRParameters.FWeekTo) + ' ' +
    '(' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekFrom, 1)) + ' - ' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekTo, 7)) + ')';

  if QRParameters.FEmployeeStatus = 0 then
    QRLabelQRShowEmpl.Caption := SActiveEmpl;
  if QRParameters.FEmployeeStatus = 1 then
    QRLabelQRShowEmpl.Caption := SInActiveEmpl;
  if QRParameters.FEmployeeStatus = 2 then
    QRLabelQRShowEmpl.Caption := '';
  for Count := 1 to (QRParameters.FWeekTo - QRParameters.FWeekFrom + 1) do
    WeekAmount[Count] := QRParameters.FWeekFrom + Count - 1;
  FillCaptionWeeks(1, WeekAmount);
end;

procedure TReportAbsenceSheduleQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportAbsenceSheduleQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:08-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportAbsenceSheduleQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLblPlantFrom.Caption + ' ' + QRLabel16.Caption + ' ' +
        QRLblPlantTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' + QRLabel20.Caption + ' ' +
        QRLblEmployeeFrom.Caption + ' ' + QRLabel22.Caption + ' ' +
        QRLblEmployeeTo.Caption);
      ExportClass.AddText(QRLabel2.Caption + ' ' + QRLabelYear.Caption);
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' + QRLabelDate.Caption +
        ' ' + QRLabelWeekFrom.Caption + ' ' + QRLabelToDate.Caption + ' ' +
        QRLabelWeekTo.Caption);
    end;
  end;
  InitArray(FAbsWeek);
  InitArray(FEmplWeek);
  InitArray(FTotWeek);
  InitArray(FEarnedWTRWeek);
end;

procedure TReportAbsenceSheduleQR.InitArray(var WeekAmount: TWeekAmount);
var
  Count: Integer;
begin
   for Count := 1 to 13 do
     WeekAmount[Count] := 0;
end;

procedure TReportAbsenceSheduleQR.QRGroupHDAbsBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  InitArray(FAbsWeek);
end;

procedure TReportAbsenceSheduleQR.QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Count, Empl: Integer;
begin
  inherited;
  InitArray(FEmplWeek);
  InitArray(FEarnedWTRWeek);
  if (ReportAbsenceSheduleDM.QueryAbsence.FieldByName('ABSENCETYPE_CODE').AsString <>
    WORKTIMEREDUCTIONAVAILABILITY) then
    Exit;
  for Count := QRParameters.FWeekFrom to QRParameters.FWeekTo do
  begin
    Empl := ReportAbsenceSheduleDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    ReportAbsenceSheduleDM.TableEarnedWTR.FindNearest([QRParameters.FYear, Count, Empl]);
    if not ReportAbsenceSheduleDM.TableEarnedWTR.eof and
      (ReportAbsenceSheduleDM.TableEarnedWTR.FieldByName('EMPLOYEE_NUMBER').AsInteger =
      Empl) and
      (ReportAbsenceSheduleDM.TableEarnedWTR.FieldByName('EARNED_WTR_YEAR').AsInteger =
      QRParameters.FYear) and
      (ReportAbsenceSheduleDM.TableEarnedWTR.FieldByName('EARNED_WTR_Week').AsInteger =
      Count)then
      FEarnedWTRWeek[Count - QRParameters.FWeekFrom + 1] :=
        ReportAbsenceSheduleDM.TableEarnedWTR.FieldByName('EARNED_WTR_MINUTES').AsInteger;
  end;
end;

procedure TReportAbsenceSheduleQR.QRBandFooterAbsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count, FTotAbs: Integer;
begin
  inherited;
  PrintBand := QRParameters.FShowTotal;
  FTotAbs := 0;
  for count := 1 to 13 do
  begin
    FTotWeek[Count] := FTotWeek[Count] + FAbsWeek[Count];
    FTotAbs := FTotAbs + FAbsWeek[Count]
  end;
  if not PrintBand then
    Exit;
  QRLabelTotAbs.Caption :=  DecodeHrsMin(FTotAbs);
  FillCaptionWeeks(40, FAbsWeek);  
end;

procedure TReportAbsenceSheduleQR.QRBandGrpFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count, TotEmpl: Integer;
begin
  inherited;
  TotEmpl := 0;
  for Count := 1 to 13 do
   FEmplWeek[Count] := FEmplWeek[Count] - FEarnedWTRWeek[Count];
  for Count := 1 to 13 do
  begin
    FAbsWeek[Count] := FAbsWeek[Count] + FEmplWeek[Count];
    TotEmpl := TotEmpl + FEmplWeek[Count];
  end;
  QRLabelTotEmpl.Caption := DecodeHrsMin(TotEmpl);
  FillCaptionWeeks(20, FEmplWeek);
end;

procedure TReportAbsenceSheduleQR.QRBandSummaryBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  Count, Ftot: Integer;
begin
  inherited;
  FTot := 0;
  FillCaptionWeeks(60, FTotWeek);
  for count := 1 to 13 do
  begin
    FTot := FTot + FTotWeek[Count];
  end;
  QRLabelTot.Caption := DecodeHrsMin(FTot);
end;

procedure TReportAbsenceSheduleQR.QRDBText4Print(sender: TObject;
  var Value: String);
begin
  inherited;
//  Value := Copy(Value,0,20);
end;

procedure TReportAbsenceSheduleQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  AbsDate: TDateTime;
  Year, Week, Index: Word;
begin
  inherited;
  AbsDate :=
    ReportAbsenceSheduleDM.QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
  ListProcsF.WeekUitDat(AbsDate, Year, Week);
  Index := Week - QRParameters.FWeekFrom + 1;
  FEmplWeek[Index] := FEmplWeek[Index] +
    Round(ReportAbsenceSheduleDM.QueryAbsence.FieldByName('SUMMIN').AsFloat);
end;

procedure TReportAbsenceSheduleQR.QRBandTitleEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
  WeekAmount: TWeekAmount;
begin
  inherited;
  for Count := 1 to (QRParameters.FWeekTo - QRParameters.FWeekFrom + 1) do
    WeekAmount[Count] := QRParameters.FWeekFrom + Count - 1;
  FillCaptionWeeks(1, WeekAmount);
end;

procedure TReportAbsenceSheduleQR.QRBandTitleEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  ValueList[1] := '';
  ValueList[2] := QRLabelW1.Caption;
  ValueList[3] := QRLabelW2.Caption;
  ValueList[4] := QRLabelW3.Caption;
  ValueList[5] := QRLabelW4.Caption;
  ValueList[6] := QRLabelW5.Caption;
  ValueList[7] := QRLabelW6.Caption;
  ValueList[8] := QRLabelW7.Caption;
  ValueList[9] := QRLabelW8.Caption;
  ValueList[10] := QRLabelW9.Caption;
  ValueList[11] := QRLabelW10.Caption;
  ValueList[12] := QRLabelW11.Caption;
  ValueList[13] := QRLabelW12.Caption;
  ValueList[14] := QRLabelW13.Caption;
  ValueList[15] := QRLabel52.Caption;
  ExportDetail;
end;

procedure TReportAbsenceSheduleQR.QRGroupHDAbsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    with ReportAbsenceSheduleDM do
    begin
      ExportClass.AddText(
        QueryAbsence.FieldByName('ABSENCETYPE_CODE').AsString + ' ' +
        TableAbsenceType.FieldByName('DESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportAbsenceSheduleQR.QRBandGrpFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    with ReportAbsenceSheduleDM do
    begin
      ValueList[1] :=
        QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
        TableEmpl.FieldByName('DESCRIPTION').AsString;
    end;
    ValueList[2] := QRLabel6.Caption;
    ValueList[3] := QRLabel7.Caption;
    ValueList[4] := QRLabel8.Caption;
    ValueList[5] := QRLabel9.Caption;
    ValueList[6] := QRLabel10.Caption;
    ValueList[7] := QRLabel12.Caption;
    ValueList[8] := QRLabel14.Caption;
    ValueList[9] := QRLabel15.Caption;
    ValueList[10] := QRLabel17.Caption;
    ValueList[11] := QRLabel19.Caption;
    ValueList[12] := QRLabel21.Caption;
    ValueList[13] := QRLabel23.Caption;
    ValueList[14] := QRLabel24.Caption;
    ValueList[15] := QRLabelTotEmpl.Caption;
    ExportDetail;
  end;
end;

procedure TReportAbsenceSheduleQR.QRBandFooterAbsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    with ReportAbsenceSheduleDM do
    begin
      ValueList[1] := QRLabel5.Caption + ' ' +
        QueryAbsence.FieldByName('ABSENCETYPE_CODE').AsString + ' ' +
        TableAbsenceType.FieldByName('DESCRIPTION').AsString;
    end;
    ValueList[2] := QRLabel25.Caption;
    ValueList[3] := QRLabel26.Caption;
    ValueList[4] := QRLabel27.Caption;
    ValueList[5] := QRLabel28.Caption;
    ValueList[6] := QRLabel29.Caption;
    ValueList[7] := QRLabel30.Caption;
    ValueList[8] := QRLabel31.Caption;
    ValueList[9] := QRLabel32.Caption;
    ValueList[10] := QRLabel33.Caption;
    ValueList[11] := QRLabel35.Caption;
    ValueList[12] := QRLabel36.Caption;
    ValueList[13] := QRLabel37.Caption;
    ValueList[14] := QRLabel38.Caption;
    ValueList[15] := QRLabelTotAbs.Caption;
    ExportDetail;
  end;
end;

procedure TReportAbsenceSheduleQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    ValueList[1] := QRLabel3.Caption;
    ValueList[2] := QRLabel39.Caption;
    ValueList[3] := QRLabel40.Caption;
    ValueList[4] := QRLabel41.Caption;
    ValueList[5] := QRLabel42.Caption;
    ValueList[6] := QRLabel43.Caption;
    ValueList[7] := QRLabel44.Caption;
    ValueList[8] := QRLabel45.Caption;
    ValueList[9] := QRLabel46.Caption;
    ValueList[10] := QRLabel47.Caption;
    ValueList[11] := QRLabel48.Caption;
    ValueList[12] := QRLabel49.Caption;
    ValueList[13] := QRLabel50.Caption;
    ValueList[14] := QRLabel51.Caption;
    ValueList[15] := QRLabelTot.Caption;
    ExportDetail;
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

end.
