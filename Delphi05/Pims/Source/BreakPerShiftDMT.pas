(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
*)
unit BreakPerShiftDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TBreakPerShiftDM = class(TGridBaseDM)
    TableMasterSHIFT_NUMBER: TIntegerField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterSTARTTIME1: TDateTimeField;
    TableMasterENDTIME1: TDateTimeField;
    TableMasterSTARTTIME2: TDateTimeField;
    TableMasterENDTIME2: TDateTimeField;
    TableMasterSTARTTIME3: TDateTimeField;
    TableMasterENDTIME3: TDateTimeField;
    TableMasterSTARTTIME4: TDateTimeField;
    TableMasterENDTIME4: TDateTimeField;
    TableMasterSTARTTIME5: TDateTimeField;
    TableMasterENDTIME5: TDateTimeField;
    TableMasterSTARTTIME6: TDateTimeField;
    TableMasterENDTIME6: TDateTimeField;
    TableMasterSTARTTIME7: TDateTimeField;
    TableMasterENDTIME7: TDateTimeField;
    TableDetailSTARTTIME1: TDateTimeField;
    TableDetailENDTIME1: TDateTimeField;
    TableDetailSTARTTIME2: TDateTimeField;
    TableDetailENDTIME2: TDateTimeField;
    TableDetailSTARTTIME3: TDateTimeField;
    TableDetailENDTIME3: TDateTimeField;
    TableDetailSTARTTIME4: TDateTimeField;
    TableDetailENDTIME4: TDateTimeField;
    TableDetailSTARTTIME5: TDateTimeField;
    TableDetailENDTIME5: TDateTimeField;
    TableDetailSTARTTIME6: TDateTimeField;
    TableDetailENDTIME6: TDateTimeField;
    TableDetailSTARTTIME7: TDateTimeField;
    TableDetailENDTIME7: TDateTimeField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableMasterPLANTLU: TStringField;
    TableDetailBREAK_NUMBER: TIntegerField;
    TableDetailPAYED_YN: TStringField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailMUTATOR: TStringField;
    TableTempBreakPerShift: TTable;
    TableDetailPLANTLU: TStringField;
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BreakPerShiftDM: TBreakPerShiftDM;

implementation

uses SystemDMT, UPimsMessageRes, UPimsConst;

{$R *.DFM}

procedure TBreakPerShiftDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforePost(DataSet);
  SetNullValues(TableDetail);
  if TableDetail.FieldByName('BREAK_NUMBER').AsInteger = 0 then
    TableDetail.FieldByName('BREAK_NUMBER').AsInteger :=
      TableDetail.RecordCount + 1;
 { if ValidateBlocksPerShift(TableDetail, TableMaster) <> 0 then
    Abort;
  if ValidateIntervalTime(TableDetail, TableMaster, 'BREAKSPERSHIFT') <> 0then
    Abort;
  }
  if (CheckBlocks(BreakPerShiftDM.Handle_Grid, TableMaster, True) <> 0 ) then
  begin
    DisplayMessage(SPIMSTimeIntervalError, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerShiftDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  DefaultBeforeDelete(DataSet);
  if TableDetail.RecordCount <> TableDetailBREAK_NUMBER.Value then
  begin
    DisplayMessage(SPimsTBDeleteLastRecord, mtWarning, [mbOk]);
    Abort;
  end;
end;

procedure TBreakPerShiftDM.TableDetailNewRecord(DataSet: TDataSet);
var
  Index: Integer;
begin
  inherited;
  DefaultNewRecord(DataSet);
  TableDetailPAYED_YN.Value := UNCHECKEDVALUE;
  Index := TableDetail.RecordCount;
  TableDetail.FieldByName('BREAK_NUMBER').AsInteger := Index + 1;
  TableDetail.FieldByName('PAYED_YN').AsString := UNCHECKEDVALUE;
  if TableTempBreakPerShift.FindKey([TableMaster.FieldByName('PLANT_CODE').AsString,
    TableMaster.FieldByName('SHIFT_NUMBER').AsInteger, Index]) then
  for Index := 1 to 7 do
  begin
    DataSet.FieldByName('STARTTIME' + IntToStr(Index)).Value :=
      TableTempBreakPerShift.FieldByName('ENDTIME' + IntToStr(Index)).Value;
    DataSet.FieldByName('ENDTIME' + IntToStr(Index)).Value :=
      TableMaster.FieldByName('ENDTIME' + IntToStr(Index)).Value;;
  end
  else
    InitializeTimeValues(TableDetail, TableMaster);
end;

// RV050.8.
procedure TBreakPerShiftDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  SystemDM.PlantTeamFilterEnable(TableMaster);
end;

// RV050.8.
procedure TBreakPerShiftDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
