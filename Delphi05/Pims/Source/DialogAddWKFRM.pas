unit DialogAddWKFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables;

type
  TDialogAddWKF = class(TDialogSelectionF)
    GroupBox1: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    ComboBoxPlusFromDept: TComboBoxPlus;
    Label14: TLabel;
    ComboBoxPlusToDept: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    ComboBoxPlusFromWK: TComboBoxPlus;
    Label15: TLabel;
    ComboBoxPlusToWK: TComboBoxPlus;
    Label2: TLabel;
    DateFrom: TDateTimePicker;
    Label4: TLabel;
    TimeFrom: TdxTimeEdit;
    Label8: TLabel;
    DateTo: TDateTimePicker;
    Label6: TLabel;
    TimeTo: TdxTimeEdit;
    StoredProcADDWK: TStoredProc;
    procedure ComboBoxPlusFromDeptChange(Sender: TObject);
    procedure ComboBoxPlusToDeptChange(Sender: TObject);
    procedure ComboBoxPlusFromWKChange(Sender: TObject);
    procedure ComboBoxPlusToWKChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPlant: String;
    FDate: TDateTime;
  end;

var
  DialogAddWKF: TDialogAddWKF;

implementation

uses ListProcsFRM, DataCollectionEntryDMT, SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TDialogAddWKF.ComboBoxPlusFromDeptChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromDept.DisplayValue <> '') and
     (ComboBoxPlusToDept.DisplayValue <> '') then
    if (ComboBoxPlusFromDept.Value > ComboBoxPlusToDept.Value) then
      ComboBoxPlusToDept.DisplayValue := ComboBoxPlusFromDept.DisplayValue;
end;

procedure TDialogAddWKF.ComboBoxPlusToDeptChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromDept.DisplayValue <> '') and
     (ComboBoxPlusToDept.DisplayValue <> '') then
    if (ComboBoxPlusFromDept.Value >  ComboBoxPlusToDept.Value) then
      ComboBoxPlusFromDept.DisplayValue := ComboBoxPlusToDept.DisplayValue;
end;

procedure TDialogAddWKF.ComboBoxPlusFromWKChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromWK.DisplayValue <> '') and
     (ComboBoxPlusToWK.DisplayValue <> '') then
    if (ComboBoxPlusFromWK.Value > ComboBoxPlusToWK.Value) then
      ComboBoxPlusToWK.DisplayValue := ComboBoxPlusFromWK.DisplayValue;
end;

procedure TDialogAddWKF.ComboBoxPlusToWKChange(Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusFromWK.DisplayValue <> '') and
     (ComboBoxPlusToWK.DisplayValue <> '') then
    if (ComboBoxPlusFromWK.Value > ComboBoxPlusToWK.Value) then
      ComboBoxPlusFromWK.DisplayValue := ComboBoxPlusToWK.DisplayValue;
end;

procedure TDialogAddWKF.FormShow(Sender: TObject);
begin
  inherited;
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryDeptPlant, ComboBoxPlusFromDept,
    FPlant, '', True, 'PLANT_CODE', '', 'DEPARTMENT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryDeptPlant, ComboBoxPlusToDept,
    FPlant, '', False, 'PLANT_CODE', '', 'DEPARTMENT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryWKPlant, ComboBoxPlusFromWK,
    FPlant, '', True, 'PLANT_CODE', '', 'WORKSPOT_CODE', 'DESCRIPTION');
  ListProcsF.FillComboBox(DataCollectionEntryDM.QueryWKPlant, ComboBoxPlusToWK,
    FPlant, '', False, 'PLANT_CODE', '', 'WORKSPOT_CODE', 'DESCRIPTION');
  DateFrom.Date := FDate;
  DateTo.Date := FDate;
end;

procedure TDialogAddWKF.btnOkClick(Sender: TObject);
var
  SDate, EDate: TDateTime;
begin
  inherited;
  SDate := GetDate(DateFrom.Date) + TimeFrom.Time;
  EDate := GetDate(DateTo.Date) + TimeTo.Time ;
  if (SDate > EDate) then
  begin
    DisplayMessage( SPimsStartEndDateTime, mtInformation, [mbOk]);
    Exit;
  end;
  StoredProcADDWK.Prepare;
  StoredProcADDWK.ParamByName('PLANT_CODE').asString := FPlant;
  StoredProcADDWK.ParamByName('DEPT_FROM').asString :=
    GetStrValue(ComboBoxPlusFromDept.DisplayValue);
  StoredProcADDWK.ParamByName('DEPT_TO').asString :=
    GetStrValue(ComboBoxPlusToDept.DisplayValue);
  StoredProcADDWK.ParamByName('WK_FROM').asString :=
    GetStrValue(ComboBoxPlusFromWK.DisplayValue);
  StoredProcADDWK.ParamByName('WK_TO').asString :=
    GetStrValue(ComboBoxPlusToWK.DisplayValue);
  StoredProcADDWK.ParamByName('SDATE').asDateTime := SDate;
  StoredProcADDWK.ParamByName('EDATE').asDateTime := EDate;
  StoredProcADDWK.ParamByName('CREATIONDATE').asdateTime := Now;
  StoredProcADDWK.ParamByName('MUTATIONDATE').asdateTime := Now;
  StoredProcADDWK.ParamByName('MUTATOR').asString :=
  	SystemDM.CurrentProgramUser;
  StoredProcADDWK.ExecProc;

  DisplayMessage(SADDWKDateCollection , mtInformation, [mbOk]);
  DialogAddWKF.Close;
end;

procedure TDialogAddWKF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
  begin
    DateTo.Date := DateFrom.Date;
    TimeTo.Time := TimeFrom.Time;
  end;

end;

procedure TDialogAddWKF.DateToChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
  begin
    DateFrom.Date := DateTo.Date;
    TimeFrom.Time := TimeTo.Time;
  end;
end;

end.
