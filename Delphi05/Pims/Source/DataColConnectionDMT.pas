(*
  MRA:10-SEP-2012 TD-21191
  - Use check-digit for reading blackboxes.
  - An extra field 'Checkdigits' must be added for
    this purpose. It can be left empty.
  MRA:26-APR-2013 TD-22475
  - Add copy/paste buttons to make it possible to copy datacol-connections
    from one workstation to another.
  MRA:14-JAN-2016 PIM-133
  - Datacollection connection copy does not always work
  - With CLO-database this did not work.
  - Cause: Field PROCESS_NUMBER did not exist in the CLO-DB. To solve
           it do not copy this field.
*)
unit DataColConnectionDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TDataColConnectionDM = class(TGridBaseDM)
    TableSerialDevice: TTable;
    DataSourceSerialDevice: TDataSource;
    TableSerialDeviceSERIALDEVICE_CODE: TStringField;
    TableSerialDeviceNAME: TStringField;
    TableSerialDeviceDEVICETYPE: TStringField;
    TableSerialDeviceBAUDRATE: TIntegerField;
    TableSerialDevicePARITY: TStringField;
    TableSerialDeviceDATABITS: TIntegerField;
    TableSerialDeviceSTOPBITS: TIntegerField;
    TableSerialDeviceINITSTRING: TStringField;
    TableSerialDeviceINITCHAR1: TIntegerField;
    TableSerialDeviceINITCHAR2: TIntegerField;
    TableSerialDeviceINITCHAR3: TIntegerField;
    TableSerialDeviceVERIFYSTRING: TStringField;
    TableSerialDeviceVERIFYPOSITION: TIntegerField;
    TableSerialDeviceENDCHARACTER1: TIntegerField;
    TableSerialDeviceENDCHARACTER2: TIntegerField;
    TableSerialDeviceCREATIONDATE: TDateTimeField;
    TableSerialDeviceMUTATIONDATE: TDateTimeField;
    TableSerialDeviceMUTATOR: TStringField;
    TableDetailCOMPUTER_NAME: TStringField;
    TableDetailCOMPORT: TIntegerField;
    TableDetailSERIALDEVICE_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterCOMPUTER_NAME: TStringField;
    TableMasterLICENSEVERSION: TIntegerField;
    TableDetailSERIALDEVICE_DESCRIPTIONLU: TStringField;
    TableDetailCOUNTER: TIntegerField;
    TableDetailINTERFACE_CODE: TStringField;
    TableDetailADDRESS: TIntegerField;
    TableSerialDeviceVALUEPOSITION: TIntegerField;
    TableDetailIPADDRESS: TStringField;
    TableDetailPORT: TStringField;
    TableDetailCHECKDIGITS: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function DataColConnectionFor(AComputerName: String): Boolean;
    procedure CopyDataColConnection(ASrcComputerName,
      ADestComputerName: String; ADataColConnectionDefined: Boolean);
  end;

var
  DataColConnectionDM: TDataColConnectionDM;

implementation

{$R *.DFM}

uses
  SystemDMT, UPimsMessageRes;

procedure TDataColConnectionDM.DataModuleCreate(Sender: TObject);
begin
//  inherited;
  TableDetail.MasterSource := DataSourceMaster;
  TableDetail.MasterFields := 'COMPUTER_NAME';
  TableDetail.IndexFieldNames := 'COMPUTER_NAME;COMPORT;ADDRESS;COUNTER';
  inherited;

end;

procedure TDataColConnectionDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableDetailCOMPORT.Value := 1;
  TableDetailADDRESS.Value := 1;
  TableDetailCOUNTER.Value := 1;
end;

procedure TDataColConnectionDM.TableDetailBeforePost(DataSet: TDataSet);
var
  Value: Integer;
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if TableDetailCHECKDIGITS.Value <> '' then
  begin
    if Length(TableDetailCHECKDIGITS.Value) <> 2 then
    begin
      DisplayMessage(SPimsInvalidCheckDigits, mtInformation, [mbOK]);
      SysUtils.Abort;
      Exit;
    end
    else
    begin
      try
        Value := StrToInt(TableDetailCHECKDIGITS.Value);
        if (Value < 10) or (Value > 99) then
        begin
          DisplayMessage(SPimsInvalidCheckDigits, mtInformation, [mbOK]);
          SysUtils.Abort;
          Exit;
        end;
      except
        DisplayMessage(SPimsInvalidCheckDigits, mtInformation, [mbOK]);
        SysUtils.Abort;
        Exit;
      end;
    end;
  end;
end;

// TD-22475
function TDataColConnectionDM.DataColConnectionFor(
  AComputerName: String): Boolean;
var
  Found: Integer;
begin
  Found := SystemDM.GetDBValue(
    Format('SELECT COUNT(*) FROM DATACOLCONNECTION D ' +
           ' WHERE D.COMPUTER_NAME = ''%s''', [AComputerName]),
           0);
  Result := Found <> 0;
end;

// TD-22475
procedure TDataColConnectionDM.CopyDataColConnection(ASrcComputerName,
  ADestComputerName: String; ADataColConnectionDefined: Boolean);
begin
  if ADataColConnectionDefined then
    SystemDM.ExecSql(
      Format('DELETE FROM DATACOLCONNECTION D ' +
             ' WHERE D.COMPUTER_NAME = ''%s''', [ADestComputerName])
             );
  // PIM-133 Leave out field PROCESS_NUMBER to prevent copy-problems.
  SystemDM.ExecSql(
    Format(
      'INSERT INTO DATACOLCONNECTION ' +
      '  (COMPUTER_NAME, COMPORT, ADDRESS, COUNTER, SERIALDEVICE_CODE, ' +
      '   INTERFACE_CODE, CREATIONDATE, MUTATIONDATE, MUTATOR, ' +
      '   IPADDRESS, PORT, CHECKDIGITS) ' +
      'SELECT ''%s'', D.COMPORT, D.ADDRESS, D.COUNTER, D.SERIALDEVICE_CODE, ' +
      '  D.INTERFACE_CODE, SYSDATE, SYSDATE, ''%s'', ' +
      '  D.IPADDRESS, D.PORT, D.CHECKDIGITS ' +
      'FROM DATACOLCONNECTION D ' +
      'WHERE D.COMPUTER_NAME = ''%s'' ',
      [ADestComputerName, SystemDM.CurrentProgramUser, ASrcComputerName]
    )
  );
  TableDetail.Refresh;
end;

end.
