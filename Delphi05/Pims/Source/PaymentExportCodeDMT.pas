unit PaymentExportCodeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TPaymentExportCodeDM = class(TGridBaseDM)
    TableMasterPAYMENT_EXPORT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableDetailPAYMENT_EXPORT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PaymentExportCodeDM: TPaymentExportCodeDM;

implementation

uses SystemDMT;

{$R *.DFM}


end.
