inherited DialogSelectionCopyFromOWF: TDialogSelectionCopyFromOWF
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Selection copy from other week'
  ClientHeight = 104
  ClientWidth = 326
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 44
    Width = 326
    inherited btnOk: TBitBtn
      Left = 48
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 162
    end
  end
  inherited stbarBase: TStatusBar
    Top = 85
    Width = 326
  end
  object GroupBox6: TGroupBox
    Left = 0
    Top = 0
    Width = 326
    Height = 44
    Align = alClient
    Caption = 'Week'
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 90
      Height = 13
      Caption = 'Week to copy from'
    end
    object dxSpinEditWeek: TdxSpinEdit
      Left = 248
      Top = 12
      Width = 69
      TabOrder = 0
      MaxValue = 53
      Value = 1
      StoredValues = 48
    end
  end
end
