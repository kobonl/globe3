(*
  SO: 05-JUL-2010 RV067.3. 550494
    PIMS User rights related issues
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase-form.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:29-JAN-2019 GLOB3-229
    - When Show per Timeblock is check it will not show totals, even
      if this is clicked. To prevent confusion:
      - Uncheck Show totals if Show per timeblock is checked.
    MRA:18-MAR-2019 GLOB3-211
    - Report Staff Planning start with Show not planned employees unchecked
    - Because of wrong values for totals
    MRA:1-APR-2019 PIM-377
    - Show start and end times in planning reports
    - Sort on employee (show all on employee-level, instead of on workspot-level)
*)
unit DialogReportStaffPlanningFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT, jpeg;

type
  TDialogReportStaffPlanningF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    CheckBoxPagePlant: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    CheckBoxPaidBreaks: TCheckBox;
    ComboBoxPlusShiftFrom: TComboBoxPlus;
    ComboBoxPlusShiftTo: TComboBoxPlus;
    QueryShift: TQuery;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    CheckBoxPageShift: TCheckBox;
    CheckBoxOnlyPlanDev: TCheckBox;
    QueryWorkSpot: TQuery;
    QueryWorkSpotWORKSPOT_CODE: TStringField;
    QueryWorkSpotDESCRIPTION: TStringField;
    QueryWorkSpotPLANT_CODE: TStringField;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    CheckBoxShowTB: TCheckBox;
    CheckBoxShowTotals: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    CheckBoxShowNotPlannedEmployees: TCheckBox;
    CheckBoxShowStartEndTimes: TCheckBox;
    CheckBoxSortOnEmployee: TCheckBox;
    RGrpSortOnEmployee: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillShift;
    procedure DateFromChange(Sender: TObject);
    procedure ComboBoxPlusShiftFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBoxShowTBClick(Sender: TObject);
  private
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportStaffPlanningF: TDialogReportStaffPlanningF;

// RV089.1.
function DialogReportStaffPlanningForm: TDialogReportStaffPlanningF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportStaffPlanningDMT, ReportStaffPlanningQRPT,
  ListProcsFRM, CalculateTotalHoursDMT, UPimsMessageRes;

// RV089.1.
var
  DialogReportStaffPlanningF_HND: TDialogReportStaffPlanningF;

// RV089.1.
function DialogReportStaffPlanningForm: TDialogReportStaffPlanningF;
begin
  if (DialogReportStaffPlanningF_HND = nil) then
  begin
    DialogReportStaffPlanningF_HND := TDialogReportStaffPlanningF.Create(Application);
    with DialogReportStaffPlanningF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportStaffPlanningF_HND;
end;

// RV089.1.
procedure TDialogReportStaffPlanningF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportStaffPlanningF_HND <> nil) then
  begin
    DialogReportStaffPlanningF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportStaffPlanningF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportStaffPlanningF.btnOkClick(Sender: TObject);
var
  DateTmpFrom, DateTmpTo: TDateTime;
begin
  inherited;
  if Int(DateTo.Date - DateFrom.Date) >= 7 then
  begin
    DisplayMessage(SWeek, mtInformation, [mbYes]);
    Exit;
  end;
  if Int(DateTo.Date - DateFrom.Date) < 0 then
  begin
    DisplayMessage(SDateFromTo, mtInformation, [mbYes]);
    Exit;
  end;
// CAR 12.12.2002
  DateTmpFrom := GetDate(DateFrom.Date);
  DateTmpTo := GetDate(DateTo.Date);
// END
  if ReportStaffPlanningQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value), GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    IntToStr(GetIntValue(ComboBoxPlusShiftFrom.Value)),
    IntToStr(GetIntValue(ComboBoxPlusShiftTO.Value)),
    DateTmpFrom, DateTmpTo,
    CheckBoxOnlyPlanDev.Checked,
    CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
    CheckBoxPageShift.Checked, CheckBoxPageWK.Checked,
    CheckBoxPaidBreaks.Checked, CheckBoxShowTB.Checked, CheckBoxAllTeams.Checked,
    CheckBoxShowTotals.Checked,
    CheckBoxExport.Checked,
    CheckBoxShowNotPlannedEmployees.Checked,
    CheckBoxShowStartEndTimes.Checked,
    CheckBoxSortOnEmployee.Checked,
    RGrpSortOnEmployee.ItemIndex)
  then
    ReportStaffPlanningQR.ProcessRecords;
end;

procedure TDialogReportStaffPlanningF.FillShift;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
    ComboBoxPlusShiftFrom.Visible := True;
    ComboBoxPlusShiftTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusShiftFrom.Visible := False;
    ComboBoxPlusShiftTo.Visible := False;
  end;
   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportStaffPlanningF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, True, False, False, True, False);
  inherited;

  ListProcsF.WeekUitDat(Now, Year, Week);
  DateFrom.Date := Int(Now + 1);
  DateTo.Date := InT(Now + 7);

  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageShift.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxOnlyPlanDev.Checked := False;
  CheckBoxShowTB.Checked := True;
  CheckBoxShowTotals.Checked := False;
   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportStaffPlanningF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportStaffPlanningDM := CreateReportDM(TReportStaffPlanningDM);
  ReportStaffPlanningQR := CreateReportQR(TReportStaffPlanningQR);
  CheckBoxShowStartEndTimes.Checked := SystemDM.RepPlanStartEndTime; // PIM-377
  CheckBoxSortOnEmployee.Checked := SystemDM.RepPlanStartEndTime; // PIM-377
end;

procedure TDialogReportStaffPlanningF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateTo.Date - DateFrom.Date >= 7 then
    DateTo.Date := DateFrom.Date + 6;
  if DateTo.Date - DateFrom.Date < 0 then
    DateTo.Date := DateFrom.Date ;
end;

procedure TDialogReportStaffPlanningF.ComboBoxPlusShiftFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
      GetIntValue(ComboBoxPlusShiftTo.Value) then
        ComboBoxPlusShiftTo.DisplayValue := ComboBoxPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportStaffPlanningF.ComboBoxPlusShiftToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
       GetIntValue(ComboBoxPlusShiftTo.Value) then
         ComboBoxPlusShiftFrom.DisplayValue := ComboBoxPlusShiftTo.DisplayValue;
end;

procedure TDialogReportStaffPlanningF.FillAll;
begin
  inherited;
  FillShift;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportStaffPlanningF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportStaffPlanningF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// GLOB3-229
procedure TDialogReportStaffPlanningF.CheckBoxShowTBClick(Sender: TObject);
begin
  inherited;
  if CheckBoxShowTB.Checked then
  begin
    CheckBoxShowTotals.Enabled := False;
    CheckBoxShowTotals.Checked := False;
    CheckBoxSortOnEmployee.Enabled := True;
  end
  else
  begin
    CheckBoxShowTotals.Enabled := True;
    CheckBoxSortOnEmployee.Enabled := False;
    CheckBoxSortOnEmployee.Checked := False;
  end;
end;

end.
