(*
  MRA:7-DEC-2009 RV048.1. 889959.
  - Workspot-dialog: Do not allow changing the department of
    the workspot, when planning has been made on that workspot+department.
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machines.
  MRA:17-JAN-2011 RV085.1.
  - Currently it is not possible to change the department for a workspot
    when any planning exists (in past or future).
  - Allow changing of the department, but first search the last-plan-date
    that could be found for, show that to the user and if user
    confirms, then allow changing.
  - Look only for planning for today or in the future!
  MRA:19-JAN-2011 RV085.5. Bugfix.
  - Workspots
    - This shows all hourtypes:
      - It should show hourtypes per country (based on country of the plant).
  - If there are NO hourtypes per country defined, then it should show
    all hourtypes.
  MRA:10-FEB-2011 RV086.2. SC-50014392.
  - Plan in other plants, related issues:
    - When teams-per-users-filtering is used, you still see
      plants/teams/department/workspots not belonging to the
      teams-per-user-selection.
  MRA:11-JUL-211 RV094.7. Bugfix
  - Show message when delete/modify is not allowed based
    on system setting (update employee-checkbox).
  MRA:1-NOV-2011. RV100.1. 20012124. Tailor made CVL.
  - Add field to enter an 'overruled contract group'.
    - This will then be used to determine hour type based
      on exceptional hour definitions and overtime definitions
      linked to that 'overruled contract group', instead of using
      the hour type linked to the workspot.
  MRA:26-JUN-2012.
  - Use last entered workspot code as default for compare jobs.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - Addition of field 'Ignore quantities in reports' (checkbox).
  MRA:18-OCT-2012 20013551 Info Tunnels
  - Addition of 1 field EFF_RUNNING_HRS_YN.
  MRA:23-JAN-2013 20013910 Planning on departments gives problems.
  - Related to this order:
    - When a department is changed for a given workspot,
      then also change the department from old-department to
      new-department for:
      - STANDARDOCCUPATION
      - STANDARDEMPLOYEEPLANNING
      - EMPLOYEEPLANNING (only for today/future records).
  MRA:4-FEB-2013 20013923 / TD-24853
  - Make separate settings for update/delete for plant/employee/workspot
  MRA:20-NOV-2013 20013196
  - Multiple counters for workspot
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:17-JUN-2014 20015178.70
  - Make default-job default functionality
  MRA:8-SEP-2014 20013476
  - Make CURRENT variable
  - Addition of Sample Time Minutes-field on Job-level.
  MRA:23-DEC-2014 20016016
  - Added setting for Personal Screen: Receive qty via
    blackbox or external application.
  MRA:19-OCT-2017 PIM-317
  - Add Batch field to Workspot for use with TimerecordingCR
  MRA:4-JAN-2019 GLOB3-202
  - Scan via machine-card readers and do not scan out
*)

unit WorkSpotDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TWorkSpotDM = class(TGridBaseDM)
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    TableMasterFAX: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterINSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterINSCAN_MARGIN_LATE: TIntegerField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField;
    TableMasterOUTSCAN_MARGIN_LATE: TIntegerField;
    TableHourtype: TTable;
    TableDetailPLANT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailWORKSPOT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailUSE_JOBCODE_YN: TStringField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailMEASURE_PRODUCTIVITY_YN: TStringField;
    TableDetailPRODUCTIVE_HOUR_YN: TStringField;
    TableDetailQUANT_PIECE_YN: TStringField;
    TableDetailAUTOMATIC_DATACOL_YN: TStringField;
    TableDetailCOUNTER_VALUE_YN: TStringField;
    TableDetailENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableDetailDATE_INACTIVE: TDateTimeField;
    TableDetailHOURTYPELU: TStringField;
    TableBUWorkspot: TTable;
    TableJobCode: TTable;
    TableJobCodePLANT_CODE: TStringField;
    TableJobCodeWORKSPOT_CODE: TStringField;
    TableJobCodeJOB_CODE: TStringField;
    TableJobCodeBUSINESSUNIT_CODE: TStringField;
    TableJobCodeDESCRIPTION: TStringField;
    TableJobCodeNORM_PROD_LEVEL: TFloatField;
    TableJobCodeBONUS_LEVEL: TFloatField;
    TableJobCodeNORM_OUTPUT_LEVEL: TFloatField;
    TableJobCodeINTERFACE_CODE: TStringField;
    TableJobCodeDATE_INACTIVE: TDateTimeField;
    TableJobCodeCREATIONDATE: TDateTimeField;
    TableJobCodeMUTATIONDATE: TDateTimeField;
    TableJobCodeMUTATOR: TStringField;
    TableBUWorkspotPLANT_CODE: TStringField;
    TableBUWorkspotWORKSPOT_CODE: TStringField;
    TableBUWorkspotBUSINESSUNIT_CODE: TStringField;
    TableBUWorkspotPERCENTAGE: TFloatField;
    TableBUWorkspotCREATIONDATE: TDateTimeField;
    TableBUWorkspotMUTATIONDATE: TDateTimeField;
    TableBUWorkspotMUTATOR: TStringField;
    DataSourceJobCode: TDataSource;
    DataSourceBUWorkSpot: TDataSource;
    TableBUWorkspotBUSINESSUNITLU: TStringField;
    QueryDel: TQuery;
    QueryWORK: TQuery;
    TableDetailAUTOMATIC_RESCAN_YN: TStringField;
    TableDetailSHORT_NAME: TStringField;
    TableJobCodeIGNOREINTERFACECODE_YN: TStringField;
    TableJobCodeINTERFACELU: TStringField;
    TableIgnoreInterface: TTable;
    TableIgnoreInterfacePLANT_CODE: TStringField;
    TableIgnoreInterfaceWORKSPOT_CODE: TStringField;
    TableIgnoreInterfaceJOB_CODE: TStringField;
    TableIgnoreInterfaceINTERFACE_CODE: TStringField;
    TableIgnoreInterfaceCREATIONDATE: TDateTimeField;
    TableIgnoreInterfaceMUTATIONDATE: TDateTimeField;
    TableIgnoreInterfaceMUTATOR: TStringField;
    TableJobCodeBULU: TStringField;
    DataSourceIgnoreInterface: TDataSource;
    TableJobForInterface: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    StringField6: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    StringField7: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    QueryInterface: TQuery;
    TableIgnoreTmp: TTable;
    TableJobCodeSHOW_AT_SCANNING_YN: TStringField;
    TableJobCodeCOMPARATION_JOB_YN: TStringField;
    TableJobCodeMAXIMUM_DEVIATION: TFloatField;
    TableCompareJOb: TTable;
    DataSourceCompareJob: TDataSource;
    TableCompareJObPLANT_CODE: TStringField;
    TableCompareJObWORKSPOT_CODE: TStringField;
    TableCompareJObJOB_CODE: TStringField;
    TableCompareJObCOMPARE_WORKSPOT_CODE: TStringField;
    TableCompareJObCOMPARE_JOB_CODE: TStringField;
    TableCompareJObCREATIONDATE: TDateTimeField;
    TableCompareJObMUTATIONDATE: TDateTimeField;
    TableCompareJObMUTATOR: TStringField;
    TableJobCodeCOMPARISON_REJECT_YN: TStringField;
    TableJobCodeSHOW_AT_PRODUCTIONSCREEN_YN: TStringField;
    TableDetailDEPTLU: TStringField;
    QueryDeptLU: TQuery;
    DataSourceDeptLU: TDataSource;
    QueryGetSumPerc: TQuery;
    QueryCheckOnWKInterface: TQuery;
    QueryBU: TQuery;
    DataSourceBULU: TDataSource;
    TableCompareJObCOMPAREWKLU: TStringField;
    QueryAllJob: TQuery;
    TableCompareJObCOMPAREJOBLU: TStringField;
    QueryAllWK: TQuery;
    QueryWKLU: TQuery;
    DataSourceWKLU: TDataSource;
    QueryJobLU: TQuery;
    DataSourceJOBLU: TDataSource;
    QueryWKLUPLANT_CODE: TStringField;
    QueryWKLUWORKSPOT_CODE: TStringField;
    QueryWKLUDESCRIPTION: TStringField;
    StoredProcDelete: TStoredProc;
    QueryJobLUPLANT_CODE: TStringField;
    QueryJobLUWORKSPOT_CODE: TStringField;
    QueryJobLUJOB_CODE: TStringField;
    QueryJobLUDESCRIPTION: TStringField;
    TableDetailGRADE: TIntegerField;
    qryOpenScans: TQuery;
    QueryInterfacePLANT_CODE: TStringField;
    QueryInterfaceWORKSPOT_CODE: TStringField;
    QueryInterfaceDESCRIPTION: TStringField;
    QueryInterfaceJOB_CODE: TStringField;
    QueryInterfaceDESCRIPTION_1: TStringField;
    QueryInterfaceINTERFACE_CODE: TStringField;
    TableDetailGROUP_EFFICIENCY_YN: TStringField;
    qryEmpPlanPWD: TQuery;
    TableDetailMACHINE_CODE: TStringField;
    TableDetailMACHINELU: TStringField;
    qryMachineLU: TQuery;
    qryCheckMachineJobs: TQuery;
    qryMachineJobExists: TQuery;
    qryCheckWorkspotJobs: TQuery;
    qryAddMachJobsToWS: TQuery;
    TableHourtypePerCountry: TTable;
    TableMasterAVERAGE_WAGE: TFloatField;
    TableMasterCUSTOMER_NUMBER: TIntegerField;
    TableMasterCOUNTRY_ID: TFloatField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    qryContractGroup: TQuery;
    TableDetailCONTRACTLU: TStringField;
    qryCompareJobCodeWK: TQuery;
    TableJobCodeEMPS_SCAN_IN_YN: TStringField;
    TableJobCodeIGNOREQUANTITIESINREPORTS_YN: TStringField;
    TableDetailEFF_RUNNING_HRS_YN: TStringField;
    TableJobCodePRODLEVELMINPERC: TIntegerField;
    TableJobCodeMACHOUTPUTMINPERC: TIntegerField;
    qryStandardOccupationUpdate: TQuery;
    qryStandardEmployeePlanningUpdate: TQuery;
    qryEmployeePlanningUpdate: TQuery;
    qryShiftOverlap: TQuery;
    TableLinkJob: TTable;
    DataSourceLinkJob: TDataSource;
    TableJob: TTable;
    DataSourceJob: TDataSource;
    TableWorkspot: TTable;
    StringField8: TStringField;
    StringField12: TStringField;
    StringField13: TStringField;
    DateTimeField4: TDateTimeField;
    StringField14: TStringField;
    DateTimeField5: TDateTimeField;
    StringField15: TStringField;
    StringField16: TStringField;
    IntegerField1: TIntegerField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    DateTimeField6: TDateTimeField;
    StringField23: TStringField;
    StringField24: TStringField;
    StringField25: TStringField;
    StringField26: TStringField;
    IntegerField2: TIntegerField;
    StringField27: TStringField;
    StringField28: TStringField;
    StringField29: TStringField;
    StringField30: TStringField;
    StringField31: TStringField;
    StringField32: TStringField;
    DataSourceWorkspot: TDataSource;
    qryLinkJob: TQuery;
    qryLinkJobDel: TQuery;
    qryLinkJobWS: TQuery;
    TableDetailTIMEREC_BY_MACHINE_YN: TStringField;
    TableJobCodeDEFAULT_YN: TStringField;
    qryResetDefaultJob: TQuery;
    TableDetailFAKE_EMPLOYEE_NUMBER: TIntegerField;
    qryFakeEmpExists: TQuery;
    TableJobCodeSAMPLE_TIME_MINS: TIntegerField;
    TableDetailPS_QTYVIA_EXTERNAL_YN: TStringField;
    TableDetailHOST: TStringField;
    TableDetailPORT: TFloatField;
    qryHostPortExists: TQuery;
    TableDetailBATCH: TFloatField;
    TableDetailROAMING_YN: TStringField;
    qryRoamingWorkspotExists: TQuery;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableJobCodeBeforePost(DataSet: TDataSet);
    procedure TableJobCodeNewRecord(DataSet: TDataSet);
    procedure TableBUWorkspotBeforePost(DataSet: TDataSet);
    procedure TableBUWorkspotNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure TableJobCodeAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableCompareJObNewRecord(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
    procedure TableDetailBeforeInsert(DataSet: TDataSet);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableHourtypeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure QueryDeptLUFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableLinkJobBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    FOldDepartmentCode: String;
    FMyRowsAffected: Integer;
    function CheckMachineJobs(APlantCode, AWorkspotCode,
      AMachineCode: String): Boolean;
    procedure ResetDefaultJob(APlantCode, AWorkspotCode, AJobCode: String);
    function FakeEmployeeNumberExists: Boolean;
  public
    { Public declarations }
    FCallNewForm, FChangeRescan, FIgnore: Boolean;
    FJob, FInterfaceCode: String;
    FAutomaticYN, FEnterCounterYN, FCounterYN, FAutoRescanYN, FMeasureProd: Boolean;
    function CheckRelatedRecords: boolean;
    function GetSumPercentage(PlantCode, WKCode: String): Double;
    procedure DeleteDetailsWK;
    function GetValueYN(ValueBool: Boolean): String;
    function GetBoolYN(ValueYN: String): Boolean;
    function CheckOtherWKInterfaceCode(WKCode, InterfaceCode: String): Boolean;
    function CheckOnWKInterfaceCode(Plant,  WKCode, JOBCODE, InterfaceCode: String): Boolean;
    function CheckOnWKAllInterfaceCode(Plant, WKCode: String): Boolean;
    procedure SelectOtherInterfaceCode(InterfaceDefined: String; var RecCount: Integer);
    procedure DeleteOtherInterfaceCodes;
    function CountRecordOfIgnoreInterface: Boolean;
    procedure DeleteCompareJob;
    procedure OpenQueryForCompareJob;
    function CheckForOpenScans: Boolean;
    function EmpPlanPWDExists: TDateTime;
    function MachineJobExists(APlantCode, AMachineCode, AJobCode: String): Boolean;
    function AddMachineJobs(APlantCode, AWorkspotCode,
      AMachineCode: String): Boolean;
    procedure ChangeDepartmentAction(APlantCode, AWorkspotCode,
      AOldDepartmentCode, ANewDepartmentCode: String);
    function LinkJobExists(APlantCode, AWorkspotCode, AJobCode: String): Boolean;
    procedure LinkJobDelete(APlantCode, AWorkspotCode, AJobCode: String);
    function FakeEmployeeNumber: Integer;
    property OldDepartmentCode: String read FOldDepartmentCode
      write FOldDepartmentCode;
    property MyRowsAffected: Integer read FMyRowsAffected write FMyRowsAffected;
  end;

var
  WorkSpotDM: TWorkSpotDM;

implementation

{$R *.DFM}

uses
  UPimsMessageRes, SystemDMT, UPimsConst, WorkspotFRM, GridBaseFRM,
  JobCodeFRM, BUPerWorkSpotFRM, UGlobalFunctions;

function TWorkSpotDM.CheckRelatedRecords: Boolean;
begin
  Result := True;
  FCallNewForm := False;
  if (TableDetail.FieldByName('USE_JOBCODE_YN').AsString = CHECKEDVALUE) then
  begin
    TableBUWorkspot.Refresh;
    if TableBUWorkspot.RecordCount > 0 then
    begin
      if (DisplayMessage(SPimsDeleteBusinessUnit,
         mtConfirmation, [mbYes, mbNo]) <> mrYes) then
        Result := False
      else
        DeleteDetailsWK;
    end;
    TableJobCode.Refresh;
    if TableJobCode.RecordCount = 0 then
      FCallNewForm := True;
  end
  else
  begin
    TableJobCode.Refresh;
    if TableJobCode.RecordCount > 0 then
    begin
      if (DisplayMessage(SPimsDeleteJob,
        mtConfirmation, [mbYes, mbNo]) <> mrYes) then
        Result := False
      else
        DeleteDetailsWK;
    end;
    TableBUWorkspot.Refresh;
    if TableBUWorkspot.RecordCount = 0 then
      FCallNewForm := True;
  end;
end;

procedure TWorkSpotDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);

  TableDetail.FieldByName('USE_JOBCODE_YN').Value := 'N';
  TableDetail.FieldByName('MEASURE_PRODUCTIVITY_YN').Value := 'N';
  TableDetail.FieldByName('AUTOMATIC_DATACOL_YN').Value := 'N';
  TableDetail.FieldByName('AUTOMATIC_RESCAN_YN').Value := 'N';
  TableDetail.FieldByName('COUNTER_VALUE_YN').Value := 'N';
  TableDetail.FieldByName('ENTER_COUNTER_AT_SCAN_YN').Value := 'N';
  TableDetail.FieldByName('PRODUCTIVE_HOUR_YN').Value := 'N';
  if not QueryDeptLU.IsEmpty then
  begin
    QueryDeptLU.First;
    TableDetail.FieldByName('DEPARTMENT_CODE').Value :=
      QueryDeptLU.FieldByName('DEPARTMENT_CODE').Value;
  end;
  TableDetail.FieldByName('TIMEREC_BY_MACHINE_YN').Value := 'N'; // 20015178
  TableDetail.FieldByName('PS_QTYVIA_EXTERNAL_YN').Value := 'N'; // 20016016
end;

function TWorkSpotDM.CheckOtherWKInterfaceCode(WKCode,
  InterfaceCode: String): Boolean;
var
  SelectStr: String;
begin
  Result := True;
  if InterfaceCode = '' then
    Exit;
  SelectStr :=
    'SELECT ' +
    '  COUNT(*) AS RECCOUNT ' +
    'FROM ' +
    '  JOBCODE ' +
    'WHERE ' +
    '  WORKSPOT_CODE <> ' + '''' +  DoubleQuote(WKCode) + '''' + ' AND ' +
    '  INTERFACE_CODE = ' + '''' + DoubleQuote(InterfaceCode) + '''';
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  QueryWork.Open;
  Result := (Round(QueryWork.FieldByName('RECCOUNT').AsFloat) = 0);
end;

function TWorkSpotDM.CheckOnWKInterfaceCode(Plant, WKCode,  JobCode,
  InterfaceCode: String): Boolean;
var
  SelectStr: String;
begin
  Result := True;
  if InterfaceCode = '' then
    Exit;
  // Pims -> Oracle, select changed to get result from 'reccount'.
  SelectStr :=
    'SELECT ' +
    '  COUNT(*) AS RECCOUNT ' +
    'FROM ' +
    '  JOBCODE ' +
    'WHERE ' +
    '  PLANT_CODE = ' + '''' + DoubleQuote(Plant) + '''' + ' AND ' +
    '  WORKSPOT_CODE = ' + '''' +  DoubleQuote(WKCode) + '''' + ' AND ' +
    '  INTERFACE_CODE = ' + '''' + DoubleQuote(InterfaceCode) + '''' + ' AND ' +
    '  JOB_CODE <> ' + '''' + DoubleQuote(JobCode) + '''';
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  QueryWork.Open;
  Result := (Round(QueryWork.FieldByName('RECCOUNT').AsFloat) = 0);
  QueryWork.Close;
end;

//CAR 21-8-2003
function TWorkSpotDM.CheckOnWKAllInterfaceCode(Plant, WKCode: String): Boolean;
begin
  // MR:13-3-2005 Query changed to simplify this action.
  QueryCheckOnWKInterface.Close;
  QueryCheckOnWKInterface.ParamByName('PLANT_CODE').AsString := Plant;
  QueryCheckOnWKInterface.ParamByName('WORKSPOT_CODE').AsString := WKCode;
  QueryCheckOnWKInterface.Open;
  Result := not QueryCheckOnWKInterface.IsEmpty;
  QueryCheckOnWKInterface.Close;
end;

procedure TWorkSpotDM.TableJobCodeBeforePost(DataSet: TDataSet);
var
  Bonus: String;
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  Bonus  :=
    Format('%*.*f', [3, 2, TableJobCodeNORM_PROD_LEVEL.Value]);
  TableJobCodeNORM_PROD_LEVEL.Value := StrToFloat(Bonus);
  Bonus  :=
    Format('%*.*f', [3, 2, TableJobCodeNORM_OUTPUT_LEVEL.Value]);
  TableJobCodeNORM_OUTPUT_LEVEL.Value := StrToFloat(Bonus);
  Bonus  :=
    Format('%*.*f', [3, 2, TableJobCodeBONUS_LEVEL.Value]);
  TableJobCodeBONUS_LEVEL.Value := StrToFloat(Bonus);

  Bonus  :=
    Format('%*.*f', [3, 2, TableJobCodeMAXIMUM_DEVIATION.Value]);
  TableJobCodeMAXIMUM_DEVIATION.Value := StrToFloat(Bonus);

  if TableJobCode.FieldByNAME('DATE_INACTIVE').Value <> Null then
    TableJobCodeDATE_INACTIVE.AsDateTime :=
      Trunc(TableJobCodeDATE_INACTIVE.AsDateTime);
  TableJobCode.FieldByNAME('IGNOREINTERFACECODE_YN').AsString :=
    BoolYN(FIgnore);
  if (TableDetail.FieldByName('AUTOMATIC_RESCAN_YN').AsString = 'Y') then
    if not CheckOnWKInterfaceCode(TableJobCode.FieldByName('PLANT_CODE').Value,
      TableJobCode.FieldByName('WORKSPOT_CODE').Value,
      TableJobCode.FieldByName('JOB_CODE').Value,
      TableJobCode.FieldByName('INTERFACE_CODE').AsString) then
  begin
    DisplayMessage(SWKInterfaceCode, mtInformation, [mbOk]);
    Abort;
  end;
  if not CheckOtherWKInterfaceCode(
    TableJobCode.FieldByName('WORKSPOT_CODE').Value,
    TableJobCode.FieldByName('INTERFACE_CODE').AsString) then
  begin
    DisplayMessage(SInterfaceCode, mtInformation, [mbOk]);
    Abort;
  end;
  // 20013476
  if TableJobCode.FieldByName('SAMPLE_TIME_MINS').Value <> Null then
  begin
    if not ((TableJobCode.FieldByName('SAMPLE_TIME_MINS').AsInteger >= 1) and
      (TableJobCode.FieldByName('SAMPLE_TIME_MINS').AsInteger <= 30)) then
    begin
      DisplayMessage(SPimsValueSampleTimeMins, mtInformation, [mbOk]);
      Abort;
    end;
  end;
  // 20015178
//  if SystemDM.EnableMachineTimeRec then // 20015178.70
  if TableJobCodeDEFAULT_YN.Value = 'Y' then
    ResetDefaultJob(TableJobCode.FieldByName('PLANT_CODE').Value,
      TableJobCode.FieldByName('WORKSPOT_CODE').Value,
      TableJobCode.FieldByName('JOB_CODE').Value
      );
end; // TableJobCodeBeforePost

procedure TWorkspotDM.SelectOtherInterfaceCode(InterfaceDefined: String;
  var RecCount: Integer);
begin
  QueryInterface.Close;
  QueryInterface.ParamByName('PLANT_CODE').AsString :=
    TableMaster.FieldByName('PLANT_CODE').AsString;
  QueryInterface.ParamByName('INTERFACE_CODE').AsString := InterfaceDefined;
  QueryInterface.Open;
  RecCount := QueryInterface.RecordCount;
end;


procedure TWorkSpotDM.TableJobCodeNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  TableJobCode.FieldByName('IGNOREINTERFACECODE_YN').AsString := 'N';
  TableJobCode.FieldByName('SHOW_AT_PRODUCTIONSCREEN_YN').AsString := 'Y';
  TableJobCode.FieldByName('SHOW_AT_SCANNING_YN').AsString := 'Y';
  TableJobCode.FieldByName('COMPARATION_JOB_YN').AsString := 'N';
  TableJobCode.FieldByName('COMPARISON_REJECT_YN').AsString := 'N';
  if not QueryBU.IsEmpty then
  begin
    QueryBU.First;
    TableJobCode.FieldByName('BUSINESSUNIT_CODE').Value :=
      QueryBU.FieldByName('BUSINESSUNIT_CODE').Value;
  end;
  TableJobCodeEMPS_SCAN_IN_YN.Value := 'N';
  // 20013478.
  TableJobCodeIGNOREQUANTITIESINREPORTS_YN.Value := 'N';
  // 20015178
  TableJobCodeDEFAULT_YN.Value := 'N';
end;

procedure TWorkSpotDM.TableBUWorkspotBeforePost(DataSet: TDataSet);
var
  Bonus: String;
begin
  SystemDM.DefaultBeforePost(DataSet);
  Bonus  :=
    Format('%*.*f', [3, 2, TableBUWorkspotPERCENTAGE.Value]);
  TableBUWorkspotPERCENTAGE.Value := StrToFloat(Bonus);
end;

//CAR 21-8-2003
function TWorkSpotDM.GetSumPercentage(PlantCode, WKCode: String): Double;
begin
  QueryGetSumPerc.Close;
  QueryGetSumPerc.ParamByName('PLANT_CODE').AsString := PlantCode;
  QueryGetSumPerc.ParamByName('WORKSPOT_CODE').AsString := WKCode;
  QueryGetSumPerc.Open;
  Result := QueryGetSumPerc.FieldByName('SUMPERC').AsFloat;
end;

procedure TWorkSpotDM.TableBUWorkspotNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not QueryBU.IsEmpty then
  begin
    QueryBU.First;
    TableBUWorkspot.FieldByName('BUSINESSUNIT_CODE').Value :=
      QueryBU.FieldByName('BUSINESSUNIT_CODE').Value;
  end;
end;

procedure TWorkSpotDM.DeleteDetailsWK;
var
  StrDel: String;
begin
//DELETE IGNOREINTERFACE
  QueryDel.Close;
  QueryDel.SQL.Clear;
  StrDel := 'DELETE FROM IGNOREINTERFACECODE WHERE PLANT_CODE= ''' +
    DoubleQuote(TableDetail.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('WORKSPOT_CODE').AsString) + '''';
  QueryDel.SQL.Add(StrDel);
  QueryDel.ExecSQL;
//DELETE Compare jobcode 21-8-2003
  QueryDel.Close;
  QueryDel.SQL.Clear;
  StrDel := 'DELETE FROM COMPAREJOB WHERE PLANT_CODE= ''' +
    DoubleQuote(TableDetail.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('WORKSPOT_CODE').AsString) + '''';
  QueryDel.SQL.Add(StrDel);
  QueryDel.ExecSQL;
//DELETE JOBCODE
  QueryDel.Close;
  QueryDel.SQL.Clear;
  StrDel := 'DELETE FROM JOBCODE WHERE PLANT_CODE= ''' +
    DoubleQuote(TableDetail.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('WORKSPOT_CODE').AsString) + '''';
  QueryDel.SQL.Add(StrDel);
  QueryDel.ExecSQL;
//DELETE BUSINESSUNIT
  QueryDel.Close;
  QueryDel.SQL.Clear;
  StrDel := 'DELETE FROM BUSINESSUNITPERWORKSPOT WHERE PLANT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('PLANT_CODE').AsString) + '''' +
    ' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('WORKSPOT_CODE').AsString) + '''';
  QueryDel.SQL.Add(StrDel);
  QueryDel.ExecSQL;
end;

procedure TWorkSpotDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  if (SystemDM.DeleteWKYN = CHECKEDVALUE) then
  begin
    DefaultBeforeDelete(DataSet);
    if DisplayMessage(SPimsDeleteJobBU,
      mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      try
        StoredProcDelete.ParamByName('PLANT_CODE').AsString :=
          DataSet.FieldByName('PLANT_CODE').AsString;
        StoredProcDelete.ParamByName('WORKSPOT_CODE').AsString :=
          DataSet.FieldByName('WORKSPOT_CODE').AsString;
        StoredProcDelete.Prepare;
        StoredProcDelete.ExecProc;
      except
      end;
    end
    else
      SysUtils.Abort;
  end
  else
  begin // RV094.7.
    DisplayMessage(SPimsWorkspotDeleteNotAllowed, mtWarning, [mbOK]);
    Abort;
  end;
end;

function TWorkSpotDM.GetValueYN(ValueBool: Boolean): String;
begin
  if ValueBool then
    Result := 'Y'
  else
    Result := 'N';
end;

function TWorkSpotDM.GetBoolYN(ValueYN: String): Boolean;
begin
  if ValueYN = 'Y' then
    Result := True
  else
    Result := False;
end;

procedure TWorkSpotDM.TableDetailBeforePost(DataSet: TDataSet);
var
  WKCode: String;
  EmpPlanDate: TDateTime;
  function HostPortCheck(APlantCode, AWorkspotCode, AHost: String;
    APort: Integer): Boolean;
  begin
    Result := False;
    with qryHostPortExists do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('HOST').AsString := AHost;
      ParamByName('PORT').AsInteger := APort;
      Open;
      if not Eof then
        Result := True;
      Close;
    end;
  end;
  function RoamingWorkspotCheck(APlantCode, AWorkspotCode: String): Boolean;
  begin
    Result := False;
    with qryRoamingWorkspotExists do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      Open;
      if not Eof then
        Result := True;
      Close;
    end;
  end;
begin
  // RV094.7.
  // 20013923
  if (SystemDM.UpdateWKYN <> CHECKEDVALUE) then
  begin
    DisplayMessage(SPimsWorkspotUpdateNotAllowed, mtWarning, [mbOK]);
    Abort;
    Exit;
  end;
  WKCode := TableDetail.FieldByName('WORKSPOT_CODE').AsString;
  if (Pos('-', WKCode) > 0) or (Pos('+', WKCode) > 0) or
    (Pos('|', WkCode) > 0) then
  begin
    DisplayMessage(SInvalidWK, mtInformation, [mbOk]);
    Abort;
  end;
  if WKCode = DUMMYSTR then
  begin
    DisplayMessage(SDummyWK, mtInformation, [mbOk]);
    Abort;
  end;
  if not CheckRelatedRecords then
   Abort;
  // RV048.1.
  // RV085.1.
  EmpPlanDate := EmpPlanPWDExists;
  if EmpPlanDate > 0 then
  begin
    // RV085.1.
//    DisplayMessage(SPimsEmpPlanPWD, mtInformation, [mbOk]);
    if DisplayMessage(Format(SPimsEmpPlanPWDFound,
      [DateToStr(EmpPlanDate)]),
      mtConfirmation, [mbYes, mbNo]) <> mrYes then
    begin
      DataSet.Cancel;
      Abort;
    end;
  end;
  inherited;
  SystemDM.DefaultBeforePost(DataSet);

  if TableDetail.FieldByName('DATE_INACTIVE').Value <> Null then
    TableDetail.FieldByName('DATE_INACTIVE').AsDateTime :=
      Trunc(TableDetail.FieldByName('DATE_INACTIVE').AsDateTime);
  TableDetail.FieldByName('AUTOMATIC_DATACOL_YN').AsString :=
    GetValueYN(WorkspotDM.FAutomaticYN);
  TableDetail.FieldByName('COUNTER_VALUE_YN').AsString :=
    GetValueYN(WorkspotDM.FCounterYN);
  TableDetail.FieldByName('ENTER_COUNTER_AT_SCAN_YN').AsString :=
    GetValueYN(WorkspotDM.FEnterCounterYN);
  TableDetail.FieldByName('AUTOMATIC_RESCAN_YN').AsString :=
    GetValueYN(WorkspotDM.FAutoRescanYN);
  TableDetail.FieldByName('MEASURE_PRODUCTIVITY_YN').AsString :=
     GetValueYN(WorkspotDM.FMeasureProd);

  if WorkspotDM.FAutoRescanYN then
    if CheckOnWKAllInterfaceCode(
      TableDetail.FieldByName('PLANT_CODE').Value,
      TableDetail.FieldByName('WORKSPOT_CODE').Value) then
    begin
      DisplayMessage(SWKRescanInterfaceCode, mtInformation, [mbOk]);
      Abort;
    end;

  // RV064.1.
  if (TableDetail.FieldByName('MACHINE_CODE').AsString <> '') then
    if not CheckMachineJobs(TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('WORKSPOT_CODE').AsString,
      TableDetail.FieldByName('MACHINE_CODE').AsString) then
    begin
      DisplayMessage(SPimsMachineWorkspotNonMatchingJobs, mtInformation, [mbOK]);
//      Abort;
    end;

  FChangeRescan := False;

  // RV100.1. When a (overruled) contract group is entered, then remove
  //          the hourtype number.
  if SystemDM.IsCVL then
  begin
    if TableDetail.FieldByName('CONTRACTGROUP_CODE').AsString <> '' then
      if TableDetail.FieldByname('HOURTYPE_NUMBER').AsString <> '' then
        TableDetail.FieldByName('HOURTYPE_NUMBER').AsString := '';
  end;

  // 20015178
  if SystemDM.EnableMachineTimeRec then
  begin
    if TableDetail.FieldByName('TIMEREC_BY_MACHINE_YN').AsString = 'Y' then
    begin
      if TableDetail.FieldByName('FAKE_EMPLOYEE_NUMBER').AsString = '' then
      begin
        DisplayMessage(SPimsPleaseEnterFakeEmpNr, mtInformation, [mbOK]);
        Abort;
      end
      else
      begin
        if FakeEmployeeNumberExists then
        begin
          DisplayMessage(SPimsFakeEmpNrInUse, mtInformation, [mbOK]);
          Abort;
        end;
      end;
    end
    else
      TableDetail.FieldByName('FAKE_EMPLOYEE_NUMBER').AsString := '';
  end;
  // PIM-203
  if SystemDM.HybridMode = hmHybrid then
  begin
    if (TableDetail.FieldByName('HOST').AsString <> '') and
      (TableDetail.FieldByName('PORT').AsString <> '') then
      if HostPortCheck(TableDetail.FieldByName('PLANT_CODE').AsString,
        TableDetail.FieldByName('WORKSPOT_CODE').AsString,
        TableDetail.FieldByName('HOST').AsString,
        TableDetail.FieldByName('PORT').AsInteger) then
      begin
        DisplayMessage(SPimsHostPortExists, mtInformation, [mbOK]);
        Abort;
      end;
  end;
  // GLOB3-202
  if SystemDM.TimeRecCRDoNotScanOut then
  begin
    if TableDetail.FieldByName('ROAMING_YN').AsString = 'Y' then
      if RoamingWorkspotCheck(TableDetail.FieldByName('PLANT_CODE').AsString,
        TableDetail.FieldByName('WORKSPOT_CODE').AsString) then
      begin
        DisplayMessage(SPimsRoamingWorkspotExists, mtInformation, [mbOK]);
        Abort;
      end;
  end;
end; // TableDetailBeforePost

procedure TWorkSpotDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  // 20013910
  ChangeDepartmentAction(
    DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString,
    OldDepartmentCode,
    DataSet.FieldByName('DEPARTMENT_CODE').AsString);
  // This calls the jobs-dialog or the business-unit-dialog.
  if FCallNewForm then
  begin
    // RV064.1.
    if (TableDetail.FieldByName('USE_JOBCODE_YN').AsString = CHECKEDVALUE) then
      AddMachineJobs(TableDetail.FieldByName('PLANT_CODE').AsString,
        TableDetail.FieldByName('WORKSPOT_CODE').AsString,
        TableDetail.FieldByName('MACHINE_CODE').AsString);
    WorkspotF.CallForm;
  end;
end;

function TWorkSpotDM.CountRecordOfIgnoreInterface: Boolean;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  COUNT(*) AS RECCOUNT ' +
    'FROM ' +
    '  IGNOREINTERFACECODE ' +
    'WHERE ' +
    '  PLANT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('PLANT_CODE').AsString) +
    ''' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('WORKSPOT_CODE').AsString)  +  '''' +
    '  AND JOB_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('JOB_CODE').AsString) + '''' +
    '  AND INTERFACE_CODE IS NOT NULL';
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
  QueryWork.Open;
  Result := (QueryWork.FieldByName('RECCOUNT').AsFloat > 0);
end;

procedure TWorkSpotDM.TableJobCodeAfterPost(DataSet: TDataSet);
begin
  inherited;
  if (FIgnore = False) then
  begin
    if CountRecordOfIgnoreInterface then
      if DisplayMessage(SDeleteOtherInterfaceCode, mtConfirmation,
        [mbYes, mbNo]) <> mrYes then
        Abort;
    DeleteOtherInterfaceCodes;
  end;
  if (TableJobCode.FieldByName('COMPARATION_JOB_YN').AsString =
    UNCHECKEDVALUE) then
    DeleteCompareJob;
end;

procedure TWorkSpotDM.DeleteCompareJob;
var
  SelectStr: String;
begin
  SelectStr :=
    'DELETE FROM COMPAREJOB WHERE PLANT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('PLANT_CODE').Value) +
    ''' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('WORKSPOT_CODE').Value) + '''' +
    ' AND JOB_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('JOB_CODE').Value) + '''';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(Selectstr);
  QueryWork.ExecSQL;
  TableCompareJOb.Refresh;
end;

procedure TWorkSpotDM.DeleteOtherInterfaceCodes;
var
  SelectStr: String;
begin
  SelectStr :=
    'DELETE FROM IGNOREINTERFACECODE WHERE PLANT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('PLANT_CODE').Value) +
    ''' AND WORKSPOT_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('WORKSPOT_CODE').Value) + '''' +
    //Car 20.2.2004 add DoubleQuote
    ' AND JOB_CODE = ''' +
    DoubleQuote(TableJobCode.FieldByName('JOB_CODE').Value) + '''';
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(Selectstr);
  QueryWork.ExecSQL;
end;

procedure TWorkSpotDM.OpenQueryForCompareJob;
begin
  if not QueryJOBLU.Active then
  begin
    QueryJOBLU.Prepare;
    QueryJOBLU.Open;
  end;
  if not QueryALLJob.Active then
    QueryALLJob.Open;
  if not QueryALLWK.Active then
    QueryALLWK.Open;

  if not QueryWKLU.Active then
  begin
    QueryWKLU.Prepare;
    QueryWKLU.Open;
  end;
end;

procedure TWorkSpotDM.DataModuleCreate(Sender: TObject);
begin
  inherited;

//CAR 21-8-2003
// MR:31-3-2005 Is now done in 'OnFilterRecord'.
//  TableDetail.Filter := 'WORKSPOT_CODE <> ''' + DUMMYSTR + '''';
  QueryGetSumPerc.Prepare;
  QueryCheckOnWKInterface.Prepare;

  QueryBU.Prepare;
  QueryBU.Open;

  OldDepartmentCode := '';

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableMaster);
  // RV086.2.
  SystemDM.PlantTeamFilterEnable(TableDetail);
  TableDetail.Filtered := True; // It has already a filter, so always turn it on.
  SystemDM.PlantTeamFilterEnable(QueryDeptLU);

  QueryDeptLU.Prepare;
  QueryDeptLU.Open;

  // RV100.1.
  qryContractGroup.Open;
end;

procedure TWorkSpotDM.TableCompareJobNewRecord(DataSet: TDataSet);
var
  LastWorkspotCode: String;
  function DetermineLastEnteredWorkspot: String;
  begin
    Result := '';
    try
      with qryCompareJobCodeWK do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString :=
          TableJobCode.FieldByName('PLANT_CODE').AsString;
        ParamByName('WORKSPOT_CODE').AsString :=
          TableJobCode.FieldByName('WORKSPOT_CODE').AsString;
        ParamByName('JOB_CODE').AsString :=
          TableJobCode.FieldByName('JOB_CODE').AsString;
        Open;
        if not Eof then
          Result := FieldByName('COMPARE_WORKSPOT_CODE').AsString;
        Close;
      end;
    except
      //
    end;
  end;
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // MRA:26-JUN-2012. Use last entered workspot code as default
  LastWorkspotCode := DetermineLastEnteredWorkspot;
  if LastWorkspotCode <> '' then
  begin
    TableCompareJob.FieldByName('COMPARE_WORKSPOT_CODE').Value :=
      LastWorkspotCode;
  end
  else
    if not QueryWKLU.IsEmpty then
    begin
      QueryWKLU.First;
      TableCompareJob.FieldByName('COMPARE_WORKSPOT_CODE').Value :=
        QueryWKLU.FieldByName('WORKSPOT_CODE').Value;
    if not QueryJOBLU.IsEmpty then
    begin
      QueryJOBLU.First;
      TableCompareJob.FieldByName('COMPARE_JOB_CODE').Value :=
        QueryJOBLU.FieldByName('JOB_CODE').Value;
    end;
  end;
end;

procedure TWorkSpotDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  // Close the Query here!
  QueryDeptLU.Close;
  QueryDeptLU.UnPrepare;
  //CAR 21-8-2003

  QueryGetSumPerc.Close;
  QueryGetSumPerc.UnPrepare;
  QueryCheckOnWKInterface.Close;
  QueryCheckOnWKInterface.Unprepare;
//Compare job form
  QueryALLJOB.Close;
  QueryJOBLU.Close;
  QueryJOBLU.Unprepare;

  QueryALLWK.Close;
  QueryWKLU.Close;
  QueryWKLU.Unprepare;
//
  QueryBU.Close;
  QueryBU.Unprepare;
  
  // RV100.1.
  qryContractGroup.Close;
end;

procedure TWorkSpotDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // Pims -> Oracle
  Accept := DataSet.FieldByName('WORKSPOT_CODE').AsString <> NULLSTR;
  // RV086.2.
  if SystemDM.PlantTeamFilterOn then
    Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

// MR:27-09-2005 Order 550410
function TWorkspotDM.CheckForOpenScans: Boolean;
begin
  qryOpenScans.Close;
  qryOpenScans.ParamByName('PLANT_CODE').AsString :=
    TableDetailPLANT_CODE.AsString;
  qryOpenScans.ParamByName('WORKSPOT_CODE').AsString :=
    TableDetailWORKSPOT_CODE.AsString;
  qryOpenScans.Open;
  Result := (qryOpenScans.FieldByName('TOTALCOUNT').AsFloat > 0);
  qryOpenScans.Close;
  if Result then
    DisplayMessage(SPimsUseJobcodesNotAllowed, mtInformation, [mbOk]);
end;

// RV048.1.
// RV085.1.
function TWorkSpotDM.EmpPlanPWDExists: TDateTime;
begin
  Result := 0;
  if OldDepartmentCode <> '' then
    if OldDepartmentCode <> TableDetailDEPARTMENT_CODE.AsString then
    begin
      // RV085.1. Query changed.
      with qryEmpPlanPWD do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString :=
          TableDetailPLANT_CODE.AsString;
        ParamByName('WORKSPOT_CODE').AsString :=
          TableDetailWORKSPOT_CODE.AsString;
        ParamByName('DEPARTMENT_CODE').AsString :=
          OldDepartmentCode;
        Open;
        // RV085.1.
//        Result := (FieldByName('TOTALCOUNT').AsFloat > 0);
        if FieldByName('EMPLOYEEPLANNING_DATE').AsString <> '' then
          Result := FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
        Close;
      end;
    end;
end;

// RV048.1.
procedure TWorkSpotDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  OldDepartmentCode := TableDetailDEPARTMENT_CODE.AsString;
end;

// RV048.1.
procedure TWorkSpotDM.TableDetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  OldDepartmentCode := '';
end;

// RV050.8.
procedure TWorkSpotDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// RV064.1.
// Check if all the workspots linked to a machine have
// the same jobs.
// Check only for jobs that are active! (Field: DATE_INACTIVE).
function TWorkSpotDM.CheckMachineJobs(
  APlantCode, AWorkspotCode, AMachineCode: String): Boolean;
begin
  // First check if there are any jobs for this workspot.
  // This can be the case when user is entering a new workspot.
  with qryCheckWorkspotJobs do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
    Open;
    Result := Eof;
    Close;
  end;
  if not Result then // There were jobs for this workspot.
  begin
    if (TableDetail.FieldByName('USE_JOBCODE_YN').AsString = CHECKEDVALUE) then
    begin
      // Check if there all jobs linked to the machine are mathing jobs.
      with qryCheckMachineJobs do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('MACHINE_CODE').AsString := AMachineCode;
        Open;
        Result := Eof;
        Close;
      end;
    end;
  end;
end;

// RV064.1.
// Check if the job exists in machinejob-table.
function TWorkSpotDM.MachineJobExists(APlantCode, AMachineCode, AJobCode: String): Boolean;
begin
  Result := False;
  if TableDetail.FieldByName('MACHINE_CODE').AsString <> '' then
  begin
    with qryMachineJobExists do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('MACHINE_CODE').AsString := AMachineCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      Open;
      Result := not Eof;
      Close;
    end;
  end;
end;

// RV064.1.
// Add machine jobs to a workspot where MACHINE_CODE-field
// has been entered.
function TWorkSpotDM.AddMachineJobs(APlantCode, AWorkspotCode,
  AMachineCode: String): Boolean;
begin
  Result := True;
  if AMachineCode <> '' then
  begin
    try
      with qryAddMachJobsToWS do
      begin
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('MACHINE_CODE').AsString := AMachineCode;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
        TableJobCode.Close;
        TableJobCode.Open;
      end;
    except
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      Result := False;
    end;
  end;
end;

// RV085.5.
procedure TWorkSpotDM.TableHourtypeFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  if TableHourtype.Active and TableHourtypePerCountry.Active then
  begin
    if not TableHourtypePerCountry.IsEmpty then
      Accept := TableHourtypePerCountry.Locate('HOURTYPE_NUMBER',
        DataSet.FieldByName('HOURTYPE_NUMBER').AsInteger, [])
    else
      Accept := True;
  end
  else
    Accept := True;
end;

// RV085.5.
procedure TWorkSpotDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  // Do this to ensure the refresh of hourtypes when another plant is chosen.
  TableHourtype.Filtered := False;
  TableHourtype.Filtered := True;
end;

procedure TWorkSpotDM.QueryDeptLUFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.2.
  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
end;

// 20013910 Change department for certain related tables
procedure TWorkSpotDM.ChangeDepartmentAction(APlantCode, AWorkspotCode,
  AOldDepartmentCode, ANewDepartmentCode: String);
  procedure StandardOccupationChange;
  begin
    try
      with qryStandardOccupationUpdate do
      begin
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        ParamByName('OLD_DEPARTMENT_CODE').AsString := AOldDepartmentCode;
        ParamByName('NEW_DEPARTMENT_CODE').AsString := ANewDepartmentCode;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  end; // StandardOccupationChange;
  procedure StandardEmployeePlanningChange;
  begin
    try
      with qryStandardEmployeePlanningUpdate do
      begin
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        ParamByName('OLD_DEPARTMENT_CODE').AsString := AOldDepartmentCode;
        ParamByName('NEW_DEPARTMENT_CODE').AsString := ANewDepartmentCode;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  end; // StandardEmployeePlanningChange;
  procedure EmployeePlanningChange;
  begin
    try
      with qryEmployeePlanningUpdate do
      begin
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        ParamByName('OLD_DEPARTMENT_CODE').AsString := AOldDepartmentCode;
        ParamByName('NEW_DEPARTMENT_CODE').AsString := ANewDepartmentCode;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  end; // EmployeePlanningChange;
begin
  try
    if AOldDepartmentCode <> '' then
      if AOldDepartmentCode <> ANewDepartmentCode then
      begin
        StandardOccupationChange;
        StandardEmployeePlanningChange;
        EmployeePlanningChange;
      end;
  finally
  end;
end; // ChangeDepartmentAction

// 20013196
procedure TWorkSpotDM.LinkJobDelete(APlantCode, AWorkspotCode,
  AJobCode: String);
begin
  try
    with qryLinkJobDel do
    begin
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      ExecSQL;
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
      // Refresh this, otherwise it will still be shown.
      TableLinkJob.Close;
      TableLinkJob.Open;
    end;
  except
    on E: EDBEngineError do
    begin
      WErrorLog(E.Message);
    end;
  end;
end;

// 20013196
function TWorkSpotDM.LinkJobExists(APlantCode, AWorkspotCode,
  AJobCode: String): Boolean;
begin
  Result := False;
  try
    with qryLinkJob do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      Open;
      Result := not Eof;
    end;
  except
    on E: EDBEngineError do
    begin
      WErrorLog(E.Message);
    end;
  end;
end;

// 20013196 Do not allow more than 1 link job per workspot.
procedure TWorkSpotDM.TableLinkJobBeforePost(DataSet: TDataSet);
  // Is there already a linkjob for this workspot?
  function LinkJobWSCheck(APlantCode, AWorkspotCode: String): Boolean;
  begin
    Result := False;
    try
      with qryLinkJobWS do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString := APlantCode;
        ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        Open;
        Result := not Eof;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
      end;
    end;
  end;
begin
  inherited;
  if LinkJobWSCheck(DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString) then
  begin
    DisplayMessage(SPimsLinkJobExist, mtInformation, [mbOk]);
    Abort;
  end;
  SystemDM.DefaultBeforePost(DataSet);
end;

// 20015178
procedure TWorkSpotDM.ResetDefaultJob(APlantCode,
  AWorkspotCode, AJobCode: String);
begin
  with qryResetDefaultJob do
  begin
    MyRowsAffected := 0;
    try
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      ExecSQL;
      MyRowsAffected := RowsAffected;
    except
      on E: EDBEngineError do
      begin
        MyRowsAffected := 0;
        WErrorLog(E.Message);
      end;
    end;
  end;
end;

// 20015178
function TWorkSpotDM.FakeEmployeeNumber: Integer;
begin
  try
    Result := 
      StrToInt('999' + TableDetail.FieldByName('PLANT_CODE').Value +
        TableDetail.FieldByName('WORKSPOT_CODE').Value);
  except
    Result := -1;
  end;
end; // FakeEmployeeNumber

// 20015178
function TWorkSpotDM.FakeEmployeeNumberExists: Boolean;
begin
  Result := False;
  with qryFakeEmpExists do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString :=
      TableDetail.FieldByName('PLANT_CODE').AsString;
    ParamByName('WORKSPOT_CODE').AsString :=
      TableDetail.FieldByName('WORKSPOT_CODE').AsString;
    ParamByName('FAKE_EMPLOYEE_NUMBER').AsInteger :=
      TableDetail.FieldByName('FAKE_EMPLOYEE_NUMBER').AsInteger;
    Open;
    if not Eof then
      Result := True;
    Close;
  end;
end; // FakeEmployeeNumberExists

end.
