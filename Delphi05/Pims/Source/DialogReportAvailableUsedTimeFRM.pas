(*
  Changes:
    MR:19-10-2004:
      Order 550346: added selections for this report.
    MR:19-10-2004:
      If the employeefrom and employeeto - grids do
      not show anything, you have to set the values
      again for component dxDBGridLayoutListEmployee!
      The 'inheritance' from the 'Dialogreportbase' is
      not working correctly for this component.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:4-FEB-2019 GLOB3-208
    - Accrual Report
*)

unit DialogReportAvailableUsedTimeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, jpeg;

type
  TDialogReportAvailableUsedTimeF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    RadioGroupStatusEmpl: TRadioGroup;
    CheckBoxExport: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CheckBoxAllTeam: TCheckBox;
    QueryBU: TQuery;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    CheckBoxSuppressZeroes: TCheckBox;
    GroupBox2: TGroupBox;
    CheckBoxHoliday: TCheckBox;
    CheckBoxWTR: TCheckBox;
    CheckBoxTimeForTime: TCheckBox;
    GroupBox3: TGroupBox;
    CheckBoxSortOnDepartment: TCheckBox;
    CheckBoxSortOnTeam: TCheckBox;
    qryAbsenceTypePerCountryExist: TQuery;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    CheckBoxHolidayCaption, CheckBoxWTRCaption,
    CheckBoxTimeForTimeCaption: String;
    procedure FillBusiness;
    function AbsenceTypePerCountryExist: Boolean;
    function GetCounterActivated(AAbsenceType: Char;
      var ACounterDescription: String): Boolean;
    procedure DetermineCaptions;
  public
    { Public declarations }
  end;

var
  DialogReportAvailableUsedTimeF: TDialogReportAvailableUsedTimeF;

// RV089.1.
function DialogReportAvailableUsedTimeForm: TDialogReportAvailableUsedTimeF;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAvailableUsedTimeQRPT, ReportAvailableUsedTimeDMT,
  ReportBaseDMT, CalculateTotalHoursDMT, ListProcsFRM, UPimsConst;

var
  DialogReportAvailableUsedTimeF_HND: TDialogReportAvailableUsedTimeF;

// RV089.1.
function DialogReportAvailableUsedTimeForm: TDialogReportAvailableUsedTimeF;
begin
  if (DialogReportAvailableUsedTimeF_HND = nil) then
  begin
    DialogReportAvailableUsedTimeF_HND := TDialogReportAvailableUsedTimeF.Create(Application);
    with DialogReportAvailableUsedTimeF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportAvailableUsedTimeF_HND;
end;

// RV089.1.
procedure TDialogReportAvailableUsedTimeF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportAvailableUsedTimeF_HND <> nil) then
  begin
    DialogReportAvailableUsedTimeF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportAvailableUsedTimeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportAvailableUsedTimeF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportAvailableUsedTimeQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      RadioGroupStatusEmpl.ItemIndex,
      CheckBoxHoliday.Checked,
      CheckBoxWTR.Checked,
      CheckBoxTimeForTime.Checked,
      CheckBoxSortOnDepartment.Checked,
      CheckBoxSortOnTeam.Checked,
      CheckBoxHoliday.Caption,
      CheckBoxWTR.Caption,
      CheckBoxTimeForTime.Caption,
      CheckBoxAllTeams.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxSuppressZeroes.Checked,
      CheckBoxExport.Checked)
  then
    ReportAvailableUsedTimeQR.ProcessRecords;
end;


procedure TDialogReportAvailableUsedTimeF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, True, True, False, False, False);
  inherited;
  FillBusiness;
  DetermineCaptions;
end;

procedure TDialogReportAvailableUsedTimeF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportAvailableUsedTimeDM := CreateReportDM(TReportAvailableUsedTimeDM);
  ReportAvailableUsedTimeQR := CreateReportQR(TReportAvailableUsedTimeQR);
  CheckBoxHolidayCaption := CheckBoxHoliday.Caption;
  CheckBoxWTRCaption := CheckBoxWTR.Caption;
  CheckBoxTimeForTimeCaption := CheckboxTimeForTime.Caption;
end;

procedure TDialogReportAvailableUsedTimeF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

procedure TDialogReportAvailableUsedTimeF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  DetermineCaptions;
  FillBusiness;
end;

procedure TDialogReportAvailableUsedTimeF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  DetermineCaptions;
  FillBusiness;
end;

procedure TDialogReportAvailableUsedTimeF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessTo.DisplayValue :=
        ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportAvailableUsedTimeF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessFrom.DisplayValue :=
        ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportAvailableUsedTimeF.CheckBoxAllTeamClick(
  Sender: TObject);
begin
  inherited;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportAvailableUsedTimeF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportAvailableUsedTimeF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

function TDialogReportAvailableUsedTimeF.AbsenceTypePerCountryExist: Boolean;
var
  CountryId: Integer;
begin
  Result := False;
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CountryId := SystemDM.GetDBValue(
      ' select NVL(p.country_id, 0) from ' +
      ' plant p ' +
      ' where ' +
      '   p.plant_code = ' + '''' + GetStrValue(CmbPlusPlantFrom.Value) + '''',
      0
    );
    if CountryID <> 0 then
    begin
      with qryAbsenceTypePerCountryExist do
      begin
        Close;
        ParamByName('COUNTRY_ID').AsInteger :=
          CountryID;
        Open;
        Result := not Eof;
        Close;
      end;
    end;
  end;
end; // AbsenceTypePerCountryExist

function TDialogReportAvailableUsedTimeF.GetCounterActivated(
  AAbsenceType: Char; var ACounterDescription: String): Boolean;
var
  CountryId: Integer;
  StrValue: String;
begin
  Result := False;
  if Trim(CmbPlusPlantFrom.Value) = '' then Exit;

  CountryId := SystemDM.GetDBValue(
    Format(
    'select p.country_id from plant p where plant_code = %s',
    [GetStrValue(CmbPlusPlantFrom.Value)]
    ), 0
  );

  if CountryId = 0 then Exit;

  Result := SystemDM.GetCounterActivated(CountryId, AAbsenceType);

  if Result then
  begin
    StrValue := SystemDM.GetDBValue(
      Format(
      'select ac.counter_description from absencetypepercountry ac ' +
      'where country_id = %d and absencetype_code = ''%s''',
      [CountryId, AAbsenceType]
      ), '*'
    );
    if StrValue <> '*' then
      ACounterDescription := StrValue;
  end;
end; // GetCounterActivated

procedure TDialogReportAvailableUsedTimeF.DetermineCaptions;
var
  NewCaption: String;
begin
  if AbsenceTypePerCountryExist then
  begin
    if GetCounterActivated(HOLIDAYAVAILABLETB, NewCaption) then
      CheckBoxHoliday.Caption := NewCaption
    else
      CheckBoxHoliday.Caption := CheckBoxHolidayCaption;
    if GetCounterActivated(WORKTIMEREDUCTIONAVAILABILITY, NewCaption) then
      CheckBoxWTR.Caption := NewCaption
    else
      CheckBoxWTR.Caption := CheckBoxWTRCaption;
    if GetCounterActivated(TIMEFORTIMEAVAILABILITY, NewCaption) then
      CheckBoxTimeForTime.Caption := NewCaption
    else
      CheckBoxTimeForTime.Caption := CheckBoxTimeForTimeCaption;
  end
  else
  begin
    CheckBoxHoliday.Caption := CheckBoxHolidayCaption;
    CheckBoxWTR.Caption := CheckBoxWTRCaption;
    CheckBoxTimeForTime.Caption := CheckBoxTimeForTimeCaption;
  end;
end;

end.
