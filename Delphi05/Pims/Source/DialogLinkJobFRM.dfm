inherited DialogLinkJobF: TDialogLinkJobF
  Left = 411
  Top = 236
  Caption = 'Link job'
  ClientHeight = 195
  ClientWidth = 504
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 504
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 208
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 504
    Height = 75
    Caption = ''
    Font.Color = clBlack
    Font.Height = -11
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 504
      Height = 75
      Align = alClient
      Caption = 'Link job'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 23
        Width = 46
        Height = 13
        Caption = 'Workspot'
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 17
        Height = 13
        Caption = 'Job'
      end
      object DBLookupComboBoxWorkspot: TDBLookupComboBox
        Tag = 1
        Left = 102
        Top = 19
        Width = 379
        Height = 19
        Ctl3D = False
        DataField = 'LINK_WORKSPOT_CODE'
        DataSource = WorkSpotDM.DataSourceLinkJob
        DropDownWidth = 243
        KeyField = 'WORKSPOT_CODE'
        ListField = 'DESCRIPTION;WORKSPOT_CODE'
        ListSource = WorkSpotDM.DataSourceWorkspot
        ParentCtl3D = False
        TabOrder = 0
        OnCloseUp = DBLookupComboBoxWorkspotCloseUp
      end
      object DBLookupComboBoxJob: TDBLookupComboBox
        Tag = 1
        Left = 102
        Top = 44
        Width = 379
        Height = 19
        Ctl3D = False
        DataField = 'LINK_JOB_CODE'
        DataSource = WorkSpotDM.DataSourceLinkJob
        DropDownWidth = 243
        KeyField = 'JOB_CODE'
        ListField = 'DESCRIPTION;JOB_CODE'
        ListSource = WorkSpotDM.DataSourceJob
        ParentCtl3D = False
        TabOrder = 1
        OnEnter = DBLookupComboBoxJobEnter
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 176
    Width = 504
  end
  inherited pnlBottom: TPanel
    Top = 135
    Width = 504
    inherited btnOk: TBitBtn
      Left = 136
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 250
      OnClick = btnCancelClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      23
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
