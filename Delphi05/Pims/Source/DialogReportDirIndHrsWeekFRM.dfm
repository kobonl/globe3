inherited DialogReportDirIndHrsWeekF: TDialogReportDirIndHrsWeekF
  Left = 269
  Caption = 'Period report direct, indirect hours per week'
  ClientHeight = 425
  ClientWidth = 574
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 574
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 278
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 574
    Height = 304
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 152
    end
    inherited LblEmployee: TLabel
      Top = 152
    end
    inherited LblToPlant: TLabel
      Top = 153
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    object Label2: TLabel [6]
      Left = 192
      Top = 194
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [7]
      Left = 222
      Top = 194
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [8]
      Left = 326
      Top = 196
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 156
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 151
    end
    object Label5: TLabel [11]
      Left = 128
      Top = 73
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [12]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [13]
      Left = 8
      Top = 194
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [14]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [15]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [16]
      Left = 315
      Top = 41
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [17]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [18]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [19]
      Left = 8
      Top = 314
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [20]
      Left = 40
      Top = 314
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [21]
      Left = 315
      Top = 70
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [22]
      Left = 315
      Top = 313
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [23]
      Left = 128
      Top = 45
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [24]
      Left = 337
      Top = 45
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [25]
      Left = 344
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [26]
      Left = 128
      Top = 316
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [27]
      Left = 336
      Top = 316
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [28]
      Left = 128
      Top = 477
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [29]
      Left = 336
      Top = 478
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object lblPeriod: TLabel [30]
      Left = 422
      Top = 195
      Width = 40
      Height = 13
      Caption = 'lblPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Left = 7
      Top = 71
    end
    inherited LblDepartment: TLabel
      Left = 39
      Top = 71
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 127
      Top = 73
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 337
      Top = 73
    end
    inherited LblToDepartment: TLabel
      Left = 314
      Top = 73
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 337
      Top = 100
    end
    inherited LblStarJobTo: TLabel
      Top = 125
    end
    inherited LblToJob: TLabel
      Top = 125
    end
    inherited LblStarJobFrom: TLabel
      Top = 125
    end
    inherited LblJob: TLabel
      Top = 125
    end
    inherited LblFromJob: TLabel
      Top = 125
    end
    object Label12: TLabel [65]
      Left = 16
      Top = 477
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [66]
      Left = 48
      Top = 477
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [67]
      Left = 323
      Top = 478
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 115
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 116
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 136
      TabOrder = 26
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 137
      TabOrder = 29
    end
    inherited CheckBoxAllTeams: TCheckBox
      TabOrder = 31
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 70
      ColCount = 137
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 333
      Top = 70
      ColCount = 138
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      Top = 73
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 151
      TabOrder = 11
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 151
      TabOrder = 12
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 142
      TabOrder = 24
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 143
      TabOrder = 21
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 22
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 34
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 32
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 23
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 150
      TabOrder = 25
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 151
      TabOrder = 28
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 96
      ColCount = 151
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 96
      ColCount = 152
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      TabOrder = 33
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 125
      TabOrder = 10
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 125
      TabOrder = 9
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object GroupBoxSelection: TGroupBox
      Left = 16
      Top = 232
      Width = 545
      Height = 66
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 16
        Width = 193
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 40
        Width = 193
        Height = 17
        Caption = 'Export'
        TabOrder = 1
      end
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 120
      Top = 192
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
      OnClick = ChangeDate
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 254
      Top = 192
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      OnClick = ChangeDate
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 116
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 117
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 136
      Top = 313
      Width = 180
      Height = 19
      ColCount = 117
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 30
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 350
      Top = 313
      Width = 180
      Height = 19
      ColCount = 118
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 27
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 350
      Top = 192
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      OnClick = ChangeDate
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
  end
  inherited stbarBase: TStatusBar
    Top = 365
    Width = 574
  end
  inherited pnlBottom: TPanel
    Top = 384
    Width = 574
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4D040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072D4469616C6F675265706F7274446972496E644872735765
        656B462E44617461536F75726365456D706C46726F6D104F7074696F6E734375
        73746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E64
        53697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C75
        6D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E73
        44420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C65
        74650D6564676F43616E496E73657274116564676F43616E4E61766967617469
        6F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C
        5265636F726473106564676F557365426F6F6B6D61726B7300000F5464784442
        47726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06
        064E756D62657206536F7274656407046373557005576964746802410942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F45
        4D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E
        0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F727420
        6E616D6505576964746802540942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060A53484F52545F4E414D4500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E0604
        4E616D6505576964746803B4000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060B4445534352495054494F4E00000F5464
        78444247726964436F6C756D6E0D436F6C756D6E416464726573730743617074
        696F6E06074164647265737305576964746802450942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D650607414444524553530000
        0F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F646507
        43617074696F6E060F4465706172746D656E7420636F64650557696474680258
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F4445504152544D454E545F434F444500000F546478444247726964436F
        6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F
        64650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        14040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072B4469616C6F675265706F72
        74446972496E644872735765656B462E44617461536F75726365456D706C546F
        104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E
        670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E
        67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E
        6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D65
        64676F43616E44656C6574650D6564676F43616E496E73657274116564676F43
        616E4E617669676174696F6E116564676F436F6E6669726D44656C6574651265
        64676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D6172
        6B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C07
        43617074696F6E06064E756D62657206536F7274656407046373557005576964
        746802310942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F454D504C4F5945455F4E554D42455200000F54647844424772
        6964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E
        060A53686F7274206E616D65055769647468024E0942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D
        4500000F546478444247726964436F6C756D6E11436F6C756D6E446573637269
        7074696F6E0743617074696F6E06044E616D6505576964746803CC000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060B44
        45534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C
        756D6E416464726573730743617074696F6E0607416464726573730557696474
        6802650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506074144445245535300000F546478444247726964436F6C756D6E0E
        436F6C756D6E44657074436F64650743617074696F6E060F4465706172746D65
        6E7420636F64650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F4445504152544D454E545F434F444500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609
        5465616D20636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkSpot
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 336
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    Left = 480
    Top = 23
  end
end
