(*
  MRA:12-FEB-2010. RV054.2. 889950.
  - Add checkbox 'Show only empl. with planned absence'.
    This shows only employees with planned absence.
    REMARK: Also totals must be checked!
  MRA:27-JAN-2011 RV085.17. Bugfix. SC-50016754.
  - When teams-per-users is defined like:
    User=KEW and Teams=20,910,
    then when running this report with selection
    from team=20 to team=910, it also shows
    teams between this!
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - To get the report to landscape:
    - Make the size wider in layout to fit in landscape.
    - Then first change the qrptBase settings about Page to
      PaperSize=Custom, Length=210.0mm, WIdth=297.0mm.
    - After that change the page-settings in layout to
      PaperSize=Default.
*)
unit ReportStaffAvailabilityQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, UPimsConst;

type
  TQRParameters = class
  private
    FYear, FWeek, FSort: Integer;
    FShiftFrom, FShiftTo: String;
    FDeptFrom, FDeptTo, FTeamFrom, FTeamTo: String;
    FShowDept, FShowTeam, FShowPlant, FShowSelection, FPagePlant, FPageDept,
    FPageTeam, FPaidBreaks, FAllTeam, FAllDept: Boolean;
    FShowTotals, FShowOnlyAbsence, FExportToFile: Boolean;
  public
    procedure SetValues(Year, Week, Sort: Integer;
      ShiftFrom, ShiftTo: String;
      DeptFrom, DeptTo, TeamFrom, TeamTo: String;
      ShowSelection, ShowPlant, ShowDept, ShowTeam, PagePlant, PageDept,
      PageTeam, PaidBreaks, AllTeam, AllDept: Boolean;
      ShowTotals, ShowOnlyAbsence, ExportToFile: Boolean);
  end;

  TAvailPerDay = Array[1..7] of String;
  TAmountsPerDay = Array[1..7] of integer;
  TShiftPerDay = Array[1..7] of Integer; // RV054.2.
  TPlantPerDay = Array[1..7] of String; // RV054.2.

  TReportStaffAvailabilityQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDDept: TQRGroup;
    QRGroupHDTeam: TQRGroup;
    QRGroupHDShift: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGroupHDDate: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelWeek: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabelYear: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBTextShiftDesc: TQRDBText;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRBandSummay: TQRBand;
    QRLabel21: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRBandFooterEmpl: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRShape1: TQRShape;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRBand2: TQRBand;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabelPlant: TQRLabel;
    QRLabelDept: TQRLabel;
    QRLabelTeam: TQRLabel;
    QRBandFooterPlant: TQRBand;
    QRBandFooterDept: TQRBand;
    QRBandFooterTeam: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRLabel183: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    QRLabel187: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel196: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRLabel200: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel204: TQRLabel;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRLabel210: TQRLabel;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRLabel214: TQRLabel;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRLabel221: TQRLabel;
    QRLabel222: TQRLabel;
    QRLabel223: TQRLabel;
    QRLabel224: TQRLabel;
    QRLabel225: TQRLabel;
    QRLabel226: TQRLabel;
    QRLabel227: TQRLabel;
    QRLabel228: TQRLabel;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRLabel236: TQRLabel;
    QRLabel237: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    QRLabel246: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel249: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel251: TQRLabel;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    QRLabel259: TQRLabel;
    QRLabel260: TQRLabel;
    QRLabel261: TQRLabel;
    QRLabel262: TQRLabel;
    QRLabel263: TQRLabel;
    QRLabel264: TQRLabel;
    QRLabel265: TQRLabel;
    QRLabel266: TQRLabel;
    QRLabel267: TQRLabel;
    QRLabel268: TQRLabel;
    QRLabel269: TQRLabel;
    QRLabel270: TQRLabel;
    QRLabel271: TQRLabel;
    QRLabel272: TQRLabel;
    QRLabel273: TQRLabel;
    QRLabel274: TQRLabel;
    QRLabel275: TQRLabel;
    QRLabel276: TQRLabel;
    QRLabel277: TQRLabel;
    QRLabel278: TQRLabel;
    QRLabel279: TQRLabel;
    QRLabel280: TQRLabel;
    QRLabel281: TQRLabel;
    QRLabel282: TQRLabel;
    QRLabel283: TQRLabel;
    QRLabel284: TQRLabel;
    QRLabel285: TQRLabel;
    QRLabel286: TQRLabel;
    QRLabel287: TQRLabel;
    QRLabel288: TQRLabel;
    QRLabel289: TQRLabel;
    QRLabel290: TQRLabel;
    QRLabel291: TQRLabel;
    QRLabel292: TQRLabel;
    QRLabel293: TQRLabel;
    QRLabel294: TQRLabel;
    QRLabel295: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel296: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDeptDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextShiftDescPrint(sender: TObject; var Value: String);
    procedure QRLabel58Print(sender: TObject; var Value: String);
    procedure QRLabel46Print(sender: TObject; var Value: String);
    procedure QRLabel103Print(sender: TObject; var Value: String);
    procedure QRLabel104Print(sender: TObject; var Value: String);
    procedure QRLabel105Print(sender: TObject; var Value: String);
    procedure QRLabel106Print(sender: TObject; var Value: String);
    procedure QRLabel107Print(sender: TObject; var Value: String);
    procedure QRBandSummayBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel108Print(sender: TObject; var Value: String);
    procedure QRLabel109Print(sender: TObject; var Value: String);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel218Print(sender: TObject; var Value: String);
    procedure QRLabel217Print(sender: TObject; var Value: String);
    procedure QRLabel216Print(sender: TObject; var Value: String);
    procedure QRLabel215Print(sender: TObject; var Value: String);
    procedure QRLabel214Print(sender: TObject; var Value: String);
    procedure QRLabel213Print(sender: TObject; var Value: String);
    procedure QRLabel293Print(sender: TObject; var Value: String);
    procedure QRLabel294Print(sender: TObject; var Value: String);
    procedure QRLabel295Print(sender: TObject; var Value: String);
    procedure QRBandFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel265Print(sender: TObject; var Value: String);
    procedure QRLabel264Print(sender: TObject; var Value: String);
    procedure QRLabel263Print(sender: TObject; var Value: String);
    procedure QRLabel262Print(sender: TObject; var Value: String);
    procedure QRLabel261Print(sender: TObject; var Value: String);
    procedure QRLabel266Print(sender: TObject; var Value: String);
    procedure FormDestroy(Sender: TObject);
    procedure qrptBaseEndPage(Sender: TCustomQuickRep);
    procedure QRBandSummayAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBand2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHdPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDeptAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDTeamAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRBandFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterTeamAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDeptAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    FListCalc: TStringList;
    FEmpl: Integer;
    FEmplDesc, FPLANT, FDept, FDirectHoursYN: String;
    
    FMinDate, FMaxDate: TDateTime;
    FShiftPerDay: TShiftPerDay;
    FPlantPerDay: TPlantPerDay;
    FEmpAvailDate: TAvailPerDay;
    FScheduleDirect, FNotScheduleDirect,
    FScheduleInDirect, FNotScheduleIndirect, FAbsence, FTotal: TAmountsPerDay;

    FScheduleDirectTeam, FNotScheduleDirectTeam,
    FScheduleInDirectTeam, FNotScheduleIndirectTeam,
    FAbsenceTeam, FTotalTeam: TAmountsPerDay;

    FScheduleDirectPlant, FNotScheduleDirectPlant,
    FScheduleInDirectPlant, FNotScheduleIndirectPlant,
    FAbsencePlant, FTotalPlant: TAmountsPerDay;

    FScheduleDirectDept, FNotScheduleDirectDept,
    FScheduleInDirectDept, FNotScheduleIndirectDept,
    FAbsenceDept, FTotalDept: TAmountsPerDay;

    FAbsRsn: Array[1..7, 1..MAX_TBS] of String;
    FTBDay: Array[1..7, 1..MAX_TBS] of Integer;

    FQRParameters: TQRParameters;
//    Duration: TDateTime;
    function TestForAbsence(AValue: String): Boolean;
    procedure DetermineTotalsWholeWeek;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    procedure InitializeAvailPerDaysOfWeek(var AvailPerDaysOfWeek: TAvailPerDay);
    procedure FillStringsPerDaysOfWeek(IndexMin: Integer);
    procedure FillDescDaysPerWeek;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
      const Year, Week, Sort: Integer;
      const ShowPlant, ShowDept, ShowTeam, AllTeam, AllDept,
        ShowSelection, PagePlant, PageDept, PageTeam, PaidBreaks: Boolean;
        ShowTotals, ShowOnlyAbsence, ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    function GetEMAAvail(Plant: String; Shift, Empl: Integer;
      DateEMA: TDateTime; Day: Integer): String;
    procedure CalculateHrsEmpDate(Empl, Shift: Integer; Plant, Dept: String);
    function IsAbsence(AbsRsn: String): Boolean;
    procedure FillAmountsPerDaysOfWeek(IndexMin: Integer;
      AmountsPerDaysOfWeek: TAmountsPerDay);
    procedure InitializeArrays( var ScheduleDirect, NotScheduleDirect,
      ScheduleInDirect, NotScheduleIndirect, Absence, Total: TAmountsPerDay);
    procedure CalculateAmount(AmmountsPerDay: TAmountsPerDay;
      var Value: String);
  end;

var
  ReportStaffAvailabilityQR: TReportStaffAvailabilityQR;
  ExportEmpLine: String;
  ExportList: array[1..6] of String;
  ExportDescription: String;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportStaffAvailabilityDMT, ListProcsFRM,
  CalculateTotalHoursDMT, UGlobalFunctions;

procedure TQRParameters.SetValues(Year, Week, Sort: Integer;
  ShiftFrom, ShiftTo, DeptFrom, DeptTo, TeamFrom, TeamTo: String;
  ShowSelection, ShowPlant, ShowDept, ShowTeam, PagePlant, PageDept,
  PageTeam, PaidBreaks, AllTeam, AllDept: Boolean;
  ShowTotals, ShowOnlyAbsence, ExportToFile: Boolean);
begin
  FYear := Year;
  FWeek := Week;
  FSort := Sort;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FShowPlant := ShowPlant;
  FShowDept := ShowDept;
  FShowTeam := ShowTeam;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageDept := PageDept;
  FPageTeam := PageTeam;
  FPaidBreaks := PaidBreaks;
  FAllTeam := AllTeam;
  FAllDept := AllDept;
  FShowTotals := ShowTotals;
  FShowOnlyAbsence := ShowOnlyAbsence;
  FExportToFile := ExportToFile;
end;

function TReportStaffAvailabilityQR.QRSendReportParameters(
  const PlantFrom, PlantTo,
  DeptFrom, DeptTo, TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
  const Year, Week, Sort: Integer;
  const ShowPlant, ShowDept, ShowTeam,  AllTeam, AllDept,
    ShowSelection, PagePlant, PageDept, PageTeam, PaidBreaks: Boolean;
    ShowTotals, ShowOnlyAbsence, ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, Week, Sort, ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo,
      ShowSelection, ShowPlant, ShowDept, ShowTeam,
      PagePlant, PageDept, PageTeam, PaidBreaks, AllTeam, AllDept, ShowTotals,
      ShowOnlyAbsence, ExportToFile);
  end;
  SetDataSetQueryReport(ReportStaffAvailabilityDM.QueryEMA);
end;

function TReportStaffAvailabilityQR.ExistsRecords: Boolean;
var
  SelectStr, SelectSort,
  SelectPlant, SelectDept, SelectTeam, SelectTablePlant, SelectTableDept,
  SelectTableTeam, SelectWHEREPlant, SelectWHEREDept, SelectWHERETeam,
  SelectGroupByPlant, SelectGroupByDept, SelectGroupByTeam: String;
//  i: Integer;
begin
//Duration := Now;
  Screen.Cursor := crHourGlass;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FDeptFrom := '1';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FShiftFrom := '1';
    QRParameters.FShiftTo := '99';
  end;
  FMinDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeek, 1);
  FMaxDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeek, 7);

// begin set strings
  SelectStr := ' SELECT ' + NL;
  SelectPlant := ''; SelectDept := ''; SelectTeam := '';
  SelectTablePlant := ''; SelectTableDept := ''; SelectTableTeam := '';
  SelectWHEREPlant := ''; SelectWHEREDept := ''; SelectWHERETeam := '';
  SelectGroupByPlant := ''; SelectGroupByDept := ''; SelectGroupByTeam := '';
  SelectSort := '';
  if QRParameters.FShowPlant then
  begin
    SelectPlant := ' EMA.PLANT_CODE, P.DESCRIPTION AS PDESC, ' + NL;
    SelectTablePlant := ', PLANT  P' + NL;
    SelectWHEREPlant := ' AND EMA.PLANT_CODE = P.PLANT_CODE ' + NL;
    SelectGroupByPlant := ' EMA.PLANT_CODE ' + NL;
  end;
  if QRParameters.FShowDept then
  begin
    SelectDept :=
      ' E.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, DIRECT_HOUR_YN, ' + NL;
    SelectTableDept := ', DEPARTMENT  D' + NL;
    SelectWHEREDept := ' AND E.PLANT_CODE = D.PLANT_CODE AND ' + NL +
      ' E.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
    if (QRParameters.FShowPlant) then
      SelectGroupByDept := SelectGroupByDept + ',';
    SelectGroupByDept := SelectGroupByDept + 'E.DEPARTMENT_CODE ' + NL;
  end;
  if QRParameters.FShowTeam then
  begin
    SelectTeam := 'E.TEAM_CODE, T.DESCRIPTION AS TDESC, ' + NL;
    SelectTableTeam := ', TEAM  T' + NL;
    SelectWHERETeam := ' AND E.TEAM_CODE = T.TEAM_CODE ' + NL;
    if (QRParameters.FShowDept) or (QRParameters.FShowPlant) then
      SelectGroupByTeam := SelectGroupByTeam + ',';
    SelectGroupByTeam := SelectGroupByTeam + 'E.TEAM_CODE ' + NL;
  end;
  // MR:07-01-2003 Always get the combination:
  // Employee_Number + Description of EMPLOYEE
  if QRParameters.FSort = 0 then
    SelectSort := 'EMA.SHIFT_NUMBER, EMA.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL
  else
    SelectSort := 'EMA.SHIFT_NUMBER, E.DESCRIPTION, EMA.EMPLOYEE_NUMBER, ' + NL;
//  end set strings
  SelectStr := SelectStr + SelectPlant + SelectDept + SelectTeam + SelectSort +
    ' EMA.AVAILABLE_TIMEBLOCK_1, EMA.AVAILABLE_TIMEBLOCK_2, ' + NL +
    ' EMA.AVAILABLE_TIMEBLOCK_3, EMA.AVAILABLE_TIMEBLOCK_4, ' + NL +
    ' EMA.AVAILABLE_TIMEBLOCK_5, EMA.AVAILABLE_TIMEBLOCK_6, ' + NL +
    ' EMA.AVAILABLE_TIMEBLOCK_7, EMA.AVAILABLE_TIMEBLOCK_8, ' + NL +
    ' EMA.AVAILABLE_TIMEBLOCK_9, EMA.AVAILABLE_TIMEBLOCK_10, ' + NL +
// MR:26-11-2004 Also get PLANT_CODE from 'EA'.
    ' EMA.EMPLOYEEAVAILABILITY_DATE, EMA.PLANT_CODE AS EMAPLANT_CODE, ' + NL +
    ' E.PLANT_CODE AS EPLANT, ' + NL +
    ' E.DEPARTMENT_CODE AS EDEPT ' + NL +
    ' FROM EMPLOYEEAVAILABILITY EMA, EMPLOYEE E ' + NL;
  SelectStr := SelectStr + SelectTablePlant + SelectTableDept + SelectTableTeam;
  SelectStr := SelectStr +  ' WHERE ' + NL +
  // MR:26-11-2004 For 'PlanInOtherPlants' don't join plants!
//    ' EMA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND EMA.PLANT_CODE = E.PLANT_CODE ' +
    ' EMA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    // RV085.17 Addition of team-per-user-filtering!
    '  AND ' + NL +
    '  ( ' + NL +
    '    (:USER_NAME = ''*'') OR ' + NL +
    '    (E.TEAM_CODE IN ' + NL +
    '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
    '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
    '  ) ' + NL +
    ' AND EMA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART ' + NL +
    ' AND EMA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND  ' + NL +
    ' AND EMA.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    ' AND EMA.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FAllDept then
    SelectStr := SelectStr +
    ' AND E.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
    ' AND E.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL;
  SelectStr := SelectStr +
    ' AND EMA.SHIFT_NUMBER >= ' + QRParameters.FShiftFrom + ' AND ' + NL +
    ' EMA.SHIFT_NUMBER <= ' + QRParameters.FShiftTo + NL;
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr +
      ' AND E.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
      ' AND E.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
  SelectStr := SelectStr + SelectWHEREPlant + SelectWHEREDept + SelectWHERETeam;

//  SelectStr := SelectStr + ' GROUP BY ' ;
 // SelectStr := SelectStr + SelectGROUPBYPlant + SelectGROUPBYDept + SelectGROUPBYTeam;
 // if (QRParameters.FShowDept) or (QRParameters.FShowPlant) or (QRParameters.FShowTeam) then
 //     SelectStr := SelectStr + ',';
 // SelectStr := SelectStr + SelectSort + ' EMA.EMPLOYEEAVAILABILITY_DATE ';

  Selectstr := SelectStr + 'ORDER BY ' + NL;
  SelectStr := SelectStr + SelectGROUPBYPlant +
    SelectGROUPBYDept + SelectGROUPBYTeam;
  if (QRParameters.FShowDept) or (QRParameters.FShowPlant) or
    (QRParameters.FShowTeam) then
     SelectStr := SelectStr + ',';
  SelectStr := SelectStr + SelectSort + ' EMA.EMPLOYEEAVAILABILITY_DATE ';


  with ReportStaffAvailabilityDM do
  begin
    QueryEMA.Active := False;
    QueryEMA.UniDirectional := False;
    QueryEMA.SQL.Clear;
    QueryEMA.SQL.Add(UpperCase(SelectStr));
// QueryEMA.SQL.SaveToFile('c:\temp\reportstaffavail.sql');
    QueryEMA.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser; // RV085.17
    QueryEMA.ParamByName('FDATESTART').AsDateTime := FMinDate;
    QueryEMA.ParamByName('FDATEEND').AsDateTime := FMaxDate;
    if not QueryEMA.Prepared then
      QueryEMA.Prepare;
    QueryEMA.Active := True;
    Result := not QueryEMA.IsEmpty;
    if Result then
    begin
      QuerySHS.Close;
      ClientDataSetSHS.Close;
      QuerySHS.ParamByName('FDATEMIN').Value := FMinDate;
      QuerySHS.ParamByName('FDATEMAX').Value := FMaxDate;
// 550284 - For speed reason - set it here !
      ClientDataSetSHS.ProviderName := 'DataSetProviderSHS';
      ClientDataSetSHS.Open;
      QueryEmp.Close;
      QueryEmp.ParamByName('FDATEMIN').Value := FMinDate;
      QueryEmp.ParamByName('FDATEMAX').Value := FMaxDate;
      QueryEmp.Open;
    end;

  end;
  InitializeArrays( FScheduleDirect, FScheduleIndirect,
    FNotScheduleDirect, FNotScheduleIndirect, FAbsence, FTotal);


  FListCalc.Clear;
  //CAR 21-7-2003

{ check if report is empty}
  Screen.Cursor := crDefault;
end;

procedure TReportStaffAvailabilityQR.InitializeArrays(var ScheduleDirect,
  NotScheduleDirect, ScheduleInDirect, NotScheduleIndirect,
  Absence, Total: TAmountsPerDay);
var
  i: Integer;
begin
  for i:= 1 to 7 do
  begin
    ScheduleDirect[i] := 0;
    ScheduleIndirect[i] := 0;
    NotScheduleDirect[i] := 0;
    NotScheduleIndirect[i] := 0;
    Absence[i] := 0;
    Total[i] := 0;
  end;
end;

procedure TReportStaffAvailabilityQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
  QRLabelShiftTo.Caption := QRParameters.FShiftTo;
  if QRParameters.FAllDept then
  begin
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
  end
  else
  begin
    QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
    QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
  QRLabelWeek.Caption := IntToStr(QRParameters.FWeek);

  QRGroupHDPlant.Enabled := False;
  QRGroupHDDept.Enabled := False;
  QRGroupHDTeam.Enabled := False;
  QRBandFooterPlant.Enabled := False;
  QRBandFooterDept.Enabled := False;
  QRBandFooterTeam.Enabled := False;

  QRGroupHDEmpl.Enabled := True;
  QRBandFooterEmpl.Enabled := True;
  if QRParameters.FShowDept then
  begin
    QRGroupHDDept.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FShowTeam then
  begin
    QRGroupHDTeam.Enabled := True;
    QRBandFooterTeam.Enabled := True;
  end;
  if QRParameters.FShowPlant then
  begin
    QRGroupHDPlant.Enabled := True;
    QRBandFooterPlant.Enabled := True;
  end;
  // MR:07-01-2003
  // Always search on Employee_number,
  // because 'description' cannot be unique.
 
  if QRParameters.FSort = 0 then
     QRGroupHDEmpl.Expression := 'QUERYEMA.EMPLOYEE_NUMBER'
 else
    QRGroupHDEmpl.Expression := 'QUERYEMA.DESCRIPTION';

end;

procedure TReportStaffAvailabilityQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportStaffAvailabilityQR.FormCreate(Sender: TObject);
var
  TBCaption: String;
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  FListCalc := TStringList.Create;
  // MR:22-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  // GLOB3-60
  // Some texts must be changed here
  if SystemDM.MaxTimeblocks = 10 then
    TBCaption := '12345678910'
  else
    TBCaption := '1234';
  QRLabel116.Caption := TBCaption;
  QRLabel117.Caption := TBCaption;
  QRLabel120.Caption := TBCaption;
  QRLabel121.Caption := TBCaption;
  QRLabel124.Caption := TBCaption;
  QRLabel125.Caption := TBCaption;
  QRLabel128.Caption := TBCaption;
end;

procedure TReportStaffAvailabilityQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  i: Integer;
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Init variables
    ExportEmpLine := '';
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel1.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' + QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelTeamFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' + QRLabelTeamTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLabelShiftFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLabelShiftTo.Caption);
      ExportClass.AddText(QRlabel2.Caption + ' ' + QRLabelYear.Caption);
      ExportClass.AddText(QRLabelDate.Caption + ' ' + QRLabelWeek.Caption);
    end;
  end;
  for i:= 1 to 7 do
  begin
    FScheduleDirect[i] := 0;
    FScheduleIndirect[i] := 0;
    FNotScheduleDirect[i] := 0;
    FNotScheduleIndirect[i] := 0;
    FAbsence[i] := 0;
    FTotal[i] := 0;
  end;
end;


procedure TReportStaffAvailabilityQR.InitializeAvailPerDaysOfWeek
 (var AvailPerDaysOfWeek: TAvailPerDay);
var
  Counter: Integer;
begin
  for Counter := 1 to 7 do
  begin
    AvailPerDaysOfWeek[Counter] :=  '';
    FShiftPerDay[Counter] := 1;
    FPlantPerDay[Counter] := '';
  end;
end;

procedure TReportStaffAvailabilityQR.FillStringsPerDaysOfWeek(IndexMin: Integer);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
      begin
        (TempComponent as TQRLabel).Caption :=
          (FEmpAvailDate[Index - IndexMin + 1]); // + '  '); // GLOB3-60 Do not add spaces
      end;
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.FillAmountsPerDaysOfWeek(IndexMin: Integer;
  AmountsPerDaysOfWeek: TAmountsPerDay);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if ((Index >= IndexMin) and (Index <= IndexMin + 6)) then
      begin
        (TempComponent as TQRLabel).Caption :=
          CalculateTotalHoursDM.DecodeHrsMin(AmountsPerDaysOfWeek[Index - IndexMin + 1]);
      end;
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 10) and (Index <= 16) then
      begin
        (TempComponent as TQRLabel).Caption :=
          SystemDM.GetDayWCode(Index - 10 + 1);
      end;
    end;
  end;
end;

// RV054.2.
function TReportStaffAvailabilityQR.TestForAbsence(AValue: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  if AValue <> '' then
  begin
    for I := 1 to Length(AValue) do
      if IsAbsence(AValue[I]) then
        Result := True;
  end;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Index: Integer;
  AbsenceFound: Boolean;
begin
  inherited;
  PrintBand := True;
  // RV054.2. When ShowOnlyAbsence, then look if there is any absence in the days
  // of the employee. If not, then do not print (PrintBand := False).
  if QRParameters.FShowOnlyAbsence then
  begin
    for Index := 1 to 7 do
    begin
      AbsenceFound := TestForAbsence(FEmpAvailDate[Index]);
      if AbsenceFound then
        Break;
    end;
    if not AbsenceFound then
      PrintBand := False;
  end;
  if PrintBand then
    FillStringsPerDaysOfWeek(70);
end;

procedure TReportStaffAvailabilityQR.QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var

  Plant: String; // MR:07-01-2003
begin
  QRGroupHdDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
  with ReportStaffAvailabilityDM do
  begin
    // MR:07-01-2003 Always search on employee_number, because 'description'
    // is not unique.
   //CAR - 14-7-2003                                        // EPLANT
    Plant := ReportStaffAvailabilityDM.QueryEMA.FieldByName('EPLANT').AsString;
    QRLabelDept.Caption := QueryEMA.FieldByName('DDESC').AsString;
  end;
  InitializeArrays(FScheduleDirectDept, FScheduleIndirectDept,
    FNotScheduleDirectDept, FNotScheduleIndirectDept, FAbsenceDept,
    FTotalDept);
end;

procedure TReportStaffAvailabilityQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  QRLabelPlant.Caption := ReportStaffAvailabilityDM.QueryEMA.FieldByName('PDESC').AsString;
  InitializeArrays(FScheduleDirectPlant, FScheduleIndirectPlant,
    FNotScheduleDirectPlant, FNotScheduleIndirectPlant, FAbsencePlant,
    FTotalPlant);
end;

procedure TReportStaffAvailabilityQR.QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdTeam.ForceNewPage := QRParameters.FPageTeam;
  inherited;
  with ReportStaffAvailabilityDM do
  begin
    QRLabelTeam.Caption := QueryEMA.FieldByName('TDESC').AsString;
  end;
  InitializeArrays( FScheduleDirectTeam, FScheduleIndirectTeam,
    FNotScheduleDirectTeam, FNotScheduleIndirectTeam, FAbsenceTeam, FTotalTeam);
end;

function TReportStaffAvailabilityQR.GetEMAAvail(Plant: String;
  Shift, Empl: Integer; DateEMA: TDateTime; Day: Integer): String;
var
  i: Integer;
  AvailTB: String;
  Found: Boolean;
begin
  Result := '';
  for i := 1 to SystemDM.MaxTimeblocks do
    FAbsRsn[Day, i] := '';
  with ReportStaffAvailabilityDM do
  begin
    // MR:14-10-2004 Locate + datefield can give errors,
    // depending how the Windows-date-format been set.
    // Use 'ADateFmt'-routines to solve this.
//    if ClientDataSetSHS.Locate('EMPLOYEE_NUMBER;SHIFT_SCHEDULE_DATE',
//      VarArrayOf([Empl, DateEMA]), []) then
    SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
    Found := ClientDataSetSHS.Locate('EMPLOYEE_NUMBER;SHIFT_SCHEDULE_DATE',
      VarArrayOf([Empl,
        FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, DateEMA)]), []);
    SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
    if Found then
    begin
      Result := '';
      for i := 1 to SystemDM.MaxTimeblocks do
      begin
        AvailTB := QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(i)).AsString;
        Result := Result + AvailTB ;
        Result := Result + ' ';
        FAbsRsn[Day, i] := AvailTB;
      end;
      if Result = '' then
        Result := 'N/A';
    end;

  end;
end;

procedure TReportStaffAvailabilityQR.CalculateHrsEmpDate(Empl, Shift: Integer;
  Plant, Dept: String );
var
  valueint,p, indexlist,i, j: integer;
  strtmp: string;
begin
  if not QRParameters.FShowTotals then
    Exit;
  indexList := FListCalc.IndexOf(IntToStr(Empl) + '@' + IntToStr(Shift) + '@' +
    Plant + '@' + Dept);
//CAR 9-7-2003 add extra check for index + 1
  if (IndexList >= 0) and ((IndexList + 1) <= (FListCalc.Count -1)) then
  begin
    StrTmp := FListCalc.Strings[IndexList + 1];
    for i := 1 to 7 do
      for j:= 1 to SystemDM.MaxTimeblocks do
      begin
        p := Pos(',', StrTmp);
        FTBDay[i,j] := StrToInt(Copy(StrTmp, 0, p -1));
        StrTmp := Copy(StrTmp, p + 1, Length(StrTmp) + 1);
      end;
  end
  else
  begin
//CAR 22-7-2003
    ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept, QRParameters.FPaidBreaks);

    FListCalc.Add(IntToStr(Empl) + '@' + IntToStr(Shift) + '@' +
      Plant + '@' + Dept);
    StrTmp := '';
    for i := 1 to 7 do
     for j := 1 to SystemDM.MaxTimeblocks do
     begin
     //CAR 22-7-2003
       ValueInt := ATBLengthClass.GetLengthTB(j, i);
       StrTmp := StrTmp + IntToStr(ValueInt) + ',' ;
       FTBDay[i,j] := ValueInt;
     end;
     StrTmp := StrTmp + ',';
    FListCalc.Add(StrTmp);
   end;
end;

function TReportStaffAvailabilityQR.IsAbsence(AbsRsn: String): Boolean;
begin
//CAR 8-7-2003 - performance changes
  Result :=
    ReportStaffAvailabilityDM.ClientDataSetAbs.Locate('ABSENCEREASON_CODE',
      VarArrayOf([AbsRsn]), []);
end;

procedure TReportStaffAvailabilityQR.QRGroupHDDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  DateEMA: TDateTime;
  Day: Integer;
begin
  inherited;
  PrintBand := False;
  with ReportStaffAvailabilityDM do
  begin
    DateEMA := QueryEMA.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
    Day :=  ListProcsF.DayWStartOnFromDate(DateEMA);
    FEmpAvailDate[Day] := GetEMAAvail(FPlant,
      QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger, FEmpl, DateEMA, Day);
    FShiftPerDay[Day] := QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger;
    FPlantPerDay[Day] := FPlant;
  end;
end;

procedure TReportStaffAvailabilityQR.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillDescDaysPerWeek;
end;

procedure TReportStaffAvailabilityQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  InitializeAvailPerDaysOfWeek(FEmpAvailDate);
  // MR:07-01-2003 Always search on employee_number, because 'description'
  // is not unique.
  FEmpl :=
    ReportStaffAvailabilityDM.QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FEmplDesc :=
    ReportStaffAvailabilityDM.QueryEMA.FieldByName('DESCRIPTION').AsString;
                                                           // EDEPT
  FDept := ReportStaffAvailabilityDM.QueryEMA.FieldByName('EDEPT').AsString;
  if QRParameters.FShowPlant then
    FPlant := ReportStaffAvailabilityDM.QueryEMA.FieldByName('PLANT_CODE').AsString
  else                                                      // EPLANT
    FPlant := ReportStaffAvailabilityDM.QueryEMA.FieldByName('EPLANT').AsString;

  if ReportStaffAvailabilityDM.TableDEPT.FindKey([FPlant, FDept]) then
    FDirectHoursYN :=
      ReportStaffAvailabilityDM.TableDEPT.FieldByName('DIRECT_HOUR_YN').AsString
  else
    FDirectHoursYN := '';
end;

procedure TReportStaffAvailabilityQR.QRDBTextDeptDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := ReportStaffAvailabilityDM.QueryEMA.FieldByName('DDESC').AsString;
end;

procedure TReportStaffAvailabilityQR.QRDBTextShiftDescPrint(sender: TObject;
  var Value: String);
var
  Plant: String;
begin
  inherited;
  with ReportStaffAvailabilityDM do
  begin
    Value := '';
    // MR:07-01-2003 Always search on employee_number, because 'description'
    // is not unique.
    //CAR 14-7-2003
    FEmpl := QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    FEmplDesc := QueryEMA.FieldByName('DESCRIPTION').AsString;
    // MR:26-11-2004 Get Plant from EmployeeAvailability, not from Employee.
//    Plant := QueryEMA.FieldByName('EPLANT').AsString;
    Plant := QueryEMA.FieldByName('EMAPLANT_CODE').AsString;

    if ClientDataSetSHIFT.FindKey([
      Plant, QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger]) then
      Value := ClientDataSetSHIFT.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffAvailabilityQR.QRLabel58Print(sender: TObject;
  var Value: String);
var
  Shift, Empl: Integer;
  Plant: String;
begin
  inherited;
  Value := '';
  with ReportStaffAvailabilityDM do
  begin
    Shift := QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger;
    // MR:07-01-2003 Always search on employee_number, because 'description'
    // is not unique.
    //CAR 14-7-2003
    Plant := '';
    Empl := QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsInteger;
                                  // EPLANT
    Plant := QueryEMA.FieldByName('EPLANT').AsString;

    if ClientDataSetTBPerEmpl.Locate(
      'EMPLOYEE_NUMBER;PLANT_CODE;SHIFT_NUMBER;TIMEBLOCK_NUMBER',
      VarArrayOf([Empl, Plant, Shift, 1]), []) then
      Value := 'X';
  end;
  if QRParameters.FExportToFile then
    ExportEmpLine := ExportEmpLine + Value + ExportClass.Sep;
end;

procedure TReportStaffAvailabilityQR.QRLabel46Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleDirect, Value);
  if QRParameters.FExportToFile then
    ExportList[1] := Value;
end;

procedure TReportStaffAvailabilityQR.CalculateAmount(AmmountsPerDay: TAmountsPerDay;
  var Value: String);
var
  i, Total: Integer;
begin
  inherited;
  Total := 0;
  for i:= 1 to 7 do
    Total := Total + AmmountsPerDay[i];
  Value := CalculateTotalHoursDM.DecodeHrsMin(Total);
end;

procedure TReportStaffAvailabilityQR.QRLabel103Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleDirect, Value);
  if QRParameters.FExportToFile then
    ExportList[2] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel104Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleInDirect, Value);
  if QRParameters.FExportToFile then
    ExportList[3] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel105Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleInDirect, Value);
  if QRParameters.FExportToFile then
    ExportList[4] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel106Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FAbsence, Value);
  if QRParameters.FExportToFile then
    ExportList[5] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel107Print(sender: TObject;
  var Value: String);
var
  i, Total: Integer;
begin
  inherited;
  Total := 0;
  for i:= 1 to 7 do
  begin
    Total := Total + FScheduleDirect[i] + FNotScheduleDirect[i] +
      FScheduleIndirect[i] + FNotScheduleIndirect[i] + FAbsence[i];
  end;
  Value := CalculateTotalHoursDM.DecodeHrsMin(Total);
  if QRParameters.FExportToFile then
    ExportList[6] := Value;
end;

procedure TReportStaffAvailabilityQR.QRBandSummayBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: Integer;
begin
  inherited;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  for i:= 1 to 7 do
    FTotal[i] := FTotal[i] + FScheduleDirect[i] + FScheduleIndirect[i] +
     FNotScheduleDirect[i] + FNotScheduleIndirect[i] + FAbsence[i] ;

  FillAmountsPerDaysOfWeek(320, FScheduleDirect);
  FillAmountsPerDaysOfWeek(330, FNotScheduleDirect);
  FillAmountsPerDaysOfWeek(340, FScheduleInDirect);
  FillAmountsPerDaysOfWeek(350, FNotScheduleInDirect);
  FillAmountsPerDaysOfWeek(360, FAbsence);
  FillAmountsPerDaysOfWeek(380, FTotal);
end;

procedure TReportStaffAvailabilityQR.QRLabel108Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := IntToStr(FEmpl);
  if QRParameters.FExportToFile then
    ExportEmpLine := Value + ' ';
end;

procedure TReportStaffAvailabilityQR.QRLabel109Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(FEmplDesc, 0 , 25);
  if QRParameters.FExportToFile then
    ExportEmpLine := ExportEmpLine + Value + ' ' + ExportClass.Sep +
      ExportClass.Sep;
end;

procedure TReportStaffAvailabilityQR.QRBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillDescDaysPerWeek;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: integer;
begin
  inherited;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;

  for i:= 1 to 7 do
    FTotalTeam[i] := FScheduleDirectTeam[i] + FScheduleIndirectTeam[i] +
     FNotScheduleDirectTeam[i] + FNotScheduleIndirectTeam[i] + FAbsenceTeam[i] ;

  FillAmountsPerDaysOfWeek(20, FScheduleDirectTeam);
  FillAmountsPerDaysOfWeek(30, FNotScheduleDirectTeam);
  FillAmountsPerDaysOfWeek(40, FScheduleIndirectTeam);
  FillAmountsPerDaysOfWeek(50, FNotScheduleIndirectTeam);
  FillAmountsPerDaysOfWeek(60, FAbsenceTeam);
  FillAmountsPerDaysOfWeek(80, FTotalTeam);

end;

procedure TReportStaffAvailabilityQR.QRLabel218Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleDirectDept, Value);
  if QRParameters.FExportToFile then
    ExportList[1] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel217Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleDirectDept, Value);
  if QRParameters.FExportToFile then
    ExportList[2] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel216Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleInDirectDept, Value);
  if QRParameters.FExportToFile then
    ExportList[3] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel215Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleInDirectDept, Value);
  if QRParameters.FExportToFile then
    ExportList[4] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel214Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FAbsenceDept, Value);
  if QRParameters.FExportToFile then
    ExportList[5] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel213Print(sender: TObject;
  var Value: String);
var
  i, Total: Integer;
begin
  inherited;
  Total := 0;
  for i:= 1 to 7 do
  begin
    Total := Total + FScheduleDirectDept[i] + FNotScheduleDirectDept[i] +
      FScheduleIndirectDept[i] + FNotScheduleIndirectDept[i] + FAbsenceDept[i];
  end;
  Value := CalculateTotalHoursDM.DecodeHrsMin(Total);
  if QRParameters.FExportToFile then
    ExportList[6] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel293Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := ReportStaffAvailabilityDM.QueryEMA.FieldByName('PDESC').AsString;
  if QRParameters.FExportToFile then
    ExportDescription := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel294Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := ReportStaffAvailabilityDM.QueryEMA.FieldByName('DDESC').AsString;
  if QRParameters.FExportToFile then
    ExportDescription := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel295Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := ReportStaffAvailabilityDM.QueryEMA.FieldByName('TDESC').AsString;
  if QRParameters.FExportToFile then
    ExportDescription := Value;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: integer;
begin
  inherited;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  for i:= 1 to 7 do
    FTotalDept[i] := FScheduleDirectDept[i] + FScheduleIndirectDept[i] +
     FNotScheduleDirectDept[i] + FNotScheduleIndirectDept[i] + FAbsenceDept[i] ;

  FillAmountsPerDaysOfWeek(120, FScheduleDirectDept);
  FillAmountsPerDaysOfWeek(130, FNotScheduleDirectDept);
  FillAmountsPerDaysOfWeek(140, FScheduleIndirectDept);
  FillAmountsPerDaysOfWeek(150, FNotScheduleIndirectDept);
  FillAmountsPerDaysOfWeek(160, FAbsenceDept);
  FillAmountsPerDaysOfWeek(180, FTotalDept);

end;

procedure TReportStaffAvailabilityQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: integer;
begin
  inherited;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  for i:= 1 to 7 do
    FTotalPlant[i] := FScheduleDirectPlant[i] + FScheduleIndirectPlant[i] +
     FNotScheduleDirectPlant[i] + FNotScheduleIndirectPlant[i] + FAbsencePlant[i] ;

  FillAmountsPerDaysOfWeek(220, FScheduleDirectPlant);
  FillAmountsPerDaysOfWeek(230, FNotScheduleDirectPlant);
  FillAmountsPerDaysOfWeek(240, FScheduleIndirectPlant);
  FillAmountsPerDaysOfWeek(250, FNotScheduleIndirectPlant);
  FillAmountsPerDaysOfWeek(260, FAbsencePlant);
  FillAmountsPerDaysOfWeek(280, FTotalPlant);


end;
procedure TReportStaffAvailabilityQR.QRLabel265Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleDirectPlant, Value);
  if QRParameters.FExportToFile then
    ExportList[2] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel264Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleInDirectPlant, Value);
  if QRParameters.FExportToFile then
    ExportList[3] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel263Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FNotScheduleInDirectPlant, Value);
  if QRParameters.FExportToFile then
    ExportList[4] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel262Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FAbsencePlant, Value);
  if QRParameters.FExportToFile then
    ExportList[5] := Value;
end;

procedure TReportStaffAvailabilityQR.QRLabel261Print(sender: TObject;
  var Value: String);
var
  i, Total: Integer;
begin
  inherited;
  Total := 0;
  for i:= 1 to 7 do
  begin
    Total := Total + FScheduleDirectPlant[i] + FNotScheduleDirectPlant[i] +
      FScheduleIndirectPlant[i] + FNotScheduleIndirectPlant[i] + FAbsencePlant[i];
  end;
  Value := CalculateTotalHoursDM.DecodeHrsMin(Total);
  if QRParameters.FExportToFile then
    ExportList[6] := Value;
end;


procedure TReportStaffAvailabilityQR.QRLabel266Print(sender: TObject;
  var Value: String);
begin
  inherited;
  CalculateAmount(FScheduleDirectPlant, Value);
  if QRParameters.FExportToFile then
    ExportList[1] := Value;
end;

procedure TReportStaffAvailabilityQR.FormDestroy(Sender: TObject);
begin
  inherited;
  FListCalc.Free;
end;

procedure TReportStaffAvailabilityQR.qrptBaseEndPage(
  Sender: TCustomQuickRep);
begin
  inherited;
{  if ReportStaffAvailabilityDM.QueryEMA.Eof then
    DisplayMessage(IntToStr(Trunc((Now - Duration) * MSecsPerDay)),
    mtInformation, [mbOK]);}
end;

procedure TReportStaffAvailabilityQR.QRBandSummayAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(QRLabel38.Caption);
      ExportClass.AddText(
        QRLabel39.Caption + ExportClass.Sep + QRLabel97.Caption + ExportClass.Sep +
        QRLabel4.Caption + ExportClass.Sep + QRLabel6.Caption + ExportClass.Sep +
        QRLabel16.Caption + ExportClass.Sep + QRLabel21.Caption + ExportClass.Sep +
        QRLabel25.Caption + ExportClass.Sep + QRLabel34.Caption + ExportClass.Sep +
        QRLabel45.Caption + ExportClass.Sep + ExportList[1]);
      ExportClass.AddText(
        QRLabel40.Caption + ExportClass.Sep + QRLabel98.Caption + ExportClass.Sep +
        QRLabel43.Caption + ExportClass.Sep + QRLabel44.Caption + ExportClass.Sep +
        QRLabel59.Caption + ExportClass.Sep + QRLabel60.Caption + ExportClass.Sep +
        QRLabel61.Caption + ExportClass.Sep + QRLabel62.Caption + ExportClass.Sep +
        QRLabel63.Caption + ExportClass.Sep + ExportList[2]);
      ExportClass.AddText(
        QRLabel41.Caption + ExportClass.Sep + QRLabel99.Caption + ExportClass.Sep +
        QRLabel64.Caption + ExportClass.Sep + QRLabel65.Caption + ExportClass.Sep +
        QRLabel66.Caption + ExportClass.Sep + QRLabel67.Caption + ExportClass.Sep +
        QRLabel68.Caption + ExportClass.Sep + QRLabel69.Caption + ExportClass.Sep +
        QRLabel70.Caption + ExportClass.Sep + ExportList[3]);
      ExportClass.AddText(
        QRLabel42.Caption + ExportClass.Sep + QRLabel100.Caption + ExportClass.Sep +
        QRLabel71.Caption + ExportClass.Sep + QRLabel72.Caption + ExportClass.Sep +
        QRLabel73.Caption + ExportClass.Sep + QRLabel74.Caption + ExportClass.Sep +
        QRLabel75.Caption + ExportClass.Sep + QRLabel76.Caption + ExportClass.Sep +
        QRLabel77.Caption + ExportClass.Sep + ExportList[4]);
      ExportClass.AddText(
        QRLabel78.Caption + ExportClass.Sep + QRLabel101.Caption + ExportClass.Sep +
        QRLabel80.Caption + ExportClass.Sep + QRLabel81.Caption + ExportClass.Sep +
        QRLabel82.Caption + ExportClass.Sep + QRLabel83.Caption + ExportClass.Sep +
        QRLabel84.Caption + ExportClass.Sep + QRLabel85.Caption + ExportClass.Sep +
        QRLabel86.Caption + ExportClass.Sep + ExportList[5]);
      ExportClass.AddText(
        QRLabel79.Caption + ExportClass.Sep + QRLabel102.Caption + ExportClass.Sep +
        QRLabel89.Caption + ExportClass.Sep + QRLabel91.Caption + ExportClass.Sep +
        QRLabel92.Caption + ExportClass.Sep + QRLabel93.Caption + ExportClass.Sep +
        QRLabel94.Caption + ExportClass.Sep + QRLabel95.Caption + ExportClass.Sep +
        QRLabel96.Caption + ExportClass.Sep + ExportList[6]);
    end;
  end;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportStaffAvailabilityQR.QRBand2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel110.Caption + ExportClass.Sep +
      QRLabel111.Caption + QRLabel114.Caption + ExportClass.Sep +
      QRLabel115.Caption + ExportClass.Sep +
      QRLabel118.Caption + ExportClass.Sep +
      QRLabel119.Caption + ExportClass.Sep +
      QRLabel122.Caption + ExportClass.Sep +
      QRLabel123.Caption + ExportClass.Sep +
      QRLabel126.Caption + ExportClass.Sep +
      QRLabel127.Caption + ExportClass.Sep +
      QRLabel130.Caption + ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      QRLabel112.Caption + QRLabel113.Caption + ExportClass.Sep +
      '"' + QRLabel116.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel117.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel120.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel121.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel124.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel125.Caption + '"' + ExportClass.Sep +
      '"' + QRLabel128.Caption + '"' + ExportClass.Sep +
      QRLabel129.Caption + ExportClass.Sep);
  end;
end;

procedure TReportStaffAvailabilityQR.QRGroupHdPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(QRLabel54.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabelPlant.Caption);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRGroupHDDeptAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(QRLabel55.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('DEPARTMENT_CODE').AsString + ExportClass.Sep +
        QRLabelDept.Caption);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRGroupHDTeamAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(QRLabel35.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('TEAM_CODE').AsString + ExportClass.Sep +
        QRLabelTeam.Caption);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRGroupHDShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(QRLabel3.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('SHIFT_NUMBER').AsString + ExportClass.Sep +
        QueryEMA.FieldByName('DESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRLabel37Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportEmpLine := ExportEmpLine + Value + ExportClass.Sep;
end;

procedure TReportStaffAvailabilityQR.DetermineTotalsWholeWeek;
var
  Day: Integer;
  DateEMA: TDateTime;
  procedure DetermineTotals;
  var
    Shift, Index: Integer;
    DirectHoursYN, ScheduledTB: String;
    PlannedHrs: Integer;
    TBSch: Array[1..MAX_TBS] of Integer;
  begin
    with ReportStaffAvailabilityDM do
    begin
//      Shift := QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger;
      Shift := FShiftPerDay[Day];
      FPlant := FPlantPerDay[Day];

      for Index := 1 to SystemDM.MaxTimeblocks do
        TBSch[Index] := 0;
      if (Pos('*', FEmpAvailDate[Day]) > 0) then
      begin
        // MR:14-10-2004 Locate + datefield can give errors,
        // depending how the Windows-date-format been set.
        // Use 'ADateFmt'-routines to solve this.
        SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
        if QueryEmp.
          Locate('EMPLOYEEPLANNING_DATE;EMPLOYEE_NUMBER;SHIFT_NUMBER;PLANT_CODE',
         VarArrayOf([FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, DateEMA),
           FEmpl, Shift, FPlant]), []) then
        begin
          while (not QueryEmp.Eof) and
            (QueryEmp.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime = DateEMA) and
            (QueryEmp.FieldByName('EMPLOYEE_NUMBER').AsInteger = FEmpl) and
            (QueryEmp.FieldByName('SHIFT_NUMBER').AsInteger = Shift) and
            (QueryEmp.FieldByName('PLANT_CODE').AsString = FPlant) do
          begin
        //   EmplPlanned := True;
            CalculateHrsEmpDate(FEmpl, Shift, FPlant,
               QueryEmp.FieldByNAME('DEPARTMENT_CODE').AsString);
            DirectHoursYN := QueryEmp.FieldByName('DIRECT_HOUR_YN').AsString;
            for Index := 1 to SystemDM.MaxTimeblocks do
            begin
              if (FAbsRsn[Day, Index] = '*') and (FTBDay[Day, Index] <> 0) then
              begin
                ScheduledTB :=
                  QueryEmp.FieldByName('SCHEDULED_TIMEBLOCK_'+ IntToStr(Index)).AsString;
                PlannedHrs := FTBDay[Day,Index];
                if (DirectHoursYN = 'Y') then
                begin
                  if (ScheduledTB = 'A') or (ScheduledTB = 'B') or
                    (ScheduledTB = 'C') then
                  begin
                    FScheduleDirect[Day] := FScheduleDirect[Day] + PlannedHrs;
                    FScheduleDirectTeam[Day] :=
                      FScheduleDirectTeam[Day] + PlannedHrs;
                    FScheduleDirectPlant[Day] :=
                      FScheduleDirectPlant[Day] + PlannedHrs;
                    FScheduleDirectDept[Day] :=
                      FScheduleDirectDept[Day] + PlannedHrs;
                    TBSch[Index] := Index;
                  end;
                end
                else
                begin
                  if (ScheduledTB = 'A')  or (ScheduledTB = 'B') or
                    (ScheduledTB = 'C') then
                  begin
                    FScheduleInDirect[Day] := FScheduleInDirect[Day] + PlannedHrs;
                    FScheduleInDirectTeam[Day] :=
                      FScheduleInDirectTeam[Day] + PlannedHrs;
                    FScheduleInDirectPlant[Day] :=
                      FScheduleInDirectPlant[Day] + PlannedHrs;
                    FScheduleInDirectDept[Day] :=
                      FScheduleInDirectDept[Day] + PlannedHrs;
                    TBSch[Index] := Index;
                  end;
                end; // if (DirectHoursYN = 'Y') then
              end; {if * or absrsn}
            end; // for Index
            QueryEMP.Next;
          end; {while}
        end; // if QueryEmp.Locate...
        SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
      end; // if Pos()
    end; // with

    // if not planned
    if FDirectHoursYN <> '' then
    begin
      CalculateHrsEmpDate(FEmpl, Shift, FPlant, FDept);
      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        PlannedHrs := FTBDay[Day,Index];
        if ((FAbsRsn[Day, Index] = '*') and  (TBSCH[Index] = 0) and
          (PlannedHrs <> 0)) then
        begin
          if (FDirectHoursYN = 'Y') then
          begin
            FNotScheduleDirect[Day] := FNotScheduleDirect[Day] + PlannedHrs;
            FNotScheduleDirectTeam[Day] := FNotScheduleDirectTeam[Day] + PlannedHrs;
            FNotScheduleDirectPlant[Day] := FNotScheduleDirectPlant[Day] + PlannedHrs;
            FNotScheduleDirectDept[Day] := FNotScheduleDirectDept[Day] + PlannedHrs;
          end;
          if (FDirectHoursYN = 'N') then
          begin
            FNotScheduleInDirect[Day] := FNotScheduleInDirect[Day] + PlannedHrs;
            FNotScheduleInDirectTeam[Day] := FNotScheduleInDirectTeam[Day] + PlannedHrs;
            FNotScheduleInDirectPlant[Day] := FNotScheduleInDirectPlant[Day] + PlannedHrs;
            FNotScheduleInDirectDept[Day] := FNotScheduleInDirectDept[Day] + PlannedHrs;
          end;
         end;
         if (PlannedHrs <> 0) and (IsAbsence(FAbsRsn[Day, Index])) then
          begin
            FAbsence[Day] := FAbsence[Day] + PlannedHrs;
            FAbsenceTeam[Day] := FAbsenceTeam[Day] + PlannedHrs;
            FAbsencePlant[Day] := FAbsencePlant[Day] + PlannedHrs;
            FAbsenceDept[Day] := FAbsenceDept[Day] + PlannedHrs;
          end;
      end;{for index}
    end;{if FDirectHoursYN}
  end;
begin
  if QRParameters.FShowOnlyAbsence then
  begin
    if
      (
        TestForAbsence(FEmpAvailDate[1]) or
        TestForAbsence(FEmpAvailDate[2]) or
        TestForAbsence(FEmpAvailDate[3]) or
        TestForAbsence(FEmpAvailDate[4]) or
        TestForAbsence(FEmpAvailDate[5]) or
        TestForAbsence(FEmpAvailDate[6]) or
        TestForAbsence(FEmpAvailDate[7])
      ) then
    else
      Exit;
  end;

  DateEMA := FMinDate;
  for Day := 1 to 7 do
  begin
    DetermineTotals;
    DateEMA := DateEMA + 1;
  end;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // RV054.2.
  // Determine totals here.
  if QRParameters.FShowTotals then
    DetermineTotalsWholeWeek;

  if QRParameters.FExportToFile and BandPrinted then
    ExportClass.AddText(ExportEmpLine);
end;

procedure TReportStaffAvailabilityQR.QRBandFooterTeamAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(
        QRLabel5.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('TEAM_CODE').AsString + ExportClass.Sep +
        ExportDescription);
      ExportClass.AddText(
        QRLabel7.Caption + ExportClass.Sep + QRLabel26.Caption + ExportClass.Sep +
        QRLabel27.Caption + ExportClass.Sep + QRLabel134.Caption + ExportClass.Sep +
        QRLabel135.Caption + ExportClass.Sep + QRLabel146.Caption + ExportClass.Sep +
        QRLabel147.Caption + ExportClass.Sep + QRLabel157.Caption + ExportClass.Sep +
        QRLabel160.Caption + ExportClass.Sep + ExportList[1]);
      ExportClass.AddText(
        QRLabel8.Caption + ExportClass.Sep + QRLabel24.Caption + ExportClass.Sep +
        QRLabel28.Caption + ExportClass.Sep + QRLabel133.Caption + ExportClass.Sep +
        QRLabel136.Caption + ExportClass.Sep + QRLabel145.Caption + ExportClass.Sep +
        QRLabel148.Caption + ExportClass.Sep + QRLabel156.Caption + ExportClass.Sep +
        QRLabel161.Caption + ExportClass.Sep + ExportList[2]);
      ExportClass.AddText(
        QRLabel10.Caption + ExportClass.Sep + QRLabel23.Caption + ExportClass.Sep +
        QRLabel29.Caption + ExportClass.Sep + QRLabel132.Caption + ExportClass.Sep +
        QRLabel137.Caption + ExportClass.Sep + QRLabel144.Caption + ExportClass.Sep +
        QRLabel149.Caption + ExportClass.Sep + QRLabel158.Caption + ExportClass.Sep +
        QRLabel159.Caption + ExportClass.Sep + ExportList[3]);
      ExportClass.AddText(
        QRLabel9.Caption + ExportClass.Sep + QRLabel19.Caption + ExportClass.Sep +
        QRLabel30.Caption + ExportClass.Sep + QRLabel131.Caption + ExportClass.Sep +
        QRLabel138.Caption + ExportClass.Sep + QRLabel143.Caption + ExportClass.Sep +
        QRLabel150.Caption + ExportClass.Sep + QRLabel155.Caption + ExportClass.Sep +
        QRLabel162.Caption + ExportClass.Sep + ExportList[4]);
      ExportClass.AddText(
        QRLabel12.Caption + ExportClass.Sep + QRLabel17.Caption + ExportClass.Sep +
        QRLabel31.Caption + ExportClass.Sep + QRLabel136.Caption + ExportClass.Sep +
        QRLabel139.Caption + ExportClass.Sep + QRLabel142.Caption + ExportClass.Sep +
        QRLabel151.Caption + ExportClass.Sep + QRLabel154.Caption + ExportClass.Sep +
        QRLabel163.Caption + ExportClass.Sep + ExportList[5]);
      ExportClass.AddText(
        QRLabel14.Caption + ExportClass.Sep + QRLabel115.Caption + ExportClass.Sep +
        QRLabel32.Caption + ExportClass.Sep + QRLabel33.Caption + ExportClass.Sep +
        QRLabel140.Caption + ExportClass.Sep + QRLabel141.Caption + ExportClass.Sep +
        QRLabel152.Caption + ExportClass.Sep + QRLabel153.Caption + ExportClass.Sep +
        QRLabel164.Caption + ExportClass.Sep + ExportList[6]);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterDeptAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(
        QRLabel279.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('DEPARTMENT_CODE').AsString + ExportClass.Sep +
        ExportDescription);
      ExportClass.AddText(
        QRLabel280.Caption + ExportClass.Sep + QRLabel267.Caption + ExportClass.Sep +
        QRLabel171.Caption + ExportClass.Sep + QRLabel182.Caption + ExportClass.Sep +
        QRLabel183.Caption + ExportClass.Sep + QRLabel194.Caption + ExportClass.Sep +
        QRLabel195.Caption + ExportClass.Sep + QRLabel205.Caption + ExportClass.Sep +
        QRLabel208.Caption + ExportClass.Sep + ExportList[1]);
      ExportClass.AddText(
        QRLabel281.Caption + ExportClass.Sep + QRLabel272.Caption + ExportClass.Sep +
        QRLabel172.Caption + ExportClass.Sep + QRLabel181.Caption + ExportClass.Sep +
        QRLabel184.Caption + ExportClass.Sep + QRLabel193.Caption + ExportClass.Sep +
        QRLabel196.Caption + ExportClass.Sep + QRLabel204.Caption + ExportClass.Sep +
        QRLabel209.Caption + ExportClass.Sep + ExportList[2]);
      ExportClass.AddText(
        QRLabel282.Caption + ExportClass.Sep + QRLabel268.Caption + ExportClass.Sep +
        QRLabel173.Caption + ExportClass.Sep + QRLabel180.Caption + ExportClass.Sep +
        QRLabel185.Caption + ExportClass.Sep + QRLabel192.Caption + ExportClass.Sep +
        QRLabel197.Caption + ExportClass.Sep + QRLabel206.Caption + ExportClass.Sep +
        QRLabel207.Caption + ExportClass.Sep + ExportList[3]);
      ExportClass.AddText(
        QRLabel283.Caption + ExportClass.Sep + QRLabel271.Caption + ExportClass.Sep +
        QRLabel174.Caption + ExportClass.Sep + QRLabel179.Caption + ExportClass.Sep +
        QRLabel186.Caption + ExportClass.Sep + QRLabel191.Caption + ExportClass.Sep +
        QRLabel198.Caption + ExportClass.Sep + QRLabel203.Caption + ExportClass.Sep +
        QRLabel210.Caption + ExportClass.Sep + ExportList[4]);
      ExportClass.AddText(
        QRLabel284.Caption + ExportClass.Sep + QRLabel270.Caption + ExportClass.Sep +
        QRLabel175.Caption + ExportClass.Sep + QRLabel178.Caption + ExportClass.Sep +
        QRLabel187.Caption + ExportClass.Sep + QRLabel190.Caption + ExportClass.Sep +
        QRLabel199.Caption + ExportClass.Sep + QRLabel202.Caption + ExportClass.Sep +
        QRLabel211.Caption + ExportClass.Sep + ExportList[5]);
      ExportClass.AddText(
        QRLabel285.Caption + ExportClass.Sep + QRLabel269.Caption + ExportClass.Sep +
        QRLabel176.Caption + ExportClass.Sep + QRLabel177.Caption + ExportClass.Sep +
        QRLabel188.Caption + ExportClass.Sep + QRLabel189.Caption + ExportClass.Sep +
        QRLabel200.Caption + ExportClass.Sep + QRLabel201.Caption + ExportClass.Sep +
        QRLabel212.Caption + ExportClass.Sep + ExportList[6]);
    end;
  end;
end;

procedure TReportStaffAvailabilityQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffAvailabilityDM do
    begin
      ExportClass.AddText(
        QRLabel286.Caption + ExportClass.Sep +
        QueryEMA.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        ExportDescription);
      ExportClass.AddText(
        QRLabel287.Caption + ExportClass.Sep + QRLabel273.Caption + ExportClass.Sep +
        QRLabel219.Caption + ExportClass.Sep + QRLabel230.Caption + ExportClass.Sep +
        QRLabel231.Caption + ExportClass.Sep + QRLabel242.Caption + ExportClass.Sep +
        QRLabel243.Caption + ExportClass.Sep + QRLabel253.Caption + ExportClass.Sep +
        QRLabel256.Caption + ExportClass.Sep + ExportList[1]);
      ExportClass.AddText(
        QRLabel288.Caption + ExportClass.Sep + QRLabel278.Caption + ExportClass.Sep +
        QRLabel220.Caption + ExportClass.Sep + QRLabel229.Caption + ExportClass.Sep +
        QRLabel232.Caption + ExportClass.Sep + QRLabel241.Caption + ExportClass.Sep +
        QRLabel244.Caption + ExportClass.Sep + QRLabel252.Caption + ExportClass.Sep +
        QRLabel257.Caption + ExportClass.Sep + ExportList[2]);
      ExportClass.AddText(
        QRLabel289.Caption + ExportClass.Sep + QRLabel274.Caption + ExportClass.Sep +
        QRLabel221.Caption + ExportClass.Sep + QRLabel228.Caption + ExportClass.Sep +
        QRLabel233.Caption + ExportClass.Sep + QRLabel240.Caption + ExportClass.Sep +
        QRLabel245.Caption + ExportClass.Sep + QRLabel254.Caption + ExportClass.Sep +
        QRLabel255.Caption + ExportClass.Sep + ExportList[3]);
      ExportClass.AddText(
        QRLabel290.Caption + ExportClass.Sep + QRLabel277.Caption + ExportClass.Sep +
        QRLabel222.Caption + ExportClass.Sep + QRLabel227.Caption + ExportClass.Sep +
        QRLabel234.Caption + ExportClass.Sep + QRLabel239.Caption + ExportClass.Sep +
        QRLabel246.Caption + ExportClass.Sep + QRLabel251.Caption + ExportClass.Sep +
        QRLabel258.Caption + ExportClass.Sep + ExportList[4]);
      ExportClass.AddText(
        QRLabel291.Caption + ExportClass.Sep + QRLabel276.Caption + ExportClass.Sep +
        QRLabel223.Caption + ExportClass.Sep + QRLabel226.Caption + ExportClass.Sep +
        QRLabel235.Caption + ExportClass.Sep + QRLabel238.Caption + ExportClass.Sep +
        QRLabel247.Caption + ExportClass.Sep + QRLabel250.Caption + ExportClass.Sep +
        QRLabel259.Caption + ExportClass.Sep + ExportList[5]);
      ExportClass.AddText(
        QRLabel292.Caption + ExportClass.Sep + QRLabel275.Caption + ExportClass.Sep +
        QRLabel224.Caption + ExportClass.Sep + QRLabel225.Caption + ExportClass.Sep +
        QRLabel236.Caption + ExportClass.Sep + QRLabel237.Caption + ExportClass.Sep +
        QRLabel248.Caption + ExportClass.Sep + QRLabel249.Caption + ExportClass.Sep +
        QRLabel260.Caption + ExportClass.Sep + ExportList[6]);
    end;
  end;
end;

end.
