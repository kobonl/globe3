inherited DialogReportCheckListScanF: TDialogReportCheckListScanF
  Left = 259
  Top = 109
  Caption = 'Checklist Scans'
  ClientHeight = 416
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 0
  end
  inherited pnlInsertBase: TPanel
    Height = 295
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 13
    end
    inherited LblPlant: TLabel
      Top = 13
    end
    inherited LblFromEmployee: TLabel
      Top = 37
    end
    inherited LblEmployee: TLabel
      Top = 36
    end
    inherited LblToPlant: TLabel
      Top = 12
    end
    inherited LblToEmployee: TLabel
      Top = 36
    end
    object Label1: TLabel [6]
      Left = 40
      Top = 397
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [7]
      Left = 8
      Top = 133
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [8]
      Left = 40
      Top = 133
      Width = 48
      Height = 13
      Caption = 'Shift Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [9]
      Left = 315
      Top = 133
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 40
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 40
    end
    object Label5: TLabel [12]
      Left = 128
      Top = 91
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [13]
      Left = 344
      Top = 91
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [14]
      Left = 8
      Top = 397
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label8: TLabel [15]
      Left = 315
      Top = 397
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    inherited LblFromDepartment: TLabel
      Top = 84
      Visible = False
    end
    inherited LblDepartment: TLabel
      Top = 84
      Visible = False
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 89
    end
    inherited LblStarDepartmentTo: TLabel
      Top = 89
    end
    inherited LblToDepartment: TLabel
      Top = 85
      Visible = False
    end
    inherited LblStarTeamTo: TLabel
      Top = 404
      Visible = False
    end
    inherited LblToTeam: TLabel
      Top = 372
      Visible = False
    end
    inherited LblStarTeamFrom: TLabel
      Top = 404
      Visible = False
    end
    inherited LblTeam: TLabel
      Top = 370
      Visible = False
    end
    inherited LblFromTeam: TLabel
      Top = 370
      Visible = False
    end
    inherited LblFromShift: TLabel
      Left = 0
      Top = 342
    end
    inherited LblShift: TLabel
      Left = 32
      Top = 342
    end
    inherited LblStartShiftFrom: TLabel
      Left = 120
      Top = 342
    end
    inherited LblToShift: TLabel
      Left = 307
      Top = 342
    end
    inherited LblStarShiftTo: TLabel
      Left = 336
      Top = 342
    end
    inherited LblFromPlant2: TLabel
      Top = 61
    end
    inherited LblPlant2: TLabel
      Top = 61
    end
    inherited LblStarPlant2From: TLabel
      Top = 63
    end
    inherited LblToPlant2: TLabel
      Top = 61
    end
    inherited LblStarPlant2To: TLabel
      Top = 63
    end
    inherited LblFromWorkspot: TLabel
      Top = 110
    end
    inherited LblWorkspot: TLabel
      Top = 110
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 112
    end
    inherited LblToWorkspot: TLabel
      Top = 110
    end
    inherited LblStarWorkspotTo: TLabel
      Top = 112
    end
    object BevelPlantDeptWS: TBevel [43]
      Left = 5
      Top = 56
      Width = 513
      Height = 74
      Style = bsRaised
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 560
      Top = 437
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      Top = 10
      ColCount = 128
      Visible = False
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      Top = 10
      ColCount = 129
      Visible = False
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Left = 128
      Top = 369
      ColCount = 145
      TabOrder = 15
      Visible = False
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 369
      ColCount = 146
      TabOrder = 16
      Visible = False
    end
    object ComboBoxPlusWorkSpotFrom: TComboBoxPlus [58]
      Left = 136
      Top = 396
      Width = 180
      Height = 19
      ColCount = 122
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 28
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus [59]
      Left = 358
      Top = 396
      Width = 180
      Height = 19
      ColCount = 123
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 32
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object DateFrom: TDateTimePicker [60]
      Left = 120
      Top = 132
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 12
      OnChange = DateToChange
    end
    object DateTo: TDateTimePicker [61]
      Left = 334
      Top = 133
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
      OnChange = DateToChange
    end
    object GroupBox1: TGroupBox [62]
      Left = 152
      Top = 179
      Width = 361
      Height = 113
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 33
      object CheckBoxShowTotal: TCheckBox
        Left = 8
        Top = 16
        Width = 209
        Height = 17
        Caption = 'Show total hours per employee'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 64
        Width = 169
        Height = 17
        Caption = 'Show selections'
        TabOrder = 2
        OnClick = DateToChange
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 88
        Width = 177
        Height = 17
        Caption = 'Export'
        TabOrder = 3
      end
      object CheckBoxShowAvail: TCheckBox
        Left = 8
        Top = 40
        Width = 161
        Height = 17
        Caption = 'Show availability'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object CheckBoxShowDetails: TCheckBox
        Left = 216
        Top = 16
        Width = 137
        Height = 17
        Caption = 'Show details'
        TabOrder = 4
      end
      object CheckBoxOnlySANAEmps: TCheckBox
        Left = 216
        Top = 40
        Width = 141
        Height = 17
        Caption = 'Only SANA employees'
        TabOrder = 5
      end
    end
    object CheckBoxAllDate: TCheckBox [63]
      Left = 539
      Top = 495
      Width = 41
      Height = 17
      Caption = 'All'
      Checked = True
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 34
      Visible = False
      OnClick = CheckBoxAllDateClick
    end
    object RadioGroupProcessedYN: TRadioGroup [64]
      Left = 8
      Top = 179
      Width = 137
      Height = 113
      Caption = 'Processed'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Yes'
        'No'
        'Both')
      ParentFont = False
      TabOrder = 17
    end
    object CheckBoxIncludeScansOtherPlants: TCheckBox [65]
      Left = 8
      Top = 157
      Width = 505
      Height = 17
      Caption = 'Include scans made in other plants'
      TabOrder = 14
      OnClick = CheckBoxIncludeScansOtherPlantsClick
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 372
      TabOrder = 30
      Visible = False
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 83
      ColCount = 144
      TabOrder = 7
      Visible = False
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 83
      ColCount = 145
      TabOrder = 8
      Visible = False
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 523
      Top = 86
      TabOrder = 9
      Visible = False
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 36
      TabOrder = 3
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 34
      TabOrder = 4
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Left = 128
      Top = 342
      TabOrder = 25
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Left = 350
      Top = 342
      TabOrder = 29
    end
    inherited CheckBoxAllShifts: TCheckBox
      Left = 522
      Top = 342
      TabOrder = 31
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 256
      TabOrder = 26
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 531
      Top = 476
      TabOrder = 27
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 523
      Top = 13
      TabOrder = 2
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Top = 59
      TabOrder = 5
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 334
      Top = 59
      TabOrder = 6
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 108
      TabOrder = 10
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 108
      TabOrder = 11
    end
    inherited EditWorkspots: TEdit
      Left = 144
      Top = 438
      TabOrder = 18
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      TabOrder = 20
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      TabOrder = 19
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 22
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 24
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 21
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 23
    end
  end
  inherited stbarBase: TStatusBar
    Top = 356
  end
  inherited pnlBottom: TPanel
    Top = 375
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 560
    Top = 96
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 8
  end
  inherited QueryEmplFrom: TQuery
    Left = 184
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 296
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4D040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072D4469616C6F675265706F7274436865636B4C6973745363
        616E462E44617461536F75726365456D706C46726F6D104F7074696F6E734375
        73746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E64
        53697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C75
        6D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E73
        44420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C65
        74650D6564676F43616E496E73657274116564676F43616E4E61766967617469
        6F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C
        5265636F726473106564676F557365426F6F6B6D61726B7300000F5464784442
        47726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06
        064E756D62657206536F7274656407046373557005576964746802410942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F45
        4D504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E
        0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F727420
        6E616D6505576964746802540942616E64496E646578020008526F77496E6465
        780200094669656C644E616D65060A53484F52545F4E414D4500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E0604
        4E616D6505576964746803B4000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060B4445534352495054494F4E00000F5464
        78444247726964436F6C756D6E0D436F6C756D6E416464726573730743617074
        696F6E06074164647265737305576964746802450942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D650607414444524553530000
        0F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F646507
        43617074696F6E060F4465706172746D656E7420636F64650557696474680258
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F4445504152544D454E545F434F444500000F546478444247726964436F
        6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F
        64650942616E64496E646578020008526F77496E6465780200094669656C644E
        616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        14040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072B4469616C6F675265706F72
        74436865636B4C6973745363616E462E44617461536F75726365456D706C546F
        104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E
        670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E
        67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E
        6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D65
        64676F43616E44656C6574650D6564676F43616E496E73657274116564676F43
        616E4E617669676174696F6E116564676F436F6E6669726D44656C6574651265
        64676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D6172
        6B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C07
        43617074696F6E06064E756D62657206536F7274656407046373557005576964
        746802310942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F454D504C4F5945455F4E554D42455200000F54647844424772
        6964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E
        060A53686F7274206E616D65055769647468024E0942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D
        4500000F546478444247726964436F6C756D6E11436F6C756D6E446573637269
        7074696F6E0743617074696F6E06044E616D6505576964746803CC000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060B44
        45534352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C
        756D6E416464726573730743617074696F6E0607416464726573730557696474
        6802650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506074144445245535300000F546478444247726964436F6C756D6E0E
        436F6C756D6E44657074436F64650743617074696F6E060F4465706172746D65
        6E7420636F64650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F4445504152544D454E545F434F444500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609
        5465616D20636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited qryPivotWorkspot: TQuery
    Left = 576
    Top = 181
  end
  object QueryWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      ' W.WORKSPOT_CODE, W.DESCRIPTION, W.PLANT_CODE'
      'FROM '
      ' WORKSPOT W'
      'LEFT JOIN DEPARTMENT D ON'
      ' W.PLANT_CODE = D.PLANT_CODE AND'
      ' W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      'LEFT JOIN DEPARTMENTPERTEAM T ON'
      ' D.PLANT_CODE = T.PLANT_CODE AND'
      ' D.DEPARTMENT_CODE = T.DEPARTMENT_CODE '
      'WHERE'
      ' W.PLANT_CODE  = :PLANT_CODE AND'
      ' ('
      '   (:USER_NAME = '#39'*'#39') OR'
      '   (T.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME)'
      '   )'
      ' )'
      'ORDER BY'
      ' W.WORKSPOT_CODE')
    Left = 56
    Top = 23
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION'
      'FROM '
      '  EMPLOYEE E'
      'ORDER BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 120
    Top = 23
  end
end
