inherited RealTimeEffMonitorDM: TRealTimeEffMonitorDM
  OldCreateOrder = True
  Left = 364
  Top = 292
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    TableName = 'PLANT'
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
  end
  inherited TableDetail: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
  end
  object dsWorkspotEffPerMin: TDataSource
    DataSet = qryWorkspotEffPerMin
    Left = 208
    Top = 208
  end
  object qryWorkspotEffPerMin: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.WORKSPOTEFFPERMINUTE_ID, T.SHIFT_DATE, T.SHIFT_NUMBER, '
      '  T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,'
      '  T.INTERVALSTARTTIME, T.INTERVALENDTIME, '
      
        '  T.WORKSPOTSECONDSACTUAL, T.WORKSPOTSECONDSNORM, T.WORKSPOTQUAN' +
        'TITY,'
      '  T.NORMLEVEL, T.START_TIMESTAMP, T.END_TIMESTAMP'
      'FROM WORKSPOTEFFPERMINUTE T'
      'WHERE '
      '  T.PLANT_CODE = :PLANT_CODE AND'
      '  T.SHIFT_DATE = :SHIFT_DATE'
      'ORDER BY T.WORKSPOTEFFPERMINUTE_ID DESC'
      ''
      ' ')
    Left = 96
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end>
    object qryWorkspotEffPerMinWORKSPOTEFFPERMINUTE_ID: TFloatField
      FieldName = 'WORKSPOTEFFPERMINUTE_ID'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.WORKSPOTEFFPERMINUTE_ID'
    end
    object qryWorkspotEffPerMinSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.SHIFT_DATE'
    end
    object qryWorkspotEffPerMinSHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.SHIFT_NUMBER'
    end
    object qryWorkspotEffPerMinPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.PLANT_CODE'
      Size = 24
    end
    object qryWorkspotEffPerMinWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.WORKSPOT_CODE'
      Size = 24
    end
    object qryWorkspotEffPerMinJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.JOB_CODE'
      Size = 24
    end
    object qryWorkspotEffPerMinINTERVALSTARTTIME: TDateTimeField
      FieldName = 'INTERVALSTARTTIME'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.INTERVALSTARTTIME'
    end
    object qryWorkspotEffPerMinINTERVALENDTIME: TDateTimeField
      FieldName = 'INTERVALENDTIME'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.INTERVALENDTIME'
    end
    object qryWorkspotEffPerMinWORKSPOTSECONDSACTUAL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'WORKSPOTSECONDSACTUAL'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.WORKSPOTSECONDSACTUAL'
    end
    object qryWorkspotEffPerMinWORKSPOTSECONDSNORM: TFloatField
      Alignment = taLeftJustify
      FieldName = 'WORKSPOTSECONDSNORM'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.WORKSPOTSECONDSNORM'
    end
    object qryWorkspotEffPerMinWORKSPOTQUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'WORKSPOTQUANTITY'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.WORKSPOTQUANTITY'
    end
    object qryWorkspotEffPerMinNORMLEVEL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'NORMLEVEL'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.NORMLEVEL'
    end
    object qryWorkspotEffPerMinSTART_TIMESTAMP: TDateTimeField
      FieldName = 'START_TIMESTAMP'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.START_TIMESTAMP'
    end
    object qryWorkspotEffPerMinEND_TIMESTAMP: TDateTimeField
      FieldName = 'END_TIMESTAMP'
      Origin = 'PIMS.WORKSPOTEFFPERMINUTE.END_TIMESTAMP'
    end
  end
  object qryEmployeeEffPerMin: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.EMPLOYEEEFFPERMINUTE_ID, T.SHIFT_DATE, T.SHIFT_NUMBER,'
      '  T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,'
      '  T.EMPLOYEE_NUMBER,'
      '  T.INTERVALSTARTTIME, T.INTERVALENDTIME,'
      
        '  T.EMPLOYEESECONDSACTUAL, T.EMPLOYEESECONDSNORM, T.EMPLOYEEQUAN' +
        'TITY,'
      '  T.NORMLEVEL'
      'FROM EMPLOYEEEFFPERMINUTE T'
      'WHERE '
      '  T.PLANT_CODE = :PLANT_CODE AND'
      '  T.SHIFT_DATE = :SHIFT_DATE'
      'ORDER BY T.EMPLOYEEEFFPERMINUTE_ID DESC'
      ''
      ' '
      ' ')
    Left = 96
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end>
    object qryEmployeeEffPerMinEMPLOYEEEFFPERMINUTE_ID: TFloatField
      FieldName = 'EMPLOYEEEFFPERMINUTE_ID'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.EMPLOYEEEFFPERMINUTE_ID'
    end
    object qryEmployeeEffPerMinSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.SHIFT_DATE'
    end
    object qryEmployeeEffPerMinSHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.SHIFT_NUMBER'
    end
    object qryEmployeeEffPerMinPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.PLANT_CODE'
      Size = 24
    end
    object qryEmployeeEffPerMinWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.WORKSPOT_CODE'
      Size = 24
    end
    object qryEmployeeEffPerMinJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.JOB_CODE'
      Size = 24
    end
    object qryEmployeeEffPerMinEMPLOYEE_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.EMPLOYEE_NUMBER'
    end
    object qryEmployeeEffPerMinINTERVALSTARTTIME: TDateTimeField
      FieldName = 'INTERVALSTARTTIME'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.INTERVALSTARTTIME'
    end
    object qryEmployeeEffPerMinINTERVALENDTIME: TDateTimeField
      FieldName = 'INTERVALENDTIME'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.INTERVALENDTIME'
    end
    object qryEmployeeEffPerMinEMPLOYEESECONDSACTUAL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEESECONDSACTUAL'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.EMPLOYEESECONDSACTUAL'
    end
    object qryEmployeeEffPerMinEMPLOYEESECONDSNORM: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEESECONDSNORM'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.EMPLOYEESECONDSNORM'
    end
    object qryEmployeeEffPerMinEMPLOYEEQUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEEQUANTITY'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.EMPLOYEEQUANTITY'
    end
    object qryEmployeeEffPerMinNORMLEVEL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'NORMLEVEL'
      Origin = 'PIMS.EMPLOYEEEFFPERMINUTE.NORMLEVEL'
    end
  end
  object dsEmployeeEffPerMin: TDataSource
    DataSet = qryEmployeeEffPerMin
    Left = 208
    Top = 264
  end
  object qryEmployeeEffPerShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  T.EMPLOYEEEFFPERSHIFT_ID, T.SHIFT_DATE, T.SHIFT_NUMBER, '
      '  T.PLANT_CODE, T.WORKSPOT_CODE, T.JOB_CODE,'
      '  T.EMPLOYEE_NUMBER,'
      
        '  T.EMPLOYEESECONDSACTUAL, T.EMPLOYEESECONDSNORM, T.EMPLOYEEQUAN' +
        'TITY,'
      '  T.NORMLEVEL'
      'FROM EMPLOYEEEFFPERSHIFT T'
      'WHERE '
      '  T.PLANT_CODE = :PLANT_CODE AND'
      '  T.SHIFT_DATE = :SHIFT_DATE'
      'ORDER BY T.EMPLOYEEEFFPERSHIFT_ID DESC')
    Left = 96
    Top = 320
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end>
    object qryEmployeeEffPerShiftEMPLOYEEEFFPERSHIFT_ID: TFloatField
      FieldName = 'EMPLOYEEEFFPERSHIFT_ID'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.EMPLOYEEEFFPERSHIFT_ID'
    end
    object qryEmployeeEffPerShiftSHIFT_DATE: TDateTimeField
      FieldName = 'SHIFT_DATE'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.SHIFT_DATE'
    end
    object qryEmployeeEffPerShiftSHIFT_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.SHIFT_NUMBER'
    end
    object qryEmployeeEffPerShiftPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.PLANT_CODE'
      Size = 24
    end
    object qryEmployeeEffPerShiftWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.WORKSPOT_CODE'
      Size = 24
    end
    object qryEmployeeEffPerShiftJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.JOB_CODE'
      Size = 24
    end
    object qryEmployeeEffPerShiftEMPLOYEE_NUMBER: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.EMPLOYEE_NUMBER'
    end
    object qryEmployeeEffPerShiftEMPLOYEESECONDSACTUAL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEESECONDSACTUAL'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.EMPLOYEESECONDSACTUAL'
    end
    object qryEmployeeEffPerShiftEMPLOYEESECONDSNORM: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEESECONDSNORM'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.EMPLOYEESECONDSNORM'
    end
    object qryEmployeeEffPerShiftEMPLOYEEQUANTITY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEEQUANTITY'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.EMPLOYEEQUANTITY'
    end
    object qryEmployeeEffPerShiftNORMLEVEL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'NORMLEVEL'
      Origin = 'PIMS.EMPLOYEEEFFPERSHIFT.NORMLEVEL'
    end
  end
  object dsEmployeeEffPerShift: TDataSource
    DataSet = qryEmployeeEffPerShift
    Left = 208
    Top = 320
  end
end
