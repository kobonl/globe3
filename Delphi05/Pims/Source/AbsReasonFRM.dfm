inherited AbsReasonF: TAbsReasonF
  Left = 268
  Top = 178
  Height = 451
  Caption = 'Absence reasons'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    Visible = False
    inherited dxMasterGrid: TdxDBGrid
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 284
    Height = 128
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object Label3: TLabel
      Left = 8
      Top = 40
      Width = 53
      Height = 13
      Caption = 'Description'
    end
    object Label2: TLabel
      Left = 336
      Top = 69
      Width = 47
      Height = 13
      Caption = 'HourType'
    end
    object Label4: TLabel
      Left = 8
      Top = 69
      Width = 68
      Height = 13
      Caption = 'Absence Type'
    end
    object lblExportCode: TLabel
      Left = 8
      Top = 96
      Width = 60
      Height = 13
      Caption = 'Export Code'
    end
    object DBEditCode: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 8
      Width = 121
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'ABSENCEREASON_CODE'
      DataSource = AbsReasonDM.DataSourceDetail
    end
    object DBCheckBoxPayed: TDBCheckBox
      Tag = 1
      Left = 400
      Top = 9
      Width = 225
      Height = 17
      Caption = 'Paid'
      DataField = 'PAYED_YN'
      DataSource = AbsReasonDM.DataSourceDetail
      TabOrder = 3
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBEditDescription: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 37
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'DESCRIPTION'
      DataSource = AbsReasonDM.DataSourceDetail
    end
    object dxDBLookupEditAbs: TdxDBLookupEdit
      Tag = 1
      Left = 96
      Top = 67
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'ABSENCETYPELU'
      DataSource = AbsReasonDM.DataSourceDetail
      DropDownRows = 4
      ListFieldName = 'DESCRIPTION;ABSENCETYPE_CODE'
    end
    object dxDBLookupEditHour: TdxDBLookupEdit
      Tag = 1
      Left = 400
      Top = 67
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 5
      DataField = 'HOURTYPELU'
      DataSource = AbsReasonDM.DataSourceDetail
      DropDownRows = 4
      ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
    end
    object DBCheckBoxOverruleWithIllness: TDBCheckBox
      Left = 400
      Top = 28
      Width = 225
      Height = 17
      Caption = 'Overrule with illness'
      DataField = 'OVERRULE_WITH_ILLNESS_YN'
      DataSource = AbsReasonDM.DataSourceDetail
      TabOrder = 4
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBCheckBoxCorrectWorkHrs: TDBCheckBox
      Left = 400
      Top = 47
      Width = 241
      Height = 17
      Caption = 'Correct with worked hours on same day'
      DataField = 'CORRECT_WORK_HRS_YN'
      DataSource = AbsReasonDM.DataSourceDetail
      TabOrder = 6
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object dxDBEditExportCode: TdxDBEdit
      Left = 96
      Top = 96
      Width = 121
      Style.BorderStyle = xbsSingle
      TabOrder = 7
      DataField = 'EXPORT_CODE'
      DataSource = AbsReasonDM.DataSourceDetail
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 129
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 125
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 124
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Absence reasons'
        end>
      KeyField = 'ABSENCEREASON_ID'
      DataSource = AbsReasonDM.DataSourceDetail
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 88
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCEREASON_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 288
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnAbsenceType: TdxDBGridLookupColumn
        Caption = 'Absence type'
        Width = 154
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCETYPELU'
        ListFieldName = 'DESCRIPTION;ABSENCETYPE_CODE'
      end
      object dxDetailGridColumnHourType: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 134
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxDetailGridColumnPayed: TdxDBGridCheckColumn
        Caption = 'Paid'
        Width = 58
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PAYED_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnOverruleWithIllness: TdxDBGridCheckColumn
        Caption = 'Overrule with illness'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OVERRULE_WITH_ILLNESS_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnCorrectWorkHrs: TdxDBGridCheckColumn
        Caption = 'Correct work hrs'
        MinWidth = 20
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CORRECT_WORK_HRS_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnExportCode: TdxDBGridColumn
        Caption = 'Export code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
