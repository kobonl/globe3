(*
  MRA:12-JAN-2010. RV050.8. 889955.
  - Added Plant-level for teams.
  - Restrict plants using teamsperuser.
  MRA:RV086.2. SC-50014392.
  - Plan in other plants, related issues:
    - When teams-per-users-filtering is used, you still see
      plants/teams/department/workspots not belonging to the
      teams-per-user-selection.
*)
unit TeamDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTeamDM = class(TGridBaseDM)
    TableDetailTEAM_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailRESP_EMPLOYEE_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableEmployee: TTable;
    TableEmployeeEMPLOYEE_NUMBER: TIntegerField;
    TableEmployeeSHORT_NAME: TStringField;
    TableEmployeePLANT_CODE: TStringField;
    TableEmployeeDESCRIPTION: TStringField;
    TableEmployeeDEPARTMENT_CODE: TStringField;
    TableEmployeeCREATIONDATE: TDateTimeField;
    TableEmployeeMUTATIONDATE: TDateTimeField;
    TableEmployeeMUTATOR: TStringField;
    TableEmployeeADDRESS: TStringField;
    TableEmployeeZIPCODE: TStringField;
    TableEmployeeCITY: TStringField;
    TableEmployeeSTATE: TStringField;
    TableEmployeeLANGUAGE_CODE: TStringField;
    TableEmployeePHONE_NUMBER: TStringField;
    TableEmployeeEMAIL_ADDRESS: TStringField;
    TableEmployeeDATE_OF_BIRTH: TDateTimeField;
    TableEmployeeSEX: TStringField;
    TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField;
    TableEmployeeSTARTDATE: TDateTimeField;
    TableEmployeeTEAM_CODE: TStringField;
    TableEmployeeENDDATE: TDateTimeField;
    TableEmployeeCONTRACTGROUP_CODE: TStringField;
    TableEmployeeIS_SCANNING_YN: TStringField;
    TableEmployeeCUT_OF_TIME_YN: TStringField;
    TableEmployeeREMARK: TStringField;
    TableEmployeeCALC_BONUS_YN: TStringField;
    TableEmployeeFIRSTAID_YN: TStringField;
    TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField;
    TableDetailEMPLOYEELU: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterPHONE: TStringField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableMasterFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TeamDM: TTeamDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TTeamDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // RV050.8. No need to fill this field (null allowed).
{
  if not TableEmployee.IsEmpty then
  begin
    TableEmployee.First;
    TableDetail.FieldByName('RESP_EMPLOYEE_NUMBER').Value :=
      TableEmployee.FieldByName('EMPLOYEE_NUMBER').Value;
  end;
}
end;

procedure TTeamDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TableMaster);
  TableMaster.Open;
  // RV086.2.
  SystemDM.PlantTeamFilterEnable(TableDetail);
end;

procedure TTeamDM.TableMasterFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TTeamDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.2.
  Accept := SystemDM.PlantTeamTeamFilter(DataSet);
end;

end.
