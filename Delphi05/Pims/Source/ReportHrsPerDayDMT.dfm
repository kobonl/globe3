inherited ReportHrsPerDayDM: TReportHrsPerDayDM
  OldCreateOrder = True
  Left = 191
  Top = 161
  Height = 479
  Width = 835
  inherited qryWorkspotMinMax: TQuery
    Left = 528
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 528
  end
  object QueryAbsence: TQuery
    AutoCalcFields = False
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryAbsenceFilterRecord
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE,'
      '  P.DESCRIPTION AS PDESC,'
      '  E.TEAM_CODE,'
      '  T.DESCRIPTION AS TDESC,'
      '  D.DEPARTMENT_CODE,'
      '  D.DESCRIPTION AS DDESC,'
      '  CAST(A.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION AS EDESC,'
      '  CAST(A.ABSENCEREASON_ID AS NUMBER) AS ABSENCEREASON_ID,'
      '  R.ABSENCEREASON_CODE,'
      '  R.DESCRIPTION AS ABSDESC,'
      '  CAST(W.WEEKNUMBER AS NUMBER) AS WEEKNUMBER,'
      '  A.ABSENCEHOUR_DATE AS REPORT_DATE,'
      '  SUM(ABSENCE_MINUTE) AS SUMMIN,'
      '  MAX(A.PLANT_CODE) AS BUCODE,'
      '  MAX(A.PLANT_CODE) AS WORKSPOT_CODE,'
      '  MAX(0) AS LISTCODE'
      'FROM   ABSENCEHOURPEREMPLOYEE A, PLANT P, ABSENCEREASON R'
      ' , DEPARTMENT D, EMPLOYEE E, TEAM T, WEEK W'
      ' WHERE'
      '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  AND P.PLANT_CODE = A.PLANT_CODE'
      '  AND E.PLANT_CODE = D.PLANT_CODE AND'
      '  E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  AND E.TEAM_CODE = T.TEAM_CODE'
      ' AND   A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  A.ABSENCEHOUR_DATE >= :FSTARTDATE AND'
      '  A.ABSENCEHOUR_DATE <= :FENDDATE'
      '  AND A.ABSENCEHOUR_DATE >= W.DATEFROM'
      '  AND A.ABSENCEHOUR_DATE <= W.DATETO'
      '  AND A.PLANT_CODE >= '#39'80'#39
      '  AND A.PLANT_CODE <= '#39'81'#39
      '  AND ( (E.TEAM_CODE >= '#39'A'#39
      '  AND E.TEAM_CODE <= '#39'E'#39')'
      ')  AND R.ABSENCEREASON_CODE >= '#39'B'#39
      '  AND R.ABSENCEREASON_CODE <= '#39'V'#39
      ' GROUP BY'
      '  A.PLANT_CODE, P.DESCRIPTION,'
      '  E.TEAM_CODE, T.DESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION,'
      '  A.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  A.ABSENCEREASON_ID, R.ABSENCEREASON_CODE,'
      '  R.DESCRIPTION, W.WEEKNUMBER, A.ABSENCEHOUR_DATE'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 48
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
    object QueryAbsencePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryAbsencePDESC: TStringField
      FieldName = 'PDESC'
      Size = 30
    end
    object QueryAbsenceTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object QueryAbsenceTDESC: TStringField
      FieldName = 'TDESC'
      Size = 30
    end
    object QueryAbsenceDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryAbsenceDDESC: TStringField
      FieldName = 'DDESC'
      Size = 30
    end
    object QueryAbsenceEDESC: TStringField
      FieldName = 'EDESC'
      Size = 40
    end
    object QueryAbsenceABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Size = 1
    end
    object QueryAbsenceABSDESC: TStringField
      FieldName = 'ABSDESC'
      Size = 30
    end
    object QueryAbsenceREPORT_DATE: TDateTimeField
      FieldName = 'REPORT_DATE'
    end
    object QueryAbsenceSUMMIN: TFloatField
      FieldName = 'SUMMIN'
    end
    object QueryAbsenceBUCODE: TStringField
      FieldName = 'BUCODE'
      Size = 6
    end
    object QueryAbsenceWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryAbsenceLISTCODE: TFloatField
      FieldName = 'LISTCODE'
    end
    object QueryAbsenceABSENCEREASON_ID: TFloatField
      FieldName = 'ABSENCEREASON_ID'
    end
    object QueryAbsenceWEEKNUMBER: TFloatField
      FieldName = 'WEEKNUMBER'
    end
    object QueryAbsenceEMPLOYEE_NUMBER: TFloatField
      FieldName = 'EMPLOYEE_NUMBER'
    end
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryAbsence
    Left = 152
    Top = 16
  end
  object QueryReportHrs: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      '')
    Left = 48
    Top = 96
  end
  object QueryBUPerWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE, '
      '  SUM(PERCENTAGE) AS SUMPERC'
      'FROM '
      '  BUSINESSUNITPERWORKSPOT'
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE')
    Left = 48
    Top = 264
  end
  object dspQueryBUWK: TDataSetProvider
    DataSet = QueryBUPerWK
    Constraints = True
    Left = 136
    Top = 264
  end
  object cdsQueryBUWK: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspQueryBUWK'
    Left = 224
    Top = 264
  end
end
