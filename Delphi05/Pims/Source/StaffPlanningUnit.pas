(*
  MRA:31-AUG-2010 RV067.MRA.20 Bugfix.
  - Changed some code to compare with characters
    'A', 'B' and 'C', instead of using ord().
  MRA:25-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  MRA:25-JUL-2018 GLOB3-60
  - Replace LightGreen with Blue.
  - It still is called LightGreen here, but we replace it with Blue via
    StaffPlanningUnit.
  - The color is not used! It uses bitmaps (imgListBase).
*)
unit StaffPlanningUnit;

interface

Uses SystemDMT, SysUtils, Dialogs, dbTables, Classes, DB, Graphics, UPimsConst,
  dxDBGrid, UPimsMessageRes;

Const

  //EMPLOYEESHOW =  27;
  // constant colors
  DarkGrey = clDkGray;
  Normal = clScrollBar;
  Invisible = clWhite;
  LightGreen = clScanPToAStock; // GLOB3-60 This color is not really used! It uses bitmaps (imgListBase)!
  Green = clLime;
  DarkRed = clRed;
  Red = clScanError;
  LightRed = clScanOut;
  Red_Levels  = clGridEdit;


  Yellow = clYellow;
  CHR_SEP = Chr(Ord(254));

  MAX_NR_BMP = 25;
  MAXColumns = 60;

Type

  TTBColor = Array[1..MAX_TBS] of Integer;
  TTBLevel = Array[1..MAX_TBS] of String;
  TPopupItemClick = procedure (Sender:TObject) of object;

var
  //car 550278
  EMPLOYEESHOW: Integer;
  FDescriptionWidth: Integer;
// number of workspots selected
  FWKCount : Integer;
// prev and current values for the selections
  FPlant,
  FTeamFrom,
  FTeamTo: String;
  FShift, FDay, FShowColumn,
  FWKDEPTSort, FEMPSort, FSelection: Integer;

  FShowOnlyAvailEmpl,
  FShowWKWithOcc: Boolean;
  FDate: TDateTime;
  FAllTeam: Boolean;
  //

// variable used for replacing the now functions in more units
  FCurrentDate: TDateTime;

  FWKList, FDeptList: TStringList;
  FWKDescList: TStringList;
  FAbsList,
  FABSDESCList,
  FListSaveEmp,
  FListABSRSN, FListOCIUpdate, FListOCIPlanned: TStringList;
 //
  FOverplanning: Boolean;
  FSaveChanges: Boolean;

function Min(X, Y: Integer): Integer;
function Max(X, Y: Integer): Integer;

procedure ExecuteSelectSQL(TmpQuery: TQuery; SelectStr: String);
procedure SetSelectSQL(TmpQuery: TQuery; SelectStr: String; Active: Boolean);
procedure ExecuteSql(QueryWork: TQuery; Selectstr: String);

procedure SetParameters(Plant, TeamFrom, TeamTo: String;
  Shift, Day, WKSort, EMPSort, Selection: Integer; AllTeam: Boolean;
  DateSelection: TDateTime; ShowOnlyAvailEmpl, ShowWKWithOcc: Boolean);


procedure GetEmpColor(OCI, Planned, ColorLevel: Integer; var ColorX: Integer);
function RedColor(Level: String): Integer;
procedure GiveMessageOverplanning;
procedure UpdateAbsList(Pressed: Boolean; Index, Employee: Integer;
  Absence: String);
function GetTableBTN(Color: Integer; Pressed, Level: String): String;
procedure GetAtributeBTN(BtnStr: String; var Color: Integer;
  var Pressed, Level: String);
//procedure GetColorLevel(i: Integer; var Level: String; var ColorLevel: Integer);

// grid forms
procedure CreateBand(dxGridTMP: TdxDBGrid; IndexBand, Width: Integer;
  CaptionStr: String);
procedure CreateColumnOfBand(dxGridTMP: TdxDBGrid;
  IndexCol, IndexBand, Width: Integer; CaptionStr, FieldByName: String;
  TagCol: Integer);

function GetLevel(i: Integer): String;
function IsRed(I: Integer): Boolean;
function GetFieldsPivot(ColumnName: String; TmpTable: TTable;  ColIndex:Integer;
  var ValColumn: String; var Employee: Integer): Boolean;

function WKEMPL_GetFieldsPivot(ColumnName: String; TmpTable: TTable;
  var Employee, IndexWK: Integer): Boolean;

function GetPlannedWKFromPivot(Index: Integer; TmpTable: TTable): String;
{function IsAllowedPlanning(ValStr, ColumnStr: String;
  TmpTable:TTable): Boolean;  }
function FillLeadSpace(Str: String; LenghStr: Integer): String;

//
function IsPressed(ValStr:String): Boolean;
function getWKCode(ValStr: String; IndexList: Integer): String;
procedure GetWKDeptCode(ValStr: String; RecCount: Integer;var WK, DEPT: STRING);
//

function GetDescWK(StrWK: String): String;
function GetLongDescWK(StrWK: String): String;

function EmployeesAcrossPlants: Integer;

function Int2Hex(AColIndex: Integer): String;
function Hex2Int(AHex: String): Integer;

implementation

function GetWKCode(ValStr: String; IndexList: Integer): String;
begin
  Result := Valstr;
  if (ValStr = DummyStr) then
    Result := FDeptList.Strings[IndexList];
end;

procedure GetWKDeptCode(ValStr: String; RecCount: Integer; var WK, DEPT: String);
begin
  WK := ValStr; Dept := FDeptList.Strings[RecCount];
  if Dept = DummyStr then
  begin
    Dept := WK;
    WK := DummyStr;
  end;
end;

function GetDescWK(StrWK: String): String;
var
  p: Integer;
begin
  Result := '';
  p := Pos(CHR_SEP, StrWK);
  if p > 0 then
    Result := Copy(StrWK, 0 , p -1);
end;

function GetLongDescWK(StrWK: String): String;
var
  p: Integer;
begin
  Result := '';
  p := Pos(CHR_SEP, StrWK);
  if p > 0 then
    Result := Copy(StrWK, p + 1, Length(StrWK) + 1);
end;

function Min(X, Y: Integer): Integer;
begin
  if x < y then
    Result := x
  else
    Result := y;
end;

function Max(X, Y: Integer): Integer;
begin
  if x > y then
    Result := x
  else
    Result := y;
end;

procedure SetParameters(Plant, TeamFrom, TeamTo: String;
  Shift, Day, WKSort, EMPSort, Selection: Integer; AllTeam: Boolean;
  DateSelection: TDateTime; ShowOnlyAvailEmpl,ShowWKWithOcc : Boolean);
begin
  FPlant := Plant;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShift := Shift;
  FDay := Day;
  FWKDEPTSort := WKSort;
  FEMPSort := EMPSort;
  FSelection := Selection;
  FAllTeam := AllTeam;
  FDate := GetDate(DateSelection);
  FShowOnlyAvailEmpl := ShowOnlyAvailEmpl;
  FShowWKWithOcc := ShowWKWithOcc;
end;

 
procedure ExecuteSql(QueryWork: TQuery; Selectstr: String);
begin
  QueryWork.Active := False;

  if SelectStr <> '' then
  begin
    QueryWork.SQL.Clear;
    QueryWork.SQL.Add(Selectstr);
  end;
  QueryWork.ExecSQL;
end;

procedure ExecuteSelectSQL(TmpQuery: TQuery; SelectStr: String);
begin
  TmpQuery.Active := False;
  TmpQuery.UniDirectional := False;
  if SelectStr <> '' then
  begin
    TmpQuery.SQL.Clear;
    TmpQuery.SQL.Add(UpperCase(SelectStr));
  end;
  if not TmpQuery.Prepared then
     TmpQuery.Prepare;
  TmpQuery.Active := True;
end;

procedure SetSelectSQL(TmpQuery: TQuery; SelectStr: String; Active: Boolean);
begin
  if SelectStr <> '' then
  begin
    TmpQuery.SQL.Clear;
    TmpQuery.SQL.Add(UpperCase(SelectStr));
  end;
  if Active then
    TmpQuery.Open;
end;

procedure GetEmpColor(OCI, Planned, ColorLevel: Integer; var ColorX: Integer);
begin
  if OCI > Planned then
     ColorX := ColorLevel;
  if OCI = Planned then
     ColorX := Green;
  if OCI < Planned then
     ColorX := Yellow;
end;

function FillLeadSpace(Str: String; LenghStr: Integer): String;
var
  i: Integer;
begin
  if (Length(Str) >= LenghStr) then
        result := Str
  else
  begin
     for i:= 1 to (LenghStr - Length(Str)) do
       Str := ' ' + Str;
     result := Str;
  end;
end;

function GetTableBTN(Color: Integer; Pressed, Level: String): String;
var
  ChrLevel: Char;
begin
  if Level <> '' then
    ChrLevel := Level[1]
  else
    ChrLevel := ' ';
  // RV067.MRA.20 Bugfix.
  case Color of
    Invisible: Result := '20';
    Normal:    begin
                 if (Pressed = 'Y') then
                   case ChrLevel of
                     'A': Result := '0';
                     'B': Result := '1';
                     'C': Result := '2';
                   else
                     Result := '3';
                   end
                 else
                   if (Pressed = 'N') then
                    case ChrLevel of
                      'A': Result := '4';
                      'B': Result := '5';
                      'C': Result := '6';
                      else
                        Result := '7';
                    end;
              end;
   DarkGrey:  case ChrLevel of
                'A': Result := '8';
                'B': Result := '9';
                'C': Result := '10';
              else
                Result := '11';
              end;
   Green:     case ChrLevel of
                'A': Result := '12';
                'B': Result := '13';
                'C': Result := '14';
              else
                Result := '15';
              end;
   LightGreen:case ChrLevel of
                'A': Result := '16';
                'B': Result := '17';
                'C': Result := '18';
              else
                Result := '19';
              end;
   Yellow:    Result := '21';
   Red:       case ChrLevel of
                'A': Result := '22';
                'B': Result := '23';
                'C': Result := '24';
              else
                Result := '25';
             end;
   end;
end;

procedure GetAtributeBTN(BtnStr: String;
  var Color: Integer; var Pressed, Level: String);
var
  IntBtn: Integer;
begin
  if BtnStr = '' then
  begin
    Color := 0;
    Pressed := '';
    Level := '';
    Exit;
  end;
  IntBtn := StrToInt(BtnStr);
  case IntBtn of
    0: begin Color := Normal; Pressed := 'Y'; Level := 'A';  end;
    1: begin Color := Normal; Pressed := 'Y'; Level := 'B';  end;
    2: begin Color := Normal; Pressed := 'Y'; Level := 'C';  end;
    3: begin Color := Normal; Pressed := 'Y'; Level :=  '';  end;
    4: begin Color := Normal; Pressed := 'N'; Level :=  'A'; end;
    5: begin Color := Normal; Pressed := 'N'; Level :=  'B'; end;
    6: begin Color := Normal; Pressed := 'N'; Level :=  'C'; end;
    7: begin Color := Normal; Pressed := 'N'; Level :=  '';  end;
    8: begin Color := DarkGrey; Pressed := 'Y'; Level :=  'A'; end;
    9: begin Color := DarkGrey; Pressed := 'Y'; Level :=  'B'; end;
   10: begin Color := DarkGrey; Pressed := 'Y'; Level :=  'C'; end;
   11: begin Color := DarkGrey; Pressed := 'Y'; Level :=  '';  end;
   12: begin Color := Green; Pressed := 'Y'; Level :=  'A'; end;
   13: begin Color := Green; Pressed := 'Y'; Level :=  'B'; end;
   14: begin Color := Green; Pressed := 'Y'; Level :=  'C'; end;
   15: begin Color := Green; Pressed := 'Y'; Level :=  '';  end;
   16: begin Color := LightGreen; Pressed := 'Y'; Level := 'A' end;
   17: begin Color := LightGreen; Pressed := 'Y'; Level := 'B' end;
   18: begin Color := LightGreen; Pressed := 'Y'; Level := 'C' end;
   19: begin Color := LightGreen; Pressed := 'Y'; Level := ''  end;
   20: begin Color := Invisible;  Pressed := 'N'; Level := ''; end;
   21: begin Color := Yellow;  Pressed := 'Y'; Level := ''; end;
   22: begin Color := Red;  Pressed := 'Y'; Level := 'A'; end;
   23: begin Color := Red;  Pressed := 'Y'; Level := 'B'; end;
   24: begin Color := Red;  Pressed := 'Y'; Level := 'C'; end;
   25: begin Color := Red;  Pressed := 'Y'; Level := ''; end;
  end;
end;

function IsRed(I: Integer): Boolean;
begin
  Result := (i = Red) or (i = darkred) or (i = lightred);
end;

function RedColor(Level: String): Integer;
begin
  Result := Normal;
  if Level = 'A' then
    Result := DarkRed;
  if Level = 'B' then
    Result := Red;
  if Level = 'C' then
    Result := LightRed;
end;

function GetFieldsPivot(ColumnName: String; TmpTable: TTable;
  ColIndex: Integer;
  var ValColumn: String; var Employee : Integer): Boolean;
var
  PosWK: Integer;
begin
  Result := False;
  PosWK := Pos('WK', ColumnName);
  if PosWK <= 0 then
    Exit;
  if ColIndex <= 0 then
    exit;
  Result := True;
  ValColumn := TmpTable.FieldByNAME(ColumnName).AsString;
  ValColumn := Trim(Copy(ValColumn, ColIndex * 2 -1, 2));
  Employee := TmpTable.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
end;

function WKEMPL_GetFieldsPivot(ColumnName: String; TmpTable: TTable;
  var Employee, IndexWK: Integer): Boolean;
var
  PosEMP: Integer;
begin
  Result := False;
  PosEmp := Pos('BTN', ColumnName);
  if posEmp <= 0 then
    Exit;
  Result := True;

  IndexWK := StrToInt(Copy(ColumnName, 3, PosEmp - 5));
  Employee := TmpTable.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
end;

procedure CreateBand(dxGridTMP: TdxDBGrid; IndexBand, Width: Integer;
  CaptionStr: String);
var
  p: Integer;
begin
  p := POS(CHR_SEP, CaptionStr);
  if P > 0 then
    CaptionStr := Copy(CaptionStr, 0, p-1) + ' ' +
      Copy(CaptionStr, p + 1, length(captionstr) + 1);
  dxGridTMP.Bands[IndexBand] := dxGridTMP.Bands.Add;
  dxGridTMP.Bands[IndexBand].Caption := CaptionStr;
  dxGridTMP.Bands[IndexBand].Sizing := False;
  dxGridTMP.Bands[IndexBand].Width := Width;
  dxGridTMP.Bands[IndexBand].DisableDragging := True;
end;

procedure CreateColumnOfBand(dxGridTMP: TdxDBGrid;
  IndexCol, IndexBand, Width: Integer; CaptionStr, FieldByName: String;
  TagCol: Integer);
begin
  dxGridTMP.Columns[IndexCol] := dxGridTMP.CreateColumn(TdxDBGridColumn);
  dxGridTMP.Columns[IndexCol].Caption := CaptionStr;

  dxGridTMP.Columns[IndexCol].BandIndex := IndexBand;
  dxGridTMP.Columns[IndexCol].Width := Width;
  dxGridTMP.Columns[IndexCol].Sizing := False;
  dxGridTMP.Columns[IndexCol].Alignment := taLeftJustify;
  dxGridTMP.Columns[IndexCol].Tag := TagCol;
  if (FieldByName <> '')  then
    dxGridTMP.Columns[IndexCol].FieldName := FieldByName;
end;

function GetLevel(i: Integer): String;
begin
  Result := '';
  case i of
    0, 4, 8, 12, 16, 22 : Result := 'A';
    1, 5, 9, 13, 17, 23: Result := 'B';
    2, 6, 10, 14, 18, 3, 7, 11, 15, 19, 21, 24, 25: Result :='C';
  end;
end;

procedure GiveMessageOverplanning;
begin
  if FOverPlanning then
  begin
    DisplayMessage(SOverPlanning, mtInformation, [mbOk]);
    FOverPlanning := False;
  end;
end;

function GetPlannedWKFromPivot(Index: Integer; TmpTable: TTable): String;
var
  i, PrevColor: Integer;
  PrevPressed, PrevLevel, BtnStr: String;
begin
  Result := '';
  for i:= 0 to FWKCount  do
  begin
    BtnStr := Trim(Copy(TmpTable.FieldByName('WK'+
      IntToStr(i)).AsString, 2* Index - 1,2));
    GetAtributeBTN(BtnStr, PrevColor, PrevPressed, PrevLevel);
    if ( PrevColor = Green) or  (PrevColor = LightGreen) then
    begin
      Result := 'WK' + IntToStr(i);
      Exit;
    end;
  end;
end;
procedure UpdateAbsList(Pressed: Boolean; Index, Employee: Integer;
  Absence: String);
var
  IndexList: Integer;
begin
  if Not Pressed then
  begin
    IndexList := FListABSRsn.IndexOF(IntToStr(Employee) + CHR_SEP + IntToStr(Index));
    if IndexList >= 0 then
    begin
      FListABSRsn.Delete(IndexList);
      FListABSRsn.Delete(IndexList);
    end;
  end
  else
  begin
    IndexList := FListABSRsn.IndexOF(IntToStr(Employee) + CHR_SEP + IntToStr(Index));
    if IndexList >= 0 then
    begin
      FListABSrSN.Delete(IndexList + 1);
      FListABSrSN.Insert(IndexList + 1, Absence);
    end
    else
    begin
      FListABSRsn.Add(IntToStr(Employee) + CHR_SEP + IntToStr(Index));
      FListABSRSN.Add(Absence);
    end;
  end;
end;

function IsPressed(ValStr:String): Boolean;
begin
  if ((ValStr = '4') or (ValStr = '5') or (ValStr = '6') or (ValStr = '7')) then
    Result := False
  else
    Result := True;
end;

// MR:29-10-2004 Order 550349
function EmployeesAcrossPlants: Integer;
begin
  if SystemDM.EmployeesAcrossPlantsYN = CHECKEDVALUE then
    Result := 1
  else
    Result := 0;
end;

function Int2Hex(AColIndex: Integer): String;
begin
  Result := IntToHex(AColIndex, 1);
end;

function Hex2Int(AHex: String): Integer;
begin
  Result := StrToInt('$'+AHex);
end;

end.
