inherited AbsReasonDM: TAbsReasonDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableDetail: TTable
    Top = 123
  end
  inherited DataSourceDetail: TDataSource
    DataSet = QueryDetail
    Top = 116
  end
  object TableAbsenceType: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'ABSENCETYPE'
    Left = 92
    Top = 269
    object TableAbsenceTypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableAbsenceTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableAbsenceTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableAbsenceTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object TableHourType: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 92
    Top = 328
    object TableHourTypeHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableHourTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableHourTypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableHourTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableHourTypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableHourTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableHourTypeBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
  object DataSourceAbsenceType: TDataSource
    DataSet = TableAbsenceType
    Left = 208
    Top = 272
  end
  object DataSourceHourType: TDataSource
    DataSet = TableHourType
    Left = 208
    Top = 324
  end
  object QueryDetail: TQuery
    BeforePost = DefaultBeforePost
    AfterPost = QueryDetailAfterPost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = QueryDetailNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    RequestLive = True
    SQL.Strings = (
      'SELECT'
      '  A.ABSENCEREASON_ID,'
      '  A.ABSENCETYPE_CODE,'
      '  A.ABSENCEREASON_CODE,'
      '  A.DESCRIPTION,'
      '  A.HOURTYPE_NUMBER,'
      '  A.PAYED_YN,'
      '  A.OVERRULE_WITH_ILLNESS_YN,'
      '  A.CORRECT_WORK_HRS_YN,'
      '  A.CREATIONDATE,'
      '  A.MUTATIONDATE,'
      '  A.MUTATOR,'
      '  A.EXPORT_CODE'
      'FROM'
      '  ABSENCEREASON A'
      'ORDER BY'
      '   A.ABSENCEREASON_CODE'
      ' ')
    Left = 96
    Top = 176
    object QueryDetailABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
    end
    object QueryDetailABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Size = 1
    end
    object QueryDetailABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Size = 1
    end
    object QueryDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object QueryDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object QueryDetailPAYED_YN: TStringField
      FieldName = 'PAYED_YN'
      Size = 1
    end
    object QueryDetailOVERRULE_WITH_ILLNESS_YN: TStringField
      FieldName = 'OVERRULE_WITH_ILLNESS_YN'
      Size = 1
    end
    object QueryDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object QueryDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object QueryDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
    object QueryDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      Size = 30
      Lookup = True
    end
    object QueryDetailABSENCETYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'ABSENCETYPELU'
      LookupDataSet = TableAbsenceType
      LookupKeyFields = 'ABSENCETYPE_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCETYPE_CODE'
      Size = 30
      Lookup = True
    end
    object QueryDetailCORRECT_WORK_HRS_YN: TStringField
      FieldName = 'CORRECT_WORK_HRS_YN'
      Origin = 'PIMS.ABSENCEREASON.CORRECT_WORK_HRS_YN'
      Size = 4
    end
    object QueryDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Origin = 'PIMS.ABSENCEREASON.EXPORT_CODE'
      Size = 24
    end
  end
end
