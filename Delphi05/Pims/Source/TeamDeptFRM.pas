(*
  MRA:12-JAN-2010. RV050.8. 889955.
  - Added Plant-level for teams/team-departments.
  - Restrict plants using teamsperuser.
  MRA:21-OCT-2010. RV073.2. Small bug.
  - Corrected typing error (caption of form): Deparments per team
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)
unit TeamDeptFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, dxDBTLCl, dxGrClms, dxEditor,
  dxExEdtr, dxDBEdtr, dxDBELib;

type
  TTeamDeptF = class(TGridBaseF)
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnResponsible: TdxDBGridColumn;
    Label1: TLabel;
    dxDetailGridColumnDepartment: TdxDBGridColumn;
    dxDetailGridColumnPlant: TdxDBGridColumn;
    dxDBLookupEditDept: TdxDBLookupEdit;
    dxDetailGridColumn3: TdxDBGridColumn;
    pnlMasterPlantGrid: TPanel;
    dxDBMasterPlantGrid: TdxDBGrid;
    dxMasterPlantGridColumnCode: TdxDBGridColumn;
    dxMasterPlantGridColumnDescription: TdxDBGridColumn;
    dxMasterPlantGridColumnCity: TdxDBGridColumn;
    dxMasterPlantGridColumnState: TdxDBGridColumn;
    dxMasterPlantGridColumnPhone: TdxDBGridColumn;
    LabelPlant: TLabel;
    dxDetailGridColumnDeptCode: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetFilterPlant;
  end;

  function TeamDeptF: TTeamDeptF;

implementation
{$R *.DFM}

uses TeamDeptDMT, SystemDMT;

var
  TeamDeptF_HND: TTeamDeptF;

function TeamDeptF: TTeamDeptF;
begin
  if (TeamDeptF_HND = nil) then
  begin
    TeamDeptF_HND := TTeamDeptF.Create(Application);
  end;
  Result := TeamDeptF_HND;
end;

procedure TTeamDeptF.FormDestroy(Sender: TObject);
begin
  inherited;
  TeamDeptF_HND := nil;
end;

procedure TTeamDeptF.FormCreate(Sender: TObject);
begin
  TeamDeptDM := CreateFormDM(TTeamDeptDM);
  if (dxDetailGrid.DataSource = nil) or (dxMasterGrid.DataSource = nil) or
    (dxDBMasterPlantGrid.DataSource = nil) then
  begin
    dxDetailGrid.DataSource := TeamDeptDM.DataSourceDetail;
    dxMasterGrid.DataSource := TeamDeptDM.DataSourceMaster;
    dxDBMasterPlantGrid.DataSource := TeamDeptDM.DataSourceMasterPlant;
  end;
  inherited;
  // 20014289
  dxDBMasterPlantGrid.BandFont.Color := clWhite;
  dxDBMasterPlantGrid.BandFont.Style := [fsBold];
end;

procedure TTeamDeptF.SetFilterPlant;
{var
  Plant: String; }
begin
  // RV050.8.
{
  if TeamDeptDM.TableDetail.RecordCount > 0 then
  begin
    TeamDeptDM.TableDetail.First;
    Plant := TeamDeptDM.TableDetail.FieldByName('PLANT_CODE').AsString;
    TeamDeptDM.TablePlant.Filtered := True;
    TeamDeptDM.TablePlant.Filter := 'PLANT_CODE = ''' + DoubleQuote(Plant) + '''';
  end
  else
    TeamDeptDM.TablePlant.Filtered := False;
}
end;

procedure TTeamDeptF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  SetFilterPlant;
  TeamDeptDM.TableDetail.Last;
  inherited;
//  dxDBLookupEditPlant.SetFocus;
  dxDBLookupEditDept.SetFocus;
end;

procedure TTeamDeptF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
//  dxDBLookupEditPlant.SetFocus;
  dxDBLookupEditDept.SetFocus;
end;

procedure TTeamDeptF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  SetFilterPlant;
//  dxDBLookupEditPlant.DataSource.DataSet.Close;
//  dxDBLookupEditPlant.DataSource.DataSet.Open;
  dxDBLookupEditDept.DataSource.DataSet.Close;
  dxDBLookupEditDept.DataSource.DataSet.Open;
  dxDBLookupEditDept.Text :=
    TeamDeptDM.TableDetail.FieldByName('DEPARTMENTCAL').AsString;
end;

procedure TTeamDeptF.FormShow(Sender: TObject);
begin
  inherited;
  SetFilterPlant;
//  dxDBLookupEditPlant.DataSource.DataSet.Close;
//  dxDBLookupEditPlant.DataSource.DataSet.Open;
  dxDBLookupEditDept.DataSource.DataSet.Close;
  dxDBLookupEditDept.DataSource.DataSet.Open;
  // RV050.8. Do following, so DetailGrid is shown OK,
  // otherwise the vertical scrollbar is not shown completely.
  pnlDetail.Align := alNone;
  pnlDetail.Align := alBottom;

  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      TeamDeptDM.TableMasterPlant.FieldByName('PLANT_CODE').AsString
  else
    TeamDeptDM.TableMasterPlant.FindKey([SystemDM.ASaveTimeRecScanning.Plant]);
end;

procedure TTeamDeptF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  dxDBLookupEditDept.Text :=
    TeamDeptDM.TableDetail.FieldByName('DEPARTMENTCAL').AsString;
end;

procedure TTeamDeptF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;

  SystemDM.ASaveTimeRecScanning.Plant :=
    TeamDeptDM.TableMasterPlant.FieldByName('PLANT_CODE').AsString
end;

end.
