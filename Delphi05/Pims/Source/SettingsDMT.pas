(*
  MRA:6-JUN-2011. RV093.4.
  - Use stored procedures to delete records, where a certain amount of records
    is deleted with commits in-between, instead of deleting them all at once.
  MRA:11-MAR-2013 20014035 Hide ID optionally in Timerecording/Personal Screen
  - Addition of column named Hide ID. This is a checkbox.
    It sets the field HIDE_ID_YN to 'Y' or 'N' in table
    WORKSTATION.
  MRA:7-APR-2015 SO-20016449
  - Time Zone Implementation
  MRA:11-MAR-2016 PIM-150
  - Add option to show only default job in timerecording-application.
*)

unit SettingsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TSettingsDM = class(TGridBaseDM)
    StoredProcCleanup_PQ: TStoredProc;
    StoredProcCleanup_PHE: TStoredProc;
    StoredProcCleanup_PHEPT: TStoredProc;
    StoredProcCleanup_SHE: TStoredProc;
    StoredProcCleanup_TRS: TStoredProc;
    StoredProcCleanup_ABSLOGAHE: TStoredProc;
    StoredProcCleanup_ABSLOGPHE: TStoredProc;
    StoredProcCleanup_ABSLOGPHEPT: TStoredProc;
    StoredProcCleanup_ABSLOGSHE: TStoredProc;
    StoredProcCleanup_ABSLOGTRS: TStoredProc;
    TableDetailCOMPUTER_NAME: TStringField;
    TableDetailLICENSEVERSION: TIntegerField;
    TableDetailTIMERECORDING_YN: TStringField;
    TableDetailSHOW_HOUR_YN: TStringField;
    TableDetailHIDE_ID_YN: TStringField;
    TableDetailCARDREADER_DELAY: TFloatField;
    TableDetailMANREGS_MULTJOBS_YN: TStringField;
    TableDetailPLANT_CODE: TStringField;
    qryPlant: TQuery;
    TableDetailPLANTLU: TStringField;
    TableDetailSHOW_ONLY_DEFJOB_YN: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    FDateFrom: TDateTime;
  public
    { Public declarations }
    function Cleanup_PQ: Boolean;
    function Cleanup_PHE: Boolean;
    function Cleanup_PHEPT: Boolean;
    function Cleanup_SHE: Boolean;
    function Cleanup_TRS: Boolean;
    function Cleanup_ABSLOGAHE: Boolean;
    function Cleanup_ABSLOGPHE: Boolean;
    function Cleanup_ABSLOGPHEPT: Boolean;
    function Cleanup_ABSLOGSHE: Boolean;
    function Cleanup_ABSLOGTRS: Boolean;
    property DateFrom: TDateTime read FDateFrom write FDateFrom;
  end;

var
  SettingsDM: TSettingsDM;

implementation

{$R *.DFM}

uses SystemDMT, UPimsMessageRes, UPimsConst, UTimeZone;

function TSettingsDM.Cleanup_ABSLOGAHE: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_ABSLOGAHE do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_ABSLOGPHE: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_ABSLOGPHE do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_ABSLOGPHEPT: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_ABSLOGPHEPT do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_ABSLOGSHE: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_ABSLOGSHE do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_ABSLOGTRS: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_ABSLOGTRS do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_PQ: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_PQ do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_PHE: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_PHE do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_PHEPT: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_PHEPT do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_SHE: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_SHE do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

function TSettingsDM.Cleanup_TRS: Boolean;
begin
  Result := True;
  try
    with StoredProcCleanup_TRS do
    begin
      ParamByName('DATEFROM').AsDateTime := DateFrom;
      ExecProc;
    end;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;

procedure TSettingsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  ActiveTables(Self, True);
end;

procedure TSettingsDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  TableDetail.FieldByName('COMPUTER_NAME').AsString :=
    Trim(TableDetail.FieldByName('COMPUTER_NAME').AsString );
end;

procedure TSettingsDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  // 20016449
  if TableDetail.FieldByName('COMPUTER_NAME').AsString = SystemDM.CurrentComputerName then
    SystemDM.DetermineWorkstationTimezonehrsdiff;
end;

end.
