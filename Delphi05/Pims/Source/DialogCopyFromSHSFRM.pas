(*
  Changes:
    MRA:21-APR-2011. RV090.1.
    - Add option to Confirm/Undo a copy-action.
    MRA:25-FEB-2014 20011800
    - Final Run System
    - Prevent it is copying data for dates <= last-export-date.
*)
unit DialogCopyFromSHSFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables, dxExGrEd, dxExELib,
  dxLayout;

type
  TDialogCopyFromSHSF = class(TDialogSelectionF)
    GroupBoxCopy: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    TableCopy: TTable;
    Label6: TLabel;
    dxSpinEditYearFrom: TdxSpinEdit;
    dxSpinEditWeekFrom: TdxSpinEdit;
    dxSpinEditYearTo: TdxSpinEdit;
    dxSpinEditWeekTo: TdxSpinEdit;
    dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    FStartWeek{, FStartEmpl}: Integer;
    FYear, FEmp: Integer;
    FStartDate, FEndDate: TDateTime; // Destination's employee Start+EndDate
    FCopy: Boolean;
    procedure GetMinDate(EmplDest, EmplSource: Integer;
      DateMax: TDateTime; var DateMin, DateSourceMin, DateDestMin: TDateTime);
  end;

var
  DialogCopyFromSHSF: TDialogCopyFromSHSF;

implementation

uses ListProcsFRM, ShiftScheduleDMT, SystemDMT,
  UPimsMessageRes, UPimsConst;

{$R *.DFM}

procedure TDialogCopyFromSHSF.FormShow(Sender: TObject);
begin
  inherited; 
  dxSpinEditWeekFrom.Value := FStartWeek;
  dxSpinEditWeekTo.Value := FStartWeek;
  dxSpinEditYearFrom.Value := FYear;
  dxSpinEditYearTo.Value := FYear;
end;

procedure TDialogCopyFromSHSF.GetMinDate(EmplDest, EmplSource: Integer;
  DateMax: TDateTime; var DateMin, DateSourceMin, DateDestMin: TDateTime);
begin
  DateSourceMin := 0;
  TableCopy.FindNearest([EmplDest, DateMin]);
  if (not TableCopy.Eof) and
    (TableCopy.FieldByName('EMPLOYEE_NUMBER').AsInteger = EmplDest) and
    (TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime >= DateMin) and
    (TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime <= DateMax) then
      DateSourceMin := TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime;

  DateDestMin := 0;
  TableCopy.FindNearest([EmplSource, DateMin]);
  if (not TableCopy.Eof) and
    (TableCopy.FieldByName('EMPLOYEE_NUMBER').AsInteger = EmplSource) and
    (TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime >= DateMin) and
    (TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime <= DateMax) then
      DateDestMin := TableCopy.FieldByNAME('SHIFT_SCHEDULE_DATE').AsDateTime;
   if (DateDestMin <> 0) or (DateSourceMin <> 0) then
   begin
     if DateDestMin = 0 then
       DateMin := DateSourceMin
     else
       if DateSourceMin = 0 then
         DateMin := DateDestMin
       else
         if (DateDestMin < DateSourceMin) then
           DateMin := DateDestMin
         else
           DateMin :=  DateSourceMin;
   end;
end;

procedure TDialogCopyFromSHSF.btnOkClick(Sender: TObject);
var
  DateMin, DateMax, DateSHS: TDateTime;
  DateSourceMin, DateDestMin: TDateTime;
  Result,
  EmployeeFrom: Integer;
  GoOn: Boolean; // 20011800
  FinalRunExportDate: TDateTime; // 20011800
  SkipCount: Integer; // 20011800
begin
  inherited;
  TableCopy.Open; // MR:20-01-2006 Optimising.
  Result := 0;
  SkipCount := 0; // 20011800
  DateMin := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekFrom.Value), 1);
  DateMax := ListProcsF.DateFromWeek(Round(dxSpinEditYearTo.Value),
    Round(dxSpinEditWeekTo.Value), 7);
  if CompareDate(DateMin, DateMax) > 1 then
  begin
    DisplayMessage( SPimsStartEndDate, mtInformation, [mbOk]);
    Exit;
  end;
  EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
  if (EmployeeFrom = FEmp) then
  begin
    DisplayMessage( SCopySameValues, mtInformation, [mbOk]);
    Exit;
  end;
  // RV090.1. Start a transaction, to make it possible to commit/rollback it
  //          later.
  SystemDM.Pims.StartTransaction;
  with ShiftScheduleDM do
  begin
    GetMinDate(FEmp, EmployeeFrom, DateMax, DateMin, DateSourceMin, DateDestMin);
    if (DateDestMin <> 0) or (DateSourceMin <> 0) then
    begin
      DateSHS := DateMin;
      while (DateSHS <= DateMax) do
      begin
        // MR:23-07-2004 Order 550323
        // Don't copy to days the employee is not-active
        if (DateSHS >= FStartDate) and
          ((DateSHS <= FEndDate) or (FEndDate = 0)) then
        begin
          GoOn := True;
          // 20011800 Final Run System
          if SystemDM.UseFinalRun then
          begin
            FinalRunExportDate :=
              SystemDM.FinalRunExportDateByEmployee(FEmp);
            if FinalRunExportDate <> NullDate then
              if DateSHS <= FinalRunExportDate then
              begin
                inc(SkipCount);
                GoOn := False;
              end;
          end;
          if GoOn then
            CopyFunction({TableSHS, }TableCopy, EmployeeFrom, FEmp,
              DateSHS, DateSHS, Result);
        end;
        DateSHS := DateSHS + 1;
      end;
    end;
  end;{with}
  // 20011800
  if SystemDM.UseFinalRun and (SkipCount > 0) then
    DisplayMessage(SPimsFinalRunShiftSchedule, mtInformation, [mbOK])
  else
    DisplayMessage( SCopyFinished, mtInformation, [mbOk]);
  FCopy := True;
end;

procedure TDialogCopyFromSHSF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TableCopy.Close;
end;

procedure TDialogCopyFromSHSF.FormCreate(Sender: TObject);
begin
  inherited;
  // MR:20-1-2006 Do this later, optimising.
//  TableCopy.Open;
end;

procedure TDialogCopyFromSHSF.btnCancelClick(Sender: TObject);
begin
  inherited;
  FCopy := False;
end;

procedure TDialogCopyFromSHSF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearFrom.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;

  try
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearTo.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogCopyFromSHSF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
