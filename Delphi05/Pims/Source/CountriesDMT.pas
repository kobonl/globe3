(*
  SO: 19-07-2010 Order 550497
  MRA:19-OCT-2011 RV099.1. Order 20012234
  - Addition of 3 fields/settings for TFT/Holiday correction.
  - These can be left empty (nullable).
    - TFT_HOL_AUTO_REPLACE_YN
    - TFT_ABSENCEREASON_ID
    - HOL_ABSENCEREASON_ID
  - Disabled some part in code that was not used.
*)
unit CountriesDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TCountriesDM = class(TGridBaseDM)
    QueryDetail: TQuery;
    DataSourceExportPayroll: TDataSource;
    TableExportPayroll: TTable;
    TableExportPayrollEXPORT_TYPE: TStringField;
    QueryDetailCOUNTRY_ID: TFloatField;
    QueryDetailCODE: TStringField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailEXPORT_TYPE: TStringField;
    QueryDetailCREATIONDATE: TDateTimeField;
    QueryDetailMUTATIONDATE: TDateTimeField;
    QueryDetailMUTATOR: TStringField;
    QueryDetailTFT_HOL_AUTO_REPLACE_YN: TStringField;
    QueryDetailTFT_ABSENCEREASON_ID: TFloatField;
    QueryDetailHOL_ABSENCEREASON_ID: TFloatField;
    qryAbsenceReason: TQuery;
    DataSourceAbsenceReason: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryDetailAfterPost(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure DefaultNewRecord(DataSet: TDataSet);
  private
    { Private declarations }
    procedure CountriesRefresh(DataSet: TDataSet);
  public
    { Public declarations }
    procedure FillAbsenceReasons(ACountryID: Integer);
  end;

var
  CountriesDM: TCountriesDM;

implementation

uses SystemDMT, UPimsConst, UPimsMessageRes;

{$R *.DFM}

procedure TCountriesDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryDetail.Open;
end;

procedure TCountriesDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  try
    if (QueryDetail.State in [dsEdit, dsInsert]) then
      QueryDetail.Post;
  finally
    QueryDetail.Close;
  end;
end;

//   in combination with small/capital letters in a key-field.
procedure TCountriesDM.CountriesRefresh(DataSet: TDataSet);
var
  MyKey: String;
begin
  with DataSet do
  begin
    if not (State in [dsEdit, dsInsert]) then
    begin
      MyKey := FieldByName('CODE').AsString;
      Close;
      Open;
      Locate('CODE', MyKey, []);
    end;
  end;
end;

procedure TCountriesDM.QueryDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  CountriesRefresh(DataSet);
end;

procedure TCountriesDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
  // RV099.1. Check if both absence reasons fields are filled in,
  //          when 'auto replace' is Yes.
  if DataSet.FieldByName('TFT_HOL_AUTO_REPLACE_YN').AsString = CHECKEDVALUE then
  begin
    if (DataSet.FieldByName('TFT_ABSENCEREASON_ID').AsString = '') or
      (DataSet.FieldByName('HOL_ABSENCEREASON_ID').AsString = '') then
    begin
      DisplayMessage(SPimsPleaseFillBothAbsReasons, mtWarning, [mbOK]);
      Abort;
    end;
  end;

  // RV099.1. Disabled this part, reason:
  //          - Mandatory fields that are left empty are already checked.
  //          - Never use hardcoded messages.
{
  if Trim(Dataset.FieldByName('code').AsString) = '' then
  begin
    DisplayMessage('Country code cannot be empty!!', mtWarning, [mbOK]);
    Abort;
  end;
  if Trim(Dataset.FieldByName('description').AsString) = '' then
  begin
    DisplayMessage('Country description cannot be empty!!', mtWarning, [mbOK]);
    Abort;
  end;
}
end;

// RV099.1.
procedure TCountriesDM.FillAbsenceReasons(ACountryID: Integer);
begin
  qryAbsenceReason.Close;
  qryAbsenceReason.SQL.Text :=
    'SELECT * FROM ABSENCEREASON ORDER BY DESCRIPTION';
  qryAbsenceReason.Open;
  if ACountryId <> 0 then
    SystemDM.UpdateAbsenceReasonPerCountry(
      ACountryId, qryAbsenceReason
    );
end;

procedure TCountriesDM.DefaultNewRecord(DataSet: TDataSet);
begin
  inherited;
  // RV099.1.
  DataSet.FieldByName('TFT_HOL_AUTO_REPLACE_YN').AsString := UNCHECKEDVALUE;
end;

end.
