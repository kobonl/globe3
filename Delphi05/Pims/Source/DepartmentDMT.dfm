inherited DepartmentDM: TDepartmentDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnFilterRecord = TableMasterFilterRecord
    TableName = 'PLANT'
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableMasterFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TableMasterINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TableMasterOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  inherited TableDetail: TTable
    OnNewRecord = TableDetailNewRecord
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'PLANT_CODE;DEPARTMENT_CODE'
    MasterFields = 'PLANT_CODE'
    TableName = 'DEPARTMENT'
    UpdateMode = upWhereKeyOnly
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailDIRECT_HOUR_YN: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'DIRECT_HOUR_YN'
      Size = 1
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailPLAN_ON_WORKSPOT_YN: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
    object TableDetailREMARK: TStringField
      FieldName = 'REMARK'
      Size = 40
    end
    object TableDetailBUSINESSLU: TStringField
      FieldKind = fkLookup
      FieldName = 'BUSINESSLU'
      LookupDataSet = TableBusiness
      LookupKeyFields = 'BUSINESSUNIT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'BUSINESSUNIT_CODE'
      LookupCache = True
      Lookup = True
    end
  end
  object TableBusiness: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = TableBusinessFilterRecord
    TableName = 'BUSINESSUNIT'
    Left = 92
    Top = 200
    object TableBusinessBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableBusinessDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableBusinessPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableBusinessCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableBusinessBONUS_FACTOR: TFloatField
      FieldName = 'BONUS_FACTOR'
    end
    object TableBusinessNORM_ILL_VS_DIRECT_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_DIRECT_HOURS'
    end
    object TableBusinessMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableBusinessMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableBusinessNORM_ILL_VS_INDIRECT_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_INDIRECT_HOURS'
    end
    object TableBusinessNORM_ILL_VS_TOTAL_HOURS: TFloatField
      FieldName = 'NORM_ILL_VS_TOTAL_HOURS'
    end
  end
  object DataSourceBusiness: TDataSource
    DataSet = TableBusiness
    Left = 204
    Top = 200
  end
end
