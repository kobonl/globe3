inherited ReportJobCommentsDM: TReportJobCommentsDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object qryJobComments: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  JC.PLANT_CODE, P.DESCRIPTION PDESCRIPTION,'
      '  JC.WORKSPOT_CODE, W.DESCRIPTION WDESCRIPTION,'
      '  JC.JOB_CODE, J.DESCRIPTION JDESCRIPTION,'
      '  JC.EMPLOYEE_NUMBER, E.DESCRIPTION EDESCRIPTION,'
      '  JC.TIMESTAMP,'
      '  JC.MESSAGE'
      'FROM JOBCOMMENT JC INNER JOIN PLANT P ON'
      '  JC.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN WORKSPOT W ON'
      '  JC.PLANT_CODE = W.PLANT_CODE AND'
      '  JC.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  INNER JOIN JOBCODE J ON'
      '  JC.PLANT_CODE = J.PLANT_CODE AND'
      '  JC.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  JC.JOB_CODE = J.JOB_CODE'
      '  INNER JOIN EMPLOYEE E ON'
      '  JC.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  JC.PLANT_CODE >= :PLANTFROM AND'
      '  JC.PLANT_CODE <= :PLANTTO AND'
      '  JC.TIMESTAMP >= :DATEFROM AND'
      '  JC.TIMESTAMP < :DATETO  '
      'ORDER BY '
      '  JC.PLANT_CODE, '
      '  JC.WORKSPOT_CODE,'
      '  JC.TIMESTAMP  '
      ' '
      ' ')
    Left = 40
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
