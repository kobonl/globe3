unit DialogMonthlyGroupEfficiencyDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, Db, DBTables;

type
  TDialogMonthlyGroupEfficiencyDM = class(TDataModule)
    qryPQWorkspot: TQuery;
    qryPQWorkspotPLANT_CODE: TStringField;
    qryPQWorkspotWORKSPOT_CODE: TStringField;
    qryPQWorkspotSUMQUANTITY: TFloatField;
    qryPQWorkspotEFFICIENCY: TFloatField;
    qryNormProdTotal: TQuery;
    qryNormProdTotalPLANT_CODE: TStringField;
    qryNormProdTotalWORKSPOT_CODE: TStringField;
    qryNormProdTotalJOB_CODE: TStringField;
    qryNormProdTotalNORM_PROD_LEVEL: TFloatField;
    qryNormProdTotalSUMPRODMINUTE: TFloatField;
    qryGroupEfficiencyInsert: TQuery;
    qryGroupEfficiencyDelete: TQuery;
    procedure qryPQWorkspotCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FGroupEfficiency: Boolean;
  public
    { Public declarations }
    procedure DetermineGroupEfficiency(
      const AYear, AMonth: Integer;
      const APlantFrom, APlantTo, ADeptFrom, ADeptTo,
        AWorkspotFrom, AWorkspotTo: String);
    property GroupEfficiency: Boolean read FGroupEfficiency write
      FGroupEfficiency;
  end;

var
  DialogMonthlyGroupEfficiencyDM: TDialogMonthlyGroupEfficiencyDM;

implementation

uses
  ListProcsFRM, UPimsConst;

{$R *.DFM}

procedure TDialogMonthlyGroupEfficiencyDM.qryPQWorkspotCalcFields(
  DataSet: TDataSet);
var
  NormProdTotal: Double;
begin
  NormProdTotal := 0;

  qryNormProdTotal.Filtered := False;
  qryNormProdTotal.Filter := 'PLANT_CODE = ' +
    qryPQWorkspotPLANT_CODE.AsString + ' AND ' +
      'WORKSPOT_CODE = ' + qryPQWorkspotWORKSPOT_CODE.AsString;
  qryNormProdTotal.Filtered := True;
  if not qryNormProdTotal.IsEmpty then
  begin
    qryNormProdTotal.First;
    while not qryNormProdTotal.Eof do
    begin
      NormProdTotal := NormProdTotal +
        ((Round(qryNormProdTotalSUMPRODMINUTE.Value) / 60) *
          qryNormProdTotalNORM_PROD_LEVEL.Value);
      qryNormProdTotal.Next;
    end;
  end;

  qryPQWorkspotEFFICIENCY.Value := 0;
  if NormProdTotal > 0 then
    qryPQWorkspotEFFICIENCY.Value :=
      Round(qryPQWorkspotSUMQUANTITY.Value) / NormProdTotal * 100;
end;

// Determine Efficiency per: Plant, Workspot.
// Store results in table GroupEfficiency.
procedure TDialogMonthlyGroupEfficiencyDM.DetermineGroupEfficiency(
  const AYear, AMonth: Integer;
  const APlantFrom, APlantTo, ADeptFrom, ADeptTo,
    AWorkspotFrom, AWorkspotTo: String);
var
  SelectStr: String;
  DateMin, DateMax: TDateTime;
  DepartmentSelection, WorkspotSelection: String;
  procedure DeleteGroupEfficiency;
  begin
    WorkspotSelection := '';
    if APlantFrom = APlantTo then
      WorkspotSelection :=
        'AND WORKSPOT_CODE >= :WORKSPOTFROM ' +
        'AND WORKSPOT_CODE <= :WORKSPOTTO';
    qryGroupEfficiencyDelete.Close;
    qryGroupEfficiencyDelete.SQL.Clear;
    qryGroupEfficiencyDelete.SQL.Add(
      'DELETE FROM GROUPEFFICIENCY ' +
      'WHERE YEAR_NUMBER = :YEAR_NUMBER ' +
      'AND MONTH_NUMBER = :MONTH_NUMBER ' +
      'AND PLANT_CODE >= :PLANTFROM ' +
      'AND PLANT_CODE <= :PLANTTO ' +
      WorkspotSelection
      );
    qryGroupEfficiencyDelete.ParamByName('YEAR_NUMBER').AsInteger := AYear;
    qryGroupEfficiencyDelete.ParamByName('MONTH_NUMBER').AsInteger := AMonth;
    qryGroupEfficiencyDelete.ParamByName('PLANTFROM').AsString :=
      APlantFrom;
    qryGroupEfficiencyDelete.ParamByName('PLANTTO').AsString :=
      APlantTo;
    if APlantFrom = APlantTo then
    begin
      qryGroupEfficiencyDelete.ParamByName('WORKSPOTFROM').AsString :=
        AWorkspotFrom;
      qryGroupEfficiencyDelete.ParamByName('WORKSPOTTO').AsString :=
        AWorkspotTo;
    end;
    qryGroupEfficiencyDelete.ExecSQL;
  end;
  function DeterminePQWorkspot: Boolean;
  begin
    DepartmentSelection := '';
    WorkspotSelection := '';
    if APlantFrom = APlantTo then
    begin
      DepartmentSelection :=
        'AND (W.DEPARTMENT_CODE >= :DEPTFROM AND ' +
        'W.DEPARTMENT_CODE <= :DEPTTO) ';
      WorkspotSelection :=
        'AND (PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND ' +
        'PQ.WORKSPOT_CODE <= :WORKSPOTTO) ';
    end;
    SelectStr :=
      'SELECT ' + NL +
      '  PQ.PLANT_CODE, PQ.WORKSPOT_CODE, SUM(PQ.QUANTITY) AS SUMQUANTITY ' + NL +
      'FROM ' + NL +
      '  PRODUCTIONQUANTITY PQ INNER JOIN WORKSPOT W ON ' + NL +
      '    PQ.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '    PQ.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
    if GroupEfficiency then
      SelectStr := SelectStr +
        '  AND W.GROUP_EFFICIENCY_YN = ''Y'' ' + NL;
    SelectStr := SelectStr +
      'WHERE ' + NL +
      '  (PQ.PLANT_CODE >= :PLANTFROM AND PQ.PLANT_CODE <= :PLANTTO) ' + NL +
      '  AND (PQ.START_DATE >= :DATEMIN AND PQ.END_DATE <= :DATEMAX) ' + NL +
      WorkspotSelection + ' ' + NL +
      DepartmentSelection + ' ' + NL +
      'GROUP BY ' + NL +
      '  PQ.PLANT_CODE, PQ.WORKSPOT_CODE' + NL;
    qryPQWorkspot.Close;
    qryPQWorkspot.SQL.Clear;
    qryPQWorkspot.SQL.Add(SelectStr);
    qryPQWorkspot.ParamByName('PLANTFROM').AsString := APlantFrom;
    qryPQWorkspot.ParamByName('PLANTTO').AsString := APlantTo;
    if APlantFrom = APlantTo then
    begin
      qryPQWorkspot.ParamByName('DEPTFROM').AsString := ADeptFrom;
      qryPQWorkspot.ParamByName('DEPTTO').AsString := ADeptTo;
      qryPQWorkspot.ParamByName('WORKSPOTFROM').AsString := AWorkspotFrom;
      qryPQWorkspot.ParamByName('WORKSPOTTO').AsString := AWorkspotTo;
    end;
    qryPQWorkspot.ParamByName('DATEMIN').AsDateTime := DateMin;
    qryPQWorkspot.ParamByName('DATEMAX').AsDateTime := DateMax;
    qryPQWorkspot.Open;
    Result := not qryPQWorkspot.IsEmpty;
  end;
  procedure DetermineNormProdTotal;
  begin
    DepartmentSelection := '';
    WorkspotSelection := '';
    if APlantFrom = APlantTo then
    begin
      DepartmentSelection :=
        'AND (W.DEPARTMENT_CODE >= :DEPTFROM AND ' +
        'W.DEPARTMENT_CODE <= :DEPTTO) ';
      WorkspotSelection :=
        'AND (P.WORKSPOT_CODE >= :WORKSPOTFROM AND ' +
        'P.WORKSPOT_CODE <= :WORKSPOTTO) ';
    end;
    SelectStr :=
      'SELECT ' + NL +
      '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, J.NORM_PROD_LEVEL, ' + NL +
      '  SUM(P.PRODUCTION_MINUTE) AS SUMPRODMINUTE ' + NL +
      'FROM ' + NL +
      '  PRODHOURPEREMPLOYEE P INNER JOIN WORKSPOT W ON ' + NL +
      '    P.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '    P.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
    if GroupEfficiency then
      SelectStr := SelectStr +
        '  AND W.GROUP_EFFICIENCY_YN = ''Y'' ' + NL;
    SelectStr := SelectStr +
      '  LEFT JOIN JOBCODE J ON ' + NL +
      '    P.PLANT_CODE = J.PLANT_CODE AND ' + NL +
      '    P.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
      '    P.JOB_CODE = J.JOB_CODE ' + NL +
      'WHERE ' + NL +
      '  (P.PLANT_CODE >= :PLANTFROM AND P.PLANT_CODE <= :PLANTTO) ' + NL +
      DepartmentSelection + NL +
      WorkspotSelection + NL +
      '  AND (P.PRODHOUREMPLOYEE_DATE >= :DATEMIN ' + NL +
      '  AND P.PRODHOUREMPLOYEE_DATE <= :DATEMAX) ' + NL +
      'GROUP BY ' + NL +
      '  P.PLANT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, J.NORM_PROD_LEVEL' + NL;
    qryNormProdTotal.Close;
    qryNormProdTotal.SQL.Clear;
    qryNormProdTotal.SQL.Add(SelectStr);
    qryNormProdTotal.ParamByName('PLANTFROM').AsString := APlantFrom;
    qryNormProdTotal.ParamByName('PLANTTO').AsString := APlantTo;
    if APlantFrom = APlantTo then
    begin
      qryNormProdTotal.ParamByName('DEPTFROM').AsString := ADeptFrom;
      qryNormProdTotal.ParamByName('DEPTTO').AsString := ADeptTo;
      qryNormProdTotal.ParamByName('WORKSPOTFROM').AsString := AWorkspotFrom;
      qryNormProdTotal.ParamByName('WORKSPOTTO').AsString := AWorkspotTo;
    end;
    qryNormProdTotal.ParamByName('DATEMIN').AsDateTime := DateMin;
    qryNormProdTotal.ParamByName('DATEMAX').AsDateTime := DateMax;
    qryNormProdTotal.Open;
  end;
  procedure InsertGroupEfficiency;
  begin
    qryPQWorkspot.First;
    while not qryPQWorkspot.Eof do
    begin
      qryGroupEfficiencyInsert.Close;
      qryGroupEfficiencyInsert.ParamByName('YEAR_NUMBER').AsInteger :=
        AYear;
      qryGroupEfficiencyInsert.ParamByName('MONTH_NUMBER').AsInteger :=
        AMonth;
      qryGroupEfficiencyInsert.ParamByName('PLANT_CODE').AsString :=
        qryPQWorkspot.FieldByName('PLANT_CODE').AsString;
      qryGroupEfficiencyInsert.ParamByName('WORKSPOT_CODE').AsString :=
        qryPQWorkspot.FieldByName('WORKSPOT_CODE').AsString;
      qryGroupEfficiencyInsert.ParamByName('EFFICIENCY').AsFloat :=
        qryPQWorkspot.FieldByName('EFFICIENCY').AsFloat;
      qryGroupEfficiencyInsert.ParamByName('CREATIONDATE').AsDateTime :=
        Now;
      qryGroupEfficiencyInsert.ParamByName('MUTATIONDATE').AsDateTime :=
        Now;
      qryGroupEfficiencyInsert.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      qryGroupEfficiencyInsert.ExecSQL;
       qryPQWorkspot.Next;
    end;
  end;
begin
  DateMin := EncodeDate(AYear, AMonth, 1);
  // MR:01-02-2005 Also add time-part, otherwise 'PQ'-comparison goes wrong.
  DateMax := EncodeDate(AYear, AMonth,
    ListProcsF.LastMounthDay(AYear, AMonth)) +
      Frac(EncodeTime(23, 59, 59, 0));

  // First delete records from table GROUPEFFICIENCY
  DeleteGroupEfficiency;
  // Then Determine PQ Workspots
  if DeterminePQWorkspot then
  begin
    // Determine the Norm Prod Totals
    DetermineNormProdTotal;
    // Now insert records for table GROUPEFFICIENCY
    InsertGroupEfficiency;
  end;

  qryPQWorkspot.Close;
  qryNormProdTotal.Close;
  qryGroupEfficiencyInsert.Close;
  qryGroupEfficiencyDelete.Close;
end;

procedure TDialogMonthlyGroupEfficiencyDM.DataModuleCreate(
  Sender: TObject);
begin
  // MR:13-03-2007 Order 550443. If GroupEfficiency is True an
  //               extra filter must be used in selects.
  GroupEfficiency := False;
end;

end.
