inherited StandardOccupationF: TStandardOccupationF
  Left = 249
  Top = 134
  Width = 1013
  Height = 687
  Caption = 'Standard Occupation'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 76
    Width = 997
    Height = 308
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 304
      Width = 995
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 995
      Height = 303
      Bands = <
        item
          Caption = 'Department'
        end
        item
          Caption = 'Workspot'
        end>
      DefaultLayout = False
      KeyField = 'DEPARTMENT_CODE'
      PopupMenu = PopupMenuCopyPaste
      DataSource = StandardOccupationDM.DataSourceDepartmentMASTER
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      OnCustomDrawCell = dxMasterGridCustomDrawCell
      object dxMasterGridColumnPLAN_ON_WORKSPOT_YN: TdxDBGridColumn
        Caption = 'Plan on workspot '
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLAN_ON_WORKSPOT_YN'
      end
      object dxMasterGridColumnDEPARTMENT_CODE: TdxDBGridColumn
        Caption = 'Dept code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENT_CODE'
      end
      object dxMasterGridColumnDEPARTMENT_DESCRIPTION: TdxDBGridColumn
        Caption = 'Dept description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENTDESCRIPTION'
      end
      object dxMasterGridColumnWORKSPOT_CODE: TdxDBGridColumn
        Caption = 'Workspot code'
        BandIndex = 1
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxMasterGridColumnWORKSPOT_DESCRIPTION: TdxDBGridColumn
        Caption = 'Workspot description'
        BandIndex = 1
        RowIndex = 0
        FieldName = 'WORKSPOTDESCRIPTION'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 548
    Width = 997
    Height = 100
    TabOrder = 3
    object GroupBox2: TGroupBox
      Left = 131
      Top = 1
      Width = 865
      Height = 98
      Align = alClient
      Caption = 'Timeblocks'
      TabOrder = 0
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 861
        Height = 81
        Align = alClient
        TabOrder = 0
        object ScrollBox1: TScrollBox
          Left = 1
          Top = 1
          Width = 859
          Height = 79
          Align = alClient
          BorderStyle = bsNone
          TabOrder = 0
          object grpBxTIMEBLOCK1: TGroupBox
            Left = 0
            Top = 0
            Width = 136
            Height = 62
            Caption = '1'
            TabOrder = 0
            object Panel1A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label6: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit1A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_1'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel1B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label7: TLabel
                Left = 8
                Top = 1
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit1B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_1'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel1C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label8: TLabel
                Left = 8
                Top = 1
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit1C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_1'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK2: TGroupBox
            Left = 136
            Top = 0
            Width = 133
            Height = 62
            Caption = '2'
            TabOrder = 1
            object Panel2A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label9: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit2A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_2'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel2B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label10: TLabel
                Left = 8
                Top = 1
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit2B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_2'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel2C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label11: TLabel
                Left = 8
                Top = 1
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit2C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_2'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK3: TGroupBox
            Left = 269
            Top = 0
            Width = 136
            Height = 62
            Caption = '3'
            TabOrder = 2
            object Panel3A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label12: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit3A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_3'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel3B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label13: TLabel
                Left = 8
                Top = 1
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit3B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_3'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel3C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label14: TLabel
                Left = 8
                Top = 1
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit3C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_3'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK4: TGroupBox
            Left = 405
            Top = 0
            Width = 136
            Height = 62
            Caption = '4'
            TabOrder = 3
            object Panel4A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit4A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_4'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel4B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label4: TLabel
                Left = 8
                Top = 1
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit4B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_4'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel4C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label5: TLabel
                Left = 8
                Top = 1
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit4C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_4'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK5: TGroupBox
            Left = 541
            Top = 0
            Width = 136
            Height = 62
            Caption = '5'
            TabOrder = 4
            object Panel5A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label17: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit5A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_5'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel5B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label18: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit5B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_5'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel5C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label19: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit5C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_5'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK6: TGroupBox
            Left = 677
            Top = 0
            Width = 136
            Height = 62
            Caption = '6'
            TabOrder = 5
            object Panel6A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label20: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit6A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_6'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel6B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label21: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit6B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_6'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel6C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label22: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit6C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_6'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK7: TGroupBox
            Left = 813
            Top = 0
            Width = 136
            Height = 62
            Caption = '7'
            TabOrder = 6
            object Panel7A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label23: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit7A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_7'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel7B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label24: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit7B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_7'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel7C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label25: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit7C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_7'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK8: TGroupBox
            Left = 949
            Top = 0
            Width = 136
            Height = 62
            Caption = '8'
            TabOrder = 7
            object Panel8A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label26: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit8A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_8'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel8B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label27: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit8B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_8'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel8C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label28: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit8C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_8'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK9: TGroupBox
            Left = 1085
            Top = 0
            Width = 136
            Height = 62
            Caption = '9'
            TabOrder = 8
            object Panel9A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label29: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit9A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_9'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel9B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label30: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit9B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_9'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel9C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label31: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit9C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_9'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
          object grpBxTIMEBLOCK10: TGroupBox
            Left = 1221
            Top = 0
            Width = 136
            Height = 62
            Caption = '10'
            TabOrder = 9
            object Panel10A: TPanel
              Left = 2
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object Label32: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'A'
              end
              object dxDBSpinEdit10A: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_A_TIMEBLOCK_10'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel10B: TPanel
              Left = 46
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              object Label33: TLabel
                Left = 8
                Top = 2
                Width = 6
                Height = 13
                Caption = 'B'
              end
              object dxDBSpinEdit10B: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_B_TIMEBLOCK_10'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
            object Panel10C: TPanel
              Left = 90
              Top = 15
              Width = 44
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 2
              object Label34: TLabel
                Left = 8
                Top = 2
                Width = 7
                Height = 13
                Caption = 'C'
              end
              object dxDBSpinEdit10C: TdxDBSpinEdit
                Left = 1
                Top = 20
                Width = 41
                TabOrder = 0
                DataField = 'OCC_C_TIMEBLOCK_10'
                DataSource = StandardOccupationDM.DataSourceDetail
              end
            end
          end
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 130
      Height = 98
      Align = alLeft
      Caption = 'Standard Occupation'
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 126
        Height = 81
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 7
          Width = 19
          Height = 13
          Caption = 'Day'
        end
        object Label2: TLabel
          Left = 4
          Top = 33
          Width = 26
          Height = 13
          Caption = 'Fixed'
        end
        object Label35: TLabel
          Left = 4
          Top = 62
          Width = 26
          Height = 13
          Caption = 'Show'
        end
        object Label36: TLabel
          Left = 41
          Top = 48
          Width = 7
          Height = 13
          Caption = 'A'
        end
        object Label37: TLabel
          Left = 73
          Top = 48
          Width = 6
          Height = 13
          Caption = 'B'
        end
        object Label38: TLabel
          Left = 107
          Top = 48
          Width = 7
          Height = 13
          Caption = 'C'
        end
        object DaySelection: TComboBox
          Tag = 1
          Left = 37
          Top = 4
          Width = 84
          Height = 21
          DropDownCount = 6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 0
          OnChange = DaySelectionChange
        end
        object DBCheckBoxFixedYN: TDBCheckBox
          Left = 37
          Top = 31
          Width = 17
          Height = 17
          Caption = 'DBCheckBoxFixedYN'
          DataField = 'FIXED_YN'
          DataSource = StandardOccupationDM.DataSourceDetail
          TabOrder = 1
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object DBCheckBoxPlantOCC_A: TDBCheckBox
          Left = 37
          Top = 61
          Width = 17
          Height = 17
          Caption = 'DBCheckBoxFixedYN'
          DataField = 'STDOCC_A_YN'
          DataSource = StandardOccupationDM.DataSourcePlant
          TabOrder = 2
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxPlantOCC_Click
        end
        object DBCheckBoxPlantOCC_B: TDBCheckBox
          Left = 69
          Top = 61
          Width = 17
          Height = 17
          Caption = 'DBCheckBoxFixedYN'
          DataField = 'STDOCC_B_YN'
          DataSource = StandardOccupationDM.DataSourcePlant
          TabOrder = 3
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxPlantOCC_Click
        end
        object DBCheckBoxPlantOCC_C: TDBCheckBox
          Left = 103
          Top = 62
          Width = 17
          Height = 17
          Caption = 'DBCheckBoxFixedYN'
          DataField = 'STDOCC_C_YN'
          DataSource = StandardOccupationDM.DataSourcePlant
          TabOrder = 4
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxPlantOCC_Click
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 384
    Width = 997
    Height = 164
    Align = alBottom
    inherited spltDetail: TSplitter
      Top = 160
      Width = 995
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 995
      Height = 159
      Bands = <
        item
        end
        item
          Caption = '1'
        end
        item
          Caption = '2'
        end
        item
          Caption = '3'
        end
        item
          Caption = '4'
        end
        item
          Caption = '5'
        end
        item
          Caption = '6'
        end
        item
          Caption = '7'
        end
        item
          Caption = '8'
        end
        item
          Caption = '9'
        end
        item
          Caption = '10'
        end>
      DefaultLayout = False
      KeyField = 'DAY_OF_WEEK'
      PopupMenu = PopupMenuCopyPaste
      DataSource = StandardOccupationDM.DataSourceDetail
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumnDayDesc: TdxDBGridColumn
        Caption = 'Desc'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DAYDESC'
      end
      object dxDetailGridColumnFIXED: TdxDBGridCheckColumn
        Caption = 'Fixed'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FIXED_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnTIMEBLOCK1A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 1
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_1'
      end
      object dxDetailGridColumnTIMEBLOCK1B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 1
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_1'
      end
      object dxDetailGridColumnTIMEBLOCK1C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 1
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_1'
      end
      object dxDetailGridColumnTIMEBLOCK2A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 2
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_2'
      end
      object dxDetailGridColumnTIMEBLOCK2B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 2
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_2'
      end
      object dxDetailGridColumnTIMEBLOCK2C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 2
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_2'
      end
      object dxDetailGridColumnTIMEBLOCK3A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 3
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_3'
      end
      object dxDetailGridColumnTIMEBLOCK3B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 3
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_3'
      end
      object dxDetailGridColumnTIMEBLOCK3C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 3
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_3'
      end
      object dxDetailGridColumnTIMEBLOCK4A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 4
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_4'
      end
      object dxDetailGridColumnTIMEBLOCK4B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 4
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_4'
      end
      object dxDetailGridColumnTIMEBLOCK4C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 4
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_4'
      end
      object dxDetailGridColumnTIMEBLOCK5A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 5
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_5'
      end
      object dxDetailGridColumnTIMEBLOCK5B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 5
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_5'
      end
      object dxDetailGridColumnTIMEBLOCK5C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 5
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_5'
      end
      object dxDetailGridColumnTIMEBLOCK6A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 6
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_6'
      end
      object dxDetailGridColumnTIMEBLOCK6B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 6
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_6'
      end
      object dxDetailGridColumnTIMEBLOCK6C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 6
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_6'
      end
      object dxDetailGridColumnTIMEBLOCK7A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 7
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_7'
      end
      object dxDetailGridColumnTIMEBLOCK7B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 7
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_7'
      end
      object dxDetailGridColumnTIMEBLOCK7C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 7
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_7'
      end
      object dxDetailGridColumnTIMEBLOCK8A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 8
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_8'
      end
      object dxDetailGridColumnTIMEBLOCK8B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 8
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_8'
      end
      object dxDetailGridColumnTIMEBLOCK8C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 8
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_8'
      end
      object dxDetailGridColumnTIMEBLOCK9A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 9
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_9'
      end
      object dxDetailGridColumnTIMEBLOCK9B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 9
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_9'
      end
      object dxDetailGridColumnTIMEBLOCK9C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 9
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_9'
      end
      object dxDetailGridColumnTIMEBLOCK10A: TdxDBGridSpinColumn
        Caption = 'A'
        BandIndex = 10
        RowIndex = 0
        FieldName = 'OCC_A_TIMEBLOCK_10'
      end
      object dxDetailGridColumnTIMEBLOCK10B: TdxDBGridSpinColumn
        Caption = 'B'
        BandIndex = 10
        RowIndex = 0
        FieldName = 'OCC_B_TIMEBLOCK_10'
      end
      object dxDetailGridColumnTIMEBLOCK10C: TdxDBGridSpinColumn
        Caption = 'C'
        BandIndex = 10
        RowIndex = 0
        FieldName = 'OCC_C_TIMEBLOCK_10'
      end
    end
  end
  object pnlPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 997
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox6: TGroupBox
      Left = 0
      Top = 0
      Width = 997
      Height = 50
      Align = alClient
      Caption = 'Plant/Shift'
      TabOrder = 0
      object Label15: TLabel
        Left = 4
        Top = 21
        Width = 24
        Height = 13
        Caption = 'Plant'
      end
      object Label16: TLabel
        Left = 360
        Top = 21
        Width = 22
        Height = 13
        Caption = 'Shift'
      end
      object cmbPlusPlant: TComboBoxPlus
        Left = 37
        Top = 18
        Width = 265
        Height = 19
        ColCount = 88
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnChange = cmbPlusPlantChange
      end
      object cmbPlusShift: TComboBoxPlus
        Left = 392
        Top = 18
        Width = 257
        Height = 19
        ColCount = 88
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        Style = csIncSearch
        ParentCtl3D = False
        TabOrder = 1
        TitleColor = clBtnFace
        OnChange = cmbPlusShiftChange
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
  end
  inherited dsrcActive: TDataSource
    Top = 152
  end
  object PopupMenuCopyPaste: TPopupMenu
    Left = 288
    Top = 148
    object CopyRecord: TMenuItem
      Caption = 'Copy'
      OnClick = CopyRecordClick
    end
    object PasteRecord: TMenuItem
      Caption = 'Paste'
      OnClick = PasteRecordClick
    end
  end
end
