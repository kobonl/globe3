inherited StaffPlanningOCIDM: TStaffPlanningOCIDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 220
  Top = 160
  Height = 479
  Width = 798
  inherited TableMaster: TTable
    Left = 20
    Top = 16
  end
  inherited TableDetail: TTable
    MasterSource = nil
    Left = 20
    Top = 68
  end
  inherited DataSourceMaster: TDataSource
    Left = 96
    Top = 16
  end
  inherited DataSourceDetail: TDataSource
    Left = 96
    Top = 68
  end
  inherited TableExport: TTable
    Left = 228
    Top = 12
  end
  inherited DataSourceExport: TDataSource
    Left = 312
    Top = 12
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    UpdateMode = upWhereChanged
    Left = 48
    Top = 328
  end
  object DataSourceOCI: TDataSource
    DataSet = TableOCI
    Left = 96
    Top = 128
  end
  object StoredProcInsertOCI: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_INSERTPIVOTOCI'
    Left = 48
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'PIVOTCOMPUTERNAME'
        ParamType = ptInput
      end>
  end
  object StoredProcUpdateOCI: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 168
    Top = 272
  end
  object StoredProcUpdateOCPL: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFPLANNING_UPDATEOCPL'
    Left = 288
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'DATESELECTION'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'DAYSELECTION'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUT'
        ParamType = ptInput
      end>
  end
  object QuerySTOC: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DEPARTMENT_CODE, WORKSPOT_CODE ,'
      '  OCC_A_TIMEBLOCK_1,OCC_A_TIMEBLOCK_2,'
      '  OCC_A_TIMEBLOCK_3,OCC_A_TIMEBLOCK_4,'
      '  OCC_A_TIMEBLOCK_5,OCC_A_TIMEBLOCK_6,'
      '  OCC_A_TIMEBLOCK_7,OCC_A_TIMEBLOCK_8,'
      '  OCC_A_TIMEBLOCK_9,OCC_A_TIMEBLOCK_10,'
      '  OCC_B_TIMEBLOCK_1,OCC_B_TIMEBLOCK_2,'
      '  OCC_B_TIMEBLOCK_3,OCC_B_TIMEBLOCK_4,'
      '  OCC_B_TIMEBLOCK_5,OCC_B_TIMEBLOCK_6,'
      '  OCC_B_TIMEBLOCK_7,OCC_B_TIMEBLOCK_8,'
      '  OCC_B_TIMEBLOCK_9,OCC_B_TIMEBLOCK_10,'
      '  OCC_C_TIMEBLOCK_1,OCC_C_TIMEBLOCK_2,'
      '  OCC_C_TIMEBLOCK_3,OCC_C_TIMEBLOCK_4,'
      '  OCC_C_TIMEBLOCK_5,OCC_C_TIMEBLOCK_6,'
      '  OCC_C_TIMEBLOCK_7,OCC_C_TIMEBLOCK_8,'
      '  OCC_C_TIMEBLOCK_9,OCC_C_TIMEBLOCK_10'
      'FROM'
      '  STANDARDOCCUPATION'
      'WHERE '
      '  PLANT_CODE =:PLANT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND '
      '  DAY_OF_WEEK = :DAY_OF_WEEK '
      'ORDER BY '
      '  DEPARTMENT_CODE, WORKSPOT_CODE '
      ''
      ' ')
    Left = 48
    Top = 200
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY_OF_WEEK'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetSTOC: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderSTOC'
    Left = 168
    Top = 208
  end
  object DataSetProviderSTOC: TDataSetProvider
    DataSet = QuerySTOC
    Constraints = True
    Left = 288
    Top = 208
  end
  object TableOCI: TTable
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'OCI_LEVEL'
    MasterSource = DataSourceMaster
    TableName = 'PIVOTOCISTAFFPLANNING'
    UpdateMode = upWhereChanged
    Left = 24
    Top = 128
  end
  object qryOCIPlanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select *'
      'from pivotocistaffplanning t'
      'where t.pivotcomputername = :COMPUTER_NAME and'
      '  t.oci_level = :OCI_LEVEL'
      ' ')
    Left = 208
    Top = 128
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'OCI_LEVEL'
        ParamType = ptUnknown
      end>
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  STDOCC_A_YN,'
      '  STDOCC_B_YN,'
      '  STDOCC_C_YN'
      'FROM'
      '  PLANT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      ''
      ' '
      ' ')
    Left = 168
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
