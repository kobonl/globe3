(*
    Changes:
      MRA: 13-OCT-2008 RV010. (Revision 10)
        Additions for ExportPARIS: (Order 550466)
        1) Export also BusinessUnit Code and Department Code of Employee.
        2) Export also Exceptional Hours.
        IMPORTANT:
          For 'Exceptional Hours' a distinctive export-code must be
          used that is not part of GRADEEXPORTCODE-table. Otherwise
          the 'exceptional hours' will be added together with the
          'normal hours' because of the 'shift-upgrade'-mechanism.
      MRA: 22-OCT 2008 RV011. (Revision 11)
        Changes for ExportPARIS: (Order 550466)
        - Use 'upgrade shift'-mechanism also for except. hours.
      MRA:07-NOV 2008 RV013. (Revision 13)
        Changes for ExportPARIS: (Order 550466)
        - Do not use 'upgrade shift'-mechanism for except. hours.
      MRA:08-DEC-2008 RV016. (ExportParis: Order 550466)
        - Do not use shifts 1 and 2 anymore. Instead of that:
          - set shift to 1 for 'exceptional hours NOT used'
          - set shift to 2 for 'exceptional hours used'
          based on export-grade-table-contents.
      MRA:02-JAN-2009 RV017. (ExportParis: Order 550466, addition).
        - When there are overtime and except. hours, then
          these must be combined.
      MRA:06-JAN-2009 RV018.
        - When weeknumber-selection is used:
          - Do not limit the date to the end of the year!
            Because a week can end in next year!
      MRA:09-JAN-2009 RV019. (ExportParis: Order 550466, addition).
        - Because om combination of overtime + except. hours, 2 bugs solved:
          - hours can also be negative! Changed comparison.
          - Changed sum-calculation in query for Paris, plant+workspot must
            be included, to prevent wrong results (too many hours).
          - Added debug-lines. This can be used under condition.
      MRA:28-JAN-2009 RV022.
        - Do not limit the date at the start of year!
          Because a week start in previous year!
      MRA:04-MAR-2009 RV023. (ExportParis: Order 550466)
        - The combined hours (Exceptional + Overtime) must be based on the
          fact that a scan is in both these 2 ranges, instead of only looking
          at the day the Exceptional and Overtime hours were made.
          This means it must be detected during the calculation of the hours!
          To do so, it is done during hour-calculation and the combined result
          is stored in an extra field in PRODHOURPEREMPLPERTYPE-table, field
          EO_PROD_MINUTE.
      MRA:19-MAR-2009 RV024. (ExportParis: Order 550466)
        - Changes for:
          - It did not compensate regular/overtime when there where
            the combined hours (exceptional + overtime).
          - When there are no regular hours made on the combined hours-date,
            then it did not add regular hours for compensation.
          - Get Regular Export Code from hour-type-table, needed when
            compensating regular hours, that can happen when no regular
            hours were made on that date.
  MRA:2-APR-2009 RV025. (ExportParis: Order 550466)
    The calculation of the combined hours is now completely done during
    the hour-calculation. So no extra calculation is needed during the
    export payroll.
  MRA:6-MAY-2009 RV026.
    Add export payroll for REINO.
  MRA:29-MAY-2009 RV029. ExportPARIS.
    Make exception when hourtype that comes from
    'exceptional hours' is of type 'Overtime'.
  MRA:23-SEP-2009 RV034.1.
    - Added field CUSTOMER_NUMBER (numeric) to PLANT-table.
      For use with export-payroll-ADP. Field PLANT.CUSTOMER_NUMBER must be
      exported for ADP for each PK-line instead of
      EXPORTPAYROLL.CUSTOMER_NUMBER, for the employee's plant.
  MRA:25-SEP-2009 RV035.1.
    - Added 2 absence type for export payroll ADP;
      'M' = Maternity leave (zwangerschapsverlof)
      'A' = Lay days (wachtdagen)
  MRA:11-NOV-2009 RV040.
    - Add tilde to end of each line for Export-ADP.
  MRA:30-NOV-2009 RV045.1.
    - TimeForTime-settings are now possible on hourtype-level, instead
      of only contractgroup-level.
  MRA:06-JAN-2010 RV045.2.
    - Add timepart to first line of Export ADP.
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  - This dialog is now inherited from DialogReportBase.
  MRA:2-FEB-2010 RV051.1. Bugfix.
  - When selecting 'all teams', it used empty values for team-from-to.
    Cause: Because this dialog is inherited from DialogReportBase, it
    was missing an inherited in btnOKClick-procedure.
  MRA:22-MAR-2010 RV056.3.
  - Export Payroll ADP. CLF.
    - When exporting days, when export code is V288 then 2 decimals must
      be used, regardless of other settings (MultiplyFactorDays).
  SO:04-AUG-2010 RV067.5. 550497
    Export Attentia
  MRA:3-SEP-2010 RV067.MRA.27 Bugfix.
  - When using teams-per-user with only 1 plant
    it did not show employees in from-to-fields.
    Reason: It filtered on 2 places, in base-form
            and in its own form.
  MRA:7-SEP-2010 RV067.MRA.30 Changes for 550497
  - Dialog must show different selections based on export-type from
    country from selected plant.
  - Counters were not correctly exported.
  - Problem with detecting export type, when switching plants.
  MRA:7-OCT-2010 RV071.9. 550497 Export Attentia
  - Some fields were not filled out to a fixed length.
  - Bug in selection-screen: checkboxes resulted in a re-load
    of the From/To-Employee-selection fields.
  - Last number for export was not stored.
  - Because there can be multiple ExportPayroll-records, for
    ADP en ATTENT, this must be filtered to get the correct record!
  - Show all counters; if they are set as active-counters.
  MRA:11-OCT-2010 RV071.13. 550497 Bugfixing.
  - Export Attentia Counters. It did also export the working day counters.
    This is disabled.
  - Export Attentia Counters. Field 11 must show date+time, not only date.
  - Export Attentia Counters. Only planned absence (holiday) for rest of year
    must be exported.
  MRA:13-OCT-2010 RV071.14. 550497 Bugfixing/Changes.
  - Not all hours export exported for Attentia.
    Reason: Problem with HourTypes-Per-Country.
  MRA:29-OCT-2010 RV075.5.
  - Changes for export attent.
  MRA:10-NOV-2010 RV079.1.
  - Changes for export Attentia Counters.
  - It must export 3 values: Used, Planned and Available.
  MRA:12-NOV-2010 RV079.4. (Export Attentia Counters).
  - GlobalDM-routines are now used for Balance-information, because
    it is needed in several places.
  MRA:13-DEC-2010 RV082.16.
  - Export ADP
    - When different hourtypes have the same
      export code, they are exported seperately.
      This must be changed: The hours for that same export
      code must be added.
  MRA:14-FEB-2011 RV086.4. Small fix.
  - Fixed: Some text-lines were hard-coded in english.
  MRA:22-FEB-2011 RV087.2. Export Payroll Select/AFAS (SO-20011509/20011510)
  - Export Payroll Select / AFAS
    - An export payroll is needed for Select/AFAS
  MRA:25-MAR-2011 RV088.1. SO-20011510.20 Export payroll AFAS.
  - Export all hours and all days. Use methods from ADP to determine days.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  - REMARK:
    - When using 'FormStyle=fsStayOnTop' then the SaveToFile-Dialog is
      hidden behind the 'DialogExportPayroll'-dialog when using Windows-XP.
      When using Windows 7, then it is NOT hidden.
      Because the SaveToFile-Dialog is a Windows-DLL-dialog, it looks like
      there is a difference in implementation between Win-XP and Win-7.
  MRA:22-JUN-2011. RV094.2. Bugfix.
  - Export Payroll - SR (VOS)
    - Gives an BDE-error: ORA-00936 missing expression.
  MRA:5-JUL-2011. RV094.5. Update.
  - Now export type AFAS and SELECT are combined, this must be separated
    for AFAS. When only AFAS should be used, then no choice must be
    shown for AFAS and SELECT.
  MRA:18-JUL-2011. RV095.2. SO-20011797
  - Export Payroll Select
    - Add option to select on either combination of:
      - Plant - employee + all related selections
      - OR:
      - Plant - department (not on employee)
    - Give message when nothing was found.
    - Bugfix:
      - After running a second time (when dialog was not closed),
        it could not found anything. Reason: There was still a filter active!
      - Solved by resetting the filter.
  MRA:29-JUL-2011. RV095.3. Bugfix
  - Export Payroll SR (VOS)
    - Export is per month, but it looks like
      it is exporting only 1 week:
      - There was a bug with detection if it was a 'month'-selection.
    - DetermineDateMinMax-procedure reorganized.
  MRA:1-AUG-2011. RV095.5. Bugfix
  - Export Payroll ATTENT/ADP (CLF)
    - This showed choice for 'period/month', this
      must be removed, because period is always used.
  MRA:10-OCT-2011. RV098.2 Bugfix for: RV095.2. SO-20011797
  - Export Payroll Select
    - When selection is on 'plant-department'-level, it
      still selects on all departments!
    - Turn sorted off for this type of export.
  MRA:11-NOV-2011. RV101.2. 20012331. Export AFAS.
  - Export Payroll AFAS
    - Use contract group-field 'Export worked days'
      to determine if worked days must be exported or not.
  MRA:29-NOV-2011. RV102.5. Bugfix.
  - Export Payroll - Attentia
    - The dialog shows a wrong selection when using
      export type 'ATTENT' (Attentia).
      For this export type it must show:
      - Date between <from> <to>
      - Export hours - checkbox
      - Export counters - checkbox
    - CAUSE: This happens when teams-per-user is
             used and there is only 1 plant shown!
  MRA:1-JUN-2012. SO-20013169 Travel time
  - Addition for travel time (day counter) with
    absence type code = 'N'.
  MRA:30-JUL-2012 20013430. Split export in 2 parts.
  - Export payroll SR
    - Split export in 2 parts (file + report)
      based on field EMPLOYEE.FREETEXT.
      Contents of field is 1 or 2.
      Based on this criteria create 2 files/reports.
  MRA:24-SEP-2012 TODO 21132 Problem in export payroll 'extern personeel'
  - Bugfix:
    - No sorting is done in query, because of differences ORA-10g and ORA-11g.
      Because of this, the StringList must be sorted. Because it also
      exports a header and footer, a second StringList must be used to
      that is not sorted to get the correct sequence:
      - Header
      - List of export-lines (sorted)
      - Footer
  MRA:18-OCT-2012 TD-21132 Related to this todo.
  - When All-Plant-Checkbox is clicked, do not select
    anymore on business unit and employees!
  - When ALL PLANTS then also ALL BUSINESS UNITS must be checked!
  MRA:2-NOV-2012 20013723 Combine Month/Period export payroll ADP
  - During export ADP: Use 2 settings based on checkboxes per hourtype/
    hourtype-per-country to decide
    if something must be exported for 'export overtime' (period) or for
    'export payroll' (month).
  - NOTE: ONLY DO THIS FOR CUSTOMER CLF (custom-made).
  - Extra: It did not check for the export-code defined at
           absencetype-per-country.
  MRA:8-NOV-2012 TODO 21132 Problem in export payroll 'extern personeel'
  - Sometimes it shows broken numbers for minutes.
    Reason: PHEPT-table can have broken numbers after rounding based on salary.
    Solution: Based on rounding-settings on contractgroup-level, round/trunc
              the minutes before they are exported.
  - Above is WRONG: Just round on 5 minutes.
  MRA:19-NOV-2012 20013288
  - Export Payroll Easy Labor (ELA)
  MRA:29-NOV-2012 20013288 Rework
  - Export Payroll Easy Labor (ELA)
    - When employee is of type 'external' (based on employee-contract) then do
      not export.
    - When employee has no employee-contract then always export.
  MRA:4-DEC-2012 20013288 Rework
  - Export of Swipe-information is not done by whole shift, reason:
    It must filter on SHIFT_DATE instead of on DATETIME_IN.
  MRA:4-DEC-2012 20013723.3 Additions.
  - Add 31-december-2012 for this special export ADP for CLF for
    'export overuren' and only for 2012.
  - Change title to 'export loon' and 'export overuren' when report is showing.
  MRA:6-DEC-2012 20013288.2 Rework
  - Only export when there is no open scan
  - Use a checkbox to turn this on or off. Checkbox 'Include employee(s) with
    open scan(s)'.
  MRA:10-DEC-2012 20013288.3 Rework
  - When checkbox 'Include employee(s) with open scan(s)' is checked, then
    when an open scan for today is found, then export it with timestamp of
    that moment.
  MRA:8-JAN-2013 TD-21897 Interface Easy Labor - Rework
  - Export Easy Labor
    - Changes needed for export-file:
      - As separator a comma must be used (instead of a semicolon)
      - Plant Code should not be exported
      - A header must be added
  MRA:9-JAN-2013 TD-21897 Interface Easy Labor - Rework
  - Export Easy Labor
    - Changes needed for Schedule-export-file:
      - As separator a comma must be used (instead of a semicolon)
      - A header must be added
  MRA:16-JAN-2013 20013288.4 Interface Easy Labor - Rework
  - Export Easy Labor
    - Needed changes:
      - For both types of export-files:
        - Change filename so it ends like _001.csv instead
          of .001.csv
      - Swipe-file:
        - Add second date (date for time out) again.
        - Header should be:
          Employee_ID, Datein, Time In, Dateout, Time Out, Department
      - Schedule-file:
        - Instead of Employee-number export Social-security-number
        - Header should be:
          Employee_ID,Plant code,Department,Date,Time In,Time Out,Available
  MRA:22-JAN-2013 20013288.130 Interface Easy Labor - addition
  - Export Easy Labor
    - Needed changes:
      - Add 2 checkboxes to export-dialog: Swipes and Schedule.
      - Default Swipes checkbox is checked.
      - Based on these 2 checkboxes, export Swipes or Schedule or both
      - When both are NOT checked, give a message about �Please make
        a choice about what to export�.
      - Use a second EXPORTPAYROLL-record with EXPORT_TYPE = 'ELA2' to
        keep track of last-export-date and sequence-number.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:27-MAY-2013 SO-20013288 Export Easy Labor
  - Make export of schedule (checkbox) default
    - Checkbox for export schedule must be checked by default, the
      checkbox for export swipe must be unchecked by default.
  MRA:18-OCT-2013 20014714
  - Addition of export payroll for WMU.
  MRA:18-APR-2014 20011800
  - Final Run System
  MRA:28-APR-2014 20011800.70 Rework
  - Final Run System Rework
    - Only show 1 plant, but also show other levels (???)
      - NOTE: Now it gets difficult to see what has been finalized.
  MRA:21-OCT-2014 TD-25958
  - Export payroll Select:
    - This gave an error about 'filter expression incorrectly terminated.'.
    - Cause: There was a name with an ' in it (O'keeffe Richard John).
      - This went wrong during use of a Filter
      - Technical note: Make use of build-in function: QuotedStr(str).
        This will add surrounding quotes and will double embedded quotes.
      - This QuotedStr has been added also in other parts (in this source)
        where filters are used.
  MRA:24-JUN-2015 20014450.50
  - Real Time Eff.
  MRA:4-JAN-2016 PIM-108
  - Instead of week-selection use date-selection for export payroll WMU.
  - GroupBoxDateSelection, DateFromDate, DateToDate added.
  MRA:19-FEB-2018 GLOB3-72
  - Export Payroll NTS
  MRA:6-APR-2018 GLOB3-110
  - Export payroll (SELECT) do not export BSN but employee number
  MRA:8-FEB-2019 GLOB3-234
  - Export Payroll Quickbooks
  MRA:15-APR-2019 GLOB3-271
  - Export payroll to Paychex
  MRA:26-APR-2019 GLOB3-297
  - Export payroll Navision
  MRA:13-MAY-2019 GLOB3-297
  - Export payroll Navision - Rework
    - Add date to export-file-name.
  - Related to this issue:
    - A Close was on wrong position, resulting in an access violation, because
      it closed the window instead of a query. When 'embed dialogs' was used,
      this gave an access violation.
  MRA:14-MAY-2019 GLOB3-271 - Rework
  - Export payroll to Paychex
  - Change extension from csv to txt
  MRA:17-MAY-2019 GLOB3-271 - Rework
  - Export payroll to Paychex
  - Export with fixed length fields, no comma's.
  MRA:20-MAY-2019 GLOB3-271 - Rework
  - Export payroll to Paychex
  - Change back to csv format.
  MRA: 24-JUN-2019 GLOB3-297
  - Export payroll Navision - Rework
  - Change format of 2 fields.
  MRA:12-JUL-2019 GLOB3-234
  - Export Payroll Quickbooks
  - Rework: A totally different export-file and format is needed.
  MRA:20-DEC-2019 GLOB3-377
  - Originally based on Quickbooks
  - Export to payroll - BambooHR
  - NOTE: Export type should be changed from QBOOKS to BAMBOO.
*)

unit DialogExportPayrollFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxfProgressBar, DialogExportPayrollDMT, dxLayout, dxExGrEd,
  dxExELib, FileCtrl, UGLobalFunctions, Grids, DBGrids, DBClient, SystemDMT,
  UPimsConst, jpeg;

const
  MaxLengthVarPart = 171;
  ExportADPUSFileName = 'IMPORT.XLS'; // ADPUS
  MySep = ';'; // ADPUS - Separator for excel-file
  MyEndOfLine = '~'; // RV040.
  RADIOGRP_PERIOD = 0;
  RADIOGRP_MONTH = 1;

// ADPUS
type
  PTExportADPUSRecord=^TExportADPUSRecord;
  TExportADPUSRecord=record
    CorporateCode: String;
    BatchID: String;
    FileNR: Integer;
    PayNR: Integer;
    TaxFrequency: String;
    TempDepartment: String;
    Shift: String;
    RegularHours: String; // Format: 2,5 is (2 hours 30 minutes)
    OvertimeHours: String;
    EarnHoursCode: String;
    Hours: String;
    EarnDollarsCode: String;
    Dollars: Double;
    TempRate: Double;
    MemoDays: Integer;
  end;

// RV082.16.
type
  PTExportCodeRecord=^TExportCodeRecord;
  TExportCodeRecord=record
    ExportCode: String;
    Minutes: Integer;
    Wage: Double;
  end;

type
  TAFASExportClass = class
  private
    FPeriod: String;
    FExportDate: String;
    FPeriodnumber: String;
    FBookyear: String;
    FCustomerNumber: Integer;
    FExportNormalHrs: Boolean;
    EmployeeList: TStringList;
    FExportWorkedDays: Boolean; // RV101.1.
  private
    function DetectExportCode(const AValue: String): String;
    function DetectDays(const AValue: String): Integer;
    function ExportLine(const AEmployeeNumber, ACustomerNumber: Integer;
      const ASumWKHrs: Integer; const AExportCode, AExportType: String): String;
    function EmployeeExists(const AEmployeeNumber: Integer): Boolean;
    procedure AddEmployee(const AEmployeeNumber: Integer);
    property Periodnumber: String read FPeriodnumber write FPeriodnumber;
    property Bookyear: String read FBookyear write FBookyear;
    property Period: String read FPeriod write FPeriod;
    property ExportDate: String read FExportDate write FExportDate;
    property CustomerNumber: Integer read FCustomerNumber write FCustomerNumber;
    property ExportNormalHrs: Boolean read FExportNormalHrs write FExportNormalHrs;
    property ExportWorkedDays: Boolean read FExportWorkedDays write FExportWorkedDays;
  end;

// 20014714
type
  PAbsenceReasonRec=^TAbsenceReasonRec;
  TAbsenceReasonRec=record
    Code: String;
    Minutes: Integer;
  end;

//   MaxLengthVarPart = 10;
type
  TDialogExportPayrollF = class(TDialogReportBaseF)
    RadioGroupSort: TRadioGroup;
    GroupBoxMonth: TGroupBox;
    GroupBoxPeriod: TGroupBox;
    GroupBoxPayDate: TGroupBox;
    GroupBoxATTENT: TGroupBox;
    GroupBoxYear: TGroupBox;
    Label10: TLabel;
    dxSpinEditMonth: TdxSpinEdit;
    LabelFrom: TLabel;
    LabelWeek: TLabel;
    LabelTo: TLabel;
    Label18: TLabel;
    dxSpinEditPeriodNr: TdxSpinEdit;
    dxSpinEditWeekMin: TdxSpinEdit;
    dxSpinEditWeekMax: TdxSpinEdit;
    dxSpinEditWKNumber: TdxSpinEdit;
    LabelWeekNr: TLabel;
    Label1: TLabel;
    DTPickerPayDate: TDateTimePicker;
    lblPeriod: TLabel;
    Label2: TLabel;
    DateTimePickerMin: TDateTimePicker;
    DateTimePickerMax: TDateTimePicker;
    CheckBoxExportHours: TCheckBox;
    CheckBoxExportCounters: TCheckBox;
    rgpAfasSelect: TRadioGroup;
    Label22: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    LblFromBU: TLabel;
    LblBU: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    lblToBU: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    QueryBU: TQuery;
    LblStarBUFrom: TLabel;
    LblStarBUTo: TLabel;
    LblFromContractGroup: TLabel;
    LblContractGroup: TLabel;
    ComboBoxPlusContractGroupFrom: TComboBoxPlus;
    LblToContractGroup: TLabel;
    ComboBoxPlusContractGroupTo: TComboBoxPlus;
    QueryContractGroup: TQuery;
    CheckBoxAllBusinessUnits: TCheckBox;
    CheckBoxAllContractGroups: TCheckBox;
    LblStarContractGroupFrom: TLabel;
    LblStarContractGroupTo: TLabel;
    BevelPlantDept: TBevel;
    GroupBoxEasyLabor: TGroupBox;
    Label3: TLabel;
    DTPickerTillDate: TDateTimePicker;
    CheckBoxIncludeOpenScans: TCheckBox;
    CheckBoxExportSwipes: TCheckBox;
    CheckBoxExportSchedule: TCheckBox;
    lblWeekPeriod: TLabel;
    GroupBoxFinalRun: TGroupBox;
    LblLastExportedTill: TLabel;
    EditFinalRunExportDate: TEdit;
    LblExportType: TLabel;
    EditExportType: TEdit;
    btnTestRun: TBitBtn;
    GroupBoxDateSelection: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DateFromDate: TDateTimePicker;
    DateToDate: TDateTimePicker;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RadioGroupSortClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ChangeDate(Sender: TObject);
    procedure tblPlantFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QueryEmplFromFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure QueryEmplToFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure CheckBoxAllContractGroupsClick(Sender: TObject);
    procedure CheckBoxAllBusinessUnitsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rgpAfasSelectClick(Sender: TObject);
    procedure CheckBoxAllPlantsClick(Sender: TObject);
    procedure btnTestRunClick(Sender: TObject);
    procedure DateFromDateCloseUp(Sender: TObject);
    procedure DateToDateCloseUp(Sender: TObject);
  private
    FExportType: TExportType;
    ExportADPUSList: TList;
    FMultiplyFactorHours: Integer;
    FMultiplyFactorDays: Integer;
    FPlantFrom: String;
    FPlantTo: String;
    FEmployeeFrom: Integer;
    FEmployeeTo: Integer;
    FCountryId: Integer;
    FIsADP_ATTENT: Boolean;
    FIsAFAS_SELECT: Boolean;
    FUseBU: Boolean;
    FUseContractGroup: Boolean;
    FContractGroupTo: String;
    FDepartmentFrom: String;
    FContractGroupFrom: String;
    FBusinessUnitTo: String;
    FDepartmentTo: String;
    FBusinessUnitFrom: String;
    FFinalRunExport: Boolean;
    FMyDateMin: TDateTime;
    FMyDateMax: TDateTime;
    FExportOK: Boolean;
    function SaveExportFile(const ExportList: TStringList;
      const ExportName: String; DialogFilter: String = '';
      DefaultExtension: String = ''): Boolean;
    procedure SetExportType(const Value: TExportType);
    procedure ExportCheckUserSelections;
    procedure ExportADP;
    procedure ExportADPUS;
    procedure ExportPARIS;
    procedure ExportSR;
    procedure ExportREINO;
    procedure ExportAFAS;
    procedure ExportSELECT;
    procedure ExportNTS;
    procedure ExportQBOOKS;
    procedure ExportPCHEX;
    procedure ExportNAVIS;
    procedure ExportBAMBOO;
    procedure ShowPeriod;
    procedure ShowWeekPeriod;
    function GetExportGuaranteedDays(Empl: Integer;
      DateMin, DateMax: TDateTime; WKDays, IllDays, HolDays, PaidDays,
      UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
      MatDays, LayDays, TravelDays: Integer): String;
    procedure ExportGuaranteedDays(Empl: Integer;
      DateMin, DateMax: TDateTime; WKDays, IllDays, HolDays, PaidDays,
      UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
      MatDays, LayDays, TravelDays: Integer;
      var ExportVarLine, ExportPart, ExportLine: String;
      var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
    procedure ExportATTENT;
    procedure ExportELA;
    procedure ExportWMU;
    procedure ExportCountersAttent(DateMin, DateMax: TDateTime);
    procedure CreateExportDataFileAttent;
    function DetermineExportType: TExportType;
    procedure DetermineDialogSettingsATTENT(AOnePlantSelected: Boolean);
    procedure FillBusinessUnit;
    procedure FillContractGroup;
    procedure ContractGroupVisible(AVis: Boolean);
    procedure ContractGroupSelectionVisible;
    procedure BusinessUnitVisible(AVis: Boolean);
    procedure BusinessUnitSelectionVisible;
    procedure HandleSelectionContractGroup;
    procedure HandleSelectionBusinessUnit;
    procedure HandleSelectionDepartment(APlantFrom, APlantTo: String);
    function GetEmployeeFrom: Integer;
    function GetEmployeeTo: Integer;
    function GetBusinessUnitFrom: String;
    function GetBusinessUnitTo: String;
    function GetContractGroupFrom: String;
    function GetContractGroupTo: String;
    function GetDepartmentFrom: String;
    function GetDepartmentTo: String;
    function GetPlantFrom: String;
    function GetPlantTo: String;
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }
    FExportList: TStringList;
    FNameExportFile: String;
    FTypeOfDialogExport: String;
    FPeriodNr: Integer;
    function FillZero(Value: String; Len: Integer): String;
    function FillSpaces(Value: String; Len: Integer): String;
    function CreateIDExportFile: Boolean;
    procedure CreateExportDataFile;
    procedure ProcessingData(var ExportPKLine: Integer);
    procedure ExportData(DateMin, DateMax: TDateTime; var ExportPKLine: Integer;
      MyEmployeeNumber: Integer);
    procedure ExportDataAttent(DateMin, DateMax: TDateTime);
    function  GetFixedEmplPart(Empl, LineNr, PeriodNr: Integer): String;
    procedure ProcessLine(var ExportVarLine, ExportPart,
      ExportLine: String; var LineNrEmpl: Integer; Empl, PeriodNr: Integer;
      var ExportPKLine: Integer);
    function GetExportAbsenceDays(CountryID, AbsDays: Integer; AbsType: String): String;
    function IsEmpl(AEmplHourType: TStringList; IndexHourType, Empl: Integer;
      var HourType, Min: Integer; var WAGE_Bonus_yn,
      Export_Code: String): Boolean;
    function Round5(Min: Integer):Integer;

    procedure ExportGuaranteedHrs(Empl, Min: Integer;
      ExportWageYN, WageSeparator: String; DateMin, DateMax: TDateTime;
      var ExportVarLine, ExportPart, ExportLine: String;
      var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
// FOR ADPUS - car 27-8-2003
    procedure ExportGuaranteedHrs_ADPUS(Empl, Min: Integer;
      ExportWageYN, WageSeparator: String; DateMin, DateMax: TDateTime);

    procedure ExportHourSick(Empl, HourTypeSick,
      HourSick: Integer; ExportWageYN: String; DateMin, DateMax: TDateTime;
      Wage_Separator: String;
      var ExportVarLine, ExportPart, ExportLine: String;
      var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
    procedure DetermineDateMinMax(
      var DateMin, DateMax: TDateTime);
    procedure ExportExtraPayment(Empl: Integer;
      var ExportVarLine, ExportPart, ExportLine: String;
      var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
    property ExportType: TExportType read FExportType write SetExportType;
    procedure NewExport_ADPUS(EmployeeNumber: Integer; PayDateStr:String;
      var AExportRecord: PTExportADPUSRecord);
    procedure ShowFinalRunExportDate;
    procedure ExportAction;
    function FinalRunExportDateOK: Boolean;
    property MultiplyFactorHours: Integer read FMultiplyFactorHours
      write FMultiplyFactorHours;
    property MultiplyFactorDays: Integer read FMultiplyFactorDays
      write FMultiplyFactorDays;
    property PlantFrom: String read GetPlantFrom write FPlantFrom;
    property PlantTo: String read GetPlantTo write FPlantTo;
    property EmployeeFrom: Integer read GetEmployeeFrom write FEmployeeFrom;
    property EmployeeTo: Integer read GetEmployeeTo write FEmployeeTo;
    property IsADP_ATTENT: Boolean read FIsADP_ATTENT write FIsADP_ATTENT;
    property IsAFAS_SELECT: Boolean read FIsAFAS_SELECT write FIsAFAS_SELECT;
    property UseBU: Boolean read FUseBU write FUseBU;
    property UseContractGroup: Boolean read FUseContractGroup write FUseContractGroup;
    property ContractGroupFrom: String read GetContractGroupFrom write FContractGroupFrom;
    property ContractGroupTo: String read GetContractGroupTo write FContractGroupTo;
    property BusinessUnitFrom: String read GetBusinessUnitFrom write FBusinessUnitFrom;
    property BusinessUnitTo: String read GetBusinessUnitTo write FBusinessUnitTo;
    property DepartmentFrom: String read GetDepartmentFrom write FDepartmentFrom;
    property DepartmentTo: String read GetDepartmentTo write FDepartmentTo;
    property FinalRunExport: Boolean read FFinalRunExport write FFinalRunExport;
    property MyDateMin: TDateTime read FMyDateMin write FMyDateMin;
    property MyDateMax: TDateTime read FMyDateMax write FMyDateMax;
    property ExportOK: Boolean read FExportOK write FExportOK;
  end;

var
  DialogExportPayrollF: TDialogExportPayrollF;
  AFASExportClass: TAFASExportClass;

function MinCnv(Minutes: Integer): String;
function RoundMinutes(RoundMinute: Integer; Minutes: Integer): Integer;
function MinHundrethsCnv(Minutes: Integer; AMySep: String=''): String;
function DateMDYCnv(ADate: TDateTime): String;

// RV089.1.
function DialogExportPayrollForm: TDialogExportPayrollF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes, ListProcsFRM,
  ReportExportPayrollQRPT, CalculateTotalHoursDMT,
  ReportExportPayrollSRQRPT, GlobalDMT,
  UScannedIDCard;

// RV089.1.
var
  DialogExportPayrollF_HND: TDialogExportPayrollF;

// RV089.1.
function DialogExportPayrollForm: TDialogExportPayrollF;
begin
  if (DialogExportPayrollF_HND = nil) then
  begin
    DialogExportPayrollF_HND := TDialogExportPayrollF.Create(Application);
    with DialogExportPayrollF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogExportPayrollF_HND;
end;

// RV089.1.
procedure TDialogExportPayrollF.FormDestroy(Sender: TObject);
begin
  inherited;
  FExportList.Free;
  DialogExportPayrollDM.Free;
  if (DialogExportPayrollF_HND <> nil) then
  begin
    DialogExportPayrollF_HND := nil;
  end;
end;

function MinCnv(Minutes: Integer): String;
begin
  Result := Format('%.2f', [Minutes / 60.0]);
end;

function RoundMinutes(RoundMinute: Integer;
  Minutes: Integer): Integer;
var
  Hours, Mins: Integer;
begin
  Hours := Minutes DIV 60;
  Mins := Minutes MOD 60;
  Mins := Round(Mins / RoundMinute) * RoundMinute;
  Result := Hours * 60 + Mins;
end;

function MinHundrethsCnv(Minutes: Integer; AMySep: String=''): String;
  function MySep: String;
  begin
    if AMySep <> '' then
      Result := AMySep
    else
      if Pos('.', Format('%.2f', [1.5])) > 0 then
        Result := '.'
      else
        Result := ',';
  end;
begin
  Result := '';
  if (Minutes div 60) = 0 then
    Result := '0'
  else
    Result := IntToStr(Minutes div 60);
  if (Minutes mod 60) > 0 then
    Result := Result + MySep + Format('%.2d', [(Round((Minutes mod 60) * 100 / 60))]);
end;

function DateMDYCnv(ADate: TDateTime): String;
var
  Day, Month, Year: Word;
begin
  DecodeDate(ADate, Year, Month, Day);
//  Format('%.*d,[length, number])
  Result := Format('%.2d/%.2d/%.4d', [Month, Day, Year]);
end;

function TDialogExportPayrollF.FillZero(Value: String; Len: Integer): String;
begin
  Result := Trim(Value);
  while Length(Result) < Len do
    Result := '0' + Result;
end;

function TDialogExportPayrollF.FillSpaces(Value: String; Len: Integer): String;
begin
  Result := Trim(Value);
  while Length(Result) < Len do
    Result := Result + ' ';
end;

// Check UserSelections
procedure TDialogExportPayrollF.ExportCheckUserSelections;
var
  Year, Week: Word;
  PrevMonth, Month, Day: Word;
  PrevWeek, PrevWeekMin, PrevWeekMax: Word;
begin
  ListProcsF.WeekUitDat(Now, Year, Week);
  DecodeDate(Now, Year, Month, Day);

  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);

  FCountryId := DialogExportPayRollDM.GetCountryId(PlantFrom, PlantTo);
  if FCountryId = -1 then // RV086.4.
    raise Exception.Create(SPimsSelPlantsCountry);

  DialogExportPayRollDM.UpdateHourTypePerCountry(FCountryId);

  if PlantFrom = PlantTo then
  begin
    EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
    EmployeeTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
  end
  else
  begin
    // RV050.8.
    if SystemDM.PlantTeamFilterOn then
    begin
      try
        QueryEmplFrom.First;
        EmployeeFrom := QueryEmplFrom.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        QueryEmplFrom.Last;
        EmployeeTo := QueryEmplFrom.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      except
        EmployeeFrom := 0;
        EmployeeTo := 999999999;
      end;
    end
    else
    begin
      EmployeeFrom := 0;
      EmployeeTo := 999999999;
    end;
  end;

  // PIM-108
  if GroupBoxDateSelection.Visible then
    Exit;

  if (ExportType = etATTENT) then
  begin
    if DateTimePickerMin.DateTime > DateTimePickerMax.DateTime then
    begin
      DisplayMessage(SPimsStartEndActiveDate, mtInformation, [mbYes]);
      Exit;
    end;
  end
  else
  begin
    if (Radiogroupsort.ItemIndex = 0) then
      if (dxSpinEditWeekMin.Value > dxSpinEditWeekMax.Value) then
      begin
        DisplayMessage(SPimsWeekStartEnd, mtInformation, [mbYes]);
        Exit;
      end;

  //car 18-07-2003
    if DialogExportPayrollDM.TableEP.FieldByNAME('WEEK_PERIOD_MONTH').AsString = '' then
    begin
     if RadioGroupSort.ItemIndex = 0 then
       FTypeOfDialogExport := EXPORTPAYROLL_PERIOD
     else
       FTypeOfDialogExport := EXPORTPAYROLL_MONTH;
    end;

    if FTypeOfDialogExport = EXPORTPAYROLL_WEEK then
    begin
      if Week > 1 then
        PrevWeek := Week - 1
      else
        PrevWeek := Week;
      if (dxSpinEditWKNumber.IntValue <> PrevWeek) then
      begin
        if DisplayMessage(SNotDefaultSelection, mtConfirmation, [mbYes, mbNo])
         <> mrYes then
         Abort;
      end;
      FPeriodNr := dxSpinEditPeriodNr.IntValue;
      dxSpinEditWeekMin.Value := dxSpinEditWKNumber.Value;
      dxSpinEditWeekMax.Value := dxSpinEditWKNumber.Value;
    end;
    if FTypeOfDialogExport = EXPORTPAYROLL_PERIOD then
    begin
      if Week > 4 then
      begin
        PrevWeekMin := Week - 4;
        PrevWeekMax := Week - 1;
      end
      else
      begin
        PrevWeekMin := 1;
        PrevWeekMax := 4;
      end;
      if (dxSpinEditWeekMin.IntValue <> PrevWeekMin) or
        (dxSpinEditWeekMax.IntValue <> PrevWeekMax) then
      begin
        if DisplayMessage(SNotDefaultSelection, mtConfirmation, [mbYes, mbNo])
         <> mrYes then
         Abort;
      end;
      FPeriodNr := dxSpinEditPeriodNr.IntValue;
    end;
    if FTypeOfDialogExport = EXPORTPAYROLL_MONTH then
    begin
      if Month > 1 then
        PrevMonth := Month -1
      else
        PrevMonth := Month;
      if (dxSpinEditMonth.IntValue <> PrevMonth)then
      begin
        if DisplayMessage(SNotDefaultSelection, mtConfirmation, [mbYes, mbNo])
         <> mrYes then
         Abort;
      end;
      FPeriodNr := dxSpinEditMonth.IntValue;
    end;
    if (FTypeOfDialogExport <> EXPORTPAYROLL_MONTH) and
       (FTypeOfDialogExport <> EXPORTPAYROLL_PERIOD) and
       (FTypeOfDialogExport <> EXPORTPAYROLL_WEEK) then
    begin
      if RadioGroupSort.ItemIndex = 0 then
        FPeriodNr := dxSpinEditPeriodNr.IntValue
      else
        FPeriodNr := dxSpinEditMonth.IntValue;
    end;
  end;//end exporttype <> etATTENT
 // PeriodIsStored := False;
  if ExportType <> etWMU then
    if FPeriodNr > 0 then
      if DialogExportPayrollDM.CheckIfStorePeriod(dxSpinEditYear.IntValue,
          FPeriodNr,
          PlantFrom, PlantTo,
          EmployeeFrom, EmployeeTo) then
       begin
       //  PeriodIsStored := True;
          if DisplayMessage(SPeriodAlreadyExported, mtConfirmation, [mbYes, mbNo])
           <> mrYes then
           Abort;
       end;
end;

//Procedure create a new record
procedure TDialogExportPayrollF.NewExport_ADPUS(EmployeeNumber: Integer;
  PayDateStr:String; var AExportRecord: PTExportADPUSRecord);
begin
  New(AExportRecord);
  AExportRecord.CorporateCode :=
    DialogExportPayRollDM.TableEP.FieldByName('CORPORATE_CODE').AsString;
  AExportRecord.BatchID :=
    DialogExportPayRollDM.TableEP.FieldByName('BATCH_ID').AsString + PayDateStr;
  AExportRecord.FileNR := EmployeeNumber;
  AExportRecord.PayNR := 0;
  AExportRecord.TaxFrequency := '';
  AExportRecord.TempDepartment := '';
  AExportRecord.Shift := '';
  AExportRecord.RegularHours := '';
  AExportRecord.OvertimeHours := '';
  AExportRecord.EarnHoursCode := '';
  AExportRecord.Hours := '';
  AExportRecord.EarnDollarsCode := '';
  AExportRecord.Dollars := 0;
  AExportRecord.TempRate := 0;
  AExportRecord.MemoDays := 0;
end;

// Export for 'ADPUS'.
procedure TDialogExportPayrollF.ExportADPUS;
var
  AExportRecord: PTExportADPUSRecord;
  I, IPos: Integer;
  Line: String;
  PayDateStr: String;
  Year, Month, Day: Word;
  DateMin, DateMax: TDateTime;
  CurrentEmployeeNumber: Integer;
  ExportPKLine: Integer;
  MyIndex, IndexHourType: Integer;
  HourType, Minutes, Hours: Integer;
  ExportCode, Wage_Bonus_YN: String;
  StrTmp: String;
  Amount, Wage: Double;
  MinExported: Integer;
  ExportCodeDummy: String;

(*  function MinCnv(Minutes: Integer): String;
  begin
    Result := Format('%.2f', [Minutes / 60.0]);
  end; *)
begin
  DialogExportPayrollDM.CurrentExportType := etADPUS;
  // Check Selections
  ExportCheckUserSelections;

  // Create the list
  ExportADPUSList := TList.Create;
  // Convert Paydate to a string in format 'MMDDYY'
  DecodeDate(DTPickerPayDate.Date, Year, Month, Day);
  PayDateStr := FillZero(IntToStr(Month), 2) + FillZero(IntToStr(Day), 2) +
    FillZero(Copy(IntToStr(Year), 3, 2), 2);
   // Get period
  DetermineDateMinMax(DateMin, DateMax);
  // Now fill the list
  ProcessingData(ExportPKLine);

  with DialogExportPayRollDM do
  begin
    QueryPRODHOURPEPT.Close;
    QueryPRODHOURPEPT.ParamByName('PLANTFROM').AsString := PlantFrom;
    QueryPRODHOURPEPT.ParamByName('PLANTTO').AsString := PlantTo;
    QueryPRODHOURPEPT.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    QueryPRODHOURPEPT.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    QueryPRODHOURPEPT.ParamByName('DATEMIN').AsDate := DateMin;
    QueryPRODHOURPEPT.ParamByName('DATEMAX').AsDate := DateMax;
    QueryPRODHOURPEPT.Open;
    QueryPRODHOURPEPT.First;
    cdsEmployeeADPUSData.First;
    while not cdsEmployeeADPUSData.Eof do
    begin
      CurrentEmployeeNumber :=
        cdsEmployeeADPUSData.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      MinExported := 0;
      while not QueryPRODHOURPEPT.Eof and
        (CurrentEmployeeNumber =
         QueryPRODHOURPEPT.FieldByName('EMPLOYEE_NUMBER').AsInteger) do
      begin
      // Get regular / overtime hours
        NewExport_ADPUS(QueryPRODHOURPEPT.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          PayDateStr, AExportRecord);

        AExportRecord.TempDepartment :=
          QueryPRODHOURPEPT.FieldByName('DEPARTMENT_CODE').AsString;
        AExportRecord.Shift := '';
        if QueryPRODHOURPEPT.FieldByName('SHIFT_NUMBER').AsString <> '1' then
          AExportRecord.Shift :=
            QueryPRODHOURPEPT.FieldByName('SHIFT_NUMBER').AsString;
      // Fill Hours
        Wage := 0;
        if DialogExportPayrollDM.TableEP.
          FieldByName('EXPORT_WAGE_YN').AsString = CHECKEDVALUE then
          DialogExportPayrollDM.GetWage(CurrentEmployeeNumber,
          QueryPRODHOURPEPT.FieldByName('HOURTYPE_NUMBER').AsInteger,
          DateMin, DateMax, Wage);

        if QueryPRODHOURPEPT.FieldByName('HOURTYPE_NUMBER').AsInteger = 1 then
        begin
          AExportRecord.RegularHours :=
            MinCnv(Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat));
          MinExported := MinExported +
            Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat);
        end
        else
        begin
          if QueryPRODHOURPEPT.FieldByName('OVERTIME_YN').AsString = 'Y' then
          begin
            AExportRecord.OvertimeHours :=
              MinCnv(Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat));
            MinExported := MinExported +
              Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat);
          end
          else
          begin
            AExportRecord.EarnHoursCode :=
              QueryPRODHOURPEPT.FieldByName('EXPORT_CODE').AsString;
            AExportRecord.Hours :=
              MinCnv(Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat));
            MinExported := MinExported +
              Round(QueryPRODHOURPEPT.FieldByName('SUMPRODUCTION_MINUTE').AsFloat);
          end;
        end;
        if Wage <> 0 then
          AExportRecord.TempRate := Wage;
        ExportADPUSList.Add(AExportRecord);
        QueryPRODHOURPEPT.Next;
      end;
      // At end of found records for 1 employee!
      if (CurrentEmployeeNumber <>
        QueryPRODHOURPEPT.FieldByName('EMPLOYEE_NUMBER').AsInteger) or
        QueryPRODHOURPEPT.Eof then
      begin
        for IndexHourType := 0 to FEmplHourTypeAbsence.Count - 1 do
        begin
          if (IndexHourType mod 2) = 0 then
          begin
            if IsEmpl(FEmplHourTypeAbsence, IndexHourType,
              CurrentEmployeeNumber,
              HourType, Minutes, Wage_Bonus_YN, ExportCodeDummy) then
            begin
              Wage := 0;
              if HourTypeFind(HourType) {ClientDataSetHourType.FindKey([HourType])} then
              begin
                ExportCode := ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;
                if DialogExportPayrollDM.TableEP.
                  FieldByName('EXPORT_WAGE_YN').AsString = CHECKEDVALUE then
                  DialogExportPayrollDM.GetWage(CurrentEmployeeNumber,
                  HourType, DateMin, DateMax, Wage);
              end
              else
                ExportCode := '';
              if ExportCode <> '' then
              begin
                NewExport_ADPUS(CurrentEmployeeNumber, PayDateStr, AExportRecord);
                AExportRecord.EarnHoursCode := ExportCode;
                AExportRecord.Hours := MinCnv(Minutes);
                MinExported := MinExported +  Minutes;
                if Wage <> 0 then
                  AExportRecord.TempRate := Wage;

                ExportADPUSList.Add(AExportRecord);
              end;
            end;
          end;
        end;
        //
        // Extra Payment days
        //
        MyIndex :=
          FEmpExtraPayment.IndexOf(IntToStr(CurrentEmployeeNumber));
        if  (MyIndex >= 0) and
          ((MyIndex + 1) <= (FEmpExtraPayment.Count -1)) then
        begin
          StrTmp := FEmpExtraPayment.Strings[MyIndex + 1];
          inc(MyIndex);
          while (pos(Chr_SEP, StrTmp) > 0) do
          begin
            ExportCode := Copy(StrTmp, 0, Pos(CHR_SEP, StrTmp) -1);
            Amount := StrToFloat(Copy(StrTmp, Pos(CHR_SEP, StrTmp) + 1, 40));
            if ExportCode <> '' then
            begin
              NewExport_ADPUS(CurrentEmployeeNumber, PayDateStr, AExportRecord);
              AExportRecord.EarnDollarsCode := ExportCode;
              AExportRecord.Dollars := Amount;
              ExportADPUSList.Add(AExportRecord);
            end;
            inc(MyIndex);
            if MyIndex >= FEmpExtraPayment.Count then
              Break;
            StrTmp := FEmpExtraPayment.Strings[MyIndex];
          end;
        end;
        //
        // Sick Pay days
        //
        MyIndex := FEmpHourSick.IndexOf(IntToStr(CurrentEmployeeNumber));
        if (MyIndex >= 0) and
          ((MyIndex + 1) <= (FEmpHourSick.Count -1)) then
        begin
          StrTmp := FEmpHourSick.Strings[MyIndex + 1];
          inc(MyIndex);
          while (pos(Chr_SEP, StrTmp) > 0) do
          begin
            HourType := StrToInt(Copy(StrTmp, 0, Pos(CHR_SEP, StrTmp) -1));
            Hours := StrToInt(Copy(StrTmp, Pos(CHR_SEP, StrTmp) + 1, 40));
            Wage := 0;
            if HourTypeFind(HourType)
              {ClientDataSetHourType.Locate('HOURTYPE_NUMBER', HourType, [])} then
            begin
              ExportCode := ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;
              if DialogExportPayrollDM.TableEP.
                FieldByName('EXPORT_WAGE_YN').AsString = CHECKEDVALUE then
                DialogExportPayrollDM.GetWage(CurrentEmployeeNumber,
                HourType, DateMin, DateMax, Wage);
            end
            else
              ExportCode := '';
            if ExportCode <> '' then
            begin
              NewExport_ADPUS(CurrentEmployeeNumber,PayDateStr, AExportRecord);
              AExportRecord.EarnHoursCode := ExportCode;
              AExportRecord.Hours := IntToStr(Hours);
              if Wage <> 0 then
                AExportRecord.TempRate := Wage;
              ExportADPUSList.Add(AExportRecord);
            end;
            inc(MyIndex);
            if MyIndex >= FEmpHourSick.Count then
              Break;
            StrTmp := FEmpHourSick.Strings[MyIndex];
          end;
        end;
        //
        // Guaranteed Hours
        //
        ExportGuaranteedHrs_ADPUS(CurrentEmployeeNumber, MinExported,
          DialogExportPayrollDM.TableEP.FieldByName('EXPORT_WAGE_YN').AsString,
          DialogExportPayrollDM.TableEP.FieldByName('WAGE_SEPARATOR').AsString,
          DateMin, DateMax);
        MyIndex := FEmpQuaranteedHrs.IndexOf(IntToStr(CurrentEmployeeNumber));
        if (MyIndex >= 0) and
          (MyIndex <= FEmpQuaranteedHrs.Count - 2) then
        begin
          IPos := Pos(' ', FEmpQuaranteedHrs.Strings[MyIndex + 1]);
          HourType := StrToInt(Copy(FEmpQuaranteedHrs.Strings[MyIndex+ 1],
            0, IPos - 1));
          Minutes :=  StrToInt(Copy(FEmpQuaranteedHrs.Strings[MyIndex + 1],
            IPos + 1, 40));
          Wage := 0;
          if HourTypeFind(HourType)
            {ClientDataSetHourType.FindKey([HourType])} then
          begin
            ExportCode := ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;
            if DialogExportPayrollDM.TableEP.
              FieldByName('EXPORT_WAGE_YN').AsString = CHECKEDVALUE then
              DialogExportPayrollDM.GetWage(CurrentEmployeeNumber,
                HourType, DateMin, DateMax, Wage);
          end
          else
            ExportCode := '';
          if ExportCode <> '' then
          begin
            NewExport_ADPUS(CurrentEmployeeNumber, PayDateStr, AExportRecord);
            if Wage <> 0 then
              AExportRecord.TempRate := Wage;
            AExportRecord.EarnHoursCode := ExportCode;
            AExportRecord.Hours := MinCnv(Minutes);
            ExportADPUSList.Add(AExportRecord);
          end;
        end;
        //
        // Payments for Worked Days
        //
        if (cdsEmployeeADPUSData.FieldByName('WKDAYS').AsInteger > 0) then
        begin
          NewExport_ADPUS(CurrentEmployeeNumber, PayDateStr,AExportRecord);
          AExportRecord.EarnDollarsCode :=
            TableEP.FieldByName('EXPORT_CODE_DAYS').AsString;
          if TableEP.FieldByName('DAYS_PAY').AsString <> '' then
            AExportRecord.Dollars :=
              cdsEmployeeADPUSData.FieldByName('WKDAYS').AsInteger *
                TableEP.FieldByName('DAYS_PAY').AsFloat;
          ExportADPUSList.Add(AExportRecord);
        end;
        //
        if (cdsEmployeeADPUSData.FieldByName('TOTALDAYS').AsInteger > 0) then
        begin
          NewExport_ADPUS(CurrentEmployeeNumber, PayDateStr, AExportRecord);
          AExportRecord.MemoDays :=
            cdsEmployeeADPUSData.FieldByName('TOTALDAYS').AsInteger;
          ExportADPUSList.Add(AExportRecord);
        end;
      end;
      cdsEmployeeADPUSData.Next;
    end; // while not cdsEmployeeADPUSData.Eof do

    QueryPRODHOURPEPT.Close;
  end;

  // Create standard export data file
  FExportList.Clear;

  // Convert record-list to a separator-delimited string-list
  // Write to excel-file
  FExportList.Clear;
  if ExportADPUSList.Count > 0 then
  begin
    // First make a header
    Line :=
      Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', [
        'Corporate Code', MySep,
        'Batch ID', MySep,
        'File #', MySep,
        'Pay #', MySep,
        'Tax Frequency', MySep,
        'Temp department', MySep,
        'Shift', MySep,
        'Reg hours', MySep,
        'Overtime hours', MySep,
        'Earn Code hours', MySep,
        'Hours', MySep,
        'Earn Code $', MySep,
        '$', MySep,
        'Temp rate', MySep,
        'Memo days'
        ]);
    FExportList.Add(Line);
    for I := 0 to ExportADPUSList.Count - 1 do
    begin
      AExportRecord := ExportADPUSList.Items[I];
      Line :=
        Format('%s%s%s%s%d%s%d%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%f%s%f%s%d', [
          AExportRecord.CorporateCode, MySep,
          AExportRecord.BatchID, MySep,
          AExportRecord.FileNR, MySep,
          AExportRecord.PayNR, MySep,
          AExportRecord.TaxFrequency, MySep,
          AExportRecord.TempDepartment, MySep,
          AExportRecord.Shift, MySep,
          AExportRecord.RegularHours, MySep,
          AExportRecord.OvertimeHours, MySep,
          AExportRecord.EarnHoursCode, MySep,
          AExportRecord.Hours, MySep,
          AExportRecord.EarnDollarsCode, MySep,
          AExportRecord.Dollars, MySep,
          AExportRecord.TempRate, MySep,
          AExportRecord.MemoDays]);
      FExportList.Add(Line);
    end;
    SaveExportFile(FExportList, ExportADPUSFileName);
  end
  else
    DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);

  // Now clear the list.
  for I := ExportADPUSList.Count - 1 downto 0 do
  begin
    AExportRecord := ExportADPUSList.Items[I];
    Dispose(AExportRecord);
    ExportADPUSList.Remove(AExportRecord);
  end;
  ExportADPUSList.Clear;
  ExportADPUSList.Free;

  DialogExportPayrollDM.StorePeriod(dxSpinEditYear.IntValue,
    FPeriodNr,
    PlantFrom, PlantTo,
    EmployeeFrom, EmployeeTo);
end; // ExportADPUS

//CAR 550299
// Export for 'PARIS'.
// MRA: 13-OCT-2008
// Additions:
// - Also export BusinessUnit Code and Department Code of Employee.
// - Also export Exceptional Hours.
procedure TDialogExportPayrollF.ExportPARIS;
var
  ExportLine: String;
  Employee_Number, Shift_Number, PHE_Minute, Grade_Empl, Grade_WK,
  Upgrade, ExportMinutes: Integer;
  DateMin, DateMax: TDateTime;
  Export_Code, Overtime_YN, Plant_Code, WK_Code: String;
  BusinessUnitCode, DepartmentCode: String;
  ExceptionalHoursIndicator: String;
  procedure FillClientDataSet_PARIS(AEmployee_Number, AAmount_Hrs: Integer;
    ABusinessUnitCode, ADepartmentCode: String;
    AExport_Code: String);
  begin
    if DialogExportPayRollDM.ClientDataSetEmployeePARIS.FindKey([
      AEmployee_Number, AExport_Code]) then
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.Edit
    else
    begin
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.Insert;
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('EMPLOYEE_NUMBER').AsInteger := AEmployee_Number;
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('BUSINESSUNIT_CODE').AsString := ABusinessUnitCode;
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('DEPARTMENT_CODE').AsString := ADepartmentCode;
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('EXPORT_CODE').AsString := AExport_Code;
      DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('AMOUNT_HRS').AsInteger := 0;
    end;
    DialogExportPayRollDM.ClientDataSetEmployeePARIS.
      FieldByName('AMOUNT_HRS').AsInteger :=
        DialogExportPayRollDM.ClientDataSetEmployeePARIS.
        FieldByName('AMOUNT_HRS').AsInteger + AAmount_Hrs;
    DialogExportPayRollDM.ClientDataSetEmployeePARIS.Post;
  end;
(*  function MinCnv(Minutes: Integer): String;
  begin
    Result := Format('%.2f', [Minutes / 60.0]);
  end; *)
  // MR:06-09-2005 Order 550408 Round minutes to 15.
(*  function RoundMinutes(RoundMinute: Integer;
    Minutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    Hours := Minutes DIV 60;
    Mins := Minutes MOD 60;
    Mins := Round(Mins / RoundMinute) * RoundMinute;
    Result := Hours * 60 + Mins;
  end; *)
  procedure DetermineGrade;
  begin
    with DialogExportPayRollDM do
    begin
      if ClientDataSetEmpl.FindKey([Employee_Number]) then
        Grade_Empl := ClientDataSetEmpl.FieldByName('GRADE').AsInteger;
      if ClientDataSetWK.FindKey([Plant_code, WK_Code]) then
        Grade_WK := ClientDataSetWK.FieldByName('GRADE').AsInteger;
      Upgrade := 0;
      if Grade_WK > Grade_Empl then
      begin
        Upgrade := Grade_WK - Grade_Empl;

{$IFDEF DEBUG1}
WDebugLog('Upgrade found. G-WK=' + IntToStr(Grade_WK) +
  ' G-Emp=' + IntToStr(Grade_Empl) +
  ' Upg=' + IntToStr(Upgrade)
  );
{$ENDIF}

      end;
      Export_Code := '';
      if ClientDataSetGradeExpCode.FindKey([Shift_Number, Overtime_YN, Upgrade]) then
        Export_Code := ClientDataSetGradeExpCode.
          FieldByName('EXPORT_CODE').AsString;

{$IFDEF DEBUG1}
if Export_Code = '' then
  WDebugLog('NO EXPORT CODE FOUND!');
{$ENDIF}

      if Export_Code <> '' then
      begin

{$IFDEF DEBUG1}
WDebugLog(Format('-> Emp=%-5s PHE_Min=%-5s EC=%-5s',
  [IntToStr(Employee_Number),IntToStr(PHE_Minute),Export_Code])
  );
{$ENDIF}

        FillClientDataSet_PARIS(Employee_Number, PHE_Minute,
          BusinessUnitCode, DepartmentCode, Export_Code);
      end;
    end;
  end;
begin
  // Check Selections
  FExportList.Clear;
  ExportCheckUserSelections;

   // Get period
  DetermineDateMinMax(DateMin, DateMax);

  DialogExportPayrollDM.DeleteEPayroll(DateMin, DateMax);

  FNameExportFile := 'PERIOD' + FillZero(IntToStr(FPeriodNr), 2) + '.XLS';
  //add header line for excel file
  ExportLine := IntToStr(FPeriodNr) + MySep +
    DialogExportPayrollDM.TableEP.FieldByName('CORPORATE_CODE').AsString;
  FExportList.Add(ExportLine);
  DialogExportPayrollDM.GetValidEmpl(PlantFrom, PlantTo,
    IntToStr(EmployeeFrom), IntToStr(EmployeeTo));

// fill hours amount from ProdHourPerEmplType

{$IFDEF DEBUG1}
WDebugLog('--- Export Payroll Paris - Start ---'
  );
{$ENDIF}
  with DialogExportPayRollDM do
  begin
    ClientDataSetEmployeePARIS.EmptyDataSet;
    ClientDataSetWK.Open;
    ClientDataSetGradeExpCode.Open;
    QueryPHETYPE_PARIS.Close;
    QueryPHETYPE_PARIS.ParamByName('PLANTFROM').AsString := PlantFrom;
    QueryPHETYPE_PARIS.ParamByName('PLANTTO').AsString := PlantTo;
    QueryPHETYPE_PARIS.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    QueryPHETYPE_PARIS.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    QueryPHETYPE_PARIS.ParamByName('DATEMIN').AsDate := DateMin;
    QueryPHETYPE_PARIS.ParamByName('DATEMAX').AsDate := DateMax;
    QueryPHETYPE_PARIS.Open;
    while not QueryPHETYPE_PARIS.Eof do
    begin
      Employee_Number :=
        QueryPHETYPE_PARIS.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      BusinessUnitCode :=
        QueryPHETYPE_PARIS.FieldByName('BUSINESSUNIT_CODE').AsString;
      DepartmentCode :=
        QueryPHETYPE_PARIS.FieldByName('DEPARTMENT_CODE').AsString;
      WK_Code :=
        QueryPHETYPE_PARIS.FieldByName('WORKSPOT_CODE').AsString;
//      Shift_Number := QueryPHETYPE_PARIS.FieldByName('SHIFT_NUMBER').AsInteger;
      Overtime_YN := QueryPHETYPE_PARIS.FieldByName('OVERTIME_YN').AsString;
      Plant_code := QueryPHETYPE_PARIS.FieldByName('PLANT_CODE').AsString;
      PHE_Minute := Round(QueryPHETYPE_PARIS.FieldByName('SUM_MIN').AsFloat);
      ExceptionalHoursIndicator :=
        QueryPHETYPE_PARIS.FieldByName('EH').AsString;
      Export_Code := QueryPHETYPE_PARIS.FieldByName('EXPORT_CODE').AsString;
      Grade_Empl := 1;
      Grade_WK := 1;
      // MRA:22-OCT-2008 RV011. Use Grade-update also for Except. hours.
      // MRA:08-DEC-2008 RV016. Set the Shift_number based on 'exceptional
      // hours' used or not.
      // Shift=1 for 'no exceptinal hours'.
      // Shift=2 for 'yes exceptional hours'.

      // Determine type of hours:
      // -  Exceptional hours
      // -  Combined (Exceptional + Overtime)
      // -  Other
      case ExceptionalHoursIndicator[1] of
      'E':
        begin
          // RV029. Make exception when hourtype that comes from
          //        'exceptional hours' is of type 'Overtime'.
          if Overtime_YN = 'N' then
          begin
            Shift_Number := 2;
            Overtime_YN := 'N';
          end
          else
            Shift_Number := 1;
        end;
      'C':
        begin
          Shift_Number := 2;
          Overtime_YN := 'Y';
        end;
      else
        Shift_Number := 1;
      end;
(*
      if ExceptionalHoursIndicator = 'E' then
      begin
        Shift_Number := 2;
        Overtime_YN := 'N';
      end
      else
        if ExceptionalHoursIndicator = 'C' then
        begin
          Shift_Number := 2;
          Overtime_YN := 'Y';
        end
        else
          Shift_Number := 1;
*)
      DetermineGrade;

      QueryPHETYPE_PARIS.Next;
    end;
    QueryAHE_PARIS.Close;
    QueryAHE_PARIS.ParamByName('PLANTFROM').AsString := PlantFrom;
    QueryAHE_PARIS.ParamByName('PLANTTO').AsString := PlantTo;
    QueryAHE_PARIS.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    QueryAHE_PARIS.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    QueryAHE_PARIS.ParamByName('DATEMIN').AsDate := DateMin;
    QueryAHE_PARIS.ParamByName('DATEMAX').AsDate := DateMax;
    QueryAHE_PARIS.Open;
    while not QueryAHE_PARIS.Eof do
    begin
      Employee_Number := QueryAHE_PARIS.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      BusinessUnitCode := QueryAHE_PARIS.FieldByName('BUSINESSUNIT_CODE').AsString;
      DepartmentCode := QueryAHE_PARIS.FieldByName('DEPARTMENT_CODE').AsString;
      Export_Code := QueryAHE_PARIS.FieldByName('EXPORT_CODE').AsString;
      PHE_Minute := Round(QueryAHE_PARIS.FieldByName('SUM_MIN').AsFloat);
      FillClientDataSet_PARIS(Employee_Number, PHE_Minute,
        BusinessUnitCode, DepartmentCode, Export_Code);
      QueryAHE_PARIS.Next;
    end;

    ClientDataSetEmployeePARIS.First;
    while not ClientDataSetEmployeePARIS.Eof do
    begin
      // MR:06-09-2005 Order 550408 Round minutes to 15.
      ExportMinutes := RoundMinutes(15,
        ClientDataSetEmployeePARIS.FieldByName('AMOUNT_HRS').AsInteger);
      // MR:06-09-2005 Order 550408 Don't export zero minutes.
      // MR:04-MAR-2009 RV023. Hours can also be negative!
      if ExportMinutes <> 0 then
      begin
        ExportLine :=
          ClientDataSetEmployeePARIS.
            FieldByName('EMPLOYEE_NUMBER').AsString + MySep +
          ClientDataSetEmployeePARIS.
            FieldByName('BUSINESSUNIT_CODE').AsString + MySep +
          ClientDataSetEmployeePARIS.
            FieldByName('DEPARTMENT_CODE').AsString + MySep +
          ClientDataSetEmployeePARIS.
            FieldByName('EXPORT_CODE').AsString + MySep +
          MinCnv(ExportMinutes
            {ClientDataSetEmployeePARIS.FieldByName('AMOUNT_HRS').AsInteger}
            );
        FExportList.Add(ExportLine);

        // MRA:04-MAR-2009 RV023. Store results in table, only for easy checking.
        InsertEPayroll(DateMin, DateMax,
          ClientDataSetEmployeePARIS.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          ClientDataSetEmployeePARIS.FieldByName('BUSINESSUNIT_CODE').AsString,
          ClientDataSetEmployeePARIS.FieldByName('DEPARTMENT_CODE').AsString,
          ClientDataSetEmployeePARIS.FieldByName('EXPORT_CODE').AsString,
          ExportMinutes);
      end;
      ClientDataSetEmployeePARIS.Next;
    end;
  end;// with
  SaveExportFile(FExportList, FNameExportFile);
  DialogExportPayrollDM.StorePeriod(dxSpinEditYear.IntValue,
    FPeriodNr, PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
end; // ExportPARIS

function TDialogExportPayrollF.DetermineExportType: TExportType;
var
  SqlCount, ExpType: String;
  ExportCount: Integer;
  SqlExportType: String; // RV067.MRA.30
const // RV067.MRA.30 This variable renamed.
  SqlExportTypeConst: String =
    ' select distinct c.export_type ' +
    ' from ' +
    '   plant p, country c ' +
    ' where ' +
    '   p.country_id = c.country_id and ' +
    '   (p.plant_code >= ''%s'') and ' +
    '   (p.plant_code <= ''%s'') and ' +
    '   nvl(c.export_type, ''*'') <> ''*'' ';

begin
  Result := DialogExportPayRollDM.DetermineExportType;
  // 20011800 Use PlantFrom and PlantTo instead of PltFrom and PltTo.
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  PlantTo := GetStrValue(CmbPlusPlantTo.Value);

  SqlExportType := Format(SqlExportTypeConst, [PlantFrom, PlantTo]);
  SqlCount := 'select count(*) from (' + SqlExportType + ')';

  ExportCount := SystemDM.GetDBValue(SqlCount, 0);

  if ExportCount <> 0 then // If 0 then use default export type from Result.
  begin
    if ExportCount > 1 then // RV086.4.
      raise Exception.Create(SPimsSelNotPosMultExports);

    ExpType := SystemDM.GetDBValue(SqlExportType, '*');
    if (ExpType <> '*') and (Trim(ExpType) <> '') then
      Result := DialogExportPayRollDM.StringToExportType(ExpType);
  end;
end;

// 20011800
procedure TDialogExportPayrollF.ExportAction;
begin
  //RV067.5.
  ExportOK := False; // 20011800
  ExportType := DetermineExportType;
  DialogExportPayrollDM.CurrentExportType := ExportType;
  case ExportType of
    etADP  : ExportADP;
    etADPUS: ExportADPUS;
    //CAR 550299
    etPARIS: ExportPARIS;
    // MR: 29-09-2004 Order 550337 (export for VOS)
    etSR   : ExportSR;
    etREINO: ExportREINO;
    etATTENT: ExportATTENT;
    etELA: ExportELA; // 20013288
    etWMU: ExportWMU; // 20014714
    etNTS: ExportNTS; // GLOB3-72
    etQBOOKS: ExportQBOOKS; // GLOB3-234
    etPCHEX: ExportPCHEX; // GLOB3-271
    etNAVIS: ExportNAVIS; // GLOB3-297
    etBAMBOO: ExportBAMBOO; // GLOB3-377
    else
      // RV094.5.
      if IsAFAS_SELECT or (ExportType = etAFAS) then
      begin
        if rgpAfasSelect.ItemIndex = 0 then
          ExportAFAS
        else
          ExportSelect;
      end;
  end;
end; // ExportAction

// 20011800 This is used as OK-button or as Final Run-button.
procedure TDialogExportPayrollF.btnOkClick(Sender: TObject);
var
  GoOn: Boolean;
  DateMin, DateMax: TDateTime;
begin
  inherited;
  // 20011800
  FinalRunExport := True;
  GoOn := True;
  if SystemDM.UseFinalRun then
  begin
    // 20011800
    DetermineDateMinMax(DateMin, DateMax);
    if not FinalRunExportDateOK then
      GoOn := False;
    if GoOn then
    begin
      if SystemDM.UseFinalRun then
        if DisplayMessage(SPimsFinalRunWarning, mtConfirmation, [mbYes, mbNo])
           <> mrYes then
           GoOn := False;
    end;
  end;
  if GoOn then
  begin
    try
      ExportAction;
    finally
      if SystemDM.UseFinalRun then
      begin
        if ExportOK then
          SystemDM.FinalRunStoreExportDate(PlantFrom,
            DialogExportPayrollDM.ExportTypeToString(ExportType),
            Trunc(MyDateMax));
        ShowFinalRunExportDate;
      end;
    end;
  end;
end; // btnOkClick

function TDialogExportPayrollF.CreateIDExportFile: Boolean;
var
  DialogFilter: String;
begin
// ID File
  FNameExportFile := 'M' +
    FillZero(IntToStr(DialogExportPayrollDM.TableEP.
      FieldByName('CUSTOMER_NUMBER').AsInteger), 5) + '.' +
    FillZero(IntToStr(DialogExportPayrollDM.TableEP.
      FieldByName('SEQUENCE_NUMBER').AsInteger + 1), 3) +
    MyEndOfLine; // RV040.
  FExportList.Add(FNameExportFile);
  if FExportType = etATTENT then
    DialogFilter := 'CSV files (*.csv)|*.CSV'
  else
    DialogFilter := '';

  if FExportType = etATTENT then
    Result := True
  else
    Result := SaveExportFile(FExportList ,'ID', DialogFilter);
end;

procedure TDialogExportPayrollF.CreateExportDataFile;
var
  ExportLine: String;
  ExportPKLine: Integer;
begin
  inherited;
//start record
  // RV045.2. Added time-part to first line of Export ADP.
  ExportLine := 'XM00' +  FillZero(IntToStr(DialogExportPayrollDM.TableEP.
    FieldByName('CUSTOMER_NUMBER').AsInteger), 5) +
    FillZero(IntToStr(DialogExportPayrollDM.TableEP.
      FieldByName('SEQUENCE_NUMBER').AsInteger + 1), 3) +
    FormatDateTime('DDMMYYHHNNSS', Now) + FillSpaces('',6) +
    FillSpaces(DialogExportPayrollDM.TableEP.FieldByName('MEDIUM').AsString, 5) +
    FillSpaces(DialogExportPayrollDM.TableEP.
      FieldByName('OPERATING_SYSTEM').AsString, 6) +
    FillZero(IntToStr(DialogExportPayrollDM.TableEP.
      FieldByName('RELEASE_NUMBER').AsInteger), 2) +
    MyEndOfLine; // RV040.
  FExportList.Add(ExportLine);

//end start
  ProcessingData(ExportPKLine);

// close record
  ExportLine := 'ZS' + FillZero('',20) + FillZero(IntToStr(ExportPKLine), 5) +
  FillZero(IntToStr(DialogExportPayrollDM.TableEP.
    FieldByName('TOTAL_PK_RECORDS').AsInteger + ExportPKLine), 5) +
    MyEndOfLine; // RV040.
  FExportList.Add(ExportLine);
 //end close record
  SaveExportFile(FExportList, FNameExportFile);

// update EP Table (ADP).
  DialogExportPayrollDM.UpdateExportPayrollTable(DialogExportPayrollDM.TableEP.
    FieldByName('TOTAL_PK_RECORDS').AsInteger + ExportPKLine,
    DialogExportPayrollDM.TableEP.FieldByName('SEQUENCE_NUMBER').AsInteger + 1);
end;

// RV067.5.
procedure TDialogExportPayrollF.CreateExportDataFileAttent;
var
  DialogFilter: String;
  ExportPKLine: Integer;
  DateMin, DateMax: TDateTime;
begin
  inherited;
  DialogFilter := 'CSV files (*.csv)|*.CSV';
  DetermineDateMinMax(DateMin, DateMax);

  ProcessingData(ExportPKLine);

  if CheckBoxExportHours.Checked then
  begin
    ExportDataAttent(DateMin, DateMax);
    SaveExportFile(FExportList, FNameExportFile, DialogFilter, 'csv');
  end;
  if CheckBoxExportCounters.Checked then
  begin
    ExportCountersAttent(DateMin, DateMax);
    SaveExportFile(FExportList, FNameExportFile + '_counters', DialogFilter, 'csv');
  end;

  // RV071.9. Store to correct EP-record. (ATTENT).
  // update EP Table
  DialogExportPayrollDM.UpdateExportPayrollTable(DialogExportPayrollDM.TableEP.
    FieldByName('TOTAL_PK_RECORDS').AsInteger + ExportPKLine,
    DialogExportPayrollDM.TableEP.FieldByName('SEQUENCE_NUMBER').AsInteger + 1,
    DialogExportPayrollDM.ExportTypeToString(etATTENT));
end;

function TDialogExportPayrollF.SaveExportFile(const ExportList: TStringList;
  const ExportName: String; DialogFilter: String = ''; DefaultExtension: String = ''): Boolean;
var
  SaveDialogExport: TSaveDialog;
begin
  SaveDialogExport := TSaveDialog.Create(Application);
  try
    SaveDialogExport.Filter := DialogFilter;
    SaveDialogExport.DefaultExt := DefaultExtension;
    with SaveDialogExport do
    begin
      Options := Options + [ofOverWritePrompt];
      FileName := ExportName;
      if DefaultExtension <> '' then
        FileName := FileName + '.' + DefaultExtension;
      Result := Execute;
      if Result then
        ExportList.SaveToFile(FileName);
    end;
  finally
    SaveDialogExport.Free;
    if ExportList.Count > 0 then // 20011800 Was something exported?
      ExportOK := True;
  end;
end;

procedure TDialogExportPayrollF.FormShow(Sender: TObject);
var
  Year, Week, Month, Day: Word;
begin
  // 20013288
  GroupBoxEasyLabor.Visible := False;
  // RV094.5.
  if IsAFAS_SELECT or (ExportType = etAFAS) then
  begin
    InitDialog(True, True, False, True, False, False, False);
    // RV095.2.
    if IsAFAS_SELECT then
    begin
      UsePlant2 := True;
      CheckBoxAllPlantsShow := True;
    end;
  end
  else
    InitDialog(True, False, False, True, False, False, False);
  inherited;
(*  if UseBU then
    FillBusinessUnit; *)
  // 20013288
  if ExportType = etELA then
  begin
    GroupBoxMonth.Visible := False;
    GroupBoxPeriod.Visible := False;
    GroupBoxPayDate.Visible := False;
    GroupBoxATTENT.Visible := False;
    GroupBoxYear.Visible := False;
    RadioGroupSort.Visible := False;
    GroupBoxEasyLabor.Visible := True;
    DTPickerTillDate.Date := Date;
    Exit;
  end;
  if UseContractGroup then
    FillContractGroup;
  ListProcsF.WeekUitDat(Now, Year, Week);
  DecodeDate(Now, Year, Month, Day);

  FTypeOfDialogExport :=
    DialogExportPayrollDM.TableEP.FieldByNAME('WEEK_PERIOD_MONTH').AsString;
  if (FTypeOfDialogExport = '') or
   ((FTypeOfDialogExport <> '') and
    (FTypeOfDialogExport[1] <> EXPORTPAYROLL_WEEK) and
    (FTypeOfDialogExport[1] <> EXPORTPAYROLL_PERIOD) and
    (FTypeOfDialogExport[1] <> EXPORTPAYROLL_MONTH)) then
  begin
    // Period (year + week to last week)
    LabelWeekNr.Visible := False;
    dxSpinEditWKNumber.Visible := False;
    lblWeekPeriod.Visible := False; // 20011800
    GroupBoxPeriod.Height := 80;
    // 20011800 Related to this order, why set to last week of year?
//    dxSpinEditWeekMin.IntValue := Week;
//    dxSpinEditWeekMax.IntValue := ListProcsF.WeeksInYear(Year);
    dxSpinEditWeekMin.IntValue := Week - 4;
    dxSpinEditWeekMax.IntValue := Week - 1;
    RadioGroupSort.ItemIndex := 0;
    dxSpinEditYear.IntValue := Year;
    GroupBoxMonth.Visible := False;
    GroupBoxPeriod.Visible := True;
    dxSpinEditPeriodNr.IntValue := 1;
    RadioGroupSort.Visible := True;
  end;
  if (FTypeOfDialogExport <> '') and (FTypeOfDialogExport[1] = EXPORTPAYROLL_WEEK) then
  begin
    // Week (year + 1 week)
    RadioGroupSort.Visible := False;
    LabelFrom.Visible := False;
    LabelWeek.Visible := False;
    LabelTo.Visible := False;
    dxSpinEditWeekMin.Visible := False;
    dxSpinEditWeekMax.Visible := False;
    //CAR 550299 - Bug
    lblPeriod.Visible := False;
    GroupBoxPeriod.Height := 65;
    if Week > 1 then
      dxSpinEditWKNumber.IntValue := Week - 1;
    ShowWeekPeriod;
    dxSpinEditYear.IntValue := Year;
    dxSpinEditPeriodNr.IntValue := 1;
  end;
  if (FTypeOfDialogExport <> '') and
    (FTypeOfDialogExport[1] = EXPORTPAYROLL_PERIOD) then
  begin
    // Period (year + week to week)
    RadioGroupSort.Visible := False;
    LabelWeekNr.Visible := False;
    dxSpinEditWKNumber.Visible := False;
    GroupBoxPeriod.Height := 80;
    dxSpinEditWeekMin.IntValue := Week - 4;
    dxSpinEditWeekMax.IntValue := Week - 1;
    dxSpinEditYear.IntValue := Year;
    dxSpinEditPeriodNr.IntValue := 1;
  end;
  if (FTypeOfDialogExport <> '') and
    (FTypeOfDialogExport[1] = EXPORTPAYROLL_MONTH) then
  begin
    // Month (1 month)
    RadioGroupSort.Visible := False;
    LabelWeekNr.Visible := False;
    dxSpinEditWKNumber.Visible := False;
    GroupBoxPeriod.Visible := False;
    dxSpinEditYear.IntValue := Year;
    GroupBoxMonth.Visible := True;
    if Month > 1 then
     dxSpinEditMonth.IntValue := Month - 1;
  end;
  // Exporttype = etADPUS
  DTPickerPayDate.Date := Date;
  // RV067.MRA.30 Determine ATTENT-settings during plant-selection.
  //!!
  GroupBoxAttent.Visible := False;
{
  Label22.Visible := Exporttype <> etATTENT;
  dxSpinEditYear.Visible := Exporttype <> etAttent;
  GroupBoxPeriod.Visible := Exporttype <> etAttent;
  GroupBoxAttent.Visible := Exporttype = etAttent;
}
  DateTimePickerMin.DateTime := Now;
  DateTimePickerMax.DateTime := Now;
  // RV095.2.
  if IsAFAS_SELECT then
  begin
    // Initially AFAS is the default, only show this for SELECT:
    rgpAfasSelectClick(Sender);
    CheckBoxAllPlantsClick(Sender);
  end;
  // RV102.5.
  if IsADP_ATTENT then
  begin
    // Do this when 'teams-per-user' is used and there
    // is only 1 plant shown, then it does not trigger
    // this event resulting in a wrong date-selection!
    CmbPlusPlantFromCloseUp(Sender);
  end;
  // 20014714
  if (ExportType = etWMU) or (ExportType = etQBOOKS) or (ExportType = etPCHEX)
    or (ExportType = etNAVIS) or (ExportType = etBAMBOO) then
  begin
    // Do not show the Period-entry
    // Instead of that show weeknumber-entry + show week-period as text
    Label18.Visible := False;
    lblWeekPeriod.Left := lblWeekPeriod.Left - (LabelWeekNr.Left - Label18.Left);
    dxSpinEditPeriodNr.Visible := False;
    LabelWeekNr.Left := Label18.Left;
    dxSpinEditWKNumber.Left := dxSpinEditPeriodNr.Left;
    // PIM-108
    DateFromDate.DateTime := Date;
    DateToDate.DateTime := DateFromDate.DateTime;
    GroupBoxYear.Visible := False;
    GroupBoxPeriod.Visible := False;
    GroupBoxDateSelection.Visible := True;
  end;
  if SystemDM.UseFinalRun then
  begin
    CmbPlusPlantFromCloseUp(Sender);
    ShowFinalRunExportDate;
  end;
  // GLOB3-72
  if ExportType = etNTS then
  begin
    DateFromDate.DateTime := Date;
    DateToDate.DateTime := DateFromDate.DateTime;
    RadioGroupSort.Visible := False;
    GroupBoxYear.Visible := False;
    GroupBoxPeriod.Visible := False;
    GroupBoxDateSelection.Visible := True;
  end;
end; // FormShow

procedure TDialogExportPayrollF.RadioGroupSortClick(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  inherited;
  if RadioGroupSort.ItemIndex = 0 then // Week/Period
  begin
    GroupBoxPeriod.Visible := True;
    GroupBoxMonth.Visible := False;
  end
  else
  begin // Month
    GroupBoxMonth.Visible := True;
    GroupBoxPeriod.Visible := False;
    DecodeDate(Now, Year, Month, Day);
    dxSpinEditMonth.IntValue := Month;
    // 20013723 related to this order.
    // Why do this, this resets the plant-selection!
{    ListProcsF.FillComboBoxPlant(tblPlant, True, CmbPlusPlantFrom);
    ListProcsF.FillComboBoxPlant(tblPlant, False, CmbPlusPlantTo); }
  end;
end;

procedure TDialogExportPayrollF.FormCreate(Sender: TObject);
begin
  inherited;
  OnePlant := SystemDM.UseFinalRun; // 20011800
  OnlyShowPlantSelection := False; // 20011800.70
// OnlyShowPlantSelection := SystemDM.UseFinalRun; // 20011800.70
  GroupBoxFinalRun.Visible := SystemDM.UseFinalRun; // 20011800
  FinalRunExport := False; // 20011800
  if SystemDM.UseFinalRun then // 20011800
  begin
    // Show 3 buttons: Test Run, Final Run, Cancel
    btnOK.Caption := SPimsFinalRun;
    btnTestRun.Visible := True;
    btnTestRun.Left := (pnlBottom.Width div 4) - (btnTestRun.Width div 2);
    btnOK.Left := (pnlBottom.Width div 4) * 2 - (btnOK.Width div 2);
    btnCancel.Left := (pnlBottom.Width div 4) * 3 - (btnCancel.Width div 2);
  end
  else
  begin
    // Show 2 buttons: OK, Cancel
    btnOK.Caption := SPimsOK;
    btnTestRun.Visible := False;
    btnOK.Left := (pnlBottom.Width div 2) - Round(btnOK.Width) - Round(btnOK.Width / 7);
    btnCancel.Left := (pnlBottom.Width div 2) + Round(btnCancel.Width / 7);
  end;

  FExportList := TStringList.Create;

  // RV095.2.
  BevelPlantDept.Visible := False;

//  if (DialogExportPayrollF_HND <> nil) then
//    DialogExportPayrollDM := CreateFormDM(TDialogExportPayrollDM)
//  else
  DialogExportPayrollDM := TDialogExportPayrollDM.Create(Self);
  // Decide what export is used.
  ExportType := DialogExportPayRollDM.DetermineExportType;
  // Make some components visible/invisible depending on ExportType.
  GroupBoxPayDate.Visible := False;

  case ExportType of
    etADPUS: GroupBoxPayDate.Visible := True;
    etSR: ReportExportPayrollSRQR := CreateReportQR(TReportExportPayrollSRQR);
  end;
  ReportExportPayrollQR := CreateReportQR(TReportExportPayrollQR);

  // MR:27-10-2004 Order 550352
  try
    MultiplyFactorHours :=
      DialogExportPayrollDM.TableEPMULTIPLY_FACTOR_HOURS.AsInteger;
  except
    MultiplyFactorHours := 100;
  end;
  try
    MultiplyFactorDays :=
      DialogExportPayrollDM.TableEPMULTIPLY_FACTOR_DAYS.AsInteger;
  except
    MultiplyFactorDays := 10;
  end;
  // RV050.8.
  // RV067.MRA.27
{
  SystemDM.PlantTeamFilterEnable(tblPlant);
  tblPlant.Open;
  if SystemDM.PlantTeamFilterOn then
  begin
    SystemDM.PlantTeamFilterEnable(QueryEmplFrom);
    QueryEmplFrom.Open;
    SystemDM.PlantTeamFilterEnable(QueryEmplTo);
    QueryEmplTo.Open;
  end;
}
  IsADP_ATTENT := DialogExportPayrollDM.DetermineADP_ATTENT;
  IsAFAS_SELECT := DialogExportPayrollDM.DetermineAFAS_SELECT;

  // RV087.2.
  rgpAfasSelect.Visible := False;
  UseBU := False;
  UseContractGroup := False;
  // RV094.5.
  if IsAFAS_SELECT or (ExportType = etAFAS) then
  begin
    UseBU := True;
    UseContractGroup := True;
    rgpAfasSelect.Visible := IsAFAS_SELECT;
    // RV094.5.
{
    GroupBoxPayDate.Visible := False;
    GroupBoxMonth.Visible := False;
    GroupBoxYear.Top := GroupBoxYear.Top + GroupBoxPayDate.Height;
    GroupBoxPeriod.Top := GroupBoxPeriod.Top + GroupBoxPayDate.Height;
}
  end;
  BusinessUnitSelectionVisible;
  ContractGroupSelectionVisible;
  if SystemDM.IsCLF then
  begin
    // Use alternative texts for 'Period' and 'Month'.
    RadioGroupSort.Items.Clear;
    RadioGroupSort.Items.Add(SPimsExportPeriod);
    RadioGroupSort.Items.Add(SPimsExportMonth);
  end;
end;

// RV089.1.
procedure TDialogExportPayrollF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogExportPayrollF.DetermineDateMinMax(
  var DateMin, DateMax: TDateTime);
  // RV095.3. Reorganized.
  procedure WeekSelection;
  begin
    DateMin := ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
       dxSpinEditWKNumber.IntValue, 1);
    DateMax := ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
       dxSpinEditWKNumber.IntValue, 7);
  end;
  procedure PeriodSelection;
  begin
    DateMin := ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
       dxSpinEditWeekMin.IntValue, 1);
    DateMax := ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
       dxSpinEditWeekMax.IntValue, 7);
  end;
  procedure MonthSelection;
  begin
    DateMin := EncodeDate(dxSpinEditYear.IntValue,
      dxSpinEditMonth.IntValue, 1);
    DateMax := EncodeDate(dxSpinEditYear.IntValue,
      dxSpinEditMonth.IntValue,
      ListProcsF.LastMounthDay(dxSpinEditYear.IntValue,
        dxSpinEditMonth.IntValue));
  end;
  procedure FromToDateSelection;
  begin
    DateMin := Trunc(DateTimePickerMin.Date);
    DateMax := Trunc(DateTimePickerMax.Date);
  end;
begin
  // PIM-108
  if GroupBoxDateSelection.Visible then
  begin
    try
      begin
        DateMin := Trunc(DateFromDate.Date);
        DateMax := Trunc(DateToDate.Date);
      end;
    except
      DateMin := Trunc(Date);
      DateMax := Trunc(Date);
    end;
    Exit;
  end;
  try
// set datemin, datemax of period selected
    DateMin := Now; DateMax := Now;
   if FExportType = etATTENT then
   begin
     // RV067.MRA.30 Trunc the dates!
     FromToDateSelection;
     Exit;
   end;
 // RV095.3.
 // Possible groupboxes:
{
    GroupBoxMonth: TGroupBox;
    GroupBoxPeriod: TGroupBox;
    GroupBoxPayDate: TGroupBox;
    GroupBoxATTENT: TGroupBox;
    GroupBoxYear: TGroupBox;
    GroupBoxEasyLabor
}
   if (not RadioGroupSort.Visible) then
   begin
     if FTypeOfDialogExport <> '' then
     begin
       case FTypeOfDialogExport[1] of
       EXPORTPAYROLL_PERIOD: PeriodSelection;
       EXPORTPAYROLL_WEEK:   WeekSelection;
       EXPORTPAYROLL_MONTH:  MonthSelection;
       else
         PeriodSelection;
       end;
     end
     else
       PeriodSelection;
     Exit;
   end;
   if (RadioGroupSort.ItemIndex = RADIOGRP_PERIOD) or
      ( (FTypeOfDialogExport <> '') and
        (FTypeOfDialogExport[1] = EXPORTPAYROLL_PERIOD)) or
     ( (FTypeOfDialogExport <> '') and
      (FTypeOfDialogExport[1] = EXPORTPAYROLL_WEEK)) then
    begin
      if (FTypeOfDialogExport <> '') and
        (FTypeOfDialogExport[1] = EXPORTPAYROLL_WEEK) then
      begin
        WeekSelection;
      end;
      if (RadioGroupSort.ItemIndex = RADIOGRP_PERIOD) or
        ( (FTypeOfDialogExport <> '') and
          (FTypeOfDialogExport[1] = EXPORTPAYROLL_PERIOD)) then
      begin
        PeriodSelection;
      end;
// MRA:28-JAN-2009 RV022. Do not limit the date at the start of year!
//                 Because a week start in previous year!
(*
    if (dxSpinEditWeekMin.IntValue = 1) and
      (DateMin < EncodeDate(dxSpinEditYear.IntValue, 1, 1)) then
        DateMin := EncodeDate(dxSpinEditYear.IntValue, 1, 1);
*)
// MRA:06-JAN-2009 RV018. Do not limit the date to the end of the year!
//                 Because a week can end in next year!
(*
    if (dxSpinEditWeekMax.IntValue =
      ListProcsF.WeeksInYear(dxSpinEditYear.IntValue)) and
      (DateMax > EncodeDate(dxSpinEditYear.IntValue, 12, 31)) then
        DateMax := EncodeDate(dxSpinEditYear.IntValue, 12, 31);
*)
    end
    else
    begin
      // Month selection
      MonthSelection;
    end;
  finally
    MyDateMin := DateMin;
    MyDateMax := DateMax;
  end;
end;

procedure TDialogExportPayrollF.ProcessingData(var ExportPKLine: Integer);
var
  DateMin, DateMax: TDateTime;
begin
// set datemin, datemax of period selected
  DetermineDateMinMax(DateMin, DateMax);

  // 20013723.3 Set date to last day of year.
  if SystemDM.IsCLF and
    (DialogExportPayrollDM.CurrentExportType = etADP) then
    if Datemax = EncodeDate(2012, 12, 30) then
      DateMax := EncodeDate(2012, 12, 31);

  DialogExportPayrollDM.cdsEmplDays.EmptyDataSet;
  DialogExportPayrollDM.FEmplHourType.Clear;
  DialogExportPayrollDM.FEmplHourTypeAbsence.Clear;
  DialogExportPayrollDM.FEmplSort.Clear;
  DialogExportPayrollDM.FEmpExtraPayment.Clear;
  DialogExportPayrollDM.FEmpQuaranteedHrs.Clear;
  DialogExportPayrollDM.FEmpHourTypeWage.Clear;
  DialogExportPayrollDM.FEmpHourSick.Clear;

  // MR:08-08-2003
  if (ExportType = etADPUS) then
    DialogExportPayrollDM.cdsEmployeeADPUSData.EmptyDataSet;
//select all valid employee
  // MR:25-11-2004 Use 'EmployeeFrom' and 'EmployeeTo' instead
  // of dxDBExtLookupEditEmplFrom.Text and dxDBExtLookupEditEmplTo.Text
  // because these first 2 variables are changed earlier.
  DialogExportPayrollDM.GetValidEmpl(PlantFrom, PlantTo,
    IntToStr(EmployeeFrom), IntToStr(EmployeeTo));
  //CAR 27-8-2003
  DialogExportPayrollDM.FillListOfEmployeeSHE(DateMin, DateMax,
    PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
  DialogExportPayrollDM.FillListOfEmployeeAHE(DateMin, DateMax,
    PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
  DialogExportPayrollDM.FEmplSort.Sorted := True;
// export data
  if FExportType <> etATTENT then
    ExportData(DateMin, DateMax, ExportPKLine, -1);
end;

function  TDialogExportPayrollF.GetFixedEmplPart(Empl, LineNr, PeriodNr: Integer): String;
var
  CustomerNumber: String; // RV034.1.
begin
  // RV034.1.
  if (ExportType = etADP) then
    CustomerNumber := FillZero(IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
      FieldByName('CUSTOMER_NUMBER').AsInteger), 5)
  else
    CustomerNumber := FillZero(IntToStr(DialogExportPayrollDM.TableEP.
      FieldByName('CUSTOMER_NUMBER').AsInteger), 5);

  Result := 'PK' + CustomerNumber // RV034.1.
    {FillZero(IntToStr(DialogExportPayrollDM.TableEP.
    FieldByName('CUSTOMER_NUMBER').AsInteger), 5)} +
    FillZero(IntToStr(Empl), 7) +
    FillZero(IntToStr(PeriodNr), 2) +
    FillSpaces(DialogExportPayrollDM.TableEP.FieldByName('PROCESS_CODE').AsString, 1) +
    '0' + FillZero('' , 6) +  '0' + FillZero('' , 6) + '00' + FillZero('' , 5) +
    FillSpaces(DialogExportPayrollDM.TableEP.FieldByName('MUTATION_CODE').AsString, 1) +
    '0' + FillZero(IntToStr(LineNr), 5);
end;

// 20013723 Export_Code is also stored in this list.
function TDialogExportPayrollF.IsEmpl(AEmplHourType: TStringList;
  IndexHourType, Empl: Integer;
  var HourType, Min: Integer; var WAGE_Bonus_yn, Export_Code: String): Boolean;
var
  ListString: String;
  PosSpace, EmplOfList: Integer;
begin
  Result := False;
  //CAR extra check for IndexHourType
  if (IndexHourType < 0) or
    (IndexHourType + 1 > AEmplHourType.Count - 1) then
    Exit;
  ListString := AEmplHourType.Strings[IndexHourType];
  if Pos(IntToStr(Empl), ListString) > 0 then
  begin

    PosSpace := Pos(' ', ListString);
    EmplOfList := StrToInt(Copy(ListString, 0, PosSpace - 1));
    Result := (Empl = EmplOfList);
    if not Result then
      Exit;
    ListString := Copy(ListString, PosSpace + 1, 50);//HT + WAGE_Bonus_YN
    PosSpace := Pos(' ', ListString);
    HourType := StrToInt(Copy(ListString, 0, PosSpace - 1));
    Wage_Bonus_YN := Copy(ListString, PosSpace + 1, 1);

    // 20013723 Export Code is also stored.
    ListString := Copy(ListString, PosSpace + 1, 50); // Skip HT + WageBonus_YN
    PosSpace := Pos(' ', ListString);
    Export_Code := Copy(ListString, PosSpace + 1, 10);

    Min :=  StrToInt(AEmplHourType.Strings[IndexHourType + 1]);
  end;
end;

function TDialogExportPayrollF.Round5(Min: Integer):Integer;
var
  Min5: Integer;
begin
  Result := Min;
  Min5 := Min mod 10;
  if (Min5 = 0) or (Min5 = 5) then
    Result := Min
  else
  begin
    if (Min5 = 1) or (Min5 = 2) then
      Result := Min - Min5;
    if (Min5 = 6) or (Min5 = 7) then
      Result := Min- Min5 + 5;
    if (Min5 = 3) or (Min5 = 4) then
      Result := Min + (5 - Min5);
    if (Min5 = 8) or (Min5 = 9) then
      Result := Min + (10 - Min5);
  end;
end;

procedure TDialogExportPayrollF.ExportHourSick(Empl, HourTypeSick,
  HourSick: Integer; ExportWageYN: String; DateMin, DateMax: TDateTime;
  Wage_Separator: String;
  var ExportVarLine, ExportPart, ExportLine: String;
  var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
var
  ExportCode: String;
  Wage: Double;
begin
  if HourTypeSick <> 0 then
  begin
    if LineNrEmpl = 0 then
    begin
      LineNrEmpl := 1;
      ExportVarLine := '';
    end;
    if ExportWageYN = CHECKEDVALUE then
      DialogExportPayrollDM.GetWage(Empl, HourTypeSick, DateMin, DateMax, Wage);

    DialogExportPayrollDM.HourTypeFind(HourTypeSick);
    { DialogExportPayrollDM.ClientDataSetHourType.FindKey([HourTypeSick]); }
    ExportCode := Copy(DialogExportPayrollDM.ClientDataSetHourType.
      FieldByName('EXPORT_CODE').AsString, 0, 4);
    HourSick := Round((HourSick /60) * MultiplyFactorHours);
    ExportPart := Chr(Ord(9)) +  FillSpaces(ExportCode, 4) + IntToStr(HourSick);

    if (ExportWageYN = CHECKEDVALUE )and (Wage <> 0) then
      ExportPart := ExportPart + Wage_Separator + FloatToStr(Wage);
    ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl, PeriodNr,
      ExportPKLine);
  end;
end;

procedure TDialogExportPayrollF.ExportExtraPayment(Empl: Integer;
 var ExportVarLine, ExportPart, ExportLine: String;
 var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
var
  StrTmp, ExportCode: String;
  IndexPayment: Integer;
  Amount: Real;
begin
  IndexPayment:= DialogExportPayrollDM.FEmpExtraPayment.IndexOf(IntToStr(Empl));
  if  (IndexPayment >= 0) and
    ((IndexPayment + 1) <= (DialogExportPayrollDM.FEmpExtraPayment.Count -1)) then
  begin
    if LineNrEmpl = 0 then
    begin
      LineNrEmpl := 1;
      ExportVarLine := '';
    end;
    StrTmp := DialogExportPayrollDM.FEmpExtraPayment.Strings[IndexPayment + 1];
    IndexPayment := IndexPayment + 1;
    while (pos(Chr_SEP, StrTmp) > 0) do
    begin
      ExportCode := Copy(StrTmp, 0, Pos(CHR_SEP, StrTmp) -1);
      Amount := StrToFloat(Copy(StrTmp, Pos(CHR_SEP,StrTmp) + 1, 40)) *
        MultiplyFactorHours;
      ExportPart := Chr(Ord(9)) +  FillSpaces(ExportCode, 4) + FloatToStr(Amount);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl, PeriodNr,
        ExportPKLine);
      IndexPayment := IndexPayment + 1;
      if IndexPayment >= DialogExportPayrollDM.FEmpExtraPayment.Count then
        Exit;
      StrTmp := DialogExportPayrollDM.FEmpExtraPayment.Strings[IndexPayment];
    end;
  end;
end;

procedure TDialogExportPayrollF.ExportGuaranteedHrs_ADPUS(Empl, Min: Integer;
 ExportWageYN, WageSeparator: String; DateMin, DateMax: TDateTime);
var
  HourType, QuaranteedMin: Integer;
  QuaranteedExportCode: String;
  Wage: Double;
begin
  DialogExportPayrollDM.GetGuaranteedHrs(Empl, HourType, QuaranteedMin,
    QuaranteedExportCode);
  if (QuaranteedMin > Min) then
  begin
    if ExportWageYN = CHECKEDVALUE then
      DialogExportPayrollDM.GetWage(Empl, HourType, DateMin, DateMax, Wage);
    DialogExportPayrollDM.AddPerEmplQuaranteedContract(Empl, HourType,
      (QuaranteedMin - Min));
  end;
end;

procedure TDialogExportPayrollF.ExportGuaranteedHrs(Empl, Min: Integer;
 ExportWageYN, WageSeparator: String; DateMin, DateMax: TDateTime;
 var ExportVarLine, ExportPart, ExportLine: String;
 var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
var
  HourType, QuaranteedMin: Integer;
  QuaranteedExportCode: String;
  Wage: Double;
begin
  DialogExportPayrollDM.GetGuaranteedHrs(Empl, HourType, QuaranteedMin,
    QuaranteedExportCode);
  if (QuaranteedMin > Min) then
  begin
    if ExportWageYN = CHECKEDVALUE then
      DialogExportPayrollDM.GetWage(Empl, HourType, DateMin, DateMax, Wage);
    if LineNrEmpl = 0 then
    begin
      LineNrEmpl := 1;
      ExportVarLine := '';
    end;
    ExportPart := Chr(Ord(9)) +
      FillSpaces(QuaranteedExportCode, 4) +
      IntToStr(Round(((QuaranteedMin - Min)  / 60) * MultiplyFactorHours));
    if (ExportWageYN = CHECKEDVALUE) and (Wage <> 0) then
      ExportPart := ExportPart +  WageSeparator + FloatToStr(Wage);

    ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
      PeriodNr, ExportPKLine);
    DialogExportPayrollDM.AddPerEmplQuaranteedContract(Empl, HourType,
      (QuaranteedMin - Min));
  end;
end;

procedure TDialogExportPayrollF.ExportData(DateMin, DateMax: TDateTime;
  var ExportPKLine: Integer;
  MyEmployeeNumber: Integer);
var
  Empl, LineNrEmpl, IndexHourType, HourType, Min,
  Hours, HourTypeSick, HoursSick, MinExported: Integer;
  WKDays, IllDays, HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays,
  BankDays, TFTDays, MatDays, LayDays, TravelDays,
  HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
  ShorterWeekDays: Integer;
  ExportLine, ExportVarLine, ExportCode, ExportPart, ExportWageYN,
  WAGE_Separator, Wage_Bonus_YN: String;
  Wage: Double;
  EndOfLine: String; // RV040.
  ExportCodeDummy: String;
  AExportCodeRecord: PTExportCodeRecord; // RV082.16.
  ExportCodeList: TList; // RV082.16.
//  ExportCodeCount: Integer; // RV082.16.
  Minutes: Integer; // RV082.16.
  I: Integer; // RV082.16.
  CountryID: Integer; // 20013723
  // RV082.16.
  procedure ExportCodeListAdd(AExportCode: String; AMinutes: Integer;
    AWage: Double);
  var
    I: Integer;
    Exists: Boolean;
    ExportCodeRecord: PTExportCodeRecord;
  begin
    Exists := False;
    for I := 0 to ExportCodeList.Count - 1 do
    begin
      ExportCodeRecord := ExportCodeList.Items[I];
      // If exists, then only summarize minutes.
      if ExportCodeRecord.ExportCode = AExportCode then
      begin
        ExportCodeRecord.Minutes := ExportCodeRecord.Minutes + AMinutes;
        Exists := True;
        Break;
      end;
    end;
    if not Exists then
    begin
      // if not exists, then add to list.
      new(ExportCodeRecord);
      ExportCodeRecord.ExportCode := AExportCode;
      ExportCodeRecord.Minutes := AMinutes;
      ExportCodeRecord.Wage := AWage;
      ExportCodeList.Add(ExportCodeRecord);
    end;
  end;
begin
  if (ExportType = etADP) then // RV040.
    EndOfLine := MyEndOfLine
  else
    EndOfLine := '';
  ExportPKLine := 0;
  ExportWageYN :=
    DialogExportPayrollDM.TableEP.FieldByName('EXPORT_WAGE_YN').AsString;
  WAGE_Separator :=
    DialogExportPayrollDM.TableEP.FieldByName('WAGE_SEPARATOR').AsString;
  DialogExportPayrollDM.ClientDataSetEmpl.First;
  while not DialogExportPayrollDM.ClientDataSetEmpl.Eof do
  begin
    Empl := DialogExportPayrollDM.ClientDataSetEmpl.
      FieldByName('EMPLOYEE_NUMBER').AsInteger;
    CountryID := DialogExportPayrollDM.ClientDataSetEmpl.
      FieldByName('COUNTRY_ID').AsInteger;
    if (ExportType = etAFAS) then // RV088.1.
    begin
      // Skip employee not in list for AFAS.
      if not AFASExportClass.EmployeeExists(Empl) then
      begin
        DialogExportPayrollDM.ClientDataSetEmpl.Next;
        Continue;
      end;
      AFASExportClass.CustomerNumber :=
        DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('CUSTOMER_NUMBER').AsInteger;
      AFASExportClass.ExportNormalHrs :=
        DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('EXPORT_NORMAL_HRS_YN').AsString = 'Y';
      // RV101.1.
      AFASExportClass.ExportWorkedDays :=
        DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('EXPORT_WORKED_DAYS_YN').AsString = 'Y';
    end;
    if (ExportType = etADPUS) then
    begin
      if not DialogExportPayrollDM.cdsEmployeeADPUSData.FindKey([Empl]) then
      begin
        DialogExportPayrollDM.cdsEmployeeADPUSData.Insert;
        DialogExportPayrollDM.cdsEmployeeADPUSData.
          FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
        DialogExportPayrollDM.cdsEmployeeADPUSData.Post;
      end;
    end;
    if (ExportType <> etAFAS) then // RV088.1.
    begin
      //build the list of sick hours
      DialogExportPayrollDM.GetHourSickPay(dxSpinEditYear.IntValue, Empl,
        DateMin, DateMax,
        DialogExportPayrollDM.ClientDataSetEmpl.FieldByNAME('STARTDATE').AsDateTime,
        HourTypeSick, HoursSick);
      //build the list of extra payment hours
      DialogExportPayrollDM.FillQueryExtraPayment(Empl, DateMin, DateMax);
    end;
    LineNrEmpl := 0;
// if will not be exported because is not in list it will be forced to be still exported
    // Employee has no employee contract.
    if (DialogExportPayrollDM.FEmplSort.IndexOf(IntToStr(Empl)) < 0) then
    begin
      if (ExportType = etADP) then
      begin
        ExportGuaranteedHrs(Empl,0, ExportWageYN, WAGE_Separator, DateMin, DateMax,
          ExportVarLine, ExportPart, ExportLine, LineNrEmpl, FPeriodNr, ExportPKLine);

// export hours sick
        ExportHourSick(Empl, HourTypeSick, HourSSick, ExportWageYN,
          DateMin, DateMax, Wage_Separator,
        ExportVarLine, ExportPart, ExportLine, LineNrEmpl, FPeriodNr, ExportPKLine);

// export extra payment
        ExportExtraPayment(Empl, ExportVarLine, ExportPart, ExportLine,
          LineNrEmpl, FPeriodNr, ExportPKLine);
      end;

      // RV088.1.
      if (ExportType <> etAFAS) then
      begin
        // MR:06-09-2004 Order 550331
        // If employee has no hours, the guaranteed days should still be added.
        ExportGuaranteedDays(Empl, DateMin, DateMax, WKDays, IllDays,
          HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
          MatDays, LayDays, TravelDays,
          ExportVarLine, ExportPart, ExportLine, LineNrEmpl, FPeriodNr,
          ExportPKLine);

        if LineNrEmpl > 0 then
        begin
          FExportList.Add(GetFixedEmplPart(Empl, LineNrEmpl, FPeriodNr) + ExportVarLine +
            Chr(Ord(9)) + EndOfLine); // RV040.
          ExportPKLine := ExportPKLine + 1;
        end;
      end; // if (ExportType <> etAFAS) then

//end export extra payment
    end // if DialogExportPayrollDM.FEmplSort.IndexOf(IntToStr(Empl)) < 0 then
    else
  // export .... information for found empl
    begin
// fill fixed part
      LineNrEmpl := 1;
      ExportVarLine := '';
// end fixed part
// begin variable part
      MinExported := 0;
      if (ExportType <> etAFAS) then
      begin
        // RV082.16. Start.
        // Only gather information here and put it in a TList.
        // When exportcodes are the same then minutes are summarized.
        try
          ExportCodeList := TList.Create;
          for IndexHourType := 0 to
            (DialogExportPayrollDM.FEmplHourType.Count -1) do
          begin
            if (IndexHourType mod 2) = 0 then
              if IsEmpl(DialogExportPayrollDM.FEmplHourType, IndexHourType, Empl,
                HourType, Min, Wage_Bonus_YN, ExportCode) then
              begin
                // 20013723 Related to this order
                // This goes wrong when export-codes per country are
                // defined for the hour-type. This will set the
                // export-code back to the non-country-export-code
                // of the hourtype!
                // To solve this, store/get export_code in/from the list.
//                DialogExportPayrollDM.HourTypeFind(HourType);
//                ExportCode := Copy(DialogExportPayrollDM.ClientDataSetHourType.
//                 FieldByName('EXPORT_CODE').AsString, 0, 4);
                ExportCode := Copy(ExportCode, 0, 4);
                Minutes := Min;
                if ExportWageYN = CHECKEDVALUE then
                  DialogExportPayrollDM.GetWage(Empl, HourType, DateMin, DateMax,
                    Wage);
                 // Add to list and summarize if needed.
                ExportCodeListAdd(ExportCode, Minutes, Wage);
              end;
          end;
          // Now Export the lines based on TList.
          for I := 0 to ExportCodeList.Count - 1 do
          begin
            AExportCodeRecord := ExportCodeList.Items[I];
            Min := AExportCodeRecord.Minutes;
            Min := Round5(Min);
            Hours := Round((Min  / 60) * MultiplyFactorHours);
            MinExported := MinExported + Min;
            ExportPart := Chr(Ord(9)) +
              FillSpaces(AExportCodeRecord.ExportCode, 4) +  IntToStr(Hours);
            if ExportWageYN = CHECKEDVALUE then
               ExportPart := ExportPart + WAGE_Separator +
                 FloatToStr(AExportCodeRecord.Wage);
            ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
              FPeriodNr, ExportPKLine);
          end;
          // Now clear the list.
          for I := ExportCodeList.Count - 1 downto 0 do
          begin
            AExportCodeRecord := ExportCodeList.Items[I];
            Dispose(AExportCodeRecord);
            ExportCodeList.Remove(AExportCodeRecord);
          end;
        finally
          ExportCodeList.Clear;
          ExportCodeList.Free;
        end;
        // RV082.16. End.
      end; // if (ExportType <> etAFAS) then
// Export difference if it is the case
// begin
      if (ExportType = etADP) then
        ExportGuaranteedHrs(Empl, MinExported, ExportWageYN, WAGE_Separator,
          DateMin, DateMax, ExportVarLine, ExportPart, ExportLine, LineNrEmpl,
          FPeriodNr, ExportPKLine);
// end
// export hours sick
      if (ExportType <> etAFAS) then
      begin
        ExportHourSick(Empl, HourTypeSick, HourSSick, ExportWageYN, DateMin, DateMax,
          Wage_Separator, ExportVarLine, ExportPart,
          ExportLine, LineNrEmpl, FPeriodNr, ExportPKLine);
//
        ExportExtraPayment(Empl, ExportVarLine, ExportPart, ExportLine,
          LineNrEmpl, FPeriodNr, ExportPKLine);
      end; // if (ExportType <> etAFAS) then
// export days for empl
// MR_START
      DialogExportPayrollDM.DetermineDays(Empl, WKDays, IllDays, HolDays,
        PaidDays, UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
        MatDays, LayDays, TravelDays, HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays,  //RV067.5.
        HolBnkRsvDays, ShorterWeekDays, ExportCodeDummy);
      if WKDays > 0 then
      begin
        // RV088.1.
        if (ExportType = etAFAS) then
        begin
          // Export worked days?
          // RV101.1.
//          if AFASExportClass.ExportNormalHrs and
          if AFASExportClass.ExportWorkedDays and
            (DialogExportPayrollDM.TableEP.
              FieldByName('EXPORT_CODE_DAYS').AsString <> '') then
            ExportPart := DialogExportPayrollDM.TableEP.
              FieldByName('EXPORT_CODE_DAYS').AsString + ';' + IntToStr(WKDays)
          else
            ExportPart := '';
        end
        else
          ExportPart := Chr(Ord(9)) +
            FillSpaces(Copy(DialogExportPayrollDM.TableEP.
              FieldByName('EXPORT_CODE_DAYS').AsString, 0, 4), 4) +
            IntToStr(WKDays * MultiplyFactorDays);
        ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
          FPeriodNr, ExportPKLine);
        DialogExportPayrollDM.SetExportToY(Empl, DateMin, DateMax);
      end; // if WKDays > 0 then
      if (ExportType = etADPUS) then
      begin
        if DialogExportPayrollDM.cdsEmployeeADPUSData.FindKey([Empl]) then
        begin
          DialogExportPayrollDM.cdsEmployeeADPUSData.Edit;
          DialogExportPayrollDM.cdsEmployeeADPUSData.
            FieldByName('WKDAYS').AsInteger := WKDays;
          DialogExportPayrollDM.cdsEmployeeADPUSData.
            FieldByName('TOTALDAYS').AsInteger := WKDays + IllDays + HolDays +
              PaidDays + UnpaidDays + WTRDays + LabourDays + BankDays +
              TFTDays + MatDays + LayDays + TravelDays;
          DialogExportPayrollDM.cdsEmployeeADPUSData.Post;
        end;
      end;
      ExportPart := GetExportAbsenceDays(CountryID, IllDays, ILLNESS);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, HolDays, HOLIDAY);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, PaidDays, PAID);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, UnpaidDays, UNPAID);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, WTRDays, WTR);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, LabourDays, LABOURTHERAPY);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, BankDays, RSNB);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      ExportPart := GetExportAbsenceDays(CountryID, TFTDays, TIMEFORTIMEAVAILABILITY);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      // RV035.1.
      ExportPart := GetExportAbsenceDays(CountryID, MatDays, MATERNITYLEAVE);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      // RV035.1.
      ExportPart := GetExportAbsenceDays(CountryID, LayDays, LAY_DAYS);
      // SO-20013169
      ExportPart := GetExportAbsenceDays(CountryID, TravelDays, TRAVELTIME);
      ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
        FPeriodNr, ExportPKLine);
      // MR:26-07-2004 Order 550331
      ExportGuaranteedDays(Empl, DateMin, DateMax, WKDays, IllDays,
        HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
        MatDays, LayDays, // RV035.1.
        TravelDays, // SO-20013169
        ExportVarLine, ExportPart, ExportLine, LineNrEmpl, FPeriodNr,
        ExportPKLine);
// end variable part
      if ExportType <> etAFAS then
        FExportList.Add(GetFixedEmplPart(Empl, LineNrEmpl, FPeriodNr) +
          ExportVarLine + Chr(Ord(9)) + EndOfLine); // RV040.
      ExportPKLine := ExportPKLine + 1;
    end;{for one empl}
    DialogExportPayrollDM.ClientDataSetEmpl.Next;
  end; // while not DialogExportPayrollDM.ClientDataSetEmpl.Eof do
end; // ExportData

procedure TDialogExportPayrollF.ExportDataAttent(DateMin, DateMax: TDateTime);
var
  GuarranteedDaysExportCode,
  Line, EndOfLine, FirmCode, WorkerClerk, EmplShift, SalaryCode, Hours: String; // RV040.
  SelDate: TDateTime;
  CountryID: Integer; // RV071.14.

  GuarranteedDays, Empl: Integer;
  Wage: Real;
  procedure ExportEmptyLine(SelDate: TDateTime);
  begin
    Line := '"' + FormatDateTime('yyyy-mm-dd', SelDate) + '","-"';
    FExportList.Add(Line);
  end;

  procedure FilterDataset(ADs: TClientDataset; ADate: TDateTime);
  var
    Year, Month, Day: Word;
  begin
    DecodeDate(ADate, Year, Month, Day);
    ADs.Filtered := False;
    ADs.Filter :=
      Format('Year(EDATE) = %d and Month(EDATE) = %d and Day(EDATE) = %d',
      [Year, Month, Day]);
    ADs.Filtered := True;
  end;
begin
  EndOfLine := '';

  FExportList.Clear;

  SelDate := DateMin;
  while SelDate <= DateMax do
  begin
    FilterDataset(DialogExportPayrollDM.cdsEmplDays, SelDate);
    if DialogExportPayrollDM.cdsEmplDays.RecordCount = 0 then
//      ExportEmptyLine(SelDate) // RV067.MRA.30
    else
    begin
      FilterDataset(DialogExportPayrollDM.cdsEmplMins, SelDate);
      while not DialogExportPayrollDM.cdsEmplMins.Eof do
      begin
        Line := '';
        Empl := DialogExportPayrollDM.
          cdsEmplMins.FieldByName('EMPLOYEE_NUMBER').AsInteger;

        if DialogExportPayrollDM.ClientDataSetEmpl.FindKey([Empl]) then
        begin
          // RV071.14.
          // Country ID
          CountryID := DialogExportPayrollDM.ClientDataSetEmpl.
              FieldByName('COUNTRY_ID').AsInteger;

          // FirmCode
          FirmCode :=
            IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
              FieldByName('CUSTOMER_NUMBER').AsInteger);

          // Worker/Clerk
          DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage,
            GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
          if WorkerClerk = 'Y' then
            WorkerClerk := 'A'
          else
            WorkerClerk := 'B';

          // Shift
          // RV075.5. Always export '00'.
          EmplShift := '00';
{          EmplShift := DialogExportPayrollDM.ClientDataSetEmpl.
            FieldByName('SHIFT_NUMBER').AsString; }

          // Export Code
          // RV067.MRA.30 Get exportcode from cdsEmplMins, if not null.
          if (DialogExportPayrollDM.cdsEmplMins.
            FieldByName('EXPORT_CODE').AsString <> '') then
            SalaryCode := DialogExportPayrollDM.
              cdsEmplMins.FieldByName('EXPORT_CODE').AsString
          else
            if DialogExportPayrollDM.HourTypeFind(DialogExportPayrollDM.cdsEmplMins.
                FieldByName('HOURTYPE_NUMBER').AsInteger, CountryID)
              {DialogExportPayrollDM.ClientDataSetHourType.
              FindKey([DialogExportPayrollDM.cdsEmplMins.
                FieldByName('HOURTYPE_NUMBER').AsInteger]) } then
              SalaryCode :=  DialogExportPayrollDM.ClientDataSetHourType.
                FieldByName('EXPORT_CODE').AsString;

          // Hours
          Hours := IntToStr(DialogExportPayrollDM.cdsEmplMins.
            FieldByName('MINS').AsInteger * 5);

          Line := Format('"%s","%s","%s","%s","%s","%s","%s"',
            [
             FillZero(FirmCode, 6),
             WorkerClerk,
             FillZero(IntToStr(Empl), 7),
             FormatDateTime('yyyy-mm-dd', SelDate),
             FillZero(SalaryCode, 7),
             FillZero(Hours, 6),
             FillZero(EmplShift, 2)
             ]);
          FExportList.Add(Line);
        end;
        DialogExportPayrollDM.cdsEmplMins.Next;
      end;
    end; //we have records for SelDate
    SelDate := SelDate + 1;
  end;
end;
(*
procedure TDialogExportPayrollF.ExportCountersAttent(DateMin, DateMax: TDateTime);
var

  GuarranteedDaysExportCode,
  Line, EndOfLine, FirmCode, WorkerClerk, SalaryCode: String; // RV040.

  GuarranteedDays, Empl: Integer;
  Wage: Real;

  WKDays, IllDays, HolDays, PaidDays,
  UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays,
  HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
  ShorterWeekDays: Integer;
  procedure AddCounterLine(ACounter: Integer);
  begin
    Line := Format('"%s","%s","%s","","","","","","%s","%s","%s","%s","","","","","","","","","%s","%s"', [
      WorkerClerk, FirmCode, SystemDM.LoginUser,
      SalaryCode,
      FormatDateTime('yyyy-mm-dd', DateMax),
      FormatDateTime('yyyy-mm-dd', Now),
      FormatDateTime('yyyy-mm-dd', DateMin),
      FillZero(IntToStr(Empl), 7),
      IntToStr(ACounter)]);
    FExportList.Add(Line);
  end;
begin
  EndOfLine := '';

  FExportList.Clear;

  DialogExportPayrollDM.FillCounters;

  while not DialogExportPayrollDM.ClientDataSetEmpl.Eof do
  begin
    Line := '';
    Empl := DialogExportPayrollDM.ClientDataSetEmpl.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;

    DialogExportPayrollDM.DetermineDays(Empl, WKDays, IllDays, HolDays, PaidDays,
    UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays,
    HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
    ShorterWeekDays);

    FirmCode :=
      FillZero(IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.FieldByName('CUSTOMER_NUMBER').AsInteger), 6);
    DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage, GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
    if WorkerClerk = 'Y' then WorkerClerk := 'A' else WorkerClerk := 'B';

//    if DialogExportPayrollDM.ClientDataSetHourType.FindKey([DialogExportPayrollDM.cdsEmplCounters.FieldByNAME('HOURTYPE_NUMBER').AsInteger]) then
//      SalaryCode :=  DialogExportPayrollDM.ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;

    AddCounterLine(WKDays);
    AddCounterLine(IllDays);
    AddCounterLine(HolDays);
    AddCounterLine(PaidDays);
    AddCounterLine(UnpaidDays);
    AddCounterLine(WTRDays);
    AddCounterLine(LabourDays);
    AddCounterLine(BankDays);
    AddCounterLine(TFTDays);
    AddCounterLine(MatDays);
    AddCounterLine(HolPrevDays);
    AddCounterLine(HolSenDays);
    AddCounterLine(HolAddBankDays);
    AddCounterLine(SatCredDays);
    AddCounterLine(HolBnkRsvDays);
    AddCounterLine(ShorterWeekDays);

    DialogExportPayrollDM.ClientDataSetEmpl.Next;
  end;
end;

*)
function FillZero(IntVal: Integer): String;
begin
  Result := IntToStr(IntVal);
  if (IntVal = 0) then
     Result := '00';
  if (length(IntToStr(IntVal)) = 1) then
    Result := '0' + IntToStr(IntVal);
end;

function DecodeHrsMin(HrsMin: Integer):String;
var
  Hrs, Min: Integer;
begin
  // RV054.4.
  if HrsMin < 0 then
    Result := '-'
  else
    Result := '';
  HrsMin := Abs(HrsMin);
  Hrs := HrsMin div 60;
  Min := HrsMin mod 60;
  Result := Result +  FillZero(Hrs) + ':' +  FillZero(Min);
  if Length(Result)= 6 then
    Result := ' ' + Result;
  if Length(Result)= 5 then
    Result := '  ' + Result;
end;

//RV067.MRA.30 This is wrong!
{
procedure TDialogExportPayrollF.ExportCountersAttent(DateMin, DateMax: TDateTime);
var

  GuarranteedDaysExportCode,
  Line, EndOfLine, FirmCode, WorkerClerk, SalaryCode, Hours: String; // RV040.

  GuarranteedDays, Empl: Integer;
  Wage: Real;
begin
  EndOfLine := '';

  FExportList.Clear;

  DialogExportPayrollDM.FillCounters;

  while not DialogExportPayrollDM.cdsEmplCounters.Eof do
  begin
    Line := '';
    Empl :=
      DialogExportPayrollDM.
        cdsEmplCounters.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if DialogExportPayrollDM.ClientDataSetEmpl.FindKey([Empl]) then
    begin
      FirmCode :=
        FillZero(IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('CUSTOMER_NUMBER').AsInteger), 6);
      DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage,
        GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
      if WorkerClerk = 'Y' then WorkerClerk := 'A' else WorkerClerk := 'B';

      if DialogExportPayrollDM.ClientDataSetHourType.
        FindKey([DialogExportPayrollDM.cdsEmplCounters.
          FieldByName('HOURTYPE_NUMBER').AsInteger]) then
        SalaryCode :=  DialogExportPayrollDM.ClientDataSetHourType.
          FieldByName('EXPORT_CODE').AsString;
      Hours := DecodeHrsMin(Round5(DialogExportPayrollDM.cdsEmplCounters.
        FieldByName('MINS').AsInteger)); //!! ??  / 60 * 300
      Line := Format('"%s","%s","%s","","","","","","%s","%s","%s","%s","","","","","","","","","%s","%s"', [
        WorkerClerk, FirmCode, SystemDM.LoginUser,
        SalaryCode,
        FormatDateTime('yyyy-mm-dd', DateMax),
        FormatDateTime('yyyy-mm-dd', Now),
        FormatDateTime('yyyy-mm-dd', DateMin),
        FillZero(IntToStr(Empl), 7),
        Hours]);
      FExportList.Add(Line);
    end;
    DialogExportPayrollDM.cdsEmplCounters.Next;
  end;
end;
}

(*
// RV067.MRA.30
procedure TDialogExportPayrollF.ExportCountersAttent(DateMin, DateMax: TDateTime);
var
  GuarranteedDaysExportCode,
  Line, EndOfLine, FirmCode, WorkerClerk, SalaryCode: String; // RV040.
  GuarranteedDays, Empl: Integer;
  Wage: Real;
  WKDays, IllDays, HolDays, PaidDays,
  UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays,
  HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
  ShorterWeekDays: Integer;
  ExportCode, ExportCodeWorkingDays: String;
  DateTimeOfSelection: TDateTime;
  procedure AddCounterLine(ACounter: Integer; AAbsenceTypeCode: String);
  begin
    // RV071.9. Show all counters; if they are set as active-counters.
//    if ACounter <> 0 then
    begin
      if AAbsenceTypeCode <> '' then
        SalaryCode := DialogExportPayrollDM.
          DetermineCounterExportCodeByEmpAbsType(Empl, AAbsenceTypeCode);
      if SalaryCode <> '' then
      begin
        Line := Format(
          '"%s","%s","%s","0","0","0","0","%s","%s","%s","%s","%s",' +
          '"0","0","0","0","0","0","0","0","%s"', [
{ 1}      WorkerClerk,
{ 2}      FillZero(FirmCode, 6),
{ 3}      SystemDM.LoginUser,
{ 8}      IntToStr(ACounter),
{ 9}      FillZero(SalaryCode, 7),
{10}      FormatDateTime('yyyy-mm-dd', DateMax),
          // RV071.13. 550497. Date+time must be shown here.
{11}      FormatDateTime('yyyy-mm-dd hh:nn:ss', DateTimeOfSelection),
{12}      FormatDateTime('yyyy-mm-dd', DateMin),
{21}      FillZero(IntToStr(Empl), 7)]
          );
        FExportList.Add(Line);
      end;
    end;
  end;
begin
  EndOfLine := '';
  SalaryCode := '';
  DateTimeOfSelection := Now;

  ExportCodeWorkingDays := '';
  if DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE',
    DialogExportPayrollDM.ExportTypeToString(etATTENT), []) then
    ExportCodeWorkingDays := DialogExportPayrollDM.TableEP.
      FieldByName('EXPORT_CODE_DAYS').AsString;

  FExportList.Clear;

//  DialogExportPayrollDM.FillCounters;

  DialogExportPayrollDM.ClientDataSetEmpl.First;
  while not DialogExportPayrollDM.ClientDataSetEmpl.Eof do
  begin
    Line := '';
    Empl := DialogExportPayrollDM.ClientDataSetEmpl.
      FieldByName('EMPLOYEE_NUMBER').AsInteger;

    // RV071.13. 550497. Only planned absence for rest of year must be shown.
    DialogExportPayrollDM.DetermineDays(Empl, WKDays, IllDays, HolDays, PaidDays,
      UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays,
      HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
      ShorterWeekDays, ExportCode);

    // Export Code
    SalaryCode := ExportCode;

    // Firm Code
    FirmCode :=
      IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
        FieldByName('CUSTOMER_NUMBER').AsInteger);

    // Worker Clerk
    DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage,
      GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
    if WorkerClerk = 'Y' then
      WorkerClerk := 'A'
    else
      WorkerClerk := 'B';

//    if DialogExportPayrollDM.ClientDataSetHourType.FindKey([DialogExportPayrollDM.cdsEmplCounters.FieldByNAME('HOURTYPE_NUMBER').AsInteger]) then
//      SalaryCode :=  DialogExportPayrollDM.ClientDataSetHourType.FieldByName('EXPORT_CODE').AsString;

    // Export Code
    SalaryCode := ExportCodeWorkingDays;

    // RV071.13. 550497 First line is working days. Should not be exported.
//    AddCounterLine(WKDays,          '');
    AddCounterLine(IllDays,         ILLNESS);
    AddCounterLine(HolDays,         HOLIDAYAVAILABLETB);
    AddCounterLine(PaidDays,        PAID);
    AddCounterLine(UnpaidDays,      UNPAID);
    AddCounterLine(WTRDays,         WORKTIMEREDUCTIONAVAILABILITY);
    AddCounterLine(LabourDays,      LABOURTHERAPY);
    AddCounterLine(BankDays,        RSNB);
    AddCounterLine(TFTDays,         TIMEFORTIMEAVAILABILITY);
    AddCounterLine(MatDays,         MATERNITYLEAVE);
    AddCounterLine(HolPrevDays,     HOLIDAYPREVIOUSYEARAVAILABILITY);
    AddCounterLine(HolSenDays,      SENIORITYHOLIDAYAVAILABILITY);
    AddCounterLine(HolAddBankDays,  ADDBANKHOLIDAYAVAILABILITY);
    AddCounterLine(SatCredDays,     SATCREDITAVAILABILITY);
    AddCounterLine(HolBnkRsvDays,   BANKHOLIDAYRESERVEAVAILABILITY);
    AddCounterLine(ShorterWeekDays, SHORTERWORKWEEKAVAILABILITY);

    DialogExportPayrollDM.ClientDataSetEmpl.Next;
  end;
end;
*)

// RV067.MRA.30
// RV071.13. 550497.
// Export Counters Attentia:
// - It must export the planned absence counters!
//   This is the same information shown in Hours Per Employee, in
//   tab-page Holiday Counters.
// RV079.1.
// - It must export 3 values: Used, Planned and Available.
procedure TDialogExportPayrollF.ExportCountersAttent(DateMin, DateMax: TDateTime);
var
  GuarranteedDaysExportCode,
  Line, EndOfLine, FirmCode, WorkerClerk, SalaryCode: String; // RV040.
  GuarranteedDays, Empl: Integer;
  Wage: Real;
{  WKDays, IllDays, HolDays, PaidDays,
  UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays, MatDays, LayDays,
  HolPrevDays, HolSenDays, HolAddBankDays, SatCredDays, HolBnkRsvDays,  //RV067.5.
  ShorterWeekDays: Integer; }
  ExportCode, ExportCodeWorkingDays: String;
  DateTimeOfSelection: TDateTime;
  Year, Month, Day, Week: Word;
  Remaining, Normal, Earned, Used, Planned, Available, CounterDesc: String;
  procedure AddCounterLine(AUsed, APlanned, AAVailable: String;
    AAbsenceTypeCode: String);
  var
    UsedHours, PlannedHours, AvailableHours: String;
  begin
    // RV071.9. Show all counters; if they are set as active-counters.
//    if ACounter <> 0 then
    begin
      if AAbsenceTypeCode <> '' then
        SalaryCode := DialogExportPayrollDM.
          DetermineCounterExportCodeByEmpAbsType(Empl, AAbsenceTypeCode);
      if SalaryCode <> '' then
      begin
{
Hoe ziet het aangeleverde bestand er uit?:

"A","123456","31ADM","0","0","0","0","00:00","0000014","2010-10-31","2010-10-12 11:22:02","2010-09-01","0","0","0","0","0","0","0","0","0020000"
"A","123456","31ADM","0","0","0","0","00:00","0000012","2010-10-31","2010-10-12 11:22:02","2010-09-01","0","0","0","0","0","0","0","0","0020000"

Hoe zou het er moeten uitzien?
"","355000","A","0001004","01",0,0,0,0,4800,0,300,0,0,0,0,0,0,0,"","","2010-05-20","TELLERCL"
"","355000","A","0001004","02",0,0,0,0,6000,0,1200,300,0,0,0,0,0,0,"","","2010-05-20","TELLERCL"

1) "" : PKWKN dit is een unieke sleutel voor iedere werknemer op onze databank. U mag dit veld leeglaten. Het leeg veld moet echter wel ge�nterfaced worden.
2) "355000": FRM: firmanummer: max 6 karakters
3) "A": AB: A= arbeider / B=bediende
4) "0001004": WKN: werknemernummer= altijd 7 karakters
5) "01": TELLER: de code van de teller. De code van de teller gekend bij Attentia. Moet altijd 2 karakters zijn.
6) "0": VORMAX = moet niet ge�nterfaced worden.
7) "0": VORGENOM = moet niet ge�nterfaced worden.
8) "0": VORSANC = moet niet ge�nterfaced worden.
9) "0": VORUIT = moet niet ge�nterfaced worden.
10) "4800": HUIDMAX = maximum saldo van de teller. Bv bij verlofteller zal dit 20dagen zijn = 20*300= 6000 (berekening, zie mailtje  07/10/2010)
11) "0": HUIDIN = moet niet ge�nterfaced worden.
12) "300": HUIDGENOM = aantal genomen uren/dagen
13) "600": HUIDVOORZ = voorzien aantal uur (bv gepland verlof).
14) "0": HUIDSANC = moet niet ge�nterfaced worden.
15) "0": HUIDUIT = moet niet ge�nterfaced worden.
16) "0": VOLGMAX = moet niet ge�nterfaced worden.
17) "0": VOLGIN = moet niet ge�nterfaced worden.
18) "0": VOLGVOORZ = moet niet ge�nterfaced worden.
19) "0": VOLGSANC = moet niet ge�nterfaced worden.
20) "0": VANAF = moet niet ge�nterfaced worden.
21) "0": TEM = moet niet ge�nterfaced worden.
22) "2010-01-31": TIJDUPDATE, formaat =YYYY-MM-DD
23) "TELLERCL": GEBRUIKER

Hoe interfacen we 1 opgenomen uur? Huidgenom = 300.
Hoe interfacen we 1 opgenomen dag? Huidgenom = 300.
De parametrisatie in ons systeem zal detecteren of het een dagen- of urenteller betreft.
}
        UsedHours := IntToStr(StrTime2IntMin(AUsed) * 5);
        PlannedHours := IntToStr(StrTime2IntMin(APlanned) * 5);
        AvailableHours := IntToStr(StrTime2IntMin(AAvailable) * 5);
        // RV075.5. Do not show "0" but just 0
        Line := Format(
{ 1 -  5} '"%s","%s","%s","%s","%s",' +
{ 6 - 10} '0,0,0,0,"%s",' +
{11 - 15} '0,"%s","%s",0,0,' +
{16 - 20} '0,0,0,0,0,' +
{21 - 23} '0,"%s","%s"',
          [
{ 1}      '',
{ 2}      FillZero(FirmCode, 6),
{ 3}      WorkerClerk,
{ 4}      FillZero(IntToStr(Empl), 7),
{ 5}      FillZero(SalaryCode, 2),
{ 6}      // Zero
{ 7}      // Zero
{ 8}      // Zero
{ 9}      // Zero
{10}      FillZero(AvailableHours, 6), // HUIDMAX (available)
{11}      // Zero
{12}      FillZero(UsedHours, 6),      // HUIDGENOM (used)
{13}      FillZero(PlannedHours, 6),   // HUIDVOORZ (planned)
{14}      // Zero
{15}      // Zero
{16}      // Zero
{17}      // Zero
{18}      // Zero
{19}      // Zero
{20}      // Zero
{21}      // Zero
{22}      FormatDateTime('yyyy-mm-dd', DateTimeOfSelection),
{23}      SystemDM.CurrentProgramUser
          ]
          );
        FExportList.Add(Line);
      end;
    end;
  end;
begin
  try
    // RV071.13. 550497.
    // To get the Planned Absence Counters, we need this datamodule.
//    HoursPerEmployeeDM := THoursPerEmployeeDM.Create(Self);

    EndOfLine := '';
    SalaryCode := '';
    DateTimeOfSelection := Now;

    ExportCodeWorkingDays := '';
    if DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE',
      DialogExportPayrollDM.ExportTypeToString(etATTENT), []) then
      ExportCodeWorkingDays := DialogExportPayrollDM.TableEP.
        FieldByName('EXPORT_CODE_DAYS').AsString;

    FExportList.Clear;

    DialogExportPayrollDM.ClientDataSetEmpl.First;
    while not DialogExportPayrollDM.ClientDataSetEmpl.Eof do
    begin
      Line := '';
      Empl := DialogExportPayrollDM.ClientDataSetEmpl.
        FieldByName('EMPLOYEE_NUMBER').AsInteger;

      // RV071.13. 550497. Only planned absence for rest of year must be shown!
      DecodeDate(DateMax, Year, Month, Day);
      ListProcsF.WeekUitDat(DateMax, Year, Week);

      // RV079.4. Use GlobalDMT-class procedures to get the balances.
      ABalanceCounters.EmployeeNumber := Empl;
      ABalanceCounters.Year := Year;
      ABalanceCounters.DateFrom := Trunc(Now);

      ABalanceCounters.GetCounters;

      // Export Code
      SalaryCode := ExportCode;

      // Firm Code
      FirmCode :=
        IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('CUSTOMER_NUMBER').AsInteger);

      // Worker Clerk
      DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage,
        GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
      if WorkerClerk = 'Y' then
        WorkerClerk := 'A'
      else
        WorkerClerk := 'B';

      // Export Code
      SalaryCode := ExportCodeWorkingDays;

      // RV071.13. 550497 First line is working days. Should not be exported.
  //    AddCounterLine(WKDays,          '');

      // Holiday
      if ABalanceCounters.
        GetCounterActivated(HOLIDAYAVAILABLETB, CounterDesc) then
      begin
        ABalanceCounters.ComputeHoliday(Remaining, Normal, Used, Planned,
          Available, True);
        AddCounterLine(Used, Planned, Available, HOLIDAYAVAILABLETB);
      end;

      // Time for Time
      if ABalanceCounters.
        GetCounterActivated(TIMEFORTIMEAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeTimeForTime(Remaining, Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, TIMEFORTIMEAVAILABILITY);
      end;

      // Work time reduction
      if ABalanceCounters.
        GetCounterActivated(WORKTIMEREDUCTIONAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeWorkTime(Remaining, Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, WORKTIMEREDUCTIONAVAILABILITY);
      end;

      // Holiday previous year
      if ABalanceCounters.
        GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeHldPrevYear(Remaining, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, HOLIDAYPREVIOUSYEARAVAILABILITY);
      end;

      // Seniority Holiday
      if ABalanceCounters.
        GetCounterActivated(SENIORITYHOLIDAYAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeSeniorityHld(Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SENIORITYHOLIDAYAVAILABILITY);
      end;

      // Additional Bank Holiday
      if ABalanceCounters.
        GetCounterActivated(ADDBANKHOLIDAYAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeAddBnkHoliday(Remaining, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, ADDBANKHOLIDAYAVAILABILITY);
      end;

      // Saturday credit
      if ABalanceCounters.
        GetCounterActivated(SATCREDITAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeSatCredit(Remaining, Earned, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SATCREDITAVAILABILITY);
      end;

      // Bank holiday to reserve
      if ABalanceCounters.
        GetCounterActivated(BANKHOLIDAYRESERVEAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeRsvBnkHoliday(Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, BANKHOLIDAYRESERVEAVAILABILITY);
      end;

      // Shorter working week
      if ABalanceCounters.
        GetCounterActivated(SHORTERWORKWEEKAVAILABILITY, CounterDesc) then
      begin
        ABalanceCounters.ComputeShorterWeek(Remaining, Earned, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SHORTERWORKWEEKAVAILABILITY);
      end;

      DialogExportPayrollDM.ClientDataSetEmpl.Next;

(*
      HoursPerEmployeeDM.FilterDataEmployee := Empl;
      HoursPerEmployeeDM.FilterDataYear := Year;
      HoursPerEmployeeDM.FilterDataWeek := Week;

      HoursPerEmployeeDM.GetCounters;

      // Export Code
      SalaryCode := ExportCode;

      // Firm Code
      FirmCode :=
        IntToStr(DialogExportPayrollDM.ClientDataSetEmpl.
          FieldByName('CUSTOMER_NUMBER').AsInteger);

      // Worker Clerk
      DialogExportPayrollDM.LastValidContr(Empl, DateMin, DateMax, Wage,
        GuarranteedDays, GuarranteedDaysExportCode, WorkerClerk);
      if WorkerClerk = 'Y' then
        WorkerClerk := 'A'
      else
        WorkerClerk := 'B';

      // Export Code
      SalaryCode := ExportCodeWorkingDays;

      // RV071.13. 550497 First line is working days. Should not be exported.
  //    AddCounterLine(WKDays,          '');

      // Holiday
      if HoursPerEmployeeDM.
        GetCounterActivated(HOLIDAYAVAILABLETB, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeHoliday(Remaining, Normal, Used, Planned,
          Available, True);
        AddCounterLine(Used, Planned, Available, HOLIDAYAVAILABLETB);
      end;

      // Time for Time
      if HoursPerEmployeeDM.
        GetCounterActivated(TIMEFORTIMEAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeTimeForTime(Remaining, Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, TIMEFORTIMEAVAILABILITY);
      end;

      // Work time reduction
      if HoursPerEmployeeDM.
        GetCounterActivated(WORKTIMEREDUCTIONAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeWorkTime(Remaining, Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, WORKTIMEREDUCTIONAVAILABILITY);
      end;

      // Holiday previous year
      if HoursPerEmployeeDM.
        GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeHldPrevYear(Remaining, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, HOLIDAYPREVIOUSYEARAVAILABILITY);
      end;

      // Seniority Holiday
      if HoursPerEmployeeDM.
        GetCounterActivated(SENIORITYHOLIDAYAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeSeniorityHld(Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SENIORITYHOLIDAYAVAILABILITY);
      end;

      // Additional Bank Holiday
      if HoursPerEmployeeDM.
        GetCounterActivated(ADDBANKHOLIDAYAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeAddBnkHoliday(Remaining, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, ADDBANKHOLIDAYAVAILABILITY);
      end;

      // Saturday credit
      if HoursPerEmployeeDM.
        GetCounterActivated(SATCREDITAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeSatCredit(Remaining, Earned, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SATCREDITAVAILABILITY);
      end;

      // Bank holiday to reserve
      if HoursPerEmployeeDM.
        GetCounterActivated(BANKHOLIDAYRESERVEAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeRsvBnkHoliday(Normal, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, BANKHOLIDAYRESERVEAVAILABILITY);
      end;

      // Shorter working week
      if HoursPerEmployeeDM.
        GetCounterActivated(SHORTERWORKWEEKAVAILABILITY, CounterDesc) then
      begin
        HoursPerEmployeeDM.ComputeShorterWeek(Remaining, Earned, Used, Planned,
          Available);
        AddCounterLine(Used, Planned, Available, SHORTERWORKWEEKAVAILABILITY);
      end;

{
      AddCounterLine(IllDays,         ILLNESS);
      AddCounterLine(PaidDays,        PAID);
      AddCounterLine(UnpaidDays,      UNPAID);
      AddCounterLine(LabourDays,      LABOURTHERAPY);
      AddCounterLine(BankDays,        RSNB);
      AddCounterLine(MatDays,         MATERNITYLEAVE);
}

      DialogExportPayrollDM.ClientDataSetEmpl.Next;
*)
    end;
  finally
//    HoursPerEmployeeDM.Free;
  end;
end;

// 20013723 Related to this order.
// Also search on CountryID
function TDialogExportPayrollF.GetExportAbsenceDays(CountryID, AbsDays: Integer;
  AbsType: String): String;
var
  ExportCode: String;
begin
  Result := '';
  if AbsDays = 0 then
    Exit;

  // 20013723 Related to this order.
  // Also search on CountryID
//  DialogExportPayrollDM.ClientDataSetAbsType.FindKey([AbsType]);
  ExportCode := '';
  if DialogExportPayrollDM.AbsenceTypeFind(AbsType, CountryID) then
    ExportCode :=
      DialogExportPayrollDM.ClientDataSetAbsType.
        FieldByName('EXPORT_CODE').AsString;
  // RV088.1.
  if (ExportType = etAFAS) then
  begin
    if ExportCode <> '' then
      Result := ExportCode + ';' + IntToStr(AbsDays)
    else
      Result := ''
  end
  else
  begin
    // 20013723
    if ExportCode <> '' then
    begin
      // RV056.3.
      if (ExportType = etADP) and (Copy(ExportCode, 0, 4) = 'V288') then
        Result := Chr(Ord(9)) +
          FillSpaces(Copy(ExportCode, 0, 4), 4) +  IntToStr(AbsDays * 100)
      else
        Result := Chr(Ord(9)) +
          FillSpaces(Copy(ExportCode, 0, 4), 4) +  IntToStr(AbsDays *
            MultiplyFactorDays);
    end;
  end;
end;

procedure TDialogExportPayrollF.ProcessLine(var ExportVarLine, ExportPart,
  ExportLine: String; var LineNrEmpl: Integer; Empl, PeriodNr: Integer;
  var ExportPKLine: Integer);
begin
  if ExportPart = '' then
    Exit;
  // RV088.1.
  if (ExportType = etAFAS) then
  begin
    if AFASExportClass.DetectExportCode(ExportPart) <> '' then
      FExportList.Add(AFASExportClass.ExportLine(Empl,
        AFASExportClass.CustomerNumber, AFASExportClass.DetectDays(ExportPart),
        AFASExportClass.DetectExportCode(ExportPart), 'D'));
  end
  else
  begin
    if Length(ExportVarLine) + Length(ExportPart) <= MaxLengthVarPart then
        ExportVarLine := ExportVarLine + ExportPart
    else
    begin
      ExportLine := GetFixedEmplPart(Empl, LineNrEmpl, PeriodNr) + ExportVarLine;
      FExportList.Add(ExportLine + Chr(Ord(9)));
      LineNrEmpl := LineNrEmpl + 1;
      ExportPKLine := ExportPKLine + 1;
      ExportVarLine := ExportPart;
    end;
  end;
end;

procedure TDialogExportPayrollF.SetExportType(const Value: TExportType);
begin
  FExportType := Value;
end;

// MR:04-12-2003 Set max of weeks and correct the component that handles
//               the week-selection
procedure TDialogExportPayrollF.ChangeDate(Sender: TObject);
begin
  inherited;
  try
    // Set max of weeks
    dxSpinEditWeekMin.MaxValue :=
      ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    // Correct the week-selection-component
    if dxSpinEditWeekMin.Value > dxSpinEditWeekMin.MaxValue then
      dxSpinEditWeekMin.Value := dxSpinEditWeekMin.MaxValue;
  except
    dxSpinEditWeekMin.Value := 1;
  end;

  try
    dxSpinEditWeekMax.MaxValue :=
      ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    if dxSpinEditWeekMax.Value > dxSpinEditWeekMax.MaxValue then
      dxSpinEditWeekMax.Value := dxSpinEditWeekMax.MaxValue;
  except
    dxSpinEditWeekMax.Value := 1;
  end;

  try
    dxSpinEditWKNumber.MaxValue :=
      ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    if dxSpinEditWKNumber.Value > dxSpinEditWKNumber.MaxValue then
      dxSpinEditWKNumber.Value := dxSpinEditWKNumber.MaxValue;
  except
    dxSpinEditWKNumber.Value := 1;
  end;
  ShowPeriod;
  ShowWeekPeriod; // 20014714
end;

// MR:05-12-2003
procedure TDialogExportPayrollF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekMin.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekMax.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

// MR:26-07-2004 Order 550331
// Calculate and export 'guaranteed-days'.
function TDialogExportPayrollF.GetExportGuaranteedDays(Empl: Integer;
   DateMin, DateMax: TDateTime; WKDays, IllDays, HolDays, PaidDays,
   UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
   MatDays, LayDays, TravelDays: Integer): String;
var
  GuaranteedDays: Integer;
  GuaranteedDaysExportCode: String;
  TotalDays: Integer;
begin
  Result := '';
  DialogExportPayrollDM.GetGuaranteedDays(Empl, DateMin, DateMax,
    GuaranteedDays, GuaranteedDaysExportCode);
  if GuaranteedDays > 0 then
  begin
    TotalDays := WKDays + IllDays + HolDays + PaidDays +
      UnpaidDays + WTRDays + LabourDays + BankDays + TFTDays +
      MatDays + LayDays + TravelDays;
    if TotalDays < GuaranteedDays then
      Result := Char(Ord(9)) +
        FillSpaces(Copy(GuaranteedDaysExportCode, 0, 4), 4) +
          IntToStr((GuaranteedDays - TotalDays) * MultiplyFactorDays);
  end;
end;

// MR:26-07-2004 Order 550331
// Works only for 'ADP', values are stored in 'ExportList'-list.
procedure TDialogExportPayrollF.ExportGuaranteedDays(Empl: Integer;
   DateMin, DateMax: TDateTime; WKDays, IllDays, HolDays, PaidDays,
   UnpaidDays, WTRDays, LabourDays, BankDays, TFTDays,
   MatDays, LayDays, TravelDays: Integer;
   var ExportVarLine, ExportPart, ExportLine: String;
   var LineNrEmpl, PeriodNr, ExportPKLine: Integer);
begin
  ExportPart := GetExportGuaranteedDays(Empl, DateMin, DateMax,
    WKDays, IllDays, HolDays, PaidDays, UnpaidDays, WTRDays, LabourDays,
      BankDays, TFTDays, MatDays, LayDays, TravelDays);
  if ExportPart <> '' then
  begin
    if LineNrEmpl = 0 then
    begin
      LineNrEmpl := 1;
      ExportVarLine := '';
    end;
    ProcessLine(ExportVarLine, ExportPart, ExportLine, LineNrEmpl, Empl,
      PeriodNr, ExportPKLine);
  end;
end;

// MR:29-09-2004 Order 550337 - Export for VOS.
procedure TDialogExportPayrollF.ExportSR;
const
  MaxTimeBlock = 4;
  EXPORT_DAYS='Y';
  EXPORT_HOURS='N';
var
  DateMin, DateMax: TDateTime;
  EmployeeNumber: Integer;
  ProdHourEmployeeDate: TDateTime;
  HoursOrDays, ProdMin: Double;
  ExportLine, ExportCode, PlantCode, WorkspotCode, Remark, JobCode,
  ExportCodeDays: String;
  HourlyWage, MaximumSalary, BonusPercentage, Efficiency: Double;
  ABSHoursOrDays, ABSEfficiency: Double;
  BonusInMoney, MonthGroupEfficiency, OverTime: Boolean;
  HolidayDayPeriod: Double;
  Days: Double;
  SaveDecimalSeparator: Char;
  ExportHours: Boolean;
  BonusYN: String;
  SelectStr, CalcBonusYN: String;
  Efficiency100Min: Boolean;
  ExportGroup: String;
  FExportList1, FExportList2: TStringList;
  ExportCodeDummy: String;
  function DetermineHourlyWage(
    const EmployeeNumber: Integer;
    const DateMax: TDateTime): Double;
  begin
    with DialogExportPayRollDM do
    begin
      qryEmpContractHourlyWage.Close;
      qryEmpContractHourlyWage.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        EmployeeNumber;
      qryEmpContractHourlyWage.ParamByName('FDATE').AsDateTime :=
        DateMax;
      qryEmpContractHourlyWage.Open;
      if not qryEmpContractHourlyWage.IsEmpty then
        Result := qryEmpContractHourlyWage.FieldByName('HOURLY_WAGE').AsFloat
      else
        Result := 0;
      qryEmpContractHourlyWage.Close;
    end;
  end;
  procedure DetermineExportPayrollSettings(
    var MonthGroupEfficiency: Boolean;
    var ExportCodeDays: String;
    var Efficiency100Min: Boolean);
  begin
    with DialogExportPayRollDM do
    begin
      MonthGroupEfficiency := False;
      ExportCodeDays := '';
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add('SELECT MONTH_GROUP_EFFICIENCY_YN, EXPORT_CODE_DAYS,');
      qryWork.SQL.Add('EFFICIENCY100MIN_YN');
      qryWork.SQL.Add('FROM EXPORTPAYROLL');
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        MonthGroupEfficiency :=
          qryWork.FieldByName('MONTH_GROUP_EFFICIENCY_YN').AsString = 'Y';
        ExportCodeDays :=
          qryWork.FieldByName('EXPORT_CODE_DAYS').AsString;
        Efficiency100Min :=
          qryWork.FieldByName('EFFICIENCY100MIN_YN').AsString = 'Y';
      end;
      qryWork.Close;
    end;
  end;
  procedure cdsEmpSalaryUpdate(const EmployeeNumber: Integer;
    const SalaryMinute: Integer;
    const Salary: Double);
  begin
    with DialogExportPayRollDM do
    begin
      if not cdsEmpSalary.FindKey([EmployeeNumber]) then
      begin
        // Insert
        cdsEmpSalary.Insert;
        cdsEmpSalary.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
          EmployeeNumber;
        cdsEmpSalary.FieldByName('SALARY_MINUTE').AsInteger :=
          SalaryMinute;
        cdsEmpSalary.FieldByName('SALARY').AsFloat :=
          Salary;
        cdsEmpSalary.Post;
      end
      else
      begin
        // Update
        cdsEmpSalary.Edit;
        cdsEmpSalary.FieldByName('SALARY_MINUTE').AsInteger :=
          cdsEmpSalary.FieldByName('SALARY_MINUTE').AsInteger +
            SalaryMinute;
        cdsEmpSalary.FieldByName('SALARY').AsFloat :=
          cdsEmpSalary.FieldByName('SALARY').AsFloat +
            Salary;
        cdsEmpSalary.Post;
      end;
    end;
  end;
  procedure cdsEmpExportLineUpdate(
    const DaysYN: String;
    const EmployeeNumber: Integer;
    const ExportCode: String;
    const WorkspotCode: String;
    const Remark: String;
    const Amount: Double;
    const Efficiency: Double;
    const BonusYN: String);
    // 20013430.
    function DetermineExportGroup: String;
    begin
      Result := '3'; // When not set, use '3' to filter in report.
      with DialogExportPayrollDM do
      begin
        with qryExportGroupSR do
        begin
          Close;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
          Open;
          if not Eof then
          begin
            if FieldByName('FREETEXT').AsString <> '' then
              Result := Trim(Copy(FieldByName('FREETEXT').AsString, 1, 30));
          end;
          Close;
        end;
      end;
    end; // DetermineExportGroup
  begin
    with DialogExportPayRollDM do
    begin
      if not cdsEmpExportLine.FindKey([EmployeeNumber, ExportCode,
        WorkspotCode, DaysYN]) then
      begin
        cdsEmpExportLine.Insert;
        cdsEmpExportLine.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
          EmployeeNumber;
        cdsEmpExportLine.FieldByName('EXPORT_CODE').AsString :=
          ExportCode;
        cdsEmpExportLine.FieldByName('WORKSPOT_CODE').AsString :=
          WorkspotCode;
        cdsEmpExportLine.FieldByName('DAYS_YN').AsString :=
          DaysYN;
        cdsEmpExportLine.FieldByName('REMARK').AsString :=
          Remark;
        cdsEmpExportLine.FieldByName('AMOUNT').AsFloat :=
          Amount;
        cdsEmpExportLine.FieldByName('EFFICIENCY').AsFloat :=
          Efficiency;
        cdsEmpExportLine.FieldByName('BONUS_YN').AsString :=
          BonusYN;
        cdsEmpExportLine.FieldByName('EXPORTGROUP').AsString :=
          DetermineExportGroup;
        cdsEmpExportLine.Post;
      end
      else
      begin
        cdsEmpExportLine.Edit;
        cdsEmpExportLine.FieldByName('AMOUNT').AsFloat :=
          cdsEmpExportLine.FieldByName('AMOUNT').AsFloat + Amount;
        cdsEmpExportLine.Post;
      end;
    end;
  end;
  procedure FillCDSGroupEfficiency(DateMin: TDateTime);
  var
    Year, Month, Day: Word;
  begin
    DecodeDate(DateMin, Year, Month, Day);
    with DialogExportPayRollDM do
    begin
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'SELECT ' +
        '  PLANT_CODE, WORKSPOT_CODE, EFFICIENCY ' +
        'FROM ' +
        '  GROUPEFFICIENCY ' +
        'WHERE ' +
        '  YEAR_NUMBER = :YEAR_NUMBER AND ' +
        '  MONTH_NUMBER = :MONTH_NUMBER ' +
        '  ORDER BY PLANT_CODE, WORKSPOT_CODE');
      qryWork.ParamByName('YEAR_NUMBER').AsInteger := Year;
      qryWork.ParamByName('MONTH_NUMBER').AsInteger := Month;
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          cdsGroupEfficiency.Insert;
          cdsGroupEfficiency.FieldByName('PLANT_CODE').AsString :=
            qryWork.FieldByName('PLANT_CODE').AsString;
          cdsGroupEfficiency.FieldByName('WORKSPOT_CODE').AsString :=
            qryWork.FieldByName('WORKSPOT_CODE').AsString;
          cdsGroupEfficiency.FieldByName('EFFICIENCY').AsFloat :=
            qryWork.FieldByName('EFFICIENCY').AsFloat;
          cdsGroupEfficiency.Post;
          qryWork.Next;
        end;
      end;
      qryWork.Close;
    end;
  end;
  // MR:29-01-2007 Order 550438 Addition of Efficiency per Employee
  procedure FillCDSGroupEmployeeEfficiency(DateMin: TDateTime);
  var
    Year, Month, Day: Word;
  begin
    DecodeDate(DateMin, Year, Month, Day);
    with DialogExportPayRollDM do
    begin
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'SELECT ' +
        '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, EMPLOYEE_NUMBER, EFFICIENCY ' +
        'FROM ' +
        '  EMPLOYEEEFFICIENCY ' +
        'WHERE ' +
        '  YEAR_NUMBER = :YEAR_NUMBER AND ' +
        '  MONTH_NUMBER = :MONTH_NUMBER ' +
        'ORDER BY ' +
        '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, EMPLOYEE_NUMBER');
      qryWork.ParamByName('YEAR_NUMBER').AsInteger := Year;
      qryWork.ParamByName('MONTH_NUMBER').AsInteger := Month;
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          cdsGroupEmployeeEfficiency.Insert;
          cdsGroupEmployeeEfficiency.FieldByName('PLANT_CODE').AsString :=
            qryWork.FieldByName('PLANT_CODE').AsString;
          cdsGroupEmployeeEfficiency.FieldByName('WORKSPOT_CODE').AsString :=
            qryWork.FieldByName('WORKSPOT_CODE').AsString;
          cdsGroupEmployeeEfficiency.FieldByName('JOB_CODE').AsString :=
            qryWork.FieldByName('JOB_CODE').AsString;
          cdsGroupEmployeeEfficiency.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
            qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          cdsGroupEmployeeEfficiency.FieldByName('EFFICIENCY').AsFloat :=
            qryWork.FieldByName('EFFICIENCY').AsFloat;
          cdsGroupEmployeeEfficiency.Post;
          qryWork.Next;
        end;
      end;
      qryWork.Close;
    end;
  end;
  function DetermineEfficiency(APlantCode, AWorkspotCode: String;
    var AEfficiency: Double): Boolean;
  begin
    Result := False;
    with DialogExportPayRollDM do
    begin
      if cdsGroupEfficiency.FindKey([APlantCode, AWorkspotCode]) then
      begin
        AEfficiency := cdsGroupEfficiency.FieldByName('EFFICIENCY').AsFloat;
        Result := True;
      end;
    end;
  end;
  // MR:29-01-2007 Order 550438 Addition of Efficiency per Employee
  function DetermineEmployeeEfficiency(APlantCode, AWorkspotCode,
    AJobCode: String; AEmployeeNumber: Integer;
    var AEfficiency: Double): Boolean;
  begin
    Result := False;
    with DialogExportPayRollDM do
    begin
      if cdsGroupEmployeeEfficiency.
        FindKey([APlantCode, AWorkspotCode, AJobCode, AEmployeeNumber]) then
      begin
        AEfficiency :=
          cdsGroupEmployeeEfficiency.FieldByName('EFFICIENCY').AsFloat;
        Result := True;
      end;
    end;
  end;
  function PlusMin(Value: Double): String;
  begin
    if Value < 0 then
      Result := '-'
    else
      Result := '+';
  end;
  // Get TimeBlock-data from clientdatasets that are available
  // in CalculateTotalHoursDMT, instead of retrieving the data
  // here again.
  function DetermineTimeBlocks(
    const APlantCode, ADepartmentCode: String;
    const AEmployeeNumber, AShiftNumber: Integer;
    const Where: String;
    var ADataSet: TDataSet): Boolean;
  begin
    // Timeblocks Per Employee
    ADataSet := CalculateTotalHoursDM.ClientDataSetTBEmpl;
    ADataSet.Filtered := False;
    ADataSet.Filter :=
      'PLANT_CODE = ' + QuotedStr(APlantCode) + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber) + ' AND ' +
      'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
      Where;
    ADataSet.Filtered := True;
    Result := not ADataSet.IsEmpty;

    // Timeblocks Per Department
    if not Result then
    begin
      // Timeblocks per Department
      ADataSet := CalculateTotalHoursDM.ClientDataSetTBDept;
      ADataSet.Filtered := False;
      ADataSet.Filter :=
        'PLANT_CODE = ' + QuotedStr(APlantCode) + ' AND ' +
        'DEPARTMENT_CODE = ' + QuotedStr(ADepartmentCode) + ' AND ' +
        'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
        Where;
      ADataSet.Filtered := True;
      Result := not ADataSet.IsEmpty;

      // Timeblocks Per Shift
      if not Result then
      begin
        // Timeblocks Per Shift
        ADataSet := CalculateTotalHoursDM.ClientDataSetTBShift;
        ADataSet.Filtered := False;
        ADataSet.Filter :=
          'PLANT_CODE = ' + QuotedStr(APlantCode) + ' AND ' +
          'SHIFT_NUMBER = ' + IntToStr(AShiftNumber) +
          Where;
        ADataSet.Filtered := True;
        Result := not ADataSet.IsEmpty;
      end;
    end;
  end;
  function DetermineHolidayDayPeriod(cdsHoliday: TDataSet): Double;
  var
    Where: String;
    ADataSet: TDataSet;
    Index, TotalTimeblockCount, TimeblockCount: Integer;
  begin
    Result := 0;
    with DialogExportPayrollDM do
    begin
      Where := '';
      for Index := 1 to MaxTimeBlocks do
      begin
        if cdsAbsenceReason.FindKey([
          cdsHoliday.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Index)).AsString, HOLIDAY]) then
        begin
          if Where = '' then
            Where := ' AND (TIMEBLOCK_NUMBER = ' + IntToStr(Index)
          else
            Where := Where + ' OR TIMEBLOCK_NUMBER = ' + IntToStr(Index);
        end;
      end;
      if Where <> '' then
        Where := Where + ')';
      // Determine number-of-timeblocks for holiday.
      if DetermineTimeBlocks(
        cdsHoliday.FieldByName('PLANT_CODE').AsString,
        cdsHoliday.FieldByName('DEPARTMENT_CODE').AsString,
        cdsHoliday.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        cdsHoliday.FieldByName('SHIFT_NUMBER').AsInteger,
        Where,
        ADataSet) then
      begin
        TimeblockCount := 0;
        ADataSet.First;
        while not ADataSet.Eof do
        begin
          ADataSet.Next;
          inc(TimeblockCount);
        end;
        // Determine TOTAL of timeblocks.
        if DetermineTimeBlocks(
          cdsHoliday.FieldByName('PLANT_CODE').AsString,
          cdsHoliday.FieldByName('DEPARTMENT_CODE').AsString,
          cdsHoliday.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          cdsHoliday.FieldByName('SHIFT_NUMBER').AsInteger,
          ' AND (TIMEBLOCK_NUMBER = 1 OR TIMEBLOCK_NUMBER = 2 OR ' +
          'TIMEBLOCK_NUMBER = 3 OR TIMEBLOCK_NUMBER = 4) ',
          ADataSet) then
        begin
          TotalTimeblockCount := 0;
          ADataSet.First;
          while not ADataSet.Eof do
          begin
            ADataSet.Next;
            inc(TotalTimeblockCount);
          end;
          if TotalTimeblockCount > 0 then
            Result := TimeblockCount / TotalTimeblockCount;
        end;
      end;
    end;
  end;
  // Determine holiday for whole period and
  // put result in Cliendataset for later use.
  procedure DetermineHoliday;
  begin
    with DialogExportPayrollDM do
    begin
      // MR:9-12-2004 Select 'plantcode' from EMPLOYEE (plantfrom-plantto).
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'SELECT ' +
        '  EAV.EMPLOYEEAVAILABILITY_DATE, ' +
        '  EAV.PLANT_CODE, EAV.SHIFT_NUMBER, EAV.EMPLOYEE_NUMBER, ' +
        '  ABT.EXPORT_CODE, EMP.DEPARTMENT_CODE, ' +
        '  EAV.AVAILABLE_TIMEBLOCK_1, EAV.AVAILABLE_TIMEBLOCK_2, ' +
        '  EAV.AVAILABLE_TIMEBLOCK_3, EAV.AVAILABLE_TIMEBLOCK_4 ' +
        'FROM ' +
        '  EMPLOYEEAVAILABILITY EAV, ABSENCEREASON AR, ABSENCETYPE ABT, ' +
        '  EMPLOYEE EMP ' +
        'WHERE ' +
        '  EAV.EMPLOYEE_NUMBER = EMP.EMPLOYEE_NUMBER AND ' +
        '  (EAV.AVAILABLE_TIMEBLOCK_1 = AR.ABSENCEREASON_CODE OR ' +
        '  EAV.AVAILABLE_TIMEBLOCK_2 = AR.ABSENCEREASON_CODE OR ' +
        '  EAV.AVAILABLE_TIMEBLOCK_3 = AR.ABSENCEREASON_CODE OR ' +
        '  EAV.AVAILABLE_TIMEBLOCK_4 = AR.ABSENCEREASON_CODE) AND ' +
        '  AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE AND ' +
        '  ABT.ABSENCETYPE_CODE = :ABSENCETYPE_CODE AND ' +
        '  EAV.EMPLOYEEAVAILABILITY_DATE >= :DATEMIN AND ' +
        '  EAV.EMPLOYEEAVAILABILITY_DATE <= :DATEMAX AND ' +
        '  EMP.PLANT_CODE >= :PLANTFROM AND ' +
        '  EMP.PLANT_CODE <= :PLANTTO AND ' +
        '  EAV.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' +
        '  EAV.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' +
        '  ABT.EXPORT_CODE IS NOT NULL ' +
        'ORDER BY ' +
        '  EAV.PLANT_CODE, EAV.EMPLOYEEAVAILABILITY_DATE, ' +
        '  EAV.SHIFT_NUMBER, EAV.EMPLOYEE_NUMBER'
        );
      qryWork.ParamByName('ABSENCETYPE_CODE').AsString := HOLIDAY;
      qryWork.ParamByName('DATEMIN').AsDateTime := DateMin;
      qryWork.ParamByName('DATEMAX').AsDateTime := DateMax;
      qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
      qryWork.ParamByName('PLANTTO').AsString := PlantTo;
      qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          cdsHoliday.Insert;
          cdsHoliday.FieldByName('PLANT_CODE').AsString :=
            qryWork.FieldByName('PLANT_CODE').AsString;
          cdsHoliday.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime :=
            qryWork.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
          cdsHoliday.FieldByName('SHIFT_NUMBER').AsInteger :=
            qryWork.FieldByName('SHIFT_NUMBER').AsInteger;
          cdsHoliday.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
            qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          cdsHoliday.FieldByName('EXPORT_CODE').AsString :=
            qryWork.FieldByName('EXPORT_CODE').AsString;
          cdsHoliday.FieldByName('DEPARTMENT_CODE').AsString :=
            qryWork.FieldByName('DEPARTMENT_CODE').AsString;
          cdsHoliday.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString :=
            qryWork.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString;
          cdsHoliday.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString :=
            qryWork.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString;
          cdsHoliday.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString :=
            qryWork.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString;
          cdsHoliday.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString :=
            qryWork.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString;
          cdsHoliday.Post;
          qryWork.Next;
        end;
      end;
      qryWork.Close;
    end;
  end;
  procedure DetermineAbsencetype;
  begin
    with DialogExportPayrollDM do
    begin
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'SELECT ' +
        '  ABSENCETYPE_CODE, EXPORT_CODE ' +
        'FROM ' +
        '  ABSENCETYPE ' +
        'ORDER BY ' +
        '  ABSENCETYPE_CODE'
        );
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          cdsAbsencetype.Insert;
          cdsAbsencetype.FieldByName('ABSENCETYPE_CODE').AsString :=
            qryWork.FieldByName('ABSENCETYPE_CODE').AsString;
          cdsAbsencetype.FieldByName('EXPORT_CODE').AsString :=
            qryWork.FieldByName('EXPORT_CODE').AsString;
          cdsAbsencetype.Post;
          qryWork.Next;
        end;
      end;
      qryWork.Close;
    end;
  end;
  procedure DetermineAbsenceReason;
  begin
    with DialogExportPayrollDM do
    begin
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'SELECT ' +
        '  AR.ABSENCEREASON_CODE, AR.ABSENCETYPE_CODE ' +
        'FROM ' +
        '  ABSENCEREASON AR, ABSENCETYPE A ' +
        'WHERE ' +
        '  AR.ABSENCETYPE_CODE = A.ABSENCETYPE_CODE ' +
        'ORDER BY ' +
        '  AR.ABSENCEREASON_CODE, AR.ABSENCETYPE_CODE'
        );
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          cdsAbsenceReason.Insert;
          cdsAbsenceReason.FieldByName('ABSENCEREASON_CODE').AsString :=
            qryWork.FieldByName('ABSENCEREASON_CODE').AsString;
          cdsAbsenceReason.FieldByName('ABSENCETYPE_CODE').AsString :=
            qryWork.FieldByName('ABSENCETYPE_CODE').AsString;
          cdsAbsenceReason.Post;
          qryWork.Next;
        end;
      end;
      qryWork.Close;
    end;
  end;
  function DetermineExportCode: String;
  var
    AbsencetypeCode: String;
  begin
    Result := '';
    AbsencetypeCode := '';
    with DialogExportPayrollDM do
    begin
      if cdsEmplDays.FieldByName('WORK').AsInteger > 0 then
        AbsencetypeCode := ExportCodeDays
      else
        if cdsEmplDays.FieldByName('TFT').AsInteger > 0 then
          AbsencetypeCode := TIMEFORTIMEAVAILABILITY
        else
          if cdsEmplDays.FieldByName('BANK').AsInteger > 0 then
            AbsencetypeCode := RSNB
          else
            if cdsEmplDays.FieldByName('ILL').AsInteger > 0 then
              AbsencetypeCode := ILLNESS
            else
              if cdsEmplDays.FieldByName('PAID').AsInteger > 0 then
                AbsencetypeCode := PAID
              else
                if cdsEmplDays.FieldByName('UNPAID').AsInteger > 0 then
                  AbsencetypeCode := UNPAID
                else
                  if cdsEmplDays.FieldByName('WTR').AsInteger > 0 then
                    AbsencetypeCode := WTR
                  else
                    if cdsEmplDays.FieldByName('LABOUR').AsInteger > 0 then
                      AbsencetypeCode := LABOURTHERAPY
                    else
                      if cdsEmplDays.FieldByName('MAT').AsInteger > 0 then
                        AbsencetypeCode := MATERNITYLEAVE
                      else
                        if cdsEmplDays.FieldByName('LAY').AsInteger > 0 then
                          AbsencetypeCode := LAY_DAYS;
      if AbsencetypeCode <> '' then
      begin
        // This is an exportcode that comes from 'EXPORTPAYROLL'-field.
        if AbsencetypeCode = ExportCodeDays then
          Result := ExportCodeDays
        else
        begin
          if cdsAbsencetype.FindKey([AbsencetypeCode]) then
            Result := cdsAbsencetype.FieldByName('EXPORT_CODE').AsString
          else
            Result := AbsencetypeCode;
        end;
      end;
    end;
    if Length(Result) > 3 then
      Result := Copy(Result, 1, 3);
  end;
  procedure PreCheckMaxSalary(const DateMin, DateMax: TDateTime;
    const PlantFrom, PlantTo: String;
    const EmployeeFrom, EmployeeTo: Integer);
  var
    SelectStr: String;
  begin
    with DialogExportPayRollDM do
    begin
      // Look for records in MaxSalaryEarning in given range.
      SelectStr :=
        'SELECT ' +
        '  MSE.EMPLOYEE_NUMBER, ' +
        '  SUM(MSE.EARNED_MAX_SALARY_MINUTES) AS SUMMIN ' +
        'FROM ' +
        '  MAXSALARYEARNING MSE, EMPLOYEE EMP ' +
        'WHERE ' +
        '  MSE.EMPLOYEE_NUMBER = EMP.EMPLOYEE_NUMBER AND ' +
        '  (MSE.MAX_SALARY_DATE >= :DATEMIN AND ' +
        '  MSE.MAX_SALARY_DATE <= :DATEMAX)  AND ' +
        '  (EMP.PLANT_CODE >= :PLANTFROM AND ' +
        '  EMP.PLANT_CODE <= :PLANTTO) ';
      if PlantFrom = PlantTo then
        SelectStr := SelectStr +
          '  AND (MSE.EMPLOYEE_NUMBER >= :EMPFROM ' +
          '  AND MSE.EMPLOYEE_NUMBER <= :EMPTO) ';
      SelectStr := SelectStr +
        'GROUP BY ' +
        '  MSE.EMPLOYEE_NUMBER';
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(SelectStr);
      qryWork.ParamByName('DATEMIN').AsDateTime := DateMin;
      qryWork.ParamByName('DATEMAX').AsDateTime := DateMax;
      qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
      qryWork.ParamByName('PLANTTO').AsString := PlantTo;
      if PlantFrom = PlantTo then
      begin
        qryWork.ParamByName('EMPFROM').AsInteger := EmployeeFrom;
        qryWork.ParamByName('EMPTO').AsInteger := EmployeeTo;
      end;
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          if tblMaxSalaryBalance.
            FindKey([qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger]) then
          begin
            tblMaxSalaryBalance.Edit;
            tblMaxSalaryBalance.
              FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger :=
                tblMaxSalaryBalance.
                  FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger -
                    Round(qryWork.FieldByName('SUMMIN').AsFloat);
            tblMaxSalaryBalance.FieldByName('MUTATIONDATE').AsDateTime := Now;
            tblMaxSalaryBalance.FieldByName('MUTATOR').AsString :=
              SystemDM.CurrentProgramUser;
            tblMaxSalaryBalance.Post;
          end;
          qryWork.Next;
        end;
      end;
      // Now delete the MaxSalaryEarning records in given range.
      // First close the table.
      tblMaxSalaryEarning.Close;
      qryWork.Close;
      qryWork.SQL.Clear;
      qryWork.SQL.Add(
        'DELETE FROM MAXSALARYEARNING ' +
        'WHERE ' +
        '  (MAX_SALARY_DATE >= :DATEMIN AND ' +
        '  MAX_SALARY_DATE <= :DATEMAX)  AND ' +
        '  (EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' +
        '  EMPLOYEE_NUMBER <= :EMPLOYEETO) '
        );
      qryWork.ParamByName('DATEMIN').AsDateTime := DateMin;
      qryWork.ParamByName('DATEMAX').AsDateTime := DateMax;
      qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      qryWork.ExecSQL;
      // Now re-open table, because 'PreCheckMaxSalary' will
      // have delete records from it.
      tblMaxSalaryEarning.Open;
    end;
  end;
  // Store the salary-part (in minutes) that came above 'maximum salary'.
  procedure MaxSalaryUpdate(
    const EmployeeNumber: Integer;
    const ProdHourEmployeeDate: TDateTime;
    const SalaryMinute: Integer);
  begin
    with DialogExportPayrollDM do
    begin
      // First check and update table MAXSALARYBALANCE
      with tblMaxSalaryBalance do
      begin
        if not FindKey([EmployeeNumber]) then
        begin
          Insert;
          FieldByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
          FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger := SalaryMinute;
          FieldByName('CREATIONDATE').AsDateTime := Now;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end
        else
        begin
          Edit;
          FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger :=
            FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger + SalaryMinute;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end;
      end;
      // Then check and update table MAXSALARYEARNING
      with tblMaxSalaryEarning do
      begin
        if not FindKey([ProdHourEmployeeDate, EmployeeNumber]) then
        begin
          Insert;
          FieldByName('MAX_SALARY_DATE').AsDateTime := ProdHourEmployeeDate;
          FieldByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
          FieldByName('EARNED_MAX_SALARY_MINUTES').AsInteger := SalaryMinute;
          FieldByName('CREATIONDATE').AsDateTime := Now;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end
        else
        begin
          Edit;
          FieldByName('EARNED_MAX_SALARY_MINUTES').AsInteger :=
            FieldByName('EARNED_MAX_SALARY_MINUTES').AsInteger + SalaryMinute;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end;
      end;
    end;
  end;
  function CalculateSalary(const HourlyWage: Double;
    const BonusInMoney: Boolean; const BonusPercentage: Double;
    const ProdMin: Double): Double;
  begin
    if BonusInMoney then
      Result := (ProdMin * (1 + BonusPercentage / 100)) / 60 * HourlyWage
    else
      Result := (ProdMin / 60) * HourlyWage;
  end;
  function CheckEmpMaxSalary(
    const EmployeeNumber: Integer;
    const ProdHourEmployeeDate: TDateTime;
    const MaximumSalary: Double;
    const HourlyWage: Double;
    const BonusInMoney: Boolean;
    const BonusPercentage: Double;
    var ProdMin: Double): Boolean;
  var
    Salary, TotalSalary: Double;
    SpareMin: Integer;
  begin
    Result := True;
    Salary := CalculateSalary(HourlyWage, BonusInMoney, BonusPercentage,
      ProdMin);
    SpareMin := 0;
    with DialogExportPayrollDM do
    begin
      if (MaximumSalary > 0) and (HourlyWage > 0) then
      begin
        // MR:03-03-2005 Following is WRONG! more than 1 employee is
        // selected the 'cdsEmpSalary' is not empty anymore!
(*
        // If cdsEmpSalary is empty we also must check the salary!
        // Because the first salary can already be higher then maximum.
        if cdsEmpSalary.FindKey([EmployeeNumber]) or
          cdsEmpSalary.IsEmpty then
        begin
          if not cdsEmpSalary.IsEmpty then
            TotalSalary := cdsEmpSalary.FieldByName('SALARY').AsFloat
          else
            TotalSalary := 0;
*)
        // MR:03-03-2005
        TotalSalary := 0;
        if cdsEmpSalary.FindKey([EmployeeNumber]) then
          TotalSalary := cdsEmpSalary.FieldByName('SALARY').AsFloat;
        Result := (TotalSalary + Salary <= MaximumSalary);
        if not Result then
        begin
          // Calculate the minutes that fit till maximumsalary is reached.
          // Because hourlywage is wage per hour, multiply result with 60
          SpareMin :=
            Trunc(60 * (MaximumSalary - TotalSalary) / HourlyWage);
          if SpareMin < 0 then
            SpareMin := 0;
          // Here we store was was above the maxsalary.
          MaxSalaryUpdate(EmployeeNumber, ProdHourEmployeeDate,
            Round(ProdMin - SpareMin));
        end;
      end;
    end;
    // A part of 'ProdMin' can be exported for the employee.
    if SpareMin > 0 then
    begin
      ProdMin := SpareMin;
      Result := True;
    end;
    // Store total of salary for an employee.
    if (MaximumSalary > 0) and (HourlyWage > 0) then
      cdsEmpSalaryUpdate(EmployeeNumber, Round(ProdMin), Salary);
  end;
  // For the employee: are there salary-minutes left?
  procedure DetermineMaxSalaryLeft(
    const DateMax: TDateTime;
    const PlantFrom, PlantTo: String;
    const EmployeeFrom, EmployeeTo: Integer);
  var
    SalaryLeft, MaximumSalary, HourlyWage, EmployeeSalary: Double;
    MinutesLeft: Integer;
    ExportCode: String;
    EmployeeFound: Boolean;
    SelectStr: String;
    procedure SalaryBalanceUpdate(
      const EmployeeNumber: Integer;
      MinutesLeft: Integer);
    begin
      with DialogExportPayrollDM do
      begin
        with tblMaxSalaryBalance do
        begin
          Edit;
          FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger :=
            FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger -
              MinutesLeft;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end;
        with tblMaxSalaryEarning do
        begin
          Insert;
          FieldByName('MAX_SALARY_DATE').AsDateTime := DateMax;
          FieldByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeNumber;
          FieldByName('EARNED_MAX_SALARY_MINUTES').AsInteger :=
            (MinutesLeft * -1);
          FieldByName('CREATIONDATE').AsDateTime := Now;
          FieldByName('MUTATIONDATE').AsDateTime := Now;
          FieldByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          Post;
        end;
      end;
    end;
    procedure DetermineContractgroupValues(
      const EmployeeNumber: Integer;
      var ExportCode: String;
      var MaximumSalary: Double);
    begin
      with DialogExportPayrollDM do
      begin
        qryWork.Close;
        qryWork.SQL.Clear;
        qryWork.SQL.Add(
          'SELECT ' +
          '  H.EXPORT_CODE, C.MAXIMUM_SALARY ' +
          'FROM ' +
          '  EMPLOYEE E, CONTRACTGROUP C, HOURTYPE H ' +
          'WHERE ' +
          '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' +
          '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND ' +
          '  C.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
          );
        qryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          EmployeeNumber;
        qryWork.Open;
        if not qryWork.IsEmpty then
        begin
          ExportCode := qryWork.FieldByName('EXPORT_CODE').AsString;
          MaximumSalary := qryWork.FieldByName('MAXIMUM_SALARY').AsFloat;
        end;
        qryWork.Close;
      end;
    end;
    procedure ActionMaxSalary(EmployeeNumber: Integer;
      EmployeeSalary: Double;
      MaxSalaryBalanceMinutes: Integer);
    begin
      DetermineContractgroupValues(EmployeeNumber, ExportCode,
        MaximumSalary);
      if (ExportCode <> '') and (MaximumSalary > 0) then
      begin
        if EmployeeSalary < MaximumSalary then
        begin
          HourlyWage := DetermineHourlyWage(EmployeeNumber, DateMax);
            SalaryLeft := MaximumSalary - EmployeeSalary;
          if HourlyWage > 0 then
          begin
            MinutesLeft := Trunc((SalaryLeft / HourlyWage) * 60);
            if MinutesLeft > MaxSalaryBalanceMinutes then
              MinutesLeft := MaxSalaryBalanceMinutes;
            cdsEmpExportLineUpdate(EXPORT_HOURS, EmployeeNumber,
              ExportCode, '', '', MinutesLeft, 0, 'N');
            // Edit the maxsalarybalance-record(s).
            SalaryBalanceUpdate(EmployeeNumber, MinutesLeft);
          end;
        end;
      end;
    end;
  begin
    with DialogExportPayrollDM do
    begin
      // MR:02-02-2005 Use query so only employees are selected
      // from the plantfrom-plantto-selection.
      SelectStr :=
        'SELECT ' +
        '  MSB.EMPLOYEE_NUMBER ' +
        'FROM ' +
        '  MAXSALARYBALANCE MSB, EMPLOYEE EMP ' +
        'WHERE ' +
        '  MSB.EMPLOYEE_NUMBER = EMP.EMPLOYEE_NUMBER AND ' +
        '  EMP.PLANT_CODE >= :PLANTFROM AND ' +
        '  EMP.PLANT_CODE <= :PLANTTO ';
      if PlantFrom = PlantTo then
        SelectStr := SelectStr +
          ' AND EMP.EMPLOYEE_NUMBER >= :EMPFROM ' +
          ' AND EMP.EMPLOYEE_NUMBER <= :EMPTO ';
      qryMaxSalaryBalance.Close;
      qryMaxSalaryBalance.SQL.Clear;
      qryMaxSalaryBalance.SQL.Add(SelectStr);
      qryMaxSalaryBalance.ParamByName('PLANTFROM').AsString := PlantFrom;
      qryMaxSalaryBalance.ParamByName('PLANTTO').AsString := PlantTo;
      if PlantFrom = PlantTo then
      begin
        qryMaxSalaryBalance.ParamByName('EMPFROM').AsInteger := EmployeeFrom;
        qryMaxSalaryBalance.ParamByName('EMPTO').AsInteger := EmployeeTo;
      end;
      qryMaxSalaryBalance.Open;
      if not qryMaxSalaryBalance.IsEmpty then
      begin
        qryMaxSalaryBalance.First;
        while not qryMaxSalaryBalance.Eof do
        begin
          tblMaxSalaryBalance.FindKey([
            qryMaxSalaryBalance.FieldByName('EMPLOYEE_NUMBER').AsInteger]);
          EmployeeFound := False;
          EmployeeSalary := 0;
          if cdsEmpSalary.FindKey([
            tblMaxSalaryBalance.FieldByName('EMPLOYEE_NUMBER').AsInteger]) then
          begin
            EmployeeFound := True;
            EmployeeSalary := cdsEmpSalary.FieldByName('SALARY').AsFloat;
          end
          else // Employee can have something left in 'max-salary-balance'
            if ((tblMaxSalaryBalance.
              FieldByName('EMPLOYEE_NUMBER').AsInteger >= EmployeeFrom) and
              (tblMaxSalaryBalance.
              FieldByName('EMPLOYEE_NUMBER').AsInteger <= EmployeeTo)) then
            begin
              EmployeeFound := True;
              EmployeeSalary := 0;
            end;
          if EmployeeFound then
            ActionMaxSalary(
              tblMaxSalaryBalance.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              EmployeeSalary,
              tblMaxSalaryBalance.
                FieldByName('MAX_SALARY_BALANCE_MINUTES').AsInteger);
          qryMaxSalaryBalance.Next;
        end;
      end;
    end;
  end;
  procedure DetermineAbsenceHours;
  var
    IndexHourType, Empl, HourType, Min{, MinExported}: Integer;
    ExportCode, Wage_Bonus_YN, WorkspotCode, Remark: String;
    MinDouble: Double;
    ExportHours: Boolean;
    function DetermineEmployee(IndexHourType: Integer): Integer;
    var
      ListString: String;
      PosSpace: Integer;
    begin
      ListString := DialogExportPayrollDM.FEmplHourType.Strings[IndexHourType];
      PosSpace := Pos(' ', ListString);
      Result := StrToInt(Copy(ListString, 0, PosSpace - 1));
    end;
  begin
    for IndexHourType := 0 to
      DialogExportPayrollDM.FEmplHourType.Count - 1 do
    begin
      if (IndexHourType mod 2) = 0 then
      begin
        Empl := DetermineEmployee(IndexHourType);
        if IsEmpl(DialogExportPayrollDM.FEmplHourType, IndexHourType, Empl,
          HourType, Min, Wage_Bonus_YN, ExportCodeDummy) then
        begin
          DialogExportPayrollDM.HourTypeFind(HourType);
{          DialogExportPayrollDM.ClientDataSetHourType.FindKey([HourType]);}
          ExportCode := Copy(DialogExportPayrollDM.ClientDataSetHourType.
            FieldByName('EXPORT_CODE').AsString, 1, 3);
          if (ExportCode <> '') then
          begin
            BonusPercentage := DialogExportPayrollDM.ClientDataSetHourType.
              FieldByName('BONUS_PERCENTAGE').AsFloat;
            WorkspotCode := ' ';
            Remark := ' ';
            DialogExportPayRollDM.qryWork.Close;
            DialogExportPayRollDM.qryWork.SQL.Clear;
            DialogExportPayRollDM.qryWork.SQL.Add(
              'SELECT ' +
              '  C.BONUS_IN_MONEY_YN, C.MAXIMUM_SALARY, ' +
              '  C.EXPORT_SR_HOURS_YN ' +
              'FROM ' +
              '  EMPLOYEE E, CONTRACTGROUP C ' +
              'WHERE ' +
              '  E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE AND ' +
              '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER '
              );
            DialogExportPayRollDM.qryWork.
              ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
            DialogExportPayRollDM.qryWork.Open;
            if not DialogExportPayRollDM.qryWork.IsEmpty then
            begin
              // RV045.1.
              if DialogExportPayrollDM.ContractGroupTFT then
                BonusInMoney :=
                  DialogExportPayRollDM.qryWork.
                    FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y'
              else
                BonusInMoney := DialogExportPayrollDM.ClientDataSetHourType.
                  FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y';
              MaximumSalary :=
                DialogExportPayRollDM.qryWork.
                  FieldByName('MAXIMUM_SALARY').AsFloat;
              ExportHours :=
                DialogExportPayRollDM.qryWork.
                  FieldByName('EXPORT_SR_HOURS_YN').AsString = 'Y';
            end
            else
            begin
              BonusInMoney := False;
              ExportHours := True;
              MaximumSalary := 0;
            end;
            DialogExportPayRollDM.qryWork.Close;
            // Get hourly wage from employee contract.
            HourlyWage := DetermineHourlyWage(Empl, DateMax);
            MinDouble := Min;
            if ExportHours then
              if CheckEmpMaxSalary(Empl, DateMin,
                MaximumSalary, HourlyWage, BonusInMoney, BonusPercentage,
                  MinDouble) then
                cdsEmpExportLineUpdate(EXPORT_HOURS, Empl, ExportCode,
                  WorkspotCode, Remark, Round(MinDouble), 0, 'N');
          end; // if (ExportCode <> '') then
        end; // if IsEmpl()
      end;
    end;
  end;
begin
  // 20013430.
  FExportList1 := nil;
  FExportList2 := nil;
  SaveDecimalSeparator := DecimalSeparator;
  try
    SystemDM.Pims.StartTransaction;

    // 20013430. Use 2 lists for 2 exportgroups.
    FExportList1 := TStringList.Create;
    FExportList2 := TStringList.Create;

    FExportList.Clear;

    ExportCheckUserSelections;

     // Get period
    DetermineDateMinMax(DateMin, DateMax);

    // Get some Export Payroll Settings
    DetermineExportPayrollSettings(MonthGroupEfficiency, ExportCodeDays,
      Efficiency100Min);

    // Export hours based on PRODHOURPEREMPPERTYPE
    with DialogExportPayRollDM do
    begin
      cdsEmplDays.EmptyDataSet;
      FEmplHourType.Clear;
      FEmplHourTypeAbsence.Clear;
      FEmplSort.Clear;
      FEmpExtraPayment.Clear;
      FEmpQuaranteedHrs.Clear;
      FEmpHourTypeWage.Clear;
      FEmpHourSick.Clear;

      tblMaxSalaryBalance.Open;
      tblMaxSalaryEarning.Open;
      // Store 'worked' in clientdataset
      FillListOfEmployeeSHE(DateMin, DateMax,
        PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
      // Store absence in clientdataset
      FillListOfEmployeeAHE(DateMin, DateMax,
        PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
      // Store holiday in clientdataset
      cdsHoliday.CreateDataSet;
      cdsHoliday.LogChanges := False;
      DetermineHoliday;
      // Store absencetypes in clientdataset
      cdsAbsencetype.CreateDataSet;
      cdsAbsencetype.LogChanges := False;
      // Store absencereason in clientdataset
      cdsAbsenceReason.CreateDataSet;
      cdsAbsenceReason.LogChanges := False;
      DetermineAbsencetype;
      DetermineAbsenceReason;
      cdsEmpSalary.CreateDataSet;
      cdsEmpSalary.LogChanges := False;
      cdsEmpExportLine.CreateDataSet;
      cdsEmpExportLine.LogChanges := False;
      // MR:13-03-2007 Order 550443.
      // Combine Group Efficiency + Employee Efficiency
      if MonthGroupEfficiency then
      begin
        cdsGroupEfficiency.CreateDataSet;
        cdsGroupEfficiency.LogChanges := False;
        FillCDSGroupEfficiency(DateMin);
        cdsGroupEmployeeEfficiency.CreateDataSet;
        cdsGroupEmployeeEfficiency.LogChanges := False;
        FillCDSGroupEmployeeEfficiency(DateMin);
      end;
      // Check if max-salary tables have values from a previous action.
      PreCheckMaxSalary(DateMin, DateMax, PlantFrom, PlantTo,
        EmployeeFrom, EmployeeTo);

      // MR:16-06-2005 Order 550402. Do this FIRST, to ensure the 'absencehours' like
      // holiday, bank-holidays are always shown in the export.
      // Determine Absence HOURS to export.
      DetermineAbsenceHours;

      // Get data from 'prodhourperemplpertype'
      // MR:9-12-2004 Only select plant-code from 'employee' with
      // PlantFrom and PlantTo.
      qryWork.Close;
      qryWork.SQL.Clear;
      // RV045.1. Added HOURTYPE.BONUS_IN_MONEY_YN.
      SelectStr :=
        'SELECT ' + NL +
        '  PH.PRODHOUREMPLOYEE_DATE, ' + NL +
        '  PH.EMPLOYEE_NUMBER, EMP.REMARK, HT.EXPORT_CODE, ' + NL +
        '  PH.HOURTYPE_NUMBER, ' + NL +
        '  CG.BONUS_IN_MONEY_YN, CG.MAXIMUM_SALARY, CG.EXPORT_SR_HOURS_YN, ' + NL +
        '  HT.OVERTIME_YN, HT.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN, ' + NL +
        '  HT.BONUS_PERCENTAGE, PH.PLANT_CODE, PH.WORKSPOT_CODE, PH.JOB_CODE, ' + NL +
        '  EMP.CALC_BONUS_YN, ' + NL +
        '  SUM(PH.PRODUCTION_MINUTE) AS PRODMIN ' + NL +
        'FROM ' + NL +
        '  PRODHOURPEREMPLPERTYPE PH, HOURTYPE HT, EMPLOYEE EMP, ' + NL +
        '  CONTRACTGROUP CG ' + NL +
        'WHERE ' + NL +
        '  PH.EMPLOYEE_NUMBER = EMP.EMPLOYEE_NUMBER AND ' + NL +
        '  EMP.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE AND ' + NL +
        '  PH.HOURTYPE_NUMBER = HT.HOURTYPE_NUMBER AND ' + NL +
        '  (HT.EXPORT_CODE IS NOT NULL) AND ' + NL +
        '  PH.PRODHOUREMPLOYEE_DATE >= :DATEMIN AND ' + NL +
        '  PH.PRODHOUREMPLOYEE_DATE <= :DATEMAX AND ' + NL +
        '  EMP.PLANT_CODE >= :PLANTFROM AND ' + NL +
        '  EMP.PLANT_CODE <= :PLANTTO ' + NL;
      if PlantFrom = PlantTo then
        SelectStr := SelectStr +
          '  AND PH.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
          '  PH.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
      SelectStr := SelectStr +
        'GROUP BY ' + NL +
        '  PH.PRODHOUREMPLOYEE_DATE, PH.EMPLOYEE_NUMBER, EMP.REMARK, ' + NL +
        '  HT.EXPORT_CODE, PH.HOURTYPE_NUMBER, ' + NL +
        '  CG.BONUS_IN_MONEY_YN, CG.MAXIMUM_SALARY, CG.EXPORT_SR_HOURS_YN, ' + NL +
// RV094.2. Bugfix. Comma-problem.
//        '  HT.OVERTIME_YN, , HT.BONUS_IN_MONEY_YN, ' + NL +
        '  HT.OVERTIME_YN, HT.BONUS_IN_MONEY_YN, ' + NL +
        '  HT.BONUS_PERCENTAGE, PH.PLANT_CODE, PH.WORKSPOT_CODE, PH.JOB_CODE, ' + NL +
        '  EMP.CALC_BONUS_YN ' + NL;
      // MR:03-03-2005 Order by added, so the output is easier to follow
      // during debugging.
      SelectStr := SelectStr + ' ' +
        'ORDER BY ' + NL +
        '  PH.EMPLOYEE_NUMBER, PH.PRODHOUREMPLOYEE_DATE ' + NL;
      qryWork.SQL.Add(SelectStr);
      qryWork.ParamByName('DATEMIN').AsDateTime := DateMin;
      qryWork.ParamByName('DATEMAX').AsDateTime := DateMax;
      qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
      qryWork.ParamByName('PLANTTO').AsString := PlantTo;
      if PlantFrom = PlantTo then
      begin
        qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      end;
// qryWork.SQL.SaveToFile('c:\temp\exportpayroll_SR1.txt');
      qryWork.Open;
      if not qryWork.IsEmpty then
      begin
        qryWork.First;
        while not qryWork.Eof do
        begin
          EmployeeNumber := qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          ProdHourEmployeeDate :=
            qryWork.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime;
          Remark := qryWork.FieldByName('REMARK').AsString;
          ExportCode := qryWork.FieldByName('EXPORT_CODE').AsString;
          // RV045.1.
          if DialogExportPayrollDM.ContractGroupTFT then
            BonusInMoney := (qryWork.FieldByName('BONUS_IN_MONEY_YN').AsString =
              'Y')
          else
            BonusInMoney := (qryWork.FieldByName('HT_BONUS_IN_MONEY_YN').AsString =
              'Y');
          MaximumSalary := qryWork.FieldByName('MAXIMUM_SALARY').AsFloat;
          OverTime :=
            (qryWork.FieldByName('OVERTIME_YN').AsString = 'Y');
          BonusPercentage := qryWork.FieldByName('BONUS_PERCENTAGE').AsFloat;
          ExportHours :=
            (qryWork.FieldByName('EXPORT_SR_HOURS_YN').AsString = 'Y');
          PlantCode := qryWork.FieldByName('PLANT_CODE').AsString;
          WorkspotCode := qryWork.FieldByName('WORKSPOT_CODE').AsString;
          JobCode := qryWork.FieldByName('JOB_CODE').AsString;
          ProdMin := qryWork.FieldByName('PRODMIN').AsInteger;
          // Get hourly wage from employee contract.
          HourlyWage := DetermineHourlyWage(EmployeeNumber, DateMax);
          CalcBonusYN := qryWork.FieldByName('CALC_BONUS_YN').AsString;
          // Overrule Efficiency depending on OverTime and BonusPercentage
          BonusYN := 'N';
          if CalcBonusYN = 'Y' then
          begin
            if OverTime or (BonusPercentage > 0) then
            begin
              Efficiency := 100 + BonusPercentage;
              BonusYN := 'Y';
            end
            else
            begin
              // MR:13-03-2007 Order 550443.
              // Combine Group Efficiency + Employee Efficiency
              if MonthGroupEfficiency then
              begin
                Efficiency := 0;
                if not DetermineEfficiency(PlantCode, WorkspotCode,
                  Efficiency) then
                  DetermineEmployeeEfficiency(PlantCode, WorkspotCode,
                    JobCode, EmployeeNumber, Efficiency);
              end;
(*
// KME:26-08-2005 Order 550405
//               if Efficiency < 100 then
                if Efficiency <= 0 then
                  Efficiency := 100;
*)
// MRA:22-09-2005 Order 550405 - Use ExportPayroll-setting to determine
// what should be done here with efficiency.
              if Efficiency100Min then // Use old method
              begin
                if Efficiency < 100 then
                  Efficiency := 100;
              end
              else
              begin // use new method
                if Efficiency <= 0 then
                  Efficiency := 100;
              end;
            end;
          end
          else
            Efficiency := 100;
          // Calculate the salary.
          // Check if salary if going above a maximum.
          if ExportHours then
            if CheckEmpMaxSalary(EmployeeNumber, ProdHourEmployeeDate,
               MaximumSalary, HourlyWage, BonusInMoney, BonusPercentage,
              ProdMin) then
              cdsEmpExportLineUpdate(EXPORT_HOURS, EmployeeNumber,
                ExportCode, WorkspotCode,
                Remark, ProdMin, Efficiency, BonusYN);
          qryWork.Next;
        end;
      end;

      // MR:16-06-2005 Order 550402. Moved to start, so the absence-hours will
      // first be exported.
{
      // Determine Absence HOURS to export.
      DetermineAbsenceHours;
}

      // Determine if salary is under max-salary.
      // If this so, then look if the 'max-salary'-tables
      // has values that can be exported.
      DetermineMaxSalaryLeft(DateMax, PlantFrom, PlantTo,
        EmployeeFrom, EmployeeTo);

      // Now determine DAYS to export.
      // Get the holiday days.
      if not cdsHoliday.IsEmpty then
      begin
        cdsHoliday.First;
        while not cdsHoliday.Eof do
        begin
          // First determine Holiday day-period
          HolidayDayPeriod := DetermineHolidayDayPeriod(cdsHoliday);
          ExportCode := cdsHoliday.FieldByName('EXPORT_CODE').AsString;
          if (HolidayDayPeriod > 0) and (ExportCode <> '') then
          begin
            cdsEmpExportLineUpdate(EXPORT_DAYS,
              cdsHoliday.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              ExportCode, '', '', HolidayDayPeriod, 0, 'N');
          end;
          cdsHoliday.Next;
        end; // while not cdsHoliday.Eof do
      end; // if not qryHoliday.IsEmpty then
      // Determine Absence days.
      if not cdsEmplDays.IsEmpty then
      begin
        cdsEmplDays.First;
        while not cdsEmplDays.Eof do
        begin
          ExportCode := DetermineExportCode;
          if (ExportCode <> '') then
          begin
            Days := cdsEmplDays.FieldByName('WORK').AsInteger +
              cdsEmplDays.FieldByName('TFT').AsInteger +
              cdsEmplDays.FieldByName('BANK').AsInteger +
              cdsEmplDays.FieldByName('ILL').AsInteger +
              cdsEmplDays.FieldByName('PAID').AsInteger +
              cdsEmplDays.FieldByName('UNPAID').AsInteger +
              cdsEmplDays.FieldByName('WTR').AsInteger +
              cdsEmplDays.FieldByName('LABOUR').AsInteger +
              cdsEmplDays.FieldByName('MAT').AsInteger +
              cdsEmplDays.FieldByName('LAY').AsInteger;
            if Days > 0 then
            begin
              if cdsHoliday.Locate('EMPLOYEE_NUMBER;EMPLOYEEAVAILABILITY_DATE',
                VarArrayOf([cdsEmplDays.FieldByName('EMPLOYEE_NUMBER').AsInteger,
                  cdsEmplDays.FieldByName('EDATE').AsDateTime]), []) then
              begin
                HolidayDayPeriod := DetermineHolidayDayPeriod(cdsHoliday);
                // Is part of the day a holiday?
                // then the rest of the day was something else.
                if HolidayDayPeriod < 1 then
                  Days := 1 - HolidayDayPeriod;
              end;
              cdsEmpExportLineUpdate(EXPORT_DAYS,
                cdsEmplDays.FieldByName('EMPLOYEE_NUMBER').AsInteger,
                ExportCode, '', '', Days, 0, 'N');
            end;
          end; // if (ExportCode <> '') then
          cdsEmplDays.Next;
        end;
      end;

      // Export all data
      if not cdsEmpExportLine.IsEmpty then
      begin
        cdsEmpExportLine.First;
        while not cdsEmpExportLine.Eof do
        begin
          EmployeeNumber :=
            cdsEmpExportLine.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          ExportCode :=
            cdsEmpExportLine.FieldByName('EXPORT_CODE').AsString;
          WorkspotCode :=
            cdsEmpExportLine.FieldByName('WORKSPOT_CODE').AsString;
          Remark :=
            cdsEmpExportLine.FieldByName('REMARK').AsString;
          if cdsEmpExportLine.
            FieldByName('DAYS_YN').AsString = EXPORT_DAYS then
            HoursOrDays :=
              cdsEmpExportLine.FieldByName('AMOUNT').AsFloat
          else
            HoursOrDays :=
              cdsEmpExportLine.FieldByName('AMOUNT').AsFloat / 60;
          Efficiency :=
            cdsEmpExportLine.FieldByName('EFFICIENCY').AsFloat;
          // 20013430
          ExportGroup := cdsEmpExportLine.FieldByName('EXPORTGROUP').AsString;
          // Force decimal separator to a '.'
          DecimalSeparator := '.';
          // Make positive, otherwise PlusMin + FillZero gives wrong result.
          ABSHoursOrDays := HoursOrDays;
          if HoursOrDays < 0 then
            ABSHoursOrDays := HoursOrDays * -1;
          ABSEfficiency := Efficiency;
          if Efficiency < 0 then
            ABSEfficiency := Efficiency * -1;
          ExportLine :=
            FillZero(IntToStr(EmployeeNumber), 6) +
            FormatDateTime('yyyymmdd', DateMin) +
            FormatDateTime('yyyymmdd', DateMax) +
            FillZero(ExportCode, 3) +
            '99' +
            PlusMin(HoursOrDays) + // Hours or Days
            FillZero(Format('%1.3f', [ABSHoursOrDays]), 12) + // Hours or Days
            '+' +
            FillZero(Format('%1.2f', [0.0]), 11) +
            '+' +
            FillZero(Format('%1.2f', [0.0]), 11) +
            Format('%-8s', [WorkspotCode]) +
            Format('%-12s', [' ']) +
            PlusMin(Efficiency) +
            FillZero(Format('%1.3f', [ABSEfficiency]), 12) +
            FillZero(Format('%1.3f', [0.0]), 10) +
            Format('%-20s', [Copy(Remark, 1, 20)]);
          if ExportGroup = '1' then
            FExportList1.Add(ExportLine)
          else
            if ExportGroup = '2' then
              FExportList2.Add(ExportLine)
            else
              FExportList.Add(ExportLine);
          cdsEmpExportLine.Next;
        end;
      end;
      // End of export
      tblMaxSalaryBalance.Close;
      tblMaxSalaryEarning.Close;
      cdsHoliday.EmptyDataSet;
      cdsHoliday.Close;
      cdsAbsencetype.EmptyDataSet;
      cdsAbsencetype.Close;
      cdsAbsenceReason.EmptyDataSet;
      cdsAbsenceReason.Close;
      cdsEmpSalary.EmptyDataSet;
      cdsEmpSalary.Close;
      // MR:13-03-2007 Order 550443.
      // Combine Group Efficiency + Employee Efficiency.
      if MonthGroupEfficiency then
      begin
        cdsGroupEfficiency.EmptyDataSet;
        cdsGroupEfficiency.Close;
        cdsGroupEmployeeEfficiency.EmptyDataSet;
        cdsGroupEmployeeEfficiency.Close;
      end;
      if FExportList1.Count > 0 then
        SaveExportFile(FExportList1, 'LO_IMP1');
      if FExportList2.Count > 0 then
        SaveExportFile(FExportList2, 'LO_IMP2');
      if FExportList.Count > 0 then
        SaveExportFile(FExportList, 'LO_IMP');
      // Show a report of the results.
      if (FExportList1.Count > 0) or (FExportList2.Count > 0) or
        (FExportList.Count > 0) then // 20011800
      begin
        if SystemDM.UseFinalRun then // 20011800
          if not FinalRunExport then
            ReportExportPayrollSRQR.Caption :=
              ReportExportPayrollSRQR.Caption + ' -  ' +
                SPimsFinalRunTestRun;
      end;
      // 20013430. Per exportgroup.
      if FExportList1.Count > 0 then
      begin
        if ReportExportPayrollSRQR.QRSendReportParameters(
          PlantFrom, PlantTo,
          IntToStr(EmployeeFrom),
          IntToStr(EmployeeTo),
          Round(dxSpinEditYear.Value),
          Round(dxSpinEditMonth.Value),
          '1')
        then
          ReportExportPayrollSRQR.ProcessRecords;
      end;
      if FExportList2.Count > 0 then
      begin
        if ReportExportPayrollSRQR.QRSendReportParameters(
          PlantFrom, PlantTo,
          IntToStr(EmployeeFrom),
          IntToStr(EmployeeTo),
          Round(dxSpinEditYear.Value),
          Round(dxSpinEditMonth.Value),
          '2')
        then
          ReportExportPayrollSRQR.ProcessRecords;
      end;
      // Use this when the group was not set yet.
      if FExportList.Count > 0 then
      begin
        if ReportExportPayrollSRQR.QRSendReportParameters(
          PlantFrom, PlantTo,
          IntToStr(EmployeeFrom),
          IntToStr(EmployeeTo),
          Round(dxSpinEditYear.Value),
          Round(dxSpinEditMonth.Value),
          '3')
        then
          ReportExportPayrollSRQR.ProcessRecords;
      end;
      cdsEmpExportLine.EmptyDataSet;
      cdsEmpExportLine.Close;
    end;
    DecimalSeparator := SaveDecimalSeparator;
    if SystemDM.Pims.InTransaction then
      SystemDM.Pims.Commit;
  except
    on E: EDBEngineError do
    begin
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      DecimalSeparator := SaveDecimalSeparator;
      DisplayMessage(GetErrorMessage(E,PIMS_POSTING), mtError, [mbOK]);
    end;
  end;
  // 20013430.
  try
  finally
    FExportList1.Clear;
    FExportList2.Clear;
    FExportList1.Free;
    FExportList2.Free;
  end;
end; // ExportSR

// RV026. Export payroll for REINO.
procedure TDialogExportPayrollF.ExportREINO;
var
  ExportLine: String;
  DateMin, DateMax: TDateTime;
begin
  //
  // Init
  //
  // Check Selections
  FExportList.Clear;
  ExportCheckUserSelections;

   // Get period
  DetermineDateMinMax(DateMin, DateMax);

  // Define filename for export-file including period
  FNameExportFile := 'PERIOD' + FillZero(IntToStr(FPeriodNr), 2) + '.TXT';

  //add header line for excel file
(*
  ExportLine := IntToStr(FPeriodNr) + MySep +
    DialogExportPayrollDM.TableEP.FieldByName('CORPORATE_CODE').AsString;
  FExportList.Add(ExportLine);
*)

  //
  // Export data
  //
  with DialogExportPayRollDM do
  begin
    with qryExportREINO do
    begin
      Close;
      ParamByName('DATEFROM').AsDateTime := DateMin;
      ParamByName('DATETO').AsDateTime := DateMax;
      ParamByName('PLANTFROM').AsString := PlantFrom;
      ParamByName('PLANTTO').AsString := PlantTo;
      ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      Open;
      while not Eof do
      begin
        ExportLine :=
          Format('%-21s', [FieldByName('EMPLOYEE_NUMBER').AsString]) +
          Format('%-30.30s', [FieldByName('DEPT').AsString]) +
          Format('%-20.20s', [FieldByName('HOURTYPE').AsString]) +
          Format('%-9s',
            [MinCnv(RoundMinutes(15, FieldByName('SUM_MIN').AsInteger))]) +
          Format('%-9s',  [FieldByName('INCENTIVE').AsString]);
        FExportList.Add(ExportLine);
        Next;
      end;
      Close;
    end;
  end;

  //
  // Save and exit
  //
  SaveExportFile(FExportList, FNameExportFile);
  DialogExportPayrollDM.StorePeriod(dxSpinEditYear.IntValue,
    FPeriodNr, PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
end;

// RV050.8.
procedure TDialogExportPayrollF.tblPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// RV050.8.
procedure TDialogExportPayrollF.QueryEmplFromFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// RV050.8.
procedure TDialogExportPayrollF.QueryEmplToFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TDialogExportPayrollF.ExportADP;
begin
  inherited;
  DialogExportPayrollDM.CurrentExportType := etADP;
  // RV071.9. Switch to correct EP-record. (ADP).
  DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE',
    DialogExportPayrollDM.ExportTypeToString(etADP), []);

  ExportCheckUserSelections;

  // 20013723 Is 'M' for Month or 'P' for Period or 'W' for Week.
  if SystemDM.IsCLF then
    DialogExportPayrollDM.PeriodMonthSign := FTypeOfDialogExport;

  FExportList.Clear;
  if CreateIDExportFile then
  begin
    CreateExportDataFile;
    //car 8-11-2003
    if (FTypeOfDialogExport = EXPORTPAYROLL_WEEK) or
      (FTypeOfDialogExport = EXPORTPAYROLL_PERIOD) then
    begin
       RadioGroupSort.ItemIndex := 0; // week/period
       // 20013723.3 Change title of report.
       if SystemDM.IsCLF then
         ReportExportPayrollQR.Caption := SPimsExportPeriod;
    end
    else
    begin
      RadioGroupSort.ItemIndex := 1; // month
      // 20013723.3 Change title of report.
      if SystemDM.IsCLF then
        ReportExportPayrollQR.Caption := SPimsExportMonth;
    end;
    if SystemDM.UseFinalRun then // 20011800
      if not FinalRunExport then
        ReportExportPayrollQR.Caption :=
          ReportExportPayrollQR.Caption + ' -  ' +
            SPimsFinalRunTestRun;
    if ReportExportPayrollQR.QRSendReportParameters(
      PlantFrom, PlantTo,
      IntToStr(EmployeeFrom),
      IntToStr(EmployeeTo),
      Round(dxSpinEditYear.Value),
      RadioGroupSort.ItemIndex,
      Round(FPeriodNR),
      Round(dxSpinEditWeekMin.Value),
      Round(dxSpinEditWeekMax.Value),

      Round(dxSpinEditMonth.Value))
    then
      ReportExportPayrollQR.ProcessRecords;
   //   if not PeriodIsStored then
      DialogExportPayrollDM.StorePeriod(dxSpinEditYear.IntValue,
        FPeriodNr,
        PlantFrom, PlantTo,
        EmployeeFrom, EmployeeTo);
  end;
end; // ExportADP

procedure TDialogExportPayrollF.ExportATTENT;
begin
  inherited;
  // RV071.9. Switch to correct EP-record. (ATTENT).
  DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE',
    DialogExportPayrollDM.ExportTypeToString(etATTENT), []);

  ExportCheckUserSelections;

  FExportList.Clear;
  if not CreateIDExportFile then
    Exit;

  CreateExportDataFileAttent;

  if ReportExportPayrollQR.QRSendReportParameters(
    PlantFrom, PlantTo,
    IntToStr(EmployeeFrom),
    IntToStr(EmployeeTo),
    DateTimePickerMin.DateTime,
    DateTimePickerMax.DateTime)
  then
    ReportExportPayrollQR.ProcessRecords;

(*
  DialogExportPayrollDM.StorePeriod(dxSpinEditYear.IntValue,
    FPeriodNr,
    PlantFrom, PlantTo,
    EmployeeFrom, EmployeeTo);
*)
end; // ExportATTENT

procedure TDialogExportPayrollF.FillAll;
begin
  inherited;
  if UseBU then
    FillBusinessUnit;
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    if UseBU then
      CheckBoxAllBusinessUnits.Checked := False;
  end;

end;

// RV067.MRA.30
procedure TDialogExportPayrollF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  if SystemDM.UseFinalRun then
    CmbPlusPlantTo.Value := CmbPlusPlantFrom.Value;
  DetermineDialogSettingsATTENT(
    (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
    );
  if SystemDM.UseFinalRun then
    ShowFinalRunExportDate;
end;

// RV067.MRA.30
procedure TDialogExportPayrollF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  DetermineDialogSettingsATTENT(
    (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
    );
end;

// RV067.MRA.30 Switch between ADP and ATTENT settings for dialog.
procedure TDialogExportPayrollF.DetermineDialogSettingsATTENT(
  AOnePlantSelected: Boolean);
var
  IsAttent: Boolean;
begin
  // Only switch when ADP+ATTENT combination is used.
  if IsADP_ATTENT then
  begin
    IsAttent := False;
    if AOnePlantSelected then
      IsAttent := DialogExportPayrollDM.
        DetermineExportATTENTByPlant(GetStrValue(CmbPlusPlantFrom.Value));
    // Switch between ADP and ATTENT settings for dialog.
    if IsAttent then
      FExportType := etATTENT
    else
      FExportType := etADP;
    GroupBoxYear.Visible := not IsAttent;
//    Label22.Visible := not IsAttent;
    // RV095.5.
//    RadioGroupSort.Visible := not IsAttent;
//    dxSpinEditYear.Visible := not IsAttent;
    GroupBoxPeriod.Visible := not IsAttent;
    GroupBoxAttent.Visible := IsAttent;
    RadioGroupSort.Visible := not IsAttent; // 20011800
  end;
end;

// RV087.2.
procedure TDialogExportPayrollF.FillBusinessUnit;
begin
  if not UseBU then
    Exit;
  if CheckBoxAllBusinessUnits.Checked then
    BusinessUnitVisible(False)
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
      (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
    then
    begin
      BusinessUnitVisible(True);
      ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
        GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
      ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
        GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
      ComboBoxPlusBusinessFrom.Visible := True;
      ComboBoxPlusBusinessTo.Visible := True;
    end
    else
    begin
      BusinessUnitVisible(False);
      CheckBoxAllBusinessUnits.Checked := True;
    end;
  end;
end;

// RV087.2.
procedure TDialogExportPayrollF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessTo.DisplayValue :=
        ComboBoxPlusBusinessFrom.DisplayValue;
end;

// RV087.2.
procedure TDialogExportPayrollF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessFrom.DisplayValue :=
        ComboBoxPlusBusinessTo.DisplayValue;
end;

// RV087.2.
procedure TDialogExportPayrollF.FillContractGroup;
begin
  if not UseContractgroup then
    Exit;
  if CheckBoxAllContractGroups.Checked then
    ContractGroupVisible(False)
  else
  begin
    ContractGroupVisible(True);
    ListProcsF.FillComboBox(QueryContractGroup,
      ComboBoxPlusContractGroupFrom, '', '', True, '', '',
      'CONTRACTGROUP_CODE', 'DESCRIPTION');
    ListProcsF.FillComboBox(QueryContractGroup,
      ComboBoxPlusContractGroupTo, '', '', False, '', '',
      'CONTRACTGROUP_CODE', 'DESCRIPTION');
  end;
end;

// RV087.2.
procedure TDialogExportPayrollF.HandleSelectionBusinessUnit;
begin
  if PlantFrom = PlantTo then
  begin
    if not CheckBoxAllBusinessUnits.Checked then
    begin
      BusinessUnitFrom := GetStrValue(ComboBoxPlusBusinessFrom.Value);
      BusinessUnitTo := GetStrValue(ComboBoxPlusBusinessTo.Value);
    end
    else
    begin
      BusinessUnitFrom := '0';
      BusinessUnitTo := 'zzzzz';
    end;
  end
  else
  begin
    BusinessUnitFrom := '0';
    BusinessUnitTo := 'zzzzz';
  end;
end;

// RV087.2.
// RV098.2.
procedure TDialogExportPayrollF.HandleSelectionDepartment(
  APlantFrom, APlantTo: String);
begin
  if APlantFrom = APlantTo then
  begin
    if not CheckBoxAllDepartments.Checked then
    begin
      DepartmentFrom := GetStrValue(CmbPlusDepartmentFrom.Value);
      DepartmentTo := GetStrValue(CmbPlusDepartmentTo.Value);
    end
    else
    begin
      DepartmentFrom := '0';
      DepartmentTo := 'zzzzz';
    end;
  end
  else
  begin
    DepartmentFrom := '0';
    DepartmentTo := 'zzzzz';
  end;
end;

// RV087.2.
procedure TDialogExportPayrollF.HandleSelectionContractGroup;
begin
  if not CheckBoxAllContractGroups.Checked then
  begin
    ContractGroupFrom := GetStrValue(ComboBoxPlusContractGroupFrom.Value);
    ContractGroupTo := GetStrValue(ComboBoxPlusContractGroupTo.Value);
  end
  else
  begin
    ContractGroupFrom :='0';
    ContractGroupTo := 'zzzzz';
  end;
end;

{ TAFASExportClass }

// RV088.1.
function TAFASExportClass.EmployeeExists(
  const AEmployeeNumber: Integer): Boolean;
begin
  Result := (EmployeeList.IndexOf(IntToStr(AEmployeeNumber)) >= 0);
end;

// RV088.1.
procedure TAFASExportClass.AddEmployee(const AEmployeeNumber: Integer);
begin
  if not EmployeeExists(AEmployeeNumber) then
    EmployeeList.Add(IntToStr(AEmployeeNumber));
end;

function TAFASExportClass.DetectDays(const AValue: String): Integer;
var
  IPos: Integer;
begin
  Result := 0;
  try
    IPos := Pos(';', AValue);
    if (IPos > 0) then
      Result := StrToInt(Copy(AValue, IPos + 1, Length(AValue)));
  except
    Result := 0;
  end;
end;

// RV088.1.
function TAFASExportClass.DetectExportCode(const AValue: String): String;
var
  IPos: Integer;
begin
  Result := '';
  IPos := Pos(';', AValue);
  if (IPos > 0) then
    Result := Copy(AValue, 1, IPos - 1);
end;

function TAFASExportClass.ExportLine(const AEmployeeNumber,
  ACustomerNumber: Integer; const ASumWKHrs: Integer;
  const AExportCode, AExportType: String): String;
  function MinConv(AMinutes: Integer; AET: String): String;
  begin
    Result := '';
    // RV088.1. Always export as minutes here!
{
    if AET = 'S' then // Salary: Return as hours
      Result := MinCnv(AMinutes)
    else // Absence: return as days
      Result := IntToStr(Round(AMinutes / 60 / 8));
}
    if AET = 'H' then // Hours
      Result := MinCnv(AMinutes)
    else // Days
      Result := IntToStr(AMinutes);
  end;
begin
  // Periodnumber;Bookyear;Period;ExportDate;Employee;Customer;Value;Exportcode
  Result := Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', [
{ 1,  2}      PeriodNumber, MySep,
{ 3,  4}      Bookyear, MySep,
{ 5,  6}      Period, MySep,
{ 7,  8}      ExportDate, MySep,
{ 9, 10}      IntToStr(AEmployeeNumber), MySep,
{11, 12}      IntToStr(ACustomerNumber), MySep,
{13, 14}      MinConv(ASumWKHrs, AExportType), MySep,
                // FieldByName('ET').AsString), MySep,
{15}          AExportCode
              ]);
end;

{ TAFASExportClass }

// RV087.2.
procedure TDialogExportPayrollF.ExportAFAS;
var
  DateMin, DateMax: TDateTime;
  DialogFilter, Line: String;
  ExportPKLine: Integer;
begin
  ExportType := etAFAS;
  DialogExportPayrollDM.CurrentExportType := ExportType;
  AFASExportClass := TAFASExportClass.Create;
  AFASExportClass.EmployeeList := TStringList.Create;
  FExportList.Sorted := True;
  try
    // Switch to correct ExportPayroll-record.
    DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE', 'AFAS', []);

    // Check Selections
    ExportCheckUserSelections;

    // Get other selections
    HandleSelectionBusinessUnit;
    HandleSelectionDepartment(PlantFrom, PlantTo);
    HandleSelectionContractGroup;

    // Set fixed values
    AFASExportClass.Periodnumber := '4';
    AFASExportClass.Bookyear := IntToStr(dxSpinEditYear.IntValue);
    AFASExportClass.Period := IntToStr(dxSpinEditPeriodNr.IntValue);
    AFASExportClass.ExportDate := FormatDateTime('ddmmyyyy', Date);

    DetermineDateMinMax(DateMin, DateMax);

    FExportList.Clear;

    with DialogExportPayrollDM.qryAFAS do
    begin
      Close;
      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
      ParamByName('PLANTFROM').AsString := PlantFrom;
      ParamByName('PLANTTO').AsString := PlantTo;
      ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      ParamByName('CONTRACTGROUPFROM').AsString := ContractGroupFrom;
      ParamByName('CONTRACTGROUPTO').AsString := ContractGroupTo;
      ParamByName('DEPARTMENTFROM').AsString := DepartmentFrom;
      ParamByName('DEPARTMENTTO').AsString := DepartmentTo;
      ParamByName('BUSINESSUNITFROM').AsString := BusinessUnitFrom;
      ParamByName('BUSINESSUNITTO').AsString := BusinessUnitTo;
      ParamByName('DATEFROM').AsDateTime := DateMin;
      ParamByName('DATETO').AsDateTime := DateMax;
      Open;
      while not Eof do
      begin
        AFASExportClass.AddEmployee(FieldByName('EMPLOYEE_NUMBER').AsInteger);
        if FieldByName('SUMWK_HRS').AsInteger <> 0 then
        begin
          // Export the line:
          // Periodnumber;Bookyear;Period;ExportDate;Employee;Customer;Value;Exportcode
          Line := AFASExportClass.ExportLine(
                    FieldByName('EMPLOYEE_NUMBER').AsInteger,
                    FieldByName('CUSTOMER_NUMBER').AsInteger,
                    FieldByName('SUMWK_HRS').AsInteger,
                    FieldByName('EXPORT_CODE').AsString,
                    'H'
                    );
          FExportList.Add(Line);
        end;
        Next;
      end; // while not Eof do
      Close;
    end; // with DialogExportPayrollDM.qryAFAS do

    // Now determine days
    ExportPKLine := 0;
    ProcessingData(ExportPKLine);

    // Save export file
    DialogFilter := 'CSV files (*.csv)|*.CSV';
    // RV094.5. Use string-const instead of hard-coded string.
    SaveExportFile(FExportList, SPimsFilenameAFAS, {'Loonmutaties ABS PIMS',}
      DialogFilter, 'csv');
  finally
    AFASExportClass.EmployeeList.Free;
    AFASExportClass.Free;
  end;
end; // ExportAFAS

// RV087.2.
// RV095.2.
// GLOB3-110 Export Employee_Number, not BSN
procedure TDialogExportPayrollF.ExportSELECT;
var
  DateMin, DateMax: TDateTime;
  DialogFilter, Line: String;
  Contractgroup, EmpNr, EmpName, LastName, FirstName, Department: String;
  Hourtype: String;
  DayI, Minutes, TotalMinutes, GrandTotalMinutes: Integer;
  DayMinutes: array[1..7] of Integer;
  GrandTotalDayMinutes: array[1..7] of Integer;
  ExportListNonSorted: TStringList; // TODO 21132
  I: Integer; // TODO 21132
  ContractgroupCode: String; // TODO 21132
//  RoundTruncSalaryHours, RoundMinutes: Integer; // TODO 21132
(*
  // TODO 21132
  function ActionRoundMinutes(AMinutes: Integer): Integer;
  var
    Hours, Mins: Integer;
  begin
    if (AMinutes > 0) and (RoundMinutes > 1) then
    begin
      Hours := AMinutes DIV 60;
      Mins := AMinutes MOD 60;
      if RoundTruncSalaryHours = 1 then // Round
        Mins := Round(Mins / RoundMinutes) * RoundMinutes
      else
        Mins := Trunc(Mins / RoundMinutes) * RoundMinutes;
      Result := Hours * 60 + Mins;
    end
    else
      Result := AMinutes;
  end; // ActionRoundMinutes
*)
  function MinConv(AMinutes: Integer): String;
  begin
    Result := '';
    if AMinutes <> 0 then
    begin
      // TODO 21132 Round/Trunc minutes here!
//      AMinutes := ActionRoundMinutes(AMinutes);
      // TODO 21132 Just round on 5 minutes
      AMinutes := RoundMinutes(5, AMinutes);
      Result := MinCnv(AMinutes);
    end;
  end; // MinConv
  function LastNameConv(AName: String): String;
  var
    IPos: Integer;
  begin
    Result := AName;
    IPos := Pos(',', AName);
    if (IPos > 0) then
      Result := Trim(Copy(AName, 1, IPos - 1));
  end; // LastNameConv
  function FirstNameConv(AName: String): String;
  var
    IPos: Integer;
  begin
    Result := AName;
    IPos := Pos(',', AName);
    if (IPos > 0) then
      Result := Trim(Copy(AName, IPos + 1, Length(AName)));
  end; // FirstNameConv
  // RV095.2.
  function PlantEmpLevel: String;
  begin
    if not CheckBoxAllPlants.Checked then
      Result := '1'
    else
      Result := '0';
  end;
  // TODO 21132
(*
  procedure ActionDetermineContractGroupSettings(AContractgroupCode: String);
  begin
    RoundTruncSalaryHours := 0;
    RoundMinutes := 0;
    try
      with DialogExportPayrollDM do
      begin
        if not ClientDataSetContract.Active then
          ClientDataSetContract.Open;
        if ClientDataSetContract.
          Locate('CONTRACTGROUP_CODE', AContractgroupCode, []) then
        begin
          // 0=Trunc 1=Round
          RoundTruncSalaryHours := ClientDataSetContract.
            FieldByName('ROUND_TRUNC_SALARY_HOURS').AsInteger;
          RoundMinutes := ClientDataSetContract.
            FieldByName('ROUND_MINUTE').AsInteger;
        end;
      end;
    except
      RoundTruncSalaryHours := 0;
      RoundMinutes := 0;
    end;
  end; // ActionDetermineContractGroupSettings
*)
begin
  // IMPORTANT:
  // Variables MyPlantFrom and MyPlantTo must be used
  // when 2 levels are possible:
  // - plant-department-level
  // - plant-employee-level
  // This is also indicated by PlantEmpLevel-variable (1=plant-employee-level,
  // 0=plant-department-level).
  ExportType := etSELECT;
  DialogExportPayrollDM.CurrentExportType := ExportType;

  // TODO 21132
  // Extra non-sorted Stringlist for header+exportlines+footer
  ExportListNonSorted := TStringList.Create;
  ExportListNonSorted.Sorted := False;

  // Switch to correct ExportPayroll-record.
  DialogExportPayrollDM.TableEP.Locate('EXPORT_TYPE', 'SELECT', []);

  // Check Selections
  ExportCheckUserSelections;

  // Get other selections
  HandleSelectionBusinessUnit;
  // RV098.2.
  HandleSelectionDepartment(MyPlantFrom, MyPlantTo);
  HandleSelectionContractGroup;

  DetermineDateMinMax(DateMin, DateMax);

  // TD-21132 When All-Plant-Checkbox is clicked, do not select
  // anymore on business unit and employees!
  if CheckBoxAllPlants.Checked then
  begin
    EmployeeFrom := 0;
    EmployeeTo := 999999999;
    BusinessUnitFrom := '0';
    BusinessUnitTo := 'zzzzz';
  end;

  // RV098.2.
  FExportList.Sorted := True; // TODO 21132 Sort the Stringlist.
  FExportList.Clear;

  // Init the grand-total-vars.
  GrandTotalMinutes := 0;
  for DayI := 1 to 7 do
    GrandTotalDayMinutes[DayI] := 0;

{$IFDEF DEBUG1}
WDebugLog('LoginUser=' + SystemDM.UserTeamLoginUser +
  ' PlantEmpLevel=' + PlantEmpLevel +
  ' MyPlantFrom=' + MyPlantFrom +
  ' MyPlantTo=' + MyPlantTo +
  ' EmpFrom=' + IntToStr(EmployeeFrom) +
  ' EmpTo=' + IntToStr(EmployeeTo) +
  ' CGFrom=' + ContractGroupFrom +
  ' CGTo=' + ContractGroupTo +
  ' DeptFrom=' + DepartmentFrom +
  ' DeptTo=' + DepartmentTo +
  ' BUFrom=' + BusinessUnitFrom +
  ' BUTo=' + BusinessUnitTo +
  ' DateFrom=' + DateToStr(DateMin) +
  ' DateTo=' + DateToStr(DateMax)
  );
{$ENDIF}

  with DialogExportPayrollDM do
  begin
    // RV095.2. Be sure filter is OFF! Or it will find nothing after the first
    //          time, when the dialog was not closed.
    qrySELECT.Filtered := False;
    qrySELECT.Filter := '';
    // Main query that gives all needed values.
    qrySELECT.Close;
    qrySELECT.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    qrySELECT.ParamByName('PLANT_EMP_LEVEL').AsString := PlantEmpLevel; // RV095.2.
    qrySELECT.ParamByName('PLANTFROM').AsString := MyPlantFrom; // RV095.2.
    qrySELECT.ParamByName('PLANTTO').AsString := MyPlantTo; // RV095.2.
    qrySELECT.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    qrySELECT.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    qrySELECT.ParamByName('CONTRACTGROUPFROM').AsString := ContractGroupFrom;
    qrySELECT.ParamByName('CONTRACTGROUPTO').AsString := ContractGroupTo;
    qrySELECT.ParamByName('DEPARTMENTFROM').AsString := DepartmentFrom;
    qrySELECT.ParamByName('DEPARTMENTTO').AsString := DepartmentTo;
    qrySELECT.ParamByName('BUSINESSUNITFROM').AsString := BusinessUnitFrom;
    qrySELECT.ParamByName('BUSINESSUNITTO').AsString := BusinessUnitTo;
    qrySELECT.ParamByName('DATEFROM').AsDateTime := DateMin;
    qrySELECT.ParamByName('DATETO').AsDateTime := DateMax;
    qrySELECT.Open;
    if not qrySELECT.Eof then // Something found?
    begin
      // This query is used as filter on
      // Contractgroup, EmpNr, Employee, Department, Hourtype
      qrySELECT_FILTER.Close;
      qrySELECT_FILTER.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
      qrySELECT_FILTER.ParamByName('PLANT_EMP_LEVEL').AsString := PlantEmpLevel; // RV095.2.
      qrySELECT_FILTER.ParamByName('PLANTFROM').AsString := MyPlantFrom; // RV095.2.
      qrySELECT_FILTER.ParamByName('PLANTTO').AsString := MyPlantTo; // RV095.2.
      qrySELECT_FILTER.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      qrySELECT_FILTER.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      qrySELECT_FILTER.ParamByName('CONTRACTGROUPFROM').AsString := ContractGroupFrom;
      qrySELECT_FILTER.ParamByName('CONTRACTGROUPTO').AsString := ContractGroupTo;
      qrySELECT_FILTER.ParamByName('DEPARTMENTFROM').AsString := DepartmentFrom;
      qrySELECT_FILTER.ParamByName('DEPARTMENTTO').AsString := DepartmentTo;
      qrySELECT_FILTER.ParamByName('BUSINESSUNITFROM').AsString := BusinessUnitFrom;
      qrySELECT_FILTER.ParamByName('BUSINESSUNITTO').AsString := BusinessUnitTo;
      qrySELECT_FILTER.ParamByName('DATEFROM').AsDateTime := DateMin;
      qrySELECT_FILTER.ParamByName('DATETO').AsDateTime := DateMax;
      qrySELECT_FILTER.Open;

      // First make a header
      Line :=
        Format('%s%s%s%s%s%s', [
          'Opdrachtgever: Lamme Textiel', MySep,
          IntToStr(dxSpinEditYear.IntValue), MySep,
          'Van ' + IntToStr(dxSpinEditWeekMin.IntValue) +
          ' tot ' + IntToStr(dxSpinEditWeekMax.IntValue), MySep
        ]);
      // TODO 21132 Add header here
//      FExportList.Add(Line);
      ExportListNonSorted.Add(Line);
      // Make a column-header
      Line :=
        Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', [
          'Contractgroep', MySep, // 1, 2
          'Nummer', MySep,           // 3, 4
          'Achternaam', MySep,    // 5, 6
          'Voornaam', MySep,      // 7, 8
          'Afdeling', MySep,      // 9, 10
          'Urensoort', MySep,     // 11, 12
          'Ma', MySep,            // 13, 14
          'Di', MySep,            // 15, 16
          'Wo', MySep,            // 17, 18
          'Do', MySep,            // 19, 20
          'Vr', MySep,            // 21, 22
          'Za', MySep,            // 23, 24
          'Zo', MySep,            // 25, 26
          'Uren Totaal'           // 27
          ]);
      // TODO 21132 Add header here
//      FExportList.Add(Line);
      ExportListNonSorted.Add(Line);
      // Now add the lines.
      // Grouped on: Contractgroup, EmpNr, Name, Department, Hourtype.
      while not qrySELECT_FILTER.Eof do
      begin
        // Set all values
        ContractgroupCode :=
          qrySELECT_FILTER.FieldByName('CONTRACTGROUP_CODE').AsString;
        Contractgroup := qrySELECT_FILTER.FieldByName('CONTRACTGROUP').AsString;
        EmpNr := qrySELECT_FILTER.FieldByName('EMPLOYEE_NUMBER').AsString;
        EmpName := qrySELECT_FILTER.FieldByName('NAME').AsString;
        Department := qrySELECT_FILTER.FieldByName('DEPARTMENT').AsString;
        Hourtype := qrySELECT_FILTER.FieldByName('HOURTYPE').AsString;
        LastName := LastNameConv(EmpName);
        FirstName := FirstNameConv(EmpName);

        // TODO 21132
        // Get Contractgroup settings for rounding here.
        // TODO 21132 Do not do this!
//        ActionDetermineContractGroupSettings(ContractGroupCode);

        // Filter on: Contractgroup, EmpNr, Name, Department, Hourtype.
        // TD-25958 Use QuotedStr(), this will add surrounding quotes and
        //          will double embedded quotes.
        qrySELECT.Filtered := False;
        qrySELECT.Filter :=
          'CONTRACTGROUP = ' + QuotedStr(Contractgroup) + ' AND ' +
          'EMPLOYEE_NUMBER = ' + QuotedStr(EmpNr) + ' AND ' +
          'NAME = ' + QuotedStr(EmpName) + ' AND ' +
          'DEPARTMENT = ' + QuotedStr(Department) + ' AND ' +
          'HOURTYPE = ' + QuotedStr(Hourtype);
        qrySELECT.Filtered := True;

        // Get the minutes of the week here.
        // Initialise
        for DayI := 1 to 7 do
          DayMinutes[DayI] := 0;
        TotalMinutes := 0;
        while not qrySELECT.Eof do
        begin
          Minutes := qrySELECT.FieldByName('PRODMIN').AsInteger;
          DayMinutes[qrySELECT.FieldByName('DAY').AsInteger] := Minutes;
          TotalMinutes := TotalMinutes + Minutes;
          qrySELECT.Next;
        end; // while not qrySELECT.Eof do
        // Set the grand-total-vars.
        GrandTotalMinutes := GrandTotalMinutes + TotalMinutes;
        for DayI := 1 to 7 do
          GrandTotalDayMinutes[DayI] := GrandTotalDayMinutes[DayI] +
            DayMinutes[DayI];
        // Export the line.
        Line :=
          Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', [
            Contractgroup, MySep, // 1, 2
            EmpNr, MySep,           // 3, 4
            Lastname, MySep,      // 5, 6
            Firstname, MySep,     // 7, 8
            Department, MySep,    // 9, 10
            Hourtype, MySep,      // 11, 12
            MinConv(DayMinutes[1]), MySep, // 13, 14
            MinConv(DayMinutes[2]), MySep, // 15, 16
            MinConv(DayMinutes[3]), MySep, // 17, 18
            MinConv(DayMinutes[4]), MySep, // 19, 20
            MinConv(DayMinutes[5]), MySep, // 21, 22
            MinConv(DayMinutes[6]), MySep, // 23, 24
            MinConv(DayMinutes[7]), MySep, // 25, 26
            MinConv(TotalMinutes)          // 27
            ]);
        FExportList.Add(Line);
        qrySELECT_FILTER.Next;
      end; // while not qrySELECT_FILTER.Eof do
      qrySELECT.Close;
      qrySELECT_FILTER.Close;
      // Export last line with grand-totals.
      Line :=
        Format('%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s', [
          'Totaal uren', MySep, // 1, 2
          '', MySep,           // 3, 4
          '', MySep,      // 5, 6
          '', MySep,     // 7, 8
          '', MySep,    // 9, 10
          '', MySep,      // 11, 12
          MinConv(GrandTotalDayMinutes[1]), MySep, // 13, 14
          MinConv(GrandTotalDayMinutes[2]), MySep, // 15, 16
          MinConv(GrandTotalDayMinutes[3]), MySep, // 17, 18
          MinConv(GrandTotalDayMinutes[4]), MySep, // 19, 20
          MinConv(GrandTotalDayMinutes[5]), MySep, // 21, 22
          MinConv(GrandTotalDayMinutes[6]), MySep, // 23, 24
          MinConv(GrandTotalDayMinutes[7]), MySep, // 25, 26
          MinConv(GrandTotalMinutes)          // 27
          ]);
      // TODO 21132
      // Add sorted export lines here: Sorted on stringlist
      if FExportList.Count > 0 then
        for I := 0 to FExportList.Count - 1 do
          ExportListNonSorted.Add(FExportList[I]);
      // TODO 21132 Add footer here
//      FExportList.Add(Line);
      ExportListNonSorted.Add(Line);
    end // if not qrySELECT.Eof then
    else // RV095.2. Show message when nothing was found.
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end; // with DialogExportPayrollDM do

  if FExportList.Count > 0 then // RV095.2. Only save when something was found.
  begin
    // Save export file
    DialogFilter := 'CSV files (*.csv)|*.CSV';
    SaveExportFile(ExportListNonSorted, // FExportList
      'Uren Export', DialogFilter, 'csv');
  end;
  //
  try
  finally
    if ExportListNonSorted.Count > 0 then
      ExportListNonSorted.Clear;
    ExportListNonSorted.Free;
  end;
end; // ExportSELECT

// RV087.2.
procedure TDialogExportPayrollF.ContractGroupVisible(AVis: Boolean);
begin
  ComboBoxPlusContractGroupFrom.Visible := AVis;
  ComboBoxPlusContractGroupTo.Visible := AVis;
end;

// RV087.2.
procedure TDialogExportPayrollF.CheckBoxAllContractGroupsClick(
  Sender: TObject);
begin
  inherited;
  FillContractGroup;
end;

// RV087.2.
procedure TDialogExportPayrollF.BusinessUnitVisible(AVis: Boolean);
begin
  ComboBoxPlusBusinessFrom.Visible := AVis;
  ComboBoxPlusBusinessTo.Visible := AVis;
end;

procedure TDialogExportPayrollF.CheckBoxAllBusinessUnitsClick(
  Sender: TObject);
begin
  inherited;
  FillBusinessUnit;
end;

// RV087.2.
procedure TDialogExportPayrollF.BusinessUnitSelectionVisible;
begin
  CheckBoxAllBusinessUnits.Visible := UseBU;
  lblFromBU.Visible := UseBU;
  lblBU.Visible := UseBU;
  ComboBoxPlusBusinessFrom.Visible := UseBU;
  ComboBoxPlusBusinessTo.Visible := UseBU;
  lblToBU.Visible := UseBU;
  LblStarBUFrom.Visible := UseBU;
  LblStarBUTo.Visible := UseBU;
end;

// RV087.2.
procedure TDialogExportPayrollF.ContractGroupSelectionVisible;
begin
  CheckBoxAllContractGroups.Visible := UseContractGroup;
  lblFromContractGroup.Visible := UseContractGroup;
  lblContractGroup.Visible := UseContractGroup;
  ComboBoxPlusContractGroupFrom.Visible := UseContractGroup;
  ComboBoxPlusContractGroupTo.Visible := UseContractGroup;
  lblToContractGroup.Visible := UseContractGroup;
  LblStarContractGroupFrom.Visible := UseContractGroup;
  LblStarContractGroupTo.Visible := UseContractGroup;
  if UseContractGroup then
    CheckBoxAllContractGroups.Checked := True;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogExportPayrollF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogExportPayrollF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// RV095.2.
procedure TDialogExportPayrollF.rgpAfasSelectClick(Sender: TObject);
begin
  inherited;
  // Do this only for AFAS+SELECT-combination.
  // Do this only for 'Export Select', not for 'AFAS'
  if IsAFAS_SELECT then
  begin
    if rgpAfasSelect.ItemIndex = 0 then
    begin
      CheckBoxAllPlants.Checked := False;
      CheckBoxAllPlants.Visible := False;
    end
    else
     CheckBoxAllPlants.Visible := True;
  end;
end;

// RV095.2.
procedure TDialogExportPayrollF.CheckBoxAllPlantsClick(Sender: TObject);
begin
  inherited;
  BevelPlantDept.Visible := CheckBoxAllPlants.Checked;
  // TD-21132 When ALL PLANTS then also ALL BUSINESS UNITS must be checked!
  CheckBoxAllBusinessUnits.Checked := CheckBoxAllPlants.Checked;
end;

// 20013288 Export Easy Labor
procedure TDialogExportPayrollF.ExportELA;
const
  MySep=','; // TD-21897
  MyExtension='csv';
  MyDialogFilter='CSV files (*.csv)|*.CSV';
var
  ExportDone, AnyExportDone: Boolean;
  EmpList: TStringList;
  LastExportDate: TDateTime;
  Hour, Minute, Second, MSecond: Word;
  LastSequenceNumber: Integer;
  MyExportType: String;
  // 20013288.130
  procedure ReadLastSequenceNumber(AExportType: String);
  begin
    try
      with DialogExportPayrollDM.qryReadLastSequenceNr do
      begin
        Close;
        ParamByName('EXPORT_TYPE').AsString := AExportType;
        Open;
        if FieldByName('SEQUENCE_NUMBER').AsString <> '' then
          LastSequenceNumber :=
            FieldByName('SEQUENCE_NUMBER').AsInteger
        else
          LastSequenceNumber := 0;
      end;
    except
      LastSequenceNumber := 0;
    end;
  end; // ReadLastSequenceNumber
  // 20013288.130
  procedure UpdateLastSequenceNumber(AExportType: String);
  begin
    try
      with DialogExportPayrollDM.qryUpdateLastSequenceNr do
      begin
        ParamByName('EXPORT_TYPE').AsString := AExportType;
        ParamByName('SEQUENCE_NUMBER').AsInteger := LastSequenceNumber;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  end; // UpdateLastSequenceNumber
  procedure ReadLastExportDate(AExportType: String);
  begin
    try
      with DialogExportPayrollDM.qryReadLastExportDate do
      begin
        Close;
        ParamByName('EXPORT_TYPE').AsString := AExportType;
        Open;
        if FieldByName('LASTEXPORTDATE').AsString <> '' then
          LastExportDate := FieldByName('LASTEXPORTDATE').AsDateTime
        else
          LastExportDate := 0;
      end;
    except
      LastExportDate := Trunc(Date);
    end;
  end; // ReadLastExportDate
  procedure UpdateLastExportDate(AExportType: String);
  begin
    try
      with DialogExportPayrollDM.qryUpdateLastExportDate do
      begin
        ParamByName('EXPORT_TYPE').AsString := AExportType;
        ExecSQL;
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        WErrorLog(E.Message);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  end; // UpdateLastExportDate
  procedure EmpIsExternalInit;
  begin
    try
      with DialogExportPayrollDM.qryEmpIsExternal do
      begin
        Filtered := False;
        Close;
        Open;
      end;
    except
    end;
  end; // EmpIsExternalInit
  function EmpIsExternal(AEmployeeNumber: Integer): Boolean;
  begin
    Result := False; // Export is OK
    try
      with DialogExportPayrollDM.qryEmpIsExternal do
      begin
        Filtered := False;
        Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
        Filtered := True;
        if not Eof then
        begin
          // When there was an emp-contract found
          Result := True; // Do not export
          while not Eof do
          begin
            // Valid contract (start- and end-date within till-date ?)
            if (Trunc(DTPickerTillDate.Date) >=
              FieldByName('STARTDATE').AsDateTime) and
              (Trunc(DTPickerTillDate.Date) <=
              FieldByName('ENDDATE').AsDateTime) then
            begin
              // When active emp-contract is not equal
              // to 'external' then export is OK.
              if FieldByName('CONTRACT_TYPE').AsInteger <> 2 then
                Result := False; // Export is OK
              Break;
            end; // if
            Next;
          end; // while
        end; // if
      end; // with
    except
      Result := False;
    end;
  end; // EmpIsExternal
  procedure ExportSwipeFile;
  var
    Filename: String;
    SelectStr: String;
    BeginIDCard, NextIDCard, EndIDCard: TScannedIDCard;
    Line: String;
    procedure StoreIDCard(var AIDCard: TScannedIDCard);
    begin
      with DialogExportPayrollDM do
      begin
        AIDCard.EmployeeCode :=
          qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        AIDCard.DateIn :=
          qryWork.FieldByName('DATETIME_IN').AsDateTime;
        AIDCard.DateOut :=
          qryWork.FieldByName('DATETIME_OUT').AsDateTime;
        AIDCard.PlantCode :=
          qryWork.FieldByName('PLANT_CODE').AsString;
        AIDCard.DepartmentCode :=
          qryWork.FieldByName('DEPARTMENT_CODE').AsString;
        AIDCard.ShiftDate :=
          qryWork.FieldByName('SHIFT_DATE').AsDateTime;
      end;
    end; // StoreIDCard
    procedure UpdateExportedFlagSwipes;
    var
      EmpField: String;
      I: Integer;
    begin
      try
        try
          with DialogExportPayrollDM do
          begin
            with qryUpdateSwipe do
            begin
              for I := 0 to EmpList.Count - 1 do
              begin
                EmpField := EmpList.Strings[I];
                Close;
                ParamByName('TILL_DATE').AsDateTime :=
                  Trunc(DTPickerTillDate.Date) + 1;
                ParamByName('EMPLOYEE_NUMBER').AsInteger := StrToInt(EmpField);
                ExecSQL;
              end;
            end; // with
          end; // with
          if SystemDM.Pims.InTransaction then
            SystemDM.Pims.Commit;
        except
          on E: EDBEngineError do
          begin
            WErrorLog(E.Message);
            if SystemDM.Pims.InTransaction then
              SystemDM.Pims.Rollback;
          end;
        end;
      finally
        //
      end;
    end; // UpdateExportedFlagSwipes
  begin
    FExportList.Clear;
    EmpList.Clear;
    // 20013288.130
    ExportDone := False;
    MyExportType := 'ELA';
    LastSequenceNumber := 0;
    LastExportDate := 0;
    ReadLastExportDate(MyExportType);
    if (LastExportDate <> Date) or (LastExportDate = 0) then
    begin
      LastSequenceNumber := 0;
      UpdateLastSequenceNumber(MyExportType);
    end;
    ReadLastSequenceNumber(MyExportType);
    try
      try
        // SWIPE<yyyymmdd><SEQUENCE>.csv
        // 20013288.4 Use underscore instead of period before sequencenumber.
        Filename := 'SWIPE' +
          FormatDateTime('yyyymmdd', Date) +
          '_' + // 20013288.4
          FillZero(IntToStr(LastSequenceNumber
          {DialogExportPayrollDM.TableEP.
            FieldByName('SEQUENCE_NUMBER').AsInteger} + 1), 3);
        // During 'SaveExportFile' it will add the extension.
//        if MyExtension <> '' then
//          Filename := Filename + '.' + MyExtension;

        PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
        PlantTo := GetStrValue(CmbPlusPlantTo.Value);
        if PlantFrom = PlantTo then
        begin
          EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
          EmployeeTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
        end;

        with DialogExportPayrollDM do
        begin
          qryWork.Close;
          qryWork.SQL.Clear;
          SelectStr :=
            'SELECT ' + NL +
            '  T.EMPLOYEE_NUMBER, T.DATETIME_IN, T.DATETIME_OUT, ' + NL +
            '  T.PLANT_CODE, W.DEPARTMENT_CODE, ' + NL +
            '  NVL(T.SHIFT_DATE, T.DATETIME_IN) SHIFT_DATE ' + NL +
            'FROM ' + NL +
            '  TIMEREGSCANNING T INNER JOIN WORKSPOT W ON ' + NL +
            '    T.PLANT_CODE = W.PLANT_CODE AND ' + NL +
            '    T.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
            'WHERE ' + NL +
            '  T.EXPORTED_YN = ''N'' ' + NL;
          // 20013288.3
          if not CheckBoxIncludeOpenScans.Checked then
            SelectStr := SelectStr +
              '  AND (T.PROCESSED_YN = ''Y'') ' + NL
          else
          begin
            // In case there were any overnight scans:
            // - Look for open scans not older than 12 hours
            SelectStr := SelectStr +
              '  AND ' + NL +
              '  ( ' + NL +
              '    (T.PROCESSED_YN = ''Y'') ' + NL +
              '    OR ' + NL +
              '    (T.PROCESSED_YN = ''N'' ' +
              '     AND T.DATETIME_IN >= SYSDATE-0.5) ' + NL +
              '  ) ' + NL;
          end;
            // 20013288 Rework: Filter on SHIFT_DATE, not on DATETIME_IN.
          SelectStr := SelectStr +
//            '  AND T.DATETIME_IN < :TILL_DATE ' + NL +
            '  AND NVL(T.SHIFT_DATE, T.DATETIME_IN) < :TILL_DATE ' + NL +
            '  AND T.PLANT_CODE >= :PLANTFROM ' + NL +
            '  AND T.PLANT_CODE <= :PLANTTO ' + NL;
          if PlantFrom = PlantTo then
            SelectStr := SelectStr +
              '  AND T.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
              '  AND T.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
          // 20013288.2 Only export when there is no open scan
          // Exclude open scans:
          if not CheckBoxIncludeOpenScans.Checked then
          begin
            SelectStr := SelectStr +
              '  AND T.EMPLOYEE_NUMBER NOT IN ' + NL +
              '    ( ' + NL +
              '      SELECT T2.EMPLOYEE_NUMBER ' + NL +
              '      FROM TIMEREGSCANNING T2 ' + NL +
              '      WHERE T2.EMPLOYEE_NUMBER = T.EMPLOYEE_NUMBER ' + NL +
              '      AND T2.EXPORTED_YN = ''N'' ' + NL +
              '      AND T2.DATETIME_OUT IS NULL ' + NL +
              '      AND NVL(T2.SHIFT_DATE, T2.DATETIME_IN) < :TILL_DATE ' + NL +
              '      AND T2.PLANT_CODE >= :PLANTFROM ' + NL +
              '      AND T2.PLANT_CODE <= :PLANTTO ' + NL;
            if PlantFrom = PlantTo then
              SelectStr := SelectStr +
                '      AND T2.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
                '      AND T2.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
            SelectStr := SelectStr +
              '    ) ' + NL;
          end;
          SelectStr := SelectStr +
            'ORDER BY ' + NL +
            '  T.EMPLOYEE_NUMBER, T.DATETIME_IN ';
          qryWork.SQL.Add(SelectStr);
//qryWork.SQL.SaveToFile('c:\temp\export_easylabor1.sql');
          qryWork.ParamByName('TILL_DATE').AsDateTime :=
            Trunc(DTPickerTillDate.Date) + 1;
          qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
          qryWork.ParamByName('PLANTTO').AsString := PlantTo;
          if PlantFrom = PlantTo then
          begin
            qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
            qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
          end;
          qryWork.Open;
          // When there is a change of department or employee or EOF then
          // store the line to the export-file.
          EmptyIDCard(BeginIDCard);
          EmptyIDCard(NextIDCard);
          EmptyIDCard(EndIDCard);
          if not qryWork.Eof then
          begin
            // TD-21897 Add header.
            // Employee_ID,Date,Time In,Time Out,Department
            // 20013288.4. Header should be:
            // Employee_ID, Datein, Time In, Dateout, Time Out, Department
            Line := 'Employee_ID' + MySep +
              'Datein' + MySep +
              'Time In' + MySep +
              'Dateout' + MySep +
              'Time Out' + MySep +
              'Department';
            FExportList.Add(Line);
          end;
          while not qryWork.Eof do
          begin
            StoreIDCard(EndIDCard);
            if BeginIDCard.EmployeeCode = NullInt then
              StoreIDCard(BeginIDCard);
            qryWork.Next;
            if not qryWork.Eof then
              StoreIDCard(NextIDCard)
            else
              EmptyIDCard(NextIDCard);

            // If there is a change of employee, plant, department, shiftdate
            // or eof reached.
            if (NextIDCard.EmployeeCode = NullInt) or
              (BeginIDCard.EmployeeCode <> NextIDCard.EmployeeCode) or
              (BeginIDCard.PlantCode <> NextIDCard.PlantCode) or
              (BeginIDCard.DepartmentCode <> NextIDCard.DepartmentCode) or
              (Trunc(BeginIDCard.ShiftDate) <> Trunc(NextIDCard.ShiftDate)) then
            begin
{
	Employee ID					9 (numeric)
	Date						mmddyyyy
	Time In	*)					hh:mm:ss
	Date						mmddyyyy
	Time Out *)					hh:mm:ss
	Plant code					6 (alphanumeric)
	Department **)				 	6 (alphanumeric)
}
              if not EmpIsExternal(BeginIDCard.EmployeeCode) then
              begin
                if EmpList.IndexOf(IntToStr(BeginIDCard.EmployeeCode)) < 0 then
                  EmpList.Add(IntToStr(BeginIDCard.EmployeeCode));
                // 20013288.3
                if EndIDCard.DateOut = 0 then
                begin
                  Decodetime(Now, Hour, Minute, Second, MSecond);
                  EndIDCard.DateOut := Trunc(Date) +
                    Frac(EncodeTime(Hour, Minute, 0, 0));
                end;
                // TD-21897 Leave out Second Date + Plant Code
                // 20013288.4 Add second date (again)
                Line :=
                  IntToStr(BeginIDCard.EmployeeCode) + MySep +
                  FormatDateTime('mmddyyyy', Trunc(BeginIDCard.DateIn)) + MySep +
                  FormatDateTime('hh:nn:ss', Frac(BeginIDCard.DateIn)) + MySep +
                  // 20013288.4
                  FormatDateTime('mmddyyyy', Trunc(EndIDCard.DateOut)) + MySep + // TD-21897
                  FormatDateTime('hh:nn:ss', Frac(EndIDCard.DateOut)) + MySep +
//                  BeginIDCard.PlantCode + MySep + // TD-21897
                  BeginIDCard.DepartmentCode;
                FExportList.Add(Line);
              end;
              if not qryWork.Eof then
                StoreIDCard(BeginIDCard);
            end; // if change of dept, emp or eof.
          end; // while
          qryWork.Close;
        end; // with
      except
      end;
    finally
      if FExportList.Count > 0 then
      begin
        ExportDone := True;
        // Save export file
        if SaveExportFile(FExportList, Filename, MyDialogFilter,
          MyExtension) then
        begin
          // Update Exported-flag for scans.
          UpdateExportedFlagSwipes;
          // 20013288.130
          UpdateLastExportDate(MyExportType);
          inc(LastSequenceNumber);
          UpdateLastSequenceNumber(MyExportType);
        end;
      end;
      if not ExportDone then
        DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
    end;
  end; // ExportSwipeFile
  procedure ExportScheduleFile;
  var
    Filename: String;
    SelectStr: String;
    Line: String;
    AShiftDayRecord: TShiftDayRecord;
    TimeBlockNumber: Integer;
    TimeIn: array[1..4] of TDateTime;
    TimeOut: array[1..4] of TDateTime;
    procedure UpdateExportFlagSchedule;
    var
      EmpField: String;
      I: Integer;
    begin
      try
        try
          with DialogExportPayrollDM do
          begin
            with qryUpdateSchedule do
            begin
              for I := 0 to EmpList.Count - 1 do
              begin
                EmpField := EmpList.Strings[I];
                Close;
                ParamByName('TILL_DATE').AsDateTime :=
                  Trunc(DTPickerTillDate.Date) + 1;
                ParamByName('EMPLOYEE_NUMBER').AsInteger := StrToInt(EmpField);
                ExecSQL;
              end;
            end; // with
          end; // with
          if SystemDM.Pims.InTransaction then
            SystemDM.Pims.Commit;
        except
          on E: EDBEngineError do
          begin
            WErrorLog(E.Message);
            if SystemDM.Pims.InTransaction then
              SystemDM.Pims.Rollback;
          end;
        end;
      finally
        //
      end;
    end; // UpdateExportFlagSchedule
  begin
    FExportList.Clear;
    EmpList.Clear;
    // 20013288.130
    ExportDone := False;
    MyExportType := 'ELA2';
    LastSequenceNumber := 0;
    LastExportDate := 0;
    ReadLastExportDate(MyExportType);
    if (LastExportDate <> Date) or (LastExportDate = 0) then
    begin
      LastSequenceNumber := 0;
      UpdateLastSequenceNumber(MyExportType);
    end;
    ReadLastSequenceNumber(MyExportType);
    try
      try
        // SCHEDULE<yyyymmdd><SEQUENCE>.csv
        // 20013288.4 Use underscore instead of period before sequencenumber.
        Filename := 'SCHEDULE' +
          FormatDateTime('yyyymmdd', Date) +
          '_' + // 20013288.4
          FillZero(IntToStr(LastSequenceNumber
          {DialogExportPayrollDM.TableEP.
            FieldByName('SEQUENCE_NUMBER').AsInteger} + 1), 3);
        // During 'SaveExportFile' it will add the extension.
//        if MyExtension <> '' then
//          Filename := Filename + '.' + MyExtension;

        PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
        PlantTo := GetStrValue(CmbPlusPlantTo.Value);
        if PlantFrom = PlantTo then
        begin
          EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
          EmployeeTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
        end;
        with DialogExportPayrollDM do
        begin
          qryWork.Close;
          qryWork.SQL.Clear;
          // 20013288.4 Instead of Employee-number export Social-security-number
          SelectStr :=
            'SELECT ' + NL +
            '  T.EMPLOYEE_NUMBER, ' + NL +
            '  E.SOCIAL_SECURITY_NUMBER, ' + NL + // 20013288.4
            '  T.PLANT_CODE, E.DEPARTMENT_CODE, ' + NL +
            '  T.SHIFT_NUMBER, ' + NL +
            '  T.EMPLOYEEAVAILABILITY_DATE, ' + NL +
            '  T.AVAILABLE_TIMEBLOCK_1, ' + NL +
            '  T.AVAILABLE_TIMEBLOCK_2, ' + NL +
            '  T.AVAILABLE_TIMEBLOCK_3, ' + NL +
            '  T.AVAILABLE_TIMEBLOCK_4 ' + NL +
            'FROM ' + NL +
            '  EMPLOYEEAVAILABILITY T INNER JOIN EMPLOYEE E ON ' + NL +
            '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
            'WHERE ' + NL +
            '  T.EXPORTED_YN = ''N'' ' + NL +
            '  AND T.EMPLOYEEAVAILABILITY_DATE < :TILL_DATE ' + NL +
            '  AND T.PLANT_CODE >= :PLANTFROM ' + NL +
            '  AND T.PLANT_CODE <= :PLANTTO ' + NL;
          if PlantFrom = PlantTo then
            SelectStr := SelectStr +
              ' AND T.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
              ' AND T.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
          SelectStr := SelectStr +
            'ORDER BY ' + NL +
//            '  T.EMPLOYEE_NUMBER, ' + NL +
            '  E.SOCIAL_SECURITY_NUMBER, ' + NL + // 20013288.4
            '  T.EMPLOYEEAVAILABILITY_DATE ';
          qryWork.SQL.Add(SelectStr);
          qryWork.ParamByName('TILL_DATE').AsDateTime :=
            Trunc(DTPickerTillDate.Date) + 1;
          qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
          qryWork.ParamByName('PLANTTO').AsString := PlantTo;
          if PlantFrom = PlantTo then
          begin
            qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
            qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
          end;
          qryWork.Open;
          if not qryWork.Eof then
          begin
            // TD-21897 Add header.
{            Line := 'Employee_ID' + MySep +
              'PlantCode' + MySep +
              'DepartmentCode' + MySep +
              'Date' + MySep +
              'Time In1' + MySep +
              'Time Out1' + MySep +
              'AbsenceCode' + MySep +
              'Time In2' + MySep +
              'Time Out2' + MySep +
              'AbsenceCode' + MySep +
              'Time In3' + MySep +
              'Time Out3' + MySep +
              'AbsenceCode' + MySep +
              'Time In4' + MySep +
              'Time Out4' + MySep +
              'AbsenceCode'; }
            // TD-21897 Leave out some fields.
            // 20013288.4 Header should be:
            // Employee_ID,Plant code,Department,Date,Time In,Time Out,Available
            Line := 'Employee_ID' + MySep +
              'Plant code' + MySep +
              'Department' + MySep +
              'Date' + MySep +
              'Time In' + MySep +
              'Time Out' + MySep +
              'Available';
            FExportList.Add(Line);
          end;
          while not qryWork.Eof do
          begin
            AShiftDayRecord.APlantCode :=
              qryWork.FieldByName('PLANT_CODE').AsString;
            AShiftDayRecord.AShiftNumber :=
              qryWork.FieldByName('SHIFT_NUMBER').AsInteger;
            AShiftDay.AEmployeeNumber :=
              qryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            AShiftDay.ADepartmentCode :=
              qryWork.FieldByName('DEPARTMENT_CODE').AsString;
            AShiftDayRecord.ADate :=
              qryWork.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
            for TimeBlockNumber := 1 to 4 do
            begin
              AShiftDay.DetermineShiftStartEndTB(AShiftDayRecord,
                TimeBlockNumber);
              TimeIn[TimeBlockNumber] := AShiftDayRecord.AStart;
              TimeOut[TimeBlockNumber] := AShiftDayRecord.AEnd;
            end;
{
	Employee ID						9 (numeric)
	Plant code						6 (alphanumeric)
	Standard work spot code (department code in PIMS) *)	6 (alphanumeric)
	Date							mmddyyyy
	Time In	(time block 1)					hh:mm:ss
	Time Out (time block 1)					hh:mm:ss
 	Absence code						1 (alphanumeric)
	  * means available
	  - means not available
	  x means absent with certain reason
	  Instead of x the predefined code will filled here
	Time In	(time block 2)					hh:mm:ss
	Time Out (time block 2)					hh:mm:ss
	Absence code						1 (alphanumeric)
	Time In	(time block 3)					hh:mm:ss
	Time Out (time block 3)					hh:mm:ss
	Absence code						1 (alphanumeric)
	Time In	(time block 4)					hh:mm:ss
	Time Out (time block 4)					hh:mm:ss
	Absence code						1 (alphanumeric)
}
{
  TD-21897 Leave out some fields. Only show first timeblock.
}
            if not EmpIsExternal(AShiftDay.AEmployeeNumber) then
            begin
              if EmpList.IndexOf(IntToStr(AShiftDay.AEmployeeNumber)) < 0 then
                EmpList.Add(IntToStr(AShiftDay.AEmployeeNumber));
{              Line :=
                IntToStr(AShiftDay.AEmployeeNumber) + MySep +
                AShiftDayRecord.APlantCode + MySep +
                AShiftDay.ADepartmentCode + MySep +
                FormatDateTime('mmddyyyy', AShiftDayRecord.ADate) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeIn[1])) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeOut[1])) + MySep +
                qryWork.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeIn[2])) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeOut[2])) + MySep +
                qryWork.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeIn[3])) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeOut[3])) + MySep +
                qryWork.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeIn[4])) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeOut[4])) + MySep +
                qryWork.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString + MySep; }
              Line :=
                // 20013288.4 Instead of Employee-number export Social-security-number
//                IntToStr(AShiftDay.AEmployeeNumber) + MySep +
                qryWork.FieldByName('SOCIAL_SECURITY_NUMBER').AsString + MySep +
                AShiftDayRecord.APlantCode + MySep +
                AShiftDay.ADepartmentCode + MySep +
                FormatDateTime('mmddyyyy', AShiftDayRecord.ADate) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeIn[1])) + MySep +
                FormatDateTime('hh:nn:ss', Frac(TimeOut[2])) + MySep +
                qryWork.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString;
              FExportList.Add(Line);
            end;
            qryWork.Next;
          end; // while
        end; // with
      except
      end;
    finally
      if FExportList.Count > 0 then
      begin
        ExportDone := True;
        // Save export file
        if SaveExportFile(FExportList, Filename, MyDialogFilter,
          MyExtension) then
        begin
          // Update Exported-flag for Schedule
          UpdateExportFlagSchedule;
          // 20013288.130
          UpdateLastExportDate(MyExportType);
          inc(LastSequenceNumber);
          UpdateLastSequenceNumber(MyExportType);
        end;
      end;
      if not ExportDone then
        DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
    end;
  end; // ExportScheduleFile;
begin
  EmpList := TStringList.Create;
  // 20013288.130
  if not (CheckBoxExportSwipes.Checked or CheckBoxExportSchedule.Checked) then
  begin
    DisplayMessage(SPimsExportEasyLaborChoice, mtInformation, [mbOK]);
    Exit;
  end;
  try
    // 20013288.130
    AnyExportDone := False;
{
    ExportDone := False;
    ReadLastExportDate;
    if (LastExportDate <> Date) or (LastExportDate = 0) then
    begin
      try
        // Update sequence for ExportPayroll-table
        DialogExportPayrollDM.UpdateExportPayrollTable(
          0,
          0,
         'ELA');
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      except
        on E: EDBEngineError do
        begin
          WErrorLog(E.Message);
          if SystemDM.Pims.InTransaction then
            SystemDM.Pims.Rollback;
        end;
      end;
    end;
}
    EmpIsExternalInit;
    // 20013288.130
    if CheckBoxExportSwipes.Checked then
    begin
      ExportSwipeFile;
      if ExportDone then
        AnyExportDone := True;
    end;
    // 20013288.130
    if CheckBoxExportSchedule.Checked then
    begin
      ExportScheduleFile;
      if ExportDone then
        AnyExportDone := True;
    end;
//    UpdateLastExportDate;
    if AnyExportDone then
    begin
      try
        // 20013288.130 This is already done
{
        // Update sequence for ExportPayroll-table
        DialogExportPayrollDM.UpdateExportPayrollTable(
          DialogExportPayrollDM.TableEP.
          FieldByName('TOTAL_PK_RECORDS').AsInteger,
          DialogExportPayrollDM.TableEP.
          FieldByName('SEQUENCE_NUMBER').AsInteger + 1,
          'ELA');
}
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Commit;
      except
        on E: EDBEngineError do
        begin
          WErrorLog(E.Message);
          if SystemDM.Pims.InTransaction then
            SystemDM.Pims.Rollback;
        end;
      end;
    end;
{    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]); }
  finally
    EmpList.Free;
  end;
end; // ExportELA

// 20014714
procedure TDialogExportPayrollF.ShowWeekPeriod;
begin
  if GroupBoxPeriod.Visible then
  begin
    if lblWeekPeriod.Visible then
    begin
      try
        lblWeekPeriod.Caption :=
          '(' +
          DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
          Round(dxSpinEditWKNumber.Value), 1)) + ' - ' +
          DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
          Round(dxSpinEditWKNumber.Value), 7)) + ')';
      except
        lblWeekPeriod.Caption := '';
      end;
    end;
  end;
end; // ShowWeekPeriod

// 20014714
procedure TDialogExportPayrollF.ExportWMU;
const
  ExportWMUFileName='ADP.csv';
  ASep=',';
var
  DateMin, DateMax: TDateTime;
  CurrentEmployee, NextEmployee: Integer;
  RegularMinutes, OvertimeMinutes: Integer;
  AbsenceList: TList;
  Line: String;
  procedure ClearAbsenceList;
  var
    I: Integer;
    AAbsenceReasonRec: PAbsenceReasonRec;
  begin
    for I := AbsenceList.Count - 1 downto 0 do
    begin
       AAbsenceReasonRec := AbsenceList.Items[I];
       Dispose(AAbsenceReasonRec);
       AbsenceList.Remove(AAbsenceReasonRec);
    end;
  end;
  procedure AddAbsenceReason(AExportCode: String; AMinutes: Integer);
  var
    AAbsenceReasonRec: PAbsenceReasonRec;
  begin
    new(AAbsenceReasonRec);
    AAbsenceReasonRec.Code := AExportCode;
    AAbsenceReasonRec.Minutes := AMinutes;
    AbsenceList.Add(AAbsenceReasonRec);
  end;
  procedure AddExportHeaderLine;
  begin
    Line :=
      'CO Code' + ASep +
      'Batch ID' + ASep +
      'File #' + ASep +
      'Reg Hours' + ASep +
      'O/T Hours' + ASep +
      'Temp Dept' + ASep +
      'Shift' + ASep +
      'Hours 3 Code' + ASep +
      'Hours 3 Amount' + ASep +
      'Hours 4 Code' + ASep +
      'Hours 4 Amount' + ASep +
      'Hours 5 Code' + ASep +
      'Hours 5 Amount';
    FExportList.Add(Line);
  end; // AddExportHeaderLine;
  procedure AddExportLine;
  var
    I: Integer;
    AAbsenceReasonRec: PAbsenceReasonRec;
  begin
    Line :=
      '0' + ASep + // CO Code
      '1' + ASep + // Batch ID
      IntToStr(CurrentEmployee) +  ASep + // Employee
      MinCnv(RegularMinutes) +  ASep +    // Regular
      MinCnv(OvertimeMinutes) + ASep +    // Overtime
      ASep + // Team Dept
      ASep;  // Shift
    for I := 0 to AbsenceList.Count - 1 do
    begin
      AAbsenceReasonRec := AbsenceList.Items[I];
      Line := Line +
        AAbsenceReasonRec.Code + ASep + // Absence Reason Code
        MinCnv(AAbsenceReasonRec.Minutes);
      if I < AbsenceList.Count - 1 then
        Line := Line + ASep;
    end;
    FExportList.Add(Line);
  end; // AddExportLine
  procedure StoreValues;
  begin
    with DialogExportPayrollDM.qryWMU do
    begin
      case FieldByName('ET').AsInteger of
      1: RegularMinutes := FieldByName('SUMWK_HRS').AsInteger;
      2: OvertimeMinutes := FieldByName('SUMWK_HRS').AsInteger;
      3: AddAbsenceReason(FieldByName('EXPORT_CODE').AsString,
        FieldByName('SUMWK_HRS').AsInteger);
      end; // case
    end;
  end; // StoreValues
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    AbsenceList := TList.Create;
    RegularMinutes := 0;
    OvertimeMinutes := 0;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryWMU do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          AddExportHeaderLine;
          CurrentEmployee := FieldByName('EMPLOYEE_NUMBER').AsInteger;
          NextEmployee := CurrentEmployee;
          while not Eof do
          begin
            if CurrentEmployee = NextEmployee then
            begin
              StoreValues;
            end
            else
            begin
              // Export line
              AddExportLine;
              RegularMinutes := 0;
              OvertimeMinutes := 0;
              ClearAbsenceList;
              StoreValues;
              CurrentEmployee := NextEmployee;
            end;
            Next;
            if not Eof then
              NextEmployee := FieldByName('EMPLOYEE_NUMBER').AsInteger
            else
              AddExportLine; // Export line for last record.
          end; // while
        end; // if
      end;
    end;
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportWMUFileName)
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
    ClearAbsenceList;
    AbsenceList.Free;
  end;
end; // ExportWMU

// 20011800
function TDialogExportPayrollF.GetEmployeeFrom: Integer;
begin
  if OnePlant then
    FEmployeeFrom := 0;
  Result := FEmployeeFrom;
end;

// 20011800
function TDialogExportPayrollF.GetEmployeeTo: Integer;
begin
  if OnePlant then
    FEmployeeTo := 999999999;
  Result := FEmployeeTo;
end;

// 20011800
function TDialogExportPayrollF.GetBusinessUnitFrom: String;
begin
  if OnePlant then
    FBusinessUnitFrom := '0';
  Result := FBusinessUnitFrom;
end;

// 20011800
function TDialogExportPayrollF.GetBusinessUnitTo: String;
begin
  if OnePlant then
    FBusinessUnitTo := 'zzzzz';
  Result := FBusinessUnitTo;
end;

// 20011800
function TDialogExportPayrollF.GetContractGroupFrom: String;
begin
  if OnePlant then
    FContractGroupFrom := '0';
  Result := FContractGroupFrom;
end;

// 20011800
function TDialogExportPayrollF.GetContractGroupTo: String;
begin
  if OnePlant then
    FContractGroupTo := 'zzzzz';
  Result := FContractGroupTo;
end;

// 20011800
function TDialogExportPayrollF.GetDepartmentFrom: String;
begin
  if OnePlant then
    FDepartmentFrom := '0';
  Result := FDepartmentFrom;
end;

// 20011800
function TDialogExportPayrollF.GetDepartmentTo: String;
begin
  if OnePlant then
    FDepartmentTo := 'zzzzz';
  Result := FDepartmentTo;
end;

// 20011800
function TDialogExportPayrollF.GetPlantFrom: String;
begin
  Result := FPlantFrom;
end;

// 20011800
function TDialogExportPayrollF.GetPlantTo: String;
begin
  if OnePlant then
    FPlantTo := FPlantFrom;
  Result := FPlantTo;
end;

// 20011800
procedure TDialogExportPayrollF.ShowFinalRunExportDate;
var
  ExportDate: TDateTime;
begin
  PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  ExportDate :=
    SystemDM.FinalRunExportDate(PlantFrom,
      DialogExportPayrollDM.ExportTypeToString(ExportType));
  if ExportDate <> NullDate then
    EditFinalRunExportDate.Text := DateToStr(ExportDate)
  else
    EditFinalRunExportDate.Text := '';
  EditExportType.Text := DialogExportPayrollDM.ExportTypeToString(ExportType);
end;

// 20011800 Test Run Button
procedure TDialogExportPayrollF.btnTestRunClick(Sender: TObject);
var
  GoOn: Boolean;
  DateMin, DateMax: TDateTime;
begin
  inherited;
  FinalRunExport := False;
  GoOn := True;
  // 20011800
  DetermineDateMinMax(DateMin, DateMax);
  if not FinalRunExportDateOK then
    GoOn := False;
  if GoOn then
    ExportAction;
end;

// 20011800 Is it allowed to export this period, or was it already exported?
function TDialogExportPayrollF.FinalRunExportDateOK: Boolean;
var
  ExportDate: TDateTime;
  Gap: Integer;
begin
  Result := True;
  if SystemDM.UseFinalRun then
  begin
    // Get last export date based on Plant+ExportType
    ExportDate := SystemDM.FinalRunExportDate(PlantFrom,
      DialogExportPayrollDM.ExportTypeToString(ExportType));
    // When an last-exportdate was found...
    if ExportDate <> NullDate then
    begin
      // Are Min and Max <= this exportdate?
      if (MyDateMin <= ExportDate) or (MyDateMax <= ExportDate) then
      begin
        // When Final Run-button used...
        if FinalRunExport then
        begin
          // Always ask for Yes/No
//          if SystemDM.IsAdmin then
          begin
            if DisplayMessage(
              SPimsFinalRunExportDateWarningAdmin, mtConfirmation,
              [mbYes, mbNo]) <> mrYes then
              Result := False;
          end
{          else
          begin
            DisplayMessage(SPimsFinalRunExportDateWarning,
              mtInformation, [mbOK]);
            Result := False;
          end; }
        end // if
        else // When Test-button used...
          if DisplayMessage(
            SPimsTestRunExportDateWarning, mtConfirmation, [mbYes, mbNo])
            <> mrYes then
            Result := False;
      end // if
      else
      begin
        // Is there a gap between exportdate and Min ?
        Gap := Round(MyDateMin - ExportDate);
        if Gap > 1 then
        begin
          // When Final Run-button used...
          if FinalRunExport then
          begin
            // Always ask for Yes/No
//            if SystemDM.IsAdmin then
            begin
              if DisplayMessage(
                Format(SPimsFinalRunExportGapWarningAdmin, [Gap]),
                mtConfirmation, [mbYes, mbNo])
                <> mrYes then
                Result := False;
            end
{            else
            begin
              DisplayMessage(Format(SPimsFinalRunExportGapWarning, [Gap]),
                mtInformation, [mbOK]);
              Result := False;
            end; }
          end
          else // When Test-button used...
            if DisplayMessage(
              Format(SPimsTestRunExportGapWarning, [Gap]),
              mtConfirmation, [mbYes, mbNo])
              <> mrYes then
              Result := False;
        end; // if
      end; // if
    end; // if
  end; // if
end; // FinalRunExportDateOK

// PIM-108
procedure TDialogExportPayrollF.DateFromDateCloseUp(Sender: TObject);
begin
  inherited;
  if DateFromDate.DateTime > DateToDate.DateTime then
    DateToDate.DateTime := DateFromDate.DateTime;
end;

// PIM-108
procedure TDialogExportPayrollF.DateToDateCloseUp(Sender: TObject);
begin
  inherited;
  if DateFromDate.DateTime > DateToDate.DateTime then
    DateToDate.DateTime := DateFromDate.DateTime;
end;

// GLOB3-72
procedure TDialogExportPayrollF.ExportNTS;
const
  ExportSALFileName = 'IT0001TRS.HLW';
  ExportABSFileName = 'FRAV0001TRS.XML';
var
  DateMin, DateMax: TDateTime;
  Line: String;
  SelectStr: String;
  RecCount: Integer;
  // With separator in between
  function IntMin2DecimalTimeSep(Minutes, HrsDigits: Integer): String;
  begin
    Result :=
      Format('%.d', [ (Minutes div 60) ]) +
        '.' +
        Format('%.2d', [ Round((Minutes Mod 60) * 100 / 60) ]);
  end;
  // Without separator in between
  function IntMin2DecimalTimeNoSep(Minutes, HrsDigits: Integer): String;
  begin
    Result :=
      Format('%.d', [ (Minutes div 60) ]) +
        Format('%.2d', [ Round((Minutes Mod 60) * 100 / 60) ]);
  end;
  function IntMin2StrDigTime(Minutes, HrsDigits: Integer): String;
  var
    Negative: boolean;
  begin
    Negative := False;
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
    Result := Format('%.'+IntToStr(HrsDigits)+'d%.2d',
      [(Minutes div 60),(Minutes mod 60)]);
    if Negative then
      Result := '-' + Result;
  end; // IntMin2StrDigTime
  function IntMinToTime(Minutes, HrsDigits: Integer): String;
  var
    Negative: boolean;
  begin
    Negative := False;
    if Minutes < 0 then
    begin
      Negative := True;
      Minutes := Minutes*(-1);
    end;
//    Result := Format('%.'+IntToStr(HrsDigits)+'d%.2d',
//      [(Minutes div 60),(Minutes mod 60)]);
    Result := Format('%.d', [ (Minutes div 60) ]) +
      '.' +
      Format('%.2d', [ (Minutes Mod 60) ]);
    if Negative then
      Result := '-' + Result;
  end;
  procedure ExportSalary;
  begin
    try
      FExportList.Clear;
      with DialogExportPayrollDM do
      begin
        // Salary hours
        // Extra rule:
        // When regular + exceptional hours are made for employee and date,
        // then add the exceptional hours to the regular hours during export.
        qryWork.Close;
        qryWork.SQL.Clear;
        SelectStr :=
          'SELECT ' + NL +
          '   SHE.EMPLOYEE_NUMBER, ' + NL +
          '   HT.EXPORT_CODE, ' + NL +
          '   SHE.SALARY_DATE, ' + NL +
          '     CASE ' + NL +
          '       WHEN SHE.HOURTYPE_NUMBER = 1 AND ' + NL +
          '         ( ' + NL +
          '           ( ' + NL +
          '            SELECT SUM(SHE2.SALARY_MINUTE) ' + NL +
          '            FROM SALARYHOURPEREMPLOYEE SHE2 ' + NL +
          '              INNER JOIN EMPLOYEE E ON SHE2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
          '            WHERE SHE2.SALARY_DATE = SHE.SALARY_DATE AND ' + NL +
          '              SHE2.EMPLOYEE_NUMBER = SHE.EMPLOYEE_NUMBER ' + NL +
          '              AND SHE2.HOURTYPE_NUMBER IN (SELECT ED.HOURTYPE_NUMBER ' + NL +
          '              FROM EXCEPTIONALHOURDEF ED ' + NL +
          '              WHERE ED.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) ' + NL +
          '           ) > 0 ' + NL +
          '         ) ' + NL +
          '         THEN ' + NL +
          '           SHE.SALARY_MINUTE + ' + NL +
          '           ( ' + NL +
          '            SELECT SUM(SHE2.SALARY_MINUTE) ' + NL +
          '            FROM SALARYHOURPEREMPLOYEE SHE2 ' + NL +
          '              INNER JOIN EMPLOYEE E ON SHE2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
          '            WHERE SHE2.SALARY_DATE = SHE.SALARY_DATE AND ' + NL +
          '              SHE2.EMPLOYEE_NUMBER = SHE.EMPLOYEE_NUMBER ' + NL +
          '              AND SHE2.HOURTYPE_NUMBER IN (SELECT ED.HOURTYPE_NUMBER ' + NL +
          '              FROM EXCEPTIONALHOURDEF ED ' + NL +
          '              WHERE ED.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) ' + NL +
          '           ) ' + NL +
          '       ELSE SHE.SALARY_MINUTE ' + NL +
          '      END SALARY_MINUTE ' + NL +
          'FROM ' + NL +
          '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE HT ON ' + NL +
          '    SHE.HOURTYPE_NUMBER = HT.HOURTYPE_NUMBER ' + NL +
          '  INNER JOIN EMPLOYEE E ON ' + NL +
          '    E.EMPLOYEE_NUMBER = SHE.EMPLOYEE_NUMBER ' + NL +
          '  INNER JOIN PLANT P ON ' + NL +
          '    P.PLANT_CODE = E.PLANT_CODE ' +
          'WHERE ' +
          '  SHE.SALARY_DATE >= :DATEFROM ' + NL +
          '  AND SHE.SALARY_DATE <= :DATETO ' + NL +
          '  AND P.PLANT_CODE >= :PLANTFROM ' + NL +
          '  AND P.PLANT_CODE <= :PLANTTO ' + NL;
          if PlantFrom = PlantTo then
            SelectStr := SelectStr +
              '  AND SHE.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
              '  AND SHE.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
          SelectStr := SelectStr +
            'ORDER BY ' + NL +
            '  SHE.EMPLOYEE_NUMBER, SHE.SALARY_DATE ';
        qryWork.SQL.Add(SelectStr);
        qryWork.ParamByName('DATEFROM').AsDateTime := DateMin;
        qryWork.ParamByName('DATETO').AsDateTime := DateMax;
        qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
        qryWork.ParamByName('PLANTTO').AsString := PlantTo;
        if PlantFrom = PlantTo then
        begin
          qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
          qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        end;
        qryWork.Open;
        while not qryWork.Eof do
        begin
          Line :=
            FillZero(qryWork.FieldByName('EMPLOYEE_NUMBER').AsString, 6) +
            FillZero(qryWork.FieldByName('EXPORT_CODE').AsString, 5) +
            FillZero('', 12) +
            FillZero('', 12) +
            FillZero('', 12) +
            FillZero('', 12) +
            FillZero('', 12) +
            FillZero('', 12) +
            FillZero('', 12) +
            FormatDateTime('ddmmyy', qryWork.FieldByName('SALARY_DATE').AsDateTime) +
            FillZero(
//              IntMin2StrDigTime(
               IntMin2DecimalTimeNoSep(
                qryWork.FieldByName('SALARY_MINUTE').AsInteger, 0),
                10
                ) +
            FillZero('', 10) +
            FillZero('', 13) +
            '                              ';
          FExportList.Add(Line);
          qryWork.Next;
        end;
        qryWork.Close;
      end;
    finally
      if FExportList.Count > 0 then
        SaveExportFile(FExportList, ExportSALFileName)
      else
        DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
    end;
  end; // ExportSalary
  procedure ExportAbsence;
  begin
    try
      FExportList.Clear;
      RecCount := 0;
      with DialogExportPayrollDM do
      begin
        qryWork.Close;
        qryWork.SQL.Clear;
        SelectStr :=
          'SELECT ' + NL +
          '  AHE.EMPLOYEE_NUMBER, AR.EXPORT_CODE, ' + NL +
          '  AHE.ABSENCEHOUR_DATE, AHE.ABSENCE_MINUTE ' + NL +
          'FROM ' + NL +
          '  ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON ' + NL +
          '    AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID ' + NL +
          '  INNER JOIN EMPLOYEE E ON ' + NL +
          '    E.EMPLOYEE_NUMBER = AHE.EMPLOYEE_NUMBER ' + NL +
          '  INNER JOIN PLANT P ON ' + NL +
          '    P.PLANT_CODE = E.PLANT_CODE ' +
          'WHERE ' +
          '  AHE.ABSENCEHOUR_DATE >= :DATEFROM ' + NL +
          '  AND AHE.ABSENCEHOUR_DATE <= :DATETO ' + NL +
          '  AND P.PLANT_CODE >= :PLANTFROM ' + NL +
          '  AND P.PLANT_CODE <= :PLANTTO ' + NL;
        if PlantFrom = PlantTo then
          SelectStr := SelectStr +
            '  AND AHE.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
            '  AND AHE.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
        SelectStr := SelectStr +
          'ORDER BY ' + NL +
          '  AHE.EMPLOYEE_NUMBER, AHE.ABSENCEHOUR_DATE ';
        qryWork.SQL.Add(SelectStr);
        qryWork.ParamByName('DATEFROM').AsDateTime := DateMin;
        qryWork.ParamByName('DATETO').AsDateTime := DateMax;
        qryWork.ParamByName('PLANTFROM').AsString := PlantFrom;
        qryWork.ParamByName('PLANTTO').AsString := PlantTo;
        if PlantFrom = PlantTo then
        begin
          qryWork.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
          qryWork.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        end;
        qryWork.Open;
        if not qryWork.Eof then
        begin
          Line := '<?xml version="1.0" encoding="utf-8"?>';
          FExportList.Add(Line);
          Line := '<Export>';
          FExportList.Add(Line);
          Line := #9 + '<KildeInfo>';
          FExportList.Add(Line);
          Line := #9 + #9 + '<EksportertDato>' +
            FormatDateTime('dd-mm-yyyy', Date) + '</EksportertDato>';
          FExportList.Add(Line);
          Line := #9 + #9 + '<KildeInfoNavn>Globe</KildeInfoNavn>';
          FExportList.Add(Line);
          Line := #9 + '</KildeInfo>';
          FExportList.Add(Line);
          while not qryWork.Eof do
          begin
            inc(RecCount);
            Line := #9 + '<Rec id=' + '"' + 'Rec_' +
              FillZero(IntTostr(RecCount), 7) + '"' + '>';
            FExportList.Add(Line);
            Line := #9 + #9 + '<Fravar>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<Klient>1</Klient>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<AnsattNr>' +
              qryWork.FieldByName('EMPLOYEE_NUMBER').AsString + '</AnsattNr>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<FKodeNr>' +
              qryWork.FieldByName('EXPORT_CODE').AsString + '</FKodeNr>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<FraDato>' +
              FormatDateTime('dd.mm.yyyy',
                qryWork.FieldByName('ABSENCEHOUR_DATE').AsDateTime) + '</FraDato>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<TilDato>' +
              FormatDateTime('dd.mm.yyyy',
                qryWork.FieldByName('ABSENCEHOUR_DATE').AsDateTime) + '</TilDato>';
            FExportList.Add(Line);
            Line := #9 + #9 + #9 + '<AntallTimer>' +
//              '1' + // Export always as 1 day -> No, export absence hours.
              IntMin2DecimalTimeSep(qryWork.FieldByName('ABSENCE_MINUTE').AsInteger, 0) +
              '</AntallTimer>';
            FExportList.Add(Line);
            Line := #9 + #9 + '</Fravar>';
            FExportList.Add(Line);
            Line := #9 + '</Rec>';
            FExportList.Add(Line);
            qryWork.Next;
          end;
          Line := '</Export>';
          FExportList.Add(Line);
        end;
        qryWork.Close;
      end;
    finally
      if FExportList.Count > 0 then
        SaveExportFile(FExportList, ExportABSFileName)
      else
        DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
    end;
  end;
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);

    ExportSalary;
    ExportAbsence;
  finally
  end;
end; // ExportNTS

(*
// GLOB3-234
procedure TDialogExportPayrollF.ExportQBOOKSXXX;
const
  ExportQBOOKSFileName='Timesheet.iif';
  ASep=#9; // Tab
var
  DateMin, DateMax: TDateTime;
  Line: String;
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryQBOOKS do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          // Header
          Line :=
            '!TIMEACT' + ASep +
            'DATE' + ASep +
            'JOB' + ASep +
            'EMP' + ASep +
            'ITEM' + ASep +
            'PITEM' + ASep +
            'DURATION' + ASep +
            'PROJ' + ASep +
            'NOTE' + ASep +
            'XFERTOPAYROLL' + ASep +
            'BILLINGSTATUS';
          FExportList.Add(Line);
          while not Eof do
          begin
            Line :=
              'TIMEACT' + ASep +
{DATE}        DateMDYCnv(FieldByName('ADATE').AsDateTime) + ASep +
{JOB}         '' + ASep +
{EMP}         FieldByName('DESCRIPTION').AsString + ASep +
{ITEM}        FieldByName('EXPORT_CODE').AsString + ASep +
{PITEM}       'Hourly' + ASep +
{DURATION}    MinHundrethsCnv(FieldByName('SUMWK_HRS').AsInteger) + ASep +
{PROJ}        '' + ASep +
{NOTE}        '' + ASep +
{XFERTO}      '' + ASep +
{BILLINGSTA}  '' + ASep;
              FExportList.Add(Line);
            Next;
          end; // while
        end; // if
        Close;
      end; // with qryQBOOKS
    end; // with
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportQBOOKSFileName)
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end;
end; // ExportQBOOKSXXX
*)

// GLOB3-234
procedure TDialogExportPayrollF.ExportQBOOKS;
const
  ExportQBOOKSFileName='hours-import.csv';
  ASep=',';
var
  DateMin, DateMax: TDateTime;
  Line: String;
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryQBOOKS do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          // Header
          Line :=
{A}         'Employee #' + ASep +
{B}         'Time Entry ID (unique)' + ASep +
{C}         'Division' + ASep +
{D}         'Department' + ASep +
{E}         'Job title' + ASep +
{F}         'Pay Code' + ASep +
{G}         'Hours Worked Date' + ASep +
{H}         'Pay Rate' + ASep +
{I}         'Rate Type' + ASep +
{J}         'Hours Worked' + ASep +
{K}         'Job Code' + ASep +
{L}         'Job Data 1' + ASep +
{M}         'Job Data 2' + ASep +
{N}         'Job Data 3' + ASep +
{O}         'Job Data 4' + ASep +
{P}         'Delete';
          FExportList.Add(Line);
          while not Eof do
          begin
            Line :=
{A}           FieldByName('EMPLOYEE_NUMBER').AsString + ASep +
{B}           '' + ASep +
{C}           '' + ASep +
{D}           FieldByName('DEPARTMENT_CODE').AsString + ASep +
{E}           '' + ASep +
{F}           FieldByName('EXPORT_CODE').AsString + ASep +
{G}           DateMDYCnv(FieldByName('ADATE').AsDateTime) + ASep +
{H}           '' + ASep +
{I}           FieldByName('EXPORT_CODE').AsString + ASep +
{J}           MinHundrethsCnv(FieldByName('SUMWK_HRS').AsInteger, '.') + ASep +
{K}           '' + ASep +
{L}           '' + ASep +
{M}           '' + ASep +
{N}           '' + ASep +
{O}           '' + ASep +
{P}           '';
              FExportList.Add(Line);
            Next;
          end; // while
        end; // if
        Close;
      end; // with qryQBOOKS
    end; // with
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportQBOOKSFileName)
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end;
end; // ExportQBOOKS

// GLOB3-271
procedure TDialogExportPayrollF.ExportPCHEX;
const
  ExportPCHEXFileName='PAYCHEX.csv';
  ASep=',';
var
  DateMin, DateMax: TDateTime;
  Line: String;
  Amount, Hours: String;
  function Rate(ABonusPerc: Double): String;
  begin
    Result := '';
    if ABonusPerc > 0 then
      Result := FloatToStr((100 + ABonusPerc)/100);
  end;
  function CommaToPeriod(AValue: String): String;
  begin
    Result := StringReplace(AValue, ',', '.', [rfIgnoreCase, rfReplaceAll]);;
  end;
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryPCHEX do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          // Header for csv format
          Line :=
            'Client ID' + ASep +
            'Worker ID' + ASep +
            'Org' + ASep +
            'Job Number' + ASep +
            'Pay Component' + ASep +
            'Rate' + ASep +
            'Rate Number' + ASep +
            'Hours' + ASep +
            'Units' + ASep +
            'Line Date' + ASep +
            'Amount' + ASep +
            'Check Seq Number' + ASep +
            'Override State' + ASep +
            'Override Local' + ASep +
            'Override Local Jurisdiction' + ASep +
            'Labor Assignment';
          FExportList.Add(Line);
          while not Eof do
          begin
            if FieldByName('ET').AsString = '3' then
            begin
              Amount := CommaToPeriod(FieldByName('SUMWK_HRS').AsString);
              Hours := '';
            end
            else
            begin
              Amount := '';
              Hours := MinHundrethsCnv(FieldByName('SUMWK_HRS').AsInteger, '.');
            end;
            // csv - format
            Line :=
{Client ID}   FieldByName('PLANT_CODE').AsString + ASep +
{Worker ID}   FieldByName('EMPLOYEE_NUMBER').AsString + ASep +
{Org}         '' + ASep +
{Job Number}  '' + ASep +
{Pay Comp.}   FieldByName('EXPORT_CODE').AsString + ASep +
{Rate}        Rate(FieldByName('BONUS_PERCENTAGE').AsFloat) + ASep +
{Rate Number} '' + ASep +
{Hours}       Hours + ASep +
{Units}       '' + ASep +
{Line Date}   DateMDYCnv(FieldByName('ADATE').AsDateTime) + ASep +
{Amount}      Amount + ASep +
{Check seq nr}'' + ASep +
{Override sta}'' + ASep +
{Override loc}'' + ASep +
{Override L J}'' + Asep +
{Labor Assign}'' + ASep;
            FExportList.Add(Line);
(*
            // fixed field length format
            Line :=
{Client ID}   Format('%8s',  [FieldByName('PLANT_CODE').AsString]) +
{Worker ID}   Format('%10s', [FieldByName('EMPLOYEE_NUMBER').AsString]) +
{Org}         Format('%60s', ['']) +
{Job Number}  Format('%25s', ['']) +
{Pay Comp.}   Format('%20s', [FieldByName('EXPORT_CODE').AsString]) +
{Rate}        Format('%9s',  [Rate(FieldByName('BONUS_PERCENTAGE').AsFloat)]) +
{Rate Number} Format('%1s',  ['']) +
{Hours}       Format('%7s',  [Hours]) +
{Units}       Format('%8s',  ['']) +
{Line Date}   Format('%10s', [DateMDYCnv(FieldByName('ADATE').AsDateTime)]) +
{Amount}      Format('%17s', [Amount]) +
{Check seq nr}Format('%2s',  ['']) +
{Override sta}Format('%2s',  ['']) +
{Override loc}Format('%15s',  ['']) +
{Override L J}Format('%2s',  ['']) +
{Labor Assign}Format('%25s', ['']);
            FExportList.Add(Line);
*)
            Next;
          end; // while
        end; // if
        Close;
      end; // with qryPCHEX
    end; // with
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportPCHEXFileName)
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end;
end; // ExportPCHEX

procedure TDialogExportPayrollF.ExportNAVIS;
const
  ExportFileName='Payroll';
  ASep=',';
var
  DateMin, DateMax: TDateTime;
  Line: String;
  decRegHours, decOver1Hours, decOver2Hours: String;
  Year, Month, Day: Word;
  function FirstName(AValue: String): String;
  var
    IPos: Integer;
  begin
    Result := AValue;
    IPos := Pos(' ', AValue);
    if IPos > 0 then
      Result := Copy(AValue, 1, IPos - 1);
  end;
  function LastName(AValue: String): String;
  var
    IPos: Integer;
  begin
    Result := AValue;
    IPos := Pos(' ', AValue);
    if IPos > 0 then
      Result := Copy(AValue, IPos + 1, Length(AValue));
  end;
  function CurrentDateString: String;
  var
    IYear, IMonth, IDay: Word;
  begin
    DecodeDate(Date, IYear, IMonth, IDay);
    Result := FillZero(IntToStr(IYear), 4) + FillZero(IntToStr(IMonth), 2) +
      FillZero(IntToStr(IDay), 2);
  end;
begin
  try
    // Check Selections
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryNAVIS do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          // Header
          Line :=
            'PayrollJrnlLine' + ASep +
            'Filler0' + ASep +
            'intYear' + ASep +
            'intMonth' + ASep +
            'intDay' + ASep +
            'ProjCode' + ASep +
            'DeptCode' + ASep +
            'EmpNo' + ASep +
            'LastName' + ASep +
            'FirstName' + ASep +
            'PayCode' + ASep +
            'PRGroup' + ASep +
            'Filler80' + ASep +
            'decShiftRegHours' + ASep +
            'decShiftOver1Hours' + ASep +
            'decShiftOver2Hours' + ASep +
            'decRegHours' + ASep +
            'decOver1Hours' + ASep +
            'decOver2Hours' + ASep +
            'Filler129' + ASep +
            'ImportEarnings' + ASep +
            'Filler162' + ASep +
            'intShiftCode' + ASep +
            'Filler185' + ASep +
            'decRegRate' + ASep +
            'decOver1Rate' + ASep +
            'decOver2Rate';
          FExportList.Add(Line);
          while not Eof do
          begin
            DecodeDate(FieldByName('ADATE').AsDateTime, Year, Month, Day);
            if FieldByName('PAYCODE').AsString = '0' then
            begin
              decRegHours := MinHundrethsCnv(FieldByName('REGULAR_MINUTE').AsInteger, '.');
              decOver1Hours := MinHundrethsCnv(FieldByName('OT15_MINUTE').AsInteger, '.');
              decOver2Hours := MinHundrethsCnv(FieldByName('OT2_MINUTE').AsInteger, '.');
            end
            else
            begin
              decRegHours := MinHundrethsCnv(FieldByName('ABSENCE_MINUTE').AsInteger, '.');
              decOver1Hours := '0';
              decOver2Hours := '0';
            end;
            Line :=
{PayrollJrnlLine}    '' + ASep +
{Filler0}            '' + ASep +
{intYear}            IntToStr(Year) + ASep +
{intMonth}           FillZero(IntToStr(Month),2) + ASep + // GLOB3-297
{intDay}             FillZero(IntToStr(Day),2) + ASep + // GLOB3-297
{ProjCode}           ''  + ASep +
{DeptCode}           '' + ASep +
{EmpNo}              FieldByName('EMPLOYEE_NUMBER').AsString + ASep +
{LastName}           LastName(FieldByName('DESCRIPTION').AsString) + ASep +
{FirstName}          FirstName(FieldByName('DESCRIPTION').AsString) + ASep +
{PayCode}            FieldByName('PAYCODE').AsString + ASep +
{PRGroup}            '' + ASep +
{Filler80}           '' + Asep +
{decShiftRegHours}   '0' + ASep +
{decShiftOver1Hours} '0' + ASep +
{decShiftOver2Hours} '0' + ASep +
{decRegHours}        decRegHours + ASep +
{decOver1Hours}      decOver1Hours + ASep +
{decOver2Hours}      decOver2Hours + ASep +
{Filler129}          '' + ASep +
{ImportEarnings}     '0' + ASep +
{Filler162}          '' + ASep +
{intShiftCode}       '' + ASep +
{Filler185}          '' + ASep +
{decRegRate}         '' + ASep +
{decOver1Rate}       '' + ASep +
{decOver2Rate}       '' + ASep;
            FExportList.Add(Line);
            Next;
          end; // while
        end; // if
        Close;
      end; // with qryNavis
    end; // with
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportFileName + CurrentDateString + '.csv')
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end;
end; // ExportNAVIS

// GLOB3-377
procedure TDialogExportPayrollF.ExportBAMBOO;
const
  ExportBAMBOOFileName='hours-import.csv';
  ASep=',';
var
  DateMin, DateMax: TDateTime;
  Line: String;
  SeqNr: Integer;
begin
  try
    // Check Selections
    SeqNr := 1;
    ExportCheckUserSelections;
    FExportList.Clear;
    DetermineDateMinMax(DateMin, DateMax);
    with DialogExportPayrollDM do
    begin
      with qryBAMBOO do
      begin
        Close;
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        ParamByName('DATEFROM').AsDateTime := DateMin;
        ParamByName('DATETO').AsDateTime := DateMax;
        Open;
        if not Eof then
        begin
          // Header
          Line :=
{A}         'Employee #' + ASep +
{B}         'Time Entry ID (unique)' + ASep +
{C}         'Division' + ASep +
{D}         'Department' + ASep +
{E}         'Job title' + ASep +
{F}         'Pay Code' + ASep +
{G}         'Hours Worked Date' + ASep +
{H}         'Pay Rate' + ASep +
{I}         'Rate Type' + ASep +
{J}         'Hours Worked' + ASep +
{K}         'Job Code' + ASep +
{L}         'Job Data 1' + ASep +
{M}         'Job Data 2' + ASep +
{N}         'Job Data 3' + ASep +
{O}         'Job Data 4' + ASep +
{P}         'Delete';
          FExportList.Add(Line);
          while not Eof do
          begin
            Line :=
{A}           FieldByName('EMPLOYEE_NUMBER').AsString + ASep +
{B}           IntToStr(SeqNr) + ASep + // Sequence-number
{C}           '' + ASep +
{D}           '' + ASep +
{E}           '' + ASep +
{F}           '' + ASep +
{G}           DateMDYCnv(FieldByName('ADATE').AsDateTime) + ASep +
{H}           '' + ASep +
{I}           FieldByName('EXPORT_CODE').AsString + ASep +
{J}           MinHundrethsCnv(FieldByName('SUMWK_HRS').AsInteger, '.') + ASep +
{K}           '' + ASep +
{L}           '' + ASep +
{M}           '' + ASep +
{N}           '' + ASep +
{O}           '' + ASep +
{P}           '';
              FExportList.Add(Line);
              inc(SeqNr);
            Next;
          end; // while
        end; // if
        Close;
      end; // with qryBAMBOO
    end; // with
  finally
    if FExportList.Count > 0 then
      SaveExportFile(FExportList, ExportBAMBOOFileName)
    else
      DisplayMessage(SPimsNotFound, mtInformation, [mbOK]);
  end;
end; // ExportBAMBOO

end.
