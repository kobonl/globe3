(*
  Changes:
    MR:08-10-2004 Use datefrom-dateto instead of
                  year, weekfrom and weekto. Order 550344.
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
    SO: 15-JUN-2010 RV065.10. 550480
    - PIMS Hours per employee other plant
    MRA:11-JAN-2011 RV084.1. SR-890041
    - The report dialog always shows all absence
      reasons for selection. Instead of that it must
      show absence reasons per country. This should be
      based on plant-from and plant-to that is selected.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    JVL:07-apr-2011 RV089.3. SO-550524
    - Add suppress zeroes      
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
*)

unit DialogReportHrsPerDayFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportHrsPerDayF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroupSort: TRadioGroup;
    GroupBoxShow: TGroupBox;
    CheckBoxDept: TCheckBox;
    CheckBoxEmpl: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusAbsReasonFrom: TComboBoxPlus;
    Label17: TLabel;
    Label18: TLabel;
    ComboBoxPlusAbsReasonTo: TComboBoxPlus;
    CheckBoxTeam: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageTeam: TCheckBox;
    CheckBoxPageEmpl: TCheckBox;
    QueryBU: TQuery;
    TableAbsReason: TTable;
    TableAbsReasonABSENCETYPE_CODE: TStringField;
    TableAbsReasonABSENCEREASON_CODE: TStringField;
    TableAbsReasonDESCRIPTION: TStringField;
    CheckBoxAllTeam: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    CheckBoxAllAbsReason: TCheckBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBoxExport: TCheckBox;
    RadioGroupTypeHrs: TRadioGroup;
    Label26: TLabel;
    Label27: TLabel;
    DateFrom: TDateTimePicker;
    Label28: TLabel;
    DateTo: TDateTimePicker;
    qryAbsenceReason: TQuery;
    CheckBoxSuppressZeroes: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;

    procedure CheckBoxAllAbsReasonClick(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxTeamClick(Sender: TObject);
    procedure CheckBoxEmplClick(Sender: TObject);
    procedure CheckBoxPageDeptClick(Sender: TObject);
    procedure CheckBoxPageTeamClick(Sender: TObject);
    procedure CheckBoxPageEmplClick(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsReasonFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsReasonToCloseUp(Sender: TObject);
    procedure DateFromCloseUp(Sender: TObject);
    procedure DateToCloseUp(Sender: TObject);
    procedure CheckBoxAllEmployeesClick(Sender: TObject);
    procedure CheckBoxAllTeamsClick(Sender: TObject);
    procedure RadioGroupTypeHrsClick(Sender: TObject);
    procedure CheckBoxAllDepartmentsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure AllEmployeeAction;
    procedure EnableIncludeNotConnectedEmp;
    procedure FillAbsenceReasons; // RV084.1.
  public
    { Public declarations }

  end;

var
  DialogReportHrsPerDayF: TDialogReportHrsPerDayF;

// RV089.1.
function DialogReportHrsPerDayForm: TDialogReportHrsPerDayF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ListProcsFRM, UPimsMessageRes,
  ReportHrsPerDayQRPT, ReportHrsPerDayDMT;

// RV089.1.
var
  DialogReportHrsPerDayF_HND: TDialogReportHrsPerDayF;

// RV089.1.
function DialogReportHrsPerDayForm: TDialogReportHrsPerDayF;
begin
  if (DialogReportHrsPerDayF_HND = nil) then
  begin
    DialogReportHrsPerDayF_HND := TDialogReportHrsPerDayF.Create(Application);
    with DialogReportHrsPerDayF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportHrsPerDayF_HND;
end;

// RV089.1.
procedure TDialogReportHrsPerDayF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportHrsPerDayF_HND <> nil) then
  begin
    DialogReportHrsPerDayF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportHrsPerDayF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportHrsPerDayF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportHrsPerDayQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(ComboBoxPlusAbsReasonFrom.Value),
      GetStrValue(ComboBoxPlusAbsReasonTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Trunc(DateFrom.DateTime),
      Trunc(DateTo.DateTime),
      RadioGroupSort.ItemIndex,
      RadioGroupTypeHrs.ItemIndex,
      CheckBoxAllAbsReason.Checked, CheckBoxAllTeams.Checked,
      CheckBoxDept.Checked, CheckBoxTeam.Checked, CheckBoxEmpl.Checked,
      CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
      CheckBoxPageDept.Checked, CheckBoxPageTeam.Checked,
      CheckBoxPageEmpl.Checked,
      CheckBoxIncludeNotConnectedEmp.Checked,
      CheckBoxExport.Checked,
      CheckBoxAllEmployees.Checked,
      //RV065.10.
      CheckBoxIncludeNotConnectedEmp.Checked,
      CheckBoxSuppressZeroes.Checked
    )
  then
    ReportHrsPerDayQR.ProcessRecords;
end;

procedure TDialogReportHrsPerDayF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

procedure TDialogReportHrsPerDayF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, True, True, False, False, False);
  CheckBoxAllEmployeesShow := True;
  inherited;
  CmbPlusPlantFromCloseUp(Sender);
//  FillBusiness;
//show absencereason
// RV084.1. Absence Reasons are filled during call to 'CloseUp' for Plant.
{
  ListProcsF.FillComboBoxMaster(TableAbsReason, 'ABSENCEREASON_CODE',
    True, ComboBoxPlusAbsReasonFrom);
  ListProcsF.FillComboBoxMaster(TableAbsReason, 'ABSENCEREASON_CODE',
    False, ComboBoxPlusAbsReasonTo);
}
//end absreason
//show team
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}
//end team
  DateFrom.DateTime := Now;
  DateTo.DateTime := Now;

  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := True;
  CheckBoxDept.Checked := False;
  CheckBoxTeam.Checked := False;
  CheckBoxEmpl.Checked := False;

//  CheckBoxAllTeam.Checked := False;
  CheckBoxAllAbsReason.Checked := False;
  RadioGroupSort.ItemIndex := 0;
  RadioGroupTypeHrs.ItemIndex := 0;

  // MR:16-11-2005 If these are not choosen, 'page'-checkboxes should be false.
  CheckBoxPageDept.Enabled := False;
  CheckBoxPageTeam.Enabled := False;
  CheckBoxPageEmpl.Enabled := False;
end;

procedure TDialogReportHrsPerDayF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportHrsPerDayDM := CreateReportDM(TReportHrsPerDayDM);
  ReportHrsPerDayQR := CreateReportQR(TReportHrsPerDayQR);
end;

procedure TDialogReportHrsPerDayF.CheckBoxAllAbsReasonClick(Sender: TObject);
begin
  inherited;
  if CheckBoxAllAbsReason.Checked then
  begin
    ComboBoxPlusAbsReasonFrom.Visible := False;
    ComboBoxPlusAbsReasonTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusAbsReasonFrom.Visible := True;
    ComboBoxPlusAbsReasonTo.Visible := True;
  end
end;

procedure TDialogReportHrsPerDayF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
{
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
    CheckBoxEmpTeam.Checked := True;
    CheckBoxEmpTeam.Enabled := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
    CheckBoxEmpTeam.Enabled := True;
  end
}
end;

procedure TDialogReportHrsPerDayF.CheckBoxDeptClick(Sender: TObject);
begin
  inherited;
  // MR:16-11-2005 Do not change Page-checkboxes automatically.
  if CheckBoxDept.Checked then // Dept is enabled
  begin
    CheckBoxTeam.Enabled := False;
    CheckBoxTeam.Checked := False;
    CheckBoxPageTeam.Enabled := False;
    CheckBoxPageTeam.Checked := False;
  end
  else
    CheckBoxTeam.Enabled := True;
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
//  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
//  CheckBoxPagePlant.Enabled := not CheckBoxDept.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxDept.Checked;
end;

procedure TDialogReportHrsPerDayF.CheckBoxTeamClick(Sender: TObject);
begin
  inherited;
  if CheckBoxTeam.Checked then
  begin
    CheckBoxDept.Checked := False;
    CheckBoxDept.Enabled := False;
    CheckBoxPageDept.Checked := False;
    CheckBoxPageDept.Enabled := False;
  end
  else
    CheckBoxDept.Enabled := True;
  CheckBoxPageTeam.Enabled := CheckBoxTeam.Checked;
//  CheckBoxPageTeam.Checked := CheckBoxTeam.Checked;
//  CheckBoxPagePlant.Enabled := not CheckBoxTeam.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxTeam.Checked;
end;

procedure TDialogReportHrsPerDayF.CheckBoxEmplClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageEmpl.Enabled := CheckBoxEmpl.Checked;
//  CheckBoxPageEmpl.Checked := CheckBoxEmpl.Checked;
//  CheckBoxPagePlant.Enabled := not CheckBoxEmpl.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxEmpl.Checked;
end;

procedure TDialogReportHrsPerDayF.CheckBoxPageDeptClick(Sender: TObject);
begin
  inherited;
//  CheckBoxPagePlant.Enabled := not CheckBoxPageDept.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxPageDept.Checked;
end;

procedure TDialogReportHrsPerDayF.CheckBoxPageTeamClick(Sender: TObject);
begin
  inherited;
//  CheckBoxPagePlant.Enabled := not CheckBoxPageTeam.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxPageTeam.Checked;
end;

procedure TDialogReportHrsPerDayF.CheckBoxPageEmplClick(Sender: TObject);
begin
  inherited;
//  CheckBoxPagePlant.Enabled := not CheckBoxPageEmpl.Checked;
//  CheckBoxPagePlant.Checked := not CheckBoxPageEmpl.Checked;
  CheckBoxPageDept.Enabled := not CheckBoxPageEmpl.Checked;
//  CheckBoxPageDept.Checked := not CheckBoxPageEmpl.Checked;
  CheckBoxPageTeam.Enabled := not CheckBoxPageEmpl.Checked;
//  CheckBoxPageTeam.Checked := not CheckBoxPageEmpl.Checked;
end;

procedure TDialogReportHrsPerDayF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessTo.DisplayValue :=
          ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportHrsPerDayF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  FillBusiness;
  FillAbsenceReasons; // RV084.1.
  AllEmployeeAction;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHrsPerDayF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  FillBusiness;
  FillAbsenceReasons; // RV084.1.
  AllEmployeeAction;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHrsPerDayF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
       ComboBoxPlusBusinessFrom.DisplayValue :=
        ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportHrsPerDayF.ComboBoxPlusAbsReasonFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsReasonFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsReasonTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsReasonFrom.Value) >
      GetStrValue(ComboBoxPlusAbsReasonTo.Value) then
        ComboBoxPlusAbsReasonTo.DisplayValue :=
          ComboBoxPlusAbsReasonFrom.DisplayValue;
end;

procedure TDialogReportHrsPerDayF.ComboBoxPlusAbsReasonToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsReasonFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsReasonTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsReasonFrom.Value) >
     GetStrValue(ComboBoxPlusAbsReasonTo.Value) then
       ComboBoxPlusAbsReasonFrom.DisplayValue :=
         ComboBoxPlusAbsReasonTo.DisplayValue;
end;

procedure TDialogReportHrsPerDayF.DateFromCloseUp(Sender: TObject);
begin
  inherited;
  if DateFrom.DateTime > DateTo.DateTime then
    DateTo.DateTime := DateFrom.DateTime;
end;

procedure TDialogReportHrsPerDayF.DateToCloseUp(Sender: TObject);
begin
  inherited;
  if DateTo.DateTime < DateFrom.DateTime then
    DateFrom.DateTime := DateTo.DateTime;
end;

// RV050.4.
procedure TDialogReportHrsPerDayF.AllEmployeeAction;
begin
  CheckBoxAllEmployeesClick(nil);
end;

// RV050.4.
procedure TDialogReportHrsPerDayF.CheckBoxAllEmployeesClick(
  Sender: TObject);
begin
  inherited;
  if CheckBoxAllEmployees.Checked then
  begin
    dxDBExtLookupEditEmplFrom.Visible := False;
    dxDBExtLookupEditEmplTo.Visible := False;
  end
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      dxDBExtLookupEditEmplFrom.Visible := True;
      dxDBExtLookupEditEmplTo.Visible := True;
    end
    else
    begin
      CheckBoxAllEmployees.Checked := True;
    end;
  end;

  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHrsPerDayF.CheckBoxAllTeamsClick(Sender: TObject);
begin
  inherited;
  if CheckBoxAllTeams.Checked then
  begin
//    ComboBoxPlusTeamFrom.Visible := False;
//    ComboBoxPlusTeamTo.Visible := False;
    CheckBoxIncludeNotConnectedEmp.Checked := True;
    CheckBoxIncludeNotConnectedEmp.Enabled := False;
  end
  else
  begin
//    ComboBoxPlusTeamFrom.Visible := True;
//    ComboBoxPlusTeamTo.Visible := True;
    CheckBoxIncludeNotConnectedEmp.Enabled := True;
  end;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

//RV065.10.
procedure TDialogReportHrsPerDayF.EnableIncludeNotConnectedEmp;
begin
  CheckBoxIncludeNotConnectedEmp.Enabled :=
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
    (not CheckBoxAllEmployees.Checked) and
    CheckBoxAllTeam.Checked and
    CheckBoxAllDepartments.Checked and
    (RadioGroupTypeHrs.ItemIndex = 0);
  if not CheckBoxIncludeNotConnectedEmp.Enabled then
  begin
    CheckBoxIncludeNotConnectedEmp.Checked := False;
    CheckBoxIncludeNotConnectedEmpClick(Self);
  end;
end;


procedure TDialogReportHrsPerDayF.RadioGroupTypeHrsClick(Sender: TObject);
begin
  inherited;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHrsPerDayF.CheckBoxAllDepartmentsClick(
  Sender: TObject);
begin
  inherited;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

// RV084.1.
procedure TDialogReportHrsPerDayF.FillAbsenceReasons;
var
  CountryId: Integer;
begin
  // When plant from-to is the same, then filter on absence reason per country.
  CountryId := 0;
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CountryId := SystemDM.GetDBValue(
      ' SELECT NVL(P.COUNTRY_ID, 0) ' +
      ' FROM PLANT P ' +
      ' WHERE ' +
      '   P.PLANT_CODE =  ' + '''' +
      GetStrValue(CmbPlusPlantFrom.Value) + '''', 0);
  end;
  SystemDM.UpdateAbsenceReasonPerCountry(
    CountryId,
    qryAbsenceReason,
    'CODE'
  );
  ListProcsF.FillComboBoxMaster(qryAbsenceReason, 'ABSENCEREASON_CODE',
    True, ComboBoxPlusAbsReasonFrom);
  ListProcsF.FillComboBoxMaster(qryAbsenceReason, 'ABSENCEREASON_CODE',
    False, ComboBoxPlusAbsReasonTo);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportHrsPerDayF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogReportHrsPerDayF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
