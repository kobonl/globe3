(*
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:26-MAR-2018 GLOB3-82
  - Changes for absence card:
  - Use from-to-employee selection
  - Show total-days at bottom
*)
unit DialogReportAbsenceCardFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT, jpeg;

type
  TDialogReportAbsenceCardF = class(TDialogReportBaseF)
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    GroupBox1: TGroupBox;
    CheckBoxExport: TCheckBox;
    Panel1: TPanel;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private

  public
    { Public declarations }
   
  end;

var
  DialogReportAbsenceCardF: TDialogReportAbsenceCardF;

// RV089.1.
function DialogReportAbsenceCardForm: TDialogReportAbsenceCardF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportAbsenceDMT, ReportAbsenceQRPT, ListProcsFRM, UPimsMessageRes,
  ReportAbsenceCardQRPT, ReportAbsenceCardDMT;

// RV089.1.
var
  DialogReportAbsenceCardF_HND: TDialogReportAbsenceCardF;

// RV089.1.
function DialogReportAbsenceCardForm: TDialogReportAbsenceCardF;
begin
  if (DialogReportAbsenceCardF_HND = nil) then
  begin
    DialogReportAbsenceCardF_HND := TDialogReportAbsenceCardF.Create(Application);
    with DialogReportAbsenceCardF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportAbsenceCardF_HND;
end;

// RV089.1.
procedure TDialogReportAbsenceCardF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportAbsenceCardF_HND <> nil) then
  begin
    DialogReportAbsenceCardF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportAbsenceCardF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportAbsenceCardF.btnOkClick(Sender: TObject);
begin
  inherited;

  if ReportAbsenceCardQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Round(dxSpinEditYear.Value),
      CheckBoxExport.Checked)
  then
    ReportAbsenceCardQR.ProcessRecords;
end;

procedure TDialogReportAbsenceCardF.FormShow(Sender: TObject);
var
  Year, Month, Day{, Week}: Word;
begin
  InitDialog(True, False, False, True, False, False, False);
  inherited;
//  CmbPlusPlantTo.Value := CmbPlusPlantFrom.Value;
//  FillEmployees;
  // MR:06-01-2004 Get current year from current date, not the year
  //               from the weeknumber, because this report is not
  //               based on weeks.
  DecodeDate(Now, Year, Month, Day);
//  ListProcsF.WeekUitDat(Now, Year, Week);

  dxSpinEditYear.Value := Year;
end;

procedure TDialogReportAbsenceCardF.FormCreate(Sender: TObject);
//var
//  Year, Week: Word;
begin
  inherited;
  // MR:06-01-2004 We do this in 'FormShow'
//  ListProcsF.WeekUitDat(Now, Year, Week);
//  dxSpinEditYear.Value := Year;

// CREATE data module of report
  ReportAbsenceCardDM := CreateReportDM(TReportAbsenceCardDM);
  ReportAbsenceCardQR := CreateReportQR(TReportAbsenceCardQR);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportAbsenceCardF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportAbsenceCardF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
