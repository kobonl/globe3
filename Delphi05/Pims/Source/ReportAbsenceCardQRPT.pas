(*
  Changes:
    MRA:07-AUG-2008 RV008.
      - Link employee to plant instead of absence during select, to
        prevent absence is not shown if it has a different (employee-)plant.
    MRA:3-MAY-2010. RV062.2.
    - SetMemoParam: First argument increased, to prevent
      problems with double line when printing to printer or PrimoPDF.
  MRA:17-NOV-2010 RV080.1.
  - Bug with double lines when printing to e.g. PrimoPDF.
    Preview is OK, but printing to a printer or PrimoPDF
    gives double lines.
  - Reports and Memo-mechanism.
    - Put Memo-mechanism in a seperate class, so
      it can be created and freed when needed.
      NOTE: This did not solve the problem.
  - IMPORTANT: When the Memo-mechanism is NOT used and the QRMemo-
    components are all set to AutoSize=True and AutoStretch=True,
    then it works also without the 'double lines' problem!
  MRA:17-DEC-2010 RV083.1.
  - This does not filter on absence reasons-per-country.
      Only show absence reasons at bottom of report
      that were shown in absence card.
  MRA:21-NOV-2017 PIM-328
  - Show any planned data (that is not used/calculated yet) in
    the report. Show all time-blocks for that planned date.
  MRA:28-NOV-2017 PIM-328.2 Rework
  - Sometimes it not show months in the right order.
  - Cause: The sorting was not on date but on plant, shift, employee, so
    when shift like 1 and 2 were used, the 2 was shown later in the year.
  MRA:26-MAR-2018 GLOB3-82
  - Changes for absence card:
  - Use from-to-employee selection
  - Show total-days at bottom per absence reason
  MRA:30-MAR-2018 GLOB3-82 Rework
  - Bugfix to prevent it prints the second page without a page-break,
    which happens only during first page and when having ForceNewPage
    set to True for QRGroupHDEmployee.
  - Solved in AfterPrint of QRBandTFEmployee.
  MRA:9-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
*)

unit ReportAbsenceCardQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, Db, DBTables;

// MR:02-09-2003
const
  MAXDAY=31;

type
  TQRParameters = class
  private
    FYear: Integer;
    FExportToFile: Boolean; // MR:02-09-2003
  public
    procedure SetValues(Year: Integer; ExportToFile: Boolean);
  end;

  TIntArray20 = Array[1..20] of Integer;

  TReportAbsenceCardQR = class(TReportBaseF)
    QRGroupHDEmployee: TQRGroup;
    QRGroupHDDate: TQRGroup;
    QRLabelMonth: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel4: TQRLabel;
    ChildBandEmpl: TQRChildBand;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    QR1: TQRMemo;
    QR2: TQRMemo;
    QR3: TQRMemo;
    QR4: TQRMemo;
    QR5: TQRMemo;
    QR6: TQRMemo;
    QR7: TQRMemo;
    QR8: TQRMemo;
    QR9: TQRMemo;
    QR10: TQRMemo;
    QR11: TQRMemo;
    QR12: TQRMemo;
    QR13: TQRMemo;
    QR14: TQRMemo;
    QR15: TQRMemo;
    QR16: TQRMemo;
    QR17: TQRMemo;
    QR18: TQRMemo;
    QR19: TQRMemo;
    QR20: TQRMemo;
    QR21: TQRMemo;
    QR22: TQRMemo;
    QR23: TQRMemo;
    QR24: TQRMemo;
    QR25: TQRMemo;
    QR28: TQRMemo;
    QR29: TQRMemo;
    QR30: TQRMemo;
    QR26: TQRMemo;
    QR27: TQRMemo;
    QR31: TQRMemo;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRShape14: TQRShape;
    QRShape15: TQRShape;
    QRShape16: TQRShape;
    QRShape17: TQRShape;
    QRShape18: TQRShape;
    QRShape19: TQRShape;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRShape22: TQRShape;
    QRShape23: TQRShape;
    QRShape24: TQRShape;
    QRShape25: TQRShape;
    QRShape26: TQRShape;
    QRShape27: TQRShape;
    QRShape28: TQRShape;
    QRShape29: TQRShape;
    QRShape30: TQRShape;
    QRShape31: TQRShape;
    QRShape32: TQRShape;
    QRShape33: TQRShape;
    QRShape34: TQRShape;
    QRShape35: TQRShape;
    QRShape36: TQRShape;
    QRShape37: TQRShape;
    QRShape38: TQRShape;
    QRShape39: TQRShape;
    QRShape40: TQRShape;
    QRShape41: TQRShape;
    QRShape42: TQRShape;
    QRShape43: TQRShape;
    QRShape44: TQRShape;
    QRShape45: TQRShape;
    QRShape46: TQRShape;
    QRShape47: TQRShape;
    QRShape48: TQRShape;
    QRShape49: TQRShape;
    QRShape50: TQRShape;
    QRShape51: TQRShape;
    QRShape52: TQRShape;
    QRShape53: TQRShape;
    QRShape54: TQRShape;
    QRShape55: TQRShape;
    QRShape56: TQRShape;
    QRShape57: TQRShape;
    QRShape58: TQRShape;
    QRShape59: TQRShape;
    QRShape60: TQRShape;
    QRShape61: TQRShape;
    QRShape62: TQRShape;
    QRShape63: TQRShape;
    QRShape64: TQRShape;
    QRShape65: TQRShape;
    QRShape66: TQRShape;
    QRShape67: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShapeDATE: TQRShape;
    QRShape3: TQRShape;
    QRShape68: TQRShape;
    QRBandFTEmployee: TQRBand;
    QRLabel3: TQRLabel;
    QRMemo1: TQRMemo;
    QRLabel1: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRMemo2: TQRMemo;
    QRBandSummary: TQRBand;
    QRBandTitle: TQRBand;
    QRLabelTitle: TQRLabel;
    procedure QRGroupHDDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFTEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QRBandFTEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandTitleAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndBasePageHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate, FMaxDate: TDateTime;
    VQRGroupHDDateHeight: Integer;
    VMemoHeight: Integer;
    VShapeHeight: Integer;
    VQRShapeDATETop: Integer;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      EmployeeFrom, EmployeeTo: String;
      const Year: Integer;
      const ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure ClearMemoField;

    procedure FillMemoField(Day: Integer; SpaceFill: Boolean);

    function GetMonth(MonthInt: Integer): String;
    procedure SetMemoHeight(HeightMemo: Integer );
    procedure SetVertShapeHeight(HeightShape: Integer );

  end;

var
  ReportAbsenceCardQR: TReportAbsenceCardQR;
  // MR:02-09-2003
  ValueList: array[1..MAXDAY] of String;
  MyFirstPage: Boolean;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportAbsenceSheduleDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  UPimsMessageRes, ReportAbsenceCardDMT, CalculateTotalHoursDMT;

// MR:02-09-2003
procedure ClearValueList;
var
  I: Integer;
begin
  for I := 1 to MAXDAY do
    ValueList[I] := '';
end;

// MR:02-09-2003
procedure ExportDetail(Month: String);
var
  I: Integer;
  Line: String;
begin
  Line := Month + ExportClass.Sep;
  for I := 1 to MAXDAY - 1 do
    Line := Line + ValueList[I] + ExportClass.Sep;
  Line := Line + ValueList[MAXDAY];
  ExportClass.AddText(Line);
end;

procedure TQRParameters.SetValues(Year: Integer; ExportToFile: Boolean);
begin
  FYear := Year;
  FExportToFile := ExportToFile;
end;

function TReportAbsenceCardQR.QRSendReportParameters(const PlantFrom, PlantTo,
  EmployeeFrom, EmployeeTo: String;
  const Year: Integer;
  const ExportToFile: Boolean ): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, ExportToFile);
  end;
  SetDataSetQueryReport(ReportAbsenceCardDM.QueryAbsence);
end;


function TReportAbsenceCardQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  Month{, ABSRecordCount}: Word;
begin
  Screen.Cursor := crHourGlass;
  {open all linked datasets}
  FMinDate := EncodeDate(QRParameters.FYear, 1, 1);
  FMaxDate := EncodeDate(QRParameters.FYear, 12, 31);
  // GLOB3-82 Show Employee's plant!
  SelectStr :=
    'SELECT ' + NL +
    '  E.PLANT_CODE, ' + NL +
    '  A.SHIFT_NUMBER, ' + NL +
    '  A.EMPLOYEE_NUMBER, ' + NL +
    '  A.ABSENCEHOUR_DATE, ' + NL +
    '  R.ABSENCEREASON_CODE, ' + NL +
    '  SUM(A.ABSENCE_MINUTE) AS SUMMIN ' + NL +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A, ' + NL +
    '  ABSENCEREASON R, ' + NL +
    '  EMPLOYEE E ' + NL +
    'WHERE ' + NL +
    '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  AND R.ABSENCEREASON_ID = A.ABSENCEREASON_ID ' + NL +
    '  AND E.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) +  ''''  + NL +
    '  AND E.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) +  ''''  + NL +
    '  AND A.ABSENCEHOUR_DATE >= :FDATEFROM AND ' + NL +
    '  A.ABSENCEHOUR_DATE <= :FDATETO ' + NL;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    SelectStr := SelectStr +
      '  AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
      '  AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  E.PLANT_CODE, A.SHIFT_NUMBER, A.EMPLOYEE_NUMBER, A.ABSENCEHOUR_DATE, ' + NL +
    '  R.ABSENCEREASON_CODE ' + NL;
  // PIM-328
  SelectStr := SelectStr +
  ' UNION ALL ' + NL +
  'SELECT ' + NL +
  '  E.PLANT_CODE, ' + NL +
  '  EA.SHIFT_NUMBER, ' + NL +
  '  EA.EMPLOYEE_NUMBER, ' + NL +
  '  EA.EMPLOYEEAVAILABILITY_DATE, ' + NL +
  '  EA.AVAILABLE_TIMEBLOCK_1 || EA.AVAILABLE_TIMEBLOCK_2 || ' + NL +
  '  EA.AVAILABLE_TIMEBLOCK_3 || EA.AVAILABLE_TIMEBLOCK_4 || ' + NL +
  '  EA.AVAILABLE_TIMEBLOCK_5 || EA.AVAILABLE_TIMEBLOCK_6 || ' + NL +
  '  EA.AVAILABLE_TIMEBLOCK_7 || EA.AVAILABLE_TIMEBLOCK_8 || ' + NL +
  '  EA.AVAILABLE_TIMEBLOCK_9 || EA.AVAILABLE_TIMEBLOCK_10, ' + NL +
  '  -2 ' + NL +
  'FROM EMPLOYEEAVAILABILITY EA, EMPLOYEE E ' + NL +
  'WHERE ' + NL +
  '   EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
  '  ( ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_1 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_2 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_3 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_4 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_5 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_6 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_7 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_8 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_9 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) OR ' + NL +
  '    (EA.AVAILABLE_TIMEBLOCK_10 IN (SELECT AR2.ABSENCEREASON_CODE FROM ABSENCEREASON AR2)) ' + NL +
  '  ) AND ' + NL +
  '  E.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) +  ''''  + ' AND ' + NL +
  '  E.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) +  ''''  + ' AND ' + NL;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    SelectStr := SelectStr +
      '  EA.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EA.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL;
  SelectStr := SelectStr +
  '  EA.EMPLOYEEAVAILABILITY_DATE >= :FDATEFROM AND ' + NL +
  '  EA.EMPLOYEEAVAILABILITY_DATE <= :FDATETO ' + NL +
  '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN ' + NL +
  '  (SELECT AHE.ABSENCEHOUR_DATE ' + NL +
  '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON ' + NL +
  '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID ' + NL +
  '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND ' + NL +
  '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) ' + NL;
// GLOB3-82
  for Month := 1 to 12 do
  begin
    SelectStr := SelectStr +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  E.PLANT_CODE, ' + NL +
      '  A.SHIFT_NUMBER, ' + NL +
      '  A.EMPLOYEE_NUMBER, ' + NL +
      Format('  TO_DATE(''%d-%d-%d'',''dd-mm-yyyy''), ',
         [1, Month, QRParameters.FYear]) + NL +
      '  CAST ('''' AS VARCHAR(1)), ' + NL +
      '  CAST(-1 AS NUMBER(10)) ' + NL +
      'FROM ' + NL +
      '  ABSENCEHOURPEREMPLOYEE A, EMPLOYEE E ' + NL +
      'WHERE ' + NL +
      '  A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  AND E.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) +  ''''  + NL +
      '  AND E.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) +  ''''  + NL +
      '  AND A.ABSENCEHOUR_DATE >= :FDATEFROM ' + NL +
      '  AND A.ABSENCEHOUR_DATE <= :FDATETO ' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      SelectStr := SelectStr +
        '  AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
  end;
  for Month := 1 to 12 do
  begin
    SelectStr := SelectStr +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  E.PLANT_CODE, ' + NL +
      '  EA.SHIFT_NUMBER, ' + NL +
      '  EA.EMPLOYEE_NUMBER, ' + NL +
      Format('  TO_DATE(''%d-%d-%d'',''dd-mm-yyyy''), ',
         [1, Month, QRParameters.FYear]) + NL +
      '  CAST ('''' AS VARCHAR(1)), ' + NL +
      '  CAST(-1 AS NUMBER(10)) ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY EA, EMPLOYEE E ' + NL +
      'WHERE ' + NL +
      '  EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  AND E.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) +  ''''  + NL +
      '  AND E.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) +  ''''  + NL +
      '  AND EA.EMPLOYEEAVAILABILITY_DATE >= :FDATEFROM ' + NL +
      '  AND EA.EMPLOYEEAVAILABILITY_DATE <= :FDATETO ' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      SelectStr := SelectStr +
        '  AND EA.EMPLOYEE_NUMBER >= :EMPLOYEEFROM ' + NL +
        '  AND EA.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL;
  end;
  SelectStr := SelectStr + ' ' +
    'ORDER BY ' +
    '  3, 4 '; // PIM-328.2 sort on date! (not on 1,2,3) // GLOB3-82 Sort on Emplyee, Date
  ReportAbsenceCardDM.QueryAbsence.Active := False;
  ReportAbsenceCardDM.QueryAbsence.UniDirectional := False;
  ReportAbsenceCardDM.QueryAbsence.SQL.Clear;
  ReportAbsenceCardDM.QueryAbsence.SQL.Add(SelectStr);
// ReportAbsenceCardDM.QueryAbsence.SQL.SaveToFile('c:\temp\reportabsencecard.sql');
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    ReportAbsenceCardDM.QueryAbsence.ParamByName('EMPLOYEEFROM').AsInteger :=
      StrToInt(QRBaseParameters.FEmployeeFrom);
    ReportAbsenceCardDM.QueryAbsence.ParamByName('EMPLOYEETO').AsInteger :=
      StrToInt(QRBaseParameters.FEmployeeTo);
  end;
  ReportAbsenceCardDM.QueryAbsence.
    ParamByName('FDATEFROM').AsDateTime := FMinDate;
  ReportAbsenceCardDM.QueryAbsence.
    ParamByName('FDATETO').AsDateTime :=  FMaxDate;
  ReportAbsenceCardDM.QueryAbsence.Prepare;
  ReportAbsenceCardDM.QueryAbsence.Active := True;
  Result := (not ReportAbsenceCardDM.QueryAbsence.IsEmpty);
  {check if report is empty}
  Screen.Cursor := crDefault;
end; // ExistsRecords

procedure TReportAbsenceCardQR.ConfigReport;
begin
  // GLOB3-82
  MyFirstPage := True;
  QRBandTitle.Height := 0;

  // MR:02-09-2003
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption + ' - ' +
    SPimsYear + ' ' + IntToStr(QRParameters.FYear);

  // GLOB3-82 Show this together in emp. header, not in 'base-title'.
  QRLabelTitle.Caption := SPimsAbsenceCard + ' - ' +
    SPimsYear + ' ' + IntToStr(QRParameters.FYear);

  qrptBase.ReportTitle := QRLblBaseTitle.Caption ;
  QRLblBaseLaundry.Caption := '';

end;

procedure TReportAbsenceCardQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportAbsenceCardQR.ClearMemoField;
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
      if (TempComponent as TQRMemo).Tag >= 1 then
      (TempComponent as TQRMemo).Lines.Clear;
  end;
end;


procedure TReportAbsenceCardQR.SetMemoHeight(HeightMemo: Integer );
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
      if (TempComponent as TQRMemo).Tag >= 1 then
      begin
        (TempComponent as TQRMemo).Height := HeightMemo;
        (TempComponent as TQRMemo).Top := 3;
      end;
  end;
end;

procedure TReportAbsenceCardQR.SetVertShapeHeight(HeightShape: Integer );
 var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRShape) then
      if (TempComponent as TQRShape).Tag = 1 then
      begin
        (TempComponent as TQRShape).Height := HeightShape;
        (TempComponent as TQRShape).Top := -3;
      end;
  end;
end;

procedure TReportAbsenceCardQR.FillMemoField(Day : Integer; SpaceFill: Boolean);
const MySpace='  ';
var
  TempComponent: TComponent;
  Counter: Integer;
  SumMin: Double;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRMemo) then
    begin
      if (TempComponent as TQRMemo).Tag = Day then
      begin
        // PIM-328 Planned data
        if ReportAbsenceCardDM.QueryAbsence.FieldByName('SUMMIN').AsFloat = -2 then
        begin
          SumMin := -1;
        end
        else
        begin
          SumMin :=
            ReportAbsenceCardDM.QueryAbsence.FieldByName('SUMMIN').AsFloat;
        end;
        if SpaceFill then
          (TempComponent as TQRMemo).Lines.Add(MySpace)
        else
        begin
          // PIM-328 Planned data
          if SumMin <> -1 then
            (TempComponent as TQRMemo).Lines.Add(MySpace +
              ReportAbsenceCardDM.QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString +
              '  ')
          else // GLOB3-60 Planned data: If more than 5 timeblocks, use 2 lines.
          begin
            if Length(ReportAbsenceCardDM.QueryAbsence.
                   FieldByName('ABSENCEREASON_CODE').AsString) > 5 then
            begin
              (TempComponent as TQRMemo).Lines.Add(
                Copy(ReportAbsenceCardDM.QueryAbsence.
                     FieldByName('ABSENCEREASON_CODE').AsString,1,5)
                );
              (TempComponent as TQRMemo).Lines.Add(
                Copy(ReportAbsenceCardDM.QueryAbsence.
                     FieldByName('ABSENCEREASON_CODE').AsString,6,5)
                );
            end
            else
              (TempComponent as TQRMemo).Lines.Add(
                   ReportAbsenceCardDM.QueryAbsence.
                     FieldByName('ABSENCEREASON_CODE').AsString
                );
          end;
          if SumMin <> -1 then
            (TempComponent as TQRMemo).Lines.Add(
              DecodeHrsMinL(Round(SumMin),
               5));
          // MR:02-09-2003
          if QRParameters.FExportToFile then
          begin
            if SumMin <> -1 then
              ValueList[Day] :=
                ReportAbsenceCardDM.QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString + ' ' +
                  DecodeHrsMinL(Round(SumMin), 5)
            else
              ValueList[Day] :=
                ReportAbsenceCardDM.QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
          end;
        end;
        Exit;
      end;
    end;
  end;
end;

function TReportAbsenceCardQR.GetMonth(MonthInt: Integer): String;
begin
  Result := '';
  case MonthInt of
   1: Result := SJanuary;
   2: Result := SFebruary;
   3: Result := SMarch;
   4: Result := SApril;
   5: Result := SMay;
   6: Result := SJune;
   7: Result := SJuly;
   8: Result := SAugust;
   9: Result := SSeptember;
  10: Result := SOctober;
  11: Result := SNovember;
  12: Result := SDecember;
  end;
end;


procedure TReportAbsenceCardQR.QRGroupHDDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Year, Month,  PrevMonth, Day: Word;
  SAVEEmpl, SaveDay: Integer;
begin
  inherited;

  // MR:02-09-2003
  ClearValueList;
{
  QRGroupHDDate.Height := 40;
  SetMemoHeight(30);
  SetVertShapeHeight(40);
  QRShapeDATE.Top := 36;
}
  QRGroupHDDate.Height := VQRGroupHDDateHeight;
  SetMemoHeight(VMemoHeight);
  SetVertShapeHeight(VShapeHeight);
  QRShapeDATE.Top := VQRShapeDATETop;

  DecodeDate(ReportAbsenceCardDM.QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
     Year, PrevMonth, Day);

  Month := PrevMonth;
  QRLabelMonth.Caption := GetMonth(Month);

  ClearMemoField;
  SaveEmpl := ReportAbsenceCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  SaveDay := 0;
  while (Month = PrevMonth) and (not ReportAbsenceCardDM.QueryAbsence.Eof) and
  (SaveEmpl = ReportAbsenceCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger) do
  begin
    if Round(ReportAbsenceCardDM.QueryAbsence.FieldByName('SUMMIN').AsFloat) = -1 then
    else
    begin
      if SaveDay = Day then
      begin
{
        QRGroupHDDate.Height := 70;
        SetMemoHeight(58);
        SetVertShapeHeight(80);
        QRShapeDATE.Top := 65;
}
        QRGroupHDDate.Height := VQRGroupHDDateHeight * 2;
        SetMemoHeight(VMemoHeight * 2);
        SetVertShapeHeight(VShapeHeight * 2);
        QRShapeDATE.Top := VQRShapeDATETop * 2;
      end;

      if Day - SaveDay > 1 then
        FillMemoField(SaveDay + 1, True);
      FillMemoField(Day, False);
      SaveDay := Day;
    end;
    ReportAbsenceCardDM.QueryAbsence.Next;
    DecodeDate(ReportAbsenceCardDM.QueryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
     Year, Month, Day);
  end;
  if not ReportAbsenceCardDM.QueryAbsence.Eof then
    ReportAbsenceCardDM.QueryAbsence.Prior;

  // MR:02-09-2003
  if QRParameters.FExportToFile then
    ExportDetail(QRLabelMonth.Caption);
end;

procedure TReportAbsenceCardQR.QRBandFTEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Column: Integer;
  Msg: String;
begin
  inherited;
  // GLOB3-82 Determine total days per absence reason
  QRMemo1.Lines.Clear;
  QRMemo2.Lines.Clear;
  with ReportAbsenceCardDM do
  begin
    qryAbsenceReasons.Close;
    qryAbsenceReasons.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryAbsenceReasons.ParamByName('DATEFROM').AsDateTime := FMinDate;
    qryAbsenceReasons.ParamByName('DATETO').AsDateTime := FMaxDate;
    qryAbsenceReasons.Open;
    qryAbsenceReasons.First;
    Column := 1;
    while not qryAbsenceReasons.Eof do
    begin
      Msg := qryAbsenceReasons.
          FieldByName('ABSENCEREASON_CODE').AsString + '  ' +
          qryAbsenceReasons.FieldByName('DESCRIPTION').AsString + ' (' +
          qryAbsenceReasons.FieldByName('DAYS').AsString + ')';
      if Column > 2 then
        Column := 1;
      case Column of
      1: QRMemo1.Lines.Add(Msg);
      2: QRMemo2.Lines.Add(Msg);
      end;
      qryAbsenceReasons.Next;
      Inc(Column);
    end; // while not qryAbsenceReasons.Eof do
  end; // with ReportAbsenceCardDM do
end;

procedure TReportAbsenceCardQR.FormCreate(Sender: TObject);
const // Use this to get more on 1 page: Lower=Smaller.
  SizePerc = 100;
begin
  inherited;
  MyFirstPage := True;
  VQRGroupHDDateHeight := Round(QRGroupHDDate.Height / 100 * SizePerc);
  VMemoHeight := Round(QR1.Height / 100 * SizePerc);
  VShapeHeight := Round(QRShape4.Height / 100 * SizePerc);
  VQRShapeDATETop := Round(QRShapeDATE.Top / 100 * SizePerc);

  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportAbsenceCardQR.QRBandFTEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
  Line: String;
begin
  inherited;
  // MR:02-09-2003
  if QRParameters.FExportToFile then
  begin
    Line := QRLabel3.Caption + ExportClass.Sep;
    for I := 0 to QRMemo1.Lines.Count - 1 do
      Line := Line + QRMemo1.Lines[I] + ExportClass.Sep;
    for I := 0 to QRMemo2.Lines.Count - 1 do
      Line := Line + QRMemo2.Lines[I] + ExportClass.Sep;
    ExportClass.AddText(Line);
  end;
  // GLOB3-82 Do this to prevent it prints the second page without a page-break,
  //          which happens only during first page and when having ForceNewPage
  //          set to True for QRGroupHDEmployee.
  if MyFirstPage then
  begin
    if not ReportAbsenceCardDM.QueryAbsence.Eof then
      qrptBase.NewPage;
    MyFirstPage := False;
  end;
  QRGroupHDEmployee.ForceNewPage := True;
end;

procedure TReportAbsenceCardQR.QRGroupHDEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
begin
  inherited;
  // MR:02-09-2003
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    // Set Filename to selected year+plant+employee.
    ExportClass.Filename := Caption + '-' +
      IntToStr(QRParameters.FYear) + '-' +
      ReportAbsenceCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + '-' +
      ReportAbsenceCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString; // + '-' + '.txt'; // RV097.1.
    // Create a header
    ExportClass.AddText(IntToStr(QRParameters.FYear));
    ExportClass.AddText(QRLabel4.Caption + ' ' +
      ReportAbsenceCardDM.QueryAbsence.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportAbsenceCardDM.TablePlant.FieldByName('DESCRIPTION').AsString + ' ' +
      QRLabel1.Caption + ' ' +
      ReportAbsenceCardDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
      ReportAbsenceCardDM.TableEmpl.FieldByName('DESCRIPTION').AsString);
    ClearValueList;
    for I := 1 to MAXDAY do
      ValueList[I] := IntToStr(I);
    ExportDetail('');
  end;
end;

procedure TReportAbsenceCardQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportAbsenceCardQR.QRBandTitleAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
  end;
end;

procedure TReportAbsenceCardQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportAbsenceCardQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportAbsenceCardQR.QRBndBasePageHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

end.


