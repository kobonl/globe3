unit MistakePerEmplDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TMistakePerEmplDM = class(TGridBaseDM)
    TablePlant: TTable;
    QueryDetail: TQuery;
    TableTeam: TTable;
    QueryPlantTeam: TQuery;
    DataSourcePlantTeam: TDataSource;
    DataSourceTeam: TDataSource;
    QueryDetailEMPLOYEE_NUMBER: TIntegerField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailNUMBER_OF_MISTAKE: TIntegerField;
    QueryDetailWEEKMISTAKE: TIntegerField;
    TableMPE: TTable;
    procedure QueryDetailCalcFields(DataSet: TDataSet);
    procedure QueryDetailAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure GotoEmpl(Empl:Integer);
    procedure UpdateValues(TempTable: TTable);
    procedure FillQueryDetail(AllTeam: Boolean;
      Team, Plant: String; DateEmpl: TDateTime);
    procedure UpdateTableMPE(Empl: Integer; DateEmp: TDateTime; Mistake: String);
  end;

var
  MistakePerEmplDM: TMistakePerEmplDM;

implementation

uses MistakePerEmplFRM, ListProcsFRM, SystemDMT;

{$R *.DFM}
procedure TMistakePerEmplDM.FillQueryDetail(AllTeam: Boolean;
  Team, Plant: String; DateEmpl: TDateTime);
var
  SelectStr: String;
begin
  if ((Team = '') and not AllTeam) or (Plant = '') or( DateEmpl = 0) then
  begin
    Team := 'EMPTY';
    Plant := 'EMPTY';
    Exit;
  end;
  SelectStr := 
    'SELECT ' +
    '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' +
    '  E.TEAM_CODE, E.PLANT_CODE, MPE.NUMBER_OF_MISTAKE ' +
    'FROM ' +
    '  EMPLOYEE E LEFT JOIN MISTAKEPEREMPLOYEE MPE ON ' +
    '    E.EMPLOYEE_NUMBER = MPE.EMPLOYEE_NUMBER AND ' +
    '    (MPE.MISTAKE_DATE = :FDATE) ';
  if ((not AllTeam) and (Team <> '')) or (Plant <> '') then
    SelectStr := SelectStr + ' WHERE ' ;
  if (not AllTeam) and (Team <> '') then
     SelectStr := SelectStr + ' (E.TEAM_CODE = ''' + DoubleQuote(Team) + ''') ' ;
  if (Plant <> '') then
  begin
    if (not AllTeam) and (Team <> '') then
      SelectStr := SelectStr + ' AND ';
    SelectStr := SelectStr + ' (E.PLANT_CODE = ''' + DoubleQuote(Plant) + ''') ';
  end;
  SelectStr := SelectStr + 
    '  ORDER BY E.EMPLOYEE_NUMBER';
  QueryDetail.Active := False;
  QueryDetail.UniDirectional := False;
  QueryDetail.SQL.Clear;
  QueryDetail.SQL.Add(UpperCase(SelectStr));
  QueryDetail.ParamByName('FDATE').Value := GetDate(DateEmpl);
  if not QueryDetail.Prepared then
     QueryDetail.Prepare;
  QueryDetail.Active := True;
end;

procedure TMistakePerEmplDM.GotoEmpl(Empl:Integer);
begin
  QueryDetail.First;
  while not QueryDetail.Eof and
    (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger < Empl ) do
    QueryDetail.Next;

  if not QueryDetail.Eof and
    (QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger = Empl ) then
     Exit
  else
    QueryDetail.First;
end;

procedure TMistakePerEmplDM.QueryDetailCalcFields(DataSet: TDataSet);
var
  Year, Week: Word;
  Empl: Integer;
  DateCurrent, DateMin, DateMax: TDateTime;
  SumMistake: Integer;
begin
  inherited;
  QueryDetailWEEKMISTAKE.Value := 0;
  Empl := QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  DateCurrent := GetDate(MistakePerEmplF.DateFrom.Date);
  ListProcsF.WeekUitDat(DateCurrent, Year, Week);
  DateMin := ListProcsF.DateFromWeek(Year, Week, 1);
  DateMax := ListProcsF.DateFromWeek(Year, Week, 7);
  TableMPE.FindNearest([Empl, DateMin]);
  SumMistake := 0;
  while not TableMPE.Eof and
    (TableMPE.FieldByName('EMPLOYEE_NUMBER').AsInteger = Empl) and
{    (DateToStr(TableMPE.FieldByName('MISTAKE_DATE').AsDateTime) <= DateToStr(DateMax))}
   ( CompareDate(TableMPE.FieldByName('MISTAKE_DATE').AsDateTime, DateMax) <= 0)
   do
  begin
    SumMistake := SumMistake + TableMPE.FieldByName('NUMBER_OF_MISTAKE').AsInteger;
    TableMPE.Next;
  end;
  QueryDetailWEEKMISTAKE.Value := SumMistake;
end;

procedure TMistakePerEmplDM.UpdateValues(TempTable: TTable);
begin
  TempTable.FieldByName('CREATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATIONDATE').Value := Now;
  TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
end;

procedure TMistakePerEmplDM.UpdateTableMPE(Empl: Integer;
  DateEmp: TDateTime; Mistake: String);
begin
  if Mistake = '' then
  begin
    if TableMPE.FindKey([Empl, DateEmp]) then
      TableMPE.Delete;
  end
  else
  begin
    if TableMPE.FindKey([Empl, DateEmp]) then
    begin
      TableMPE.Edit;
      TableMPE.FieldByName('NUMBER_OF_MISTAKE').AsInteger := StrToInt(Mistake);
      UpdateValues(TableMPE);
      TableMPE.Post;
    end
    else
    begin
      TableMPE.Insert;
      TableMPE.FieldByName('NUMBER_OF_MISTAKE').AsInteger := StrToInt(Mistake);
      TableMPE.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
      TableMPE.FieldByName('MISTAKE_DATE').AsDateTime := DateEmp;
      UpdateValues(TableMPE);
      TableMPE.Post;
    end;
  end;
end;

procedure TMistakePerEmplDM.QueryDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  MistakePerEmplF.EditMistake.Text :=
     QueryDetail.FieldByName('NUMBER_OF_MISTAKE').AsString;
end;

end.
