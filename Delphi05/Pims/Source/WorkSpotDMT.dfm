inherited WorkSpotDM: TWorkSpotDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 278
  Top = 108
  Height = 755
  Width = 919
  inherited TableMaster: TTable
    AfterScroll = TableMasterAfterScroll
    OnFilterRecord = TableMasterFilterRecord
    TableName = 'PLANT'
    Left = 56
    Top = 8
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableMasterFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TableMasterINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TableMasterOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
    object TableMasterAVERAGE_WAGE: TFloatField
      FieldName = 'AVERAGE_WAGE'
    end
    object TableMasterCUSTOMER_NUMBER: TIntegerField
      FieldName = 'CUSTOMER_NUMBER'
    end
    object TableMasterCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
  end
  inherited TableDetail: TTable
    BeforeInsert = TableDetailBeforeInsert
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    BeforeDelete = TableDetailBeforeDelete
    OnNewRecord = TableDetailNewRecord
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    TableName = 'WORKSPOT'
    Left = 56
    Top = 64
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailUSE_JOBCODE_YN: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableDetailMEASURE_PRODUCTIVITY_YN: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object TableDetailPRODUCTIVE_HOUR_YN: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object TableDetailQUANT_PIECE_YN: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object TableDetailAUTOMATIC_DATACOL_YN: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object TableDetailCOUNTER_VALUE_YN: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object TableDetailENTER_COUNTER_AT_SCAN_YN: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object TableDetailDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourtype
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Lookup = True
    end
    object TableDetailAUTOMATIC_RESCAN_YN: TStringField
      FieldName = 'AUTOMATIC_RESCAN_YN'
      Size = 1
    end
    object TableDetailSHORT_NAME: TStringField
      DisplayWidth = 20
      FieldName = 'SHORT_NAME'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailDEPTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPTLU'
      LookupDataSet = QueryDeptLU
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailGRADE: TIntegerField
      FieldName = 'GRADE'
    end
    object TableDetailGROUP_EFFICIENCY_YN: TStringField
      FieldName = 'GROUP_EFFICIENCY_YN'
      Size = 1
    end
    object TableDetailMACHINE_CODE: TStringField
      FieldName = 'MACHINE_CODE'
      Size = 6
    end
    object TableDetailMACHINELU: TStringField
      FieldKind = fkLookup
      FieldName = 'MACHINELU'
      LookupDataSet = qryMachineLU
      LookupKeyFields = 'MACHINE_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'MACHINE_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Size = 6
    end
    object TableDetailCONTRACTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'CONTRACTLU'
      LookupDataSet = qryContractGroup
      LookupKeyFields = 'CONTRACTGROUP_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CONTRACTGROUP_CODE'
      Size = 30
      Lookup = True
    end
    object TableDetailEFF_RUNNING_HRS_YN: TStringField
      FieldName = 'EFF_RUNNING_HRS_YN'
      Size = 1
    end
    object TableDetailTIMEREC_BY_MACHINE_YN: TStringField
      FieldName = 'TIMEREC_BY_MACHINE_YN'
      Size = 4
    end
    object TableDetailFAKE_EMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'FAKE_EMPLOYEE_NUMBER'
    end
    object TableDetailPS_QTYVIA_EXTERNAL_YN: TStringField
      FieldName = 'PS_QTYVIA_EXTERNAL_YN'
      Size = 4
    end
    object TableDetailHOST: TStringField
      FieldName = 'HOST'
      Size = 120
    end
    object TableDetailPORT: TFloatField
      Alignment = taLeftJustify
      FieldName = 'PORT'
    end
    object TableDetailBATCH: TFloatField
      FieldName = 'BATCH'
    end
    object TableDetailROAMING_YN: TStringField
      FieldName = 'ROAMING_YN'
      Size = 4
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 164
    Top = 8
  end
  inherited DataSourceDetail: TDataSource
    Left = 164
    Top = 60
  end
  inherited TableExport: TTable
    Left = 284
    Top = 484
  end
  inherited DataSourceExport: TDataSource
    Left = 384
    Top = 484
  end
  object TableHourtype: TTable
    DatabaseName = 'Pims'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = TableHourtypeFilterRecord
    TableName = 'HOURTYPE'
    Left = 56
    Top = 160
  end
  object TableBUWorkspot: TTable
    BeforePost = TableBUWorkspotBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = TableBUWorkspotNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;BUSINESSUNIT_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'BUSINESSUNITPERWORKSPOT'
    Left = 392
    Top = 60
    object TableBUWorkspotPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableBUWorkspotWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableBUWorkspotBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableBUWorkspotPERCENTAGE: TFloatField
      FieldName = 'PERCENTAGE'
    end
    object TableBUWorkspotCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableBUWorkspotMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableBUWorkspotMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableBUWorkspotBUSINESSUNITLU: TStringField
      FieldKind = fkLookup
      FieldName = 'BUSINESSUNITLU'
      LookupDataSet = QueryBU
      LookupKeyFields = 'BUSINESSUNIT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'BUSINESSUNIT_CODE'
      Size = 30
      Lookup = True
    end
  end
  object TableJobCode: TTable
    BeforePost = TableJobCodeBeforePost
    AfterPost = TableJobCodeAfterPost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = TableJobCodeNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'JOBCODE'
    Left = 392
    Top = 8
    object TableJobCodePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableJobCodeBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableJobCodeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableJobCodeNORM_PROD_LEVEL: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
      OnValidate = DefaultNotEmptyValidate
      MaxValue = 99999.99
    end
    object TableJobCodeBONUS_LEVEL: TFloatField
      FieldName = 'BONUS_LEVEL'
      OnValidate = DefaultNotEmptyValidate
      MaxValue = 99999.99
    end
    object TableJobCodeNORM_OUTPUT_LEVEL: TFloatField
      FieldName = 'NORM_OUTPUT_LEVEL'
      OnValidate = DefaultNotEmptyValidate
      MaxValue = 99999.99
    end
    object TableJobCodeINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Size = 6
    end
    object TableJobCodeDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableJobCodeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableJobCodeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableJobCodeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableJobCodeIGNOREINTERFACECODE_YN: TStringField
      FieldName = 'IGNOREINTERFACECODE_YN'
      Size = 1
    end
    object TableJobCodeINTERFACELU: TStringField
      FieldKind = fkLookup
      FieldName = 'INTERFACELU'
      LookupDataSet = TableIgnoreInterface
      LookupKeyFields = 'INTERFACE_CODE'
      LookupResultField = 'INTERFACE_CODE'
      KeyFields = 'INTERFACE_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
    object TableJobCodeBULU: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'BULU'
      LookupDataSet = QueryBU
      LookupKeyFields = 'BUSINESSUNIT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'BUSINESSUNIT_CODE'
      Size = 30
      Lookup = True
    end
    object TableJobCodeSHOW_AT_SCANNING_YN: TStringField
      FieldName = 'SHOW_AT_SCANNING_YN'
      Size = 1
    end
    object TableJobCodeCOMPARATION_JOB_YN: TStringField
      FieldName = 'COMPARATION_JOB_YN'
      Size = 1
    end
    object TableJobCodeMAXIMUM_DEVIATION: TFloatField
      Alignment = taLeftJustify
      FieldName = 'MAXIMUM_DEVIATION'
      MaxValue = 999.99
    end
    object TableJobCodeCOMPARISON_REJECT_YN: TStringField
      FieldName = 'COMPARISON_REJECT_YN'
      Size = 1
    end
    object TableJobCodeSHOW_AT_PRODUCTIONSCREEN_YN: TStringField
      FieldName = 'SHOW_AT_PRODUCTIONSCREEN_YN'
      Size = 1
    end
    object TableJobCodeEMPS_SCAN_IN_YN: TStringField
      FieldName = 'EMPS_SCAN_IN_YN'
      Size = 1
    end
    object TableJobCodeIGNOREQUANTITIESINREPORTS_YN: TStringField
      FieldName = 'IGNOREQUANTITIESINREPORTS_YN'
      Size = 1
    end
    object TableJobCodePRODLEVELMINPERC: TIntegerField
      FieldName = 'PRODLEVELMINPERC'
    end
    object TableJobCodeMACHOUTPUTMINPERC: TIntegerField
      FieldName = 'MACHOUTPUTMINPERC'
    end
    object TableJobCodeDEFAULT_YN: TStringField
      FieldName = 'DEFAULT_YN'
      Size = 4
    end
    object TableJobCodeSAMPLE_TIME_MINS: TIntegerField
      FieldName = 'SAMPLE_TIME_MINS'
    end
  end
  object DataSourceJobCode: TDataSource
    DataSet = TableJobCode
    Left = 528
    Top = 8
  end
  object DataSourceBUWorkSpot: TDataSource
    DataSet = TableBUWorkspot
    Left = 528
    Top = 56
  end
  object QueryDel: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'DELETE  FROM JOBCODE '
      'WHERE PLANT_CODE =:PLANT_CODE AND'
      'WORKSPOT_CODE =:WORKSPOT_CODE')
    Left = 280
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryWORK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 176
    Top = 488
  end
  object TableIgnoreInterface: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;INTERFACE_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
    MasterSource = DataSourceJobCode
    TableName = 'IGNOREINTERFACECODE'
    Left = 396
    Top = 168
    object TableIgnoreInterfacePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableIgnoreInterfaceWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableIgnoreInterfaceJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object TableIgnoreInterfaceINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Required = True
      Size = 6
    end
    object TableIgnoreInterfaceCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableIgnoreInterfaceMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableIgnoreInterfaceMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object DataSourceIgnoreInterface: TDataSource
    DataSet = TableIgnoreInterface
    Left = 528
    Top = 168
  end
  object TableJobForInterface: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'JOBCODE'
    Left = 400
    Top = 224
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object StringField3: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object StringField4: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object StringField5: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object FloatField1: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
      MaxValue = 99999.99
    end
    object FloatField2: TFloatField
      FieldName = 'BONUS_LEVEL'
      MaxValue = 99999.99
    end
    object FloatField3: TFloatField
      FieldName = 'NORM_OUTPUT_LEVEL'
      MaxValue = 99999.99
    end
    object StringField6: TStringField
      FieldName = 'INTERFACE_CODE'
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField7: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField9: TStringField
      FieldName = 'IGNOREINTERFACECODE_YN'
      Size = 1
    end
    object StringField10: TStringField
      FieldKind = fkLookup
      FieldName = 'INTERFACELU'
      LookupDataSet = TableIgnoreInterface
      LookupKeyFields = 'INTERFACE_CODE'
      LookupResultField = 'INTERFACE_CODE'
      KeyFields = 'INTERFACE_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
    object StringField11: TStringField
      FieldKind = fkLookup
      FieldName = 'BULU'
      LookupKeyFields = 'BUSINESSUNIT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'BUSINESSUNIT_CODE'
      LookupCache = True
      Size = 6
      Lookup = True
    end
  end
  object QueryInterface: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  W.PLANT_CODE, W.WORKSPOT_CODE, W.DESCRIPTION,'
      '  J.JOB_CODE, J.DESCRIPTION, J.INTERFACE_CODE'
      'FROM'
      '  WORKSPOT W, JOBCODE J'
      'WHERE'
      '  W.PLANT_CODE = J.PLANT_CODE AND'
      '  W.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  (W.PLANT_CODE = :PLANT_CODE) AND'
      '  (J.INTERFACE_CODE IS NOT NULL) AND'
      '  (J.INTERFACE_CODE <> :INTERFACE_CODE)'
      'ORDER BY'
      '  W.PLANT_CODE, W.WORKSPOT_CODE,'
      '  J.JOB_CODE, J.INTERFACE_CODE'
      ''
      ''
      ' '
      ' ')
    Left = 56
    Top = 488
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INTERFACE_CODE'
        ParamType = ptUnknown
      end>
    object QueryInterfacePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryInterfaceWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryInterfaceDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object QueryInterfaceJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object QueryInterfaceDESCRIPTION_1: TStringField
      FieldName = 'DESCRIPTION_1'
      Size = 30
    end
    object QueryInterfaceINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Size = 6
    end
  end
  object TableIgnoreTmp: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;INTERFACE_CODE'
    TableName = 'IGNOREINTERFACECODE'
    Left = 528
    Top = 224
  end
  object TableCompareJOb: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = TableCompareJObNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 
      'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;COMPARE_WORKSPOT_CODE;COMPARE_' +
      'JOB_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
    MasterSource = DataSourceJobCode
    TableName = 'COMPAREJOB'
    Left = 396
    Top = 112
    object TableCompareJObPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableCompareJObWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableCompareJObJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object TableCompareJObCOMPARE_WORKSPOT_CODE: TStringField
      FieldName = 'COMPARE_WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableCompareJObCOMPARE_JOB_CODE: TStringField
      FieldName = 'COMPARE_JOB_CODE'
      Required = True
      Size = 6
    end
    object TableCompareJObCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableCompareJObMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableCompareJObMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableCompareJObCOMPAREWKLU: TStringField
      FieldKind = fkLookup
      FieldName = 'COMPAREWKLU'
      LookupDataSet = QueryAllWK
      LookupKeyFields = 'PLANT_CODE;WORKSPOT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;COMPARE_WORKSPOT_CODE'
      Size = 30
      Lookup = True
    end
    object TableCompareJObCOMPAREJOBLU: TStringField
      FieldKind = fkLookup
      FieldName = 'COMPAREJOBLU'
      LookupDataSet = QueryAllJob
      LookupKeyFields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE;COMPARE_WORKSPOT_CODE;COMPARE_JOB_CODE'
      Size = 30
      Lookup = True
    end
  end
  object DataSourceCompareJob: TDataSource
    DataSet = TableCompareJOb
    Left = 528
    Top = 112
  end
  object QueryDeptLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryDeptLUFilterRecord
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DEPARTMENT_CODE, DESCRIPTION'
      'FROM '
      '  DEPARTMENT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE'
      'ORDER BY'
      '  DEPARTMENT_CODE')
    Left = 164
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceDeptLU: TDataSource
    DataSet = QueryDeptLU
    Left = 256
    Top = 112
  end
  object QueryGetSumPerc: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE,'
      '  SUM(PERCENTAGE)  AS SUMPERC '
      'FROM'
      '  BUSINESSUNITPERWORKSPOT '
      'WHERE'
      '  PLANT_CODE  = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE '
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE')
    Left = 176
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryCheckOnWKInterface: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE,'
      '  INTERFACE_CODE, COUNT(*)'
      'FROM'
      '  JOBCODE '
      'WHERE'
      '  PLANT_CODE  = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  INTERFACE_CODE IS NOT NULL'
      'GROUP  BY '
      '  PLANT_CODE, WORKSPOT_CODE, INTERFACE_CODE'
      'HAVING '
      '  COUNT(*) > 1')
    Left = 56
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE,BUSINESSUNIT_CODE, DESCRIPTION '
      'FROM '
      '  BUSINESSUNIT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE ')
    Left = 48
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceBULU: TDataSource
    DataSet = QueryBU
    Left = 156
    Top = 188
  end
  object QueryAllJob: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceCompareJob
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, DESCRIPTION '
      'FROM '
      '  JOBCODE  ')
    Left = 392
    Top = 344
  end
  object QueryAllWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, DESCRIPTION '
      'FROM '
      '  WORKSPOT   ')
    Left = 392
    Top = 288
  end
  object QueryWKLU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceJobCode
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, DESCRIPTION '
      'FROM'
      '  WORKSPOT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE'
      '  AND WORKSPOT_CODE <> '#39'0'#39' AND'
      '  USE_JOBCODE_YN = '#39'Y'#39)
    Left = 496
    Top = 288
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
    object QueryWKLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryWKLUWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryWKLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object DataSourceWKLU: TDataSource
    DataSet = QueryWKLU
    Left = 584
    Top = 288
  end
  object QueryJobLU: TQuery
    CachedUpdates = True
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWKLU
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, '
      '  DESCRIPTION '
      'FROM '
      '  JOBCODE'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE')
    Left = 488
    Top = 352
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
    object QueryJobLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryJobLUWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryJobLUJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object QueryJobLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object DataSourceJOBLU: TDataSource
    DataSet = QueryJobLU
    Left = 584
    Top = 352
  end
  object StoredProcDelete: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'WORKSPOT_DELETECASCADE'
    Left = 48
    Top = 352
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptInput
      end>
  end
  object qryOpenScans: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*) AS TOTALCOUNT'
      'FROM TIMEREGSCANNING T'
      'WHERE T.PLANT_CODE = :PLANT_CODE AND'
      'T.WORKSPOT_CODE = :WORKSPOT_CODE'
      'AND T.DATETIME_OUT IS NULL'
      ' ')
    Left = 168
    Top = 352
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmpPlanPWD: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT MAX(EP.EMPLOYEEPLANNING_DATE) AS EMPLOYEEPLANNING_DATE'
      'FROM EMPLOYEEPLANNING EP'
      'WHERE EP.PLANT_CODE = :PLANT_CODE AND'
      'EP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      'EP.DEPARTMENT_CODE = :DEPARTMENT_CODE AND'
      'EP.EMPLOYEEPLANNING_DATE >= TRUNC(SYSDATE) AND'
      'NOT ('
      '  (EP.SCHEDULED_TIMEBLOCK_1 = '#39'N'#39') AND'
      '  (EP.SCHEDULED_TIMEBLOCK_2 = '#39'N'#39') AND'
      '  (EP.SCHEDULED_TIMEBLOCK_3 = '#39'N'#39') AND'
      '  (EP.SCHEDULED_TIMEBLOCK_4 = '#39'N'#39')'
      ')'
      ''
      ''
      ''
      ''
      ' ')
    Left = 48
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryMachineLU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, MACHINE_CODE, DESCRIPTION'
      'FROM '
      '  MACHINE'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE')
    Left = 156
    Top = 248
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryCheckMachineJobs: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  J.JOB_CODE, COUNT(*) CNT'
      'FROM'
      '  JOBCODE J INNER JOIN WORKSPOT W ON'
      '    J.PLANT_CODE = W.PLANT_CODE AND'
      '    J.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  INNER JOIN MACHINE M ON'
      '    W.PLANT_CODE = M.PLANT_CODE AND'
      '    W.MACHINE_CODE = M.MACHINE_CODE'
      'WHERE'
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  M.MACHINE_CODE = :MACHINE_CODE AND'
      '  ((J.DATE_INACTIVE IS NULL) OR'
      '   (J.DATE_INACTIVE IS NOT NULL AND J.DATE_INACTIVE > SYSDATE))'
      'GROUP BY'
      '  J.JOB_CODE'
      'HAVING'
      '  COUNT(*) <>'
      '    (SELECT COUNT(*)'
      '     FROM'
      '       WORKSPOT W INNER JOIN MACHINE M ON'
      '         W.PLANT_CODE = M.PLANT_CODE AND'
      '         W.MACHINE_CODE = M.MACHINE_CODE'
      '     WHERE'
      '       W.PLANT_CODE = :PLANT_CODE AND'
      '       W.MACHINE_CODE = :MACHINE_CODE'
      '     )'
      'ORDER BY'
      '  J.JOB_CODE'
      '')
    Left = 392
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryMachineJobExists: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT MJ.JOB_CODE'
      'FROM MACHINEJOBCODE MJ'
      'WHERE MJ.PLANT_CODE = :PLANT_CODE AND'
      'MJ.MACHINE_CODE = :MACHINE_CODE AND'
      'MJ.JOB_CODE = :JOB_CODE'
      ' ')
    Left = 504
    Top = 408
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryCheckWorkspotJobs: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT JOB_CODE'
      'FROM JOBCODE'
      'WHERE PLANT_CODE = :PLANT_CODE'
      'AND WORKSPOT_CODE = :WORKSPOT_CODE')
    Left = 296
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryAddMachJobsToWS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO JOBCODE'
      
        '(PLANT_CODE, WORKSPOT_CODE, JOB_CODE, BUSINESSUNIT_CODE, DESCRIP' +
        'TION,'
      
        ' NORM_PROD_LEVEL, BONUS_LEVEL, NORM_OUTPUT_LEVEL, INTERFACE_CODE' +
        ', DATE_INACTIVE,'
      
        ' CREATIONDATE, MUTATIONDATE, MUTATOR, IGNOREINTERFACECODE_YN, SH' +
        'OW_AT_SCANNING_YN,'
      
        ' COMPARATION_JOB_YN, MAXIMUM_DEVIATION, COMPARISON_REJECT_YN, SH' +
        'OW_AT_PRODUCTIONSCREEN_YN)'
      'SELECT'
      
        '  MJ.PLANT_CODE, :WORKSPOT_CODE, MJ.JOB_CODE, MJ.BUSINESSUNIT_CO' +
        'DE, MJ.DESCRIPTION,'
      
        '  MJ.NORM_PROD_LEVEL, MJ.BONUS_LEVEL, MJ.NORM_OUTPUT_LEVEL, '#39#39', ' +
        'MJ.DATE_INACTIVE,'
      '  SYSDATE, SYSDATE, :MUTATOR, '#39'N'#39', MJ.SHOW_AT_SCANNING_YN,'
      '  '#39'N'#39', 0, '#39'N'#39', MJ.SHOW_AT_PRODUCTIONSCREEN_YN'
      'FROM MACHINEJOBCODE MJ'
      'WHERE MJ.PLANT_CODE = :PLANT_CODE AND'
      'MJ.MACHINE_CODE = :MACHINE_CODE AND'
      'MJ.JOB_CODE NOT IN'
      '  (SELECT J.JOB_CODE FROM JOBCODE J'
      '   WHERE J.PLANT_CODE = :PLANT_CODE AND'
      '   J.WORKSPOT_CODE = :WORKSPOT_CODE)'
      ''
      ' '
      ' ')
    Left = 56
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableHourtypePerCountry: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'COUNTRY_ID'
    MasterFields = 'COUNTRY_ID'
    MasterSource = DataSourceMaster
    TableName = 'HOURTYPEPERCOUNTRY'
    Left = 56
    Top = 112
  end
  object qryContractGroup: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CONTRACTGROUP_CODE,'
      '  DESCRIPTION'
      'FROM '
      '  CONTRACTGROUP'
      'ORDER BY'
      '  CONTRACTGROUP_CODE')
    Left = 176
    Top = 544
  end
  object qryCompareJobCodeWK: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT C.COMPARE_WORKSPOT_CODE'
      'FROM COMPAREJOB C'
      'WHERE C.PLANT_CODE = :PLANT_CODE AND'
      'C.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      'C.JOB_CODE = :JOB_CODE'
      'ORDER BY C.MUTATIONDATE DESC'
      ' ')
    Left = 280
    Top = 544
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryShiftOverlap: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT S1.SHIFT_NUMBER'
      'FROM SHIFT S1 LEFT JOIN SHIFT S2 ON'
      '  S1.PLANT_CODE = S2.PLANT_CODE AND'
      
        '  NOT (S1.STARTTIME1 > S2.STARTTIME1 AND S1.STARTTIME1 < S2.ENDT' +
        'IME1) AND'
      
        '  NOT (S1.ENDTIME1 > S2.STARTTIME1 AND S1.ENDTIME1 < S2.ENDTIME1' +
        ')'
      'WHERE S2.SHIFT_NUMBER = :SHIFT_NUMBER'
      ''
      ''
      ' ')
    Left = 384
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryStandardOccupationUpdate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'UPDATE STANDARDOCCUPATION SO3'
      'SET SO3.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE,'
      '  SO3.MUTATIONDATE = SYSDATE,'
      '  SO3.MUTATOR = '#39'ABS'#39
      'WHERE SO3.ROWID IN'
      '('
      'SELECT A.ROWID'
      'FROM'
      '('
      'SELECT SO.PLANT_CODE, SO.WORKSPOT_CODE, SO.DEPARTMENT_CODE,'
      '  SO.SHIFT_NUMBER, SO.DAY_OF_WEEK,'
      '  (SELECT SO2.DEPARTMENT_CODE'
      '   FROM STANDARDOCCUPATION SO2'
      '   WHERE SO2.PLANT_CODE = SO.PLANT_CODE AND'
      '   SO2.WORKSPOT_CODE = SO.WORKSPOT_CODE AND'
      '   SO2.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE AND'
      '   SO2.SHIFT_NUMBER = SO.SHIFT_NUMBER AND'
      '   SO2.DAY_OF_WEEK = SO.DAY_OF_WEEK) NEWDEPARTMENTCODE'
      'FROM STANDARDOCCUPATION SO'
      'WHERE SO.PLANT_CODE = :PLANT_CODE AND'
      '  SO.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  SO.DEPARTMENT_CODE = :OLD_DEPARTMENT_CODE'
      ') A'
      'WHERE A.NEWDEPARTMENTCODE IS NULL'
      ')'
      ' '
      ' '
      ' ')
    Left = 280
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'OLD_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryStandardEmployeePlanningUpdate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'UPDATE STANDARDEMPLOYEEPLANNING SP3'
      'SET SP3.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE,'
      '  SP3.MUTATIONDATE = SYSDATE,'
      '  SP3.MUTATOR = '#39'ABS'#39
      'WHERE SP3.ROWID IN'
      '('
      'SELECT A.ROWID'
      'FROM'
      '('
      'SELECT '
      '  SP.PLANT_CODE, SP.DAY_OF_WEEK, SP.SHIFT_NUMBER, '
      '  SP.DEPARTMENT_CODE, SP.WORKSPOT_CODE, SP.EMPLOYEE_NUMBER,'
      
        '    (SELECT SP2.DEPARTMENT_CODE FROM STANDARDEMPLOYEEPLANNING SP' +
        '2 '
      
        '    WHERE SP2.PLANT_CODE = SP.PLANT_CODE AND SP2.WORKSPOT_CODE =' +
        ' SP.WORKSPOT_CODE'
      '   AND SP2.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE AND '
      '   SP2.SHIFT_NUMBER = SP.SHIFT_NUMBER AND '
      '   SP2.DAY_OF_WEEK = SP.DAY_OF_WEEK AND'
      '   SP2.EMPLOYEE_NUMBER = SP.EMPLOYEE_NUMBER) NEWDEPARTMENTCODE'
      'FROM STANDARDEMPLOYEEPLANNING SP'
      'WHERE SP.PLANT_CODE = :PLANT_CODE AND'
      '  SP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  SP.DEPARTMENT_CODE = :OLD_DEPARTMENT_CODE'
      ') A'
      'WHERE A.NEWDEPARTMENTCODE IS NULL'
      ')'
      ' ')
    Left = 280
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'OLD_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeePlanningUpdate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'UPDATE EMPLOYEEPLANNING EP3'
      'SET EP3.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE,'
      '  EP3.MUTATIONDATE = SYSDATE,'
      '  EP3.MUTATOR = '#39'ABS'#39
      'WHERE EP3.ROWID IN'
      '('
      'SELECT A.ROWID'
      'FROM'
      '('
      'SELECT '
      '  EP.EMPLOYEEPLANNING_DATE,'
      '  EP.PLANT_CODE, EP.SHIFT_NUMBER, '
      '  EP.DEPARTMENT_CODE, EP.WORKSPOT_CODE, EP.EMPLOYEE_NUMBER,'
      '    (SELECT EP2.DEPARTMENT_CODE FROM EMPLOYEEPLANNING EP2 '
      
        '    WHERE EP2.EMPLOYEEPLANNING_DATE = EP.EMPLOYEEPLANNING_DATE A' +
        'ND'
      '    EP2.PLANT_CODE = EP.PLANT_CODE AND '
      '    EP2.WORKSPOT_CODE = EP.WORKSPOT_CODE AND'
      '    EP2.DEPARTMENT_CODE = :NEW_DEPARTMENT_CODE AND '
      '    EP2.SHIFT_NUMBER = EP.SHIFT_NUMBER AND '
      '    EP2.EMPLOYEE_NUMBER = EP.EMPLOYEE_NUMBER) NEWDEPARTMENTCODE'
      'FROM EMPLOYEEPLANNING EP'
      'WHERE '
      '  EP.EMPLOYEEPLANNING_DATE >= TRUNC(SYSDATE) AND'
      '  EP.PLANT_CODE = :PLANT_CODE AND'
      '  EP.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  EP.DEPARTMENT_CODE = :OLD_DEPARTMENT_CODE'
      ') A'
      'WHERE A.NEWDEPARTMENTCODE IS NULL'
      ')'
      ' ')
    Left = 280
    Top = 304
    ParamData = <
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'NEW_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'OLD_DEPARTMENT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableLinkJob: TTable
    BeforePost = TableLinkJobBeforePost
    OnNewRecord = DefaultNewRecord
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'LINKJOB'
    Left = 632
    Top = 528
  end
  object DataSourceLinkJob: TDataSource
    DataSet = TableLinkJob
    Left = 728
    Top = 528
  end
  object TableJob: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE'
    MasterFields = 'PLANT_CODE;WORKSPOT_CODE'
    MasterSource = DataSourceWorkspot
    TableName = 'JOBCODE'
    Left = 632
    Top = 480
  end
  object DataSourceJob: TDataSource
    DataSet = TableJob
    Left = 728
    Top = 480
  end
  object TableWorkspot: TTable
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;WORKSPOT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceMaster
    TableName = 'WORKSPOT'
    Left = 632
    Top = 432
    object StringField8: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField12: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object StringField13: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField14: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField15: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField16: TStringField
      FieldName = 'USE_JOBCODE_YN'
      Size = 1
    end
    object IntegerField1: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField17: TStringField
      FieldName = 'MEASURE_PRODUCTIVITY_YN'
      Size = 1
    end
    object StringField18: TStringField
      FieldName = 'PRODUCTIVE_HOUR_YN'
      Size = 1
    end
    object StringField19: TStringField
      DefaultExpression = #39'Y'#39
      FieldName = 'QUANT_PIECE_YN'
      Size = 1
    end
    object StringField20: TStringField
      FieldName = 'AUTOMATIC_DATACOL_YN'
      Size = 1
    end
    object StringField21: TStringField
      FieldName = 'COUNTER_VALUE_YN'
      Size = 1
    end
    object StringField22: TStringField
      FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
      Size = 1
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object StringField23: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourtype
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Lookup = True
    end
    object StringField24: TStringField
      FieldName = 'AUTOMATIC_RESCAN_YN'
      Size = 1
    end
    object StringField25: TStringField
      DisplayWidth = 20
      FieldName = 'SHORT_NAME'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object StringField26: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPTLU'
      LookupDataSet = QueryDeptLU
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      Size = 30
      Lookup = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'GRADE'
    end
    object StringField27: TStringField
      FieldName = 'GROUP_EFFICIENCY_YN'
      Size = 1
    end
    object StringField28: TStringField
      FieldName = 'MACHINE_CODE'
      Size = 6
    end
    object StringField29: TStringField
      FieldKind = fkLookup
      FieldName = 'MACHINELU'
      LookupDataSet = qryMachineLU
      LookupKeyFields = 'MACHINE_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'MACHINE_CODE'
      Size = 30
      Lookup = True
    end
    object StringField30: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Size = 6
    end
    object StringField31: TStringField
      FieldKind = fkLookup
      FieldName = 'CONTRACTLU'
      LookupDataSet = qryContractGroup
      LookupKeyFields = 'CONTRACTGROUP_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'CONTRACTGROUP_CODE'
      Size = 30
      Lookup = True
    end
    object StringField32: TStringField
      FieldName = 'EFF_RUNNING_HRS_YN'
      Size = 1
    end
  end
  object DataSourceWorkspot: TDataSource
    DataSet = TableWorkspot
    Left = 728
    Top = 432
  end
  object qryLinkJob: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT T.LINK_WORKSPOT_CODE, T.LINK_JOB_CODE'
      'FROM LINKJOB T'
      'WHERE T.PLANT_CODE = :PLANT_CODE'
      'AND T.WORKSPOT_CODE = :WORKSPOT_CODE'
      'AND T.JOB_CODE = :JOB_CODE'
      'ORDER BY 1,2')
    Left = 728
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryLinkJobDel: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'DELETE FROM LINKJOB T'
      'WHERE T.PLANT_CODE = :PLANT_CODE'
      'AND T.WORKSPOT_CODE = :WORKSPOT_CODE'
      'AND T.JOB_CODE = :JOB_CODE'
      ''
      ' '
      ' ')
    Left = 728
    Top = 328
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryLinkJobWS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT T.LINK_JOB_CODE'
      'FROM LINKJOB T'
      'WHERE T.PLANT_CODE = :PLANT_CODE'
      'AND T.WORKSPOT_CODE = :WORKSPOT_CODE'
      ''
      ' '
      ' ')
    Left = 728
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryResetDefaultJob: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE JOBCODE J '
      'SET J.DEFAULT_YN = '#39'N'#39
      'WHERE J.PLANT_CODE = :PLANT_CODE AND '
      'J.WORKSPOT_CODE = :WORKSPOT_CODE AND '
      'J.JOB_CODE <> :JOB_CODE AND'
      'J.DEFAULT_YN = '#39'Y'#39)
    Left = 504
    Top = 536
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryFakeEmpExists: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT'
      '  W.FAKE_EMPLOYEE_NUMBER'
      'FROM'
      '  WORKSPOT W'
      'WHERE'
      
        '  NOT (W.PLANT_CODE = :PLANT_CODE AND W.WORKSPOT_CODE = :WORKSPO' +
        'T_CODE) AND'
      '  W.FAKE_EMPLOYEE_NUMBER = :FAKE_EMPLOYEE_NUMBER '
      ' '
      ' ')
    Left = 648
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'FAKE_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryHostPortExists: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.HOST'
      'FROM WORKSPOT T'
      'WHERE not (T.PLANT_CODE = :PLANT_CODE AND'
      'T.WORKSPOT_CODE = :WORKSPOT_CODE) AND'
      'T.HOST = :HOST AND T.PORT = :PORT'
      ''
      ' ')
    Left = 648
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'HOST'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'PORT'
        ParamType = ptUnknown
      end>
  end
  object qryRoamingWorkspotExists: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.ROAMING_YN'
      'FROM WORKSPOT T'
      'WHERE NOT (T.PLANT_CODE = :PLANT_CODE AND'
      'T.WORKSPOT_CODE = :WORKSPOT_CODE) AND'
      'T.ROAMING_YN = '#39'Y'#39
      ''
      ' '
      ' '
      ' ')
    Left = 648
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
end
