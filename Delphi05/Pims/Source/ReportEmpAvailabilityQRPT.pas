(*
  Changes
    MRA:08-08-2008 REV008.
      - Use Employee's plant during select-statement, to prevent availability
        is not shown that is booked on another plant.
    MRA:22-FEB-2010. RV054.6.
    - Turn filtering off.
    MRA:19-JAN-2011 RV085.4. Bugfix.
    - This show always all absence reason codes, as
      explanation of the shown codes.
      This is wrong!
    - It must only show the absence reasons codes
      that were really shown.
    MRA:26-FEB-2014 SO-20014442
    - Adjustments to report.
    - Custom-made solution for VOS.
    - Filter on 'only SANA-employees',
    - Use Year/Month for period.
    MRA:28-MAR-2014 SO-20014442 Rework
    - It gave a wrong result for totals for illness, etc.
     - Reason: When the last week ends in next year, then it used
               the next year to determine the totals for illness, etc.
    MRA:2-JUL-2018 GLOB3-60
    - Extension 4 to 10 blocks for planning.
*)
unit ReportEmpAvailabilityQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FYearFrom, FYearTo: Integer;
    FTeamFrom, FTeamTo: String;
    FShowSelection, FAllTeam, FAllEmp: Boolean;
    FExportToFile: Boolean; // MR:17-09-2003
    FDateFrom, FDateTo: TDateTime; // 20014442
    FOnlySANAEmps: Boolean; // 20014442
  public
    procedure SetValues(YearFrom, YearTo: Integer;
      TeamFrom, TeamTo: String; ShowSelection, AllTeam, AllEmp,
      ExportToFile: Boolean;
      DateFrom, DateTo: TDateTime; // 20014442
      OnlySANAEmps: Boolean // 20014442
      );
  end;

  TStrArray = Array[1..7] of String;

  TReportEmpAvailabilityQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmplFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmplTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRGroupHDEmpl: TQRGroup;
    QRLabel47: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelYearFrom: TQRLabel;
    QRLabelYearTo: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel5: TQRLabel;
    QRGroupYear: TQRGroup;
    ChildBand1: TQRChildBand;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabelYear: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRBandDetail: TQRBand;
    QRLabelWeek: TQRLabel;
    QRLblAvail1: TQRLabel;
    QRLblShift1: TQRLabel;
    QRLblAvail2: TQRLabel;
    QRLblShift2: TQRLabel;
    QRLblAvail3: TQRLabel;
    QRLblShift3: TQRLabel;
    QRLblAvail4: TQRLabel;
    QRLblShift4: TQRLabel;
    QRLblAvail5: TQRLabel;
    QRLblShift5: TQRLabel;
    QRLblAvail6: TQRLabel;
    QRLblShift6: TQRLabel;
    QRLblAvail7: TQRLabel;
    QRLblShift7: TQRLabel;
    QRBandGrpFooterEmpl: TQRBand;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabelIllness: TQRLabel;
    QRLabelLastYear_TFT: TQRLabel;
    QRLabelLASTYEAR_WTR: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabelLastYear_Hol: TQRLabel;
    QRLabel99: TQRLabel;
    QRLblAbsRsn11: TQRLabel;
    QRLblAbsRsn6: TQRLabel;
    QRLblAbsRsn1: TQRLabel;
    QRLblAbsRsn2: TQRLabel;
    QRLblAbsRsn7: TQRLabel;
    QRLblAbsRsn12: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabelNorm_Hol: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabelEarned_WTR: TQRLabel;
    QRLabelEarned_TFT: TQRLabel;
    QRLabelUsed_TFT: TQRLabel;
    QRLabeluSED_wtr: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabelUsed_Hol: TQRLabel;
    QRLabel115: TQRLabel;
    QRLblAbsRsn13: TQRLabel;
    QRLblAbsRsn8: TQRLabel;
    QRLblAbsRsn3: TQRLabel;
    QRLblAbsRsn4: TQRLabel;
    QRLblAbsRsn9: TQRLabel;
    QRLblAbsRsn14: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabelPlanned_Hol: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabelPlanned_WTR: TQRLabel;
    QRLabelPlanned_TFT: TQRLabel;
    QRLabelAvail_TFT: TQRLabel;
    QRLabelAvail_WTR: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabelAvai_Hol: TQRLabel;
    QRLabel131: TQRLabel;
    QRLblAbsRsn15: TQRLabel;
    QRLblAbsRsn10: TQRLabel;
    QRLblAbsRsn5: TQRLabel;
    QRShape1: TQRShape;
    QRBandSummary: TQRBand;
    QRLblOnlySANAEmps: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupYearBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGrpFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupYearAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandGrpFooterEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    AbsenceReasonList: TStringList; // RV085.4.
    FDateNow: TDateTime;
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public

    FEmpl: Integer;
    FYear: Word;
    FPlannedHol, FPlannedWTR, FPlannedTFT: Integer;
    FMinDate, FMaxDate: TDateTime;
    FAvailArray, FShiftArray: TStrArray;
    FPrintChild, FEmplPrinted: Boolean;
    procedure InitializeArrayStrPerDaysOfWeek(var FStrArray: TStrArray);
(*    procedure FillStringsPerDaysOfWeek(IndexMin: Integer; FStrArray: TStrArray); *)
    procedure FillStringsPerDaysOfWeek(AName: String; FStrArray: TStrArray);
    procedure FillDescDaysPerWeek;
    procedure FillStringsAbsenceReason;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      EmployeeFrom, EmployeeTo, TeamFrom, TeamTo: String;
      const YearFrom, YearTo: Integer; const ShowSelection, AllTeam, AllEmp,
      ExportToFile: Boolean;
      DateFrom, DateTo: TDateTime; // 20014442
      OnlySANAEmps: Boolean // 20014442
      ): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportEmpAvailabilityQR: TReportEmpAvailabilityQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportEmpAvailabilityDMT, ListProcsFRM,
  CalculateTotalHoursDMT, UPimsConst, UPimsMessageRes;

procedure TQRParameters.SetValues(YearFrom, YearTo: Integer;
  TeamFrom, TeamTo: String; ShowSelection, AllTeam, AllEmp,
  ExportToFile: Boolean;
  DateFrom, DateTo: TDateTime; // 20014442
  OnlySANAEmps: Boolean // 20014442
  );
begin
  FYearFrom := YearFrom;
  FYearTo := YearTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShowSelection := ShowSelection;
  FAllTeam := AllTeam;
  FAllEmp := AllEmp;
  FExportToFile := ExportToFile;
  FDateFrom := DateFrom; // 20014442
  FDateTo := DateTo; // 20014442
  FOnlySANAEmps := OnlySANAEmps; // 20014442
end;

function TReportEmpAvailabilityQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo, TeamFrom, TeamTo: String;
  const YearFrom, YearTo: Integer; const ShowSelection, AllTeam, AllEmp,
  ExportToFile: Boolean;
  DateFrom, DateTo: TDateTime; // 20014442
  OnlySANAEmps: Boolean // 20014442
  ): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(YearFrom, YearTo, TeamFrom, TeamTo, ShowSelection,
      AllTeam, AllEmp, ExportToFile,
      DateFrom, DateTo, OnlySANAEmps // 20014442
      );
  end;
  SetDataSetQueryReport(ReportEmpAvailabilityDM.QueryEMA);
end;

function TReportEmpAvailabilityQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  MaxWeek: Integer;
begin

  Screen.Cursor := crHourGlass;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999';
  end;
  if SystemDM.IsVOS then // 20014442
  begin
    FMinDate := QRParameters.FDateFrom;
    FMaxDate := QRParameters.FDateTo;
  end
  else
  begin
    FMinDate := ListProcsF.DateFromWeek(QRParameters.FYearFrom, 1, 1);
    MaxWeek := ListProcsF.WeeksInYear(QRParameters.FYearTo);
    FMaxDate := ListProcsF.DateFromWeek(QRParameters.FYearTo, MaxWeek, 7);
  end;

  SelectStr :=
    'SELECT ' + NL +
    '  EMA.PLANT_CODE AS EMAPLANT_CODE, ' + NL +
    '  EMA.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
    '  EMA.EMPLOYEEAVAILABILITY_DATE,  ' + NL +
    '  E.DEPARTMENT_CODE, E.PLANT_CODE, ' + NL +
    '  EMA.AVAILABLE_TIMEBLOCK_1, EMA.AVAILABLE_TIMEBLOCK_2, ' + NL +
    '  EMA.AVAILABLE_TIMEBLOCK_3, EMA.AVAILABLE_TIMEBLOCK_4, ' + NL +
    '  EMA.AVAILABLE_TIMEBLOCK_5, EMA.AVAILABLE_TIMEBLOCK_6, ' + NL +
    '  EMA.AVAILABLE_TIMEBLOCK_7, EMA.AVAILABLE_TIMEBLOCK_8, ' + NL +
    '  EMA.AVAILABLE_TIMEBLOCK_9, EMA.AVAILABLE_TIMEBLOCK_10, ' + NL +
    '  EMA.SHIFT_NUMBER  ' + NL +
    'FROM ' + NL +
    '  EMPLOYEEAVAILABILITY EMA, EMPLOYEE E ' + NL +
    'WHERE ' + NL +
    '  EMA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  if SystemDM.IsVOS then // 20014442
  begin
    if QRParameters.FOnlySANAEmps then
      SelectStr := SelectStr +
        '  AND E.FREETEXT = ' + '''' + '1' + '''' + ' ' + NL
    else
      SelectStr := SelectStr +
        '  AND E.FREETEXT IS NULL ' + NL;
  end;
  if not QRParameters.FAllEmp then
    SelectStr := SelectStr +
      '  AND E.EMPLOYEE_NUMBER >= '+ QRBaseParameters.FEmployeeFrom +
      '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
  SelectStr := SelectStr +
    '  AND E.PLANT_CODE >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom)  + '''' + NL +
    '  AND E.PLANT_CODE <= '''+
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr + ' AND E.TEAM_CODE >= ''' +
      DoubleQuote(QRParameters.FTeamFrom) + '''' + ' AND E.TEAM_CODE <= '''+
      DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
  SelectStr := SelectStr  +
    '  AND EMA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART ' + NL +
    '  AND EMA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND ' + NL +
    'ORDER BY ' + NL +
    '  EMA.EMPLOYEE_NUMBER, EMA.EMPLOYEEAVAILABILITY_DATE' + NL;

  with ReportEmpAvailabilityDM do
  begin
    QueryEMA.Active := False;
    QueryEMA.UniDirectional := False;
    QueryEMA.SQL.Clear;
    QueryEMA.SQL.Add(UpperCase(SelectStr));
    QueryEMA.Params[0].Value := FMinDate;
    QueryEMA.Params[1].Value := FMaxDate;
    if not QueryEMA.Prepared then
      QueryEMA.Prepare;
    QueryEMA.Active := True;
    Result := not QueryEMA.IsEmpty;
    if Result then
    begin
      ClientDataSetSHS.Close;
      QuerySHS.ParamByName('DATEFROM').Value := FMinDate;
      QuerySHS.ParamByName('DATETO').Value := FMaxDate;
      // MR:28-04-2005 We only have to call these procedure once.
//      FillStringsAbsenceReason; // RV085.4.
      FillDescDaysPerWeek;
    end;
  end;
  FYear := 0;
 //CAR : Update progress indicator
  if Result then
  begin
    ProgressIndicatorPos := 0;
    Report_RecordCount := ReportEmpAvailabilityDM.QueryEMA.RecordCount;
  end;

  FDateNow := Trunc(Now);
  Screen.Cursor := crDefault;
end;

procedure TReportEmpAvailabilityQR.ConfigReport;
var
  Year, Month, Day: Word;
begin
  // MR:17-09-2003
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  if QRParameters.FAllEmp then
  begin
    QRLabelEmplFrom.Caption := '*';
    QRLabelEmplTo.Caption := '*';
  end
  else
  begin
    QRLabelEmplFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLabelEmplTo.Caption := QRBaseParameters.FEmployeeTo;
  end;
  QRLabelYearFrom.Caption := IntToStr(QRParameters.FYearFrom);
  QRLabelYearTo.Caption := IntToStr(QRParameters.FYearTo);
  QRGroupHDEmpl.ForceNewPage := True;
  if SystemDM.IsVOS then // 20014442
  begin
    QRLabel2.Caption := SPimsYearMonth;
    DecodeDate(QRParameters.FDateFrom, Year, Month, Day);
    QRLabelYearFrom.Caption := Format('%d/%d', [Year, Month]);
    DecodeDate(QRParameters.FDateTo, Year, Month, Day);
    QRLabelYearTo.Caption := Format('%d/%d', [Year, Month]);
    if QRParameters.FOnlySANAEmps then
       QRLblOnlySANAEmps.Caption := SPimsOnlySANAEmps
    else
       QRLblOnlySANAEmps.Caption := '';
  end
  else
    QRLblOnlySANAEmps.Caption := '';
  // GLOB3-60 Hide some labels to make more space
  QRLabel21.Caption := '';
  QRLabel23.Caption := '';
  QRLabel24.Caption := '';
  QRLabel25.Caption := '';
  // GLOB3-60 Shift -> Only show first letter:
  QRLblShift1.Caption := Copy(QRLblShift1.Caption, 1,1);
  QRLblShift2.Caption := Copy(QRLblShift2.Caption, 1,1);
  QRLblShift3.Caption := Copy(QRLblShift3.Caption, 1,1);
  QRLblShift4.Caption := Copy(QRLblShift4.Caption, 1,1);
  QRLblShift5.Caption := Copy(QRLblShift5.Caption, 1,1);
  QRLblShift6.Caption := Copy(QRLblShift6.Caption, 1,1);
  QRLblShift7.Caption := Copy(QRLblShift7.Caption, 1,1);
end;

procedure TReportEmpAvailabilityQR.FreeMemory;
begin
  inherited;
  AbsenceReasonList.Free; // RV085.4.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportEmpAvailabilityQR.FormCreate(Sender: TObject);
var
  TBCaption: String;
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  AbsenceReasonList := TStringList.Create; // RV085.4.
  // MR:17-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  // GLOB3-60
  // Some texts must be changed here
  if SystemDM.MaxTimeblocks = 10 then
    TBCaption := '12345678910'
  else
    TBCaption := '1234';
  QRLabel27.Caption := TBCaption;
  QRLabel29.Caption := TBCaption;
  QRLabel32.Caption := TBCaption;
  QRLabel37.Caption := TBCaption;
  QRLabel41.Caption := TBCaption;
  QRLabel43.Caption := TBCaption;
  QRLabel48.Caption := TBCaption;
end;

procedure TReportEmpAvailabilityQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  FYear := 0;
  // MR:17-09-2003
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Show Selections
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel1.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' + QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelTeamFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' + QRLabelTeamTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLabelEmplFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLabelEmplTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLabelYearFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelYearTo.Caption);
      if SystemDM.IsVOS then // 20014442
        ExportClass.AddText(QRLblOnlySANAEmps.Caption);
    end;
  end;
end;

procedure TReportEmpAvailabilityQR.InitializeArrayStrPerDaysOfWeek
 (var FStrArray: TStrArray);
var
  Counter: Integer;
begin
  for Counter := 1 to 7 do
    FStrArray[Counter] := '';
end;

// MR:28-04-2005 Replaced by other function that looks on
// component-name instead of 'tag'.
(*
procedure TReportEmpAvailabilityQR.FillStringsPerDaysOfWeek(IndexMin: Integer;
  FStrArray: TStrArray);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
        (TempComponent as TQRLabel).Caption := FStrArray[Index - IndexMin + 1];
    end;
  end;
end;
*)

// RV085.4.
procedure TReportEmpAvailabilityQR.FillStringsAbsenceReason;
var
  Index: Integer;
  CmpName, AbsenceReason, Code, Description: String;
  TempComponent: TComponent;
  procedure DetermineCodeDescription;
  var
    IPos: Integer;
  begin
    IPos := Pos(';', AbsenceReason);
    if IPos > 0 then
    begin
      Code := Copy(AbsenceReason, 1, IPos-1);
      Description := Copy(AbsenceReason, IPos+1, Length(AbsenceReason));
    end;
  end;
begin
  if AbsenceReasonList.Count > 0 then
    AbsenceReasonList.Sort;
  Index := 0;
  while (Index < AbsenceReasonList.Count) and (Index < 15) do
  begin
    AbsenceReason := AbsenceReasonList[Index];
    DetermineCodeDescription;
    CmpName := Format('QRLblAbsRsn%d', [Index + 1]);
    TempComponent := FindComponent(CmpName);
    if (TempComponent <> nil) then
      (TempComponent as TQRLabel).Caption := Code + ' = ' + Description;
    inc(Index);
  end;
  while (Index < 15) do
  begin
    CmpName := Format('QRLblAbsRsn%d', [Index + 1]);
    TempComponent := FindComponent(CmpName);
    if (TempComponent <> nil) then
      (TempComponent as TQRLabel).Caption := '';
    inc(Index);
  end;
end;

(*
procedure TReportEmpAvailabilityQR.FillStringsAbsenceReason;
var
  Index: Integer;
  CmpName: String;
  TempComponent: TComponent;
begin
  // MR:28-04-2005 Because the wrong components were taken,
  // we don't use the 'tag' to identity the component.
  ReportEmpAvailabilityDM.ClientDataSetAbsRsn.First;
  for Index := 1 to 15 do
  begin
    CmpName := Format('QRLblAbsRsn%d', [Index]);
    TempComponent := FindComponent(CmpName);
    if (TempComponent <> nil) then
    begin
      // MR:28-04-2005 Order 550397, also show the 'absencereason_code'.
      if (not ReportEmpAvailabilityDM.ClientDataSetAbsRsn.Eof) then
        (TempComponent as TQRLabel).Caption :=
          ReportEmpAvailabilityDM.ClientDataSetAbsRsn.
            FieldByName('ABSENCEREASON_CODE').AsString + ' = ' +
          ReportEmpAvailabilityDM.ClientDataSetAbsRsn.
            FieldByName('DESCRIPTION').AsString
      else
        (TempComponent as TQRLabel).Caption := '';
    end;
    if (not ReportEmpAvailabilityDM.ClientDataSetAbsRsn.Eof) then
      ReportEmpAvailabilityDM.ClientDataSetAbsRsn.Next;
  end;
end;
*)

procedure TReportEmpAvailabilityQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 30) and (Index <= 36) then
        (TempComponent as TQRLabel).Caption :=
           SystemDM.GetDayWCode(Index - 30 + 1);
    end;
  end;
end;

procedure TReportEmpAvailabilityQR.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FPrintChild;
  // MR:28-04-2005 We call this once, at the start of the report.
//  FillDescDaysPerWeek;
end;

procedure TReportEmpAvailabilityQR.QRGroupYearBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Year, Week: Word;
begin
  inherited;
  if not FEmplPrinted then
    QRGroupYear.ForceNewPage := True
  else
    QRGroupYear.ForceNewPage := False;
  FEmplPrinted := False;
  ListProcsF.WeekUitDat(ReportEMPAvailabilityDM.QueryEMA.
    FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime, Year, Week);
  if (Year <> FYear) then
    PrintBand := True
  else
    PrintBand := False;
  FYear := Year;  
  FPrintChild := PrintBand;
  if PrintBand then
  begin
    FPlannedHol := 0;
    FPlannedWTR := 0;
    FPlannedTFT := 0;
  end;
  QRLabelYear.Caption := IntToStr(Year);
end;

procedure TReportEmpAvailabilityQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FEmplPrinted := True;
  FEmpl :=
    ReportEMPAvailabilityDM.QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FPlannedHol := 0;
  FPlannedWTR := 0;
  FPlannedTFT := 0;
  //CAR 17-7-2003
  ReportEmpAvailabilityDM.ClientDataSetSHS.Close;
  ReportEmpAvailabilityDM.QuerySHS.ParamByName('EMPLOYEE').Value := FEmpl;
  ReportEmpAvailabilityDM.ClientDataSetSHS.Open;
  AbsenceReasonList.Clear; // RV085.4.
end;

//CAR 17-7-2003 performance changes
procedure TReportEmpAvailabilityQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  DateEMA: TDateTime;
  Day, CurrentYear, Year, Week, CurrentWeek: Word;
  Shift, SaveShift: Integer;
  procedure AddAbsenceReasonList(AAbsenceReasonCode, ADescription: String);
  var
    Key: String;
  begin
    Key := AAbsenceReasonCode + ';' + ADescription;
    if AbsenceReasonList.IndexOf(Key) = -1 then // does not exists
      AbsenceReasonList.Add(Key);
  end;
  procedure AbsenceReasonListAction;
  var
    AbsenceReasonCode: String;
    Index: Integer;
  begin
    for Index := 1 to SystemDM.MaxTimeblocks do
    begin
      AbsenceReasonCode :=
        ReportEmpAvailabilityDM.QueryEMA.
          FieldByName(Format('AVAILABLE_TIMEBLOCK_%d', [Index])).AsString;
      if ReportEmpAvailabilityDM.ClientDataSetAbsRsn.
        Locate('ABSENCEREASON_CODE', AbsenceReasonCode, []) then
        AddAbsenceReasonList(AbsenceReasonCode,
          ReportEmpAvailabilityDM.ClientDataSetAbsRsn.
            FieldByName('DESCRIPTION').AsString);
    end;
  end;
begin
  inherited;

  with ReportEMPAvailabilityDM do
  begin
    InitializeArrayStrPerDaysOfWeek(FAvailArray);
    InitializeArrayStrPerDaysOfWeek(FShiftArray);
    DateEMA := QueryEMA.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
    Day := ListProcsF.DayWStartOnFromDate(DateEMA);
    ListProcsF.WeekUitDat(DateEMA, Year, CurrentWeek);
    Week := CurrentWeek; CurrentYear := Year;

    SaveShift := -1;
    while not QueryEMA.Eof and (CurrentWeek = Week) and (CurrentYear = Year) and
      (QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsInteger = FEmpl) do
    begin
      FAvailArray[Day] := GetAvail(FEmpl, DateEMA);
      FShiftArray[Day] := GetShift(FEmpl, DateEMA);
      Shift := QueryEMA.FieldByName('SHIFT_NUMBER').AsInteger;
      //CAR 550276
      if (DateEMA >= FDateNow) and (Shift >= 0)  then
       GetPlannedMin(DateEMA, FEmpl, Day, Shift, SaveShift,
          FPlannedHol, FPlannedWTR, FPlannedTFT);
      SaveShift := Shift;
      AbsenceReasonListAction; // RV085.4.
      QueryEMA.Next;
      //Car: Update progress indicator
      ProgressIndicatorPos := ProgressIndicatorPos + 1;

      DateEMA := QueryEMA.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
      Day := ListProcsF.DayWStartOnFromDate(DateEMA);
      ListProcsF.WeekUitDat(DateEMA, Year, Week);
    end;
    if not QueryEMA.Eof then
      QueryEMA.Prior;
    // MR:28-04-2005 Functions replaced by two separate functions.
    FillStringsPerDaysOfWeek('Avail', FAvailArray);
    FillStringsPerDaysOfWeek('Shift', FShiftArray);
(*
    FillStringsPerDaysOfWeek(10, FAvailArray);
    FillStringsPerDaysOfWeek(20, FShiftArray);
*)
    QRLabelWeek.Caption := IntToStr(CurrentWeek);
  end;
  // CAR : Update progress indicator
  UpdateProgressIndicator;
end;

procedure TReportEmpAvailabilityQR.QRBandGrpFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  LastYear_Hol, Norm_Hol, Used_Hol, LastYear_WTR, Earned_WTR, Used_WTR,
  LastYear_TFT, Earned_TFT, Used_TFT, Ill_Min: Integer;
begin
  inherited;
  // 20014442 Related to this order
  // For FYear: Do not use FYear here, because FYear
  // is calculated based on last data read from EmployeeAvailability, and
  // that can be a wrong year when the last week ends in the next year.
  // NOTE: It can only calculate totals for 1 year (based on balance-table)
  //       so when multiple years are involved, then only use
  //       the last year (Year-To).
  ReportEMPAvailabilityDM.GetAbsenceTotal(QRParameters.FYearTo {FYear}, // 20014442
    FEmpl, LastYear_Hol, Norm_Hol,
    Used_Hol,  LastYear_WTR, Earned_WTR, Used_WTR,
    LastYear_TFT, Earned_TFT, Used_TFT,  Ill_Min);
  QRLabelLastYear_Hol.Caption := DecodeHrsMin(LastYear_Hol);
  QRLabelLastYear_WTR.Caption := DecodeHrsMin(LastYear_WTR);
  QRLabelLastYear_TFT.Caption := DecodeHrsMin(LastYear_TFT);
  QRLabelIllness.Caption := DecodeHrsMin(Ill_Min);

  QRLabelNorm_Hol.Caption := DecodeHrsMin(Norm_Hol);
  QRLabelUsed_Hol.Caption := DecodeHrsMin(Used_Hol);
  QRLabelPlanned_hol.Caption := DecodeHrsMin(FPlannedHol);
  QRLabelAvai_Hol.Caption :=
    DecodeHrsMin(LastYear_Hol + Norm_Hol - Used_Hol - FPlannedHol);

  QRLabelEarned_WTR.Caption := DecodeHrsMin(Earned_WTR);
  QRLabelUsed_WTR.Caption := DecodeHrsMin(Used_WTR);
  QRLabelPlanned_WTR.Caption := DecodeHrsMin(FPlannedWTR);
  QRLabelAvail_WTR.Caption :=
    DecodeHrsMin(LastYear_WTR + Earned_WTR - Used_WTR - FPlannedWTR);

  QRLabelLastYear_TFT.Caption := DecodeHrsMin(LastYear_TFT);
  QRLabelEarned_TFT.Caption := DecodeHrsMin(Earned_TFT);
  QRLabelUsed_TFT.Caption := DecodeHrsMin(Used_TFT);
  QRLabelPlanned_TFT.Caption := DecodeHrsMin(FPlannedTFT);
  QRLabelAvail_TFT.Caption :=
    DecodeHrsMin(LastYear_TFT + Earned_TFT - Used_TFT - FPlannedTFT);
  FYear := 0;
  // MR:28-04-2005 We only have to call this procedure once,
  // during start of the report.
  // RV085.4.
  FillStringsAbsenceReason;
end;

procedure TReportEmpAvailabilityQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  // MR:17-09-2003
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportEmpAvailabilityQR.QRGroupHDEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel5.Caption + ' ' +
      ReportEmpAvailabilityDM.QueryEMA.FieldByName('EMPLOYEE_NUMBER').AsString +
      ' ' +
      ReportEmpAvailabilityDM.QueryEMA.FieldByName('DESCRIPTION').AsString);
  end;
end;

procedure TReportEmpAvailabilityQR.QRGroupYearAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel7.Caption + ' ' + QRLabelYear.Caption);
  end;
end;

procedure TReportEmpAvailabilityQR.ChildBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-09-2003
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Columns header
    ExportClass.AddText(
      QRLabel19.Caption + ' ' + QRLabel21.Caption + ExportClass.Sep + // 1
      QRLabel26.Caption + ExportClass.Sep + ExportClass.Sep + // 2 + 3
      QRLabel30.Caption + ExportClass.Sep + ExportClass.Sep + // 4 + 5
      QRLabel33.Caption + ExportClass.Sep + ExportClass.Sep + // 6 + 7
      QRLabel38.Caption + ExportClass.Sep + ExportClass.Sep + // 8 + 9
      QRLabel40.Caption + ExportClass.Sep + ExportClass.Sep + // 10 + 11
      QRLabel44.Caption + ExportClass.Sep + ExportClass.Sep + // 12 + 13
      QRLabel46.Caption + ExportClass.Sep + ExportClass.Sep   // 14 + 15
      );
    ExportClass.AddText(
      QRLabel23.Caption + ExportClass.Sep + // 1
      QRLabel27.Caption + ExportClass.Sep + // 2
      QRLabel28.Caption + ExportClass.Sep + // 3
      QRLabel29.Caption + ExportClass.Sep + // 4
      QRLabel31.Caption + ExportClass.Sep + // 5
      QRLabel32.Caption + ExportClass.Sep + // 6
      QRLabel34.Caption + ExportClass.Sep + // 7
      QRLabel37.Caption + ExportClass.Sep + // 8
      QRLabel39.Caption + ExportClass.Sep + // 9
      QRLabel41.Caption + ExportClass.Sep + // 10
      QRLabel42.Caption + ExportClass.Sep + // 11
      QRLabel43.Caption + ExportClass.Sep + // 12
      QRLabel45.Caption + ExportClass.Sep + // 13
      QRLabel48.Caption + ExportClass.Sep + // 14
      QRLabel51.Caption + ExportClass.Sep   // 15
      );
  end;
end;

procedure TReportEmpAvailabilityQR.QRBandDetailAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-09-2003
  if QRParameters.FExportToFile then
  begin
    // Detail
    ExportClass.AddText(
      QRLabelWeek.Caption + ExportClass.Sep + // 1
      QRLblAvail1.Caption + ExportClass.Sep + // 2
      QRLblShift1.Caption + ExportClass.Sep + // 3
      QRLblAvail2.Caption + ExportClass.Sep + // 4
      QRLblShift2.Caption + ExportClass.Sep + // 5
      QRLblAvail3.Caption + ExportClass.Sep + // 6
      QRLblShift3.Caption + ExportClass.Sep + // 7
      QRLblAvail4.Caption + ExportClass.Sep + // 8
      QRLblShift4.Caption + ExportClass.Sep + // 9
      QRLblAvail5.Caption + ExportClass.Sep + // 10
      QRLblShift5.Caption + ExportClass.Sep + // 11
      QRLblAvail6.Caption + ExportClass.Sep + // 12
      QRLblShift6.Caption + ExportClass.Sep + // 13
      QRLblAvail7.Caption + ExportClass.Sep + // 14
      QRLblShift7.Caption + ExportClass.Sep   // 15
      );
  end;
end;

procedure TReportEmpAvailabilityQR.QRBandGrpFooterEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  // MR:17-09-2003
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Explanation of codes and hours
    ExportClass.AddText(QRLabel83.Caption);
    // MR:28-04-2005 QRLabel-names are changed for 'AbsenceReason'.
    ExportClass.AddText(QRLabel84.Caption + ExportClass.Sep +
      QRLblAbsRsn1.Caption + ExportClass.Sep +
      QRLblAbsRsn2.Caption + ExportClass.Sep +
      QRLblAbsRsn3.Caption + ExportClass.Sep +
      QRLblAbsRsn4.Caption + ExportClass.Sep +
      QRLblAbsRsn5.Caption);
    ExportClass.AddText(QRLabel85.Caption + ExportClass.Sep +
      QRLblAbsRsn6.Caption + ExportClass.Sep +
      QRLblAbsRsn7.Caption + ExportClass.Sep +
      QRLblAbsRsn8.Caption + ExportClass.Sep +
      QRLblAbsRsn9.Caption + ExportClass.Sep +
      QRLblAbsRsn10.Caption);
    ExportClass.AddText(QRLabel86.Caption + ExportClass.Sep +
      QRLblAbsRsn11.Caption + ExportClass.Sep +
      QRLblAbsRsn12.Caption + ExportClass.Sep +
      QRLblAbsRsn13.Caption + ExportClass.Sep +
      QRLblAbsRsn14.Caption + ExportClass.Sep +
      QRLblAbsRsn15.Caption);
    ExportClass.AddText('' + ExportClass.Sep +
      QRLabel99.Caption + ExportClass.Sep +
      QRLabel106.Caption + ExportClass.Sep +
      QRLabel115.Caption + ExportClass.Sep +
      QRLabel122.Caption + ExportClass.Sep +
      QRLabel131.Caption);
    ExportClass.AddText(QRLabel89.Caption + ExportClass.Sep +
      QRLabelLastYear_Hol.Caption + ExportClass.Sep +
      QRLabelNorm_Hol.Caption + ExportClass.Sep +
      QRLabelUsed_Hol.Caption + ExportClass.Sep +
      QRLabelPlanned_Hol.Caption + ExportClass.Sep +
      QRLabelAvai_Hol.Caption);
    ExportClass.AddText('' + ExportClass.Sep +
      QRLabel97.Caption + ExportClass.Sep +
      QRLabel108.Caption + ExportClass.Sep +
      QRLabel113.Caption + ExportClass.Sep +
      QRLabel124.Caption + ExportClass.Sep +
      QRLabel129.Caption);
    ExportClass.AddText(QRLabel91.Caption + ExportClass.Sep +
      QRLabelLastYear_WTR.Caption + ExportClass.Sep +
      QRLabelEarned_WTR.Caption + ExportClass.Sep +
      QRLabelUsed_WTR.Caption + ExportClass.Sep +
      QRLabelPlanned_WTR.Caption + ExportClass.Sep +
      QRLabelAvail_WTR.Caption);
    ExportClass.AddText(QRLabel92.Caption + ExportClass.Sep +
      QRLabelLastYear_TFT.Caption + ExportClass.Sep +
      QRLabelEarned_TFT.Caption + ExportClass.Sep +
      QRLabelUsed_TFT.Caption + ExportClass.Sep +
      QRLabelPlanned_TFT.Caption + ExportClass.Sep +
      QRLabelAvail_TFT.Caption);
    ExportClass.AddText(QRLabel93.Caption + ExportClass.Sep +
      QRLabelIllness.Caption + ExportClass.Sep +
      '' + ExportClass.Sep +
      '' + ExportClass.Sep +
      '' + ExportClass.Sep +
      '');
  end;
end;

// MR:28-04-2005 Separate function that searches on comp-name instead
// of Tag.
procedure TReportEmpAvailabilityQR.FillStringsPerDaysOfWeek(AName: String;
  FStrArray: TStrArray);
var
  TempComponent: TComponent;
  Index: Integer;
  CmpName: String;
begin
  for Index := 1 to 7 do
  begin
    CmpName := Format('QRLbl%s%d', [AName, Index]);
    TempComponent := FindComponent(CmpName);
    if (TempComponent <> nil) then
      (TempComponent as TQRLabel).Caption := FStrArray[Index];
  end;
end;

procedure TReportEmpAvailabilityQR.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // Disable filter here.
  ReportEmpAvailabilityDM.QueryEMA.Filtered := False;
  ReportEmpAvailabilityDM.QueryEMA.Active := False;
end;

end.
