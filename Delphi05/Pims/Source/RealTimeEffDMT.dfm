object RealTimeEffDM: TRealTimeEffDM
  OldCreateOrder = False
  Left = 275
  Top = 124
  Height = 675
  Width = 1305
  object qryMergeWSEffPerMinuteXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'MERGE INTO WORKSPOTEFFPERMINUTE W '
      'USING DUAL '
      'ON (W.PLANT_CODE = :PLANT_CODE AND '
      '  W.SHIFT_DATE = :SHIFT_DATE AND '
      '  W.SHIFT_NUMBER = :SHIFT_NUMBER AND '
      '  W.WORKSPOT_CODE = :WORKSPOT_CODE AND '
      '  W.JOB_CODE = :JOB_CODE AND '
      '  W.INTERVALSTARTTIME = :INTERVALSTARTTIME) '
      'WHEN NOT MATCHED THEN '
      'INSERT VALUES ( '
      'SEQ_WORKSPOTEFFPERMINUTE.NEXTVAL, '
      ':SHIFT_DATE, '
      ':SHIFT_NUMBER, '
      ':PLANT_CODE, '
      ':WORKSPOT_CODE, '
      ':JOB_CODE, '
      ':INTERVALSTARTTIME, '
      ':INTERVALENDTIME, '
      ':WORKSPOTSECONDSACTUAL, '
      ':WORKSPOTSECONDSNORM, '
      ':WORKSPOTQUANTITY, '
      ':NORMLEVEL, '
      'SYSDATE, '
      'SYSDATE, '
      ':MUTATOR, '
      ':START_TIMESTAMP, '
      ':END_TIMESTAMP) '
      'WHEN MATCHED THEN '
      '  UPDATE SET '
      
        '    W.WORKSPOTQUANTITY = W.WORKSPOTQUANTITY + :WORKSPOTQUANTITY,' +
        ' '
      '    W.WORKSPOTSECONDSNORM = '
      '      CASE WHEN :NORMLEVEL <> 0 '
      
        '        THEN (W.WORKSPOTQUANTITY + :WORKSPOTQUANTITY) * 3600 / :' +
        'NORMLEVEL ELSE 0 END, '
      '    W.MUTATIONDATE = SYSDATE, '
      '    W.MUTATOR = :MUTATOR ')
    Left = 440
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALSTARTTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALSTARTTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALENDTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTSECONDSACTUAL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTSECONDSNORM'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_TIMESTAMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'END_TIMESTAMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryWSEffPerMinSelect: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT W.*'
      'FROM WORKSPOTEFFPERMINUTE W'
      'WHERE'
      ' W.PLANT_CODE = :PLANT_CODE AND'
      ' W.SHIFT_DATE = :SHIFT_DATE AND'
      ' W.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      ' W.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      ' W.JOB_CODE = :JOB_CODE AND'
      ' W.INTERVALSTARTTIME = :INTERVALSTARTTIME'
      '')
    Left = 80
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALSTARTTIME'
        ParamType = ptUnknown
      end>
  end
  object qryWSEffPerMinInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WORKSPOTEFFPERMINUTE'
      '('
      'WORKSPOTEFFPERMINUTE_ID,'
      'SHIFT_DATE,'
      'SHIFT_NUMBER,'
      'PLANT_CODE,'
      'WORKSPOT_CODE,'
      'JOB_CODE,'
      'INTERVALSTARTTIME,'
      'INTERVALENDTIME,'
      'WORKSPOTSECONDSACTUAL,'
      'WORKSPOTSECONDSNORM,'
      'WORKSPOTQUANTITY,'
      'NORMLEVEL,'
      'CREATIONDATE,'
      'MUTATIONDATE,'
      'MUTATOR,'
      'START_TIMESTAMP,'
      'END_TIMESTAMP'
      ')'
      'VALUES'
      '('
      'SEQ_WORKSPOTEFFPERMINUTE.NEXTVAL,'
      ':SHIFT_DATE,'
      ':SHIFT_NUMBER,'
      ':PLANT_CODE,'
      ':WORKSPOT_CODE,'
      ':JOB_CODE,'
      ':INTERVALSTARTTIME,'
      ':INTERVALENDTIME,'
      ':WORKSPOTSECONDSACTUAL,'
      ':WORKSPOTSECONDSNORM,'
      ':WORKSPOTQUANTITY,'
      ':NORMLEVEL,'
      'SYSDATE,'
      'SYSDATE,'
      ':MUTATOR,'
      ':START_TIMESTAMP,'
      ':END_TIMESTAMP'
      ')'
      '')
    Left = 80
    Top = 152
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALSTARTTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALENDTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTSECONDSACTUAL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTSECONDSNORM'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'START_TIMESTAMP'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'END_TIMESTAMP'
        ParamType = ptUnknown
      end>
  end
  object qryWSEffPerMinUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE WORKSPOTEFFPERMINUTE W'
      'SET'
      '  W.WORKSPOTQUANTITY = :WORKSPOTQUANTITY,'
      '  W.WORKSPOTSECONDSNORM ='
      '    CASE WHEN :NORMLEVEL <> 0'
      '      THEN (:WORKSPOTQUANTITY) * 3600 / :NORMLEVEL ELSE 0 END,'
      '  W.MUTATIONDATE = SYSDATE,'
      '  W.MUTATOR = :MUTATOR'
      'WHERE'
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.SHIFT_DATE = :SHIFT_DATE AND'
      '  W.SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  W.WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  W.JOB_CODE = :JOB_CODE AND'
      '  W.INTERVALSTARTTIME = :INTERVALSTARTTIME'
      ''
      ' ')
    Left = 80
    Top = 208
    ParamData = <
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'WORKSPOTQUANTITY'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORMLEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'INTERVALSTARTTIME'
        ParamType = ptUnknown
      end>
  end
  object qryCopyThis: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 80
    Top = 16
  end
  object qryCalcEff: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'begin'
      '  -- Call the procedure'
      '  calc_efficiency(:PLANT_CODE);'
      'end;')
    Left = 80
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryRecalcEff: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'begin'
      '  -- Call the procedure'
      '  recalc_efficiency(:PLANT_CODE,'
      '                    :SHIFT_DATE,'
      '                    :SHIFT_NUMBER,'
      '                    :WORKSPOT_CODE,'
      '                    :JOB_CODE);'
      'end;'
      '')
    Left = 168
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'SHIFT_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
end
