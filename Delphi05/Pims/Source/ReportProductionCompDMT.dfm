inherited ReportProductionCompDM: TReportProductionCompDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 581
  Width = 741
  object QueryProductionComp: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProductionCompFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  PQ.JOB_CODE,'
      '  PQ.QUANTITY AS PIECES'
      'FROM '
      '  PRODUCTIONQUANTITY PQ,'
      '  PLANT P,'
      '  JOBCODE J,'
      '  WORKSPOT W,'
      '  BUSINESSUNIT B,'
      '  DEPARTMENT D,'
      '  PRODHOURPEREMPLOYEE PE'
      'WHERE'
      '  PQ.QUANTITY <> 0 AND'
      '  PQ.PLANT_CODE >= :PLANTFROM AND'
      '  PQ.PLANT_CODE <= :PLANTTO AND'
      '  B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  B.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND'
      '  PQ.WORKSPOT_CODE <= :WORKSPOTTO AND'
      '  PQ.START_DATE >= :DATEFROM AND'
      '  PQ.START_DATE <= :DATETO AND'
      '  PQ.PLANT_CODE = J.PLANT_CODE AND'
      '  PQ.PLANT_CODE = P.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = J.JOB_CODE AND'
      '  J.PLANT_CODE = B.PLANT_CODE AND'
      '  J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE AND'
      '  PQ.PLANT_CODE = W.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  J.BUSINESSUNIT_CODE = D.BUSINESSUNIT_CODE AND'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  PQ.PLANT_CODE = PE.PLANT_CODE AND'
      '  PQ.SHIFT_NUMBER = PE.SHIFT_NUMBER AND'
      '  PQ.WORKSPOT_CODE = PE.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = PE.JOB_CODE'
      'ORDER BY'
      '  P.PLANT_CODE, P.DESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProduction: TDataSource
    DataSet = QueryProductionComp
    Left = 184
    Top = 24
  end
  object QueryPQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE, '
      '  SUM(QUANTITY) AS QUANTITY '
      'FROM '
      '  PRODUCTIONQUANTITY '
      'WHERE '
      '  QUANTITY <> 0 AND  '
      '  PLANT_CODE >= :PLANTFROM AND  PLANT_CODE <= :PLANTTO AND '
      '  (END_DATE > :FSTARTDATE AND START_DATE < :FENDDATE)'
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE ')
    Left = 192
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryCheck: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 64
    Top = 224
  end
  object QueryCompJob: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  C.PLANT_CODE, C.WORKSPOT_CODE, C.JOB_CODE,'
      '  C.COMPARE_WORKSPOT_CODE, W.DESCRIPTION AS WDESC,'
      '  C.COMPARE_JOB_CODE, J.DESCRIPTION AS JDESC'
      'FROM '
      '  COMPAREJOB C , WORKSPOT W, JOBCODE J '
      'WHERE'
      '  C.PLANT_CODE = W.PLANT_CODE AND'
      '  C.COMPARE_WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  C.PLANT_CODE = J.PLANT_CODE AND'
      '  C.COMPARE_WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  C.COMPARE_JOB_CODE = J.JOB_CODE   '
      'ORDER BY '
      '  C.PLANT_CODE, C.WORKSPOT_CODE, C.JOB_CODE')
    Left = 72
    Top = 96
  end
end
