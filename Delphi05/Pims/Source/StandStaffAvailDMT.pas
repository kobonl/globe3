(*
  Changes:
    MR:22-01-2008 Order 550461, Oracle 10, RV002. RECNO-Float/Integer error.
    MR:24-01-2008 Order 550461, Oracle 10, RV003. Fix query QueryDetail.
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    SO: O1-JUL-2010 RV067.1. 550490
    - added QueryAbs for the right click menu in std staff availability
  MRA:25-APR-2013 TD-22429 Employee Active in Future
  - Remove compare with STARTDATE, to prevent it does NOT show employees
    who will be active in future.
  MRA:9-FEB-2018 PIM-354
  - Memory leak in planning dialogs
  - The OnDestroy-event was not assigned!
  MRA:18-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)

unit StandStaffAvailDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, Dblup1a, StandStaffAvailFRM, Provider,
  DBClient, UPimsConst;

type
  TArrayTB = Array[1..MAX_TBS] of Integer;

  TStandStaffAvailDM = class(TGridBaseDM)
    TableDetailPLANT_CODE: TStringField;
    TableDetailDAY_OF_WEEK: TIntegerField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailAVAILABLE_TIMEBLOCK_1: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_2: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_3: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_4: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    QueryDetail: TQuery;
    QueryDetailD11: TStringField;
    QueryDetailD21: TStringField;
    QueryDetailD31: TStringField;
    QueryDetailD4: TStringField;
    QueryDetailD51: TStringField;
    QueryDetailD61: TStringField;
    QueryDetailD71: TStringField;
    QueryDetailEMPLOYEE_NUMBER: TIntegerField;
    QueryDetailDESCRIPTION: TStringField;
    QueryDetailPLANT: TStringField;
    QueryDetailSHIFT: TIntegerField;
    QueryDetailD12: TStringField;
    QueryDetailD13: TStringField;
    QueryDetailD14: TStringField;
    QueryDetailD22: TStringField;
    QueryDetailD23: TStringField;
    QueryDetailD24: TStringField;
    QueryDetailD32: TStringField;
    QueryDetailD33: TStringField;
    QueryDetailD34: TStringField;
    QueryDetailD42: TStringField;
    QueryDetailD43: TStringField;
    QueryDetailD44: TStringField;
    QueryDetailD52: TStringField;
    QueryDetailD53: TStringField;
    QueryDetailD54: TStringField;
    QueryDetailD62: TStringField;
    QueryDetailD63: TStringField;
    QueryDetailD64: TStringField;
    QueryDetailD72: TStringField;
    QueryDetailD73: TStringField;
    QueryDetailD74: TStringField;
    QueryDetailTotalHrs: TStringField;
    QueryDetailTEAM: TStringField;
    QueryDetailDEPARTMENT: TStringField;
    QueryEmpl: TQuery;
    QueryDetailRECNO: TFloatField;
    QueryDetailD11CALC: TStringField;
    QueryDetailD12CALC: TStringField;
    QueryDetailD13CALC: TStringField;
    QueryDetailD14CALC: TStringField;
    QueryDetailD15CALC: TStringField;
    QueryDetailD16CALC: TStringField;
    QueryDetailD17CALC: TStringField;
    QueryDetailD18CALC: TStringField;
    QueryDetailD19CALC: TStringField;
    QueryDetailD110CALC: TStringField;
    QueryDetailD21CALC: TStringField;
    QueryDetailD22CALC: TStringField;
    QueryDetailD23CALC: TStringField;
    QueryDetailD24CALC: TStringField;
    QueryDetailD25CALC: TStringField;
    QueryDetailD26CALC: TStringField;
    QueryDetailD27CALC: TStringField;
    QueryDetailD28CALC: TStringField;
    QueryDetailD29CALC: TStringField;
    QueryDetailD210CALC: TStringField;
    QueryDetailD31CALC: TStringField;
    QueryDetailD32CALC: TStringField;
    QueryDetailD33CALC: TStringField;
    QueryDetailD34CALC: TStringField;
    QueryDetailD35CALC: TStringField;
    QueryDetailD36CALC: TStringField;
    QueryDetailD37CALC: TStringField;
    QueryDetailD38CALC: TStringField;
    QueryDetailD39CALC: TStringField;
    QueryDetailD310CALC: TStringField;
    QueryDetailD41CALC: TStringField;
    QueryDetailD42CALC: TStringField;
    QueryDetailD43CALC: TStringField;
    QueryDetailD44CALC: TStringField;
    QueryDetailD45CALC: TStringField;
    QueryDetailD46CALC: TStringField;
    QueryDetailD47CALC: TStringField;
    QueryDetailD48CALC: TStringField;
    QueryDetailD49CALC: TStringField;
    QueryDetailD410CALC: TStringField;
    QueryDetailD51CALC: TStringField;
    QueryDetailD52CALC: TStringField;
    QueryDetailD53CALC: TStringField;
    QueryDetailD54CALC: TStringField;
    QueryDetailD55CALC: TStringField;
    QueryDetailD56CALC: TStringField;
    QueryDetailD57CALC: TStringField;
    QueryDetailD58CALC: TStringField;
    QueryDetailD59CALC: TStringField;
    QueryDetailD510CALC: TStringField;
    QueryDetailD61CALC: TStringField;
    QueryDetailD62CALC: TStringField;
    QueryDetailD63CALC: TStringField;
    QueryDetailD64CALC: TStringField;
    QueryDetailD65CALC: TStringField;
    QueryDetailD66CALC: TStringField;
    QueryDetailD67CALC: TStringField;
    QueryDetailD68CALC: TStringField;
    QueryDetailD69CALC: TStringField;
    QueryDetailD610CALC: TStringField;
    QueryDetailD71CALC: TStringField;
    QueryDetailD72CALC: TStringField;
    QueryDetailD73CALC: TStringField;
    QueryDetailD74CALC: TStringField;
    QueryDetailD75CALC: TStringField;
    QueryDetailD76CALC: TStringField;
    QueryDetailD77CALC: TStringField;
    QueryDetailD78CALC: TStringField;
    QueryDetailD79CALC: TStringField;
    QueryDetailD710CALC: TStringField;
    QueryDetailEMP: TFloatField;
    QueryExec: TQuery;
    QuerySelect: TQuery;
    ClientDataSetTotalHours: TClientDataSet;
    ClientDataSetTotalHoursEMPLOYEE_NUMBER: TIntegerField;
    ClientDataSetTotalHoursPLANT_CODE: TStringField;
    ClientDataSetTotalHoursDEPARTMENT_CODE: TStringField;
    ClientDataSetTotalHoursSHIFT_NUMBER: TIntegerField;
    ClientDataSetTotalHoursTOTALHRS: TIntegerField;
    ClientDataSetSave: TClientDataSet;
    ClientDataSetSaveRECNO: TFloatField;
    ClientDataSetSaveDAY1: TStringField;
    ClientDataSetSaveDAY2: TStringField;
    ClientDataSetSaveDAY3: TStringField;
    ClientDataSetSaveDAY4: TStringField;
    ClientDataSetSaveDAY5: TStringField;
    ClientDataSetSaveDAY6: TStringField;
    ClientDataSetSaveDAY7: TStringField;
    ClientDataSetTotalHoursTB1: TIntegerField;
    ClientDataSetTotalHoursTB2: TIntegerField;
    ClientDataSetTotalHoursTB3: TIntegerField;
    ClientDataSetTotalHoursTB4: TIntegerField;
    QueryUpdateSTA: TQuery;
    QueryInsertSTA: TQuery;
    QuerySTAOldValues: TQuery;
    QueryPlant: TQuery;
    QueryTeam: TQuery;
    QueryPlantTeam: TQuery;
    QueryShiftPlant: TQuery;
    DataSourceEmpl: TDataSource;
    QueryDetailCONTRACTHOURS: TFloatField;
    QueryAbs: TQuery;
    QueryDetailD15: TStringField;
    QueryDetailD16: TStringField;
    QueryDetailD17: TStringField;
    QueryDetailD18: TStringField;
    QueryDetailD19: TStringField;
    QueryDetailD110: TStringField;
    QueryDetailD25: TStringField;
    QueryDetailD26: TStringField;
    QueryDetailD27: TStringField;
    QueryDetailD28: TStringField;
    QueryDetailD29: TStringField;
    QueryDetailD210: TStringField;
    QueryDetailD35: TStringField;
    QueryDetailD36: TStringField;
    QueryDetailD37: TStringField;
    QueryDetailD38: TStringField;
    QueryDetailD39: TStringField;
    QueryDetailD310: TStringField;
    QueryDetailD45: TStringField;
    QueryDetailD46: TStringField;
    QueryDetailD47: TStringField;
    QueryDetailD48: TStringField;
    QueryDetailD49: TStringField;
    QueryDetailD410: TStringField;
    QueryDetailD55: TStringField;
    QueryDetailD56: TStringField;
    QueryDetailD57: TStringField;
    QueryDetailD58: TStringField;
    QueryDetailD59: TStringField;
    QueryDetailD510: TStringField;
    QueryDetailD65: TStringField;
    QueryDetailD66: TStringField;
    QueryDetailD67: TStringField;
    QueryDetailD68: TStringField;
    QueryDetailD69: TStringField;
    QueryDetailD610: TStringField;
    QueryDetailD75: TStringField;
    QueryDetailD76: TStringField;
    QueryDetailD77: TStringField;
    QueryDetailD78: TStringField;
    QueryDetailD79: TStringField;
    QueryDetailD710: TStringField;
    ClientDataSetTotalHoursTB5: TIntegerField;
    ClientDataSetTotalHoursTB6: TIntegerField;
    ClientDataSetTotalHoursTB7: TIntegerField;
    ClientDataSetTotalHoursTB8: TIntegerField;
    ClientDataSetTotalHoursTB9: TIntegerField;
    ClientDataSetTotalHoursTB10: TIntegerField;
    TableDetailAVAILABLE_TIMEBLOCK_5: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_6: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_7: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_8: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_9: TStringField;
    TableDetailAVAILABLE_TIMEBLOCK_10: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
    FExistShiftZero: Boolean;
    FOldSTADay: Array[1..7,1..MAX_TBS] of String;
    procedure GetOldValues(Plant: String; Employee, Shift: Integer);
    procedure CalculateTotalHours(DataSet: TDataSet;  var TotalHrs: Integer);

    function CheckEmployeePlanningFuture(Plant: String; Empl, Shift: Integer;
      UpdateTable: Boolean; IndexDayMin, IndexDayMax: Integer): Boolean;
    function CheckIfExistPlannings(Plant: String; Empl, Shift: Integer;
      UpdateTable: Boolean; IndexDayMin, IndexDayMax: Integer): Boolean;
  public
    { Public declarations }

    FTBValid: TArrayTB;
    FSTADAY: Array[1..7, 1..MAX_TBS] of String;
    FCurrentDate: TDateTime;
    procedure FillQueryDetail(AllTeam, AllPlant, AllShift,
      AllEmployees: Boolean;
      TeamFrom, TeamTo, Plant, Shift: String);
    procedure GetValidTB(Employee, Shift: Integer; Plant, Department: String);
    procedure UpdateValues(TempTable: TTable);
    procedure UpdateStandardAvail(Plant, Dept: String;
      Empl, Shift: Integer; IndexDayMin, IndexDayMax: Integer ;
      var UpdateTable: Boolean);

    procedure GotoCurrentRecord(RecNo: Double);
    procedure DeleteCalculateHours(Plant, Dept: String; Empl, Shift: Integer);
//CAR Changes for performance 3-10-2003 :550244
    procedure DefaultAfterScroll(DataSet: TDataSet);
    procedure QueryDetailCalcFieldsOnRefresh(DataSet: TDataSet);
    procedure QueryDetailCalcFieldsAfterSave(DataSet: TDataSet);
  end;

var
  StandStaffAvailDM: TStandStaffAvailDM;

implementation

{$R *.DFM}

uses
  SystemDMT, CalculateTotalHoursDMT, UPimsMessageRes,
  ListProcsFRM;

procedure TStandStaffAvailDM.FillQueryDetail(AllTeam, AllPlant,
  AllShift, AllEmployees: Boolean;
  TeamFrom, TeamTo, Plant, Shift: String);
begin
  ClientDataSetSave.EmptyDataSet;
  QueryDetail.Active := False;
  if AllTeam then
  begin
    QueryDetail.ParamByName('TEAMFROM').asString := '*';
    QueryDetail.ParamByName('TEAMTO').asString := '*';
  end
  else
  begin
    QueryDetail.ParamByName('TEAMFROM').asString := TeamFrom;
    QueryDetail.ParamByName('TEAMTO').asString := TeamTo;
  end;
  if AllPlant then
    QueryDetail.ParamByName('PLANT_CODE').AsString := '*'
  else
    QueryDetail.ParamByName('PLANT_CODE').AsString := Plant;
  if AllShift then
    QueryDetail.ParamByName('SHIFT_NUMBER').AsInteger := -1
  else
    QueryDetail.ParamByName('SHIFT_NUMBER').AsInteger := StrToInt(Shift);
  if AllEmployees then // show also not available employees
    QueryDetail.ParamByName('ALL_EMP').AsInteger := 1
  else
    QueryDetail.ParamByName('ALL_EMP').AsInteger := 0;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryDetail.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryDetail.ParamByName('USER_NAME').AsString := '*';
  QueryDetail.ParamByName('SDATE').asDateTime := GetDate(FCurrentDate);
  if not QueryDetail.Prepared then
    QueryDetail.Prepare;
  QueryDetail.Active := True;
end;

procedure TStandStaffAvailDM.GotoCurrentRecord(RecNo: Double);
begin
  QueryDetail.First;
  QueryDetail.Locate('RECNO', Recno, []);
end;

procedure TStandStaffAvailDM.QueryDetailCalcFieldsOnRefresh(
  DataSet: TDataSet);
var
  TotalHrs: Integer;
  Day, TBlock: Integer;
begin
  inherited;
  // for export of big employee numbers
  DataSet.FieldByNAME('FLOAT_EMP').AsFloat :=
    DataSet.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
// define calculated fields they are all equal with fields of query
  for Day:= 1 to 7 do
    for TBlock := 1 to SystemDM.MaxTimeblocks do
      QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)+'CALC').AsString :=
        QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)).AsString;

//end restore calc field with last changed value
  CalculateTotalHours(DataSet, TotalHrs);
  DataSet.FieldByName('TOTALHRS').AsString :=
    CalculateTotalHoursDM.DecodeHrsMin(TotalHrs);
end; // QueryDetailCalcFieldsOnRefresh

procedure TStandStaffAvailDM.QueryDetailCalcFieldsAfterSave(DataSet: TDataSet);
var
  TotalHrs: Integer;
  Day, TBlock: Integer;
begin
  inherited;
  DataSet.FieldByName('FLOAT_EMP').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
// recalculate field if was changed - and not refresh the whole query
  if ClientDataSetSave.FindKey([
    QueryDetail.FieldByName('RECNO').AsFloat]) then
  begin
    for Day := 1 to 7 do
    begin
      for TBlock := 1 to SystemDM.MaxTimeblocks do
      begin
        QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)+'CALC').Value :=
          ClientDataSetSave.FieldByName('DAY'+IntToStr(Day)).AsString[TBlock];

        if ClientDataSetSave.FieldByName('DAY'+IntToStr(Day)).AsString[TBlock] = '!' then
          QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)+'CALC').Value := ''
      end;
    end;
  end
  else
    for Day := 1 to 7 do
      for TBlock := 1 to SystemDM.MaxTimeblocks do
        QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)+'CALC').AsString :=
          QueryDetail.FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)).AsString;
//end restore calc field with last changed value
  CalculateTotalHours(DataSet, TotalHrs);
  DataSet.FieldByName('TOTALHRS').AsString :=
    CalculateTotalHoursDM.DecodeHrsMin(TotalHrs);
end; // QueryDetailCalcFieldsAfterSave

//CAR : 10-3-2003 : 550244
procedure TStandStaffAvailDM.CalculateTotalHours(DataSet: TDataSet;
  var TotalHrs: Integer);
var
  Empl, Shift, IndexTB, IndexDay: Integer;
  Plant, Dept: String;
begin
  Empl := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Shift := DataSet.FieldByName('SHIFT').AsInteger;
  Plant := DataSet.FieldByName('PLANT').AsString;
  Dept := DataSet.FieldByName('DEPARTMENT').AsString;
  TotalHrs := 0;
  if ClientDataSetTotalHours.FindKey([Plant, Empl, Dept, Shift]) then
  begin
    TotalHrs := ClientDataSetTotalHours.FieldByName('TOTALHRS').AsInteger;
    Exit;
  end;
  ClientDataSetTotalHours.Insert;
  ClientDataSetTotalHours.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
  ClientDataSetTotalHours.FieldByName('PLANT_CODE').AsString := Plant;
  ClientDataSetTotalHours.FieldByName('DEPARTMENT_CODE').AsString := Dept;
  ClientDataSetTotalHours.FieldByName('SHIFT_NUMBER').AsInteger := Shift;

  ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept,
    StandStaffAvailF.CheckBoxPaidYN.Checked);

  for IndexTB := 1 to SystemDM.MaxTimeblocks do
  begin
    ClientDataSetTotalHours.FieldByName('TB' + IntToStr(IndexTB)).AsInteger :=
       ATBLengthClass.GetValidTB(IndexTB);
    for IndexDay := 1 to 7 do
      if (DataSet.FieldByName('D' + IntToStr(IndexDay) + IntToStr(IndexTB)).
        AsString = DEFAULTAVAILABILITYCODE) then
          TotalHrs := TotalHrs + ATBLengthClass.GetLengthTB(IndexTB, IndexDay);
  end;
  ClientDataSetTotalHours.FieldByName('TOTALHRS').AsInteger := TotalHrs;
  ClientDataSetTotalHours.Post;
end;

// Car 550244: replace store procedure used before
procedure TStandStaffAvailDM.GetValidTB(Employee, Shift: Integer;
  Plant, Department: String);
var
  IndexTB: Integer;
begin
  if ClientDataSetTotalHours.FindKey([Plant, Employee, Department, Shift]) then
  begin
    for IndexTB:= 1 to SystemDM.MaxTimeblocks do
      FTBValid[IndexTB] := ClientDataSetTotalHours.
        FieldByName('TB' + IntToStr(IndexTB)).AsInteger;
  end;
end;

procedure TStandStaffAvailDM.DefaultAfterScroll(DataSet: TDataSet);
begin
  inherited;
  GetValidTB(
    QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    QueryDetail.FieldByName('SHIFT').AsInteger,
    QueryDetail.FieldByName('PLANT').AsString,
    QueryDetail.FieldByName('DEPARTMENT').AsString);
  StandStaffAvailF.FillEditDetail(True);
end;


procedure TStandStaffAvailDM.UpdateValues(TempTable: TTable);
begin
  TempTable.FieldByName('CREATIONDATE').Value := FCurrentDate;
  TempTable.FieldByName('MUTATIONDATE').Value := FCurrentDate;
  TempTable.FieldByName('MUTATOR').Value      := SystemDM.CurrentProgramUser;
end;

procedure TStandStaffAvailDM.DeleteCalculateHours(Plant, Dept: String;
 Empl, Shift: Integer);
begin
  if ClientDataSetTotalHours.FindKey([Plant, Empl, Dept, Shift]) then
    ClientDataSetTotalHours.Delete;
end;

procedure TStandStaffAvailDM.GetOldValues(Plant: String; Employee, Shift: Integer);
var
  IndexDay, IndexTB: Integer;
begin
  for IndexDay := 1 to 7 do
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      FOldSTADay[IndexDay,IndexTB] := '';
 
  QuerySTAOldValues.Active := False;
  QuerySTAOldValues.ParamByName('PLANT').AsString := Plant;
  QuerySTAOldValues.ParamByName('SHIFT').AsInteger := Shift;
  QuerySTAOldValues.ParamByName('EMPLOYEE').AsInteger := Employee;
  QuerySTAOldValues.Active := True;
  while not QuerySTAOldValues.Eof do
  begin
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      FOldSTADay[QuerySTAOldValues.FieldByName('DAY_OF_WEEK').AsInteger, IndexTB] :=
        QuerySTAOldValues.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(IndexTB)).AsString;
    QuerySTAOldValues.Next;
  end;
  QuerySTAOldValues.Active := False;
end;

function TStandStaffAvailDM.CheckIfExistPlannings(Plant: String;
  Empl, Shift: Integer; UpdateTable: Boolean;
  IndexDayMin, IndexDayMax: Integer): Boolean;
var
  SelectStr, Temp, TempUpdate: String;
  IndexDay, IndexTB: Integer;
begin
  Result := False;
  if Shift = 0 then
    Exit;

  for IndexDay := IndexDayMin to IndexDayMax do
  begin
    Temp := '';
    TempUpdate := '';
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
    begin
      if ( (FOldSTADay[IndexDay, IndexTB] <> '') and
        (FOldSTADay[IndexDay, IndexTB] <>  FSTADAY[IndexDay, IndexTB]) and
        (FSTADAY[IndexDay, IndexTB] <> DEFAULTAVAILABILITYCODE)) then
      begin
        Temp := Temp +  'SCHEDULED_TIMEBLOCK_' + IntToStr(IndexTB) +
          ' IN (''A'', ''B'', ''C'') OR ';
        if UpdateTable then
          TempUpdate := TempUpdate +
            Format('SCHEDULED_TIMEBLOCK_%d = ''N'',',[IndexTB]);
      end;
    end;
    if Temp <> '' then
    begin
     // check if exist stdplanning for one timeblock
      Temp := Copy(Temp, 1, length(Temp)-3);
      SelectStr := 
        'SELECT ' +
        '  COUNT(*) AS RECNO ' +
        'FROM ' +
        '  STANDARDEMPLOYEEPLANNING ' +
        'WHERE ' + '(' + Temp + ') ' +
        '  AND DAY_OF_WEEK = ' + IntToStr(IndexDay) +
        '  AND SHIFT_NUMBER = ' + IntToStr(Shift) +
        '  AND EMPLOYEE_NUMBER = ' + IntToStr(Empl) +
        '  AND PLANT_CODE = ''' + DoubleQuote(Plant) + '''';
      QuerySelect.Active := False;
      QuerySelect.SQL.Clear;
      QuerySelect.SQL.Add(Selectstr);
      QuerySelect.Active := True;
      if Round(QuerySelect.FieldByName('RECNO').AsFloat) > 0 then
      begin
        if UpdateTable and (Length(TempUpdate) > 0) then
        begin
          TempUpdate := Copy(TempUpdate,1,length(TempUpdate)-1);
          SelectStr := 
            ' UPDATE STANDARDEMPLOYEEPLANNING  SET ' +
            TempUpdate + ', MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
            ' WHERE PLANT_CODE = ' + '''' + DoubleQuote(Plant) + '''' +
            ' AND DAY_OF_WEEK = ' + IntToStr(IndexDay) +
            ' AND SHIFT_NUMBER = ' + IntToStr(Shift) +
            ' AND EMPLOYEE_NUMBER = ' + IntToStr(Empl);
          QueryExec.Active := False;
          QueryExec.SQL.Clear;
          QueryExec.SQL.Add(Selectstr);
          QueryExec.Prepare;
          QueryExec.ParamByName('MUTATOR').AsString :=
            SystemDM.CurrentProgramUser;
          QueryExec.ParamByName('MUTATIONDATE').AsDateTime := FCurrentDate;
          QueryExec.ExecSQL;
         end;
         if not UpdateTable then
         begin
           Result := true;
           QuerySelect.Close;
           Exit;
         end;
       end; // if there are STD records
     end;  // if changes from '*' -> '-'
  end; // for days
end;

function TStandStaffAvailDM.CheckEmployeePlanningFuture(Plant: String;
  Empl, Shift: Integer;  UpdateTable: Boolean;
  IndexDayMin, IndexDayMax: Integer): Boolean;
var
  SelectStr, Temp, TempUpdate: String;
  IndexDay, IndexTB: Integer;
begin
  Result:= False;
  if Shift = 0 then
    Exit;
  for IndexDay := IndexDayMin to IndexDayMax do
  begin
    Temp := '';
    TempUpdate := '';
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
    begin
      if ( (FOldSTADay[IndexDay, IndexTB] <> '') and
        (FOldSTADay[IndexDay, IndexTB] <>  FSTADAY[IndexDay, IndexTB]) and
        (FSTADAY[IndexDay, IndexTB] <> DEFAULTAVAILABILITYCODE)) then
      begin
       Temp := Temp +  'P.SCHEDULED_TIMEBLOCK_' + IntToStr(IndexTB) +
         ' IN (''A'', ''B'', ''C'') OR ';
        if UpdateTable then
          TempUpdate := TempUpdate +
            Format('P.SCHEDULED_TIMEBLOCK_%d = ''N'',',[IndexTB]);
      end;
    end;

    if Temp <> '' then
    begin
     // check if exist employeeplanning exist in the future for one TB
      Temp := Copy(Temp, 1, length(Temp)-3);
      SelectStr :=
        'SELECT ' + NL +
        '  P.EMPLOYEEPLANNING_DATE ' + NL +
        'FROM ' + NL +
        '  EMPLOYEEPLANNING P, SHIFTSCHEDULE S ' + NL +
        'WHERE ' + NL +
        '  P.EMPLOYEE_NUMBER = :EMPLOYEE AND ' + NL +
        '  P.SHIFT_NUMBER = :SHIFT AND ' + NL +
        '  S.SHIFT_NUMBER = 0 AND S.EMPLOYEE_NUMBER = :EMPLOYEE AND ' + NL +
        '  P.EMPLOYEEPLANNING_DATE = S.SHIFT_SCHEDULE_DATE AND ' + NL +
        '  P.EMPLOYEEPLANNING_DATE > :FDATE AND (' + Temp  + ')';
      QuerySelect.Active := False;
      QuerySelect.SQL.Clear;
      QuerySelect.SQL.Add(Selectstr);
      QuerySelect.Prepare;
      QuerySelect.ParamByName('FDATE').AsDateTime := FCurrentDate;
      QuerySelect.ParamByName('EMPLOYEE').AsInteger := Empl;
      QuerySelect.ParamByName('SHIFT').AsInteger := Shift;
      QuerySelect.Open;
      if UpdateTable and (TempUpdate <> '' )then
        TempUpdate := Copy(TempUpdate,1,length(TempUpdate)-1);
      while Not QuerySelect.Eof do
      begin
        if IndexDay = ListProcsF.DayWStartOnFromDate(QuerySelect.
          FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime) then
        begin
          if UpdateTable and (TempUpdate <> '' )then
          begin

            SelectStr := 
              ' UPDATE EMPLOYEEPLANNING  SET ' +
              TempUpdate + ', MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
              ' WHERE PLANT_CODE = ' + '''' + DoubleQuote(Plant) + '''' +
              ' AND EMPLOYEEPLANNING_DATE = :EDATE'  +
              ' AND SHIFT_NUMBER = ' + IntToStr(Shift) +
              ' AND EMPLOYEE_NUMBER = ' + IntToStr(Empl);
            QueryExec.Active := False;
            QueryExec.SQL.Clear;
            QueryExec.SQL.Add(Selectstr);
            QueryExec.Prepare;
            QueryExec.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
            QueryExec.ParamByName('MUTATIONDATE').AsDateTime := FCurrentDate;
            QueryExec.ParamByName('EDATE').AsDateTime :=
              QuerySelect.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
            QueryExec.ExecSQL;
          end;
          if Not UpdateTable then
          begin
            Result := True;
            QuerySelect.Close;
            Exit;
          end;
        end;
        QuerySelect.Next;
      end;
    end;
  end;
end;

//CAR: 550244 Add queries for Update and Insert
procedure TStandStaffAvailDM.UpdateStandardAvail(Plant, Dept: String;
  Empl, Shift: Integer; IndexDayMin, IndexDayMax: Integer;
  var UpdateTable: Boolean);
var
  IndexDay: Integer;
  CheckSTDPlanning, CheckEMPPlanning: Boolean;
  procedure PrepareParameters(QueryWork: TQuery; InsertValue: Boolean;
    Plant: String; Empl, Shift : Integer);
  begin
    QueryWork.ParamByName('PLANT_CODE').AsString := Plant;
    QueryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    QueryWork.ParamByName('SHIFT_NUMBER').AsInteger := Shift;
    QueryWork.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    QueryWork.ParamByName('MUTATIONDATE').AsDateTime := FCurrentDate;
    if InsertValue then
      QueryWork.ParamByName('CREATIONDATE').AsDateTime := FCurrentDate;
  end;

  procedure PrepareSTAParameters(QueryWork: TQuery; Day: Integer);
  begin
    QueryWork.ParamByName('DAY_OF_WEEK').AsInteger := Day;
    QueryWork.ParamByName('AV1').AsString := FSTADAY[Day, 1];
    QueryWork.ParamByName('AV2').AsString := FSTADAY[Day, 2];
    QueryWork.ParamByName('AV3').AsString := FSTADAY[Day, 3];
    QueryWork.ParamByName('AV4').AsString := FSTADAY[Day, 4];
    QueryWork.ParamByName('AV5').AsString := FSTADAY[Day, 5];
    QueryWork.ParamByName('AV6').AsString := FSTADAY[Day, 6];
    QueryWork.ParamByName('AV7').AsString := FSTADAY[Day, 7];
    QueryWork.ParamByName('AV8').AsString := FSTADAY[Day, 8];
    QueryWork.ParamByName('AV9').AsString := FSTADAY[Day, 9];
    QueryWork.ParamByName('AV10').AsString := FSTADAY[Day, 10];
    QueryWork.Prepare;
    QueryWork.ExecSQL;
  end;

  procedure UpdateTableStandardAvail(Plant: String; Empl, Shift, Day: Integer);
  var
    UpdateTable: Boolean;
    IndexTB: Integer;
  begin
    UpdateTable:= False;
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      if FOldSTADay[Day, IndexTB] <> '' then
      begin
        UpdateTable := True;
        Break;
      end;
    if UpdateTable then
    begin
{      if (FOldSTADay[Day,1] = FSTADAY[Day,1]) and
        (FOldSTADay[Day,2] = FSTADAY[Day,2]) and
        (FOldSTADay[Day,3] = FSTADAY[Day,3]) and
        (FOldSTADay[Day,4] = FSTADAY[Day,4]) then
        Exit; }
      UpdateTable := False;
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
        if (FOldSTADay[Day,IndexTB] <> FSTADAY[Day,IndexTB]) then
          UpdateTable := True;
      if not UpdateTable then
        Exit;
      PrepareSTAParameters(QueryUpdateSTA, Day);
    end
    else
      PrepareSTAParameters(QueryInsertSTA, Day);
  end;
begin
  inherited;
  if FTBValid[1] = 0 then
    Exit;

  UpdateTable := False;

  CheckSTDPlanning := False;
  CheckEMPPlanning := False;

  // fill FOLDSTADAY - with values not saved yet - old existing values
  GetOldValues(Plant, Empl, Shift);

  if CheckIfExistPlannings(Plant, Empl, Shift, False,
    IndexDayMin, IndexDayMax) then
  begin
    if DisplayMessage(SStdStaffAvailPlanned,
      mtConfirmation,[mbYes, mbNo]) = mrNo then
      Exit;
    CheckSTDPlanning := True;
  end;
  if FExistShiftZero then
    if CheckEmployeePlanningFuture(Plant, Empl, Shift, False,
      IndexDayMin, IndexDayMax) then
    begin
      if DisplayMessage(SStdStaffAvailFuturePlanned,
        mtConfirmation,[mbYes, mbNo]) = mrNo then
        Exit;
      CheckEMPPlanning := True;
    end;
    // update stand planning table
  if CheckSTDPlanning then
    CheckIfExistPlannings(Plant, Empl, Shift, True, IndexDayMin, IndexDayMax);
  // update empl planning table
  if CheckEMPPlanning  then
    CheckEmployeePlanningFuture(Plant, Empl, Shift, True,
      IndexDayMin, IndexDayMax);
 //CAR 550244 - set parameters for queries
  PrepareParameters(QueryUpdateSTA, False, Plant, Empl, Shift);
  PrepareParameters(QueryInsertSTA, True, Plant, Empl, Shift);
  for IndexDay := IndexDayMin to IndexDayMax do
    UpdateTableStandardAvail(Plant, Empl, Shift, IndexDay);

  DeleteCalculateHours(Plant, Dept, Empl, Shift);
  UpdateTable := True;
end;

procedure TStandStaffAvailDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := StandStaffAvailDM.DataModuleDestroy;
  //CAR: 10-3-2003: 550244
  QueryExec.Active := False;
  QueryExec.SQL.Clear;
  QueryExec.SQL.Add(
    'SELECT ' +
    '  SHIFT_NUMBER ' +
    'FROM ' +
    '  SHIFT ' +
    'WHERE ' +
    '  SHIFT_NUMBER = 0');
  QueryExec.Active := True;
  FExistShiftZero := not QueryExec.IsEmpty ;

  ClientDataSetTotalHours.CreateDataSet;
  ClientDataSetSave.CreateDataSet;

  QueryDetail.Prepare;
  QueryUpdateSTA.Prepare;
  QueryInsertSTA.Prepare;
  QuerySTAOldValues.Prepare;
// get current date once when form is open
  FCurrentDate := Now;
// fill fixed valued
//not now  QueryDetail.ParamByName('MUT').AsString     := SystemDM.CurrentProgramUser;
//not now  QueryDetail.ParamByName('SDATE').AsDateTime := GetDate(FCurrentDate);
  //Car 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryTeam.Sql.Clear;
    QueryTeam.Sql.Add(
      'SELECT ' +
      '  T.TEAM_CODE, T.DESCRIPTION ' +
      'FROM ' +
      '  TEAM T, TEAMPERUSER U ' +
      'WHERE ' +
      '  U.USER_NAME = :USER_NAME AND ' +
      '  T.TEAM_CODE = U.TEAM_CODE ' +
      'ORDER BY ' +
      '  T.TEAM_CODE');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryTeam.Prepare;
//Car 2-4-2004
    QueryEmpl.SQL.Clear;
    QueryEmpl.Sql.Add(
      'SELECT ' +
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER, '+
      '  E.EMPLOYEE_NUMBER || '' | '' || E.DESCRIPTION AS VALUEDISPLAY, ' +
      '  E.DESCRIPTION, E.SHORT_NAME, E.DEPARTMENT_CODE, ' +
      '  E.TEAM_CODE, E.ADDRESS ' +
      'FROM ' +
      '  EMPLOYEE E, TEAMPERUSER T ' +
      'WHERE '+
      '  T.USER_NAME = :USER_NAME AND '+
      '  T.TEAM_CODE = E.TEAM_CODE ' +
      'ORDER BY ' +
      '  E.EMPLOYEE_NUMBER ');
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end;
  QueryEmpl.Prepare;
  QueryEmpl.Open;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlant);
  QueryPlant.Open;
end;

procedure TStandStaffAvailDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  // CAR : 550244
  ClientDataSetSave.EmptyDataSet;
  ClientDataSetTotalHours.EmptyDataSet;
  QueryDetail.Close;
  QueryDetail.UnPrepare;
  // used in Dialog of copy
  QueryEmpl.Close;
  QueryEmpl.Unprepare;
  //used for update standard availability
  QueryUpdateSTA.Close;
  QueryUpdateSTA.Unprepare;
  QueryInsertSTA.Close;
  QueryInsertSTA.Unprepare;
  QuerySTAOldValues.Close;
  QuerySTAOldValues.UnPrepare;
  QueryTeam.Close;
  QueryTeam.UnPrepare;
end;

// RV050.8.
procedure TStandStaffAvailDM.QueryPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
