(*
  Changes:
  JVL:14-APR-2011. RV089. SO-550532
  - Memorize selected plant
  MRA:27-APR-2011. RV089. SO-55032. Re-work
  - When there is a memorized plant, then jump to
    first business-unit-record that has this plant set.
*)  

unit BusinessUnitFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, Mask, dxDBTLCl, dxGrClms,
  dxEditor, dxExEdtr, dxDBEdtr, dxDBELib;

type
  TBusinessUnitF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    DBEditBusinessUnit: TDBEdit;
    DBEditName: TDBEdit;
    DBEditBonusFactor: TDBEdit;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridLookupColumn;
    dxDBLookupEdit1: TdxDBLookupEdit;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label14: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEditDirH: TDBEdit;
    DBEditIndH: TDBEdit;
    DBEditTotH: TDBEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


function BusinessUnitF: TBusinessUnitF;

implementation

{$R *.DFM}

uses
  BusinessUnitDMT, SystemDMT;

var
  BusinessUnitF_HND: TBusinessUnitF;

function BusinessUnitF: TBusinessUnitF;
begin
  if (BusinessUnitF_HND = nil) then
    BusinessUnitF_HND := TBusinessUnitF.Create(Application);
  Result := BusinessUnitF_HND;
end;


procedure TBusinessUnitF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditBusinessUnit.SetFocus;
end;

procedure TBusinessUnitF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditBusinessUnit.SetFocus;
end;

procedure TBusinessUnitF.FormCreate(Sender: TObject);
begin
  BusinessUnitDM := CreateFormDM(TBusinessUnitDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := BusinessUnitDM.DataSourceDetail;
  inherited;
end;

procedure TBusinessUnitF.FormDestroy(Sender: TObject);
begin
  inherited;
  BusinessUnitF_HND := nil;
end;

procedure TBusinessUnitF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;

  SystemDM.ASaveTimeRecScanning.Plant :=
    BusinessUnitDM.TableDetail.FieldByName('PLANT_CODE').AsString;
end;

procedure TBusinessUnitF.FormShow(Sender: TObject);
begin
  inherited;
  // RV089.
  if SystemDM.ASaveTimeRecScanning.Plant = '' then
    SystemDM.ASaveTimeRecScanning.Plant :=
      BusinessUnitDM.TableDetail.FieldByName('PLANT_CODE').AsString
  else
    BusinessUnitDM.TableDetail.Locate('PLANT_CODE',
      SystemDM.ASaveTimeRecScanning.Plant, []);
end;

end.
