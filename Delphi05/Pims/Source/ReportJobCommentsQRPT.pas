(*
  MRA:24-JUL-2015 PIM-50
  - New Report Job Comments
*)
unit ReportJobCommentsQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls, SystemDMT, ReportJobCommentsDMT, UPimsConst;

type
  TQRParameters = class
  private
    FWorkspotFrom, FWorkspotTo: String;
    FDateFrom, FDateTo: TDateTime;
    FShowSelection: Boolean;
    FExportToFile: Boolean;
  public
    procedure SetValues(WorkspotFrom, WorkspotTo: String;
      DateFrom, DateTo: TDateTime;
      ShowSelection: Boolean;
      ExportToFile: Boolean);
  end;

type
  TReportJobCommentsQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLblToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblFromWorkspot: TQRLabel;
    QRLblWorkspot: TQRLabel;
    QRLblWorkspotFrom: TQRLabel;
    QRLblToWorkspot: TQRLabel;
    QRLblWorkspotTo: TQRLabel;
    QRLblFromDate: TQRLabel;
    QRLblDate: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLblToDate: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRGroupHdPlant: TQRGroup;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRDBText4: TQRDBText;
    QRGroupHeader: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRBandDetails: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRGroupHdWorkspot: TQRGroup;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel7: TQRLabel;
    QRBandSummary: TQRBand;
    procedure FormCreate(Sender: TObject);
    procedure QRBandDetailsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const WorkspotFrom, WorkspotTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowSelection: Boolean;
      const ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportJobCommentsQR: TReportJobCommentsQR;

implementation

{$R *.DFM}

{ TReportJobCommentsQR }

{ TQRParameters }

procedure ExportDetail(
  WorkspotCode, WorkspotDesc, JobCode, JobDesc: String;
  Comment, EmployeeNumber, EmployeeName: String;
  TimeStamp: String);
begin
  ExportClass.AddText(
    WorkspotCode + ExportClass.Sep +
    WorkspotDesc + ExportClass.Sep +
    JobCode + ExportClass.Sep +
    JobDesc + ExportClass.Sep +
    Comment + ExportClass.Sep +
    EmployeeNumber + ExportClass.Sep +
    EmployeeName + ExportClass.Sep +
    TimeStamp
    );
end;

procedure TQRParameters.SetValues(WorkspotFrom, WorkspotTo: String;
  DateFrom, DateTo: TDateTime;
  ShowSelection, ExportToFile: Boolean);
begin
  FWorkspotFrom := WorkspotFrom;
  FWorkspotTo := WorkspotTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
end;

procedure TReportJobCommentsQR.ConfigReport;
begin
//  inherited;
  ExportClass.ExportDone := False;
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    QRLblWorkspotFrom.Caption := QRParameters.FWorkspotFrom;
    QRLblWorkspotTo.Caption := QRParameters.FWorkspotTo;
  end
  else
  begin
    QRLblWorkspotFrom.Caption := '*';
    QRLblWorkspotTo.Caption := '*';
  end;
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);
end; // ConfigReport

function TReportJobCommentsQR.QRSendReportParameters(const PlantFrom,
  PlantTo: String; const WorkspotFrom, WorkspotTo: String;
  const DateFrom, DateTo: TDateTime; const ShowSelection,
  ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '', '');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      WorkspotFrom, WorkspotTo,
      DateFrom, DateTo,
      ShowSelection,
      ExportToFile);
  end;
  SetDataSetQueryReport(ReportJobCommentsDM.qryJobComments);
end;

function TReportJobCommentsQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  JC.PLANT_CODE, P.DESCRIPTION PDESCRIPTION, ' + NL +
    '  JC.WORKSPOT_CODE, W.DESCRIPTION WDESCRIPTION, ' + NL +
    '  JC.JOB_CODE, J.DESCRIPTION JDESCRIPTION, ' + NL +
    '  JC.EMPLOYEE_NUMBER, E.DESCRIPTION EDESCRIPTION, ' + NL +
    '  JC.TIMESTAMP, ' + NL +
    '  JC.MESSAGE ' + NL +
    'FROM JOBCOMMENT JC INNER JOIN PLANT P ON ' + NL +
    '  JC.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '  JC.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '  JC.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '  JC.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '  JC.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '  JC.JOB_CODE = J.JOB_CODE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '  JC.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  JC.PLANT_CODE >= :PLANTFROM ' + NL +
    '  AND JC.PLANT_CODE <= :PLANTTO ' + NL +
    '  AND JC.TIMESTAMP >= :DATEFROM ' + NL +
    '  AND JC.TIMESTAMP < :DATETO ' + NL;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    SelectStr := SelectStr +
      ' AND WORKSPOT_CODE >= :WORKSPOTFROM ' + NL +
      ' AND WORKSPOT_CODE <= :WORKSPOTTO ' + NL;
  end;
  SelectStr := SelectStr +
    'ORDER BY ' + NL +
    '  JC.PLANT_CODE, ' + NL +
    '  JC.WORKSPOT_CODE, ' + NL +
    '  JC.TIMESTAMP ';

  with ReportJobCommentsDM do
  begin
    with qryJobComments do
    begin
      Close;
      ParamByName('PLANTFROM').AsString := QRBaseParameters.FPlantFrom;
      ParamByName('PLANTTO').AsString := QRBaseParameters.FPlantTo;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        ParamByName('WORKSPOTFROM').AsString := QRParameters.FWorkspotFrom;
        ParamByName('WORKSPOTTO').AsString := QRParameters.FWorkspotTo;
      end;
      ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
      ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo + 1);
      Open;
      Result := not Eof;
    end;
  end;
end; // ExistsRecords

procedure TReportJobCommentsQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportJobCommentsQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption;
end;

procedure TReportJobCommentsQR.QRBandDetailsBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  QRLabel7.Caption :=
    FormatDateTime('dd-mm-yyyy hh:nn',
      ReportJobCommentsDM.qryJobComments.FieldByName('TIMESTAMP').AsDateTime);
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    with ReportJobCommentsDM do
    begin
      ExportDetail(
        qryJobComments.FieldByName('WORKSPOT_CODE').AsString,
        qryJobComments.FieldByName('WDESCRIPTION').AsString,
        qryJobComments.FieldByName('JOB_CODE').AsString,
        qryJobComments.FieldByName('JDESCRIPTION').AsString,
        qryJobComments.FieldByName('MESSAGE').AsString,
        qryJobComments.FieldByName('EMPLOYEE_NUMBER').AsString,
        qryJobComments.FieldByName('EDESCRIPTION').AsString,
        QRLabel7.Caption
        );
    end;
  end;
end;

procedure TReportJobCommentsQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      // Plant
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      // Workspot
      ExportClass.AddText(QRLblFromWorkspot.Caption + ' ' +
        QRLblWorkspot.Caption + ' ' + QRLblWorkspotFrom.Caption + ' ' +
        QRLblToWorkspot.Caption + ' ' + QRLblWorkspotTo.Caption);
      // Date
      ExportClass.AddText(QRLblFromDate.Caption + ' ' +
        QRLblDate.Caption + ' ' + QRLblDateFrom.Caption + ' ' +
        QRLblToDate.Caption + ' ' + QRLblDateTo.Caption);
    end;
  end;
end;

procedure TReportJobCommentsQR.QRBandSummaryBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportJobCommentsQR.QRGroupHdPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    with ReportJobCommentsDM do
    begin
      ExportClass.AddText(
        QRLabel54.Caption + ExportClass.Sep +
        qryJobComments.FieldByName('PLANT_CODE').AsString  + ExportClass.Sep +
        qryJobComments.FieldByName('PDESCRIPTION').AsString);
    end;
  end;
end;

procedure TReportJobCommentsQR.QRGroupHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportDetail(
      QRLabel1.Caption, // Workspot
      '',
      QRLabel3.Caption, // Job
      '',
      QRLabel4.Caption, // Comment
      QRLabel5.Caption, // Employee
      '',
      QRLabel6.Caption // Date
      );
  end;
end;

end.
