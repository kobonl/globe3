(*
  MRA:3-DEC-2009 RV047.2.
  - Filter employees on user by team-per-user.
  MRA:4-DEC-2009 RV047.4.
  - Log when an id-card is deleted.
  MRA:8-NOV-2010 RV077.4.
  - Prevent editing record without first setting to edit-mode.
  - DataSourceDetail.AutoEdit is set to 'False'.
*)
unit IDCardDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, Provider, DBClient;

type
  TIDCardDM = class(TGridBaseDM)
    TableMasterIDCARD_NUMBER: TStringField;
    TableMasterEMPLOYEE_NUMBER: TIntegerField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterSTARTDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterENDDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterEMPLOYEELU: TStringField;
    TableMasterEMPLOYEESHORTLU: TStringField;
    TableDetailIDCARD_NUMBER: TStringField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailSTARTDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailENDDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailEMPLOYEELU: TStringField;
    TableDetailEMPLOYEESHORTLU: TStringField;
    TableDetailEMPCALC: TFloatField;
    QueryIdCardEmpl: TQuery;
    QueryEmpl: TQuery;
    ClientDataSetEmpl: TClientDataSet;
    DataSetProviderEmpl: TDataSetProvider;
    DataSourceEmployee: TDataSource;
    QueryEmployee: TQuery;
    procedure TableMasterNewRecord(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
  end;

var
  IDCardDM: TIDCardDM;

implementation

uses SystemDMT, UPimsConst, UPimsMessageRes, UGlobalFunctions;

{$R *.DFM}

procedure TIDCardDM.TableMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  DataSet.FieldByName('STARTDATE').AsDateTime := Now;
  if not ClientDataSetEmpl.IsEmpty then
  begin
    ClientDataSetEmpl.First;
    DataSet.FieldByName('EMPLOYEE_NUMBER').Value :=
      ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').Value;
  end;
end;

procedure TIDCardDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByNAME('FLOAT_EMP').AsFLOAT :=
    DataSet.FieldByNAME('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TIDCardDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if DataSet.FieldByNAME('ENDDATE').Value <> Null then
    DataSet.FieldByNAME('ENDDATE').AsDateTime :=
      Trunc(DataSet.FieldByNAME('ENDDATE').AsDateTime);
  if DataSet.FieldByNAME('STARTDATE').Value <> Null then
    DataSet.FieldByNAME('STARTDATE').AsDateTime :=
      Trunc(DataSet.FieldByNAME('STARTDATE').AsDateTime);
  // MR:27-07-2005 Check for dates.
  if DataSet.FieldByName('ENDDATE').Value <> Null then
    if (DataSet.FieldByName('STARTDATE').AsDateTime >
      DataSet.FieldByName('ENDDATE').AsDateTime) then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbOK]);
      DataSet.Cancel;
      SysUtils.Abort;
    end;
end;

procedure TIDCardDM.DataModuleCreate(Sender: TObject);
begin
// 550279 - CAR 12-19-2003
//  QueryIdCardEmpl.Prepare;
  // RV047.2. Used by filtering for grid-contents (TableDetail).
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    TableDetail.Filtered := True;
    TableDetail.OnFilterRecord := FilterRecord;
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end
  else
    QueryEmpl.ParamByName('USER_NAME').AsString := '*';
  ClientDataSetEmpl.Open;
  // RV047.2. Used by employee-lookup-component.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmployee.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmployee.ParamByName('USER_NAME').AsString := '*';
  QueryEmployee.Open;
  inherited;
end;

procedure TIDCardDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
// 550279 - CAR 12-19-2003
  QueryEmployee.Close;
  QueryEmpl.Close;
  QueryIdCardEmpl.Close;
//  QueryIdCardEmpl.UnPrepare;
  ClientDataSetEmpl.Close;
end;

//550279 - CAR
procedure TIDCardDM.FilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := ClientDataSetEmpl.FindKey([DataSet.
    FieldByName('EMPLOYEE_NUMBER').AsInteger]);
end;

procedure TIDCardDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  // RV047.4.
  try
    WLog('IDCard deleted for EMP=' +
      DataSet.FieldByName('EMPLOYEE_NUMBER').AsString +
      ' IDCARD=' +
      DataSet.FieldByName('IDCARD_NUMBER').AsString
      );
  except
    // Ignore error.
  end;
  SystemDM.DefaultBeforeDelete(DataSet);
end;

end.
