(*
    SO:08-JUL-2010 RV067.4. 550497
    MRA:31-AUG-2010 RV067.MRA.21. Changes linked to order 550497.
    MRA:1-OCT-2010 RV071.1.
    - Sort order of detail-table must be on code.
      Solved by setting the TableDetail.IndexName to primary key.
*)
unit AbsTypePerCountryDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TAbsTypePerCountryDM = class(TGridBaseDM)
    TableDetailCOUNTRY_ID: TIntegerField;
    TableDetailABSENCETYPE_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailACTIVE_YN: TStringField;
    TableDetailEXPORT_CODE: TStringField;
    TableDetailCOUNTER_ACTIVE_YN: TStringField;
    TableDetailCOUNTER_DESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableMasterCOUNTRY_ID: TFloatField;
    TableMasterEXPORT_TYPE: TStringField;
    TableMasterCODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    procedure DefaultBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AbsTypePerCountryDM: TAbsTypePerCountryDM;

implementation

uses SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TAbsTypePerCountryDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
  // RV067.MRA.21. Use UPimsMessageRes-strings instead of hardcoded strings.
  if Trim(Dataset.FieldByName('DESCRIPTION').AsString) = '' then
  begin
    DisplayMessage(SPimsDescriptionNotEmpty, mtWarning, [mbOK]);
    Abort;
  end;
  if
    (Dataset.FieldByName('COUNTER_ACTIVE_YN').AsString = 'Y') and
    (Trim(Dataset.FieldByName('COUNTER_DESCRIPTION').AsString) = '')
  then
  begin
    DisplayMessage(SPimsCounterNameFilled, mtWarning, [mbOK]);
    Abort;
  end;
end;

end.
