(*
  Changes:
    MR:13-10-2005 Order 550411
      Select should give results for 3 situations:
      1. USE_JOBCODE_YN = 'Y' AND JOB_CODE <> '0'
      2. USE_JOBCODE_YN = 'N' (for any jobcode; '0' or not '0')
      3. USE_JOBCODE_YN = 'Y' AND JOB_CODE = '0'
      This can happen if user switches these settings in Pims on Workspot-level.
      The hours from PRODHOURPEREMPLOYEE should always be shown, regardless
      of these settings.
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Field WEEKNUMBER gave error like:
        QueryDirIndHrs:
          Type mismatch for field WEEKNUMBER, expecting float, actual integer.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'. (QueryDirIndHrs).
      Also field-component for QueryDirIndHrs for this field must be 'float'.
    MRA:08-08-2008 RV008.
      - Ordering added (order by) (Oracle 10).
      - Show period according to week-selection.
*)

unit ReportDirIndHrsWeekQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type

  TQRParameters = class
  private
    FYear, FWeekFrom, FWeekTo: Integer;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWorkSpotFrom, FWorkSpotTo,
    FJobCodeFrom, FJobCodeTo: String;
    FShowSelection: Boolean;
    FExportToFile: Boolean;
  public
    procedure SetValues(Year, WeekFrom, WeekTo: Integer;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WorkSpotFrom, WorkSpotTo,
      JobCodeFrom, JobCodeTo: String;
      ShowSelection: Boolean;
      ExportToFile: Boolean);
  end;

  TReportDirIndHrsWeekQR = class(TReportBaseF)
    QRBandDetailEmployee: TQRBand;
    QRBandSummary: TQRBand;
    QRLabel4: TQRLabel;
    QRLabelTotDirHrs: TQRLabel;
    QRLabelTotIndHrs: TQRLabel;
    QRLabelTotWeekHrs: TQRLabel;
    QRLabelTotIndPerc: TQRLabel;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabelYear: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelJobCodeFrom: TQRLabel;
    QRLabelJobCodeTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWorkSpotFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWorkSpotTo: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabelDirAvg: TQRLabel;
    QRLabelIndAvg: TQRLabel;
    QRLabelTotAvg: TQRLabel;
    ChildBandChildTitle: TQRChildBand;
    QRShape1: TQRShape;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRShape2: TQRShape;
    QRGroupWeekNumber: TQRGroup;
    QRBandWeekNumberFooter: TQRBand;
    QRLabelWeek: TQRLabel;
    QRLabelDirHrs: TQRLabel;
    QRLabelIndHrs: TQRLabel;
    QRLabelTotHrs: TQRLabel;
    QRLabelIndPerc: TQRLabel;
    ChildBandWeekUnknown: TQRChildBand;
    QRLabelWeekUN: TQRLabel;
    QRLabelDirHrsUN: TQRLabel;
    QRLabelUnknownBusinessUnit: TQRLabel;
    QRLabelIndHrsUN: TQRLabel;
    QRLabelTotHrsUN: TQRLabel;
    QRLabelIndPercUN: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotDirHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelTotIndHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotWeekHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelTotIndPercPrint(sender: TObject; var Value: String);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelDirAvgPrint(sender: TObject; var Value: String);
    procedure QRLabelIndAvgPrint(sender: TObject; var Value: String);
    procedure QRLabelTotAvgPrint(sender: TObject; var Value: String);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandWeekNumberFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandWeekUnknownBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    AllPlants, AllEmployees, AllBusinessUnits: Boolean;
    function DetermineSUMMIN: Double;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate, FMaxDate: TDateTime;
    FDir, FTotDir, FInd, FTotInd, FIndPerc, FAvgDir, FAvgInd: Integer;
    FDirUnknown, FIndUnknown: Integer;
    FTotWeek: Integer;
    FPrintChild: Boolean;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WorkSpotFrom, WorkSpotTo,
      JobCodeFrom, JobCodeTo, EmployeeFrom, EmployeeTo: String;
      const Year, WeekFrom, WeekTo: Integer;
      const ShowSelection: Boolean;
      const ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure PrintDetailAmounts(Week, DirAmount, IndAmount: Integer);
    procedure PrintDetailAmountsUnknown(Week, DirAmount, IndAmount: Integer);
  end;

var
  ReportDirIndHrsWeekQR: TReportDirIndHrsWeekQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportDirIndHrsWeekDMT, ListProcsFRM, UPimsConst, Math;

// MR:12-12-2002
procedure ExportDetail(
  Week, DirectHrs, IndirectHrs, TotalHrs, IndirectPerc: String);
begin
  ExportClass.AddText(
    Week + ExportClass.Sep +
    DirectHrs + ExportClass.Sep +
    IndirectHrs + ExportClass.Sep +
    TotalHrs + ExportClass.Sep +
    IndirectPerc
    );
end;

procedure TQRParameters.SetValues(Year, WeekFrom, WeekTo: Integer;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WorkSpotFrom, WorkSpotTo,
      JobCodeFrom, JobCodeTo: String;
      ShowSelection: Boolean;
      ExportToFile: Boolean);
begin
  FYear := Year;
  FWeekFrom := WeekFrom;
  FWeekTo := WeekTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FWorkSpotFrom := WorkSpotFrom;
  FWorkSpotTo := WorkSpotTo;
  FJobCodeFrom := JobCodeFrom;
  FJobCodeTo := JobCodeTo;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
end;

function TReportDirIndHrsWeekQR.QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WorkSpotFrom, WorkSpotTo,
      JobCodeFrom, JobCodeTo, EmployeeFrom, EmployeeTo: String;
      const Year, WeekFrom, WeekTo: Integer;
      const ShowSelection: Boolean;
      const ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, WeekFrom, WeekTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WorkSpotFrom, WorkSpotTo,
      JobCodeFrom, JobCodeTo, ShowSelection, ExportToFile);
  end;
  SetDataSetQueryReport(ReportDirIndHrsWeekDM.QueryDirIndHrs);
end;

function TReportDirIndHrsWeekQR.ExistsRecords: Boolean;
var
  SelectStr, SelectStr1, SelectStr2, SelectStr3, TempStr: String;
begin
  Screen.Cursor := crHourGlass;
  FMinDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekFrom, 1);
  FMaxDate :=
    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeekTo, 7);
  with ReportDirIndHrsWeekDM do
  begin
    FillWeekTable(FMinDate, FMaxDate);
    AllPlants := DetermineAllPlants(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
    AllEmployees := DetermineAllEmployees(
      QRBaseParameters.FEmployeeFrom, QRBaseParameters.FEmployeeTo);
    AllBusinessUnits := DetermineAllBusinessUnits(
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
  end;
  TempStr :=
    'SELECT ' + NL +
    '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL +
    '  A.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  D.DIRECT_HOUR_YN, ' + NL +
    '  A.PLANT_CODE, ' + NL +
    '  A.WORKSPOT_CODE, ' + NL +
    '  SUM(A.PRODUCTION_MINUTE) AS SUMMIN, ' + NL;
  SelectStr1 := TempStr;
  SelectStr2 := TempStr;
  SelectStr3 := TempStr;
//
  TempStr :=
    '  MAX(J.BUSINESSUNIT_CODE) AS BUCODE, ' + NL +
    '  MAX(1) AS LISTCODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '  MAX(BW.BUSINESSUNIT_CODE) AS BUCODE, ' + NL +
    '  MAX(2) AS LISTCODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '  MAX(J.BUSINESSUNIT_CODE) AS BUCODE, ' + NL + // Dummy!
    '  MAX(3) AS LISTCODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '    AND (A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
    '    AND A.PRODHOUREMPLOYEE_DATE <= :FENDDATE) ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if not AllPlants then
  begin
    TempStr :=
      '    AND A.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '    AND A.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    A.PRODHOUREMPLOYEE_DATE >= WK.DATEFROM AND ' + NL +
    '    A.PRODHOUREMPLOYEE_DATE <= WK.DATETO ' + NL +
    '  INNER JOIN DEPARTMENT D ON   ' + NL +
    '    A.PLANT_CODE = D.PLANT_CODE ' + NL +
    '    AND E.DEPARTMENT_CODE = D.DEPARTMENT_CODE   ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    A.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND A.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (A.JOB_CODE <> ''' + DUMMYSTR + ''') ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''N'' ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (A.JOB_CODE = ''' + DUMMYSTR + ''')' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if not AllEmployees then
    begin
      TempStr :=
        '  AND A.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND A.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
  end;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    TempStr :=
      '  AND D.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND D.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    TempStr :=
      '  AND W.WORKSPOT_CODE >= ''' +
      DoubleQuote(QRParameters.FWorkSpotFrom) + '''' + NL +
      '  AND W.WORKSPOT_CODE <= ''' +
      DoubleQuote(QRParameters.FWorkSpotTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
// -> SelectStr1
  TempStr :=
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    A.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND A.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND A.JOB_CODE = J.JOB_CODE   ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if not AllBusinessUnits then
    begin
      TempStr := TempStr +
        '    AND J.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '    AND J.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
    end;
  end;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
    (QRParameters.FWorkSpotFrom = QRParameters.FWorkSpotTo) then
  begin
    if (QRParameters.FJobCodeFrom <> '') and
      (QRParameters.FJobCodeTo <> '') then
    begin
      TempStr := TempStr +
        '  AND A.JOB_CODE >= ''' +
        DoubleQuote(QRParameters.FJobCodeFrom) + '''' + NL +
        '  AND A.JOB_CODE <= ''' +
        DoubleQuote(QRParameters.FJobCodeTo) + '''' + NL;
    end;
  end;
  SelectStr1 := SelectStr1 + TempStr;
// -> SelectStr2
  TempStr :=
    '  INNER JOIN BUSINESSUNITPERWORKSPOT BW ON ' + NL +
    '    BW.PLANT_CODE = W.PLANT_CODE ' + NL +
    '    AND BW.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if not AllBusinessUnits then
    begin
      TempStr := TempStr +
        '    AND BW.BUSINESSUNIT_CODE >= ''' +
        DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '    AND BW.BUSINESSUNIT_CODE <= ''' +
        DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
    end;
  end;
  SelectStr2 := SelectStr2 + TempStr;
// -> SelecStr3
  TempStr :=
    '  LEFT JOIN JOBCODE J ON ' + NL +
    '    A.PLANT_CODE = J.PLANT_CODE ' + NL +
    '    AND A.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '    AND A.JOB_CODE = J.JOB_CODE   ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    'GROUP BY ' + NL +
    '  WK.WEEKNUMBER, ' + NL +
    '  A.PRODHOUREMPLOYEE_DATE, ' + NL +
    '  D.DIRECT_HOUR_YN, ' + NL +
    '  A.PLANT_CODE, ' + NL +
    '  A.WORKSPOT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  SelectStr := SelectStr1 + ' UNION ' + NL +
    SelectStr2 + ' UNION ' + NL +
    SelectStr3 + ' ' + NL +
    'ORDER BY ' + NL +
    '  1, ' + NL +  // WK.WEEKNUMBER
    '  2, ' + NL +  // A.PRODHOUREMPLOYEE_DATE
    '  3, ' + NL +  // D.DIRECT_HOUR_YN
    '  4, ' + NL +  // A.PLANT_CODE
    '  5 ' + NL; // A.WORKSPOT_CODE

  ReportDirIndHrsWeekDM.QueryDirIndHrs.Active := False;
  ReportDirIndHrsWeekDM.QueryDirIndHrs.UniDirectional := False;
  ReportDirIndHrsWeekDM.QueryDirIndHrs.SQL.Clear;
  ReportDirIndHrsWeekDM.QueryDirIndHrs.SQL.Add(UpperCase(SelectStr));
//
//ReportDirIndHrsWeekDM.QueryDirIndHrs.SQL.SaveToFile('c:\temp\RepDirIndHrsWeek.sql');
//
  ReportDirIndHrsWeekDM.QueryDirIndHrs.ParamByName('FSTARTDATE').Value :=
    FMinDate;
  ReportDirIndHrsWeekDM.QueryDirIndHrs.ParamByName('FENDDATE').Value :=
    FMaxDate;
  if not ReportDirIndHrsWeekDM.QueryDirIndHrs.Prepared then
    ReportDirIndHrsWeekDM.QueryDirIndHrs.Prepare;
  ReportDirIndHrsWeekDM.QueryDirIndHrs.Active := True;
  Result := not ReportDirIndHrsWeekDM.QueryDirIndHrs.IsEmpty;
  if Result then
  begin
    ReportDirIndHrsWeekDM.cdsQueryBUWK.Close;
    ReportDirIndHrsWeekDM.cdsQueryBUWK.Open;
  end;
  FDir := 0;
  FTotDir := 0;
  FInd := 0;
  FTotInd := 0;
  FIndPerc := 0;
  FDirUnknown := 0;
  FIndUnknown := 0;
  FTotWeek := 0;
 {check if report is empty}
  Screen.Cursor := crDefault;
end;

procedure TReportDirIndHrsWeekQR.ConfigReport;
begin
  // MR:12-12-2002
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
  QRLabelWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
  QRLabelWeekTo.Caption := IntToStr(QRParameters.FWeekTo) + ' ' +
    '(' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekFrom, 1)) + ' - ' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekTo, 7)) + ')';

  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWorkSpotFrom.Caption := QRParameters.FWorkSpotFrom;
  QRLabelWorkSpotTo.Caption := QRParameters.FWorkSpotTo;

  QRLabelJobCodeFrom.Caption := QRParameters.FJobCodeFrom;
  QRLabelJobCodeTo.Caption := QRParameters.FJobCodeTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWorkSpotFrom.Caption := '*';
    QRLabelWorkSpotTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
  end
  else
    if QRParameters.FWorkSpotFrom <> QRParameters.FWorkSpotTo then
    begin
      QRLabelJobCodeFrom.Caption := '*';
      QRLabelJobCodeTo.Caption := '*';
    end;

end;

procedure TReportDirIndHrsWeekQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportDirIndHrsWeekQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:12-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

// Businessunitperworkspot and Percentage
function TReportDirIndHrsWeekQR.DetermineSUMMIN: Double;
begin
  with ReportDirIndHrsWeekDM do
  begin
    Result := QueryDirIndHrs.FieldByName('SUMMIN').AsInteger;
    // Only do this for the second select of the UNIONS.
    // Only this select is of correct type (join to BUSINESSUNITPERWORKSPOT).
    if QueryDirIndHrs.FieldByName('LISTCODE').AsInteger = 2 then
    begin
      if cdsQueryBUWK.Locate('PLANT_CODE;WORKSPOT_CODE;BUSINESSUNIT_CODE',
        VarArrayOf([QueryDirIndHrs.FieldByName('PLANT_CODE').AsString,
          QueryDirIndHrs.FieldByName('WORKSPOT_CODE').AsString,
          QueryDirIndHrs.FieldByName('BUCODE').AsString]), []) then
        Result :=
          cdsQueryBUWK.FieldByName('SUMPERC').AsFloat / 100 *
            QueryDirIndHrs.FieldByName('SUMMIN').AsInteger;
      if Result = 0 then
        Result := QueryDirIndHrs.FieldByName('SUMMIN').AsInteger;
    end;
  end;
end;

procedure TReportDirIndHrsWeekQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  FDir := 0;
  FTotDir := 0;
  FInd := 0;
  FTotInd := 0;
  FIndPerc := 0;
  FTotWeek := 0;

  // MR:12-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(
          QRLabel11.Caption
          );
        // From Plant PlantFrom to PlantTo
        ExportClass.AddText(
          QRLabel13.Caption + ' ' +
          QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' +
          QRLabel49.Caption + ' ' +
          QRLabelPlantTo.Caption
          );
        // From BusinessUnit BUFrom to BUTo
        ExportClass.AddText(
          QRLabel25.Caption + ' ' +
          QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' +
          QRLabel16.Caption + ' ' +
          QRLabelBusinessTo.Caption
          );
        // From Department DepartmentFrom to DepartmentTo
        ExportClass.AddText(
          QRLabel47.Caption + ' ' +
          QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' +
          QRLabel50.Caption + ' ' +
          QRLabelDeptTo.Caption
          );
        // From Workspot WorkspotFrom to WorkspotTo
        ExportClass.AddText(
          QRLabel87.Caption + ' ' +
          QRLabel88.Caption + ' ' +
          QRLabelWorkspotFrom.Caption + ' ' +
          QRLabel90.Caption + ' ' +
          QRLabelWorkspotTo.Caption
          );
        // From Employee EmployeeFrom to EmployeeTo
        ExportClass.AddText(
          QRLabel18.Caption + ' ' +
          QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' +
          QRLabel22.Caption + ' ' +
          QRLabelEmployeeTo.Caption
          );
        // From Jobcode JobcodeFrom to JobcodeTo
        ExportClass.AddText(
          QRLabel51.Caption + ' ' +
          QRLabelAbsenceRsn.Caption + ' ' +
          QRLabelJobCodeFrom.Caption + ' ' +
          QRLabel53.Caption + ' ' +
          QRLabelJobCodeTo.Caption
          );
        // Year Year
        ExportClass.AddText(
          QRLabel2.Caption + ' ' + QRLabelYear.Caption
          );
        // From Week to Week
        ExportClass.AddText(
          QRLabelFromDate.Caption + ' ' +
          QRLabelDate.Caption + ' ' +
          QRLabelWeekFrom.Caption + ' ' +
          QRLabelToDate.Caption + ' ' +
          QRLabelWeekTo.Caption
          );
      end;
      // Show Column-names
      ExportDetail(
        QRLabel6.Caption, // Week
        QRLabel7.Caption, // Direct Hours
        QRLabel8.Caption, // Indirect Hours
        QRLabel9.Caption, // Total Hours
        QRLabel10.Caption  // Indirect %
        );
    end;
end;


procedure TReportDirIndHrsWeekQR.QRLabelTotDirHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FTotDir);
end;

procedure TReportDirIndHrsWeekQR.QRLabelTotIndHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FTotInd);
end;

procedure TReportDirIndHrsWeekQR.QRLabelTotWeekHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FTotDir + FTotInd);
end;

procedure TReportDirIndHrsWeekQR.QRLabelTotIndPercPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if (FTotInd + FTotDir) <> 0 then
    Value := IntToStr(Round((FTotInd * 100)/(FTotInd + FTotDir)))
  else
    Value := '0.0';
  Value := Copy(Value, 0, 4);
end;

procedure TReportDirIndHrsWeekQR.PrintDetailAmounts(
  Week, DirAmount, IndAmount: Integer);
begin
  QRLabelWeek.Caption := IntToStr(Week);
  QRLabelDirHrs.Caption := DecodeHrsMin(DirAmount);
  QRLabelIndHrs.Caption := DecodeHrsMin(IndAmount);
  QRLabelTotHrs.Caption := DecodeHrsMin(DirAmount + IndAmount);
  if (DirAmount <> 0) or (IndAmount <> 0) then
    QRLabelIndPerc.Caption :=
      IntToStr(Round((IndAmount * 100)/(DirAmount + IndAmount)))
  else
    QRLabelIndPerc.Caption := '0.0';
  // MR:13-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      IntToStr(Week),
      IntToStr(DirAmount),
      IntToStr(IndAmount),
      IntToStr(DirAmount + IndAmount),
      QRLabelIndPerc.Caption
      );
end;

procedure TReportDirIndHrsWeekQR.PrintDetailAmountsUnknown(
  Week, DirAmount, IndAmount: Integer);
begin
  QRLabelWeekUN.Caption := IntToStr(Week);
  QRLabelDirHrsUN.Caption := DecodeHrsMin(DirAmount);
  QRLabelIndHrsUN.Caption := DecodeHrsMin(IndAmount);
  QRLabelTotHrsUN.Caption := DecodeHrsMin(DirAmount + IndAmount);
  if (DirAmount <> 0) or (IndAmount <> 0) then
    QRLabelIndPercUN.Caption :=
      IntToStr(Round((IndAmount * 100)/(DirAmount + IndAmount)))
  else
    QRLabelIndPercUN.Caption := '0.0';
  // MR:13-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      IntToStr(Week) + ' ' + QRLabelUnknownBusinessUnit.Caption,
      IntToStr(DirAmount),
      IntToStr(IndAmount),
      IntToStr(DirAmount + IndAmount),
      QRLabelIndPercUN.Caption
      );
end;

procedure TReportDirIndHrsWeekQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if (ReportDirIndHrsWeekDM.
    QueryDirIndHrs.FieldByName('LISTCODE').AsInteger <> 3) or
    (AllPlants) or (AllBusinessUnits) then
  begin
    if ReportDirIndHrsWeekDM.
      QueryDirIndHrs.FieldByName('DIRECT_HOUR_YN').AsString = 'Y' then
      FDir := FDir + Round(DetermineSUMMIN)
    else
      FInd := Find + Round(DetermineSUMMIN);
  end
  else
  begin
    if ReportDirIndHrsWeekDM.
      QueryDirIndHrs.FieldByName('DIRECT_HOUR_YN').AsString = 'Y' then
      FDirUnknown := FDirUnknown + Round(DetermineSUMMIN)
    else
      FIndUnknown := FIndUnknown + Round(DetermineSUMMIN);
  end;
end;

procedure TReportDirIndHrsWeekQR.QRLabelDirAvgPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if FTotWeek > 0 then
    FAvgDir := Round(FTotDir / FTotWeek)
  else
    FAvgDir := 0;
  Value := DecodeHrsMin(FAvgDir);
end;

procedure TReportDirIndHrsWeekQR.QRLabelIndAvgPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if FTotWeek > 0 then
    FAvgInd := Round(FTotInd / FTotWeek)
  else
    FTotWeek := 0;
  Value := DecodeHrsMin(FAvgInd);
end;

procedure TReportDirIndHrsWeekQR.QRLabelTotAvgPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FAvgInd + FAvgDir);
end;

procedure TReportDirIndHrsWeekQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  IndirectPerc: String; // MR:13-12-2002
begin
  inherited;
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    if (FTotInd + FTotDir) <> 0 then
      IndirectPerc := IntToStr(Round((FTotInd * 100)/(FTotInd + FTotDir)))
    else
      IndirectPerc := '0.0';
    // Total
    ExportDetail(
      QRLabel4.Caption,
      IntToStr(FTotDir),
      IntToStr(FTotInd),
      IntToStr(FTotDir + FTotInd),
      IndirectPerc
      );
    // Average
    ExportDetail(
      QRLabel5.Caption,
      IntToStr(FAvgDir),
      IntToStr(FAvgInd),
      IntToStr(FAvgDir + FAvgInd),
      ''
      );
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportDirIndHrsWeekQR.QRGroupWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  FDir := 0;
  FInd := 0;
  FDirUnknown := 0;
  FIndUnknown := 0;
end;

procedure TReportDirIndHrsWeekQR.QRBandWeekNumberFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintDetailAmounts(
    ReportDirIndHrsWeekDM.QueryDirIndHrs.FieldByName('WEEKNUMBER').AsInteger,
      FDir, FInd);
  FTotDir := FTotDir + FDir + FDirUnknown;
  FTotInd := FTotInd + FInd + FIndUnknown;
  FTotWeek := FTotWeek + 1;
end;

procedure TReportDirIndHrsWeekQR.ChildBandWeekUnknownBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  // Print Unknown, if there is any unknown found.
  if (FDirUnknown <> 0) or (FIndUnknown <> 0) then
  begin
    PrintBand := True;
    PrintDetailAmountsUnknown(
      ReportDirIndHrsWeekDM.QueryDirIndHrs.FieldByName('WEEKNUMBER').AsInteger,
        FDirUnknown, FIndUnknown);
  end;
end;

end.
