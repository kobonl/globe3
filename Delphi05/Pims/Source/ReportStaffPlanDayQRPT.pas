(*
  Changes:
  MRA:24-MAY-2012 20013169. Changes to report staff planning per day
  - Report Staff Planning per Day
    - Above the report is mentioned the day and date
    - The planning is displayed in a table
    - Only the name of the employee should be shown and also sorted on name
    - Only the name of the workspot should be shown
    - The report must be displayed in landscape
    NOTE: This should be done when sorting on employee is checked
    Technical problem: The column-header did not align correct to landscape,
    resulting in a part not shown in 'clSilver'.
    Solved by showing the color with the TQRLabel's by resizing them.
  MRA:31-MAY-2012 20013169. Rework.
  - Some changes were made:
    - Columns for timeblocks are lined to the right and now have
      some more space.
    - Font is now bold for grey column-header.
    - Font is now regular for employee in left column.
    - Change made for export because timeblocks were not in correct
      column in export-file.
    - Description was wrong (showed 'desc') for plant and shift when exported.
  MRA:15-JUN-2012 20013169. Rework.
  - Make font same as employee-font: Arial-8-regular.
  MRA:18-JAN-2013. 20013910.
  - Plan on departments gives problems.
  MRA:6-FEB-2013. 20013910. Rework.
  - Plan on departments gives problems:
    - Only first found department was used, all others were not shown.
    - CAUSE: Grouping went wrong because workspot is '0' for
             planning-on-departments.
  MRA:5-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - This report was already in landscape but not correct.
  - To get the report (correct) to landscape:
    - Make the size wider in layout to fit in landscape.
    - Then first change the qrptBase settings about Page to
      PaperSize=Custom, Length=210.0mm, WIdth=297.0mm.
    - After that change the page-settings in layout to
      PaperSize=Default.
  MRA:5-DEC-2018 PIM-412 Bugfix
  - It gave a difference when comparing with Report staff planning.
  - Reason: It used an uppercase for the whole select, but some
    departments were partly in lowercase, like 'TowelF'.
  MRA:23-JAN-2019 GLOB3-225
  - Changes for extension 4 to 10 blocks for planning
  - When MaxTimeblocks=4 do not show extra blocks in report-header.
  MRA:18-FEB-2019 GLOB3-255
  - Report Staff Planning Per Day does not fit on Letter-format
  - It is unknown why it gives this problem
  - To solve it: The footer has been disabled and we print the date+time
    and page-nummer in the header.
  MRA:22-FEB-2019 GLOB3-264
  - Report staff planning per day: export must be corrected
  MRA:25-FEB-2019 GLOB3-264
  - Bugfix for export.
  MRA:1-JUL-2019 PIM-377
  - Show start and end times in planning reports
  - It did not show a workspot starting with 0, reason, when selecting ALL,
    it takes '1' as From-value, this must be '0', also for departments.
*)
unit ReportStaffPlanDayQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, Printers, UPimsConst;

type
  TEmpPlanRecord = record
    ADate: TDateTime;
    AStart, AEnd: TDateTime;
    AWorkspotCode, AWDescription: String;
  end;

  TTBWeekArray = Array[1..MAX_TBS] of Integer;
  TTBSCHEDULE = Array[1..MAX_TBS] of String;
  TEmpPlanRecordArray = Array[1..MAX_TBS] of TEmpPlanRecord;

  TQRParameters = class
  private
    FShiftFrom, FShiftTo: String;
    FDeptFrom, FDeptTo, FTeamFrom, FTeamTo, FWKFrom, FWKTo: String;
    FShowOnlyDiff, FShowSelection, FPagePlant,  FPageShift, FPageWK,
    FPaidBreaks, FShowTB, FAllTeam: Boolean;
    FDateFrom, FDateTo: TDateTime;
    FShowTotals: Boolean;
    FShowNPlannedWorkspots: Boolean;
    FSortEMP: Boolean;
    FExportToFile: Boolean;
    FShowStartEndTimes: Boolean;
    FSortOnEmployeeNumber: Integer;
  public
    procedure SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
      ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK, PaidBreaks,
      ShowTB, AllTeam: Boolean; DateFrom, DateTo: TDateTime;
      ShowTotals, ShowNPlannedWorkspots, SortEMP, ExportToFile,
      ShowStartEndTimes: Boolean; SortOnEmployeeNumber: Integer);
  end;

  TReportStaffPlanDayQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRGroupHDPlant: TQRGroup;
    QRGroupHDShift: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabelShiftDesc: TQRLabel;
    QRLabelWKCode: TQRLabel;
    QRBandWKOnTimeblockFooter: TQRBand;
    QRBandEmpOnTimeblockFooter: TQRBand;
    QRBandDetail: TQRBand;
    QRMemoWKonTB1: TQRMemo;
    QRMemoWKOnTB2: TQRMemo;
    QRMemoWKOnTB3: TQRMemo;
    QRMemoWKOnTB5: TQRMemo;
    QRMemoEmpTB1: TQRMemo;
    QRMemoEmpTB2: TQRMemo;
    QRMemoEmpTB3: TQRMemo;
    QRMemoEmpTB4: TQRMemo;
    QRBandSummary: TQRBand;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabelDay: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabelPlantDesc: TQRLabel;
    QRLabelEmpl: TQRLabel;
    QRBandHeaderWorkspot: TQRBand;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    ChildBandEmpHeader: TQRChildBand;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRMemoEmpTB5: TQRMemo;
    QRMemoWKOnTB4: TQRMemo;
    QRLabel33: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRLabel34: TQRLabel;
    QRSysData3: TQRSysData;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRMemoEmpTotal: TQRMemo;
    QRMemoWKTotal: TQRMemo;
    QRBandFooterShift: TQRBand;
    QRLabelTotalShift: TQRLabel;
    QRLabel38: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabelShiftDescFooter: TQRLabel;
    QRLabel37: TQRLabel;
    QRBandFooterPlant: TQRBand;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabelPlantDescFooter: TQRLabel;
    QRLabelTotalPlant: TQRLabel;
    QRShape3: TQRShape;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDeptDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextShiftDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandEmpOnTimeblockFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDateFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandWKOnTimeblockFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandHeaderWorkspotBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandHeaderEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandEmpHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandHeaderWorkspotAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandEmpHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandEmpOnTimeblockFooterAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandWKOnTimeblockFooterAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBndBasePageFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBndBasePageHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
   private
    FQRParameters: TQRParameters;
    FEmpPlanRecord: TEmpPlanRecordArray;
    TotalCaption: String;
    function MyDecodeHrsMin(HrsMin: Integer): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
    function MaxTB: Integer;
  public
    FStartDay, FEndDay: Integer;

    FMinDate, FMaxDate: TDateTime;
    FStartDate, FEndDate: TDateTime;
    FEmpPlanning, FWKPlanning: TTBWeekArray;
    FMaxTB: Integer;

    FDayDesc, FDate: Array[1..7] of String;
    FShowOnlyDiff, FPaidBreaks, FWKPrint,FSortEMP: Boolean;

    FTotalLines: Integer;

    FPlantTotal: Integer;
    FShiftTotal: Integer;

    procedure InitializeDayPlanning(var DayPlanning:
      TTBWeekArray);
    procedure FillDescDaysPerWeek;
    function QRSendReportParameters(const PlantFrom, PlantTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom, ShiftTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK,
        PaidBreaks, ShowTB, AllTeam, ShowTotals, ShowNPlannedWorkspots,
        SortEmp, ExportToFile, ShowStartEndTimes: Boolean;
        SortOnEmployeeNumber:  Integer): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;

  end;

var
  ReportStaffPlanDayQR: TReportStaffPlanDayQR;

implementation

{$R *.DFM}

uses
  SystemDMT, ReportStaffPlanDayDMT, ListProcsFRM,
  CalculateTotalHoursDMT, UGLobalFunctions;

procedure TQRParameters.SetValues(ShiftFrom, ShiftTo: String;
  DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
  ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK, PaidBreaks, ShowTB,
  AllTeam: Boolean;  DateFrom, DateTo: TDateTime;
  ShowTotals, ShowNPlannedWorkspots, SortEmp, ExportToFile,
  ShowStartEndTimes: Boolean;
  SortOnEmployeeNumber: Integer);
begin
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FWKFrom := WKFrom;
  FWKTo := wkTo;
  FShowOnlyDiff := ShowOnlyDiff;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageWK := PageWK;
  FPageShift := PageShift;
  FPaidBreaks := PaidBreaks;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowTB := ShowTB;
  FAllTeam:= AllTeam;
  FShowTotals := ShowTotals;
  FShowNPlannedWorkspots := ShowNPlannedWorkspots;
  FSortEmp := SortEmp;
  FExportToFile := ExportToFile;
  FShowStartEndTimes := ShowStartEndTimes;
  FSortOnEmployeeNumber := SortOnEmployeeNumber;
end;

function TReportStaffPlanDayQR.QRSendReportParameters(const PlantFrom, PlantTo,
  DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom, ShiftTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowOnlyDiff, ShowSelection, PagePlant, PageShift,  PageWK,
    PaidBreaks, ShowTB, AllTeam, ShowTotals, ShowNPlannedWorkspots,
    SortEmp, ExportToFile, ShowStartEndTimes: Boolean;
    SortOnEmployeeNumber: Integer): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    if FQRParameters = Nil then
      FQRParameters := TQRParameters.Create;
    FQRParameters.SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo,
      ShowOnlyDiff, ShowSelection, PagePlant,  PageShift,PageWK, PaidBreaks,
      ShowTB, AllTeam, DateFrom, DateTo, ShowTotals, ShowNPlannedWorkspots,
      SortEmp, ExportToFile, ShowStartEndTimes, SortOnEmployeeNumber);
  end;
  SetDataSetQueryReport(ReportStaffPlanDayDM.QueryEMP);
end;

function TReportStaffPlanDayQR.ExistsRecords: Boolean;
var
  SelectStr: String;
 // i, K: Integer;
//  Year{, Month, Day}: Word;
//  MyDate: TDateTime;
begin
  Screen.Cursor := crHourGlass;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FDeptFrom := '0';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FWKFrom := '0';
    QRParameters.FWKTo := 'zzzzzz';
    QRParameters.FShiftFrom := '0';
    QRParameters.FShiftTo := '999999';
  end;
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  FSortEMP := QRParameters.FSORTEMP;
  // 20013910
  // Use another way to recognize it is about planning-on-departments instead
  // of on workspots.
  // NOTE: When only '0' is returned for workspot (when it is about
  //       plan-on-depts), then because of grouping it results in only 1 record
  //       (first dept found).
  //       So, be sure it gets different results.
  //       Use a flag to know it is about a workspot or department.
  if not FSortEmp  then
    SelectStr :=
      'SELECT ' + NL +
      '  DISTINCT EMP.PLANT_CODE, EMP.SHIFT_NUMBER, ' + NL +
      '  CASE ' + NL +
      '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN EMP.WORKSPOT_CODE ' + NL +
      '    ELSE EMP.DEPARTMENT_CODE ' + NL +
      '  END WORKSPOT_CODE, ' + NL +
      '  EMP.DEPARTMENT_CODE, EMP.EMPLOYEE_NUMBER, ' + NL +
      '  E.DESCRIPTION, EMP.EMPLOYEEPLANNING_DATE, ' + NL +
      '  CASE ' + NL +
      '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
      '    ELSE ''D'' ' + NL +
      '  END FLAG, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_5, EMP.SCHEDULED_TIMEBLOCK_6, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_7, EMP.SCHEDULED_TIMEBLOCK_8, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_9, EMP.SCHEDULED_TIMEBLOCK_10 ' + NL
  else
    SelectStr :=
      'SELECT DISTINCT EMP.PLANT_CODE, EMP.SHIFT_NUMBER, ' + NL +
      '  EMP.EMPLOYEE_NUMBER, ' + NL +
      '  CASE ' + NL +
      '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN EMP.WORKSPOT_CODE ' + NL +
      '    ELSE EMP.DEPARTMENT_CODE ' + NL +
      '  END WORKSPOT_CODE, ' + NL +
      '  EMP.DEPARTMENT_CODE,  ' + NL +
      '  E.DESCRIPTION, EMP.EMPLOYEEPLANNING_DATE, ' + NL +
      '  CASE ' + NL +
      '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
      '    ELSE ''D'' ' + NL +
      '  END FLAG, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_5, EMP.SCHEDULED_TIMEBLOCK_6, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_7, EMP.SCHEDULED_TIMEBLOCK_8, ' + NL +
      '  EMP.SCHEDULED_TIMEBLOCK_9, EMP.SCHEDULED_TIMEBLOCK_10 ' + NL;
  // 20013910 Query had to be changed for 'plan on departments', to prevent
  //          it finds nothing.
  SelectStr := SelectStr + ' ' +
    'FROM ' + NL +
    '    EMPLOYEEPLANNING EMP INNER JOIN EMPLOYEE E ON ' + NL +
    '      EMP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '    LEFT JOIN WORKSPOT W ON ' + NL +
    '      EMP.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '      EMP.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '    LEFT JOIN DEPARTMENTPERTEAM DT ON ' + NL +
    '      DT.PLANT_CODE = W.PLANT_CODE  AND ' + NL +
    '      DT.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' + NL +
    '    LEFT JOIN TEAM T ON ' + NL +
    '      DT.PLANT_CODE = T.PLANT_CODE AND ' + NL +
    '      DT.TEAM_CODE = T.TEAM_CODE ' + NL +
    'WHERE ' + NL +
    '  EMP.EMPLOYEEPLANNING_DATE >= :FDATESTART '+ NL +
    '  AND EMP.EMPLOYEEPLANNING_DATE <= :FDATEEND  '+ NL +
    '  AND EMP.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND EMP.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
    '  AND ((EMP.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
    '  AND EMP.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
    '  OR (EMP.WORKSPOT_CODE = ' + '''' + DummyStr + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''))' + NL +
    '  AND EMP.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + ' ' + NL +
    '  AND EMP.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + ' ' + NL;
  // GLOB3-60 Filter out not-planned-at-all lines
  SelectStr := SelectStr +
    '  AND ' + NL +
    '  (NOT ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_2 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_3 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_4 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_5 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_6 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_7 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_8 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_9 = ''N'' AND ' + NL +
    '     EMP.SCHEDULED_TIMEBLOCK_10 = ''N'') ' + NL +
    '   ) ' + NL;

  if not QRParameters.FAllTeam then
    SelectStr := SelectStr + ' AND E.TEAM_CODE >= ''' +
      DoubleQuote(QRParameters.FTeamFrom) + '''' + ' AND E.TEAM_CODE <= ''' +
      DoubleQuote(QRParameters.FTeamTo) + '''' + ' ' + NL;

  if QRParameters.FShowNPlannedWorkspots then
  begin
    SelectStr := SelectStr + ' ' +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  DISTINCT OCP.PLANT_CODE, OCP.SHIFT_NUMBER, ' + NL +
      '  CASE ' + NL +
      '    WHEN OCP.WORKSPOT_CODE <> ''0'' THEN OCP.WORKSPOT_CODE ' + NL +
      '    ELSE OCP.DEPARTMENT_CODE ' + NL +
      '  END WORKSPOT_CODE, ' + NL +
      '  OCP.DEPARTMENT_CODE, -1,' + NL +
      '  CAST('''' AS VARCHAR(40)), ' + NL +
      '  OCP.OCC_PLANNING_DATE,  ' + NL +
      '  CASE ' + NL +
      '    WHEN OCP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
      '    ELSE ''D'' ' + NL +
      '  END FLAG, ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)) ' + NL +
      'FROM ' + NL +
      '  OCCUPATIONPLANNING OCP LEFT JOIN WORKSPOT W ON ' + NL +
      '    OCP.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '    OCP.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      '  LEFT JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    OCP.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    OCP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL +
      '  LEFT JOIN TEAM T ON ' + NL +
      '    DT.PLANT_CODE = T.PLANT_CODE AND ' + NL +
      '    DT.TEAM_CODE = T.TEAM_CODE ' + NL +
      'WHERE ' + NL +
      '  OCC_PLANNING_DATE >= :FDATESTART ' + ' AND OCC_PLANNING_DATE <= :FDATEEND  '+ NL +
      '  AND OCP.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND OCP.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '  AND ((OCP.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
      '  AND OCP.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
      '  OR (OCP.WORKSPOT_CODE = ' + '''' + DummyStr + '''' + NL +
      '  AND OCP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
      '  AND OCP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''))' + NL +
      '  AND OCP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
      '  AND OCP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND OCP.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + ' ' + NL +
      '  AND OCP.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo  + ' ' + NL +
      '  AND ((OCP.OCC_A_TIMEBLOCK_1 <> 0) OR (OCP.OCC_A_TIMEBLOCK_2 <> 0 ) ' + NL +
      '  OR (OCP.OCC_A_TIMEBLOCK_3 <> 0) OR (OCP.OCC_A_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (OCP.OCC_A_TIMEBLOCK_5 <> 0) OR (OCP.OCC_A_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (OCP.OCC_A_TIMEBLOCK_7 <> 0) OR (OCP.OCC_A_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (OCP.OCC_A_TIMEBLOCK_9 <> 0) OR (OCP.OCC_A_TIMEBLOCK_10 <> 0) ' + NL +
      '  OR (OCP.OCC_B_TIMEBLOCK_1 <> 0) OR (OCP.OCC_B_TIMEBLOCK_2 <> 0) ' + NL +
      '  OR (OCP.OCC_B_TIMEBLOCK_3 <> 0) OR (OCP.OCC_B_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (OCP.OCC_B_TIMEBLOCK_5 <> 0) OR (OCP.OCC_B_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (OCP.OCC_B_TIMEBLOCK_7 <> 0) OR (OCP.OCC_B_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (OCP.OCC_B_TIMEBLOCK_9 <> 0) OR (OCP.OCC_B_TIMEBLOCK_10 <> 0) ' + NL +
      '  OR (OCP.OCC_C_TIMEBLOCK_1 <> 0) OR (OCP.OCC_C_TIMEBLOCK_2 <> 0) ' + NL +
      '  OR (OCP.OCC_C_TIMEBLOCK_3 <> 0) OR (OCP.OCC_C_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (OCP.OCC_C_TIMEBLOCK_5 <> 0) OR (OCP.OCC_C_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (OCP.OCC_C_TIMEBLOCK_7 <> 0) OR (OCP.OCC_C_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (OCP.OCC_C_TIMEBLOCK_9 <> 0) OR (OCP.OCC_C_TIMEBLOCK_10 <> 0))' + NL;
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr + ' AND T.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + ' AND T.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
//    MyDate := EncodeDate(2002,1,1);
    SelectStr := SelectStr + ' ' +
      'UNION ' + NL +
      'SELECT ' + NL +
      '  DISTINCT STO.PLANT_CODE, STO.SHIFT_NUMBER, ' + NL +
      '  CASE ' + NL +
      '    WHEN STO.WORKSPOT_CODE <> ''0'' THEN STO.WORKSPOT_CODE ' + NL +
      '    ELSE STO.DEPARTMENT_CODE ' + NL +
      '  END WORKSPOT_CODE, ' + NL +
      '  STO.DEPARTMENT_CODE, -2, ' + NL +
      '  CAST( '''' AS VARCHAR(40)), ' + NL +
      // Pims -> Oracle. Gives error 'invalid month'.
//      '  CAST ( '''+ DateToStr(MyDate) +  ''' AS TIMESTAMP) ' +
//      '  TO_DATE(''1-1-1900 12:00'',''DD-MM-YYYY HH24:MI'') ' + NL +
      '  SYSDATE, ' + NL +
      '  CASE ' + NL +
      '    WHEN STO.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
      '    ELSE ''D'' ' + NL +
      '  END FLAG, ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
      '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)) ' + NL +
      'FROM ' + NL +
      '  STANDARDOCCUPATION STO LEFT JOIN WORKSPOT W ON ' + NL +
      '    STO.PLANT_CODE = W.PLANT_CODE AND ' + NL +
      '    STO.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
      '  LEFT JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    STO.PLANT_CODE = DT.PLANT_CODE  AND ' + NL +
      '    STO.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL +
      '  LEFT JOIN TEAM T ON ' + NL +
      '    DT.PLANT_CODE = T.PLANT_CODE AND ' + NL +
      '    DT.TEAM_CODE = T.TEAM_CODE ' + NL +
      'WHERE ' + NL +
      '  STO.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
      '  AND STO.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND STO.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND STO.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '  AND ((STO.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
      '  AND STO.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
      '  OR (STO.WORKSPOT_CODE = ' + '''' + DummyStr + '''' + NL +
      '  AND STO.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
      '  AND STO.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''))' + NL +
      '  AND STO.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + ' ' + NL +
      '  AND STO.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo  + ' ' + NL +
      '  AND ((STO.OCC_A_TIMEBLOCK_1 <> 0) OR (STO.OCC_A_TIMEBLOCK_2 <> 0 )' + NL +
      '  OR (STO.OCC_A_TIMEBLOCK_3 <> 0) OR (STO.OCC_A_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (STO.OCC_A_TIMEBLOCK_5 <> 0) OR (STO.OCC_A_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (STO.OCC_A_TIMEBLOCK_7 <> 0) OR (STO.OCC_A_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (STO.OCC_A_TIMEBLOCK_9 <> 0) OR (STO.OCC_A_TIMEBLOCK_10 <> 0) ' + NL +
      '  OR (STO.OCC_B_TIMEBLOCK_1 <> 0) OR (STO.OCC_B_TIMEBLOCK_2 <> 0) ' + NL +
      '  OR (STO.OCC_B_TIMEBLOCK_3 <> 0) OR (STO.OCC_B_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (STO.OCC_B_TIMEBLOCK_5 <> 0) OR (STO.OCC_B_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (STO.OCC_B_TIMEBLOCK_7 <> 0) OR (STO.OCC_B_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (STO.OCC_B_TIMEBLOCK_9 <> 0) OR (STO.OCC_B_TIMEBLOCK_10 <> 0) ' + NL +
      '  OR (STO.OCC_C_TIMEBLOCK_1 <> 0) OR (STO.OCC_C_TIMEBLOCK_2 <> 0) ' + NL +
      '  OR (STO.OCC_C_TIMEBLOCK_3 <> 0) OR (STO.OCC_C_TIMEBLOCK_4 <> 0) ' + NL +
      '  OR (STO.OCC_C_TIMEBLOCK_5 <> 0) OR (STO.OCC_C_TIMEBLOCK_6 <> 0) ' + NL +
      '  OR (STO.OCC_C_TIMEBLOCK_7 <> 0) OR (STO.OCC_C_TIMEBLOCK_8 <> 0) ' + NL +
      '  OR (STO.OCC_C_TIMEBLOCK_9 <> 0) OR (STO.OCC_C_TIMEBLOCK_10 <> 0) )' + NL;
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr + ' AND T.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + ' AND T.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
    if FStartDay <= FEndDay then
      SelectStr := SelectStr +  ' AND STO.DAY_OF_WEEK >= ' + IntToStr(FStartDay) +
      ' AND STO.DAY_OF_WEEK <= ' + IntToStr(FEndDay) + NL
    else
      SelectStr := SelectStr + ' AND ' +
        ' ( (STO.DAY_OF_WEEK >= ' + IntToStr(FStartDay) + ' AND STO.DAY_OF_WEEK <= 7)' +
        ' OR (STO.DAY_OF_WEEK >= 1 AND STO.DAY_OF_WEEK <= ' + IntToStr(FEndDay) + '))' + NL;
  end;
  if QRParameters.FSortEMP and (QRParameters.FSortOnEmployeeNumber=1) then
    SelectStr := SelectStr +  ' ORDER BY 1, 2, 6, 4, 5, 3, 7' // 3=EmpNr 6=EmpDescr
  else
    SelectStr := SelectStr +  ' ORDER BY 1, 2, 3, 4, 5, 6, 7';


  with ReportStaffPlanDayDM do
  begin
    QueryEMP.Active := False;
    QueryEMP.UniDirectional := False;
    QueryEMP.SQL.Clear;
    // MRA:5-DEC-2018 PIM-412 Bugfix. No uppercase!
    QueryEMP.SQL.Add(SelectStr);
//    QueryEMP.SQL.Add(UpperCase(SelectStr));
// QueryEMP.SQL.SaveToFile('c:\temp\RepStaffPlanDay.txt');
    QueryEMP.ParamByName('FDATESTART').Value := GetDate(QRParameters.FDateFrom);
    QueryEMP.ParamByName('FDATEEND').Value :=  GetDate(QRParameters.FDateTo);
    if not QueryEMP.Prepared then
      QueryEMP.Prepare;
    QueryEMP.Active := True;
    if QueryEMP.IsEmpty then
      Result := False
    else
    begin
      Result := True;
    end;
    if Result then
      RequestEarlyLateOpen(GetDate(QRParameters.FDateFrom),
        GetDate(QRParameters.FDateTo));
  end;
  FEndDate := GetDate(QRParameters.FDateTo);
  FStartDate := GetDate(QRParameters.FDateFrom);
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);

  FPaidBreaks := QRParameters.FPaidBreaks;
  FShowOnlyDiff := QRParameters.FShowOnlyDiff;
  FSortEMP := QRParameters.FSORTEMP;
  if NOT FSortEMP then
    QRGroupHDWK.Expression := 'WORKSPOT_CODE'
  else
    QRGroupHDWK.Expression := '';

  // 20013169
  qrptBase.Page.Orientation := poLandscape;

{ check if report is empty}
  Screen.Cursor := crDefault;
end;

procedure TReportStaffPlanDayQR.ConfigReport;
begin
  // MR:23-09-2003
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelShiftFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end
  else
  begin
    QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
    QRLabelShiftTo.Caption := QRParameters.FShiftTo;
    QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
    QRLabelDeptTo.Caption := QRParameters.FDeptTo;
    QRLabelWKFrom.Caption := QRParameters.FWKFrom;
    QRLabelWKTo.Caption := QRParameters.FWKTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  if QRParameters.FShowStartEndTimes then
  begin
    QRLabel35.Caption := TotalCaption;
    QRLabel36.Caption := TotalCaption;
  end
  else
  begin
    QRLabel35.Caption := '';
    QRLabel36.Caption := '';
  end;
end;

procedure TReportStaffPlanDayQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportStaffPlanDayQR.FormCreate(Sender: TObject);
var
  TBCaption: String;
  I: Integer;
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  TotalCaption := QRLabel36.Caption;
  // GLOB3-60 Some texts must be filled here.
  TBCaption := Copy(QRLabel242.Caption, 1, Length(QRLabel242.Caption)-1);
  // Workspot-level
  QRLabel14.Caption := TBCaption + '5';
  QRLabel19.Caption := TBCaption + '6';
  QRLabel21.Caption := TBCaption + '7';
  QRLabel25.Caption := TBCaption + '8';
  QRLabel26.Caption := TBCaption + '9';
  QRLabel27.Caption := TBCaption + '10';
  // Emp-level
  QRLabel15.Caption := TBCaption + '5';
  QRLabel28.Caption := TBCaption + '6';
  QRLabel29.Caption := TBCaption + '7';
  QRLabel30.Caption := TBCaption + '8';
  QRLabel31.Caption := TBCaption + '9';
  QRLabel32.Caption := TBCaption + '10';
  // GLOB3-225
  for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
  begin
    case I of
     5: begin QRLabel14.Caption := ''; QRLabel15.Caption := ''; end;
     6: begin QRLabel19.Caption := ''; QRLabel28.Caption := ''; end;
     7: begin QRLabel21.Caption := ''; QRLabel29.Caption := ''; end;
     8: begin QRLabel25.Caption := ''; QRLabel30.Caption := ''; end;
     9: begin QRLabel26.Caption := ''; QRLabel31.Caption := ''; end;
    10: begin QRLabel27.Caption := ''; QRLabel32.Caption := ''; end;
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(
        QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(
        QRLabel47.Caption + ' ' + QRLabelDepartment.Caption + ' ' +
        QRLabelDeptFrom.Caption + ' ' + QRLabel50.Caption + ' ' +
        QRLabelDeptTo.Caption);
      ExportClass.AddText(
        QRLabel87.Caption + ' ' + QRLabel88.Caption + ' ' +
        QRLabelTeamFrom.Caption + ' ' + QRLabel90.Caption + ' ' +
        QRLabelTeamTo.Caption);
      ExportClass.AddText(
        QRLabel18.Caption + ' ' + QRLabel20.Caption + ' ' +
        QRLabelWKFrom.Caption + ' ' + QRLabel22.Caption + ' ' +
        QRLabelWKTo.Caption);
      ExportClass.AddText(
        QRLabel108.Caption + ' ' + QRLabel109.Caption + ' ' +
        QRLabelShiftFrom.Caption + ' ' + QRLabel2.Caption + ' ' +
        QRLabelShiftTo.Caption);
      ExportClass.AddText(
        QRLabel111.Caption + ' ' + QRLabelDateFrom.Caption);
    end;
  end;
end;

procedure TReportStaffPlanDayQR.InitializeDayPlanning
 (var DayPlanning: TTBWeekArray);
var
  i: Integer;
begin
  for i := 1 to MAX_TBS do
    DayPlanning[i] := 0;
  for i := 1 to MAX_TBS do
  begin
    FEmpPlanRecord[i].ADate := 0;
    FEmpPlanRecord[i].AStart := 0;
    FEmpPlanRecord[i].AEnd := 0;
    FEmpPlanRecord[i].AWorkspotCode := '';
    FEmpPlanRecord[i].AWDescription := '';
  end;
end;

procedure TReportStaffPlanDayQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 100) and (Index <= 106) then
      begin
        (TempComponent as TQRLabel).Caption := FDate[Index - 100 + 1];
      end;
      if (Index >= 10) and (Index <= 16) then
      begin
        (TempComponent as TQRLabel).Caption := FDayDesc[Index - 10 + 1];
      end;
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  FPlantTotal := 0;
  QRLabelDate.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDay.Caption :=
    SystemDM.GetDayWDescription(
      ListProcsF.PimsDayOfWeek(QRParameters.FDateFrom));
  with ReportStaffPlanDayDM do
  begin
    if ReportStaffPlanDayDM.TablePlant.FindKey([
      QueryEMP.FieldByName('PLANT_CODE').AsString]) then
    begin
      QRLabelPlantDesc.Caption := TablePlant.FieldByName('DESCRIPTION').AsString;
      QRLabelPlantDescFooter.Caption := TablePlant.FieldByName('DESCRIPTION').AsString;
    end;
  end;
  inherited;
end;

procedure TReportStaffPlanDayQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Empl, Shift: Integer;
  Plant, Dept: String;
begin
  inherited;
  PrintBand := False; // 20013169
  QRGroupHDEmpl.Height := 0;

  QRGroupHDEmpl.ForceNewPage := QRParameters.FPageWK;
//  PrintBand := FSortEmp;
  InitializeDayPlanning(FEmpPlanning);
  QRMemoEmpTB1.Lines.Clear;
  QRMemoEmpTB2.Lines.Clear;
  QRMemoEmpTB3.Lines.Clear;
  QRMemoEmpTB4.Lines.Clear;
  QRMemoEmpTB5.Lines.Clear;
  QRMemoEmpTotal.Lines.Clear;

  QRLabelEmpl.Caption :=
    ReportStaffPlanDayDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString +
    ' ' +
    ReportStaffPlanDayDM.QueryEMP.FieldByName('DESCRIPTION').AsString;

  Empl := ReportStaffPlanDayDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Shift := ReportStaffPlanDayDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
  Plant := ReportStaffPlanDayDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanDayDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
  if Empl > 0 then
  begin
    FTotalLines := 0;
{    if QRParameters.FSortEMP then
      FTotalLines := 0
    else
      FTotalLines := FTotalLines + 1; }
    ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept,  True{FPaidBreaks});
  end;
end;

procedure TReportStaffPlanDayQR.QRDBTextDeptDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanDayDM do
  begin
    Value := '';
    if TableDept.FindKey([QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('DEPARTMENT_CODE').AsString]) then
      Value := TableDept.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanDayQR.QRDBTextShiftDescPrint(sender: TObject;
  var Value: String);
var
  Plant: String;
begin
  inherited;
  with ReportStaffPlanDayDM do
  begin
    Value := '';
    if TableShift.FindKey([Plant, QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]) then
      Value := TableShift.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  WKCode, Plant, Dept, TeamFrom, TeamTo: String;
  Flag: String;
begin
  QRGroupHDWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
  PrintBand := not FSortEmp;
  if Not PrintBand then
    Exit;
  FTotalLines := 0;
{  if QRParameters.FSortEMP then
    FTotalLines := FTotalLines + 1
  else
    FTotalLines := 0; }
  QRMemoWKOnTB1.Lines.Clear;
  QRMemoWKOnTB2.Lines.Clear;
  QRMemoWKOnTB3.Lines.Clear;
  QRMemoWKOnTB4.Lines.Clear;
  QRMemoWKOnTB5.Lines.Clear;
  QRMemoWKTotal.Lines.Clear;
  TeamFrom := QRParameters.FTeamFrom;
  TeamTo := QRParameters.FTeamTo;

  Plant := ReportStaffPlanDayDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanDayDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
  WKCode := ReportStaffPlanDayDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString;
  // 20013910
  Flag := ReportStaffPlanDayDM.QueryEMP.FieldByName('FLAG').AsString;
  FWKPrint := True;
  if not QRParameters.FAllTeam then
    if not ReportStaffPlanDayDM.ISNotDeptValid(Dept,Plant, TeamFrom, TeamTo) then
    begin
      PrintBand := False;
      FWKPrint := False;
      Exit;
    end;

  FWKPrint := True;
  PrintBand := FWKPrint;
  QRLabelWKCode.Caption := '';
  ReportStaffPlanDayDM.TableDept.FindKey([Plant, Dept]);
  // 20013910 Use Flag to determine if it is about a workspot or a department
  if Flag = 'W' then
  begin
    if ReportStaffPlanDayDM.TableWK.FindKey([Plant, WKCode]) then
      QRLabelWKCode.Caption := WKCode + ' ' +
        ReportStaffPlanDayDM.TableWK.FieldByName('DESCRIPTION').AsString + ' (' +
        Dept + ' ' +
        ReportStaffPlanDayDM.TableDept.FieldByName('DESCRIPTION').AsString + ')';
  end
  else
  begin
    QRLabelWKCode.Caption :=
      ReportStaffPlanDayDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
    if ReportStaffPlanDayDM.TableDept.FindKey([Plant, Dept]) then
      QRLabelWKCode.Caption := QRLabelWKCode.Caption + ' ' +
       ReportStaffPlanDayDM.TableDept.FieldByName('DESCRIPTION').AsString;
  end;

end;

procedure TReportStaffPlanDayQR.QRBandEmpOnTimeblockFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FSortEmp;
end;

procedure TReportStaffPlanDayQR.QRGroupHDShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRShape3.Width := QRGroupHDShift.Width;
  QRGroupHdShift.ForceNewPage := QRParameters.FPageShift;
  FShiftTotal := 0;
  with ReportStaffPlanDayDM do
  begin
    FMaxTB := ShiftTBCount(QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger);
    if TableShift.FindKey([QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]) then
    begin
      QRLabelShiftDesc.Caption := TableShift.FieldByName('DESCRIPTION').AsString;
      QRLabelShiftDescFooter.Caption := TableShift.FieldByName('DESCRIPTION').AsString;
    end;
  end;
  inherited;
end;

procedure TReportStaffPlanDayQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  I: Integer;
  StrTmp, Plant, Dept, WKCode: String;
  FTBPlanned: Array[1..MAX_TBS] of Integer;
  Schedule_TB: TTBSchedule;
  Flag: String;
  Contents: String;
  DateEmp: TDateTime;
  DayEmp: Integer;
  IFirst, ILast: Integer;
  OldStart, OldEnd: TDateTime;
  FEmpPlanningTotal: Integer;
  function TimeDiffMinutes(AValue1, AValue2: TDatetime): Integer;
  begin
    Result := ATBLengthClass.GetMinutes(AValue1) - ATBLengthClass.GetMinutes(AValue2);
  end;
  procedure ShowStartEnd(AQRMemo: TQRMemo; Contents: String; ATimeblock: Integer);
  var
    Content: String;
  begin
    Content := '';
    if QRParameters.FShowStartEndTimes then
        Content :=
          DateTime2StringTime(FEmpPlanRecord[ATimeblock].AStart) + '-' +
            DateTime2StringTime(FEmpPlanRecord[ATimeblock].AEnd);
    AQRMemo.Lines.Add(Contents);
    if Content <> '' then
    begin
      AQRMemo.Lines.Add(Content);
    end;
  end; // ShowStartEnd
  function MaxMemoEmp: Integer;
  var
    AQRMemo: TQRMemo;
    I: Integer;
  begin
    Result := 1;
    for I := 1 to 5 do
    begin
      AQRMemo := (FindComponent('QRMemoEmpTB' + IntToStr(I)) as TQRMemo);
      if Assigned(AQRMemo) then
        if AQRMemo.Lines.Count > Result then
          Result := AQRMemo.Lines.Count;
    end;
  end; // MaxMemoEmp
  function MaxMemoWK: Integer;
  var
    AQRMemo: TQRMemo;
    I: Integer;
  begin
    Result := 1;
    for I := 1 to 5 do
    begin
      AQRMemo := (FindComponent('QRMemoWKOnTB' + IntToStr(I)) as TQRMemo);
      if Assigned(AQRMemo) then
        if AQRMemo.Lines.Count > Result then
          Result := AQRMemo.Lines.Count;
    end;
  end; // MaxMemoWK
begin
  inherited;
  PrintBand := False;
  with ReportStaffPlanDayDM.QueryEMP do
  begin
    if FieldByName('EMPLOYEE_NUMBER').AsInteger < 0 then
      Exit;

    for I := 1 to SystemDM.MaxTimeblocks do
      SCHEDULE_TB[I] := FieldByName('SCHEDULED_TIMEBLOCK_'+IntToStr(I)).AsString;
    for I := 1 to SystemDM.MaxTimeblocks do
    begin
      if ((SCHEDULE_TB[I] = 'A') or (SCHEDULE_TB[I] = 'B') or
        (SCHEDULE_TB[I] = 'C')) then
        FTBPlanned[I] := I
      else
        FTBPlanned[I] := 0;
    end;
  end;

  // PIM-377
  DateEmp := FMinDate;
  DayEMP := ListProcsF.DayWStartOnFromDate(DateEmp);
  for i := 1 to SystemDM.MaxTimeblocks do
  begin
    if  ATBLengthClass.ExistTB(I) and (FTBPlanned[i] <> 0) then
    begin
      FEmpPlanRecord[i].ADate := DateEmp;
      FEmpPlanRecord[i].AStart :=
        ATBLengthClass.GetStartTime(i, DayEMP);
      FEmpPlanRecord[i].AEnd :=
        ATBLengthClass.GetEndTime(i, DayEMP);
      if FEmpPlanRecord[i].AStart <> 0 then
      begin
        FEmpPlanRecord[i].AWorkspotCode :=
          ReportStaffPlanDayDM.QueryEMP.
            FieldByName('WORKSPOT_CODE').AsString;
        FEmpPlanRecord[i].AWDescription := '';
{        FEmpPlanRecord[IndexDay][i].AWDescription :=
          ReportStaffPlanDayDM.QueryEMP.
            FieldByName('WDESCRIPTION').AsString; }
      end;
      FEmpPlanning[i] := FEmpPlanning[i] +
        ATBLengthClass.GetLengthTB(I, DayEMP);
    end;
  end; // for
  if QRParameters.FShowStartEndTimes then
  begin
    // Early late: Only do this for first filled-in timeblock and
    // last filled-in timeblock!
    // Determine first and last filled-in timeblock
    IFirst := 0;
    ILast := 0;
    for i := 1 to SystemDM.MaxTimeblocks do
    begin
      if FEmpPlanRecord[i].AStart <> 0 then
      begin
        IFirst := i;
        Break;
      end;
    end; // for
    for i := SystemDM.MaxTimeblocks downto 1 do
    begin
      if FEmpPlanRecord[i].AStart <> 0 then
      begin
        ILast := i;
        Break;
      end;
    end; // for
    if IFirst <> 0 then
    begin
      i := IFirst;
      OldStart := FEmpPlanRecord[i].AStart;
      FEmpPlanRecord[i].AStart :=
        ReportStaffPlanDayDM.RequestEarlyLateFindEarlyTime(DateEmp,
          ReportStaffPlanDayDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            ATBLengthClass.GetStartTime(i, DayEMP)
            );
        if OldStart <> FEmpPlanRecord[i].AStart then
          FEmpPlanning[i] := FEmpPlanning[i] +
            TimeDiffMinutes(OldStart, FEmpPlanRecord[i].AStart);
    end; // if
    if ILast <> 0 then
    begin
      i := ILast;
      OldEnd := FEmpPlanRecord[i].AEnd;
      FEmpPlanRecord[i].AEnd :=
        ReportStaffPlanDayDM.RequestEarlyLateFindLateTime(DateEmp,
          ReportStaffPlanDayDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            ATBLengthClass.GetEndTime(i, DayEMP)
            );
      if OldEnd <> FEmpPlanRecord[i].AEnd then
        FEmpPlanning[i] := FEmpPlanning[i] +
          TimeDiffMinutes(FEmpPlanRecord[i].AEnd, OldEnd);
    end; // if
  end; // if
  FEmpPlanningTotal := 0;
  for i := 1 to SystemDM.MaxTimeblocks do
    FEmpPlanningTotal := FEmpPlanningTotal +
      FEmpPlanning[i];
  FPlantTotal := FPlantTotal + FEmpPlanningTotal;
  FShiftTotal := FShiftTotal + FEmpPlanningTotal;

  if not FSortEmp then
  begin
    StrTmp := ReportStaffPlanDayDM.QueryEMP.FieldByName('DESCRIPTION').AsString;
    for I := 1 to MaxTB do
    begin
      if FTBPlanned[I] <> 0  then
        Contents := StrTmp
      else
        if I <= FMaxTB then
          Contents := '-'
        else
          Contents := '';
      case I of
        1:
          begin
            ShowStartEnd(QRMemoWKonTB1, Contents, I);
            inc(FTotalLines);
          end;
        2: ShowStartEnd(QRMemoWKonTB2, Contents, I);
        3: ShowStartEnd(QRMemoWKonTB3, Contents, I);
        4: ShowStartEnd(QRMemoWKonTB4, Contents, I);
        5: ShowStartEnd(QRMemoWKonTB5, Contents, I);
        6:
          begin
            ShowStartEnd(QRMemoWKonTB1, Contents, I);
            FTotalLines := FTotalLines + 2;
          end;
        7: ShowStartEnd(QRMemoWKonTB2, Contents, I);
        8: ShowStartEnd(QRMemoWKonTB3, Contents, I);
        9: ShowStartEnd(QRMemoWKonTB4, Contents, I);
        10: ShowStartEnd(QRMemoWKonTB5, Contents, I);
      end; // case
    end; // for
    if QRParameters.FShowStartEndTimes then
    begin
      for I := 1 to FTotalLines do
        QRMemoWKTotal.Lines.Add('');

      QRMemoWKTotal.Lines.Add(MyDecodeHrsMin(FEmpPlanningTotal));
    end;
  end; // if
  if FSortEmp then
  begin
    StrTmp := '';
    Plant := ReportStaffPlanDayDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
    Dept := ReportStaffPlanDayDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
    WKCode := ReportStaffPlanDayDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString;
    // 20013910
    Flag := ReportStaffPlanDayDM.QueryEMP.FieldByName('FLAG').AsString;
    if Flag = 'W' then
    begin
      if ReportStaffPlanDayDM.TableWK.FindKey([Plant, WKCode]) then
        StrTmp := WKCode + ' ' +
          ReportStaffPlanDayDM.TableWK.FieldByName('DESCRIPTION').AsString ;
    end
    else
    begin
      ReportStaffPlanDayDM.TableDept.FindKey([Plant, Dept]);
      StrTmp :=
        ReportStaffPlanDayDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
      if ReportStaffPlanDayDM.TableDept.FindKey([Plant, Dept]) then
        StrTmp := StrTmp + ' ' +
         ReportStaffPlanDayDM.TableDept.FieldByName('DESCRIPTION').AsString;
    end;
    for I := 1 to MaxTB do
    begin
      if FTBPlanned[I] <> 0 then
        Contents := StrTmp
      else
        if I <= FMaxTB then
          Contents := '-'
        else
          Contents := '';
      case I of
        1:
          begin
            ShowStartEnd(QRMemoEmpTB1, Contents, I);
            inc(FTotalLines);
          end;
        2: ShowStartEnd(QRMemoEmpTB2, Contents, I);
        3: ShowStartEnd(QRMemoEmpTB3, Contents, I);
        4: ShowStartEnd(QRMemoEmpTB4, Contents, I);
        5: ShowStartEnd(QRMemoEmpTB5, Contents, I);
        6:
          begin
            ShowStartEnd(QRMemoEmpTB1, Contents, I);
            FTotalLines := FTotalLines + 2;
          end;
        7: ShowStartEnd(QRMemoEmpTB2, Contents, I);
        8: ShowStartEnd(QRMemoEmpTB3, Contents, I);
        9: ShowStartEnd(QRMemoEmpTB4, Contents, I);
        10: ShowStartEnd(QRMemoEmpTB5, Contents, I);
      end; // case
    end; // for
    if QRParameters.FShowStartEndTimes then
    begin
      for I := 1 to FTotalLines do
        QRMemoEmpTotal.Lines.Add('');
      QRMemoEmpTotal.Lines.Add(MyDecodeHrsMin(FEmpPlanningTotal));
    end;
  end; // if
end; // QRBandDetailBeforePrint

procedure TReportStaffPlanDayQR.QRBandDateFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportStaffPlanDayQR.QRBandWKOnTimeblockFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := (not FSortEmp) and (FWKPrint);
end;

procedure TReportStaffPlanDayQR.QRBandHeaderWorkspotBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := Not FSortEmp;
end;

procedure TReportStaffPlanDayQR.QRBandHeaderEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FSortEmp;
end;

procedure TReportStaffPlanDayQR.ChildBandEmpHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FSortEmp;
end;

procedure TReportStaffPlanDayQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportStaffPlanDayQR.QRBandHeaderWorkspotAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel241.Caption);
    ExportClass.AddText(
      ExportClass.Sep +
      QRLabel242.Caption + ExportClass.Sep +
      QRLabel243.Caption + ExportClass.Sep +
      QRLabel244.Caption + ExportClass.Sep +
      QRLabel245.Caption + ExportClass.Sep +
      QRLabel14.Caption + ExportClass.Sep +
      QRLabel36.Caption
    );
    ExportClass.AddText(
      ExportClass.Sep +
      QRLabel19.Caption + ExportClass.Sep +
      QRLabel21.Caption + ExportClass.Sep +
      QRLabel25.Caption + ExportClass.Sep +
      QRLabel26.Caption + ExportClass.Sep +
      QRLabel27.Caption + ExportClass.Sep
    );
  end;
end;

procedure TReportStaffPlanDayQR.ChildBandEmpHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabel3.Caption);
    ExportClass.AddText(
      ExportClass.Sep +
      QRLabel4.Caption + ExportClass.Sep +
      QRLabel5.Caption + ExportClass.Sep +
      QRLabel6.Caption + ExportClass.Sep +
      QRLabel7.Caption + ExportClass.Sep +
      QRLabel15.Caption + ExportClass.Sep +
      QRLabel35.Caption
    );
    ExportClass.AddText(
      ExportClass.Sep +
      QRLabel28.Caption + ExportClass.Sep +
      QRLabel29.Caption + ExportClass.Sep +
      QRLabel30.Caption + ExportClass.Sep +
      QRLabel31.Caption + ExportClass.Sep +
      QRLabel32.Caption + ExportClass.Sep
    );
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffPlanDayDM do
    begin
      ExportClass.AddText(QRLabel16.Caption +
        QRLabel17.Caption +
        QueryEMP.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabelPlantDesc.Caption);
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHDShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffPlanDayDM do
    begin
      ExportClass.AddText(QRLabel23.Caption +
        QRLabel24.Caption +
        QueryEMP.FieldByName('SHIFT_NUMBER').AsString + ExportClass.Sep +
        QRLabelShiftDesc.Caption);
      // 20013169
      ExportClass.AddText(QRLabel8.Caption +
        QRlabel9.Caption +
        QRlabelDate.Caption + ExportClass.Sep +
        QRLabel10.Caption +
        QRLabel12.Caption + 
        QRLabelDay.Caption);
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHDWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabelWKCode.Caption);
  end;
end;

procedure TReportStaffPlanDayQR.QRGroupHDEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabelEmpl.Caption);
  end;
end;

procedure TReportStaffPlanDayQR.QRBandEmpOnTimeblockFooterAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
  I: Integer;
  MaxI: Integer;
  LastEmp: String;
begin
  inherited;
  LastEmp := '';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // First determine the highest count
    MaxI := QRMemoEmpTB1.Lines.Count;
    if QRMemoEmpTB2.Lines.Count > MaxI then
      MaxI := QRMemoEmpTB2.Lines.Count;
    if QRMemoEmpTB3.Lines.Count > MaxI then
      MaxI := QRMemoEmpTB3.Lines.Count;
    if QRMemoEmpTB4.Lines.Count > MaxI then
      MaxI := QRMemoEmpTB4.Lines.Count;
    if QRMemoEmpTB5.Lines.Count > MaxI then
      MaxI := QRMemoEmpTB5.Lines.Count;
    for I := 0 to MaxI - 1 do
    begin
      if (LastEmp <> QRLabelEmpl.Caption) then
        Line := QRLabelEmpl.Caption + ExportClass.Sep
      else
        Line := ExportClass.Sep;
      if I <= QRMemoEmpTB1.Lines.Count - 1 then
        Line := Line + QRMemoEmpTB1.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoEmpTB2.Lines.Count - 1 then
        Line := Line + QRMemoEmpTB2.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoEmpTB3.Lines.Count - 1 then
        Line := Line + QRMemoEmpTB3.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoEmpTB4.Lines.Count - 1 then
        Line := Line + QRMemoEmpTB4.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoEmpTB5.Lines.Count - 1 then
        Line := Line + QRMemoEmpTB5.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoEmpTotal.Lines.Count - 1 then
        Line := Line + QRMemoEmpTotal.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      ExportClass.AddText(Line);
      LastEmp := QRLabelEmpl.Caption;
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRBandWKOnTimeblockFooterAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
  I: Integer;
  MaxI: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // First determine the highest count
    MaxI := QRMemoWKonTB1.Lines.Count;
    if QRMemoWKOnTB2.Lines.Count > MaxI then
      MaxI := QRMemoWKOnTB2.Lines.Count;
    if QRMemoWKOnTB3.Lines.Count > MaxI then
      MaxI := QRMemoWKOnTB3.Lines.Count;
    if QRMemoWKOnTB4.Lines.Count > MaxI then
      MaxI := QRMemoWKOnTB4.Lines.Count;
    if QRMemoWKOnTB5.Lines.Count > MaxI then
      MaxI := QRMemoWKOnTB5.Lines.Count;
    for I := 0 to MaxI - 1 do
    begin
      Line := ExportClass.Sep;
      if I <= QRMemoWKOnTB1.Lines.Count - 1 then
        Line := Line + QRMemoWKOnTB1.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoWKOnTB2.Lines.Count - 1 then
        Line := Line + QRMemoWKOnTB2.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoWKOnTB3.Lines.Count - 1 then
        Line := Line + QRMemoWKOnTB3.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoWKOnTB4.Lines.Count - 1 then
        Line := Line + QRMemoWKOnTB4.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoWKOnTB5.Lines.Count - 1 then
        Line := Line + QRMemoWKOnTB5.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      if I <= QRMemoWKTotal.Lines.Count - 1 then
        Line := Line + QRMemoWKTotal.Lines.Strings[I];
      Line := Line + ExportClass.Sep;
      ExportClass.AddText(Line);
    end;
  end;
end;

procedure TReportStaffPlanDayQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  QRBandSummary.Height := 0;
end;

function TReportStaffPlanDayQR.MaxTB: Integer;
begin
  if FMaxTB > (MIN_TBS+1) then
    Result := SystemDM.MaxTimeblocks
  else
    Result := (MIN_TBS+1);
  if Result > SystemDM.MaxTimeblocks then
    Result := SystemDM.MaxTimeblocks;
end; // MaxTB

procedure TReportStaffPlanDayQR.QRBndBasePageFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  Self.QRShapeBaseFooterLine.Width := QRBndBasePageFooter.Width;
end;

function TReportStaffPlanDayQR.MyDecodeHrsMin(HrsMin: Integer): String;
begin
  if HrsMin = 0 then
    Result := '-'
  else
    Result := DecodeHrsMin(HrsMin);
end;

procedure TReportStaffPlanDayQR.QRBandFooterShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowStartEndTimes;
  Self.QRLabelTotalShift.Caption := MyDecodeHrsMin(FShiftTotal);
end;

procedure TReportStaffPlanDayQR.QRBandFooterShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    Line := QRLabel38.Caption + QRLabel37.Caption +
      ReportStaffPlanDayDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsString + ExportClass.Sep +
      QRLabelShiftDescFooter.Caption +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabelTotalShift.Caption;
    ExportClass.AddText(Line);
  end;
end;

procedure TReportStaffPlanDayQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowStartEndTimes;
  Self.QRLabelTotalPlant.Caption := MyDecodeHrsMin(FPlantTotal);
end;

procedure TReportStaffPlanDayQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Line: String;  
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    Line := QRLabel39.Caption + QRLabel40.Caption +
      ReportStaffPlanDayDM.QueryEMP.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
      QRLabelPlantDescFooter.Caption +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabelTotalPlant.Caption;
    ExportClass.AddText(Line);
  end;
end;

procedure TReportStaffPlanDayQR.QRBndBasePageHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  Self.QRShapeBaseHeaderLine1.Width := QRBndBasePageHeader.Width;
  Self.QRShapeBaseHeaderLine2.Width := QRBndBasePageHeader.Width;
end;

end.
