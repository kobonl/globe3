(*
  Changes:
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  MRA:10-NOV-2010. RV079.2.
  - When adding records, then first record for Monday is ok.
    But when adding additional records, it fills TB4, when it should
    be empty.
  MRA:18-JAN-2013. 20013910.
  - Plan on departments gives problems.
  MRA:15-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)
unit StandardOccupationDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, Dblup1a, Provider, DBClient;

const
  SPimsPasteConfirmOverwrite = 'Overwrite existing rows?';

type
  TStandardOccupationDM = class(TGridBaseDM)
    TableDetailPLANT_CODE: TStringField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailWORKSPOT_CODE: TStringField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    TableDetailDAY_OF_WEEK: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_1: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_2: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_3: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_4: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_1: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_2: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_3: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_4: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_1: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_2: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_3: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_4: TIntegerField;
    TableDetailFIXED_YN: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailDAYDESCRIPTIONLU: TStringField;
    DataSourcePlantShift: TDataSource;
    DataSourceShift: TDataSource;
    QueryPlantShift: TQuery;
    QueryDepartmentMASTER: TQuery;
    DataSourceDepartmentMASTER: TDataSource;
    QueryTimeBlockPerDepartment: TQuery;
    QueryTimeBlockPerShift: TQuery;
    QueryDepartmentMASTERPLANT_CODE: TStringField;
    QueryDepartmentMASTERDEPARTMENT_CODE: TStringField;
    QueryDepartmentMASTERDEPARTMENTDESCRIPTION: TStringField;
    QueryDepartmentMASTERWORKSPOT_CODE: TStringField;
    QueryDepartmentMASTERWORKSPOTDESCRIPTION: TStringField;
    QueryDepartmentMASTERPLAN_ON_WORKSPOT_YN: TStringField;
    QueryStandardOccupationFROM: TQuery;
    QueryStandardOccupationFROMDAY_OF_WEEK: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_1: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_2: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_3: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_4: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_1: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_2: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_3: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_4: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_1: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_2: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_3: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_4: TIntegerField;
    QueryStandardOccupationFROMFIXED_YN: TStringField;
    QueryShift: TQuery;
    TableDetailDAYNR: TIntegerField;
    TableDetailDAYDESC: TStringField;
    TableDetailPLANTLU: TStringField;
    QueryShiftPLANT_CODE: TStringField;
    QueryShiftSHIFT_NUMBER: TIntegerField;
    QueryShiftDESCRIPTION: TStringField;
    TableDetailSHIFTLU: TStringField;
    QuerySelectDeptPerTeam: TQuery;
    ClientDataSetExportDept: TClientDataSet;
    DataSetProviderExportDept: TDataSetProvider;
    TableDetailOCC_A_TIMEBLOCK_5: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_6: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_7: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_8: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_9: TIntegerField;
    TableDetailOCC_A_TIMEBLOCK_10: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_5: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_6: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_7: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_8: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_9: TIntegerField;
    TableDetailOCC_B_TIMEBLOCK_10: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_5: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_6: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_7: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_8: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_9: TIntegerField;
    TableDetailOCC_C_TIMEBLOCK_10: TIntegerField;
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantSTDOCC_A_YN: TStringField;
    TablePlantSTDOCC_B_YN: TStringField;
    TablePlantSTDOCC_C_YN: TStringField;
    DataSourcePlant: TDataSource;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TablePlantAVERAGE_WAGE: TFloatField;
    TablePlantCUSTOMER_NUMBER: TIntegerField;
    TablePlantCOUNTRY_ID: TFloatField;
    TablePlantTIMEZONE: TMemoField;
    TablePlantTIMEZONEHRSDIFF: TSmallintField;
    TablePlantCUTOFFTIMESHIFTDATE: TDateTimeField;
    TablePlantREALTIMEINTERVAL: TFloatField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_5: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_6: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_7: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_8: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_9: TIntegerField;
    QueryStandardOccupationFROMOCC_A_TIMEBLOCK_10: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_5: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_6: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_7: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_8: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_9: TIntegerField;
    QueryStandardOccupationFROMOCC_B_TIMEBLOCK_10: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_5: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_6: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_7: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_8: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_9: TIntegerField;
    QueryStandardOccupationFROMOCC_C_TIMEBLOCK_10: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailBeforeInsert(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure TableExportCalcFields(DataSet: TDataSet);
    procedure QueryPlantShiftFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    // For storing Copy-From Values
    FCopyFromSHIFT_NUMBER: Integer;
    FCopyFromPLAN_ON_WORKSPOT_YN: String;
    FCopyFromPLANT_CODE: String;
    FCopyFromDEPARTMENT_CODE: String;
    FCopyFromWORKSPOT_CODE: String;
    // For storing current ShiftNumber
    FShiftNumber: Integer;
    // For storing Last-Detail-Record-values
    FDay, FTB1A, FTB2A, FTB3A, FTB4A, FTB1B, FTB2B, FTB3B, FTB4B, FTB1C,
    FTB2C, FTB3C, FTB4C: Integer;
    FTB5A, FTB6A, FTB7A, FTB8A, FTB9A, FTB10A, FTB5B, FTB6B, FTB7B, FTB8B,
    FTB9B, FTB10B, FTB5C, FTB6C, FTB7C, FTB8C, FTB9C, FTB10C: Integer;
    FFixed: String;
    // Paste or No-Paste?
    FPasteAction: Boolean;
    function GetShiftNumber: Integer;
    procedure SetShiftNumber(const Value: Integer);
    procedure FillTimeBlock;
    procedure SetPasteAction(const Value: Boolean);
    function CopyFromValuesExists: Boolean;
    function SourceTargetEqual: Boolean;
    { Private declarations }
  public
    { Public declarations }
    procedure FilterRecordExport(DataSet: TDataSet; var Accept: Boolean);
    procedure SetTimeBlock(Day, TB1A, TB2A, TB3A, TB4A, TB5A, TB6A, TB7A, TB8A,
      TB9A, TB10A, TB1B, TB2B, TB3B, TB4B, TB5B, TB6B, TB7B, TB8B, TB9B, TB10B,
      TB1C, TB2C, TB3C, TB4C, TB5C, TB6C, TB7C, TB8C, TB9C, TB10C: Integer;
      Fixed: String);
    procedure SetCopyFromValues(
      ACopyFromSHIFT_NUMBER: Integer;
      ACopyFromPLAN_ON_WORKSPOT_YN, ACopyFromPLANT_CODE,
      ACopyFromDEPARTMENT_CODE, ACopyFromWORKSPOT_CODE: String);
    procedure PasteExecute;
    
    property ShiftNumber: Integer read GetShiftNumber write SetShiftNumber;
    property PasteAction: Boolean write SetPasteAction;
  end;

var
  StandardOccupationDM: TStandardOccupationDM;

implementation

{$R *.DFM}

uses
  SystemDMT, StandardOccupationFRM, UPimsConst;

procedure TStandardOccupationDM.DataModuleCreate(Sender: TObject);
var
  SelectStr: String;
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := StandardOccupationDM.DataModuleDestroy;
  //Car 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryDepartmentMASTER.Sql.Clear;

    // 20013910 Use '0' as workspot code for plan on departments!
    SelectStr :=
      'SELECT ' +
      '  D.PLANT_CODE, D.DEPARTMENT_CODE, ' +
      '  D.DESCRIPTION AS DEPARTMENTDESCRIPTION, ' +
//      '  D.DEPARTMENT_CODE AS WORKSPOT_CODE, ' +
      '  ''0'' AS WORKSPOT_CODE, ' +
      '  D.DESCRIPTION AS WORKSPOTDESCRIPTION, D.PLAN_ON_WORKSPOT_YN ' +
      'FROM ' +
      '  DEPARTMENT D, TEAMPERUSER U, DEPARTMENTPERTEAM T ' +
      'WHERE ' +
      '  D.PLANT_CODE = :PLANT_CODE AND D.PLAN_ON_WORKSPOT_YN = ''N''' +
      '  AND U.TEAM_CODE = T.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' +
      '  T.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND T.PLANT_CODE =:PLANT_CODE ' +
      'UNION ' +
      'SELECT ' +
      '  W.PLANT_CODE, W.DEPARTMENT_CODE,' +
      '  D.DESCRIPTION AS DEPARTMENTDESCRIPTION,' +
      '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION,' +
      '  D.PLAN_ON_WORKSPOT_YN ' +
      'FROM ' +
      '  DEPARTMENT D, WORKSPOT W, TEAMPERUSER U, DEPARTMENTPERTEAM T ' +
      'WHERE ' +
      '  D.PLANT_CODE = :PLANT_CODE AND ' +
      '  W.PLANT_CODE = D.PLANT_CODE AND ' +
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND ' +
      '  D.PLAN_ON_WORKSPOT_YN = ''Y'' AND ' +
      '  U.TEAM_CODE = T.TEAM_CODE AND U.USER_NAME = :USER_NAME AND ' +
      '  T.PLANT_CODE = :PLANT_CODE AND T.DEPARTMENT_CODE = W.DEPARTMENT_CODE ' +
      'ORDER BY ' +
      '  1, 2, 4';
     QueryDepartmentMASTER.Sql.Add(SelectStr);
     QueryDepartmentMASTER.ParamByName('USER_NAME').AsString :=
       SystemDM.UserTeamLoginUser;
     // FOR export table
     QuerySelectDeptPerTeam.ParamByName('USER_NAME').AsString :=
       SystemDM.UserTeamLoginUser;
      //set Filtered property only on the export form event - for the speed reason
     TableExport.OnFilterRecord := FilterRecordExport;
     ClientDataSetExportDept.Open;
  end;
  QueryDepartmentMASTER.Prepare;

  TableDetail.MasterSource := DataSourceDepartmentMASTER;
  TableDetail.MasterFields := 'PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE';
  TableDetail.IndexFieldNames :=
    'PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE;DAY_OF_WEEK';
  SetTimeBlock(1,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '');
  SetPasteAction(False);
  SetCopyFromValues(0, '', '', '', '');
 // car 550279
 QueryTimeBlockPerDepartment.Prepare;
 QueryTimeBlockPerShift.Prepare;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlantShift);
  QueryPlantShift.Open;
end;

procedure TStandardOccupationDM.FillTimeBlock;
var
  Day: Integer;
begin
  // Take last values of Timeblock, if it exists, for
  // New-TimeBlock-Record.
  if TableDetail.RecordCount > 0 then
  begin
    TableDetail.Last;
    Day := TableDetailDAY_OF_WEEK.Value;
    if (Day >= 0) and (Day <= 6) then
      inc(Day);
    SetTimeBlock(
      Day,
      TableDetailOCC_A_TIMEBLOCK_1.Value,
      TableDetailOCC_A_TIMEBLOCK_2.Value,
      TableDetailOCC_A_TIMEBLOCK_3.Value,
      TableDetailOCC_A_TIMEBLOCK_4.Value,
      TableDetailOCC_A_TIMEBLOCK_5.Value,
      TableDetailOCC_A_TIMEBLOCK_6.Value,
      TableDetailOCC_A_TIMEBLOCK_7.Value,
      TableDetailOCC_A_TIMEBLOCK_8.Value,
      TableDetailOCC_A_TIMEBLOCK_9.Value,
      TableDetailOCC_A_TIMEBLOCK_10.Value,
      TableDetailOCC_B_TIMEBLOCK_1.Value,
      TableDetailOCC_B_TIMEBLOCK_2.Value,
      TableDetailOCC_B_TIMEBLOCK_3.Value,
      TableDetailOCC_B_TIMEBLOCK_4.Value,
      TableDetailOCC_B_TIMEBLOCK_5.Value,
      TableDetailOCC_B_TIMEBLOCK_6.Value,
      TableDetailOCC_B_TIMEBLOCK_7.Value,
      TableDetailOCC_B_TIMEBLOCK_8.Value,
      TableDetailOCC_B_TIMEBLOCK_9.Value,
      TableDetailOCC_B_TIMEBLOCK_10.Value,
      TableDetailOCC_C_TIMEBLOCK_1.Value,
      TableDetailOCC_C_TIMEBLOCK_2.Value,
      TableDetailOCC_C_TIMEBLOCK_3.Value,
      TableDetailOCC_C_TIMEBLOCK_4.Value,
      TableDetailOCC_C_TIMEBLOCK_5.Value,
      TableDetailOCC_C_TIMEBLOCK_6.Value,
      TableDetailOCC_C_TIMEBLOCK_7.Value,
      TableDetailOCC_C_TIMEBLOCK_8.Value,
      TableDetailOCC_C_TIMEBLOCK_9.Value,
      TableDetailOCC_C_TIMEBLOCK_10.Value,
      TableDetailFIXED_YN.Value
      );
    TableDetail.First;
  end
  else
    SetTimeBlock(1,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '');
end;

procedure TStandardOccupationDM.TableDetailBeforeInsert(DataSet: TDataSet);
begin
  inherited;
  if not FPasteAction then
    FillTimeBlock;
end;

procedure TStandardOccupationDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // First set Key-values from Outside
  // 20013910
  if QueryDepartmentMASTERPLAN_ON_WORKSPOT_YN.Value = 'Y' then
    TableDetailWORKSPOT_CODE.Value := QueryDepartmentMASTERWORKSPOT_CODE.Value
  else
    TableDetailWORKSPOT_CODE.Value := DummyStr;
  TableDetailSHIFT_NUMBER.Value := FShiftNumber;

  if FPasteAction then
  begin
    TableDetailDAY_OF_WEEK.Value :=
      QueryStandardOccupationFROMDAY_OF_WEEK.Value;

    TableDetailOCC_A_TIMEBLOCK_1.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_1.Value;
    TableDetailOCC_A_TIMEBLOCK_2.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_2.Value;
    TableDetailOCC_A_TIMEBLOCK_3.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_3.Value;
    TableDetailOCC_A_TIMEBLOCK_4.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_4.Value;
    TableDetailOCC_A_TIMEBLOCK_5.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_5.Value;
    TableDetailOCC_A_TIMEBLOCK_6.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_6.Value;
    TableDetailOCC_A_TIMEBLOCK_7.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_7.Value;
    TableDetailOCC_A_TIMEBLOCK_8.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_8.Value;
    TableDetailOCC_A_TIMEBLOCK_9.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_9.Value;
    TableDetailOCC_A_TIMEBLOCK_10.Value :=
      QueryStandardOccupationFROMOCC_A_TIMEBLOCK_10.Value;

    TableDetailOCC_B_TIMEBLOCK_1.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_1.Value;
    TableDetailOCC_B_TIMEBLOCK_2.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_2.Value;
    TableDetailOCC_B_TIMEBLOCK_3.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_3.Value;
    TableDetailOCC_B_TIMEBLOCK_4.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_4.Value;
    TableDetailOCC_B_TIMEBLOCK_5.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_5.Value;
    TableDetailOCC_B_TIMEBLOCK_6.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_6.Value;
    TableDetailOCC_B_TIMEBLOCK_7.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_7.Value;
    TableDetailOCC_B_TIMEBLOCK_8.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_8.Value;
    TableDetailOCC_B_TIMEBLOCK_9.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_9.Value;
    TableDetailOCC_B_TIMEBLOCK_10.Value :=
      QueryStandardOccupationFROMOCC_B_TIMEBLOCK_10.Value;

    TableDetailOCC_C_TIMEBLOCK_1.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_1.Value;
    TableDetailOCC_C_TIMEBLOCK_2.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_2.Value;
    TableDetailOCC_C_TIMEBLOCK_3.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_3.Value;
    TableDetailOCC_C_TIMEBLOCK_4.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_4.Value;
    TableDetailOCC_C_TIMEBLOCK_5.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_5.Value;
    TableDetailOCC_C_TIMEBLOCK_6.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_6.Value;
    TableDetailOCC_C_TIMEBLOCK_7.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_7.Value;
    TableDetailOCC_C_TIMEBLOCK_8.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_8.Value;
    TableDetailOCC_C_TIMEBLOCK_9.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_9.Value;
    TableDetailOCC_C_TIMEBLOCK_10.Value :=
      QueryStandardOccupationFROMOCC_C_TIMEBLOCK_10.Value;

    TableDetailFIXED_YN.Value :=
      QueryStandardOccupationFROMFIXED_YN.Value;
  end
  else
  begin
    TableDetailDAY_OF_WEEK.Value := FDay;

    TableDetailOCC_A_TIMEBLOCK_1.Value := FTB1A;
    TableDetailOCC_A_TIMEBLOCK_2.Value := FTB2A;
    TableDetailOCC_A_TIMEBLOCK_3.Value := FTB3A;
    TableDetailOCC_A_TIMEBLOCK_4.Value := FTB4A;
    TableDetailOCC_A_TIMEBLOCK_5.Value := FTB5A;
    TableDetailOCC_A_TIMEBLOCK_6.Value := FTB6A;
    TableDetailOCC_A_TIMEBLOCK_7.Value := FTB7A;
    TableDetailOCC_A_TIMEBLOCK_8.Value := FTB8A;
    TableDetailOCC_A_TIMEBLOCK_9.Value := FTB9A;
    TableDetailOCC_A_TIMEBLOCK_10.Value := FTB10A;

    TableDetailOCC_B_TIMEBLOCK_1.Value := FTB1B;
    TableDetailOCC_B_TIMEBLOCK_2.Value := FTB2B;
    TableDetailOCC_B_TIMEBLOCK_3.Value := FTB3B;
    TableDetailOCC_B_TIMEBLOCK_4.Value := FTB4B;
    TableDetailOCC_B_TIMEBLOCK_5.Value := FTB5B;
    TableDetailOCC_B_TIMEBLOCK_6.Value := FTB6B;
    TableDetailOCC_B_TIMEBLOCK_7.Value := FTB7B;
    TableDetailOCC_B_TIMEBLOCK_8.Value := FTB8B;
    TableDetailOCC_B_TIMEBLOCK_9.Value := FTB9B;
    TableDetailOCC_B_TIMEBLOCK_10.Value := FTB10B;

    TableDetailOCC_C_TIMEBLOCK_1.Value := FTB1C;
    TableDetailOCC_C_TIMEBLOCK_2.Value := FTB2C;
    TableDetailOCC_C_TIMEBLOCK_3.Value := FTB3C;
    TableDetailOCC_C_TIMEBLOCK_4.Value := FTB4C; // Wrong: Was FTB3C! RV079.2.
    TableDetailOCC_C_TIMEBLOCK_5.Value := FTB5C;
    TableDetailOCC_C_TIMEBLOCK_6.Value := FTB6C;
    TableDetailOCC_C_TIMEBLOCK_7.Value := FTB7C;
    TableDetailOCC_C_TIMEBLOCK_8.Value := FTB8C;
    TableDetailOCC_C_TIMEBLOCK_9.Value := FTB9C;
    TableDetailOCC_C_TIMEBLOCK_10.Value := FTB10C;

    if FFixed = '' then
      FFixed := 'Y';
    TableDetailFIXED_YN.Value := FFixed;
  end;
end;

procedure TStandardOccupationDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  QueryDepartmentMASTER.Active := False;
  //Car 550279
  QueryDepartmentMASTER.UnPrepare;
  QueryTimeBlockPerDepartment.Close;
  QueryTimeBlockPerDepartment.UnPrepare;
  QueryTimeBlockPerShift.Close;
  QueryTimeBlockPerShift.UnPrepare;
  ClientDataSetExportDept.Close;
end;

function TStandardOccupationDM.GetShiftNumber: Integer;
begin
  Result := FShiftNumber;
end;

procedure TStandardOccupationDM.SetShiftNumber(const Value: Integer);
begin
  FShiftNumber := Value;
end;

procedure TStandardOccupationDM.SetTimeBlock(Day,
  TB1A, TB2A, TB3A, TB4A, TB5A, TB6A, TB7A, TB8A, TB9A, TB10A,
  TB1B, TB2B, TB3B, TB4B, TB5B, TB6B, TB7B, TB8B, TB9B, TB10B,
  TB1C, TB2C, TB3C, TB4C, TB5C, TB6C, TB7C, TB8C, TB9C, TB10C: Integer;
  Fixed: String);
begin
  FDay  := Day;
  FTB1A := TB1A;
  FTB2A := TB2A;
  FTB3A := TB3A;
  FTB4A := TB4A;
  FTB5A := TB5A;
  FTB6A := TB6A;
  FTB7A := TB7A;
  FTB8A := TB8A;
  FTB9A := TB9A;
  FTB10A := TB10A;
  FTB1B := TB1B;
  FTB2B := TB2B;
  FTB3B := TB3B;
  FTB4B := TB4B;
  FTB5B := TB5B;
  FTB6B := TB6B;
  FTB7B := TB7B;
  FTB8B := TB8B;
  FTB9B := TB9B;
  FTB10B := TB10B;
  FTB1C := TB1C;
  FTB2C := TB2C;
  FTB3C := TB3C;
  FTB4C := TB4C;
  FTB5C := TB5C;
  FTB6C := TB6C;
  FTB7C := TB7C;
  FTB8C := TB8C;
  FTB9C := TB9C;
  FTB10C := TB10C;
  FFixed := Fixed;
end;

// Copy-Paste
procedure TStandardOccupationDM.SetPasteAction(const Value: Boolean);
begin
  FPasteAction := Value;
end;

// Copy-Paste
function TStandardOccupationDM.CopyFromValuesExists: Boolean;
begin
  Result := (FCopyFromPLANT_CODE <> '') and (FCopyFromDEPARTMENT_CODE <> '') and
    (FCopyFromWORKSPOT_CODE <> '');
end;

// Copy-Paste
// If user presses Copy-button, Copy-From-Value-Keys are set
procedure TStandardOccupationDM.SetCopyFromValues(
  ACopyFromSHIFT_NUMBER: Integer;
  ACopyFromPLAN_ON_WORKSPOT_YN, ACopyFromPLANT_CODE,
  ACopyFromDEPARTMENT_CODE, ACopyFromWORKSPOT_CODE: String);
begin
  FCopyFromSHIFT_NUMBER        := ACopyFromSHIFT_NUMBER;
  FCopyFromPLAN_ON_WORKSPOT_YN := ACopyFromPLAN_ON_WORKSPOT_YN;
  FCopyFromPLANT_CODE          := ACopyFromPLANT_CODE;
  FCopyFromDEPARTMENT_CODE     := ACopyFromDEPARTMENT_CODE;
  FCopyFromWORKSPOT_CODE       := ACopyFromWORKSPOT_CODE;
end;

// Copy-Paste
function TStandardOccupationDM.SourceTargetEqual: Boolean;
begin
  Result := (FCopyFromPLANT_CODE = TableDetailPLANT_CODE.Value) and
    (FCopyFromDEPARTMENT_CODE = TableDetailDEPARTMENT_CODE.Value) and
    (FCopyFromWORKSPOT_CODE = TableDetailWORKSPOT_CODE.Value) and
    (FCopyFromSHIFT_NUMBER = TableDetailSHIFT_NUMBER.Value);
end;

// Copy-Paste
procedure TStandardOccupationDM.PasteExecute;
var
  WorkspotSelection, SQLStr: String;
  RecCount: Integer;
  GoOn: Boolean;
  Proc: TDataSetNotifyEvent;
begin
  if not CopyFromValuesExists then
    Exit;
  if SourceTargetEqual then
    Exit;

  WorkspotSelection := '';
  if FCopyFromPLAN_ON_WORKSPOT_YN = 'Y' then
    WorkspotSelection := ' WORKSPOT_CODE = :WORKSPOT_CODE and ';

  SQLStr :=
    'SELECT ' +
    '  PLANT_CODE, ' +
    '  DEPARTMENT_CODE, ' +
    '  WORKSPOT_CODE, ' +
    '  SHIFT_NUMBER, ' +
    '  DAY_OF_WEEK, ' +
    '  OCC_A_TIMEBLOCK_1, ' +
    '  OCC_A_TIMEBLOCK_2, ' +
    '  OCC_A_TIMEBLOCK_3, ' +
    '  OCC_A_TIMEBLOCK_4, ' +
    '  OCC_A_TIMEBLOCK_5, ' +
    '  OCC_A_TIMEBLOCK_6, ' +
    '  OCC_A_TIMEBLOCK_7, ' +
    '  OCC_A_TIMEBLOCK_8, ' +
    '  OCC_A_TIMEBLOCK_9, ' +
    '  OCC_A_TIMEBLOCK_10, ' +
    '  OCC_B_TIMEBLOCK_1, ' +
    '  OCC_B_TIMEBLOCK_2, ' +
    '  OCC_B_TIMEBLOCK_3, ' +
    '  OCC_B_TIMEBLOCK_4, ' +
    '  OCC_B_TIMEBLOCK_5, ' +
    '  OCC_B_TIMEBLOCK_6, ' +
    '  OCC_B_TIMEBLOCK_7, ' +
    '  OCC_B_TIMEBLOCK_8, ' +
    '  OCC_B_TIMEBLOCK_9, ' +
    '  OCC_B_TIMEBLOCK_10, ' +
    '  OCC_C_TIMEBLOCK_1, ' +
    '  OCC_C_TIMEBLOCK_2, ' +
    '  OCC_C_TIMEBLOCK_3, ' +
    '  OCC_C_TIMEBLOCK_4, ' +
    '  OCC_C_TIMEBLOCK_5, ' +
    '  OCC_C_TIMEBLOCK_6, ' +
    '  OCC_C_TIMEBLOCK_7, ' +
    '  OCC_C_TIMEBLOCK_8, ' +
    '  OCC_C_TIMEBLOCK_9, ' +
    '  OCC_C_TIMEBLOCK_10, ' +
    '  FIXED_YN ' +
    'FROM ' +
    '  STANDARDOCCUPATION ' +
    'WHERE ' +
    '  PLANT_CODE = :PLANT_CODE AND ' +
    '  DEPARTMENT_CODE = :DEPARTMENT_CODE AND ' +
    WorkspotSelection +
    '  SHIFT_NUMBER = :SHIFT_NUMBER';

  RecCount := 0;
  try
    try
      QueryStandardOccupationFROM.SQL.Clear;
      QueryStandardOccupationFROM.SQL.Add(SQLStr);
      QueryStandardOccupationFROM.ParamByName('PLANT_CODE').Value :=
        FCopyFromPLANT_CODE;
      QueryStandardOccupationFROM.ParamByName('DEPARTMENT_CODE').Value :=
        FCopyFromDEPARTMENT_CODE;
      if WorkspotSelection <> '' then
        QueryStandardOccupationFROM.ParamByName('WORKSPOT_CODE').Value :=
          FCopyFromWORKSPOT_CODE;
      QueryStandardOccupationFROM.ParamByName('SHIFT_NUMBER').Value :=
        FCopyFromSHIFT_NUMBER;
      QueryStandardOccupationFROM.Open;
      RecCount := QueryStandardOccupationFROM.RecordCount;
    except
      // Ignore error
    end;
    if RecCount > 0 then
    begin
      GoOn := True;
      if TableDetail.RecordCount > 0 then
      begin
        if DisplayMessage(SPimsPasteConfirmOverwrite,
           mtConfirmation, [mbYes, mbNo]) <> mrYes then
          GoOn := False;
        // First Delete existing records
        if GoOn then
        begin
          Proc := TableDetail.BeforeDelete;
          try
            TableDetail.BeforeDelete := nil;
            TableDetail.Last;
            while not TableDetail.Bof do
              TableDetail.Delete;
          finally
            TableDetail.BeforeDelete := Proc;
          end;
        end;
      end;
      if GoOn then
      begin
        SetPasteAction(True);
        QueryStandardOccupationFROM.First;
        while not QueryStandardOccupationFROM.Eof do
        begin
          TableDetail.Insert;
          TableDetail.Post;
          QueryStandardOccupationFROM.Next;
        end;
      end;
    end;
  finally
    SetPasteAction(False);
    QueryStandardOccupationFROM.Close;
  end;
end;

procedure TStandardOccupationDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  TableDetailDAYDESC.Value :=
    SystemDM.GetDayWDescription(TableDetail.FieldByName('DAY_OF_WEEK').AsInteger);
end;

procedure TStandardOccupationDM.DefaultBeforePost(DataSet: TDataSet);
begin
  inherited;
//  TableDetail.FieldByName('DAY_OF_WEEK').AsInteger := ItemIndex
  // 20013910 Store '0' for workspot code for plan-on-department.
  if QueryDepartmentMASTERPLAN_ON_WORKSPOT_YN.Value = 'N' then
    TableDetail.FieldByName('WORKSPOT_CODE').AsString := DummyStr;
end;

procedure TStandardOccupationDM.TableExportCalcFields(DataSet: TDataSet);
begin
  inherited;
  TableExport.FieldByName('DAYDESC').Value :=
    SystemDM.GetDayWDescription(TableExport.FieldByName('DAY_OF_WEEK').AsInteger);
end;

 

//car 550279 : team selection rights  - for export table
procedure TStandardOccupationDM.FilterRecordExport(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := ClientDataSetExportDept.FindKey([
    DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('DEPARTMENT_CODE').AsString]);
end;

// RV050.8.
procedure TStandardOccupationDM.QueryPlantShiftFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
