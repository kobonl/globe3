(*
  MRA:14-MAY-2009 RV027.
    - Memory-problem: Changes to prevent memory-problems.
  MRA:23-NOV-2009 RV045.1.
    - TimeForTime-settings are now possible on hourtype-level, instead
      of only contractgroup-level.
  MRA:10-DEC-2009 RV048.4. Bugfix.
    - When using 'Process Planned Absence' it can go wrong when
      processing scans for a week, but for Saturday + Sunday no timeblocks
      have been defined, and there are only scans for Monday till Friday.
      This results in removing of salary-hours for Friday and next Monday,
      because it does not find timeblocks for Saturday, so it takes
      the previous day as bookingday and removes first the salary.
      On Sunday there is no scan, but it tries to find the nearest shift-day,
      this is Monday and it removes the salary.
      As a result for 2 days the salary-hours are removed.
      Main-problem is: It does not look at the scans really, but just
      tries to calculate something for each day of the given period. And because
      it first deletes the salary for that day (without checking first if there
      are scans on that day), salary-hours are lost afterwards.
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  MRA:1-SEP-2010. RV067.MRA.22 Change for order 550491.
  - Procedure added that checks on employee availability.
  MRA:10-SEP-2010 RV067.MRA.31 Order 550515
  - A check must be made to ensure the balances are correct
    for absence hours. Compare ABSENCEHOURPEREMPLOYEE with
    ABSENCETOTALS. Queries qryAHECheck + qryATCheck added.
  MRA:22-SEP-2010 RV067.MRA.35 Change for 550491
  - It did not create salary records when there was no
    employee availability, but it should do this,
    even if there is no employee availability.
  MRA:19-OCT-2011 RV099.1. 20012234 TFT/Holiday correction
  - Make correction for TFT/Holiday:
    - Based on settings made in COUNTRY-table, try to book
      vacation first on TFT when there are enough hours left in balance.
      When not, then book them on vacation.
    - During this employee-availability will be changed.
  MRA:18-OCT-2012 SC-50025602
  - Problem with Absence Balance, sometimes values are in balance that
    do not belong there. For example -8 hours for 'used holiday'.
    Cause: The balance is not initialized before it is reprocessed.
    Added query: qryAHEInit
  MRA:28-MAR-2013 TD-22355 Performance issue.
  - Instead of TTable use TQuery to improve speed.
  - All TTables are removed from this datamodule.
  MRA:29-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)

unit DialogProcessAbsenceHrsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Dblup1a, CalculateTotalHoursDMT, DBClient;

type
  TDialogProcessAbsenceHrsDM = class(TDataModule)
    Query: TQuery;
    qryBreakPerEmployee: TQuery;
    qryBreakPerDepartment: TQuery;
    qryBreakPerShift: TQuery;
    qryAbsenceHourPerEmployee: TQuery;
    QueryWork: TQuery;
    QueryEmpl: TQuery;
    qryIllnessMsg: TQuery;
    qryEmpAvail: TQuery;
    cdsEmpAvail: TClientDataSet;
    cdsEmpAvailPLANT_CODE: TStringField;
    cdsEmpAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField;
    cdsEmpAvailSHIFT_NUMBER: TIntegerField;
    cdsEmpAvailEMPLOYEE_NUMBER: TIntegerField;
    qryWork: TQuery;
    qryEmpAvailUpdate: TQuery;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_1: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_2: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_3: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_4: TStringField;
    qryCntr: TQuery;
    qryTRS: TQuery;
    QueryTmp: TQuery;
    qryEmpBookProdHrs: TQuery;
    qryCheckEmpAvail: TQuery;
    qryAHECheck: TQuery;
    qryATCheck: TQuery;
    qryDeleteSHE: TQuery;
    qryPHEPTCheck1: TQuery;
    qryPHE: TQuery;
    qryPHEPTCheck2: TQuery;
    qryAHEInit: TQuery;
    qryAbsTotTFTUpdate: TQuery;
    qryDELSHE: TQuery;
    qryDELAHE: TQuery;
    qryAHEUpdate: TQuery;
    qryAHEFind: TQuery;
    qryAHEInsert: TQuery;
    qryAbsenceReason: TQuery;
    qrySHEInsert: TQuery;
    qryAbsenceTotal: TQuery;
    qrySHE: TQuery;
    qryAHECorrectFind: TQuery;
    qryCalcSalary: TQuery;
    qryDELAHECorrect: TQuery;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_5: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_6: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_7: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_8: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_9: TStringField;
    cdsEmpAvailAVAILABLE_TIMEBLOCK_10: TStringField;
    procedure DefaultPostError(DataSet: TDataSet;
      E: EDatabaseError; var Action: TDataAction);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
  public
    { Public declarations }
    procedure DetermineUnpaidIllness(
      AEmployeeNumber: Integer;
      APaidIllnessPeriod: Integer;
      AUnpaidIllnessAbsenceReasonCode: String;
      ADateFrom, ADateTo: TDateTime);
    function SearchCdsEmpAvail(
      const PlantCode: String;
      const EmployeeAvailabilityDate: TDateTime;
      const ShiftNumber, EmployeeNumber: Integer;
      const ATB1, ATB2, ATB3, ATB4, ATB5, ATB6, ATB7, ATB8, ATB9, ATB10: String;
      var ANTB1, ANTB2, ANTB3, ANTB4, ANTB5, ANTB6, ANTB7, ANTB8,
      ANTB9, ANTB10: String): Boolean;
    function CheckEmployeeAvailability(
      const AEmployeeNumber: Integer;
      const AEmployeeAvailabilityDate: TDateTime;
      var AAvailableTimeblock_1, AAvailableTimeblock_2,
      AAvailableTimeblock_3, AAvailableTimeblock_4,
      AAvailableTimeblock_5, AAvailableTimeblock_6,
      AAvailableTimeblock_7, AAvailableTimeblock_8,
      AAvailableTimeblock_9, AAvailableTimeblock_10: String): Boolean;
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
  end;

var
  DialogProcessAbsenceHrsDM: TDialogProcessAbsenceHrsDM;

implementation

{$R *.DFM}

uses
  SystemDMT, UPimsConst, GlobalDMT;

procedure TDialogProcessAbsenceHrsDM.DefaultPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  SystemDM.DefaultPostError(DataSet, E, Action);
end;

function TDialogProcessAbsenceHrsDM.SearchCdsEmpAvail(
  const PlantCode: String;
  const EmployeeAvailabilityDate: TDateTime;
  const ShiftNumber, EmployeeNumber: Integer;
  const ATB1, ATB2, ATB3, ATB4, ATB5, ATB6, ATB7, ATB8, ATB9, ATB10: String;
  var ANTB1, ANTB2, ANTB3, ANTB4, ANTB5, ANTB6, ANTB7, ANTB8,
  ANTB9, ANTB10: String): Boolean;
begin
  Result := False;
  ANTB1 := ATB1;
  ANTB2 := ATB2;
  ANTB3 := ATB3;
  ANTB4 := ATB4;
  ANTB5 := ATB5;
  ANTB6 := ATB6;
  ANTB7 := ATB7;
  ANTB8 := ATB8;
  ANTB9 := ATB9;
  ANTB10 := ATB10;
  if not cdsEmpAvail.IsEmpty then
    if cdsEmpAvail.FindKey([
      PlantCode, EmployeeAvailabilityDate, ShiftNumber, EmployeeNumber]) then
    begin
      ANTB1 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString;
      ANTB2 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString;
      ANTB3 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString;
      ANTB4 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString;
      ANTB5 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString;
      ANTB6 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString;
      ANTB7 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString;
      ANTB8 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString;
      ANTB9 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString;
      ANTB10 := cdsEmpAvail.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString;
      Result := True;
    end;
end;

// MR:17-09-2004 Order 550335
// Determines by using CONTRACTGROUP- and ILLNESSMESSAGE-table
// if the employee has 'unpaid-illness'-days.
// Updates table 'EmployeeAvailability'.
procedure TDialogProcessAbsenceHrsDM.DetermineUnpaidIllness(
  AEmployeeNumber: Integer;
  APaidIllnessPeriod: Integer;
  AUnpaidIllnessAbsenceReasonCode: String;
  ADateFrom, ADateTo: TDateTime);
var
  IllnessDays: Integer;
  TimeBlockNo: Integer;
  LineNumber: Integer;
  TB: String;
begin
  if APaidIllnessPeriod > 0 then
  begin
    // Get the illness message records for this employee.
    qryWork.Close;
    qryWork.SQL.Clear;
    qryWork.SQL.Add(
      'SELECT ' +
      '  LINE_NUMBER ' +
      'FROM ' +
      '  ILLNESSMESSAGE ' +
      'WHERE ' +
      '(EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER) AND ' +
      '( ' +
      '  ( ' +
      '    ILLNESSMESSAGE_STARTDATE <= :DATEFROM AND ' +
      '    (ILLNESSMESSAGE_ENDDATE >= :DATEFROM OR ' +
      '     ILLNESSMESSAGE_ENDDATE IS NULL) ' +
      '    AND ' +
      '    (ILLNESSMESSAGE_ENDDATE <= :DATETO OR ' +
      '     ILLNESSMESSAGE_ENDDATE IS NULL) ' +
      '  ) ' +
      'OR ' +
      '  ( ' +
      '    ILLNESSMESSAGE_STARTDATE >= :DATEFROM AND ' +
      '    (ILLNESSMESSAGE_ENDDATE <= :DATETO OR ' +
      '     ILLNESSMESSAGE_ENDDATE IS NULL) ' +
      '  ) ' +
      'OR ' +
      '  (ILLNESSMESSAGE_STARTDATE >= :DATEFROM AND ' +
      '   ILLNESSMESSAGE_STARTDATE <= :DATETO AND ' +
      '   (ILLNESSMESSAGE_ENDDATE >= :DATEFROM OR ' +
      '    ILLNESSMESSAGE_ENDDATE IS NULL) ' +
      '  ) ' +
      ') ' +
      'GROUP BY ' +
      '  LINE_NUMBER '
      );
    qryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    qryWork.ParamByName('DATEFROM').AsDateTime := Trunc(ADateFrom);
    qryWork.ParamByName('DATETO').AsDateTime := Trunc(ADateTo);
    qryWork.Open;
    if not qryWork.IsEmpty then
    begin
      qryWork.First;
      while not qryWork.Eof do
      begin
        // For one and the same 'linenumber' calculate the
        // the illnessdays and look if this comes above a limit.
        LineNumber := qryWork.FieldByName('LINE_NUMBER').AsInteger;
        IllnessDays := 0;
        qryIllnessMsg.Close;
        qryIllnessMsg.SQL.Clear;
        qryIllnessMsg.SQL.Add(
          'SELECT ' +
          '  ILLNESSMESSAGE_STARTDATE, ' +
          '  ILLNESSMESSAGE_ENDDATE, ' +
          '  ABSENCEREASON_CODE, ' +
          '  LINE_NUMBER ' +
          'FROM ' +
          '  ILLNESSMESSAGE ' +
          'WHERE ' +
          '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' +
          '  LINE_NUMBER = :LINE_NUMBER ' +
          'ORDER BY ' +
          '  ILLNESSMESSAGE_STARTDATE'
          );
        qryIllnessMsg.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        qryIllnessMsg.ParamByName('LINE_NUMBER').AsInteger := LineNumber;
        qryIllnessMsg.Open;
        if not qryIllnessMsg.IsEmpty then
        begin
          qryIllnessMsg.First;
          while not qryIllnessMsg.Eof do
          begin
            qryEmpAvail.Close;
            qryEmpAvail.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
              AEmployeeNumber;
            qryEmpAvail.ParamByName('ABSENCEREASON_CODE').AsString :=
              qryIllnessMsg.FieldByName('ABSENCEREASON_CODE').AsString;
            qryEmpAvail.ParamByName('ABSENCETYPE_CODE').AsString := ILLNESS;
            qryEmpAvail.ParamByName('DATEFROM').AsDateTime :=
              qryIllnessMsg.FieldByName('ILLNESSMESSAGE_STARTDATE').AsDateTime;
            if qryIllnessMsg.FieldByName('ILLNESSMESSAGE_ENDDATE').Value = Null then
              qryEmpAvail.ParamByName('DATETO').AsDateTime :=
                EncodeDate(2099, 12, 31)
            else
              qryEmpAvail.ParamByName('DATETO').AsDateTime :=
                qryIllnessMsg.FieldByName('ILLNESSMESSAGE_ENDDATE').AsDateTime;
            qryEmpAvail.Open;
            if not qryEmpAvail.IsEmpty then
            begin
              qryEmpAvail.First;
              while not qryEmpAvail.Eof do
              begin
                inc(IllnessDays);
                if IllnessDays > APaidIllnessPeriod then
                begin
                  // Remember this record, so it can be used later.
                  cdsEmpAvail.Insert;
                  // Primary fields from 'employeeavailability'
                  cdsEmpAvail.FieldByName('PLANT_CODE').AsString :=
                    qryEmpAvail.FieldByName('PLANT_CODE').AsString;
                  cdsEmpAvail.
                    FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime :=
                      qryEmpAvail.
                        FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
                  cdsEmpAvail.FieldByName('SHIFT_NUMBER').AsInteger :=
                    qryEmpAvail.FieldByName('SHIFT_NUMBER').AsInteger;
                  cdsEmpAvail.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
                    qryEmpAvail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
                  for TimeBlockNo := 1 to SystemDM.MaxTimeblocks do
                  begin
                    TB := 'AVAILABLE_TIMEBLOCK_' + IntToStr(TimeBlockNo);
                    if (qryEmpAvail.FieldByName(TB).AsString =
                      qryIllnessMsg.
                        FieldByName('ABSENCEREASON_CODE').AsString) then
                      cdsEmpAvail.FieldByName(TB).AsString :=
                        AUnpaidIllnessAbsenceReasonCode
                      else
                        cdsEmpAvail.FieldByName(TB).AsString :=
                          qryEmpAvail.FieldByName(TB).AsString;
                  end;
                  cdsEmpAvail.Post;
                end;
                qryEmpAvail.Next;
              end; // while not qryEmpAvail.Eof do
            end; // if not qryEmpAvail.IsEmpty then
            qryIllnessMsg.Next;
          end; // while not qryIllnessMsg.Eof do
        end; // if not qryIllnessMsg.IsEmpty then
        qryIllnessMsg.Close;
        // Keep the contents of 'cdsEmpAvail' for later use!
        qryWork.Next;
      end; // while not qryWork.Eof do
    end; // if not qryWork.IsEmpty then
  end;  // if APaidIllnessPeriod > 0 then
end;

procedure TDialogProcessAbsenceHrsDM.DataModuleCreate(Sender: TObject);
begin
  cdsEmpAvail.CreateDataSet;
  // MRA:14-MAY-2009 RV027.
  // Set LogChanges to False for performance reasons.
  try
    try
      cdsEmpAvail.LogChanges := False;
    except
      // Ignore error.
    end;
  finally
    ContractGroupTFT := GlobalDM.ContractGroupTFTExists; // RV045.1.
  end;
end;

procedure TDialogProcessAbsenceHrsDM.DataModuleDestroy(Sender: TObject);
begin
  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
  cdsEmpAvail.Close;
end;

// RV067.MRA.22 Change for order 550491.
function TDialogProcessAbsenceHrsDM.CheckEmployeeAvailability(
  const AEmployeeNumber: Integer;
  const AEmployeeAvailabilityDate: TDateTime;
  var AAvailableTimeblock_1, AAvailableTimeblock_2,
  AAvailableTimeblock_3, AAvailableTimeblock_4,
  AAvailableTimeblock_5, AAvailableTimeblock_6,
  AAvailableTimeblock_7, AAvailableTimeblock_8,
  AAvailableTimeblock_9, AAvailableTimeblock_10: String): Boolean;
begin
  // RV067.MRA.35 Change for 550491
  // Set default to 'available', to be sure it will create salary-records,
  // even if there was no availability-record found.
  // Result must always be TRUE!
  Result := True;
  AAvailableTimeblock_1 := '*';
  AAvailableTimeblock_2 := '*';
  AAvailableTimeblock_3 := '*';
  AAvailableTimeblock_4 := '*';
  AAvailableTimeblock_5 := '*';
  AAvailableTimeblock_6 := '*';
  AAvailableTimeblock_7 := '*';
  AAvailableTimeblock_8 := '*';
  AAvailableTimeblock_9 := '*';
  AAvailableTimeblock_10 := '*';
  with qryCheckEmpAvail do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime :=
      AEmployeeAvailabilityDate;
    Open;
    if not Eof then
    begin
      AAvailableTimeblock_1 := FieldByName('AVAILABLE_TIMEBLOCK_1').AsString;
      AAvailableTimeblock_2 := FieldByName('AVAILABLE_TIMEBLOCK_2').AsString;
      AAvailableTimeblock_3 := FieldByName('AVAILABLE_TIMEBLOCK_3').AsString;
      AAvailableTimeblock_4 := FieldByName('AVAILABLE_TIMEBLOCK_4').AsString;
      AAvailableTimeblock_5 := FieldByName('AVAILABLE_TIMEBLOCK_5').AsString;
      AAvailableTimeblock_6 := FieldByName('AVAILABLE_TIMEBLOCK_6').AsString;
      AAvailableTimeblock_7 := FieldByName('AVAILABLE_TIMEBLOCK_7').AsString;
      AAvailableTimeblock_8 := FieldByName('AVAILABLE_TIMEBLOCK_8').AsString;
      AAvailableTimeblock_9 := FieldByName('AVAILABLE_TIMEBLOCK_9').AsString;
      AAvailableTimeblock_10 := FieldByName('AVAILABLE_TIMEBLOCK_10').AsString;
//      Result := True; // RV067.MRA.35
    end;
    Close;
  end;
end;

end.
