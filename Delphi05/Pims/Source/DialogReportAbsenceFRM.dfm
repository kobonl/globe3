inherited DialogReportAbsenceF: TDialogReportAbsenceF
  Caption = 'Report absence hours'
  ClientHeight = 541
  ClientWidth = 606
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 606
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 310
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 606
    Height = 420
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 178
    end
    inherited LblEmployee: TLabel
      Top = 178
    end
    inherited LblToPlant: TLabel
      Top = 179
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 180
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 180
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [9]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [10]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [11]
      Left = 315
      Top = 41
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [12]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel [13]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel [14]
      Left = 8
      Top = 430
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [15]
      Left = 40
      Top = 430
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [16]
      Left = 315
      Top = 430
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [17]
      Left = 8
      Top = 124
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel [18]
      Left = 40
      Top = 124
      Width = 77
      Height = 13
      Caption = 'Absence reason'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel [19]
      Left = 315
      Top = 70
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label18: TLabel [20]
      Left = 315
      Top = 123
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel [21]
      Left = 128
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [22]
      Left = 336
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [23]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [24]
      Left = 128
      Top = 128
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel [25]
      Left = 336
      Top = 128
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label24: TLabel [26]
      Left = 128
      Top = 126
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label25: TLabel [27]
      Left = 336
      Top = 126
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label26: TLabel [28]
      Left = 8
      Top = 96
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label27: TLabel [29]
      Left = 40
      Top = 96
      Width = 66
      Height = 13
      Caption = 'Absence type'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label28: TLabel [30]
      Left = 315
      Top = 96
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromDate: TLabel [31]
      Left = 8
      Top = 207
      Width = 24
      Height = 13
      Caption = 'From'
    end
    object LblDate: TLabel [32]
      Left = 40
      Top = 207
      Width = 23
      Height = 13
      Caption = 'Date'
    end
    object LblToDate: TLabel [33]
      Left = 315
      Top = 206
      Width = 10
      Height = 13
      Caption = 'to'
    end
    inherited LblFromDepartment: TLabel
      Top = 70
    end
    inherited LblDepartment: TLabel
      Top = 70
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 69
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 156
    end
    inherited LblToTeam: TLabel
      Top = 152
    end
    inherited LblStarTeamFrom: TLabel
      Top = 156
    end
    inherited LblTeam: TLabel
      Top = 152
    end
    inherited LblFromTeam: TLabel
      Top = 152
    end
    inherited LblFromShift: TLabel
      Top = 476
    end
    inherited LblShift: TLabel
      Top = 476
    end
    inherited LblStartShiftFrom: TLabel
      Top = 476
    end
    inherited LblToShift: TLabel
      Top = 478
    end
    inherited LblStarShiftTo: TLabel
      Top = 476
    end
    inherited LblFromPlant2: TLabel
      Top = 511
    end
    inherited LblPlant2: TLabel
      Top = 511
    end
    inherited LblStarPlant2From: TLabel
      Top = 511
    end
    inherited LblToPlant2: TLabel
      Top = 511
    end
    inherited LblStarPlant2To: TLabel
      Top = 511
    end
    inherited LblFromWorkspot: TLabel
      Top = 576
    end
    inherited LblWorkspot: TLabel
      Top = 576
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 576
    end
    inherited LblToWorkspot: TLabel
      Top = 576
    end
    inherited LblStarWorkspotTo: TLabel
      Top = 576
    end
    inherited LblWorkspots: TLabel
      Top = 544
    end
    inherited BtnWorkspots: TSpeedButton
      Top = 541
    end
    inherited LblStarJobTo: TLabel
      Top = 601
    end
    inherited LblToJob: TLabel
      Top = 601
    end
    inherited LblStarJobFrom: TLabel
      Top = 601
    end
    inherited LblJob: TLabel
      Top = 601
    end
    inherited LblFromJob: TLabel
      Top = 601
    end
    inherited LblFromDateTime: TLabel
      Top = 450
    end
    inherited LblDateTime: TLabel
      Top = 450
    end
    inherited LblToDateTime: TLabel
      Top = 450
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 125
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 126
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 151
      ColCount = 133
      TabOrder = 12
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 151
      ColCount = 134
      TabOrder = 13
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 521
      Top = 156
      TabOrder = 14
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 65
      ColCount = 136
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 65
      ColCount = 137
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      Top = 67
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 178
      TabOrder = 15
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 178
      TabOrder = 16
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Top = 475
      ColCount = 136
      TabOrder = 31
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Top = 475
      ColCount = 137
      TabOrder = 29
    end
    inherited CheckBoxAllShifts: TCheckBox
      Top = 475
      TabOrder = 30
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 28
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 538
      Top = 506
      TabOrder = 40
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 538
      Top = 451
      TabOrder = 32
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Top = 509
      ColCount = 146
      TabOrder = 33
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Top = 509
      ColCount = 147
      TabOrder = 34
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 574
      ColCount = 25
      TabOrder = 36
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Top = 574
      ColCount = 148
      TabOrder = 27
    end
    inherited EditWorkspots: TEdit
      Top = 542
      TabOrder = 20
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Top = 599
      ColCount = 153
      TabOrder = 22
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 599
      ColCount = 152
      TabOrder = 21
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 448
      TabOrder = 37
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Top = 448
      TabOrder = 39
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 448
      TabOrder = 23
    end
    inherited DatePickerTo: TDateTimePicker
      Top = 448
      TabOrder = 38
    end
    object GroupBoxSelection: TGroupBox
      Left = 280
      Top = 256
      Width = 241
      Height = 161
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 26
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 36
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 8
        Top = 56
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 2
        OnClick = CheckBoxPageDeptClick
      end
      object CheckBoxPageTeam: TCheckBox
        Left = 8
        Top = 76
        Width = 169
        Height = 17
        Caption = 'New page per team'
        TabOrder = 3
        OnClick = CheckBoxPageTeamClick
      end
      object CheckBoxPageEmpl: TCheckBox
        Left = 8
        Top = 96
        Width = 161
        Height = 17
        Caption = 'New page per employee'
        TabOrder = 4
        OnClick = CheckBoxPageEmplClick
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 136
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 6
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 8
        Top = 116
        Width = 217
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 5
      end
    end
    object RadioGroupSort: TRadioGroup
      Left = 152
      Top = 256
      Width = 121
      Height = 161
      Caption = 'Sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Reason'
        'Week')
      ParentFont = False
      TabOrder = 25
    end
    object GroupBoxShow: TGroupBox
      Left = 8
      Top = 256
      Width = 137
      Height = 161
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 24
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 16
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 0
        OnClick = CheckBoxDeptClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 128
        Width = 73
        Height = 17
        Caption = 'Employee'
        TabOrder = 2
        OnClick = CheckBoxEmplClick
      end
      object CheckBoxTeam: TCheckBox
        Left = 8
        Top = 72
        Width = 73
        Height = 17
        Caption = 'Team'
        TabOrder = 1
        OnClick = CheckBoxTeamClick
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 40
      Width = 180
      Height = 19
      ColCount = 126
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 40
      Width = 180
      Height = 19
      ColCount = 127
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusAbsReasonFrom: TComboBoxPlus
      Left = 120
      Top = 123
      Width = 180
      Height = 19
      ColCount = 127
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsReasonFromCloseUp
    end
    object ComboBoxPlusAbsReasonTo: TComboBoxPlus
      Left = 334
      Top = 123
      Width = 180
      Height = 19
      ColCount = 128
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsReasonToCloseUp
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 520
      Top = 430
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 35
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object CheckBoxAllAbsReason: TCheckBox
      Left = 521
      Top = 121
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      OnClick = CheckBoxAllAbsReasonClick
    end
    object CheckBoxEmpTeam: TCheckBox
      Left = 8
      Top = 235
      Width = 241
      Height = 17
      Caption = 'Include employees not connected to teams'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 19
    end
    object ComboBoxPlusAbsenceTypeFrom: TComboBoxPlus
      Left = 120
      Top = 95
      Width = 180
      Height = 19
      ColCount = 128
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsenceTypeFromCloseUp
    end
    object ComboBoxPlusAbsenceTypeTo: TComboBoxPlus
      Left = 334
      Top = 95
      Width = 180
      Height = 19
      ColCount = 129
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusAbsenceTypeToCloseUp
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 205
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 17
      OnChange = DateFromChange
    end
    object DateTo: TDateTimePicker
      Left = 335
      Top = 205
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 18
      OnChange = DateToChange
    end
  end
  inherited stbarBase: TStatusBar
    Top = 481
    Width = 606
  end
  inherited pnlBottom: TPanel
    Top = 500
    Width = 606
    inherited btnOk: TBitBtn
      Left = 184
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 248
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        47040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507274469616C6F675265706F7274416273656E6365462E4461
        7461536F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A
        650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67
        106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A69
        6E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564
        676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D656467
        6F43616E496E73657274116564676F43616E4E617669676174696F6E11656467
        6F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264
        73106564676F557365426F6F6B6D61726B7300000F546478444247726964436F
        6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265
        7206536F7274656407046373557005576964746802410942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060F454D504C4F5945
        455F4E554D42455200000F546478444247726964436F6C756D6E0F436F6C756D
        6E53686F72744E616D650743617074696F6E060A53686F7274206E616D650557
        6964746802540942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060A53484F52545F4E414D4500000F54647844424772696443
        6F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557
        6964746803B4000942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060B4445534352495054494F4E00000F5464784442477269
        64436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741
        64647265737305576964746802450942616E64496E646578020008526F77496E
        6465780200094669656C644E616D6506074144445245535300000F5464784442
        47726964436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F
        6E060F4465706172746D656E7420636F646505576964746802580942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F444550
        4152544D454E545F434F444500000F546478444247726964436F6C756D6E0A43
        6F6C756D6E5465616D0743617074696F6E06095465616D20636F64650942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060954
        45414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0E040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507254469616C6F675265706F72
        74416273656E6365462E44617461536F75726365456D706C546F104F7074696F
        6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F
        42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F
        436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074
        696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E
        44656C6574650D6564676F43616E496E73657274116564676F43616E4E617669
        676174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F61
        64416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54
        6478444247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F
        6E06064E756D62657206536F7274656407046373557005576964746802310942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C75
        6D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F72
        74206E616D65055769647468024E0942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060A53484F52545F4E414D4500000F5464
        78444247726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07
        43617074696F6E06044E616D6505576964746803CC000942616E64496E646578
        020008526F77496E6465780200094669656C644E616D65060B44455343524950
        54494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464
        726573730743617074696F6E0607416464726573730557696474680265094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D650607
        4144445245535300000F546478444247726964436F6C756D6E0E436F6C756D6E
        44657074436F64650743617074696F6E060F4465706172746D656E7420636F64
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D65060F4445504152544D454E545F434F444500000F54647844424772696443
        6F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D2063
        6F64650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 420
  end
  object TableAbsReason: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCEREASON_CODE'
    TableName = 'ABSENCEREASON'
    Left = 496
    Top = 23
    object TableAbsReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableAbsenceType: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCETYPE_CODE'
    TableName = 'ABSENCETYPE'
    Left = 464
    Top = 23
    object TableAbsenceTypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_ID,'
      '  ABSENCEREASON_CODE,'
      '  DESCRIPTION,'
      '  ABSENCETYPE_CODE'
      'FROM'
      '  ABSENCEREASON'
      'ORDER BY'
      '  ABSENCEREASON_CODE'
      '')
    Left = 296
    Top = 24
  end
end
