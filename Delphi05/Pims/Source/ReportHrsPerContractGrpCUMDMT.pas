(*
  Changes:
    MRA:24-MAY-2011. RV093.1. SO-20011749.
    - New report: Report Hours Per Contract Group Cumulative.
      Based on ReportHrsPerWKCUMDMT.
*)
unit ReportHrsPerContractGrpCUMDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, DBClient, Provider;

type
  TReportHrsPerContractGrpCUMDM = class(TReportBaseDM)
    QueryProdHour: TQuery;
    DataSourceProd: TDataSource;
    QueryBUPerWK: TQuery;
    dspBUPerWK: TDataSetProvider;
    cdsBUPerWK: TClientDataSet;
    QueryEmpl: TQuery;
    dspEmpl: TDataSetProvider;
    cdsEmpl: TClientDataSet;
    qryProdHourSum: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProdHourFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportHrsPerContractGrpCUMDM: TReportHrsPerContractGrpCUMDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportHrsPerContractGrpCUMDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryProdHour);
end;

procedure TReportHrsPerContractGrpCUMDM.QueryProdHourFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
