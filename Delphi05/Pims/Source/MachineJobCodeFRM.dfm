inherited MachineJobCodeF: TMachineJobCodeF
  Left = 270
  Top = 135
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Machine Job codes'
  ClientHeight = 473
  ClientWidth = 695
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 695
    Height = 15
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 11
      Width = 693
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 693
      Height = 10
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'WorkSpot'
        end>
      Visible = False
      ShowBands = True
      object dxMasterGridColumn12: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 147
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        DisableEditor = True
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxMasterGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumn3: TdxDBGridLookupColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTLU'
      end
      object dxMasterGridColumn4: TdxDBGridLookupColumn
        Caption = 'Hour type'
        DisableEditor = True
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
      end
      object dxMasterGridColumn8: TdxDBGridDateColumn
        Caption = 'Date inative'
        DisableEditor = True
        MinWidth = 16
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
        DateValidation = True
      end
      object dxMasterGridColumn11: TdxDBGridCheckColumn
        Caption = 'Use job codes'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USE_JOBCODE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn5: TdxDBGridCheckColumn
        Caption = 'Automatic data colection'
        DisableEditor = True
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_DATACOL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn6: TdxDBGridCheckColumn
        Caption = 'Counter value'
        DisableEditor = True
        Width = 76
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_VALUE_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn7: TdxDBGridCheckColumn
        Caption = 'Enter scan counter '
        DisableEditor = True
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn9: TdxDBGridCheckColumn
        Caption = 'Measure productivity'
        DisableEditor = True
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MEASURE_PRODUCTIVITY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxMasterGridColumn10: TdxDBGridCheckColumn
        Caption = 'Productive hour'
        DisableEditor = True
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRODUCTIVE_HOUR_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 230
    Width = 695
    Height = 243
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox2: TGroupBox
      Left = 1
      Top = 1
      Width = 693
      Height = 241
      Align = alClient
      Caption = 'Machine Job codes'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 75
        Width = 43
        Height = 13
        Caption = 'Job code'
      end
      object Label2: TLabel
        Left = 8
        Top = 99
        Width = 62
        Height = 13
        Caption = 'Business unit'
      end
      object Label7: TLabel
        Left = 8
        Top = 155
        Width = 63
        Height = 13
        Caption = 'Date inactive'
      end
      object Label8: TLabel
        Left = 8
        Top = 25
        Width = 50
        Height = 13
        Caption = 'Plant code'
      end
      object Label9: TLabel
        Left = 8
        Top = 51
        Width = 65
        Height = 13
        Caption = 'Machine code'
      end
      object DBEditJobCodes: TDBEdit
        Tag = 1
        Left = 86
        Top = 74
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'JOB_CODE'
        DataSource = MachineDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBEditDescription: TDBEdit
        Tag = 1
        Left = 150
        Top = 74
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = MachineDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 1
      end
      object dxDBDateEditInactiveDate: TdxDBDateEdit
        Left = 86
        Top = 151
        Width = 113
        Style.BorderStyle = xbs3D
        TabOrder = 2
        DataField = 'DATE_INACTIVE'
        DataSource = MachineDM.DataSourceJobCode
      end
      object DBEditPlant: TDBEdit
        Tag = 1
        Left = 86
        Top = 23
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'PLANT_CODE'
        DataSource = MachineDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 3
      end
      object DBEditWK: TDBEdit
        Tag = 1
        Left = 86
        Top = 49
        Width = 59
        Height = 19
        Ctl3D = False
        DataField = 'MACHINE_CODE'
        DataSource = MachineDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 4
      end
      object DBEditPlantDesc: TDBEdit
        Tag = 1
        Left = 150
        Top = 23
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = MachineDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 5
      end
      object DBEditWKDesc: TDBEdit
        Tag = 1
        Left = 150
        Top = 49
        Width = 211
        Height = 19
        Ctl3D = False
        DataField = 'DESCRIPTION'
        DataSource = MachineDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        TabOrder = 6
      end
      object GroupBox1: TGroupBox
        Left = 456
        Top = 17
        Width = 225
        Height = 96
        Caption = 'Levels per hour'
        TabOrder = 7
        object Label3: TLabel
          Left = 9
          Top = 15
          Width = 104
          Height = 13
          Caption = 'Norm production level'
        end
        object Label4: TLabel
          Left = 11
          Top = 43
          Width = 54
          Height = 13
          Caption = 'Bonus level'
        end
        object Label5: TLabel
          Left = 12
          Top = 71
          Width = 102
          Height = 13
          Caption = 'Norm machine output'
        end
        object dxDBMaskEdit1: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 12
          Width = 73
          Style.BorderStyle = xbsSingle
          TabOrder = 0
          DataField = 'NORM_PROD_LEVEL'
          DataSource = MachineDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEdit2: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 40
          Width = 73
          Style.BorderStyle = xbsSingle
          TabOrder = 1
          DataField = 'BONUS_LEVEL'
          DataSource = MachineDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
        object dxDBMaskEdit3: TdxDBMaskEdit
          Tag = 1
          Left = 128
          Top = 68
          Width = 73
          Style.BorderStyle = xbsSingle
          TabOrder = 2
          DataField = 'NORM_OUTPUT_LEVEL'
          DataSource = MachineDM.DataSourceJobCode
          IgnoreMaskBlank = False
          MaxLength = 6
          StoredValues = 2
        end
      end
      object DBCheckBox1: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 183
        Width = 179
        Height = 17
        Caption = 'Show job code at scanning'
        Ctl3D = False
        DataField = 'SHOW_AT_SCANNING_YN'
        DataSource = MachineDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 8
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBox2: TDBCheckBox
        Tag = 1
        Left = 86
        Top = 207
        Width = 267
        Height = 17
        Caption = 'Show job code at production screen'
        Ctl3D = False
        DataField = 'SHOW_AT_PRODUCTIONSCREEN_YN'
        DataSource = MachineDM.DataSourceJobCode
        ParentCtl3D = False
        TabOrder = 9
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBLookupComboBoxBusinessUnit: TDBLookupComboBox
        Tag = 1
        Left = 86
        Top = 101
        Width = 275
        Height = 19
        Ctl3D = False
        DataField = 'BUSINESSUNIT_CODE'
        DataSource = MachineDM.DataSourceJobCode
        DropDownWidth = 243
        KeyField = 'BUSINESSUNIT_CODE'
        ListField = 'DESCRIPTION;BUSINESSUNIT_CODE'
        ListSource = MachineDM.DataSourceBULU
        ParentCtl3D = False
        TabOrder = 10
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 41
    Width = 695
    Height = 189
    inherited spltDetail: TSplitter
      Top = 185
      Width = 693
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 693
      Height = 184
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Job codes'
        end>
      KeyField = 'JOB_CODE'
      DataSource = MachineDM.DataSourceJobCode
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Code'
        Width = 45
        BandIndex = 0
        RowIndex = 0
        FieldName = 'JOB_CODE'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 131
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridLookupColumn
        Caption = 'Business unit'
        Width = 105
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BULU'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Bonus level'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_LEVEL'
      end
      object dxDetailGridColumn5: TdxDBGridColumn
        Caption = 'Norm production level'
        Width = 113
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_PROD_LEVEL'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Caption = 'Norm machine output'
        Width = 109
        BandIndex = 0
        RowIndex = 0
        FieldName = 'NORM_OUTPUT_LEVEL'
      end
      object dxDetailGridColumn9: TdxDBGridDateColumn
        Caption = 'Date inactive'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 744
  end
  inherited dsrcActive: TDataSource
    Left = 8
    Top = 80
  end
end
