inherited ReportEmplDM: TReportEmplDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 40
    Top = 8
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object TableEmployee: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EMPLOYEE'
    Left = 40
    Top = 64
    object TableEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableEmployeeSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableEmployeeDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableEmployeeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableEmployeeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableEmployeeADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableEmployeeZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableEmployeeCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableEmployeeSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableEmployeeLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableEmployeePHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableEmployeeEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableEmployeeDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableEmployeeSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableEmployeeSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableEmployeeTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableEmployeeENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableEmployeeCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableEmployeeIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableEmployeeCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableEmployeeREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableEmployeeCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, E.NAME, E.SHORT_NAME,'
      '  E.ADDRESS, E.ZIPCODE, E.CITY, '
      '  E.SOCIALSECURITYNUMBER, E.CONTRACTGROUPCODE'
      'FROM '
      '  EMPLOYEE E '
      'WHERE'
      '  E.PLANT_CODE  >= :PLANTFROM AND '
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  E.EMPLOYEE_NUMBER  >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO ')
    Left = 40
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object DataSourceQryEmpl: TDataSource
    DataSet = QueryEmpl
    Left = 144
    Top = 128
  end
  object DataSourceEmpl: TDataSource
    DataSet = TableEmployee
    Left = 144
    Top = 64
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 144
    Top = 8
  end
end
