unit ReportQualityIncCalculationDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportQualityIncCalculationDM = class(TReportBaseDM)
    QueryWork: TQuery;
    QueryReportDSDay: TQuery;
    QueryReportDSDayEMPLOYEE_NUMBER: TIntegerField;
    QueryReportDSDayDESCRIPTION: TStringField;
    QueryReportDSDayHDAY1: TIntegerField;
    QueryReportDSDayMDAY1: TIntegerField;
    QueryReportDSDayHDAY2: TIntegerField;
    QueryReportDSDayMDAY2: TIntegerField;
    QueryReportDSDayHDAY3: TIntegerField;
    QueryReportDSDayMDAY3: TIntegerField;
    QueryReportDSDayHDAY4: TIntegerField;
    QueryReportDSDayMDAY4: TIntegerField;
    QueryReportDSDayHDAY5: TIntegerField;
    QueryReportDSDayMDAY5: TIntegerField;
    QueryReportDSDayHDAY6: TIntegerField;
    QueryReportDSDayMDAY7: TIntegerField;
    QueryReportDSDayHDAY7: TIntegerField;
    QueryReportDSDayHTOTAL: TIntegerField;
    QueryReportDSDayMTOTAL: TIntegerField;
    QueryReportDSDayMDAY6: TIntegerField;
    QueryReportDSTotal: TQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    QueryBonusPerHour: TQuery;
    QueryBonusPerHourMISTAKE: TIntegerField;
    QueryBonusPerHourBONUS_PER_HOUR: TCurrencyField;
    QueryReportDSDayBONUS: TFloatField;
    QueryReportDSTotalBONUS: TFloatField;
    QueryIns: TQuery;
    TableOverTime: TTable;
    TableHT: TTable;
    procedure QueryReportDSDayCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryReportDSTotalCalcFields(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    StartWeekDate: TdateTime;
    FCalculateWithOvertimeBonus: Boolean;
    procedure SetCalculateWithOvertimeBonus(Value: Boolean);
  public
    { Public declarations }
    property CalculateWithOvertimeBonus: Boolean
      read FCalculateWithOvertimeBonus write SetCalculateWithOvertimeBonus;
    function DeleteOldQualityRows(const PlantFrom, PlantTo: String;
    	const Year, Week: Integer): boolean;
    procedure PopulateReportQuery(PerDay: Boolean; PlantFrom, PlantTo: String;
    	StartDate: TDateTime);
    function BonusCalculation(const Empl, Mistakes, SalaryMinutes: Integer;
      Overtime: Double): Double;
    function OvertimeCalculation(const
      SalaryMinutes: Integer; BonusPercentage: Double): Double;
    function SaveBonus(ActiveQuery: TQuery; Const Year,Week: integer): boolean;
  end;

var
  ReportQualityIncCalculationDM: TReportQualityIncCalculationDM;

implementation

uses SystemDMT;
{$R *.DFM}

{ TReportQualityIncCalculationDM }

procedure TReportQualityIncCalculationDM.SetCalculateWithOvertimeBonus(
  Value: Boolean);
begin
  FCalculateWithOvertimeBonus := Value;
end;

function TReportQualityIncCalculationDM.DeleteOldQualityRows(
  const PlantFrom, PlantTo: String; const Year, Week: Integer): boolean;
begin
  SystemDM.Pims.StartTransaction;
  try
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'DELETE FROM QUALITYINCENTIVE A ' +
    'WHERE A.QUALITYINCENTIVE_YEAR = :YEAR AND ' +
    'A.QUALITYINCENTIVE_WEEK = :WEEK AND ' +
    'A.EMPLOYEE_NUMBER IN (SELECT B.EMPLOYEE_NUMBER ' +
    'FROM EMPLOYEE B WHERE B.PLANT_CODE >= :PCODEFROM AND ' +
    'B.PLANT_CODE <= :PCODETO)'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('YEAR').asInteger := Year;
  QueryWork.ParamByName('WEEK').asInteger := Week;
  QueryWork.ParamByName('PCODEFROM').asString := PlantFrom;
  QueryWork.ParamByName('PCODETO').asString := PlantTo;
  QueryWork.ExecSql;
  SystemDM.Pims.Commit;
  result := true;
  except
    SystemDM.Pims.Rollback;
    result := false;
  end;
end;

procedure TReportQualityIncCalculationDM.PopulateReportQuery(
  PerDay: Boolean; PlantFrom, PlantTo: String; StartDate: TDateTime);
var
  SelectStr: String;
begin
  StartWeekDate := StartDate;
  QueryReportDSDay.Active := False;
  QueryReportDSTotal.Active := False;
  SelectStr :=
    'SELECT ' +
    '  A.EMPLOYEE_NUMBER, A.DESCRIPTION ' +
    'FROM ' +
    '  EMPLOYEE A ' +
    'WHERE ' +
    '  A.PLANT_CODE >= :PLANTFROM AND ' +
    '  A.PLANT_CODE <= :PLANTTO AND ' +
    '  A.EMPLOYEE_NUMBER IN ' +
    '  (SELECT ' +
    '    EMPLOYEE_NUMBER ' +
    '  FROM ' +
    '    MISTAKEPEREMPLOYEE ' +
    '  WHERE ' +
    '    NUMBER_OF_MISTAKE IS NOT NULL AND ' +
    '    MISTAKE_DATE >= :DAY1 AND ' +
    '    MISTAKE_DATE <= :DAY7 ' +
    '   ) ' +
    'ORDER BY ' +
    '  A.EMPLOYEE_NUMBER';
  if PerDay then
  begin
    QueryReportDSDay.Active := False;
    QueryReportDSDay.SQL.Clear;
    QueryReportDSDay.SQL.Add(SelectStr);
    QueryReportDSDay.Prepare;
    QueryReportDSDay.ParamByName('PLANTFROM').asString := PlantFrom;
    QueryReportDSDay.ParamByName('PLANTTO').asString := PlantTo;
    QueryReportDSDay.ParamByName('DAY1').asDateTime := StartDate;
    QueryReportDSDay.ParamByName('DAY7').asDateTime := StartDate + 6;
    QueryReportDSDay.Active := True;
  end
  else
  begin
    QueryReportDSTotal.Active := False;
    QueryReportDSTotal.SQL.Clear;
    QueryReportDSTotal.SQL.Add(SelectStr);
    QueryReportDSTotal.Prepare;
    QueryReportDSTotal.ParamByName('PLANTFROM').asString := PlantFrom;
    QueryReportDSTotal.ParamByName('PLANTTO').asString := PlantTo;
    QueryReportDSTotal.ParamByName('DAY1').asDateTime := StartDate;
    QueryReportDSTotal.ParamByName('DAY7').asDateTime := StartDate + 6;
    QueryReportDSTotal.Active := True;
  end;
end;

procedure TReportQualityIncCalculationDM.QueryReportDSDayCalcFields(DataSet: TDataSet);
var
  HTotal, MTotal, index: integer;
  Overtime: Double;
  WeekDates: array[1..7] of TDateTime;
begin
  inherited;
  Htotal := 0; MTotal := 0; Overtime := 0;
  for index:=1 to 7 do
    WeekDates[index] := StartWeekDate + index - 1;
  // Retrieve Mistakes
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  MISTAKE_DATE, NUMBER_OF_MISTAKE ' +
    'FROM ' +
    '  MISTAKEPEREMPLOYEE ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO AND ' +
    '  MISTAKE_DATE >= :STARTDATE AND ' +
    '  MISTAKE_DATE <= :ENDDATE'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger :=
  	DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger;
  QueryWork.ParamByName('STARTDATE').asDateTime := WeekDates[1];
  QueryWork.ParamByName('ENDDATE').asDateTime := WeekDates[7];
  QueryWork.Active := True;

  While not QueryWork.Eof do
  begin
    for index:=1 to 7 do
    begin
    	if (QueryWork.FieldByName('MISTAKE_DATE').asDateTime = WeekDates[index])
      	and (not QueryWork.FieldByName('NUMBER_OF_MISTAKE').IsNull) then
        begin
          DataSet.FieldByName(Format('MDAY%d',[index])).asInteger :=
            QueryWork.FieldByName('NUMBER_Of_MISTAKE').asInteger;
          MTotal := MTotal + QueryWork.FieldByName('NUMBER_Of_MISTAKE').asInteger;
       end;
    end;
    QueryWork.Next;
  end;
  // Retrieve Salary Minutes
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  S.SALARY_DATE, ' +
    '  SUM(S.SALARY_MINUTE) AS TOTSAL, ' +
    '  S.HOURTYPE_NUMBER, H.BONUS_PERCENTAGE ' +
    'FROM ' +
    '  SALARYHOURPEREMPLOYEE S, HOURTYPE H ' +
    'WHERE ' +
    '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' +
    '  AND S.EMPLOYEE_NUMBER = :EMPNO ' +
    '  AND S.SALARY_DATE >= :STARTDATE ' +
    '  AND S.SALARY_DATE <= :ENDDATE ' +
    'GROUP BY ' +
    '  S.SALARY_DATE, ' +
    '  S.HOURTYPE_NUMBER, H.BONUS_PERCENTAGE ' +
    'ORDER BY ' +
    '  S.SALARY_DATE'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger;
  QueryWork.ParamByName('STARTDATE').asDateTime := WeekDates[1];
  QueryWork.ParamByName('ENDDATE').asDateTime := WeekDates[7];
  QueryWork.Active := True;
  QueryWork.First;
  for index:=1 to 7 do
  begin
    while (not QueryWork.Eof) and
      (QueryWork.FieldByName('SALARY_DATE').asDateTime = WeekDates[index]) do
    begin
      DataSet.FieldByName(Format('HDAY%d',[index])).asInteger :=
        DataSet.FieldByName(Format('HDAY%d',[index])).asInteger +
          QueryWork.FieldByName('TOTSAL').asInteger;
      HTotal := HTotal + QueryWork.FieldByName('TOTSAL').asInteger;

      Overtime := Overtime +
        OvertimeCalculation(QueryWork.FieldByName('TOTSAL').asInteger,
          QueryWork.FieldByName('BONUS_PERCENTAGE').AsFloat);

      QueryWork.Next;
    end;
  end;
  QueryWork.Active := False;
  DataSet.FieldByName('HTOTAL').asInteger := HTotal;
  DataSet.FieldByName('MTOTAL').asInteger := MTotal;
  DataSet.FieldByName('BONUS').asFloat :=
    BonusCalculation(
      DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger, MTotal, HTotal,
        Overtime);
end;


procedure TReportQualityIncCalculationDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  QueryBonusPerHour.Active := true;
end;

function TReportQualityIncCalculationDM.BonusCalculation(const Empl, Mistakes,
  SalaryMinutes: Integer; Overtime: Double): Double;
var
  BonusPerHr: Double;
begin
  BonusPerHr := 0;
  QueryBonusPerHour.First;
  while not QueryBonusPerHour.Eof do
  begin
    if Mistakes < QueryBonusPerHour.FieldByName('MISTAKE').asInteger then
  	break;
    BonusPerHr := QueryBonusPerHour.FieldByName('BONUS_PER_HOUR').asFloat;
    QueryBonusPerHour.Next;
  end;
  result := BonusPerHr * ((SalaryMinutes + Overtime) / 60);
end;

function TReportQualityIncCalculationDM.OvertimeCalculation(const
  SalaryMinutes: Integer; BonusPercentage: Double): Double;
begin
  Result := 0;
  if CalculateWithOvertimeBonus then
    if BonusPercentage > 0 then
      Result := SalaryMinutes * BonusPercentage / 100;
end;

procedure TReportQualityIncCalculationDM.QueryReportDSTotalCalcFields(
  DataSet: TDataSet);
var
  HTotal, MTotal, Index: integer;
  Overtime: Double;
  WeekDates: Array[1..7] of TDateTime;
begin
  inherited;
  Htotal := 0;
  // MTotal := 0;
   Overtime := 0;
  // Retrieve Mistakes
  for index:=1 to 7 do
   WeekDates[index] := StartWeekDate + index - 1;
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  SUM(NUMBER_OF_MISTAKE) AS MTOTAL ' +
    'FROM ' +
    '  MISTAKEPEREMPLOYEE ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO AND ' +
    '  MISTAKE_DATE >= :STARTDATE AND ' +
    '  MISTAKE_DATE <= :ENDDATE'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger :=
  	DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger;
  QueryWork.ParamByName('STARTDATE').asDateTime := WeekDates[1];
  QueryWork.ParamByName('ENDDATE').asDateTime := WeekDates[7];
  QueryWork.Active := True;
  MTotal := QueryWork.FieldByName('MTOTAL').asInteger;

  // Retrieve Salary Minutes
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  SUM(S.SALARY_MINUTE) AS TOTSAL, ' +
    '  S.HOURTYPE_NUMBER, H.BONUS_PERCENTAGE ' +
    'FROM ' +
    '  SALARYHOURPEREMPLOYEE S, HOURTYPE H ' +
    'WHERE ' +
    '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' +
    '  AND S.EMPLOYEE_NUMBER = :EMPNO ' +
    '  AND S.SALARY_DATE >= :STARTDATE ' +
    '  AND S.SALARY_DATE <= :ENDDATE ' +
    'GROUP BY ' +
    '  S.HOURTYPE_NUMBER, H.BONUS_PERCENTAGE'
    );
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger;
  QueryWork.ParamByName('STARTDATE').asDateTime := WeekDates[1];
  QueryWork.ParamByName('ENDDATE').asDateTime := WeekDates[7];
  QueryWork.Active := True;

  QueryWork.First;
  while not QueryWork.Eof do
  begin
    HTotal := HTotal + QueryWork.FieldByName('TOTSAL').asInteger;

    Overtime := Overtime +
      OvertimeCalculation(QueryWork.FieldByName('TOTSAL').asInteger,
        QueryWork.FieldByName('BONUS_PERCENTAGE').AsFloat);

    QueryWork.Next;
  end;
  QueryWork.Active := False;
  DataSet.FieldByName('HTOTAL').asInteger := HTotal;
  DataSet.FieldByName('MTOTAL').asInteger := MTotal;
  DataSet.FieldByName('BONUS').asFloat :=
    BonusCalculation(
       DataSet.FieldByName('EMPLOYEE_NUMBER').asInteger, MTotal, HTotal,
         Overtime);
end;

function TReportQualityIncCalculationDM.SaveBonus(ActiveQuery: TQuery;
  Const Year, Week: integer): boolean;
begin
  result := True;
  if not ActiveQuery.Active or ActiveQuery.IsEmpty then exit;
  try
    SystemDM.Pims.StartTransaction;
    ActiveQuery.First;
    QueryWork.Active := False;
    QueryIns.SQL.Clear;
    QueryIns.SQL.Add(
      'INSERT INTO QUALITYINCENTIVE(EMPLOYEE_NUMBER, ' +
      'QUALITYINCENTIVE_YEAR,QUALITYINCENTIVE_WEEK,BONUS, ' +
      'CREATIONDATE,MUTATIONDATE,MUTATOR) ' +
      'VALUES (:EMPNO,:QYEAR,:QWEEK,:BONUS, :CDATE,:MDATE,:MUT)'
      );
    QueryIns.Prepare;
    while not ActiveQuery.Eof do
    begin
      QueryIns.Close;
      QueryIns.ParamByName('EMPNO').asInteger :=
        ActiveQuery.FieldByName('EMPLOYEE_NUMBER').asInteger;
      QueryIns.ParamByName('QYEAR').asInteger := Year;
      QueryIns.ParamByName('QWEEK').asInteger := Week;
      QueryIns.ParamByName('BONUS').asFloat :=
        ActiveQuery.FieldByName('BONUS').asFloat;
      QueryIns.ParamByName('CDATE').asDateTime := Now;
      QueryIns.ParamByName('MDATE').asDateTime := Now;
      QueryIns.ParamByName('MUT').asString := SystemDM.CurrentProgramUser;
      QueryIns.ExecSQL;
      ActiveQuery.Next;
    end;
    SystemDM.Pims.Commit;
  except
    SystemDM.Pims.Rollback;
    result := False;
  end;
end;

procedure TReportQualityIncCalculationDM.DataModuleDestroy(
  Sender: TObject);
begin
  inherited;
//  FListEmplHT.Free;
end;

end.
