inherited PaymentExportCodeDM: TPaymentExportCodeDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    Top = 40
    object TableMasterPAYMENT_EXPORT_CODE: TStringField
      FieldName = 'PAYMENT_EXPORT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
  end
  inherited TableDetail: TTable
    TableName = 'PAYMENTEXPORTCODE'
    object TableDetailPAYMENT_EXPORT_CODE: TStringField
      FieldName = 'PAYMENT_EXPORT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      OnValidate = DefaultNotEmptyValidate
    end
  end
end
