(*
  Changes:
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
  MRA:17-DEC-2010. RV083.3. SR-890041
  - Report Absence Hours - Dialog
    - The report dialog always shows all absence
      reasons for selection. Instead of that it must
      show absence reasons per country. This should be
      based on plant-from and plant-to that is selected.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogReportAbsenceFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportAbsenceF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    RadioGroupSort: TRadioGroup;
    GroupBoxShow: TGroupBox;
    CheckBoxDept: TCheckBox;
    CheckBoxEmpl: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusAbsReasonFrom: TComboBoxPlus;
    Label17: TLabel;
    Label18: TLabel;
    ComboBoxPlusAbsReasonTo: TComboBoxPlus;
    CheckBoxTeam: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageTeam: TCheckBox;
    CheckBoxPageEmpl: TCheckBox;
    QueryBU: TQuery;
    TableAbsReason: TTable;
    TableAbsReasonABSENCETYPE_CODE: TStringField;
    TableAbsReasonABSENCEREASON_CODE: TStringField;
    TableAbsReasonDESCRIPTION: TStringField;
    CheckBoxAllTeam: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    CheckBoxAllAbsReason: TCheckBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBoxEmpTeam: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label26: TLabel;
    Label27: TLabel;
    ComboBoxPlusAbsenceTypeFrom: TComboBoxPlus;
    Label28: TLabel;
    ComboBoxPlusAbsenceTypeTo: TComboBoxPlus;
    TableAbsenceType: TTable;
    TableAbsenceTypeABSENCETYPE_CODE: TStringField;
    TableAbsenceTypeDESCRIPTION: TStringField;
    LblFromDate: TLabel;
    LblDate: TLabel;
    DateFrom: TDateTimePicker;
    LblToDate: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxSuppressZeroes: TCheckBox;
    qryAbsenceReason: TQuery;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
    procedure CheckBoxAllAbsReasonClick(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxTeamClick(Sender: TObject);
    procedure SetEnabledDept(Status: Boolean);
    procedure CheckBoxEmplClick(Sender: TObject);
    procedure CheckBoxPageDeptClick(Sender: TObject);
    procedure CheckBoxPageTeamClick(Sender: TObject);
    procedure CheckBoxPageEmplClick(Sender: TObject);
//    procedure ComboBoxPlusTeamToChange(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsReasonFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsReasonToCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsenceTypeFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusAbsenceTypeToCloseUp(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure CheckBoxAllTeamsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure FillAbsenceReasons; // RV083.3.
  public
    { Public declarations }
  end;

var
  DialogReportAbsenceF: TDialogReportAbsenceF;

// RV089.1.
function DialogReportAbsenceForm: TDialogReportAbsenceF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportAbsenceDMT, ReportAbsenceQRPT, ListProcsFRM, UPimsMessageRes;

// RV089.1.
var
  DialogReportAbsenceF_HND: TDialogReportAbsenceF;

// RV089.1.
function DialogReportAbsenceForm: TDialogReportAbsenceF;
begin
  if (DialogReportAbsenceF_HND = nil) then
  begin
    DialogReportAbsenceF_HND := TDialogReportAbsenceF.Create(Application);
    with DialogReportAbsenceF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportAbsenceF_HND;
end;

// RV089.1.
procedure TDialogReportAbsenceF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportAbsenceF_HND <> nil) then
  begin
    DialogReportAbsenceF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportAbsenceF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportAbsenceF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportAbsenceQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(ComboBoxPlusAbsenceTypeFrom.Value),
      GetStrValue(ComboBoxPlusAbsenceTypeTo.Value),
      GetStrValue(ComboBoxPlusAbsReasonFrom.Value),
      GetStrValue(ComboBoxPlusAbsReasonTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      // MR:12-07-2004 Order 550321
      Trunc(DateFrom.DateTime),
      Trunc(DateTo.DateTime),
      RadioGroupSort.ItemIndex,
      CheckBoxAllAbsReason.Checked, CheckBoxAllTeams.Checked,
      CheckBoxDept.Checked, CheckBoxTeam.Checked, CheckBoxEmpl.Checked,
      CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
      CheckBoxPageDept.Checked, CheckBoxPageTeam.Checked,
      CheckBoxPageEmpl.Checked,
      CheckBoxEmpTeam.Checked,
      CheckBoxSuppressZeroes.Checked,
      CheckBoxExport.Checked)
  then
    ReportAbsenceQR.ProcessRecords;
end;

procedure TDialogReportAbsenceF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

procedure TDialogReportAbsenceF.FormShow(Sender: TObject);
var
//  Year, Week: Word;
  Year, Month, Day: Word;
begin
  InitDialog(True, True, True, True, False, False, False);
  inherited;
  FillBusiness;
//show abencetype
  ListProcsF.FillComboBoxMaster(TableAbsenceType, 'ABSENCETYPE_CODE',
    True, ComboBoxPlusAbsenceTypeFrom);
  ListProcsF.FillComboBoxMaster(TableAbsenceType, 'ABSENCETYPE_CODE',
    False, ComboBoxPlusAbsenceTypeTo);
//show absencereason
  // RV083.3.
{
  ListProcsF.FillComboBoxMaster(TableAbsReason, 'ABSENCEREASON_CODE',
    True, ComboBoxPlusAbsReasonFrom);
  ListProcsF.FillComboBoxMaster(TableAbsReason, 'ABSENCEREASON_CODE',
    False, ComboBoxPlusAbsReasonTo);
}
  FillAbsenceReasons;
//end absreason
//show team
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}
//end team

  // MR:12-07-2004 Order 550321
  // Use From Date to Date instead of Year and Week From To.
  DecodeDate(Now, Year, Month, Day);
  DateFrom.DateTime := Trunc(Now); // current
  DateTo.DateTime := Trunc(EncodeDate(Year, 12, 31)); // end of the year

  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := True;
  CheckBoxDept.Checked := False;
  CheckBoxTeam.Checked := False;
  CheckBoxEmpl.Checked := False;

//  CheckBoxAllTeam.Checked := False;
  CheckBoxAllAbsReason.Checked := False;
  RadioGroupSort.ItemIndex := 0;
end;

procedure TDialogReportAbsenceF.FormCreate(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;
  ListProcsF.WeekUitDat(Now, Year, Week);
// CREATE data module of report
  ReportAbsenceDM := CreateReportDM(TReportAbsenceDM);
  ReportAbsenceQR := CreateReportQR(TReportAbsenceQR);
  ReportAbsenceQR.qrptBase.ShowProgress := False;
end;

procedure TDialogReportAbsenceF.CheckBoxAllAbsReasonClick(Sender: TObject);
begin
  inherited;
  if CheckBoxAllAbsReason.Checked then
  begin
    ComboBoxPlusAbsReasonFrom.Visible := False;
    ComboBoxPlusAbsReasonTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusAbsReasonFrom.Visible := True;
    ComboBoxPlusAbsReasonTo.Visible := True;
  end
end;

procedure TDialogReportAbsenceF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
{
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
    CheckBoxEmpTeam.Checked := True;
    CheckBoxEmpTeam.Enabled := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
    CheckBoxEmpTeam.Enabled := True;
  end
}  
end;

procedure TDialogReportAbsenceF.CheckBoxDeptClick(Sender: TObject);
  procedure SetTeamEnabled(Status: Boolean);
  begin
    CheckBoxTeam.Enabled := Status;
    CheckBoxTeam.Checked := False;
    CheckBoxPageTeam.Enabled := Status;
    CheckBoxPageTeam.Checked := False;
  end;
begin
  inherited;
  SetTeamEnabled(not CheckBoxDept.Checked);
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxDept.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxDept.Checked;
end;

procedure TDialogReportAbsenceF.SetEnabledDept(Status: Boolean);
begin
  CheckBoxDept.Checked := False;
  CheckBoxDept.Enabled := Status;
  CheckBoxPageDept.Checked := False;
  CheckBoxPageDept.Enabled := Status;
end;

procedure TDialogReportAbsenceF.CheckBoxTeamClick(Sender: TObject);
begin
  inherited;
  SetEnabledDept(not CheckBoxTeam.Checked);
  CheckBoxPageTeam.Enabled := CheckBoxTeam.Checked;
  CheckBoxPageTeam.Checked := CheckBoxTeam.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxTeam.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxTeam.Checked;
end;

procedure TDialogReportAbsenceF.CheckBoxEmplClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageEmpl.Enabled := CheckBoxEmpl.Checked;
  CheckBoxPageEmpl.Checked := CheckBoxEmpl.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxEmpl.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxEmpl.Checked;
end;

procedure TDialogReportAbsenceF.CheckBoxPageDeptClick(Sender: TObject);
begin
  inherited;
  CheckBoxPagePlant.Enabled := not CheckBoxPageDept.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxPageDept.Checked;
end;

procedure TDialogReportAbsenceF.CheckBoxPageTeamClick(Sender: TObject);
begin
  inherited;
  CheckBoxPagePlant.Enabled := not CheckBoxPageTeam.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxPageTeam.Checked;
end;

procedure TDialogReportAbsenceF.CheckBoxPageEmplClick(Sender: TObject);
begin
  inherited;
  CheckBoxPagePlant.Enabled := not CheckBoxPageEmpl.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxPageEmpl.Checked;
  CheckBoxPageDept.Enabled := not CheckBoxPageEmpl.Checked;
  CheckBoxPageDept.Checked := not CheckBoxPageEmpl.Checked;
  CheckBoxPageTeam.Enabled := not CheckBoxPageEmpl.Checked;
  CheckBoxPageTeam.Checked := not CheckBoxPageEmpl.Checked;
end;

procedure TDialogReportAbsenceF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  FillBusiness;
  FillAbsenceReasons;
end;

procedure TDialogReportAbsenceF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  FillBusiness;
  FillAbsenceReasons;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessTo.DisplayValue :=
        ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessFrom.DisplayValue :=
        ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusAbsenceTypeFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsenceTypeFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsenceTypeTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsenceTypeFrom.Value) >
      GetStrValue(ComboBoxPlusAbsenceTypeTo.Value) then
        ComboBoxPlusAbsenceTypeTo.DisplayValue :=
          ComboBoxPlusAbsenceTypeFrom.DisplayValue;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusAbsenceTypeToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsenceTypeFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsenceTypeTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsenceTypeFrom.Value) >
      GetStrValue(ComboBoxPlusAbsenceTypeTo.Value) then
        ComboBoxPlusAbsenceTypeFrom.DisplayValue :=
          ComboBoxPlusAbsenceTypeTo.DisplayValue;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusAbsReasonFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsReasonFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsReasonTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsReasonFrom.Value) >
      GetStrValue(ComboBoxPlusAbsReasonTo.Value) then
        ComboBoxPlusAbsReasonTo.DisplayValue :=
          ComboBoxPlusAbsReasonFrom.DisplayValue;
end;

procedure TDialogReportAbsenceF.ComboBoxPlusAbsReasonToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusAbsReasonFrom.DisplayValue <> '') and
     (ComboBoxPlusAbsReasonTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusAbsReasonFrom.Value) >
      GetStrValue(ComboBoxPlusAbsReasonTo.Value) then
        ComboBoxPlusAbsReasonFrom.DisplayValue :=
          ComboBoxPlusAbsReasonTo.DisplayValue;
end;

procedure TDialogReportAbsenceF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
    DateTo.Date := DateFrom.Date;
end;

procedure TDialogReportAbsenceF.DateToChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
    DateFrom.Date := DateTo.Date;
end;

procedure TDialogReportAbsenceF.CheckBoxAllTeamsClick(Sender: TObject);
begin
  inherited;
  if CheckBoxAllTeams.Checked then
  begin
//    ComboBoxPlusTeamFrom.Visible := False;
//    ComboBoxPlusTeamTo.Visible := False;
    CheckBoxEmpTeam.Checked := True;
    CheckBoxEmpTeam.Enabled := False;
  end
  else
  begin
//    ComboBoxPlusTeamFrom.Visible := True;
//    ComboBoxPlusTeamTo.Visible := True;
    CheckBoxEmpTeam.Enabled := True;
  end
end;

// RV083.3.
procedure TDialogReportAbsenceF.FillAbsenceReasons;
var
  CountryId: Integer;
begin
  // When plant from-to is the same, then filter on absence reason per country.
  CountryId := 0;
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CountryId := SystemDM.GetDBValue(
      ' SELECT NVL(P.COUNTRY_ID, 0) ' +
      ' FROM PLANT P ' +
      ' WHERE ' +
      '   P.PLANT_CODE =  ' + '''' +
      GetStrValue(CmbPlusPlantFrom.Value) + '''', 0);
  end;
  SystemDM.UpdateAbsenceReasonPerCountry(
    CountryId,
    qryAbsenceReason,
    'CODE'
  );
  ListProcsF.FillComboBoxMaster(qryAbsenceReason, 'ABSENCEREASON_CODE',
    True, ComboBoxPlusAbsReasonFrom);
  ListProcsF.FillComboBoxMaster(qryAbsenceReason, 'ABSENCEREASON_CODE',
    False, ComboBoxPlusAbsReasonTo);
end;

end.
