(*
  Changes:
    MRA:10-JUN-2011. RV093.6. 20011796
    - New report Holiday Card. Shows holiday and other types in card-report.
      With totals at the end and totals of availability (when possible).
    MRA:8-DEC-2011. RV103.2. 20011796. Addition.
    - Added parameters that will give the descriptions of the absence-checkboxes
      to the report.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogReportHolidayCardFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT, jpeg;

type
  TDialogReportHolidayCardF = class(TDialogReportBaseF)
    GroupBoxShow: TGroupBox;
    Label5: TLabel;
    GroupBoxSelections: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    QueryBU: TQuery;
    TableAbsReason: TTable;
    TableAbsReasonABSENCETYPE_CODE: TStringField;
    TableAbsReasonABSENCEREASON_CODE: TStringField;
    TableAbsReasonDESCRIPTION: TStringField;
    CheckBoxAllTeam: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    TableAbsenceType: TTable;
    TableAbsenceTypeABSENCETYPE_CODE: TStringField;
    TableAbsenceTypeDESCRIPTION: TStringField;
    LblFromDate: TLabel;
    LblDate: TLabel;
    DateFrom: TDateTimePicker;
    LblToDate: TLabel;
    DateTo: TDateTimePicker;
    qryAbsenceReason: TQuery;
    CheckBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    CheckBoxHoliday: TCheckBox;
    CheckBoxWorkTimeReduction: TCheckBox;
    CheckBoxTimeForTime: TCheckBox;
    CheckBoxSeniorityHoliday: TCheckBox;
    CheckBoxAddBankHoliday: TCheckBox;
    CheckBoxRsvBankHoliday: TCheckBox;
    CheckBoxShorterWWeek: TCheckBox;
    CheckBoxBankHoliday: TCheckBox;
    qryAbsenceTypePerCountryExist: TQuery;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { private declarations }
    CheckBoxHolidayCaption, CheckBoxWorkTimeReductionCaption,
    CheckBoxTimeForTimeCaption: String;
    procedure SetCheckBoxesVisible(AVisible: Boolean);
    function AbsenceTypePerCountryExist: Boolean;
    function GetCounterActivated(AAbsenceType: Char;
      var ACounterDescription: String): Boolean;
  public
    { Public declarations }
  end;

var
  DialogReportHolidayCardF: TDialogReportHolidayCardF;

function DialogReportHolidayCardForm: TDialogReportHolidayCardF;

implementation

{$R *.DFM}

uses
  ReportHolidayCardDMT, ReportHolidayCardQRPT, ListProcsFRM,
  UPimsMessageRes, UPimsConst;

var
  DialogReportHolidayCardF_HND: TDialogReportHolidayCardF;

function DialogReportHolidayCardForm: TDialogReportHolidayCardF;
begin
  if (DialogReportHolidayCardF_HND = nil) then
  begin
    DialogReportHolidayCardF_HND := TDialogReportHolidayCardF.Create(Application);
    with DialogReportHolidayCardF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportHolidayCardF_HND;
end;

procedure TDialogReportHolidayCardF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportHolidayCardF_HND <> nil) then
  begin
    DialogReportHolidayCardF_HND := nil;
  end;
end;

procedure TDialogReportHolidayCardF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportHolidayCardF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportHolidayCardQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Trunc(DateFrom.DateTime),
      Trunc(DateTo.DateTime),
      CheckBoxAllTeams.Checked,
      CheckBoxAllDepartments.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxHoliday.Checked,
      CheckBoxWorkTimeReduction.Checked,
      CheckBoxTimeForTime.Checked,
      CheckBoxSeniorityHoliday.Checked,
      CheckBoxAddBankHoliday.Checked,
      CheckBoxRsvBankHoliday.Checked,
      CheckBoxShorterWWeek.Checked,
      CheckBoxBankHoliday.Checked,
      CheckBoxHoliday.Caption,
      CheckBoxWorkTimeReduction.Caption,
      CheckBoxTimeForTime.Caption,
      CheckBoxSeniorityHoliday.Caption,
      CheckBoxAddBankHoliday.Caption,
      CheckBoxRsvBankHoliday.Caption,
      CheckBoxShorterWWeek.Caption,
      CheckBoxBankHoliday.Caption,
      CheckBoxExport.Checked)
  then
    ReportHolidayCardQR.ProcessRecords;
end;

procedure TDialogReportHolidayCardF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

procedure TDialogReportHolidayCardF.FormShow(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  InitDialog(True, True, True, True, False, False, False);
  inherited;
  FillBusiness;

  DecodeDate(Now, Year, Month, Day);
  DateFrom.DateTime := Trunc(EncodeDate(Year, 1, 1)); // start of the year
  DateTo.DateTime := Trunc(Now); // till now

//  CheckBoxShowSelection.Checked := True;

  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
   );
end;

procedure TDialogReportHolidayCardF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportHolidayCardDM := CreateReportDM(TReportHolidayCardDM);
  ReportHolidayCardQR := CreateReportQR(TReportHolidayCardQR);
  ReportHolidayCardQR.qrptBase.ShowProgress := False;
  CheckBoxHolidayCaption := CheckBoxHoliday.Caption;
  CheckBoxWorkTimeReductionCaption := CheckBoxWorkTimeReduction.Caption;
  CheckBoxTimeForTimeCaption := CheckBoxTimeForTime.Caption;
end;

procedure TDialogReportHolidayCardF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  );
  FillBusiness;
end;

procedure TDialogReportHolidayCardF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  SetCheckBoxesVisible(
     (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  );
  FillBusiness;
end;

procedure TDialogReportHolidayCardF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessTo.DisplayValue :=
        ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportHolidayCardF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
      ComboBoxPlusBusinessFrom.DisplayValue :=
        ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportHolidayCardF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
    DateTo.Date := DateFrom.Date;
end;

procedure TDialogReportHolidayCardF.DateToChange(Sender: TObject);
begin
  inherited;
  if DateFrom.Date > DateTo.Date then
    DateFrom.Date := DateTo.Date;
end;

function TDialogReportHolidayCardF.GetCounterActivated(AAbsenceType: Char;
  var ACounterDescription: String): Boolean;
var
  CountryId: Integer;
  StrValue: String;
begin
  Result := False;
  if Trim(CmbPlusPlantFrom.Value) = '' then Exit;

  CountryId := SystemDM.GetDBValue(
    Format(
    'select p.country_id from plant p where plant_code = %s',
    [GetStrValue(CmbPlusPlantFrom.Value)]
    ), 0
  );

  if CountryId = 0 then Exit;

  Result := SystemDM.GetCounterActivated(CountryId, AAbsenceType);

  if Result then
  begin
    StrValue := SystemDM.GetDBValue(
      Format(
      'select ac.counter_description from absencetypepercountry ac ' +
      'where country_id = %d and absencetype_code = ''%s''',
      [CountryId, AAbsenceType]
      ), '*'
    );
    if StrValue <> '*' then
      ACounterDescription := StrValue;
  end;
end;

function TDialogReportHolidayCardF.AbsenceTypePerCountryExist: Boolean;
var
  CountryId: Integer;
begin
  Result := False;
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CountryId := SystemDM.GetDBValue(
      ' select NVL(p.country_id, 0) from ' +
      ' plant p ' +
      ' where ' +
      '   p.plant_code = ' + '''' + GetStrValue(CmbPlusPlantFrom.Value) + '''',
      0
    );
    if CountryID <> 0 then
    begin
      with qryAbsenceTypePerCountryExist do
      begin
        Close;
        ParamByName('COUNTRY_ID').AsInteger :=
          CountryID;
        Open;
        Result := not Eof;
        Close;
      end;
    end;
  end;
end;

procedure TDialogReportHolidayCardF.SetCheckBoxesVisible(
  AVisible: Boolean);
  procedure SetVisible(ACk: TCheckbox; AAbsenceType: Char);
  var
    Desc: String;
    ACheckBoxVisible: Boolean;
  begin
    if AVisible then
    begin
      ACheckBoxVisible := AVisible and GetCounterActivated(AAbsenceType, Desc);
      if ACheckBoxVisible then
      begin
        ACk.Visible := True;
        ACk.Caption := Desc;
      end
      else
      begin
        ACk.Visible := False;
        ACk.Checked := False;
      end;
    end
    else
    begin
      ACk.Visible := False;
      ACk.Checked := False;
    end;
  end;
begin
  if AbsenceTypePerCountryExist then
  begin
    // RV067.MRA.29
    SetVisible(CheckBoxHoliday, HOLIDAYAVAILABLETB);
    SetVisible(CheckBoxWorkTimeReduction, WORKTIMEREDUCTIONAVAILABILITY);
    SetVisible(CheckBoxTimeForTime, TIMEFORTIMEAVAILABILITY);
    // New counters.
//    SetVisible(CheckBoxPrevYearHoliday, HOLIDAYPREVIOUSYEARAVAILABILITY);
    SetVisible(CheckBoxSeniorityHoliday, SENIORITYHOLIDAYAVAILABILITY);
    SetVisible(CheckBoxAddBankHoliday, ADDBANKHOLIDAYAVAILABILITY);
    // RV071.11. 550497 Sat Credit.
//    SetVisible(CheckBoxSaturdayCredit, SATCREDITAVAILABILITY);
    SetVisible(CheckBoxRsvBankHoliday, BANKHOLIDAYRESERVEAVAILABILITY);
    SetVisible(CheckBoxShorterWWeek, SHORTERWORKWEEKAVAILABILITY);
    SetVisible(CheckBoxBankHoliday, RSNB);
  end
  else
  begin
    // RV067.MRA.29
    // These must be visible when there are NO 'per-country'-definitions.
    CheckBoxHoliday.Caption := CheckBoxHolidayCaption;
    CheckBoxHoliday.Visible := True;
    CheckBoxHoliday.Checked := True;
    CheckBoxWorkTimeReduction.Caption := CheckBoxWorkTimeReductionCaption;
    CheckBoxWorkTimeReduction.Visible := True;
    CheckBoxWorkTimeReduction.Checked := False;
    CheckBoxTimeForTime.Caption := CheckBoxTimeForTimeCaption;
    CheckBoxTimeForTime.Visible := True;
    CheckBoxTimeForTime.Checked := True;

    CheckBoxBankHoliday.Visible := False;
    CheckBoxBankHoliday.Checked := False;
    // New counters. These are not default visible.
//    CheckBoxPrevYearHoliday.Visible := False;
//    CheckBoxPrevYearHoliday.Checked := False;
    CheckBoxSeniorityHoliday.Visible := False;
    CheckBoxSeniorityHoliday.Checked := False;
    CheckBoxAddBankHoliday.Visible := False;
    CheckBoxAddBankHoliday.Checked := False;
    // RV071.11. 550497 Sat Credit.
//    CheckBoxSaturdayCredit.Visible := False;
//    CheckBoxSaturdayCredit.Checked := False;
    CheckBoxRsvBankHoliday.Visible := False;
    CheckBoxRsvBankHoliday.Checked := False;
    CheckBoxShorterWWeek.Visible := False;
    CheckBoxShorterWWeek.Checked := False;
  end;
end;

end.
