(*
  MRA:27-AUG-2010. RV067.MRA.12. Bugfix.
    - When trying to change the Line-field, an error occurs about:
    Example of message:
      ORA-04091: table SYSTEM.TEST1 is mutating, trigger/function may not see it
      ORA-06512: at �SYSTEM.MUTAT_TRIG�, line 4
      ORA-04088: error during execution of trigger �SYSTEM.MUTAT_TRIG�
    - Problem: It did a query on the mutated row, which is not allowed.
    - Problem: The trigger did a query.
    Solved by removing 2 triggers for OVERTIMEDEFINITION-table, because
    the needed checks are already done here.
  MRA:27-APR-2011. RV091.1. SO-20011677.
  - Copy/Paste option for Overtime definitions.
  MRA:12-FEB-2018 GLOB3-81
  - Change for overtime hours (only used for NTS)
  - Add from-to-time as extra restriction for day-period
  - Add day-of-week as extra setting.
  - Related to this order: Display of description made larger of hourtype,
    it was 20, but should be 30 (TableDetailHOURTYPELU).
  MRA:3-SEP-2018 PIM-395
  - Wrong overtime at SPO (this was not a bug, but could be solved by changing
    the overtime-definition itself)
  - Note: They use the overtime-method made for NTS
  - Removed check for start/endtime based on previous overtime-lines, when
    using NTS-overtime-method, because that does not work with this method.
  MRA:22-OCT-2018 GLOB3-172
  - Overtime definition/calculation issues
  - Copy/Paste was wrong: Field DAY_OF_WEEK was missing.
  MRA:22-FEB-2019 GLOB3-223
  - Restructure availability of functionality made for NTS
*)

unit OvertimeDefDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TOvertimeDefDM = class(TGridBaseDM)
    TableHourType: TTable;
    DataSourceHourType: TDataSource;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterTIME_FOR_TIME_YN: TStringField;
    TableMasterBONUS_IN_MONEY_YN: TStringField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableMasterMUTATOR: TStringField;
    TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableMasterWEEKS_IN_PERIOD: TIntegerField;
    TableMasterWORK_TIME_REDUCTION_YN: TStringField;
    TableDetailLINE_NUMBER: TIntegerField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableHourTypeHOURTYPE_NUMBER: TIntegerField;
    TableHourTypeDESCRIPTION: TStringField;
    TableHourTypeCREATIONDATE: TDateTimeField;
    TableHourTypeOVERTIME_YN: TStringField;
    TableHourTypeMUTATIONDATE: TDateTimeField;
    TableHourTypeMUTATOR: TStringField;
    TableHourTypeCOUNT_DAY_YN: TStringField;
    TableHourTypeEXPORT_CODE: TStringField;
    TableDetailHOURTYPELU: TStringField;
    TableHourTypeBONUS_PERCENTAGE: TFloatField;
    TableDetailSTARTTIME: TIntegerField;
    TableDetailENDTIME: TIntegerField;
    TableDetailSTARTHOURMIN: TStringField;
    TableDetailENDHOURMIN: TStringField;
    TableHourTypeIGNORE_FOR_OVERTIME_YN: TStringField;
    TableHourTypeMINIMUM_WAGE: TFloatField;
    TableHourTypeWAGE_BONUS_ONLY_YN: TStringField;
    qryOvertimeDef: TQuery;
    TableMasterCALC_OVERTIMETYPE: TStringField;
    TableDetailFROMTIME: TDateTimeField;
    TableDetailTOTIME: TDateTimeField;
    TableDetailDAY_OF_WEEK: TIntegerField;
    TableDetailDAYDESC: TStringField;
    TableDetailFROMTIMESTRING: TStringField;
    TableDetailTOTIMESTRING: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailAfterScroll(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableHourTypeFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableMasterAfterScroll(DataSet: TDataSet);
    procedure TableMasterCalcFields(DataSet: TDataSet);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
    function OvertimeDefStartEndTimeCheck(DataSet: TDataSet): Boolean;
  public
    { Public declarations }
    FStartTime,
    FEndTime: Integer;
    FContractGroupCodeOriginal, FContractGroupLineOriginal: String;
    function OvertimeDefFor(AContractGroupCode: String): Boolean;
    procedure CopyOvertimeDef(ASrcContractGroupCode,
      ADestContractGroupCode: String; AOvertimeDefDefined: Boolean);
  end;

var
  OvertimeDefDM: TOvertimeDefDM;

implementation

uses SystemDMT, OvertimeDefFRM, UPimsMessageRes;

{$R *.DFM}

procedure TOvertimeDefDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableHourType.Active := True;
  FContractGroupCodeOriginal := '';
  FContractGroupLineOriginal := '';
end;

procedure TOvertimeDefDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  TableHourType.Active := False;
end;

procedure TOvertimeDefDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not TableHourType.IsEmpty then
  begin
    TableHourType.First;
    TableDetail.FieldByName('HOURTYPE_NUMBER').Value :=
      TableHourType.FieldByName('HOURTYPE_NUMBER').Value;
  end;
  TableDetail.FieldByName('STARTTIME').asInteger:= 0;
  TableDetail.FieldByName('ENDTIME').asInteger:= 0;
  TableDetail.FieldByName('LINE_NUMBER').AsInteger := 0;

  if TableDetail.FieldByName('FROMTIME').Value = Null then
    TableDetail.FieldByName('FROMTIME').Value := 0;
  if TableDetail.FieldByName('TOTIME').Value = Null then
    TableDetail.FieldByName('TOTIME').Value := 0;
end;

procedure TOvertimeDefDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if (not DataSet.Active) or (DataSet.IsEmpty) then
    Exit;
  TOvertimeDefF(Owner).SetTimeFields;
{
  TOvertimeDefF(Owner).dxSpinEditStartHour.Value :=
    TableDetail.FieldByName('STARTTIME').asInteger div 60;
  TOvertimeDefF(Owner).dxSpinEditStartMin.Value :=
    TableDetail.FieldByName('STARTTIME').asInteger mod 60;
  TOvertimeDefF(Owner).dxSpinEditEndHour.Value :=
    TableDetail.FieldByName('ENDTIME').asInteger div 60;
  TOvertimeDefF(Owner).dxSpinEditEndMin.Value :=
    TableDetail.FieldByName('ENDTIME').asInteger mod 60;
}
end;


procedure TOvertimeDefDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  TableDetail.FieldByName('STARTHOURMIN').asString :=
    Format('%.2d:%.2d',[(TableDetail.FieldByName('STARTTIME').asInteger div 60),
    (TableDetail.FieldByName('STARTTIME').asInteger mod 60)]);
  TableDetail.FieldByName('ENDHOURMIN').asString :=
    Format('%.2d:%.2d',[(TableDetail.FieldByName('ENDTIME').asInteger div 60),
    (TableDetail.FieldByName('ENDTIME').asInteger mod 60)]);
  if SystemDM.OvertimePerDay then // GLOB3-223
  begin
    if TableDetail.FieldByName('DAY_OF_WEEK').AsString <> '' then
      TableDetailDAYDESC.Value :=
        SystemDM.GetDayWDescription(TableDetail.FieldByName('DAY_OF_WEEK').AsInteger);
    TableDetailFROMTIMESTRING.Value :=
      FormatDateTime('hh:nn', TableDetail.FieldByName('FROMTIME').AsDateTime);
    TableDetailTOTIMESTRING.Value :=
      FormatDateTime('hh:nn', TableDetail.FieldByName('TOTIME').AsDateTime);
  end;
end;

procedure TOvertimeDefDM.TableHourTypeFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := (TableHourTypeOVERTIME_YN.Value = 'Y');
end;

// MR:20-05-2005 Replacement for Trigger.
function TOvertimeDefDM.OvertimeDefStartEndTimeCheck(
  DataSet: TDataSet): Boolean;
begin
  Result := True;
  if (FStartTime > FEndTime) then
  begin
    DisplayMessage(SPimsStartEndTime, mtInformation, [mbOK]);
    Result := False;
  end;
  if SystemDM.OvertimePerDay then // PIM-395 Skip next check // GLOB3-223
    Exit;
  if Result then
  begin
    qryOvertimeDef.Close;
    // RV067.MRA.12. Use correct record-key here.
    qryOvertimeDef.ParamByName('NEW_CONTRACTGROUP_CODE').AsString :=
      FContractGroupCodeOriginal;
//      DataSet.FieldByName('CONTRACTGROUP_CODE').AsString;
    try
      qryOvertimeDef.ParamByName('NEW_LINE_NUMBER').AsInteger :=
        StrToInt(FContractGroupLineOriginal);
    except
      qryOvertimeDef.ParamByName('NEW_LINE_NUMBER').AsInteger :=
        DataSet.FieldByName('LINE_NUMBER').AsInteger;
    end;
//      DataSet.FieldByName('LINE_NUMBER').AsInteger;
    qryOvertimeDef.Open;
    if not qryOvertimeDef.IsEmpty then
    begin
      qryOvertimeDef.First;
      while (not qryOvertimeDef.Eof) and Result do
      begin
        if (((FStartTime < qryOvertimeDef.FieldByName('STARTTIME').AsInteger) and
             (FEndTime > qryOvertimeDef.FieldByName('STARTTIME').AsInteger))
          or ((FStartTime > qryOvertimeDef.FieldByName('STARTTIME').AsInteger) and
              (FStartTime < qryOvertimeDef.FieldByName('ENDTIME').AsInteger)))
          then
          begin
            DisplayMessage(SPIMSTimeIntervalError, mtInformation, [mbOk]);
            Result := False;
          end;
        qryOvertimeDef.Next;
      end;
    end;
    qryOvertimeDef.Close;
  end;
end;

procedure TOvertimeDefDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  if not OvertimeDefStartEndTimeCheck(DataSet) then
  begin
    DataSet.Cancel;
    SysUtils.Abort;
  end;
  SystemDM.DefaultBeforePost(DataSet);
  TableDetail.FieldByName('STARTTIME').asInteger:=	FStartTime;
  TableDetail.FieldByName('ENDTIME').asInteger:=	FEndTime;
end;

procedure TOvertimeDefDM.TableMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  if (not DataSet.Active) or (DataSet.IsEmpty) then
    Exit;
  TOvertimeDefF(Owner).SetTimeFields;
end;

procedure TOvertimeDefDM.TableMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  case TOvertime(TableMaster.
    FieldByName('OVERTIME_PER_DAY_WEEK_PERIOD').AsInteger) of
  OTDay: TableMaster.FieldByName('CALC_OVERTIMETYPE').AsString := SOvertimeDay;
  OTWeek: TableMaster.FieldByName('CALC_OVERTIMETYPE').AsString := SOvertimeWeek;
  OTMonth: TableMaster.FieldByName('CALC_OVERTIMETYPE').AsString := SOvertimeMonth;
  OTPeriod: TableMaster.FieldByName('CALC_OVERTIMETYPE').AsString := SPeriodReport;
  end;
end;

procedure TOvertimeDefDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  // RV067.MRA.12. Get original record-key here.
  FContractGroupCodeOriginal :=
    DataSet.FieldByName('CONTRACTGROUP_CODE').AsString;
  FContractGroupLineOriginal :=
    DataSet.FieldByName('LINE_NUMBER').AsString;
end;

// RV091.1.
function TOvertimeDefDM.OvertimeDefFor(
  AContractGroupCode: String): Boolean;
var
  HoursFound: Integer;
begin
  HoursFound := SystemDM.GetDBValue(
    Format('SELECT COUNT(*) FROM OVERTIMEDEFINITION O ' +
           ' WHERE O.CONTRACTGROUP_CODE = ''%s''', [AContractGroupCode]),
           0);
  Result := HoursFound <> 0;
end;

// RV091.1.
procedure TOvertimeDefDM.CopyOvertimeDef(ASrcContractGroupCode,
  ADestContractGroupCode: String; AOvertimeDefDefined: Boolean);
begin
  if AOvertimeDefDefined then
    SystemDM.ExecSql(
      Format('DELETE FROM OVERTIMEDEFINITION O ' +
             ' WHERE O.CONTRACTGROUP_CODE = ''%s''', [ADestContractGroupCode])
             );
  SystemDM.ExecSql(
    Format(
      'INSERT INTO OVERTIMEDEFINITION ' +
      '  (LINE_NUMBER, CONTRACTGROUP_CODE, ' +
      '  CREATIONDATE, HOURTYPE_NUMBER, STARTTIME, MUTATIONDATE, ' +
      '  MUTATOR, ENDTIME, FROMTIME, TOTIME, DAY_OF_WEEK) ' +
      'SELECT O.LINE_NUMBER, ''%s'', ' +
      '  SYSDATE, O.HOURTYPE_NUMBER, O.STARTTIME, SYSDATE, ' +
      '  ''%s'', O.ENDTIME, O.FROMTIME, O.TOTIME, O.DAY_OF_WEEK ' +
      'FROM OVERTIMEDEFINITION O ' +
      'WHERE O.CONTRACTGROUP_CODE = ''%s'' ',
    [ADestContractGroupCode, SystemDM.CurrentProgramUser , ASrcContractGroupCode]
    )
  );
  TableDetail.Refresh;
end;

end.
