(*
  MRA:22-MAR-2016 PIM-152
  - Report Ghost Counts
*)
unit ReportGhostCountsQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls, SystemDMT, ReportGhostCountsDMT;

type
  TQRParameters = class
  private
    FShiftFrom, FShiftTo: String;
    FDepartmentFrom, FDepartmentTo: String;
    FWorkspotFrom, FWorkspotTo: String;
    FDateFrom, FDateTo: TDateTime;
    FShowAllShift,
    FShowAllDepartment,
    FShowSelection, FExportToFile,
    FOnlyShowGhostCounts,
    FShowCompareJobs: Boolean;
  public
    procedure SetValues(
      ShiftFrom, ShiftTo: String;
      DepartmentFrom, DepartmentTo: String;
      WorkspotFrom, WorkspotTo: String;
      DateFrom, DateTo: TDateTime;
      ShowAllShift,
      ShowAllDepartment,
      ShowSelection, ExportToFile,
      OnlyShowGhostCounts,
      ShowCompareJobs: Boolean
      );
  end;

type
  TReportGhostCountsQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDShift: TQRGroup;
    QRGroupHDDept: TQRGroup;
    QRGroupColumnHeader: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLBLToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblFromDept: TQRLabel;
    QRLblDept: TQRLabel;
    QRLblDeptFrom: TQRLabel;
    QRLblToDept: TQRLabel;
    QRLblDeptTo: TQRLabel;
    QRLblFromWorkspot: TQRLabel;
    QRLblWorkspot: TQRLabel;
    QRLblWorkspotFrom: TQRLabel;
    QRLblToWorkspot: TQRLabel;
    QRLblWorkspotTo: TQRLabel;
    QRLblFromDate: TQRLabel;
    QRLblDate: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLblToDate: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRLabelWorkspotColumnHeader: TQRLabel;
    QRLabelJobColumnHeader: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel12: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRBandFooterWK: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelWKTotalQtyHrs: TQRLabel;
    QRLabelWKTotalQty: TQRLabel;
    QRLabelWKTotalQtyNoHrs: TQRLabel;
    QRBandFooterDEPT: TQRBand;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelDeptTotalQty: TQRLabel;
    QRLabelDeptTotalQtyHrs: TQRLabel;
    QRLabelDeptTotalQtyNoHrs: TQRLabel;
    QRBandFooterPlant: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRShape1: TQRShape;
    QRLabelPlantTotalQty: TQRLabel;
    QRLabelPlantTotalQtyHrs: TQRLabel;
    QRLabelPlantTotalQtyNoHrs: TQRLabel;
    QRLabelJobTotalQty: TQRLabel;
    QRLabelJobTotalQtyHrs: TQRLabel;
    QRLabelJobTotalQtyNoHrs: TQRLabel;
    QRBandSummary: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRLblFromShift: TQRLabel;
    QRLblShift: TQRLabel;
    QRLblShiftFrom: TQRLabel;
    QRLblToShift: TQRLabel;
    QRLblShiftTo: TQRLabel;
    QRLabel13: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRBandFooterShift: TQRBand;
    QRLabelTotShift: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabelShiftTotalQty: TQRLabel;
    QRLabelShiftTotalQtyHrs: TQRLabel;
    QRLabelShiftTotalQtyNoHrs: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupColumnHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    procedure ExportDetails(
      AGroupCode, AGroupDescription,
      AQty, AQtyHrs, AQtyNoHrs: String);
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    TotalPlantQty, TotalPlantQtyHrs, TotalPlantQtyNoHrs: Double;
    TotalShiftQty, TotalShiftQtyHrs, TotalShiftQtyNoHrs: Double;
    TotalDeptQty, TotalDeptQtyHrs, TotalDeptQtyNoHrs: Double;
    TotalWorkspotQty, TotalWorkspotQtyHrs, TotalWorkspotQtyNoHrs: Double;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const ShiftFrom, ShiftTo: String;
      const DepartmentFrom, DepartmentTo: String;
      const WorkspotFrom, WorkspotTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowAllShift: Boolean;
      const ShowAllDepartment: Boolean;
      const ShowSelection, ExportToFile, OnlyShowGhostCounts,
            ShowCompareJobs: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportGhostCountsQR: TReportGhostCountsQR;

implementation

{$R *.DFM}

uses
  UPimsMessageRes,
  UPimsConst,
  ListProcsFRM,
  UGlobalFunctions;

{ TQRParameters }

procedure TQRParameters.SetValues(ShiftFrom, ShiftTo,
  DepartmentFrom, DepartmentTo, WorkspotFrom, WorkspotTo: String;
  DateFrom, DateTo: TDateTime;
  ShowAllShift, ShowAllDepartment, ShowSelection, ExportToFile,
  OnlyShowGhostCounts, ShowCompareJobs: Boolean);
begin
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FWorkspotFrom := WorkspotFrom;
  FWorkspotTo := WorkspotTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowAllShift := ShowAllShift;
  FShowAllDepartment := ShowAllDepartment;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  FOnlyShowGhostCounts := OnlyShowGhostCounts;
  FShowCompareJobs := ShowCompareJobs;
end;

{ TReportGhostCountsQR }

function TReportGhostCountsQR.QRSendReportParameters(const PlantFrom,
  PlantTo, ShiftFrom, ShiftTo, DepartmentFrom, DepartmentTo,
  WorkspotFrom, WorkspotTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowAllShift, ShowAllDepartment,
  ShowSelection, ExportToFile, OnlyShowGhostCounts,
  ShowCompareJobs: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '-1', '-1');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(ShiftFrom, ShiftTo, DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo, DateFrom, DateTo,
      ShowAllShift, ShowAllDepartment, ShowSelection, ExportToFile,
      OnlyShowGhostCounts, ShowCompareJobs);
  end;
  SetDataSetQueryReport(ReportGhostCountsDM.qryGhostCounts);
end;

procedure TReportGhostCountsQR.ConfigReport;
begin
//  inherited; // DO NOT INHERIT !!!
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblShiftFrom.Caption := '*';
  QRLblShiftTo.Caption := '*';
  QRLblDeptFrom.Caption := '*';
  QRLblDeptTo.Caption := '*';
  QRLblWorkspotFrom.Caption := DisplayWorkspotFrom(QRParameters.FWorkspotFrom);
  QRLblWorkspotTo.Caption := DisplayWorkspotTo(QRParameters.FWorkspotTo);
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if not QRParameters.FShowAllDepartment then
    begin
      QRLblDeptFrom.Caption := QRParameters.FDepartmentFrom;
      QRLblDeptTo.Caption := QRParameters.FDepartmentTo;
    end;
    if not QRParameters.FShowAllShift then
    begin
      QRLblShiftFrom.Caption := QRParameters.FShiftFrom;
      QRLblShiftTo.Caption := QRParameters.FShiftTo;
    end;
  end;
end; // ConfigReport

function TReportGhostCountsQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  UserName: String;
begin
  Screen.Cursor := crHourGlass;
  try
    with ReportGhostCountsDM do
    begin
      SelectStr :=
        'SELECT ' + NL +
        '  V.PLANT_CODE, V.PDESCRIPTION, ' + NL +
        '  V.SHIFT_NUMBER, V.SDESCRIPTION, ' + NL +
        '  V.DEPARTMENT_CODE, V.DDESCRIPTION,  ' + NL +
        '  V.WORKSPOT_CODE, V.WDESCRIPTION, ' + NL +
        '  V.JOB_CODE, V.JDESCRIPTION, ' + NL +
        '  SUM(V.QTY_ALL) QTY_ALL, ' + NL +
        '  SUM(V.QTY_HRS) QTY_HRS, ' + NL +
        '  SUM(V.QTY_NOHRS) QTY_NOHRS ' + NL +
        'FROM ' + NL +
        '  V_GHOSTCOUNTS V ' + NL +
        'WHERE ' + NL +
        '  V.QTY_ALL >= 0 AND ' + NL +
        '  V.QTY_HRS >= 0 AND ' + NL +
        '  V.QTY_NOHRS >= 0 AND ' + NL;
      if QRParameters.FOnlyShowGhostCounts then
        SelectStr := SelectStr +
          '  V.QTY_NOHRS <> 0 AND ' + NL;
      if not QRParameters.FShowCompareJobs then
        SelectStr := SelectStr +
          '  V.COMPARATION_JOB_YN = ''N'' AND ' + NL;
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        UserName := SystemDM.UserTeamLoginUser
      else
        UserName := '*';
      if UserName <> '*' then
        SelectStr := SelectStr +
          '( ' + NL +
          '  SELECT COUNT(*) FROM DEPARTMENTPERTEAM DT ' + NL +
          '  WHERE ' + NL +
          '    V.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
          '    V.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL +
          '  AND ( ' + NL +
          '    (''*'' = ' + MyQuotes(UserName) + ') OR ' + NL +
          '    (DT.TEAM_CODE IN ' + NL +
          '      (SELECT U.TEAM_CODE ' + NL +
          '       FROM TEAMPERUSER U ' + NL +
          '       WHERE U.USER_NAME = ' + MyQuotes(UserName) + ')) ' + NL +
          '  ) ) > 0 AND ' + NL;

      SelectStr := SelectStr +
        '  V.SHIFT_DATE >= ' + ToDate(QRParameters.FDateFrom) + NL +
        '  AND V.SHIFT_DATE <= ' + ToDate(QRParameters.FDateTo) + NL;
      SelectStr := SelectStr +
        '  AND V.PLANT_CODE >= ' + MyQuotes(QRBaseParameters.FPlantFrom) + NL +
        '  AND V.PLANT_CODE <= ' + MyQuotes(QRBaseParameters.FPlantTo) + NL;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      begin
        if not QRParameters.FShowAllShift then
          SelectStr := SelectStr +
            '  AND V.SHIFT_NUMBER >= ' + QRParameters.FShiftFrom + NL +
            '  AND V.SHIFT_NUMBER <= ' + QRParameters.FShiftTo + NL;
        if not QRParameters.FShowAllDepartment then
          SelectStr := SelectStr +
            '  AND V.DEPARTMENT_CODE >= ' + MyQuotes(QRParameters.FDepartmentFrom) + NL +
            '  AND V.DEPARTMENT_CODE <= ' + MyQuotes(QRParameters.FDepartmentTo) + NL;
        if SystemDM.WorkspotInclSel then
        begin
          SelectStr := SelectStr +
            ' AND ' +
            ' (' + NL +
            '   (' + NL +
            '     V.WORKSPOT_CODE IN ' + NL +
            '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
            '      WHERE PW.PIVOTCOMPUTERNAME = ' + MyQuotes(SystemDM.CurrentComputerName) +
            '      AND ' + NL +
            '      PW.PLANT_CODE = ' + MyQuotes(QRBaseParameters.FPlantFrom) + ') ' + NL +
            '   )' + NL +
            ' OR ' + NL +
            '   ( ' + NL +
            '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
            '      WHERE PW.PIVOTCOMPUTERNAME = ' + MyQuotes(SystemDM.CurrentComputerName) +
            '      AND ' + NL +
            '      PW.PLANT_CODE = ' + MyQuotes(QRBaseParameters.FPlantFrom) + ') = 0 ' + NL +
            '   ) ' + NL +
            ' ) ' + NL;
        end
        else
        begin
          SelectStr := SelectStr +
            '  AND V.WORKSPOT_CODE >= ' + MyQuotes(QRParameters.FWorkspotFrom) + NL +
            '  AND V.WORKSPOT_CODE <= ' + MyQuotes(QRParameters.FWorkspotTo) + NL;
        end;
      end; // if (PlantFrom = PlantTo)
      SelectStr := SelectStr + ' ' +
        'GROUP BY ' + NL +
        '  V.PLANT_CODE, V.PDESCRIPTION, ' + NL +
        '  V.SHIFT_NUMBER, V.SDESCRIPTION, ' + NL +
        '  V.DEPARTMENT_CODE, V.DDESCRIPTION,  ' + NL +
        '  V.WORKSPOT_CODE, V.WDESCRIPTION, ' + NL +
        '  V.JOB_CODE, V.JDESCRIPTION ' + NL +
        'ORDER BY ' +
        '  V.PLANT_CODE, ' + NL +
        '  V.SHIFT_NUMBER, ' + NL +
        '  V.DEPARTMENT_CODE, ' + NL +
        '  V.WORKSPOT_CODE, ' + NL +
        '  V.JOB_CODE ' + NL;
      qryGhostCounts.Close;
      qryGhostCounts.SQL.Clear;
      qryGhostCounts.SQL.Add(SelectStr);
//    qryGhostCounts.SQL.SaveToFile('c:\temp\ReportGhostCounts.sql');
      qryGhostCounts.Open;
      Result := not qryGhostCounts.Eof;
    end; // with
  finally
    Screen.Cursor := crDefault;
  end;
end; // ExistsRecords

procedure TReportGhostCountsQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportGhostCountsQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption;
end;

procedure TReportGhostCountsQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  Printband := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      ExportClass.AddText(QRLblFromShift.Caption + ' ' +
        QRLblShift.Caption + QRLblShiftFrom.Caption + ' ' +
        QRLblToShift.Caption + QRLblShiftTo.Caption);
      ExportClass.AddText(QRLblFromDept.Caption + ' ' +
        QRLblDept.Caption + QRLblDeptFrom.Caption + ' ' +
        QRLblToDept.Caption + QRLblDeptTo.Caption);
      ExportClass.AddText(QRLblFromWorkspot.Caption + ' ' +
        QRLblWorkspot.Caption + QRLblWorkspotFrom.Caption + ' ' +
        QRLblToWorkspot.Caption + QRLblWorkspotTo.Caption);
      ExportClass.AddText(QRLblFromDate.Caption + ' ' +
        QRLblDate.Caption + ' ' +
        QRLblDateFrom.Caption + ' ' +
        QRLblToDate.Caption + ' ' +
        QRLblDateTo.Caption);
    end;
  end;
end; // QRBandTitleBeforePrint

procedure TReportGhostCountsQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelJobTotalQty.Caption :=
    Format('%.0n',
      [ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_ALL').AsFloat]);
  QRLabelJobTotalQtyHrs.Caption :=
    Format('%.0n',
      [ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_HRS').AsFloat]);
  QRLabelJobTotalQtyNoHrs.Caption :=
    Format('%.0n',
      [ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_NOHRS').AsFloat]);
  TotalPlantQty := TotalPlantQty +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_ALL').AsFloat;
  TotalPlantQtyHrs := TotalPlantQtyHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_HRS').AsFloat;
  TotalPlantQtyNoHrs := TotalPlantQtyNoHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_NOHRS').AsFloat;
  TotalShiftQty := TotalShiftQty +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_ALL').AsFloat;
  TotalShiftQtyHrs := TotalShiftQtyHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_HRS').AsFloat;
  TotalShiftQtyNoHrs := TotalShiftQtyNoHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_NOHRS').AsFloat;
  TotalDeptQty := TotalDeptQty +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_ALL').AsFloat;
  TotalDeptQtyHrs := TotalDeptQtyHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_HRS').AsFloat;
  TotalDeptQtyNoHrs := TotalDeptQtyNoHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_NOHRS').AsFloat;
  TotalWorkspotQty := TotalWorkspotQty +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_ALL').AsFloat;
  TotalWorkspotQtyHrs := TotalWorkspotQtyHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_HRS').AsFloat;
  TotalWorkspotQtyNoHrs := TotalWorkspotQtyNoHrs +
    ReportGhostCountsDM.qryGhostCounts.FieldByName('QTY_NOHRS').AsFloat;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      '',
      ReportGhostCountsDM.qryGhostCounts.FieldByName('JOB_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('JDESCRIPTION').AsString,
      QRLabelJobTotalQty.Caption,
      QRLabelJobTotalQtyHrs.Caption,
      QRLabelJobTotalQtyNoHrs.Caption);
  end;
end;

procedure TReportGhostCountsQR.QRGroupHDPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  TotalPlantQty := 0;
  TotalPlantQtyHrs := 0;
  TotalPlantQtyNoHrs := 0;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabel54.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PDESCRIPTION').AsString,
      '',
      '',
      ''
      );
  end;
end;

procedure TReportGhostCountsQR.QRGroupHDShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  TotalShiftQty := 0;
  TotalShiftQtyHrs := 0;
  TotalShiftQtyNoHrs := 0;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabel13.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('SHIFT_NUMBER').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('SDESCRIPTION').AsString,
      '',
      '',
      ''
      );
  end;
end;

procedure TReportGhostCountsQR.QRGroupHDDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  TotalDeptQty := 0;
  TotalDeptQtyHrs := 0;
  TotalDeptQtyNoHrs := 0;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabel35.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PDESCRIPTION').AsString,
      '',
      '',
      ''
      );
  end;
end;

procedure TReportGhostCountsQR.QRGroupHDWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  TotalWorkspotQty := 0;
  TotalWorkspotQtyHrs := 0;
  TotalWorkspotQtyNoHrs := 0;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabel12.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('WORKSPOT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('WDESCRIPTION').AsString,
      '',
      '',
      ''
      );
  end;
end;

procedure TReportGhostCountsQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelWKTotalQty.Caption := Format('%.0n', [TotalWorkspotQty]);
  QRLabelWKTotalQtyHrs.Caption := Format('%.0n', [TotalWorkspotQtyHrs]);
  QRLabelWKTOtalQtyNoHrs.Caption := Format('%.0n', [TotalWorkspotQtyNoHrs]);
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      '',
      ReportGhostCountsDM.qryGhostCounts.FieldByName('WORKSPOT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('WDESCRIPTION').AsString,
      QRLabelWKTotalQty.Caption,
      QRLabelWKTotalQtyHrs.Caption,
      QRLabelWKTotalQtyNoHrs.Caption);
  end;
end;

procedure TReportGhostCountsQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelDeptTotalQty.Caption := Format('%.0n', [TotalDeptQty]);
  QRLabelDeptTotalQtyHrs.Caption := Format('%.0n', [TotalDeptQtyHrs]);
  QRLabelDeptTotalQtyNoHrs.Caption := Format('%.0n', [TotalDeptQtyNoHrs]);
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabelTotDept.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PDESCRIPTION').AsString,
      QRLabelDeptTotalQty.Caption,
      QRLabelDeptTotalQtyHrs.Caption,
      QRLabelDeptTotalQtyNoHrs.Caption);
  end;
end;

procedure TReportGhostCountsQR.QRBandFooterShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelShiftTotalQty.Caption := Format('%.0n', [TotalShiftQty]);
  QRLabelShiftTotalQtyHrs.Caption := Format('%.0n', [TotalShiftQtyHrs]);
  QRLabelShiftTotalQtyNoHrs.Caption := Format('%.0n', [TotalShiftQtyNoHrs]);
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabelTotShift.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('SHIFT_NUMBER').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('SDESCRIPTION').AsString,
      QRLabelShiftTotalQty.Caption,
      QRLabelShiftTotalQtyHrs.Caption,
      QRLabelShiftTotalQtyNoHrs.Caption);
  end;
end; // QRBandFooterShiftBeforePrint

procedure TReportGhostCountsQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  QRLabelPlantTotalQty.Caption := Format('%.0n', [TotalPlantQty]);
  QRLabelPlantTotalQtyHrs.Caption := Format('%.0n', [TotalPlantQtyHrs]);
  QRLabelPlantTotalQtyNoHrs.Caption := Format('%.0n', [TotalPlantQtyNoHrs]);
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      QRLabelTotPlant.Caption,
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PLANT_CODE').AsString + ' ' +
      ReportGhostCountsDM.qryGhostCounts.FieldByName('PDESCRIPTION').AsString,
      QRLabelPlantTotalQty.Caption,
      QRLabelPlantTotalQtyHrs.Caption,
      QRLabelPlantTotalQtyNoHrs.Caption);
  end;
end;

procedure TReportGhostCountsQR.QRGroupColumnHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and PrintBand then
  begin
    ExportDetails(
      '',
      '',
      QRLabel8.Caption,
      QRLabel2.Caption,
      QRLabel6.Caption);
    ExportDetails(
      '',
      '',
      QRLabel1.Caption,
      QRLabel3.Caption,
      QRLabel7.Caption);
    ExportDetails(
      QRLabelWorkspotColumnHeader.Caption,
      '',
      '',
      QRLabel4.Caption,
      QRLabel9.Caption);
    ExportDetails(
      QRLabelJobColumnHeader.Caption,
      '',
      '',
      QRLabel5.Caption,
      QRLabel10.Caption);
  end;
end; // QRGroupColumnHeaderBeforePrint

procedure TReportGhostCountsQR.ExportDetails(AGroupCode, AGroupDescription,
  AQty, AQtyHrs, AQtyNoHrs: String);
begin
  ExportClass.AddText(
    AGroupCode + ExportClass.Sep +
    AGroupDescription + ExportClass.Sep +
    AQty + ExportClass.Sep +
    AQtyHrs + ExportClass.Sep +
    AQtyNoHrs + ExportClass.Sep
    );
end; // ExportDetails

procedure TReportGhostCountsQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end; // QRBandSummaryBeforePrint

end.
