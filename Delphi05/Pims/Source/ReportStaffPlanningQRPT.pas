(*
  Changes:
  MRA:11-JAN-2010. RV050.7. 889947.
  - When selecting 1 plant and 1 department, it still shows
    all departments of that plant.
  MRA:10-MAY-2011. RV092.14. Bugfix. 550518
  - The team-selection must refer to the workspot-department-teams, not to the
    team of the employee! Otherwise you can not show employees
    who worked in other plants.
  - Addition of teams-per-user.
  MRA:17-FEB-2012. 2.0.161.214.2. TD-20437 Changes.
  - Report Staff Planning
    - Employee names were truncated to 15 positions.
    - Some other texts at totals were overwriting some other parts.
    - TECHNICAL NOTE: Fixed by setting these QRLabel-components to:
      - AutoSize=False, AutoStretch=True, WordWrap=True
      - Because of AutoSize=False, the max. width in the report must be
        adjusted (in designer) for these QRLabels.
      - Because of AutoStretch=True + WordWrap=True it will add
        lines below when it does not fit on 1 line.
      - No truncation is needed.
      - The above is only possible when there is enough room below
        the QRLabels.
  MRA:6-FEB-2013. 20013910. Rework.
  - Plan on departments gives problems
    - Only first found department was used, all others were not shown.
    - CAUSE: Grouping went wrong because workspot is '0' for
             planning-on-departments.
  MRA:5-FEB-2018 PIM-335
  - Report Planning shows negative values (when showing totals)
  - Note: The export does not give this problem!
  - Remark: There was 1 QRLabel too much under an existing QRLabel on
    Tuesday in footer of workspot-total that had to be removed.
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:17-SEP-2018 GLOB3-60.1. Rework
  - It did not show the plant totals anymore
  MRA:1-APR-2019 PIM-377
  - Show start and end times in planning reports
  - Sort on employee (show all on employee-level, instead of on workspot-level)
  MRA:28-JUN-2019 PIM-377 - Rework
  - Show start and end times in planning reports
  - It did not show a workspot starting with 0, reason, when selecting ALL,
    it takes '1' as From-value, this must be '0', also for departments.
  - For Early/Late: Show a different start/end because of early/late-definition.
  - Make font smaller for workspots. Note: This uses short-name, but when using
    too much capitals it still can be too long.
*)
unit ReportStaffPlanningQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, UPimsConst;

const
  AvailableDummyWorkspotCode = 'N-PLAN';

type

  TEmpPlanRecord = record
    ADate: TDateTime;
    AStart, AEnd: TDateTime;
    AWorkspotCode, AWDescription: String;
  end;

  TTBWeekArray = Array[1..7, 1..MAX_TBS] of Integer;
  TTBSCHEDULE = Array[1..MAX_TBS] of String;
  TEmpPlanRecordArray = Array[1..7, 1..MAX_TBS] of TEmpPlanRecord;

  TQRParameters = class
  private
    FShiftFrom, FShiftTo: String;
    FDeptFrom, FDeptTo, FTeamFrom, FTeamTo, FWKFrom, FWKTo: String;
    FShowOnlyDiff, FShowSelection, FPagePlant,  FPageShift, FPageWK,
    FPaidBreaks, FShowTB, FAllTeam: Boolean;
    FDateFrom, FDateTo: TDateTime;
    FShowTotals, FExportToFile, FShowNotPlannedEmployees,
    FShowStartEndTimes, FSortOnEmployee: Boolean;
    FSortOnEmployeeNumber: Integer;
  public
    procedure SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
      ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK, PaidBreaks,
      ShowTB, AllTeam: Boolean; DateFrom, DateTo: TDateTime;
      ShowTotals, ExportToFile, ShowNotPlannedEmployees,
      ShowStartEndTimes, SortOnEmployee: Boolean;
      SortOnEmployeeNumber: Integer);
  end;


  TReportStaffPlanningQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDShift: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDEmp: TQRGroup;
    QRGroupDate: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabelPlantDesc: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabelShiftDesc: TQRLabel;
    QRLabelWKCode: TQRLabel;
    QRBandWKFooter: TQRBand;
    QRBandEmpFooter: TQRBand;
    QRLabel73: TQRLabel;
    QRBandPlantFooter: TQRBand;
    QRLabelTB1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRShape2: TQRShape;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel175: TQRLabel;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRLabel183: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    QRLabel187: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel191: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel195: TQRLabel;
    QRLabel196: TQRLabel;
    QRLabel197: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel199: TQRLabel;
    QRLabel200: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel204: TQRLabel;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRLabel210: TQRLabel;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRLabel214: TQRLabel;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRLabel221: TQRLabel;
    QRLabel222: TQRLabel;
    QRLabel223: TQRLabel;
    QRLabel224: TQRLabel;
    QRLabel225: TQRLabel;
    QRLabel226: TQRLabel;
    QRLabel227: TQRLabel;
    QRLabel228: TQRLabel;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel233: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRLabel236: TQRLabel;
    QRLabel237: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRBandHeader: TQRBand;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    QRLabel246: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel249: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel251: TQRLabel;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRLabel258: TQRLabel;
    QRLabel259: TQRLabel;
    QRLabel260: TQRLabel;
    QRLabel261: TQRLabel;
    QRLabel262: TQRLabel;
    ChildBandEmpFooter: TQRChildBand;
    ChildBandWKTotalCode: TQRChildBand;
    ChildBandPlantFTTotals: TQRChildBand;
    QRLabel302: TQRLabel;
    ChildBandHeader: TQRChildBand;
    QRLabel12: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel287: TQRLabel;
    QRLabel288: TQRLabel;
    QRLabel289: TQRLabel;
    QRLabel290: TQRLabel;
    QRLabel295: TQRLabel;
    QRLabel296: TQRLabel;
    QRLabel297: TQRLabel;
    QRLabel298: TQRLabel;
    QRLabel301: TQRLabel;
    QRLabel309: TQRLabel;
    QRLabel310: TQRLabel;
    QRBandSummary: TQRBand;
    ChildBandWKOccStandard: TQRChildBand;
    ChildBandPlantFTOccTotals: TQRChildBand;
    ChildBandWKTotals: TQRChildBand;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel305: TQRLabel;
    QRLabel307: TQRLabel;
    QRLabel308: TQRLabel;
    QRLabel269: TQRLabel;
    QRLabel267: TQRLabel;
    QRLabel266: TQRLabel;
    QRLabel265: TQRLabel;
    QRLabel264: TQRLabel;
    QRLabel263: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel157: TQRLabel;
    QRMemoWKOCTB: TQRMemo;
    QRMemoWKOC1: TQRMemo;
    QRMemoWKOC2: TQRMemo;
    QRMemoWKOC3: TQRMemo;
    QRMemoWKOC4: TQRMemo;
    QRMemoWKOC5: TQRMemo;
    QRMemoWKOC6: TQRMemo;
    QRMemoWKOC7: TQRMemo;
    QRMemoWKOCTot: TQRMemo;
    QRMemoWKOCWeeksTot: TQRMemo;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRShape1: TQRShape;
    QRMemoWKTB: TQRMemo;
    QRMemoWK1: TQRMemo;
    QRMemoWK2: TQRMemo;
    QRMemoWK3: TQRMemo;
    QRMemoWK4: TQRMemo;
    QRMemoWK5: TQRMemo;
    QRMemoWK6: TQRMemo;
    QRMemoWK7: TQRMemo;
    QRMemoWKTot: TQRMemo;
    QRMemoWKWeeksTot: TQRMemo;
    QRMemoWKDiff: TQRMemo;
    ChildBandEmp2: TQRChildBand;
    QRDBText2: TQRDBText;
    QRLblEmpDescChild: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel303: TQRLabel;
    QRLabel304: TQRLabel;
    QRLabel306: TQRLabel;
    QRDBText5: TQRDBText;
    QRLblEmpDesc: TQRLabel;
    QRMemoETB: TQRMemo;
    QRMemoE1: TQRMemo;
    QRMemoE2: TQRMemo;
    QRMemoE3: TQRMemo;
    QRMemoE4: TQRMemo;
    QRMemoE5: TQRMemo;
    QRMemoE6: TQRMemo;
    QRMemoE7: TQRMemo;
    QRMemoETot: TQRMemo;
    QRMemoEWeeksTot: TQRMemo;
    ChildBandPlantTotals: TQRChildBand;
    QRLabel240: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel279: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel280: TQRLabel;
    QRLabel270: TQRLabel;
    QRLabel271: TQRLabel;
    QRLabel272: TQRLabel;
    QRLabel273: TQRLabel;
    QRLabel274: TQRLabel;
    QRLabel275: TQRLabel;
    QRLabel276: TQRLabel;
    QRLabel291: TQRLabel;
    QRLabel292: TQRLabel;
    QRLabel281: TQRLabel;
    QRLabel282: TQRLabel;
    QRLabel277: TQRLabel;
    QRLabel278: TQRLabel;
    QRLabel283: TQRLabel;
    QRLabel284: TQRLabel;
    QRLabel285: TQRLabel;
    QRLabel286: TQRLabel;
    QRLabel294: TQRLabel;
    QRLabel293: TQRLabel;
    QRLabel158: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel161: TQRLabel;
    QRMemoPTotTB: TQRMemo;
    QRMemoP1: TQRMemo;
    QRMemoP2: TQRMemo;
    QRMemoP3: TQRMemo;
    QRMemoP4: TQRMemo;
    QRMemoP5: TQRMemo;
    QRMemoP6: TQRMemo;
    QRMemoP7: TQRMemo;
    QRMemoPTot: TQRMemo;
    QRMemoPWeeksTot: TQRMemo;
    QRMemoPDiff: TQRMemo;
    QRMemoPTotOccTB: TQRMemo;
    QRMemoPO1: TQRMemo;
    QRMemoPO2: TQRMemo;
    QRMemoPO3: TQRMemo;
    QRMemoPO4: TQRMemo;
    QRMemoPO5: TQRMemo;
    QRMemoPO6: TQRMemo;
    QRMemoPO7: TQRMemo;
    QRMemoPOTot: TQRMemo;
    QRMemoPOWeeksTot: TQRMemo;
    QRBandDetail: TQRBand;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRDBTextDeptDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextShiftDescPrint(sender: TObject; var Value: String);
    procedure QRLabelPlantDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandEmpFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandWKFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel160Print(sender: TObject; var Value: String);
    procedure QRLabel9Print(sender: TObject; var Value: String);
    procedure QRLabel43Print(sender: TObject; var Value: String);
    procedure QRLabel57Print(sender: TObject; var Value: String);
    procedure QRLabel72Print(sender: TObject; var Value: String);
    procedure QRLabel74Print(sender: TObject; var Value: String);
    procedure QRBandPlantFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel155Print(sender: TObject; var Value: String);
    procedure QRLabel199Print(sender: TObject; var Value: String);
    procedure QRLabel200Print(sender: TObject; var Value: String);
    procedure QRLabel238Print(sender: TObject; var Value: String);
    procedure QRLabel239Print(sender: TObject; var Value: String);
    procedure QRLabel118Print(sender: TObject; var Value: String);
    procedure QRLabel117Print(sender: TObject; var Value: String);
    procedure QRLabel116Print(sender: TObject; var Value: String);
    procedure QRLabel115Print(sender: TObject; var Value: String);
    procedure QRLabel195Print(sender: TObject; var Value: String);
    procedure QRLabel196Print(sender: TObject; var Value: String);
    procedure QRLabel197Print(sender: TObject; var Value: String);
    procedure QRLabel198Print(sender: TObject; var Value: String);
    procedure QRLabel154Print(sender: TObject; var Value: String);
    procedure QRLabel153Print(sender: TObject; var Value: String);
    procedure QRLabel152Print(sender: TObject; var Value: String);
    procedure QRLabel151Print(sender: TObject; var Value: String);
    procedure QRLabel234Print(sender: TObject; var Value: String);
    procedure QRLabel235Print(sender: TObject; var Value: String);
    procedure QRLabel236Print(sender: TObject; var Value: String);
    procedure QRLabel237Print(sender: TObject; var Value: String);
    procedure QRLabel156Print(sender: TObject; var Value: String);
    procedure QRGroupHDShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel30Print(sender: TObject; var Value: String);
    procedure QRLabel64Print(sender: TObject; var Value: String);
    procedure QRLabel162Print(sender: TObject; var Value: String);
    procedure QRLabel33Print(sender: TObject; var Value: String);
    procedure QRLabel174Print(sender: TObject; var Value: String);
    procedure QRLabel303Print(sender: TObject; var Value: String);
    procedure QRLabel304Print(sender: TObject; var Value: String);
    procedure QRLabel34Print(sender: TObject; var Value: String);
    procedure QRLabel308Print(sender: TObject; var Value: String);
    procedure QRLabel263Print(sender: TObject; var Value: String);
    procedure QRLabel59Print(sender: TObject; var Value: String);
    procedure QRLabel305Print(sender: TObject; var Value: String);
    procedure QRLabel307Print(sender: TObject; var Value: String);
    procedure QRLabel63Print(sender: TObject; var Value: String);
    procedure QRLabel36Print(sender: TObject; var Value: String);
    procedure QRLabel35Print(sender: TObject; var Value: String);
    procedure QRLabel54Print(sender: TObject; var Value: String);
    procedure QRLabel264Print(sender: TObject; var Value: String);
    procedure QRLabel265Print(sender: TObject; var Value: String);
    procedure QRLabel266Print(sender: TObject; var Value: String);
    procedure QRLabel267Print(sender: TObject; var Value: String);
    procedure QRLabel31Print(sender: TObject; var Value: String);
    procedure QRLabel32Print(sender: TObject; var Value: String);
    procedure QRLabel270Print(sender: TObject; var Value: String);
    procedure QRLabel271Print(sender: TObject; var Value: String);
    procedure QRLabel272Print(sender: TObject; var Value: String);
    procedure QRLabel273Print(sender: TObject; var Value: String);
    procedure QRLabel274Print(sender: TObject; var Value: String);
    procedure QRLabel275Print(sender: TObject; var Value: String);
    procedure QRLabel276Print(sender: TObject; var Value: String);
    procedure QRLabel277Print(sender: TObject; var Value: String);
    procedure QRLabel278Print(sender: TObject; var Value: String);
    procedure QRLabel283Print(sender: TObject; var Value: String);
    procedure QRLabel284Print(sender: TObject; var Value: String);
    procedure QRLabel285Print(sender: TObject; var Value: String);
    procedure QRLabel286Print(sender: TObject; var Value: String);
    procedure QRLabel294Print(sender: TObject; var Value: String);
    procedure QRBandHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLblEmpDescPrint(sender: TObject; var Value: String);
    procedure QRLblEmpDescChildPrint(sender: TObject; var Value: String);
    procedure ChildBandPlantFTOccTotalsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandWKTotalsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandWKTotalsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandEmp2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandEmp2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandEmpFooterBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandPlantFTTotalsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandWKOccStandardBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandWKTotalCodeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandEmpFooterAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandWKTotalCodeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandWKOccStandardAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandPlantFTTotalsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandPlantTotalsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandPlantTotalsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandPlantFTOccTotalsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
   private
    duration: TDateTime;
    FQRParameters: TQRParameters;
    FMaxTBToShow: Integer;
    function MyDecodeHrsMin(HrsMin: Integer): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FStartDay, FEndDay: Integer;

    FMinDate, FMaxDate: TDateTime;
    FStartDate, FEndDate: TDateTime;
    FEmpPlanning, FWKPlanning, FPlantPlanning, FSTOWK,
    FSTOPLANT: TTBWeekArray;
    FEmpPlanRecord: TEmpPlanRecordArray;

    FDayDesc, FDate: Array[1..7] of String;
    FShowOnlyDiff, FPaidBreaks, FWKPrint: Boolean;
    procedure InitializePlanningPerDaysOfWeek(var PlanningPerDaysOfWeek:
      TTBWeekArray);
//    procedure FillStringsPerDaysOfWeek(IndexMin: Integer);
    procedure FillDescDaysPerWeek;
    function QRSendReportParameters(const PlantFrom, PlantTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom, ShiftTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK,
        PaidBreaks, ShowTB, AllTeam, ShowTotals,
        ExportToFile, ShowNotPlannedEmployees,
        ShowStartEndTimes, SortOnEmployee: Boolean;
        SortOnEmployeeNumber: Integer): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;

//    procedure CalculateHrsEmpDate(Empl, Shift: Integer; Plant, Dept: String);
    procedure FillAmountsPerDaysOfWeek(IndexMin, IndexMax: Integer;
      AmountsPerDaysOfWeek: TTBWeekArray);
    function GetTotalTBSTOPlantPerDay(Day_Number: Integer): String;
    function GetTotalTBEMPPerDay(Day_Number: Integer): String;
    function GetTotalTBSTOWKPerDay(Day_Number: Integer): String;
    function GetTotalTBWKPerDay(Day_Number: Integer): String;
    function GetTotalTBPlantPerDay(Day_Number: Integer): String;
    function EmployeeDescription: String;
    property MaxTBToShow: Integer read FMaxTBToShow write FMaxTBToShow;
  end;

var
  ReportStaffPlanningQR: TReportStaffPlanningQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportStaffPlanningDMT, ListProcsFRM,
  CalculateTotalHoursDMT, UPimsMessageRes, UGlobalFunctions;

procedure TQRParameters.SetValues(ShiftFrom, ShiftTo: String;
  DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
  ShowOnlyDiff, ShowSelection, PagePlant,  PageShift, PageWK, PaidBreaks,
  ShowTB, AllTeam: Boolean;  DateFrom, DateTo: TDateTime;
  ShowTotals, ExportToFile, ShowNotPlannedEmployees,
  ShowStartEndTimes, SortOnEmployee: Boolean;
  SortOnEmployeeNumber: Integer);
begin
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FWKFrom := WKFrom;
  FWKTo := wkTo;
  FShowOnlyDiff := ShowOnlyDiff;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageWK := PageWK;
  FPageShift := PageShift;
  FPaidBreaks := PaidBreaks;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowTB := ShowTB;
  FAllTeam:= AllTeam;
  FShowTotals := ShowTotals;
  FExportToFile := ExportToFile;
  FShowNotPlannedEmployees := ShowNotPlannedEmployees;
  FShowStartEndTimes := ShowStartEndTimes;
  FSortOnEmployee := SortOnEmployee;
  FSortOnEmployeeNumber := SortOnEmployeeNumber;
end;

function TReportStaffPlanningQR.QRSendReportParameters(const PlantFrom, PlantTo,
  DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom, ShiftTo: String;
  const DateFrom, DateTo: TDateTime;
  const ShowOnlyDiff, ShowSelection, PagePlant, PageShift,  PageWK,
    PaidBreaks, ShowTB, AllTeam, ShowTotals, ExportToFile,
    ShowNotPlannedEmployees, ShowStartEndTimes, SortOnEmployee: Boolean;
    SortOnEmployeeNumber: Integer): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    if FQRParameters = Nil then
      FQRParameters := TQRParameters.Create;
    FQRParameters.SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo,
      ShowOnlyDiff, ShowSelection, PagePlant,  PageShift,PageWK, PaidBreaks,
      ShowTB, AllTeam, DateFrom, DateTo, ShowTotals, ExportToFile,
      ShowNotPlannedEmployees, ShowStartEndTimes, SortOnEmployee,
      SortOnEmployeeNumber);
  end;
  SetDataSetQueryReport(ReportStaffPlanningDM.QueryEMP);
end;

function TReportStaffPlanningQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  i, K: Integer;
  Year, Month, Day: Word;
begin
  duration := now;
  Screen.Cursor := crHourGlass;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    // PIM-377 Use '0', not '1', or a code that starts with 0 will not be shown!
    QRParameters.FDeptFrom := '0';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FWKFrom := '0';
    QRParameters.FWKTo := 'zzzzzz';
    QRParameters.FShiftFrom := '0';
    QRParameters.FShiftTo := '999999';
  end;
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);

  // RV050.7. Query changed.
  // Workspot: Can be in selection OR can be '0'.
  // 20013910
  // Use another way to recognize it is about planning-on-departments instead
  // of on workspots.
  // NOTE: When only '0' is returned for workspot (when it is about
  //       plan-on-depts), then because of grouping it results in only 1 record
  //       (first dept found).
  //       So, be sure it gets different results.
  //       Use a flag to know it is about a workspot or department.
  SelectStr :=
    'SELECT ' + NL +
    '  DISTINCT EMP.PLANT_CODE, EMP.SHIFT_NUMBER, ' + NL +
    '  CASE ' + NL +
    '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN EMP.WORKSPOT_CODE ' + NL +
    '    ELSE EMP.DEPARTMENT_CODE ' + NL +
    '  END WORKSPOT_CODE, ' + NL +
    '  CASE ' + NL +
    '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN (SELECT W.SHORT_NAME FROM WORKSPOT W WHERE W.PLANT_CODE = EMP.PLANT_CODE AND W.WORKSPOT_CODE = EMP.WORKSPOT_CODE) ' + NL +
    '    ELSE (SELECT D.DESCRIPTION FROM DEPARTMENT D WHERE D.PLANT_CODE = EMP.PLANT_CODE AND D.DEPARTMENT_CODE = EMP.DEPARTMENT_CODE) ' + NL +
    '  END WDESCRIPTION, ' + NL +
    '  EMP.DEPARTMENT_CODE, EMP.EMPLOYEE_NUMBER, ' + NL +
    '  E.DESCRIPTION, EMP.EMPLOYEEPLANNING_DATE, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_5, EMP.SCHEDULED_TIMEBLOCK_6, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_7, EMP.SCHEDULED_TIMEBLOCK_8, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_9, EMP.SCHEDULED_TIMEBLOCK_10, ' + NL +
    '  CASE ' + NL +
    '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
    '    ELSE ''D'' ' + NL +
    '  END FLAG ' + NL +
    'FROM ' + NL +
    '  EMPLOYEEPLANNING EMP INNER JOIN EMPLOYEE E ON ' + NL +
    '    EMP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    EMP.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    EMP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL;
  SelectStr := SelectStr +
    'WHERE ' + NL +
    '  ((EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_5 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_6 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_7 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_8 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_9 IN (''A'',''B'',''C'')) OR ' + NL +
    '  (EMP.SCHEDULED_TIMEBLOCK_10 IN (''A'',''B'',''C''))) ' + NL +
    '  AND EMP.EMPLOYEEPLANNING_DATE >= :FDATESTART ' + NL +
    '  AND EMP.EMPLOYEEPLANNING_DATE <= :FDATEEND ' + NL +
    '  AND EMP.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND EMP.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND ((EMP.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
    '  AND EMP.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
    '  OR (EMP.WORKSPOT_CODE = ''' + DummyStr + '''' + '))' + NL +
    '  AND (EMP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + ''')' + NL +
    '  AND EMP.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
    '  AND EMP.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + NL;
    // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
    //           Plus: Addition of 'teams-per-user'-selection.
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr +
        ' AND DT.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        ' AND DT.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
        ' AND ( ' + NL +
        '    (:USER_NAME = ''*'') OR ' + NL +
        '    (DT.TEAM_CODE IN ' + NL +
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
        '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
        '  ) ';
    SelectStr := SelectStr + ' ' +
       'UNION ' + NL +
       'SELECT ' + NL +
       '  DISTINCT OCP.PLANT_CODE, OCP.SHIFT_NUMBER, ' + NL +
       '  CASE ' + NL +
       '    WHEN OCP.WORKSPOT_CODE <> ''0'' THEN OCP.WORKSPOT_CODE ' + NL +
       '    ELSE OCP.DEPARTMENT_CODE ' + NL +
       '  END WORKSPOT_CODE, ' + NL +
       '  CASE ' + NL +
       '    WHEN OCP.WORKSPOT_CODE <> ''0'' THEN (SELECT W.SHORT_NAME FROM WORKSPOT W WHERE W.PLANT_CODE = OCP.PLANT_CODE AND W.WORKSPOT_CODE = OCP.WORKSPOT_CODE) ' + NL +
       '    ELSE (SELECT D.DESCRIPTION FROM DEPARTMENT D WHERE D.PLANT_CODE = OCP.PLANT_CODE AND D.DEPARTMENT_CODE = OCP.DEPARTMENT_CODE) ' + NL +
       '  END WDESCRIPTION, ' + NL +
       '  OCP.DEPARTMENT_CODE, -1, ' + NL +
       '  CAST('''' AS VARCHAR(40)), ' + NL +
       '  OCP.OCC_PLANNING_DATE, ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CASE ' + NL +
       '    WHEN OCP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
       '    ELSE ''D'' ' + NL +
       '  END FLAG ' + NL +
       'FROM ' + NL +
       '  OCCUPATIONPLANNING OCP INNER JOIN WORKSPOT W ON ' + NL +
       '    OCP.PLANT_CODE = W.PLANT_CODE ' + NL +
       '    AND OCP.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr +
       '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
       '    OCP.PLANT_CODE = DT.PLANT_CODE ' + NL +
       '    AND OCP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL;
    SelectStr := SelectStr +
       'WHERE ' + NL +
       '  OCP.OCC_PLANNING_DATE >= :FDATESTART ' + NL +
       '  AND OCP.OCC_PLANNING_DATE <= :FDATEEND  ' + NL +
       '  AND OCP.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
       '  AND OCP.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
       '  AND ((OCP.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
       '  AND OCP.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
       '  OR (OCP.WORKSPOT_CODE = ''' + DummyStr + '''' + '))' + NL +
       '  AND (OCP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
       '  AND OCP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + ''')' + NL +
       '  AND OCP.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
       '  AND OCP.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo  + NL +
       '  AND ((OCP.OCC_A_TIMEBLOCK_1 <> 0) OR (OCP.OCC_A_TIMEBLOCK_2 <> 0 )' + NL +
       '  OR (OCP.OCC_A_TIMEBLOCK_3 <> 0) OR (OCP.OCC_A_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (OCP.OCC_A_TIMEBLOCK_5 <> 0) OR (OCP.OCC_A_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (OCP.OCC_A_TIMEBLOCK_7 <> 0) OR (OCP.OCC_A_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (OCP.OCC_A_TIMEBLOCK_9 <> 0) OR (OCP.OCC_A_TIMEBLOCK_10 <> 0) ' + NL +
       '  OR (OCP.OCC_B_TIMEBLOCK_1 <> 0) OR (OCP.OCC_B_TIMEBLOCK_2 <> 0) ' + NL +
       '  OR (OCP.OCC_B_TIMEBLOCK_3 <> 0) OR (OCP.OCC_B_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (OCP.OCC_B_TIMEBLOCK_5 <> 0) OR (OCP.OCC_B_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (OCP.OCC_B_TIMEBLOCK_7 <> 0) OR (OCP.OCC_B_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (OCP.OCC_B_TIMEBLOCK_9 <> 0) OR (OCP.OCC_B_TIMEBLOCK_10 <> 0) ' + NL +
       '  OR (OCP.OCC_C_TIMEBLOCK_1 <> 0) OR (OCP.OCC_C_TIMEBLOCK_2 <> 0) ' + NL +
       '  OR (OCP.OCC_C_TIMEBLOCK_3 <> 0) OR (OCP.OCC_C_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (OCP.OCC_C_TIMEBLOCK_5 <> 0) OR (OCP.OCC_C_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (OCP.OCC_C_TIMEBLOCK_7 <> 0) OR (OCP.OCC_C_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (OCP.OCC_C_TIMEBLOCK_9 <> 0) OR (OCP.OCC_C_TIMEBLOCK_10 <> 0))' + NL;
    // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
    //           Plus: Addition of 'teams-per-user'-selection.
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr +
        ' AND DT.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        ' AND DT.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
        ' AND ( ' + NL +
        '    (:USER_NAME = ''*'') OR ' + NL +
        '    (DT.TEAM_CODE IN ' + NL +
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
        '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
        '  ) ';
     SelectStr := SelectStr + ' ' +
       'UNION ' + NL +
       'SELECT ' + NL +
       '  DISTINCT STO.PLANT_CODE, STO.SHIFT_NUMBER, ' + NL +
       '  CASE ' + NL +
       '    WHEN STO.WORKSPOT_CODE <> ''0'' THEN STO.WORKSPOT_CODE ' + NL +
       '    ELSE STO.DEPARTMENT_CODE ' + NL +
       '  END WORKSPOT_CODE, ' + NL +
       '  CASE ' + NL +
       '    WHEN STO.WORKSPOT_CODE <> ''0'' THEN (SELECT W.SHORT_NAME FROM WORKSPOT W WHERE W.PLANT_CODE = STO.PLANT_CODE AND W.WORKSPOT_CODE = STO.WORKSPOT_CODE) ' + NL +
       '    ELSE (SELECT D.DESCRIPTION FROM DEPARTMENT D WHERE D.PLANT_CODE = STO.PLANT_CODE AND D.DEPARTMENT_CODE = STO.DEPARTMENT_CODE) ' + NL +
       '  END WDESCRIPTION, ' + NL +
       '  STO.DEPARTMENT_CODE, -2, ' + NL +
       '  CAST( '''' AS VARCHAR(40)), ' + NL +
       // Pims -> Oracle
//       '  CAST ('''+ DateToStr(MyDate) +  ''' AS TIMESTAMP), ' + NL +
//       '  SYSDATE, ' + NL +
       '  CAST (TO_DATE(' + '''' + '1-1-1980' + '''' + ',' +
       '''' + 'dd-mm-yyyy' +  '''' + ') AS DATE), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CAST('''' AS VARCHAR(1)), CAST('''' AS VARCHAR(1)), ' + NL +
       '  CASE ' + NL +
       '    WHEN STO.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
       '    ELSE ''D'' ' + NL +
       '  END FLAG ' + NL +
       'FROM ' + NL +
       '  STANDARDOCCUPATION STO INNER JOIN WORKSPOT W ON ' + NL +
       '    STO.PLANT_CODE = W.PLANT_CODE ' + NL +
       '    AND STO.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr +
       '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
       '    STO.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
       '    STO.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL;
    SelectStr := SelectStr +
       'WHERE ' + NL +
       '  STO.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
       '  AND STO.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
       '  AND ((STO.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
       '  AND STO.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
       '  OR (STO.WORKSPOT_CODE = ''' + DummyStr + '''' + '))' + NL +
       '  AND (STO.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
       '  AND STO.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + ''')' + NL +
       '  AND STO.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
       '  AND STO.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo  + NL +
       '  AND ((STO.OCC_A_TIMEBLOCK_1 <> 0) OR (STO.OCC_A_TIMEBLOCK_2 <> 0 ) ' + NL +
       '  OR (STO.OCC_A_TIMEBLOCK_3 <> 0) OR (STO.OCC_A_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (STO.OCC_A_TIMEBLOCK_5 <> 0) OR (STO.OCC_A_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (STO.OCC_A_TIMEBLOCK_7 <> 0) OR (STO.OCC_A_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (STO.OCC_A_TIMEBLOCK_9 <> 0) OR (STO.OCC_A_TIMEBLOCK_10 <> 0) ' + NL +
       '  OR (STO.OCC_B_TIMEBLOCK_1 <> 0) OR (STO.OCC_B_TIMEBLOCK_2 <> 0) ' + NL +
       '  OR (STO.OCC_B_TIMEBLOCK_3 <> 0) OR (STO.OCC_B_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (STO.OCC_B_TIMEBLOCK_5 <> 0) OR (STO.OCC_B_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (STO.OCC_B_TIMEBLOCK_7 <> 0) OR (STO.OCC_B_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (STO.OCC_B_TIMEBLOCK_9 <> 0) OR (STO.OCC_B_TIMEBLOCK_10 <> 0) ' + NL +
       '  OR (STO.OCC_C_TIMEBLOCK_1 <> 0) OR (STO.OCC_C_TIMEBLOCK_2 <> 0) ' + NL +
       '  OR (STO.OCC_C_TIMEBLOCK_3 <> 0) OR (STO.OCC_C_TIMEBLOCK_4 <> 0) ' + NL +
       '  OR (STO.OCC_C_TIMEBLOCK_5 <> 0) OR (STO.OCC_C_TIMEBLOCK_6 <> 0) ' + NL +
       '  OR (STO.OCC_C_TIMEBLOCK_7 <> 0) OR (STO.OCC_C_TIMEBLOCK_8 <> 0) ' + NL +
       '  OR (STO.OCC_C_TIMEBLOCK_9 <> 0) OR (STO.OCC_C_TIMEBLOCK_10 <> 0) )';
       if FStartDay <= FEndDay then
         SelectStr := SelectStr +  ' AND STO.DAY_OF_WEEK >= ' + IntToStr(FStartDay) +
         ' AND STO.DAY_OF_WEEK <= ' + IntToStr(FEndDay) + NL
       else
         SelectStr := SelectStr + ' AND ' +
           ' ( (STO.DAY_OF_WEEK >= ' + IntToStr(FStartDay) + ' AND STO.DAY_OF_WEEK <= 7)' +
           ' OR (STO.DAY_OF_WEEK >= 1 AND STO.DAY_OF_WEEK <= ' + IntToStr(FEndDay) + '))' + NL;
    // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
    //           Plus: Addition of 'teams-per-user'-selection.
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr +
        ' AND DT.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        ' AND DT.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
        ' AND ( ' + NL +
        '    (:USER_NAME = ''*'') OR ' + NL +
        '    (DT.TEAM_CODE IN ' + NL +
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
        '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
        '  ) ';
    if QRParameters.FShowNotPlannedEmployees then
    begin
      SelectStr := SelectStr + ' ' +
        'UNION ' + NL +
        'SELECT ' + NL +
        '  DISTINCT EA.PLANT_CODE, ' + NL +
        '  EA.SHIFT_NUMBER, ' + NL +
        '  CAST(' + '''' + AvailableDummyWorkspotCode + '''' +
        '  '''' ' + NL +
        '  AS VARCHAR(6)), ' + NL +
        '  CAST('''' AS VARCHAR(6)), ' + NL +
        '  EA.EMPLOYEE_NUMBER, ' + NL +
        '  CAST('''' AS VARCHAR(40)), ' + NL +
        '  EA.EMPLOYEEAVAILABILITY_DATE, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_1, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_2, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_3, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_4, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_5, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_6, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_7, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_8, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_9, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_10, ' + NL +
        '  ''X'' AS FLAG ' + NL +
        'FROM ' + NL +
        // RV092.14. Addition of 'teams-per-user'-selection.
        '  EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE E ON ' + NL +
        '    EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
        'WHERE ' + NL +
        '  (EA.AVAILABLE_TIMEBLOCK_1 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_2 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_3 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_4 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_5 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_6 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_7 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_8 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_9 = ' + '''' + '*' + '''' + ' OR ' + NL +
        '   EA.AVAILABLE_TIMEBLOCK_10 = ' + '''' + '*' + '''' + ' ) ' + NL +
        '  AND EA.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '  AND EA.PLANT_CODE <= ''' +
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
        '  AND EA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART ' + NL +
        '  AND EA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND ' + NL +
        '  AND EA.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
        '  AND EA.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + NL;
      // RV092.14. Addition of 'teams-per-user'-selection.
      if not QRParameters.FAllTeam then
        SelectStr := SelectStr +
          ' AND E.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
          ' AND E.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
          ' AND ( ' + NL +
          '    (:USER_NAME = ''*'') OR ' + NL +
          '    (E.TEAM_CODE IN ' + NL +
          '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
          '       WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
          '  ) ';
      end;
      if not QRParameters.FSortOnEmployee then // Show per employee
      begin
        // Plant, Shift, WorkspotCode, DepartmentCode, EmployeeNumber, EmployeePlanningDate
        if QRParameters.FSortOnEmployeeNumber = 0 then
          SelectStr := SelectStr + ' ' +
            'ORDER BY 1, 2, 3, 5, 6, 8'
        else
          // Plant, Shift, WorkspotCode, DepartmentCode, (Employee-)Description, EmployeePlanningDate
          SelectStr := SelectStr + ' ' +
            'ORDER BY 1, 2, 3, 5, 7, 8'
      end
      else
      begin
        // Plant, Shift, EmployeeNumber, EmployeePlanningDate
        if QRParameters.FSortOnEmployeeNumber = 0 then
          SelectStr := SelectStr + ' ' +
            'ORDER BY 1, 2, 6, 8'
        else
          // Plant, Shift, (Employee-)Description, EmployeePlanningDate
          SelectStr := SelectStr + ' ' +
            'ORDER BY 1, 2, 7, 8'
      end;

  with ReportStaffPlanningDM do
  begin
    QueryEMP.Active := False;
    QueryEMP.UniDirectional := False;
    QueryEMP.SQL.Clear;
    QueryEMP.SQL.Add(SelectStr);
// QueryEMP.SQL.SaveToFile('c:\temp\reportStaffPlanning.sql');
    QueryEMP.ParamByName('FDATESTART').Value := GetDate(QRParameters.FDateFrom);
    QueryEMP.ParamByName('FDATEEND').Value :=  GetDate(QRParameters.FDateTo);
    // RV092.14.
    if not QRParameters.FAllTeam then
      QueryEMP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    if not QueryEMP.Prepared then
      QueryEMP.Prepare;
    QueryEMP.Active := True;
    Result := not QueryEMP.IsEmpty;
    if Result then
    begin
      QueryEmployee.Close;
      QueryEmployee.Open;
      QueryOCP.Close;
      QueryOCP.ParamByName('DATEMIN').Value := GetDate(QRParameters.FDateFrom);
      QueryOCP.ParamByName('DATEMAX').Value := GetDate(QRParameters.FDateTo);
      QueryOCP.Open;
      QueryEmpPln.Close;
      QueryEmpPln.ParamByName('DATEMIN').Value := GetDate(QRParameters.FDateFrom);
      QueryEmpPln.ParamByName('DATEMAX').Value := GetDate(QRParameters.FDateTo);
      QueryEmpPln.Open;
      RequestEarlyLateOpen(GetDate(QRParameters.FDateFrom),
        GetDate(QRParameters.FDateTo));
    end;
  end;
  FEndDate :=  GetDate(QRParameters.FDateTo);
  FStartDate := GetDate(QRParameters.FDateFrom);
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  for i:= 1 to 7 do
  begin
    if (FStartDate + i - 1) <= FEndDate then
    begin
      K := FStartDay + i - 1;
      if K >= 8 then
        K := K - 8 + 1;
      FDayDesc[i] := SystemDM.GetDayWCode(K);
      DecodeDate(QRParameters.FDateFrom + i - 1, Year, Month, Day);
      FDate[i] := IntToStr(Day);
    end
    else
    begin
      FDayDesc[i] := '';
      FDate[i] := '';
    end;
  end;
  FPaidBreaks := QRParameters.FPaidBreaks;
  FShowOnlyDiff := QRParameters.FShowOnlyDiff;
{ check if report is empty}

  Screen.Cursor := crDefault;
end;

procedure TReportStaffPlanningQR.ConfigReport;
begin
  // MR:29-09-2003
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelShiftFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end
  else
  begin
    QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
    QRLabelShiftTo.Caption := QRParameters.FShiftTo;
    QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
    QRLabelDeptTo.Caption := QRParameters.FDeptTo;
    QRLabelWKFrom.Caption := QRParameters.FWKFrom;
    QRLabelWKTo.Caption := QRParameters.FWKTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
// show tb or not
   QRBandEmpFooter.Enabled  := QRParameters.FShowTB;
   ChildBandEmpFooter.Enabled := QRParameters.FShowTB;
   QRBandWKFooter.Enabled := QRParameters.FShowTB;
   QRBandPlantFooter.Enabled := QRParameters.FShowTB;

   ChildBandEmp2.Enabled := not QRParameters.FShowTB;
   ChildBandWKTotals.Enabled := not QRParameters.FShowTB;
   ChildBandPlantTotals.Enabled := not QRParameters.FShowTB;
   ChildBandPlantFTTotals.Enabled := not QRParameters.FShowTB;
   ChildBandPlantFTOccTotals.Enabled := not QRParameters.FShowTB;

   QRGroupHDWK.Enabled := not QRParameters.FSortOnEmployee;
   if QRGroupHDWK.Enabled then
     QRGroupHDWK.Expression := 'WORKSPOT_CODE'
   else
     QRGroupHDWK.Expression := '';
end;

procedure TReportStaffPlanningQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportStaffPlanningQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportStaffPlanningQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelTeamFrom.Caption + ' ' +
        QRlabel90.Caption + ' ' + QRLabelTeamTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLabelWKFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLabelWKTo.Caption);
      ExportClass.AddText(QRLabel108.Caption + ' ' +
        QRLabel109.Caption + ' ' + QRLabelShiftFrom.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLabelShiftTo.Caption);
      ExportClass.AddText(QRLabel110.Caption + ' ' +
        QRLabel111.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabel113.Caption + ' ' + QRLabelDateTo.Caption);
    end;
  end;
end;

procedure TReportStaffPlanningQR.InitializePlanningPerDaysOfWeek
 (var PlanningPerDaysOfWeek: TTBWeekArray);
var
  Counter, i: Integer;
begin
  for Counter := 1 to 7 do
    for i := 1 to SystemDM.MaxTimeblocks do
    begin
      PlanningPerDaysOfWeek[Counter][i] := 0;
      FEmpPlanRecord[Counter][i].ADate := 0;
      FEmpPlanRecord[Counter][i].AStart := 0;
      FEmpPlanRecord[Counter][i].AEnd := 0;
      FEmpPlanRecord[Counter][i].AWorkspotCode := '';
      FEmpPlanRecord[Counter][i].AWDescription := '';
    end;
end;

procedure TReportStaffPlanningQR.FillAmountsPerDaysOfWeek(IndexMin, IndexMax: Integer;
  AmountsPerDaysOfWeek: TTBWeekArray);
var
//  TempComponent: TComponent;
//  Counter, Index, IndexDay, IndexTB: Integer;
  Day, Timeblock: Integer;
  C: TComponent;
  WeekTotal, WeeksTotal: Integer;
  MyMaxTB: Integer;
//  RequestEarlyLate: TDateTime;
//  RequestEarlyMessage, RequestLateMessage: String;
  function WKDiff: String;
  var
    TotalWeekWK, TotalWeekSTO, I, J: Integer;
  begin
    TotalWeekSTO := 0;
    TotalWeekWK := 0;
    for i := 1 to 7 do
      for j := 1 to SystemDM.MaxTimeblocks do
      begin
        TotalWeekSTO := TotalWeekSTO + FSTOWK[i,j];
        TotalWeekWK := TotalWeekWK + FWKPlanning[i,j];
      end;
    Result := MyDecodeHrsMin(TotalWeekWK - TotalWeekSTO);
  end; // WKDiff
  function MyMaxTBResult: Integer;
  var
    Day, TB, Total: Integer;
  begin
    Result := 1;
    for TB := MaxTBToShow downto 1 do
    begin
      Total := 0;
      if QRParameters.FShowStartEndTimes then
      begin
        for Day := 1 to 7 do
          if FEmpPlanRecord[Day][TB].AStart <> 0 then
            Total := Total + 1;
      end
      else
      begin
        for Day := 1 to 7 do
          Total := Total + AmountsPerDaysOfWeek[Day][TB];
      end;
      if Total <> 0 then
      begin
        Result := TB;
        Break;
      end;
    end; // for TB
  end; // MyMaxTB
begin

  // Details Employee
  WeeksTotal := 0;
  if IndexMin = 121 then
  begin
    MyMaxTB := MyMaxTBResult;
    for Timeblock := 1 to MaxTBToShow do
    begin
      QRMemoETB.Lines.Add(IntToStr(Timeblock));
      if QRParameters.FSortOnEmployee then
        QRMemoETB.Lines.Add('');
      WeekTotal := 0;
      for Day := 1 to 7 do
      begin
        C := FindComponent('QRMemoE' + IntToStr(Day));
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            // PIM-177
            // Show workspot (per emp, timeblock, day)
            if QRParameters.FSortOnEmployee then
            begin
              if FEmpPlanRecord[Day][Timeblock].AStart <> 0 then
                TQRMemo(C).Lines.Add(Copy(FEmpPlanRecord[Day][Timeblock].AWDescription, 1, 13)) // 12
              else
                TQRMemo(C).Lines.Add('');
            end;
            if QRParameters.FShowStartEndTimes then
              TQRMemo(C).Lines.Add(
                DateTime2StringTime(FEmpPlanRecord[Day][Timeblock].AStart) + '-' +
                DateTime2StringTime(FEmpPlanRecord[Day][Timeblock].AEnd))
            else
              TQRMemo(C).Lines.Add(MyDecodeHrsMin(AmountsPerDaysOfWeek[Day][Timeblock]));
            WeekTotal := WeekTotal + AmountsPerDaysOfWeek[Day][Timeblock];
          end;
      end; // for Day
      WeeksTotal := WeeksTotal + WeekTotal;
      QRMemoETot.Lines.Add(MyDecodeHrsMin(WeekTotal));
      QRMemoEWeeksTot.Lines.Add('');
      if QRParameters.FSortOnEmployee then
      begin
        QRMemoETot.Lines.Add('');
        QRMemoEWeeksTot.Lines.Add('');
      end;
      if MyMaxTB = Timeblock then
        Break;
    end; // for Timeblock
    QRMemoEWeeksTot.Lines.Add(MyDecodeHrsMin(WeeksTotal));
  end; // if

  // Totals Workspot -> Determine first max. timeblocks to show based on selected shift
  WeeksTotal := 0;
  if IndexMin = 221 then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      QRMemoWKTB.Lines.Add(IntToStr(Timeblock));
      WeekTotal := 0;
      for Day := 1 to 7 do
      begin
        C := FindComponent('QRMemoWK' + IntToStr(Day));
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            TQRMemo(C).Lines.Add(MyDecodeHrsMin(AmountsPerDaysOfWeek[Day][Timeblock]));
            WeekTotal := WeekTotal + AmountsPerDaysOfWeek[Day][Timeblock];
          end;
      end; // for Day
      WeeksTotal := WeeksTotal + WeekTotal;
      QRMemoWKTot.Lines.Add(MyDecodeHrsMin(WeekTotal));
      QRMemoWKWeeksTot.Lines.Add('');
      QRMemoWKDiff.Lines.Add('');
    end; // for Timeblock
    QRMemoWKWeeksTot.Lines.Add(MyDecodeHrsMin(WeeksTotal));
    QRMemoWKDiff.Lines.Add(WKDiff);
  end; // if

  // Totals Workspot Occupation Standard Totals
  WeeksTotal := 0;
  if IndexMin = 321 then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      QRMemoWKOCTB.Lines.Add(IntToStr(Timeblock));
      WeekTotal := 0;
      for Day := 1 to 7 do
      begin
        C := FindComponent('QRMemoWKOC' + IntToStr(Day));
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            TQRMemo(C).Lines.Add(MyDecodeHrsMin(AmountsPerDaysOfWeek[Day][Timeblock]));
            WeekTotal := WeekTotal + AmountsPerDaysOfWeek[Day][Timeblock];
          end;
      end; // for Day
      WeeksTotal := WeeksTotal + WeekTotal;
      QRMemoWKOCTot.Lines.Add(MyDecodeHrsMin(WeekTotal));
      QRMemoWKOCWeeksTot.Lines.Add('');
    end; // for Timeblock
    QRMemoWKOCWeeksTot.Lines.Add(MyDecodeHrsMin(WeeksTotal));
  end; // if

  // Totals Plant
  WeeksTotal := 0;
  if IndexMin = 421 then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      QRMemoPTotTB.Lines.Add(IntToStr(Timeblock));
      WeekTotal := 0;
      for Day := 1 to 7 do
      begin
        C := FindComponent('QRMemoP' + IntToStr(Day));
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            TQRMemo(C).Lines.Add(MyDecodeHrsMin(AmountsPerDaysOfWeek[Day][Timeblock]));
            WeekTotal := WeekTotal + AmountsPerDaysOfWeek[Day][Timeblock];
          end;
      end; // for Day
      WeeksTotal := WeeksTotal + WeekTotal;
      QRMemoPTot.Lines.Add(MyDecodeHrsMin(WeekTotal));
      QRMemoPWeeksTot.Lines.Add('');
      QRMemoPDiff.Lines.Add('');
    end; // for Timeblock
    QRMemoPWeeksTot.Lines.Add(MyDecodeHrsMin(WeeksTotal));
    QRMemoPDiff.Lines.Add(WKDiff);
  end; // if

  // Totals Plant Occupation Standard
  WeeksTotal := 0;
  if IndexMin = 521 then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      QRMemoPTotOccTB.Lines.Add(IntToStr(Timeblock));
      WeekTotal := 0;
      for Day := 1 to 7 do
      begin
        C := FindComponent('QRMemoPO' + IntToStr(Day));
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            TQRMemo(C).Lines.Add(MyDecodeHrsMin(AmountsPerDaysOfWeek[Day][Timeblock]));
            WeekTotal := WeekTotal + AmountsPerDaysOfWeek[Day][Timeblock];
          end;
      end; // for Day
      WeeksTotal := WeeksTotal + WeekTotal;
      QRMemoPOTot.Lines.Add(MyDecodeHrsMin(WeekTotal));
      QRMemoPOWeeksTot.Lines.Add('');
    end; // for Timeblock
    QRMemoPOWeeksTot.Lines.Add(MyDecodeHrsMin(WeeksTotal));
  end; // if
end; // FillAmountsPerDaysOfWeek

procedure TReportStaffPlanningQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 100) and (Index <= 106) then
      begin
        (TempComponent as TQRLabel).Caption := FDate[Index - 100 + 1];
      end;
      if (Index >= 10) and (Index <= 16) then
      begin
        (TempComponent as TQRLabel).Caption := FDayDesc[Index - 10 + 1];
      end;
    end;
  end;
end;

procedure TReportStaffPlanningQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  InitializePlanningPerDaysOfWeek(FPlantPlanning);
  InitializePlanningPerDaysOfWeek(FSTOPlant);
end;

procedure TReportStaffPlanningQR.QRDBTextDeptDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanningDM do
  begin
    Value := '';
    if QueryDept.Locate('PLANT_CODE;DEPARTMENT_CODE',
      VarArrayOf([QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('DEPARTMENT_CODE').AsString]) , [])  then
       Value := QueryDept.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanningQR.QRDBTextShiftDescPrint(sender: TObject;
  var Value: String);
var
  Plant: String;
begin
  inherited;
  with ReportStaffPlanningDM do
  begin
    Value := '';
    if QueryShift.Locate('PLANT_CODE;SHIFT_NUMBER',
       VarArrayOf([Plant, QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]), []) then
      Value := QueryShift.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanningQR.QRLabelPlantDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanningDM do
  begin
    Value := '';
    if TablePlant.FindKey([QueryEMP.FieldByName('PLANT_CODE').AsString]) then
    // MR: 22-02-2005 Order 550380 Don't truncate the description
//      Value := Copy(TablePlant.FieldByName('DESCRIPTION').AsString,0,10);
      Value := TablePlant.FieldByName('DESCRIPTION').AsString;
  end;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  WKCode, Plant, Dept, TeamFrom, TeamTo, DeptDesc: String;
  Shift: Integer;
  Flag: String;
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  QRGroupHDWK.ForceNewPage := QRParameters.FPageWK;
  TeamFrom := QRParameters.FTeamFrom;
  TeamTo := QRParameters.FTeamTo;
  InitializePlanningPerDaysOfWeek(FSTOWK);
  InitializePlanningPerDaysOfWeek(FWKPlanning);
  Plant := ReportStaffPlanningDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanningDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
  Shift := ReportStaffPlanningDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
  WKCode := ReportStaffPlanningDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString;
  // 20013910 Use Flag to determine if it is about a workspot or a department
  Flag := ReportStaffPlanningDM.QueryEMP.FieldByName('FLAG').AsString;
  if WKCode <> AvailableDummyWorkspotCode then
  begin
    if Flag = 'D' then
      WKCode := '0';
  end;

  FWKPrint := True;
  if not QRParameters.FAllTeam then
    if not ReportStaffPlanningDM.ISNotDeptValid(Dept,Plant, TeamFrom, TeamTo) then
    begin
      PrintBand := False;
      FWKPrint := False;
      Exit;
    end;
  ReportStaffPlanningDM.GetSTO(Plant, WKCode, Dept, Shift, FPaidBreaks,
    FStartDate, FEndDate, FSTOWK);
  if FShowOnlyDiff then
    FWKPrint := ReportStaffPlanningDM.CheckWKIsPrinted(Plant, WKCode, Dept,
      Shift, FPaidBreaks,FStartDay, FStartDate, FEndDate)
  else
    FWKPrint := True;
  if not QRParameters.FSortOnEmployee then
    PrintBand := FWKPrint;
  QRLabelWKCode.Caption := '';
  DeptDesc := '';
  if ReportStaffPlanningDM.QueryDept.Locate('PLANT_CODE;DEPARTMENT_CODE',
       VarArrayOf([Plant,Dept]), []) then
    DeptDesc :=
      ReportStaffPlanningDM.QueryDept.FieldByName('DESCRIPTION').AsString;

  // 20013910 Use Flag to determine if it is about a workspot or a department
  if WKCode <> AvailableDummyWorkspotCode then
  begin
    if Flag = 'W' then
    begin
      if ReportStaffPlanningDM.QueryWK.Locate('PLANT_CODE;WORKSPOT_CODE',
        VarArrayOf([Plant, WKCode]) , []) then
      begin
        QRLabelWKCode.Caption := WKCode + ' ' +
          ReportStaffPlanningDM.QueryWK.FieldByName('DESCRIPTION').AsString ;
        if DeptDesc <> '' then
          QRLabelWKCode.Caption := QRLabelWKCode.Caption + ' (' +
            Dept + ' ' +  DeptDesc + ')';
      end;
    end
    else
    begin
      QRLabelWKCode.Caption :=
        ReportStaffPlanningDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
      if DeptDesc <> '' then
        QRLabelWKCode.Caption := QRLabelWKCode.Caption + ' ' + DeptDesc;
    end;
  end
  else
  begin
    QRLabelWKCode.Caption := SPimsNotPlannedWorkspot;
  end;
end;

procedure TReportStaffPlanningQR.QRGroupHDEmpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Empl, Shift: Integer;
  Plant, Dept{, EmplDescription}: String;
begin
  inherited;
  PrintBand := False;
  // 20013910 Related to this order: Do this somewhere else.
{  QRLblEmpDesc.Caption := '';
  QRLblEmpDescChild.Caption := ''; }

  QRMemoETB.Lines.Clear;
  QRMemoE1.Lines.Clear;
  QRMemoE2.Lines.Clear;
  QRMemoE3.Lines.Clear;
  QRMemoE4.Lines.Clear;
  QRMemoE5.Lines.Clear;
  QRMemoE6.Lines.Clear;
  QRMemoE7.Lines.Clear;
  QRMemoETot.Lines.Clear;
  QRMemoEWeeksTot.Lines.Clear;

  InitializePlanningPerDaysOfWeek(FEmpPlanning);
  if not QRParameters.FSortOnEmployee then
    if not FWKPrint then
    begin
      PrintBand := FWKPrint;
      Exit;
    end;
  Empl := ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Shift := ReportStaffPlanningDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
  Plant := ReportStaffPlanningDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanningDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
  if Empl > 0 then
  begin
    ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept,  FPaidBreaks);
  end;
end; // QRGroupHDEmpBeforePrint

procedure TReportStaffPlanningQR.QRGroupDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // MR:26-07-2004 Order 550327 Also show not planned employees.
  if QRParameters.FShowNotPlannedEmployees then
  begin
    with ReportStaffPlanningDM do
    begin
      if QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger > 0 then
      begin
        if QueryEMP.FieldByName('WORKSPOT_CODE').AsString <>
          AvailableDummyWorkspotCode then
        begin
          if not cdsPlannedEmployee.FindKey([
            QueryEMP.FieldByName('PLANT_CODE').AsString,
            QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger,
            QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime
            ]) then
          begin
            cdsPlannedEmployee.Insert;
            cdsPlannedEmployee.FieldByName('PLANT_CODE').AsString :=
              QueryEMP.FieldByName('PLANT_CODE').AsString;
            cdsPlannedEmployee.FieldByName('SHIFT_NUMBER').AsInteger :=
              QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
            cdsPlannedEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger :=
              QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            cdsPlannedEmployee.FieldByName('PLAN_DATE').AsDateTime :=
              QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
            cdsPlannedEmployee.Post;
          end;
        end;
      end;
    end;
  end;

  if not QRParameters.FSortOnEmployee then
    if not FWKPrint then
    begin
      PrintBand := FWKPrint;
      Exit;
    end;
end; // QRGroupDateBeforePrint

procedure TReportStaffPlanningQR.QRBandEmpFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i, j: Integer;
  TotalWeek: Integer;
begin
  inherited;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FSortOnEmployee then
    PrintBand := FWKPrint
  else
    PrintBand := True;
  if not PrintBand then
    Exit;
  if (ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger < 0) then
  begin
    PrintBand := False;
    Exit;
  end;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := FEMPPlanning[i][j] + TotalWeek;
  if TotalWeek = 0 then
  begin
    PrintBand := False;
    Exit;
  end;
  FillAmountsPerDaysOfWeek(121, 184, FEmpPlanning);
  for i:= 1 to 7 do
    for j := 1 to SystemDM.MaxTimeblocks do
    begin
      FWKPlanning[i][j] := FWKPlanning[i][j]+ FEmpPlanning[i][j];
    end;
  PrintBand := False;
end;

procedure TReportStaffPlanningQR.QRBandWKFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i, j: Integer;
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  QRMemoWKTB.Lines.Clear;
  QRMemoWK1.Lines.Clear;
  QRMemoWK2.Lines.Clear;
  QRMemoWK3.Lines.Clear;
  QRMemoWK4.Lines.Clear;
  QRMemoWK5.Lines.Clear;
  QRMemoWK6.Lines.Clear;
  QRMemoWK7.Lines.Clear;
  QRMemoWKTot.Lines.Clear;
  QRMemoWKWeeksTot.Lines.Clear;
  QRMemoWKDiff.Lines.Clear;

  QRMemoWKOCTB.Lines.Clear;
  QRMemoWKOC1.Lines.Clear;
  QRMemoWKOC2.Lines.Clear;
  QRMemoWKOC3.Lines.Clear;
  QRMemoWKOC4.Lines.Clear;
  QRMemoWKOC5.Lines.Clear;
  QRMemoWKOC6.Lines.Clear;
  QRMemoWKOC7.Lines.Clear;
  QRMemoWKOCTot.Lines.Clear;
  QRMemoWKOCWeeksTot.Lines.Clear;

  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FSortOnEmployee then
    PrintBand := FWKPrint
  else
    PrintBand := True;
  if not PrintBand then
    Exit;

  FillAmountsPerDaysOfWeek(221, 284, FWKPlanning);

  FillAmountsPerDaysOfWeek(321, 384, FSTOWK);
  for i:= 1 to 7 do
    for j := 1 to SystemDM.MaxTimeblocks do
    begin
      FPlantPlanning[i][j] := FPlantPlanning[i][j]+ FWKPlanning[i][j];
      FSTOPlant[I][J] := FSTOPlant[I][J] + FSTOWK[I][J];
    end;
  PrintBand := False;
end;

procedure TReportStaffPlanningQR.ChildBandHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FShowTB;
  if PrintBand then
    FillDescDaysPerWeek;
  if PrintBand then
    if QRParameters.FSortOnEmployee then
    begin
      QRLabel12.Caption := '';
    end;
end;

procedure TReportStaffPlanningQR.QRLabel160Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if Pos('(', QRLabelWKCode.Caption) > 0 then
    Value := Copy(QRLabelWKCode.Caption,0, Pos('(', QRLabelWKCode.Caption) -1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel9Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FEMPPlanning[i][1] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel43Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FEMPPlanning[i][2] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel57Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FEMPPlanning[i][3] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel72Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FEMPPlanning[i][4] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel74Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i , j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FEMPPlanning[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRBandPlantFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;

  QRMemoPTotTB.Lines.Clear;
  QRMemoP1.Lines.Clear;
  QRMemoP2.Lines.Clear;
  QRMemoP3.Lines.Clear;
  QRMemoP4.Lines.Clear;
  QRMemoP5.Lines.Clear;
  QRMemoP6.Lines.Clear;
  QRMemoP7.Lines.Clear;
  QRMemoPTot.Lines.Clear;
  QRMemoPWeeksTot.Lines.Clear;
  QRMemoPDiff.Lines.Clear;

  QRMemoPTotOccTB.Lines.Clear;
  QRMemoPO1.Lines.Clear;
  QRMemoPO2.Lines.Clear;
  QRMemoPO3.Lines.Clear;
  QRMemoPO4.Lines.Clear;
  QRMemoPO5.Lines.Clear;
  QRMemoPO6.Lines.Clear;
  QRMemoPO7.Lines.Clear;
  QRMemoPOTot.Lines.Clear;

  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  FillAmountsPerDaysOfWeek(421, 484, FPlantPlanning);
  FillAmountsPerDaysOfWeek(521, 584, FSTOPlant);
  
  PrintBand := False;
end;

procedure TReportStaffPlanningQR.QRLabel155Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i , j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FWKPlanning[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel199Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i , j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FSTOWK[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel200Print(sender: TObject;
  var Value: String);
var
  TotalWeekWK, TotalWeekSTO, I, J: Integer;
begin
  inherited;
  TotalWeekSTO := 0;
  TotalWeekWK := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
    begin
      TotalWeekSTO := TotalWeekSTO + FSTOWK[I,J];
      TotalWeekWK := TotalWeekWK + FWKPlanning[I,J];
    end;
  Value := MyDecodeHrsMin(TotalWeekWK - TotalWeekSTO);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel238Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i , j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FSTOPlant[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;


procedure TReportStaffPlanningQR.QRLabel239Print(sender: TObject;
  var Value: String);
var
  TotalWeekWK, TotalWeekSTO, I, J: Integer;
begin
  inherited;
  TotalWeekSTO := 0;
  TotalWeekWK := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
    begin
      TotalWeekSTO := TotalWeekSTO + FSTOPlant[I,J];
      TotalWeekWK := TotalWeekWK + FPlantPlanning[I,J];
    end;
  Value := MyDecodeHrsMin(TotalWeekWK - TotalWeekSTO);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel118Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FWKPlanning[i][1] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel117Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FWKPlanning[i][2] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;


procedure TReportStaffPlanningQR.QRLabel116Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FWKPlanning[i][3] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;


procedure TReportStaffPlanningQR.QRLabel115Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FWKPlanning[i][4] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel195Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOWK[i][1] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel196Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOWK[i][2] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel197Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOWK[i][3] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel198Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOWK[i][4] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel154Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FPlantPlanning[i][1] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel153Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FPlantPlanning[i][2] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel152Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FPlantPlanning[i][3] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel151Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FPlantPlanning[i][4] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel234Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOPlant[i][1] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel235Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOPlant[i][2] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel236Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOPlant[i][3] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel237Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    TotalWeek := FSTOPlant[i][4] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel156Print(sender: TObject;
  var Value: String);
var
  TotalWeek, I, J: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FPlantPlanning[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRGroupHDShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHdShift.ForceNewPage := QRParameters.FPageShift;
  if QRParameters.FShowNotPlannedEmployees then
    ReportStaffPlanningDM.cdsPlannedEmployee.EmptyDataSet;
  // MR:16-02-2005 Order 550380
  with ReportStaffPlanningDM do
  begin
    if QueryShift.Locate('PLANT_CODE;SHIFT_NUMBER',
       VarArrayOf([QueryEMP.FieldByName('PLANT_CODE').AsString,
         QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]), []) then
      QRLabelShiftDesc.Caption :=
        QueryShift.FieldByName('DESCRIPTION').AsString;
    MaxTBToShow := ShiftTBCount(QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger);
  end;
  inherited;
end;

procedure TReportStaffPlanningQR.QRBandHeaderBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowTB;
  if PrintBand then
    FillDescDaysPerWeek;
  if PrintBand then
    if QRParameters.FSortOnEmployee then
      QRLabel241.Caption := '';
end;

procedure TReportStaffPlanningQR.QRLabel30Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(1);
  (Sender as TQRLabel).Caption := Value;
end;


procedure TReportStaffPlanningQR.QRLabel64Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(2);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel308Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i , j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to 7 do
    for j:= 1 to SystemDM.MaxTimeblocks do
      TotalWeek := TotalWeek + FWKPlanning[I,J];
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;


procedure TReportStaffPlanningQR.QRLabel162Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(3);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel33Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(4);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel174Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(5);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel303Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(6);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel304Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(7);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel34Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel59Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(5);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel263Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(4);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel305Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(6);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel307Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(7);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel63Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel36Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(3);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel35Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(2);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel54Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBWKPerDay(4);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel264Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(5);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel265Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(6);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel266Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOWKPerDay(7);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel267Print(sender: TObject;
  var Value: String);
var
  TotalWeek, i, j: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i := 1 to 7 do
    for j := 1 to SystemDM.MaxTimeblocks do
      TotalWeek := FSTOWK[i][j] + TotalWeek;
  Value := MyDecodeHrsMin(TotalWeek);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel31Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(2);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel32Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBEMPPerDay(3);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel270Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel271Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(2);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel272Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(3);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel273Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(4);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel274Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(5);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel275Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(6);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel276Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBPlantPerDay(7);
  (Sender as TQRLabel).Caption := Value;
end;

function TReportStaffPlanningQR.GetTotalTBSTOPlantPerDay(Day_Number: Integer): String;
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to SystemDM.MaxTimeblocks do
    TotalWeek := FSTOPlant[Day_Number][i] + TotalWeek;
  Result := MyDecodeHrsMin(TotalWeek);
end;

function TReportStaffPlanningQR.GetTotalTBEMPPerDay(Day_Number: Integer): String;
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  Result := '';
  for i:= 1 to SystemDM.MaxTimeblocks do
  begin
    // PIM-177 Get start-time of the shift
    if QRParameters.FShowStartEndTimes then
    begin
      // Get start-time of the shift
      if FEmpPlanRecord[Day_Number][i].AStart <> 0 then
      begin
        Result := DateTime2StringTime(FEmpPlanRecord[Day_Number][i].AStart);
        Break;
      end;
    end
    else
      TotalWeek := FEMPPlanning[Day_Number][i] + TotalWeek;
  end;
  // PIM-177 Get end-time of the shift
  if QRParameters.FShowStartEndTimes then
  begin
    for i := SystemDM.MaxTimeblocks downto 1 do
      if FEmpPlanRecord[Day_Number][i].AEnd <> 0 then
      begin
        Result := Result + '-' +
          DateTime2StringTime(FEmpPlanRecord[Day_Number][i].AEnd);
        Break;
      end;
  end;
  if not QRParameters.FShowStartEndTimes then
    Result := MyDecodeHrsMin(TotalWeek);
end;

function TReportStaffPlanningQR.GetTotalTBSTOWKPerDay(Day_Number: Integer): String;
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to SystemDM.MaxTimeblocks do
    TotalWeek := FSTOWK[Day_Number][i] + TotalWeek;
  Result := MyDecodeHrsMin(TotalWeek);
end;

function TReportStaffPlanningQR.GetTotalTBWKPerDay(Day_Number: Integer): String;
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to SystemDM.MaxTimeblocks do
    TotalWeek := FWKPlanning[Day_Number][i] + TotalWeek;
  Result := MyDecodeHrsMin(TotalWeek);
end;

function TReportStaffPlanningQR.GetTotalTBPlantPerDay(Day_Number: Integer): String;
var
  TotalWeek, i: Integer;
begin
  inherited;
  TotalWeek := 0;
  for i:= 1 to SystemDM.MaxTimeblocks do
    TotalWeek := FPlantPlanning[Day_Number][i] + TotalWeek;
  Result := MyDecodeHrsMin(TotalWeek);
end;

procedure TReportStaffPlanningQR.QRLabel277Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel278Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(2);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel283Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(3);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel284Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(4);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel285Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(5);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel286Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(6);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRLabel294Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := GetTotalTBSTOPlantPerDay(7);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningQR.QRBandHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(QRLabel241.Caption);
    ExportClass.AddText(
      QRLabel242.Caption + ' ' +
      QRLabel243.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel244.Caption + ExportClass.Sep +
      QRLabel247.Caption + ExportClass.Sep +
      QRLabel248.Caption + ExportClass.Sep +
      QRLabel251.Caption + ExportClass.Sep +
      QRLabel252.Caption + ExportClass.Sep +
      QRLabel255.Caption + ExportClass.Sep +
      QRLabel256.Caption + ExportClass.Sep +
      QRLabel259.Caption + ExportClass.Sep +
      QRLabel261.Caption + ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel245.Caption + ExportClass.Sep +
      QRLabel246.Caption + ExportClass.Sep +
      QRLabel249.Caption + ExportClass.Sep +
      QRLabel250.Caption + ExportClass.Sep +
      QRLabel253.Caption + ExportClass.Sep +
      QRLabel254.Caption + ExportClass.Sep +
      QRLabel257.Caption + ExportClass.Sep +
      QRLabel258.Caption + ExportClass.Sep +
      QRLabel260.Caption + ExportClass.Sep +
      QRLabel262.Caption + ExportClass.Sep);
  end;
end;

procedure TReportStaffPlanningQR.ChildBandHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(QRLabel12.Caption);
    ExportClass.AddText(
      QRLabel19.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel21.Caption + ExportClass.Sep +
      QRLabel27.Caption + ExportClass.Sep +
      QRLabel28.Caption + ExportClass.Sep +
      QRLabel288.Caption + ExportClass.Sep +
      QRLabel289.Caption + ExportClass.Sep +
      QRLabel296.Caption + ExportClass.Sep +
      QRLabel297.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel301.Caption + ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel25.Caption + ExportClass.Sep +
      QRLabel26.Caption + ExportClass.Sep +
      QRLabel29.Caption + ExportClass.Sep +
      QRLabel287.Caption + ExportClass.Sep +
      QRLabel290.Caption + ExportClass.Sep +
      QRLabel295.Caption + ExportClass.Sep +
      QRLabel298.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel309.Caption + ExportClass.Sep +
      QRLabel310.Caption + ExportClass.Sep);
  end;
end;

procedure TReportStaffPlanningQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Plant
    with ReportStaffPlanningDM do
    begin
      ExportClass.AddText(QRLabel16.Caption + ExportClass.Sep +
        QRLabel17.Caption + ExportClass.Sep +
        QueryEMP.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabelPlantDesc.Caption
        );
    end;
  end;
end;

procedure TReportStaffPlanningQR.QRGroupHDShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Shift
    with ReportStaffPlanningDM do
    begin
      ExportClass.AddText(QRLabel23.Caption + ExportClass.Sep +
        QRLabel24.Caption + ExportClass.Sep +
        QueryEMP.FieldByName('SHIFT_NUMBER').AsString + ExportClass.Sep +
        QRLabelShiftDesc.Caption
        );
    end;
  end;
end;

procedure TReportStaffPlanningQR.QRGroupHDWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Workspot
    ExportClass.AddText(QRLabelWKCode.Caption);
  end;
end;

procedure TReportStaffPlanningQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportStaffPlanningQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

// MR:26-07-2004 Order 550327
// Instead of '00:00' show '-'
function TReportStaffPlanningQR.MyDecodeHrsMin(HrsMin: Integer): String;
begin
  if HrsMin = 0 then
    Result := '-'
  else
    Result := DecodeHrsMin(HrsMin);
end;

function TReportStaffPlanningQR.EmployeeDescription: String;
var
  Empl: Integer;
begin
  Result := '';
  Empl := ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if Empl > 0 then
  begin
  //CAR 22-7-2003
    if ReportStaffPlanningDM.QueryEmployee.Locate('EMPLOYEE_NUMBER',
      Empl, []) then
      Result :=
        Copy(ReportStaffPlanningDM.
          QueryEmployee.FieldByName('DESCRIPTION').AsString, 1, {15} 30)
    else
      Result := '';
  end;
end; // EmployeeDescription

procedure TReportStaffPlanningQR.QRLblEmpDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := EmployeeDescription;
end;

procedure TReportStaffPlanningQR.QRLblEmpDescChildPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := EmployeeDescription;
end;

procedure TReportStaffPlanningQR.ChildBandPlantFTOccTotalsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
end;

procedure TReportStaffPlanningQR.ChildBandWKTotalsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffPlanningDM do
    begin
      ExportClass.AddText(QRLabel60.Caption + ExportClass.Sep +
        QRLabel61.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel34.Caption + ExportClass.Sep +
        QRLabel35.Caption + ExportClass.Sep +
        QRLabel36.Caption + ExportClass.Sep +
        QRLabel54.Caption + ExportClass.Sep +
        QRLabel59.Caption + ExportClass.Sep +
        QRLabel305.Caption + ExportClass.Sep +
        QRLabel307.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel308.Caption + ExportClass.Sep +
        QRLabel269.Caption + ExportClass.Sep
        );
      ExportClass.AddText(QRLabel62.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel63.Caption + ExportClass.Sep +
        QRLabel64.Caption + ExportClass.Sep +
        QRLabel162.Caption + ExportClass.Sep +
        QRLabel263.Caption + ExportClass.Sep +
        QRLabel264.Caption + ExportClass.Sep +
        QRLabel265.Caption + ExportClass.Sep +
        QRLabel266.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel267.Caption + ExportClass.Sep
        );
    end;
  end;
end;

procedure TReportStaffPlanningQR.ChildBandWKTotalsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i,j : Integer;
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;

  for i:= 1 to 7 do
    for j := 1 to SystemDM.MaxTimeblocks do
    begin
      FPlantPlanning[i][j] := FPlantPlanning[i][j]+ FWKPlanning[i][j];
      FSTOPlant[I][J] := FSTOPlant[I][J] + FSTOWK[I][J];
    end;

end;

procedure TReportStaffPlanningQR.ChildBandEmp2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffPlanningDM do
    begin
      ExportClass.AddText(QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString +
        ExportClass.Sep +
        EmployeeDescription + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel30.Caption + ExportClass.Sep +
        QRLabel31.Caption + ExportClass.Sep +
        QRLabel32.Caption + ExportClass.Sep +
        QRLabel33.Caption + ExportClass.Sep +
        QRLabel174.Caption + ExportClass.Sep +
        QRLabel303.Caption + ExportClass.Sep +
        QRLabel304.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel306.Caption + ExportClass.Sep
        );
    end;
  end;
end;

procedure TReportStaffPlanningQR.ChildBandEmp2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i,j : Integer;
begin
  inherited;
  if QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  if (ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger < 0) then
  begin
    PrintBand := False;
    Exit;
  end;
  for i:= 1 to 7 do
    for j := 1 to SystemDM.MaxTimeblocks do
      FWKPlanning[i][j] := FWKPlanning[i][j]+ FEmpPlanning[i][j];
end;

procedure TReportStaffPlanningQR.ChildBandEmpFooterBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FSortOnEmployee then
    PrintBand := FWKPrint
  else
    PrintBand := True;
  if not PrintBand then
    Exit;
  if (ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger < 0) then
  begin
    PrintBand := False;
    Exit;
  end;
end;

procedure TReportStaffPlanningQR.ChildBandPlantFTTotalsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  PrintBand := True;
end;

procedure TReportStaffPlanningQR.ChildBandWKOccStandardBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FSortOnEmployee then
    PrintBand := FWKPrint
  else
    PrintBand := True;
  if not PrintBand then
    Exit;
end;

procedure TReportStaffPlanningQR.ChildBandWKTotalCodeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FSortOnEmployee;
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if not QRParameters.FShowTB then
  begin
    PrintBand := False;
    Exit;
  end;
  PrintBand := FWKPrint;
  if not PrintBand then
    Exit;
end;

procedure TReportStaffPlanningQR.ChildBandEmpFooterAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Day: Integer;
  EmpNr, EmpName, TB: String;
  C: TComponent;
  Amount: array[1..8] of String;
  AmountTot: String;
  LineNumber: Integer;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    LineNumber := 0;
    while LineNumber < QRMemoETB.Lines.Count do
    begin
      AmountTot := '';
      TB := QRMemoETB.Lines.Strings[LineNumber];
      if LineNumber = 0 then
      begin
        EmpNr := ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString;
        EmpName := EmployeeDescription;
      end
      else
      begin
        EmpNr := '';
        EmpName := '';
      end;
      if LineNumber = QRMemoETB.Lines.Count - 1 then
        AmountTot := QRMemoEWeeksTot.Lines.Strings[QRMemoEWeeksTot.Lines.Count-1];
      for Day := 1 to 8 do
      begin
        Amount[Day] := '';
        if Day < 8 then
          C := Self.FindComponent('QRMemoE' + IntToStr(Day))
        else
          C := Self.FindComponent('QRMemoETot');
        if Assigned(C) then
          if (C is TQRMemo) then
          begin
            if LineNumber <= TQRMemo(C).Lines.Count-1 then
              Amount[Day] := TQRMemo(C).Lines.Strings[LineNumber];
          end;
      end; // for Day
      ExportClass.AddText(
        EmpNr + ExportClass.Sep +
        EmpName + ExportClass.Sep +
        TB + ExportClass.Sep +
        Amount[1] + ExportClass.Sep +
        Amount[2] + ExportClass.Sep +
        Amount[3] + ExportClass.Sep +
        Amount[4] + ExportClass.Sep +
        Amount[5] + ExportClass.Sep +
        Amount[6] + ExportClass.Sep +
        Amount[7] + ExportClass.Sep +
        Amount[8] + ExportClass.Sep +
        AmountTot + ExportClass.Sep
        );
      inc(LineNumber);
    end; // while
  end; // if
end;

procedure TReportStaffPlanningQR.ChildBandWKTotalCodeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Day, Timeblock: Integer;
  C: TComponent;
  Amount: array[1..8] of String;
  AmountTot: String;
  AmountDiff: String;
  TB, TotalStr, WKCode: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      AmountTot := '';
      AmountDiff := '';
      TotalStr := '';
      WKCode := '';
      TB := IntToStr(Timeblock);
      if Timeblock = 1 then
      begin
        TotalStr := QRLabel159.Caption;
        WKCode := QRLabel160.Caption;
      end;
      if Timeblock = MaxTBToShow then
      begin
        AmountTot := QRMemoWKWeeksTot.Lines.Strings[QRMemoWKWeeksTot.Lines.Count-1];
        AmountDiff := QRMemoWKDiff.Lines.Strings[QRMemoWKDiff.Lines.Count-1];
      end;
      for Day := 1 to 8 do
      begin
        Amount[Day] := '';
        if Day < 8 then
          C := Self.FindComponent('QRMemoWK' + IntToStr(Day))
        else
          C := Self.FindComponent('QRMemoWKTot');
          if Assigned(C) then
          if (C is TQRMemo) then
          begin
            if Timeblock <= TQRMemo(C).Lines.Count then
              Amount[Day] := TQRMemo(C).Lines.Strings[Timeblock-1];
          end;
      end;
       ExportClass.AddText(
        TotalStr + ExportClass.Sep +
        WKCode + ExportClass.Sep +
        TB + ExportClass.Sep +
        Amount[1] + ExportClass.Sep +
        Amount[2] + ExportClass.Sep +
        Amount[3] + ExportClass.Sep +
        Amount[4] + ExportClass.Sep +
        Amount[5] + ExportClass.Sep +
        Amount[6] + ExportClass.Sep +
        Amount[7] + ExportClass.Sep +
        Amount[8] + ExportClass.Sep +
        AmountTot + ExportClass.Sep +
        AmountDiff + ExportClass.Sep
      );
    end; // for Timeblock
  end; // if
end;

procedure TReportStaffPlanningQR.ChildBandWKOccStandardAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Day, Timeblock: Integer;
  C: TComponent;
  Amount: array[1..8] of String;
  AmountTot: String;
  TB, TitleStr: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      AmountTot := '';
      TitleStr := '';
      TB := IntToStr(Timeblock);
      if Timeblock = 1 then
      begin
        TitleStr := QRLabel157.Caption;
      end;
      if Timeblock = MaxTBToShow then
        AmountTot := QRMemoWKOCWeeksTot.Lines.Strings[QRMemoWKOCWeeksTot.Lines.Count-1];
      for Day := 1 to 8 do
      begin
        Amount[Day] := '';
        if Day < 8 then
          C := Self.FindComponent('QRMemoWKOC' + IntToStr(Day))
        else
          C := Self.FindComponent('QRMemoWKOCTot');
          if Assigned(C) then
          if (C is TQRMemo) then
          begin
            if Timeblock <= TQRMemo(C).Lines.Count then
              Amount[Day] := TQRMemo(C).Lines.Strings[Timeblock-1];
          end;
      end;
       ExportClass.AddText(
        TitleStr + ExportClass.Sep +
        TB + ExportClass.Sep +
        Amount[1] + ExportClass.Sep +
        Amount[2] + ExportClass.Sep +
        Amount[3] + ExportClass.Sep +
        Amount[4] + ExportClass.Sep +
        Amount[5] + ExportClass.Sep +
        Amount[6] + ExportClass.Sep +
        Amount[7] + ExportClass.Sep +
        Amount[8] + ExportClass.Sep +
        AmountTot + ExportClass.Sep
      );
    end; // for Timeblock
  end; // if
end;

procedure TReportStaffPlanningQR.ChildBandPlantFTTotalsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Day, Timeblock: Integer;
  C: TComponent;
  Amount: array[1..8] of String;
  AmountTot: String;
  AmountDiff: String;
  TB, TotalStr, PlantCodeDesc: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      AmountTot := '';
      AmountDiff := '';
      TotalStr := '';
      PlantCodeDesc := '';
      TB := IntToStr(Timeblock);
      if Timeblock = 1 then
      begin
        TotalStr := QRLabel158.Caption;
        PlantCodeDesc := ReportStaffPlanningDM.
          QueryEMP.FieldByName('PLANT_CODE').AsString + ' ' + QRLabel161.Caption;
      end;
      if Timeblock = MaxTBToShow then
      begin
        AmountTot := QRMemoPWeeksTot.Lines.Strings[QRMemoPWeeksTot.Lines.Count-1];
        AmountDiff := QRMemoPDiff.Lines.Strings[QRMemoPDiff.Lines.Count-1];
      end;
      for Day := 1 to 8 do
      begin
        Amount[Day] := '';
        if Day < 8 then
          C := Self.FindComponent('QRMemoP' + IntToStr(Day))
        else
          C := Self.FindComponent('QRMemoPTot');
          if Assigned(C) then
          if (C is TQRMemo) then
          begin
            if Timeblock <= TQRMemo(C).Lines.Count then
              Amount[Day] := TQRMemo(C).Lines.Strings[Timeblock-1];
          end;
      end;
       ExportClass.AddText(
        TotalStr + ExportClass.Sep +
        PlantCodeDesc + ExportClass.Sep +
        TB + ExportClass.Sep +
        Amount[1] + ExportClass.Sep +
        Amount[2] + ExportClass.Sep +
        Amount[3] + ExportClass.Sep +
        Amount[4] + ExportClass.Sep +
        Amount[5] + ExportClass.Sep +
        Amount[6] + ExportClass.Sep +
        Amount[7] + ExportClass.Sep +
        Amount[8] + ExportClass.Sep +
        AmountTot + ExportClass.Sep +
        AmountDiff + ExportClass.Sep
      );
    end; // for Timeblock
  end; // if
end;

procedure TReportStaffPlanningQR.ChildBandPlantFTOccTotalsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Day, Timeblock: Integer;
  C: TComponent;
  Amount: array[1..8] of String;
  AmountTot: String;
  TB, TitleStr, OccStandard: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    for Timeblock := 1 to MaxTBToShow do
    begin
      AmountTot := '';
      TitleStr := '';
      OccStandard := '';
      TB := IntToStr(Timeblock);
      if Timeblock = 1 then
      begin
        TitleStr := QRLabel240.Caption;
        OccStandard := QRLabel201.Caption;
      end;
      if Timeblock = MaxTBToShow then
        AmountTot := QRMemoPOWeeksTot.Lines.Strings[QRMemoPOWeeksTot.Lines.Count-1];
      for Day := 1 to 8 do
      begin
        Amount[Day] := '';
        if Day < 8 then
          C := Self.FindComponent('QRMemoPO' + IntToStr(Day))
        else
          C := Self.FindComponent('QRMemoPOTot');
          if Assigned(C) then
          if (C is TQRMemo) then
          begin
            if Timeblock <= TQRMemo(C).Lines.Count then
              Amount[Day] := TQRMemo(C).Lines.Strings[Timeblock-1];
          end;
      end;
       ExportClass.AddText(
        TitleStr + ExportClass.Sep +
        OccStandard +
        TB + ExportClass.Sep + ExportClass.Sep +
        Amount[1] + ExportClass.Sep +
        Amount[2] + ExportClass.Sep +
        Amount[3] + ExportClass.Sep +
        Amount[4] + ExportClass.Sep +
        Amount[5] + ExportClass.Sep +
        Amount[6] + ExportClass.Sep +
        Amount[7] + ExportClass.Sep +
        Amount[8] + ExportClass.Sep +
        AmountTot + ExportClass.Sep
      );
    end; // for Timeblock
  end; // if
end;

procedure TReportStaffPlanningQR.ChildBandPlantTotalsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True; // GLOB3-60.1 Added here.
  if not QRParameters.FShowTotals then
  begin
    PrintBand := False;
    Exit;
  end;
  if QRParameters.FShowTB then // GLOB3-60 'not' removed.
  begin
    PrintBand := False;
    Exit;
  end;
end;

procedure TReportStaffPlanningQR.ChildBandPlantTotalsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportStaffPlanningDM do
    begin
      ExportClass.AddText(QRLabel279.Caption + ExportClass.Sep +
        QueryEMP.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabel280.Caption + ExportClass.Sep +
        QRLabel270.Caption + ExportClass.Sep +
        QRLabel271.Caption + ExportClass.Sep +
        QRLabel272.Caption + ExportClass.Sep +
        QRLabel273.Caption + ExportClass.Sep +
        QRLabel274.Caption + ExportClass.Sep +
        QRLabel275.Caption + ExportClass.Sep +
        QRLabel276.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel291.Caption + ExportClass.Sep +
        QRLabel292.Caption + ExportClass.Sep
        );
      ExportClass.AddText(QRLabel281.Caption + ExportClass.Sep +
        QRLabel282.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel277.Caption + ExportClass.Sep +
        QRLabel278.Caption + ExportClass.Sep +
        QRLabel283.Caption + ExportClass.Sep +
        QRLabel284.Caption + ExportClass.Sep +
        QRLabel285.Caption + ExportClass.Sep +
        QRLabel286.Caption + ExportClass.Sep +
        QRLabel294.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabel293.Caption + ExportClass.Sep
        );
    end;
  end;
end;

procedure TReportStaffPlanningQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  DateEMP: TDateTime;
  i, DayEMP : Integer;
  IndexDay: Integer;
  FTBPlanned: Array[1..MAX_TBS] of Integer;
  Schedule_TB: TTBSchedule;
  IFirst, ILast: Integer;
  OldStart, OldEnd: TDateTime;
  function TimeDiffMinutes(AValue1, AValue2: TDatetime): Integer;
  begin
    Result := ATBLengthClass.GetMinutes(AValue1) - ATBLengthClass.GetMinutes(AValue2);
  end;
begin
  inherited;
  PrintBand := False;
  with ReportStaffPlanningDM.QueryEMP do
  begin
    if FieldByName('EMPLOYEE_NUMBER').AsInteger < 0 then
      Exit;

    // MR:26-07-2004 Order 550327 Also show not planned employees.
    if QRParameters.FShowNotPlannedEmployees then
    begin
      if FieldByName('WORKSPOT_CODE').AsString = AvailableDummyWorkspotCode then
        if ReportStaffPlanningDM.cdsPlannedEmployee.FindKey([
          FieldByName('PLANT_CODE').AsString,
          FieldByName('SHIFT_NUMBER').AsInteger,
          FieldByName('EMPLOYEE_NUMBER').AsInteger,
          FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime
          ]) then
        begin
          PrintBand := False;
          Exit;
        end;
    end;

    DateEMP := FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;

    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      // 20013910 Related to this order.
      // Assign this here without using a procedure.
      SCHEDULE_TB[i] :=
        ReportStaffPlanningDM.QueryEmp.
          FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
      if ((SCHEDULE_TB[i] = 'A') or (SCHEDULE_TB[i] = 'B') or
        (SCHEDULE_TB[i] = 'C')) or
        (QRParameters.FShowNotPlannedEmployees and
         (SCHEDULE_TB[i] = '*')) then
        FTBPlanned[i]:= i
      else
        FTBPlanned[i] := 0;
    end;
  end;

  DayEMP := ListProcsF.DayWStartOnFromDate(DateEMP);
  IndexDay := Trunc(DateEMP) - Trunc(FStartDate) + 1;
  if (IndexDay >= 1) and (IndexDay <= 7) then
  begin
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      // PIM-177
      if  ATBLengthClass.ExistTB(I) and (FTBPlanned[i] <> 0) then
      begin
        // PIM-177
        FEmpPlanRecord[IndexDay][i].ADate := DateEmp;
        FEmpPlanRecord[IndexDay][i].AStart :=
          ATBLengthClass.GetStartTime(i, DayEMP);
        FEmpPlanRecord[IndexDay][i].AEnd :=
          ATBLengthClass.GetEndTime(i, DayEMP);
        if FEmpPlanRecord[IndexDay][i].AStart <> 0 then
        begin
          FEmpPlanRecord[IndexDay][i].AWorkspotCode :=
            ReportStaffPlanningDM.QueryEMP.
              FieldByName('WORKSPOT_CODE').AsString;
          FEmpPlanRecord[IndexDay][i].AWDescription :=
            ReportStaffPlanningDM.QueryEMP.
              FieldByName('WDESCRIPTION').AsString;
        end;
        FEmpPlanning[IndexDay][i] := FEmpPlanning[IndexDay][i] +
          ATBLengthClass.GetLengthTB(I, DayEMP);
      end;
    end; // for
    // Early late: Only do this for first filled-in timeblock and
    // last filled-in timeblock!
    // Determine first and last filled-in timeblock
    IFirst := 0;
    ILast := 0;
    for i := 1 to SystemDM.MaxTimeblocks do
    begin
      if FEmpPlanRecord[IndexDay][i].AStart <> 0 then
      begin
        IFirst := i;
        Break;
      end;
    end; // for
    for i := SystemDM.MaxTimeblocks downto 1 do
    begin
      if FEmpPlanRecord[IndexDay][i].AStart <> 0 then
      begin
        ILast := i;
        Break;
      end;
    end; // for
    if IFirst <> 0 then
    begin
      i := IFirst;
      OldStart := FEmpPlanRecord[IndexDay][i].AStart;
      FEmpPlanRecord[IndexDay][i].AStart :=
        ReportStaffPlanningDM.RequestEarlyLateFindEarlyTime(DateEmp,
          ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            ATBLengthClass.GetStartTime(i, DayEMP)
            );
        if OldStart <> FEmpPlanRecord[IndexDay][i].AStart then
          FEmpPlanning[IndexDay][i] := FEmpPlanning[IndexDay][i] +
            TimeDiffMinutes(OldStart, FEmpPlanRecord[IndexDay][i].AStart);
    end; // if
    if ILast <> 0 then
    begin
      i := ILast;
      OldEnd := FEmpPlanRecord[IndexDay][i].AEnd;
      FEmpPlanRecord[IndexDay][i].AEnd :=
        ReportStaffPlanningDM.RequestEarlyLateFindLateTime(DateEmp,
          ReportStaffPlanningDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            ATBLengthClass.GetEndTime(i, DayEMP)
            );
      if OldEnd <> FEmpPlanRecord[IndexDay][i].AEnd then
        FEmpPlanning[IndexDay][i] := FEmpPlanning[IndexDay][i] +
          TimeDiffMinutes(FEmpPlanRecord[IndexDay][i].AEnd, OldEnd);
    end; // if
  end; // if
end; // QRBandDetailBeforePrint

end.
