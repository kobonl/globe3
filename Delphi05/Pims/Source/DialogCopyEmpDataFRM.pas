(*
  MRA:11-NOV-2016 PIM-174
  - New employee and add all related data
*)
unit DialogCopyEmpDataFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxExEdtr,
  dxEdLib, dxCntner, dxEditor, dxExGrEd, dxExELib, SystemDMT, Db, DBTables,
  dxLayout;

type
  TDialogCopyEmpDataF = class(TDialogSelectionF)
    gboxEmployee: TGroupBox;
    gboxPeriod: TGroupBox;
    Label1: TLabel;
    Label6: TLabel;
    dxSpinEditYearFrom: TdxSpinEdit;
    dxSpinEditWeekFrom: TdxSpinEdit;
    Label4: TLabel;
    dxSpinEditWeekTo: TdxSpinEdit;
    qryEmployee: TQuery;
    dsEmployee: TDataSource;
    qryEmployeePLANT_CODE: TStringField;
    qryEmployeeEMPLOYEE_NUMBER: TIntegerField;
    qryEmployeeVALUEDISPLAY: TStringField;
    qryEmployeeDESCRIPTION: TStringField;
    qryEmployeeSHORT_NAME: TStringField;
    qryEmployeeDEPARTMENT_CODE: TStringField;
    qryEmployeeTEAM_CODE: TStringField;
    qryEmployeeADDRESS: TStringField;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployee: TdxDBGridLayout;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    gboxMessages: TGroupBox;
    mmMessages: TMemo;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure dxDBExtLookupEditEmployeeCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FEmployeeNumber: Integer;
    FOtherEmployeeNumber: Integer;
    FDateFrom: TDateTime;
    FDateTo: TDateTime;
    FPlantCode: String;
    FStartDate: TDateTime;
    FYear: Integer;
    FWeekTo: Integer;
    FWeekFrom: Integer;
  public
    { Public declarations }
    property EmployeeNumber: Integer read FEmployeeNumber write FEmployeeNumber;
    property PlantCode: String read FPlantCode write FPlantCode;
    property OtherEmployeeNumber: Integer read FOtherEmployeeNumber write FOtherEmployeeNumber;
    property DateFrom: TDateTime read FDateFrom write FDateFrom;
    property DateTo: TDateTime read FDateTo write FDateTo;
    property StartDate: TDateTime read FStartDate write FStartDate;
    property Year: Integer read FYear write FYear;
    property WeekFrom: Integer read FWeekFrom write FWeekFrom;
    property WeekTo: Integer read FWeekTo write FWeekTo;
  end;

var
  DialogCopyEmpDataF: TDialogCopyEmpDataF;

implementation

uses
  ListProcsFRM, DialogCopyEmpDataDMT, UPimsMessageRes;

{$R *.DFM}

procedure TDialogCopyEmpDataF.FormCreate(Sender: TObject);
var
  Year, Month, Day: Word;
begin
  inherited;
  DialogCopyEmpDataDM := TDialogCopyEmpDataDM.Create(Self);
  DialogCopyEmpDataDM.Memo := mmMessages;
  DecodeDate(Date, Year, Month, Day);
  dxSpinEditYearFrom.Value := Year;
  dxSpinEditWeekFrom.Value := ListProcsF.WeekOfYear(Date);
  dxSpinEditWeekTo.Value := ListProcsF.WeekOfYear(Date);
end;

procedure TDialogCopyEmpDataF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearFrom.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;

  try
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYearFrom.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
end;

procedure TDialogCopyEmpDataF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

procedure TDialogCopyEmpDataF.dxDBExtLookupEditEmployeeCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  if Self.dxDBExtLookupEditEmployee.Text <> '' then
  begin
    try
      OtherEmployeeNumber := qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    except
      OtherEmployeeNumber := -1;
    end;
  end;
end;

procedure TDialogCopyEmpDataF.btnOkClick(Sender: TObject);
begin
  inherited;
  DateFrom := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekFrom.Value), 1);
  DateTo := ListProcsF.DateFromWeek(Round(dxSpinEditYearFrom.Value),
    Round(dxSpinEditWeekTo.Value), 7);
  Year := Round(dxSpinEditYearFrom.Value);
  WeekFrom := Round(dxSpinEditWeekFrom.Value);
  WeekTo := Round(dxSpinEditWeekTo.Value);
  DialogCopyEmpDataDM.Year := Year;
  DialogCopyEmpDataDM.WeekFrom := WeekFrom;
  DialogCopyEmpDataDM.WeekTo := WeekTo;
  if DialogCopyEmpDataDM.CopyEmpDataAction(EmployeeNumber, StartDate,
    OtherEmployeeNumber, DateFrom, DateTo) then
  begin
    DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
    ModalResult := mrOK;
  end
  else
    ModalResult := mrNone;
end;

procedure TDialogCopyEmpDataF.FormShow(Sender: TObject);
begin
  inherited;
  qryEmployee.Close;
  qryEmployee.ParamByName('PLANT_CODE').AsString := PlantCode;
  qryEmployee.ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
  qryEmployee.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  qryEmployee.Open;
  if not qryEmployee.Eof then
  begin
    // Assign first Employee here as default!
    OtherEmployeeNumber :=
      qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  end
  else
    btnCancelClick(Sender);
end;

procedure TDialogCopyEmpDataF.btnCancelClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TDialogCopyEmpDataF.FormDestroy(Sender: TObject);
begin
  inherited;
  DialogCopyEmpDataDM.Free;
end;

end.
