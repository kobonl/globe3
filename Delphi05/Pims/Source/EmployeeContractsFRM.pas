(*
  SO:07-JUN-2010 RV065.6.
  - changed the holiday hours per year edit box to be possible to enter the hours
  and the minutes in separate spin edit boxes
  - changed the hours per week edit box to be possible to enter the hours
  and the minutes in separate spin edit boxes
  SO:19-JUL-2010 RV067.4. 550497
  - added new fields SENIORITY_HOURS, WORKER_YN for Belgium
  MRA:27-AUG-2010. RV067.MRA.13. Bugfix.
  - Hours:Minutes values shown in details-panel are shown different
    compared with grid. This must be shown both as Hours:Mins.
  - Made the hour-fields in detail-panel somewhat wider.
  MRA:31-AUG-2010. RV067.MRA.21 Changes linked to order 550497.
  - Two fields in grid changed to english instead of dutch:
    - Seniority hours + Worker.
  MRA:1-OCT-2010 RV071.2.
  - Test for employee's plant belonging to country 'BEL' is not always OK.
  - Performance-issue. ClientDataSetMaster is not used anymore!
  - When entering 99 minutes it must be prevented or converted to 59 when
    saving.
  - REMARK: When exporting, the values for e.g. 'hol. hrs. per week', will
            be exported as it is in database, not as it is shown in
            the dialog. These values are stored in a different format!
            Reason: Some reports expect these values as stored in database.
            Example: Stored is: 2,25. Shown is 2:15 hrs.
  MRA:27-APR-2011 RV091.3. Bugfix.
  - In the grid there was a Clear-option for the Start- en End-Date.
    This is disabled, because these fields cannot be empty.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDBPicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
*)
unit EmployeeContractsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, dxEdLib, dxDBELib, dxEditor,
  dxExEdtr, dxDBTLCl, dxGrClms, ImgList, ComCtrls, DBPicker, SystemDMT,
  dxDBEdtr;

type
  TEmployeeContractsF = class(TGridBaseF)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dxDBMaskEdit1: TdxDBMaskEdit;
    Label4: TLabel;
    dxDBEditDays: TdxDBEdit;
    Label5: TLabel;
    DBRadioGroupContractType: TDBRadioGroup;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnPlant: TdxDBGridLookupColumn;
    dxMasterGridColumnDepartment: TdxDBGridLookupColumn;
    dxMasterGridColumnStartDate: TdxDBGridDateColumn;
    dxDetailGridColumnStartDate: TdxDBGridDateColumn;
    dxDetailGridColumnEndDate: TdxDBGridDateColumn;
    dxDetailGridColumnHourlyWage: TdxDBGridCurrencyColumn;
    dxDetailGridColumnContractType: TdxDBGridSpinColumn;
    dxDetailGridColumnHoursWeek: TdxDBGridSpinColumn;
    dxDetailGridColumnDaysWeek: TdxDBGridSpinColumn;
    dxDetailGridColumnHoliday: TdxDBGridSpinColumn;
    DBPickerStartDate: TDBPicker;
    DBPickerEndDate: TDBPicker;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    dxDBEditGuaranteedDays: TdxDBEdit;
    lblExportCode: TLabel;
    dxDBEditExportCode: TdxDBEdit;
    dxDetailGridColumnGuaranteedDays: TdxDBGridColumn;
    dxDetailGridColumnExportCode: TdxDBGridColumn;
    Label7: TLabel;
    dxSpinEditHHolidayHours: TdxSpinEdit;
    dxSpinEditMHolidayHours: TdxSpinEdit;
    dxSpinEditHHoursPerWeek: TdxSpinEdit;
    dxSpinEditMHoursPerWeek: TdxSpinEdit;
    LabelSeniority: TLabel;
    dxSpinEditHSeniority: TdxSpinEdit;
    dxSpinEditMSeniority: TdxSpinEdit;
    DBRadioGroupWorker: TDBRadioGroup;
    dxDetailGridColumnSeniority: TdxDBGridColumn;
    dxDetailGridColumnWorker: TdxDBGridCheckColumn;
    Label8: TLabel;
    dxDBLookupEdit1: TdxDBLookupEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxDetailGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dxSpinEditHDaysPerWeekKeyPress(Sender: TObject; var Key: Char);
    procedure dxSpinEditHDaysPerWeekMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxSpinEditMinuteChange(Sender: TObject);
  private
    { Private declarations }
    procedure UpdateExportCodeField(OK: Boolean);
    procedure EnableSave(Status: Boolean);
    procedure SetTimeFields;

    procedure EnableFieldsForBelgium;
    function CheckMinutes(AMins: TdxSpinEdit): Double;
  public
    { Public declarations }
  end;

  function EmployeeContractsF: TEmployeeContractsF;

implementation

uses EmployeeContractsDMT, UPimsMessageRes,
  UPimsConst;
{$R *.DFM}

var
  EmployeeContractsF_HND: TEmployeeContractsF;

function EmployeeContractsF: TEmployeeContractsF;
begin
  if (EmployeeContractsF_HND = nil) then
  begin
    EmployeeContractsF_HND := TEmployeeContractsF.Create(Application);
  end;
  Result := EmployeeContractsF_HND;
end;

procedure TEmployeeContractsF.FormDestroy(Sender: TObject);
begin
  inherited;
  EmployeeContractsF_HND := Nil;
end;

procedure TEmployeeContractsF.FormCreate(Sender: TObject);
begin
  EmployeeContractsDM := CreateFormDM(TEmployeeContractsDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := EmployeeContractsDM.DataSourceDetail;
    dxMasterGrid.DataSource := EmployeeContractsDM.DataSourceMaster;
  end;
  inherited;
end;

procedure TEmployeeContractsF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBPickerStartDate.SetFocus;
end;

procedure TEmployeeContractsF.dxDetailGridMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  G: TdxDBGrid;
  CurCol: TdxDBTreeListColumn;
  HitTest: TdxTreeListHitTest;
begin
  inherited;
  G := Sender as TdxDBGrid;
  if G.Count > 0 then
  try
    HitTest := G.GetHitTestInfoAt(X, Y);
    if HitTest = htLabel then
    begin
      CurCol := G.GetColumnAt(X, Y);
      if (CurCol <> nil) and
        (CurCol.Name = 'dxDetailGridColumnContractType') then
      begin
        G.Hint := '0 - '+SPIMSContractTypePermanent+#13+
          '1 - '+SPIMSContractTypeTemporary+#13+
          '2 - '+SPIMSContractTypePartTime+#13;
        G.ShowHint := True
      end
      else
        G.ShowHint := False;
    end;
  except
    // do nothing
  end;
end;

procedure TEmployeeContractsF.dxBarButtonExportGridClick(Sender: TObject);
begin
  // RV071.2. ClientDataSetMaster is not used anymore!
  
  // RV071.2. Open here, only if needed.
(*  if not EmployeeContractsDM.ClientDataSetMaster.Active then
  begin
    try
      EmployeeContractsDM.ClientDataSetMaster.Open;
      EmployeeContractsDM.ClientDataSetMaster.LogChanges := False;
    except
    end;
  end; *)

  FloatEmpl := 'EMPLOYEE_NUMBER';
  //car 550279 - 18-12-2003- set here for speed reasons
  // RV071.2. Is done in DataModule.
//  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
//    EmployeeContractsDM.TableExport.Filtered := True;

  inherited;
end;

procedure TEmployeeContractsF.FormShow(Sender: TObject);
begin
  inherited;
  //
  EmployeeContractsDM.QueryMaster.Open;
 //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      EmployeeContractsDM.QueryMaster.
        FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
    EmployeeContractsDM.QueryMaster.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee, []);
  //
  // MR:29-07-2004
  UpdateExportCodeField(True);
  //RV065.6.
  SetTimeFields;
  //RV067.4.
  EnableFieldsForBelgium;
end;

procedure TEmployeeContractsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
 //CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    EmployeeContractsDM.QueryMaster.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

// MR:26-07-2004 Order 550331
// 2 extra fields added for export-payroll
procedure TEmployeeContractsF.UpdateExportCodeField(OK: Boolean);
begin
  lblExportCode.Enabled := OK;
  dxDBEditExportCode.Enabled := OK;
end;

// MR:26-07-2004 Order 550331
// 2 extra fields added for export-payroll
procedure TEmployeeContractsF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
//var
//  OK: Boolean;
begin
  inherited;
  // RV067.MRA.13. Bugfix.
  if ASelected or AFocused then
  begin
    // RV071.2.
    if AColumn = dxMasterGridColumnCode then
      EnableFieldsForBelgium;

    if AColumn = dxDetailGridColumnHoliday then
      AText := EmployeeContractsDM.ConvertTime(
        EmployeeContractsDM.TableDetail, 'HOLIDAY_HOUR_PER_YEAR');
    if AColumn = dxDetailGridColumnHoursWeek then
      AText := EmployeeContractsDM.ConvertTime(
        EmployeeContractsDM.TableDetail, 'CONTRACT_HOUR_PER_WEEK');
    if EmployeeContractsDM.IsBelgium then
      if AColumn = dxDetailGridColumnSeniority then
        AText := EmployeeContractsDM.ConvertTime(
          EmployeeContractsDM.TableDetail, 'SENIORITY_HOURS');
    SetTimeFields;
  end;


//  if AFocused or ASelected then
//  begin
//    if AColumn = dxDetailGridColumnGuaranteedDays then
//    begin
//      OK := False;
//      if dxDetailGridColumnGuaranteedDays.Field.AsString <> '' then
//        OK := dxDetailGridColumnGuaranteedDays.Field.AsInteger > 0;
//      UpdateExportCodeField(OK);
//    end;
//  end;
end;

procedure TEmployeeContractsF.EnableSave(Status: Boolean);
begin
  dxBarBDBNavPost.Enabled:= Status;
  dxBarBDBNavCancel.Enabled:= Status;
end;

//<-- BEGIN RV065.6.
procedure TEmployeeContractsF.dxSpinEditHDaysPerWeekKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  EnableSave(True);
end;

procedure TEmployeeContractsF.dxSpinEditHDaysPerWeekMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  EnableSave(True);
end;

procedure TEmployeeContractsF.dxDetailGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
end;

procedure TEmployeeContractsF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  SetTimeFields;
end;

procedure TEmployeeContractsF.SetTimeFields;
begin
  with EmployeeContractsDM do
  begin
    dxSpinEditHHolidayHours.Value :=
      Trunc(TableDetail.FieldByName('HOLIDAY_HOUR_PER_YEAR').AsFloat);
    dxSpinEditMHolidayHours.Value :=
      (TableDetail.FieldByName('HOLIDAY_HOUR_PER_YEAR').AsFloat - dxSpinEditHHolidayHours.Value) * 60;

    dxSpinEditHHoursPerWeek.Value :=
      Trunc(TableDetail.FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat);
    dxSpinEditMHoursPerWeek.Value :=
      (TableDetail.FieldByName('CONTRACT_HOUR_PER_WEEK').AsFloat - dxSpinEditHHoursPerWeek.Value) * 60;

    //RV067.4.
    if IsBelgium then
    begin
      dxSpinEditHSeniority.Value :=
        Trunc(TableDetail.FieldByName('SENIORITY_HOURS').AsFloat);
      // RV067.MRA.13. Bugfix. Wrong var. was used.
      dxSpinEditMSeniority.Value :=
        (TableDetail.FieldByName('SENIORITY_HOURS').AsFloat - dxSpinEditHSeniority.Value) * 60;
//        (TableDetail.FieldByName('SENIORITY_HOURS').AsFloat - dxSpinEditHHoursPerWeek.Value) * 60;
    end;
  end;
end;

// RV071.2. Check on minutes!
function TEmployeeContractsF.CheckMinutes(AMins: TdxSpinEdit): Double;
begin
  if AMins.Value > 59 then
    AMins.Value := 59;
  Result := AMins.Value;
end;

procedure TEmployeeContractsF.dxBarBDBNavPostClick(Sender: TObject);
  function ReadTime(AHours, AMins: TdxSpinEdit): Double;
  begin
    Result := AHours.Value + AMins.Value / 60;
  end;
begin
  inherited;
  EmployeeContractsDM.TableDetail.Edit;

  // RV071.2. Check on minutes!
  dxSpinEditMHolidayHours.Value := CheckMinutes(dxSpinEditMHolidayHours);
  EmployeeContractsDM.HolidayHours := ReadTime(dxSpinEditHHolidayHours, dxSpinEditMHolidayHours);
  dxSpinEditMHoursPerWeek.Value := CheckMinutes(dxSpinEditMHoursPerWeek);
  EmployeeContractsDM.HoursPerWeek := ReadTime(dxSpinEditHHoursPerWeek, dxSpinEditMHoursPerWeek);
  //RV067.4.
  if EmployeeContractsDM.IsBelgium then
  begin
    dxSpinEditMSeniority.Value := CheckMinutes(dxSpinEditMSeniority);
    EmployeeContractsDM.SeniorityHours := ReadTime(dxSpinEditHSeniority, dxSpinEditMSeniority);
  end;

  EmployeeContractsDM.TableDetail.Post;
end;

procedure TEmployeeContractsF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  SetTimeFields;
  //RV067.4.
  EnableFieldsForBelgium;
end;
//--> END RV065.6.

//RV067.4.
procedure TEmployeeContractsF.EnableFieldsForBelgium;
var
  Enable: Boolean;
begin
  Enable := EmployeeContractsDM.IsBelgium;

  LabelSeniority.Visible := Enable;
  dxSpinEditHSeniority.Visible := Enable;
  dxSpinEditMSeniority.Visible := Enable;
  DBRadioGroupWorker.Visible := Enable;
  dxDetailGridColumnSeniority.Visible := Enable;
  dxDetailGridColumnWorker.Visible := Enable;
end;

procedure TEmployeeContractsF.dxSpinEditMinuteChange(
  Sender: TObject);
begin
  inherited;
  if (Sender is TdxSpinEdit) then
    (Sender as TdxSpinEdit).Value := CheckMinutes(Sender as TdxSpinEdit);
end;

end.
