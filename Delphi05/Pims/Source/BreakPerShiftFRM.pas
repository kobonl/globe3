unit BreakPerShiftFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, CalculateTotalHoursDMT;

type
  TBreakPerShiftF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridTimeColumn;
    dxMasterGridColumn4: TdxDBGridTimeColumn;
    dxMasterGridColumn5: TdxDBGridTimeColumn;
    dxMasterGridColumn6: TdxDBGridTimeColumn;
    dxMasterGridColumn7: TdxDBGridTimeColumn;
    dxMasterGridColumn8: TdxDBGridTimeColumn;
    dxMasterGridColumn9: TdxDBGridTimeColumn;
    dxMasterGridColumn10: TdxDBGridTimeColumn;
    dxMasterGridColumn11: TdxDBGridTimeColumn;
    dxMasterGridColumn12: TdxDBGridTimeColumn;
    dxMasterGridColumn13: TdxDBGridTimeColumn;
    dxMasterGridColumn14: TdxDBGridTimeColumn;
    dxMasterGridColumn15: TdxDBGridTimeColumn;
    dxMasterGridColumn16: TdxDBGridTimeColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    dxMasterGridColumn17: TdxDBGridColumn;
    dxMasterGridColumn18: TdxDBGridLookupColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBCheckBoxPayedYN: TDBCheckBox;
    DBEditBreaksDesc: TdxDBEdit;
    DBEditPlant: TDBEdit;
    DBEditUnitName: TDBEdit;
    DBEditShift: TDBEdit;
    DBEditShiftName: TDBEdit;
    DBEditBreaksPerShift: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    dxDBTimeEditST1: TdxDBTimeEdit;
    dxDBTimeEditET1: TdxDBTimeEdit;
    dxDBTimeEditST2: TdxDBTimeEdit;
    dxDBTimeEditET2: TdxDBTimeEdit;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
 
  end;

function BreakPerShiftF: TBreakPerShiftF;

implementation

{$R *.DFM}
uses
  SystemDMT, BreakPerShiftDMT;

var
  BreakPerShiftF_HND: TBreakPerShiftF;

function BreakPerShiftF: TBreakPerShiftF;
begin
  if (BreakPerShiftF_HND = nil) then
  begin
    BreakPerShiftF_HND := TBreakPerShiftF.Create(Application);
    BreakPerShiftDM.Handle_Grid := BreakPerShiftF_HND.dxDetailGrid;
  end;
  Result := BreakPerShiftF_HND;
end;

procedure TBreakPerShiftF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditBreaksDesc.SetFocus;
end;

procedure TBreakPerShiftF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditBreaksDesc.SetFocus;
end;

procedure TBreakPerShiftF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  for Index := 1 to 7 do
  begin
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
    dxMasterGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
  end;
  dxMasterGrid.SetFocus;
end;

procedure TBreakPerShiftF.FormDestroy(Sender: TObject);
begin
  inherited;
  BreakPerShiftF_HND := nil;
end;

procedure TBreakPerShiftF.FormCreate(Sender: TObject);
begin
  BreakPerShiftDM := CreateFormDM(TBreakPerShiftDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := BreakPerShiftDM.DataSourceDetail;
    dxMasterGrid.DataSource := BreakPerShiftDM.DataSourceMaster;
  end;
  inherited;
end;

procedure TBreakPerShiftF.dxBarButtonExportGridClick(Sender: TObject);
begin
  CreateExportColumns(dxMasterGrid, ' ', 'PLANT_CODE');
  CreateExportColumns(dxMasterGrid,   ' ', 'PLANTLU');
  inherited;

end;

procedure TBreakPerShiftF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
end;

end.
