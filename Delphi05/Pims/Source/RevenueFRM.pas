(*
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDBPicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)
unit RevenueFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Dblup1a, Mask, DBCtrls, dxEditor, dxExEdtr,
  dxEdLib, ComCtrls, dxDBTLCl, dxGrClms, dxDBELib, DBPicker, SystemDMT;

type
  TRevenueF = class(TGridBaseF)
    pnlTeamPlantShift: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cmbPlusBU: TComboBoxPlus;
    dxDetailGridColumnRevenue: TdxDBGridColumn;
    Label4: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    Label9: TLabel;
    dxSpinEditWeek: TdxSpinEdit;
    dxDetailGridColumnDate: TdxDBGridDateColumn;
    AmountRevenue: TdxDBEdit;
    DateRevenue: TDBPicker;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cmbPlusBUChange(Sender: TObject);
    procedure FormHide(Sender: TObject);

    procedure dxSpinEditYearChange(Sender: TObject);
    procedure dxSpinEditWeekChange(Sender: TObject);

    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
  private
    { Private declarations }
    procedure FillBU;
    procedure CorrectWeekComponent;
  public
    { Public declarations }
    procedure EnabledNavButtons(Active: Boolean);
    procedure RefreshGrid;
  end;

function RevenueF: TRevenueF;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UPimsConst, UPimsMessageRes, RevenueDMT;

var
  RevenueF_HDN: TRevenueF;

function RevenueF: TRevenueF;
begin
  if RevenueF_HDN = nil then
    RevenueF_HDN := TRevenueF.Create(Application);
  Result := RevenueF_HDN;
end;

procedure TRevenueF.FormCreate(Sender: TObject);
begin
  RevenueDM := CreateFormDM(TRevenueDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := RevenueDM.DataSourceDetail;
  inherited;
  ActiveGrid := dxDetailGrid;
end;

procedure TRevenueF.FormDestroy(Sender: TObject);
begin
  inherited;
  RevenueF_HDN := nil;
end;

procedure TRevenueF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SUndoChanges, mtInformation, [mbOk]);
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end;
  Action := caFree;
end;

procedure TRevenueF.FillBU;
begin
  ListProcsF.FillComboBoxMaster(RevenueDM.TableBU, 'BUSINESSUNIT_CODE',
    True, cmbPlusBU);
  RefreshGrid;
end;

procedure TRevenueF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;
  FillBU;
  ListProcsF.WeekUitDat(Now, Year, Week);
  dxSpinEditYear.Value := Year;
  dxSpinEditWeek.Value := Week;
  EnabledNavButtons(False);
end;

procedure TRevenueF.cmbPlusBUChange(Sender: TObject);
begin
  inherited;
  RefreshGrid;
end;

procedure TRevenueF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
  dxBarBDBNavInsert.Enabled := Not Active;
  dxBarBDBNavDelete.Enabled := Not Active;
end;

procedure TRevenueF.FormHide(Sender: TObject);
begin
   EnabledNavButtons(False);
   inherited;
end;

procedure TRevenueF.RefreshGrid;
begin
  if dxBarBDBNavPost.Enabled then
  begin
    EnabledNavButtons(False);
    RevenueDM.TableDetail.Cancel;
  end;
  if RevenueDM <> Nil then
  begin
    RevenueDM.SetValues(GetStrValue(cmbPlusBU.Value), dxSpinEditYear.IntValue,
      dxSpinEditWeek.IntValue);
    RevenueDM.TableDetail.Refresh;
  end;
end;

procedure TRevenueF.dxSpinEditYearChange(Sender: TObject);
begin
  inherited;
  CorrectWeekComponent;
  RefreshGrid;
end;

procedure TRevenueF.dxSpinEditWeekChange(Sender: TObject);
begin
  inherited;
  CorrectWeekComponent;
  RefreshGrid;
end;

procedure TRevenueF.dxBarBDBNavInsertClick(Sender: TObject);
var
  DateMin: TDateTime;
begin
  EnabledNavButtons(True);
  DateRevenue.SetFocus;
  DateMin := ListProcsF.DateFromWeek(dxSpinEditYear.IntValue,
    dxSpinEditWeek.IntValue, 1);
  DateRevenue.Date := GetDate(DateMin);
  RevenueDM.FInsertMode := True;
  RevenueDM.TableDetail.Cancel;
  RevenueDM.TableDetail.Refresh;
  dxDetailGrid.DataSource.DataSet.Append;
  AmountRevenue.Text := '';
end;

procedure TRevenueF.dxBarBDBNavPostClick(Sender: TObject);
begin
  if RevenueDM.SaveRecord then
  begin
    RevenueDM.FInsertMode := False;
    EnabledNavButtons(False);
    dxDetailGrid.DataSource.DataSet.Cancel;
    RefreshGrid;
  end
  else
    EnabledNavButtons(True);
end;

procedure TRevenueF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  EnabledNavButtons(False);
  RevenueDM.FInsertMode := False;
  dxDetailGrid.DataSource.DataSet.Cancel;
end;

procedure TRevenueF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  if RevenueDM.DeleteRecord then
    RefreshGrid;
end;

// MR:04-12-2003 Set max of weeks and correct the component that handles
//               the week-selection
procedure TRevenueF.CorrectWeekComponent;
begin
  try
    // Set max of weeks
    dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    // Correct the week-selection-component
    if dxSpinEditWeek.Value > dxSpinEditWeek.MaxValue then
      dxSpinEditWeek.Value := dxSpinEditWeek.MaxValue;
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

end.
