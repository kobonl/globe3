inherited AbsReasonPerCountryDM: TAbsReasonPerCountryDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    TableName = 'COUNTRY'
    object TableMasterCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableMasterEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Size = 6
    end
    object TableMasterCODE: TStringField
      FieldName = 'CODE'
      Required = True
      Size = 3
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
  end
  inherited TableDetail: TTable
    IndexName = 'XPKABSENCEREASONPERCOUNTRY'
    MasterFields = 'COUNTRY_ID'
    TableName = 'ABSENCEREASONPERCOUNTRY'
    Top = 147
    object TableDetailCOUNTRY_ID: TIntegerField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableDetailABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
      Required = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailabsencetype: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'absencetype'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'absencetype'
      KeyFields = 'ABSENCEREASON_ID'
      Size = 30
      Lookup = True
    end
    object TableDetailabsencereason_code: TStringField
      FieldKind = fkLookup
      FieldName = 'absencereason_code'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'ABSENCEREASON_CODE'
      KeyFields = 'ABSENCEREASON_ID'
      Lookup = True
    end
    object TableDetaildescription: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'description'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCEREASON_ID'
      Size = 30
      Lookup = True
    end
    object TableDetailpayed_yn: TStringField
      FieldKind = fkLookup
      FieldName = 'payed_yn'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'PAYED_YN'
      KeyFields = 'ABSENCEREASON_ID'
      Lookup = True
    end
    object TableDetailoverrule_with_illness_yn: TStringField
      FieldKind = fkLookup
      FieldName = 'overrule_with_illness_yn'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'OVERRULE_WITH_ILLNESS_YN'
      KeyFields = 'ABSENCEREASON_ID'
      Lookup = True
    end
    object TableDetailhourtype: TStringField
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'hourtype'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'hourtype'
      KeyFields = 'ABSENCEREASON_ID'
      Size = 30
      Lookup = True
    end
    object TableDetailexport_code: TStringField
      FieldKind = fkLookup
      FieldName = 'export_code'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_ID'
      LookupResultField = 'EXPORT_CODE'
      KeyFields = 'ABSENCEREASON_ID'
      Size = 6
      Lookup = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 140
  end
  object TableAbsenceType: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'ABSENCETYPE'
    Left = 92
    Top = 269
    object TableAbsenceTypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableAbsenceTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableAbsenceTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableAbsenceTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  object TableHourType: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 92
    Top = 328
    object TableHourTypeHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableHourTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableHourTypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableHourTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableHourTypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableHourTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableHourTypeBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
  object DataSourceAbsenceType: TDataSource
    DataSet = TableAbsenceType
    Left = 208
    Top = 272
  end
  object DataSourceHourType: TDataSource
    DataSet = TableHourType
    Left = 208
    Top = 324
  end
  object TableAbsenceReason: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'ABSENCEREASON'
    Left = 316
    Top = 141
    object TableAbsenceReasonABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
      Required = True
    end
    object TableAbsenceReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceReasonCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
      Required = True
    end
    object TableAbsenceReasonMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableAbsenceReasonMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableAbsenceReasonPAYED_YN: TStringField
      FieldName = 'PAYED_YN'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonOVERRULE_WITH_ILLNESS_YN: TStringField
      FieldName = 'OVERRULE_WITH_ILLNESS_YN'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonabsencetype: TStringField
      FieldKind = fkLookup
      FieldName = 'absencetype'
      LookupDataSet = TableAbsenceType
      LookupKeyFields = 'ABSENCETYPE_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCETYPE_CODE'
      Lookup = True
    end
    object TableAbsenceReasonhourtype: TStringField
      FieldKind = fkLookup
      FieldName = 'hourtype'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      Lookup = True
    end
    object TableAbsenceReasonEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 24
    end
  end
end
