inherited ReportCheckListScanDM: TReportCheckListScanDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryTimeRegScan: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryTimeRegScanFilterRecord
    SQL.Strings = (
      'SELECT'
      '  T.EMPLOYEE_NUMBER, E.DESCRIPTION, T.DATETIME_IN,'
      '  W.PLANT_CODE AS WPLANT_CODE,'
      '  E.PLANT_CODE AS EPLANT_CODE,'
      '  E.DEPARTMENT_CODE AS EDEPARTMENT_CODE,'
      '  T.WORKSPOT_CODE, W.DESCRIPTION AS WDESCRIPTION,'
      '  T.DATETIME_OUT, T.PROCESSED_YN,'
      '  T.IDCARD_IN, T.IDCARD_OUT, T.SHIFT_NUMBER, T.JOB_CODE,'
      '  J.DESCRIPTION AS JOBDESC,'
      '  T.PLANT_CODE , P.DESCRIPTION AS PDESCRIPTION,'
      '  W.DEPARTMENT_CODE, E.CUT_OF_TIME_YN , P.INSCAN_MARGIN_EARLY,'
      '  P.OUTSCAN_MARGIN_LATE, P.INSCAN_MARGIN_LATE,'
      '  P.OUTSCAN_MARGIN_EARLY,'
      '  T.SHIFT_DATE'
      'FROM'
      '  TIMEREGSCANNING T LEFT JOIN  EMPLOYEE E ON'
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  LEFT JOIN PLANT P ON'
      '    T.PLANT_CODE = P.PLANT_CODE'
      '  LEFT JOIN WORKSPOT W ON'
      '    T.PLANT_CODE = W.PLANT_CODE AND'
      '    T.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  LEFT JOIN JOBCODE J ON'
      '    T.PLANT_CODE = J.PLANT_CODE AND'
      '    T.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '    T.JOB_CODE = J.JOB_CODE'
      ' '
      ' '
      ' ')
    Left = 64
    Top = 40
  end
  object QueryRequest: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER '
      'FROM '
      '  REQUESTEARLYLATE  '
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 64
    Top = 168
  end
  object ClientDataSetEmpReq: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpRequest'
    Left = 64
    Top = 248
  end
  object DataSetProviderEmpRequest: TDataSetProvider
    DataSet = QueryRequest
    Constraints = True
    Left = 200
    Top = 248
  end
  object QueryEmployeeAvailability: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER,'
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2,'
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4,'
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6,'
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8,'
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY'
      'WHERE'
      '  EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  EMPLOYEEAVAILABILITY_DATE = :DATEFROM'
      'ORDER BY'
      '  EMPLOYEE_NUMBER'
      ''
      ' ')
    Left = 64
    Top = 104
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end>
    object QueryEmployeeAvailabilityEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryEmployeeAvailabilitySHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_5'
      Size = 4
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_6'
      Size = 4
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_7'
      Size = 4
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_8'
      Size = 4
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_9'
      Size = 4
    end
    object QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Origin = 'PIMS.EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_10'
      Size = 4
    end
  end
end
