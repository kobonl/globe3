inherited ReportAbsenceCardDM: TReportAbsenceCardDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited qryWorkspotMinMax: TQuery
    Top = 128
  end
  object QueryAbsence: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    Left = 56
    Top = 24
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryAbsence
    Left = 184
    Top = 24
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceAbsence
    TableName = 'PLANT'
    Left = 56
    Top = 192
  end
  object TableAbs: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'ABSENCEREASON'
    Left = 56
    Top = 88
  end
  object TableEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    MasterFields = 'EMPLOYEE_NUMBER'
    MasterSource = DataSourceAbsence
    TableName = 'EMPLOYEE'
    Left = 56
    Top = 144
  end
  object QueryAbsencePlants: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A'
      'WHERE'
      '  A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  A.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  A.ABSENCEHOUR_DATE >= :FDATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :FDATETO'
      'GROUP BY  '
      '  A.PLANT_CODE'
      'ORDER BY'
      '  1'
      ''
      ' ')
    Left = 56
    Top = 256
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReasons: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  B.ABSENCEREASON_CODE, B.DESCRIPTION, COUNT(*) DAYS'
      'FROM'
      '('
      'SELECT'
      '  A.ABSENCEREASON_CODE, A.DESCRIPTION, COUNT(*)'
      'FROM'
      '('
      'SELECT'
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION, A.ABSENCEHOUR_DATE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON'
      '    R.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      'WHERE'
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  A.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :DATETO'
      'GROUP BY R.ABSENCEREASON_CODE, R.DESCRIPTION, A.ABSENCEHOUR_DATE'
      'UNION ALL'
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_1, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK' +
        '_1) DESCRIPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ('
      
        '    (EA.AVAILABLE_TIMEBLOCK_1 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND'
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO'
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN'
      '  (SELECT AHE.ABSENCEHOUR_DATE'
      '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON'
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_2, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK' +
        '_2) DESCRIPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_2 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_3, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK' +
        '_3) DESCRIPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_3 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_4, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK' +
        '_4) DESCRIPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_4 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_5, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_5) DESCR' +
        'IPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_5 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_6, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_6) DESCR' +
        'IPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_6 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_7, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_7) DESCR' +
        'IPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_7 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_8, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_8) DESCR' +
        'IPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_8 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_9, (SELECT AR2.DESCRIPTION FROM ABSENCE' +
        'REASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_9) DESCR' +
        'IPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_9 IN (SELECT AR2.ABSENCEREASON_CODE ' +
        'FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      'UNION ALL '
      'SELECT'
      
        '  EA.AVAILABLE_TIMEBLOCK_10, (SELECT AR2.DESCRIPTION FROM ABSENC' +
        'EREASON AR2 '
      
        '  WHERE AR2.ABSENCEREASON_CODE = EA.AVAILABLE_TIMEBLOCK_10) DESC' +
        'RIPTION, EA.EMPLOYEEAVAILABILITY_DATE'
      'FROM EMPLOYEEAVAILABILITY EA'
      'WHERE'
      '  ( '
      
        '    (EA.AVAILABLE_TIMEBLOCK_10 IN (SELECT AR2.ABSENCEREASON_CODE' +
        ' FROM ABSENCEREASON AR2))'
      '  ) AND '
      '  EA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND '
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO '
      '  AND EA.EMPLOYEEAVAILABILITY_DATE NOT IN '
      '  (SELECT AHE.ABSENCEHOUR_DATE '
      
        '  FROM ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON' +
        ' '
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID '
      '  WHERE AHE.EMPLOYEE_NUMBER = EA.EMPLOYEE_NUMBER AND '
      '    AHE.ABSENCEHOUR_DATE = EA.EMPLOYEEAVAILABILITY_DATE) '
      '    '
      ') A'
      'GROUP BY'
      '  A.ABSENCEREASON_CODE, A.DESCRIPTION, A.ABSENCEHOUR_DATE'
      ') B'
      'GROUP BY'
      '  B.ABSENCEREASON_CODE, B.DESCRIPTION'
      'ORDER BY'
      '  1'
      ''
      ' '
      ' '
      '')
    Left = 56
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeeAvailability: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE,'
      '  '#39'-'#39' AS DEPARTMENT_CODE,'
      '  T.EMPLOYEEAVAILABILITY_DATE,'
      '  T.SHIFT_NUMBER,'
      '  T.EMPLOYEE_NUMBER,'
      '  T.AVAILABLE_TIMEBLOCK_1, T.AVAILABLE_TIMEBLOCK_2,'
      '  T.AVAILABLE_TIMEBLOCK_3, T.AVAILABLE_TIMEBLOCK_4,'
      '  T.AVAILABLE_TIMEBLOCK_5, T.AVAILABLE_TIMEBLOCK_6,'
      '  T.AVAILABLE_TIMEBLOCK_7, T.AVAILABLE_TIMEBLOCK_8,'
      '  T.AVAILABLE_TIMEBLOCK_9, T.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY T'
      'WHERE '
      '  T.EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE AND'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' '
      ' '
      ' ')
    Left = 320
    Top = 248
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryWork: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    Left = 320
    Top = 304
  end
end
