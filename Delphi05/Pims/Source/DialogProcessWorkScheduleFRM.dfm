inherited DialogProcessWorkScheduleF: TDialogProcessWorkScheduleF
  Caption = 'Process Work Schedules'
  Position = poOwnerFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 1
  end
  inherited pnlInsertBase: TPanel
    Font.Color = clBlack
    Font.Height = -11
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 637
      Height = 153
      Align = alTop
      Caption = 'Process Work Schedules'
      TabOrder = 0
      object lblYear: TLabel
        Left = 8
        Top = 24
        Width = 22
        Height = 13
        Caption = 'Year'
      end
      object Label2: TLabel
        Left = 112
        Top = 24
        Width = 31
        Height = 13
        Caption = 'Label2'
      end
      object lblFrom: TLabel
        Left = 8
        Top = 48
        Width = 24
        Height = 13
        Caption = 'From'
      end
      object Label1: TLabel
        Left = 56
        Top = 48
        Width = 27
        Height = 13
        Caption = 'Week'
      end
      object Label3: TLabel
        Left = 184
        Top = 48
        Width = 12
        Height = 13
        Caption = 'To'
      end
      object SpinEditFromWeek: TSpinEdit
        Left = 112
        Top = 44
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
      end
      object SpinEditToWeek: TSpinEdit
        Left = 208
        Top = 44
        Width = 65
        Height = 22
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
      end
      object RadioGrpPlanningMode: TRadioGroup
        Left = 2
        Top = 88
        Width = 633
        Height = 63
        Align = alBottom
        Caption = 'Planning'
        ItemIndex = 0
        Items.Strings = (
          'Do no process when there is planning'
          'Delete existing planning')
        TabOrder = 2
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 153
      Width = 637
      Height = 200
      Align = alClient
      Caption = 'Messages'
      TabOrder = 1
      object Memo1: TMemo
        Left = 2
        Top = 15
        Width = 633
        Height = 183
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  inherited pnlBottom: TPanel
    inherited btnOk: TBitBtn
      OnClick = btnOkClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
