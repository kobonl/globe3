object DialogProcessAbsenceHrsDM: TDialogProcessAbsenceHrsDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 197
  Top = 153
  Height = 519
  Width = 905
  object Query: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 48
    Top = 16
  end
  object qryBreakPerEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  BREAK_NUMBER,'
      '  DESCRIPTION,'
      '  PAYED_YN,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  BREAKPEREMPLOYEE'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  PAYED_YN = :PAYED_YN'
      '')
    Left = 152
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PAYED_YN'
        ParamType = ptUnknown
      end>
  end
  object qryBreakPerDepartment: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  DEPARTMENT_CODE,'
      '  SHIFT_NUMBER,'
      '  BREAK_NUMBER,'
      '  DESCRIPTION,'
      '  PAYED_YN,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  BREAKPERDEPARTMENT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  DEPARTMENT_CODE = :DEPARTMENT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  PAYED_YN = :PAYED_YN'
      '')
    Left = 152
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PAYED_YN'
        ParamType = ptUnknown
      end>
  end
  object qryBreakPerShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  BREAK_NUMBER,'
      '  DESCRIPTION,'
      '  PAYED_YN,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  BREAKPERSHIFT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  PAYED_YN = :PAYED_YN'
      '')
    Left = 152
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PAYED_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceHourPerEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEHOUR_DATE,'
      '  EMPLOYEE_NUMBER,'
      '  MANUAL_YN,'
      '  ABSENCE_MINUTE'
      'FROM '
      '  ABSENCEHOURPEREMPLOYEE'
      'WHERE '
      '  ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE AND'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  MANUAL_YN = :MANUAL_YN')
    Left = 152
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 152
    Top = 260
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, DESCRIPTION '
      'FROM '
      '  EMPLOYEE '
      'WHERE '
      '  TEAM_CODE >=:TEAMFROM AND TEAM_CODE <= :TEAMTO'
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 264
    Top = 160
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end>
  end
  object qryIllnessMsg: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ILLNESSMESSAGE_STARTDATE,'
      '  ILLNESSMESSAGE_ENDDATE, '
      '  ABSENCEREASON_CODE, '
      '  LINE_NUMBER'
      'FROM '
      '  ILLNESSMESSAGE'
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND '
      '  LINE_NUMBER = :LINE_NUMBER'
      'ORDER BY '
      '  ILLNESSMESSAGE_STARTDATE'
      ' ')
    Left = 368
    Top = 16
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'LINE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEmpAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EAV.PLANT_CODE, EAV.EMPLOYEEAVAILABILITY_DATE, '
      '  EAV.SHIFT_NUMBER,'
      '  EAV.EMPLOYEE_NUMBER, '
      '  EAV.AVAILABLE_TIMEBLOCK_1, EAV.AVAILABLE_TIMEBLOCK_2,'
      '  EAV.AVAILABLE_TIMEBLOCK_3, EAV.AVAILABLE_TIMEBLOCK_4,'
      '  EAV.AVAILABLE_TIMEBLOCK_5, EAV.AVAILABLE_TIMEBLOCK_6,'
      '  EAV.AVAILABLE_TIMEBLOCK_7, EAV.AVAILABLE_TIMEBLOCK_8,'
      '  EAV.AVAILABLE_TIMEBLOCK_9, EAV.AVAILABLE_TIMEBLOCK_10,'
      '  AR.ABSENCEREASON_CODE, AR.ABSENCETYPE_CODE'
      'FROM '
      '  EMPLOYEEAVAILABILITY EAV, ABSENCEREASON AR'
      'WHERE '
      '  EAV.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  AR.ABSENCEREASON_CODE = :ABSENCEREASON_CODE AND'
      '  AR.ABSENCETYPE_CODE = :ABSENCETYPE_CODE AND'
      '  EAV.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND'
      '  EAV.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND'
      '  (EAV.AVAILABLE_TIMEBLOCK_1 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_2 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_3 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_4 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_5 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_6 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_7 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_8 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_9 = AR.ABSENCEREASON_CODE OR'
      '  EAV.AVAILABLE_TIMEBLOCK_10 = AR.ABSENCEREASON_CODE)'
      'ORDER BY '
      '  EAV.EMPLOYEEAVAILABILITY_DATE'
      ' ')
    Left = 368
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object cdsEmpAvail: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'cdsPrimary'
        Fields = 
          'PLANT_CODE;EMPLOYEEAVAILABILITY_DATE;SHIFT_NUMBER;EMPLOYEE_NUMBE' +
          'R'
      end>
    IndexName = 'cdsPrimary'
    Params = <>
    StoreDefs = True
    Left = 368
    Top = 160
    object cdsEmpAvailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
    end
    object cdsEmpAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField
      FieldName = 'EMPLOYEEAVAILABILITY_DATE'
    end
    object cdsEmpAvailSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsEmpAvailEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_5: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_5'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_6: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_6'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_7: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_7'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_8: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_8'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_9: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_9'
      Size = 1
    end
    object cdsEmpAvailAVAILABLE_TIMEBLOCK_10: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_10'
      Size = 1
    end
  end
  object qryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 368
    Top = 252
  end
  object qryEmpAvailUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 368
    Top = 300
  end
  object qryCntr: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 456
    Top = 12
  end
  object qryTRS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  TRS.EMPLOYEE_NUMBER, '
      '  TRUNC(TRS.DATETIME_IN) AS DATEIN, '
      '  TRUNC(TRS.DATETIME_OUT) AS DATEOUT'
      'FROM '
      '  TIMEREGSCANNING TRS'
      'WHERE '
      '  ( '
      '    (TRS.EMPLOYEE_NUMBER >= :EMPFROM AND '
      '     TRS.EMPLOYEE_NUMBER <= :EMPTO)'
      '  ) AND '
      '  TRS.PROCESSED_YN =  '#39'Y'#39'  AND '
      '  TRS.DATETIME_OUT IS NOT NULL AND '
      '  ( '
      '   ((TRS.DATETIME_IN <= :DATEFROM) AND '
      '    (TRS.DATETIME_OUT > :DATEFROM)) OR '
      '   ((TRS.DATETIME_IN > :DATEFROM) AND '
      '    (TRS.DATETIME_OUT < :DATETO)) OR '
      '   ((TRS.DATETIME_OUT >= :DATETO) AND '
      '    (TRS.DATETIME_IN < :DATETO)) '
      '  ) '
      '  AND '
      '    TRS.DATETIME_IN >= :DATEFROM '
      'GROUP BY '
      
        '  TRS.EMPLOYEE_NUMBER, TRUNC(TRS.DATETIME_IN), TRUNC(TRS.DATETIM' +
        'E_OUT)'
      'ORDER BY '
      '  1,2 '
      '  '
      ''
      ' ')
    Left = 48
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end>
  end
  object QueryTmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 152
    Top = 308
  end
  object qryEmpBookProdHrs: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.BOOK_PROD_HRS_YN, E.TEAM_CODE'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      ' ('
      '  (:USER_NAME = '#39'*'#39') OR'
      '  (E.TEAM_CODE IN'
      
        '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :' +
        'USER_NAME))'
      '  ) AND'
      '  E.PLANT_CODE >= :PLANTFROM AND E.PLANT_CODE <= :PLANTTO AND'
      '  E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO AND'
      
        '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND E.EMPLOYEE_NUMBER <= :E' +
        'MPLOYEETO AND'
      '  E.IS_SCANNING_YN = :IS_SCANNING_YN'
      ' '
      ' '
      ' '
      ' ')
    Left = 456
    Top = 116
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IS_SCANNING_YN'
        ParamType = ptUnknown
      end>
  end
  object qryCheckEmpAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.AVAILABLE_TIMEBLOCK_1,'
      '  T.AVAILABLE_TIMEBLOCK_2,'
      '  T.AVAILABLE_TIMEBLOCK_3,'
      '  T.AVAILABLE_TIMEBLOCK_4,'
      '  T.AVAILABLE_TIMEBLOCK_5,'
      '  T.AVAILABLE_TIMEBLOCK_6,'
      '  T.AVAILABLE_TIMEBLOCK_7,'
      '  T.AVAILABLE_TIMEBLOCK_8,'
      '  T.AVAILABLE_TIMEBLOCK_9,'
      '  T.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  EMPLOYEEAVAILABILITY T'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE'
      '  '
      ' ')
    Left = 456
    Top = 68
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'EMPLOYEEAVAILABILITY_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryAHECheck: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  EXTRACT(YEAR FROM AH.ABSENCEHOUR_DATE) ABSENCE_YEAR, AH.EMPLOY' +
        'EE_NUMBER,'
      '  R.ABSENCETYPE_CODE, SUM(AH.ABSENCE_MINUTE) ABSENCE_MINUTE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE AH INNER JOIN EMPLOYEE E ON'
      '    AH.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN ABSENCEREASON R ON'
      '    AH.ABSENCEREASON_ID = R.ABSENCEREASON_ID'
      'WHERE'
      ' (   (:USER_NAME = '#39'*'#39') OR   (E.TEAM_CODE IN '
      
        '     (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = ' +
        ':USER_NAME)) )  AND'
      '  AH.ABSENCEHOUR_DATE >= :DATEFROM AND  '
      '  AH.ABSENCEHOUR_DATE <= :DATETO AND  '
      '  E.PLANT_CODE >= :PLANTFROM AND  '
      '  E.PLANT_CODE <= :PLANTTO AND  '
      '  E.TEAM_CODE >= :TEAMFROM AND  '
      '  E.TEAM_CODE <= :TEAMTO AND  '
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND  '
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO '
      'GROUP BY'
      '  EXTRACT(YEAR FROM AH.ABSENCEHOUR_DATE), AH.EMPLOYEE_NUMBER,'
      '  R.ABSENCETYPE_CODE'
      'ORDER BY '
      '  1, 2, 3  '
      ''
      ' '
      ' ')
    Left = 264
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object qryATCheck: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.*'
      'FROM '
      '  ABSENCETOTAL T '
      'WHERE '
      '  T.ABSENCE_YEAR = :ABSENCE_YEAR'
      'ORDER BY '
      '  T.ABSENCE_YEAR, T.EMPLOYEE_NUMBER'
      ' '
      ' ')
    Left = 264
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryDeleteSHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SALARYHOURPEREMPLOYEE S'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND S.MANUAL_YN = :MANUAL_YN'
      
        '  AND (S.SALARY_DATE >= :STARTDATE AND S.SALARY_DATE <= :ENDDATE' +
        ')'
      ' ')
    Left = 552
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end>
  end
  object qryPHEPTCheck1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PRODHOUREMPLOYEE_DATE, A.EMPLOYEE_NUMBER,'
      '  A.PEREMPL, A.PEREMPLPT'
      'FROM'
      '('
      '  SELECT PRODHOUREMPLOYEE_DATE, EMPLOYEE_NUMBER,'
      '    SUM(PT.PROD_MIN_EXCL_BREAK) PEREMPLPT,'
      
        '   (SELECT NVL(SUM(PE.PRODUCTION_MINUTE),0) FROM PRODHOURPEREMPL' +
        'OYEE PE'
      '    WHERE PE.PRODHOUREMPLOYEE_DATE = PT.PRODHOUREMPLOYEE_DATE'
      '    AND PE.EMPLOYEE_NUMBER = PT.EMPLOYEE_NUMBER'
      '    ) PEREMPL'
      '  FROM PRODHOURPEREMPLPERTYPE PT'
      '  WHERE PT.PRODHOUREMPLOYEE_DATE >= :DATEFROM'
      '    AND PT.PRODHOUREMPLOYEE_DATE <= :DATETO'
      '    AND PT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  GROUP BY PT.PRODHOUREMPLOYEE_DATE, EMPLOYEE_NUMBER'
      '  ORDER BY PT.PRODHOUREMPLOYEE_DATE, EMPLOYEE_NUMBER'
      ') A'
      'WHERE A.PEREMPL <> A.PEREMPLPT'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 264
    Top = 208
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryPHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.PRODHOUREMPLOYEE_DATE, T.PLANT_CODE, T.SHIFT_NUMBER,'
      
        'T.EMPLOYEE_NUMBER, T.WORKSPOT_CODE, T.JOB_CODE, T.PRODUCTION_MIN' +
        'UTE,'
      'T.MANUAL_YN, T.PAYED_BREAK_MINUTE'
      'FROM PRODHOURPEREMPLOYEE T'
      'WHERE T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      'T.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE'
      ''
      ' '
      ' ')
    Left = 264
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'PRODHOUREMPLOYEE_DATE'
        ParamType = ptUnknown
      end>
  end
  object qryPHEPTCheck2: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PRODHOUREMPLOYEE_DATE, A.EMPLOYEE_NUMBER,'
      '  A.PEREMPL, A.PEREMPLPT'
      'FROM'
      '('
      '  SELECT PE.PRODHOUREMPLOYEE_DATE, PE.EMPLOYEE_NUMBER,'
      '    SUM(PE.PRODUCTION_MINUTE) PEREMPL,'
      
        '    (SELECT NVL(SUM(PT.PROD_MIN_EXCL_BREAK),0) FROM PRODHOURPERE' +
        'MPLPERTYPE PT'
      '    WHERE PE.PRODHOUREMPLOYEE_DATE = PT.PRODHOUREMPLOYEE_DATE'
      '    AND PE.EMPLOYEE_NUMBER = PT.EMPLOYEE_NUMBER'
      '    ) PEREMPLPT'
      '  FROM PRODHOURPEREMPLOYEE PE'
      '  WHERE PE.PRODHOUREMPLOYEE_DATE >= :DATEFROM'
      '    AND PE.PRODHOUREMPLOYEE_DATE <= :DATETO'
      '    AND PE.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  GROUP BY PE.PRODHOUREMPLOYEE_DATE, EMPLOYEE_NUMBER'
      '  ORDER BY PE.PRODHOUREMPLOYEE_DATE, EMPLOYEE_NUMBER'
      ') A'
      'WHERE A.PEREMPL <> A.PEREMPLPT'
      ''
      ''
      ''
      ''
      ' '
      ' ')
    Left = 264
    Top = 256
    ParamData = <
      item
        DataType = ftString
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryARCorrPerCG: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AR.ABSENCEREASON_CODE'
      'FROM'
      '  ABSREASONCORRPERCONTRGRP ARC INNER JOIN ABSENCEREASON AR ON'
      '    ARC.ABSENCEREASON_ID = AR.ABSENCEREASON_ID'
      'WHERE'
      '  ARC.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE'
      'ORDER BY'
      '  1'
      ''
      ' '
      ' '
      ' ')
    Left = 368
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryAHEInit: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ABSENCETOTAL ABT'
      'SET ABT.USED_HOLIDAY_MINUTE = 0,'
      '  ABT.ILLNESS_MINUTE = 0,'
      '  ABT.USED_TFT_MINUTE = 0,'
      '  ABT.USED_WTR_MINUTE = 0,'
      '  ABT.USED_HLD_PREV_YEAR_MINUTE = 0,'
      '  ABT.USED_SENIORITY_HOLIDAY_MINUTE = 0,'
      '  ABT.USED_ADD_BANK_HLD_MINUTE = 0,'
      '  ABT.USED_SAT_CREDIT_MINUTE = 0,'
      '  ABT.USED_BANK_HLD_RESERVE_MINUTE = 0,'
      '  ABT.USED_SHORTWWEEK_MINUTE = 0,'
      '  ABT.USED_TRAVELTIME_MINUTE = 0,'
      '  ABT.MUTATIONDATE = SYSDATE,'
      '  ABT.MUTATOR = :MUTATOR'
      'WHERE ABT.ABSENCE_YEAR = :ABSENCE_YEAR AND'
      'ABT.EMPLOYEE_NUMBER IN'
      '('
      'SELECT E.EMPLOYEE_NUMBER'
      'FROM ABSENCETOTAL ABT INNER JOIN EMPLOYEE E ON'
      '  ABT.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  ABT.ABSENCE_YEAR = :ABSENCE_YEAR AND'
      '  E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  E.TEAM_CODE >= :TEAMFROM AND'
      '  E.TEAM_CODE <= :TEAMTO AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      ')'
      ''
      ' ')
    Left = 264
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsTotTFTUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ABSENCETOTAL ABT'
      
        'SET ABT.EARNED_TFT_MINUTE = ABT.EARNED_TFT_MINUTE - :EARNED_TFT_' +
        'MINUTE,'
      '  ABT.MUTATIONDATE = SYSDATE,'
      '  ABT.MUTATOR = :MUTATOR'
      'WHERE ABT.ABSENCE_YEAR = :ABSENCE_YEAR AND'
      'ABT.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 368
    Top = 208
    ParamData = <
      item
        DataType = ftFloat
        Name = 'EARNED_TFT_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryDELSHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SALARYHOURPEREMPLOYEE S'
      'WHERE'
      '  S.SALARY_DATE = :SALARY_DATE'
      '  AND S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND S.HOURTYPE_NUMBER = :HOURTYPE_NUMBER'
      '  AND S.MANUAL_YN = :MANUAL_YN'
      ''
      ' '
      ' ')
    Left = 48
    Top = 160
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryDELAHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM ABSENCEHOURPEREMPLOYEE A'
      'WHERE'
      '  A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE'
      '  AND A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND A.ABSENCEREASON_ID = :ABSENCEREASON_ID'
      '  AND A.MANUAL_YN = :MANUAL_YN'
      ''
      ' '
      ' '
      ' ')
    Left = 48
    Top = 208
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAHEUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE ABSENCEHOURPEREMPLOYEE A'
      'SET ABSENCE_MINUTE = :ABSENCE_MINUTE,'
      '  MUTATIONDATE = SYSDATE,'
      '  MUTATOR = :MUTATOR'
      'WHERE'
      '  A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE'
      '  AND A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND A.ABSENCEREASON_ID = :ABSENCEREASON_ID'
      '  AND A.MANUAL_YN = :MANUAL_YN'
      ''
      ''
      ' '
      ' ')
    Left = 48
    Top = 256
    ParamData = <
      item
        DataType = ftInteger
        Name = 'ABSENCE_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAHEFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_ID,'
      '  ABSENCE_MINUTE'
      'FROM ABSENCEHOURPEREMPLOYEE A'
      'WHERE'
      '  A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE'
      '  AND A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND A.ABSENCEREASON_ID = :ABSENCEREASON_ID'
      '  AND A.MANUAL_YN = :MANUAL_YN'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 552
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end>
  end
  object qryAHEInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO ABSENCEHOURPEREMPLOYEE'
      '('
      'ABSENCEHOUR_DATE,'
      'EMPLOYEE_NUMBER,'
      'PLANT_CODE,'
      'MANUAL_YN,'
      'SHIFT_NUMBER,'
      'ABSENCE_MINUTE,'
      'REMARK,'
      'CREATIONDATE,'
      'MUTATIONDATE,'
      'MUTATOR,'
      'ABSENCEREASON_ID'
      ')'
      'VALUES'
      '('
      ':ABSENCEHOUR_DATE,'
      ':EMPLOYEE_NUMBER,'
      ':PLANT_CODE,'
      ':MANUAL_YN,'
      ':SHIFT_NUMBER,'
      ':ABSENCE_MINUTE,'
      ':REMARK,'
      'SYSDATE,'
      'SYSDATE,'
      ':MUTATOR,'
      ':ABSENCEREASON_ID'
      ')'
      '')
    Left = 552
    Top = 64
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'REMARK'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCEREASON_ID'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT A.ABSENCEREASON_ID, A.ABSENCETYPE_CODE, A.ABSENCEREASON_C' +
        'ODE, A.CORRECT_WORK_HRS_YN'
      'FROM ABSENCEREASON A'
      'WHERE A.ABSENCEREASON_CODE = :ABSENCEREASON_CODE'
      ' ')
    Left = 48
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end>
  end
  object qrySHEInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SALARYHOURPEREMPLOYEE'
      '('
      'SALARY_DATE,'
      'EMPLOYEE_NUMBER,'
      'HOURTYPE_NUMBER,'
      'SALARY_MINUTE,'
      'MANUAL_YN,'
      'CREATIONDATE,'
      'MUTATIONDATE,'
      'EXPORTED_YN,'
      'MUTATOR,'
      'FROMMANUALPROD_YN'
      ')'
      'VALUES'
      '('
      ':SALARY_DATE,'
      ':EMPLOYEE_NUMBER,'
      ':HOURTYPE_NUMBER,'
      ':SALARY_MINUTE,'
      ':MANUAL_YN,'
      'SYSDATE,'
      'SYSDATE,'
      #39'N'#39','
      ':MUTATOR,'
      #39'N'#39
      ')')
    Left = 48
    Top = 112
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SALARY_MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MANUAL_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotal: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  T.LAST_YEAR_TFT_MINUTE,'
      '  T.EARNED_TFT_MINUTE,'
      '  T.USED_TFT_MINUTE'
      'FROM '
      '  ABSENCETOTAL T'
      'WHERE'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.ABSENCE_YEAR = :ABSENCE_YEAR'
      '')
    Left = 152
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qrySHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT NVL(SUM(S.SALARY_MINUTE),0) SALARY_MINUTE '
      'FROM SALARYHOURPEREMPLOYEE S '
      'WHERE S.SALARY_DATE = :SALARY_DATE AND '
      'S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'AND S.MANUAL_YN = '#39'N'#39)
    Left = 456
    Top = 216
    ParamData = <
      item
        DataType = ftDate
        Name = 'SALARY_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryAHECorrectFind: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  NVL(SUM(ABSENCE_MINUTE),0) ABSENCE_MINUTE'
      'FROM ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON AR ON'
      '  A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID'
      'WHERE'
      '  A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE'
      '  AND A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND A.MANUAL_YN = '#39'N'#39
      '  AND AR.CORRECT_WORK_HRS_YN = '#39'Y'#39
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 552
    Top = 216
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCalcSalary: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 456
    Top = 272
  end
  object qryDELAHECorrect: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM ABSENCEHOURPEREMPLOYEE A'
      'WHERE'
      '  A.ABSENCEHOUR_DATE = :ABSENCEHOUR_DATE'
      '  AND A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND A.MANUAL_YN = '#39'N'#39
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 552
    Top = 272
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'ABSENCEHOUR_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
