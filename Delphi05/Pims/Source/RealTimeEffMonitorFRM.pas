(*
  MRA:29-FEB-2016 PIM-142
  - Addition of Real Time Efficiency Monitor-dialog
  MRA:11-APR-2016 PIM-96
  - Also refresh:
    - When dialog is opened
    - When date is changed
    - When plant is changed
*)
unit RealTimeEffMonitorFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, SystemDMT, ComCtrls, StdCtrls, Buttons, dxEditor,
  dxExEdtr, dxExGrEd, dxExELib, dxDBTLCl, dxGrClms, dxLayout, dxEdLib,
  dxDBELib;

type
  TActiveGrid = (WorkspotEffPerMinute, EmployeeEffPerMinute, EmployeeEffPerShift);
  TRealTimeEffMonitorF = class(TGridBaseF)
    pnlMasterPlant: TPanel;
    grpBoxPlantDate: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    dxDBExtLookupEditPlant: TdxDBExtLookupEdit;
    BtnRefresh: TBitBtn;
    DateEditFrom: TDateTimePicker;
    pnlPages: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    dxDBGridWorkspotEffPerMin: TdxDBGrid;
    dxDBGridWorkspotEffPerMinSHIFT_DATE: TdxDBGridDateColumn;
    dxDBGridWorkspotEffPerMinSHIFT_NUMBER: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinPLANT_CODE: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinWORKSPOT_CODE: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinJOB_CODE: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinINTERVALSTARTTIME: TdxDBGridDateColumn;
    dxDBGridWorkspotEffPerMinINTERVALENDTIME: TdxDBGridDateColumn;
    dxDBGridWorkspotEffPerMinWORKSPOTSECONDSACTUAL: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinWORKSPOTSECONDSNORM: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinWORKSPOTQUANTITY: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinNORMLEVEL: TdxDBGridMaskColumn;
    dxDBGridWorkspotEffPerMinSTART_TIMESTAMP: TdxDBGridDateColumn;
    dxDBGridWorkspotEffPerMinEND_TIMESTAMP: TdxDBGridDateColumn;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    dxDBGridEmployeeEffPerMin: TdxDBGrid;
    dxDBGridEmployeeEffPerMinSHIFT_DATE: TdxDBGridDateColumn;
    dxDBGridEmployeeEffPerMinSHIFT_NUMBER: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinPLANT_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinWORKSPOT_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinJOB_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinINTERVALSTARTTIME: TdxDBGridDateColumn;
    dxDBGridEmployeeEffPerMinINTERVALENDTIME: TdxDBGridDateColumn;
    dxDBGridEmployeeEffPerMinEMPLOYEESECONDSACTUAL: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinEMPLOYEESECONDSNORM: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinEMPLOYEEQUANTITY: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinNORMLEVEL: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerMinColumnEMPLOYEE_NUMBER: TdxDBGridColumn;
    dxDBGridEmployeeEffPerShift: TdxDBGrid;
    dxDBGridEmployeeEffPerShiftSHIFT_DATE: TdxDBGridDateColumn;
    dxDBGridEmployeeEffPerShiftSHIFT_NUMBER: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftPLANT_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftWORKSPOT_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftJOB_CODE: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftEMPLOYEE_NUMBER: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftEMPLOYEESECONDSACTUAL: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftEMPLOYEESECONDSNORM: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftEMPLOYEEQUANTITY: TdxDBGridMaskColumn;
    dxDBGridEmployeeEffPerShiftNORMLEVEL: TdxDBGridMaskColumn;
    gBoxWorkspotEffPerMinute: TGroupBox;
    Label3: TLabel;
    dxDBDateEdit1: TdxDBDateEdit;
    Label4: TLabel;
    Label5: TLabel;
    dxDBEdit1: TdxDBEdit;
    dxDBEdit2: TdxDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    dxDBEdit3: TdxDBEdit;
    dxDBEdit4: TdxDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    dxDBEdit7: TdxDBEdit;
    dxDBEdit8: TdxDBEdit;
    dxDBEdit9: TdxDBEdit;
    dxDBEdit10: TdxDBEdit;
    Label13: TLabel;
    dxDBDateEdit2: TdxDBDateEdit;
    dxDBDateEdit3: TdxDBDateEdit;
    gBoxEmployeeEffPerMinute: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    dxDBDateEdit4: TdxDBDateEdit;
    dxDBEdit5: TdxDBEdit;
    dxDBEdit6: TdxDBEdit;
    dxDBEdit11: TdxDBEdit;
    dxDBEdit12: TdxDBEdit;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    dxDBEdit13: TdxDBEdit;
    dxDBEdit14: TdxDBEdit;
    dxDBEdit15: TdxDBEdit;
    dxDBEdit16: TdxDBEdit;
    dxDBDateEdit5: TdxDBDateEdit;
    dxDBDateEdit6: TdxDBDateEdit;
    gBoxEmployeeEffPerShift: TGroupBox;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    dxDBEdit17: TdxDBEdit;
    dxDBEdit18: TdxDBEdit;
    dxDBEdit19: TdxDBEdit;
    dxDBEdit20: TdxDBEdit;
    dxDBDateEdit7: TdxDBDateEdit;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    dxDBEdit21: TdxDBEdit;
    dxDBEdit22: TdxDBEdit;
    dxDBEdit23: TdxDBEdit;
    dxDBEdit24: TdxDBEdit;
    Label30: TLabel;
    dxDBEdit25: TdxDBEdit;
    dxDBEdit26: TdxDBEdit;
    Label31: TLabel;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
    procedure DateEditFromChange(Sender: TObject);
    procedure dxDBExtLookupEditPlantCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
  private
    { Private declarations }
    ActiveFormGrid: TActiveGrid;
    procedure SetDefaultValues;
    procedure ChangeGrid;
    procedure ChangeFilter;
    procedure RefreshGrid;
  public
    { Public declarations }
  end;

function RealTimeEffMonitorF: TRealTimeEffMonitorF;

implementation

{$R *.DFM}

uses
  RealTimeEffMonitorDMT;

var
  RealTimeEffMonitorF_HND: TRealTimeEffMonitorF;

function RealTimeEffMonitorF: TRealTimeEffMonitorF;
begin
  if (RealTimeEffMonitorF_HND = nil) then
  begin
    RealTimeEffMonitorF_HND := TRealTimeEffMonitorF.Create(Application);
    RealTimeEffMonitorDM :=
      RealTimeEffMonitorF_HND.CreateFormDM(TRealTimeEffMonitorDM);
    RealTimeEffMonitorF_HND.SetDefaultValues;
  end;
  Result := RealTimeEffMonitorF_HND;
end;

procedure TRealTimeEffMonitorF.FormDestroy(Sender: TObject);
begin
  inherited;
  RealTimeEffMonitorF_HND := nil;
end;

procedure TRealTimeEffMonitorF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TRealTimeEffMonitorF.FormShow(Sender: TObject);
begin
  inherited;
  Refresh;
  RefreshGrid; // PIM-96
end;

procedure TRealTimeEffMonitorF.SetDefaultValues;
begin
  PageControl1.ActivePageIndex := 0;
  ActiveFormGrid := WorkspotEffPerMinute;
  DateEditFrom.DateTime := Date;
  ChangeFilter;
  ChangeGrid;
end;

procedure TRealTimeEffMonitorF.ChangeGrid;
begin
  case ActiveFormGrid of
  WorkspotEffPerMinute:
    begin
      ActiveGrid := dxDBGridWorkspotEffPerMin;
      dsrcActive.DataSet :=
        RealTimeEffMonitorDM.qryWorkspotEffPerMin;
      gBoxWorkspotEffPerMinute.Visible := True;
      gBoxEmployeeEffPerMinute.Visible := False;
      gBoxEmployeeEffPerShift.Visible := False;
    end;
  EmployeeEffPerMinute:
    begin
      ActiveGrid := dxDBGridEmployeeEffPerMin;
      dsrcActive.DataSet :=
        RealTimeEffMonitorDM.qryEmployeeEffPerMin;
      gBoxWorkspotEffPerMinute.Visible := False;
      gBoxEmployeeEffPerMinute.Visible := True;
      gBoxEmployeeEffPerShift.Visible := False;
    end;
  EmployeeEffPerShift:
    begin
      ActiveGrid := Self.dxDBGridEmployeeEffPerShift;
      dsrcActive.DataSet :=
        RealTimeEffMonitorDM.qryEmployeeEffPerShift;
      gBoxWorkspotEffPerMinute.Visible := False;
      gBoxEmployeeEffPerMinute.Visible := False;
      gBoxEmployeeEffPerShift.Visible := True;
    end;
  end;
end;

procedure TRealTimeEffMonitorF.ChangeFilter;
var
  ADataFilter: TDataFilter;
begin
  ADataFilter.PlantCode :=
    RealTimeEffMonitorDM.TableMaster.FieldByName('PLANT_CODE').AsString;
  ADataFilter.ShiftDate := Trunc(DateEditFrom.DateTime);
  RealTimeEffMonitorDM.DataFilter := ADataFilter;
end;

procedure TRealTimeEffMonitorF.PageControl1Change(Sender: TObject);
begin
  inherited;
  case PageControl1.ActivePageIndex of
  0:
    begin
      ActiveFormGrid := WorkspotEffPerMinute;
    end;
  1:
    begin
      ActiveFormGrid := EmployeeEffPerMinute;
    end;
  2:
    begin
     ActiveFormGrid := EmployeeEffPerShift;
    end;
  end;
  ChangeFilter;
  ChangeGrid;
  RefreshGrid;
end;

procedure TRealTimeEffMonitorF.BtnRefreshClick(Sender: TObject);
begin
  inherited;
  RefreshGrid;
end;

procedure TRealTimeEffMonitorF.RefreshGrid;
begin
  ChangeFilter;
  case ActiveFormGrid of
  WorkspotEffPerMinute:
    begin
      with RealTimeEffMonitorDM.qryWorkspotEffPerMin do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString :=
          RealTimeEffMonitorDM.DataFilter.PlantCode;
        ParamByName('SHIFT_DATE').AsDateTime := Trunc(DateEditFrom.DateTime);
        Open;
      end;
    end;
  EmployeeEffPerMinute:
    begin
      with RealTimeEffMonitorDM.qryEmployeeEffPerMin do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString :=
          RealTimeEffMonitorDM.DataFilter.PlantCode;
        ParamByName('SHIFT_DATE').AsDateTime := Trunc(DateEditFrom.DateTime);
        Open;
      end;
    end;
  EmployeeEffPerShift:
    begin
      with RealTimeEffMonitorDM.qryEmployeeEffPerShift do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString :=
          RealTimeEffMonitorDM.DataFilter.PlantCode;
        ParamByName('SHIFT_DATE').AsDateTime := Trunc(DateEditFrom.DateTime);
        Open;
      end;
    end;
  end; // case
end; // RefreshGrid

procedure TRealTimeEffMonitorF.DateEditFromChange(Sender: TObject);
begin
  inherited;
  RefreshGrid; // PIM-96
end;

procedure TRealTimeEffMonitorF.dxDBExtLookupEditPlantCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  RefreshGrid; // PIM-96
end;

end.
