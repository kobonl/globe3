inherited DialogReportEmpInfoDevPeriodF: TDialogReportEmpInfoDevPeriodF
  Caption = 'Report Employee Info and Deviations'
  ClientHeight = 334
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 0
  end
  inherited pnlInsertBase: TPanel
    Height = 213
    TabOrder = 3
    inherited lblFromDateBase: TLabel
      Top = 116
    end
    inherited lblDateBase: TLabel
      Top = 116
    end
    inherited lblToDateBase: TLabel
      Top = 116
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 160
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 161
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 162
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 163
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 161
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 162
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 162
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 163
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 161
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 162
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 162
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 163
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 163
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 162
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object GroupBox1: TGroupBox [75]
      Left = 0
      Top = 143
      Width = 635
      Height = 70
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 27
      object CBoxShowSelection: TCheckBox
        Left = 10
        Top = 15
        Width = 183
        Height = 17
        Caption = 'Show selection'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 10
        Top = 41
        Width = 207
        Height = 17
        Caption = 'Export'
        TabOrder = 1
      end
      object CBoxShowDetails: TCheckBox
        Left = 314
        Top = 15
        Width = 295
        Height = 17
        Caption = 'Show Details'
        TabOrder = 2
        Visible = False
      end
    end
    inherited DatePickerFromBase: TDateTimePicker
      Top = 114
      TabOrder = 28
    end
    inherited DatePickerToBase: TDateTimePicker
      Top = 114
      TabOrder = 29
    end
  end
  inherited stbarBase: TStatusBar
    Top = 315
  end
  inherited pnlBottom: TPanel
    Top = 274
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        50040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507304469616C6F675265706F7274456D70496E666F44657650
        6572696F64462E44617461536F75726365456D706C46726F6D104F7074696F6E
        73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42
        616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F43
        6F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F707469
        6F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44
        656C6574650D6564676F43616E496E73657274116564676F43616E4E61766967
        6174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164
        416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F5464
        78444247726964436F6C756D6E0C436F6C756D6E4E756D626572074361707469
        6F6E06064E756D62657206536F72746564070463735570055769647468024109
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060F454D504C4F5945455F4E554D42455200000F546478444247726964436F6C
        756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A53686F
        7274206E616D6505576964746802540942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F54
        6478444247726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F
        6E06044E616D6505576964746803B4000942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060B4445534352495054494F4E0000
        0F546478444247726964436F6C756D6E0D436F6C756D6E416464726573730743
        617074696F6E06074164647265737305576964746802450942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D650607414444524553
        5300000F546478444247726964436F6C756D6E0E436F6C756D6E44657074436F
        64650743617074696F6E060F4465706172746D656E7420636F64650557696474
        6802580942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F4445504152544D454E545F434F444500000F5464784442477269
        64436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D
        20636F64650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        17040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072E4469616C6F675265706F72
        74456D70496E666F446576506572696F64462E44617461536F75726365456D70
        6C546F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F
        76696E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F
        76696E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C5369
        7A696E6700094F7074696F6E7344420B106564676F43616E63656C4F6E457869
        740D6564676F43616E44656C6574650D6564676F43616E496E73657274116564
        676F43616E4E617669676174696F6E116564676F436F6E6669726D44656C6574
        65126564676F4C6F6164416C6C5265636F726473106564676F557365426F6F6B
        6D61726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E456D
        706C0743617074696F6E06064E756D62657206536F7274656407046373557005
        576964746802310942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F454D504C4F5945455F4E554D42455200000F54647844
        4247726964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074
        696F6E060A53686F7274206E616D65055769647468024E0942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060A53484F52545F
        4E414D4500000F546478444247726964436F6C756D6E11436F6C756D6E446573
        6372697074696F6E0743617074696F6E06044E616D6505576964746803CC0009
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060B4445534352495054494F4E00000F546478444247726964436F6C756D6E0D
        436F6C756D6E416464726573730743617074696F6E0607416464726573730557
        6964746802650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D6506074144445245535300000F546478444247726964436F6C75
        6D6E0E436F6C756D6E44657074436F64650743617074696F6E060F4465706172
        746D656E7420636F64650942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060F4445504152544D454E545F434F444500000F54
        6478444247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F
        6E06095465616D20636F64650942616E64496E646578020008526F77496E6465
        780200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
end
