(*
  MRA:30-JUL-2012 20013430. Split export in 2 parts.
  - Export payroll SR
    - Split export in 2 parts (file + report)
      based on field EMPLOYEE.FREETEXT.
      Contents of field is 1 or 2.
      Based on this criteria create 2 files/reports.
*)
unit ReportExportPayrollSRQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, Db, DBTables, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FYear, FMonth: Integer;
    FExportGroup: String;
  public
    procedure SetValues(Year, Month: Integer; ExportGroup: String);
  end;

  TReportExportPayrollSRQR = class(TReportBaseF)
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabelHrs: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLabel16: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLblEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLblEmployeeTo: TQRLabel;
    QRLblMonth: TQRLabel;
    QRLabel24: TQRLabel;
    QRGroupHDEmpl: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel23: TQRLabel;
    QRLblYear: TQRLabel;
    QueryEmpl: TQuery;
    QRLabel3: TQRLabel;
    QRBandDetail: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText6: TQRDBText;
    QRLblEmpDescription: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRLblHrDay: TQRLabel;
    QRExportGroup: TQRLabel;
    QRLblExportGroupValue: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText4Print(sender: TObject; var Value: String);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRLblHrDayPrint(sender: TObject; var Value: String);
  private
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FEmpl, FContrEmpl, FIndexEmpl: Integer;
    function QRSendReportParameters(const PlantFrom, PlantTo, EmployeeFrom,
      EmployeeTo: String;
      const Year, Month: Integer;
      const ExportGroup: String): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportExportPayrollSRQR: TReportExportPayrollSRQR;

implementation

{$R *.DFM}

uses
  SystemDMT, UGlobalFunctions, UPimsMessageRes, UPimsConst,
  DialogExportPayrollFRM, DialogExportPayrollDMT;

procedure TQRParameters.SetValues(Year, Month: Integer; ExportGroup: String);
begin
  FYear := Year;
  FMonth := Month;
  FExportGroup := ExportGroup;
end;

function TReportExportPayrollSRQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo: String;
  const Year, Month: Integer;
  const ExportGroup: String): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
  {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(Year, Month, ExportGroup);
  end;
  SetDataSetQueryReport(QueryEmpl);
end;

function TReportExportPayrollSRQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  SelectEmployee: String;
begin
  Screen.Cursor := crHourGlass;
  // Change the decimal-separator!
  DecimalSeparator := '.';
  {initialize variables}
  SelectEmployee := '';
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    SelectEmployee :=
      '  AND EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom +
      '  AND EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo;
  end;
  QueryEmpl.Active := False;
  QueryEmpl.UniDirectional := False;
  QueryEmpl.SQL.Clear;
  SelectStr :=
    'SELECT ' +
    '  EMPLOYEE_NUMBER, DESCRIPTION ' +
    'FROM ' +
    '  EMPLOYEE ' +
    'WHERE ';
  // 20013430.
  if (QRParameters.FExportGroup = '1') or (QRParameters.FExportGroup = '2') then
    SelectStr := SelectStr +
      '  FREETEXT = ' + '''' + QRParameters.FExportGroup + '''' + ' AND ';
  SelectStr := SelectStr +
    '  PLANT_CODE  >= ''' +
    DoubleQuote(QRBaseParameters.FPlantFrom)+ '''' +
    '  AND PLANT_CODE <= '''+
    DoubleQuote(QRBaseParameters.FPlantTo) + '''' +
    SelectEmployee + ' ' +
    'ORDER BY ' +
    '  EMPLOYEE_NUMBER ';
  QueryEmpl.SQL.Add(UpperCase(SelectStr));
  if not QueryEmpl.Prepared then
    QueryEmpl.Prepare;
  QueryEmpl.Open;
  Result := True;
  {check if report is empty}
  if QueryEmpl.IsEmpty or
    DialogExportPayrollDM.cdsEmpExportLine.IsEmpty then
    Result := False;

  // 20013430. Set a filter for the clientdataset.
  //           '3' is used when there was no '1' or '2' found.
  with DialogExportPayrollDM do
  begin
    if (QRParameters.FExportGroup = '1') or
      (QRParameters.FExportGroup = '2') or
      (QRParameters.FExportGroup = '3') then
    begin
      cdsEmpExportLine.Filtered := False;
      cdsEmpExportLine.Filter := 'EXPORTGROUP = ' + '''' +
        QRParameters.FExportGroup + '''';
      cdsEmpExportLine.Filtered := True;
    end;
  end;

  Screen.Cursor := crDefault;
end;

procedure TReportExportPayrollSRQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption :=  QRBaseParameters.FPlantTo;
  QRLblEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLblEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  QRLblExportGroupValue.Caption := QRParameters.FExportGroup;
  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRLblEmployeeFrom.Caption := '*';
    QRLblEmployeeTo.Caption := '*';
  end;
  QRLblYear.Caption := IntToStr(QRParameters.FYear);
  QRLblMonth.Caption := IntToStr(QRParameters.FMonth);
end;

procedure TReportExportPayrollSRQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportExportPayrollSRQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  if qrptBase.DataSet = nil then
    qrptBase.DataSet := DialogExportPayrollDM.cdsEmpExportLine;
end;

procedure TReportExportPayrollSRQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FEmpl :=
    DialogExportPayrollDM.cdsEmpExportLine.
      FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if QueryEmpl.Locate('EMPLOYEE_NUMBER', FEmpl, []) then
    QRLblEmpDescription.Caption :=
      QueryEmpl.FieldByName('DESCRIPTION').AsString;
end;

procedure TReportExportPayrollSRQR.QRDBText4Print(sender: TObject;
  var Value: String);
var
  ValueBackup: String;
begin
  inherited;
  ValueBackup := Value;
  // Is this field representing days or hours?
  try
    if Value <> '' then
    begin
      if DialogExportPayrollDM.cdsEmpExportLine.
          FieldByName('DAYS_YN').AsString = 'N' then
        Value := IntMin2StrDigTime(Trunc(StrToFloat(Value)), 1, True)
      else
        Value := Format('%6.2f', [StrToFloat(Value)]);
    end;
  except
    Value := ValueBackup;
  end;
end;

procedure TReportExportPayrollSRQR.QRDBText6Print(sender: TObject;
  var Value: String);
var
  ValueBackup: String;
begin
  inherited;
  ValueBackup := Value;
  // Only show Efficiency/Bonus if there's a workspot
  try
    if DialogExportPayrollDM.cdsEmpExportLine.
        FieldByName('WORKSPOT_CODE').AsString = '' then
      Value := ''
    else
      if Value <> '' then
      begin
        Value := Format('%.2f', [StrToFloat(Value)]);
        if DialogExportPayrollDM.cdsEmpExportLine.
          FieldByName('BONUS_YN').AsString = 'Y' then
          Value := Value + ' ' + SPimsBonus
        else
          Value := Value + '  ' + SPimsEfficiency;
      end;
  except
    Value := ValueBackup;
  end;
end;

procedure TReportExportPayrollSRQR.QRLblHrDayPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // Show text 'hours' or 'days'
  if DialogExportPayrollDM.cdsEmpExportLine.
      FieldByName('DAYS_YN').AsString = 'N' then
    Value := SPimsHours
  else
    Value := SPimsDays;
end;

end.


