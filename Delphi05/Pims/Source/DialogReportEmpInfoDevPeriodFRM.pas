(*
  MRA:17-NOV-2015 PIM-23
  - New Report Employee Info and Deviations (per Period).
*)
unit DialogReportEmpInfoDevPeriodFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxExEdtr, dxEdLib, dxCntner, dxEditor,
  dxExGrEd, dxExELib, Dblup1a, ExtCtrls, SystemDMT, ReportEmpInfoDevPeriodDMT,
  ReportEmpInfoDevPeriodQRPT, jpeg;

type
  TDialogReportEmpInfoDevPeriodF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    CBoxShowDetails: TCheckBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportEmpInfoDevPeriodF: TDialogReportEmpInfoDevPeriodF;

function DialogReportEmpInfoDevPeriodForm: TDialogReportEmpInfoDevPeriodF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes, ListProcsFRM;

var
  DialogReportEmpInfoDevPeriodF_HND: TDialogReportEmpInfoDevPeriodF;

function DialogReportEmpInfoDevPeriodForm: TDialogReportEmpInfoDevPeriodF;
begin
  if (DialogReportEmpInfoDevPeriodF_HND = nil) then
  begin
    DialogReportEmpInfoDevPeriodF_HND := TDialogReportEmpInfoDevPeriodF.Create(Application);
    with DialogReportEmpInfoDevPeriodF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportEmpInfoDevPeriodF_HND;
end;

procedure TDialogReportEmpInfoDevPeriodF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportEmpInfoDevPeriodF_HND <> nil) then
  begin
    DialogReportEmpInfoDevPeriodF_HND := nil;
  end;
end;

procedure TDialogReportEmpInfoDevPeriodF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, True, True, False, False, False);
  UseDate := True;
  inherited;
//
end;

procedure TDialogReportEmpInfoDevPeriodF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportEmpInfoDevPeriodF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportEmpInfoDevPeriodDM := CreateReportDM(TReportEmpInfoDevPeriodDM);
  ReportEmpInfoDevPeriodQR := CreateReportQR(TReportEmpInfoDevPeriodQR);
end;

procedure TDialogReportEmpInfoDevPeriodF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportEmpInfoDevPeriodQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    Trunc(DatePickerFromBase.Date),
    Trunc(DatePickerToBase.Date),
    CheckBoxAllDepartments.Checked,
    CheckBoxAllTeams.Checked,
    CBoxShowSelection.Checked,
    CheckBoxExport.Checked,
    CBoxShowDetails.Checked) then
    ReportEmpInfoDevPeriodQR.ProcessRecords;
end;

procedure TDialogReportEmpInfoDevPeriodF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportEmpInfoDevPeriodF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
