unit MachineWorkspotFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls;

type
  TMachineWorkspotF = class(TGridBaseF)
    dxDetailGridColumnWorkspotCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function MachineWorkspotF: TMachineWorkspotF;

var
  MachineWorkspotF_HDN: TMachineWorkspotF;

implementation

{$R *.DFM}

uses
  SystemDMT, MachineDMT;

function MachineWorkspotF: TMachineWorkspotF;
begin
  if (MachineWorkspotF_HDN = nil) then
    MachineWorkspotF_HDN := TMachineWorkspotF.Create(Application);
  Result := MachineWorkspotF_HDN;
end;

procedure TMachineWorkspotF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  MachineWorkspotF_HDN := nil;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TMachineWorkspotF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
