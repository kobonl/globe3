inherited IDCardF: TIDCardF
  Left = 284
  Top = 239
  Width = 694
  Height = 502
  Caption = 'Id cards'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 678
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Width = 676
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 676
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 299
    Width = 678
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 681
      Height = 163
      Align = alLeft
      Caption = 'Cards'
      TabOrder = 0
      OnEnter = GroupBox1Enter
      object Label1: TLabel
        Left = 8
        Top = 28
        Width = 37
        Height = 13
        Caption = 'Number'
      end
      object Label2: TLabel
        Left = 8
        Top = 58
        Width = 46
        Height = 13
        Caption = 'Employee'
      end
      object Label3: TLabel
        Left = 408
        Top = 28
        Width = 55
        Height = 13
        Caption = 'Date active'
      end
      object Label11: TLabel
        Left = 408
        Top = 58
        Width = 62
        Height = 13
        Caption = 'Date expired'
      end
      object DBEditCardNumber: TDBEdit
        Tag = 1
        Left = 70
        Top = 24
        Width = 75
        Height = 19
        Ctl3D = False
        DataField = 'IDCARD_NUMBER'
        DataSource = IDCardDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 0
      end
      object DBPicker1: TDBPicker
        Tag = 1
        Left = 480
        Top = 24
        Width = 145
        Height = 21
        CalAlignment = dtaLeft
        Date = 41843
        Time = 41843
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 2
        DataField = 'STARTDATE'
        DataSource = IDCardDM.DataSourceDetail
      end
      object dxDBDateEdit1: TdxDBDateEdit
        Left = 480
        Top = 56
        Width = 145
        TabOrder = 3
        DataField = 'ENDDATE'
        DataSource = IDCardDM.DataSourceDetail
      end
      object dxDBLUEmployee: TdxDBExtLookupEdit
        Tag = 1
        Left = 70
        Top = 56
        Width = 315
        TabOrder = 1
        OnEnter = dxDBLUEmployeeEnter
        OnExit = dxDBLUEmployeeExit
        OnKeyPress = dxDBLUEmployeeKeyPress
        DataField = 'EMPLOYEE_NUMBER'
        DataSource = IDCardDM.DataSourceDetail
        PopupHeight = 180
        PopupWidth = 500
        OnPopup = dxDBLUEmployeePopup
        DBGridLayout = dxDBGridLayoutList1ItemEmployee
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 678
    Height = 144
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 140
      Width = 676
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 676
      Height = 139
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Id cards'
          Width = 718
        end>
      KeyField = 'IDCARD_NUMBER'
      DataSource = IDCardDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IDCARD_NUMBER'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Caption = 'Empl nr'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridLookupColumn
        Caption = 'Empl name'
        Width = 336
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEELU'
        DropDownWidth = 400
        ListFieldName = 'DESCRIPTION;SHORT_NAME;EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumn6: TdxDBGridColumn
        Caption = 'Empl shortname'
        DisableEditor = True
        Width = 128
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEESHORTLU'
      end
      object dxDetailGridColumn5: TdxDBGridDateColumn
        Caption = 'Date active'
        DisableEditor = True
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTDATE'
      end
      object dxDetailGridColumn4: TdxDBGridDateColumn
        Caption = 'Date expired'
        Width = 69
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENDDATE'
        DateValidation = True
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 496
    Top = 106
    object dxDBGridLayoutList1ItemEmployee: TdxDBGridLayout
      Data = {
        C9020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365071B494443617264444D2E4461
        7461536F75726365456D706C6F796565094F7074696F6E7344420B106564676F
        43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43
        616E496E73657274116564676F43616E4E617669676174696F6E116564676F43
        6F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72647310
        6564676F557365426F6F6B6D61726B730000135464784442477269644D61736B
        436F6C756D6E0F454D504C4F5945455F4E554D4245520743617074696F6E0606
        4E756D6265720942616E64496E646578020008526F77496E6465780200094669
        656C644E616D65060F454D504C4F5945455F4E554D4245520000135464784442
        477269644D61736B436F6C756D6E0A53484F52545F4E414D450743617074696F
        6E060A53686F7274206E616D650942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060A53484F52545F4E414D45000013546478
        4442477269644D61736B436F6C756D6E0B4445534352495054494F4E07436170
        74696F6E060B4465736372697074696F6E0942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060B4445534352495054494F4E00
        00135464784442477269644D61736B436F6C756D6E11504C414E545F44455343
        52495054494F4E0743617074696F6E0605506C616E740942616E64496E646578
        020008526F77496E6465780200094669656C644E616D650611504C414E545F44
        45534352495054494F4E000000}
    end
  end
end
