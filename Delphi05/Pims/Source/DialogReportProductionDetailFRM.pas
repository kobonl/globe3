(*
  MRA:20-MAY-2010 RV063.2. 889997.
  - Addition of shift selection
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:18-NOV-2010 RV080.3.
  - Temporary disable shifts: Because shifts are not
    used yet in Production Quantity-records (PQ) it is not possible
    to select on a shift. (Shift number is 0 in PQ-table).
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
  MRA:19-JUL-2011 RV095.1.
  - Workspot moved to DialogReportBase-form.
  MRA:12-JUL-2012 20012858.130.
  - Addition of setting to show shifts or not. Default not.
  MRA:23-JUL-2012 20012858.130.3. Change.
  - Report Production Details
    - Add option to show compare jobs or not using
      a checkbox.
      Note: A compare job is a job where field COMPARATION_JOB_YN
      is set to 'Y'.
  MRA:26-FEB-2013 Small change.
  - ProgressBar did not reset anymore.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
*)
unit DialogReportProductionDetailFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportProductionDetailF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    GroupBoxShow: TGroupBox;
    CheckBoxBU: TCheckBox;
    CheckBoxEmpl: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    CheckBoxWK: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    QueryBU: TQuery;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBoxJobcode: TCheckBox;
    CheckBoxPageJob: TCheckBox;
    Label16: TLabel;
    Label13: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    Label14: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    DataSourceWorkSpot: TDataSource;
    CheckBoxDept: TCheckBox;
    CheckBoxPageBU: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxShowBonus: TCheckBox;
    CheckBoxShowSales: TCheckBox;
    CheckBoxOnlyJob: TCheckBox;
    CheckBoxShowPayroll: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    ProgressBar: TProgressBar;
    CheckBoxShowShifts: TCheckBox;
    CheckBoxShowCompareJobs: TCheckBox;
    RadioGroupIncludeDowntime: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillEmpl;
    procedure FillBusiness;
{    procedure FillWorkSpot; }
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxWKClick(Sender: TObject);
    procedure CheckBoxJobcodeClick(Sender: TObject);
    procedure SetNewPageWK;
    procedure SetNewPageJobCode;
    procedure CheckBoxBUClick(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure ShowDateSelection(ShowYN: Boolean);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportProductionDetailF: TDialogReportProductionDetailF;

// RV089.1.
function DialogReportProductionDetailForm: TDialogReportProductionDetailF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportProductionDetailDMT, ReportProductionDetailQRPT,
  ListProcsFRM, UPimsMessageRes;

// RV089.1.
var
  DialogReportProductionDetailF_HND: TDialogReportProductionDetailF;

// RV089.1.
function DialogReportProductionDetailForm: TDialogReportProductionDetailF;
begin
  if (DialogReportProductionDetailF_HND = nil) then
  begin
    DialogReportProductionDetailF_HND := TDialogReportProductionDetailF.Create(Application);
    with DialogReportProductionDetailF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportProductionDetailF_HND;
end;

// RV089.1.
procedure TDialogReportProductionDetailF.FormDestroy(Sender: TObject);
begin
  inherited;
//  ReportProductionDetailDM.Free; // 20015220
//  ReportProductionDetailDM := nil;
  if (DialogReportProductionDetailF_HND <> nil) then
  begin
    DialogReportProductionDetailF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportProductionDetailF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportProductionDetailF.btnOkClick(Sender: TObject);
begin
  inherited;
  ProgressBar.Position := 0; // MRA:26-FEB-2013 Small change.
  if DateTimeFrom(DateFrom.Date) > DateTimeTo(DateTo.Date) then // 20015220
  begin
    DisplayMessage( SDateFromTo, mtInformation, [mbOk]);
    Exit;
  end;
  ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := False;
  ReportProductionDetailQR.EnableWorkspotIncludeForThisReport := False;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
  begin
    ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := True;
    ReportProductionDetailQR.EnableWorkspotIncludeForThisReport := True;
    ReportProductionDetailQR.InclExclWorkspotsResult := EditWorkspots.Text;
    if not ReportProductionDetailQR.InclExclWorkspotsAll then
      ReportProductionDetailQR.InclExclWorkspotsResult :=
        GetStrValue(ReportProductionDetailQR.InclExclWorkspotsResult);
    ReportProductionDetailDM.InclExclWorkspotsResult :=
      ReportProductionDetailQR.InclExclWorkspotsResult;
  end;
  // 20015220
  ReportProductionDetailDM.UseDateTime := SystemDM.DateTimeSel;
  ReportProductionDetailQR.UseDateTime := SystemDM.DateTimeSel;
  // 20015221
  ReportProductionDetailDM.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportProductionDetailQR.IncludeOpenScans := SystemDM.InclOpenScans;
  if ReportProductionDetailQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      IntToStr(GetIntValue(CmbPlusShiftFrom.Value)),
      IntToStr(GetIntValue(CmbPlusShiftTo.Value)),
      IntToStr(GetIntValue( dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue( dxDBExtLookupEditEmplTo.Text)),
      DateTimeFrom(DateFrom.Date), // 20015220
      DateTimeTo(DateTo.Date), // 20015220
      True, //      CheckBoxAllShifts.Checked, // RV080.3.
      CheckBoxBU.Checked, CheckBoxDept.Checked, CheckBoxWK.Checked,
      CheckBoxJobcode.Checked, CheckBoxEmpl.Checked,
      CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
      CheckBoxPageBU.Checked, CheckBoxPageDept.Checked,
      CheckBoxPageWK.Checked, CheckBoxPageJob.Checked,
      CheckBoxShowBonus.Checked,
      CheckBoxShowSales.Checked,
      CheckBoxOnlyJob.Checked,
      CheckBoxShowPayroll.Checked,
      CheckBoxExport.Checked,
      CheckBoxShowShifts.Checked,
      CheckBoxShowCompareJobs.Checked,
      RadioGroupIncludeDowntime.ItemIndex, // 20015586
      ProgressBar
      )
  then
    ReportProductionDetailQR.ProcessRecords;
  ProgressBar.Position := 0; // MRA:26-FEB-2013 Small change.
  Screen.Cursor := crDefault;
end;

procedure TDialogReportProductionDetailF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;

procedure TDialogReportProductionDetailF.FillEmpl;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin

    dxDBExtLookupEditEmplFrom.Visible := True;
    dxDBExtLookupEditEmplTo.Visible := True;
  end
  else
  begin
    dxDBExtLookupEditEmplFrom.Visible := False;
    dxDBExtLookupEditEmplTo.Visible := False;
  end;
end;
{
procedure TDialogReportProductionDetailF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    //RV067.2.
    QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}

procedure TDialogReportProductionDetailF.FormShow(Sender: TObject);
begin
  if SystemDM.WorkspotInclSel then
    EnableWorkspotIncludeForThisReport := True; // 20015223
  if SystemDM.DateTimeSel then
  begin
    UseDateTime := True; // 20015220
    ShowDateSelection(False);
  end;
  InitDialog(True, True, True, False, False, True, False); // RV080.3. Do not use shift-selection.
  inherited;

  //CAR 550254
{
  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    False, ComboBoxPlusTeamTo);
}
  //
  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageBU.Checked := False;
  CheckBoxPageDept.Checked := False;
  CheckBoxPageWK.Checked := False;
  CheckBoxPageJob.Checked := False;

  CheckBoxPageBU.Enabled := False;
  CheckBoxPageDept.Enabled := False;
  CheckBoxPageWK.Enabled := False;
  CheckBoxPageJob.Enabled := False;

  CheckBoxBU.Checked := False;
  CheckBoxDept.Checked := False;
  CheckBoxWK.Checked := True;
  CheckBoxJobCode.Checked := False;

  CheckBoxEmpl.Checked := False;
  CheckBoxShowBonus.Checked := True;
end;

procedure TDialogReportProductionDetailF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
// 20015220 + 20015221 Do not create this here but once in project.
//  ReportProductionDetailDM := CreateReportDM(TReportProductionDetailDM);
  Self.ReportDM := ReportProductionDetailDM;
  ReportProductionDetailDM.Init; // 20015220 + 20015221
  ReportProductionDetailQR := CreateReportQR(TReportProductionDetailQR);
end;

procedure TDialogReportProductionDetailF.CheckBoxDeptClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
  CheckBoxPageDept.Checked := CheckBoxDept.Checked;

end;

procedure TDialogReportProductionDetailF.SetNewPageWK;
begin
  CheckBoxPageDept.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageDept.Checked := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxWK.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageBU.Checked := not CheckBoxWK.Checked;
end;

procedure TDialogReportProductionDetailF.CheckBoxWKClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageWK.Enabled := CheckBoxWK.Checked;
  CheckBoxPageWK.Checked := CheckBoxWK.Checked;
  CheckBoxJobCode.Enabled := CheckBoxWK.Checked;
  CheckBoxJobCode.Checked := CheckBoxWK.Checked;

end;

procedure TDialogReportProductionDetailF.SetNewPageJobCode;
begin
  CheckBoxPageDept.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageDept.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Checked := not CheckBoxJobcode.Checked;
end;

procedure TDialogReportProductionDetailF.CheckBoxJobcodeClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageJob.Enabled := CheckBoxJobcode.Checked;
  CheckBoxPageJob.Checked := CheckBoxJobcode.Checked;

end;

procedure TDialogReportProductionDetailF.CheckBoxBUClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageBU.Enabled := CheckBoxBU.Checked;
  CheckBoxPageBU.Checked := CheckBoxBU.Checked;
end;

procedure TDialogReportProductionDetailF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
     GetStrValue(ComboBoxPlusBusinessTo.Value) then
       ComboBoxPlusBusinessTo.DisplayValue :=
         ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportProductionDetailF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportProductionDetailF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue;
}
end;

procedure TDialogReportProductionDetailF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue;
}
end;

procedure TDialogReportProductionDetailF.FillAll;
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportProductionDetailF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportProductionDetailF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// 20015220
procedure TDialogReportProductionDetailF.ShowDateSelection(
  ShowYN: Boolean);
begin
  Label2.Visible := ShowYN;
  Label4.Visible := ShowYN;
  Label26.Visible := ShowYN;
  DateFrom.Visible := ShowYN;
  DateTo.Visible := ShowYN;
end;

end.
