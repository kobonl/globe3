inherited DialogReportHrsPerContractGrpCUMF: TDialogReportHrsPerContractGrpCUMF
  Caption = 'Report hours per contract group cumulative'
  ClientHeight = 522
  ClientWidth = 592
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 592
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 296
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 592
    Height = 401
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 176
    end
    inherited LblEmployee: TLabel
      Top = 176
    end
    inherited LblToPlant: TLabel
      Top = 177
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 180
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 180
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [15]
      Left = 8
      Top = 477
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [16]
      Left = 8
      Top = 410
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [17]
      Left = 315
      Top = 71
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [18]
      Left = 128
      Top = 177
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [19]
      Left = 336
      Top = 177
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [20]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [21]
      Left = 128
      Top = 411
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [22]
      Left = 336
      Top = 408
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [23]
      Left = 128
      Top = 478
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [24]
      Left = 344
      Top = 478
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [25]
      Left = 40
      Top = 410
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [26]
      Left = 40
      Top = 477
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [27]
      Left = 315
      Top = 411
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [28]
      Left = 315
      Top = 477
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [29]
      Left = 8
      Top = 204
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [30]
      Left = 40
      Top = 204
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [31]
      Left = 315
      Top = 204
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 70
    end
    inherited LblDepartment: TLabel
      Top = 70
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 72
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 72
    end
    inherited LblToDepartment: TLabel
      Top = 72
    end
    inherited LblFromPlant2: TLabel
      Top = 227
    end
    inherited LblPlant2: TLabel
      Top = 227
    end
    inherited LblStarPlant2From: TLabel
      Top = 229
    end
    inherited LblToPlant2: TLabel
      Top = 227
    end
    inherited LblStarPlant2To: TLabel
      Top = 229
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    object LblStarContractGroupFrom: TLabel [59]
      Left = 128
      Top = 149
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToContractGroup: TLabel [60]
      Left = 315
      Top = 151
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarContractGroupTo: TLabel [61]
      Left = 336
      Top = 149
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblContractGroup: TLabel [62]
      Left = 40
      Top = 151
      Width = 70
      Height = 13
      Caption = 'Contractgroup'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromContractGroup: TLabel [63]
      Left = 8
      Top = 151
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarJobTo: TLabel
      Top = 125
    end
    inherited LblToJob: TLabel
      Top = 125
    end
    inherited LblStarJobFrom: TLabel
      Top = 125
    end
    inherited LblJob: TLabel
      Top = 125
    end
    inherited LblFromJob: TLabel
      Top = 125
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 126
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 127
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      ColCount = 146
      TabOrder = 30
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      ColCount = 147
      TabOrder = 31
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 521
      TabOrder = 32
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 69
      ColCount = 146
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 69
      ColCount = 147
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      Top = 72
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 176
      TabOrder = 13
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 176
      TabOrder = 14
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 151
      TabOrder = 29
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Left = 334
      ColCount = 152
      TabOrder = 26
    end
    inherited CheckBoxAllShifts: TCheckBox
      Left = 521
      TabOrder = 27
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      Top = 230
      TabOrder = 25
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 522
      Top = 178
      TabOrder = 15
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 36
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Top = 225
      ColCount = 156
      TabOrder = 23
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Top = 225
      ColCount = 157
      TabOrder = 24
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 96
      ColCount = 157
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 96
      ColCount = 158
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      TabOrder = 28
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 124
      ColCount = 157
      TabOrder = 10
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 124
      ColCount = 156
      TabOrder = 9
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 34
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 22
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 33
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 35
    end
    object GroupBoxSelection: TGroupBox
      Left = 152
      Top = 248
      Width = 369
      Height = 149
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 21
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 24
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 55
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 192
        Top = 24
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 4
      end
      object CheckBoxPageWK: TCheckBox
        Left = 192
        Top = 55
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 5
        Visible = False
      end
      object CheckBoxPageJob: TCheckBox
        Left = 192
        Top = 87
        Width = 169
        Height = 17
        Caption = 'New page per jobcode'
        TabOrder = 6
        Visible = False
      end
      object CheckBoxPageBU: TCheckBox
        Left = 8
        Top = 87
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 2
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 118
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 3
      end
    end
    object GroupBoxShow: TGroupBox
      Left = 8
      Top = 248
      Width = 137
      Height = 149
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 20
      object CheckBoxBU: TCheckBox
        Left = 8
        Top = 19
        Width = 89
        Height = 17
        Caption = 'Business unit'
        TabOrder = 0
        OnClick = CheckBoxBUClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 125
        Width = 73
        Height = 17
        Caption = 'Employee'
        TabOrder = 4
      end
      object CheckBoxWK: TCheckBox
        Left = 8
        Top = 72
        Width = 73
        Height = 17
        Caption = 'Workspot'
        TabOrder = 2
        OnClick = CheckBoxWKClick
      end
      object CheckBoxJobcode: TCheckBox
        Left = 8
        Top = 99
        Width = 73
        Height = 17
        Caption = 'Jobcode'
        TabOrder = 3
        OnClick = CheckBoxJobcodeClick
      end
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 46
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 1
        OnClick = CheckBoxDeptClick
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 127
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 128
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 136
      Top = 409
      Width = 180
      Height = 19
      ColCount = 132
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 18
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 350
      Top = 409
      Width = 180
      Height = 19
      ColCount = 133
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 19
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 202
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 16
    end
    object DateTo: TDateTimePicker
      Left = 334
      Top = 202
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 17
    end
    object ComboBoxPlusContractGroupFrom: TComboBoxPlus
      Left = 120
      Top = 148
      Width = 180
      Height = 19
      ColCount = 168
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusContractGroupFromCloseUp
    end
    object ComboBoxPlusContractGroupTo: TComboBoxPlus
      Left = 334
      Top = 148
      Width = 180
      Height = 19
      ColCount = 169
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusContractGroupToCloseUp
    end
  end
  inherited stbarBase: TStatusBar
    Top = 462
    Width = 592
  end
  inherited pnlBottom: TPanel
    Top = 481
    Width = 592
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 272
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 544
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        54040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507344469616C6F675265706F7274487273506572436F6E7472
        61637447727043554D462E44617461536F75726365456D706C46726F6D104F70
        74696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E65
        64676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E671065
        64676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E670009
        4F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F
        43616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E
        617669676174696F6E116564676F436F6E6669726D44656C657465126564676F
        4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300
        000F546478444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743
        617074696F6E06064E756D62657206536F727465640704637355700557696474
        6802410942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F454D504C4F5945455F4E554D42455200000F5464784442477269
        64436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E06
        0A53686F7274206E616D6505576964746802540942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D45
        00000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D65074361
        7074696F6E06044E616D6505576964746803B4000942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060B444553435249505449
        4F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E4164647265
        73730743617074696F6E06074164647265737305576964746802450942616E64
        496E646578020008526F77496E6465780200094669656C644E616D6506074144
        445245535300000F546478444247726964436F6C756D6E0E436F6C756D6E4465
        7074436F64650743617074696F6E060F4465706172746D656E7420636F646505
        576964746802580942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F4445504152544D454E545F434F444500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609
        5465616D20636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        1B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507324469616C6F675265706F72
        74487273506572436F6E747261637447727043554D462E44617461536F757263
        65456D706C546F104F7074696F6E73437573746F6D697A650B0E6564676F4261
        6E644D6F76696E670E6564676F42616E6453697A696E67106564676F436F6C75
        6D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E6564676F4675
        6C6C53697A696E6700094F7074696F6E7344420B106564676F43616E63656C4F
        6E457869740D6564676F43616E44656C6574650D6564676F43616E496E736572
        74116564676F43616E4E617669676174696F6E116564676F436F6E6669726D44
        656C657465126564676F4C6F6164416C6C5265636F726473106564676F557365
        426F6F6B6D61726B7300000F546478444247726964436F6C756D6E0A436F6C75
        6D6E456D706C0743617074696F6E06064E756D62657206536F72746564070463
        73557005576964746802310942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060F454D504C4F5945455F4E554D42455200000F
        546478444247726964436F6C756D6E0F436F6C756D6E53686F72744E616D6507
        43617074696F6E060A53686F7274206E616D65055769647468024E0942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060A5348
        4F52545F4E414D4500000F546478444247726964436F6C756D6E11436F6C756D
        6E4465736372697074696F6E0743617074696F6E06044E616D65055769647468
        03CC000942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060B4445534352495054494F4E00000F546478444247726964436F6C
        756D6E0D436F6C756D6E416464726573730743617074696F6E06074164647265
        737305576964746802650942616E64496E646578020008526F77496E64657802
        00094669656C644E616D6506074144445245535300000F546478444247726964
        436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F44
        65706172746D656E7420636F64650942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060F4445504152544D454E545F434F4445
        00000F546478444247726964436F6C756D6E0A436F6C756D6E5465616D074361
        7074696F6E06095465616D20636F64650942616E64496E646578020008526F77
        496E6465780200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited QueryEmplTo: TQuery
    Left = 376
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 80
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkSpot
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 328
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    Left = 496
    Top = 23
  end
  object QueryContractGroup: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION'
      'FROM'
      '  CONTRACTGROUP CG'
      'ORDER BY'
      '  CG.CONTRACTGROUP_CODE'
      ' ')
    Left = 408
    Top = 24
  end
end
