inherited AbsReasonPerCountryF: TAbsReasonPerCountryF
  Left = 287
  Top = 137
  Height = 451
  Caption = 'Absence reasons per country'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'code'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'description'
      end
      object dxMasterGridColumnExportType: TdxDBGridColumn
        Caption = 'Export Type'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'export_type'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 284
    Height = 128
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object Label3: TLabel
      Left = 8
      Top = 40
      Width = 53
      Height = 13
      Caption = 'Description'
    end
    object Label4: TLabel
      Left = 8
      Top = 69
      Width = 68
      Height = 13
      Caption = 'Absence Type'
    end
    object Label2: TLabel
      Left = 400
      Top = 69
      Width = 50
      Height = 13
      Caption = 'Hour Type'
    end
    object lblExportCode: TLabel
      Left = 8
      Top = 96
      Width = 60
      Height = 13
      Caption = 'Export Code'
    end
    object DBEditCode: TdxDBEdit
      Left = 96
      Top = 8
      Width = 121
      Enabled = False
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'ABSENCEREASON_CODE'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      ReadOnly = True
      StoredValues = 64
    end
    object DBCheckBoxPayed: TDBCheckBox
      Left = 400
      Top = 9
      Width = 225
      Height = 17
      Caption = 'Paid'
      DataField = 'PAYED_YN'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      Enabled = False
      ReadOnly = True
      TabOrder = 3
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBEditDescription: TdxDBEdit
      Left = 96
      Top = 37
      Width = 233
      Enabled = False
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'DESCRIPTION'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      ReadOnly = True
      StoredValues = 64
    end
    object DBCheckBoxOverruleWithIllness: TDBCheckBox
      Left = 400
      Top = 36
      Width = 225
      Height = 17
      Caption = 'Overrule with illness'
      DataField = 'OVERRULE_WITH_ILLNESS_YN'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      Enabled = False
      ReadOnly = True
      TabOrder = 4
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBEditAbsenceType: TdxDBEdit
      Left = 96
      Top = 67
      Width = 233
      Enabled = False
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'ABSENCETYPE'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      ReadOnly = True
      StoredValues = 64
    end
    object DBEditHourType: TdxDBEdit
      Left = 496
      Top = 67
      Width = 129
      Enabled = False
      Style.BorderStyle = xbsSingle
      TabOrder = 5
      DataField = 'HOURTYPE'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      ReadOnly = True
      StoredValues = 64
    end
    object dxDBEditExportCode: TdxDBEdit
      Left = 96
      Top = 96
      Width = 121
      Enabled = False
      Style.BorderStyle = xbsSingle
      TabOrder = 6
      DataField = 'export_code'
      DataSource = AbsReasonPerCountryDM.DataSourceDetail
      ReadOnly = True
      StoredValues = 64
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 129
    inherited spltDetail: TSplitter
      Top = 125
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 124
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Absence reasons'
        end>
      KeyField = 'ABSENCEREASON_ID'
      OnEnter = dxDetailGridEnter
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 88
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCEREASON_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 288
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnAbsenceType: TdxDBGridLookupColumn
        Caption = 'Absence type'
        Width = 154
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ABSENCETYPE'
        ListFieldName = 'DESCRIPTION;ABSENCETYPE_CODE'
      end
      object dxDetailGridColumnHourType: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 134
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPE'
      end
      object dxDetailGridColumnPayed: TdxDBGridCheckColumn
        Caption = 'Paid'
        Width = 58
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PAYED_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnOverruleWithIllness: TdxDBGridCheckColumn
        Caption = 'Overrule with illness'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OVERRULE_WITH_ILLNESS_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnExportCode: TdxDBGridColumn
        Caption = 'Export code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Export_code'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
  end
end
