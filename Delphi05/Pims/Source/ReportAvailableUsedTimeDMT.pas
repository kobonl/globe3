(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportAvailableUsedTimeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, Provider, DBClient;

type
  TReportAvailableUsedTimeDM = class(TReportBaseDM)
    DataSourceEmpl: TDataSource;
    QueryEmpl: TQuery;
    QueryEmplEMPLOYEE_NUMBER: TIntegerField;
    QueryEmplDESCRIPTION: TStringField;
    QueryEmplSTARTDATE: TDateTimeField;
    QueryEmplDEPARTMENT_CODE: TStringField;
    QueryEmpAvail: TQuery;
    QueryEmpAvailPLANT_CODE: TStringField;
    QueryEmpAvailSHIFT_NUMBER: TIntegerField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_1: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_2: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_3: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_4: TStringField;
    QueryEmplENDDATE: TDateTimeField;
    QueryEmplUSED_HOLIDAY_MINUTE: TFloatField;
    QueryEmplLAST_YEAR_HOLIDAY_MINUTE: TFloatField;
    QueryEmplNORM_HOLIDAY_MINUTE: TFloatField;
    QueryEmplLAST_YEAR_WTR_MINUTE: TFloatField;
    QueryEmplEARNED_WTR_MINUTE: TFloatField;
    QueryEmplUSED_WTR_MINUTE: TFloatField;
    QueryEmplLAST_YEAR_TFT_MINUTE: TFloatField;
    QueryEmplEARNED_TFT_MINUTE: TFloatField;
    QueryEmplUSED_TFT_MINUTE: TFloatField;
    cdsAbsenceReason: TClientDataSet;
    dspAbsenceReason: TDataSetProvider;
    qryAbsenceReason: TQuery;
    QueryEmplPLANT_CODE: TStringField;
    QueryEmplPDESCRIPTION: TStringField;
    QueryEmplTEAM_CODE: TStringField;
    QueryEmplTDESCRIPTION: TStringField;
    QueryEmplDDESCRIPTION: TStringField;
    QueryEmpAvailEMPLOYEEAVAILABILITY_DATE: TDateTimeField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_5: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_6: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_7: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_8: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_9: TStringField;
    QueryEmpAvailAVAILABLE_TIMEBLOCK_10: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportAvailableUsedTimeDM: TReportAvailableUsedTimeDM;

implementation

uses CalculateTotalHoursDMT, SystemDMT;

{$R *.DFM}

procedure TReportAvailableUsedTimeDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // MR:06-10-2003
  cdsAbsenceReason.Open;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
end;

procedure TReportAvailableUsedTimeDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  // MR:06-10-2003
  cdsAbsenceReason.Close;
end;

procedure TReportAvailableUsedTimeDM.QueryEmplFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
