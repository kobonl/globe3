(*
  MRA:21-OCT-2013 20014715
  - New Report Incentive Program (WMU)
  - REMARK: The paper-size has been fixed to Letter-format + Landscape
  MRA:27-NOV-2013 20014715 Rework
  - It goes wrong when on 1 day scans are made on different departments,
    because it uses the department to see what first/last scan is.
    To solve this, do not look at department when looking for first/last scan.
  - Example:
    - Here shift 1 is from 6:00 to 14:30.
      - Scan 1: 5:49 - 7:07 - Department 1
      - Scan 2: 7:07 - 10:00 - Department 2
      - Scan 3: 10:00 - 12:00 - Department 2
    - Here the first scan is 5:49, but when it looks at department 2 it
      find 7:07 as first scan, but that is not really the first scan, but
      then it thinks the 7:07 is too late.
  - Also: Sometimes it showed some value double for early/late. To prevent
          this a string-list is used to gather only unique values.
*)
unit ReportIncentiveProgramQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls, SystemDMT, UScannedIDCard, CalculateTotalHoursDMT;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FMonth, FYear: Integer;
    FShowSelection: Boolean;
    FExportToFile: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      Month, Year: Integer;
      ShowSelection: Boolean;
      ExportToFile: Boolean);
  end;
  
type
  TReportIncentiveProgramQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblFromEmp: TQRLabel;
    QRLblEmp: TQRLabel;
    QRLblEmpFrom: TQRLabel;
    QRLblToEmp: TQRLabel;
    QRLblEmpTo: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLblMonthValue: TQRLabel;
    QRLblToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblMonth: TQRLabel;
    QRLblYear: TQRLabel;
    QRLblYearValue: TQRLabel;
    QRBandHD: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRLabel15: TQRLabel;
    QRBandDetailEmployee: TQRBand;
    QRDBText1: TQRDBText;
    QRDBTextName: TQRDBText;
    QRDBText2: TQRDBText;
    QRBandSummary: TQRBand;
    QRLabel4: TQRLabel;
    QRLblAbsenceValue: TQRLabel;
    QRLblLateEarlyValue: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandHDAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText2Print(sender: TObject; var Value: String);
    procedure QRBandDetailEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
    procedure SetHorShapeWidth(WidthShape: Integer);
  public
    { Public declarations }
    function QRSendReportParameters(
      const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const Month, Year: Integer;
      const ShowSelection: Boolean;
      const ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportIncentiveProgramQR: TReportIncentiveProgramQR;

implementation

uses
  ReportIncentiveProgramDMT, ListProcsFRM, UGlobalFunctions, UPimsMessageRes;

{$R *.DFM}

{ TReportIncentiveProgramQR }

procedure ExportDetail(
  EmployeeNumber, EmployeeName, Absence, LateEarly, Employed60: String);
  function ReplaceSep(AValue: String): String;
  begin
    Result := StringReplace(AValue, ExportClass.Sep, ',', [rfReplaceAll]);
  end;
begin
  ExportClass.AddText(
    EmployeeNumber + ExportClass.Sep +
    EmployeeName + ExportClass.Sep +
    ReplaceSep(Absence) + ExportClass.Sep +
    ReplaceSep(LateEarly) + ExportClass.Sep +
    Employed60
    );
end;

function TReportIncentiveProgramQR.ExistsRecords: Boolean;
var
//  SelectStr: String;
  LastDatePrevMonth, EndDate: TDateTime;
  Year, Month: Word;
  function Determine3rdMonday(AYear, AMonth: Word; AEndDate: TDateTime): TDateTime;
  begin
    Result := AEndDate;
    // Find the 1st Monday
    while ListProcsF.PimsDayOfWeek(Result) <> 2 do // Monday?
      Result := Result + 1;
    // 3rd Monday is 2 weeks further
    Result := Result + 14;
  end;
begin
  // Determine previous month
  Year := QRParameters.FYear;
  Month := QRParameters.FMonth;
  if Month > 1 then
    Month := Month - 1
  else
  begin
    Year := Year - 1;
    Month := 12;
  end;
  LastDatePrevMonth := EncodeDate(Year, Month,
    ListProcsF.LastMounthDay(Year, Month));
  // Determine 3rd Monday of next month
  Year := QRParameters.FYear;
  Month := QRParameters.FMonth;
  if Month < 12 then
    Month := Month + 1
  else
  begin
    Year := Year + 1;
    Month := 1;
  end;
  EndDate := Determine3rdMonday(Year, Month, EncodeDate(Year, Month, 1));

// !!!TESTING!!!
{
  WDebugLog('LastDatePrevMonth=' +
  DateToStr(LastDatePrevMonth) +
  ' EndDate=' + DateToStr(EndDate)
  );
}

  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRBaseParameters.FEmployeeFrom := '0';
    QRBaseparameters.FEmployeeTo := '99999999';
  end;

  with ReportIncentiveProgramDM.qryEmployee do
  begin
    Close;
    ParamByName('PLANTFROM').AsString := QRBaseParameters.FPlantFrom;
    ParamByName('PLANTTO').AsString := QRBaseParameters.FPlantTo;
    ParamByName('EMPLOYEEFROM').AsString := QRBaseParameters.FEmployeeFrom;
    ParamByName('EMPLOYEETO').AsString := QRBaseparameters.FEmployeeTo;
    ParamByName('LASTDATEPREVMONTH').AsDateTime := LastDatePrevMonth;
    ParamByName('ENDDATE').AsDateTime := EndDate;
    ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
    ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
    Open;
    Result := not Eof;
  end;
  if Result then
  begin
    SetHorShapeWidth(Self.QRBndBasePageHeader.Width);
    with ReportIncentiveProgramDM.qryAbsence do
    begin
      Close;
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      Open;
    end;
{    with ReportIncentiveProgramDM.qryEarlyLate do
    begin
      Close;
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo + 1; // Uses < for compare!
      Open;
    end; }
  end;
end; // ExistsRecords

procedure TReportIncentiveProgramQR.ConfigReport;
begin
//  inherited;
  ExportClass.ExportDone := False;
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    QRLblEmpFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmpTo.Caption := QRBaseParameters.FEmployeeTo;
  end
  else
  begin
    QRLblEmpFrom.Caption := '*';
    QRLblEmpTo.Caption := '*';
  end;
  QRLblMonthValue.Caption := IntToStr(QRParameters.FMonth);
  QRLblYearValue.Caption := IntToStr(QRParameters.FYear);
end; // ConfigReport

function TReportIncentiveProgramQR.QRSendReportParameters(
  const PlantFrom, PlantTo, EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const Month, Year: Integer;
  const ShowSelection, ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      Month, Year,
      ShowSelection,
      ExportToFile);
  end;
  SetDataSetQueryReport(ReportIncentiveProgramDM.qryEmployee);
end; // QRSendReportParameters

procedure TReportIncentiveProgramQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportIncentiveProgramQR.SetHorShapeWidth(WidthShape: Integer);
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  QRLabel4.Width := WidthShape;
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRShape) then
    begin
      if  (TempComponent as TQRShape).Top < 1 then
        (TempComponent as TQRShape).Top := 1;
      (TempComponent as TQRShape).Width := WidthShape;
    end;
  end;
end; // SetHorShapeWidth

{ TQRParameters }

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime; Month,
  Year: Integer; ShowSelection, ExportToFile: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FMonth := Month;
  FYear := Year;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
end; // SetValues

procedure TReportIncentiveProgramQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportIncentiveProgramQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      // Plant
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      // Employee
      ExportClass.AddText(QRLblFromEmp.Caption + ' ' +
        QRLblEmp.Caption + ' ' + QRLblEmpFrom.Caption + ' ' +
        QRLblToEmp.Caption + ' ' + QRLblEmpTo.Caption);
      // Month
      ExportClass.AddText(QRLblMonth.Caption + ' ' +
        QRLblMonthValue.Caption);
      // Year
      ExportClass.AddText(QRLblYear.Caption + ' ' +
        QRLblYearValue.Caption);
    end;
  end;
end;

procedure TReportIncentiveProgramQR.QRBandHDAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportDetail(
      QRLabel6.Caption, // Number
      QRLabel3.Caption, // Name
      QRLabel7.Caption, // Absence
      QRLabel8.Caption, // Late / Early
      QRLabel15.Caption // Employed > 60
      );
  end;
end;

procedure TReportIncentiveProgramQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportIncentiveProgramQR.QRBandDetailEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    // Detail
    with ReportIncentiveProgramDM do
    begin
      ExportDetail(
        qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsString,
        qryEmployee.FieldByName('DESCRIPTION').AsString,
        QRLblAbsenceValue.Caption,
        QRLblLateEarlyValue.Caption,
        qryEmployee.FieldByName('EMPLOYED60').AsString
        );
    end;
  end;
end;

procedure TReportIncentiveProgramQR.QRDBText2Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if Value = 'Y' then
    Value := SPimsReportYes
  else
    Value := SPimsReportNo;
end;

procedure TReportIncentiveProgramQR.QRBandDetailEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  EarlyLateList: TStringList;
  function DetermineAbsence: String;
  var
    ABSStart, ABSEnd: TDateTime;
    ABSReason: String;
    procedure CreateLine;
    begin
      if ABSReason <> '' then
      begin
        if Result <> '' then
          Result := Result + '; ';
        if ABSStart = ABSEnd then
          Result := Result + ABSReason + ' ' + DateToStr(ABSStart)
        else
          Result := Result + ABSReason + ' ' + DateToStr(ABSStart) +
            ' ' + SPimsReportTill + ' ' + DateToStr(ABSEnd);
      end;
    end; // CreateLine
  begin
    Result := '';
    with ReportIncentiveProgramDM do
    begin
      qryAbsence.Filtered := False;
      qryAbsence.Filter := 'EMPLOYEE_NUMBER = ' +
        qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsString;
      qryAbsence.Filtered := True;
      if not qryAbsence.Eof then
      begin
        ABSStart := 0;
        ABSEnd := 0;
        ABSReason := '';
        while not qryAbsence.Eof do
        begin
          if (ABSStart = 0) then
          begin
            ABSStart := qryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
            ABSEnd := ABSStart;
            ABSReason := qryAbsence.FieldByName('DESCRIPTION').AsString;
          end
          else
          begin
            if (ABSReason =
              qryAbsence.FieldByName('DESCRIPTION').AsString) and
              (ABSEnd + 1 =
              qryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime) then
            begin
              ABSEnd := qryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
            end
            else
            begin
              CreateLine;
              ABSStart :=
                qryAbsence.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
              ABSEnd := ABSStart;
              ABSReason := qryAbsence.FieldByName('DESCRIPTION').AsString;
            end;
          end;
          qryAbsence.Next;
          if qryAbsence.Eof then
            CreateLine;
        end; // while
      end; // if
    end; // with
  end; // DetermineAbsence
(*
  function DetermineLateEarlyOLD: String;
//  var
//    StartDate, EndDate: TDateTime;
//    EmployeeIDCard: TScannedIDCard;
    function IsAbsent(ADate: TDateTime): Boolean;
    begin
      Result := False;
      with ReportIncentiveProgramDM.qryAbsence do
      begin
        First;
        while (not Result) and (not Eof) do
        begin
          if FieldByName('ABSENCEHOUR_DATE').AsDateTime = ADate then
            Result := True;
          Next;
        end;
      end;
    end; // IsAbsent
  begin
    Result := '';
    with ReportIncentiveProgramDM do
    begin
      qryEarlyLate.Filtered := False;
      qryEarlyLate.Filter := 'EMPLOYEE_NUMBER = ' +
        qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsString;
      qryEarlyLate.Filtered := True;
      // Also look if employee was not absent during the scan-date.
      qryAbsence.Filtered := False;
      qryAbsence.Filter := 'EMPLOYEE_NUMBER = ' +
        qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsString;
      qryAbsence.Filtered := True;
      if not qryEarlyLate.Eof then
      begin
        while not qryEarlyLate.Eof do
        begin
          if not IsAbsent(qryEarlyLate.FieldByName('SDATE').AsDateTime) then
          begin
            // Late
            if (qryEarlyLate.FieldByName('DATETIME_IN').AsInteger >
              qryEarlyLate.FieldByName('STARTTIME').AsInteger) then
            begin
              if Result <> '' then
                Result := Result + '; ';
              Result := Result +
                DateToStr(qryEarlyLate.FieldByName('SDATE').AsDateTime) + ' ' +
                  SPimsReportLate + ' ' +
                  FormatDateTime('hh:mm', qryEarlyLate.FieldByName('SDATETIME_IN').AsDateTime);
  //                IntMin2StringTime(
  //                  qryEarlyLate.FieldByName('DATETIME_IN').AsInteger, True);
            end;
            // Eerly
            if qryEarlyLate.FieldByName('DATETIME_OUT').AsInteger <
              qryEarlyLate.FieldByName('ENDTIME').AsInteger then
            begin
              if Result <> '' then
                Result := Result + '; ';
              Result := Result +
                DateToStr(qryEarlyLate.FieldByName('SDATE').AsDateTime) + ' ' +
                SPimsReportEarly + ' ' +
                FormatDateTime('hh:mm', qryEarlyLate.FieldByName('SDATETIME_OUT').AsDateTime);
  //              IntMin2Stringtime(
  //                qryEarlyLate.FieldByName('DATETIME_OUT').AsInteger, True);
            end;
          end; // if not IsAbsent
          qryEarlyLate.Next;
        end; // while
      end; // if
    end; // with
  end; // DetermineLateEarlyOLD
*)
  function DetermineLateEarly: String;
  var
    EmployeeIDCard: TScannedIDCard;
    StartDate, EndDate: TDateTime;
    function IsAbsent(ADate: TDateTime): Boolean;
    begin
      Result := False;
      with ReportIncentiveProgramDM.qryAbsence do
      begin
        First;
        while (not Result) and (not Eof) do
        begin
          if FieldByName('ABSENCEHOUR_DATE').AsDateTime = ADate then
            Result := True;
          Next;
        end;
      end;
    end; // IsAbsent
    // 20014715 Rework
    // Use a stringlist to get the results, to prevent it shows
    // some double values.
    procedure AddToEarlyLateList(AValue: String);
      function ValueExists: Boolean;
      begin
        Result := (EarlyLateList.IndexOf(AValue) <> -1);
      end;
    begin
      if not ValueExists then
        EarlyLateList.Add(AValue);
    end;
    // 20014715 Rework
    function DetermineResult: String;
    var
      I: Integer;
    begin
      Result := '';
      for I := 0 to EarlyLateList.Count - 1 do
      begin
        if Result <> '' then
           Result := Result + '; ';
        Result := Result + EarlyLateList.Strings[I];
      end;

    end;
  begin
    Result := '';
    EarlyLateList.Clear;
    with ReportIncentiveProgramDM do
    begin
      // Shift-loop (based on scans of employee)
      qryShiftLoop.Close;
      qryShiftLoop.ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      qryShiftLoop.ParamByName('DATETO').AsDateTime := QRParameters.FDateTo + 1;
      qryShiftLoop.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      qryShiftLoop.Open;
      qryShiftLoop.First;
      while not qryShiftLoop.Eof do
      begin
        // Also look if employee was not absent during the scan-date.
        qryAbsence.Filtered := False;
        qryAbsence.Filter := 'EMPLOYEE_NUMBER = ' +
          qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsString;
        qryAbsence.Filtered := True;
        EmptyIDCard(EmployeeIDCard);
        EmployeeIDCard.EmployeeCode :=
          qryEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        EmployeeIDCard.ShiftNumber :=
          qryShiftLoop.FieldByName('SHIFT_NUMBER').AsInteger;
        EmployeeIDCard.PlantCode :=
          qryShiftLoop.FieldByName('PLANT_CODE').AsString;
        EmployeeIDCard.DepartmentCode :=
          qryShiftLoop.FieldByName('DEPARTMENT_CODE').AsString;
        EmployeeIDCard.DateIn :=
          qryShiftLoop.FieldByName('DATE_IN').AsDateTime + 0.5;
        EmployeeIDCard.DateOut :=
          qryShiftLoop.FieldByName('DATE_IN').AsDateTime + 0.5;
        AProdMinClass.GetShiftDay(EmployeeIDCard, StartDate, EndDate);
        if qryShiftLoop.FieldByName('SHIFT_DATE').AsDateTime <> Trunc(StartDate) then
        begin
          // Add 1 day! Or we are looking at wrong shiftdate!
          StartDate := StartDate + 1;
          EndDate := EndDate + 1;
        end;
        // Look if it not starting before the Month-period, otherwise skip it.
        if (Trunc(StartDate) >= QRParameters.FDateFrom) and
          (Trunc(EndDate) >= QRParameters.FDateFrom) then
        begin
          // Now look for first/last scan within this period
          // Add margin of 2 or 4 hours:
          // 1/12 = 2 hours
          // 1/6 = 4 hours
          // 20014715 Rework: Do not look at department (and plant), in this
          //          query, to prevent it thinks a scan is too late.
          qryFirstLastScans.Close;
          qryFirstLastScans.ParamByName('DATEFROM').AsDateTime :=
            StartDate - 1/6;
          qryFirstLastScans.ParamByName('DATETO').AsDateTime :=
            EndDate + 1/6;
          qryFirstLastScans.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            EmployeeIDCard.EmployeeCode;
          // 20014715 Rework
//          qryFirstLastScans.ParamByName('PLANT_CODE').AsString :=
//            EmployeeIDCard.PlantCode;
          qryFirstLastScans.ParamByName('SHIFT_NUMBER').AsInteger :=
            EmployeeIDCard.ShiftNumber;
          // 20014715 Rework
//          qryFirstLastScans.ParamByName('DEPARTMENT_CODE').AsString :=
//            EmployeeIDCard.DepartmentCode;
          qryFirstLastScans.Open;
          if not qryFirstLastScans.Eof then
          begin
// TESTING
{WDebugLog('Emp=' + IntToStr(EmployeeIDCard.EmployeeCode) +
     ' Pl='  + EmployeeIDCard.PlantCode +
     ' Sh='  + IntToStr(EmployeeIDCard.ShiftNumber) +
     ' Dept='  + EmployeeIDCard.DepartmentCode +
     ' ShDate=' + DateToStr(qryShiftLoop.FieldByName('SHIFT_DATE').AsDateTime) +
     ' Start='  + DateTimeToStr(StartDate) +
     ' End='  + DateTimeToStr(EndDate) +
     ' Min=' + DateTimeToStr(qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime) +
     ' Max=' + DateTimeToStr(qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime)
    );}
            // if values were not 0
            if not ((qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime = 0) and
               (qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime = 0)) then
            begin
              // Late
              if qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime > StartDate then
              begin
                if not IsAbsent(Trunc(qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime)) then
                begin
                  // if Result <> '' then
                  //   Result := Result + '; ';
// TESTING
//WDebugLog('-> LATE');
                  // Result := Result +
                  AddToEarlyLateList(
                    DateToStr(Trunc(qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime)) + ' ' +
                      SPimsReportLate + ' ' +
                      FormatDateTime('hh:mm', qryFirstLastScans.FieldByName('DATETIME_IN').AsDateTime)
                    );
                end; // if
              end; // if
              // Early
              if qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime < EndDate then
              begin
                if not IsAbsent(Trunc(qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime)) then
                begin
                  // if Result <> '' then
                  //  Result := Result + '; ';
// TESTING
//WDebugLog('-> EARLY');
                  // Result := Result +
                  AddToEarlyLateList(
                    DateToStr(Trunc(qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime)) + ' ' +
                      SPimsReportEarly + ' ' +
                      FormatDateTime('hh:mm', qryFirstLastScans.FieldByName('DATETIME_OUT').AsDateTime)
                     );
                end; // if
              end; // if
            end; // if values were not 0
//            else
//              WDebugLog('Result of min/max scan was 0!');
          end; // if not Eof
        end; // if
        qryShiftLoop.Next;
      end; // while Shift-loop
      qryShiftLoop.Close;
    end; // with
    Result := DetermineResult;
  end; // DetermineLateEarly
begin
  inherited;
  EarlyLateList := TStringList.Create;
  try
    QRLblAbsenceValue.Caption := DetermineAbsence;
    QRLblLateEarlyValue.Caption := DetermineLateEarly;
  finally
    EarlyLateList.Free;
  end;
end;

end.
