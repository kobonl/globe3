(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)  
unit BreakPerDeptFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, CalculateTotalHoursDMT;

type
  TBreakPerDeptF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridTimeColumn;
    dxMasterGridColumn4: TdxDBGridTimeColumn;
    dxMasterGridColumn5: TdxDBGridTimeColumn;
    dxMasterGridColumn6: TdxDBGridTimeColumn;
    dxMasterGridColumn7: TdxDBGridTimeColumn;
    dxMasterGridColumn8: TdxDBGridTimeColumn;
    dxMasterGridColumn9: TdxDBGridTimeColumn;
    dxMasterGridColumn10: TdxDBGridTimeColumn;
    dxMasterGridColumn11: TdxDBGridTimeColumn;
    dxMasterGridColumn12: TdxDBGridTimeColumn;
    dxMasterGridColumn13: TdxDBGridTimeColumn;
    dxMasterGridColumn14: TdxDBGridTimeColumn;
    dxMasterGridColumn15: TdxDBGridTimeColumn;
    dxMasterGridColumn16: TdxDBGridTimeColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    Label1: TLabel;
    dxDBTimeEditST1: TdxDBTimeEdit;
    dxDBTimeEditET1: TdxDBTimeEdit;
    dxDBTimeEditST2: TdxDBTimeEdit;
    dxDBTimeEditET2: TdxDBTimeEdit;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    DBEditTimeBlock: TDBEdit;
    DBEditDesc: TDBEdit;
    DBCheckBoxPayedYN: TDBCheckBox;
    PanelDept: TPanel;
    dxDBGridDept: TdxDBGrid;
    dxDBGridDeptColumn6: TdxDBGridColumn;
    dxDBGridColumn1: TdxDBGridColumn;
    dxDBGridColumn2: TdxDBGridColumn;
    dxDBGridDeptColumn4: TdxDBGridLookupColumn;
    dxDBGridDeptColumn5: TdxDBGridColumn;
    dxDBGridColumn3: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBGridDeptClick(Sender: TObject);
    procedure dxMasterGridClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxDBGridDeptChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure ScrollBarMasterScroll(Sender: TObject;
      ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function BreakPerDeptF: TBreakPerDeptF;

implementation

{$R *.DFM}
uses
  SystemDMT, BreakPerDeptDMT, UPimsMessageRes;

var
  BreakPerDeptF_HND: TBreakPerDeptF;

function BreakPerDeptF: TBreakPerDeptF;
begin
  if (BreakPerDeptF_HND = nil) then
  begin
    BreakPerDeptF_HND := TBreakPerDeptF.Create(Application);
    BreakPerDeptDM.Handle_Grid := BreakPerDeptF_HND.dxDetailGrid;
  end;
  Result := BreakPerDeptF_HND;
end;

procedure TBreakPerDeptF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TBreakPerDeptF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TBreakPerDeptF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  for Index := 1 to 7 do
  begin
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
    dxMasterGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
  end;
  BreakPerDeptDM.SetDepartment;
  BreakPerDeptF_HND.dxDetailGrid.DataSource.DataSet.Close;
  BreakPerDeptF_HND.dxDetailGrid.DataSource.DataSet.Open;
 
  dxMasterGrid.SetFocus;
  
end;

procedure TBreakPerDeptF.FormDestroy(Sender: TObject);
begin
  inherited;
  BreakPerDeptF_HND := nil;
end;

procedure TBreakPerDeptF.dxDBGridDeptClick(Sender: TObject);
begin
  inherited;
  BreakPerDeptDM.SetDepartment;
 
end;

procedure TBreakPerDeptF.dxMasterGridClick(Sender: TObject);
begin
  inherited;
  BreakPerDeptDM.SetDepartment;
end;

procedure TBreakPerDeptF.FormCreate(Sender: TObject);
begin
  BreakPerDeptDM := CreateFormDM(TBreakPerDeptDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) or
    (dxDBGridDept.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := BreakPerDeptDM.DataSourceDetail;
    dxMasterGrid.DataSource := BreakPerDeptDM.DataSourceMaster;
    dxDBGridDept.DataSource := BreakPerDeptDM.DataSourceDept;
  end;
  inherited;
  // 20014289
  dxDBGridDept.BandFont.Color := clWhite;
  dxDBGridDept.BandFont.Style := [fsBold];
end;

procedure TBreakPerDeptF.dxDBGridDeptChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  BreakPerDeptDM.SetDepartment;
end;

procedure TBreakPerDeptF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  BreakPerDeptDM.SetDepartment;
end;

procedure TBreakPerDeptF.ScrollBarMasterScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  dxDBGridDept.SetFocus; 
end;

procedure TBreakPerDeptF.dxBarButtonExportGridClick(Sender: TObject);
begin
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'PLANT_CODE');
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'PLANTLU');
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'DEPARTMENT_CODE');
  CreateExportColumnsDesc(SExportDescDept , 'DEPTLU');
  inherited;

end;

procedure TBreakPerDeptF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
end;

procedure TBreakPerDeptF.FormResize(Sender: TObject);
begin
  inherited;
//CAR 15-10-2003 - on maximize screen give the same height for both grids
  if BreakPerDeptF.ClientHeight > 600 then
    pnlMasterGrid.Height := 200
  else
    pnlMasterGrid.Height := 120;
end;

end.
