inherited WorkSpotPerStationF: TWorkSpotPerStationF
  Left = 313
  Top = 129
  Height = 419
  Caption = 'Workspots per workstation'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Workstations'
          Width = 712
        end>
      KeyField = 'COMPUTER_NAME'
      DataSource = WorkSpotPerStationDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnComputer: TdxDBGridColumn
        Caption = 'Computer name'
        Width = 359
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPUTER_NAME'
      end
      object dxMasterGridColumnLicense: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'License version'
        Width = 353
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LICENSEVERSION'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 283
    Height = 97
    object LabelSequence: TLabel
      Left = 12
      Top = 16
      Width = 47
      Height = 13
      Caption = 'Sequence'
    end
    object Label1: TLabel
      Left = 12
      Top = 44
      Width = 24
      Height = 13
      Caption = 'Plant'
    end
    object LabelWorkspot: TLabel
      Left = 324
      Top = 16
      Width = 46
      Height = 13
      Caption = 'Workspot'
    end
    object lblWorkspotDescription: TLabel
      Left = 480
      Top = 16
      Width = 109
      Height = 13
      Caption = 'lblWorkspotDescription'
    end
    object dxDBSpinEditSequence: TdxDBSpinEdit
      Tag = 1
      Left = 72
      Top = 12
      Width = 69
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'SEQUENCE_NUMBER'
      DataSource = WorkSpotPerStationDM.DataSourceDetail
      StoredValues = 48
    end
    object dxDBLookupEditPlant: TdxDBLookupEdit
      Tag = 1
      Left = 72
      Top = 40
      Width = 241
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'PLANTLU'
      DataSource = WorkSpotPerStationDM.DataSourceDetail
      ReadOnly = False
      OnSelectionChange = dxDBLookupEditPlantSelectionChange
      ListFieldName = 'DESCRIPTION;PLANT_CODE'
      StoredValues = 64
    end
    object ButtonCopy: TButton
      Left = 380
      Top = 68
      Width = 155
      Height = 25
      Caption = '&Copy from other workstation'
      TabOrder = 3
      OnClick = ButtonCopyClick
    end
    object ButtonAdd: TButton
      Left = 540
      Top = 68
      Width = 97
      Height = 25
      Caption = '&Add all workspots'
      TabOrder = 4
      OnClick = ButtonAddClick
    end
    object dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit
      Tag = 1
      Left = 392
      Top = 12
      Width = 81
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      OnEnter = dxDBExtLookupEditWorkspotEnter
      OnExit = dxDBExtLookupEditWorkspotExit
      OnKeyPress = dxDBExtLookupEditWorkspotKeyPress
      DataField = 'WORKSPOT_CODE'
      DataSource = WorkSpotPerStationDM.DataSourceDetail
      ImmediateDropDown = True
      PopupHeight = 200
      PopupMinHeight = 200
      PopupWidth = 271
      OnPopup = dxDBExtLookupEditWorkspotPopup
      DBGridLayout = dxDBGridLayoutListWorkspotItem
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 128
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 124
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 123
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Workspots per workstation'
          Width = 718
        end>
      KeyField = 'SEQUENCE_NUMBER'
      DataSource = WorkSpotPerStationDM.DataSourceDetail
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoMouseScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumnSequence: TdxDBGridSpinColumn
        Alignment = taLeftJustify
        Caption = 'Sequence number'
        DisableEditor = True
        Width = 107
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SEQUENCE_NUMBER'
      end
      object dxDetailGridColumnPlant: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 183
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDetailGridColumnWorkspotCode: TdxDBGridColumn
        Caption = 'Workspot code'
        DisableEditor = True
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxDetailGridColumnWorkspotDescr: TdxDBGridColumn
        Caption = 'Workspot description'
        DisableEditor = True
        Width = 268
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOTLU'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  object dxDBGridLayoutListWorkspot: TdxDBGridLayoutList
    Left = 488
    Top = 107
    object dxDBGridLayoutListWorkspotItem: TdxDBGridLayout
      Data = {
        30020000545046301054647844424772696457726170706572000542616E6473
        0E010743617074696F6E0608576F726B73706F7400000D44656661756C744C61
        796F7574091348656164657250616E656C526F77436F756E740201084B657946
        69656C64060D574F524B53504F545F434F44450D53756D6D61727947726F7570
        730E001053756D6D617279536570617261746F7206022C200A44617461536F75
        7263650727576F726B53706F7450657253746174696F6E444D2E44617461536F
        75726365576F726B73706F74094F7074696F6E7344420B106564676F43616E63
        656C4F6E457869740D6564676F43616E44656C6574650D6564676F43616E496E
        73657274116564676F43616E4E617669676174696F6E116564676F436F6E6669
        726D44656C657465126564676F4C6F6164416C6C5265636F726473106564676F
        557365426F6F6B6D61726B730000135464784442477269644D61736B436F6C75
        6D6E0D574F524B53504F545F434F44450743617074696F6E0604436F64650653
        6F7274656407046373557005576964746802450942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060D574F524B53504F545F43
        4F44450000135464784442477269644D61736B436F6C756D6E0B444553435249
        5054494F4E0743617074696F6E060B4465736372697074696F6E055769647468
        03B8000942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060B4445534352495054494F4E000000}
    end
  end
end
