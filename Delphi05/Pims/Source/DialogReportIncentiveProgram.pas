unit DialogReportIncentiveProgram;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxCntner, dxEditor, dxExEdtr, dxExGrEd,
  dxExELib, Dblup1a, ExtCtrls;

type
  TDialogReportBaseF1 = class(TDialogReportBaseF)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportBaseF1: TDialogReportBaseF1;

implementation

{$R *.DFM}

end.
