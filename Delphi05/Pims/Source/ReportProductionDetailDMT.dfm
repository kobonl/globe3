inherited ReportProductionDetailDM: TReportProductionDetailDM
  OldCreateOrder = True
  Left = 209
  Top = 132
  Height = 581
  Width = 817
  inherited qryPlantMinMax: TQuery
    Left = 624
    Top = 128
  end
  inherited qryTeamMinMax: TQuery
    Left = 624
    Top = 184
  end
  inherited qryEmplMinMax: TQuery
    Left = 624
    Top = 248
  end
  inherited qryBUMinMax: TQuery
    Left = 624
    Top = 312
  end
  inherited qryWeekDelete: TQuery
    Left = 504
    Top = 64
  end
  inherited qryWeekInsert: TQuery
    Left = 504
    Top = 120
  end
  inherited qryWorkspotMinMax: TQuery
    Left = 624
    Top = 8
  end
  inherited qryDepartmentMinMax: TQuery
    Left = 624
    Top = 64
  end
  object QueryProduction: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProductionFilterRecord
    SQL.Strings = (
      'SELECT'
      '  P.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  PQ.SHIFT_NUMBER,'
      
        '  (SELECT S.DESCRIPTION FROM SHIFT S WHERE S.PLANT_CODE = P.PLAN' +
        'T_CODE'
      '   AND S.SHIFT_NUMBER = PQ.SHIFT_NUMBER) SHIFTDESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  J.JOB_CODE, J.DESCRIPTION AS JOBCODEDESCRIPTION,'
      '  PQ.QUANTITY AS PIECES'
      'FROM '
      '  PRODUCTIONQUANTITY PQ,'
      '  PLANT P,'
      '  JOBCODE J,'
      '  WORKSPOT W,'
      '  BUSINESSUNIT B,'
      '  DEPARTMENT D,'
      '  PRODHOURPEREMPLOYEE PE'
      'WHERE'
      '  PQ.QUANTITY <> 0 AND'
      '  PQ.PLANT_CODE >= :PLANTFROM AND'
      '  PQ.PLANT_CODE <= :PLANTTO AND'
      '  B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  B.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND'
      '  PQ.WORKSPOT_CODE <= :WORKSPOTTO AND'
      '  PQ.START_DATE >= :DATEFROM AND'
      '  PQ.START_DATE <= :DATETO AND'
      '  PQ.SHIFT_DATE >= :SHIFTDATEFROM AND'
      '  PQ.SHIFT_DATE <= :SHIFTDATETO AND'
      '  PQ.SHIFT_DATE >= :PHEDATEFROM AND'
      '  PQ.SHIFT_DATE <= :PHEDATETO AND'
      '  PQ.PLANT_CODE = J.PLANT_CODE AND'
      '  PQ.PLANT_CODE = P.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = J.JOB_CODE AND'
      '  J.PLANT_CODE = B.PLANT_CODE AND'
      '  J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE AND'
      '  PQ.PLANT_CODE = W.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  J.BUSINESSUNIT_CODE = D.BUSINESSUNIT_CODE AND'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  PQ.PLANT_CODE = PE.PLANT_CODE AND'
      '  PQ.SHIFT_NUMBER = PE.SHIFT_NUMBER AND'
      '  PQ.WORKSPOT_CODE = PE.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = PE.JOB_CODE'
      'ORDER BY'
      '  P.PLANT_CODE, P.DESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION,'
      '  PQ.SHIFT_NUMBER,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION'
      ''
      '')
    Left = 40
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFTDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'SHIFTDATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'PHEDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'PHEDATETO'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProduction: TDataSource
    DataSet = QueryProduction
    Left = 144
    Top = 16
  end
  object qryRevenue: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SUM(REVENUE_AMOUNT) AS REVENUESUM'
      'FROM'
      '  REVENUE'
      'WHERE'
      '  BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE AND'
      '  REVENUE_DATE >= :DATEFROM AND'
      '  REVENUE_DATE <= :DATETO')
    Left = 40
    Top = 272
    ParamData = <
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object cdsEmpPieces: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 
          'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBE' +
          'R;SHIFT_NUMBER'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 48
    Top = 360
    object cdsEmpPiecesPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsEmpPiecesWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsEmpPiecesJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object cdsEmpPiecesDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object cdsEmpPiecesSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsEmpPiecesEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmpPiecesEMPPIECES: TFloatField
      FieldName = 'EMPPIECES'
    end
    object cdsEmpPiecesEMPTOTALMINUTES: TIntegerField
      FieldName = 'EMPTOTALMINUTES'
    end
    object cdsEmpPiecesEMPTOTALBREAKMINUTES: TIntegerField
      FieldName = 'EMPTOTALBREAKMINUTES'
    end
    object cdsEmpPiecesFOUND: TIntegerField
      FieldName = 'FOUND'
    end
    object cdsEmpPiecesBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object cdsEmpPiecesPRODMINS: TIntegerField
      FieldName = 'PRODMINS'
    end
    object cdsEmpPiecesOVERTIMEMINS: TIntegerField
      FieldName = 'OVERTIMEMINS'
    end
    object cdsEmpPiecesBONUS_PERCENTAGE: TFloatField
      FieldName = 'BONUS_PERCENTAGE'
    end
    object cdsEmpPiecesHOURLY_WAGE: TFloatField
      FieldName = 'HOURLY_WAGE'
    end
    object cdsEmpPiecesBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
    end
    object cdsEmpPiecesIGNOREQUANTITIESINREPORTS_YN: TStringField
      FieldName = 'IGNOREQUANTITIESINREPORTS_YN'
      Size = 1
    end
    object cdsEmpPiecesSHIFT_NUMBER_FILTER: TIntegerField
      FieldName = 'SHIFT_NUMBER_FILTER'
    end
    object cdsEmpPiecesBREAKMINS: TIntegerField
      FieldName = 'BREAKMINS'
    end
  end
  object qryEmpl: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  E.DEPARTMENT_CODE, E.CUT_OF_TIME_YN,'
      '  P.INSCAN_MARGIN_EARLY, P.INSCAN_MARGIN_LATE,'
      '  P.OUTSCAN_MARGIN_EARLY, P.OUTSCAN_MARGIN_LATE,'
      '  E.CONTRACTGROUP_CODE'
      'FROM'
      '  EMPLOYEE E, PLANT P'
      'WHERE'
      '  E.PLANT_CODE = P.PLANT_CODE'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' '
      ' ')
    Left = 48
    Top = 432
  end
  object dspEmpl: TDataSetProvider
    DataSet = qryEmpl
    Constraints = True
    Left = 120
    Top = 432
  end
  object cdsEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEmpl'
    Left = 192
    Top = 432
    object cdsEmplEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmplDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 40
    end
    object cdsEmplDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object cdsEmplCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object cdsEmplINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object cdsEmplINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object cdsEmplOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object cdsEmplOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
    object cdsEmplCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Size = 6
    end
  end
  object qryJob: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MIN(DESCRIPTION) AS DESCRIPTION'
      'FROM'
      '  JOBCODE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE')
    Left = 40
    Top = 216
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryMain: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 40
    Top = 72
  end
  object qryPHEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 40
    Top = 128
  end
  object qryPHE: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 96
    Top = 128
  end
  object cdsEmpPiecesDate: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 
          'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBE' +
          'R;SHIFT_NUMBER;START_DATE'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 184
    Top = 360
    object cdsEmpPiecesDatePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsEmpPiecesDateWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsEmpPiecesDateJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object cdsEmpPiecesDateDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object cdsEmpPiecesDateSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsEmpPiecesDateEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmpPiecesDateEMPPIECES: TFloatField
      FieldName = 'EMPPIECES'
    end
    object cdsEmpPiecesDateEMPTOTALMINUTES: TIntegerField
      FieldName = 'EMPTOTALMINUTES'
    end
    object cdsEmpPiecesDateEMPTOTALBREAKMINUTES: TIntegerField
      FieldName = 'EMPTOTALBREAKMINUTES'
    end
    object cdsEmpPiecesDateFOUND: TIntegerField
      FieldName = 'FOUND'
    end
    object cdsEmpPiecesDateBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object cdsEmpPiecesDatePRODMINS: TIntegerField
      FieldName = 'PRODMINS'
    end
    object cdsEmpPiecesDateOVERTIMEMINS: TIntegerField
      FieldName = 'OVERTIMEMINS'
    end
    object cdsEmpPiecesDateBONUS_PERCENTAGE: TFloatField
      FieldName = 'BONUS_PERCENTAGE'
    end
    object cdsEmpPiecesDateHOURLY_WAGE: TFloatField
      FieldName = 'HOURLY_WAGE'
    end
    object cdsEmpPiecesDateBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
    end
    object cdsEmpPiecesDateSTART_DATE: TDateField
      FieldName = 'START_DATE'
    end
    object cdsEmpPiecesDateIGNOREQUANTITIESINREPORTS_YN: TStringField
      FieldName = 'IGNOREQUANTITIESINREPORTS_YN'
      Size = 1
    end
    object cdsEmpPiecesDateSHIFT_NUMBER_FILTER: TIntegerField
      FieldName = 'SHIFT_NUMBER_FILTER'
    end
    object cdsEmpPiecesDateBREAKMINS: TIntegerField
      FieldName = 'BREAKMINS'
    end
  end
  object qryDeleteShiftPeriod: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM SHIFTPERIOD'
      'WHERE COMPUTERNAME = :COMPUTERNAME')
    Left = 280
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end>
  end
  object qryInsertShiftPeriod: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO SHIFTPERIOD'
      '(COMPUTERNAME, PLANT_CODE, SHIFT_NUMBER, STARTTIME, ENDTIME)'
      'VALUES'
      
        '(:COMPUTERNAME, :PLANT_CODE, :SHIFT_NUMBER, :STARTTIME, :ENDTIME' +
        ')'
      ''
      ' ')
    Left = 392
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTTIME'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'ENDTIME'
        ParamType = ptUnknown
      end>
  end
  object qryProdSummary: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 280
    Top = 80
  end
end
