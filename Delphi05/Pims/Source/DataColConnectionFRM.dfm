inherited DataColConnectionF: TDataColConnectionF
  Left = 256
  Top = 135
  Width = 653
  Height = 515
  Caption = 'Data Collection Connection'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 637
    Height = 163
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 159
      Width = 635
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 635
      Height = 158
      Bands = <
        item
          Caption = 'Workstation'
        end>
      KeyField = 'COMPUTER_NAME'
      DataSource = DataColConnectionDM.DataSourceMaster
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridColumn1: TdxDBGridColumn
        Caption = 'COMPUTER NAME'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPUTER_NAME'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 337
    Width = 637
    Height = 140
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 329
      Height = 138
      Align = alLeft
      Caption = 'Data Collection Connection'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 71
        Width = 44
        Height = 13
        Caption = 'Com port'
      end
      object Label6: TLabel
        Left = 8
        Top = 20
        Width = 27
        Height = 13
        Caption = 'Name'
      end
      object Label7: TLabel
        Left = 8
        Top = 96
        Width = 39
        Height = 13
        Caption = 'Counter'
      end
      object Label4: TLabel
        Left = 161
        Top = 70
        Width = 39
        Height = 13
        Caption = 'Address'
      end
      object Label3: TLabel
        Left = 8
        Top = 45
        Width = 61
        Height = 13
        Caption = 'Serial Device'
      end
      object DBEditComputerName: TDBEdit
        Left = 83
        Top = 16
        Width = 231
        Height = 19
        Ctl3D = False
        DataField = 'COMPUTER_NAME'
        DataSource = DataColConnectionDM.DataSourceMaster
        Enabled = False
        ParentCtl3D = False
        TabOrder = 0
      end
      object dxDBSpinEditCOMPORT: TdxDBSpinEdit
        Tag = 1
        Left = 83
        Top = 65
        Width = 73
        TabOrder = 2
        DataSource = DataColConnectionDM.DataSourceDetail
        MaxValue = 255
        MinValue = 1
        StoredValues = 48
      end
      object DBRadioGroup1: TDBRadioGroup
        Tag = 1
        Left = 82
        Top = 84
        Width = 231
        Height = 32
        Columns = 2
        DataField = 'COUNTER'
        DataSource = DataColConnectionDM.DataSourceDetail
        Items.Strings = (
          '1'
          '2')
        TabOrder = 4
        Values.Strings = (
          '1'
          '2')
      end
      object dxDBSpinEditADDRESS: TdxDBSpinEdit
        Tag = 1
        Left = 241
        Top = 65
        Width = 73
        TabOrder = 3
        DataSource = DataColConnectionDM.DataSourceDetail
        MaxValue = 255
        MinValue = 1
        StoredValues = 48
      end
      object DBLookupComboBoxSERIALDEVICE: TDBLookupComboBox
        Tag = 1
        Left = 83
        Top = 40
        Width = 231
        Height = 21
        DataField = 'SERIALDEVICE_CODE'
        DataSource = DataColConnectionDM.DataSourceDetail
        DropDownWidth = 250
        KeyField = 'SERIALDEVICE_CODE'
        ListField = 'NAME;SERIALDEVICE_CODE'
        ListSource = DataColConnectionDM.DataSourceSerialDevice
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 330
      Top = 1
      Width = 306
      Height = 138
      Align = alClient
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 20
        Width = 73
        Height = 13
        Caption = 'Interface Code'
      end
      object DBEditINTERFACE_CODE: TDBEdit
        Tag = 1
        Left = 115
        Top = 16
        Width = 118
        Height = 19
        Ctl3D = False
        DataField = 'INTERFACE_CODE'
        DataSource = DataColConnectionDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 0
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 40
        Width = 302
        Height = 96
        Align = alBottom
        Caption = 'Socket Fields'
        TabOrder = 1
        object Label5: TLabel
          Left = 8
          Top = 18
          Width = 22
          Height = 13
          Caption = 'Host'
        end
        object Label8: TLabel
          Left = 8
          Top = 40
          Width = 20
          Height = 13
          Caption = 'Port'
        end
        object Label9: TLabel
          Left = 8
          Top = 64
          Width = 54
          Height = 13
          Caption = 'Checkdigits'
        end
        object Label10: TLabel
          Left = 240
          Top = 64
          Width = 28
          Height = 13
          Caption = '10-40'
        end
        object DBEditIPADDRESS: TDBEdit
          Left = 115
          Top = 15
          Width = 118
          Height = 19
          Ctl3D = False
          DataField = 'IPADDRESS'
          DataSource = DataColConnectionDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 0
        end
        object DBEditPORT: TDBEdit
          Left = 115
          Top = 39
          Width = 118
          Height = 19
          Ctl3D = False
          DataField = 'PORT'
          DataSource = DataColConnectionDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 1
        end
        object DBEditCheckDigits: TDBEdit
          Left = 115
          Top = 63
          Width = 118
          Height = 19
          Ctl3D = False
          DataField = 'CHECKDIGITS'
          DataSource = DataColConnectionDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 2
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 189
    Width = 637
    Height = 148
    Align = alBottom
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 144
      Width = 635
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 635
      Height = 143
      Bands = <
        item
          Caption = 'Data Collection Connection'
        end>
      KeyField = 'COMPORT'
      DataSource = DataColConnectionDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnSERIALDEVICE: TdxDBGridColumn
        Caption = 'SERIALDEVICE'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SERIALDEVICE_DESCRIPTIONLU'
      end
      object dxDetailGridColumnCOMPORT: TdxDBGridSpinColumn
        Caption = 'COM PORT'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COMPORT'
        MinValue = 1
        MaxValue = 255
      end
      object dxDetailGridColumnADDRESS: TdxDBGridSpinColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
      object dxDetailGridColumnCOUNTER: TdxDBGridSpinColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER'
        MinValue = 1
        MaxValue = 2
      end
      object dxDetailGridColumnINTERFACE_CODE: TdxDBGridColumn
        Caption = 'Interfacecode'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'INTERFACE_CODE'
      end
      object dxDetailGridColumnIPADDRESS: TdxDBGridColumn
        Caption = 'HOST'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IPADDRESS'
      end
      object dxDetailGridColumnPORT: TdxDBGridColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PORT'
      end
      object dxDetailGridColumnCHECKDIGITS: TdxDBGridColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CHECKDIGITS'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonCopy: TdxBarButton
      Action = CopyAct
    end
    inherited dxBarButtonPaste: TdxBarButton
      Action = PasteAct
    end
  end
  inherited StandardMenuActionList: TActionList
    object CopyAct: TAction
      Caption = '&Copy'
      ShortCut = 16451
      OnExecute = CopyActExecute
    end
    object PasteAct: TAction
      Caption = '&Paste'
      ShortCut = 16470
      OnExecute = PasteActExecute
    end
  end
end
