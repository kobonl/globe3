(*
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  MRA:4-MAY-2011 RV092.9. Bugfix.
  - When called from 'employee regs', it does not show correct shifts, when
    it is not the first plant to show (based on employee-plant/shift).
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:10-DEC-2018 GLOB3-173
  - Shift selection changes when selection change is made for teams
  - When first a shift was selected and then a different team is selected,
    it changes the shift that was selected to the first one again.
    Reason: It fills ths comboxes of plants and shifts again.
  - To prevent is, remember the last selected plant + shift and assign these
    after it filled the comboxes again.
  MRA:27-MAY-2019 GLOB3-309
  - Distinguish between standard and day planning
  MRA:3-JUN-2019 GLOB3-284
  - Related to this issue: Show cancel button when 'embedded' is used for
    options 2 and 3, so you can go back to main dialog.
  MRA:29-JUL-2019 GLOB3-284
  - Week start at Wednesday
  MRA:2-AUG-2019 GLOB3-284
  - Week start at Wendesday
  - It should convert the contents of table STANDARDEMLPOYEEPLANNING
    because this has a DAY_OF_WEEK-field that is not correct anymore after
    conversion from Sunday to Wednesday.
*)
unit DialogStaffPlanningFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Dblup1a, Registry, SystemDMT, jpeg;

type
  TDialogStaffPlanningF = class(TDialogBaseF)
    RadioGroupSelection: TRadioGroup;
    RadioGroupEmpl: TRadioGroup;
    RadioGroupWKSort: TRadioGroup;
    GroupBoxSelection: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    cmbPlusShift: TComboBoxPlus;
    cmbPlusPlant: TComboBoxPlus;
    CheckBoxAllTeam: TCheckBox;
    GroupBoxReadStdPln: TGroupBox;
    LabelFillDay: TLabel;
    DateFrom: TDateTimePicker;
    Label1: TLabel;
    DateTo: TDateTimePicker;
    LblFromPlant: TLabel;
    cmbPlusPlantFrom: TComboBoxPlus;
    LblToPlant: TLabel;
    cmbPlusPlantTo: TComboBoxPlus;
    LabelReadPast: TLabel;
    DateSourceFrom: TDateTimePicker;
    LabelTo: TLabel;
    DateSourceTo: TDateTimePicker;
    Label12: TLabel;
    Label13: TLabel;
    cmbPlusTeamFrom: TComboBoxPlus;
    Label14: TLabel;
    cmbPlusTeamTo: TComboBoxPlus;
    Label2: TLabel;
    Label3: TLabel;
    LabelTeamReadSTD: TLabel;
    cmbPlusTeamSFrom: TComboBoxPlus;
    Label7: TLabel;
    cmbPlusTeamSTo: TComboBoxPlus;
    CheckBoxRDPAllTeam: TCheckBox;
    Label4: TLabel;
    Label8: TLabel;
    CheckBoxOnlyAvailEmp: TCheckBox;
    CheckBoxWK: TCheckBox;
    GroupBoxDate: TGroupBox;
    LabelDate: TLabel;
    LabelDay: TLabel;
    DateSelection: TDateTimePicker;
    DaySelection: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CheckBoxAllTeam1Click(Sender: TObject);
    procedure RadioGroupSelectionClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure DateSourceFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure CheckBoxRDPAllTeamClick(Sender: TObject);
    procedure cmbPlusTeamFromCloseUp(Sender: TObject);
    procedure cmbPlusTeamToCloseUp(Sender: TObject);
    procedure cmbPlusPlantCloseUp(Sender: TObject);
    procedure cmbPlusPlantFromCloseUp(Sender: TObject);
    procedure cmbPlusPlantToCloseUp(Sender: TObject);
    procedure cmbPlusTeamSFromCloseUp(Sender: TObject);
    procedure cmbPlusTeamSToCloseUp(Sender: TObject);
    procedure dxBarButtonSettingsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cmbPlusShiftCloseUp(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
   // Duration: TDateTime;
    FEdit_Staff_Planning: String;
    FMyShift: String;
    FMyPlant: String;
    FMyDate: TDateTime;
    FMyTeam: String;
    FormInit: Boolean;
    FLastSelectedShift: String;
    FLastSelectedPlant: String;
    procedure FillPlants;
    procedure FillShifts;
    procedure SetVisibleGroupBox(GrpVisible: Boolean);
    function ReadRegistryShowLevel: Boolean;
    function ReadRegistryShowLevelC: Boolean;
    procedure WriteRegistryShowLevel(const Value: Boolean);
    procedure WriteRegistryShowLevelC(const Value: Boolean);
  public
    { Public declarations }
    //CAR User rights: 27-10-2003
    property Edit_Staff_Planning: String read FEdit_Staff_Planning
      write FEdit_Staff_Planning;
    property MyPlant: String read FMyPlant write FMyPlant;
    property MyShift: String read FMyShift write FMyShift;
    property MyDate: TDateTime read FMyDate write FMyDate;
    property MyTeam: String read FMyTeam write FMyTeam;
    property LastSelectedPlant: String read FLastSelectedPlant
      write FLastSelectedPlant; // GLOB3-173
    property LastSelectedShift: String read FLastSelectedShift
      write FLastSelectedShift; // GLOB3-173
  end;

var
  DialogStaffPlanningF: TDialogStaffPlanningF;

// RV089.1.
function DialogStaffPlanningForm: TDialogStaffPlanningF;

implementation

{$R *.DFM}
uses  ListProcsFRM, DialogStaffPlanningDMT, StaffPlanningFRM,
  StaffPlanningEMPDMT, StaffPlanningOCIDMT, StaffPlanningUnit, UPimsMessageRes,
  UPimsConst, SideMenuUnitMaintenanceFRM, DialogSettingsFRM;

// RV089.1.
var
  DialogStaffPlanningF_HND: TDialogStaffPlanningF;

// RV089.1.
function DialogStaffPlanningForm: TDialogStaffPlanningF;
begin
  if (DialogStaffPlanningF_HND = nil) then
  begin
    DialogStaffPlanningF_HND := TDialogStaffPlanningF.Create(Application);
    with DialogStaffPlanningF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
//      btnCancel.Enabled := False; // GLOB3-284
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogStaffPlanningF_HND;
end;

// RV089.1.
procedure TDialogStaffPlanningF.FormDestroy(Sender: TObject);
begin
  inherited;
  StaffPlanningEMPDM.Free;
  DialogStaffPlanningDM.Free;
  StaffPlanningOCIDM.Free;
  FWKList.Free;
  FDeptList.Free;
  FWKDescList.Free;
  FABSList.Free;
  FABSDescList.Free;
  if (DialogStaffPlanningF_HND <> nil) then
  begin
    DialogStaffPlanningF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogStaffPlanningF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogStaffPlanningF.FormCreate(Sender: TObject);
begin
  inherited;
  FormInit := True;
  //CAR 550278 - depends on the resolution
  // 50 is height of header top bands
  //17 is height of one row
  // 17 * 9 is height of second grid
  //ScrollBarGrid has height 20
  EMPLOYEESHOW := ((Screen.Height - 50 - (17*2) - 21) div 17);
  //end CAR: 5-6-2004
  FWKList := TStringList.Create;
  FDeptList := TStringList.Create;
  FWKDescList := TStringList.Create;
  FABSList := TStringList.Create;
  FABSDescList := TStringList.Create;
  DialogStaffPlanningDM := TDialogStaffPlanningDM.Create(Application);
  StaffPlanningEMPDM := TStaffPlanningEMPDM.Create(Application);
  StaffPlanningOCIDM := TStaffPlanningOCIDM.Create(Application);
  StaffPlanningOCIDM.ShowLevel := ReadRegistryShowLevel;
  StaffPlanningOCIDM.ShowLevelC := ReadRegistryShowLevelC;
  MyDate := 0;
  MyPlant := '';
  MyShift := '';
  MyTeam := '';
  LastSelectedPlant := '';
  LastSelectedShift := '';
end;

procedure TDialogStaffPlanningF.FormShow(Sender: TObject);
begin
  inherited;
  RadioGroupSelection.ItemIndex := 0;
  // MR:26-09-2005 Order 550412
  // FormShow is called more than once, use a boolean to
  // prevent the initialisation of all settings/user-selections,
  // when it is called after the first time (dialog stays open).
  if not FormInit then
    Exit;

  //CAR 28 - 7-2003 - speed changes: Now function is a select replaced it
  // with a variable
  FCurrentDate := Now;
  //
  //CAR 27-10-2003 - User rights
  if Edit_Staff_Planning = ITEM_VISIBIL then
  begin
    RadioGroupSelection.Items.Delete(RadioGroupSelection.Items.Count-1);
    RadioGroupSelection.Items.Delete(RadioGroupSelection.Items.Count-1);
  end;
  //
  cmbPlusTeamFrom.ShowSpeedButton := False;
  cmbPlusTeamFrom.ShowSpeedButton := True;
  cmbPlusTeamTo.ShowSpeedButton := False;
  cmbPlusTeamTo.ShowSpeedButton := True;

  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;
  cmbPlusShift.ShowSpeedButton := False;
  cmbPlusShift.ShowSpeedButton := True;

  cmbPlusPlantFrom.ShowSpeedButton := False;
  cmbPlusPlantFrom.ShowSpeedButton := True;
  cmbPlusPlantTo.ShowSpeedButton := False;
  cmbPlusPlantTo.ShowSpeedButton := True;

  cmbPlusTeamSFrom.ShowSpeedButton := False;
  cmbPlusTeamSFrom.ShowSpeedButton := True;
  cmbPlusTeamSTo.ShowSpeedButton := False;
  cmbPlusTeamSTo.ShowSpeedButton := True;


  ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam,
    'TEAM_CODE', True, cmbPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam,
    'TEAM_CODE', False, cmbPlusTeamTo);
  FillPlants;

  // MR:09-12-2003 If properties are set, then change to a specific Plant,
  //               Date and Shift
  if MyDate <> 0 then
    FCurrentDate := MyDate;
  if MyShift <> '' then
    cmbPlusShift.DisplayValue := MyShift;
  if MyPlant <> '' then
  begin
    cmbPlusPlant.DisplayValue := MyPlant;
    // RV092.9. Be sure the shifts are filled again, because the plant can
    //          be different.
    FillShifts;
    // RV092.9. Also do this again.
    if MyShift <> '' then
      cmbPlusShift.DisplayValue := MyShift;
  end;
  if MyTeam <> '' then
  begin
    cmbPlusTeamFrom.DisplayValue := MyTeam;
    cmbPlusTeamTo.DisplayValue := MyTeam;
  end;
  DateSelection.DateTime := FCurrentDate;
  RadioGroupEmpl.ItemIndex := 0;
  RadioGroupWKSort.ItemIndex := 0;
  DateFrom.Date := FCurrentDate;
  DateTo.Date := FCurrentDate;
  DateSourceFrom.Date := FCurrentDate - 7;
  DateSourceTo.Date := FCurrentDate - 7;
  CheckBoxOnlyAvailEmp.Checked := True;
  CheckBoxWK.Checked := True;
  // MR:26-09-2005
  FormInit := False;
end;

procedure TDialogStaffPlanningF.FillPlants;
begin
  // MR:14-03-2007 Order 550427. Filter plants depending on team-selection.
  if CheckBoxAllTeam.Checked then
    ListProcsF.FillComboBoxPlant(
      DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, True, cmbPlusPlant)
  else
  begin
    if cmbPlusTeamFrom.DisplayValue = cmbPlusTeamTo.DisplayValue then
    begin
      DialogStaffPlanningDM.QueryPlantTeam.Close;
      DialogStaffPlanningDM.QueryPlantTeam.ParamByName('TEAM_CODE').AsString :=
        GetStrValue(cmbPlusTeamFrom.Value);
      DialogStaffPlanningDM.QueryPlantTeam.Open;
      if DialogStaffPlanningDM.QueryPlantTeam.IsEmpty then
         ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant},
           True, cmbPlusPlant)
      else
        ListProcsF.FillComboBox(DialogStaffPlanningDM.QueryPlantTeam,
          cmbPlusPlant, GetStrValue(cmbPlusTeamFrom.Value), '',
            True, 'TEAM_CODE','', 'PLANT_CODE', 'DESCRIPTION');
    end
    else
      ListProcsF.FillComboBoxPlant(
        DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, True, cmbPlusPlant);
  end;
  FillShifts;
  // MR:14-03-2007 Order 550427. Old method, always all plants are shown.
{
  ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.ClientDataSetPlant,
    True, cmbPlusPlant);
  FillShifts;
}
end;

procedure TDialogStaffPlanningF.FillShifts;
begin
  DialogStaffPlanningDM.ClientDataSetShift.Filter := 'PLANT_CODE = ''' +
    DoubleQuote(GetStrValue(cmbPlusPlant.Value)) +  '''';
  ListProcsF.FillComboBox(DialogStaffPlanningDM.ClientDataSetShift,
    CmbPlusShift, '','',True,'','','SHIFT_NUMBER','DESCRIPTION');
   //Car 2-4-2004 - set property in order to be numerical sorted
  cmbPlusShift.IsSorted := True;
  if (LastSelectedPlant = cmbPlusPlant.DisplayValue) and
    (LastSelectedShift <> '') then
  begin
    if DialogStaffPlanningDM.ClientDataSetShift.Locate('SHIFT_NUMBER',
      GetIntValue(LastSelectedShift), []) then
      cmbPlusShift.DisplayValue := LastSelectedShift
    else
      LastSelectedShift := '';
  end;
end;

procedure TDialogStaffPlanningF.CheckBoxAllTeam1Click(Sender: TObject);
begin
  inherited;
  if CheckBoxAllTeam.Checked then
  begin
    cmbPlusTeamFrom.Visible := False;
    cmbPlusTeamTo.Visible := False;
  end
  else
  begin
    cmbPlusTeamFrom.Visible := True;
    cmbPlusTeamTo.Visible := True;
  end;
  FillPlants;
end;

procedure TDialogStaffPlanningF.SetVisibleGroupBox(GrpVisible: Boolean);
begin
  GroupBoxSelection.Visible := GrpVisible;
  GroupBoxDate.Visible := GrpVisible;
  RadioGroupEmpl.Visible := GrpVisible;
  RadioGroupWKSort.Visible := GrpVisible;
  GroupBoxReadStdPln.Visible := Not GrpVisible;
end;

procedure TDialogStaffPlanningF.RadioGroupSelectionClick(Sender: TObject);
var
 i: Integer;
begin
  inherited;
  SetVisibleGroupBox(True);
  btnCancel.ModalResult := mrCancel;
  case RadioGroupSelection.ItemIndex of
    0: // Day Planning
    begin
      // RV089.1. // GLOB3-284
      if (DialogStaffPlanningF_HND <> nil) then
      begin
        btnCancel.Visible := False;
        btnCancel.Enabled := False;
      end;
      LabelDate.Visible := True;
      DateSelection.Visible := True;
      DaySelection.Visible := False;
      LabelDay.Visible := False;
    end;
    1: // Standard Planning
    begin
      // RV089.1. // GLOB3-284
      if (DialogStaffPlanningF_HND <> nil) then
      begin
        btnCancel.Visible := False;
        btnCancel.Enabled := False;
      end;
      DaySelection.Clear;
      for i:= 1 to 7 do
        DaySelection.Items.Add(SystemDM.GetDayWDescription(i));
      // MR:26-09-2005 Order 550412
//      i := ListProcsF.DayWStartOnFromDate(Now);
      i := ListProcsF.DayWStartOnFromDate(DateSelection.Date);
      DaySelection.ItemIndex := i - 1;
      LabelDate.Visible := False;
      DateSelection.Visible := False;
      DaySelection.Visible := True;
      LabelDay.Visible := True;
    end;
    2: // Read Standard Planning into Day Planning
    begin
      // RV089.1. // GLOB3-284
//      if (DialogStaffPlanningF_HND <> nil) then
      btnCancel.Visible := True;
      btnCancel.Enabled := True;
      btnCancel.ModalResult := mrNone;
      SetVisibleGroupBox(False);
      LabelReadPast.Visible := False;
      DateSourceFrom.Visible := False;
      DateSourceTo.Visible := False;
      LabelTo.Visible:= False;
      ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, True,
        cmbPlusPlantFrom);
      ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, False,
        cmbPlusPlantTo);
      ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam, 'TEAM_CODE',
        True, cmbPlusTeamSFrom);
      ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam, 'TEAM_CODE',
        False, cmbPlusTeamSTo);
      // MR:26-09-2005 Order 550412
      cmbPlusPlantFrom.DisplayValue := cmbPlusPlant.DisplayValue;
      cmbPlusPlantTo.DisplayValue := cmbPlusPlant.DisplayValue;
      cmbPlusTeamSFrom.DisplayValue := cmbPlusTeamFrom.DisplayValue;
      cmbPlusTeamSTo.DisplayValue := cmbPlusTeamTo.DisplayValue;
      DateFrom.DateTime := DateSelection.Date;
      DateTo.DateTime := DateSelection.Date;
    end;
    3: // Read Past into Day Planning
    begin
      // RV089.1. // GLOB3-284
//      if (DialogStaffPlanningF_HND <> nil) then
      btnCancel.Visible := True;
      btnCancel.Enabled := True;
      btnCancel.ModalResult := mrNone;
      SetVisibleGroupBox(False);
      ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, True,
        cmbPlusPlantFrom);
      ListProcsF.FillComboBoxPlant(DialogStaffPlanningDM.QueryPlant {ClientDataSetPlant}, False,
        cmbPlusPlantTo);
      ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam, 'TEAM_CODE',
        True, cmbPlusTeamSFrom);
      ListProcsF.FillComboBoxMaster(DialogStaffPlanningDM.ClientDataSetTeam, 'TEAM_CODE',
        False, cmbPlusTeamSTo);
      LabelReadPast.Visible := True;
      DateSourceFrom.Visible := True;
      DateSourceTo.Visible := True;
      LabelTo.Visible:= True;
      // MR:26-09-2005 Order 550412
      cmbPlusPlantFrom.DisplayValue := cmbPlusPlant.DisplayValue;
      cmbPlusPlantTo.DisplayValue := cmbPlusPlant.DisplayValue;
      cmbPlusTeamSFrom.DisplayValue := cmbPlusTeamFrom.DisplayValue;
      cmbPlusTeamSTo.DisplayValue := cmbPlusTeamTo.DisplayValue;
      DateFrom.DateTime := DateSelection.Date;
      DateTo.DateTime := DateSelection.Date;
      DateSourceFrom.Date := DateFrom.DateTime - 7;
      DateSourceTo.Date := DateTo.DateTime - 7;
      CheckBoxRDPAllTeam.Checked := CheckBoxAllTeam.Checked;
    end;
  end;
end;

procedure TDialogStaffPlanningF.btnOkClick(Sender: TObject);
var
  SaveTitle,
  CaptionForm: String;
begin
  inherited;
 // duration := now;
  DateSelection.Date := GetDate(DateSelection.Date);

  if RadioGroupSelection.ItemIndex = 2 then
  begin
    if DateFrom.Date > DateTo.Date then
    begin
       DisplayMessage(SDateFromTo, mtInformation, [mbOK]);
       Exit;
    end;
    if DialogStaffPlanningDM.CheckTableRDP(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value), GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value),CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date) then
    begin
      if (SystemDM.ReadPlanningOnceYN = UNCHECKEDVALUE) then
      begin
        if DisplayMessage(SReadStdPlnIntoDaySelection + ' ' + SContinueYN,
          mtConfirmation, [mbYes, mbNo]) <> mrYes then
         Exit;
      end
      else
      begin
        DisplayMessage(SReadStdPlnIntoDaySelection, mtInformation, [mbOk]);
        Exit;
      end;
    end;
    DialogStaffPlanningDM.ProcessReadSTDPLNIntoDayPLN(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value),
      GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value),CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date);
    DialogStaffPlanningDM.WriteIntoRDP(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value),
      GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value), CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date);
    DisplayMessage(SReadStdPlnIntoDaySelectionFinish, mtInformation, [mbOk]);
    FormShow(Sender);
    FSaveChanges := True;
    Exit;
  end;

  if RadioGroupSelection.ItemIndex = 3 then
  begin
    if DateFrom.Date > DateTo.Date then
    begin
       DisplayMessage(SDateFromTo, mtInformation, [mbOK]);
       Exit;
    end;
    if (GetDate(DateFrom.Date) =GetDate(DateSourceFrom.Date)) and
      (GetDate(DateTo.Date) = GetDate(DateSourceTo.Date)) then
    begin
       DisplayMessage(SDateIntervalValidate, mtInformation, [mbOK]);
       Exit;
    end;
    if GetDate(DateTo.Date - DateFrom.Date) <>
      GetDate(DateSourceTo.Date - DateSourceFrom.Date) then
    begin
       DisplayMessage(SSameRangeDate, mtInformation, [mbOK]);
       Exit;
    end;
    if DialogStaffPlanningDM.CheckTableRDP(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value),GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value), CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date) then
    begin
      if (SystemDM.ReadPlanningOnceYN = UNCHECKEDVALUE) then
      begin
        if DisplayMessage(SReadStdPlnIntoDaySelection + ' ' + SContinueYN,
          mtConfirmation, [mbYes, mbNo]) <> mrYes then
         Exit;
      end
      else
      begin
        DisplayMessage(SReadStdPlnIntoDaySelection, mtInformation, [mbOk]);
        Exit;
      end;
    end;
    DialogStaffPlanningDM.ProcessReadPastIntoDayPLN(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value),GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value), CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date, DateSourceFrom.Date, DateSourceTo.Date);
    DialogStaffPlanningDM.WriteIntoRDP(GetStrValue(cmbPlusPlantFrom.Value),
      GetStrValue(cmbPlusPlantTo.Value),GetStrValue(cmbPlusTeamSFrom.Value),
      GetStrValue(cmbPlusTeamSTo.Value),CheckBoxRDPAllTeam.Checked,
      DateFrom.Date, DateTo.Date);
    DisplayMessage(SReadPastIntoDayPlanFinish, mtInformation, [mbOk]);
    FormShow(Sender);
    FSaveChanges := True;
 
    Exit;
  end;
// for 0 or 1 selection
   if (cmbPlusShift.Value = '') or
      (cmbPlusPlant.Value = '') or
      ((RadioGroupSelection.ItemIndex = 1) and (DaySelection.ItemIndex = -1)) then
    begin
      DisplayMessage(SEmptyValues, mtInformation, [mbOk]);
      Exit;
    end;

    SetParameters(GetStrValue(cmbPlusPlant.Value),
      GetStrValue(cmbPlusTeamFrom.Value),
      GetStrValue(cmbPlusTeamTo.Value),
      GetIntValue(cmbPlusShift.Value), DaySelection.ItemIndex + 1, // GLOB3-284
//      GetIntValue(cmbPlusShift.Value), SystemDM.GetDayIndex(DaySelection.ItemIndex + 1), // GLOB3-284 This does not work!
      RadioGroupWKSort.ItemIndex, RadioGroupEmpl.ItemIndex,
      RadioGroupSelection.ItemIndex, CheckBoxAllTeam.Checked,
      DateSelection.Date, CheckBoxOnlyAvailEmp.Checked,
      CheckBoxWK.Checked);


    DialogStaffPlanningDM.FillWK(FAllTeam, FTeamFrom, FTeamTo, FPlant,
        FWKDEPTSort, FSelection, FDate, FShift, FDay);

    DialogStaffPlanningDM.FillAbsDesc;
    try
      StaffPlanningEMPDM.ShowLevelC := StaffPlanningOCIDM.ShowLevelC;
      StaffPlanningEMPDM.DataCreate;
      if StaffPlanningEMPDM.FResult then
      begin
        try
          StaffPlanningOCIDM.DataCreate;
          //Car 27.2.2004 - because it appears on the task bar -
          // show the name of the new form dialog created
          SaveTitle :=  Application.Title;
        // set caption form
          if RadioGroupSelection.ItemIndex = 0 then
             CaptionForm := SPimsDayPlanning;
          if RadioGroupSelection.ItemIndex = 1 then
            CaptionForm := SPimsSTDPlanning;
          StaffPlanningF.Caption := StaffPlanningF.Caption +
            SPimsTeamSelection ;
          if CheckBoxAllTeam.Checked then
            CaptionForm :=  StaffPlanningF.Caption +
              SPimsTeamSelectionAll
          else
            CaptionForm := StaffPlanningF.Caption +
              SPimsTeam + GetStrValue(cmbPlusTeamFrom.Value) +
              SPimsStaffTo +
              GetStrValue(cmbPlusTeamTo.Value);
          CaptionForm := CaptionForm +
            SPimsPlant + GetStrValue(cmbPlusPlant.Value) +
            SPimsShift  + GetStrValue(cmbPlusShift.Value);
//Car: 550301
// RV089.1. Do not do this!
{
          ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_Hide);
          Application.Title := SideMenuUnitMaintenanceF.Caption;
          ShowWindow(Application.Handle, SW_RESTORE);
}

          StaffPlanningF  := TStaffPlanningF.Create(Application);
//          if SystemDM.EmbedDialogs then
//            StaffPlanningF.FormStyle := fsStayOnTop;
          if RadioGroupSelection.ItemIndex = 0 then // Day Planning
          begin
            StaffPlanningF.dxDBGridPlanning.Bands[0].Caption :=
              DateToStr(DateSelection.Date);
            StaffPlanningF.dxDBGridPlanning.BandColor := clPimsDarkBlue; // GLOB3-309
            StaffPlanningF.dxDBGridPlanning.BandFont.Color := clWhite; // GLOB3-309
          end;
          if RadioGroupSelection.ItemIndex = 1 then // Standard Planning
          begin
            StaffPlanningF.dxDBGridPlanning.Bands[0].Caption :=
              SystemDM.GetDayWDescription(DaySelection.ItemIndex + 1);
            StaffPlanningF.dxDBGridPlanning.BandColor := clDarkRed; // GLOB3-309
            StaffPlanningF.dxDBGridPlanning.BandFont.Color := clWhite;   // GLOB3-309
          end;
          StaffPlanningF.Caption := CaptionForm;
          StaffPlanningF.Edit_Staff_Planning :=  Edit_Staff_Planning;
          StaffPlanningF.ShowModal;

        finally
        // free form
          if Assigned(StaffPlanningF) then
            StaffPlanningF.Free;
          //Car 27.02.2004  hide application from task bar
//          Application.ProcessMessages;
// RV089.1. Do not do this!
{
          ShowWindow(Application.Handle, SW_RESTORE);
          ShowWindow(Application.Handle, SW_HIDE);
          ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);
          SetActiveWindow(DialogStaffPlanningF.Handle);
          Application.Title := SaveTitle;
}
        end;
      end;
    finally
    end;
end;

procedure TDialogStaffPlanningF.DateSourceFromChange(Sender: TObject);
begin
  inherited;
  if Int(DateFrom.Date) <= Int(DateTo.Date) then
     DateSourceTo.Date := DateSourceFrom.Date + (DateTo.Date - DateFrom.Date);
end;

procedure TDialogStaffPlanningF.DateToChange(Sender: TObject);
begin
  inherited;
  if Int(DateTo.Date) >= Int(DateFrom.Date) then
    DateSourceTo.Date := DateSourceFrom.Date + (DateTo.Date - DateFrom.Date);
end;

procedure TDialogStaffPlanningF.DateFromChange(Sender: TObject);
begin
  inherited;
  if Int(DateFrom.Date) > Int(DateTo.Date) then
    DateTo.Date := DateFrom.Date;
  if Int(DateTo.Date) >= Int(DateFrom.Date) then
    DateSourceTo.Date := DateSourceFrom.Date + (DateTo.Date - DateFrom.Date);  
end;

procedure TDialogStaffPlanningF.btnCancelClick(Sender: TObject);
begin
  inherited;
  if (RadioGroupSelection.ItemIndex = 2) or (RadioGroupSelection.ItemIndex = 3) then
    FormShow(Sender);

end;

procedure TDialogStaffPlanningF.CheckBoxRDPAllTeamClick(Sender: TObject);
begin
  inherited;
  if CheckBoxRDPAllTeam.Checked then
  begin
    cmbPlusTeamSFrom.Visible := False;
    cmbPlusTeamSTo.Visible := False;
  end
  else
  begin
    cmbPlusTeamSFrom.Visible := True;
    cmbPlusTeamSTo.Visible := True;
  end;
end;

procedure TDialogStaffPlanningF.cmbPlusTeamFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
    cmbPlusTeamTo.DisplayValue := cmbPlusTeamFrom.DisplayValue;
  FillPlants;
end;

procedure TDialogStaffPlanningF.cmbPlusTeamToCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamFrom.DisplayValue := cmbPlusTeamTo.DisplayValue;
  FillPlants;
end;

procedure TDialogStaffPlanningF.cmbPlusPlantCloseUp(Sender: TObject);
begin
  inherited;
  LastSelectedPlant := cmbPlusPlant.DisplayValue;
  FillShifts;
end;

procedure TDialogStaffPlanningF.cmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusPlantFrom.DisplayValue <> '') and
     (cmbPlusPlantTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusPlantFrom.Value) > GetStrValue(cmbPlusPlantTo.Value) then
    cmbPlusPlantTo.DisplayValue := cmbPlusPlantFrom.DisplayValue;
end;

procedure TDialogStaffPlanningF.cmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusPlantFrom.DisplayValue <> '') and
     (cmbPlusPlantTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusPlantFrom.Value) > GetStrValue(cmbPlusPlantTo.Value) then
      cmbPlusPlantFrom.DisplayValue := cmbPlusPlantTo.DisplayValue;
end;

procedure TDialogStaffPlanningF.cmbPlusTeamSFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamSFrom.DisplayValue <> '') and
     (cmbPlusTeamSTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamSFrom.Value) >
      GetStrValue(cmbPlusTeamSTo.Value) then
    cmbPlusTeamSTo.DisplayValue := cmbPlusTeamSFrom.DisplayValue;
end;

procedure TDialogStaffPlanningF.cmbPlusTeamSToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamSFrom.DisplayValue <> '') and
     (cmbPlusTeamSTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamSFrom.Value) >
      GetStrValue(cmbPlusTeamSTo.Value) then
      cmbPlusTeamSFrom.DisplayValue := cmbPlusTeamSTo.DisplayValue;
end;

procedure TDialogStaffPlanningF.dxBarButtonSettingsClick(Sender: TObject);
begin
  inherited;
  DialogSettingsF := TDialogSettingsF.Create(Application);
//  if SystemDM.EmbedDialogs then
//    DialogSettingsF.FormStyle := fsStayOnTop;
  try
    DialogSettingsF.ShowLevel := StaffPlanningOCIDM.ShowLevel;
    DialogSettingsF.ShowLevelC := StaffPlanningOCIDM.ShowLevelC;
    if DialogSettingsF.ShowModal = mrOK then
    begin
      WriteRegistryShowLevel(DialogSettingsF.ShowLevel);
      WriteRegistryShowLevelC(DialogSettingsF.ShowLevelC);
      StaffPlanningOCIDM.ShowLevel := DialogSettingsF.ShowLevel;
      StaffPlanningOCIDM.ShowLevelC := DialogSettingsF.ShowLevelC;
    end;
  finally
    DialogSettingsF.Free;
  end;
end;

function TDialogStaffPlanningF.ReadRegistryShowLevel: Boolean;
var
  ResultReg: String;
begin
  Result := True;
  ResultReg := SystemDM.ReadRegistry('\Software\ABS\Pims', 'ShowLevelYN');
  if ResultReg <> '' then
  begin
    if ResultReg = 'Y' then
      Result := True
    else
      Result := False;
  end;
end;

function TDialogStaffPlanningF.ReadRegistryShowLevelC: Boolean;
var
  ResultReg: String;
begin
  Result := True;
  ResultReg := SystemDM.ReadRegistry('\Software\ABS\Pims', 'ShowLevelCYN');
  if ResultReg <> '' then
  begin
    if ResultReg = 'Y' then
      Result := True
    else
      Result := False;
  end;
end;

procedure TDialogStaffPlanningF.WriteRegistryShowLevel(const Value: Boolean);
var
  ValueYN: String;
begin
  if Value then
    ValueYN := 'Y'
  else
    ValueYN := 'N';
  SystemDM.WriteRegistry('\Software\ABS\Pims', 'ShowLevelYN', ValueYN);
end;

procedure TDialogStaffPlanningF.WriteRegistryShowLevelC(const Value: Boolean);
var
  ValueYN: String;
begin
  if Value then
    ValueYN := 'Y'
  else
    ValueYN := 'N';
  SystemDM.WriteRegistry('\Software\ABS\Pims', 'ShowLevelCYN', ValueYN);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogStaffPlanningF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogStaffPlanningF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// GLOB3-173
procedure TDialogStaffPlanningF.cmbPlusShiftCloseUp(Sender: TObject);
begin
  inherited;
  LastSelectedPlant := cmbPlusPlant.DisplayValue;
  LastSelectedShift := cmbPlusShift.DisplayValue;
end;

end.
