inherited MachineDM: TMachineDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 224
  Top = 203
  Height = 593
  Width = 741
  inherited TableMaster: TTable
    OnFilterRecord = TableMasterFilterRecord
    TableName = 'PLANT'
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
  end
  inherited TableDetail: TTable
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    BeforeDelete = TableDetailBeforeDelete
    IndexFieldNames = 'PLANT_CODE;MACHINE_CODE'
    MasterFields = 'PLANT_CODE'
    TableName = 'MACHINE'
    object TableDetailPLANT_CODE: TStringField
      Tag = 1
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailMACHINE_CODE: TStringField
      Tag = 1
      FieldName = 'MACHINE_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      Tag = 1
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDetailSHORT_NAME: TStringField
      Tag = 1
      FieldName = 'SHORT_NAME'
      Required = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 24
    end
    object TableDetailDEPARTMENTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPARTMENTLU'
      LookupDataSet = TableDepartment
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      Size = 30
      Lookup = True
    end
  end
  object TableJobCode: TTable
    BeforeEdit = TableJobCodeBeforeEdit
    BeforePost = TableJobCodeBeforePost
    AfterPost = TableJobCodeAfterPost
    BeforeDelete = TableJobCodeBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = TableJobCodeNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE;MACHINE_CODE;JOB_CODE'
    MasterFields = 'PLANT_CODE;MACHINE_CODE'
    MasterSource = DataSourceDetail
    TableName = 'MACHINEJOBCODE'
    Left = 88
    Top = 208
    object TableJobCodePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeMACHINE_CODE: TStringField
      FieldName = 'MACHINE_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableJobCodeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableJobCodeNORM_PROD_LEVEL: TFloatField
      FieldName = 'NORM_PROD_LEVEL'
      Required = True
    end
    object TableJobCodeBONUS_LEVEL: TFloatField
      FieldName = 'BONUS_LEVEL'
      Required = True
    end
    object TableJobCodeNORM_OUTPUT_LEVEL: TFloatField
      FieldName = 'NORM_OUTPUT_LEVEL'
      Required = True
    end
    object TableJobCodeDATE_INACTIVE: TDateTimeField
      FieldName = 'DATE_INACTIVE'
    end
    object TableJobCodeSHOW_AT_SCANNING_YN: TStringField
      FieldName = 'SHOW_AT_SCANNING_YN'
      Size = 1
    end
    object TableJobCodeSHOW_AT_PRODUCTIONSCREEN_YN: TStringField
      FieldName = 'SHOW_AT_PRODUCTIONSCREEN_YN'
      Required = True
      Size = 1
    end
    object TableJobCodeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableJobCodeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableJobCodeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableJobCodeBULU: TStringField
      FieldKind = fkLookup
      FieldName = 'BULU'
      LookupDataSet = QueryBU
      LookupKeyFields = 'BUSINESSUNIT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'BUSINESSUNIT_CODE'
      Size = 30
      Lookup = True
    end
  end
  object DataSourceJobCode: TDataSource
    DataSet = TableJobCode
    Left = 208
    Top = 208
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceMaster
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE,BUSINESSUNIT_CODE, DESCRIPTION '
      'FROM '
      '  BUSINESSUNIT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE ')
    Left = 88
    Top = 264
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceBULU: TDataSource
    DataSet = QueryBU
    Left = 208
    Top = 264
  end
  object QueryWorkspot: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE,WORKSPOT_CODE, DESCRIPTION'
      'FROM '
      '  WORKSPOT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE AND'
      '  MACHINE_CODE = :MACHINE_CODE ')
    Left = 88
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkspot: TDataSource
    DataSet = QueryWorkspot
    Left = 208
    Top = 320
  end
  object QueryInsertJobCode: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO JOBCODE'
      '('
      ' PLANT_CODE,'
      ' WORKSPOT_CODE,'
      ' JOB_CODE,'
      ' BUSINESSUNIT_CODE,'
      ' DESCRIPTION,'
      ' NORM_PROD_LEVEL,'
      ' BONUS_LEVEL,'
      ' NORM_OUTPUT_LEVEL,'
      ' INTERFACE_CODE,'
      ' DATE_INACTIVE,'
      ' CREATIONDATE,'
      ' MUTATIONDATE,'
      ' MUTATOR,'
      ' IGNOREINTERFACECODE_YN,'
      ' SHOW_AT_SCANNING_YN,'
      ' COMPARATION_JOB_YN,'
      ' MAXIMUM_DEVIATION,'
      ' COMPARISON_REJECT_YN,'
      ' SHOW_AT_PRODUCTIONSCREEN_YN'
      ')'
      'VALUES'
      '('
      ' :PLANT_CODE,'
      ' :WORKSPOT_CODE,'
      ' :JOB_CODE,'
      ' :BUSINESSUNIT_CODE,'
      ' :DESCRIPTION,'
      ' :NORM_PROD_LEVEL,'
      ' :BONUS_LEVEL,'
      ' :NORM_OUTPUT_LEVEL,'
      ' :INTERFACE_CODE,'
      ' :DATE_INACTIVE,'
      ' :CREATIONDATE,'
      ' :MUTATIONDATE,'
      ' :MUTATOR,'
      ' :IGNOREINTERFACECODE_YN,'
      ' :SHOW_AT_SCANNING_YN,'
      ' :COMPARATION_JOB_YN,'
      ' :MAXIMUM_DEVIATION,'
      ' :COMPARISON_REJECT_YN,'
      ' :SHOW_AT_PRODUCTIONSCREEN_YN'
      ')')
    Left = 208
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DESCRIPTION'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORM_PROD_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'BONUS_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORM_OUTPUT_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INTERFACE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATE_INACTIVE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'IGNOREINTERFACECODE_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'SHOW_AT_SCANNING_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'COMPARATION_JOB_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'MAXIMUM_DEVIATION'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'COMPARISON_REJECT_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'SHOW_AT_PRODUCTIONSCREEN_YN'
        ParamType = ptUnknown
      end>
  end
  object QueryWorkspotsPerMachine: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, WORKSPOT_CODE'
      'FROM'
      '  WORKSPOT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  MACHINE_CODE = :MACHINE_CODE AND'
      '  USE_JOBCODE_YN = '#39'Y'#39)
    Left = 88
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryCheckJobCode: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT PLANT_CODE, WORKSPOT_CODE, JOB_CODE'
      'FROM JOBCODE'
      'WHERE PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE'
      '')
    Left = 88
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdateJobcode: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE JOBCODE'
      'SET'
      ' BUSINESSUNIT_CODE = :BUSINESSUNIT_CODE,'
      ' DESCRIPTION = :DESCRIPTION,'
      ' NORM_PROD_LEVEL = :NORM_PROD_LEVEL,'
      ' BONUS_LEVEL = :BONUS_LEVEL,'
      ' NORM_OUTPUT_LEVEL = :NORM_OUTPUT_LEVEL,'
      ' DATE_INACTIVE = :DATE_INACTIVE,'
      ' MUTATIONDATE = :MUTATIONDATE,'
      ' MUTATOR = :MUTATOR,'
      ' SHOW_AT_SCANNING_YN = :SHOW_AT_SCANNING_YN,'
      ' SHOW_AT_PRODUCTIONSCREEN_YN = :SHOW_AT_PRODUCTIONSCREEN_YN'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE'
      ' ')
    Left = 320
    Top = 376
    ParamData = <
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DESCRIPTION'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORM_PROD_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'BONUS_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftFloat
        Name = 'NORM_OUTPUT_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATE_INACTIVE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'SHOW_AT_SCANNING_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'SHOW_AT_PRODUCTIONSCREEN_YN'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryDeleteJobcode: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM JOBCODE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE'
      ''
      ' ')
    Left = 440
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryMachineJobs: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceDetail
    SQL.Strings = (
      'SELECT'
      '  JOB_CODE'
      'FROM'
      '  MACHINEJOBCODE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  MACHINE_CODE = :MACHINE_CODE '
      ' ')
    Left = 216
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryCheckJobInUse: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  J.JOB_CODE'
      'FROM '
      '  JOBCODE J INNER JOIN MACHINEJOBCODE MJ ON'
      '    J.PLANT_CODE = MJ.PLANT_CODE AND'
      '    J.JOB_CODE = MJ.JOB_CODE'
      '  INNER JOIN WORKSPOT W ON'
      '    J.PLANT_CODE = W.PLANT_CODE AND'
      '    J.WORKSPOT_CODE = W.WORKSPOT_CODE'
      'WHERE '
      '  W.PLANT_CODE = :PLANT_CODE AND'
      '  W.MACHINE_CODE = :MACHINE_CODE AND'
      '  J.JOB_CODE = :JOB_CODE ')
    Left = 320
    Top = 432
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MACHINE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableDepartment: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    MasterSource = DataSourceMaster
    TableName = 'DEPARTMENT'
    Left = 312
    Top = 136
  end
  object DataSourceDepartment: TDataSource
    DataSet = TableDepartment
    Left = 416
    Top = 136
  end
end
