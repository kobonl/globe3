inherited DataColConnectionDM: TDataColConnectionDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 531
  Width = 741
  inherited TableMaster: TTable
    TableName = 'WORKSTATION'
    object TableMasterCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 32
    end
    object TableMasterLICENSEVERSION: TIntegerField
      FieldName = 'LICENSEVERSION'
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'COMPUTER_NAME;COMPORT;ADDRESS;COUNTER'
    MasterFields = 'COMPUTER_NAME'
    TableName = 'DATACOLCONNECTION'
    object TableDetailCOMPUTER_NAME: TStringField
      FieldName = 'COMPUTER_NAME'
      Required = True
      Size = 32
    end
    object TableDetailCOMPORT: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'COMPORT'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailSERIALDEVICE_CODE: TStringField
      FieldName = 'SERIALDEVICE_CODE'
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailSERIALDEVICE_DESCRIPTIONLU: TStringField
      FieldKind = fkLookup
      FieldName = 'SERIALDEVICE_DESCRIPTIONLU'
      LookupDataSet = TableSerialDevice
      LookupKeyFields = 'SERIALDEVICE_CODE'
      LookupResultField = 'NAME'
      KeyFields = 'SERIALDEVICE_CODE'
      LookupCache = True
      OnValidate = DefaultNotEmptyValidate
      Lookup = True
    end
    object TableDetailCOUNTER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'COUNTER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailINTERFACE_CODE: TStringField
      FieldName = 'INTERFACE_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailADDRESS: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'ADDRESS'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailIPADDRESS: TStringField
      FieldName = 'IPADDRESS'
      Size = 30
    end
    object TableDetailPORT: TStringField
      FieldName = 'PORT'
      Size = 10
    end
    object TableDetailCHECKDIGITS: TStringField
      FieldName = 'CHECKDIGITS'
      Size = 4
    end
  end
  object TableSerialDevice: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'SERIALDEVICE'
    Left = 88
    Top = 208
    object TableSerialDeviceSERIALDEVICE_CODE: TStringField
      FieldName = 'SERIALDEVICE_CODE'
      Required = True
      Size = 6
    end
    object TableSerialDeviceNAME: TStringField
      FieldName = 'NAME'
      Required = True
      Size = 30
    end
    object TableSerialDeviceDEVICETYPE: TStringField
      FieldName = 'DEVICETYPE'
      Required = True
      Size = 3
    end
    object TableSerialDeviceBAUDRATE: TIntegerField
      FieldName = 'BAUDRATE'
    end
    object TableSerialDevicePARITY: TStringField
      FieldName = 'PARITY'
      Required = True
      Size = 1
    end
    object TableSerialDeviceDATABITS: TIntegerField
      FieldName = 'DATABITS'
    end
    object TableSerialDeviceSTOPBITS: TIntegerField
      FieldName = 'STOPBITS'
    end
    object TableSerialDeviceINITSTRING: TStringField
      FieldName = 'INITSTRING'
      Size = 40
    end
    object TableSerialDeviceINITCHAR1: TIntegerField
      FieldName = 'INITCHAR1'
    end
    object TableSerialDeviceINITCHAR2: TIntegerField
      FieldName = 'INITCHAR2'
    end
    object TableSerialDeviceINITCHAR3: TIntegerField
      FieldName = 'INITCHAR3'
    end
    object TableSerialDeviceVERIFYSTRING: TStringField
      FieldName = 'VERIFYSTRING'
    end
    object TableSerialDeviceVERIFYPOSITION: TIntegerField
      FieldName = 'VERIFYPOSITION'
    end
    object TableSerialDeviceENDCHARACTER1: TIntegerField
      FieldName = 'ENDCHARACTER1'
    end
    object TableSerialDeviceENDCHARACTER2: TIntegerField
      FieldName = 'ENDCHARACTER2'
    end
    object TableSerialDeviceCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableSerialDeviceMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableSerialDeviceMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableSerialDeviceVALUEPOSITION: TIntegerField
      FieldName = 'VALUEPOSITION'
    end
  end
  object DataSourceSerialDevice: TDataSource
    DataSet = TableSerialDevice
    Left = 200
    Top = 208
  end
end
