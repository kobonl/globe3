inherited EmployeeContractsF: TEmployeeContractsF
  Left = 370
  Top = 221
  Width = 670
  Height = 447
  Caption = 'Employee contract'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 654
    Height = 117
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 113
      Width = 652
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 652
      Height = 112
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Employee'
          Width = 715
        end>
      KeyField = 'EMPLOYEE_NUMBER'
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Number'
        Width = 69
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Name'
        Width = 261
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnPlant: TdxDBGridLookupColumn
        Caption = 'Plant'
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
        ListFieldName = 'DESCRIPTION;PLANT_CODE'
      end
      object dxMasterGridColumnDepartment: TdxDBGridLookupColumn
        Caption = 'Department'
        Width = 133
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENTLU'
        ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
      end
      object dxMasterGridColumnStartDate: TdxDBGridDateColumn
        Caption = 'Start date'
        Width = 120
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTDATE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 291
    Width = 654
    Height = 117
    object Label1: TLabel
      Left = 7
      Top = 14
      Width = 49
      Height = 13
      Caption = 'Start date'
    end
    object Label2: TLabel
      Left = 7
      Top = 41
      Width = 43
      Height = 13
      Caption = 'End date'
    end
    object Label3: TLabel
      Left = 7
      Top = 67
      Width = 60
      Height = 13
      Caption = 'Hourly wage'
    end
    object Label4: TLabel
      Left = 188
      Top = 14
      Width = 71
      Height = 13
      Caption = 'Days per week'
    end
    object Label5: TLabel
      Left = 188
      Top = 67
      Width = 75
      Height = 13
      Caption = 'Hours per week'
    end
    object Label7: TLabel
      Left = 188
      Top = 41
      Width = 79
      Height = 13
      Caption = 'Hol. hrs. p. year'
    end
    object LabelSeniority: TLabel
      Left = 188
      Top = 93
      Width = 72
      Height = 13
      Caption = 'Seniority hours'
    end
    object Label8: TLabel
      Left = 7
      Top = 93
      Width = 71
      Height = 13
      Caption = 'Work Schedule'
    end
    object dxDBMaskEdit1: TdxDBMaskEdit
      Left = 80
      Top = 64
      Width = 45
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'HOURLY_WAGE'
      DataSource = EmployeeContractsDM.DataSourceDetail
      IgnoreMaskBlank = False
      MaxLength = 6
      StoredValues = 2
    end
    object dxDBEditDays: TdxDBEdit
      Left = 270
      Top = 10
      Width = 40
      Style.BorderStyle = xbsSingle
      TabOrder = 3
      DataField = 'CONTRACT_DAY_PER_WEEK'
      DataSource = EmployeeContractsDM.DataSourceDetail
      MaxLength = 1
      StoredValues = 2
    end
    object DBRadioGroupContractType: TDBRadioGroup
      Left = 373
      Top = 1
      Width = 119
      Height = 115
      Anchors = [akRight, akBottom]
      Caption = 'Contract type'
      DataField = 'CONTRACT_TYPE'
      DataSource = EmployeeContractsDM.DataSourceDetail
      Items.Strings = (
        'Permanent'
        'Temporary'
        'External')
      TabOrder = 10
      Values.Strings = (
        '0'
        '1'
        '2')
    end
    object DBPickerStartDate: TDBPicker
      Tag = 1
      Left = 80
      Top = 10
      Width = 97
      Height = 21
      CalAlignment = dtaLeft
      Date = 42313.4699657523
      Time = 42313.4699657523
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 0
      DataField = 'STARTDATE'
      DataSource = EmployeeContractsDM.DataSourceDetail
    end
    object DBPickerEndDate: TDBPicker
      Tag = 1
      Left = 80
      Top = 39
      Width = 97
      Height = 21
      CalAlignment = dtaLeft
      Date = 42313.4712350579
      Time = 42313.4712350579
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 1
      DataField = 'ENDDATE'
      DataSource = EmployeeContractsDM.DataSourceDetail
    end
    object GroupBox1: TGroupBox
      Left = 492
      Top = 1
      Width = 169
      Height = 67
      Anchors = [akRight, akBottom]
      Caption = 'Export Payroll'
      TabOrder = 11
      object Label6: TLabel
        Left = 8
        Top = 19
        Width = 83
        Height = 13
        Caption = 'Guaranteed days'
      end
      object lblExportCode: TLabel
        Left = 8
        Top = 42
        Width = 60
        Height = 13
        Caption = 'Export Code'
      end
      object dxDBEditGuaranteedDays: TdxDBEdit
        Left = 96
        Top = 16
        Width = 64
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'GUARANTEED_DAYS'
        DataSource = EmployeeContractsDM.DataSourceDetail
        MaxLength = 5
        StoredValues = 2
      end
      object dxDBEditExportCode: TdxDBEdit
        Left = 96
        Top = 39
        Width = 64
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'EXPORT_CODE'
        DataSource = EmployeeContractsDM.DataSourceDetail
        MaxLength = 6
        StoredValues = 2
      end
    end
    object dxSpinEditHHolidayHours: TdxSpinEdit
      Left = 270
      Top = 39
      Width = 52
      TabOrder = 4
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      MaxValue = 9999
      StoredValues = 16
    end
    object dxSpinEditMHolidayHours: TdxSpinEdit
      Left = 328
      Top = 39
      Width = 40
      TabOrder = 5
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      OnChange = dxSpinEditMinuteChange
      MaxValue = 59
      StoredValues = 16
    end
    object dxSpinEditHHoursPerWeek: TdxSpinEdit
      Left = 270
      Top = 65
      Width = 52
      TabOrder = 6
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      MaxValue = 9999
      StoredValues = 16
    end
    object dxSpinEditMHoursPerWeek: TdxSpinEdit
      Left = 328
      Top = 65
      Width = 40
      TabOrder = 7
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      OnChange = dxSpinEditMinuteChange
      MaxValue = 59
      StoredValues = 16
    end
    object dxSpinEditHSeniority: TdxSpinEdit
      Left = 270
      Top = 91
      Width = 52
      TabOrder = 8
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      MaxValue = 9999
      StoredValues = 16
    end
    object dxSpinEditMSeniority: TdxSpinEdit
      Left = 328
      Top = 91
      Width = 40
      TabOrder = 9
      OnKeyPress = dxSpinEditHDaysPerWeekKeyPress
      OnMouseUp = dxSpinEditHDaysPerWeekMouseUp
      OnChange = dxSpinEditMinuteChange
      MaxValue = 59
      StoredValues = 16
    end
    object DBRadioGroupWorker: TDBRadioGroup
      Left = 493
      Top = 67
      Width = 168
      Height = 45
      Anchors = [akRight, akBottom]
      Columns = 2
      DataField = 'WORKER_YN'
      DataSource = EmployeeContractsDM.DataSourceDetail
      Items.Strings = (
        'Worker'
        'Clerk')
      TabOrder = 12
      Values.Strings = (
        'Y'
        'N')
    end
    object dxDBLookupEdit1: TdxDBLookupEdit
      Left = 80
      Top = 89
      Width = 97
      TabOrder = 13
      DataField = 'LUWORKSCHEDULE'
      DataSource = EmployeeContractsDM.DataSourceDetail
      ListFieldName = 'CODEDESC'
      CanDeleteText = True
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 143
    Width = 654
    Height = 148
    Align = alBottom
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 144
      Width = 652
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 652
      Height = 143
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Employee contract'
        end>
      KeyField = 'STARTDATE'
      OnMouseMove = dxDetailGridMouseMove
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumnStartDate: TdxDBGridDateColumn
        Caption = 'Start date'
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTDATE'
        DateButtons = [btnToday]
      end
      object dxDetailGridColumnEndDate: TdxDBGridDateColumn
        Caption = 'End date'
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENDDATE'
        DateButtons = [btnToday]
      end
      object dxDetailGridColumnHourlyWage: TdxDBGridCurrencyColumn
        Caption = 'Hourly wage'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURLY_WAGE'
        DisplayFormat = '0.00;-0.00'
      end
      object dxDetailGridColumnContractType: TdxDBGridSpinColumn
        Caption = 'Contract type'
        Width = 110
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACT_TYPE'
      end
      object dxDetailGridColumnHoursWeek: TdxDBGridSpinColumn
        Caption = 'Hours per week'
        Width = 83
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACT_HOUR_PER_WEEK'
      end
      object dxDetailGridColumnDaysWeek: TdxDBGridSpinColumn
        Caption = 'Days per week'
        Width = 79
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACT_DAY_PER_WEEK'
      end
      object dxDetailGridColumnHoliday: TdxDBGridSpinColumn
        Caption = 'Holiday hours per year'
        MinWidth = 0
        Width = 60
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOLIDAY_HOUR_PER_YEAR'
      end
      object dxDetailGridColumnSeniority: TdxDBGridColumn
        Caption = 'Seniority hours'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SENIORITY_HOURS'
      end
      object dxDetailGridColumnGuaranteedDays: TdxDBGridColumn
        Caption = 'Guar. days'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'GUARANTEED_DAYS'
      end
      object dxDetailGridColumnExportCode: TdxDBGridColumn
        Caption = 'Exportcode'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
      object dxDetailGridColumnWorker: TdxDBGridCheckColumn
        Caption = 'Worker'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKER_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
end
