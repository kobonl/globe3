inherited ExtraPaymentF: TExtraPaymentF
  Left = 218
  Top = 163
  Width = 764
  Height = 514
  Caption = 'Extra payments'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 113
    Width = 756
    Height = 11
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 7
      Width = 754
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Width = 754
      Height = 6
    end
  end
  inherited pnlDetail: TPanel
    Top = 359
    Width = 756
    Height = 121
    TabOrder = 3
    object Label4: TLabel
      Left = 16
      Top = 24
      Width = 86
      Height = 13
      Caption = 'Employee Number'
    end
    object Label5: TLabel
      Left = 16
      Top = 56
      Width = 103
      Height = 13
      Caption = 'Payment export code'
    end
    object Label1: TLabel
      Left = 16
      Top = 88
      Width = 81
      Height = 13
      Caption = 'Payment amount'
    end
    object DBEdit2: TDBEdit
      Left = 128
      Top = 86
      Width = 121
      Height = 21
      DataField = 'PAYMENT_AMOUNT'
      DataSource = ExtraPaymentDM.DataSourceDetail
      TabOrder = 2
    end
    object DBLookupCmbBoxEmployee: TDBLookupComboBox
      Tag = 1
      Left = 128
      Top = 24
      Width = 273
      Height = 21
      DataField = 'EMPLU'
      DataSource = ExtraPaymentDM.DataSourceDetail
      DropDownWidth = 250
      KeyField = 'DESCRIPTION'
      ListField = 'EMPLOYEE_NUMBER;DESCRIPTION'
      ListSource = ExtraPaymentDM.DataSourceEmpl
      TabOrder = 0
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Tag = 1
      Left = 128
      Top = 56
      Width = 121
      Height = 21
      DataField = 'PAYMENT_EXPORT_CODE'
      DataSource = ExtraPaymentDM.DataSourceDetail
      DropDownRows = 5
      DropDownWidth = 121
      KeyField = 'PAYMENT_EXPORT_CODE'
      ListField = 'PAYMENT_EXPORT_CODE'
      ListSource = ExtraPaymentDM.DataSourcePaymentExpCode
      TabOrder = 1
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 124
    Width = 756
    Height = 235
    inherited spltDetail: TSplitter
      Top = 231
      Width = 754
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 754
      Height = 230
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Extra payments'
          Width = 458
        end>
      DefaultLayout = False
      KeyField = 'PAYMENT_DATE'
      ShowBands = True
      object dxDetailGridColumn2: TdxDBGridLookupColumn
        Caption = 'Employee'
        Width = 342
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLU'
        ListFieldName = 'EMPLOYEE_NUMBER;DESCRIPTION'
      end
      object dxDetailGridColumn3: TdxDBGridLookupColumn
        Caption = 'Payment export code'
        Width = 167
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PAYMENTEXPCODELU'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Payment amount'
        Width = 135
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PAYMENT_AMOUNT'
      end
    end
  end
  object pnlTeamPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 756
    Height = 87
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 756
      Height = 73
      Align = alTop
      TabOrder = 0
      object Label3: TLabel
        Left = 16
        Top = 32
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object DateFrom: TDateTimePicker
        Left = 64
        Top = 30
        Width = 180
        Height = 21
        CalAlignment = dtaLeft
        Date = 0.544151967595099
        Time = 0.544151967595099
        DateFormat = dfShort
        DateMode = dmComboBox
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnShadow
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Kind = dtkDate
        ParseInput = False
        ParentFont = False
        TabOrder = 0
        OnChange = DateFromChange
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 568
    Top = 56
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 680
    Top = 64
  end
  inherited StandardMenuActionList: TActionList
    Left = 632
    Top = 32
  end
  inherited dsrcActive: TDataSource
    Left = 640
    Top = 64
  end
end
