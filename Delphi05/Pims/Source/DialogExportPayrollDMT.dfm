inherited DialogExportPayrollDM: TDialogExportPayrollDM
  OldCreateOrder = True
  Left = 258
  Top = 105
  Height = 724
  Width = 1075
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 136
    Top = 8
  end
  object TableEP: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'EXPORTPAYROLL'
    Left = 344
    Top = 8
    object TableEPEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Required = True
      Size = 6
    end
    object TableEPSEQUENCE_NUMBER: TIntegerField
      FieldName = 'SEQUENCE_NUMBER'
    end
    object TableEPCUSTOMER_NUMBER: TIntegerField
      FieldName = 'CUSTOMER_NUMBER'
    end
    object TableEPMEDIUM: TStringField
      FieldName = 'MEDIUM'
      Size = 6
    end
    object TableEPOPERATING_SYSTEM: TStringField
      FieldName = 'OPERATING_SYSTEM'
      Size = 6
    end
    object TableEPRELEASE_NUMBER: TIntegerField
      FieldName = 'RELEASE_NUMBER'
    end
    object TableEPPROCESS_CODE: TStringField
      FieldName = 'PROCESS_CODE'
      Size = 1
    end
    object TableEPMUTATION_CODE: TStringField
      FieldName = 'MUTATION_CODE'
      Size = 1
    end
    object TableEPEXPORT_CODE_DAYS: TStringField
      FieldName = 'EXPORT_CODE_DAYS'
      Size = 6
    end
    object TableEPTOTAL_PK_RECORDS: TIntegerField
      FieldName = 'TOTAL_PK_RECORDS'
    end
    object TableEPWEEK_PERIOD_MONTH: TStringField
      FieldName = 'WEEK_PERIOD_MONTH'
      Size = 1
    end
    object TableEPEXPORT_WAGE_YN: TStringField
      FieldName = 'EXPORT_WAGE_YN'
      Size = 1
    end
    object TableEPWAGE_SEPARATOR: TStringField
      FieldName = 'WAGE_SEPARATOR'
      Size = 6
    end
    object TableEPCORPORATE_CODE: TStringField
      FieldName = 'CORPORATE_CODE'
      Size = 6
    end
    object TableEPBATCH_ID: TStringField
      FieldName = 'BATCH_ID'
      Size = 6
    end
    object TableEPDAYS_PAY: TFloatField
      FieldName = 'DAYS_PAY'
    end
    object TableEPMONTH_GROUP_EFFICIENCY_YN: TStringField
      FieldName = 'MONTH_GROUP_EFFICIENCY_YN'
      Size = 1
    end
    object TableEPMULTIPLY_FACTOR_HOURS: TIntegerField
      FieldName = 'MULTIPLY_FACTOR_HOURS'
      Required = True
    end
    object TableEPMULTIPLY_FACTOR_DAYS: TIntegerField
      FieldName = 'MULTIPLY_FACTOR_DAYS'
      Required = True
    end
    object TableEPEFFICIENCY100MIN_YN: TStringField
      FieldName = 'EFFICIENCY100MIN_YN'
      Size = 1
    end
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT'
      
        '  E.PLANT_CODE, P.COUNTRY_ID, E.EMPLOYEE_NUMBER, E.CONTRACTGROUP' +
        '_CODE,'
      '  E.STARTDATE,  E.DESCRIPTION, E.SHIFT_NUMBER, E.GRADE,'
      '  CG.EXPORT_NORMAL_HRS_YN, CG.EXPORT_WORKED_DAYS_YN,'
      '  NVL(P.CUSTOMER_NUMBER, 0) AS CUSTOMER_NUMBER'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '    E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      'WHERE'
      '  E.PLANT_CODE >= :PLANTFROM  AND'
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ''
      ' '
      ' '
      ' ')
    Left = 48
    Top = 192
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object QueryLastContract: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EC.STARTDATE, EC.HOURLY_WAGE, '
      '  EC.GUARANTEED_DAYS, EC.EXPORT_CODE, EC.WORKER_YN '
      'FROM '
      '  EMPLOYEECONTRACT EC '
      'WHERE '
      '  (EC.EMPLOYEE_NUMBER = :EMPLOYEE) AND '
      '  ('
      '    ( (EC.STARTDATE <= :DATEMAX ) AND '
      '      (EC.ENDDATE >= :DATEMAX) AND (EC.CONTRACT_TYPE <> 2)) '
      '    OR '
      '    ( (EC.STARTDATE <= :DATEMAX ) AND '
      '    (EC.ENDDATE IS NULL) AND (EC.CONTRACT_TYPE <> 2)) '
      '  ) '
      'ORDER BY '
      '  EC.STARTDATE')
    Left = 240
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QuerySHE: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.COUNTRY_ID, S.EMPLOYEE_NUMBER, S.SALARY_DATE,'
      '  S.HOURTYPE_NUMBER, H.WAGE_BONUS_ONLY_YN,'
      '  MAX(NVL((SELECT HC.EXPORT_CODE'
      '    FROM HOURTYPEPERCOUNTRY HC'
      '    WHERE HC.COUNTRY_ID = P.COUNTRY_ID AND'
      
        '    HC.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER), H.EXPORT_CODE)) AS ' +
        'EXPORT_CODE,'
      '  SUM(S.SALARY_MINUTE) AS SUMMIN'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN HOURTYPE H ON'
      '    S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  NVL((SELECT HC.EXPORT_CODE'
      '    FROM HOURTYPEPERCOUNTRY HC'
      '    WHERE HC.COUNTRY_ID = P.COUNTRY_ID AND'
      
        '    HC.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER), H.EXPORT_CODE) IS N' +
        'OT NULL AND'
      '  S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  S.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  S.SALARY_DATE >= :DATEFROM AND'
      '  S.SALARY_DATE <= :DATETO'
      'GROUP BY'
      
        '  P.COUNTRY_ID, S.EMPLOYEE_NUMBER, S.SALARY_DATE, S.HOURTYPE_NUM' +
        'BER,'
      '  H.WAGE_BONUS_ONLY_YN'
      'ORDER BY'
      '  S.EMPLOYEE_NUMBER, S.SALARY_DATE, S.HOURTYPE_NUMBER,'
      '  H.WAGE_BONUS_ONLY_YN')
    Left = 144
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object QueryAHE: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PL.COUNTRY_ID,'
      '  A.EMPLOYEE_NUMBER, '
      '  A.ABSENCEHOUR_DATE,    '
      '  R.ABSENCETYPE_CODE,    '
      '  R.HOURTYPE_NUMBER,    '
      '  H.WAGE_BONUS_ONLY_YN,'
      '  A.ABSENCE_MINUTE,'
      '  NVL((SELECT HC.EXPORT_CODE'
      '    FROM HOURTYPEPERCOUNTRY HC'
      '    WHERE HC.COUNTRY_ID = PL.COUNTRY_ID AND'
      
        '    HC.HOURTYPE_NUMBER = R.HOURTYPE_NUMBER), H.EXPORT_CODE) EXPO' +
        'RT_CODE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN ABSENCEREASON R ON'
      '    A.ABSENCEREASON_ID = R.ABSENCEREASON_ID'
      '  INNER JOIN ABSENCETYPE T ON'
      '    R.ABSENCETYPE_CODE = T.ABSENCETYPE_CODE'
      '  INNER JOIN HOURTYPE H ON'
      '    R.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER '
      '  INNER JOIN PLANT PL ON'
      '    PL.PLANT_CODE = E.PLANT_CODE'
      'WHERE'
      '  NVL((SELECT HC.EXPORT_CODE'
      '    FROM HOURTYPEPERCOUNTRY HC'
      '    WHERE HC.COUNTRY_ID = PL.COUNTRY_ID AND'
      
        '    HC.HOURTYPE_NUMBER = R.HOURTYPE_NUMBER), H.EXPORT_CODE) IS N' +
        'OT NULL AND'
      '  R.PAYED_YN = '#39'Y'#39' AND'
      '  A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  A.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  A.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :DATETO'
      'ORDER BY'
      '  A.EMPLOYEE_NUMBER,'
      '  A.ABSENCEHOUR_DATE'
      ''
      ' ')
    Left = 240
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object QueryExtraPayment: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EP.EMPLOYEE_NUMBER, EP.PAYMENT_EXPORT_CODE, '
      '  SUM(EP.PAYMENT_AMOUNT) AS SUMAMOUNT '
      'FROM '
      '  EXTRAPAYMENT EP '
      'WHERE '
      '  EP.EMPLOYEE_NUMBER = :EMPLOYEE AND '
      '  EP.PAYMENT_DATE >= :DATEMIN AND '
      '  EP.PAYMENT_DATE <= :DATEMAX '
      'GROUP BY '
      '  EP.EMPLOYEE_NUMBER, EP.PAYMENT_EXPORT_CODE '
      'ORDER BY '
      '  EP.EMPLOYEE_NUMBER, EP.PAYMENT_EXPORT_CODE')
    Left = 48
    Top = 8
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QueryPRODHOURPEPT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  P.EMPLOYEE_NUMBER, '
      '  W.DEPARTMENT_CODE, '
      '  P.SHIFT_NUMBER, '
      '  P.HOURTYPE_NUMBER, '
      '  H.EXPORT_CODE, '
      '  H.OVERTIME_YN, '
      '  SUM(P.PRODUCTION_MINUTE) AS SUMPRODUCTION_MINUTE '
      'FROM '
      '  PRODHOURPEREMPLPERTYPE P, '
      '  HOURTYPE H, '
      '  WORKSPOT W '
      'WHERE '
      '  P.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND '
      '  P.PLANT_CODE >= :PLANTFROM AND '
      '  P.PLANT_CODE <= :PLANTTO AND '
      '  P.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND '
      '  P.EMPLOYEE_NUMBER <= :EMPLOYEETO AND '
      '  P.PRODHOUREMPLOYEE_DATE >= :DATEMIN AND '
      '  P.PRODHOUREMPLOYEE_DATE <= :DATEMAX AND '
      '  P.PLANT_CODE = W.PLANT_CODE AND '
      '  P.WORKSPOT_CODE = W.WORKSPOT_CODE AND '
      '  H.EXPORT_CODE IS NOT NULL '
      'GROUP BY '
      '  P.EMPLOYEE_NUMBER, '
      '  W.DEPARTMENT_CODE, '
      '  P.SHIFT_NUMBER, '
      '  P.HOURTYPE_NUMBER, '
      '  H.EXPORT_CODE, '
      '  H.OVERTIME_YN')
    Left = 432
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QueryAbsenceType: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ABT.EXPORT_CODE '
      'FROM '
      '  ABSENCEREASON AC, ABSENCETYPE ABT '
      'WHERE '
      '  AC.HOURTYPE_NUMBER = :HOURTYPE_NUMBER AND '
      '  AC.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE AND '
      '  ABT.EXPORT_CODE IS NOT NULL')
    Left = 432
    Top = 56
    ParamData = <
      item
        DataType = ftInteger
        Name = 'HOURTYPE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object cdsEmployeeADPUSData: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byEmployee'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byEmployee'
    Params = <>
    StoreDefs = True
    Left = 432
    Top = 104
    object cdsEmployeeADPUSDataEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmployeeADPUSDataWKDAYS: TIntegerField
      FieldName = 'WKDAYS'
    end
    object cdsEmployeeADPUSDataTOTALDAYS: TIntegerField
      FieldName = 'TOTALDAYS'
    end
  end
  object QueryABSType_ADP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  -1 COUNTRY_ID, A.ABSENCETYPE_CODE, A.EXPORT_CODE'
      'FROM'
      '  ABSENCETYPE A'
      'WHERE A.EXPORT_CODE IS NOT NULL'
      'UNION'
      
        '  SELECT ABTC.COUNTRY_ID, ABTC.ABSENCETYPE_CODE, ABTC.EXPORT_COD' +
        'E'
      '  FROM ABSENCETYPE A2 INNER JOIN ABSENCETYPEPERCOUNTRY ABTC ON'
      '    A2.ABSENCETYPE_CODE = ABTC.ABSENCETYPE_CODE'
      '  WHERE ABTC.EXPORT_CODE IS NOT NULL'
      'ORDER BY'
      '  1, 2'
      ''
      ' ')
    Left = 48
    Top = 360
  end
  object ClientDataSetAbsType: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsType'
    Left = 160
    Top = 360
  end
  object DataSetProviderAbsType: TDataSetProvider
    DataSet = QueryABSType_ADP
    Constraints = True
    Left = 288
    Top = 360
  end
  object QueryHourType: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  -1 COUNTRY_ID,'
      '  H.HOURTYPE_NUMBER,'
      '  H.EXPORT_CODE,'
      '  H.WAGE_BONUS_ONLY_YN,'
      
        '  H.BONUS_PERCENTAGE, H.OVERTIME_YN, H.MINIMUM_WAGE, H.COUNT_DAY' +
        '_YN,'
      '  H.DESCRIPTION,'
      '  H.TIME_FOR_TIME_YN,'
      '  H.BONUS_IN_MONEY_YN,'
      '  H.EXPORT_OVERTIME_YN,'
      '  H.EXPORT_PAYROLL_YN'
      'FROM'
      '  HOURTYPE H'
      'WHERE'
      '  H.EXPORT_CODE IS NOT NULL'
      'UNION'
      '  SELECT'
      '    HC.COUNTRY_ID,'
      '    H.HOURTYPE_NUMBER,'
      '    HC.EXPORT_CODE,'
      '    H.WAGE_BONUS_ONLY_YN,'
      
        '    H.BONUS_PERCENTAGE, H.OVERTIME_YN, H.MINIMUM_WAGE, H.COUNT_D' +
        'AY_YN,'
      '    HC.DESCRIPTION,'
      '    H.TIME_FOR_TIME_YN,'
      '    H.BONUS_IN_MONEY_YN,'
      '    HC.EXPORT_OVERTIME_YN,'
      '    HC.EXPORT_PAYROLL_YN'
      '  FROM'
      '    HOURTYPE H INNER JOIN HOURTYPEPERCOUNTRY HC ON'
      '      H.HOURTYPE_NUMBER = HC.HOURTYPE_NUMBER'
      '  WHERE'
      '    HC.EXPORT_CODE IS NOT NULL'
      'ORDER BY'
      '  1, 2'
      ''
      ' ')
    Left = 48
    Top = 304
  end
  object ClientDataSetHourType: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderHourType'
    Left = 160
    Top = 304
  end
  object DataSetProviderHourType: TDataSetProvider
    DataSet = QueryHourType
    Constraints = True
    Left = 288
    Top = 304
  end
  object QueryContract: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.CONTRACTGROUP_CODE, C.HOURTYPE_NUMBER, C.GUARANTEED_HOURS,'
      
        '  C.TIME_FOR_TIME_YN, C.BONUS_IN_MONEY_YN, C.ROUND_TRUNC_SALARY_' +
        'HOURS,'
      '  C.ROUND_MINUTE'
      'FROM'
      '  CONTRACTGROUP C'
      'ORDER BY'
      '  C.CONTRACTGROUP_CODE')
    Left = 48
    Top = 248
  end
  object ClientDataSetContract: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderContract'
    Left = 152
    Top = 248
  end
  object DataSetProviderContract: TDataSetProvider
    DataSet = QueryContract
    Constraints = True
    Left = 288
    Top = 248
  end
  object QueryEmpContract: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EC.EMPLOYEE_NUMBER'
      'FROM'
      '  EMPLOYEECONTRACT EC'
      'WHERE'
      '  EC.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EC.CONTRACT_TYPE <> 2 AND'
      '  EC.STARTDATE <= :FDATE AND'
      '  ( (EC.ENDDATE IS NULL) OR ( EC.ENDDATE >= :FDATE))'
      ' '
      ' '
      ' ')
    Left = 48
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderEmpl'
    Left = 152
    Top = 184
  end
  object DataSetProviderEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 288
    Top = 184
  end
  object QueryPHETYPE_PARIS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  P.PRODHOUREMPLOYEE_DATE,'
      '  P.PLANT_CODE, P.SHIFT_NUMBER, P.WORKSPOT_CODE,'
      '  H.OVERTIME_YN, P.EMPLOYEE_NUMBER, E.CONTRACTGROUP_CODE,'
      '  B.BUSINESSUNIT_CODE, E.DEPARTMENT_CODE,'
      '  P.HOURTYPE_NUMBER, H.EXPORT_CODE,'
      '  CASE'
      '    WHEN P.HOURTYPE_NUMBER IN'
      
        '      (SELECT E2.HOURTYPE_NUMBER FROM EXCEPTIONALHOURDEF E2 WHER' +
        'E E2.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) THEN '#39'E'#39
      '    WHEN P.HOURTYPE_NUMBER IN'
      
        '      (SELECT E2.OVERTIME_HOURTYPE_NUMBER FROM EXCEPTIONALHOURDE' +
        'F E2 WHERE E2.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) THEN '#39'C' +
        #39
      '    WHEN P.HOURTYPE_NUMBER IN'
      
        '      (SELECT O2.HOURTYPE_NUMBER FROM OVERTIMEDEFINITION O2 WHER' +
        'E O2.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE) THEN '#39'O'#39
      '    ELSE '#39'N'#39
      '  END EH,'
      '  NVL(SUM(P.PRODUCTION_MINUTE),0) AS SUM_MIN'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE P INNER JOIN HOURTYPE H ON'
      '    P.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN BUSINESSUNIT B ON'
      '    B.PLANT_CODE = E.PLANT_CODE'
      'WHERE'
      '  P.PLANT_CODE >= :PLANTFROM AND'
      '  P.PLANT_CODE <= :PLANTTO AND'
      '  P.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  P.EMPLOYEE_NUMBER <= :EMPLOYEETO  AND'
      '  P.PRODHOUREMPLOYEE_DATE >= :DATEMIN AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :DATEMAX'
      'GROUP BY'
      '  P.PRODHOUREMPLOYEE_DATE,'
      '  P.PLANT_CODE, P.SHIFT_NUMBER, P.WORKSPOT_CODE,'
      '  H.OVERTIME_YN, P.EMPLOYEE_NUMBER, E.CONTRACTGROUP_CODE,'
      '  B.BUSINESSUNIT_CODE, E.DEPARTMENT_CODE,'
      '  P.HOURTYPE_NUMBER, H.EXPORT_CODE'
      'ORDER BY'
      '  P.EMPLOYEE_NUMBER,'
      '  P.PRODHOUREMPLOYEE_DATE'
      ''
      '')
    Left = 432
    Top = 152
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QueryAHE_PARIS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AHE.PLANT_CODE, H.EXPORT_CODE,'
      '  AHE.EMPLOYEE_NUMBER, '
      '  B.BUSINESSUNIT_CODE, E.DEPARTMENT_CODE,'
      '  SUM(AHE.ABSENCE_MINUTE) AS SUM_MIN'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON A ON'
      '    AHE.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '  INNER JOIN HOURTYPE H ON'
      '    A.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '    AHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN BUSINESSUNIT B ON'
      '    E.PLANT_CODE = B.PLANT_CODE'
      'WHERE'
      '  AHE.PLANT_CODE >= :PLANTFROM AND'
      '  AHE.PLANT_CODE <= :PLANTTO AND'
      '  AHE.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  AHE.EMPLOYEE_NUMBER <= :EMPLOYEETO  AND'
      '  AHE.ABSENCEHOUR_DATE >= :DATEMIN AND'
      '  AHE.ABSENCEHOUR_DATE <= :DATEMAX  AND'
      '  H.EXPORT_CODE IS NOT NULL'
      'GROUP BY'
      '  AHE.PLANT_CODE, H.EXPORT_CODE,  AHE.EMPLOYEE_NUMBER,'
      '  B.BUSINESSUNIT_CODE, E.DEPARTMENT_CODE'
      'ORDER BY'
      '  AHE.PLANT_CODE, H.EXPORT_CODE,  AHE.EMPLOYEE_NUMBER,'
      '  B.BUSINESSUNIT_CODE, E.DEPARTMENT_CODE'
      '')
    Left = 432
    Top = 200
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QueryWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, GRADE'
      'FROM '
      '  WORKSPOT '
      'ORDER BY'
      '  PLANT_CODE, WORKSPOT_CODE')
    Left = 48
    Top = 128
  end
  object ClientDataSetWK: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderWK'
    Left = 152
    Top = 128
  end
  object DataSetProviderWK: TDataSetProvider
    DataSet = QueryWK
    Constraints = True
    Left = 288
    Top = 128
  end
  object ClientDataSetGradeExpCode: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderGradeExpCode'
    Left = 168
    Top = 416
  end
  object DataSetProviderGradeExpCode: TDataSetProvider
    DataSet = QueryGradeExpCode
    Constraints = True
    Left = 312
    Top = 416
  end
  object QueryGradeExpCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  SHIFT_NUMBER, OVERTIME_YN, UPGRADE, EXPORT_CODE'
      'FROM '
      '  GRADEEXPORTCODE'
      'ORDER BY '
      '  SHIFT_NUMBER, OVERTIME_YN, UPGRADE')
    Left = 48
    Top = 416
  end
  object ClientDataSetEmployeePARIS: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetEmployeePARISIdx'
        Fields = 'EMPLOYEE_NUMBER;EXPORT_CODE'
      end>
    IndexName = 'ClientDataSetEmployeePARISIdx'
    Params = <>
    StoreDefs = True
    Left = 432
    Top = 248
    object ClientDataSetEmployeePARISEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object ClientDataSetEmployeePARISBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object ClientDataSetEmployeePARISDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object ClientDataSetEmployeePARISEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object ClientDataSetEmployeePARISAMOUNT_HRS: TIntegerField
      FieldName = 'AMOUNT_HRS'
    end
  end
  object cdsEmplDays: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'PrimaryIndex'
        Fields = 'EMPLOYEE_NUMBER;EDATE'
      end>
    IndexName = 'PrimaryIndex'
    Params = <>
    StoreDefs = True
    Left = 432
    Top = 296
    object cdsEmplDaysEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmplDaysEDATE: TDateTimeField
      FieldName = 'EDATE'
    end
    object cdsEmplDaysWORK: TIntegerField
      FieldName = 'WORK'
    end
    object cdsEmplDaysTFT: TIntegerField
      FieldName = 'TFT'
    end
    object cdsEmplDaysBANK: TIntegerField
      FieldName = 'BANK'
    end
    object cdsEmplDaysILL: TIntegerField
      FieldName = 'ILL'
    end
    object cdsEmplDaysHOL: TIntegerField
      FieldName = 'HOL'
    end
    object cdsEmplDaysPAID: TIntegerField
      FieldName = 'PAID'
    end
    object cdsEmplDaysUNPAID: TIntegerField
      FieldName = 'UNPAID'
    end
    object cdsEmplDaysWTR: TIntegerField
      FieldName = 'WTR'
    end
    object cdsEmplDaysLABOUR: TIntegerField
      FieldName = 'LABOUR'
    end
    object cdsEmplDaysMAT: TIntegerField
      FieldName = 'MAT'
    end
    object cdsEmplDaysLAY: TIntegerField
      FieldName = 'LAY'
    end
    object cdsEmplDaysPREV: TIntegerField
      FieldName = 'PREV'
    end
    object cdsEmplDaysSEN: TIntegerField
      FieldName = 'SEN'
    end
    object cdsEmplDaysADDBANK: TIntegerField
      FieldName = 'ADDBANK'
    end
    object cdsEmplDaysSAT: TIntegerField
      FieldName = 'SAT'
    end
    object cdsEmplDaysRSVBANK: TIntegerField
      FieldName = 'RSVBANK'
    end
    object cdsEmplDaysSHWEEK: TIntegerField
      FieldName = 'SHWEEK'
    end
    object cdsEmplDaysEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
    object cdsEmplDaysTRA: TIntegerField
      FieldName = 'TRA'
    end
  end
  object qryEmpContractHourlyWage: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  HOURLY_WAGE '
      'FROM '
      '  EMPLOYEECONTRACT '
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  STARTDATE <= :FDATE AND'
      '  ((ENDDATE IS NULL) OR ( ENDDATE >= :FDATE))'
      '')
    Left = 560
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FDATE'
        ParamType = ptUnknown
      end>
  end
  object qryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 530
    Top = 56
  end
  object cdsEmpSalary: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 536
    Top = 104
    object cdsEmpSalaryEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmpSalarySALARY_MINUTE: TIntegerField
      FieldName = 'SALARY_MINUTE'
    end
    object cdsEmpSalarySALARY: TFloatField
      FieldName = 'SALARY'
    end
  end
  object cdsEmpExportLine: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'EMPLOYEE_NUMBER;EXPORT_CODE;WORKSPOT_CODE;DAYS_YN'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 536
    Top = 152
    object cdsEmpExportLineEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmpExportLineEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object cdsEmpExportLineWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsEmpExportLineEFFICIENCY: TFloatField
      FieldName = 'EFFICIENCY'
    end
    object cdsEmpExportLineREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object cdsEmpExportLineDAYS_YN: TStringField
      FieldName = 'DAYS_YN'
      Size = 1
    end
    object cdsEmpExportLineAMOUNT: TFloatField
      FieldName = 'AMOUNT'
    end
    object cdsEmpExportLineBONUS_YN: TStringField
      FieldName = 'BONUS_YN'
      Size = 1
    end
    object cdsEmpExportLineEXPORTGROUP: TStringField
      FieldName = 'EXPORTGROUP'
      Size = 30
    end
  end
  object cdsGroupEfficiency: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'PLANT_CODE;WORKSPOT_CODE'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 544
    Top = 200
    object cdsGroupEfficiencyPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsGroupEfficiencyWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsGroupEfficiencyEFFICIENCY: TFloatField
      FieldName = 'EFFICIENCY'
    end
  end
  object cdsHoliday: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 
          'PLANT_CODE;EMPLOYEEAVAILABILITY_DATE;SHIFT_NUMBER;EMPLOYEE_NUMBE' +
          'R'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 544
    Top = 248
    object cdsHolidayEMPLOYEEAVAILABILITY_DATE: TDateTimeField
      FieldName = 'EMPLOYEEAVAILABILITY_DATE'
    end
    object cdsHolidayPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsHolidaySHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsHolidayEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsHolidayEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object cdsHolidayDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object cdsHolidayAVAILABLE_TIMEBLOCK_1: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_1'
      Size = 1
    end
    object cdsHolidayAVAILABLE_TIMEBLOCK_2: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_2'
      Size = 1
    end
    object cdsHolidayAVAILABLE_TIMEBLOCK_3: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_3'
      Size = 1
    end
    object cdsHolidayAVAILABLE_TIMEBLOCK_4: TStringField
      FieldName = 'AVAILABLE_TIMEBLOCK_4'
      Size = 1
    end
  end
  object cdsAbsencetype: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'ABSENCETYPE_CODE'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 544
    Top = 296
    object cdsAbsencetypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Size = 1
    end
    object cdsAbsencetypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
  end
  object tblMaxSalaryEarning: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'MAXSALARYEARNING'
    Left = 592
    Top = 344
  end
  object tblMaxSalaryBalance: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'MAXSALARYBALANCE'
    Left = 520
    Top = 408
  end
  object cdsAbsenceReason: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'ABSENCEREASON_CODE;ABSENCETYPE_CODE'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 592
    Top = 392
    object cdsAbsenceReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Size = 1
    end
    object cdsAbsenceReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Size = 1
    end
  end
  object qryMaxSalaryBalance: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 520
    Top = 360
  end
  object cdsGroupEmployeeEfficiency: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;EMPLOYEE_NUMBER'
      end>
    IndexName = 'byPrimary'
    Params = <>
    StoreDefs = True
    Left = 592
    Top = 448
    object cdsGroupEmployeeEfficiencyPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsGroupEmployeeEfficiencyWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object cdsGroupEmployeeEfficiencyJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object cdsGroupEmployeeEfficiencyEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsGroupEmployeeEfficiencyEFFICIENCY: TFloatField
      FieldName = 'EFFICIENCY'
    end
  end
  object qryDeleteEPayroll: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM EPAYROLL EP'
      'WHERE EP.DATEFROM = :DATEFROM AND'
      'EP.DATETO = :DATETO')
    Left = 48
    Top = 472
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryInsertEPayroll: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO EPAYROLL'
      '('
      '  DATEFROM,'
      '  DATETO,'
      '  EMPLOYEE_NUMBER,'
      '  BUSINESSUNIT_CODE,'
      '  DEPARTMENT_CODE,'
      '  EXPORT_CODE,'
      '  MINUTE,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'VALUES'
      '('
      '  :DATEFROM,'
      '  :DATETO,'
      '  :EMPLOYEE_NUMBER,'
      '  :BUSINESSUNIT_CODE,'
      '  :DEPARTMENT_CODE,'
      '  :EXPORT_CODE,'
      '  :MINUTE,'
      '  :CREATIONDATE,'
      '  :MUTATIONDATE,'
      '  :MUTATOR'
      ')   ')
    Left = 168
    Top = 472
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXPORT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'MINUTE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryExportREINO: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.EMPLOYEE_NUMBER,'
      '  A.DEPT,'
      '  A.HOURTYPE,'
      '  A.SUM_MIN,'
      '  A.INCENTIVE,'
      '  A.HOURLY_WAGE AS PAYRATE'
      'FROM'
      '('
      'SELECT'
      '  P.EMPLOYEE_NUMBER,'
      '  D.DESCRIPTION AS DEPT,'
      '  NVL(EC.HOURLY_WAGE, 0) AS HOURLY_WAGE,'
      '  B.BONUS_FACTOR AS INCENTIVE,'
      '  H.HOURTYPE_NUMBER,'
      '  H.DESCRIPTION AS HOURTYPE,'
      '  SUM(P.PRODUCTION_MINUTE) AS SUM_MIN'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE P INNER JOIN HOURTYPE H ON'
      '    P.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  LEFT JOIN EMPLOYEECONTRACT EC ON'
      '    EC.EMPLOYEE_NUMBER = P.EMPLOYEE_NUMBER AND'
      '    EC.STARTDATE <= P.PRODHOUREMPLOYEE_DATE AND'
      
        '    (EC.ENDDATE IS NULL OR EC.ENDDATE >= P.PRODHOUREMPLOYEE_DATE' +
        ')'
      '  INNER JOIN WORKSPOT W ON'
      '    P.PLANT_CODE = W.PLANT_CODE AND'
      '    P.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  INNER JOIN EMPLOYEE E ON'
      '    P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN BUSINESSUNIT B ON'
      '    E.PLANT_CODE = B.PLANT_CODE'
      '  INNER JOIN DEPARTMENT D ON'
      '    W.PLANT_CODE = D.PLANT_CODE AND'
      '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      'WHERE'
      '  (P.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :DATETO) AND'
      '  (E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO) AND'
      '  (E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO)'
      'GROUP BY'
      '  P.EMPLOYEE_NUMBER,'
      '  D.DESCRIPTION,'
      '  EC.HOURLY_WAGE,'
      '  B.BONUS_FACTOR,'
      '  H.HOURTYPE_NUMBER, H.DESCRIPTION'
      'UNION'
      'SELECT'
      '  A.EMPLOYEE_NUMBER,'
      '  NULL,'
      '  NVL(EC.HOURLY_WAGE, 0) AS HOURLY_WAGE,'
      '  NULL,'
      '  H.HOURTYPE_NUMBER,'
      '  H.DESCRIPTION AS HOURTYPE,'
      '  SUM(A.ABSENCE_MINUTE) AS SUM_ABSENCE_MIN'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON AR ON'
      '    A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID'
      '  LEFT JOIN EMPLOYEECONTRACT EC ON'
      '    EC.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND'
      '    EC.STARTDATE <= A.ABSENCEHOUR_DATE AND'
      '    (EC.ENDDATE IS NULL OR EC.ENDDATE >= A.ABSENCEHOUR_DATE)'
      '  INNER JOIN HOURTYPE H ON'
      '    AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  (A.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :DATETO) AND'
      '  (E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO) AND'
      '  (E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO)'
      'GROUP BY'
      '  A.EMPLOYEE_NUMBER,'
      '  EC.HOURLY_WAGE,'
      '  H.HOURTYPE_NUMBER,'
      '  H.DESCRIPTION'
      'ORDER BY'
      '  1, 2, 5'
      ') A'
      ' '
      ' '
      ' '
      ' ')
    Left = 488
    Top = 472
    ParamData = <
      item
        DataType = ftDate
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end>
  end
  object cdsEmplMins: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'PrimaryIndex'
        Fields = 'EDATE;EMPLOYEE_NUMBER;HOURTYPE_NUMBER'
      end>
    IndexName = 'PrimaryIndex'
    Params = <>
    StoreDefs = True
    Left = 432
    Top = 344
    object cdsEmplMinsEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmplMinsHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object cdsEmplMinsEDATE: TDateField
      FieldName = 'EDATE'
    end
    object cdsEmplMinsMINS: TIntegerField
      FieldName = 'MINS'
    end
    object cdsEmplMinsEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
  end
  object cdsEmplCounters: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'PrimaryIndex'
        Fields = 'EMPLOYEE_NUMBER;HOURTYPE_NUMBER'
      end>
    IndexName = 'PrimaryIndex'
    Params = <>
    StoreDefs = True
    Left = 432
    Top = 392
    object cdsEmplCountersEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsEmplCountersHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
    end
    object cdsEmplCountersMINS: TIntegerField
      FieldName = 'MINS'
    end
  end
  object qryExportTypeAttent: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT C.EXPORT_TYPE'
      'FROM PLANT P INNER JOIN COUNTRY C ON'
      '  P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE '
      '  P.PLANT_CODE = :PLANT_CODE  ')
    Left = 264
    Top = 472
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryEP_ADP_ATTENT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*)'
      'FROM EXPORTPAYROLL T'
      'WHERE T.EXPORT_TYPE = '#39'ADP'#39' OR T.EXPORT_TYPE = '#39'ATTENT'#39
      'HAVING COUNT(*) = 2')
    Left = 376
    Top = 472
  end
  object qryExpCodeCounter: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.EXPORT_CODE '
      'FROM '
      '  ABSENCETYPEPERCOUNTRY T INNER JOIN PLANT P ON'
      '    T.COUNTRY_ID = P.COUNTRY_ID'
      '  INNER JOIN EMPLOYEE E ON'
      '    P.PLANT_CODE = E.PLANT_CODE'
      'WHERE'
      '  T.COUNTER_ACTIVE_YN = '#39'Y'#39' AND'
      '  T.EXPORT_CODE IS NOT NULL AND'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  T.ABSENCETYPE_CODE = :ABSENCETYPE_CODE ')
    Left = 568
    Top = 496
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryHourTypeXXX: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  H.HOURTYPE_NUMBER, H.EXPORT_CODE, H.WAGE_BONUS_ONLY_YN,'
      
        '  H.BONUS_PERCENTAGE, H.OVERTIME_YN, H.MINIMUM_WAGE, H.COUNT_DAY' +
        '_YN,'
      '  H.DESCRIPTION, H.TIME_FOR_TIME_YN, H.BONUS_IN_MONEY_YN'
      'FROM'
      '  HOURTYPE H'
      'WHERE'
      '  H.EXPORT_CODE IS NOT NULL'
      'ORDER BY'
      '  H.HOURTYPE_NUMBER'
      ''
      ' ')
    Left = 48
    Top = 528
  end
  object qryEP_AFAS_SELECT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*)'
      'FROM EXPORTPAYROLL T'
      'WHERE T.EXPORT_TYPE = '#39'AFAS'#39' OR T.EXPORT_TYPE = '#39'SELECT'#39
      'HAVING COUNT(*) = 2'
      ' ')
    Left = 168
    Top = 528
  end
  object qrySELECT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.CONTRACTGROUP_CODE,'
      '  A.CONTRACTGROUP,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.NAME,'
      '  A.HOURTYPE,'
      '  PIMSDAYOFWEEK(A.PRODHOUREMPLOYEE_DATE) DAY,'
      '  A.DEPARTMENT,'
      '  SUM(A.PRODUCTION_MINUTE) PRODMIN'
      'FROM'
      '('
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION CONTRACTGROUP,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION NAME,'
      '  D.DESCRIPTION DEPARTMENT,'
      '  H.DESCRIPTION HOURTYPE,'
      '  PET.PRODHOUREMPLOYEE_DATE,'
      '  PET.PRODUCTION_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE PET INNER JOIN EMPLOYEE E ON'
      '  PET.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN WORKSPOT W ON'
      '  PET.PLANT_CODE = W.PLANT_CODE AND'
      '  PET.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  INNER JOIN DEPARTMENT D ON'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '  E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  INNER JOIN HOURTYPE H ON'
      '  PET.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  ) AND'
      '  ('
      
        '    (:PLANT_EMP_LEVEL = '#39'1'#39' AND E.PLANT_CODE >= :PLANTFROM AND E' +
        '.PLANT_CODE <= :PLANTTO)'
      '    OR'
      
        '    (:PLANT_EMP_LEVEL = '#39'0'#39' AND D.PLANT_CODE >= :PLANTFROM AND D' +
        '.PLANT_CODE <= :PLANTTO)'
      '  ) AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM AND'
      '  E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  PET.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  PET.PRODHOUREMPLOYEE_DATE <= :DATETO'
      'UNION ALL'
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  D.DESCRIPTION,'
      '  H.DESCRIPTION,'
      '  SHE.SALARY_DATE,'
      '  SHE.SALARY_MINUTE'
      'FROM SALARYHOURPEREMPLOYEE SHE INNER JOIN EMPLOYEE E ON'
      '  SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN DEPARTMENT D ON'
      '  E.PLANT_CODE = D.PLANT_CODE AND'
      '  E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '  E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  INNER JOIN HOURTYPE H ON'
      '  SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  ) AND'
      '  ('
      
        '    (:PLANT_EMP_LEVEL = '#39'1'#39' AND E.PLANT_CODE >= :PLANTFROM AND E' +
        '.PLANT_CODE <= :PLANTTO)'
      '    OR'
      
        '    (:PLANT_EMP_LEVEL = '#39'0'#39' AND D.PLANT_CODE >= :PLANTFROM AND D' +
        '.PLANT_CODE <= :PLANTTO)'
      '  ) AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM AND'
      '  E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  SHE.MANUAL_YN = '#39'Y'#39' AND'
      '  SHE.SALARY_DATE >= :DATEFROM AND'
      '  SHE.SALARY_DATE <= :DATETO'
      'ORDER BY'
      '  NAME, PRODHOUREMPLOYEE_DATE'
      ') A'
      'GROUP BY'
      '  A.CONTRACTGROUP_CODE,'
      '  A.CONTRACTGROUP,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.NAME,'
      '  A.HOURTYPE,'
      '  PIMSDAYOFWEEK(A.PRODHOUREMPLOYEE_DATE),'
      '  A.DEPARTMENT'
      'HAVING SUM(A.PRODUCTION_MINUTE) <> 0'
      'ORDER BY'
      '  NAME, A.DEPARTMENT, HOURTYPE, DAY'
      ''
      ' '
      ' ')
    Left = 272
    Top = 528
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qrySELECT_FILTER: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  B.CONTRACTGROUP_CODE,'
      '  B.CONTRACTGROUP,'
      '  B.EMPLOYEE_NUMBER,'
      '  B.NAME,'
      '  B.DEPARTMENT,'
      '  B.HOURTYPE'
      'FROM'
      '('
      'SELECT'
      '  A.CONTRACTGROUP_CODE,'
      '  A.CONTRACTGROUP,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.NAME,'
      '  A.HOURTYPE,'
      '  PIMSDAYOFWEEK(A.PRODHOUREMPLOYEE_DATE) DAY,'
      '  A.DEPARTMENT,'
      '  SUM(A.PRODUCTION_MINUTE) PRODMIN'
      'FROM'
      '('
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION CONTRACTGROUP,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION NAME,'
      '  D.DESCRIPTION DEPARTMENT,'
      '  H.DESCRIPTION HOURTYPE,'
      '  PET.PRODHOUREMPLOYEE_DATE,'
      '  PET.PRODUCTION_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLPERTYPE PET INNER JOIN EMPLOYEE E ON'
      '  PET.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN WORKSPOT W ON'
      '  PET.PLANT_CODE = W.PLANT_CODE AND'
      '  PET.WORKSPOT_CODE = W.WORKSPOT_CODE'
      '  INNER JOIN DEPARTMENT D ON'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '  E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  INNER JOIN HOURTYPE H ON'
      '  PET.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  ) AND'
      '  ('
      
        '    (:PLANT_EMP_LEVEL = '#39'1'#39' AND E.PLANT_CODE >= :PLANTFROM AND E' +
        '.PLANT_CODE <= :PLANTTO)'
      '    OR'
      
        '    (:PLANT_EMP_LEVEL = '#39'0'#39' AND D.PLANT_CODE >= :PLANTFROM AND D' +
        '.PLANT_CODE <= :PLANTTO)'
      '  ) AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM AND'
      '  E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  PET.PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  PET.PRODHOUREMPLOYEE_DATE <= :DATETO'
      'UNION ALL'
      'SELECT'
      '  CG.CONTRACTGROUP_CODE,'
      '  CG.DESCRIPTION,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  D.DESCRIPTION,'
      '  H.DESCRIPTION,'
      '  SHE.SALARY_DATE,'
      '  SHE.SALARY_MINUTE'
      'FROM SALARYHOURPEREMPLOYEE SHE INNER JOIN EMPLOYEE E ON'
      '  SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN DEPARTMENT D ON'
      '  E.PLANT_CODE = D.PLANT_CODE AND'
      '  E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '  INNER JOIN CONTRACTGROUP CG ON'
      '  E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  INNER JOIN HOURTYPE H ON'
      '  SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  ) AND'
      '  ('
      
        '    (:PLANT_EMP_LEVEL = '#39'1'#39' AND E.PLANT_CODE >= :PLANTFROM AND E' +
        '.PLANT_CODE <= :PLANTTO)'
      '    OR'
      
        '    (:PLANT_EMP_LEVEL = '#39'0'#39' AND D.PLANT_CODE >= :PLANTFROM AND D' +
        '.PLANT_CODE <= :PLANTTO)'
      '  ) AND'
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM AND'
      '  E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  SHE.MANUAL_YN = '#39'Y'#39' AND'
      '  SHE.SALARY_DATE >= :DATEFROM AND'
      '  SHE.SALARY_DATE <= :DATETO'
      'ORDER BY'
      '  NAME, PRODHOUREMPLOYEE_DATE'
      ') A'
      'GROUP BY'
      '  A.CONTRACTGROUP_CODE,'
      '  A.CONTRACTGROUP,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.NAME,'
      '  A.HOURTYPE,'
      '  PIMSDAYOFWEEK(A.PRODHOUREMPLOYEE_DATE),'
      '  A.DEPARTMENT'
      'HAVING SUM(A.PRODUCTION_MINUTE) <> 0'
      'ORDER BY'
      '  NAME, A.DEPARTMENT, HOURTYPE, DAY'
      ') B'
      'GROUP BY'
      '  B.CONTRACTGROUP_CODE,'
      '  B.CONTRACTGROUP,'
      '  B.EMPLOYEE_NUMBER,'
      '  B.NAME,'
      '  B.DEPARTMENT,'
      '  B.HOURTYPE'
      ''
      ''
      ''
      ''
      ' ')
    Left = 352
    Top = 528
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_EMP_LEVEL'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAFAS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.EMPLOYEE_NUMBER,'
      '  A.CUSTOMER_NUMBER,'
      '  A.EXPORT_CODE,'
      '  SUM(A.SUMWK_HRS) SUMWK_HRS'
      'FROM'
      '('
      '  SELECT'
      '    S.EMPLOYEE_NUMBER,'
      '    P.CUSTOMER_NUMBER,'
      '    H.EXPORT_CODE,'
      '    CG.EXPORT_NORMAL_HRS_YN,'
      '    S.HOURTYPE_NUMBER,'
      '    '#39'S'#39' ET,'
      '    CASE'
      
        '      WHEN CG.EXPORT_NORMAL_HRS_YN = '#39'N'#39' AND S.HOURTYPE_NUMBER =' +
        ' 1 THEN 0'
      '      ElSE'
      '        SUM(S.SALARY_MINUTE)'
      '    END SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '    INNER JOIN PLANT P ON'
      '      E.PLANT_CODE = P.PLANT_CODE'
      '    INNER JOIN DEPARTMENT D ON'
      '      E.PLANT_CODE = D.PLANT_CODE AND'
      '      E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '    INNER JOIN CONTRACTGROUP CG ON'
      '      E.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM'
      '    AND D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO'
      '    AND D.DEPARTMENT_CODE >= :DEPARTMENTFROM'
      '    AND D.DEPARTMENT_CODE <= :DEPARTMENTTO'
      '    AND E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM'
      '    AND E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      '  GROUP BY'
      '    S.EMPLOYEE_NUMBER,'
      '    P.CUSTOMER_NUMBER,'
      '    H.EXPORT_CODE,'
      '    CG.EXPORT_NORMAL_HRS_YN,'
      '    S.HOURTYPE_NUMBER,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    A.EMPLOYEE_NUMBER,'
      '    P.CUSTOMER_NUMBER,'
      '    H.EXPORT_CODE,'
      '    '#39'N'#39','
      '    0,'
      '    '#39'S'#39','
      '    SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN PLANT P ON'
      '      E.PLANT_CODE = P.PLANT_CODE'
      '    INNER JOIN DEPARTMENT D ON'
      '      E.PLANT_CODE = D.PLANT_CODE AND'
      '      E.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND D.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM'
      '    AND D.BUSINESSUNIT_CODE <= :BUSINESSUNITTO'
      '    AND D.DEPARTMENT_CODE >= :DEPARTMENTFROM'
      '    AND D.DEPARTMENT_CODE <= :DEPARTMENTTO'
      '    AND E.CONTRACTGROUP_CODE >= :CONTRACTGROUPFROM'
      '    AND E.CONTRACTGROUP_CODE <= :CONTRACTGROUPTO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    A.EMPLOYEE_NUMBER,'
      '    P.CUSTOMER_NUMBER,'
      '    H.EXPORT_CODE,'
      '    NULL,'
      '    NULL,'
      '    NULL'
      '  ORDER BY'
      '    EMPLOYEE_NUMBER, EXPORT_CODE'
      ') A'
      'GROUP BY'
      '  A.EMPLOYEE_NUMBER,'
      '  A.CUSTOMER_NUMBER,'
      '  A.EXPORT_CODE'
      ' ')
    Left = 432
    Top = 528
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'CONTRACTGROUPTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryExportGroupSR: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT E.FREETEXT '
      'FROM EMPLOYEE E '
      'WHERE E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 472
    Top = 552
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryEmpIsExternal: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EC.EMPLOYEE_NUMBER, EC.STARTDATE,'
      '  NVL(EC.ENDDATE, TO_DATE('#39'31-12-2099'#39', '#39'dd-mm-yyyy'#39')) ENDDATE,'
      '  EC.CONTRACT_TYPE'
      'FROM'
      '  EMPLOYEECONTRACT EC'
      'ORDER BY'
      '  EC.EMPLOYEE_NUMBER,'
      '  EC.STARTDATE DESC'
      ''
      ' '
      ' ')
    Left = 632
    Top = 544
  end
  object qryUpdateSwipe: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE TIMEREGSCANNING T'
      'SET'
      '  T.EXPORTED_YN = '#39'Y'#39
      'WHERE'
      '  T.EXPORTED_YN = '#39'N'#39
      '  AND NVL(T.SHIFT_DATE, T.DATETIME_IN) < :TILL_DATE'
      '  AND T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' '
      ' '
      ' ')
    Left = 624
    Top = 56
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'TILL_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryUpdateSchedule: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY T'
      'SET'
      '  T.EXPORTED_YN = '#39'Y'#39
      'WHERE'
      '  T.EXPORTED_YN = '#39'N'#39
      '  AND T.EMPLOYEEAVAILABILITY_DATE < :TILL_DATE'
      '  AND T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 624
    Top = 112
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'TILL_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryReadLastExportDate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT LASTEXPORTDATE'
      'FROM EXPORTPAYROLL'
      'WHERE EXPORT_TYPE = :EXPORT_TYPE ')
    Left = 624
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryUpdateLastExportDate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EXPORTPAYROLL'
      'SET LASTEXPORTDATE = TRUNC(SYSDATE)'
      'WHERE EXPORT_TYPE = :EXPORT_TYPE ')
    Left = 632
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryReadLastSequenceNr: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.SEQUENCE_NUMBER'
      'FROM EXPORTPAYROLL T'
      'WHERE T.EXPORT_TYPE = :EXPORT_TYPE')
    Left = 672
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryUpdateLastSequenceNr: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'UPDATE EXPORTPAYROLL T'
      'SET T.SEQUENCE_NUMBER = :SEQUENCE_NUMBER'
      'WHERE T.EXPORT_TYPE = :EXPORT_TYPE')
    Left = 672
    Top = 336
    ParamData = <
      item
        DataType = ftInteger
        Name = 'SEQUENCE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EXPORT_TYPE'
        ParamType = ptUnknown
      end>
  end
  object qryWMU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.EMPLOYEE_NUMBER,'
      '  A.EXPORT_CODE,'
      '  A.HOURTYPE_NUMBER,'
      '  A.ET,'
      '  A.SUMWK_HRS'
      'FROM'
      '('
      '  SELECT'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    S.HOURTYPE_NUMBER,'
      '    '#39'1'#39' ET,'
      '    SUM(S.SALARY_MINUTE) SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    S.HOURTYPE_NUMBER = 1 AND'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      '  GROUP BY'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    S.HOURTYPE_NUMBER,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    S.HOURTYPE_NUMBER,'
      '    '#39'2'#39' ET,'
      '    SUM(S.SALARY_MINUTE) SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    S.HOURTYPE_NUMBER <> 1 AND'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      
        '    AND S.HOURTYPE_NUMBER IN (SELECT OD.HOURTYPE_NUMBER FROM OVE' +
        'RTIMEDEFINITION OD WHERE OD.CONTRACTGROUP_CODE = E.CONTRACTGROUP' +
        '_CODE)'
      '  GROUP BY'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    S.HOURTYPE_NUMBER,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    A.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    H.HOURTYPE_NUMBER,'
      '    '#39'3'#39','
      '    SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    A.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    H.HOURTYPE_NUMBER,'
      '    NULL'
      ') A'
      'ORDER BY A.EMPLOYEE_NUMBER, A.ET'
      ' '
      ' ')
    Left = 672
    Top = 400
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryQBOOKS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.ADATE,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.DEPARTMENT_CODE,'
      '  A.EXPORT_CODE,'
      '  A.SUMWK_HRS'
      'FROM'
      '('
      '  SELECT'
      '    S.SALARY_DATE AS ADATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    E.DEPARTMENT_CODE,'
      '    H.EXPORT_CODE,'
      '    '#39'1'#39' ET,'
      '    SUM(S.SALARY_MINUTE) SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      '  GROUP BY'
      '    S.SALARY_DATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    E.DEPARTMENT_CODE,'
      '    H.EXPORT_CODE,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    A.ABSENCEHOUR_DATE AS ADATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DEPARTMENT_CODE,'
      '    H.EXPORT_CODE,'
      '    '#39'3'#39','
      '    SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    A.ABSENCEHOUR_DATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DEPARTMENT_CODE,'
      '    H.EXPORT_CODE,'
      '    NULL'
      ') A'
      'ORDER BY A.ADATE, A.EMPLOYEE_NUMBER, A.EXPORT_CODE'
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 672
    Top = 456
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryPCHEX: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.ADATE,'
      '  A.DESCRIPTION,'
      '  A.EXPORT_CODE,'
      '  A.BONUS_PERCENTAGE,'
      '  A.SUMWK_HRS,'
      '  A.ET'
      'FROM'
      '('
      '  SELECT'
      '    E.PLANT_CODE,'
      '    S.SALARY_DATE AS ADATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    H.EXPORT_CODE,'
      '    H.BONUS_PERCENTAGE,'
      '    '#39'1'#39' ET,'
      '    SUM(S.SALARY_MINUTE) SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      '  GROUP BY'
      '    E.PLANT_CODE,'
      '    S.SALARY_DATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    H.EXPORT_CODE,'
      '    H.BONUS_PERCENTAGE,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    E.PLANT_CODE,'
      '    A.ABSENCEHOUR_DATE AS ADATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    H.EXPORT_CODE,'
      '    H.BONUS_PERCENTAGE,'
      '    '#39'2'#39','
      '    SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    E.PLANT_CODE,'
      '    A.ABSENCEHOUR_DATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    H.EXPORT_CODE,'
      '    H.BONUS_PERCENTAGE,'
      '    NULL'
      '  UNION ALL'
      '    SELECT'
      '      E.PLANT_CODE,'
      '      EP.PAYMENT_DATE,'
      '      EP.EMPLOYEE_NUMBER,'
      '      E.DESCRIPTION,'
      '      EP.PAYMENT_EXPORT_CODE,'
      '      0,'
      '      '#39'3'#39','
      '      SUM(EP.PAYMENT_AMOUNT)'
      '    FROM EXTRAPAYMENT EP INNER JOIN EMPLOYEE E ON'
      '      EP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '      INNER JOIN PAYMENTEXPORTCODE PE ON'
      '      EP.PAYMENT_EXPORT_CODE = PE.PAYMENT_EXPORT_CODE'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    EP.PAYMENT_EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND EP.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND EP.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND EP.PAYMENT_DATE >= :DATEFROM'
      '    AND EP.PAYMENT_DATE <= :DATETO'
      '  GROUP BY'
      '    E.PLANT_CODE,'
      '    EP.PAYMENT_DATE,'
      '    EP.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    EP.PAYMENT_EXPORT_CODE,'
      '    NULL,'
      '    NULL'
      ') A'
      'ORDER BY A.ADATE, A.EMPLOYEE_NUMBER, A.EXPORT_CODE'
      ' '
      ' ')
    Left = 672
    Top = 512
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryNAVIS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.SALARY_DATE AS ADATE,'
      '  S.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  (SELECT SUM(S2.SALARY_MINUTE) FROM SALARYHOURPEREMPLOYEE S2'
      
        '   WHERE S2.SALARY_DATE = S.SALARY_DATE AND S2.EMPLOYEE_NUMBER =' +
        ' S.EMPLOYEE_NUMBER AND S2.HOURTYPE_NUMBER = 1) REGULAR_MINUTE,'
      
        '  (SELECT SUM(S2.SALARY_MINUTE) FROM SALARYHOURPEREMPLOYEE S2 IN' +
        'NER JOIN HOURTYPE H ON S2.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      
        '   WHERE H.HOURTYPE_NUMBER <> 1 AND H.OVERTIME_YN = '#39'Y'#39' AND H.BO' +
        'NUS_PERCENTAGE = 50'
      
        '   AND S2.SALARY_DATE = S.SALARY_DATE AND S2.EMPLOYEE_NUMBER = S' +
        '.EMPLOYEE_NUMBER) OT15_MINUTE,'
      
        '  (SELECT SUM(S2.SALARY_MINUTE) FROM SALARYHOURPEREMPLOYEE S2 IN' +
        'NER JOIN HOURTYPE H ON S2.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      
        '   WHERE H.HOURTYPE_NUMBER <> 1 AND H.OVERTIME_YN = '#39'Y'#39' AND H.BO' +
        'NUS_PERCENTAGE = 100'
      
        '   AND S2.SALARY_DATE = S.SALARY_DATE AND S2.EMPLOYEE_NUMBER = S' +
        '.EMPLOYEE_NUMBER) OT2_MINUTE,'
      '   0 AS ABSENCE_MINUTE,'
      '  '#39'0'#39' AS PAYCODE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN HOURTYPE H ON'
      '    S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NAM' +
        'E = :USER_NAME))'
      '  )'
      '  AND E.PLANT_CODE >= :PLANTFROM'
      '  AND E.PLANT_CODE <= :PLANTTO'
      '  AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '  AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '  AND S.SALARY_DATE >= :DATEFROM'
      '  AND S.SALARY_DATE <= :DATETO'
      'GROUP BY '
      '  S.EMPLOYEE_NUMBER,'
      '  S.SALARY_DATE,'
      '  E.DESCRIPTION    '
      'UNION ALL'
      '  SELECT'
      '    A.ABSENCEHOUR_DATE AS ADATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION,'
      '    0 as REGULAR_MINUTE,'
      '    0 as OT15_MINUTE,'
      '    0 as OT2_MINUTE,'
      '    SUM(A.ABSENCE_MINUTE) AS ABSENCE_MINUTE,'
      '    '#39'11'#39' AS PAYCODE'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    A.ABSENCEHOUR_DATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    E.DESCRIPTION'
      'ORDER by 2, 1')
    Left = 688
    Top = 13
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryBAMBOO: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.ADATE,'
      '  A.EMPLOYEE_NUMBER,'
      '  A.EXPORT_CODE,'
      '  A.SUMWK_HRS'
      'FROM'
      '('
      '  SELECT'
      '    S.SALARY_DATE AS ADATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    '#39'1'#39' ET,'
      '    SUM(S.SALARY_MINUTE) SUMWK_HRS'
      '  FROM'
      '    SALARYHOURPEREMPLOYEE S INNER JOIN EMPLOYEE E ON'
      '      S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN HOURTYPE H ON'
      '      S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND S.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND S.SALARY_DATE >= :DATEFROM'
      '    AND S.SALARY_DATE <= :DATETO'
      '  GROUP BY'
      '    S.SALARY_DATE,'
      '    S.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    NULL'
      '  UNION ALL'
      '  SELECT'
      '    A.ABSENCEHOUR_DATE AS ADATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    '#39'3'#39','
      '    SUM(A.ABSENCE_MINUTE) AS SUMWK_HRS'
      '  FROM'
      '    ABSENCEHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON'
      '      A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '    INNER JOIN ABSENCEREASON AR ON'
      '      AR.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      '    INNER JOIN ABSENCETYPE ABT ON'
      '      AR.ABSENCETYPE_CODE = ABT.ABSENCETYPE_CODE'
      '    INNER JOIN HOURTYPE H ON'
      '      AR.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  WHERE'
      '    ('
      '      (:USER_NAME = '#39'*'#39') OR'
      '      (E.TEAM_CODE IN'
      
        '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_N' +
        'AME = :USER_NAME))'
      '    ) AND'
      '    H.EXPORT_CODE IS NOT NULL'
      '    AND E.PLANT_CODE >= :PLANTFROM'
      '    AND E.PLANT_CODE <= :PLANTTO'
      '    AND A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM'
      '    AND A.EMPLOYEE_NUMBER <= :EMPLOYEETO'
      '    AND A.ABSENCEHOUR_DATE >= :DATEFROM'
      '    AND A.ABSENCEHOUR_DATE <= :DATETO'
      '  GROUP BY'
      '    A.ABSENCEHOUR_DATE,'
      '    A.EMPLOYEE_NUMBER,'
      '    H.EXPORT_CODE,'
      '    NULL'
      ') A'
      'ORDER BY A.ADATE, A.EMPLOYEE_NUMBER, A.EXPORT_CODE'
      ''
      ''
      ''
      ''
      ''
      ''
      ' ')
    Left = 744
    Top = 456
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
