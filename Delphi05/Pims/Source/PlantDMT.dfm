inherited PlantDM: TPlantDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 248
  Top = 156
  Height = 479
  Width = 747
  inherited TableMaster: TTable
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableMasterPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableMasterFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    BeforeDelete = TableDetailBeforeDelete
    AfterScroll = TableDetailAfterScroll
    OnCalcFields = TableDetailCalcFields
    OnFilterRecord = TableDetailFilterRecord
    TableName = 'PLANT'
    UpdateMode = upWhereKeyOnly
    Left = 100
    Top = 116
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 6
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object TableDetailZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object TableDetailCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableDetailSTATE: TStringField
      FieldName = 'STATE'
    end
    object TableDetailPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object TableDetailFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailAVERAGE_WAGE: TFloatField
      FieldName = 'AVERAGE_WAGE'
    end
    object TableDetailCUSTOMER_NUMBER: TIntegerField
      FieldName = 'CUSTOMER_NUMBER'
    end
    object TableDetailCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
    end
    object TableDetailCOUNTRYLU: TStringField
      FieldKind = fkLookup
      FieldName = 'COUNTRYLU'
      LookupDataSet = qryCountry
      LookupKeyFields = 'COUNTRY_ID'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'COUNTRY_ID'
      Size = 30
      Lookup = True
    end
    object TableDetailTIMEZONE: TMemoField
      FieldName = 'TIMEZONE'
      BlobType = ftMemo
      Size = 400
    end
    object TableDetailTIMEZONELU: TStringField
      FieldKind = fkLookup
      FieldName = 'TIMEZONELU'
      LookupDataSet = SystemDM.cdsTimeZone
      LookupKeyFields = 'TIMEZONE'
      LookupResultField = 'TIMEZONE'
      KeyFields = 'TIMEZONE'
      Size = 100
      Lookup = True
    end
    object TableDetailTIMEZONECALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'TIMEZONECALC'
      Size = 80
      Calculated = True
    end
    object TableDetailTIMEZONEHRSDIFF: TSmallintField
      FieldName = 'TIMEZONEHRSDIFF'
    end
    object TableDetailCUTOFFTIMESHIFTDATE: TDateTimeField
      FieldName = 'CUTOFFTIMESHIFTDATE'
    end
    object TableDetailREALTIMEINTERVAL: TFloatField
      Alignment = taLeftJustify
      FieldName = 'REALTIMEINTERVAL'
    end
    object TableDetailSTAFFPLAN_SHOWTOT_YN: TStringField
      FieldName = 'STAFFPLAN_SHOWTOT_YN'
      Size = 4
    end
  end
  inherited DataSourceDetail: TDataSource
    Top = 116
  end
  object StoredProcDelete: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'PLANT_DELETECASCADE'
    Left = 96
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end>
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE '
      'FROM '
      '  PLANT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE')
    Left = 96
    Top = 224
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object StoredProcUpdateNewPlant: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'PLANT_UPDATECASCADE'
    Left = 208
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'OLD_PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NEW_PLANT'
        ParamType = ptInput
      end>
  end
  object StoredProcDelOldPlant: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'PLANT_DELETE_OLDPLANT'
    Left = 208
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'OLD_PLANT'
        ParamType = ptInput
      end>
  end
  object QueryExec: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 80
    Top = 288
  end
  object qryCountry: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNTRY_ID, CODE, DESCRIPTION'
      'FROM COUNTRY')
    Left = 336
    Top = 120
  end
  object StoredProcNEWPlant: TStoredProc
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    StoredProcName = 'PLANT_INSERTNEWPLANT'
    Left = 208
    Top = 176
    ParamData = <
      item
        DataType = ftString
        Name = 'OLD_PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NEW_PLANT'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'DESCRIPTION'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ADDRESS'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'ZIPCODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CITY'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'STATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PHONE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'FAX'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'INSCAN_MARGIN_EARLY'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'INSCAN_MARGIN_LATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'MUTATIONDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'OUTSCAN_MARGIN_EARLY'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'OUTSCAN_MARGIN_LATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'AVERAGE_WAGE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'CUSTOMER_NUMBER'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'COUNTRY_ID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TIMEZONE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'TIMEZONEHRSDIFF'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CUTOFFTIMESHIFTDATE'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'REALTIMEINTERVAL'
        ParamType = ptInput
      end>
  end
  object TableFinalRun: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDetail
    TableName = 'FINALRUN'
    Left = 332
    Top = 48
    object TableFinalRunPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableFinalRunEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Required = True
      Size = 6
    end
    object TableFinalRunEXPORT_DATE: TDateTimeField
      FieldName = 'EXPORT_DATE'
      Required = True
    end
    object TableFinalRunCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableFinalRunMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableFinalRunMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableFinalRunPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = tblPlantLU
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      Size = 30
      Lookup = True
    end
  end
  object DataSourceFinalRun: TDataSource
    DataSet = TableFinalRun
    Left = 432
    Top = 48
  end
  object tblPlantLU: TTable
    OnCalcFields = TableExportCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 436
    Top = 108
    object tblPlantLUPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object tblPlantLUDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
end
