(*
  MRA:27-JAN-2009 RV021.
    Team selection correction for FillWK-procedure, to prevent multiple
    the same records.
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  MRA:26-AUG-2010. RV067.MRA.9. Bugfix.
  - Add scheme-name when counting column-names for PIVOTWKPEREMP,
    in case there are multiple schemes.
    NOTE: When there is only PIMS-scheme there is no problem.
          But when there are schemes like: PIMS, PIMSPCO, PIMSCLF, PIMSRFL...
          it gave a problem.
  - Also: The 'ALTER TABLE ADD <column>'-command was wrong!
  MRA:19-NOV-2010. RV080.5.
  - It gives an error-message:
    General SQL error.
    ORA-00902: Invalid datatype.
*)
unit DialogWorkspotPerEmployeeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, StaffPlanningUnit, Provider, DBClient;

const Max_WK_Number = 401;

type

  TDialogWorkspotPerEmployeeDM = class(TGridBaseDM)
    QueryWork: TQuery;
    QueryTeam: TQuery;
    ClientDataSetTeam: TClientDataSet;
    DataSetProviderTeam: TDataSetProvider;
    QueryPlant: TQuery;
    ClientDataSetPlant: TClientDataSet;
    DataSetProviderPlant: TDataSetProvider;
    TablePivot: TTable;
    QueryEmpl: TQuery;
    ClientDataSetEmpl: TClientDataSet;
    DataSetProviderEmpl: TDataSetProvider;
    StoredProcInsertEMP: TStoredProc;
    ClientDataSetWKPerEmpl: TClientDataSet;
    DataSetProviderWKPerEmpl: TDataSetProvider;
    ClientDataSetWK: TClientDataSet;
    DataSetProviderWK: TDataSetProvider;
    DataSourcePIVOT: TDataSource;
    ClientDataSetSave: TClientDataSet;
    DataSetProviderSave: TDataSetProvider;
    ClientDataSetSaveEMPLOYEE_NUMBER: TIntegerField;
    ClientDataSetSaveWORKSPOT_CODE: TStringField;
    ClientDataSetSaveDEPARTMENT_CODE: TStringField;
    QueryCountColumn: TQuery;
    QueryUpdateDB: TQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure TablePivotAfterScroll(DataSet: TDataSet);
    procedure QueryPlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
 
    FEmpl_Filled: Integer;

    FEmplDesc_Filled: String;
    procedure FillWK;
    procedure FillEmployee;
    procedure FillEmpLevel;
    procedure InsertIntoEMPPivot;
    procedure FillPerEmplPIVOTTable(Empl: Integer;
      Name: String;  EditTable: Boolean;  var UpdateTable: Boolean);
    procedure FillPIVOTTable;
    procedure FillEmpl_PIVOTTable(EmplLast: Integer; LastDesc: String);

  public
    { Public declarations }
    
    function FillWorkspotPerEmployee: Boolean;
    function GetWkCode: String;
    procedure SaveChanges;
    procedure Insert_SaveClientDataSet(Empl: Integer; Wk, Dept: String);
  end;

var
  DialogWorkspotPerEmployeeDM: TDialogWorkspotPerEmployeeDM;

implementation

{$R *.DFM}
uses UPimsConst, SystemDMT, ListProcsFRM, UPimsMessageRes,
  WorkspotPerEmployeeFRM;

function TDialogWorkspotPerEmployeeDM.GetWkCode: String;
begin
  Result :=
    DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('WKCODE').AsString;
  if Result = DummyStr then
    Result :=
      DialogWorkspotPerEmployeeDM.ClientDataSetWK.FieldByName('DEPTCODE').AsString;
end;

procedure TDialogWorkspotPerEmployeeDM.FillWK;
var
  SelectStr: String;
begin
  // MR:14-02-2006 Select-statement changed so it is easier to read.
{
  SelectStr :=
    'SELECT ' + NL +
    '  DISTINCT W.WORKSPOT_CODE AS WKCODE, ' + NL +
    '  CAST(W.SHORT_NAME AS VARCHAR(30)) AS DESCRIPTION, ' +  NL +
//    ' W.SHORT_NAME AS DESCRIPTION, ' +
    '  W.DESCRIPTION AS LONGDESC, D.DEPARTMENT_CODE AS DEPTCODE,  ' +  NL +
    '  D.PLAN_ON_WORKSPOT_YN ' + NL +
    'FROM ' + NL +
    '  WORKSPOT W, DEPARTMENT D ' ;
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then
  begin
    SelectStr := SelectStr + ', DEPARTMENTPERTEAM T';
    //car 550279
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', TEAMPERUSER U WHERE '+
        ' T.TEAM_CODE = U.TEAM_CODE AND ' +
        ' U.USER_NAME = :USER_NAME AND '
    else
      SelectStr := SelectStr + ' WHERE ' ;
    SelectStr := SelectStr +
      ' T.TEAM_CODE >= :TEAMFROM AND T.TEAM_CODE <= :TEAMTO ' +
      ' AND T.PLANT_CODE = :PLANT_CODE AND ' +
      ' T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ';
  end
  else
  begin
    //car 550279
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', DEPARTMENTPERTEAM T, TEAMPERUSER U WHERE '+
        ' T.TEAM_CODE = U.TEAM_CODE AND ' +
        ' U.USER_NAME = :USER_NAME AND ' +
        ' T.PLANT_CODE = :PLANT_CODE AND ' +
        ' T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND '
    else
      SelectStr := SelectStr + ' WHERE ';
  end;
  SelectStr := SelectStr + ' D.PLANT_CODE = :PLANT_CODE AND ' +
    ' W.PLANT_CODE = :PLANT_CODE AND ' +
    ' D.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ' +
    ' D.PLAN_ON_WORKSPOT_YN = ''Y''';

//department table
  SelectStr := SelectStr + ' UNION  ' +
    ' SELECT DISTINCT CAST(''0'' AS VARCHAR(6)) AS WKCODE, '+
//    ' SELECT DISTINCT ''0'' AS WKCODE, '+
    ' D.DESCRIPTION AS DESCRIPTION, D.DESCRIPTION AS LONGDESC,' +
    ' D.DEPARTMENT_CODE AS DEPTCODE, D.PLAN_ON_WORKSPOT_YN FROM DEPARTMENT D';
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then
  begin
     SelectStr := SelectStr + ', DEPARTMENTPERTEAM T ';
       //car 550279
     if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
       SelectStr := SelectStr + ', TEAMPERUSER U WHERE '+
         ' T.TEAM_CODE = U.TEAM_CODE AND ' +
         ' U.USER_NAME = :USER_NAME AND '
     else
       SelectStr := SelectStr + ' WHERE ' ;

     SelectStr := SelectStr +
       ' T.TEAM_CODE >= :TEAMFROM AND T.TEAM_CODE <= :TEAMTO' +
       ' AND T.PLANT_CODE = :PLANT_CODE AND ' +
       ' T.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND';
  end
  else
  begin
    //car 550279
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr + ', DEPARTMENTPERTEAM T, TEAMPERUSER U WHERE '+
        ' T.TEAM_CODE = U.TEAM_CODE AND ' +
        ' U.USER_NAME = :USER_NAME AND ' +
        ' T.PLANT_CODE = :PLANT_CODE AND ' +
        ' T.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND '
    else
      SelectStr := SelectStr + ' WHERE ';
  end;
  SelectStr := SelectStr + ' D.PLANT_CODE =  :PLANT_CODE AND ' +
    'D.PLAN_ON_WORKSPOT_YN = ''N''';
}
  SelectStr :=
    'SELECT ' + NL +
    '  DISTINCT W.WORKSPOT_CODE AS WKCODE, ' + NL +
    '  CAST(W.SHORT_NAME AS VARCHAR(30)) AS DESCRIPTION, ' + NL +
    '  W.DESCRIPTION AS LONGDESC, D.DEPARTMENT_CODE AS DEPTCODE, ' + NL +
    '  D.PLAN_ON_WORKSPOT_YN ' + NL +
    'FROM ' + NL +
    '  WORKSPOT W INNER JOIN DEPARTMENT D ON ' + NL +
    '    W.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
    '  LEFT JOIN DEPARTMENTPERTEAM T ON ' + NL +
    '    W.PLANT_CODE = T.PLANT_CODE AND ' + NL +
    '    W.DEPARTMENT_CODE = T.DEPARTMENT_CODE ' + NL +
(* RV021.
    '  LEFT JOIN TEAMPERUSER U ON ' + NL +
    '    T.TEAM_CODE = U.TEAM_CODE ' + NL +
*)
    'WHERE ' + NL +
(* RV021.
    '  ((:USER_NAME = ''*'') OR (U.USER_NAME = :USER_NAME)) AND ' + NL +
*)
    '  ( ' + NL +
    '    (:USER_NAME = ''*'') OR ' + NL +
    '    (T.TEAM_CODE IN ' + NL +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '  ) AND ' + NL +
    '  ((:TEAMFROM = ''*'') OR ' + NL +
    '   (T.TEAM_CODE >= :TEAMFROM AND T.TEAM_CODE <= :TEAMTO)) AND ' + NL +
    '  W.PLANT_CODE = :PLANT_CODE AND ' + NL +
    '  D.PLAN_ON_WORKSPOT_YN = ''Y'' ' + NL +
    'UNION ' + NL +
    '  SELECT ' + NL +
    '    DISTINCT CAST(''0'' AS VARCHAR(6)) AS WKCODE, ' + NL +
    '    D.DESCRIPTION AS DESCRIPTION, ' + NL +
    '    D.DESCRIPTION AS LONGDESC, ' + NL +
    '    D.DEPARTMENT_CODE AS DEPTCODE, ' + NL +
    '    D.PLAN_ON_WORKSPOT_YN ' + NL +
    '  FROM  ' + NL +
    '    DEPARTMENT D LEFT JOIN DEPARTMENTPERTEAM T ON ' + NL +
    '      D.PLANT_CODE = T.PLANT_CODE AND ' + NL +
    '      D.DEPARTMENT_CODE = T.DEPARTMENT_CODE ' + NL +
(* RV021.
    '    LEFT JOIN TEAMPERUSER U ON ' + NL +
    '      T.TEAM_CODE = U.TEAM_CODE ' + NL +
*)
    '  WHERE ' + NL +
(* RV021.
    '    ((:USER_NAME = ''*'') OR (U.USER_NAME = :USER_NAME)) AND ' + NL +
*)
    '  ( ' + NL +
    '    (:USER_NAME = ''*'') OR ' + NL +
    '    (T.TEAM_CODE IN ' + NL +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '  ) AND ' + NL +
    '    ((:TEAMFROM = ''*'') OR ' + NL +
    '     (T.TEAM_CODE >= :TEAMFROM AND T.TEAM_CODE <= :TEAMTO)) AND ' + NL +
    '    D.PLANT_CODE =  :PLANT_CODE AND ' + NL +
    '    D.PLAN_ON_WORKSPOT_YN = ''N'' ' + NL;
    if FWKDEPTSort = 0 then
      SelectStr := SelectStr + 'ORDER BY 1'
    else
      SelectStr := SelectStr + 'ORDER BY 2';
  QueryWork.Close;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(SelectStr);
// QueryWork.SQL.SaveToFile('c:\temp\wstest.sql');

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryWork.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryWork.ParamByName('USER_NAME').AsString := '*';
  if not FAllTeam then
  begin
    QueryWork.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QueryWork.ParamByName('TEAMTO').AsString := FTeamTo;
  end
  else
  begin
    QueryWork.ParamByName('TEAMFROM').AsString := '*';
    QueryWork.ParamByName('TEAMTO').AsString := '*';
  end;
  QueryWork.ParamByName('PLANT_CODE').AsString := FPlant;
  ClientDataSetWK.Close;
  DataSetProviderWK.DataSet := QueryWork;
  ClientDataSetWK.Open;
  FWKCount := ClientDataSetWK.RecordCount;
end;

procedure TDialogWorkspotPerEmployeeDM.DataModuleDestroy(Sender: TObject);
var
  SelectStr: String;
begin
  inherited;

  SelectStr := 'DELETE FROM PIVOTEMPSTAFFPLANNING WHERE ' +
    ' PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentComputerName + '''';
  ExecuteSql(QueryWork, SelectStr);
  SelectStr := 'DELETE FROM PIVOTOCISTAFFPLANNING WHERE ' +
    ' PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentComputerName + '''';
  ExecuteSql(QueryWork, SelectStr);

  ClientDataSetTeam.Close;
  ClientDataSetPlant.Close;
  ClientDataSetWKPerEmpl.Close;
  ClientDataSetWK.Close;
end;

procedure TDialogWorkspotPerEmployeeDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryTeam.SQL.Clear;
    QueryTeam.SQL.Add(
      'SELECT ' +
      '  T.TEAM_CODE, T.DESCRIPTION ' +
      'FROM ' +
      '  TEAM T, TEAMPERUSER U ' +
      'WHERE ' +
      '  U.USER_NAME = :USER_NAME AND ' +
      '  T.TEAM_CODE = U.TEAM_CODE ' +
      'ORDER BY ' +
      '  T.TEAM_CODE');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryTeam.Prepare;
  end;
  //  ActiveTables(Self, True);
  ClientDataSetTeam.Open;
(* ORACLE TEST *)
  // Pims -> Oracle
  StoredProcInsertEMP.StoredProcName := 'WORKSPOTPEREMPLOYEE_INSPIVOT';
//    StoredProcInsertEMP.StoredProcName := 'WORKSPOTPEREMPLOYEE_INSERTPIVOT';
  StoredProcInsertEMP.Prepare;
  ClientDataSetSave.CreateDataSet;
  //
  TablePivot.Filtered := True;
  TablePivot.Filter :=
    'PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentCOMPUTERName + '''';

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryPlant);
  ClientDataSetPlant.Open;
end;

procedure TDialogWorkspotPerEmployeeDM.FillEmployee;
var
  SelectStr: String;
begin
  //CAR 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    SelectStr := 
      'SELECT ' +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' +
      '  E.SHORT_NAME, E.TEAM_CODE, E.PLANT_CODE ' +
      'FROM ' +
      '  EMPLOYEE E, TEAMPERUSER T ' +
      'WHERE ' +
      '  T.USER_NAME = :USER_NAME AND ' +
      '  E.TEAM_CODE = T.TEAM_CODE AND ';
    if (not FAllTeam) then
      SelectStr := SelectStr +
        ' E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO AND ';

    SelectStr := SelectStr + ' E.PLANT_CODE = :PLANT ';
    if FEMPSort = 0 then
      SelectStr := SelectStr + ' ORDER BY E.EMPLOYEE_NUMBER ';
    if FEMPSort = 1 then
      SelectStr := SelectStr + ' ORDER BY E.DESCRIPTION ';
  end
  else
  begin
    SelectStr :=
      'SELECT ' +
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, ' +
      '  E.SHORT_NAME, E.TEAM_CODE, E.PLANT_CODE ' +
      'FROM ' +
      '  EMPLOYEE E ' +
      'WHERE ';
    if (not FAllTeam) then
      SelectStr := SelectStr +
        ' E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO AND ';

    SelectStr := SelectStr + ' E.PLANT_CODE = :PLANT ';
    if FEMPSort = 0 then
      SelectStr := SelectStr + ' ORDER BY E.EMPLOYEE_NUMBER ';
    if FEMPSort = 1 then
      SelectStr := SelectStr + ' ORDER BY E.DESCRIPTION ';
  end;
  QueryEmpl.SQL.Clear;
  QueryEmpl.SQL.Add(UpperCase(SelectStr));
  QueryEmpl.Prepare;

  ClientDataSetEmpl.Close;
  ClientDataSetEmpl.Filter := '';
  //CAR 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  QueryEmpl.ParamByName('PLANT').Value := FPlant;
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then
  begin
    QueryEmpl.ParamByName('TEAMFROM').Value := FTeamFrom;
    QueryEmpl.ParamByName('TEAMTO').Value := FTeamTo;
  end;
  ClientDataSetEmpl.Active := True;
end;

function TDialogWorkspotPerEmployeeDM.FillWorkspotPerEmployee: Boolean;
var
  CountCol, i: Integer;
  SqlStr: String;
  MyDatabaseUserName: String;
begin
  Result := False;
  FillWK;
 //get column number of pivot table
  QueryCountColumn.Close;
  QueryCountColumn.SQL.Clear;
  // Pims -> Oracle
(*
select COUNT(COLUMN_NAME)
from dba_tab_columns
where table_name = 'PIVOTWKPEREMP'
AND OWNER='PIMS'
*)
  // RV067.MRA.9. Bugfix.
  // Add scheme-name, in case there are multiple schemes.
  MyDatabaseUserName := SystemDM.Pims.Params.Values['USER NAME'];
  QueryCountColumn.SQL.Add(
    'SELECT ' +
    '  COUNT(COLUMN_NAME) AS COUNTCOL ' +
    'FROM ' +
    '  SYS.ALL_TAB_COLUMNS T ' +
    'WHERE ' +
    '  T.TABLE_NAME = ''PIVOTWKPEREMP''' +
    '  AND OWNER = ' + '''' + MyDatabaseUserName + '''');

//    QueryCountColumn.SQL.Add(
//      'SELECT ' +
//      '  COUNT(*)  AS COUNTCOL ' +
//      'FROM  ' +
//      '  RDB$RELATION_FIELDS REL ' +
//      'WHERE ' +
//      '  RDB$RELATION_NAME = ''PIVOTWKPEREMP''');

  QueryCountColumn.Open;
  CountCol := Round(QueryCountColumn.FieldByName('COUNTCOL').AsFloat);
  CountCol := CountCol - 3;
  if (CountCol < ClientDataSetWK.RecordCount) then
  begin
    if (ClientDataSetWK.RecordCount > Max_WK_Number ) then
    begin
      DisplayMessage(SSelectedWorkspots, mtWarning, [mbOK]);
      DialogWorkspotPerEmployeeDM.ClientDataSetWK.First;
      DialogWorkspotPerEmployeeDM.ClientDataSetWK.MoveBy(Max_WK_Number + 1);
      while (ClientDataSetWK.RecordCount > Max_WK_Number) do
        DialogWorkspotPerEmployeeDM.ClientDataSetWK.Delete;
      FWKCount := ClientDataSetWK.RecordCount;
    end;

    // RV067.MRA.9. Bugfix.
    // ALTER TABLE ADD <column> command was wrong!
    // There were no round-brackets used.
    // Syntax should be:
    {
      alter table
        table_name
      add
      (
        column1_name column1_datatype column1_constraint,
        column2_name column2_datatype column2_constraint,
        column3_name column3_datatype column3_constraint
       );
    }
    // start updating database with more columns
    QueryUpdateDB.Close;
    QueryUpdateDB.SQL.Clear;
    QueryUpdateDB.ParamCheck := False;
    SqlStr := 'ALTER TABLE PIVOTWKPEREMP';
    for i := CountCol to  (ClientDataSetWK.RecordCount - 1)  do
    begin
      // RV080.5. Datatype is wrong! It must be VARCHAR2 instead of CHAR !
      if i = CountCol then
        SqlStr := SqlStr +
          ' ADD  (WK' + IntToStr(i) + '1_BTN VARCHAR2(2),' // CHAR(2)
      else
        SqlStr := SqlStr +
          ' WK' + IntToStr(i) + '1_BTN VARCHAR2(2),'; // CHAR(2)
    end;
    QueryUpdateDB.SQL.Add(Copy(SqlStr, 1, Length(SqlStr) - 1) + ')');
    QueryUpdateDB.ExecSQL;
  end;

  if ClientDataSetWK.IsEmpty then
  begin
    DisplayMessage(SEmptyWK, mtWarning, [mbOK]);
    Exit;
  end;
  FillEmployee;

  if not ClientDataSetEmpl.IsEmpty  then
  begin
    InsertIntoEMPPivot;

    FillPIVOTTable;

    Result := True;
  end
  else
  begin
    DisplayMessage(SNoEmployee, mtWarning, [mbOK]);
    Exit;
  end;
end;

procedure TDialogWorkspotPerEmployeeDM.InsertIntoEMPPivot;
begin
  if FAllTeam then
  begin
    StoredProcInsertEMP.ParamByName('TEAMFROM').AsString := '*';
    StoredProcInsertEMP.ParamByName('TEAMTO').AsString := '*'
  end
  else
  begin
    StoredProcInsertEMP.ParamByName('TEAMFROM').AsString := FTeamFrom;
    StoredProcInsertEMP.ParamByName('TEAMTO').AsString := FTeamTo;
  end;
  //Car 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    StoredProcInsertEMP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    StoredProcInsertEMP.ParamByName('USER_NAME').AsString := '*';
  StoredProcInsertEMP.ParamByName('PLANT_CODE').AsString := FPlant;
  StoredProcInsertEMP.ParamByName('PIVOTCOMPUTERNAME').AsString :=
  	SystemDM.CurrentComputerName;
  StoredProcInsertEMP.ExecProc;
end;

procedure TDialogWorkspotPerEmployeeDM.FillEmpLevel;
var
  SelectStr:String;
begin
  ClientDataSetWKPerEmpl.Filter := '';
  SelectStr :=
    'SELECT ' +
    '  W.EMPLOYEE_NUMBER, W.PLANT_CODE, W.DEPARTMENT_CODE, ' +
    '  W.WORKSPOT_CODE, W.EMPLOYEE_LEVEL, W.STARTDATE_LEVEL ';
  if not FAllTeam then
  begin
    SelectStr := SelectStr +
      'FROM ' +
      '  WORKSPOTSPEREMPLOYEE W, EMPLOYEE E ';
    //CAR 550279
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    begin
      SelectStr := SelectStr +
        '  , TEAMPERUSER T ' +
        'WHERE ' +
        '  T.USER_NAME = :USER_NAME AND ' +
        '  E.TEAM_CODE = T.TEAM_CODE AND ';
    end
    else
      SelectStr := SelectStr + ' WHERE ';
  end
  else
  begin
    SelectStr := SelectStr + ' FROM WORKSPOTSPEREMPLOYEE W ';
    //CAR 550279
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    begin
      SelectStr := SelectStr +
        '  , EMPLOYEE E, TEAMPERUSER T ' +
        'WHERE ' +
        '  T.USER_NAME = :USER_NAME AND ' +
        '  E.TEAM_CODE = T.TEAM_CODE AND ' +
        '  E.EMPLOYEE_NUMBER = W.EMPLOYEE_NUMBER AND ';
    end
    else
       SelectStr := SelectStr + ' WHERE ';
  end;

  SelectStr := SelectStr + ' W.PLANT_CODE = :PLANT ';
  if not FAllTeam then
    SelectStr := SelectStr +
      '  AND W.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' +
      '  AND E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO ';
  SelectStr := SelectStr + ' ' +
    'ORDER BY ' +
    '  W.EMPLOYEE_NUMBER, W.PLANT_CODE, W.DEPARTMENT_CODE, W.WORKSPOT_CODE ' ;
  SetSelectSQL(QueryWork, SelectStr, False);
  QueryWork.ParamByName('PLANT').AsString := FPlant;
  if not FAllTeam then
  begin
    QueryWork.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QueryWork.ParamByName('TEAMTO').AsString := FTeamTo;
  end;
  //CAR 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryWork.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  ClientDataSetWKPerEmpl.Close;
  DataSetProviderWKPerEmpl.DataSet := QueryWorK;
  ClientDataSetWKPerEmpl.Open;
end;

procedure TDialogWorkspotPerEmployeeDM.FillPIVOTTable;
var
  k, Empl: Integer;
  Name: String;
  UpdateTable: Boolean;
begin
  if TablePivot.TableName = '' then
    TablePivot.TableName := 'PIVOTWKPEREMP';

  ClientDataSetEmpl.Filter := '';
  ClientDataSetEmpl.First;
  Empl := 1;
  FillEmpLevel;

  TablePivot.CachedUpdates := False;
  if FEmpSort = 0 then
    TablePivot.IndexFieldNames := 'PIVOTCOMPUTERNAME;EMPLOYEE_NUMBER'
  else
    TablePivot.IndexFieldNames := 'PIVOTCOMPUTERNAME;DESCRIPTION';
  TablePivot.CachedUpdates := True;
  k := 1;
  while (not ClientDataSetEmpl.Eof) and (K <= EmployeeSHOW) do
  begin
    Empl := ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    Name := ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString;
    FillPerEmplPIVOTTable(Empl, Name, False, UpdateTable);
    ClientDataSetEmpl.Next;
    Inc(K);
  end;
  // save last employee - the last pivot values filled
  FEmpl_Filled := Empl;
  FEmplDesc_Filled := Name;

  TablePivot.Filter := '';
  TablePivot.Filter :=
    'PIVOTCOMPUTERNAME = ''' + SystemDM.CurrentCOMPUTERName + '''';
  if not TablePivot.Active then
    TablePivot.Open
  else
    TablePivot.Refresh;
  TablePivot.First;
end;

procedure TDialogWorkspotPerEmployeeDM.FillPerEmplPIVOTTable(Empl: Integer;
  Name: String;  EditTable: Boolean;  var UpdateTable: Boolean);
var
  RecCount : Integer;
  WKName, StrBtn, WK, Dept: String;
  UpdateStr: String;
  FirstTime: Boolean;
begin
  UpdateTable := False;
  UpdateStr := ' UPDATE ' + TMPWKPEREMPL + ' SET ';
  FirstTime := True;

  if EditTable then
    if not (TablePivot.State in [dsEdit, dsInsert]) then
      TablePivot.Edit;
  ClientDataSetWK.First;
  RecCount := 0;
  while not ClientDataSetWK.Eof do
  begin
    WK := ClientDataSetWK.FieldByName('WKCODE').AsString;
    Dept := ClientDataSetWK.FieldByName('DEPTCODE').AsString;
    StrBtn := '';
    if ClientDataSetWKPerEmpl.FindKey([Empl, FPlant, DEPT, WK]) then
      StrBtn := ClientDataSetWKPerEmpl.FieldByName('EMPLOYEE_LEVEL').AsString;
// fill in table pivot
    if StrBtn <> '' then
    begin
      WKName := 'WK' + IntToStr(RecCount) + '1';
      if FirstTime then
        FirstTime := False
      else
        UpdateStr := UpdateStr + ', ';
      UpdateStr := UpdateStr + WKName + '_BTN' + ' = ''' + StrBtn + '''';
      if EditTable then
        TablePivot.FieldByName(WKName + '_BTN').AsString := StrBtn;
    end;
    ClientDataSetWK.Next;
    inc(RecCount);

  end;
  {while wk/dept}

  if EditTable and not FirstTime then
    TablePivot.Post;

  if not FirstTime then
  begin
    if Not EditTable then
    begin
      if FEMPSort = 0 then
        UpdateStr := UpdateStr + ' WHERE PIVOTCOMPUTERNAME = ' +
          '''' + SystemDM.CurrentComputerName + '''' +
          ' AND ' + 'EMPLOYEE_NUMBER = ' + IntToStr(Empl)
      else
        UpdateStr := UpdateStr + ' WHERE PIVOTCOMPUTERNAME = ' +
          '''' + SystemDM.CurrentComputerName + '''' +
          ' AND ' + 'DESCRIPTION = ' + '''' + DoubleQuote(Name) + '''' +
          ' AND  EMPLOYEE_NUMBER = ' + IntToStr(Empl);
      ExecuteSql(QueryWork, UpdateStr);
    end;
    UpdateTable := True;
  end;
end;

procedure TDialogWorkspotPerEmployeeDM.TablePivotAfterScroll(
  DataSet: TDataSet);
var
  EmplMin, EmplMax: Integer;
  DescMin, DescMax: String;
begin
  inherited;
  if not DataSet.Active then
    Exit;
  if WorkspotPerEmployeeF = Nil then
    Exit;

  FillEmpl_PIVOTTable(DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    DataSet.FieldByName('DESCRIPTION').AsString);

  //set the color per line in grid
  WorkspotPerEmployeeF.dxDBGridPlanning.FullRefresh;
  WorkspotPerEmployeeF.dxDBGridPlanning.Hint := '';
  EmplMin := WorkspotPerEmployeeF.dxDBGridPlanning.Items[0].Values[0];
  EmplMax:= WorkspotPerEmployeeF.dxDBGridPlanning.Items[
    WorkspotPerEmployeeF.dxDBGridPlanning.Count-1].Values[0];
  DescMin := WorkspotPerEmployeeF.dxDBGridPlanning.Items[0].Values[1];
  DescMax := WorkspotPerEmployeeF.dxDBGridPlanning.Items[
    WorkspotPerEmployeeF.dxDBGridPlanning.Count-1].Values[1];

  if ((EmplMin > EmplMax) and (FEmpSort = 0 ))  or
    ((FEmpSort = 1) and
     (WorkspotPerEmployeeF.dxDBGridPlanning.Items[0].Values[1]>
      WorkspotPerEmployeeF.dxDBGridPlanning.Items[
      WorkspotPerEmployeeF.dxDBGridPlanning.Count-1].Values[1] )) then
    TablePivot.Last;
end;

procedure TDialogWorkspotPerEmployeeDM.FillEmpl_PIVOTTable(EmplLast: Integer;
  LastDesc: String);
var
  Empl: Integer;
  Name: String;
  UpdateTable: Boolean;
  RefreshTable: Boolean;
begin
  if FEMPSort = 0 then
    if EmplLast <= FEmpl_Filled then
      Exit;
  if FEMPSort = 1 then
    if LastDesc <= FEmplDesc_Filled then
      Exit;

  Screen.Cursor := crHourGlass;
  UpdateTable := False;
  RefreshTable := False;
  
  if FEMPSort = 0 then
  begin
    ClientDataSetWKPerEmpl.Filtered := True;
    ClientDataSetWKPerEmpl.Filter := 'EMPLOYEE_NUMBER > ' +
      IntToStr(FEmpl_Filled) + ' AND EMPLOYEE_NUMBER <= ' +  IntToStr(EmplLast);

    ClientDataSetEmpl.Filtered := True;
    ClientDataSetEmpl.Filter  := 'EMPLOYEE_NUMBER > ' + IntToStr(FEmpl_Filled) +
      ' AND EMPLOYEE_NUMBER <= ' +  IntToStr(EmplLast);
  end
  else
  begin
    ClientDataSetEmpl.Filtered := True;
    ClientDataSetEmpl.Filter  := 'DESCRIPTION > ''' +
      DoubleQuote(FEmplDesc_Filled) + ''' AND DESCRIPTION <= ''' +
      DoubleQuote(LastDesc) + '''';
  end;
  ClientDataSetEmpl.First;
  if TablePivot.FieldByName('EMPLOYEE_NUMBER').AsInteger =
    ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger then
    FillPerEmplPIVOTTable(TablePivot.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString, True, UpdateTable)
  else
  begin

    while not ClientDataSetEmpl.Eof do
    begin
      Empl := ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Name := ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString;
      FillPerEmplPIVOTTable(Empl, Name,  False, UpdateTable);
      if UpdateTable then
        RefreshTable := True;
      ClientDataSetEmpl.Next;
    end;
   if RefreshTable then
     TablePivot.Refresh;

  end;
  FEmpl_Filled := EmplLast;
  FEmplDesc_Filled := LastDesc;

  Screen.Cursor := crDefault;
end;

procedure TDialogWorkspotPerEmployeeDM.SaveChanges;
var
  Empl: Integer;
  WK, Dept, SelectStr, UpdateStr, InsertStr: String;
begin
   ClientDataSetSave.First;
   while not ClientDataSetSave.Eof do
  begin
    Empl := ClientDataSetSave.FieldByName('EMPLOYEE_NUMBER').AsInteger;;
    WK := ClientDataSetSave.FieldByName('WORKSPOT_CODE').AsString;
    Dept := ClientDataSetSave.FieldByName('DEPARTMENT_CODE').AsString;
    if ClientDataSetWKPerEmpl.FindKey([Empl, FPlant, DEPT, WK]) then
    begin
      SelectStr := 
        'SELECT ' +
        '  EMPLOYEE_NUMBER ' +
        'FROM ' +
        '  WORKSPOTSPEREMPLOYEE ' +
        'WHERE ' +
        '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
        '  AND WORKSPOT_CODE = :WORKSPOT_CODE AND ' +
        '  DEPARTMENT_CODE = :DEPARTMENT_CODE ';
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(Selectstr);
      QueryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
      QueryWork.ParamByName('WORKSPOT_CODE').AsString := WK;
      QueryWork.ParamByName('DEPARTMENT_CODE').AsString := Dept;
      QueryWork.Open;
      if not QueryWork.IsEmpty then
      begin
        UpdateStr :=
          'UPDATE WORKSPOTSPEREMPLOYEE SET '+
          ' EMPLOYEE_LEVEL = :EMPLOYEE_LEVEL, ' +
          ' STARTDATE_LEVEL = :STARTDATE_LEVEL, '+
          ' MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
          ' WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
          ' AND WORKSPOT_CODE = :WORKSPOT_CODE AND ' +
          ' DEPARTMENT_CODE = :DEPARTMENT_CODE ';
        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add(UpdateStr);
        QueryWork.ParamByName('EMPLOYEE_LEVEL').AsString :=
          ClientDataSetWKPerEmpl.FieldByName('EMPLOYEE_LEVEL').AsString;
        QueryWork.ParamByName('STARTDATE_LEVEL').AsDateTime :=
          Trunc(ClientDataSetWKPerEmpl.FieldByName('STARTDATE_LEVEL').AsDateTime);
        QueryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
        QueryWork.ParamByName('WORKSPOT_CODE').AsString := WK;
        QueryWork.ParamByName('DEPARTMENT_CODE').AsString := Dept;
        QueryWork.ParamByName('MUTATOR').AsString :=
          SystemDM.CurrentComputerName;
        QueryWork.ParamByName('MUTATIONDATE').AsDateTime := FCurrentDate;
        QueryWork.ExecSQL;
      end
      else
      begin
        InsertStr :=
          'INSERT INTO WORKSPOTSPEREMPLOYEE (EMPLOYEE_NUMBER, ' +
          ' DEPARTMENT_CODE, WORKSPOT_CODE, EMPLOYEE_LEVEL, PLANT_CODE, ' +
          ' STARTDATE_LEVEL, CREATIONDATE, MUTATIONDATE, MUTATOR) ' +
          ' VALUES (:EMPLOYEE_NUMBER, ' +
          ' :DEPARTMENT_CODE, :WORKSPOT_CODE, :EMPLOYEE_LEVEL, :PLANT_CODE, ' +
          ' :STARTDATE_LEVEL, :CREATIONDATE, :MUTATIONDATE, :MUTATOR) ';

        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add(InsertStr);
        QueryWork.ParamByName('EMPLOYEE_LEVEL').AsString :=
          ClientDataSetWKPerEmpl.FieldByName('EMPLOYEE_LEVEL').AsString;
        QueryWork.ParamByName('STARTDATE_LEVEL').AsDateTime :=
          Trunc(ClientDataSetWKPerEmpl.FieldByName('STARTDATE_LEVEL').AsDateTime);
        QueryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
        QueryWork.ParamByName('WORKSPOT_CODE').AsString := WK;
        QueryWork.ParamByName('PLANT_CODE').AsString := FPlant;
        QueryWork.ParamByName('DEPARTMENT_CODE').AsString := Dept;
        QueryWork.ParamByName('MUTATOR').AsString :=
          SystemDM.CurrentComputerName;
        QueryWork.ParamByName('MUTATIONDATE').AsDateTime := FCurrentDate;
        QueryWork.ParamByName('CREATIONDATE').AsDateTime := FCurrentDate;
        QueryWork.ExecSQL;
      end;
    end
    else
    begin
      SelectStr := 
        'DELETE FROM WORKSPOTSPEREMPLOYEE ' +
        ' WHERE EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ' +
        ' AND WORKSPOT_CODE = :WORKSPOT_CODE AND '+
        ' DEPARTMENT_CODE = :DEPARTMENT_CODE ';
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(SelectStr);
      QueryWork.ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
      QueryWork.ParamByName('WORKSPOT_CODE').AsString := WK;
      QueryWork.ParamByName('DEPARTMENT_CODE').AsString := Dept;
      QueryWork.ExecSQL;
    end;
    ClientDataSetSave.Next;
  end; // while
  ClientDataSetSave.EmptyDataSet;
end;

procedure TDialogWorkspotPerEmployeeDM.Insert_SaveClientDataSet(Empl: Integer;
  Wk, Dept: String);
begin
  if not DialogWorkspotPerEmployeeDM.ClientDataSetSave.FindKey([Empl,
    WK, Dept]) then
  begin
    ClientDataSetSave.Insert;
    ClientDataSetSave.FieldByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    ClientDataSetSave.FieldByName('WORKSPOT_CODE').AsString := WK;
    ClientDataSetSave.FieldByName('DEPARTMENT_CODE').AsString := DEPT;
    ClientDataSetSave.Post;
  end;
end;

// RV050.8.
procedure TDialogWorkspotPerEmployeeDM.QueryPlantFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
