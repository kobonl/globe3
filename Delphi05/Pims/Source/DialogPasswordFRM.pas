unit DialogPasswordFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ActnList, dxBarDBNav, dxBar, ComDrvN, Buttons,
  ExtCtrls, ComCtrls, DialogBaseFRM;

type
  TDialogKind = (dkVerify, dkChange);

  TDialogPasswordF = class(TDialogBaseF)
    LabelPassword: TLabel;
    EditPassword: TEdit;
    EditConfirmPassword: TEdit;
    LabelConfirmPassword: TLabel;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure EditConfirmPasswordChange(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    DialogKind: TDialogKind;
  public
    { Public declarations }
//    function CheckPassword : boolean;

    function VerifyPassword : Boolean;
    function ChangePassword : Boolean;
  end;
var
  DialogPasswordF: TDialogPasswordF;

implementation

uses SystemDMT, UPIMSMessageRes, UPimsConst;
{$R *.DFM}

function TDialogPasswordF.ChangePassword : Boolean;
begin
  DialogKind := dkChange;
  EditConfirmPassword.Visible := True;
  LabelConfirmPassword.Visible := True;
  ShowModal;
  Result := (ModalResult = mrOk);
  If Result then
    SystemDm.SettingsPassword := EditConfirmPassword.Text;
end;

function TDialogPasswordF.VerifyPassword : boolean;
begin
  Caption := LSPIMSEnterPassword;
  LabelPassword.Caption := LSPIMSEnterPassword;
  DialogKind := dkVerify;
  ShowModal;
  Result := (ModalResult = mrOk);
end;


procedure TDialogPasswordF.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  PasswordOK: Boolean;
begin
  inherited;
  CanClose := True;
  if (ModalResult = mrOk) then
  begin
    if (DialogKind = dkChange) then
    begin
      PasswordOK :=
        (EditPassword.Text = EditConfirmPassword.Text) and (EditPassword.Text <> '');
      if not PasswordOK then
      begin
        if (EditPassword.Text = '') then
        begin
          DisplayMessage(SPimsNoEmptyPasswordAllowed, mtInformation, [mbOk]);
          EditPassword.SetFocus;
        end
        else
        begin
          DisplayMessage(SPimsPasswordNotConfirmed, mtInformation, [mbOk]);
          EditPassword.Text := '';
          EditConfirmPassword.Text  := '';
          EditPassword.SetFocus;
        end;
      end;
    end
    else
    begin
      PasswordOK := (EditPassword.Text = PASSWORDDEFAULT) or
        (EditPassword.Text = SystemDM.SettingsPassword);
      if not PasswordOK then
      begin
        DisplayMessage(SPimsIncorrectPassword, mtInformation, [mbOk]);
          EditPassword.Text := '';
          EditPassword.SetFocus;
      end;
    end;
    CanClose := PasswordOK;
  end;
end;

procedure TDialogPasswordF.EditConfirmPasswordChange(Sender: TObject);
begin
  inherited;
  if (Length(EditPassword.Text)=Length(EditConfirmPassword.Text)) and
     (DialogKind = dkChange) and (EditPassword.Text<>EditConfirmPassword.Text) then
     DisplayMessage(SPimsPasswordNotConfirmed, mtInformation, [mbOk]);
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogPasswordF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
