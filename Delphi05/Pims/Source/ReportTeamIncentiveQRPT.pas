(*
  Changes:
    MR:03-04-2006 Order 550424
      Add performance to Total-lines.
    MRA:24-FEB-2009 RV022. V2.0.139.142.
      - Added missing column-name 'Performance' to 2 headers.
      - Set some QRLabels to 'right-justify'.
      - Made more space between empl-number + description.
    MRA: 08-JUL-2009 RV032.
      - Year, Week, Day From, Day To
      use this:
      - Year, Week From, Week To.
      With a maximum of 5 weeks.
      - Open tables only when needed.
    MRA:9-DEC-2009 RV048.3.
    - This report gives wrong results when using
      option 'Show only jobs'. When this option is checked,
      it shows wrong result, when unchecked it is OK.
      Reason: When there are jobcodes with the same code but
      belonging to different workspots, it goes wrong.
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed dialogs in Pims, instead of
    open it as a modal dialog.
  - Because of embedding, a different var must be used for
    ReportProductionDetailDM -> Use ReportProductionDetailDM2 for this
    report, to prevent problems when it is freed (before report
    production details is called).
    Reason: It is possible one dialog is freeing this DataModule that
    another dialog had created.
  MRA:11-JUL-2011 RV094.6. Bugfix
  - Could give message about 'Format '%8.2f' invalid or
    incompatible with argument.
    - Solved by replacing 'Format('%8.2f', [0])' with
      'Format('%8.2f', [0.0])'.
  MRA:25-NOV-2011 RV102.2. SO-20012421
  - Report Team Incentive
    - When Group by Department is used, it gives an
      error-message about:
      'QueryProdHour: Field 'BUSINESSUNIT_CODE' not found.
  MRA:25-NOV-2011 RV102.3. SC-50021318 SO-20012421
  - Report Team Incentive
    - QRLabelWeek is not used, but is shown in translated version.
      To prevent this, the contents is emptied.
  MRA:12-JAN-2012 RV104.2. 20012421. Small change.
  - Report Team Incentive
    - The 'to'-value for date needs some more space when selections
      are shown in report to show it correct.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - When a job (JOBCODE-table) has field IGNOREQUANTITIESINREPORTS_YN set
    to 'Y' then the report should show no quantities for that job, only
    the time must be shown.
  MRA:2-AUG-2012 BUGFIX.
  - Searching on cdsEmpPiecesDate failed when using 'not-only-jobs':
    - Reason: The key has changed, so use Locate instead of FindKey!
  ROP:4-DEC-2012 TD-21526. Removed decimals from fields Norm amount
  ROP:12-FEB-2013 TD-21526. Removed decimals from fields Total pieces
  MRA:14-FEB-2013 TD-21526. Do not remove decimals for fields Total pieces
  - Reason: When more than 1 employee worked on the same job the pieces
    are divided and can therefore result in decimals.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  MRA:18-MAR-2013 TD-22296 Related to this todo.
  - Report production details does not filter on department anymore.
  - Also: Filter on business units was missing.
  ROP: 26-MAR-2013 - 20013965
  - remove description from group footers (only show code)
  - label "bonus" did not allways show up.
  MRA:27-MAR-2013 - 20013965
  - Correction for change about 'remove descriptions from group footers':
    - Assign empty string for descriptions for totals in group-footers, the
      components are not removed.
  - Bonus is not always showing up, cause:
    - It is emptied when incentive should not be shown, but when it should
      be shown again, the original string is gone.
      To solve this, the original string must be stored in an extra variable.
  - When saving to PDF it gives some extra lines that do not belong there:
    - Some QRLabel-components had to be emptied:
      - These are not used anymore but are there to prevent error-messages
        from translation-tool.
  MRA:28-AUG-2013 20013965.60 Rework
  - It showed only the employee-number when option 'show detailed report' was
    unchecked. This is changed, so it will show employee number + name.
  MRA:18-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:19-AUG-2014 20015220.90 Rework
  - Make more room for from-to-columns in selection-part, because of
    from-to-date that can be much larger when time-part is shown.
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
  MRA:9-JUN-2015 SO-20014550.50
  - Real Time Efficiency
  - Get data about productivity/hours from a view
  MRA:15-JAN-2016 PIM-135
  - Add salary hours and difference-columns to report.
  MRA:15-JAN-2016 PIM-137
  - Add EmployeeFrom, EmployeeTo to real-time-eff-build-query for reports where
    employees can be selected.
  MRA:5-FEB-2016 PIM-135
  - Production reports add salary-hours column
  - Related to this order:
  - Also show down-time-jobs + breaks.
  - Show down-time-jobs based on dialog-setting:
    - Show it not (default), yes, or only.
  - Only show salary/difference columns when show-down-time is set to yes.
*)

unit ReportTeamIncentiveQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, ComCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  PTTRSRecord=^TTRSRecord;
  TTRSRecord=record
    PlantCode, WorkspotCode, JobCode: String;
    EmployeeNumber: Integer;
    DateTimeIn, DateTimeOut: TDateTime;
  end;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FSortOn, FGroupBy: Integer;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo, FTeamFrom, FTeamTo, FShiftFrom, FShiftTo: String;
    FShowSelection, FShowDetail, FPagePlant, FPageEmpl, FAllTeam, FOnlyJob,
    FShowIncentive: Boolean;
    FExportToFile: Boolean;
    FIncludeDownTime: Integer; // 20015586
  public
    procedure SetValues(
      DateFrom, DateTo: TDateTime;
      SortOn, GroupBy: Integer;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
      AllTeam, ShowSelection, ShowDetail, PagePlant, PageEmpl, OnlyJob,
      ShowIncentive, ExportToFile: Boolean;
      IncludeDownTime: Integer // 20015586
      );
  end;

   TReportTeamIncentiveQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel2: TQRLabel;
    QRLabelYear: TQRLabel;
    QRGroupHdPlant: TQRGroup;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelJobCodeFrom: TQRLabel;
    QRLabelJobCodeTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRGroupHDEmpl: TQRGroup;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRGroupHDDate: TQRGroup;
    QRDBText10: TQRDBText;
    QRGroupHDBU: TQRGroup;
    QRBandFooterDEPT: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRGroupHDDept: TQRGroup;
    QRBandFooterWK: TQRBand;
    QRGroupHDWK: TQRGroup;
    QRBandFooterJobCode: TQRBand;
    QRLabelTotJob: TQRLabel;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabelDayTo: TQRLabel;
    QRLabelDayFrom: TQRLabel;
    QRGroupJob: TQRGroup;
    QRDBText16: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel106: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabelDay: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabelWK: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLblJobWrkHrs: TQRLabel;
    QRLabelNormAmount: TQRLabel;
    QRLabelTotPieces: TQRLabel;
    QRLabelPiecesPerH: TQRLabel;
    QRLabelPerformance: TQRLabel;
    QRDBText2: TQRDBText;
    QRLblWorkspotWrkHrs: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabelWKBonus: TQRLabel;
    QRLblDeptWrkHrs: TQRLabel;
    QRLblBUWrkHrs: TQRLabel;
    QRLblPlantWrkHrs: TQRLabel;
    QRLabelDeptBonus: TQRLabel;
    QRLabelBUBonus: TQRLabel;
    QRLabelPlantBonus: TQRLabel;
    QRLabelPlant: TQRLabel;
    QRLabelEmplDesc: TQRLabel;
    QRLabelBUDesc: TQRLabel;
    QRLabelDeptDesc: TQRLabel;
    QRLabelWKDesc: TQRLabel;
    QRLabelJobDesc: TQRLabel;
    QRLabelWKFDesc: TQRLabel;
    QRLabelDeptFDesc: TQRLabel;
    QRLabelBUFDesc: TQRLabel;
    QRLabelPlantFDesc: TQRLabel;
    QRBandFooterEmp: TQRBand;
    QRLabel17: TQRLabel;
    QRLabelEmpDesc: TQRLabel;
    QRDBTextEmpl: TQRDBText;
    QRLblEmpWrkHrs: TQRLabel;
    QRLabel27: TQRLabel;
    QRBandSummary: TQRBand;
    QRLabel32: TQRLabel;
    QRLblSummaryWrkHrs: TQRLabel;
    QRLabel42: TQRLabel;
    ChildBandEmpl: TQRChildBand;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRLabel46: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabelNorm: TQRLabel;
    ChildBandPlant: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRDBText4: TQRDBText;
    QRLabelJobDescF: TQRLabel;
    QRLabelNormF: TQRLabel;
    QRLblDetailWrkHrs: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLblWeekFrom: TQRLabel;
    QRLabel76: TQRLabel;
    QRLblWeekTo: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelWeek: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate1: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelIncludeDowntime: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLblJobSalHrs: TQRLabel;
    QRLblDetailSalHrs: TQRLabel;
    QRLblWorkspotSalHrs: TQRLabel;
    QRLblDeptSalHrs: TQRLabel;
    QRLblBUSalHrs: TQRLabel;
    QRLblEmpSalHrs: TQRLabel;
    QRLblPlantSalHrs: TQRLabel;
    QRLblSummarySalHrs: TQRLabel;
    QRLblJobDiff: TQRLabel;
    QRLblDetailDiff: TQRLabel;
    QRLblWorkspotDiff: TQRLabel;
    QRLblDeptDiff: TQRLabel;
    QRLblBUDiff: TQRLabel;
    QRLblEmpDiff: TQRLabel;
    QRLblPlantDiff: TQRLabel;
    QRLblSummaryDiff: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDescBUPrint(sender: TObject; var Value: String);
    procedure QRDBTextWKDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextEmplDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextDescDeptPrint(sender: TObject; var Value: String);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelWKFDescPrint(sender: TObject; var Value: String);
    procedure QRLabelDeptFDescPrint(sender: TObject; var Value: String);
    procedure QRLabelBUFDescPrint(sender: TObject; var Value: String);
    procedure QRLabelPlantFDescPrint(sender: TObject; var Value: String);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterJobCodeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelEmpDescPrint(sender: TObject; var Value: String);
    procedure QRLblJobWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblWorkspotWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblDeptWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblBUWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblPlantWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblEmpWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLblSummaryWrkHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelNormAmountPrint(sender: TObject; var Value: String);
    procedure QRLabel6Print(sender: TObject; var Value: String);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure QRLabelTotPiecesPrint(sender: TObject; var Value: String);
    procedure QRLabel38Print(sender: TObject; var Value: String);
    procedure QRLabelPiecesPerHPrint(sender: TObject; var Value: String);
    procedure QRLabel39Print(sender: TObject; var Value: String);
    procedure QRLabelPerformancePrint(sender: TObject; var Value: String);
    procedure QRLabel40Print(sender: TObject; var Value: String);
    procedure QRLabelWKBonusPrint(sender: TObject; var Value: String);
    procedure QRLabelDeptBonusPrint(sender: TObject; var Value: String);
    procedure QRLabelBUBonusPrint(sender: TObject; var Value: String);
    procedure QRLabel27Print(sender: TObject; var Value: String);
    procedure QRLabelPlantBonusPrint(sender: TObject; var Value: String);
    procedure QRLabel42Print(sender: TObject; var Value: String);
    procedure ChildBandEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel46Print(sender: TObject; var Value: String);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel17Print(sender: TObject; var Value: String);
    procedure QRBandFooterEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelWKPrint(sender: TObject; var Value: String);
    procedure QRLabel7Print(sender: TObject; var Value: String);
    procedure QRLabel58Print(sender: TObject; var Value: String);
    procedure QRLabel37Print(sender: TObject; var Value: String);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHdPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDEmplAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDateAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDeptAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupJobAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJobCodeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterEmpAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabel66Print(sender: TObject; var Value: String);
    procedure QRLabel68Print(sender: TObject; var Value: String);
    procedure QRLabel69Print(sender: TObject; var Value: String);
    procedure QRLabel70Print(sender: TObject; var Value: String);
    procedure QRLabel71Print(sender: TObject; var Value: String);
    procedure QRLabelWeekPrint(sender: TObject; var Value: String);
    procedure QRLblJobSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblDetailSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblWorkspotSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblDeptSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblBUSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblEmpSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblPlantSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblSummarySalHrsPrint(sender: TObject; var Value: String);
    procedure QRLblJobDiffPrint(sender: TObject; var Value: String);
    procedure QRLblDetailDiffPrint(sender: TObject; var Value: String);
    procedure QRLblWorkspotDiffPrint(sender: TObject; var Value: String);
    procedure QRLblDeptDiffPrint(sender: TObject; var Value: String);
    procedure QRLblBUDiffPrint(sender: TObject; var Value: String);
    procedure QRLblEmpDiffPrint(sender: TObject; var Value: String);
    procedure QRLblPlantDiffPrint(sender: TObject; var Value: String);
    procedure QRLblSummaryDiffPrint(sender: TObject; var Value: String);
  private
    FQRParameters: TQRParameters;
    FProgressBar: TProgressBar;
    FBonusDescription: String;
    function IncludeJobQuantity: Boolean;
    function DecodeHrsSalMin(AValue: Integer): String;
    function DecodeHrsDiffMin(AValue: Integer): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
//    FTRSList: TList;
    ATRSRecord: PTTRSRecord;
    FMinDate, FMaxDate: TDateTime;
    FJobNorm, FWKNorm, FTotalWKPieces, FTotalJobPieces,
    FJob_NormProd, FWKBonus, FDeptBonus, FBUBonus, FEmplBonus, FPlantBonus,
    FTotBonus, FJobBonus: Double;
    FPlantWorkHrs, FEmplWorkHrs, FBUWorkHrs, FDeptWorkHrs, FWKWorkHrs,
    FJobWorkHrs, FTOTWorkHrs: Integer;
    FPlantWorkHrsPerf, FEmplWorkHrsPerf, FBUWorkHrsPerf, FDeptWorkHrsPerf,
    FWKWorkHrsPerf, FJobWorkHrsPerf, FTOTWorkHrsPerf: Double;
    FPlantSalHrs, FEmplSalHrs, FBUSalHrs, FDeptSalHrs, FWKSalHrs,
    FJobSalHrs, FTotSalHrs: Integer; // PIM-135
    FDept: String;
    FBonusFactor : Double;
    SalaryCaption, HoursCaption, DiffCaption: String;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo, TeamFrom, TeamTo,
      ShiftFrom, ShiftTo: String;
      const DateFrom, DateTo: TDateTime;
      const {Year, } { Week, DayFrom, DayTo, }
        {WeekFrom, WeekTo, }
        SortOn, GroupBy: Integer;
      const AllTeam, ShowSelection, DetailRep, PagePlant, PageEmpl,
        OnlyJob, ShowIncentive, ExportToFile: Boolean;
      const IncludeDownTime: Integer // 20015586
      ): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;

    function GetSTRUntilSpace(var TempStr: String): String;
(*    procedure GetProdQStrings(ProdQString: String;
      var Plant, WKCode, JobCode: String;
      var StartDate, EndDate: TDateTime; var Quantity: Real); *)
(*    function GetTotalPieces(StartDate, EndDate: TDateTime;
      Empl: Integer; QuantPerMin: Real; Plant, WK, Job: String): Real; *)
(*    function GetPiecesOnInt(StartDate, EndDate: TDateTime;
      Empl: Integer; QuantPerMin: Real; Plant, WK, Job: String): Real; *)
(*    procedure GetTRSStrings(TRSStr: String;
      var TRSPlant, TRSWK, TRSJob: String; var TRSEmpl: Integer;
      var DateTimeIn, DateTimeOut: TDateTime); *)
(*    function CompareDateTime(DateTime1, DateTime2: TDateTime): Integer; *)
    property ProgressBar: TProgressBar read FProgressBar write FProgressBar;
    property BonusDescription: String read FBonusDescription
      write FBonusDescription;
  end;

var
  ReportTeamIncentiveQR: TReportTeamIncentiveQR;
  
function AddMinute(ADate: TDateTime): TDateTime;

implementation

{$R *.DFM}
uses
  SystemDMT, ListProcsFRM, UPimsConst, UPimsMessageRes,
  ReportTeamIncentiveDMT, UGlobalFunctions, ReportProductionDetailDMT;

procedure TQRParameters.SetValues(
  DateFrom, DateTo: TDateTime;
  {Year, }(* Week, DayFrom, DayTo, *)
  {WeekFrom, WeekTo,}
  SortOn, GroupBy: Integer;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo, TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
  AllTeam, ShowSelection, ShowDetail, PagePlant, PageEmpl, OnlyJob,
  ShowIncentive, ExportToFile: Boolean;
  IncludeDownTime: Integer // 20015586
  );
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
{
  FYear := Year;
  FWeekFrom := WeekFrom;
  FWeekTo := WeekTo;
}
(*  FWeek := Week;
  FDayFrom := DayFrom;
  FDayTo := DayTo; *)
  FSortOn := SortOn;
  FGroupBy := GroupBy;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    FWKFrom := ReportTeamIncentiveQR.InclExclWorkspotsResult;
    FWKTo := ReportTeamIncentiveQR.InclExclWorkspotsResult;
  end
  else
  begin
    FWKFrom := WKFrom;
    FWKTo := WKTo;
  end;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;

  FShowSelection := ShowSelection;
  FAllTeam := AllTeam;
  FShowDetail := ShowDetail;
  FPagePlant := PagePlant;
  FPageEmpl := PageEmpl;
  FOnlyJob  := OnlyJob;
  FShowIncentive := ShowIncentive;
  FExportToFile := ExportToFile;
  FIncludeDownTime := IncludeDownTime; // 20015586
end;

function TReportTeamIncentiveQR.QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, EmployeeFrom, EmployeeTo, TeamFrom, TeamTo,
      ShiftFrom, ShiftTo: String;
      const DateFrom, DateTo: TDateTime;
      const SortOn, GroupBy: Integer;
      const AllTeam, ShowSelection, DetailRep, PagePlant, PageEmpl,
       OnlyJob, ShowIncentive, ExportToFile: Boolean;
      const IncludeDownTime: Integer // 20015586
      ): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(
      DateFrom, DateTo,
      SortOn, GroupBy,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo, ShiftFrom, ShiftTo,
      AllTeam, ShowSelection, DetailRep, PagePlant, PageEmpl, OnlyJob,
      ShowIncentive, ExportToFile,
      IncludeDownTime // 20015586
      );
  end;
  SetDataSetQueryReport(ReportTeamIncentiveDM.QueryProdHour);
end;

function TReportTeamIncentiveQR.ExistsRecords: Boolean;
var
  SelectStr{, SelStr, GroupByStr}: String;
//  MaxDate: TDateTime;
  AllPlants, AllWorkspots, AllTeams: Boolean;
//  SelStr2, GroupByStr2: String; // 20015221
//  UserName: String;
  function MyDateTimeFrom(AValue: TDateTime): TDateTime; // 20015220
  begin
    if UseDateTime then
      Result := QRParameters.FDateFrom
    else
      Result := AValue;
  end;
  function MyDateTimeTo(AValue: TDateTime): TDateTime; // 20015220
  begin
    if UseDateTime then
      Result := QRParameters.FDateTo
    else
      Result := AValue;
  end;
begin
  // RV048.3.
  if QRParameters.FOnlyJob then
    QRGroupJob.Expression := 'QueryProdHour.JOB_CODEWK'
  else
    QRGroupJob.Expression := 'QueryProdHour.JOB_CODE';

  // Init/Reset Client Datasets.
  ReportProductionDetailDM.cdsEmpl.Close;
  ReportProductionDetailDM.cdsEmpl.Filtered := False;
  ReportProductionDetailDM.cdsEmpl.Open;

  Screen.Cursor := crHourGlass;
  Update;
  Application.ProcessMessages;

  // 20015220
  FMinDate := Trunc(QRParameters.FDateFrom);
  FMaxDate := Trunc(QRParameters.FDateTo);

  AllPlants := ReportProductionDetailDM.
    DetermineAllPlants(QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllTeams := ReportProductionDetailDM.DetermineAllTeams(
    QRParameters.FTeamFrom, QRParameters.FTeamTo);
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    AllWorkspots := ReportTeamIncentiveDM.DetermineAllWorkspots(
      QRBaseParameters.FPlantFrom, QRParameters.FWKFrom, QRParameters.FWKTo)
  else
    AllWorkspots := True;

  if (QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo) then
  begin
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999';
    QRParameters.FJobFrom := '1';
    QRParameters.FJobTo := 'zzzzzz';
    QRParameters.FBusinessFrom := '1';
    QRParameters.FBusinessTo := 'zzzzzz';
    QRParameters.FDeptFrom := '1';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FShiftFrom := '0';
    QRParameters.FShiftTo := '99';
  end;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    if InclExclWorkspotsAll then
    begin
      QRParameters.FJobFrom := '1';
      QRParameters.FJobTo := 'zzzzzz';
    end;
  end
  else
  begin
    if (QRParameters.FWKFrom <> QRParameters.FWKTo) then
    begin
      QRParameters.FJobFrom := '1';
      QRParameters.FJobTo := 'zzzzzz';
    end;
  end;

{
  if QRParameters.FSortOn = 0 then
  begin
    GroupByStr := ' PHE.PLANT_CODE, PHE.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
     ' PHE.PRODHOUREMPLOYEE_DATE, ' + NL;
    GroupByStr2 := ' T.PLANT_CODE, T.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
     ' TRUNC(T.DATETIME_IN), ' + NL;
    SelStr2 := ' T.PLANT_CODE, T.EMPLOYEE_NUMBER, E.DESCRIPTION, ' + NL +
     ' TRUNC(T.DATETIME_IN) AS PRODHOUREMPLOYEE_DATE, ' + NL;
  end
  else
  begin
    GroupByStr := ' PHE.PLANT_CODE, E.DESCRIPTION, PHE.EMPLOYEE_NUMBER,  ' + NL +
     ' PHE.PRODHOUREMPLOYEE_DATE, ' + NL;
    GroupByStr2 := ' T.PLANT_CODE, E.DESCRIPTION, T.EMPLOYEE_NUMBER,  ' + NL +
     ' TRUNC(T.DATETIME_IN), ' + NL;
    SelStr2 := ' T.PLANT_CODE, E.DESCRIPTION, T.EMPLOYEE_NUMBER,  ' + NL +
     ' TRUNC(T.DATETIME_IN) AS PRODHOUREMPLOYEE_DATE, ' + NL;
  end;
  SelStr := GroupByStr;
  if QRParameters.FGroupBy = 0 then
  begin
    GroupByStr := GroupByStr + 'J.BUSINESSUNIT_CODE , ' + NL;
    SelStr := SelStr + 'J.BUSINESSUNIT_CODE , ' + NL;
    GroupByStr2 := GroupByStr2 + 'J.BUSINESSUNIT_CODE , ' + NL;
    SelStr2 := SelStr2 + 'J.BUSINESSUNIT_CODE , ' + NL;
  end
  else
  begin
    GroupByStr := GroupByStr + 'W.DEPARTMENT_CODE, ' + NL;
    SelStr := SelStr + 'W.DEPARTMENT_CODE, ' + NL;
    GroupByStr2 := GroupByStr2 + 'W.DEPARTMENT_CODE, ' + NL;
    SelStr2 := SelStr2 + 'W.DEPARTMENT_CODE, ' + NL;
  end;
  if not QRParameters.FOnlyJob then
  begin
    GroupByStr := GroupByStr + ' PHE.WORKSPOT_CODE, PHE.JOB_CODE ' + NL;
    SelStr := SelStr + ' PHE.WORKSPOT_CODE, PHE.JOB_CODE ' + NL;
    GroupByStr2 := GroupByStr2 + ' T.WORKSPOT_CODE, T.JOB_CODE ' + NL;
    SelStr2 := SelStr2 + ' T.WORKSPOT_CODE, T.JOB_CODE ' + NL;
  end
  else
  begin
    GroupByStr := GroupByStr + '  PHE.JOB_CODE, PHE.WORKSPOT_CODE ';
    SelStr := SelStr +
      ' PHE.JOB_CODE, PHE.WORKSPOT_CODE ' +
      ', PHE.JOB_CODE || '' (''' +
      ' || PHE.WORKSPOT_CODE || '')'' AS JOB_CODEWK';
    GroupByStr2 := GroupByStr2 + '  T.JOB_CODE, T.WORKSPOT_CODE ';
    SelStr2 := SelStr2 +
      ' T.JOB_CODE, T.WORKSPOT_CODE ' +
      ', T.JOB_CODE || '' (''' +
      ' || T.WORKSPOT_CODE || '')'' AS JOB_CODEWK';
  end;

  // 20015220 + 20015221
  // Do NOT look at PHE for these orders, because then it then cannot compare
  // with part of a day, like '8:00 - 9:00'.

  SelectStr :=
    'SELECT ' + NL +
    SelStr +
    ', SUM(PHE.PRODUCTION_MINUTE) AS WORKEDHRS, ' + NL +
    '  MAX(P.DESCRIPTION) AS PDESCRIPTION ' + NL;
  if not (QRParameters.FGroupBy = 0) then
    SelectStr := SelectStr +
      ' , MAX(D.DESCRIPTION) AS DDESCRIPTION ' + NL;
  SelectStr := SelectStr +
    ' , MAX(W.DESCRIPTION) AS WDESCRIPTION ' + NL +
    ' , MAX(W.DEPARTMENT_CODE) AS WDEPARTMENT_CODE ' + NL +
    ' , MAX(J.DESCRIPTION) AS JDESCRIPTION ' + NL +
    ' , MAX(J.NORM_PROD_LEVEL) AS NORM_PROD_LEVEL ' + NL;
  // RV102.2.
  if (QRParameters.FGroupBy = 0) then
    SelectStr := SelectStr +
      ' , MAX(J.BUSINESSUNIT_CODE) AS JBUSINESSUNIT_CODE ' + NL
  else
    SelectStr := SelectStr +
      ' , MAX(J.BUSINESSUNIT_CODE) AS BUSINESSUNIT_CODE ' + NL;
  SelectStr := SelectStr +
    ' , MAX(B.DESCRIPTION) AS BDESCRIPTION ' + NL +
    ' , MAX(B.BONUS_FACTOR) AS BONUS_FACTOR ' + NL;
  SelectStr := SelectStr +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE PHE, WORKSPOT W, ' + NL +
    '  JOBCODE J, EMPLOYEE E, PLANT P, BUSINESSUNIT B ' + NL;
  if not (QRParameters.FGroupBy = 0) then
    SelectStr := SelectStr +
      '  , DEPARTMENT D ' + NL;
  SelectStr := SelectStr +
    'WHERE ' + NL;
  // 20015586
  if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
  begin
    SelectStr := SelectStr +
      '  ((PHE.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PHE.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  end;
  SelectStr := SelectStr +
    '  PHE.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND PHE.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND PHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    '  AND PHE.PLANT_CODE = W.PLANT_CODE ' + NL +
    '  AND PHE.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  AND PHE.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '  PHE.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
    '  AND PHE.JOB_CODE = J.JOB_CODE ' + NL +
    '  AND W.USE_JOBCODE_YN = ''Y''' + NL +
    '  AND W.MEASURE_PRODUCTIVITY_YN = ''Y''' + NL;
    //RV067.2.
    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    (' + ''''  + SystemDM.UserTeamLoginUser + '''' + ' = ''*'') OR ' + NL +
      '    (E.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = ' + '''' + SystemDM.UserTeamLoginUser + '''' + ')) ' + NL +
      '  ) ';
  if not QRParameters.FAllTeam then
    SelectStr := SelectStr + ' AND E.TEAM_CODE >= ''' +
      DoubleQuote(QRParameters.FTeamFrom) +
      '''' + ' AND E.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    SelectStr := SelectStr +
      '  AND PHE.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
      '  AND PHE.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectStr := SelectStr +
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PHE.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
      SelectStr := SelectStr +
        '  AND PHE.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) + '''' + NL +
        '  AND PHE.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) + '''' + NL;
    SelectStr := SelectStr +
      '  AND W.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND W.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND J.BUSINESSUNIT_CODE >= ''' + DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '  AND J.BUSINESSUNIT_CODE <= ''' + DoubleQuote(QRParameters.FBusinessTo) + '''' + NL +
      // CAR 23-10-2003
      '  AND PHE.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
      '  AND PHE.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + NL;
  end;

  SelectStr := SelectStr +
    '  AND PHE.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE ' + NL +
    '  AND PHE.PRODHOUREMPLOYEE_DATE < :FENDDATE ' +
    '  AND PHE.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  AND J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL;
  if not (QRParameters.FGroupBy = 0) then
    SelectStr := SelectStr +
      '  AND PHE.PLANT_CODE = D.PLANT_CODE ' + NL +
      '  AND W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      if not InclExclWorkspotsAll then
      begin
        SelectStr := SelectStr +
          '  AND PHE.JOB_CODE >= ''' + DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
          '  AND PHE.JOB_CODE <= ''' + DoubleQuote(QRParameters.FJobTo) + '''' + NL;
      end;
    end
    else
    begin
      if (QRParameters.FWKFrom = QRParameters.FWKTo) then
      begin
        SelectStr := SelectStr +
          '  AND PHE.JOB_CODE >= ''' + DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
          '  AND PHE.JOB_CODE <= ''' + DoubleQuote(QRParameters.FJobTo) + '''' + NL;
      end;
    end;
  end;
  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + GroupByStr + ' ' + NL;

  // 20015220 + 20015221 Here we search for open scans (not older than 12 hours)
  if UseDateTime or IncludeOpenScans then
  begin
    SelectStr := ''; // Do not include the first part about PHE!
    SelectStr := SelectStr + ' ' +
//    'UNION ' + NL +
    ' SELECT ' + NL +
    SelStr2 + NL +
    ', 0 AS WORKEDHRS, ' + NL +
    '  MAX(P.DESCRIPTION) AS PDESCRIPTION ' + NL;
    if not (QRParameters.FGroupBy = 0) then
      SelectStr := SelectStr +
        ' , MAX(D.DESCRIPTION) AS DDESCRIPTION ' + NL;
    SelectStr := SelectStr +
      ' , MAX(W.DESCRIPTION) AS WDESCRIPTION ' + NL +
      ' , MAX(W.DEPARTMENT_CODE) AS WDEPARTMENT_CODE ' + NL +
      ' , MAX(J.DESCRIPTION) AS JDESCRIPTION ' + NL +
      ' , MAX(J.NORM_PROD_LEVEL) AS NORM_PROD_LEVEL ' + NL;
    // RV102.2.
    if (QRParameters.FGroupBy = 0) then
      SelectStr := SelectStr +
        ' , MAX(J.BUSINESSUNIT_CODE) AS JBUSINESSUNIT_CODE ' + NL
    else
      SelectStr := SelectStr +
        ' , MAX(J.BUSINESSUNIT_CODE) AS BUSINESSUNIT_CODE ' + NL;
    SelectStr := SelectStr +
      ' , MAX(B.DESCRIPTION) AS BDESCRIPTION ' + NL +
      ' , MAX(B.BONUS_FACTOR) AS BONUS_FACTOR ' + NL;
    SelectStr := SelectStr +
    ' FROM ' + NL +
    '   timeregscanning T inner join employee e on ' + NL +
    '     T.employee_number = e.employee_number ' + NL +
    '   inner join workspot w on ' + NL +
    '     T.plant_code = w.plant_code and ' + NL +
    '     T.workspot_code = w.workspot_code ' + NL +
    '   inner join jobcode j on ' + NL +
    '     T.plant_code = j.plant_code and ' + NL +
    '     T.workspot_code = j.workspot_code and ' + NL +
    '     T.job_code = j.job_code ' + NL +
    '   inner join businessunit b on ' + NL +
    '     j.businessunit_code = b.businessunit_code ' + NL +
    '   inner join plant p on ' + NL +
    '     T.plant_code = p.plant_code ' + NL;
    if not (QRParameters.FGroupBy = 0) then
      SelectStr := SelectStr +
        '  inner join DEPARTMENT D on ' + NL +
        '    w.plant_code = d.plant_code and ' + NL +
        '    w.department_code = d.department_code ' + NL;
    SelectStr := SelectStr +
    ' WHERE ' + NL;
    // 20015586
    if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
    begin
      SelectStr := SelectStr +
        '  ((T.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
        '   (T.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
    end;
    SelectStr := SelectStr +
    '  (' + NL +
    '    (T.PROCESSED_YN = ''Y'' ' + NL +
    '    AND ' + NL +
    '    T.DATETIME_OUT >= ' + ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ' AND ' + NL +
    '    T.DATETIME_IN < ' + ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ' ' + NL +
    '    ) ' + NL +
    '    OR ' + NL +
    '    (T.PROCESSED_YN = ''N'' ' + NL +
    '     AND ' + ReportProductionDetailDM.OpenScan('T.DATETIME_OUT') + ' IS NOT NULL ' + NL +
    '    ) ' + NL +
    '  )' + NL +
    '  AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '  AND W.MEASURE_PRODUCTIVITY_YN = ''Y'' ' + NL +
    '  AND T.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND T.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND DATETIME_RANGE_OVERLAP_MINS(' + ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) +
    ',' + ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ', T.DATETIME_IN, ' +
    ReportProductionDetailDM.OpenScan('T.DATETIME_OUT') + ') > 0 ' + NL; // 20015221
    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    ('  + ''''  + SystemDM.UserTeamLoginUser + '''' + ' = ''*'') OR ' + NL +
      '    (E.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = '  + ''''  + SystemDM.UserTeamLoginUser + '''' + ')) ' + NL +
      '  ) ' + NL;
    if not QRParameters.FAllTeam then
      SelectStr := SelectStr + ' AND E.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) +
        '''' + ' AND E.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      SelectStr := SelectStr +
        '  AND T.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND T.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL;
      if SystemDM.WorkspotInclSel then // 20015223
      begin
        SelectStr := SelectStr +
          ' AND ' + NL +
          ' (' + NL +
          '   (' + NL +
          '     T.WORKSPOT_CODE IN ' + NL +
          '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
          '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
          '      AND ' + NL +
          '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
          '   )' + NL +
          ' OR ' + NL +
          '   ( ' + NL +
          '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
          '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
          '      AND ' + NL +
          '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
          '   ) ' + NL +
          ' ) ' + NL;
      end
      else
        SelectStr := SelectStr +
          '  AND T.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) + '''' + NL +
          '  AND T.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) + '''' + NL;
      SelectStr := SelectStr +
        '  AND W.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
        '  AND W.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
        '  AND J.BUSINESSUNIT_CODE >= ''' + DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND J.BUSINESSUNIT_CODE <= ''' + DoubleQuote(QRParameters.FBusinessTo) + '''' + NL +
        '  AND T.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
        '  AND T.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + NL;
    end;
    if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    begin
      if SystemDM.WorkspotInclSel then // 20015223
      begin
        if not InclExclWorkspotsAll then
        begin
          SelectStr := SelectStr +
            '  AND T.JOB_CODE >= ''' + DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
            '  AND T.JOB_CODE <= ''' + DoubleQuote(QRParameters.FJobTo) + '''' + NL;
        end;
      end
      else
      begin
        if (QRParameters.FWKFrom = QRParameters.FWKTo) then
        begin
          SelectStr := SelectStr +
            '  AND T.JOB_CODE >= ''' + DoubleQuote(QRParameters.FJobFrom) + '''' + NL +
            '  AND T.JOB_CODE <= ''' + DoubleQuote(QRParameters.FJobTo) + '''' + NL;
        end;
      end;
    end;
    SelectStr := SelectStr + ' ' +
      'GROUP BY ' + GroupByStr2 + ' ' + NL;
    SelectStr := SelectStr +
      'ORDER BY ' + NL +
      '   1,  ' + NL + // PLANT_CODE
      '   2,  ' + NL + // T.EMPLOYEE_NUMBER,
      '   3,  ' + NL + // E.DESCRIPTION,
      '   4,  ' + NL + // T.PRODHOUREMPLOYEE_DATE,
      '   9,  ' + NL + // B.BUSINESSUNIT_CODE,
      '   10, ' + NL + // T.JOB_CODE,
      '   11  ' + NL; // T.WORKSPOT_CODE
  end
  else
    SelectStr := SelectStr +
      'ORDER BY ' + GroupByStr;

}

(*
  MaxDate := FMaxDate + 1;
  with ReportTeamIncentiveDM.QueryProdHour do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
    // 20015220 + 20015221 No params here.
    if UseDateTime or IncludeOpenScans then
    begin
//      ParamByName('FSTARTDATE').AsDateTime := Trunc(QRParameters.FDateFrom);
//      ParamByName('FENDDATE').AsDateTime := Trunc(QRParameters.FDateTo + 1);
    end
    else
    begin
      ParamByName('FSTARTDATE').AsDateTime := FMinDate;
      ParamByName('FENDDATE').AsDateTime := MaxDate;
    end;
// SQL.SaveToFile('c:\temp\ReportTeamIncentive.sql');
    if not Prepared then
      Prepare;
    Active := True;
    if (RecordCount = 0) then
    begin
      Result := False;
      Exit;
    end
    else
      Result := True;
  end;
*)

  SelectStr :=
    ReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo,
      QRParameters.FDeptFrom, QRParameters.FDeptTo,
      QRParameters.FWKFrom, QRParameters.FWKTo,
      QRParameters.FJobFrom, QRParameters.FJobTo,
      QRParameters.FTeamFrom, QRParameters.FTeamTo,
      '0', '999', // ShiftFrom, ShiftTo
      QRBaseParameters.FEmployeeFrom, QRBaseParameters.FEmployeeTo, // PIM-137
      FMinDate, FMaxDate,
      AllPlants,
      False, // AllBusinessUnits
      False, // AllDepartments
      AllWorkspots,
      AllTeams,
      True, // AllShifts
      False, // ShowShifts
      QRParameters.FOnlyJob, // ShowOnlyJob
      QRParameters.FIncludeDownTime, // PIM-135
      False, // SelectShift
      True, // SelectDate
      QRParameters.FGroupBy + 1,
      QRParameters.FSortOn,
      True, // FilterOnJob
      True // TeamInventiveReport
      );

  with ReportTeamIncentiveDM.QueryProdHour do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(SelectStr);
// SQL.SaveToFile('c:\temp\ReportTeamIncentive.sql');
    Open;
    Result := not Eof;
  end;

(*
  if Result then
  begin
    // 20014550.50 Real Time Eff: Build a second query here
    SelectStr :=
      ReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
        QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo,
        QRParameters.FBusinessFrom, QRParameters.FBusinessTo,
        QRParameters.FDeptFrom, QRParameters.FDeptTo,
        QRParameters.FWKFrom, QRParameters.FWKTo,
        QRParameters.FTeamFrom, QRParameters.FTeamTo,
        '0', '999', // ShiftFrom, ShiftTo
        FMinDate, FMaxDate,
        AllPlants,
        False, // AllBusinessUnits
        False, // AllDepartments
        AllWorkspots,
        AllTeams,
        True, // AllShifts
        False, // ShowShifts
        QRParameters.FOnlyJob, // ShowOnlyJob
        QRParameters.FIncludeDownTime = DOWNTIME_ONLY, // ShowOnlyDownTime
        False, // SelectShift
        True // SelectDate
        );

   with ReportProductionDetailDM do
   begin
     QueryProduction.SQL.Clear;
     QueryProduction.SQL.Add(SelectStr);
// QueryProduction.SQL.SaveToFile('c:\temp\ReportTeamIncentiveQueryProduction.sql');
     QueryProduction.Open;
   end;
*)
{
    ReportProductionDetailDM.InitAll(
      QRBaseParameters.FPlantFrom,
      QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, // TD-22296
      QRParameters.FBusinessTo, // TD-22296
      QRParameters.FDeptFrom, // TD-22296
      QRParameters.FDeptTo, // TD-22296
      QRParameters.FWKFrom,
      QRParameters.FWKTo,
      QRParameters.FTeamFrom,
      QRParameters.FTeamTo,
      '0', '999', // ShiftFrom, ShiftTo
      MyDateTimeFrom(FMinDate), // 20015220
      MyDateTimeTo(MaxDate), // 20015220
      AllPlants,
      False, // TD-22296 (AllBusinessUnits)
      False, // TD-22296 (AllDepartments)
      AllWorkspots,
      AllTeams,
      True, // AllShifts
      False, // ShowShifts
      ProgressBar,
      QRParameters.FIncludeDownTime = DOWNTIME_ONLY // 20015586
      );
}
(*  end; *)

  Screen.Cursor := crDefault;
end;

procedure TReportTeamIncentiveQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := DisplayWorkspotFrom(QRParameters.FWKFrom); // 20015223
  QRLabelWKTo.Caption := DisplayWorkspotTo(QRParameters.FWKTo); // 20015223
  QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
  QRLabelJobCodeFrom.Caption := QRParameters.FJobFrom;
  QRLabelJobCodeTo.Caption := QRParameters.FJobTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  if UseDateTime then
  begin
    QRLabelDateFrom.Caption := DateTimeToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateTimeToStr(QRParameters.FDateTo);
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  end;

(*
  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
  QRLblWeekFrom.Caption := IntToStr(QRParameters.FWeekFrom);
  QRLblWeekTo.Caption := IntToStr(QRParameters.FWeekTo) + ' ' +
  '(' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekFrom, 1)) + ' - ' +
    DateTimeToStr(ListProcsF.DateFromWeek(QRParameters.FYear,
    QRParameters.FWeekTo, 7)) +
  ')';
*)
  // RV102.3. Is not used.
  QRLabelWeek.Caption := '';
(*
  QRLabelWeek.Caption := IntToStr(QRParameters.FWeek);
  QRLabelDayFrom.Caption := IntToStr(QRParameters.FDayFrom);
  QRLabelDayTo.Caption := IntToStr(QRParameters.FDayTo);
*)
  // 20013965 Empty these strings to prevent they show up when a PDF is made.
  QRLabel74.Caption := '';
  QRLabel75.Caption := '';
  QRLabel76.Caption := '';
  QRLabel2.Caption := '';
  QRLblWeekFrom.Caption := '';
  QRLblWeekTo.Caption := '';
  QRLabelYear.Caption := '';
  QRLabel26.Caption := '';
  QRLabel24.Caption := '';
  QRLabel28.Caption := '';
  QRLabel31.Caption := '';
  QRLabelDayFrom.Caption := '';
  QRLabelDayTo.Caption := '';
  QRLabelDate.Caption := '';
  QRLabelWeek.Caption := '';
  QRLabel29.Caption := '';
  QRLabel30.Caption := '';

  QRLabelShiftTo.Caption := QRParameters.FShiftTo;
  QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
    QRLabelJobCodeFrom.Caption := '*';
    QRLabelJobCodeTo.Caption := '*';
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
    QRLabelTeamTo.Caption := '*';
    QRLabelTeamFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
    QRLabelShiftFrom.Caption := '*';
  end;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    if InclExclWorkspotsAll then
    begin
      QRLabelJobCodeFrom.Caption := '*';
      QRLabelJobCodeTo.Caption := '*';
    end;
  end
  else
  begin
    if (QRParameters.FWKFrom <> QRParameters.FWKTo) then
    begin
      QRLabelJobCodeFrom.Caption := '*';
      QRLabelJobCodeTo.Caption := '*';
    end;
  end;

  QRGroupHDBU.Enabled := False;
  QRBandFooterBU.Enabled := False;
  QRGroupHDDept.Enabled := False;
  QRBandFooterDept.Enabled := False;
  if QRParameters.FGroupBy = 0 then
  begin
    QRGroupHDBU.Enabled := True;
    QRBandFooterBU.Enabled := True;
  end
  else
  begin
    QRGroupHDDept.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FOnlyJob then
  begin
    QRGroupHDWK.Enabled := False;
    QRGroupHDWK.Expression := '';
    QRBandFooterWK.Enabled := False;
  end
  else
  begin
    QRGroupHDWK.Enabled := True;
    QRGroupHDWK.Expression := 'QueryProdHour.WORKSPOT_CODE';
    QRBandFooterWK.Enabled := True;
  end;
  // 20015586
  case QRParameters.FIncludeDownTime of
  DOWNTIME_NO:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeNo;
  DOWNTIME_YES:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeYes;
  DOWNTIME_ONLY:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeOnly;
  end;
  // Do not show Salary/Diff columns
  if QRParameters.FIncludeDownTime = DOWNTIME_YES then
  begin
    QRLabel77.Caption := SalaryCaption;
    QRLabel79.Caption := HoursCaption;
    QRLabel84.Caption := DiffCaption;
    QRLabel80.Caption := SalaryCaption;
    QRLabel81.Caption := HoursCaption;
    QRLabel85.Caption := DiffCaption;
    QRLabel82.Caption := SalaryCaption;
    QRLabel83.Caption := HoursCaption;
    QRLabel86.Caption := DiffCaption;
  end
  else
  begin
    QRLabel77.Caption := '';
    QRLabel79.Caption := '';
    QRLabel84.Caption := '';
    QRLabel80.Caption := '';
    QRLabel81.Caption := '';
    QRLabel85.Caption := '';
    QRLabel82.Caption := '';
    QRLabel83.Caption := '';
    QRLabel86.Caption := '';
  end;
end;

procedure TReportTeamIncentiveQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportTeamIncentiveQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
(*  FTRSList := nil; *)
  // MR:29-09-2003
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  // 20013965
  BonusDescription := QRLabel58.Caption;
  SalaryCaption := QRLabel77.Caption;
  HoursCaption := QRLabel79.Caption;
  DiffCaption := QRLabel84.Caption;
end;

function AddMinute(ADate: TDateTime): TDateTime;
var
  Hr, Min, Sec, MSec: Word;
begin
  // First zero the seconds and mseconds.
  DecodeTime(ADate, Hr, Min, Sec, MSec);
  ADate := Trunc(ADate) + EncodeTime(Hr, Min, 0, 0);
  // Now add a minute
  Result := ADate + (1 / 24 / 60);
end;

procedure TReportTeamIncentiveQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  FTOTWorkHrs := 0;
  FTotSalHrs := 0;
  FTotBonus := 0.00;
  FTOTWorkHrsPerf := 0;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' +
        QRLabel1.Caption + ' ' + QRLabelPlantFrom.Caption + ' ' +
        QRLabel49.Caption + ' ' + QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel25.Caption + ' ' +
        QRLabel48.Caption + ' ' + QRLabelBusinessFrom.Caption + ' ' +
        QRLabel16.Caption + ' ' + QRLabelBusinessTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelWKFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' + QRLabelWKTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabelAbsenceRsn.Caption + ' ' + QRLabelJobCodeFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLabelJobCodeTo.Caption);
      ExportClass.AddText(QRLabel4.Caption + ' ' +
        QRLabel23.Caption + ' ' + QRLabelTeamFrom.Caption + ' ' +
        QRLabel19.Caption + ' ' + QRLabelTeamTo.Caption);
      ExportClass.AddText(QRLabel51.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLabelEmployeeFrom.Caption + ' ' +
        QRLabel53.Caption + ' ' + QRLabelEmployeeTo.Caption);
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' +
        QRLabelDate1.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabel78.Caption + ' ' + QRLabelDateTo.Caption);
      ExportClass.AddText(QRLabelIncludeDowntime.Caption); // 20015586
(*
      ExportClass.AddText(QRLabel2.Caption + ' ' +
        QRLabelYear.Caption);
      ExportClass.AddText(QRLabel74.Caption + ' ' + QRLabel75.Caption + ' ' +
        QRLblWeekFrom.Caption + ' ' + QRLabel76.Caption + ' ' +
        QRLblWeekTo.Caption);
*)
(*
      ExportClass.AddText(QRLabelDate.Caption + ' ' +
        QRLabelWeek.Caption);
      ExportClass.AddText(QRLabel26.Caption + ' ' +
        QRLabel24.Caption + ' ' + QRLabelDayFrom.Caption + ' ' +
        QRLabel28.Caption + ' ' + QRLabelDayTo.Caption);
*)
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  FPlantWorkHRS := 0;
  FPlantSalHrs := 0;
  FPlantBonus := 0;
  FPlantWorkHrsPerf := 0;
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    if not TablePlant.Active then
      TablePlant.Active := True;
    TablePlant.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString]);
    QRLabelPlant.Caption := TablePlant.FieldByName('DESCRIPTION').AsString;
}
    QRLabelPlant.Caption := QueryProdHour.FieldByName('PDESCRIPTION').AsString;
  end;
end;

procedure TReportTeamIncentiveQR.QRDBTextDescBUPrint(sender: TObject;
  var Value: String);
{ var
  BU_Code: String; }
begin
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    BU_code :=
      QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString;
    if not TableBU.Active then
      TableBU.Active := True;
    TableBU.FindKey([BU_code]);
    Value := Copy(TableBU.FieldByName('DESCRIPTION').AsString,0,15);
}
    Value := Copy(QueryProdHour.FieldByName('BDESCRIPTION').AsString,0,15);
  end;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRDBTextWKDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableWK.Active then
      TableWK.Active := True;
    TableWK.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
      QueryProdHour.FieldByName('WORKSPOT_CODE').AsString]);
    Value := Copy(TableWK.FieldByName('DESCRIPTION').AsString,0 , 15);
}
    Value := Copy(QueryProdHour.FieldByName('WDESCRIPTION').AsString,0 , 15);
  end;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRDBTextEmplDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableEmpl.Active then
      TableEmpl.Active := True;
    TableEmpl.FindKey([QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger]);
    Value := Copy(TableEmpl.FieldByName('DESCRIPTION').AsString, 0 , 10);
}
    Value := Copy(QueryProdHour.FieldByName('EDESCRIPTION').AsString, 0 , 10);
  end;
  (Sender as TQRLabel).Caption := Value;
end;
procedure TReportTeamIncentiveQR.QRDBTextDescDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableDept.Active then
      TableDept.Active := True;
    TableDept.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
      QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString]);
    Value := Copy(TableDept.FieldByName('DESCRIPTION').AsString,0,15);
}
    Value := Copy(QueryProdHour.FieldByName('DDESCRIPTION').AsString,0,15);
  end;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRGroupHDEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  FEmplWorkHrs := 0;
  FEmplSalHrs := 0;
  FEmplBonus := 0;
  FEmplWorkHrsPerf := 0;
  QRGroupHdEmpl.ForceNewPage := QRParameters.FPageEmpl;
  inherited;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableEmpl.Active then
      TableEmpl.Active := True;
    TableEmpl.FindKey([QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger]);
    QRLabelEmplDesc.Caption := TableEmpl.FieldByName('DESCRIPTION').AsString;
}
    QRLabelEmplDesc.Caption := QueryProdHour.FieldByName('EDESCRIPTION').AsString;
  end;
  PrintBand := QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRGroupHDDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetail;
  with ReportTeamIncentiveDM do
  begin
  QRLabelDay.Caption := SystemDM.GetDayWDescription(ListProcsF.
    DayWStartOnFromDate(
      QueryProdHour.FieldByName('SHIFT_DATE').AsDateTime)) + ' (' +
    DateTimeToStr(
      QueryProdHour.FieldByName('SHIFT_DATE').AsDateTime) + ')';
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FBUWorkHrs := 0;
  FBUSalHrs := 0;
  FBUBonus := 0;
  FBUWorkHrsPerf := 0;
  PrintBand := QRParameters.FShowDetail;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableBU.Active then
      TableBU.Active := True;
    TableBU.FindKey([QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString]);
    QRLabelBUDesc.Caption := TableBU.FieldByName('DESCRIPTION').AsString;
}
    QRLabelBUDesc.Caption := QueryProdHour.FieldByName('BDESCRIPTION').AsString;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FDeptWorkHrs := 0;
  FDeptSalHrs := 0;
  FDeptBonus := 0;
  FDeptWorkHrsPerf := 0;
  PrintBand := QRParameters.FShowDetail;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableDept.Active then
      TableDept.Active := True;
    TableDept.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
      QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString]);
    QRLabelDeptDesc.Caption := TableDept.FieldByName('DESCRIPTION').AsString;
}
    QRLabelDeptDesc.Caption := QueryProdHour.FieldByName('DDESCRIPTION').AsString;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FWKWorkHrs := 0;
  FWKSalHrs := 0;
  FWKNorm := 0;
  FWKWorkHrsPerf := 0;
  FTotalWKPieces := 0;
  FWKBonus := 0;
  PrintBand := QRParameters.FShowDetail;
  with ReportTeamIncentiveDM do
  begin
{
    if not TableWK.Active then
      TableWK.Active := True;
    TableWK.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
      QueryProdHour.FieldByName('WORKSPOT_CODE').AsString]);
    QRLabelWKDesc.Caption := TableWK.FieldByName('DESCRIPTION').AsString;
    FDept := TableWK.FieldByName('DEPARTMENT_CODE').AsString;
}
    QRLabelWKDesc.Caption := QueryProdHour.FieldByName('WDESCRIPTION').AsString;
    FDept := QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString; // WDEPARTMENT_CODE
  end;
end;

function TReportTeamIncentiveQR.GetSTRUntilSpace(var TempStr: String): String;
var
  PosStr: Integer;
begin
  Result := '';
  PosStr := Pos(CHR_SEP, TempStr);
  if PosStr > 0 then
  begin
    Result := Copy(TempStr, 0, PosStr - 1);
    TempStr := Copy(TempStr, PosStr + 1, 50);
  end;
end;

(*
procedure TReportTeamIncentiveQR.GetProdQStrings(ProdQString: String;
  var Plant, WKCode, JobCode: String;
  var StartDate, EndDate: TDateTime;
  var Quantity: Real);
var
 QuantityStr, EndDateStr, StartDateStr: String;
begin
  Plant := ''; WKCode := ''; JobCode := '';
  Plant := GetSTRUntilSpace(ProdQString);
  if Plant = '' then
    Exit;
  WKCode := GetSTRUntilSpace(ProdQString);
  if WKCode = '' then
    Exit;
  JobCode := GetSTRUntilSpace(ProdQString);
  if JobCode = '' then
    Exit;
  StartDateStr := GetSTRUntilSpace(ProdQString);
  if StartDateStr = '' then
    Exit
  else
    StartDate := StrToFloat(StartDateStr);
  EndDateStr := GetSTRUntilSpace(ProdQString);
  if EndDateStr = '' then
    Exit
  else
    EndDate := StrToFloat(EndDateStr);
  QuantityStr := ProdQString;
  if QuantityStr <> '' then
    Quantity := StrToFloat(QuantityStr);
end;
*)

// MR:24-01-2003 Procedure changed because determination of
// 'total pieces' was wrong.
procedure TReportTeamIncentiveQR.QRGroupJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Performance: Double;
//  MinDate: TDateTime;
  EmplTotal: Double;
//  CurrentEmpl: Integer;
  EmplPieces: Double;
begin
  inherited;
  PrintBand := QRParameters.FShowDetail and not QRParameters.FOnlyJob;
  FJobBonus := 0.00;
// begin total pieces
  FTotalJobPieces := 0;
  FJobWorkHrs := 0;
  FJobSalHrs := 0;
  FJobWorkHrsPerf := 0;
  if not QRParameters.FOnlyJob then
  begin
    with ReportTeamIncentiveDM do
    begin
//      MinDate := QueryProdHour.FieldByName('SHIFT_DATE').AsDateTime;
//      CurrentEmpl := QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    end;

    EmplPieces := 0;

    if IncludeJobQuantity then
      EmplPieces :=
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('EMPLOYEEQUANTITY').AsFloat;
(*
    // 20014550.50
    // Get Employee Pieces
    with ReportProductionDetailDM do
    begin
      if QueryProduction.Locate(
        'PLANT_CODE;BUSINESSUNIT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;SHIFT_DATE',
        VarArrayOf([ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString,
        CurrentEmpl,
        MinDate]),
        []) then
      begin
        if IncludeJobQuantity then
          EmplPieces :=
            QueryProduction.FieldByName('EMPLOYEEQUANTITY').AsFloat;
      end;
    end;
*)    
    EmplTotal := EmplPieces;
    FTotalJobPieces := EmplTotal;

  end;
{
    // Get Employee Pieces
    with ReportProductionDetailDM do
    begin
      if cdsEmpPiecesDate.Locate(
        'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;START_DATE',
        VarArrayOf([ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('WDEPARTMENT_CODE').AsString,
        CurrentEmpl,
        MinDate]),
        []) then
      begin
        // MR:15-02-2005 Also check on businessunit!
        if cdsEmpPiecesDate.FieldByName('BUSINESSUNIT_CODE').AsString =
          ReportTeamIncentiveDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString then
        begin
          // 20013478.
          if IncludeJobQuantity // 20015586
            then
            EmplPieces :=
              cdsEmpPiecesDate.FieldByName('EMPPIECES').AsFloat;

          if UseDateTime or IncludeOpenScans then // 20015520 + 20015221
            FJobWorkHrs := FJobWorkHrs +
              cdsEmpPiecesDate.FieldByName('PRODMINS').AsInteger;
        end;
      end;
    end;
    EmplTotal := EmplPieces;
    FTotalJobPieces := EmplTotal;
  end;
}

  with ReportTeamIncentiveDM do
  begin
    QRLabelJobDesc.Caption := QueryProdHour.FieldByName('JDESCRIPTION').AsString;

    // RV048.3.
    if QRParameters.FOnlyJob then
      QRLabelJobDescF.Caption :=
        QueryProdHour.FieldByName('JDESCRIPTION').AsString + ' (' +
        QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + ')'
    else
      QRLabelJobDescF.Caption :=
        QueryProdHour.FieldByName('JDESCRIPTION').AsString;

    FJob_NormProd := QueryProdHour.FieldByName('NORM_PROD_LEVEL').AsFloat;

    QRLabelNorm.Caption := Format('%8.2n', [FJob_NormProd]);
    QRLabelNormF.Caption := QRLabelNorm.Caption;
    FBonusFactor := QueryProdHour.FieldByName('BONUS_FACTOR').AsFloat;
  end;
  if QRParameters.FOnlyJob then
    exit;

  // MR:23-02-2005 Pims -> Oracle, sum-result must be 'float'
  if not (UseDateTime or IncludeOpenScans) then // 20015520 + 20015221
  begin
    FJobWorkHrs :=
      Round(ReportTeamIncentiveDM.QueryProdHour.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60); // WORKEDHRS
    FJobSalHrs :=
      Round(ReportTeamIncentiveDM.QueryProdHour.FieldByName('PRODHOURMIN').AsFloat); // PIM-135
  end;
  FJobNorm := (FJob_NormProd * FJobWorkHrs)/60;
  // MR:03-04-2006 Order 550424. Calculate Performance: WorkedHrs * Performance
  if FJobNorm <> 0 then
    FJobWorkHrsPerf := FJobWorkHrs * (FTotalJobPieces / FJobNorm * 100)
  else
    FJobWorkHrsPerf := 0;

  FWKNorm := FWKNorm + FJobNorm;
  FWKWorkHrs := FWKWorkHrs + FJobWorkHrs;
  FWKWorkHrsPerf := FWKWorkHrsPerf + FJobWorkHrsPerf;
  FDeptWorkHrs := FDeptWorkHrs + FJobWorkHrs;
  FDeptWorkHrsPerf := FDeptWorkHrsPerf + FJobWorkHrsPerf;
  FBUWorkHrs := FBUWorkHrs + FJobWorkHrs;
  FBUWorkHrsPerf := FBUWorkHrsPerf + FJobWorkHrsPerf;
  FEmplWorkHrs := FEmplWorkHrs + FJobWorkHrs;
  FEmplWorkHrsPerf := FEmplWorkHrsPerf + FJobWorkHrsPerf;
  FPlantWorkHrs := FPlantWorkHrs + FJobWorkHrs;
  FPlantWorkHrsPerf := FPlantWorkHrsPerf + FJobWorkHrsPerf;
  FTotalWKPieces := FTotalWKPieces + FTotalJobPieces;

  // PIM-135
  FWKSalHrs := FWKSalHrs + FJobSalHrs;
  FDeptSalHrs := FDeptSalhrs + FJobSalHrs;
  FBUSalHrs := FBUSalHrs + FJobSalHrs;
  FEmplSalhrs := FEmplSalHrs + FJobSalHrs;
  FPlantSalHrs := FPlantSalHrs + FJobSalHrs;

  if FJobNorm <> 0 then
  begin
    Performance :=  (FTotalJobPieces / FJobNorm * 100) - 100;
    FJobBonus := 0;
    if Performance > 0 then
    begin
      FJobBonus := (Performance * FJobWorkHrs * FBonusFactor) / 60;
      FWKBonus := FWKBonus + FJobBonus;
      FEmplBonus := FEmplBonus + FJobBonus;
      FDeptBonus := FDeptBonus + FJobBonus;
      FBUBonus := FBUBonus + FJobBonus;
      FPlantBonus := FPlantBonus + FJobBonus;
    end;
  end;

  // 20015220 When nothing was found do not print this line
  if (FTotalJobPieces = 0) and (FJobWorkHrs = 0) and (FJobSalhrs = 0) then // PIM-135
    PrintBand := False;
end;

(*
procedure TReportTeamIncentiveQR.GetTRSStrings(TRSStr: String;
  var TRSPlant, TRSWK, TRSJob: String;
  var TRSEmpl: Integer;
  var DateTimeIn, DateTimeOut: TDateTime);
var
  TRSEmplStr, DateTimeInStr, DateTimeOutStr: String;
begin
  TRSPlant := ''; TRSWK := ''; TRSJob := ''; TRSEmpl := 0;
  DateTimeIn := 0; DateTimeOut := 0;
  TRSPlant := GetSTRUntilSpace(TRSStr);
  if TRSPlant = '' then
    Exit;
  TRSEmplStr := GetSTRUntilSpace(TRSStr);
  if TRSEmplStr = '' then
    Exit;
  TRSEmpl := StrToInt(TRSEmplStr);
  DateTimeInStr := GetSTRUntilSpace(TRSStr);
  if DateTimeInStr = '' then
    Exit
  else
    DateTimeIn := StrToFloat(DateTimeInStr);
  DateTimeOutStr := GetSTRUntilSpace(TRSStr);
  if DateTimeOutStr = '' then
    Exit
  else
    DateTimeOut := StrToFloat(DateTimeOutStr);
  TRSWK := GetSTRUntilSpace(TRSStr);
  if TRSWK = '' then
    Exit;
  TRSJob := TRSStr;
  if TRSJob = '' then
    Exit;
end;
*)

(*
function TReportTeamIncentiveQR.GetTotalPieces(StartDate, EndDate: TDateTime;
   Empl: Integer; QuantPerMin: Real; Plant, WK, Job: String): Real;
var
  TRSPlant, TRSWK, TRSJob: String;
  TRSEmpl, i: Integer;
  DateTimeIn, DateTimeOut: TDateTime;
  TotPieces: Real;
begin
  TotPieces := 0;
//  Result := 0;
  for i:= 0 to FTRSList.Count - 1 do
  begin
    ATRSRecord := FTRSList.Items[i];
    TRSPlant := ATRSRecord.PlantCode;
    TRSWK := ATRSRecord.WorkspotCode;
    TRSJob := ATRSRecord.JobCode;
    TRSEmpl := ATRSRecord.EmployeeNumber;
    DateTimeIn := ATRSRecord.DateTimeIn;
    DateTimeOut := ATRSRecord.DateTimeOut;

    if ((TRSEmpl = Empl)and (Plant = TRSPlant) and (WK = TRSWK) and
        (Job = TRSJob)) then
      TotPieces := TotPieces +
        GetPiecesOnInt(Max(DateTimeIn, StartDate), Min(DateTimeOut, EndDate),
          Empl, QuantPerMin, TRSPlant, TRSWK, TRSJob);
    if (CompareDateTime(DateTimeIn, EndDate) = 1) then
    begin
      Result := TotPieces;
      Exit;
    end;
  end;
  Result := TotPieces;
end;
*)

(*
// datetime1 < datetime2 then result = -1
// datetime1 > datetime2 then result = 1
// datetime1 = datetime2 then result = 0
function TReportTeamIncentiveQR.CompareDateTime(DateTime1, DateTime2: TDateTime):
  Integer;
var
  Hrs1, Min1, Sec, MSec, Hrs2, Min2: Word;
  Year1, Year2, Month1, Month2, Day1, Day2: Word;
begin
  Result := -1;
  DecodeTime(DateTime1, Hrs1, Min1, Sec, MSec);
  DecodeDate(DateTime1, Year1, Month1, Day1);
  DecodeTime(DateTime2, Hrs2, Min2, Sec, MSec);
  DecodeDate(DateTime2, Year2, Month2, Day2);
  if (Year1 > Year2) or
    ((Year1 = Year2) and (Month1 > Month2)) or
    ((Year1 = Year2) and (Month1 = Month2) and (Day1 > Day2)) or
    ((Year1 = Year2) and (Month1 = Month2) and (Day1 = Day2) and (Hrs1 > Hrs2)) or
    ((Year1 = Year2) and (Month1 = Month2) and (Day1 = Day2) and (Hrs1 = Hrs2) and
     (Min1 > Min2)) then
     Result := 1
  else
  begin
    if ((Year1 = Year2) and (Month1 = Month2) and (Day1 = Day2) and (Hrs1 = Hrs2) and
     (Min1 = Min2)) then
       Result := 0
  end;
end;
*)

(*
function TReportTeamIncentiveQR.GetPiecesOnInt(StartDate, EndDate: TDateTime;
   Empl: Integer; QuantPerMin: Real; Plant, WK, Job: String): Real;
var
  TRSPlant, TRSWK, TRSJob: String;
  TRSEmpl, EmplNr, i: Integer;
  TimeIn, TimeOut,
  DateTimeIn, DateTimeOut: TDateTime;
  TotPieces: Real;
begin
  TotPieces := 0;
  TimeIn := StartDate;
  TimeOut := TimeIn;
  while TimeIn <= EndDate do
  begin
    Timeout := AddMinute(TimeOut);
    if  (CompareDateTime(TimeOut, EndDate) = 1) then
    begin
      Result := TotPieces;
      Exit;
    end;
    EmplNr := 1;
    for i:= 0 to FTRSList.Count - 1 do
    begin
      ATRSRecord := FTRSList.Items[i];
      TRSPlant := ATRSRecord.PlantCode;
      TRSWK := ATRSRecord.WorkspotCode;
      TRSJob := ATRSRecord.JobCode;
      TRSEmpl := ATRSRecord.EmployeeNumber;
      DateTimeIn := ATRSRecord.DateTimeIn;
      DateTimeOut := ATRSRecord.DateTimeOut;
      if (TRSEmpl <> Empl) and (Plant = TRSPlant) and (WK = TRSWK) and
        (Job = TRSJob) and
        (CompareDateTime(DateTimeIn, TimeIn) <= 0) and
        (CompareDateTime(DateTimeOut, TimeOut) >= 0) then
        EmplNr := EmplNr + 1;
      if DateTimeIn > TimeOut then
        Break;
    end;
    TotPieces := TotPieces + (QuantPerMin / EmplNr);
    TimeIn := Timeout;
  end;
  Result := TotPieces;
end;
*)

procedure TReportTeamIncentiveQR.QRLabelWKFDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013965 Empty description
//  Value := QRLabelWKDesc.Caption;
  Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelDeptFDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013965 Empty description
//  Value := QRLabelDeptDesc.Caption;
  Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelBUFDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013965 Empty description
//  Value := QRLabelBUDesc.Caption;
  Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelPlantFDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013965 Empty description
//  Value := QRLabelPlant.Caption;
  Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRBandFooterJobCodeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Performance: Double;
begin
  inherited;
 
  PrintBand := QRParameters.FShowDetail and  QRParameters.FOnlyJob;
  if not QRParameters.FOnlyJob then
    exit;
  FJobNorm := (FJob_NormProd * FJobWorkHrs)/60;

  // 20015220 When nothing was found, then do not print this line
  if (FJobWorkHrs = 0) and (FTotalJobPieces = 0) and (FJobSalHrs = 0) then // PIM-135
    PrintBand := False;


  FWKNorm := FWKNorm + FJobNorm;
  FWKWorkHrs := FWKWorkHrs + FJobWorkHrs;
  FWKWorkHrsPerf := FWKWorkHrsPerf + FJobWorkHrsPerf;
  FDeptWorkHrs := FDeptWorkHrs + FJobWorkHrs;
  FDeptWorkHrsPerf := FDeptWorkHrsPerf + FJobWorkHrsPerf;
  FBUWorkHrs := FBUWorkHrs + FJobWorkHrs;
  FBUWorkHrsPerf := FBUWorkHrsPerf + FJobWorkHrsPerf;
  FEmplWorkHrs := FEmplWorkHrs + FJobWorkHrs;
  FEmplWorkHrsPerf := FEmplWorkHrsPerf + FJobWorkHrsPerf;
  FPlantWorkHrs := FPlantWorkHrs + FJobWorkHrs;
  FPlantWorkHrsPerf := FPlantWorkHrsPerf + FJobWorkHrsPerf;
  FTotalWKPieces := FTotalWKPieces + FTotalJobPieces;

  // PIM-135
  FWKSalHrs := FWKSalHrs + FJobSalHrs;
  FDeptSalHrs := FDeptSalHrs + FJobSalHrs;
  FBUSalHrs := FBUSalHrs + FJobSalHrs;
  FEmplSalHrs := FEmplSalHrs + FJobSalHrs;
  FPlantSalHrs := FPlantSalHrs + FJobSalHrs;

  if FJobNorm <> 0 then
  begin
    Performance :=  (FTotalJobPieces / FJobNorm * 100) - 100;
    FJobBonus := 0;
    if Performance > 0 then
    begin
      FJobBonus := (Performance * FJobWorkHrs * FBonusFactor) / 60;
      FWKBonus := FWKBonus + FJobBonus;
      FEmplBonus := FEmplBonus + FJobBonus;
      FDeptBonus := FDeptBonus + FJobBonus;
      FBUBonus := FBUBonus + FJobBonus;
      FPlantBonus := FPlantBonus + FJobBonus;
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDetail;
  if PrintBand then // 20015220 + 20015221
    if ((FBUWorkHrs = 0) and (FBUWorkHrsPerf = 0) and (FBUBonus = 0) and (FBUSalHrs = 0)) then // PIM-135
      PrintBand := False;
end;

procedure TReportTeamIncentiveQR.QRLabelEmpDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  // 20013965 Empty description
//  Value := QRLabelEmplDesc.Caption;
  Value := '';
  // 20013965.60 Name of employee must be shown here.
  if not QRParameters.FShowDetail then
    Value := ReportTeamIncentiveDM.QueryProdHour.
      FieldByName('EDESCRIPTION').AsString;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblJobWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FJobWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblWorkspotWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FWKWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblDeptWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FDeptWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblBUWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FBUWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblPlantWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FPlantWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblEmpWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FEmplWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblSummaryWrkHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FTOTWorkHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelNormAmountPrint(sender: TObject;
  var Value: String);
begin
  inherited;
// TD-21526 [ROP 4-dec-2012]  Value := Format('%8.2f', [FJobNorm]);
// ROP 05-MAR-2013 TD-21527   Value := Format('%8.0f', [FJobNorm]);
  Value := Format('%8.0n', [FJobNorm]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel6Print(sender: TObject;
  var Value: String);
begin
  inherited;
// TD-21526 [ROP 4-dec-2012]  Value := Format('%8.2f', [FWKNorm]);
// ROP 05-MAR-2013 TD-21527   Value := Format('%8.0f', [FWKNorm]);
  Value := Format('%8.0n', [FWKNorm]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FTotWorkHrs := FTotWorkHrs + FPlantWorkHrs;
  FTotSalHrs := FTotSalHrs + FPlantSalhrs;
  FTotWorkHrsPerf := FTotWorkHrsPerf + FPlantWorkHrsPerf;
  FTotBonus := FTotBonus + FPlantBonus;
end;

procedure TReportTeamIncentiveQR.FormDestroy(Sender: TObject);
(* var
  I: Integer; *)
begin
  inherited;
(*
  if FTRSList <> nil then
  begin
    for I:=FTRSList.Count - 1  downto 0 do
    begin
      ATRSRecord := FTRSList.Items[I];
      Dispose(ATRSRecord);
      FTRSList.Remove(ATRSRecord);
    end;
    FTRSList.Free;
  end;
*)
end;

procedure TReportTeamIncentiveQR.QRLabelTotPiecesPrint(sender: TObject;
  var Value: String);
begin
  inherited;
// TD-21526 [ROP 12-feb-2012]  Value := Format('%8.2f', [FTotalJobPieces]);
//  Value := Format('%8.0f', [FTotalJobPieces]);
// TD-21526 MRA
// ROP 05-MAR-2013 TD-21527   Value := Format('%8.2f', [FTotalJobPieces]);
  Value := Format('%8.2n', [FTotalJobPieces]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel38Print(sender: TObject;
  var Value: String);
begin
  inherited;
// TD-21526 [ROP 12-feb-2012]  Value := Format('%8.2f', [FTotalWKPieces]);
//  Value := Format('%8.0f', [FTotalWKPieces]);
// TD-21526 MRA
// ROP 05-MAR-2013 TD-21527   Value := Format('%8.2f', [FTotalWKPieces]);
  Value := Format('%8.2n', [FTotalWKPieces]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelPiecesPerHPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if FJobWorkHrs <> 0 then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [((FTotalJobPieces/FJobWorkHrs)*60)])
  Value := Format('%8.2n', [((FTotalJobPieces/FJobWorkHrs)*60)])
  else
    Value :=  '    0.00';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel39Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FWKWorkHrs <> 0 then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [(FTotalWKPieces/ FWKWorkHrs)*60])
  Value := Format('%8.2n', [(FTotalWKPieces/ FWKWorkHrs)*60])
  else
    Value :=  '    0.00';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelPerformancePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if FJobNorm <> 0 then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [(FTotalJobPieces/ FJobNorm * 100)])
  Value := Format('%8.2n', [(FTotalJobPieces/ FJobNorm * 100)])
  else
    Value := '    0.00';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel40Print(sender: TObject;
  var Value: String);
begin
  inherited;
// MR:03-04-2006 Order 550424. Other calculation for performance.
  if FWKWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [FWKWorkHrsPerf / FWKWorkHrs])
  Value := Format('%8.2n', [FWKWorkHrsPerf / FWKWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [0.0]);
  Value := Format('%8.2n', [0.0]);
{
  if FWKNorm <> 0 then
    Value := Format('%8.2f', [(FTotalWKPieces/ FWKNorm * 100)])
  else
    Value :=  '    0.00';
}
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelWKBonusPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [FWKBonus])
  Value := Format('%8.2n', [FWKBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelDeptBonusPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [FDeptBonus])
  Value := Format('%8.2n', [FDeptBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelBUBonusPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527     Value := Format('%8.2f', [FBUBonus])
  Value := Format('%8.2n', [FBUBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel27Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FEmplBonus])
    Value := Format('%8.2n', [FEmplBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabelPlantBonusPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FPlantBonus])
    Value := Format('%8.2n', [FPlantBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel42Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FTotBonus])
    Value := Format('%8.2n', [FTotBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.ChildBandEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRLabel46Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FJobBonus])
    Value := Format('%8.2n', [FJobBonus])
  else
    Value := '';
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
//  MinDate: TDateTime;
  EmplTotal: Double;
//  CurrentEmpl: Integer;
  Dept: String;
  EmplPieces: Double;
begin
  inherited;
  PrintBand := False;
  if not QRParameters.FOnlyJob then
     exit;
  with ReportTeamIncentiveDM do
  begin
   // FTotalJobPieces := 0;
//    MinDate := QueryProdHour.FieldByName('SHIFT_DATE').AsDateTime;
//    CurrentEmpl := QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  end;
  Dept :=
    ReportTeamIncentiveDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString; // WDEPARTMENT_CODE

  // Get Employee Pieces
  // 20014550.50
  EmplPieces := 0;
  if IncludeJobQuantity then
    EmplPieces :=
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('EMPLOYEEQUANTITY').AsFloat;
(*
  with ReportProductionDetailDM do
  begin
    if QueryProduction.Locate(
      'PLANT_CODE;BUSINESSUNIT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;SHIFT_DATE',
      VarArrayOf([ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString, // WDEPARTMENT_CODE
      CurrentEmpl,
      MinDate]),
      []) then
    begin
      if IncludeJobQuantity then
        EmplPieces :=
          QueryProduction.FieldByName('EMPLOYEEQUANTITY').AsFloat;
    end;
  end;
*)

(*
  EmplPieces := 0;
  with ReportProductionDetailDM do
  begin
    cdsEmpPiecesDate.Filtered := False;
    cdsEmpPiecesDate.Filter :=
      'PLANT_CODE = ' +
       '''' + ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString + '''' + ' AND ' +
      'WORKSPOT_CODE = ' +
      '''' + ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + '''' + ' AND ' +
      'JOB_CODE = ' +
      '''' + ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString + ''''  + ' AND ' +
      'DEPARTMENT_CODE = ' +
      '''' + ReportTeamIncentiveDM.QueryProdHour.FieldByName('WDEPARTMENT_CODE').AsString + ''''  + ' AND ' +
      'EMPLOYEE_NUMBER = ' + IntToStr(CurrentEmpl) + ' AND ' +
      'BUSINESSUNIT_CODE = ' +
      '''' + ReportTeamIncentiveDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString  + '''';
    cdsEmpPiecesDate.Filtered := True;
    if not cdsEmpPiecesDate.IsEmpty then
    begin
      begin
        cdsEmpPiecesDate.First;
        while not cdsEmpPiecesDate.Eof do
        begin
          if cdsEmpPiecesDate.FieldByName('START_DATE').AsDateTime = MinDate then
          begin
            // 20013478.
            if IncludeJobQuantity // 20015586
              then
              EmplPieces := EmplPieces +
                cdsEmpPiecesDate.FieldByName('EMPPIECES').AsFloat;
          end;

          cdsEmpPiecesDate.Next;
        end;
      end;
    end;
  end;
*)
  EmplTotal := EmplPieces;
// 20014550.50 Do not use this!
(*
  if UseDateTime or IncludeOpenScans then // 20015220 + 20015221
    FJobWorkHrs := FJobWorkHrs +
      ReportProductionDetailDM.DetermineTimeRecProdMins(
        QRParameters.FDateFrom, QRParameters.FDateTo,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('WDEPARTMENT_CODE').AsString,
        ReportTeamIncentiveDM.QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString,
        CurrentEmpl,
        0,
        MinDate,
        0);
*)

  FTotalJobPieces := FTotalJobPieces + EmplTotal;
  // MR:23-02-2005 Pims -> Oracle, sum-result must be 'float'
  if not (UseDateTime or IncludeOpenScans) then // 20015220 + 20015221
  begin
    FJobWorkHrs := FJobWorkHrs +
      Round(ReportTeamIncentiveDM.QueryProdHour.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60); // WORKEDHRS
    FJobSalHrs := FJobSalHrs +
      Round(ReportTeamIncentiveDM.QueryProdHour.FieldByName('PRODHOURMIN').AsFloat); // PIM-135
  end;
  // MR:03-04-2006 Order 550424. Calculate Performance: WorkedHrs * Performance
{
  if not ReportTeamIncentiveDM.TableJob.Active then
    ReportTeamIncentiveDM.TableJob.Active := True;
  ReportTeamIncentiveDM.TableJob.
    FindKey([
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('PLANT_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
      ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString]);
  FJob_NormProd := ReportTeamIncentiveDM.
    TableJob.FieldByName('NORM_PROD_LEVEL').AsFloat;
}
  FJob_NormProd := ReportTeamIncentiveDM.
    QueryProdHour.FieldByName('NORM_PROD_LEVEL').AsFloat;
  FJobNorm := (FJob_NormProd * FJobWorkHrs) / 60;
  if FJobNorm <> 0 then
    FJobWorkHrsPerf := FJobWorkHrsPerf +
      (FJobWorkHrs * (FTotalJobPieces / FJobNorm * 100));

  // 20015220 If nothing was found then do not print the line
  if (FJobWorkHrs = 0) and (FTotalJobPieces = 0) and (FJobSalHrs = 0) then // PIM-135
    PrintBand := False;
end;

procedure TReportTeamIncentiveQR.ChildBandPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := not QRParameters.FShowDetail;
end;

procedure TReportTeamIncentiveQR.QRLabel17Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowDetail then
    Value := ''
  else
    Value := SRepTeamTotEmpl;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRBandFooterEmpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // MRA:24-FEB-2009 Added more space between Emp-number and descr.
  if not QRParameters.FShowDetail then
  begin
    QRDBTextEmpl.Left := 0;
    QRLabelEmpDesc.Left := 50;  // 30
  end
  else
  begin
    QRDBTextEmpl.Left := 92;
    QRLabelEmpDesc.Left := 144;
  end;
end;

procedure TReportTeamIncentiveQR.QRLabelWKPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FOnlyJob then
    Value := SPimsJobs
  else
    Value := SPimsColumnWorkspot;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel7Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FOnlyJob then
    Value := ''
  else
    Value := SPimsJobs;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel58Print(sender: TObject;
  var Value: String);
begin
  inherited;
// [ROP] 20013965
//  Value := (Sender as TQRLabel).Caption;
// [ROP] 20013965 end
  if not QRParameters.FShowIncentive then
    Value := ''
  else
    Value := BonusDescription; // 20013965
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel37Print(sender: TObject;
  var Value: String);
begin
  inherited;
// [ROP] 20013965
//  Value := (Sender as TQRLabel).Caption;
// [ROP] 20013965 end
  if not QRParameters.FShowIncentive then
    Value := ''
  else
    Value := BonusDescription; // 20013965
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabel32.Caption + ExportClass.Sep + // Total
        ExportClass.Sep +
        ExportClass.Sep +
        QRLblSummaryWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblSummarySalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblSummaryDiff.Caption + ExportClass.Sep + // Diff
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel66.Caption + ExportClass.Sep + // Performance
        QRLabel42.Caption // Bonus
        );
    end;
  end;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHdPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(QRLabel54.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabelPlant.Caption);
    end;
  end;
end;

procedure TReportTeamIncentiveQR.ChildBandPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel52.Caption + ExportClass.Sep + // Employee
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel57.Caption + ' ' + QRLabel59.Caption + ExportClass.Sep + // Worked hours
      QRLabel77.Caption + ' ' + QRLabel79.Caption + ExportClass.Sep + // Salary hours
      QRLabel84.Caption + ExportClass.Sep + // Difference
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel72.Caption + ExportClass.Sep + // Performance
      QRLabel58.Caption); // Bonus
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDEmplAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(QRLabel106.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsString +
          ExportClass.Sep +
        QRLabelEmplDesc.Caption);
    end;
  end;
end;

procedure TReportTeamIncentiveQR.ChildBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabelWK.Caption + ExportClass.Sep + // Workspot
      QRLabel7.Caption + ExportClass.Sep + // Job
      QRLabel56.Caption + ExportClass.Sep + // Norm
      QRLabel8.Caption + ' ' + QRLabel9.Caption + ExportClass.Sep +  // Worked hours
      QRLabel80.Caption + ' ' + QRLabel81.Caption + ExportClass.Sep + // Salary hours
      QRLabel85.Caption + ExportClass.Sep + // Diff.
      QRLabel10.Caption + ' ' + QRLabel12.Caption + ExportClass.Sep + // Norm amount
      QRLabel14.Caption + ' ' + QRLabel15.Caption + ExportClass.Sep + // Total pieces
      QRLabel33.Caption + ' ' + QRLabel34.Caption + ExportClass.Sep + // Pieces per hour
      QRLabel36.Caption + ExportClass.Sep + // Performance
      QRLabel37.Caption); // Bonus
{    ExportClass.AddText(
      QRLabel7.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel9.Caption + ExportClass.Sep +
      QRLabel12.Caption + ExportClass.Sep +
      QRLabel15.Caption + ExportClass.Sep +
      QRLabel34.Caption + ExportClass.Sep +
      ExportClass.Sep); }
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDDateAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(QRLabelDay.Caption);
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDBUAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(QRLabel55.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString +
          ExportClass.Sep +
        QRLabelBUDesc.Caption);
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDDeptAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(QRLabel35.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString +
          ExportClass.Sep +
        QRLabelDeptDesc.Caption);
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupHDWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QueryProdHour.FieldByName('WORKSPOT_CODE').AsString +
          ExportClass.Sep +
        QRLabelWK.Caption);
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRGroupJobAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QueryProdHour.FieldByName('JOB_CODE').AsString + // Job
        ExportClass.Sep +
        QRLabelJobDesc.Caption + ExportClass.Sep +
        QRLabelNorm.Caption + ExportClass.Sep + // Norm
        QRLblJobWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblJobSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblJobDiff.Caption + ExportClass.Sep + // Diff
        QRLabelNormAmount.Caption + ExportClass.Sep + // Norm amount
        QRLabelTotPieces.Caption + ExportClass.Sep + // Total pieces
        QRLabelPiecesPerH.Caption + ExportClass.Sep + // Pieces per hour
        QRLabelPerformance.Caption + ExportClass.Sep + // Performance
        QRLabel46.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterJobCodeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QueryProdHour.FieldByName('JOB_CODE').AsString + // Job
        ExportClass.Sep +
        QRLabelJobDescF.Caption + ExportClass.Sep +
        QRLabelNormF.Caption + ExportClass.Sep + // Norm
        QRLblDetailWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblDetailSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblDetailDiff.Caption + ExportClass.Sep + // Diff
        QRLabel61.Caption + ExportClass.Sep + // Norm amount
        QRLabel62.Caption + ExportClass.Sep + // Total pieces
        QRLabel63.Caption + ExportClass.Sep + // Pieces per hour
        QRLabel64.Caption + ExportClass.Sep + // Performance
        QRLabel65.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabelTotJob.Caption + ExportClass.Sep + // Job
        QueryProdHour.FieldByName('WORKSPOT_CODE').AsString + ' ' +
          QRLabelWKFDesc.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLblWorkspotWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblWorkspotSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblWorkspotDiff.Caption + ExportClass.Sep + // Diff
        QRLabel6.Caption + ExportClass.Sep + // Norm amount
        QRLabel38.Caption + ExportClass.Sep + // Total pieces
        QRLabel39.Caption + ExportClass.Sep + // Pieces per hour
        QRLabel40.Caption + ExportClass.Sep + // Performance
        QRLabelWKBonus.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabelTotDept.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
          QRLabelDeptFDesc.Caption + ExportClass.Sep + // Dept
        ExportClass.Sep +
        QRLblDeptWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblDeptSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblDeptDiff.Caption + ExportClass.Sep + // Diff
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel71.Caption + ExportClass.Sep + // Performance
        QRLabelDeptBonus.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterBUAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabelTotBU.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('BUSINESSUNIT_CODE').AsString + ' ' +
          QRLabelBUFDesc.Caption + ExportClass.Sep + // Business Unit
        ExportClass.Sep +
        QRLblBUWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblBUSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblBUDiff.Caption + ExportClass.Sep + // Diff
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel70.Caption + ExportClass.Sep + // Performance
        QRLabelBUBonus.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterEmpAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabel17.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('EMPLOYEE_NUMBER').AsString + ' ' +
          QRLabelEmpDesc.Caption + ExportClass.Sep + // Employee
        ExportClass.Sep +
        QRLblEmpWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblEmpSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblEmpDiff.Caption + ExportClass.Sep + // Diff
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel69.Caption + ExportClass.Sep + // Performance
        QRLabel27.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportTeamIncentiveDM do
    begin
      ExportClass.AddText(
        QRLabelTotPlant.Caption + ExportClass.Sep +
        QueryProdHour.FieldByName('PLANT_CODE').AsString + ' ' +
          QRLabelPlantFDesc.Caption + ExportClass.Sep + // Plant
        ExportClass.Sep +
        QRLblPlantWrkHrs.Caption + ExportClass.Sep + // Worked hours
        QRLblPlantSalHrs.Caption + ExportClass.Sep + // Salary hours
        QRLblPlantDiff.Caption + ExportClass.Sep + // Diff
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        QRLabel68.Caption + ExportClass.Sep + // Performance
        QRLabelPlantBonus.Caption // Bonus
        );
    end;
  end;
end;

procedure TReportTeamIncentiveQR.QRLabel66Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FTOTWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FTOTWorkHrsPerf / FTOTWorkHrs])
    Value := Format('%8.2n', [FTOTWorkHrsPerf / FTOTWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [0.0]);
    Value := Format('%8.2n', [0.0]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel68Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FPlantWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FPlantWorkHrsPerf / FPlantWorkHrs])
    Value := Format('%8.2n', [FPlantWorkHrsPerf / FPlantWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [0.0]);
    Value := Format('%8.2n', [0.0]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel69Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FEmplWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FEmplWorkHrsPerf / FEmplWorkHrs])
    Value := Format('%8.2n', [FEmplWorkHrsPerf / FEmplWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [0.0]);
    Value := Format('%8.2n', [0.0]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel70Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FBUWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FBUWorkHrsPerf / FBUWorkHrs])
    Value := Format('%8.2n', [FBUWorkHrsPerf / FBUWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [0.0]);
    Value := Format('%8.2n', [0.0]);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLabel71Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if FDeptWorkHrs > 0 then
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [FDeptWorkHrsPerf / FDeptWorkHrs])
    Value := Format('%8.2n', [FDeptWorkHrsPerf / FDeptWorkHrs])
  else
// ROP 05-MAR-2013 TD-21527    Value := Format('%8.2f', [0.0]);
    Value := Format('%8.2n', [0.0]);
  (Sender as TQRLabel).Caption := Value;
end;

// RV102.3. Be sure it is emptied.
procedure TReportTeamIncentiveQR.QRLabelWeekPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
end;

// 20015586
function TReportTeamIncentiveQR.IncludeJobQuantity: Boolean;
begin
  // 20014550.50
  Result :=
    ReportTeamIncentiveDM.QueryProdHour.
      FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y';
  if (ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString = DOWNJOB_1) or
    (ReportTeamIncentiveDM.QueryProdHour.FieldByName('JOB_CODE').AsString = DOWNJOB_2) then
  begin
    Result := QRParameters.FIncludeDownTime <> DOWNTIME_NO;
  end;
end; // IncludeJobQuantity

// PIM-135
function TReportTeamIncentiveQR.DecodeHrsSalMin(AValue: Integer): String;
begin
  if QRParameters.FIncludeDownTime <> DOWNTIME_YES then
    Result := ''
  else
    Result := DecodeHrsMin(AValue);
end;

function TReportTeamIncentiveQR.DecodeHrsDiffMin(AValue: Integer): String;
begin
  if QRParameters.FIncludeDownTime <> DOWNTIME_YES then
    Result := ''
  else
    Result := DecodeHrsMin(AValue);
end;

procedure TReportTeamIncentiveQR.QRLblJobSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FJobSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblDetailSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FJobSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblWorkspotSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FWKSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblDeptSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FDeptSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblBUSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FBUSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblEmpSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FEmplSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblPlantSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FPlantSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblSummarySalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsSalMin(FTotSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblJobDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FJobWorkHrs - FJobSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblDetailDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FJobWorkHrs - FJobSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblWorkspotDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FWKWorkHrs - FWKSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblDeptDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FDeptWorkHrs - FDeptSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblBUDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FBUWorkHrs - FBUSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblEmpDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FEmplWorkHrs - FEmplSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblPlantDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FPlantWorkHrs - FPlantSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportTeamIncentiveQR.QRLblSummaryDiffPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsDiffMin(FTotWorkHrs - FTotSalHrs);
  (Sender as TQRLabel).Caption := Value;
end;

end.
