(*
   MRA:4-DEC-2009. RV047.3.
   - Queries for user-selections put in component.
   - Planning gives wrong counts at bottom because no check is done on
     the Plant. Only Workspot + Department are used to see if employees
     are planned. This goes wrong when there are workspots + departments
     with the same codes as the plant that was choosen.
  MRA:4-JAN-2010. RV049.1.
  - Problem with showing standard availability solved, cause was
    a wrong place where the param was set for the User-Team-Selection.
  MRA:22-FEB-2010. RV054.7. 889962.
  - Sort on Description for popup-menu for Absence-reasons: QueryAbsReason.
  SO: O1-JUL-2010 RV067.1. 550490
    - changed the SQL statements to use = '*' instead of <> '-'
  MRA:20-JUL-2010. RV067.MRA.1. 550493.
  - Standard planning does not show correct qty.
    Cause: Some queries do not look if employee is not active anymore (ENDDATE).
    Changed: QueryEMP and QuerySTDEMP.
  MRA:31-AUG-2010 RV067.MRA.19 Bugfix.
  - It counts columns for a pivot-table. But this
    goes wrong when there are multiple schemes, like
    PIMSPCO, PIMSCLF etcetera. When there is only PIMS
    then it is no problem.
  MRA:27-SEP-2010 RV068.3. Bugfix.
  - Staff Planning gives errors when database is PIMS/PIMS.
    Reason: It used 'Pims' as scheme-name and fails when this does
    not exist.
  - Also check on wk-counter. It can never be 0 or negative!
  MRA:11-NOV-2010 RV079.3. Optimise.
  - Use 'LogChanges := False' for all Clientdatasets to improve performance.
  MRA:14-JAN-2011 RV084.6. Bugfix.
  - Staff Planning dialog
    - When extra worskpots are added, there are autom. 2 procedures
      changed in database. Because of syntax-errors in these procedures, it
      gives an error afterwards resulting in an error when you want
      to open the Staff Planning-dialog.
    - Wrong procedures:
      - STAFFPLANNING_INSRECPIVOTEMP
      - STAFFPLANNING_INSERTPIVOTLEVEL
  MRA:17-JAN-2011 RV085.2. CANCELLED.
  - When the department of a workspot is changed
    and there was planning, then this planning is not
    shown anymore, although the record still exists.
    Reason: It does not look at the real values of the
    record, but look only at the workspot-record and
    gets the workspot_code + department_code from there.
  - To solve it, it must first look at workspot-record,
    but if it did not match, then look at the real
    staff-planning-record values.
  - CANCELLED: Should not be solved for now.
  MRA:6-MAY-2011. RV092.11. Bugfix. 550518
  - Plan in other plants goes wrong:
    - When using teams-per-user it does not show
      employees who are available for another plant.
  - IMPORTANT: When 'teams-per-users' is used, but
               all-teams are selected, then DO NOT look
               for 'teams-per-users', or it will not find
               all related information.
  MRA:9-FEB-2018 PIM-354
  - Memory leak in planning dialogs
  - The OnDestroy-event was not assigned!
  MRA:25-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - IMPORTANT:
    - QueryEMP + QuerySTDEMP: Added distinct to select. Reason: They can give
      double records because they are linked to departmentperteam!
  MRA:18-FEB-2019 GLOB3-258
  - Planning and record size too big
  - Record size too big: Add BYTE to parts where workspots are added for pivot-
    tables.
*)
unit StaffPlanningEMPDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, UPimsConst, StaffPlanningUnit, Provider,
  DBClient{, IBCustomDataSet, IBQuery};

type

  TStaffPlanningEMPDM = class(TGridBaseDM)
    QueryWork: TQuery;
    DataSourcePIVOT: TDataSource;
    QueryEmpl: TQuery;
    QuerySelect: TQuery;
    QueryWorkTmp: TQuery;
    QuerySHS: TQuery;
    QuerySTA: TQuery;
    QueryEMA: TQuery;
    QueryEMP: TQuery;
    QuerySTDEMP: TQuery;
    QueryUpdateEMP: TQuery;
    QueryUpdateSTDEMP: TQuery;
    StoredProcFillEPL: TStoredProc;
    StoredProcSTDEPL: TStoredProc;
    ClientDataSetWKPerEmpl: TClientDataSet;
    DataSetProviderWKPerEmpl: TDataSetProvider;
    ClientDataSetAbsRsn: TClientDataSet;
    DataSetProviderAbsRsn: TDataSetProvider;
    QueryAbs: TQuery;
    QueryTBPerEmpl: TQuery;
    ClientDataSetTBPerEmpl: TClientDataSet;
    DataSetProviderTBPerEmpl: TDataSetProvider;
    QueryTBPerDept: TQuery;
    ClientDataSetTBPerDept: TClientDataSet;
    DataSetProviderTBPerDept: TDataSetProvider;
    ClientDataSetEMA: TClientDataSet;
    DataSetProviderEMA: TDataSetProvider;
    ClientDataSetSHS: TClientDataSet;
    DataSetProviderSHS: TDataSetProvider;
    ClientDataSetEmpl: TClientDataSet;
    DataSetProviderEmpl: TDataSetProvider;
    ClientDataSetEMPLN: TClientDataSet;
    DataSetProviderEMPLN: TDataSetProvider;
    TableDetailABSENCEREASON_ID: TIntegerField;
    TableDetailABSENCETYPE_CODE: TStringField;
    TableDetailABSENCEREASON_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailPAYED_YN: TStringField;
    TablePivot: TTable;
    QueryUpdateDB: TQuery;
    QueryCountColumn: TQuery;
    StoredProcInsertEMP: TStoredProc;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure TablePivotAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    FTBColor, FTBColorEmpl, FTBValid: TTBColor;
    FTBLevel, FTBPressed, FTBPressedEmpl : TTBLevel;
    //saved value for scroll message
    FEmpl_Filled: Integer;
    FEmplDesc_Filled: String;
    FShowLevelC: Boolean;
    FMaxTimeblocksToShow: Integer;
    // fill procedures for pivot query
    procedure FillEmployee;
    procedure GetValidTBPerShift;
    procedure FillPIVOTTable;
    function CreateTablePivot(EMPSort: Integer): Boolean;
    procedure FillEmplPlanned;
    procedure FillEmpAvail;
    procedure FillEmpLevel;
    procedure FillSHS;
    procedure InsertIntoEMPPivot;
    procedure FillEmpAvailOnShiftZero(EmplNumber: Integer; var Avail: String);
    procedure ChangeColorEmpl(EmplNumber: Integer; EmplAvailStr: String);

    procedure FillEmpl_PIVOTTable(EmplLast: Integer;
      LastDesc: String);
    procedure FillPerEmplPIVOTTable(EmplNumber: Integer; Name: String;
      DefaultBtn: String; EditTable: Boolean; var UpdateTable: Boolean);
    procedure GetPerEmplPlannedTB(EmplNumber: Integer; var TB, TBShift: TTBColor);
// update tables - on save action
    procedure UpdateEMAEmpl(EmplNumber: Integer; EmplWKStr: String);
    procedure UpdateEMPEmpl(EmplNumber: Integer; EmplWKStr: String;
      var EmplIndexPlanned: TTBColor);
    procedure UpdateEMPNotVisible(EmplNumber: Integer; EmplIndexPlanned: TTBColor);
  public
    { Public declarations }
    FResult: Boolean;
// PIVOT TABLE - create - fill
    procedure SetFieldPIVOT(EmplNumber, ColIndex: Integer;
      ValStr, ColumnStr: String; ValCol, Color: Integer;
      SetColor, Pressed: Boolean);
    procedure SaveEMP_SPL;
    procedure DataCreate;
    procedure OpenTable;
    procedure AddInList(StrEmplNumber: String; ListStr: TStringList);
    property ShowLevelC: Boolean read FShowLevelC write FShowLevelC;
    property MaxTimeblocksToShow: Integer read FMaxTimeblocksToShow write FMaxTimeblocksToShow;
  end;

var
  StaffPlanningEMPDM: TStaffPlanningEMPDM;

implementation

{$R *.DFM}
uses SystemDMT, UPimsMessageRes, ListProcsFRM, StaffPlanningFRM,
  UGlobalFunctions;

//CAR 28-7-2003- changes for performance

procedure TStaffPlanningEMPDM.FillEmployee;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  E.EMPLOYEE_NUMBER, ' + NL +
    // RV092.11. Show plant-info in description when employee is
    //           from a different plant.
    //           Do do also in stored procedure:
    //           - STAFFPLANNING_INSERTPIVOTEMP
    '  CASE ' + NL +
    '    WHEN E.PLANT_CODE = :PLANT_CODE THEN E.DESCRIPTION ' + NL +
    '    WHEN E.PLANT_CODE <> :PLANT_CODE THEN E.DESCRIPTION || '' ('' || E.PLANT_CODE || '')'' ' + NL +
    '  END DESCRIPTION, ' + NL +
    '  E.SHORT_NAME, E.TEAM_CODE, E.PLANT_CODE ' + NL +
    'FROM ' + NL +
    '  EMPLOYEE E ' + NL +
    'WHERE '+ NL;
  // FSelection: 0 = day planning
  //             1 = standard planning
  if FShowOnlyAvailEmpl and (FSelection = 0) then
  begin
//RV067.1.
    SelectStr := SelectStr +
      '  (E.EMPLOYEE_NUMBER IN ' + NL +
      '  (SELECT ' + NL +
      '    A.EMPLOYEE_NUMBER ' + NL +
      '  FROM ' + NL +
      '    EMPLOYEEAVAILABILITY A ' + NL +
      '  WHERE ' + NL +
      '    E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND ' + NL +
      '    (A.PLANT_CODE = :PLANT_CODE) AND ' + NL +
      '    A.SHIFT_NUMBER = :SHIFT AND ' + NL +
      '    A.EMPLOYEEAVAILABILITY_DATE = :DATEPLN  AND ' + NL +
      '    ((( A.AVAILABLE_TIMEBLOCK_1 = :AV1 ) AND ' + NL +
      '     ( A.AVAILABLE_TIMEBLOCK_1 IS NOT NULL )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_2 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_2 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_3 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_3 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_4 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_4 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_5 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_5 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_6 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_6 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_7 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_7 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_8 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_8 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_9 = :AV1 ) AND ' + NL +
      '      ( A.AVAILABLE_TIMEBLOCK_9 IS NOT NULL  )) OR ' + NL +
      '     (( A.AVAILABLE_TIMEBLOCK_10 = :AV1 ) AND ' + NL +
      '     ( A.AVAILABLE_TIMEBLOCK_10 IS NOT NULL  )))) ' + NL +
      '  OR E.EMPLOYEE_NUMBER IN ' + NL +
      '  ( ' + NL +
      '  SELECT ' + NL +
      '    H.EMPLOYEE_NUMBER ' + NL +
      '  FROM ' + NL +
      '    SHIFTSCHEDULE H, STANDARDAVAILABILITY S ' + NL +
      '  WHERE ' + NL +
      '    (H.PLANT_CODE = :PLANT_CODE) ' + NL +
      '    AND (S.PLANT_CODE = :PLANT_CODE) ' + NL +
      '    AND ' + NL +
      '    H.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
      '    H.SHIFT_SCHEDULE_DATE = :DATEPLN AND ' + NL +
      '    H.SHIFT_NUMBER = 0 AND S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER  '+ NL +
      '    AND S.SHIFT_NUMBER = :SHIFT AND ' + NL +
      '    S.DAY_OF_WEEK = :DAY_WEEK )) AND '+ NL;
  end;
  if FShowOnlyAvailEmpl and (FSelection = 1) then
  begin
    SelectStr := SelectStr +
      '  (E.EMPLOYEE_NUMBER IN ( ' + NL +
      '  SELECT ' + NL +
      '    EMPLOYEE_NUMBER ' + NL +
      '  FROM ' + NL +
      '    STANDARDAVAILABILITY S ' + NL +
      '  WHERE ' + NL +
      '    (S.PLANT_CODE = :PLANT_CODE) AND ' + NL +
      '    S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER  ' + NL +
      '    AND S.SHIFT_NUMBER = :SHIFT AND ' + NL +
      '    S.DAY_OF_WEEK = :DAY_WEEK AND ' + NL +
      '    ((S.AVAILABLE_TIMEBLOCK_1 = :AV3) OR ' + NL +
      '     (S.AVAILABLE_TIMEBLOCK_2 = :AV3)  OR ' + NL +
      '    (S.AVAILABLE_TIMEBLOCK_3 = :AV3) OR ' + NL +
      '     (S.AVAILABLE_TIMEBLOCK_4 = :AV3)  OR ' + NL +
      '    (S.AVAILABLE_TIMEBLOCK_5 = :AV3) OR ' + NL +
      '     (S.AVAILABLE_TIMEBLOCK_6 = :AV3)  OR ' + NL +
      '    (S.AVAILABLE_TIMEBLOCK_7 = :AV3) OR ' + NL +
      '     (S.AVAILABLE_TIMEBLOCK_8 = :AV3)  OR ' + NL +
      '    (S.AVAILABLE_TIMEBLOCK_9 = :AV3) OR ' + NL +
      '    (S.AVAILABLE_TIMEBLOCK_10 = :AV3)))) AND '+ NL;
  end;

  // Team selection
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then
    SelectStr := SelectStr +
      'E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO AND '+ NL;
  //CAR 550274 - team selection
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then // RV092.11.
    if (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
      SelectStr := SelectStr +
      '  ( ' + NL +
      '    (:USER_NAME = ''*'') OR ' + NL +
      '    (E.TEAM_CODE IN ' + NL +
      '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' +
      '         WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
      '  ) ' + NL +
      '  AND ' + NL;

  // RV092.11. Start.
{
  SelectStr :=
    SelectStr +
      ' (E.PLANT_CODE = :PLANT_CODE OR :ALL_PLANTS = 1) AND '+ NL;
}
  // RV092.11. End.

  SelectStr :=
    SelectStr +
    '(E.STARTDATE <= :FDATE) ' + ' AND((E.ENDDATE >= :FDATE) OR ' + NL +
    '(E.ENDDATE IS NULL))'+ NL;
  if FEMPSort = 0 then
    SelectStr := SelectStr + ' ORDER BY E.EMPLOYEE_NUMBER ';
  if FEMPSort = 1 then
    SelectStr := SelectStr + ' ORDER BY E.DESCRIPTION ';

  ClientDataSetEmpl.Close;
  ClientDataSetEmpl.Filtered := False;
  ClientDataSetEmpl.Filter := '';

  QueryEmpl.SQL.Clear;
  QueryEmpl.SQL.Add(UpperCase(SelectStr));
//  QueryEmpl.SQL.SaveToFile('c:\temp\planning2.sql');
  QueryEmpl.Prepare;

  if FShowOnlyAvailEmpl then
  begin
    if (FSelection = 0) then
      QueryEmpl.ParamByName('DAY_WEEK').AsInteger :=
        ListProcsF.DayWStartOnFromDate(FDate)
    else
      QueryEmpl.ParamByName('DAY_WEEK').AsInteger :=  FDay;
  end;
//CAR 28-7-2004: Active employee are related to selected date - for day planning
  if (FSelection = 0) then
    QueryEmpl.ParamByName('FDATE').AsDateTime := FDate
  else
    QueryEmpl.ParamByName('FDATE').AsDateTime := Trunc(Now);
    //
  // RV092.11 Is not used here. Begin.
//  QueryEmpl.ParamByName('ALL_PLANTS').AsInteger := EmployeesAcrossPlants;
  // RV092.11. End.
  // RV092.11. Always search on plant_code.
//  if FShowOnlyAvailEmpl then // RV092.11.
    QueryEmpl.ParamByName('PLANT_CODE').AsString := FPlant;
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then
  begin
    QueryEmpl.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QueryEmpl.ParamByName('TEAMTO').AsString := FTeamTo;
  end;
  if (FSelection = 0) and FShowOnlyAvailEmpl then
  begin
    QueryEmpl.ParamByName('DATEPLN').AsDateTime := FDate;
    QueryEmpl.ParamByName('AV1').AsString := '*';
  end;
  if FShowOnlyAvailEmpl then
    QueryEmpl.ParamByName('SHIFT').AsInteger := FSHIFT;

  if FShowOnlyAvailEmpl and (FSelection = 1) then
     QueryEmpl.ParamByName('AV3').AsString := '*';
  //CAR 550274
  if (not FAllTeam) and (FTeamFrom <> '') and (FTeamTo <> '') then // RV092.11.
    if (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
      QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  ClientDataSetEmpl.Active := True;
  // RV092.11.
  // TODO: It looks like the result-set is filter again later on
  // because it leaves out employees not belonging to the 'other' plant. 
end; // FillEmployee

procedure TStaffPlanningEMPDM.GetValidTBPerShift;
var
  Index, IndexMax: Integer;
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  COUNT(*) AS RECNO ' + NL +
    'FROM ' + NL +
    '  TIMEBLOCKPERSHIFT ' + NL +
    'WHERE ' + NL +
    '  PLANT_CODE = ''' + DoubleQuote(FPlant) + '''' + ' AND ' + NL +
    '  SHIFT_NUMBER = ' + IntToStr(FShift);
  ExecuteSelectSQL(QuerySelect, SelectStr);
  // MR:23-02-2005 Pim->Oracle
  IndexMax := Round(QuerySelect.FieldByName('RECNO').AsFloat);
  // Determine max. timeblocks to show in planning dialog.
  MaxTimeblocksToShow := IndexMax;
  if MaxTimeblocksToShow < MIN_TBS then
    MaxTimeblocksToShow := MIN_TBS;
  if MaxTimeblocksToShow > SystemDM.MaxTimeblocks then
    MaxTimeblocksToShow := SystemDM.MaxTimeblocks;
  for Index := 1 to IndexMax do
    FTBValid[index] := Index;
  for Index := (IndexMax + 1) to SystemDM.MaxTimeblocks do
    FTBValid[index] := 0;
end; // GetValidTBPerShift

procedure TStaffPlanningEMPDM.FillSHS;
var
  DaySTA : Integer;
  SelectStr: String;
begin
  if FSelection = 1 then
    Exit;
  DaySTA := ListProcsF.DayWStartOnFromDate(FDate);
  ClientDataSetSHS.Close;
  ClientDataSetSHS.Filter := '';
  QuerySHS.Close;
  QuerySHS.SQL.Clear;
  SelectStr :=
    'SELECT ' + NL +
    '  S.EMPLOYEE_NUMBER, S.SHIFT_NUMBER , ' + NL +
    '  S.AVAILABLE_TIMEBLOCK_1, S.AVAILABLE_TIMEBLOCK_2, ' + NL +
    '  S.AVAILABLE_TIMEBLOCK_3, S.AVAILABLE_TIMEBLOCK_4, ' + NL +
    '  S.AVAILABLE_TIMEBLOCK_5, S.AVAILABLE_TIMEBLOCK_6, ' + NL +
    '  S.AVAILABLE_TIMEBLOCK_7, S.AVAILABLE_TIMEBLOCK_8, ' + NL +
    '  S.AVAILABLE_TIMEBLOCK_9, S.AVAILABLE_TIMEBLOCK_10 ' + NL +
    'FROM ' + NL +
    '  SHIFTSCHEDULE H, STANDARDAVAILABILITY S ' + NL +
    'WHERE ' + NL +
    '  H.SHIFT_SCHEDULE_DATE = :DATESHS AND ' + NL +
    '  H.SHIFT_NUMBER = 0  AND ' + NL +
    '  S.EMPLOYEE_NUMBER = H.EMPLOYEE_NUMBER  AND ' + NL +
    '  S.PLANT_CODE = :PLANT_CODE AND ' + NL +
    '  S.SHIFT_NUMBER = :FSHIFT AND ' + NL +
    '  S.DAY_OF_WEEK = :S_DAY ' + NL +
    'ORDER BY ' +
    '  S.EMPLOYEE_NUMBER ';
  QuerySHS.SQL.Add(SelectStr);
  QuerySHS.ParamByName('DATESHS').AsDateTime := GetDate(FDate);
  QuerySHS.ParamByName('S_DAY').AsInteger := DaySTA;
  QuerySHS.ParamByName('FSHIFT').AsInteger := FShift;
  QuerySHS.ParamByName('PLANT_CODE').AsString := FPlant;
  ClientDataSetSHS.Open;
  try
    ClientDataSetSHS.LogChanges := False; // RV079.3.
  except
  end;
end; // FillSHS

procedure TStaffPlanningEMPDM.FillEmpAvailOnShiftZero(EmplNumber: Integer;
  var Avail: String);
var
  Index: Integer;
  StrSHS, StrEMA: String;
begin
  if FSelection = 1 then
    Exit;
  Avail := '';
  if ClientDataSetSHS.FindKey([EmplNumber]) then
  begin
    if ClientDataSetEMA.FindKey([EmplNumber, 0]) then
    begin
      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        StrSHS :=
          ClientDataSetSHS.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Index)).AsString;
        StrEMA :=
          ClientDataSetEMA.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Index)).AsString;
        if StrSHS = '*' then
          Avail := Avail +  StrEMA
        else
          Avail := Avail + ' ';
      end
    end
    else
    begin
      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        StrSHS :=
          ClientDataSetSHS.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Index)).AsString;
        if (StrSHS = '') then
          Avail := Avail + ' '
        else
          Avail := Avail + StrSHS;
      end;
    end;
  end;
end; // FillEmpAvailOnShiftZero

procedure  TStaffPlanningEMPDM.FillEmpAvail;
var
  SelectStr: String;
begin
  ClientDataSetEMA.Close;
  if FSelection = 0 then
  begin
    QueryEMA.Close;
    QueryEMA.SQL.Clear;
    // MR:03-12-2004 Only get EmployeeAvailability for selected plant,
    // so not ALL plants.
    SelectStr :=
      'SELECT ' + NL +
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER, ' + NL +
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2, ' + NL +
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4, ' + NL +
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6, ' + NL +
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8, ' + NL +
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10 ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY ' + NL +
      'WHERE ' + NL +
      '  (PLANT_CODE = :PLANT_CODE) AND ' + NL +
      '  (SHIFT_NUMBER = :SHIFT OR SHIFT_NUMBER = 0) AND ' + NL +
      '  EMPLOYEEAVAILABILITY_DATE = :DATEEMA AND ' + NL +
      '  ((AVAILABLE_TIMEBLOCK_1 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_1 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_2 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_2 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_3 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_3 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_4 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_4 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_5 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_5 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_6 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_6 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_7 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_7 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_8 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_8 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_9 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_9 <> ''-'') OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_10 IS NOT NULL) OR ' + NL +
      '  (AVAILABLE_TIMEBLOCK_10 <> ''-'') ' + NL +
      '  ) ' + NL +
      'ORDER BY ' + NL +
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER ';
    QueryEMA.SQL.Add(SelectStr);
    QueryEMA.ParamByName('DATEEMA').AsDateTime := GetDate(FDate);
    QueryEMA.ParamByName('SHIFT').AsInteger := FShift;
    QueryEMA.ParamByName('PLANT_CODE').AsString := FPlant;
    DataSetProviderEMA.DataSet := QueryEMA;
  end;
  if FSelection = 1 then
  begin
    QuerySTA.Close;
    QuerySTA.SQL.Clear;
    // MR:03-12-2004 Only get StandardAvailability for selected plant,
    // so not ALL plants.
    SelectStr :=
      'SELECT ' + NL + 
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER, ' + NL +
      '  AVAILABLE_TIMEBLOCK_1, AVAILABLE_TIMEBLOCK_2, ' + NL +
      '  AVAILABLE_TIMEBLOCK_3, AVAILABLE_TIMEBLOCK_4, ' + NL +
      '  AVAILABLE_TIMEBLOCK_5, AVAILABLE_TIMEBLOCK_6, ' + NL +
      '  AVAILABLE_TIMEBLOCK_7, AVAILABLE_TIMEBLOCK_8, ' + NL +
      '  AVAILABLE_TIMEBLOCK_9, AVAILABLE_TIMEBLOCK_10 ' + NL +
      'FROM ' + NL +
      '  STANDARDAVAILABILITY ' + NL +
      'WHERE ' + NL +
      '  (PLANT_CODE = :PLANT_CODE) AND ' + NL +
      '  SHIFT_NUMBER = :SHIFT AND ' + NL +
      '  DAY_OF_WEEK = :DAY_WEEK AND ' + NL +
      '  (( AVAILABLE_TIMEBLOCK_1 = ''*'' ) OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_2 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_3 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_4 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_5 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_6 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_7 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_8 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_9 = ''*'') OR ' + NL +
      '    (AVAILABLE_TIMEBLOCK_10 =''*'')) ' + NL +
      'ORDER BY ' + NL +
      '  EMPLOYEE_NUMBER, SHIFT_NUMBER ';
    QuerySTA.SQL.Add(SelectStr);
    QuerySTA.ParamByName('DAY_WEEK').AsInteger := FDay;
    QuerySTA.ParamByName('SHIFT').AsInteger := FShift;
    QuerySTA.ParamByName('PLANT_CODE').AsString := FPlant;
    DataSetProviderEMA.DataSet := QuerySTA;
  end;
  ClientDataSetEMA.Filter := '';
  ClientDataSetEMA.Open;
  try
    ClientDataSetEMA.LogChanges := False; // RV079.3.
  except
  end;
end; // FillEmpAvail

// fill for all shifts
procedure TStaffPlanningEMPDM.FillEmplPlanned;
begin
  ClientDataSetEMPLN.Close;
  if FSelection = 0 then
  begin
    QueryEMP.ParamByName('DATEEMA').AsDateTime := GetDate(FDate);
    QueryEMP.ParamByName('ALL_PLANTS').AsInteger := EmployeesAcrossPlants;
    QueryEMP.ParamByName('PLANT_CODE').AsString := FPlant;
    if (not FAllTeam) and (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
      QueryEMP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      QueryEMP.ParamByName('USER_NAME').AsString := '*';
    QueryEMP.ParamByName('FDATE').AsDateTime := FDate;
    if (FAllTeam) then
      QueryEMP.ParamByName('ALL_TEAMS').AsInteger := 1
    else
      QueryEMP.ParamByName('ALL_TEAMS').AsInteger := 0;
    QueryEMP.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QueryEMP.ParamByName('TEAMTO').AsString := FTeamTo;
    DataSetProviderEMPLN.DataSet := QueryEMP;
  end
  else
  begin
    QuerySTDEMP.ParamByName('DAY_WEEK').AsInteger := FDay;
    QuerySTDEMP.ParamByName('ALL_PLANTS').AsInteger := EmployeesAcrossPlants;
    QuerySTDEMP.ParamByName('PLANT_CODE').AsString := FPlant;
    if (not FAllTeam) and (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
      QuerySTDEMP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      QuerySTDEMP.ParamByName('USER_NAME').AsString := '*';
    QuerySTDEMP.ParamByName('FDATE').AsDateTime := Trunc(Now);
    if (FAllTeam) then
      QuerySTDEMP.ParamByName('ALL_TEAMS').AsInteger := 1
    else
      QuerySTDEMP.ParamByName('ALL_TEAMS').AsInteger := 0;
    QuerySTDEMP.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QuerySTDEMP.ParamByName('TEAMTO').AsString := FTeamTo;
    DataSetProviderEMPLN.DataSet := QuerySTDEMP;
  end;
  ClientDataSetEMPLN.Filter := '';
  ClientDataSetEMPLN.Open;
  try
    ClientDataSetEMPLN.LogChanges := False; // RV079.3.
  except
  end;
end; // FillEmplPlanned

function TStaffPlanningEMPDM.CreateTablePivot(EMPSort: Integer): Boolean;
var
  SqlStr: String;
  CountCol, i: Integer;
  MyDatabaseUserName: String;
begin
  FWKCount :=  FWKList.Count - 1;
  if (FWKCount < 0 ) then
  begin
    DisplayMessage(SEmptyWK, mtWarning, [mbOK]);
    Result := False;
    Exit;
  end;
  //get column number of pivot table
  QueryCountColumn.Close;
  QueryCountColumn.SQL.Clear;
  // Pims -> Oracle
  // RV067.MRA.19 Bugfix.
  // Add scheme-name, in case there are multiple schemes.
  //
  // RV068.3. Bugfix. Use UpperCase !
  MyDatabaseUserName := UpperCase(SystemDM.Pims.Params.Values['USER NAME']);
  QueryCountColumn.SQL.Add(
    'SELECT ' + NL +
    '  COUNT(*) AS COUNTCOL ' + NL +
    'FROM  ' + NL +
    '  SYS.ALL_TAB_COLUMNS T' + NL +
    'WHERE ' + NL +
    '  T.TABLE_NAME = ''PIVOTEMPSTAFFPLANNING''' +
    '  AND OWNER = ' + '''' + MyDatabaseUserName + '''');

  QueryCountColumn.Open;
  CountCol := Round(QueryCountColumn.FieldByName('COUNTCOL').AsFloat);
  CountCol := CountCol - 4;
  // RV068.3. Bugfix. This must never <= 0 !
  if CountCol <= 0 then
    CountCol := 1;
  // MR:10-03-2005 FWKCount is one less than total. So we add 1 here.
  if (CountCol < (FWKCount + 1)) then
  begin
    if FWKCount > 400 then
    begin
      DisplayMessage(SSelectedWorkspots, mtWarning, [mbOK]);
      FWKCount := 400;
    end;

    // start updating database with more columns  - planning table
    QueryUpdateDB.Close;
    QueryUpdateDB.SQL.Clear;
    QueryUpdateDB.ParamCheck := False;
    // GLOB3-60 -> changed size for WK-fields from 8 -> 20
    SqlStr := 'ALTER TABLE PIVOTEMPSTAFFPLANNING';
    for i := CountCol to  FWKCount do
    begin
      if i = CountCol then
        SqlStr := SqlStr +
          ' ADD  (WK' + IntToStr(i) + ' CHAR(20 BYTE),'
      else
        SqlStr := SqlStr +
          ' WK' + IntToStr(i) + ' CHAR(20 BYTE),';
    end;
    QueryUpdateDB.SQL.Add(Copy(SqlStr, 1, Length(SqlStr) - 1) + ')');
// QueryUpdateDB.SQL.SaveToFile('c:\temp\updateDB.sql');
    QueryUpdateDB.ExecSQL;
 // start update insert procedure
    QueryUpdateDB.Close;
    QueryUpdateDB.SQL.Clear;
    SqlStr := 'CREATE OR REPLACE PROCEDURE STAFFPLANNING_INSRECPIVOTEMP ' + NL +
              '( PIVOTCOMPUTERNAME VARCHAR2, ' + NL +
              '  EMPLOYEE_NUMBER INTEGER, ' + NL  +
              '  DESCRIPTION VARCHAR2, ' + NL  +
              ' STR VARCHAR2 )' +NL +
              ' IS ' + NL +
              ' BEGIN  '+ NL +
              '  INSERT INTO PIVOTEMPSTAFFPLANNING (PIVOTCOMPUTERNAME , ' + NL +
              '  EMPLOYEE_NUMBER, DESCRIPTION ' + NL;
    for i:= 0 to FWKCount do
    begin
      SqlStr := SqlStr + ', WK' + IntToStr(i);
      if (i mod 15 = 0) then
        SqlStr := SqlStr + NL;
    end;
    SqlStr := SqlStr + ', WKEMPL)' + NL;//  +
    SqlStr := SqlStr +
      ' VALUES (PIVOTCOMPUTERNAME , EMPLOYEE_NUMBER, DESCRIPTION, ' + NL;
    for i:= 0 to FWKCount  do
    begin
      SqlStr := SqlStr + ' STR, ';
      if (i mod 15 = 0) then
        SqlStr := SqlStr + NL;
    end;
    SqlStr := SqlStr + ' STR); ' + NL;
    SqlStr := SqlStr + ' END STAFFPLANNING_INSRECPIVOTEMP;';
    QueryUpdateDB.SQL.Add(SqlStr);
    QueryUpdateDB.ExecSQL;
    // next table for occupation update - more columns
    QueryUpdateDB.Close;
    QueryUpdateDB.SQL.Clear;
    QueryUpdateDB.ParamCheck := False;
    // GLOB3-60 -> changed size for WK from 24 to 60
    SqlStr := 'ALTER TABLE PIVOTOCISTAFFPLANNING';
    for i := CountCol to FWKCount  do
    begin
      if i = CountCol then
        SqlStr := SqlStr +
          ' ADD  (WK' + IntToStr(i) + ' CHAR(60 BYTE),'
      else
        SqlStr := SqlStr +
          ' WK' + IntToStr(i) + ' CHAR(60 BYTE),';
    end;
    QueryUpdateDB.SQL.Add(Copy(SqlStr, 1, Length(SqlStr) - 1) + ')');
    QueryUpdateDB.ExecSQL;
// start update insert procedure
    QueryUpdateDB.Close;
    QueryUpdateDB.SQL.Clear;
    SqlStr :=
      'CREATE OR REPLACE PROCEDURE STAFFPLANNING_INSERTPIVOTLEVEL ' + NL +
      '( PIVOTCOMPUTERNAME VARCHAR2, ' + NL +
      '  LEVEL_OCI VARCHAR2, ' + NL  +
      '  VAL VARCHAR2) ' + NL  +
      ' IS ' + NL +
      ' BEGIN  '+ NL +
      ' INSERT INTO PIVOTOCISTAFFPLANNING (PIVOTCOMPUTERNAME ,OCI_LEVEL ' +  NL;
    for i:= 0 to FWKCount  do
    begin
      SqlStr := SqlStr + ', WK' + IntToStr(i);
      if (i mod 15 = 0) then
        SqlStr := SqlStr + NL;
    end;
    SqlStr := SqlStr + ' )' + NL  +
      '  VALUES (PIVOTCOMPUTERNAME , LEVEL_OCI ' + NL;
    for i:= 0 to FWKCount do
    begin
      SqlStr := SqlStr + ', VAL ';
      if (i mod 15 = 0) then
        SqlStr := SqlStr + NL;
     end;
    SqlStr := SqlStr + ' ); ' + NL;
    SqlStr := SqlStr + ' END STAFFPLANNING_INSERTPIVOTLEVEL;';
    QueryUpdateDB.SQL.Add(SqlStr);
    QueryUpdateDB.ExecSQL;
  end;
  Result := True;

  OpenTable;
end; // CreateTablePivot

//tables used for save
procedure TStaffPlanningEMPDM.OpenTable;
begin
  FillEmplPlanned;
end;

procedure TStaffPlanningEMPDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FListABSRSN.Free;
  FListOCIUpdate.Free;
 //CAR 28-7-2003- changes for performance
  QuerySHS.Close;
  QuerySHS.UnPrepare;
  ClientDataSetSHS.Close;

  QueryEMA.Close;
  QueryEMA.UnPrepare;
  QuerySTA.Close;
  QuerySTA.UnPrepare;
  ClientDataSetEMA.Close;

  QueryEmpl.Close;
  QueryEmpl.UnPrepare;
  ClientDataSetEmpl.Close;

  QueryEMP.Close;
  QueryEMP.UnPrepare;
  QuerySTDEMP.Close;
  QuerySTDEMP.UnPrepare;
  ClientDataSetEMPLN.Close;

  ClientDataSetAbsRsn.Close;
  ClientDataSetTBPerEmpl.Close;
  ClientDataSetTBPerDept.Close;
  ClientDataSetWKPerEmpl.Close;
// used for update planning tables
  QueryUpdateEMP.Close;
  QueryUpdateEMP.UnPrepare;
  QueryUpdateSTDEMP.Close;
  QueryUpdateSTDEMP.UnPrepare;

  TablePivot.Close;
end; // DataModuleDestroy

procedure  TStaffPlanningEMPDM.InsertIntoEMPPivot;
begin
  if FAllTeam then
  begin
    StoredProcInsertEMP.ParamByName('TEAMFROM').asString := '*';
    StoredProcInsertEMP.ParamByName('TEAMTO').asString := '*'
  end
  else
  begin
    StoredProcInsertEMP.ParamByName('TEAMFROM').asString := FTeamFrom;
    StoredProcInsertEMP.ParamByName('TEAMTO').asString := FTeamTo;
  end;

  if EmployeesAcrossPlants = 1 then
    StoredProcInsertEMP.ParamByName('EMP_PLANT_CODE').asString := '*'
  else
    StoredProcInsertEMP.ParamByName('EMP_PLANT_CODE').asString := FPlant;
  StoredProcInsertEMP.ParamByName('PLANT_CODE').asString := FPlant;
  if FSelection = 0 then
    StoredProcInsertEMP.ParamByName('CURRENTDATE').asDateTime := FDate
  else
    StoredProcInsertEMP.ParamByName('CURRENTDATE').asDateTime := NOW();

  StoredProcInsertEMP.ParamByName('PIVOTCOMPUTERNAME').AsString :=
  	SystemDM.CurrentCOMPUTERName;
  StoredProcInsertEMP.ParamByName('SHIFT').asInteger := FSHIFT;
  if FShowOnlyAvailEmpl then
    StoredProcInsertEMP.ParamByName('ONLYEMPAVAIL').asInteger := 1
  else
    StoredProcInsertEMP.ParamByName('ONLYEMPAVAIL').asInteger := 0;
  StoredProcInsertEMP.ParamByName('TYPEPLANNING').asInteger := FSelection;
  StoredProcInsertEMP.ParamByName('DATESELECTION').asDateTime := FDate;
  if FSelection = 0 then
    FDay := ListProcsF.DayWStartOnFromDate(FDate);
  StoredProcInsertEMP.ParamByName('DAY_WEEK').asInteger := FDay;
 //CAR 550274
 // R092.11. (not FAllteam) added.
  if (not FAllTeam) and (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
    StoredProcInsertEMP.ParamByName('USER_NAME').AsString :=
      SystemDM.UserTeamLoginUser
  else // RV092.11 Here always fill this argument!
    StoredProcInsertEMP.ParamByName('USER_NAME').AsString := '*';
  StoredProcInsertEMP.Prepare;
  StoredProcInsertEMP.ExecProc;
end; // InsertIntoEMPPivot

procedure TStaffPlanningEMPDM.DataCreate;
begin
  FResult := CreateTablePIVOT(FEMPSort);

  if Not FResult then
    Exit;

  FillEmployee;

  if not ClientDataSetEmpl.IsEmpty  then
  begin
//insert lines filed with default values
    InsertIntoEMPPivot;
// update lines if necessary
    FillPIVOTTable;
  end
  else
  begin
    DisplayMessage(SNoEmployee, mtWarning, [mbOK]);
    FResult := False;
  end;
end; // DataCreate

procedure TStaffPlanningEMPDM.FillEmpLevel;
var
  SelectStr:String;
begin
  //CAR 550274- Team Selection
  ClientDataSetWKPerEmpl.Filter := '';
  // MR:6-12-2004 Take DEPARTMENT_CODE from WORKSPOTPEREMPLOYEE!
  // Otherwise if 'not FAllTeam' the DEPARTMENT_CODE from the
  // Employee will be taken instead of the Workspot that's stored
  // in WORKSPOTPEREMPLOYEE.
  SelectStr :=
    'SELECT ' + NL +
    '  W.EMPLOYEE_NUMBER, W.PLANT_CODE, W.DEPARTMENT_CODE, '+ NL +
    '  W.WORKSPOT_CODE, W.EMPLOYEE_LEVEL ' + NL;
  if not FAllTeam then
  begin
    SelectStr := SelectStr +
      ' FROM ' + NL +
      '   WORKSPOTSPEREMPLOYEE W INNER JOIN EMPLOYEE E ON ' + NL +
      '     W.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
  end
  else
  begin
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      SelectStr := SelectStr +
        ' FROM ' + NL +
        '   WORKSPOTSPEREMPLOYEE W INNER JOIN EMPLOYEE E ON ' + NL +
        '     W.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL
    else
      SelectStr := SelectStr +
        ' FROM ' + NL +
        '   WORKSPOTSPEREMPLOYEE W ' + NL;
  end;
  SelectStr := SelectStr +
    ' WHERE ' + NL +
    '   (W.PLANT_CODE = :PLANT_CODE OR :ALL_PLANTS = 1) ' + NL;

  if not FAllTeam then
    SelectSTR := SelectSTR +
      ' AND E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO ' + NL;

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    SelectStr := SelectStr +
      '  AND ' + NL +
      '  ( ' + NL +
      '    (:USER_NAME = ''*'') OR ' + NL +
      '    (E.TEAM_CODE IN ' + NL +
      '        (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' +
      '         WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
      '  ) ' + NL;
  end;

  SelectStr := SelectStr +
    ' ORDER BY ' + NL +
    '   W.EMPLOYEE_NUMBER, W.PLANT_CODE, W.DEPARTMENT_CODE, W.WORKSPOT_CODE';

  SetSelectSQL(QuerySelect, SelectStr, False);
// QuerySelect.SQL.SaveToFile('c:\temp\emp_level.sql');
  QuerySelect.ParamByName('ALL_PLANTS').AsInteger := EmployeesAcrossPlants;
  QuerySelect.ParamByName('PLANT_CODE').AsString := FPlant;
  if (not FAllTeam) then
  begin
    QuerySelect.ParamByName('TEAMFROM').AsString := FTeamFrom;
    QuerySelect.ParamByName('TEAMTO').AsString := FTeamTo;
  end;
  // RV092.11.
  if (not FAllTeam) and (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
    QuerySelect.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else // RV092.11. Do not always fill this argument!
    if (SystemDM.UserTeamSelectionYN = CHECKEDVALUE) then
      QuerySelect.ParamByName('USER_NAME').AsString := '*';
  ClientDataSetWKPerEmpl.Close;
  DataSetProviderWKPerEmpl.DataSet := QuerySelect;
  ClientDataSetWKPerEmpl.Open;
  try
    ClientDataSetWKPerEmpl.LogChanges := False; // RV079.3.
  except
  end;
end; // FillEmpLevel

procedure TStaffPlanningEMPDM.GetPerEmplPlannedTB(EmplNumber: Integer;
  var TB, TBShift: TTBColor);
var
  StrTmp: String;
  Shift, Index: Integer;
begin
  for Index := 1 to SystemDM.MaxTimeblocks do
  begin
    TB[Index] := 0;
    TBShift[Index] := 0;
  end;
  ClientDataSetEMPLN.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber);
  ClientDataSetEMPLN.First;
  while not ClientDataSetEMPLN.Eof do
  begin
    Shift := ClientDataSetEMPLN.FieldByName('SHIFT_NUMBER').AsInteger;
    for Index := 1 to SystemDM.MaxTimeblocks do
    begin
      StrTmp :=
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_' +
          IntToStr(Index)).AsString;
      if (StrTmp = 'A') OR (StrTmp = 'B') OR (StrTmp = 'C') then
      begin
        TB[Index] := TB[Index] + 1;
        if Shift = FShift then
          TBShift[Index] := TBShift[Index] + 1;
      end;
    end;
    ClientDataSetEMPLN.Next;
  end;
  ClientDataSetEMPLN.Filter := '';
end; // GetPerEmplPlannedTB

procedure TStaffPlanningEMPDM.ChangeColorEmpl(EmplNumber: Integer;
  EmplAvailStr: String);
var
  i, IndexList, IndexList1: Integer;
  AbsReason: String;
begin
  try
    if EmplAvailStr = '' then
    begin
      for i := 1 to SystemDM.MaxTimeblocks do
        FTBColorEmpl[i] := Invisible;
      Exit;
    end;
    for i:= 1 to SystemDM.MaxTimeblocks do
      if (FTBColorEmpl[i] <> Invisible) then
      begin
        AbsReason := EmplAvailStr[i];
        if (AbsReason = ' ') or (AbsReason = '-') then
           FTBCOLOREmpl[i] := Invisible
        else
          if FSelection = 0 then
             if (AbsReason <>  '*')  then
             begin
               IndexList := FABSList.IndexOf(AbsReason);
               if (IndexList >= 0) then
                 if (FABSList.Strings[IndexList] <> AbsReason) then
                    IndexList :=  FABSList.IndexOf(AbsReason + '0');
                 if (IndexList >= 0) then
                 begin
                    FTBCOLOREmpl[i] := DarkGrey;
                    FTBPressedEmpl[I] := 'Y';
                    IndexList1 :=
                      FListABSRSN.IndexOf(IntToStr(EmplNumber) + CHR_SEP + IntToStr(i));
                    if IndexList1 < 0 then
                    begin
                      FListABSRSN.Add(IntToStr(EmplNumber) + CHR_SEP + IntToStr(i));
                      FListABSRSN.Add(AbsReason);
                    end;
                  end  // if IndexList
                  else
                    FTBCOLOREmpl[i] := Invisible;
             end; // absreason
      end;   // if color invisible
  except
    on E:Exception do
      WLog(E.Message);
  end;
end; // ChangeColorEmpl

procedure TStaffPlanningEMPDM.FillEmpl_PIVOTTable(EmplLast: Integer;
  LastDesc: String);
var
  EmplNumber: Integer;
  Name, DefaultBtn: String;
  UpdateTable: Boolean;
  RefreshTable: Boolean;
begin
  if FEMPSort = 0 then
    if EmplLast <= FEmpl_Filled then
      Exit;
  if FEMPSort = 1 then
    if LastDesc <= FEmplDesc_Filled then
      Exit;

  Screen.Cursor := crHourGlass;
  UpdateTable := False;
  RefreshTable := False;
  if FShowOnlyAvailEmpl then
    DefaultBtn := '7'
  else
    DefaultBtn := '20';

  if FEMPSort = 0 then
  begin
    ClientDataSetWKPerEmpl.Filtered := True;
    ClientDataSetWKPerEmpl.Filter := 'EMPLOYEE_NUMBER > ' +
      IntToStr(FEmpl_Filled) + ' AND EMPLOYEE_NUMBER <= ' +  IntToStr(EmplLast);

    ClientDataSetEmpl.Filtered := True;
    ClientDataSetEmpl.Filter  := 'EMPLOYEE_NUMBER > ' + IntToStr(FEmpl_Filled) +
      ' AND EMPLOYEE_NUMBER <= ' +  IntToStr(EmplLast);
  end
  else
  begin
    ClientDataSetEmpl.Filtered := True;
    ClientDataSetEmpl.Filter  := 'DESCRIPTION > ''' +
      DoubleQuote(FEmplDesc_Filled) + ''' AND DESCRIPTION <= ''' +
      DoubleQuote(LastDesc) + '''';
  end;
  ClientDataSetEmpl.First;
  if TablePivot.FieldByName('EMPLOYEE_NUMBER').AsInteger =
    ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger then
    FillPerEmplPIVOTTable(TablePivot.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString, DefaultBtn, True,
      UpdateTable )
  else
  begin
    while not ClientDataSetEmpl.Eof do
    begin
      EmplNumber := ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Name := ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString;
      FillPerEmplPIVOTTable(EmplNumber, Name, DefaultBtn, False, UpdateTable);
      if UpdateTable then
        RefreshTable := True;
      ClientDataSetEmpl.Next;
    end;
   if RefreshTable then
     TablePivot.Refresh;
  end;
  FEmpl_Filled := EmplLast;
  FEmplDesc_Filled := LastDesc;

  Screen.Cursor := crDefault;
end; // FillEmpl_PIVOTTable

procedure TStaffPlanningEMPDM.FillPerEmplPIVOTTable(EmplNumber: Integer; Name: String;
  DefaultBtn: String; EditTable: Boolean;
  var UpdateTable: Boolean);
var
  i, RecCount, MaxLengthTBEmpl: Integer;
  WKCode, WKName, StrBtn, WK, Dept, LevelStr, StrWK: String;
  DefaultWK: String;
  TBValidStr, TBValidEmplStr, EmplAvailStr, EmplPlannedStr,
  UpdateStr: String;
  UpdatePivot, InvisibleWK, FirstTime: Boolean;
  TB, TBShift: TTBColor;
  function CheckInvisibleEmpl: Boolean;
  var
    i: Integer;
  begin
    Result := True;
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      if FTBColorEmpl[i] <> Invisible then
        Result := False;
    end;
  end;
  procedure DeleteInvisibleLine;
  begin
    if FShowOnlyAvailEmpl then
    begin
      UpdateStr := 'DELETE FROM ' + TMPSTAFFPLANNINGEMP;
      UpdateStr := UpdateStr + ' WHERE PIVOTCOMPUTERNAME = ' +
        '''' + SystemDM.CurrentComputerName + '''' +
        ' AND ' + 'EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber);
      ExecuteSql(QueryWork, UpdateStr);
    end;
  end;
{$IFDEF DEBUG}
  procedure Show_DEBUG_Info;
  var
    I: Integer;
  begin
    WDebugLog('ClientDataSetEMPLN:');
    WDebugLog('EMPLOYEE_NUMBER;PLANT_CODE;WORKSPOT_CODE;DEPARTMENT_CODE;' +
      'SHIFT_NUMBER;SCHEDULED_TIMEBLOCK_1;SCHEDULED_TIMEBLOCK_2;' +
      'SCHEDULED_TIMEBLOCK_3;SCHEDULED_TIMEBLOCK_4');
    ClientDataSetEMPLN.First;
    while not ClientDataSetEMPLN.Eof do
    begin
      WDebugLog(
        ClientDataSetEMPLN.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('PLANT_CODE').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('WORKSPOT_CODE').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('DEPARTMENT_CODE').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SHIFT_NUMBER').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_1').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_2').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_3').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_4').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_5').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_6').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_7').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_8').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_9').AsString + ';' +
        ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_10').AsString
        );
      ClientDataSetEMPLN.Next;
    end;
    ClientDataSetEMPLN.First;
    WDebugLog('FWKList:');
    for I := 0 to FWKCount do
      WDebugLog(FWKList.Strings[I]);
  end;
{$ENDIF}
  function TBShiftCheck: Integer;
  var
    Index: Integer;
  begin
    { ((TBShift[1] + TBShift[2] + TBShift[3] + TBShift[4]) <> 0) then }
    Result := 0;
    for Index := 1 to SystemDM.MaxTimeblocks do
      Result := Result + TBShift[Index];
  end; // TBShiftCheck
begin
{$IFDEF DEBUG}
  Show_DEBUG_Info;
{$ENDIF}

  DefaultWK := '';
  for i:= 1 to SystemDM.MaxTimeblocks do
  begin
    DefaultWK  :=  DefaultWK + FillLeadSpace(DefaultBtn, 2);
  end;

  UpdateTable := False;
  UpdatePivot := False;
  UpdateStr := ' UPDATE ' + TMPSTAFFPLANNINGEMP + ' SET ';
  FirstTime := True;
// initialize per empl
  for i:= 1 to SystemDM.MaxTimeblocks do
  begin
    FTBPressedEmpl[i] := 'N';
    FTBCOLOREmpl[i] := Normal;
  end;
  TBValidStr := '';

  for i := 1 to SystemDM.MaxTimeblocks do
  begin
    if ClientDataSetTBPerEmpl.FindKey([EmplNumber, FPlant, FShift, i]) then
      TBValidEmplStr := TBValidEmplStr + IntToStr(i)
    else
      Break;
  end;

// if exists tb defined per employee set color of employee invisible
  if TBValidStr <> '' then
    for i := Length(TBValidStr) + 1 to SystemDM.MaxTimeblocks do
      FTBColorEmpl[i] := Invisible;

// init with empl avail - leave normal color or
// changed into dark grey or invisible
  EmplAvailStr := '';
  if FSelection =  0 then
    ClientDataSetEMA.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber);
  if ClientDataSetEMA.FindKey([EmplNumber, FShift]) then
  begin
    for i := 1 to SystemDM.MaxTimeblocks do
       EmplAvailStr :=  EmplAvailStr +
         ClientDataSetEMA.FieldByName('AVAILABLE_TIMEBLOCK_' +
           IntToStr(i)).AsString;
  end
  else
    FillEmpAvailOnShiftZero(EmplNumber, EmplAvailStr);

  ChangeColorEmpl(EmplNumber, EmplAvailStr);
// end availabilities

  GetPerEmplPlannedTB(EmplNumber, TB, TBShift);
  for i := 1 to SystemDM.MaxTimeblocks do
  begin
    if TBShift[I] > 0 then
    begin
      FTBPressedEmpl[i] := 'Y';
      if FTBColorEmpl[i] = Invisible then
        FTBColorEmpl[i] := Red
      else
        FTBColorEmpl[i] := Green;
    end;
  end;

  if CheckInvisibleEmpl then
  begin
    DeleteInvisibleLine;
    Exit;
  end;

  ClientDataSetEMPLN.Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber) +
    ' AND SHIFT_NUMBER = ' + IntToStr(FShift);
  ClientDataSetTBPerDept.Filter := 'PLANT_CODE = ''' +
    DoubleQuote(FPlant) + ''' AND ' +
    ' SHIFT_NUMBER = ' + IntToStr(FShift);

//  set max tb valid per one employee
  MaxLengthTBEmpl := 0;
  if EditTable then
    if not (TablePivot.State in [dsEdit, dsInsert]) then
      TablePivot.Edit;
  for RecCount := 0 to FWKCount do
  begin
// initialize per wk/dept - based on empl color
    TBValidStr := TBValidEmplStr;
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      FTBPressed[i] := FTBPressedEmpl[i];
      if (FTBColorEmpl[i] = Invisible) or (FTBColorEmpl[i] = DarkGrey) then
        FTBColor[i] := FTBColorEmpl[i]
      else
        FTBCOLOR[i] := Normal;
      FTBLevel[i] := '';
    end;

    WKCode := FWKList.Strings[RecCount];
    GetWKDeptCode(WKCode, RecCount, WK,  DEPT);

// set invalid tblocks for each wk/dept and for empl
// if there is no tb defined per dept then get from shift
    if (TBValidStr = '') then
      if ClientDataSetTBPerDept.FindKey([FPlant, FShift, Dept]) then
      begin
        for i := 1 to ClientDataSetTBPerDept.FieldByName('RECNO').AsInteger do
          TBValidStr :=  TBValidStr + IntToStr(i);
      end;

    if TBValidStr = '' then
      for i:= 1 to SystemDM.MaxTimeblocks do
        if FTBValid[i] > 0 then
          TBValidStr := TBValidStr + IntToStr(FTBValid[i]);

    MaxLengthTBEmpl := Max(MaxLengthTBEmpl, Length(TBValidStr));
    for i:= (length(TBValidStr) + 1) to SystemDM.MaxTimeblocks do
    begin
      FTBColor[i] := Invisible;
      FTBPressed[i] := 'N';
    end;
//end
    InvisibleWK := True;
    for i := 1 to SystemDM.MaxTimeblocks do
      if FTBColor[i] <> Invisible then
        InvisibleWK :=  False;

// empl PLANNING - color
    if (not InvisibleWK) and
      (TBShiftCheck <> 0) then
      begin
        // RV085.2. CANCELLED.
        if ClientDataSetEMPLN.FindKey([EmplNumber, WK, Dept, FShift]) then
          for i := 1 to SystemDM.MaxTimeblocks do
          begin
            EmplPlannedStr :=
              ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_' +
                IntToStr(i)).AsString;
            if (EmplPlannedStr = 'A') OR (EmplPlannedStr = 'B') OR
              (EmplPlannedStr= 'C') then
            begin

              if FTBColorEmpl[i] <> Invisible  then
              begin
                if Pos(IntToStr(i), TBValidEmplStr) > 0 then
                  FTBColor[i] := LightGreen
                else
                  FTBColor[i] := Green;

                if FTBColorEmpl[i] = Red then
                  FTBColor[i] := Red;
                FTBPressed[i] := 'Y';
              end;
            end;
          end;
       end;

// level on wk/dept
    if Not InvisibleWK then
    begin
      LevelStr := '';
      if ClientDataSetWKPerEmpl.FindKey([EmplNumber, FPlant, DEPT, WK]) then
        LevelStr :=
          ClientDataSetWKPerEmpl.FieldByName('EMPLOYEE_LEVEL').AsString;
      // MR:10-01-2005 Order 550361
      if (UpperCase(LevelStr) = 'C') then
        if not ShowLevelC then
          LevelStr := '';
    end;

// fill in table pivot
 //  if Not InvisibleWK then
    WKName := 'WK' + IntToStr(RecCount); //+ IntToStr(i);
    StrWK := '';
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      if FTBColor[i] <> Invisible then
        FTBLevel[i] := LevelStr;
      StrBtn := GetTableBTN(FTBColor[i], FTBPressed[i], FTBLevel[i]);
      StrWK := StrWK + FillLeadSpace(StrBtn, 2);
    end;//for

    if StrWK <> DefaultWK then
    begin
        if FirstTime then
          FirstTime := False
        else
          UpdateStr := UpdateStr + ', ';
        UpdateStr := UpdateStr + WKName + ' = ''' + StrWK + '''';
        UpdatePivot := True;
        if EditTable then
          TablePivot.FieldByName(WKName).AsString := StrWK;
     end;

  end;
  {while wk/dept}

  if EditTable and UpdatePivot then
    TablePivot.Post;
// SET MAXIMAL INVIZIBLE BLOCKS PER EMPLOYEE
  for i:= (MaxLengthTBEmpl + 1) to SystemDM.MaxTimeblocks do
  begin
    FTBColorEmpl[i] := Invisible;
    FTBPressedEmpl[i] := 'N';
  end;

  for i := 1 to SystemDM.MaxTimeblocks do
    if (TB[I] > 0) then
    begin
      FTBPressedEmpl[i] := 'Y';
      if (FTBColorEmpl[i] <> Invisible) and (FTBColorEmpl[i] <> Red) then
      begin
        If (TB[I] > 1) then
          FTBColorEmpl[i] := Yellow
        else
        begin
          if Pos(IntToStr(i), TBValidEmplStr) > 0 then
            FTBColorEmpl[i] := LightGreen
          else
            FTBColorEmpl[i] := Green;
        end;
      end;
    end;
  if EditTable then
    TablePivot.Edit;

  StrWK := '';
  for i:= 1 to SystemDM.MaxTimeblocks do
  begin
    StrBtn := GetTableBTN(FTBColorEmpl[i], FTBPressedEmpl[i], '');
    StrWK := StrWK + FillLeadSpace(StrBtn, 2);
  end;

  if StrWK <> DefaultWK then
  begin
    if FirstTime then
      FirstTime := False
    else
      UpdateStr := UpdateStr + ', ';
    UpdateStr := UpdateStr +  ' WKEMPL' +
      ' = ''' + StrWK + '''';
    UpdatePivot := True;
    if EditTable then
      TablePivot.FieldByName('WKEMPL').AsString :=
        StrWK;
  end;
  if UpdatePivot and EditTable then
    TablePivot.Post;
  if not FirstTime then
  begin
    if Not EditTable then
    begin
      UpdateStr := UpdateStr + ' WHERE PIVOTCOMPUTERNAME = ' +
        '''' + SystemDM.CurrentComputerName + '''' +
        ' AND ' + 'EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber);
      ExecuteSql(QueryWork, UpdateStr);
    end;
    UpdateTable := True;
  end;
end; // FillPerEmplPIVOTTable

procedure TStaffPlanningEMPDM.FillPIVOTTable;
var
  k, EmplNumber: Integer;
  Name, DefaultBtn: String;
  UpdateTable: Boolean;
begin
  GetValidTBPerShift;
  if TablePivot.TableName = '' then
    TablePivot.TableName := 'PIVOTEMPSTAFFPLANNING';

  ClientDataSetEmpl.Filter := '';
  ClientDataSetEmpl.First;
  EmplNumber := 1;
  FillEmpLevel;
  FillEmpAvail;
  FillSHS;

  TablePivot.CachedUpdates := False;
  if FEmpSort = 0 then
  begin
    TablePivot.IndexFieldNames := 'PIVOTCOMPUTERNAME;EMPLOYEE_NUMBER'
  end
  else
  begin
    TablePivot.IndexFieldNames := 'PIVOTCOMPUTERNAME;DESCRIPTION';
  end;
 TablePivot.CachedUpdates := True;
  if FShowOnlyAvailEmpl then
    DefaultBtn := '7'
  else
    DefaultBtn := '20';
  FListABSRsn.Clear;
  k := 1;
  while (not ClientDataSetEmpl.Eof) and (K <= EmployeeSHOW) do
  begin
    EmplNumber := ClientDataSetEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    Name := ClientDataSetEmpl.FieldByName('DESCRIPTION').AsString;
    FillPerEmplPIVOTTable(EmplNumber, Name, DefaultBtn, False, UpdateTable);
    ClientDataSetEmpl.Next;
    Inc(K);
  end;
  // save last employee - the last pivot values filled
  FEmpl_Filled := EmplNumber;
  FEmplDesc_Filled := Name;

  TablePivot.Filter := '';
  TablePivot.Filter := 'PIVOTCOMPUTERNAME = ''' +
    SystemDM.CurrentCOMPUTERName + '''';


  if not TablePivot.Active then
    TablePivot.Open
  else
    TablePivot.Refresh;
  TablePivot.First;

  ClientDataSetEMPLN.Filter := '';
  ClientDataSetEMA.Filter := '';
  ClientDataSetWKPerEmpl.Filter := '';
end; // FillPIVOTTable

procedure TStaffPlanningEMPDM.SetFieldPIVOT(EmplNumber, ColIndex: Integer;
  ValStr, ColumnStr: String; ValCol, Color: Integer;
  SetColor, Pressed: Boolean);
var
  i, PrevColor: Integer;
  StrTmp1, StrTmp2, PrevBtn, PrevPressed, PrevLevel: String;
  ChangeBtn: Boolean;
  function ColIndexStr: String;
  begin
    // GLOB3-60 ColIndex can now be max. 10. Because of that we
    //          convert it to hex (10->A), so it is still stored as 1 position.
    Result := Int2Hex(ColIndex); { Format('%.2d',[ColIndex]); }
  end; // ColIndexStr
begin
  FListOCIUpdate.Clear;
  for i:= 0 to FWKCount do
  begin
    ChangeBtn := False;
    PrevBtn := Trim(Copy(TablePivot.FieldByName('WK' + IntToStr(i)).AsString,
      2 * ColIndex - 1, 2));

    GetAtributeBtn(PrevBtn, PrevColor, PrevPressed, PrevLevel);
    if Pressed and (PrevPressed = 'N') then
    begin
      PrevPressed := 'Y';
      ChangeBtn := True;
    end
    else
      if not Pressed and (PrevPressed = 'Y') then
      begin
        PrevPressed := 'N';
        ChangeBtn := True;
      end;

    if SetColor or (Not SetColor and
      ('WK' + IntToStr(i) = ColumnStr)) then
    begin
      if Not SetColor then
      begin
// only one button has changed color into green or into normal
        if (PrevColor <> Color) and
          ((Color = Green) or (Color = LightGreen)) then
          FListOCIUpdate.Add('WK' + IntToStr(i) + ColIndexStr + ' ' +
            PrevBtn + '+1')
        else
          if (PrevColor <> Color) and
           ((PrevColor = Green) or (PrevColor = LightGreen)) then
            FListOCIUpdate.Add('WK' + IntToStr(i) + ColIndexStr +
              ' ' + PrevBtn + '-1');
      end;
// only if was green because now is dark grey
      if SetColor then
      begin
        if ((PrevColor = Green) or (PrevColor = LightGreen)) and
         (Color <> PrevColor) then
        FListOCIUpdate.Add('WK' + IntToStr(i) + ColIndexStr + ' ' +
          PrevBtn + '-1');
      end;
      ChangeBtn := True;
      PrevColor := Color;
    end;
    if ChangeBtn then
      if Trim(Copy(TablePivot.FieldByName('WK' +
        IntToStr(i)).AsString, 2 * ColIndex - 1, 2)) <> '20' then
      begin
        StrTmp1 :=
          FillLeadSpace(GetTableBTN(PrevColor, PrevPressed, PrevLevel), 2);
        StrTmp2 := TablePivot.FieldByName('WK' + IntToStr(i)).AsString;
        StrTmp2[2 * ColIndex -1] := StrTmp1[1];
        StrTmp2[2 * ColIndex ] := StrTmp1[2];
        TablePivot.FieldByName('WK' + IntToStr(i)).AsString := StrTmp2;;
      end;

  end;
  PrevBtn := Trim(Copy(
    TablePivot.FieldByName('WKEMPL').AsString,
     2 * ColIndex -1, 2));
  GetAtributeBtn(PrevBtn, PrevColor, PrevPressed, PrevLevel);

  if Pressed then
    PrevPressed := 'Y'
  else
    PrevPressed := 'N';
// change empl color button from: normal - green - yellow
  if ((Color = Green) or (Color = LightGreen)) and
    ((PrevColor = Green) or (PrevColor = LightGreen) or
     (PrevColor = Yellow)) then
    Color := Yellow
  else
    if (Color = Normal) and (PrevColor = Yellow) then
    begin
      if ClientDataSetTBPerEmpl.FindKey([EmplNumber, FPlant, FShift, ColIndex]) then
         Color := LightGreen
      else
       Color := Green;
    end;
  if Trim(Copy(
    TablePivot.FieldByName('WKEMPL').AsString,
    2 * ColIndex - 1, 2)) <> '20' then
  begin
   StrTmp1 := FillLeadSpace(GetTableBTN(Color, PrevPressed, PrevLevel), 2);
   StrTmp2 :=
     TablePivot.FieldByName('WKEMPL').AsString;
   StrTmp2[2 * ColIndex -1] :=  StrTmp1[1];
   StrTmp2[2 * ColIndex] :=  StrTmp1[2];
   TablePivot.FieldByName('WKEMPL').AsString :=
     StrTmp2;
  end;
end; // SetFieldPIVOT

// Update Employee Availability
procedure TStaffPlanningEMPDM.UpdateEMAEmpl(EmplNumber: Integer; EmplWKStr: String);
var
  i, p, Index, ValColor, PosAbs, SHIFT: Integer;
  Absence, WKEmpl: TTBLevel;
  ValPressed, ValLevel, SelectStr, Temp: String;
begin
  ClientDataSetEMA.Filter := '';

  Shift := FShift;
  if not ClientDataSetEMA.FindKey([EmplNumber, FShift]) then
  begin
    if not ClientDataSetEMA.FindKey([EmplNumber, 0]) then
      Exit;
    Shift := 0;
  end;
// get the values for employee for all workspots of Btn: WKEmpl1..4_Btn
  for i := 1 to SystemDM.MaxTimeblocks do
  begin
   p:= Pos(CHR_SEP, EmplWKStr);
   if p > 0 then
   begin
     WKEmpl[i] := Copy(EmplWKStr, 0, p-1);
     Delete(EmplWKStr, 1, p);
   end
   else
     WKEmpl[i] := '';
  end;
  Temp := '';
  for i:= 1 to SystemDM.MaxTimeblocks do
  begin
    Absence[i] := '-';
    GetAtributeBTN(WKEmpl[i], ValColor, ValPressed, ValLevel);

    if (ValColor <> Invisible) and (ValColor <> Red) then
    begin
      if (ValColor <> DarkGrey) then
      begin
        Absence[i] := '*';

        Temp := Temp + 'AVAILABLE_TIMEBLOCK_' + IntToStr(i) + ' = ''*'',';
      end
      else
      begin
        Index := FListAbsRsn.IndexOf(IntToStr(EmplNumber) + CHR_SEP + IntToStr(i));
        if Index >= 0 then
        begin
          Absence[i] := FListAbsRsn.Strings[Index + 1];
          PosAbs := Pos(' ', Absence[i]);
          if PosAbs > 0 then
            Absence[i] := Copy(Absence[i], 0, PosAbs - 1);
          PosAbs := Pos('&', Absence[i]);
          if PosABS > 0 then
            Absence[i] := Copy(Absence[i], 0, PosABS - 1) +
              Copy(Absence[i], PosABS + 1, 10);
          Absence[i] := Copy(Absence[i], 0, 1);

          Temp := Temp + 'AVAILABLE_TIMEBLOCK_' + IntToStr(i) + ' = ''' +
            Absence[i] + ''',';
        end;
      end;
    end;
  end;
  if Temp <> ''  then
  begin
    SelectStr := 'UPDATE  EMPLOYEEAVAILABILITY SET ' +
      Temp +
      ' MUTATOR = :MUTATOR, MUTATIONDATE = :CURRENTDATE ' +
      ' WHERE EMPLOYEE_NUMBER = ' + IntToStr(EmplNumber) + ' AND  ' +
      ' PLANT_CODE = ''' + DoubleQuote(FPlant) +
      ''' AND SHIFT_NUMBER = ' + IntToStr(Shift) +
      ' AND EMPLOYEEAVAILABILITY_DATE = :FDate';

    QueryWorkTmp.Active := False;
    QueryWorkTmp.SQL.Clear;
    QueryWorkTmp.SQL.Add(SelectStr);
    QueryworkTMP.ParamByName('FDATE').AsDateTime := FDate;
    QueryworkTMP.ParamByName('CURRENTDATE').AsDateTime := FCurrentDate;
    QueryworkTMP.ParamByName('MUTATOR').AsString :=
      SystemDM.CurrentProgramUser;
    QueryWorkTmp.Active := False;
    QueryWorkTmp.ExecSQL;
  end;
end; // UpdateEMAEmpl

procedure TStaffPlanningEMPDM.SaveEMP_SPL;
var
  EmplNumber, RecCount: Integer;
  EmplIndexPlanned: TTBColor;
  EmplWKStr: String;
begin

  if FListSaveEmp.Count = 0 then
    Exit;
  if TablePivot.State in [dsEdit] then
    TablePivot.Post;

  RecCount := 0;
  while (RecCount <= FListSaveEmp.Count - 1) do
  begin
    EmplNumber := StrToInt(FListSaveEmp.Strings[RecCount]);
    EmplWKStr := '';
    if (RecCount + 1) <= FListSaveEmp.Count - 1 then
      EmplWKStr := FListSaveEmp.Strings[RecCount + 1];
    if (FSelection = 0) then
       UpdateEMAEMPL(EmplNumber, EmplWKStr);
    EmplWKStr := '';
    if (RecCount + 2) <= FListSaveEmp.Count - 1 then
      EmplWKStr := FListSaveEmp.Strings[RecCount + 2];
    UpdateEMPEmpl(EmplNumber, EmplWKStr, EmplIndexPlanned);
    UpdateEMPNotVisible(EmplNumber, EmplIndexPlanned);
    RecCount := RecCount + 3;
  end;
  FListSaveEmp.Clear;

end; // SaveEMP_SPL

// check if exists in EMP wk/dept which are not selected but are planned
// Update Employee Planning / Standard Employee Planning
procedure TStaffPlanningEMPDM.UpdateEMPNotVisible(EmplNumber: Integer;
  EmplIndexPlanned: TTBColor);
var
  WK, Dept: String;
  IndexPlanned: Integer;
  UpdateTable: Boolean;
  function EmpIndexPlannedCheck: Integer;
  var
    Index: Integer;
  begin
{
  if (EmplIndexPlanned[1] = 0) and (EmplIndexPlanned[2] = 0) and
    (EmplIndexPlanned[3] = 0) and (EmplIndexPlanned[4] = 0) then
}
    Result := 0;
    for Index := 1 to SystemDM.MaxTimeblocks do
      Result := Result + EmplIndexPlanned[Index];
  end; // EmpIndexPlannedCheck
begin
  if (EmpIndexPlannedCheck = 0) then
    Exit;

  ClientDataSetEMPLN.Filter := '';
  ClientDataSetEMPLN.First;
  while not ClientDataSetEMPLN.Eof do
  begin
    WK := ClientDataSetEMPLN.FieldByName('WORKSPOT_CODE').AsString;
    Dept := ClientDataSetEMPLN.FieldByName('DEPARTMENT_CODE').AsString;
    UpdateTable := False;
    if FWKList.IndexOf(WK) < 0 then
    begin
      for IndexPlanned := 1 to MAX_TBS do
      begin
       if (EmplIndexPlanned[IndexPlanned] <> 0) and
         (ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_'+
            IntToStr(IndexPlanned)).AsString = 'A') or
         (ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_'+
            IntToStr(IndexPlanned)).AsString = 'B') or
         (ClientDataSetEMPLN.FieldByName('SCHEDULED_TIMEBLOCK_'+
            IntToStr(IndexPlanned)).AsString = 'C') then
       begin
         UpdateTable := True;
         if FSelection = 0  then
           QueryUpdateEMP.ParamByName('TB' +
             IntToStr(IndexPlanned)).AsString := 'N'
         else
           QueryUpdateSTDEMP.ParamByName('TB' +
             IntToStr(IndexPlanned)).AsString := 'N';
         EmplIndexPlanned[IndexPlanned] := 0;
       end;
     end;
     if UpdateTable then
     begin
       if FSelection = 0 then
       begin
         QueryUpdateEMP.Active := False;
         QueryUpdateEMP.ParamByName('DEPT').AsString := Dept;
         QueryUpdateEMP.ParamByName('WK').AsString := WK;
         QueryUpdateEMP.ParamByName('EMPL').AsInteger := EmplNumber;
         QueryUpdateEMP.ParamByName('SHIFT').AsInteger := FShift;
         QueryUpdateEMP.ParamByName('PLANT').AsString := FPlant;
         QueryUpdateEMP.ParamByName('DATEEMA').AsDateTime :=  GetDate(FDate);
         QueryUpdateEMP.ExecSQL;
         QueryUpdateEMP.Active := False;
       end
       else
       begin
         QueryUpdateSTDEMP.Active := False;
         QueryUpdateSTDEMP.ParamByName('DEPT').AsString := Dept;
         QueryUpdateSTDEMP.ParamByName('WK').AsString := WK;
         QueryUpdateSTDEMP.ParamByName('EMPL').AsInteger := EmplNumber;
         QueryUpdateSTDEMP.ParamByName('SHIFT').AsInteger := FShift;
         QueryUpdateSTDEMP.ParamByName('PLANT').AsString := FPlant;
         QueryUpdateSTDEMP.ParamByName('DAY_WEEK').AsInteger :=  FDay;
         QueryUpdateSTDEMP.ExecSQL;
         QueryUpdateSTDEMP.Active := False;
       end;
     end;
    end;
    if (EmpIndexPlannedCheck = 0) then
      Exit;
    ClientDataSetEMPLN.Next;
  end;
end; // UpdateEMPNotVisible

// save extra : workspot/dept which are not in list and are planned
// Insert/Update Employee Planning / Standard Employee Planning
procedure TStaffPlanningEMPDM.UpdateEMPEmpl(EmplNumber: Integer; EmplWKStr: String;
  var EmplIndexPlanned: TTBColor);
var
  WK, Dept, WKCODE: String;
  Index, i, j, p, pLevel: Integer;
  TB, WKPlanned, DeptPlanned, LevelPlanned: TTBLevel;
  WKTmp, Level, IndexTmp: String;
begin
// get the planned workspot from the SaveEmpList
  for j := 1 to MAX_TBS do
  begin
    EmplIndexPlanned[j] := 0;
    WKPlanned[j] := '';
    DeptPlanned[j] := '';
  end;
  if EmplWKStr <> '' then
    for j := 1 to SystemDM.MaxTimeblocks do
    begin
      p := Pos(CHR_SEP, EmplWKStr);
      if p > 0 then
      begin
        PLevel := Pos(CHR_SEP1, EmplWKStr);
        if pLevel > 0 then
        begin
        // wk index in FWkList => workspot, dept planned
          WKTmp := Copy(EmplWKStr, 1, pLevel - 2);
        // index of TB  for wk, dept planned
          IndexTmp := Copy(EmplWKStr, plevel - 1, 1);
        // value of Btn - in table Pivot
          Level := Copy(EmplWKStr, plevel + 1, p - plevel - 1)
        end;
        if WKTmp <> '' then
          GetWKDeptCode(FWKList.Strings[StrToInt(WKTmp)],
            StrToInt(WKTmp), WK, DEPT);
        if IndexTmp <> '' then
        begin
          // GLOB3-60 Index (of timeblock) is stored in hex! 10 will be A.
          Index := Hex2Int(IndexTmp);
          WKPlanned[Index] :=  WK;
          DeptPlanned[Index] := DEPT;
          LevelPlanned[Index] := Level;
        end;
        Delete(EmplWKStr, 1, P);
      end
      else
        Break;
  end;
  for i := 0 to FWKCount do
  begin
    GetWKDeptCode(FWKList.Strings[i], i, WK, DEPT);
    WKCode := WK;
    if WK = DummyStr then
      WKCode := Dept;
    for J := 1 to SysteMDM.MaxTimeblocks do
    begin
      TB[J] := 'N';
      if (WKPlanned[J] = WK) and (DeptPlanned[J] = Dept) then
      begin
        if  LevelPlanned[J] <> '' then
        begin
          TB[J] := GetLevel(StrToInt(LevelPlanned[J]));
          if TB[J] = '' then
            TB[J] := 'N';
          EmplIndexPlanned[J] := J;
        end;
      end;
    end;
    if (FSelection = 0) then
    begin
      StoredProcFillEPL.ParamByName('PLANT').AsString := FPlant;
      StoredProcFillEPL.ParamByName('WORKSPOT').AsString := WK;
      StoredProcFillEPL.ParamByName('DEPARTMENT').AsString := DEPT;
      StoredProcFillEPL.ParamByName('EMPLOYEE').AsInteger := EmplNumber;
      StoredProcFillEPL.ParamByName('SHIFT').AsInteger := FShift;
      StoredProcFillEPL.ParamByName('DATEEPL').AsDateTime := GetDate(FDATE);
      StoredProcFillEPL.ParamByName('DATENOW').AsDateTime := FCurrentDate;
      StoredProcFillEPL.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      for Index := 1 to MAX_TBS do
        StoredProcFillEPL.
          ParamByName('SCHEDULED_TIMEBLOCK_'+IntToStr(Index)).AsString := TB[Index];
      StoredProcFillEPL.Prepare;
      StoredProcFillEPL.ExecProc;
    end
    else
    begin
{$IFDEF DEBUG}
  WDebugLog('- P=' + FPlant +
    ' WS=' + WK +
    ' Dept=' + DEPT +
    ' Emp=' + IntToStr(EmplNumber) +
    ' Sh=' + IntToStr(FShift) +
    ' Day=' + IntToStr(FDAY) +
    ' Date=' + DateToStr(FCurrentDate) +
    ' TB1=' + TB[1] +
    ' TB2=' + TB[2] +
    ' TB3=' + TB[3] +
    ' TB1=' + TB[4] +
    ' TB2=' + TB[5] +
    ' TB3=' + TB[6] +
    ' TB1=' + TB[7] +
    ' TB2=' + TB[8] +
    ' TB3=' + TB[9] +
    ' TB4=' + TB[10]
    );
{$ENDIF}
      StoredProcSTDEPL.ParamByName('PLANT').AsString := FPlant;
      StoredProcSTDEPL.ParamByName('WORKSPOT').AsString := WK;
      StoredProcSTDEPL.ParamByName('DEPARTMENT').AsString := DEPT;
      StoredProcSTDEPL.ParamByName('EMPLOYEE').AsInteger := EmplNumber;
      StoredProcSTDEPL.ParamByName('SHIFT').AsInteger := FShift;
      StoredProcSTDEPL.ParamByName('DAYEPL').AsInteger := FDAY;
      StoredProcSTDEPL.ParamByName('DATENOW').AsDateTime := FCurrentDate;
      StoredProcSTDEPL.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      for Index := 1 to MAX_TBS do
        StoredProcSTDEPL.
          ParamByName('SCHEDULED_TIMEBLOCK_'+IntToStr(Index)).AsString := TB[Index];
      StoredProcSTDEPL.Prepare;
      StoredProcSTDEPL.ExecProc;
    end;
 end;
end; // UpdateEMPEmpl

procedure TStaffPlanningEMPDM.AddInList(StrEmplNumber: String;
  ListStr: TStringList);
var
  IndexList: Integer;
  PlannedEmpl: Boolean;
  StrList: String;
  function GetEmplWKStr(var PlannedEmp: Boolean): String;
  var
    i, ValInt: Integer;
    ValStr: String;
  begin
    Result := '';
    PlannedEmp:= False;
    for i := 1 to SysteMDM.MaxTimeblocks do
    begin
      ValStr := Trim(Copy(TablePivot.FieldByName('WKEMPL' ).AsString,
        2*i -1,2));
      Result := Result + ValStr +  CHR_SEP;
      ValInt := StrToInt(ValStr);
      if ((ValInt >= 12) and (ValInt <= 19)) or
        ((ValInt>= 21) and (ValInt <= 25)) then
          PlannedEmp := True;
    end;
  end;

  function GetPlannedEmplWKStr: String;
  var
    i, j, CheckWK: Integer;
    ValInt: Integer;
  begin
    Result := '';
    CheckWK := 0;
    for i := 0 to FWKCount do
    begin
      for j := 1 to SystemDM.MaxTimeblocks do
      begin
        ValInt := StrToInt(Trim(Copy(TablePivot.FieldByNAME('WK' +
          IntToStr(i)).AsString,
            2*j - 1, 2)));
        if ((ValInt >= 12) and (ValInt <= 19)) or
          ((ValInt >= 21) and(ValInt <= 25) ) then
        begin // GLOB3-60 Store j (column of timeblock) as hex. so 10 will be A.
          Result := Result + IntToStr(i) + Int2Hex{IntToStr}(j) + CHR_SEP1 +
            Trim(Copy(TablePivot.FieldByNAME('WK' + IntToStr(i)).AsString,
             2*j - 1, 2)) + CHR_SEP;
          CheckWK := CheckWK + 1;
        end;
      end;
      if CheckWK = 4 then //? GLOB3-60
        Exit;
    end;
  end;
begin
  IndexList := ListStr.IndexOf(StrEmplNumber);
  if IndexList < 0 then
  begin
  // add employee number changed
    ListStr.Add(StrEmplNumber);
    StrList := GetEmplWKStr(PlannedEmpl);
    // add 4 values of employee - all - workspots
    ListStr.Add(StrList);
    StrList := '';
    if PlannedEmpl then
  // add values of planned workspot:
  // index WK in FWKList + IndexTB [1..4] + chr_sep1 + value (Btn) + chr_sep
      StrList := GetPlannedEmplWKStr;
    ListStr.Add(StrList);
  end
  else
  begin
    ListStr.Delete(IndexList + 1);
    ListStr.Delete(IndexList + 1);
    StrList := GetEmplWKStr(PlannedEmpl);
    ListStr.Insert(IndexList + 1, StrList);
    StrList := '';
    if PlannedEmpl then
      StrList := GetPlannedEmplWKStr;
    ListStr.Insert(IndexList + 2, StrList);
  end;
end; // AddInList


procedure TStaffPlanningEMPDM.DataModuleCreate(Sender: TObject);
//var
//  SelectStr: String;
begin
  inherited;
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := StaffPlanningEMPDM.DataModuleDestroy;
  ShowLevelC := True; // MR:10-01-2005 Order 550361
  ClientDataSetAbsRsn.Open;
  try
    ClientDataSetAbsRsn.LogChanges := False; // RV079.3.
  except
  end;
  ClientDataSetTBPerEmpl.Open;
  try
    ClientDataSetTBPerEmpl.LogChanges := False; // RV079.3.
  except
  end;
  ClientDataSetTBPerDept.Open;
  try
    ClientDataSetTBPerDept.LogChanges := False; // RV079.3.
  except
  end;
  // RV092.11.
  ClientDataSetEmpl.Open;
  try
    ClientDataSetEmpl.LogChanges := False; // RV079.3.
  except
  end;

  ClientDataSetTBPerEmpl.Filter := '';
  ClientDataSetTBPerDept.Filter := '';

  QuerySTA.Prepare;
  QueryEMA.Prepare;
  QuerySHS.Prepare;

  QueryUpdateEMP.Prepare;
  QueryUpdateSTDEMP.Prepare;

  FListABSRSN := TStringList.Create;
  FListOCIUpdate := TStringList.Create;
  TablePivot.Filtered := True;
  TablePivot.Filter := 'PIVOTCOMPUTERNAME = ''' +
    SystemDM.CurrentCOMPUTERName + '''';
  QueryEMP.Prepare;
  QuerySTDEMP.Prepare;
end; // DataModuleCreate

procedure TStaffPlanningEMPDM.TablePivotAfterScroll(DataSet: TDataSet);
var
  EmplMin, EmplMax: Integer;
begin
  inherited;

  if not DataSet.Active then
    Exit;
  if StaffPlanningF = Nil then
    Exit;

   FillEmpl_PIVOTTable(DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    DataSet.FieldByName('DESCRIPTION').AsString);

  //set the color per line in grid
  StaffPlanningF.dxDBGridPlanning.FullRefresh;
  //Car 24-03-2004 error solved
  if StaffPlanningF.dxDBGridPlanning.Count <= 1 then
    Exit;
  //
  EmplMin := StaffPlanningF.dxDBGridPlanning.Items[1].Values[0];
  EmplMax:= StaffPlanningF.dxDBGridPlanning.Items[
    StaffPlanningF.dxDBGridPlanning.Count-1].Values[0];
  if ((EmplMin > EmplMax) and (FEmpSort = 0 ))  or
    ((FEmpSort = 1) and
     (StaffPlanningF.dxDBGridPlanning.Items[1].Values[1]>
      StaffPlanningF.dxDBGridPlanning.Items[
      StaffPlanningF.dxDBGridPlanning.Count-1].Values[1] )) then
    TablePivot.Last;
end; // TablePivotAfterScroll

end.

