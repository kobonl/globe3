inherited ReportHrsPerWKCUMDM: TReportHrsPerWKCUMDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryProdHour: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProdHourFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE, '
      '  W.DEPARTMENT_CODE, '
      '  P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER,'
      '  P.PRODHOUREMPLOYEE_DATE ,'
      '  SUM(P.PRODUCTION_MINUTE) AS SUMMIN '
      'FROM '
      '  PRODHOURPEREMPLOYEE P , WORKSPOT W,'
      '  DEPARTMENT D'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  P.PLANT_CODE, D.BUSINESSUNIT_CODE, '
      '  W.DEPARTMENT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, '
      '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE ')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProd: TDataSource
    DataSet = QueryProdHour
    Left = 184
    Top = 24
  end
  object QueryBUPerWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProd
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE, '
      ' SUM(PERCENTAGE) AS SUMPERC'
      'FROM '
      '  BUSINESSUNITPERWORKSPOT'
      'GROUP BY '
      '  PLANT_CODE, WORKSPOT_CODE, BUSINESSUNIT_CODE'
      '')
    Left = 72
    Top = 256
  end
  object dspBUPerWK: TDataSetProvider
    DataSet = QueryBUPerWK
    Constraints = True
    Left = 168
    Top = 256
  end
  object cdsBUPerWK: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspBUPerWK'
    Left = 272
    Top = 256
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, DESCRIPTION '
      'FROM '
      '  EMPLOYEE '
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 72
    Top = 176
  end
  object dspEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 168
    Top = 176
  end
  object cdsEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEmpl'
    Left = 272
    Top = 176
  end
  object QueryWSWorkingDays: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProdHourFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE, '
      '  W.DEPARTMENT_CODE, '
      '  P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER,'
      '  P.PRODHOUREMPLOYEE_DATE ,'
      '  SUM(P.PRODUCTION_MINUTE) AS SUMMIN '
      'FROM '
      '  PRODHOURPEREMPLOYEE P , WORKSPOT W,'
      '  DEPARTMENT D'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  P.PLANT_CODE, D.BUSINESSUNIT_CODE, '
      '  W.DEPARTMENT_CODE, P.WORKSPOT_CODE, P.JOB_CODE, '
      '  P.EMPLOYEE_NUMBER, P.PRODHOUREMPLOYEE_DATE ')
    Left = 72
    Top = 320
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
end
