inherited DialogCopyFromSHSF: TDialogCopyFromSHSF
  Left = 315
  Top = 299
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Copy from other employee'
  ClientHeight = 202
  ClientWidth = 519
  OldCreateOrder = True
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 142
    Width = 519
    inherited btnOk: TBitBtn
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      OnClick = btnCancelClick
    end
  end
  inherited stbarBase: TStatusBar
    Top = 183
    Width = 519
  end
  object GroupBoxCopy: TGroupBox
    Left = 0
    Top = 0
    Width = 519
    Height = 142
    Align = alClient
    Caption = 'Copy shift schedule of'
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 40
      Width = 46
      Height = 13
      Caption = 'Employee'
    end
    object Label4: TLabel
      Left = 288
      Top = 72
      Width = 18
      Height = 13
      Caption = 'thru'
    end
    object Label6: TLabel
      Left = 16
      Top = 72
      Width = 53
      Height = 13
      Caption = 'Year/Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object dxSpinEditYearFrom: TdxSpinEdit
      Left = 88
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 184
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditYearTo: TdxSpinEdit
      Left = 344
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 424
      Top = 70
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 88
      Top = 40
      Width = 177
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      AutoSize = False
      DataField = 'VALUEDISPLAY'
      DataSource = ShiftScheduleDM.DataSourceEmpl
      PopupHeight = 150
      PopupWidth = 350
      DBGridLayout = dxDBGridLayoutListEmployeeFrom
      Height = 19
    end
  end
  object TableCopy: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'SHIFTSCHEDULE'
    Left = 472
    Top = 104
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 328
    Top = 16
    object dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        3E040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365071E53686966745363686564756C65444D2E44617461536F75
        726365456D706C104F7074696F6E73437573746F6D697A650B0E6564676F4261
        6E644D6F76696E670E6564676F42616E6453697A696E67106564676F436F6C75
        6D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E6564676F4675
        6C6C53697A696E6700094F7074696F6E7344420B106564676F43616E63656C4F
        6E457869740D6564676F43616E44656C6574650D6564676F43616E496E736572
        74116564676F43616E4E617669676174696F6E116564676F436F6E6669726D44
        656C657465126564676F4C6F6164416C6C5265636F726473106564676F557365
        426F6F6B6D61726B7300000F546478444247726964436F6C756D6E0C436F6C75
        6D6E4E756D6265720743617074696F6E06064E756D62657206536F7274656407
        046373557005576964746802410942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060F454D504C4F5945455F4E554D42455200
        000F546478444247726964436F6C756D6E0F436F6C756D6E53686F72744E616D
        650743617074696F6E060A53686F7274206E616D650557696474680254094261
        6E64496E646578020008526F77496E6465780200094669656C644E616D65060A
        53484F52545F4E414D4500000F546478444247726964436F6C756D6E0A436F6C
        756D6E4E616D650743617074696F6E06044E616D6505576964746803B4000942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0B4445534352495054494F4E00000F546478444247726964436F6C756D6E0D43
        6F6C756D6E416464726573730743617074696F6E060741646472657373055769
        64746802450942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506074144445245535300000F546478444247726964436F6C756D
        6E0E436F6C756D6E44657074436F64650743617074696F6E060F446570617274
        6D656E7420636F646505576964746802580942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060F4445504152544D454E545F43
        4F444500000F546478444247726964436F6C756D6E0A436F6C756D6E5465616D
        0743617074696F6E06095465616D20636F64650942616E64496E646578020008
        526F77496E6465780200094669656C644E616D6506095445414D5F434F444500
        0000}
    end
  end
end
