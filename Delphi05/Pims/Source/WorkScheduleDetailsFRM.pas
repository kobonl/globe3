(*
  MRA:3-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:13-JUL-2018 GLOB3-60
  - Related to this order.
  - Changes made to show list of shifts in correct numerical sorting-order.
*)
unit WorkScheduleDetailsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, SystemDMT, WorkScheduleDetailsDMT, StdCtrls, Mask,
  DBCtrls, dxEditor, dxEdLib, dxDBELib, dxExEdtr, Menus;

type
  TWorkScheduleDetailsF = class(TGridBaseF)
    dxMasterGridColumnCODE: TdxDBGridColumn;
    dxMasterGridColumnDESCRIPTION: TdxDBGridColumn;
    dxMasterGridColumnREPEATS: TdxDBGridColumn;
    dxMasterGridColumnCALCREFERENCEWEEK: TdxDBGridColumn;
    dxDetailGridColumnMYROW: TdxDBGridColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    lblDay1: TLabel;
    lblDay2: TLabel;
    lblDay3: TLabel;
    lblDay4: TLabel;
    lblDay5: TLabel;
    lblDay6: TLabel;
    lblDay7: TLabel;
    Label2: TLabel;
    dxDBEditShift1: TdxDBEdit;
    dxDBEditShift2: TdxDBEdit;
    dxDBEditShift3: TdxDBEdit;
    dxDBEditShift4: TdxDBEdit;
    dxDBEditShift5: TdxDBEdit;
    dxDBEditShift6: TdxDBEdit;
    dxDBEditShift7: TdxDBEdit;
    Label3: TLabel;
    dxDBEditAbsRsn1: TdxDBEdit;
    dxDBEditAbsRsn2: TdxDBEdit;
    dxDBEditAbsRsn3: TdxDBEdit;
    dxDBEditAbsRsn4: TdxDBEdit;
    dxDBEditAbsRsn5: TdxDBEdit;
    dxDBEditAbsRsn6: TdxDBEdit;
    dxDBEditAbsRsn7: TdxDBEdit;
    Label4: TLabel;
    DBRGrpType1: TDBRadioGroup;
    DBRGrpType2: TDBRadioGroup;
    DBRGrpType3: TDBRadioGroup;
    Label5: TLabel;
    dxDBSpinEditRepeats1: TdxDBSpinEdit;
    DBRGrpType4: TDBRadioGroup;
    DBRGrpType5: TDBRadioGroup;
    DBRGrpType6: TDBRadioGroup;
    DBRGrpType7: TDBRadioGroup;
    dxDBSpinEditRepeats2: TdxDBSpinEdit;
    dxDBSpinEditRepeats3: TdxDBSpinEdit;
    dxDBSpinEditRepeats4: TdxDBSpinEdit;
    dxDBSpinEditRepeats5: TdxDBSpinEdit;
    dxDBSpinEditRepeats6: TdxDBSpinEdit;
    dxDBSpinEditRepeats7: TdxDBSpinEdit;
    Label6: TLabel;
    DBText1: TDBText;
    PopupMenuShift: TPopupMenu;
    PopupMenuAbsRsn: TPopupMenu;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBEditMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure dxDBEditAbsRsnExit(Sender: TObject);
    procedure dxDBEditAbsRsnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxDBEditShiftExit(Sender: TObject);
    procedure dxDBEditShiftKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    ColumnName: String;
    procedure AbsRsnPopupItemClick(Sender: TObject);
    procedure ShiftPopupItemClick(Sender: TObject);
    procedure CreateAbsReasonPopupMenu;
    procedure CreateShiftNumberPopupMenu;
  public
    { Public declarations }
  end;

function WorkScheduleDetailsF: TWorkScheduleDetailsF;

var
  WorkScheduleDetailsF_HDN: TWorkScheduleDetailsF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes;

function WorkScheduleDetailsF: TWorkScheduleDetailsF;
begin
  if WorkScheduleDetailsF_HDN = nil then
  begin
    WorkScheduleDetailsF_HDN := TWorkScheduleDetailsF.Create(Application);
  end;
  Result := WorkScheduleDetailsF_HDN;
end;

procedure TWorkScheduleDetailsF.FormCreate(Sender: TObject);
begin
  WorkScheduleDetailsDM := CreateFormDM(TWorkScheduleDetailsDM);
  if (dxMasterGrid.DataSource = Nil) then
    dxMasterGrid.DataSource := WorkScheduleDetailsDM.DataSourceMaster;
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := WorkScheduleDetailsDM.DataSourceDetail;
  inherited; // Do this HERE, not at the start! Or synch with buttons will fail!
  lblDay1.Caption := SystemDM.GetDayWDescription(1);
  lblDay2.Caption := SystemDM.GetDayWDescription(2);
  lblDay3.Caption := SystemDM.GetDayWDescription(3);
  lblDay4.Caption := SystemDM.GetDayWDescription(4);
  lblDay5.Caption := SystemDM.GetDayWDescription(5);
  lblDay6.Caption := SystemDM.GetDayWDescription(6);
  lblDay7.Caption := SystemDM.GetDayWDescription(7);
  CreateAbsReasonPopupMenu;
  CreateShiftNumberPopupMenu;
end;

procedure TWorkScheduleDetailsF.FormDestroy(Sender: TObject);
begin
  inherited;
  WorkScheduleDetailsF_HDN := nil;
end;

procedure TWorkScheduleDetailsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TWorkScheduleDetailsF.CreateAbsReasonPopupMenu;
var
  NewItem: TMenuItem;
begin
  PopupMenuAbsRsn.Items.Clear;

  with WorkScheduleDetailsDM do
  begin
    with qryAbsRsn do
    begin
      Close;
      Open;
      while not Eof  do
      begin
        NewItem := TMenuItem.Create(Self);
        NewItem.Caption := FieldByName('ABSENCEREASON_CODE').AsString +  ' | ' +
          FieldByName('DESCRIPTION').AsString;
        PopupMenuAbsRsn.Items.Add(NewItem);
        NewItem.OnClick := AbsRsnPopupItemClick;
        Next;
      end;
      Close;
    end;
  end;
end; // CreateAbsReasonPopupMenu

procedure TWorkScheduleDetailsF.CreateShiftNumberPopupMenu;
var
  NewItem: TMenuItem;
begin
  PopupMenuShift.Items.Clear;

  // GLOB3-60 Sort numerical on shift-number!
  // Query is changed, because it gave a wrong sorting-order.
  // First add a '-'
  NewItem := TMenuItem.Create(Self);
  NewItem.Caption := '- | -';
  PopupMenuShift.Items.Add(NewItem);
  NewItem.OnClick := ShiftPopupItemClick;

  with WorkScheduleDetailsDM do
  begin
    with qryShift do
    begin
      Close;
      Open;
      while not Eof  do
      begin
        NewItem := TMenuItem.Create(Self);
        NewItem.Caption := FieldByName('SHIFT_NUMBER').AsString +  ' | ' +
          FieldByName('DESCRIPTION').AsString;
        PopupMenuShift.Items.Add(NewItem);
        NewItem.OnClick := ShiftPopupItemClick;
        Next;
      end;
      Close;
    end;
  end;
end; // CreateShiftNumberPopupMenu

procedure TWorkScheduleDetailsF.AbsRsnPopupItemClick(Sender: TObject);
var
  PosAbs: Integer;
  Absence: String;
begin
  inherited;
  Absence := (Sender as TMenuItem).Caption;
  PosAbs := Pos('-', Absence);
  if PosAbs > 0 then
    Absence := '-'
  else
  begin
    PosAbs := Pos('*', Absence);
    if PosAbs > 0 then
      Absence := '*'
    else
    begin
      PosAbs := Pos('|', Absence);
      if  PosAbs > 0 then
        Absence := Trim(Copy(Absence, 2, 1));
    end;
  end;    
  with WorkScheduleDetailsDM do
  begin
    if not (TableDetail.State in [dsEdit]) then
      TableDetail.Edit;
    TableDetail.FieldByName(ColumnName).AsString := Absence;
  end;
end;

procedure TWorkScheduleDetailsF.ShiftPopupItemClick(Sender: TObject);
var
  PosShift: Integer;
  Shift: String;
  ShiftColumnName: String;
begin
  inherited;
  Shift := (Sender as TMenuItem).Caption;
  PosShift := Pos('|', Shift);
  if PosShift > 0 then
  begin
    Shift := Trim(Copy(Shift, 1, PosShift-1));
  end;
  with WorkScheduleDetailsDM do
  begin
    if not (TableDetail.State in [dsEdit]) then
      TableDetail.Edit;
    TableDetail.FieldByName(ColumnName).AsString := Shift;
    ShiftColumnName := ColumnName + '_NUMBER';
    if (Shift = '-') then
      TableDetail.FieldByName(ShiftColumnName).AsString := '-1'
    else
      TableDetail.FieldByName(ShiftColumnName).AsString := Shift;
  end;
end;

procedure TWorkScheduleDetailsF.dxDBEditMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  ColumnName := (Sender as TdxDBEdit).DataField;
end;

// Check on valid absense reason when entered by keyboard
procedure TWorkScheduleDetailsF.dxDBEditAbsRsnExit(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;

  if ((Sender as TdxDBEdit).Text = '') then
  begin
    DisplayMessage(SPimsFieldRequired, mtInformation, [mbOk]);
    (Sender as TdxDBEdit).SetFocus;
    Exit;
  end;
  if (not WorkScheduleDetailsDM.cdsAbsRsn.
    Locate('ABSENCEREASON_CODE', (Sender as TdxDBEdit).Text, []))
    and ((Sender as TdxDBEdit).Text <> '*') and
    ((Sender as TdxDBEdit).Text <> '-') then
  begin
    DisplayMessage(SEmplAvail, mtInformation, [mbOk]);
    (Sender as TdxDBEdit).SetFocus;
  end;
end;

procedure TWorkScheduleDetailsF.dxDBEditAbsRsnKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  dxDBEditAbsRsnExit(Sender);
end;

// Check on valid shift when entered by keyboard
procedure TWorkScheduleDetailsF.dxDBEditShiftExit(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;

  if ((Sender as TdxDBEdit).Text = '') then
  begin
    DisplayMessage(SPimsFieldRequired, mtInformation, [mbOk]);
    (Sender as TdxDBEdit).SetFocus;
    Exit;
  end;
  if (not WorkScheduleDetailsDM.cdsShift.
    Locate('SHIFT_NUMBER', (Sender as TdxDBEdit).Text, [])) then
  begin
    DisplayMessage(SPimsInvalidShift, mtInformation, [mbOk]);
    (Sender as TdxDBEdit).SetFocus;
  end;
end;

procedure TWorkScheduleDetailsF.dxDBEditShiftKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  dxDBEditShiftExit(Sender);
end;

end.
