(*
  MRA:15-MAY-2013 SO-20014137
  - New report employee illness.
    - Shows who is ill based on today's date.
*)
unit ReportEmployeeIllnessQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, QRCtrls,
  QuickRpt, ExtCtrls;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FShowSelection: Boolean;
    FExportToFile: Boolean;
    FShowOnlyActualIllness: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      ShowSelection: Boolean;
      ExportToFile: Boolean;
      ShowOnlyActualIllness: Boolean);
  end;

type
  TReportEmployeeIllnessQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLblFromPlant: TQRLabel;
    QRLblFromEmp: TQRLabel;
    QRLblEmp: TQRLabel;
    QRLblEmpFrom: TQRLabel;
    QRLblToEmp: TQRLabel;
    QRLblEmpTo: TQRLabel;
    QRLblPlant: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLblDate: TQRLabel;
    QRLblToPlant: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLblFromDate: TQRLabel;
    QRLblToDate: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLblDateTo: TQRLabel;
    QRLblShowActiveIllness: TQRLabel;
    QRBandDetails: TQRBand;
    QRBandTitleEmpl: TQRBand;
    QRShape5: TQRShape;
    QRLblEmployee: TQRLabel;
    QRLblStartDate: TQRLabel;
    QRLblTotal: TQRLabel;
    QRLblIllnessDays: TQRLabel;
    QRShape6: TQRShape;
    QRLblPhone: TQRLabel;
    QRDBTextEmpNr: TQRDBText;
    QRDBTextEmpName: TQRDBText;
    QRDBTextPhone: TQRDBText;
    QRDBTextStartDate: TQRDBText;
    QRDBTextTotal: TQRDBText;
    QRBandSummary: TQRBand;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailsAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    FShowActiveIllnessCaption: String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    procedure FreeMemory; override;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const ShowSelection: Boolean;
      const ExportToFile: Boolean;
      const ShowOnlyActualIllness: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    property ShowActiveIllnessCaption: String read FShowActiveIllnessCaption
      write FShowActiveIllnessCaption;
  end;

var
  ReportEmployeeIllnessQR: TReportEmployeeIllnessQR;

implementation

uses
  SystemDMT, ReportEmployeeIllnessDMT, UPimsConst;

{$R *.DFM}

procedure ExportDetail(AEmployeeNumber, AEmployeeName, APhone, AStartDate,
  ATotal: String);
begin
  ExportClass.AddText(
    AEmployeeNumber + ExportClass.Sep +
    AEmployeeName + ExportClass.Sep +
    APhone + ExportClass.Sep +
    AStartDate + ExportClass.Sep +
    ATotal);
end;

{ TQRParameters }

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  ShowSelection, ExportToFile, ShowOnlyActualIllness: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
  FShowOnlyActualIllness := ShowOnlyActualIllness;
end;

{ TReportEmployeeIllnessF }

function TReportEmployeeIllnessQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;
  with ReportEmployeeIllnessDM do
  begin
    SelectStr :=
      'SELECT ' + NL +
      '  T.EMPLOYEE_NUMBER, E.DESCRIPTION, E.PHONE_NUMBER, E.STARTDATE, ' + NL +
      '  COUNT(*) TOTAL ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY T LEFT JOIN ABSENCEREASON AR ON ' + NL +
      '    T.AVAILABLE_TIMEBLOCK_1 = AR.ABSENCEREASON_CODE OR ' + NL +
      '    T.AVAILABLE_TIMEBLOCK_2 = AR.ABSENCEREASON_CODE OR ' + NL +
      '    T.AVAILABLE_TIMEBLOCK_3 = AR.ABSENCEREASON_CODE OR ' + NL +
      '    T.AVAILABLE_TIMEBLOCK_4 = AR.ABSENCEREASON_CODE ' + NL +
      '  INNER JOIN EMPLOYEE E ON ' + NL +
      '    T.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      'WHERE ' + NL +
      '  E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom)  + '''' + NL +
      '  AND E.PLANT_CODE <= '''+
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
      '  AND ' + NL;
    if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
      SelectStr := SelectStr +
        '  E.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom +
        '  AND E.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
        '  AND ' + NL;
    SelectStr := SelectStr +
      '  T.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND ' + NL +
      '  T.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND ' + NL +
      '  T.EMPLOYEEAVAILABILITY_DATE >= E.STARTDATE AND ' + NL +
      '  AR.ABSENCETYPE_CODE = :ILLNESS ' + NL;
    // Who are ill based on today's date? (sysdate)
    if QRParameters.FShowOnlyActualIllness then
    begin
      SelectStr := SelectStr +
        ' AND T.EMPLOYEE_NUMBER IN ' + NL +
        ' ( ' + NL +
        '    SELECT ' + NL +
        '      T2.EMPLOYEE_NUMBER ' + NL +
        '    FROM ' + NL +
        '      EMPLOYEEAVAILABILITY T2 LEFT JOIN ABSENCEREASON AR2 ON ' + NL +
        '        T2.AVAILABLE_TIMEBLOCK_1 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '        T2.AVAILABLE_TIMEBLOCK_2 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '        T2.AVAILABLE_TIMEBLOCK_3 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '        T2.AVAILABLE_TIMEBLOCK_4 = AR2.ABSENCEREASON_CODE ' + NL +
        '      INNER JOIN EMPLOYEE E2 ON ' + NL +
        '        T2.EMPLOYEE_NUMBER = E2.EMPLOYEE_NUMBER ' + NL +
        '    WHERE ' + NL +
        '      E2.PLANT_CODE >= ''' +
        DoubleQuote(QRBaseParameters.FPlantFrom)  + '''' + NL +
        '      AND E2.PLANT_CODE <= '''+
        DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
        '      AND ' + NL;
      if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
        SelectStr := SelectStr +
          '  E2.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom +
          '  AND E2.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
          '  AND ' + NL;
      SelectStr := SelectStr +
        '  T2.EMPLOYEEAVAILABILITY_DATE >= TRUNC(SYSDATE) AND ' + NL +
        '  T2.EMPLOYEEAVAILABILITY_DATE < TRUNC(SYSDATE+1) AND ' + NL +
        '  AR2.ABSENCETYPE_CODE = :ILLNESS ' + NL +
        ' ) ';
    end;
    SelectStr := SelectStr +
      'GROUP BY ' + NL +
      '  T.EMPLOYEE_NUMBER, E.DESCRIPTION, E.PHONE_NUMBER, E.STARTDATE ' + NL +
      'ORDER BY ' + NL +
      '  E.DESCRIPTION';
    with qryEmpIllness do
    begin
      Close;
      SQL.Clear;
      SQL.Add(SelectStr);
      ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
      ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
      ParamByName('ILLNESS').AsString := ILLNESS;
      Open;
      Result := not Eof;
// SQL.SaveToFile('c:\temp\RepEmpIllness.sql');
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TReportEmployeeIllnessQR.ConfigReport;
begin
//  inherited; // Do not inherite
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    QRLblEmpFrom.Caption := QRBaseParameters.FEmployeeFrom;
    QRLblEmpTo.Caption := QRBaseParameters.FEmployeeTo;
  end
  else
  begin
    QRLblEmpFrom.Caption := '*';
    QRLblEmpTo.Caption := '*';
  end;
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);
  if QRParameters.FShowOnlyActualIllness then
    QRLblShowActiveIllness.Caption := ShowActiveIllnessCaption
  else
    QRLblShowActiveIllness.Caption := '';
end;

function TReportEmployeeIllnessQR.QRSendReportParameters(const PlantFrom,
  PlantTo, EmployeeFrom, EmployeeTo: String; const DateFrom,
  DateTo: TDateTime; const ShowSelection, ExportToFile,
  ShowOnlyActualIllness: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      ShowSelection,
      ExportToFile,
      ShowOnlyActualIllness);
  end;
  SetDataSetQueryReport(ReportEmployeeIllnessDM.qryEmpIllness);
end;

procedure TReportEmployeeIllnessQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  ShowActiveIllnessCaption := QRLblShowActiveIllness.Caption;
end;

procedure TReportEmployeeIllnessQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportEmployeeIllnessQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Show selections
    if QRParameters.FShowSelection then
    begin
      // Selections
      ExportClass.AddText(QRLabel11.Caption);
      // From Plant PlantCode to PlantCode
      ExportClass.AddText(QRLblFromPlant.Caption + ' ' +
        QRLblPlant.Caption + ' ' + QRLblPlantFrom.Caption + ' ' +
        QRLblToPlant.Caption + ' ' + QRLblPlantTo.Caption);
      // From Employee EmployeeNumber to EmployeeNumber
      ExportClass.AddText(QRLblFromEmp.Caption + ' ' +
        QRLblEmp.Caption + ' ' + QRLblEmpFrom.Caption + ' ' +
        QRLblToEmp.Caption + ' ' + QRLblEmpTo.Caption);
      // From Date to Date
      ExportClass.AddText(QRLblFromDate.Caption + ' ' +
        QRLblDate.Caption + ' ' + QRLblDateFrom.Caption + ' ' +
        QRLblToDate.Caption + ' ' + QRLblDateTo.Caption);
      // Show only active illness
      ExportClass.AddText(QRLblShowActiveIllness.Caption);
    end;

    // Show column names
    ExportDetail(
      QRLblEmployee.Caption,   // Employee number
      '',                      // Employee name
      QRLblPhone.Caption,      // Phone
      QRLblStartDate.Caption,  // Startdate
      QRLblTotal.Caption       // Total
      );
    ExportDetail(
      '',
      '',
      '',
      '',
      QRLblIllnessDays.Caption // illness days
      );
  end;
end;

procedure TReportEmployeeIllnessQR.QRBandDetailsAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  with ReportEmployeeIllnessDM do
  begin
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    begin
      ExportDetail(
        qryEmpIllness.FieldByName('EMPLOYEE_NUMBER').AsString, // Employee nr
        qryEmpIllness.FieldByName('DESCRIPTION').AsString,     // Employee name
        qryEmpIllness.FieldByName('PHONE_NUMBER').AsString,    // Phone
        DateToStr(qryEmpIllness.FieldByName('STARTDATE').AsDateTime), // Startdate
        qryEmpIllness.FieldByName('TOTAL').AsString            // Total
        );
    end;
  end;
end;

procedure TReportEmployeeIllnessQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

end.
