inherited MachineF: TMachineF
  Left = 372
  Top = 191
  Height = 413
  Caption = 'Machines'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
        end>
      KeyField = 'PLANT_CODE'
      DataSource = MachineDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnPLANT_CODE: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumnDESCRIPTION: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnCITY: TdxDBGridColumn
        Caption = 'City'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxMasterGridColumnPHONE: TdxDBGridColumn
        Caption = 'Phone'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 256
    Height = 118
    object lblCode: TLabel
      Left = 16
      Top = 13
      Width = 25
      Height = 13
      Caption = 'Code'
    end
    object lblDescription: TLabel
      Left = 16
      Top = 38
      Width = 53
      Height = 13
      Caption = 'Description'
    end
    object lblShortName: TLabel
      Left = 16
      Top = 62
      Width = 56
      Height = 13
      Caption = 'Short Name'
    end
    object lblDepartment: TLabel
      Left = 16
      Top = 88
      Width = 57
      Height = 13
      Caption = 'Department'
    end
    object DBEditCode: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 8
      Width = 105
      Style.BorderStyle = xbsSingle
      TabOrder = 0
      DataField = 'MACHINE_CODE'
      DataSource = MachineDM.DataSourceDetail
    end
    object DBEditDescription: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 34
      Width = 241
      Style.BorderStyle = xbsSingle
      TabOrder = 1
      DataField = 'DESCRIPTION'
      DataSource = MachineDM.DataSourceDetail
    end
    object btnJobCodes: TButton
      Left = 472
      Top = 35
      Width = 161
      Height = 25
      Caption = 'Job Codes'
      TabOrder = 5
      OnClick = btnJobCodesClick
    end
    object btnShowWorkspots: TButton
      Left = 472
      Top = 7
      Width = 161
      Height = 25
      Caption = 'Show Workspots'
      TabOrder = 4
      OnClick = btnShowWorkspotsClick
    end
    object DBEditShortName: TdxDBEdit
      Tag = 1
      Left = 96
      Top = 60
      Width = 153
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'SHORT_NAME'
      DataSource = MachineDM.DataSourceDetail
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 96
      Top = 88
      Width = 241
      Height = 19
      Ctl3D = False
      DataField = 'DEPARTMENTLU'
      DataSource = MachineDM.DataSourceDetail
      ListSource = MachineDM.DataSourceDepartment
      ParentCtl3D = False
      TabOrder = 3
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 101
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 97
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 96
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Machines'
        end>
      KeyField = 'MACHINE_CODE'
      DataSource = MachineDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridMACHINE_CODE: TdxDBGridMaskColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MACHINE_CODE'
      end
      object dxDetailGridDESCRIPTION: TdxDBGridMaskColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnSHORT_NAME: TdxDBGridColumn
        Caption = 'Short Name'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHORT_NAME'
      end
      object dxDetailGridColumnDEPARTMENT: TdxDBGridLookupColumn
        Caption = 'Department'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENTLU'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
