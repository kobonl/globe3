(*
  MRA:17-MAY-2010. RV064.1. Order 550478
  - Addition of machines.
  MRA:25-JUN-2012 Bugfix.
  - Field Max Deviation is cleared with no reason.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - Addition of field 'Ignore quantities in reports' (checkbox).
  MRA:18-OCT-2012 20013551 Info Tunnels
  - Addition of 2 fields to enter a 'minimum %':
    - ProdLevelMinPerc
    - MachOutputMinPerc
  MRA:20-NOV-2013 20013196
  - Multiple counters for workspot
    - Addition of link job-dialog.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:17-JUN-2014 20015178.70
  - Make default-job default functionality
  MRA:8-SEP-2014 20013476
  - Make CURRENT variable
  - Addition of Sample Time Minutes-field on Job-level.
  MRA:4-MAY-2015 SO-20014450.50
  - Real time efficiency
  - Make 'sample time' setting invisible
*)
unit JobCodeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEdLib, dxDBELib, dxEditor, dxExEdtr, DBCtrls,
  StdCtrls, Mask, dxDBTLCl, dxGrClms, dxDBEdtr;

type
  TJobCodeF = class(TGridBaseF)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEditJobCodes: TDBEdit;
    DBEditDescription: TDBEdit;
    DBEditInterfaceCode: TDBEdit;
    dxDBDateEditInactiveDate: TdxDBDateEdit;
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridLookupColumn;
    dxMasterGridColumn4: TdxDBGridLookupColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridLookupColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxMasterGridColumn5: TdxDBGridCheckColumn;
    dxMasterGridColumn6: TdxDBGridCheckColumn;
    dxMasterGridColumn7: TdxDBGridCheckColumn;
    dxMasterGridColumn8: TdxDBGridDateColumn;
    dxMasterGridColumn9: TdxDBGridCheckColumn;
    dxMasterGridColumn10: TdxDBGridCheckColumn;
    dxDetailGridColumn5: TdxDBGridColumn;
    dxDetailGridColumn6: TdxDBGridColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    dxMasterGridColumn11: TdxDBGridCheckColumn;
    dxMasterGridColumn12: TdxDBGridLookupColumn;
    DBRadioGroupQty: TDBRadioGroup;
    Label8: TLabel;
    DBEditPlant: TDBEdit;
    Label9: TLabel;
    DBEditWK: TDBEdit;
    DBEditPlantDesc: TDBEdit;
    DBEditWKDesc: TDBEdit;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dxDBMaskEdit1: TdxDBMaskEdit;
    dxDBMaskEdit2: TdxDBMaskEdit;
    dxDBMaskEdit3: TdxDBMaskEdit;
    DBCheckBoxIgnoreInterface: TDBCheckBox;
    dxDetailGridColumn8: TdxDBGridCheckColumn;
    ButtonShowForm: TButton;
    DBCheckBox1: TDBCheckBox;
    dxDetailGridColumn9: TdxDBGridDateColumn;
    GroupBoxCompareJob: TGroupBox;
    Label13: TLabel;
    DBCheckBoxCompJob: TDBCheckBox;
    DBEditPercentage: TDBEdit;
    ButtonCompareJob: TButton;
    DBRadioGroupRejectYN: TDBRadioGroup;
    DBCheckBox2: TDBCheckBox;
    DBLookupComboBoxBusinessUnit: TDBLookupComboBox;
    DBCheckBox3: TDBCheckBox;
    dxDBMaskEditProdLevelMinPerc: TdxDBMaskEdit;
    Label10: TLabel;
    dxDBMaskEditMachOutputMinPerc: TdxDBMaskEdit;
    GroupBoxLinkJob: TGroupBox;
    ButtonLinkJob: TButton;
    CheckBoxLinkJob: TCheckBox;
    DBCheckBoxDefaultJob: TDBCheckBox;
    dxDetailGridColumnDefaultYN: TdxDBGridCheckColumn;
    Label11: TLabel;
    dxDBMaskEdit4: TdxDBMaskEdit;
    Label12: TLabel;
    dxDetailGridColumnSampleTimeMins: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure DBCheckBoxIgnoreInterfaceClick(Sender: TObject);
    procedure ButtonShowFormClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure DBEditInterfaceCodeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
    procedure dxDetailGridClick(Sender: TObject);
    procedure DBCheckBoxCompJobClick(Sender: TObject);
    procedure ButtonCompareJobClick(Sender: TObject);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure ButtonLinkJobClick(Sender: TObject);
    procedure CheckBoxLinkJobClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    InsertForm: TForm;
    procedure DisableForJobsFromMachines;
    procedure LevelFieldsEnable;
    procedure EnableCheckBoxLinkJob; // 20013196
  public
    { Public declarations }
    FPlantCode,
    FWKCode: String;
    procedure DisableCheckBoxIgnoreInterface;
    procedure DisableComparionJob;
  end;

function JobCodeF: TJobCodeF;

var
  JobCodeF_HDN: TJobCodeF;
  
implementation

{$R *.DFM}

uses
  SystemDMT, WorkSpotDMT, UPimsMessageRes, UPimsConst, IgnoreInterfaceCodeFRM,
  CompareJobCodeFRM, WorkSpotFRM, DialogLinkJobFRM;

function JobCodeF: TJobCodeF;
begin
  if (JobCodeF_HDN = nil) then
    JobCodeF_HDN := TJobCodeF.Create(Application);
  Result := JobCodeF_HDN;
end;

procedure TJobCodeF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  if WorkspotDM.TableDetail.FieldByName('MACHINE_CODE').AsString <> '' then
  begin
    DisplayMessage(SPimsPleaseDefineJobForMachine, mtError, [mbOk]);
//  Abort;
  end;
  inherited;
  DBEditJobCodes.SetFocus;
  DisableCheckBoxIgnoreInterface;
  EnableCheckBoxLinkJob; // 20013196
  DBEditPercentage.Enabled := False;
end;

procedure TJobCodeF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  if DBEditJobCodes.Enabled then
    DBEditJobCodes.SetFocus
  else
    DBEditInterfaceCode.SetFocus;
end;

procedure TJobCodeF.FormDestroy(Sender: TObject);
begin
  inherited;
  JobCodeF_HDN := Nil;
end;

procedure TJobCodeF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
  if (dxDetailGrid.Items[0].Values[0] = NULL ) then
  begin
    DisplayMessage(SPimsJobCodeWorkSpot, mtError, [mbOk]);
    Abort;
  end;
  inherited;
  JobCodeF_HDN := Nil;

  WorkspotF.SetGridForm(WorkspotF);
end;

procedure TJobCodeF.dxBarBDBNavPostClick(Sender: TObject);
var
  RecCount: Integer;
  BookMark: TBookMark;
begin
  inherited;
//  if SystemDM.EnableMachineTimeRec then // 20015178.70
  WorkspotDM.MyRowsAffected := 0; // 20015178
  if not(WorkspotDM.TableJobCode.State in [dsInsert, dsEdit]) then
    WorkspotDM.TableJobCode.Edit;

  WorkspotDM.TableJobCode.Post;
// select all other interface code of other job, wk but the same plant
  WorkSpotDM.SelectOtherInterfaceCode(DBEditInterfaceCode.Text, RecCount);
// if check  update pe interface_code
//1. interface code delete from the ignoreinterface table
  if (DBCheckBoxIgnoreInterface.Checked ) then
  begin
    if WorkSpotDM.TableIgnoreTmp.FindKey([
      WorkSpotDM.TableJobCode.FieldByName('PLANT_CODE').Value,
      WorkSpotDM.TableJobCode.FieldByName('WORKSPOT_CODE').Value,
      WorkSpotDM.TableJobCode.FieldByName('JOB_CODE').Value,
      WorkSpotDM.TableJobCode.FieldByNAME('INTERFACE_CODE').AsString]) then
    begin
      WorkSpotDM.TableIgnoreTmp.Delete;
      if (RecCount >= 1) then
      begin
        InsertForm := IgnoreInterfaceCodeF;
        IgnoreInterfaceCodeF.FInterfaceDefined := DBEditInterfaceCode.Text;
        InsertForm.Show;
      end;
      exit;
    end;
  end;
  if (DBCheckBoxIgnoreInterface.Checked ) and (RecCount >= 1) then
    if (WorkspotDM.TableIgnoreInterface.RecordCount = 0) then
    begin
      InsertForm := IgnoreInterfaceCodeF;
      IgnoreInterfaceCodeF.FInterfaceDefined := DBEditInterfaceCode.Text;
      InsertForm.Show;
    end;
  // 20015178
//  if SystemDM.EnableMachineTimeRec then // 20015178.70
  if WorkspotDM.MyRowsAffected > 0 then
  begin
    BookMark := nil;
    try
      BookMark := WorkspotDM.TableJobCode.GetBookmark;
      WorkspotDM.TableJobCode.Close;
      WorkspotDM.TableJobCode.Open;
      WorkspotDM.TableJobCode.GotoBookMark(BookMark);
    finally
      if Assigned(BookMark) then
        WorkspotDM.TableJobCode.FreeBookMark(BookMark);
    end;
  end;
end;

procedure TJobCodeF.DBCheckBoxIgnoreInterfaceClick(Sender: TObject);
begin
  inherited;
  WorkspotDM.FIgnore := DBCheckBoxIgnoreInterface.Checked;
  ButtonShowForm.Enabled := DBCheckBoxIgnoreInterface.Checked;
end;

procedure TJobCodeF.ButtonShowFormClick(Sender: TObject);
var
  RecCount: Integer;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  WorkSpotDM.SelectOtherInterfaceCode(DBEditInterfaceCode.Text, RecCount);
  if RecCount = 0 then
  begin
    DisplayMessage(SPimsNoOtherInterfaceCodes, mtError, [mbOk]);
    Exit;
  end;
  InsertForm := IgnoreInterfaceCodeF;
  IgnoreInterfaceCodeF.FInterfaceDefined := DBEditInterfaceCode.Text;
  InsertForm.Show;
end;

procedure TJobCodeF.DisableCheckBoxIgnoreInterface;
begin
 if WorkSpotDM <> nil then
   if (WorkSpotDM.TableJobCode.FieldByName('INTERFACE_CODE').AsString = '') then
    begin
      DBCheckBoxIgnoreInterface.Enabled := False;
      DBCheckBoxIgnoreInterface.Checked := False;
      ButtonShowForm.Enabled := False;
    end
    else
    begin
      DBCheckBoxIgnoreInterface.Enabled := True;
      ButtonShowForm.Enabled := DBCheckBoxIgnoreInterface.Checked;
    end;
end;

procedure TJobCodeF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  if (WorkSpotDM.DataSourceJobCode.DataSet.State in [dsInsert]) then
    Exit;
  DisableCheckBoxIgnoreInterface;
  EnableCheckBoxLinkJob; // 20013196
end;

procedure TJobCodeF.DBEditInterfaceCodeKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
   if (DBEditInterfaceCode.Text = '') then
    begin
      DBCheckBoxIgnoreInterface.Enabled := False;
      DBCheckBoxIgnoreInterface.Checked := False;
      WorkspotDM.FIgnore := False;
      ButtonShowForm.Enabled := False;
    end
    else
    begin
      DBCheckBoxIgnoreInterface.Enabled := True;
      ButtonShowForm.Enabled := DBCheckBoxIgnoreInterface.Checked;
      WorkspotDM.FIgnore := DBCheckBoxIgnoreInterface.Checked;
    end;
end;

// 20013551
procedure TJobCodeF.LevelFieldsEnable;
begin
  if WorkspotDM.TableDetail.
    FieldByName('EFF_RUNNING_HRS_YN').AsString = 'Y' then
  begin
    dxDBMaskEdit1.Enabled := False; // Norm prod level
    dxDBMaskEdit2.Enabled := False; // Bonus level
    dxDBMaskEditProdLevelMinPerc.Enabled := False; // Norm Prod Level Min. Perc.
  end
  else
  begin
    dxDBMaskEdit1.Enabled := True; // Norm prod level
    dxDBMaskEdit2.Enabled := True; // Bonus level
    dxDBMaskEditProdLevelMinPerc.Enabled := True; // Norm Prod Level Min. Perc.
  end;
end;

procedure TJobCodeF.FormShow(Sender: TObject);
begin
  inherited;
  CheckBoxLinkJob.Checked := False; // 20013196
  ButtonLinkJob.Enabled := False; // 20013196
  DisableForJobsFromMachines;
  DisableCheckBoxIgnoreInterface;
  EnableCheckBoxLinkJob; // 20013196
  if WorkSpotDM.TableJobCode.RecordCount = 0 then
    DBCheckBoxCompJob.Checked := False;
  DisableComparionJob;

  // 20013551
  LevelFieldsEnable;

  // 20015178
//  if not SystemDM.EnableMachineTimeRec then // 20015178.70
//  DBCheckBoxDefaultJob.Visible := False;
end;

procedure TJobCodeF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  inherited;
  if WorkspotDM.MachineJobExists(
    WorkspotDM.TableDetail.FieldByName('PLANT_CODE').AsString,
    WorkspotDM.TableDetail.FieldByName('MACHINE_CODE').AsString,
    WorkspotDM.TableJobCode.FieldByName('JOB_CODE').AsString) then
  begin
    DisplayMessage(SPimsDeleteNotAllowedForMachineJob, mtError, [mbOK]);
    Abort;
  end;
  if WorkspotDM.CountRecordOfIgnoreInterface then
    if DisplayMessage(SDeleteOtherInterfaceCode, mtConfirmation, [mbYes, mbNo]) <> mrYes then
       Abort;
  WorkspotDM.DeleteOtherInterfaceCodes;
  WorkspotDM.TableJobCode.Delete;
end;

procedure TJobCodeF.dxDetailGridClick(Sender: TObject);
begin
//  inherited;

end;

procedure TJobCodeF.DisableComparionJob;
begin
 DBEditPercentage.Enabled := DBCheckBoxCompJob.Checked;
 ButtonCompareJob.Enabled := DBCheckBoxCompJob.Checked;
 DBRadioGroupRejectYN.Enabled := DBCheckBoxCompJob.Checked;
 // MRA:25-JUN-2012 Bugfix.
 // Why do this? This always clears the field!
// DBEditPercentage.Text := '';
 if not DBCheckBoxCompJob.Checked then
   DBEditPercentage.Color := clDisabled
 else
   DBEditPercentage.Color := clWindow;
end;

procedure TJobCodeF.DBCheckBoxCompJobClick(Sender: TObject);
begin
  inherited;
  DisableComparionJob;
  if not DBCheckBoxCompJob.Checked then
    DBRadioGroupRejectYN.ItemIndex := 0;
end;

procedure TJobCodeF.ButtonCompareJobClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Exit;
  end;
  InsertForm := CompareJobCodeSF;
  InsertForm.Show;
end;

procedure TJobCodeF.DisableForJobsFromMachines;
begin
{ if WorkspotDM.TableDetail.FieldByName('MACHINE_CODE').AsString <> '' then
 begin
   dxBarBDBNavInsert.Enabled := False;
 end; }
end;

procedure TJobCodeF.dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
  ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
  ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
  var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
  var ADone: Boolean);
var
  Exists: Boolean;
begin
  inherited;
  // RV064.1.
  // Only allow change for certain fields when job exists in machine-job-table.
  if ASelected or AFocused then
  begin
    Exists := WorkspotDM.MachineJobExists(
      WorkspotDM.TableDetail.FieldByName('PLANT_CODE').AsString,
      WorkspotDM.TableDetail.FieldByName('MACHINE_CODE').AsString,
      WorkspotDM.TableJobCode.FieldByName('JOB_CODE').AsString);
    DBEditJobCodes.Enabled := not Exists;
    DBEditDescription.Enabled := not Exists;

    // 20013551
    if Exists then
    begin
      dxDBMaskEdit1.Enabled := not Exists;
      dxDBMaskEdit2.Enabled := not Exists;
    end
    else
      LevelFieldsEnable;

    dxDBMaskEdit3.Enabled := not Exists;
    DBLookupComboBoxBusinessUnit.Enabled := not Exists;
    DBCheckBox1.Enabled := not Exists;
    DBCheckBox2.Enabled := not Exists;
    dxDBDateEditInactiveDate.Enabled := not Exists;
    dxBarButtonEditMode.Enabled := not Exists;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TJobCodeF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

// 20013196
procedure TJobCodeF.ButtonLinkJobClick(Sender: TObject);
begin
  inherited;
  try
    DialogLinkJobF := TDialogLinkJobF.Create(Self);
    DialogLinkJobF.PlantCode :=
      WorkSpotDM.TableJobCode.FieldByName('PLANT_CODE').AsString;
    DialogLinkJobF.WorkspotCode :=
      WorkSpotDM.TableJobCode.FieldByName('WORKSPOT_CODE').AsString;
    DialogLinkJobF.JobCode :=
      WorkSpotDM.TableJobCode.FieldByName('JOB_CODE').AsString;
    DialogLinkJobF.ShowModal;
  finally
    DialogLinkJobF.Free;
  end;
end;

// 20013196
procedure TJobCodeF.EnableCheckBoxLinkJob;
begin
  CheckBoxLinkJob.Checked :=
    WorkspotDM.LinkJobExists(
      WorkSpotDM.TableJobCode.FieldByName('PLANT_CODE').AsString,
      WorkSpotDM.TableJobCode.FieldByName('WORKSPOT_CODE').AsString,
      WorkSpotDM.TableJobCode.FieldByName('JOB_CODE').AsString
      );
end;

// 20013196
procedure TJobCodeF.CheckBoxLinkJobClick(Sender: TObject);
begin
  inherited;
  ButtonLinkJob.Enabled := CheckBoxLinkJob.Checked;
  if not CheckBoxLinkJob.Checked then
    if WorkspotDM.LinkJobExists(
      WorkSpotDM.TableJobCode.FieldByName('PLANT_CODE').AsString,
      WorkSpotDM.TableJobCode.FieldByName('WORKSPOT_CODE').AsString,
      WorkSpotDM.TableJobCode.FieldByName('JOB_CODE').AsString
      ) then
      if DisplayMessage(SPimsDeleteLinkJob,
        mtConfirmation, [mbYes, mbNo]) = mrYes then
      begin
        WorkspotDM.LinkJobDelete(
          WorkSpotDM.TableJobCode.FieldByName('PLANT_CODE').AsString,
          WorkSpotDM.TableJobCode.FieldByName('WORKSPOT_CODE').AsString,
          WorkSpotDM.TableJobCode.FieldByName('JOB_CODE').AsString
          );
      end
      else
      begin
        CheckBoxLinkJob.Checked := True;
      end;
end;

procedure TJobCodeF.FormCreate(Sender: TObject);
begin
  inherited;
  // 20015178
//  if not SystemDM.EnableMachineTimeRec then // 20015178.70
//  begin
//    dxDetailGridColumnDefaultYN.Visible := False;
//    dxDetailGridColumnDefaultYN.DisableCustomizing := True;
//  end;

  // 20014450.50 Sample time setting: Make invisible
  Label11.Visible := False;
  dxDBMaskEdit4.Visible := False;
  Label12.Visible := False;
  dxDetailGridColumnSampleTimeMins.Visible := False;
  dxDetailGridColumnSampleTimeMins.DisableCustomizing := True;
  dxDetailGridColumnSampleTimeMins.DisableGrouping := True;
  dxDetailGridColumnSampleTimeMins.DisableDragging := True;
  dxDetailGridColumnSampleTimeMins.DisableEditor := True;
  dxDetailGridColumnSampleTimeMins.DisableFilter := True;
end;

end.
