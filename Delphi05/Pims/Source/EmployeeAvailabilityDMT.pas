(*
  Changes: MR: 1-11-2004 Order 550349
    Plan employees across plants.
  MRA:27-JAN-2009 RV021.
    Team selection correction for QueryEmpl, to prevent multiple
    the same records in combobox.
  MRA:22-FEB-2010. RV054.7. 889962.
  - Sort on Description for popup-menu for Absence-reasons: QueryAbsReason.
  MRA:22-FEB-2010. RV054.10. 889957.
  - When adding new availability for a week, then:
    - when there is no shiftschedule, it does not fill in anything.
    - when there is no standard-availability, it does not fill in anything.
  - To solve it: First step is to show an error-message about this.
  MRA:31-MAR-2010. RV059.1. Bugfix.
  - When using 'Copy from week', when source-plant was empty, it still
    tried to copy this, resulting in an SQL-error-message.
  MRA:21-OCT-2010. RV073.6. Inactive employees.
  - Inactive employees are still shown in this dialog.
    Depending system date, the employee is shown or not.
    - Only when employee is active till system date.
    Changed: Query for QueryEmpl
  MRA:12-NOV-2010 RV079.4.
  - Balances
    - GlobalDM-routines are now used for Balance-information, because
      it is needed in several places.
  MRA:21-APR-2011. RV089.1.
  - Use other function for DisplayMessage with: Yes, No, All, NoToAll
  MRA:13-JUL-2011 RV094.8. Bugfix.
  - When Employee Availability Dialog is called from Employee Regs,
    it does not detect correct when an employee is not-active.
    As a result, it shows availability for the last employee!
  MRA:3-SEP-2013 TD-22511 Copy function gives problems.
  - When copying from a week where 1 day (or more) is not scheduled then:
    - Employee availability is not changed at all.
    - Planning is not deleted.
    - Shift Schedule has changed (which is OK), but is now different from
      availability that was not changed at all.
    Example:
    - Week 18: Shift Schedule: [-] [1] [1] [1] [1] (Mon-Fri)
    - Week 19: Shift Schedule: [1] [1] [1] [1] [1] (Mon-Fri)
    - Week 18: Emp Avail: [- **] [1 **] [1 **] [1 **] [1 **] (Mon-Fri)
    - Week 19: Emp Avail: [1 **] [1 **] [1 **] [1 **] [1 **] (Mon-Fri)
    - Planning Week 19: Monday: <planned on 1 workspot for 2 timeblocks>
    - A copy is made from week 18 to 19
    - Expected result:
      - Week 19:
        - Shift Schedule is now the same as week 18
        - Emp Avail is now the same as week 18
        - Planning is deleted for MO of week 19
  - Cause: It did not delete the employee availability-record.
           After that was done it also deleted the planning.
  - Also:
    When a shift was changed, it did not update emp. avail. and because
    of that also did not change planning.
    Cause: The new shift was not changed during update of emp. avail.
  - Added error trapping + database transaction.
  MRA:14-FEB-2014 20011800
  - Final Run System
  NRA:30-SEP-2015 PIM-53
  - Add Holiday functionality Rework:
    - It only worked when there was no availability before
    - We now use the existing copy-function with some changes
  MRA:20-OCT-2016 PIM-236
  - It did not save changes to the database when only the shift was changed
    for existing employee availability.
  MRA:9-FEB-2018 PIM-354
  - Memory leak in planning dialogs
  - The OnDestroy-event was not assigned!
  MRA:22-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)

unit EmployeeAvailabilityDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, EmployeeAvailabilityFRM, Provider, DBClient,
  UPimsConst;

const
  T0 = '00:00';
  NO_SHIFT_SCHEDULE_FOUND = -1;
  NO_STANDARD_AVAIL_FOUND = -2;

type

  TArrayStr = Array[1..7, 1..MAX_TBS+1] of String;
  TBArrayInt = Array[1..MAX_TBS] of Integer;
  TPlantArrayStr = Array[1..7] of String;

  TEmployeeAvailabilityDM = class(TGridBaseDM)
    QueryWork: TQuery;
    TableTempPivot: TTable;
    StoredProcEMA: TStoredProc;
    TablePivot: TTable;
    TablePivotPIVOTCOMPUTERNAME: TStringField;
    TablePivotEMPLOYEE_NR: TIntegerField;
    TablePivotYEAR_NR: TIntegerField;
    TablePivotWEEK_NR: TIntegerField;
    TablePivotDAY11: TStringField;
    TablePivotDAY12: TStringField;
    TablePivotDAY13: TStringField;
    TablePivotDAY14: TStringField;
    TablePivotDAY21: TStringField;
    TablePivotDAY22: TStringField;
    TablePivotDAY23: TStringField;
    TablePivotDAY24: TStringField;
    TablePivotDAY31: TStringField;
    TablePivotDAY32: TStringField;
    TablePivotDAY33: TStringField;
    TablePivotDAY34: TStringField;
    TablePivotDAY41: TStringField;
    TablePivotDAY42: TStringField;
    TablePivotDAY43: TStringField;
    TablePivotDAY44: TStringField;
    TablePivotDAY51: TStringField;
    TablePivotDAY52: TStringField;
    TablePivotDAY53: TStringField;
    TablePivotDAY54: TStringField;
    TablePivotDAY61: TStringField;
    TablePivotDAY62: TStringField;
    TablePivotDAY63: TStringField;
    TablePivotDAY64: TStringField;
    TablePivotDAY71: TStringField;
    TablePivotDAY72: TStringField;
    TablePivotDAY73: TStringField;
    TablePivotDAY74: TStringField;
    TablePivotDAY15: TStringField;
    TablePivotDAY25: TStringField;
    TablePivotDAY35: TStringField;
    TablePivotDAY45: TStringField;
    TablePivotDAY55: TStringField;
    TablePivotDAY65: TStringField;
    TablePivotDAY75: TStringField;
    QueryCheckIsPlanned: TQuery;
    qryShift: TQuery;
    QueryEmpl: TQuery;
    QueryIlnessMsg: TQuery;
    QuerySTA: TQuery;
    cdsAbsRsn: TClientDataSet;
    dspAbsRsn: TDataSetProvider;
    qryAbsRsn: TQuery;
    cdsShift: TClientDataSet;
    dspShift: TDataSetProvider;
    QuerySelect: TQuery;
    QuerySelectPlanning: TQuery;
    TablePivotPLANT1: TStringField;
    TablePivotPLANT2: TStringField;
    TablePivotPLANT3: TStringField;
    TablePivotPLANT4: TStringField;
    TablePivotPLANT5: TStringField;
    TablePivotPLANT6: TStringField;
    TablePivotPLANT7: TStringField;
    qryEMAInsert: TQuery;
    qryEMADelete: TQuery;
    qryEMAUpdate: TQuery;
    qryEMADeleteRange: TQuery;
    qryEMA: TQuery;
    qryPivotEMP: TQuery;
    qryEMAPlannedHours: TQuery;
    qrySHSFind: TQuery;
    qrySHSDelete: TQuery;
    qrySHSInsert: TQuery;
    qrySHSUpdate: TQuery;
    qryEMAFind: TQuery;
    qryEMAUpdate2: TQuery;
    qrySTA: TQuery;
    TablePivotDAY16: TStringField;
    TablePivotDAY17: TStringField;
    TablePivotDAY18: TStringField;
    TablePivotDAY19: TStringField;
    TablePivotDAY110: TStringField;
    TablePivotDAY111: TStringField;
    TablePivotDAY26: TStringField;
    TablePivotDAY27: TStringField;
    TablePivotDAY28: TStringField;
    TablePivotDAY29: TStringField;
    TablePivotDAY210: TStringField;
    TablePivotDAY211: TStringField;
    TablePivotDAY36: TStringField;
    TablePivotDAY37: TStringField;
    TablePivotDAY38: TStringField;
    TablePivotDAY39: TStringField;
    TablePivotDAY310: TStringField;
    TablePivotDAY311: TStringField;
    TablePivotDAY46: TStringField;
    TablePivotDAY47: TStringField;
    TablePivotDAY48: TStringField;
    TablePivotDAY49: TStringField;
    TablePivotDAY410: TStringField;
    TablePivotDAY411: TStringField;
    TablePivotDAY56: TStringField;
    TablePivotDAY57: TStringField;
    TablePivotDAY58: TStringField;
    TablePivotDAY59: TStringField;
    TablePivotDAY510: TStringField;
    TablePivotDAY511: TStringField;
    TablePivotDAY66: TStringField;
    TablePivotDAY67: TStringField;
    TablePivotDAY68: TStringField;
    TablePivotDAY69: TStringField;
    TablePivotDAY610: TStringField;
    TablePivotDAY611: TStringField;
    TablePivotDAY76: TStringField;
    TablePivotDAY77: TStringField;
    TablePivotDAY78: TStringField;
    TablePivotDAY79: TStringField;
    TablePivotDAY710: TStringField;
    TablePivotDAY711: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableEMAPIVOTAfterScroll(DataSet: TDataSet);
    procedure TableEMAPIVOTNewRecord(DataSet: TDataSet);
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure TableEMAPIVOTAfterDelete(DataSet: TDataSet);
  private
    { Private declarations }
    FPlanInOtherPlants: Boolean;
    FMyDate: TDateTime;
    FPasteSkipCount: Integer;
    procedure UpdateSHS(Empl: Integer; DateEMA: TDateTime;
      Plant: String; Shift:Integer);
    function AbsReason(DateEMA: TDateTime): String;
    function IsPlannedEMP(DeleteAction: Boolean;
      Empl, ShiftOld, ShiftNew, IndexDay: Integer; DateEMA: TDateTime;
      PlantOld, PlantNew: String; SaveValues: TArrayStr): Boolean;
    procedure UpdateEMPTable(Day, OldShift, NewShift, Empl: Integer;
      PlantOld, PlantNew: String; ValueToSave: TArrayStr);
    function SHSFind(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime): Boolean;
    procedure SHSDelete(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime);
    procedure SHSInsert(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime;
      APlantCode: String;
      AShiftNumber: Integer);
    procedure SHSUpdate(AEmployeeNumber: Integer;
      AShiftScheduleDate: TDateTime;
      APlantCode: String;
      AShiftNumber: Integer);
  public
    { Public declarations }

    FEmployee, FYear, FWeek: Integer;
    // FEMAARRAY - ARRAY with old values
    // FSTAArray - array with new values !!!!
    FEMAArray, FSTAArray, FSaveDayFrom: TArrayStr;
    FEmployeeFrom: Integer;
    FPlantFrom, FDeptFrom, FTablePivotName: String;
    FListTBPerEmpl: TStringList;
//car 550276 - array for saving the day changed by the user
    FDayChanged: Array[1..7] of Integer;
    // Order 550349 store plants for each day of one record.
    FSTAPlantArray: TPlantArrayStr; // actual values
    FEMAPlantArray: TPlantArrayStr; // old values
    FSaveDayPlantFrom: TPlantArrayStr; // saved values
    procedure FillPivotTable;
    function FillDefaultValues(Week: Integer): Integer;
    procedure UpdateSHS_EMA;
    function CheckEmplAlreadyPlanned(DeleteAction: Boolean): Boolean;
//Car 6-4-2004
    procedure UpdateEMP(DeleteAction: Boolean);
    procedure FillEMAArray;
    procedure CopySTA_EMA(AYear, AWeek: Integer);
    // RV079.4.
{    procedure CalculatePlannedHours(var PlannedHrs_Hol,
      PlannedHrs_TFT, PlannedHrs_WTR: Integer); }
    // RV079.4.
{    procedure ComputeHoliday(var RemainingH, NormalH, UsedH,
      RemainingTFT, UsedTFT, EarnedTFT, RemainingW, UsedW, EarnedW,
      PlannedH, PlannedTFT, PlannedW, IllnessMsg, IllnessMsgUntil,
      AvailableH,  AvailableTFT,  AvailableW: string); }
    function SetCopyFromValues(AEmployeeNumber,
      ASourceWeekNumber: Integer): Boolean;
    function PasteExecute(AEmployeeNumber,
      ASourceWeekNumber, ATargetWeekNumber: Integer): Boolean;
    function PasteOneDay(AEmployeeNumber, IndexDay, ATargetWeekNumber: Integer;
      var DlgMsgResult, QuestionResult: Integer; AEMAAdded: Boolean=False): Boolean; 
    procedure InsertPivotYear(TableTempPivot: TTable; Year, Week: Integer;
      DayStr: TArrayStr; PlantStr: TPlantArrayStr);
    procedure UpdateEMAPivot(Year, Week: Integer; Update, Delete: Boolean);
    function IsShift(Plant: String; Shift: Integer): Boolean;
    procedure ValidTB(Employee, Shift: Integer;
      Plant, Department: String; var TBValidSTR: String);

    function GetDateEMA(Year, Week, Day: Integer): TDateTime;
    procedure DeleteEMA;
    procedure GetOldEMAArray(Empl, Week, Year: Integer);
    //car 550276
    procedure InitializeDayChangedArray;
    function CopyFromOtherWeek(AEmployeeNumber,
      ASourceWeekNumber, ATargetWeekNumber: Integer): Boolean;
    function ValidTBForDay(const APlantCode,
      ADepartmentCode: String; const AShiftNumber, AEmployeeNumber: Integer;
      const ADate: TDateTime): Boolean;
    procedure OpenQueryEmpl;
    function FinalRunCheck(ACompareDate: TDateTime): Boolean; Overload;
    function FinalRunCheck(AYear, AWeek: Integer; ADay: Integer=1): Boolean; Overload;
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write FPlanInOtherPlants;
    property MyDate: TDateTime read FMyDate write FMyDate;
    property PasteSkipCount: Integer read FPasteSkipCount write FPasteSkipCount; // 20011800
  end;

var
  EmployeeAvailabilityDM: TEmployeeAvailabilityDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UGlobalFunctions, SystemDMT, UPimsMessageRes,
  CalculateTotalHoursDMT;

// RV079.4. Not needed anymore
(*
procedure TEmployeeAvailabilityDM.ComputeHoliday(
  var RemainingH, NormalH, UsedH,
  RemainingTFT, UsedTFT, EarnedTFT, RemainingW, UsedW, EarnedW,
  PlannedH, PlannedTFT, PlannedW, IllnessMsg, IllnessMsgUntil,
  AvailableH,  AvailableTFT,  AvailableW: string);
var
  Val_RemainingH, Val_NormalH, Val_UsedH, Val_RemainingTFT, Val_UsedTFT,
  Val_EarnedTFT, Val_RemainingW, Val_UsedW, Val_EarnedW: Integer;
  Val_PlannedH, Val_PlannedTFT, Val_PlannedW: Integer;
begin
  RemainingH := CalculateTotalHoursDM.DecodeHrsMin(0);
  NormalH := RemainingH; UsedH := RemainingH;
  PlannedH := RemainingH; AvailableH := RemainingH;
  RemainingTFT := RemainingH; UsedTFT := RemainingH; EarnedTFT := RemainingH;
  PlannedTFT := RemainingH; AvailableTFT := RemainingH;
  RemainingW := RemainingH; UsedW := RemainingH; EarnedW := RemainingH;
  PlannedW := RemainingH; AvailableW := RemainingH;
  IllnessMsg := RemainingH;  IllnessMsgUntil := IllnessMsg;
// ilness prev year
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  SUM(ILLNESS_MINUTE) TOT ' +
    'FROM ' +
    '  ABSENCETOTAL ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR<=:AYEAR');
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').AsInteger := FEmployee;
  QueryWork.ParamByName('AYEAR').asInteger := FYear;
  QueryWork.Active := True;
  if not QueryWork.IsEmpty then
    IllnessMsgUntil :=
      CalculateTotalHoursDM.DecodeHrsMin(
        Round(QueryWork.FieldByName('TOT').AsFloat));
  // Retreive all data from ABSENCETOTAL
  QueryWork.Active := False;
  QueryWork.SQL.Clear;
  QueryWork.SQL.Add(
    'SELECT ' +
    '  NORM_HOLIDAY_MINUTE,USED_HOLIDAY_MINUTE, ' +
    '  LAST_YEAR_HOLIDAY_MINUTE,LAST_YEAR_TFT_MINUTE, ' +
    '  EARNED_TFT_MINUTE, USED_TFT_MINUTE, LAST_YEAR_WTR_MINUTE, ' +
    '  EARNED_WTR_MINUTE, USED_WTR_MINUTE, ILLNESS_MINUTE ' +
    'FROM ' +
    '  ABSENCETOTAL ' +
    'WHERE ' +
    '  EMPLOYEE_NUMBER=:EMPNO AND ABSENCE_YEAR=:AYEAR');
  QueryWork.Prepare;
  QueryWork.ParamByName('EMPNO').asInteger := FEmployee;
  QueryWork.ParamByName('AYEAR').asInteger := FYear;
  QueryWork.Active := True;
  Val_RemainingH := 0; Val_NormalH := 0; Val_RemainingTFT := 0; Val_EarnedTFT:=0;
  Val_UsedTFT := 0; Val_RemainingW := 0; Val_EarnedW := 0; Val_UsedW := 0;
  IllnessMSG := ''; Val_UsedH := 0;
  if not QueryWork.IsEmpty then
  begin
    Val_RemainingH :=
      Round(QueryWork.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').asFloat);
    RemainingH := CalculateTotalHoursDM.DecodeHrsMin(Val_RemainingH);
    Val_NormalH := Round(QueryWork.FieldByName('NORM_HOLIDAY_MINUTE').asFloat);
    NormalH := CalculateTotalHoursDM.DecodeHrsMin(Val_NormalH);
    Val_UsedH := Round(QueryWork.FieldByName('USED_HOLIDAY_MINUTE').asFloat);
    UsedH := CalculateTotalHoursDM.DecodeHrsMin(Val_UsedH);
    Val_RemainingTFT :=
      Round(QueryWork.FieldByName('LAST_YEAR_TFT_MINUTE').asFloat);
    RemainingTFT := CalculateTotalHoursDM.DecodeHrsMin(Val_RemainingTFT);
    Val_EarnedTFT := Round(QueryWork.FieldByName('EARNED_TFT_MINUTE').asFloat);
    EarnedTFT := CalculateTotalHoursDM.DecodeHrsMin(Val_EarnedTFT);
    Val_UsedTFT := Round(QueryWork.FieldByName('USED_TFT_MINUTE').asFloat);
    UsedTFT := CalculateTotalHoursDM.DecodeHrsMin(Val_UsedTFT);
    Val_RemainingW :=
      Round(QueryWork.FieldByName('LAST_YEAR_WTR_MINUTE').asFloat);
    RemainingW := CalculateTotalHoursDM.DecodeHrsMin(Val_RemainingW);
    Val_EarnedW := Round(QueryWork.FieldByName('EARNED_WTR_MINUTE').asFloat);
    EarnedW := CalculateTotalHoursDM.DecodeHrsMin(Val_EarnedW);
    Val_UsedW := Round(QueryWork.FieldByName('USED_WTR_MINUTE').asFloat);
    UsedW := CalculateTotalHoursDM.DecodeHrsMin(Val_UsedW);
    IllnessMsg := CalculateTotalHoursDM.DecodeHrsMin(
      Round(QueryWork.FieldByName('ILLNESS_MINUTE').asFloat));
  end;
  // Compute Planned
  CalculatePlannedHours(Val_PlannedH, Val_PlannedTFT, Val_PlannedW);
  PlannedH :=  CalculateTotalHoursDM.DecodeHrsMin(Val_PlannedH);
  PlannedTFT := CalculateTotalHoursDM.DecodeHrsMin(Val_PlannedTFT);
  PlannedW := CalculateTotalHoursDM.DecodeHrsMin(Val_PlannedW);
  AvailableH := CalculateTotalHoursDM.DecodeHrsMin(
    Val_RemainingH + Val_NormalH - Val_UsedH - Val_PlannedH);
  AvailableTFT := CalculateTotalHoursDM.DecodeHrsMin(
    Val_RemainingTFT + Val_EarnedTFT - Val_UsedTFT - Val_PlannedTFT);
  AvailableW := CalculateTotalHoursDM.DecodeHrsMin(
    Val_RemainingW + Val_EarnedW - Val_UsedW - Val_PlannedW);
end;
*)

// RV079.4. Not needed anymore
(*
procedure TEmployeeAvailabilityDM.CalculatePlannedHours(var PlannedHrs_Hol,
  PlannedHrs_TFT, PlannedHrs_WTR: Integer);
var
  Plant, PrevPlant, Dept: String;
  AbsRsn, AbsType: Array[1..4] of String;
  PrevShift, Shift, FDay, IndexTB: Integer;
  CalculateHrs: Boolean;
  Planned_Hol, Planned_TFT, Planned_WTR: Real;
  CurrentDate: TDateTime;
begin
  // Pims -> Oracle
  // MR:17-03-2005 Rewritten to improve speed.
  // Also: PrevPlant is used (PlanInOtherPlants).
  Plant := QueryEmpl.FieldByName('PLANT_CODE').AsString;
  Dept := QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString;
  Planned_Hol := 0;
  Planned_TFT := 0;
  Planned_WTR := 0;
  CurrentDate := Trunc(Now);
  qryEMAPlannedHours.Close;
  qryEMAPlannedHours.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
    FEmployee;
  qryEMAPlannedHours.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime :=
    CurrentDate;
  qryEMAPlannedHours.Open;
  if not qryEMAPlannedHours.IsEmpty then
  begin
    PrevShift := 0;
    PrevPlant := '';
    qryEMAPlannedHours.First;
    while not qryEMAPlannedHours.Eof do
    begin
      Plant := qryEMAPlannedHours.FieldByName('PLANT_CODE').AsString;
      Shift := qryEMAPlannedHours.FieldByName('SHIFT_NUMBER').AsInteger;
      CalculateHrs := False;
      for IndexTB := 1 to 4 do
      begin
        AbsRsn[IndexTB] :=
          qryEMAPlannedHours.FieldByName(
            Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString;
        AbsType[IndexTB] := '';
        // MR:02-01-2003 Use Locate because of key-change for
        // ABSENCEREASON-table
        if (AbsRsn[IndexTB] <> '*') and (AbsRsn[IndexTB] <> '') and
          (AbsRsn[IndexTB] <> '-') then
          if cdsAbsRsn.Locate('ABSENCEREASON_CODE',
            VarArrayOf([AbsRsn[IndexTB]]), []) then
            AbsType[IndexTB] :=
              cdsAbsRsn.FieldByName('ABSENCETYPE_CODE').AsString;
        if not CalculateHrs then
          if (AbsType[IndexTB] = HOLIDAYAVAILABLETB) or
             (AbsType[IndexTB] = TIMEFORTIMEAVAILABILITY) or
             (AbsType[IndexTB] = WORKTIMEREDUCTIONAVAILABILITY) then
            CalculateHrs := True;
      end;
      if CalculateHrs then
      begin
        FDay :=
          ListProcsF.DayWStartOnFromDate(
            qryEMAPlannedHours.
              FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime);
        if (PrevShift <> Shift) or (PrevPlant <> Plant) then
          ATBLengthClass.FillTBLength(FEmployee, Shift, Plant, Dept, True);
        for IndexTB := 1 to 4 do
        begin
          if (AbsType[IndexTB] = HOLIDAYAVAILABLETB) then
            Planned_Hol := Planned_Hol +
              ATBLengthClass.GetLengthTB(IndexTB, FDay);
          if (AbsType[IndexTB] = TIMEFORTIMEAVAILABILITY) then
            Planned_TFT := Planned_TFT +
             ATBLengthClass.GetLengthTB(IndexTB, FDay);
          if (AbsType[IndexTB] = WORKTIMEREDUCTIONAVAILABILITY) then
            Planned_WTR := Planned_WTR +
              ATBLengthClass.GetLengthTB(IndexTB, FDay);
        end;
      end;
      PrevPlant := Plant;
      PrevShift := Shift;
      qryEMAPlannedHours.Next;
    end;
  end;
  qryEMAPlannedHours.Close;
  PlannedHRS_WTR := Round(Planned_WTR);
  PlannedHRS_HOL := Round(Planned_HOL);
  PlannedHRS_TFT := Round(Planned_TFT);
end;
*)

procedure TEmployeeAvailabilityDM.DataModuleCreate(Sender: TObject);
begin
  if not Assigned(Self.OnDestroy) then
    Self.OnDestroy := EmployeeAvailabilityDM.DataModuleDestroy;
  FTablePivotName := TMPEMPLOYEEAVAILABILITY;
  ExecSql(QueryWork,'DELETE FROM ' + FTablePivotName +
  ' WHERE PIVOTCOMPUTERNAME = ''' +  SystemDM.CurrentComputerName + '''');
  FListTBPerEmpl.Free;
  FListTBPerEmpl := TStringList.Create;
  cdsShift.Open;
  StoredProcEMA.Prepare;
  QueryCheckIsPlanned.Prepare;
  QueryIlnessMsg.Prepare;
  QuerySTA.Prepare;
  QuerySelectPlanning.Prepare;
  cdsAbsRsn.Open;
end;

procedure TEmployeeAvailabilityDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  ExecSql(QueryWork,'DELETE FROM ' + FTablePivotName +
   ' WHERE PIVOTCOMPUTERNAME = ''' +  SystemDM.CurrentComputerName + '''' );
  FListTBPerEmpl.Free;

  //car 15-7-2003
  cdsShift.Close;
  QueryEmpl.Close;
  QueryCheckIsPlanned.Close;
  QueryCheckIsPlanned.Unprepare;
  //  21-8-2003
  QueryIlnessMsg.Close;
  QueryIlnessMsg.Unprepare;
  QuerySTA.Close;
  QuerySTA.UnPrepare;
  cdsAbsRsn.Close;
  QuerySelectPlanning.Close;
  QuerySelectPlanning.UnPrepare;

  StoredProcEMA.Close;
  StoredProcEMA.UnPrepare;
end;

procedure TEmployeeAvailabilityDM.FillPivotTable;
var
  Plant: String;
  LastWeek: Word;
  StartDate, EndDate: TDateTime;
begin
// empty table EMAPIVOT
  FEmployee := QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if (FEmployee = 0) or (FYear = 0 ) then
    Exit;

  TablePivot.Filter := '';
  TablePivot.First;
  TablePivot.FindNearest([SystemDM.CurrentComputerName, FEmployee, FYear, 1]);
  if {(not PlanInOtherPlants) and}
    (TablePivot.FieldByName('PIVOTCOMPUTERNAME').AsString =
    SystemDM.CurrentComputerName) and
    (TablePivot.FieldByName('EMPLOYEE_NR').AsInteger = FEmployee) and
    (TablePivot.FieldByName('YEAR_NR').AsInteger = FYear) then
  begin
    TablePivot.Filter := 'PIVOTCOMPUTERNAME = ''' +
      SystemDM.CurrentComputerName + ''' AND YEAR_NR =' + IntToStr(FYear) +
      ' AND EMPLOYEE_NR = ' + IntToStr(FEmployee);
    Exit;
  end;

  StartDate := GetDate(ListProcsF.DateFromWeek(FYear, 1 , 1));
  LastWeek := ListProcsF.WeeksInYear(FYear);
  EndDate := GetDate(ListProcsF.DateFromWeek(FYear, LastWeek ,7));
  // Always get data for all plants! Independent of 'PlanInOtherPlants'
  Plant := '*'; //Oracle
  // Empty the table, otherwise it will be filled with duplicate values.
  TablePivot.Active := False;
  TablePivot.EmptyTable;
  TablePivot.Active := True;

 // StoredProcEMA.UnPrepare;
  StoredProcEMA.ParamByName('PIVOTCOMPUTERNAME').AsString :=
    SystemDM.CurrentComputerName;
  StoredProcEMA.ParamByName('PLANT_CODE').AsString := Plant;
  StoredProcEMA.ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
  StoredProcEMA.ParamByName('DEPARTMENT_CODE').AsString :=
    QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString;
  StoredProcEMA.ParamByName('YEAR_NR').AsInteger := FYear;
  StoredProcEMA.ParamByName('DATEMIN').AsDateTime := StartDate;
  StoredProcEMA.ParamByName('DATEMAX').AsDateTime := EndDate;
 // StoredProcEMA.Prepare;
  StoredProcEMA.ExecProc;

  if TablePivot.State in [dsEdit, dsInsert] then
    TablePivot.Post;

  TablePivot.Filter := 'PIVOTCOMPUTERNAME = ''' +
    DoubleQuote(SystemDM.CurrentComputerName) +
    ''' AND YEAR_NR =' + IntToStr(FYear) +
    ' AND EMPLOYEE_NR = ' + IntToStr(FEmployee);
end;

procedure TEmployeeAvailabilityDM.InsertPivotYear(TableTempPivot: TTable;
  Year, Week: Integer;
  DayStr: TArrayStr; PlantStr: TPlantArrayStr);
var
  IndexDay, IndexTB: Integer;
begin
  if Week = 0 then
    Exit;
  if not TableTempPivot.FindKey([SystemDM.CurrentComputerName, FEmployee,
    Year, Week]) then
    TableTempPivot.Insert
  else
    TableTempPivot.Edit;
  TableTempPivot.FieldByName('PIVOTCOMPUTERNAME').AsString :=
    SystemDM.CurrentComputerName;
  TableTempPivot.FieldByName('EMPLOYEE_NR').AsInteger := FEMPLOYEE;
  TableTempPivot.FieldByName('YEAR_NR').AsInteger := YEAR;
  TableTempPivot.FieldByName('WEEK_NR').AsInteger := Week;
  for IndexDay := 1 to 7 do
  begin
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      TableTempPivot.FieldByName(
        Format('DAY%d%d', [IndexDay, IndexTB])).AsString :=
          DayStr[IndexDay, IndexTB];
    TableTempPivot.FieldByName(
      Format('DAY%d%d', [IndexDay, ISHIFT])).AsString := DayStr[IndexDay, ISHIFT];
    // Always do this, independent of 'PlanInOtherPlants'
    TableTempPivot.FieldByName(
      Format('PLANT%d', [IndexDay])).AsString :=
        PlantStr[IndexDay];
  end;
  TableTempPivot.Post;
end;

procedure TEmployeeAvailabilityDM.TableEMAPIVOTAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  if (EmployeeAvailabilityDM <> Nil) then
  begin
    if not TablePivot.Active then
      Exit;
    if TablePivot.Filter = '' then
      Exit;
    //CAR 7-16-2003  - refresh the detail panel
    EmployeeAvailabilityF_HND.FillEditDetail;
  end;
end;

 // if it is planned one TB   - CAR 15-7-2003
function TEmployeeAvailabilityDM.IsPlannedEMP(DeleteAction: Boolean;
  Empl, ShiftOld, ShiftNew, IndexDay: Integer;
  DateEMA: TDateTime; PlantOld, PlantNew: String;
  SaveValues: TArrayStr): Boolean;
var
  PlannedShift, IndexTB: Integer;
  function CheckIfDeletePerTB(IndexTB: Integer): Boolean;
  var
    SelectStr: String;
  begin
    Result := False;
    if ShiftNew = -1 then
    //delete shift planned
    begin
      if DeleteAction then
      begin
        if ShiftOld = 0 then
        begin
          SelectStr :=
            'SELECT ' +
            Format('AVAILABLE_TIMEBLOCK_%d ', [IndexTB]) +
            'FROM ' +
            '  STANDARDAVAILABILITY ' +
            'WHERE ' +
            '  EMPLOYEE_NUMBER = :EMPL AND ' +
            '  SHIFT_NUMBER = :SHIFT AND ' +
            '  DAY_OF_WEEK = :DAY AND ' +
            Format('AVAILABLE_TIMEBLOCK_%d ', [IndexTB]) + ' = ''*''';
          QuerySelect.Close;
          QuerySelect.Sql.Clear;
          QuerySelect.Sql.Add(SelectStr);
          QuerySelect.ParamByName('EMPL').AsInteger := Empl;
          QuerySelect.ParamByName('SHIFT').AsInteger := PlannedShift;
          QuerySelect.ParamByName('DAY').AsInteger := IndexDay;
          QuerySelect.Open;
          Result := QuerySelect.IsEmpty;
          Exit;
        end;
      end;
      Result := True;
      Exit;
    end;
    if (ShiftNew <> PlannedShift) and (ShiftNew <> 0) then
    begin
      Result := True;
      Exit;
    end;
    if (ShiftNew = PlannedShift) and (ShiftNew <> 0) then
    begin
      if SaveValues[IndexDay, IndexTB] <> '*' then
        Result := True;
      Exit;
    end;
    if (ShiftNew = 0) then
    begin
      if SaveValues[IndexDay, IndexTB] <> '*' then
      begin
        Result := True;
        Exit;
      end;
      // if savevalues is available check also stand avail
      SelectStr :=
        'SELECT ' +
        Format('AVAILABLE_TIMEBLOCK_%d ', [IndexTB]) +
        'FROM ' +
        '  STANDARDAVAILABILITY ' +
        'WHERE ' +
        '  EMPLOYEE_NUMBER = :EMPL AND ' +
        '  SHIFT_NUMBER = :SHIFT AND ' +
        '  DAY_OF_WEEK = :DAY AND ' +
        Format('AVAILABLE_TIMEBLOCK_%d ', [IndexTB]) + ' = ''*''';
      QuerySelect.Close;
      QuerySelect.Sql.Clear;
      QuerySelect.Sql.Add(SelectStr);
      QuerySelect.ParamByName('EMPL').AsInteger := Empl;
      QuerySelect.ParamByName('SHIFT').AsInteger := PlannedShift;
      QuerySelect.ParamByName('DAY').AsInteger := IndexDay;
      QuerySelect.Open;
      Result := QuerySelect.IsEmpty;
    end;
  end;
begin
  Result := False;
  DateEMA := GetDate(DateEMA);

  QueryCheckIsPlanned.Active := False;
  QueryCheckIsPlanned.ParamByName('PCODE').asString := PlantOld;
  QueryCheckIsPlanned.ParamByName('PDATE').asDateTime := DateEMA;
  QueryCheckIsPlanned.ParamByName('EMPNO').asInteger := Empl;
  QueryCheckIsPlanned.Active := True;
  while (not QueryCheckIsPlanned.Eof)  do
  begin
    if ( (ShiftOld > 0) and
         (QueryCheckIsPlanned.FieldByName('SHIFT_NUMBER').AsInteger =
          ShiftOld) ) or
      (ShiftOld <= 0) then
    begin
      PlannedShift :=
        QueryCheckIsPlanned.FieldByName('SHIFT_NUMBER').AsInteger;
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
        if ((QueryCheckIsPlanned.FieldByName(
          Format('SCHEDULED_TIMEBLOCK_%d', [IndexTB])).AsString = 'A') or
          (QueryCheckIsPlanned.FieldByName(
          Format('SCHEDULED_TIMEBLOCK_%d', [IndexTB])).AsString = 'B') or
          (QueryCheckIsPlanned.FieldByName(
          Format('SCHEDULED_TIMEBLOCK_%d', [IndexTB])).AsString = 'C')) then
        begin
          Result := CheckIfDeletePerTB(IndexTB);
          if Result then
            Exit;
        end;
    end;
    QueryCheckIsPlanned.Next;
  end;
end;

// IF one of TB was planned
function TEmployeeAvailabilityDM.CheckEmplAlreadyPlanned(
  DeleteAction: Boolean): Boolean;
var
  PlantCodeOld, PlantCodeNew: String;
  IndexDay, ShiftNrOld, ShiftNrNew: Integer;
  DateEMA: TDateTime;
begin
  Result := False;
  for IndexDay := 1 to 7 do
  begin
    DateEMA := GetDateEMA(FYear, TablePivot.FieldByName('WEEK_NR').AsInteger,
      IndexDay);
    // Always do this, independent of 'PlanInOtherPlants'
    PlantCodeOld := FEMAPlantArray[IndexDay];
    PlantCodeNew := FSTAPlantArray[IndexDay];
    if (FEMAArray[IndexDay, ISHIFT] = '-') or (FEMAArray[IndexDay, ISHIFT] = '') then
      ShiftNrOld := -1
    else
      ShiftNrOld :=  StrToInt(FEMAArray[IndexDay, ISHIFT]);

    if ((FSTAArray[IndexDay, ISHIFT] = '-') or (FSTAArray[IndexDay, ISHIFT] = '')) then
      ShiftNrNew := -1
    else
      ShiftNrNew :=  StrToInt(FSTAArray[IndexDay, ISHIFT]);
    if IsPlannedEMP(DeleteAction, FEmployee, ShiftNrOld,  ShiftNrNew,
      IndexDay, DateEMA, PlantCodeOld, PlantCodeNew, FSTAArray) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;

// Update Employee Planning
procedure TEmployeeAvailabilityDM.UpdateEMP(DeleteAction: Boolean);
var
  IndexDay, ShiftNew, ShiftOld: Integer;
  DateEMA: TDateTime;
  PlantCodeOld, PlantCodeNew: String;
begin
  for IndexDay := 1 to 7 do
  begin
  //car 550276
    if (FSTAArray[IndexDay, ISHIFT] = '-') or (FSTAArray[IndexDay, ISHIFT] = '') then
      ShiftNew := -1
    else
      ShiftNew := StrToInt(FSTAArray[IndexDay, ISHIFT]);
    if (FEMAArray[IndexDay, ISHIFT] = '-') or (FEMAArray[IndexDay, ISHIFT] = '') then
      ShiftOld := -1
    else
      ShiftOld := StrToInt(FEMAArray[IndexDay, ISHIFT]);
    DateEMA := GetDateEMA(FYear,
      TablePivot.FieldByName('WEEK_NR').AsInteger, IndexDay);
    PlantCodeOld := FEMAPlantArray[IndexDay];
    PlantCodeNew := FSTAPlantArray[IndexDay];
    //Car 4-6-2004
    if IsPlannedEMP(DeleteAction, FEmployee, ShiftOld,  ShiftNew,
      IndexDay, DateEMA, PlantCodeOld, PlantCodeNew, FSTAArray) then
      UpdateEMPTable(IndexDay, ShiftOld, ShiftNew, FEmployee,
        PlantCodeOld, PlantCodeNew, FSTAArray);
  end;
end;

// PIM-53 Related to this order
// This creates an extra pivot-record (in PivotEmpAvailable-table), when
// availability was entered for last week of the year, resulting in 1
// record for the next year. Why is this done?
// Example: Week 52 is the last week for year 2015.
// When this is added as availability, it creates an extra line for the next
// year.
procedure TEmployeeAvailabilityDM.UpdateEMAPivot(Year, Week: Integer;
  Update, Delete: Boolean);
begin
// PIM-53 Related to this order:  < to <=  in this if ???
  if (Week <> 1) and (Week <= ListProcsF.WeeksInYear(Year)) then
    Exit;
  if TableTempPivot.TableName = '' then
  begin
    TableTempPivot.TableName := FTablePivotName;
    TableTempPivot.Open;
  end
  else
    TableTempPivot.Refresh;
  if (Week = 1) then
  begin
    Year := Year - 1;
    Week := ListProcsF.WeeksInYear(Year);
    if Update then
      InsertPivotYear(TableTempPivot, Year, Week, FEMAArray, FEMAPlantArray);
    if Delete then
      if TableTempPivot.FindKey([FEmployee, Year, Week]) then
        TableTempPivot.Delete;
  end;
  if (Week >= ListProcsF.WeeksInYear(Year)) then
  begin
    Year := Year + 1;
    Week := 1;
    if Update then
      InsertPivotYear(TableTempPivot, Year, Week,  FSTAArray, FSTAPlantArray);
    if Delete then
      if TableTempPivot.FindKey([FEmployee, Year, Week]) then
        TableTempPivot.Delete;
   end;
end;

// Update Employee Planning Table
procedure TEmployeeAvailabilityDM.UpdateEMPTable(Day, OldShift, NewShift,
  Empl: Integer; PlantOld, PlantNew: String; ValueToSave: TArrayStr);
var
  IndexTB, PlannedShift: Integer;
  DateEMA: TDateTime;
  Temp,  TBAvailability: String;
  CountNotAvailable: Integer;
begin
  DateEMA := GetDate(ListProcsF.DateFromWeek(FYear,
    TablePivot.FieldByName('WEEK_NR').AsInteger, Day));
  if OldShift <= 0 then
  begin
    QuerySelect.Close;
    QuerySelect.SQL.Clear;
    QuerySelect.SQL.Add(
      'SELECT ' +
      '  DISTINCT SHIFT_NUMBER ' +
      'FROM ' +
      '  EMPLOYEEPLANNING ' +
      'WHERE ' +
      '  EMPLOYEEPLANNING_DATE = :EDATE AND ' +
      '  EMPLOYEE_NUMBER = :EMPNO');
    QuerySelect.Prepare;
    // Always search for all-plants, independent of PlanInOtherPlants
//    QuerySelect.ParamByName('PCODE').asString := '';
    QuerySelect.ParamByName('EDATE').asDateTime := DateEMA;
    QuerySelect.ParamByName('EMPNO').asInteger := Empl;
    QuerySelect.Open;
  end
  else
  begin
    QuerySelect.Close;
    QuerySelect.SQL.Clear;
    QuerySelect.SQL.Add(
      'SELECT ' +
      '  DISTINCT SHIFT_NUMBER ' +
      'FROM ' +
      '  EMPLOYEEPLANNING ' +
      'WHERE ' +
      '  EMPLOYEEPLANNING_DATE = :EDATE AND ' +
      '  EMPLOYEE_NUMBER = :EMPNO AND ' +
      '  SHIFT_NUMBER = :SNO');
    QuerySelect.Prepare;
    QuerySelect.ParamByName('EDATE').asDateTime := DateEMA;
    QuerySelect.ParamByName('EMPNO').asInteger := Empl;
    QuerySelect.ParamByName('SNO').AsInteger := OldShift;
    QuerySelect.Open;
  end;
  while not QuerySelect.Eof do
  begin
    PlannedShift := QuerySelect.FieldByName('SHIFT_NUMBER').AsInteger;

    QuerySelectPlanning.Close;
    QuerySelectPlanning.ParamByName('EMPNO').AsInteger := Empl;
    QuerySelectPlanning.ParamByName('DAY').AsInteger := Day;
    // Always search for all plants, independent of PlanInOtherPlants
    // Pims -> Oracle: Use '*' instead of empty string!
    QuerySelectPlanning.ParamByName('PCODE').AsString := '*';
    QuerySelectPlanning.ParamByName('SNO').AsInteger := PlannedShift;
    QuerySelectPlanning.ParamByName('EDATE').AsDateTime := DateEMA;
    QuerySelectPlanning.Open;
    Temp := NullStr;
    CountNotAvailable := 0;
    if not QuerySelectPlanning.IsEmpty then
    begin
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
      begin
        if QuerySelectPlanning.FieldByName('RECNO').AsInteger = 1 then
          TBAvailability :=
            QuerySelectPlanning.FieldByName(
              Format('EMA_TB%d', [IndexTB])).AsString;
        if QuerySelectPlanning.FieldByName('RECNO').AsInteger = 2 then
          TBAvailability :=
            QuerySelectPlanning.FieldByName(
              Format('STD_TB%d', [IndexTB])).AsString;
        if QuerySelectPlanning.FieldByName('RECNO').AsInteger = 3 then
        begin
          TBAvailability :=
            QuerySelectPlanning.FieldByName(
              Format('STD_TB%d', [IndexTB])).AsString;
          if TBAvailability = '*' then
            TBAvailability :=
              QuerySelectPlanning.FieldByName(
                Format('EMA_TB%d', [IndexTB])).AsString;
        end;
        if TBAvailability <> '*' then
        begin
          Temp := Temp + Format('SCHEDULED_TIMEBLOCK_%d=''N'',', [IndexTB]);
          CountNotAvailable := CountNotAvailable + 1;
        end;
      end;
    end;
    if (CountNotAvailable = 0) or (CountNotAvailable = 4) then
    begin
      // Always on all plants, independent of PlanInOtherPlants
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'DELETE FROM EMPLOYEEPLANNING ' +
        'WHERE ' +
        'EMPLOYEEPLANNING_DATE=:EDATE AND ' +
        'SHIFT_NUMBER = :SNO AND ' +
        'EMPLOYEE_NUMBER = :EMPNO');
      QueryWork.Prepare;
      QueryWork.ParamByName('EDATE').asDateTime := DateEMA;
      QueryWork.ParamByName('SNO').asInteger := PlannedShift;
      QueryWork.ParamByName('EMPNO').asInteger := Empl;
      QueryWork.ExecSQL;
    end
    else
    begin
      // Always on all plants, independent of PlanInOtherPlants
      Temp := Copy(Temp, 1, Length(Temp)-1);
      QueryWork.Active := False;
      QueryWork.SQL.Clear;
      QueryWork.SQL.Add(
        'UPDATE EMPLOYEEPLANNING SET ' +
        Temp + ' ' +
        ',MUTATOR = :MUTATOR, MUTATIONDATE = :MUTATIONDATE ' +
        'WHERE ' +
        'EMPLOYEEPLANNING_DATE = :EDATE AND ' +
        'SHIFT_NUMBER = :SHNO AND ' +
        'EMPLOYEE_NUMBER = :EMPNO');
      QueryWork.Prepare;
      QueryWork.ParamByName('EDATE').asDateTime := DateEMA;
      QueryWork.ParamByName('SHNO').asInteger := PlannedShift;
      QueryWork.ParamByName('EMPNO').asInteger := Empl;
      QueryWork.ParamByName('MUTATOR').asString :=
        SystemDM.CurrentProgramUser;
      QueryWork.ParamByName('MUTATIONDATE').asDateTime := Now;
      QueryWork.ExecSQL;
    end;
    QuerySelect.Next;
  end;//while
end;

procedure TEmployeeAvailabilityDM.GetOldEMAArray(Empl, Week, Year: Integer);
var
  IndexDay, IndexTB: Integer;
begin
  qryPivotEMP.Close;
  qryPivotEMP.ParamByName('EMPLOYEE_NR').AsInteger := Empl;
  qryPivotEMP.ParamByName('YEAR_NR').AsInteger := Year;
  qryPivotEMP.ParamByName('WEEK_NR').AsInteger := Week;
  qryPivotEMP.ParamByName('PIVOTCOMPUTERNAME').AsString :=
    SystemDM.CurrentComputerName;
  qryPivotEMP.Prepare;
  qryPivotEMP.Open;
  if not qryPivotEMP.IsEmpty then
  begin
    for IndexDay := 1 to 7 do
    begin
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
        FEMAArray[IndexDay, IndexTB] := qryPivotEMP.FieldByName(
          Format('DAY%d%d', [IndexDay, IndexTB])).AsString;
      FEMAArray[IndexDay, ISHIFT] := qryPivotEMP.FieldByName(
        Format('DAY%d%d', [IndexDay, ISHIFT])).AsString;
      // Always do this, independent of PlanInOtherPlants
      FEMAPlantArray[IndexDay] := qryPivotEMP.FieldByName(
        Format('PLANT%d', [IndexDay])).AsString
    end;
  end;
  qryPivotEMP.Close;
end;

procedure TEmployeeAvailabilityDM.FillEMAArray;
var
  IndexDay, IndexTB: Integer;
begin
  for IndexDay := 1 to 7 do
  begin
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      FEMAArray[IndexDay, IndexTB] :=
        TablePivot.FieldByName(
          Format('DAY%d%d', [IndexDay, IndexTB])).AsString;
    FEMAArray[IndexDay, ISHIFT] :=
      TablePivot.FieldByName(
        Format('DAY%d%d', [IndexDay, ISHIFT])).AsString;
    FEMAPlantArray[IndexDay] :=
      TablePivot.FieldByName(Format('PLANT%d', [IndexDay])).AsString;
  end;
end;

procedure TEmployeeAvailabilityDM.ValidTB(Employee, Shift: Integer;
  Plant, Department: String; var TBValidStr: String);
var
  IndexList: Integer;
begin
  IndexList := FListTBPerEmpl.IndexOf(IntToStr(Employee) + CHR_SEP +
    IntToStr(Shift) + CHR_SEP + Plant + CHR_SEP + Department);
  if (IndexList >= 0)  and ((IndexList + 1) < FListTBPerEmpl.Count) then
  begin
    TBValidStr := FListTBPerEmpl.Strings[IndexList + 1];
    Exit;
  end
  else
    if IndexList >= 0 then
    begin
      TBValidStr := '';
      exit;
    end;
  TBValidStr := '';
  FListTBPerEmpl.Add(IntToStr(Employee) + CHR_SEP +
    IntToStr(Shift) + CHR_SEP + Plant + CHR_SEP + Department);
  // CAR 21-7-2003
  ATBLengthClass.ValidTB(Employee, Shift, Plant, Department);
  for IndexList := 1 to SystemDM.MaxTimeblocks do
  // CAR 21-7-2003
    if ATBLengthClass.ExistTB(IndexList) then
      TBValidStr := TBValidStr + IntToStr(IndexList);
  FListTBPerEmpl.ADD(TBValidStr);
end;

function TEmployeeAvailabilityDM.IsShift(Plant: String; Shift: Integer): Boolean;
begin
 //CAR 14-7-2003
  Result := cdsShift.Locate('PLANT_CODE;SHIFT_NUMBER',
    VarArrayOf([Plant, Shift]), []);
end;

// fill value default in array FSTAArray
// fill with '!' on valid TB with no value
// fill with '-' for shift if does not exist and with number if it is
// RV054.10.
function TEmployeeAvailabilityDM.FillDefaultValues(Week: Integer): Integer;
var
  IndexDay, IndexTB, Shift: Integer;
  DateEMA: TDateTime;
  Department, Plant, TBValidSTR: String;
  ShiftScheduleErrorCount: Integer;
  StandStaffAvailErrorCount: Integer;
begin
  Result := 0;
  ShiftScheduleErrorCount := 0;
  StandStaffAvailErrorCount := 0;
  for IndexDay := 1 to 7 do
  begin
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      FSTAArray[IndexDay, IndexTB] := '';
    FSTAArray[IndexDay, ISHIFT] := '';
    FSTAPlantArray[IndexDay] := '';
  end;
//CAR 21-8-2003
//  QuerySTA.Close;
  // MR:1-12-2004 Do this for each day!
//  QuerySTA.ParamByName('PLANT_CODE').AsString :=
//    QueryEmpl.FieldByName('PLANT_CODE').AsString;
//  QuerySTA.ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
  for IndexDay := 1 to 7 do
  begin
    DateEMA := GetDateEMA(FYear, Week, IndexDay);
    if SHSFind(FEmployee, DateEMA) then
    begin
      Plant := qrySHSFind.FieldByName('PLANT_CODE').AsString;
      Shift := qrySHSFind.FieldByName('SHIFT_NUMBER').AsInteger;
      Department := QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString;
      if Shift = -1 then
        FSTAArray[IndexDay, ISHIFT] := '-'
      else
        if Shift >= 0 then
        begin
          FSTAArray[IndexDay, ISHIFT] := IntToStr(Shift);
          FSTAPlantArray[IndexDay] := Plant;
        end;
      if Shift > -1 then
      begin
        QuerySTA.Close;
        // MR:1-12-2004 Search for 'Plant' that comes from 'ShiftSchedule'!
        QuerySTA.ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
        QuerySTA.ParamByName('PLANT_CODE').AsString := Plant;
        QuerySTA.ParamByName('SHIFT_NUMBER').AsInteger := Shift;
        QuerySTA.ParamByName('DAY_OF_WEEK').AsInteger := IndexDay;
        QuerySTA.Open;
        if not QuerySTA.IsEmpty then
        begin
          ValidTB(FEmployee, Shift, Plant, Department, TBValidSTR);
          for IndexTB := 1 to SystemDM.MaxTimeblocks do
            if (Pos(IntToStr(IndexTB), TBValidStr) > 0 ) then
            begin
              if QuerySTA.FieldByName(
                Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString = '*' then
              begin
                FSTAArray[IndexDay, IndexTB] := AbsReason(DateEma);
                if FSTAArray[IndexDay, IndexTB] = '' then
                  FSTAArray[IndexDay, IndexTB] := '*';
              end
              else
                FSTAArray[IndexDay, IndexTB] :=
                  QuerySTA.FieldByName(
                    Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString;
             //is valid but still not filled then fill with '!'
              if FSTAArray[IndexDay, IndexTB] = '' then
                 FSTAArray[IndexDay, IndexTB] := '!';
            end;
        end
        else
          inc(StandStaffAvailErrorCount);
      end// if shift >= 0
      else
        inc(ShiftScheduleErrorCount);
    end // if SHSFind
    else
      inc(ShiftScheduleErrorCount);
  end; //for day
  // RV054.10.
  if ShiftScheduleErrorCount >= 7 then
    Result := NO_SHIFT_SCHEDULE_FOUND
  else
    if StandStaffAvailErrorCount >= 7 then
      Result := NO_STANDARD_AVAIL_FOUND;
end;

//CAR 21-8-2003
function TEmployeeAvailabilityDM.AbsReason(DateEMA: TDateTime): String;
begin
  Result := '';
  QueryIlnessMsg.Close;
  QueryIlnessMsg.ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
  QueryIlnessMsg.ParamByName('CURRENTDATE').AsDateTime := DateEMA;
  QueryIlnessMsg.Open;
  Result := QueryIlnessMsg.FieldByName('ILL').AsString;
end;

procedure TEmployeeAvailabilityDM.TableEMAPIVOTNewRecord(
  DataSet: TDataSet);
begin
  inherited;
  TablePivot.FieldByName('PIVOTCOMPUTERNAME').AsString :=
    SystemDM.CurrentComputerName;
  TablePivot.FieldByName('WEEK_NR').AsInteger := FWeek; // 1; // PIM-53 Why is this 1 ??? Changed to FWeek
  TablePivot.FieldByName('YEAR_NR').AsInteger := FYEAR;
  TablePivot.FieldByName('EMPLOYEE_NR').AsInteger := FEmployee;
end;

function TEmployeeAvailabilityDM.GetDateEMA(Year, Week,
  Day: Integer): TDateTime;
begin
  Result := ListProcsF.DateFromWeek(Year, Week, Day);
end;

// Update ShiftSchedule + Employee Availability
procedure TEmployeeAvailabilityDM.UpdateSHS_EMA;
var
  Week, IndexDay: Integer;
  DateEMA: TDateTime;
  PlantCodeOld, PlantCodeNew, ShiftNr: String;
  // Update Employee Availability
  procedure UpdateEMATable(DateEMA: TDateTime; PlantCodeOld, PlantCodeNew,
    ShiftNr: String);
  var
    IndexTB: Integer;
    ShiftFilled, PlantChanged, BlocksFilled, {BlocksChanged,}
    RecordExists: Boolean;
  begin
    // MR:16-03-2005 Rewritten to improve speed.
    // Also: a delete with a query is faster than 'table.delete'.
    qryEMA.Close;
    qryEMA.ParamByName('PLANT_CODE').AsString := PlantCodeOld;
    qryEMA.ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
    qryEMA.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
    qryEMA.Open;
    RecordExists := not qryEMA.IsEmpty;
    ShiftFilled := not ((ShiftNr = '-') or (ShiftNr = ''));
    PlantChanged := False;
    if RecordExists then
      PlantChanged :=  (PlantCodeOld <> PlantCodeNew);
    BlocksFilled := False;
    if ShiftFilled then
    begin
      // Are Timeblocks changed and filled?
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
      begin
        if (TablePIVOT.FieldByName(
          Format('DAY%d%d', [IndexDay, IndexTB])).AsString <> '') and
           (TablePIVOT.FieldByName(
             Format('DAY%d%d', [IndexDay, IndexTB])).AsString <> ' ') then
          BlocksFilled := True;
      end;
    end;
    if RecordExists then
    begin
      // Shift is empty.
      if not ShiftFilled then
      begin
        // Delete record.
        with qryEMADelete do
        begin
          ParamByName('PLANT_CODE').AsString := PlantCodeOld;
          ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
          ExecSQL;
        end;
      end
      else
      begin
        // Shift is filled.
        // Blocks are filled {and changed}. // PIM-236
        // PIM-236 Also do this when shift has changed!
        if PlantChanged or (BlocksFilled{ and BlocksChanged}) then // PIM-236
        begin
          // edit record.
          with qryEMAUpdate do
          begin
            ParamByName('NEW_PLANT_CODE').AsString := PlantCodeNew;
            ParamByName('NEW_SHIFT_NUMBER').AsInteger := StrToInt(ShiftNr);
            for IndexTB := 1 to MAX_TBS do
              ParamByName('NEW_AVAILABLE_TIMEBLOCK_'+IntToStr(IndexTB)).AsString :=
                TablePIVOT.FieldByName(Format('DAY%d%d', [IndexDay, IndexTB])).AsString;
            ParamByName('NEW_MUTATIONDATE').AsDateTime := Now;
            ParamByName('NEW_MUTATOR').AsString := SystemDM.CurrentProgramUser;
            ParamByName('PLANT_CODE').AsString := PlantCodeOld;
            ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
            ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
            ExecSQL;
          end;
        end;
      end;
    end
    else
    begin
      // Shift and blocks are filled.
      if ShiftFilled and BlocksFilled then
      begin
        // Insert record.
        with qryEMAInsert do
        begin
          ParamByName('PLANT_CODE').AsString := PlantCodeNew;
          ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
          ParamByName('SHIFT_NUMBER').AsInteger := StrToInt(ShiftNr);
          ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
          for IndexTB := 1 to MAX_TBS do
            ParamByName('AVAILABLE_TIMEBLOCK_'+IntToStr(IndexTB)).AsString :=
              TablePIVOT.FieldByName(Format('DAY%d%d', [IndexDay, IndexTB])).AsString;
          ParamByName('CREATIONDATE').AsDateTime := Now;
          ParamByName('MUTATIONDATE').AsDateTime := Now;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end;
      end;
    end;
  end;
begin
  Week := TablePivot.FieldByName('WEEK_NR').AsInteger;
  for IndexDay := 1 to 7 do
  begin
    DateEMA := GetDateEMA(FYear, Week, IndexDay);
    PlantCodeNew :=
      TablePivot.FieldByName(Format('PLANT%d', [IndexDay])).AsString;
    PlantCodeOld := FEMAPlantArray[IndexDay];
    ShiftNr := TablePivot.FieldByName(Format('DAY%d%d', [IndexDay, ISHIFT])).AsString;
    if (PlantCodeNew <> '') then
      UpdateEMATable(DateEMA, PlantCodeOld, PlantCodeNew, ShiftNr);
    if ShiftNr = '-' then
      ShiftNr := '-1';
    // Pims -> Oracle, don't allow empty plant!
    if (PlantCodeNew <> '') and (ShiftNr <> '') then
      UpdateSHS(FEmployee, DateEMA, PlantCodeNew, StrToInt(ShiftNr))
    else
    begin
      // MR:16-03-2005 Delete the Shift Schedule record with a TQuery,
      // this is faster then 'TableSHS.Delete'.
      SHSDelete(FEmployee, DateEMA);
    end;
  end;
end; // UpdateSHS_EMA

procedure TEmployeeAvailabilityDM.DefaultBeforeDelete(DataSet: TDataSet);
var
  IndexDay, IndexTB: Integer;
begin
  inherited;
  FillEMAArray;
  for IndexDay := 1 to 7 do
  begin
    for IndexTB := 1 to SystemDM.MaxTimeblocks do
      FSTAArray[IndexDay, IndexTB] := '';
    FSTAArray[IndexDay, ISHIFT] := '-';
    FSTAPlantArray[IndexDay] := '';
  end;
  FWeek := TablePivot.FieldByName('WEEK_NR').AsInteger;
  if CheckEmplAlreadyPlanned(True) then
    if DisplayMessage(SEmployeePlanned,
      mtConfirmation, [mbYes, mbNo]) = mrYes then
    begin
      DeleteEMA;
      UpdateEMP(True);
    end
    else
      Abort;
end;

// Delete Employee Availability
procedure TEmployeeAvailabilityDM.DeleteEMA;
begin
  inherited;
  // Always delete records for all plants! Independent of PlanInOtherPlants
  with qryEMADeleteRange do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
    ParamByName('DATEMIN').AsDateTime := GetDateEMA(FYear, FWeek, 1);
    ParamByName('DATEMAX').AsDateTime := GetDateEMA(FYear, FWeek, 7);
    ExecSql;
  end;
end;

procedure TEmployeeAvailabilityDM.TableEMAPIVOTAfterDelete(
  DataSet: TDataSet);
begin
  inherited;
  DeleteEMA;
  UpdateEMAPivot(FYear, FWeek, False, True);
end;

procedure TEmployeeAvailabilityDM.CopySTA_EMA(AYear, AWeek: Integer);
var
  IndexDay, IndexTB: Integer;
  CheckPlanning: Boolean;
  SkipCount: Integer; // 20011800
begin
  SkipCount := 0; // 20011800
  if (TablePivot.RecordCount = 0) then
    Exit;
  CheckPlanning := False;
  FillEMAArray;
  FillDefaultValues(AWeek);
  if CheckEmplAlreadyPlanned(False) then
    if DisplayMessage(SEmployeePlanned, mtConfirmation,
      [mbYes, mbNo]) = mrYes then
      CheckPlanning := True
    else
      Exit;
  TablePivot.Edit;
  for IndexDay := 1 to 7 do
  begin
    // 20011800
    if FinalRunCheck(AYear, AWeek, IndexDay) then
    begin
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
      begin
        if (FSTAArray[IndexDay, IndexTB] <> '') and
          (FSTAArray[IndexDay, IndexTB] <> '!') then
          TablePivot.FieldByName(
            Format('DAY%d%d', [IndexDay, IndexTB])).Value :=
              FSTAArray[IndexDay, IndexTB];
      end;
      if (FSTAArray[IndexDay, ISHIFT] <> '') and
        (FSTAArray[IndexDay, ISHIFT] <> '!') then
        TablePivot.FieldByName(
          Format('DAY%d%d', [IndexDay, ISHIFT])).Value :=
            FSTAArray[IndexDay, ISHIFT];
      TablePivot.FieldByName(
        Format('PLANT%d', [IndexDay])).AsString :=
          FSTAPlantArray[IndexDay];
    end
    else
      inc(SkipCount);
  end;
  TablePivot.Post;
  UpdateSHS_EMA;
  if CheckPlanning then
    UpdateEMP(False);
  if SkipCount > 0 then // 20011800
    DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]);
end;

// Update ShiftSchedule Table
procedure  TEmployeeAvailabilityDM.UpdateSHS(Empl: Integer;
  DateEMA: TDateTime; Plant: String; Shift: Integer);
var
  PrevShift: Integer;
  PrevPlant: String;
begin
  if SHSFind(Empl, DateEMA) then
  begin
    PrevPlant := qrySHSFind.FieldByName('PLANT_CODE').AsString;
    PrevShift := qrySHSFind.FieldByName('SHIFT_NUMBER').AsInteger;
    if (PrevPlant <> Plant) or (PrevShift <> Shift) then
      SHSUpdate(Empl, DateEMA, Plant, Shift);
  end
  else
    SHSInsert(Empl, DateEMA, Plant, Shift);
end;

// MR:24-11-2004 Disabled, because it didn't work correct and
// was not used in practice.
// TD-22511 This IS used via 'Copy from other week'-button.
function TEmployeeAvailabilityDM.SetCopyFromValues(AEmployeeNumber,
  ASourceWeekNumber: Integer): Boolean;
var
  IndexDay, IndexTB: Integer;
  OldWeekNumber: Integer;
begin
  Result := True;
  if (AEmployeeNumber = 0) or (ASourceWeekNumber = 0) then
    Result := False
  else
  begin
    FEmployeeFrom := AEmployeeNumber;
    FPlantFrom := QueryEmpl.FieldByName('PLANT_CODE').AsString;
    FDeptFrom := QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString;
    OldWeekNumber := TablePivot.FieldByName('WEEK_NR').AsInteger;
    if TablePivot.Locate('WEEK_NR', ASourceWeekNumber, []) then
    begin
      for IndexDay := 1 to 7 do
      begin
        for IndexTB := 1 to SystemDM.MaxTimeblocks do
          FSaveDayFrom[IndexDay, IndexTB] :=
            TablePivot.FieldByName(
              Format('DAY%d%d', [IndexDay, IndexTB])).AsString;
        FSaveDayFrom[IndexDay, ISHIFT] :=
          TablePivot.FieldByName(
            Format('DAY%d%d', [IndexDay, ISHIFT])).AsString;
        FSaveDayPlantFrom[IndexDay] :=
          TablePivot.FieldByName(
            Format('PLANT%d', [IndexDay])).AsString;
      end;
    end
    else
      Result := False;
    TablePivot.Locate('WEEK_NR', OldWeekNumber, []);
  end;
end;

// PIM-53
// - Changed so it can work with Add Holiday/Absence-functionality
// - This originally only worked when there was existing employee availability
//   to copy to. Now it will also insert it when it does not exist.
function TEmployeeAvailabilityDM.PasteOneDay(AEmployeeNumber,
  IndexDay, ATargetWeekNumber: Integer;
  var DlgMsgResult, QuestionResult: Integer;
  AEMAAdded: Boolean=False): Boolean;
var
  DateEMA: TDateTime;
  IndexTB: Integer;
  NewShiftNr, OldShiftNr: Integer;
  CheckPlanning, EMAFound: Boolean;
  PlantCodeOld, PlantCodeNew: String;
begin
  Result := False;
  // 20011800
  if not FinalRunCheck(FYear, ATargetWeekNumber, IndexDay) then
  begin
    PasteSkipCount := PasteSkipCount + 1;
    Exit;
  end;

  if TablePivot.FieldByName(
    Format('DAY%d%d', [IndexDay, ISHIFT])).AsString = '' then
      OldShiftNr := -2
  else
    if TablePivot.FieldByName(
      Format('DAY%d%d', [IndexDay, ISHIFT])).AsString = '-' then
        OldShiftNr := -1
    else
      OldShiftNr := TablePivot.FieldByName(
        Format('DAY%d%d', [IndexDay, ISHIFT])).AsInteger;

  if FSaveDayFrom[IndexDay, ISHIFT] = '' then
    NewShiftNr := -2
  else
    if FSaveDayFrom[IndexDay, ISHIFT] = '-' then
      NewShiftNr := -1
    else
      NewShiftNr := StrToInt(FSaveDayFrom[IndexDay, ISHIFT]);

  PlantCodeOld :=
    TablePivot.FieldByName(
      Format('PLANT%d', [IndexDay])).AsString;
  PlantCodeNew := FSaveDayPlantFrom[IndexDay];

  if (NewShiftNr <> -2) or (OldShiftNr <> -2) then
  begin
    DateEMA := GetDateEMA(FYear, ATargetWeekNumber, IndexDay);
    if OldShiftNr <> -2 then
      if (DlgMsgResult <> mrAll) and (DlgMsgResult <> mrNoToAll) then
      begin
        if AEMAAdded then
          DlgMsgResult := mrYes
        else
          DlgMsgResult :=
            DisplayMessage4Buttons(
              SEMAOverwrite + IntToStr(FEmployeeFrom)+ ', ' +
              DateToStr(DateEMA) + ' '+ SEMAExists);
      end;
    CheckPlanning := True;
    if (DlgMsgResult = mrAll) or (DlgMsgResult = mrYes) or
      (DlgMsgResult = 0) then
    begin
      if IsPlannedEMP(False, FEmployeeFrom,
        OldShiftNr, NewShiftNr, IndexDay, DateEMA,
        PlantCodeOld, PlantCodeNew, FSaveDayFrom) then
      begin
        if (QuestionResult = 0) then
          QuestionResult :=
            DisplayMessage(SEmployeePlanned, mtConfirmation, [mbYes, mbNo]);
        if (QuestionResult = mrNo) then
          CheckPlanning := False;
      end;
    end;
    if CheckPlanning then
    begin
      // MR:20-1-2006 Use query instead of TableEMA
      with qryEMAFind do
      begin
        Close;
        ParamByName('PLANT_CODE').AsString := PlantCodeOld;
        ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
        ParamByName('SHIFT_NUMBER').AsInteger := OldShiftNr;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
        Open;
        EMAFound := not IsEmpty;
      end;
      if EMAFound then
      begin
      //car 550276 - copy over
        if ((DlgMsgResult = mrAll) or (DlgMsgResult = mrYes)) then
        begin
          Result := True;
          // update EMA table
          // MR:20-1-2006 Use Query instead of TableEMA
          // RV059.1.
          if PlantCodeNew <> '' then
          begin
            with qryEMAUpdate2 do
            begin
              // TD-22511 It did not change the shift!
              ParamByName('PLANT_CODE').AsString := PlantCodeOld;
              ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
              ParamByName('SHIFT_NUMBER').AsInteger := OldShiftNr;
              ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
              ParamByName('NEW_PLANT_CODE').AsString := PlantCodeNew;
              ParamByName('NEW_SHIFT_NUMBER').AsInteger := NewShiftNr; // TD-22511
              for IndexTB := 1 to MAX_TBS do
                ParamByName('AVAILABLE_TIMEBLOCK_'+IntToStr(IndexTB)).AsString :=
                  FSaveDayFrom[IndexDay, IndexTB];
              ParamByName('MUTATIONDATE').AsDateTime := Now;
              ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
              ExecSQL;
            end;
          end
          else
          begin
            // TD-22511
            // PlantCodeNew was empty (but old not),
            // meaning: delete EMA-record (Emp. Avail. record)
            // When this is deleted, it will also delete the planning
            // later in this procedure.
            if PlantCodeOld <> '' then
            begin
              with qryEMADelete do
              begin
                ParamByName('PLANT_CODE').AsString := PlantCodeOld;
                ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
                ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
                ExecSQL;
              end;
            end;
          end;
          // Pims -> Oracle, don't allow empty Plant!
          if (PlantCodeNew <> '') then
            UpdateSHS(FEmployee, DateEMA, PlantCodeNew, NewShiftNr);
          if IsPlannedEMP(False, FEmployeeFrom,
            OldShiftNr, NewShiftNr, IndexDay, DateEMA,
            PlantCodeOld, PlantCodeNew, FSaveDayFrom) then
            UpdateEMPTable(IndexDay, OldShiftNr, NewShiftNr, FEmployee,
              PlantCodeOld, PlantCodeNew, FSaveDayFrom);
          TablePivot.Edit;
          for IndexTB := 1 to SystemDM.MaxTimeblocks do
            TablePivot.FieldByName(
              Format('DAY%d%d', [IndexDay, IndexTB])).AsString :=
                FSaveDayFrom[IndexDay, IndexTB];
          TablePivot.FieldByName(
            Format('DAY%d%d', [IndexDay, ISHIFT])).AsString :=
              FSaveDayFrom[IndexDay, ISHIFT];
          TablePivot.FieldByName(
            Format('PLANT%d', [IndexDay])).AsString :=
              PlantCodeNew;
          TablePivot.Post;
        end;
      end // if EMAFound
      else
      begin
        // Insert EMA here! PIM-53
        with qryEMAInsert do
        begin
          ParamByName('PLANT_CODE').AsString := PlantCodeNew;
          ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := DateEMA;
          ParamByName('SHIFT_NUMBER').AsInteger := NewShiftNr;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := FEmployee;
          for IndexTB := 1 to MAX_TBS do
            ParamByName('AVAILABLE_TIMEBLOCK_'+IntToStr(IndexTB)).AsString :=
              FSaveDayFrom[IndexDay, IndexTB];
          ParamByName('CREATIONDATE').AsDateTime := Now;
          ParamByName('MUTATIONDATE').AsDateTime := Now;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end; // with
        // ALso Insert/Update Shift Schedule
        if (PlantCodeNew <> '') then
          UpdateSHS(FEmployee, DateEMA, PlantCodeNew, NewShiftNr);
        // update EMAPIVOT table  - old shift is not in EMA table
        if (DlgMsgResult = mrAll) or (DlgMsgResult = mrYes) or
          (DlgMsgResult = 0) then // PIM-53 also check on 0
        begin
          Result := True;
          TablePivot.Edit;
          for IndexTB := 1 to SystemDM.MaxTimeblocks do
            TablePivot.FieldByName(
              Format('DAY%d%d', [IndexDay, IndexTB])).AsString :=
                FSaveDayFrom[IndexDay, IndexTB];
          TablePivot.FieldByName(
            Format('DAY%d%d', [IndexDay, ISHIFT])).AsString :=
              FSaveDayFrom[IndexDay, ISHIFT];
          TablePivot.FieldByName(
            Format('PLANT%d', [IndexDay])).AsString :=
              PlantCodeNew;
          TablePivot.Post;
        end;
      end;
    end;  // if CheckPlanning  then
  end;  // copy over OldShiftNr
end; // PasteOneDay

// MR:24-11-2004 Disabled, because it didn't work correct and
// was not used in practice.
// TD-22511 This IS used via 'Copy from other week'-button.
function TEmployeeAvailabilityDM.PasteExecute(AEmployeeNumber,
  ASourceWeekNumber, ATargetWeekNumber: Integer): Boolean;
var
  IndexDay: Integer;
  DlgMsgResult, QuestionResult: Integer;
begin
  Result := False;
  if (AEmployeeNumber = 0) or (ASourceWeekNumber = 0) or
    (ATargetWeekNumber = 0) then
    Exit;
  if (ASourceWeekNumber = ATargetWeekNumber) then
  begin
    DisplayMessage(SCopyTo, mtInformation, [mbOk]);
    Exit;
  end;
  DlgMsgResult := 0;
  QuestionResult := 0;
  // TD-22511 Added error-trapping
  try
    SystemDM.Pims.StartTransaction;
    try
      for IndexDay := 1 to 7 do
      begin
        PasteOneDay(AEmployeeNumber, IndexDay, ATargetWeekNumber,
          DlgMsgResult, QuestionResult);
      end;
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
    except
      // Trap errors here
      on E:EdbEngineError do
      begin
        // Log error!
        WErrorLog(E.message);
        DisplayMessage(E.message, mtError, [mbOK]);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
      on E:Exception do
      begin
        // Log error!
        WErrorLog(E.message);
        DisplayMessage(E.message, mtError, [mbOK]);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  finally
    //
  end;
end; // PasteExecute

procedure TEmployeeAvailabilityDM.InitializeDayChangedArray;
var
  IndexDay: Integer;
begin
  for IndexDay := 1 to 7 do
    FDayChanged[IndexDay] := 0;
end;

function TEmployeeAvailabilityDM.CopyFromOtherWeek(AEmployeeNumber,
  ASourceWeekNumber, ATargetWeekNumber: Integer): Boolean;
begin
  Result := SetCopyFromValues(AEmployeeNumber, ASourceWeekNumber);
  if Result then
    Result := PasteExecute(AEmployeeNumber, ASourceWeekNumber,
      ATargetWeekNumber);
end;

// MR:20-1-2006 BEGIN Use query for finding/updating SHS (optimising)
function TEmployeeAvailabilityDM.SHSFind(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime): Boolean;
begin
  with qrySHSFind do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    Open;
    Result := not IsEmpty;
  end;
end;

procedure TEmployeeAvailabilityDM.SHSDelete(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime);
begin
  with qrySHSDelete do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ExecSQL;
  end;
end;

procedure TEmployeeAvailabilityDM.SHSInsert(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime;
  APlantCode: String;
  AShiftNumber: Integer);
begin
  with qrySHSInsert do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    ParamByName('CREATIONDATE').AsDateTime := Now;
    ParamByName('MUTATIONDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end;

procedure TEmployeeAvailabilityDM.SHSUpdate(AEmployeeNumber: Integer;
  AShiftScheduleDate: TDateTime;
  APlantCode: String;
  AShiftNumber: Integer);
begin
  with qrySHSUpdate do
  begin
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('SHIFT_SCHEDULE_DATE').AsDateTime := AShiftScheduleDate;
    ParamByName('PLANT_CODE').AsString := APlantCode;
    ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    ParamByName('MUTATIONDATE').AsDateTime := Now;
    ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    ExecSQL;
  end;
end;
// MR:20-1-2006 END Use query for finding/updating SHS (optimising)

function TEmployeeAvailabilityDM.ValidTBForDay(const APlantCode,
  ADepartmentCode: String; const AShiftNumber, AEmployeeNumber: Integer;
  const ADate: TDateTime): Boolean;
var
  AShiftDayRecord: TShiftDayRecord;
begin
  AShiftDayRecord.APlantCode := APlantCode;
  AShiftDayRecord.AShiftNumber := AShiftNumber;
  AShiftDay.AEmployeeNumber := AEmployeeNumber;
  AShiftDay.ADepartmentCode := ADepartmentCode;
  AShiftDayRecord.ADate := ADate;
  AShiftDay.DetermineShiftStartEnd(AShiftDayRecord);
  Result := AShiftDayRecord.AValid;
end;

// RV094.8. Do this with a separate procedure.
procedure TEmployeeAvailabilityDM.OpenQueryEmpl;
begin
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    QueryEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
  else
    QueryEmpl.ParamByName('USER_NAME').AsString := '*';
  // RV073.6. Inactive employees.
  QueryEmpl.ParamByName('FCURRENTDATE').AsDateTime := MyDate; // RV094.8.
  QueryEmpl.Open;
end;

// 20011800
function TEmployeeAvailabilityDM.FinalRunCheck(
  ACompareDate: TDateTime): Boolean;
var
  FinalRunExportDate: TDateTime;
begin
  Result := True;
  if SystemDM.UseFinalRun then
  begin
    FinalRunExportDate :=
      SystemDM.FinalRunExportDateByPlant(
        QueryEmpl.FieldByName('PLANT_CODE').AsString);
    if FinalRunExportDate <> NullDate then
      if ACompareDate <= FinalRunExportDate then
        Result := False;
  end;
end; // FinalRunCheck

// 20011800
function TEmployeeAvailabilityDM.FinalRunCheck(AYear,
  AWeek: Integer; ADay: Integer=1): Boolean;
begin
  Result := FinalRunCheck(ListProcsF.DateFromWeek(AYear, AWeek, ADay));
end; // FinalRunCheck

end.

