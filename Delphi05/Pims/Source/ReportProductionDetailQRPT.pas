(*
  Changes:
    MRA: 05-MAY-2008 RV006.
      Added 'Order By' for all queries, otherwise sorting can go wrong under
      Oracle 10.
    MRA: 06-OCT-2008 RV010.
      Quotes added for Code-fields when Filters are used. If not, then it gives
      problems when a Code-field contains A-Z-characters.
      Example, it crashed when JOB_CODE contained '2222av'.
    MRA: 13-NOV-2008 RV014.
      Totals are wrong when a workspot is shown with multiple jobs.
      It should totalize always per job, instead of only per workspot!
      -> Todo: Pieces is wrong! When compared with Report Product.
    MRA: 24-FEB-2009 RV022.
      Team-selection is now only on DEPARTMENTPERTEAM, but it must be
      on EMPLOYEE.TEAM-level.
    MRA: 09-JUL-2009 RV032.
      When there are jobs with same code for different workspots:
        When 'Show only jobs' set to NO:
          The result is wrong: Some employee appear more than once, but only on
          1 job correct with hours + quantities.
        When 'Show only jobs' set to YES:
          The result looks OK, but same job-codes are grouped together, so
          you cannot see to what workspot they belong.
    MRA:3-MAY-2010. RV062.2.
    - SetMemoParam: First argument increased, to prevent
      problems with double line when printing to printer or PrimoPDF.
    MRA:20-MAY-2010 RV063.2. 889997.
    - Addition of shift selection
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:17-NOV-2010 RV080.1.
  - Bug with double lines when printing to e.g. PrimoPDF.
    Preview is OK, but printing to a printer or PrimoPDF
    gives double lines.
  - Reports and Memo-mechanism.
    - Put Memo-mechanism in a seperate class, so
      it can be created and freed when needed.
      NOTE: This did not solve the problem.
  - IMPORTANT: When the Memo-mechanism is NOT used and the QRMemo-
    components are all set to AutoSize=True and AutoStretch=True,
    then it works also without the 'double lines' problem!
  MRA:24-APR-2012 20012858.
  - Addition of system-setting to define if efficiency must be calculated
    by quantity or time.
  - Addition of efficiency-calculation based on time. For this the theoretical
    time must be calculated for all levels that are possible in this report.
  - To calculate efficiency based on time:
    - Efficiency = TheoreticalTime / ActualTime * 100
  MRA:4-MAY-2012 20012858 Bugfix.
  - When there were 2 jobs on different shifts for 1 workspot, it showed
    them twice, resulting in double pieces. Reason: It did not add the shift
    in the cdsPieces that stores the pieces.
  MRA:8-MAY-2012 20012858. Production Reports and NOJOB.
  - Filter out the NOJOB-job to prevent wrong quantities when
    this job has a negative quantity.
  MRA:12-JUL-2012 20012858.130. Bugfix.
  - This report gives no hours for employees when shifts do not match.
    There is a shift on productionquantity-level and a shift
    on scan-level, but when they do not match, then
    it will not show hours (based on scans).
  - To solve this, the shift must always be based on scans.
    Just ignore the shifts from the productionquantity.
  MRA:23-JUL-2012 20012858.130.3. Change.
  - Report Production Details
    - Add option to show compare jobs or not using
      a checkbox.
      Note: A compare job is a job where field COMPARATION_JOB_YN
      is set to 'Y'.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - When a job (JOBCODE-table) has field IGNOREQUANTITIESINREPORTS_YN set
    to 'Y' then the report should show no quantities for that job, only
    the time must be shown.
  MRA:8-OCT-2012 20013489 Overnight-shift-system
  - Filter on SHIFT_DATE to make it possible to see info about a whole
    (overnight-) shift.
  - When Show Shifts-option is used then it repeats some shifts at the end.
    Solved by filtering this out.
  MRA:22-OCT-2012 20013489 Overnight-shift-system
  - When Show Shifts-option is used, it gives a different result when it is
    not used. Cause: It did not filter on TRS.SHIFT_DATE.
  MRA:8-JAN-2013 TD-21167
  - Report shows incorrect data
    - Changed selection for QueryProduction:
      - As base use same query as qryMain, to get correct info for grouping.
    - Added more debugging.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  ROP 13-MAR-2013 SR-20013769 - reorganised header
  MRA:18-MAR-2013 TD-22296
  - Report production details does not filter on department anymore.
  - Also: Filter on business units was missing.
  MRA:21-MAR-2013 TD-21527
  - Also do this for time-values (DecodeHrsMin). For this purpose function
    DecodeHrsMin has extra argument that decides if it should new format or not.
    Default not.
  MRA:21-MAR-2013 TD-22321 (also SO-20013769 related to this order)
  - Save to PDF gives problems: When the preview is in landscape, the
    Save-to-PDF is in portrait.
    - Note: printing to PrimoPDF is OK.
  - When only using landscape-mode (no switching), then it still gives
    a wrong result when saving-to-PDF.
  MRA:12-APR-2013 TD-21527
  - Use CurrencyFormat to set the currency symbol (instead of dollar-sign).
  MRA:7-MAY-2013 TD-22321 Save to PDF-landscape issue
  - This report switches to portrait or landscape based on what it should
    show. When switched to landscape and saving as PDF it still uses portrait
    as orientation. NOTE: The preview itself and also when printing to
    a printer appears to be OK.
  - To solve this (for now), the report is now fixed to landscape-mode.
  - Technical note: It is not possible to change the length and width of the
    page during run-time using qrptBase.Page-variable.
  MRA:1-APR-2014 TD-24599
  - When using show shifts it shows a shift -999. This must not be done.
    - Solution: Show 'No match found' instead of -999.
  - IMPORTANT:
    Shift -999 indicates 'no match found' with employee-scans.
    This is to show quantities for 'no employee'. So it must NOT be left out in
    the report, or you will get a difference when comparing the report without
    'show shifts'.
    A solution would be to call -999 'No employee' or something.
  - Related to this order: Also export shifts!
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:19-AUG-2014 20015220.90 Rework
  - Make more room for from-to-columns in selection-part, because of
    from-to-date that can be much larger when time-part is shown.
  MRA:22-SEP-2014 20015220.100 (rework)
  - When 'ignore quantities in reports' on job-level was set to 'Yes' it did
    not show details for such a job. But it must still show something when hours
    were made on that job!
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
  MRA:27-OCT-2014 20015220.110 Rework
  - When ProductionQuantity-records are entered over longer periods than
    5 min. then it cannot always find it, depending on what was entered
    for 'selection on time'.
  MRA:4-NOV-2014 TD-26051
  - Translate issue with report production details
  - There are 7 headers and several texts are not translated or have a
    wrong translation.
  - Because of troubles with the translation tool, a part of the texts is now
    copied from a header that is correctly translated.
  MRA:9-JUN-2015 SO-20014450.50
  - Real Time Efficiency
  - Get data from a view
  MRA:8-OCT-2015 PIM-12
  - There was a difference in efficiency shown per employee and per job.
  MRA:22-DEC-2015 PIM-12
  - Bugfix: Do not look for overtimemins!
  MRA:15-JAN-2016 PIM-137
  - Add EmployeeFrom, EmployeeTo to real-time-eff-build-query for reports where
    employees can be selected.
  MRA:15-JAN-2016 PIM-135
  - Add salary hours and difference-columns to report.
  - Because of this a reorganisation was needed for this report because it
    had become very complex to change anything.
    What has been changed:
    - Now it uses only 1 column-header instead of 8. The column-header
      (ChildBands) are still there but are minimized in size. To prevent
      problems with translation-tool they are not removed.
    - Also the BU-group-footers are limited to 1. Here also the ones
      that are not used anymore are minimized in size. To prevent problems
      with translation-tool they are not removed.
    - Use Trim() for Efficiency-fields, or it 'moves' each time you reprint
      the report!
      In general: When Format() is used with a %8.2 (width-specifier) use
      Trim() to prevent any problems during printing.
  - Also: It is very difficult to align the QRMemo-fields, so it is now
    done in the program itself based on dummy-QRLabel-fields that are
    put over the QRMemo-fields. During ConfigReport-procedure the
    QRMemo-fields are then aligned via these dummy-QRLabel-fields.
  MRA:5-FEB-2016 PIM-135
  - Production reports add salary-hours column
  - Related to this order:
  - Also show down-time-jobs + breaks.
  - Show down-time-jobs based on dialog-setting:
    - Show it not (default), yes, or only.
  MRA:15-FEB-2016 PIM-135
  - Do not show salary/diff columns.
*)

unit ReportProductionDetailQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, DB, Printers,
  CalculateTotalHoursDMT, UScannedIDCard, ReportBaseDMT, ComCtrls,
  QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FTeamFrom, FTeamTo, FShiftFrom, FShiftTo: String;
    FAllShifts, FShowBU, FShowDept, FShowWK, FShowJob, FShowEmpl,
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK, FPageJob,
    FShowBonus, FShowSales, FShowOnlyJob, FShowPayroll: Boolean;
    FExportToFile, FShowShifts, FShowCompareJobs: Boolean; // MR:19-09-2003
    FIncludeDownTime: Integer; // 20015586
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
      AllShifts, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
      ShowBonus, ShowSales, ShowOnlyJob, ShowPayroll,
      ExportToFile, ShowShifts, ShowCompareJobs: Boolean;
      IncludeDownTime: Integer); // 20015586
  end;

  TAmountsPerDay = Array[1..7] of Integer;

  TReportProductionDetailQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDescBU: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterBU: TQRBand;
    QRLabelTotPlant: TQRLabel;
    QRBandFooterWK: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRBandFooterDEPT: TQRBand;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabelTotDept: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabelTotBU: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabelWKOvertimeHrs: TQRLabel;
    QRLabelWKPayrollDollars: TQRLabel;
    QRShape1: TQRShape;
    QRLabel2: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelWKCostsPerPiece: TQRLabel;
    QRLabelWKCostsPerHr: TQRLabel;
    QRLabelTotalDeptPieces: TQRLabel;
    QRLabelTotalBUPieces: TQRLabel;
    QRLabelTotalPlantPieces: TQRLabel;
    QRLabelWKWorkedHrs: TQRLabel;
    QRLabelTotalDeptWorkedHrs: TQRLabel;
    QRLabelTotalBUWorkedHrs: TQRLabel;
    QRLabelTotalPlantWorkedHrs: TQRLabel;
    QRLabelTotalDeptOvertimeHrs: TQRLabel;
    QRLabelTotalBUOvertimeHrs: TQRLabel;
    QRLabelTotalPlantOvertimeHrs: TQRLabel;
    QRLabelTotalDeptPayrollDollars: TQRLabel;
    QRLabelTotalBUPayrollDollars: TQRLabel;
    QRLabelTotalPlantPayrollDollars: TQRLabel;
    QRLabelTotalDeptCostsPerPiece: TQRLabel;
    QRLabelTotalBUCostsPerPiece: TQRLabel;
    QRLabelTotalPlantCostsPerPiece: TQRLabel;
    QRLabelTotalDeptCostsPerHr: TQRLabel;
    QRLabelTotalBUCostsPerHr: TQRLabel;
    QRLabelTotalPlantCostsPerHr: TQRLabel;
    QRLabelTotalBUPiecesPerHr: TQRLabel;
    QRLabelTotalPlantPiecesPerHr: TQRLabel;
    QRLabelWKPieces: TQRLabel;
    QRLabelTotalBUSales: TQRLabel;
    QRDBText2: TQRDBText;
    QRBandFooterJob: TQRBand;
    QRMENr: TQRMemo;
    QRMEName: TQRMemo;
    QRMPieces: TQRMemo;
    QRMWrkHrs: TQRMemo;
    QRMOvertime: TQRMemo;
    QRMPayroll: TQRMemo;
    QRMCostPiece: TQRMemo;
    QRMCostsPerHr: TQRMemo;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRDBTextStandard1: TQRDBText;
    QRMPiecesHr: TQRMemo;
    QRMPerf: TQRMemo;
    QRMBonus: TQRMemo;
    QRLabelWKPiecesPerHr: TQRLabel;
    QRLabelTotalDeptPiecesPerHr: TQRLabel;
    ChildBandBU1: TQRChildBand;
    ChildBandBU2: TQRChildBand;
    ChildBandBU3: TQRChildBand;
    QRLabel191: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabelTotalBUWorkedHrs1: TQRLabel;
    QRLabelTotalBUPieces1: TQRLabel;
    QRLabelTotalBUOvertimeHrs1: TQRLabel;
    QRLabelTotalBUPayrollDollars1: TQRLabel;
    QRLabel196: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel197: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabelTotalBUWorkedHrs2: TQRLabel;
    QRLabelTotalBUWorkedHrs3: TQRLabel;
    QRLabelTotalBUPieces2: TQRLabel;
    QRLabelTotalBUPieces3: TQRLabel;
    QRLabelTotalBUCostsPerPiece1: TQRLabel;
    QRLabelTotalBUCostsPerHr1: TQRLabel;
    QRLabelTotalBUPiecesPerHr1: TQRLabel;
    QRLabelTotalBUPercPayrollToSales: TQRLabel;
    QRLabelTotalBUIncomePerWorkedHour: TQRLabel;
    QRLabelTotalBUMarginalRatePerHour: TQRLabel;
    QRLabelTotalBUSales1: TQRLabel;
    QRLabelTotalBUPercPayrollToSales1: TQRLabel;
    QRLabelTotalBUIncomePerWorkedHour1: TQRLabel;
    QRLabelTotalBUMarginalRatePerHour1: TQRLabel;
    QRLabelTotalBUSales2: TQRLabel;
    QRLabelTotalBUPercPayrollToSales2: TQRLabel;
    QRLabelTotalBUIncomePerWorkedHour2: TQRLabel;
    QRLabelTotalBUMarginalRatePerHour2: TQRLabel;
    QRLabelTotalBUSales3: TQRLabel;
    QRLabelTotalBUPercPayrollToSales3: TQRLabel;
    QRLabelTotalBUIncomePerWorkedHour3: TQRLabel;
    QRLabelTotalBUMarginalRatePerHour3: TQRLabel;
    QRLabelTotalBUPiecesPerHr2: TQRLabel;
    QRLabelTotalBUPiecesPerHr3: TQRLabel;
    QRLabelDeptEfficiency: TQRLabel;
    QRLabelDeptIncentive: TQRLabel;
    QRLabelBUEfficiency: TQRLabel;
    QRLabelBUIncentive: TQRLabel;
    QRLabelBUEfficiency1: TQRLabel;
    QRLabelBUIncentive1: TQRLabel;
    QRLabelBUEfficiency3: TQRLabel;
    QRLabelBUEfficiency2: TQRLabel;
    QRLabelPlantEfficiency: TQRLabel;
    QRLabelPlantIncentive: TQRLabel;
    QRLabelLandscapeW: TQRLabel;
    QRLabelLandscapeJ: TQRLabel;
    QRLblJobDescription: TQRLabel;
    QRShape20: TQRShape;
    QRShape21: TQRShape;
    QRBandSummary: TQRBand;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabelFromShift: TQRLabel;
    QRLabelShift: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabelToShift: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabel195: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBTextSDESCRIPTION: TQRDBText;
    QRGroup1: TQRGroup;
    QRGroupHDJOB: TQRGroup;
    QRGroupHDSHIFT: TQRGroup;
    ChildBand6: TQRChildBand;
    ChildBand7: TQRChildBand;
    ChildBand8: TQRChildBand;
    ChildBand9: TQRChildBand;
    ChildBand10: TQRChildBand;
    ChildBand11: TQRChildBand;
    ChildBand12: TQRChildBand;
    QRLabel199: TQRLabel;
    QRLabel200: TQRLabel;
    QRLabel201: TQRLabel;
    QRLabel202: TQRLabel;
    QRLabel203: TQRLabel;
    QRLabel204: TQRLabel;
    QRLabel205: TQRLabel;
    QRLabel206: TQRLabel;
    QRLabel207: TQRLabel;
    QRLabel208: TQRLabel;
    QRLabel209: TQRLabel;
    QRShape22: TQRShape;
    QRLabel179: TQRLabel;
    QRLabel180: TQRLabel;
    QRLabel181: TQRLabel;
    QRLabel182: TQRLabel;
    QRLabel183: TQRLabel;
    QRLabel184: TQRLabel;
    QRLabel185: TQRLabel;
    QRLabel186: TQRLabel;
    QRLabel187: TQRLabel;
    QRLabel188: TQRLabel;
    QRLabel189: TQRLabel;
    QRLabel210: TQRLabel;
    QRLabel211: TQRLabel;
    QRLabel212: TQRLabel;
    QRLabel213: TQRLabel;
    QRLabel214: TQRLabel;
    QRLabel215: TQRLabel;
    QRLabel216: TQRLabel;
    QRLabel217: TQRLabel;
    QRLabel218: TQRLabel;
    QRLabel219: TQRLabel;
    QRLabel220: TQRLabel;
    QRLabel221: TQRLabel;
    QRShape18: TQRShape;
    QRShape15: TQRShape;
    QRLabel156: TQRLabel;
    QRLabel157: TQRLabel;
    QRLabel158: TQRLabel;
    QRLabel159: TQRLabel;
    QRLabel160: TQRLabel;
    QRLabel161: TQRLabel;
    QRLabel162: TQRLabel;
    QRLabel163: TQRLabel;
    QRLabel164: TQRLabel;
    QRLabel165: TQRLabel;
    QRLabel166: TQRLabel;
    QRLabel167: TQRLabel;
    QRShape11: TQRShape;
    QRShape10: TQRShape;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel168: TQRLabel;
    QRLabel169: TQRLabel;
    QRLabel170: TQRLabel;
    QRLabel171: TQRLabel;
    QRLabel172: TQRLabel;
    QRLabel173: TQRLabel;
    QRLabel174: TQRLabel;
    QRLabel175: TQRLabel;
    QRLabel176: TQRLabel;
    QRLabel177: TQRLabel;
    QRLabel178: TQRLabel;
    QRLabel222: TQRLabel;
    QRLabel505: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRShape8: TQRShape;
    QRLabel3: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel192: TQRLabel;
    QRLabel224: TQRLabel;
    QRLabel225: TQRLabel;
    QRLabel226: TQRLabel;
    QRLabel227: TQRLabel;
    QRLabel228: TQRLabel;
    QRLabel229: TQRLabel;
    QRLabel230: TQRLabel;
    QRLabel231: TQRLabel;
    QRLabel232: TQRLabel;
    QRLabel503: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRLabel501: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    QRLabel93: TQRLabel;
    QRLabel94: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel96: TQRLabel;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabel190: TQRLabel;
    QRLabel234: TQRLabel;
    QRLabel235: TQRLabel;
    QRLabel236: TQRLabel;
    QRLabel237: TQRLabel;
    QRLabel238: TQRLabel;
    QRLabel239: TQRLabel;
    QRLabel240: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel242: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    QRLabel246: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel249: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel251: TQRLabel;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRGroupColumnHeader: TQRGroup;
    QRLabel502: TQRLabel;
    QRLabel504: TQRLabel;
    QRLabel506: TQRLabel;
    QRLabel507: TQRLabel;
    QRLabel508: TQRLabel;
    QRLabelIncludeDowntime: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabel76: TQRLabel;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel78: TQRLabel;
    QRLabelWrkHrs: TQRLabel;
    QRLabelQuantity: TQRLabel;
    QRLabelPcsPerHr: TQRLabel;
    QRLabelEff: TQRLabel;
    QRLabelOvertime: TQRLabel;
    QRLabelPayroll: TQRLabel;
    QRLabelCostPerPc: TQRLabel;
    QRLabelLaborRatePHr: TQRLabel;
    QRLabelIncentive: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabelSalHrs: TQRLabel;
    QRLabelDiffHrs: TQRLabel;
    QRMSalHrs: TQRMemo;
    QRMDiffHrs: TQRMemo;
    QRLabelTotalDeptSalHrs: TQRLabel;
    QRLabelTotalBUSalHrs: TQRLabel;
    QRLabelTotalPlantSalHrs: TQRLabel;
    QRLabelTotalDeptDiffHrs: TQRLabel;
    QRLabelTotalBUDiffHrs: TQRLabel;
    QRLabelTotalPlantDiffHrs: TQRLabel;
    QRLabelWKSalHrs: TQRLabel;
    QRLabelWKDiffHrs: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDescDeptPrint(sender: TObject; var Value: String);
    procedure QRBandFooterJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText11Print(sender: TObject; var Value: String);
    procedure QRDBTextStandard2Print(sender: TObject; var Value: String);
    procedure QRLabelTotalBUSalesBPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalBUSalesPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUIncomePerWorkedHourBPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUMarginalRatePerHourPrint(sender: TObject;
      var Value: String);
    procedure QRLabelWKBonusPrint(sender: TObject; var Value: String);
    procedure QRLabelWKPerformancePrint(sender: TObject; var Value: String);
    procedure QRLabelTotalBUSalesNPPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUPercPayrollToSalesNPPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUIncomePerWorkedHourNPPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUMarginalRatePerHourNPPrint(sender: TObject;
      var Value: String);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelWKPrint(sender: TObject; var Value: String);
    procedure QRLabelJobPrint(sender: TObject; var Value: String);
    procedure QRLabelEmplPrint(sender: TObject; var Value: String);
    procedure QRDBTextStandard1Print(sender: TObject; var Value: String);
    procedure QRLabelWKOvertimeHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptPiecesPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelWKPiecesPerHrPrint(sender: TObject;
      var Value: String);
    procedure ChildBandBU1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandBU2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandBU3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotalPlantPiecesPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantPiecesPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUPiecesPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUPiecesPerHr1Print(sender: TObject;
      var Value: String);
    procedure QRDBText14Print(sender: TObject; var Value: String);
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText6Print(sender: TObject; var Value: String);
    procedure QRDBText10Print(sender: TObject; var Value: String);
    procedure QRDBText1Print(sender: TObject; var Value: String);
    procedure QRBandFooterDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelDeptEfficiencyPrint(sender: TObject;
      var Value: String);
    procedure QRLabelDeptIncentivePrint(sender: TObject;
      var Value: String);
    procedure QRLblJobDescriptionPrint(sender: TObject; var Value: String);
    procedure ChildBand1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHdPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDBUAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDDEPTAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterJobAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandFooterWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandBU1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandBU2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBandBU3AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDJOBAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLabelTotDeptPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptWorkedHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalDeptPiecesPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotBUPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalBUWorkedHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUPiecesPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUOvertimeHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUPayrollDollarsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUCostsPerPiecePrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUCostsPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelBUEfficiencyPrint(sender: TObject; var Value: String);
    procedure QRLabelBUIncentivePrint(sender: TObject; var Value: String);
    procedure QRLabelTotalBUPiecesPerHr2Print(sender: TObject;
      var Value: String);
    procedure QRLabelBUEfficiency2Print(sender: TObject;
      var Value: String);
    procedure QRLabelBUIncentive1Print(sender: TObject; var Value: String);
    procedure QRLabelTotPlantPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalPlantWorkedHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantOvertimeHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantPayrollDollarsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantCostsPerPiecePrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantCostsPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelPlantEfficiencyPrint(sender: TObject;
      var Value: String);
    procedure QRLabelPlantIncentivePrint(sender: TObject;
      var Value: String);
    procedure QRLabelWKWorkedHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelWKPiecesPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptOvertimeHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalDeptPayrollDollarsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalDeptCostsPerHrPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalDeptCostsPerPiecePrint(sender: TObject;
      var Value: String);
    procedure QRLabelMeasureHrsPrint(sender: TObject; var Value: String);
    procedure QRDBText15Print(sender: TObject; var Value: String);
    procedure ChildBand12BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand12AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand11AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand11BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand10AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand9BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand9AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand8BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand8AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand7BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand7AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand6BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand6AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupColumnHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupColumnHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText5Print(sender: TObject; var Value: String);
    procedure QRDBTextSDESCRIPTIONPrint(sender: TObject;
      var Value: String);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelBlankPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptDiffHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalDeptSalHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUSalHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalBUDiffHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalPlantSalHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalPlantDiffHrsPrint(sender: TObject;
      var Value: String);
    procedure QRLabelWKSalHrsPrint(sender: TObject; var Value: String);
    procedure QRLabelWKDiffHrsPrint(sender: TObject; var Value: String);
  private
    FQRParameters: TQRParameters;
    MyProgressBar: TProgressBar;
    TotalEmplPieces: Double;
    //550297
    TotalJobDetailPieces: Double;
    procedure Translate;
    function DecodeHrsSalMin(AValue: Integer): String;
    function DecodeHrsDiffMin(AValue: Integer): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FPrintPlant, {FPrintWK, FPrintJob, FPrintDept, FPrintBU,}
    FPrintFooterDept: Boolean;
    FPlant, FPlantDesc, FBU, FBUDesc,
    FWK, FWKDesc, FDept, FDeptDesc: String;
    FSaveLandscape: String;
    TotalDetailPieces: Double;
    TotalPlantPieces, TotalBUPieces, TotalDeptPieces: Double;

    TotalPlantWorkedHrs, TotalBUWorkedHrs, TotalDeptWorkedHrs: Integer;
    TotalPlantOvertimeHrs, TotalBUOvertimeHrs, TotalDeptOvertimeHrs: Integer;
    TotalPlantPayrollDollars, TotalBUPayrollDollars,
    TotalDeptPayrollDollars: Double;

    TotalPlantEfficiency, TotalPlantIncentive: Double;
    TotalDeptEfficiency, TotalDeptIncentive,
    //550297
    TotalJobIncentive: Double;
    TotalBUEfficiency, TotalBUIncentive,
    TotalJobEfficiency,
    TotalPayroll_PerJob: Double;
    TotalEmplOvertime_PerJob: Integer;
    TotalDeptNormAmount,
    TotalBUNormAmount,
    TotalPlantNormAmount: Double;

    TotalJobWorkedHrs: Integer;
    TotalJobPayrollDollars: Double;
    TotalJobOvertimeHrs: Integer;

    // 20012858 For storing theo. time.
    TotalJobTheoTime: Double;
    TotalPlantTheoTime: Double;
    TotalBUTheoTime: Double;
    TotalDeptTheoTime: Double;

    // PIM-135
    TotalPlantSalHrs, TotalBUSalHrs, TotalDeptSalHrs: Integer;
    TotalJobSalHrs: Integer;
    TotalPlantDiffHrs, TotalBUDiffHrs, TotalDeptDiffHrs: Integer;
    TotalJobDiffHrs: Integer;

    Hour, Min, Sec, MSec, Hour1, Min1, Sec1, MSec1: Word;
    //550297 Car
    FJobWorkedHrs: Integer;
    LastPageWidth, LastPageLength: Integer; // SO-20013769

    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      TeamFrom, TeamTo, ShiftFrom, ShiftTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const AllShifts, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
        ShowSelection, PagePlant,
        PageBU, PageDept, PageWK, PageJob, ShowBonus, ShowSales,
        ShowOnlyJob,  ShowPayroll, ExportToFile, ShowShifts,
        ShowCompareJobs: Boolean;
        IncludeDownTime: Integer; // 20015586
        ProgressBar: TProgressBar): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure SetHorShapeWidth(WidthShape: Integer);
    function PrintFooterBU(PrintBand: Boolean): Boolean;
    function PrintFooterDept(PrintBand: Boolean): Boolean;
    function PrintFooterPlant(PrintBand: Boolean): Boolean;
  end;

var
  ReportProductionDetailQR: TReportProductionDetailQR;
  MyJobDescription: String;
  ExportTotalDeptLine: String;
  ExportTotalBULine: String;
  ExportTotalPlantLine: String;
  ExportTotalWKLine: String;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportProductionDetailDMT, UGlobalFunctions, ListProcsFRM,
  UPimsConst, UPimsMessageRes;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  TeamFrom, TeamTo, ShiftFrom, ShiftTo: String;
  AllShifts, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
  ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
  ShowBonus, ShowSales, ShowOnlyJob, ShowPayroll, ExportToFile,
  ShowShifts, ShowCompareJobs: Boolean;
  IncludeDownTime: Integer); // 20015586
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    FWKFrom := ReportProductionDetailQR.InclExclWorkspotsResult;
    FWKTo := ReportProductionDetailQR.InclExclWorkspotsResult;
  end
  else
  begin
    FWKFrom := WKFrom;
    FWKTo := WKTo;
  end;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FAllShifts := AllShifts;
  FShowBU := ShowBU;
  FShowDept := ShowDept;
  FShowWK := ShowWK;
  FShowJob := ShowJob;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FPageJob := PageJob;
  FShowBonus := ShowBonus;
  FShowSales := ShowSales;
  FShowOnlyJob := ShowOnlyJob;
  FShowPayroll := ShowPayroll;
  FExportToFile := ExportToFile;
  FShowShifts := ShowShifts;
  FShowCompareJobs := ShowCompareJobs;
  FIncludeDownTime := IncludeDownTime; // 20015586
end;

function TReportProductionDetailQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  TeamFrom, TeamTo, ShiftFrom, ShiftTo, EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const AllShifts, ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
  ShowSelection, PagePlant,
  PageBU, PageDept, PageWK, PageJob, ShowBonus, ShowSales,
  ShowOnlyJob, ShowPayroll, ExportToFile, ShowShifts, ShowCompareJobs: Boolean;
  IncludeDownTime: Integer; // 20015586
  ProgressBar: TProgressBar): Boolean;
begin
  MyProgressBar := ProgressBar;
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      TeamFrom, TeamTo, ShiftFrom, ShiftTo, AllShifts,
      ShowBU, ShowDept, ShowWK, ShowJob, ShowEmpl,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK, PageJob,
      ShowBonus, ShowSales, ShowOnlyJob, ShowPayroll, ExportToFile, ShowShifts,
      ShowCompareJobs,
      IncludeDownTime); // 20015586
  end;
  SetDataSetQueryReport(ReportProductionDetailDM.QueryProduction);
end;

function TReportProductionDetailQR.ExistsRecords: Boolean;
var
  SelectStr: String;
  Counter: Integer;
  AllPlants, AllBusinessUnits, AllDepartments,
  AllWorkspots, AllTeams: Boolean;
//  UserName: String;
begin

{$IFDEF DEBUG15}
WDebugLog('Selections:');
WDebugLog('PL: ' + QRBaseParameters.FPlantFrom + ' - ' + QRBaseParameters.FPlantTo);
WDebugLog('BU: ' + QRParameters.FBusinessFrom + ' - ' + QRParameters.FBusinessTo);
WDebugLog('DE: ' + QRParameters.FDeptFrom + ' - ' + QRParameters.FDeptTo);
WDebugLog('WS: ' + QRParameters.FWKFrom + ' - ' + QRParameters.FWKTo);
WDebugLog('TE: ' + QRParameters.FTeamFrom + ' - ' + QRParameters.FTeamTo);
WDebugLog('DT: ' + DateToStr(QRParameters.FDateFrom) + ' - ' +
  DateToStr(QRParameters.FDateTo));
if QRParameters.FShowShifts then
  WDebugLog('-> Show Shifts')
else
  WDebugLog('-> No Shifts');
{$ENDIF}

  // Init/Reset Client Datasets.
  ReportProductionDetailDM.cdsEmpl.Close;
  ReportProductionDetailDM.cdsEmpl.Filtered := False;
  ReportProductionDetailDM.cdsEmpl.Open;

  // 20012858.130.3.
  ReportProductionDetailDM.ShowCompareJobs := QRParameters.FShowCompareJobs;

  Screen.Cursor := crHourGlass;
  Update;
  Application.ProcessMessages;
  DecodeTime(Time,  Hour, Min, Sec, MSec);

  AllPlants := ReportProductionDetailDM.
    DetermineAllPlants(QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllTeams := ReportProductionDetailDM.DetermineAllTeams(
    QRParameters.FTeamFrom, QRParameters.FTeamTo);
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    AllBusinessUnits := ReportProductionDetailDM.
      DetermineAllBusinessUnits(
        QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
    AllDepartments := ReportProductionDetailDM.DetermineAllDepartments(
      QRBaseParameters.FPlantFrom, QRParameters.FDeptFrom, QRParameters.FDeptTo);
    AllWorkspots := ReportProductionDetailDM.DetermineAllWorkspots(
      QRBaseParameters.FPlantFrom, QRParameters.FWKFrom, QRParameters.FWKTo);
  end
  else
  begin
    AllBusinessUnits := True;
    AllDepartments := True;
    AllWorkspots := True;
  end;

  // 20014450.50
  SelectStr :=
    ReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo,
      QRParameters.FDeptFrom, QRParameters.FDeptTo,
      QRParameters.FWKFrom, QRParameters.FWKTo,
      '1', 'zzzzzz',
      QRParameters.FTeamFrom, QRParameters.FTeamTo,
      '0', '999', // ShiftFrom, ShiftTo
      '', '', // EmployeeFrom, EmployeeTo PIM-137
      Trunc(QRParameters.FDateFrom),
      Trunc(QRParameters.FDateTo),
      AllPlants,
      AllBusinessUnits,
      AllDepartments,
      AllWorkspots,
      AllTeams,
      QRParameters.FAllShifts,
      QRParameters.FShowShifts, // ShowShifts
      QRParameters.FShowOnlyJob, // ShowOnlyJob
      QRParameters.FIncludeDownTime, // PIM-135
      QRParameters.FShowShifts, // SelectShift
      False // SelectDate
      );

  // 20014450.50 OLD QUERY
(*
  // TD-21167
  SelectStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE, A.PDESCRIPTION, ' + NL +
    '  A.BUSINESSUNIT_CODE, A.BDESCRIPTION, ' + NL +
    '  A.BONUS_FACTOR, ' + NL +
    '  A.DEPARTMENT_CODE, A.DDESCRIPTION, ' + NL;
  if QRParameters.FShowShifts then
    SelectStr := SelectStr +
      '  A.SHIFT_NUMBER, A.SDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  A.WORKSPOT_CODE, A.WDESCRIPTION, ' + NL +
    '  A.JOB_CODE, A.JDESCRIPTION, ' + NL +
    '  A.NORM_PROD_LEVEL ' + NL;
  if UseDateTime or IncludeOpenScans then // 20015220 + 20015221
    SelectStr := SelectStr +
      '  , SUM(A.PIECES) PIECES ' + NL +
      '  , SUM(A.MINUTES) MINUTES ' + NL;
  SelectStr := SelectStr +
    ' FROM ' + NL +
    '(' + NL;
  SelectStr := SelectStr +
    ReportProductionDetailDM.ReturnMainQuery(
      QRBaseParameters.FPlantFrom,
      QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, // TD-22296
      QRParameters.FBusinessTo, // TD-22296
      QRParameters.FDeptFrom, // TD-22296
      QRParameters.FDeptTo, // TD-22296
      QRParameters.FWKFrom,
      QRParameters.FWKTo,
      QRParameters.FTeamFrom,
      QRParameters.FTeamTo,
      QRParameters.FShiftFrom,
      QRParameters.FShiftTo,
      QRParameters.FDateFrom,
      QRParameters.FDateTo,
      AllPlants,
      AllBusinessUnits, // TD-22296
      AllDepartments, // TD-22296
      AllWorkspots,
      AllTeams,
      QRParameters.FAllShifts,
      QRParameters.FShowShifts,
      True,
      QRParameters.FIncludeDownTime = DOWNTIME_ONLY // 20015586
      ); // Summary

    SelectStr := SelectStr +
      ') A ' + NL;
  SelectStr := SelectStr +
    'GROUP BY ' + NL +
    '  A.PLANT_CODE, A.PDESCRIPTION, ' + NL +
    '  A.BUSINESSUNIT_CODE, A.BDESCRIPTION, ' + NL +
    '  A.BONUS_FACTOR, ' + NL +
    '  A.DEPARTMENT_CODE, A.DDESCRIPTION, ' + NL;
  if QRParameters.FShowShifts then
    SelectStr := SelectStr +
      '  A.SHIFT_NUMBER, A.SDESCRIPTION, ' + NL;
  SelectStr := SelectStr +
    '  A.WORKSPOT_CODE, A.WDESCRIPTION, ' + NL +
    '  A.JOB_CODE, A.JDESCRIPTION, ' + NL +
    '  A.NORM_PROD_LEVEL ' + NL;
  if QRParameters.FShowOnlyJob then
  begin
    SelectStr := SelectStr +
      'ORDER BY ' + NL +
        ' PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, ' + NL;
    if QRParameters.FShowShifts then
      SelectStr := SelectStr +
        ' SHIFT_NUMBER, ' + NL;
    SelectStr := SelectStr +
      ' JOB_CODE, WORKSPOT_CODE ';
  end
  else
  begin
    SelectStr := SelectStr +
      'ORDER BY ' + NL +
        ' PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, ';
    if QRParameters.FShowShifts then
      SelectStr := SelectStr +
        ' SHIFT_NUMBER, ';
    SelectStr := SelectStr +
        ' WORKSPOT_CODE, JOB_CODE ' + NL;
  end;

  // 20015220 Do not trunc the dates here, but later if needed.
  // QRParameters.FDateFrom := Trunc(QRParameters.FDateFrom);
  // QRParameters.FDateTo := Trunc(QRParameters.FDateTo) + 1;
*)

  with ReportProductionDetailDM do
  begin
    QueryProduction.Close;
    QueryProduction.UniDirectional := False;
    QueryProduction.SQL.Clear;
    QueryProduction.SQL.Add(SelectStr);

//    QueryProduction.SQL.SaveToFile('C:\TEMP\ReportProductionDetail.SQL');

    QueryProduction.Open;
    Result := not QueryProduction.Eof;

    // 20014550.50 This is needed for payroll-info
    if Result then
      InitPHEPTQuery(
        QRBaseParameters.FPlantFrom,
        QRBaseParameters.FPlantTo,
        QRParameters.FWKFrom,
        QRParameters.FWKTo,
        Trunc(QRParameters.FDateFrom),
        Trunc(QRParameters.FDateTo),
        AllPlants, AllWorkspots, QRParameters.FShowShifts);
  end;


(*
// WLog('Fase 1 - QueryProduction.Open');

  if Result then
  begin
    ReportProductionDetailDM.InitAll(
      QRBaseParameters.FPlantFrom,
      QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, // TD-22296
      QRParameters.FBusinessTo, // TD-22296
      QRParameters.FDeptFrom, // TD-22296
      QRParameters.FDeptTo, // TD-22296
      QRParameters.FWKFrom,
      QRParameters.FWKTo,
      QRParameters.FTeamFrom,
      QRParameters.FTeamTo,
      QRParameters.FShiftFrom,
      QRParameters.FShiftTo,
      QRParameters.FDateFrom,
      QRParameters.FDateTo,
      AllPlants,
      AllBusinessUnits, // TD-22296
      AllDepartments, // TD-22296
      AllWorkspots,
      AllTeams,
      QRParameters.FAllShifts,
      QRParameters.FShowShifts,
      MyProgressBar,
      QRParameters.FIncludeDownTime = DOWNTIME_ONLY // 20015586
      );
  end;
*)

  // SR-20013769 Moved to BandTitle.
  // TD-22321 This must be done HERE! Not in BandTitle.
  //          Otherwise resizing goes already wrong in preview!
  if Result then
  begin
    if (QRParameters.FShowBonus and QRParameters.FShowSales) or
      (QRParameters.FShowSales and QRParameters.FShowPayroll) or
      ( not QRParameters.FShowPayroll and  QRParameters.FShowBonus and
      QRParameters.fShowSales )then
    begin
      SetHorShapeWidth(QRBandTitle.Width); // TD-22321
      QRLabelLandscapeW.Caption := FSaveLandscape;
      QRLabelLandscapeJ.Caption := FSaveLandscape;
      QRLabel501.Caption := FSaveLandscape;
      QRLabel502.Caption := FSaveLandscape;
      QRLabel503.Caption := FSaveLandscape;
      QRLabel504.Caption := FSaveLandscape;
      QRLabel505.Caption := FSaveLandscape;
    end
    else
    begin
      SetHorShapeWidth(QRBandTitle.Width); // TD-22321
      QRLabel501.Caption := '';
      QRLabel502.Caption := '';
      QRLabel503.Caption := '';
      QRLabel504.Caption := '';
      QRLabel505.Caption := '';
    end;
    if QRParameters.FShowOnlyJob then
    begin
      QRGroupHDJOB.Color := clSilver;
      QRLabelLandscapeJ.Color := clSilver;
      for Counter := 0 to QRGroupHDJOB.ControlCount -1 do
      begin
        if QRGroupHDJOB.Controls[Counter] is TQRDBText then
          (QRGroupHDJOB.Controls[Counter] as TQRDBText).Color := clSilver;
        if QRGroupHDJOB.Controls[Counter] is TQRLabel then
         (QRGroupHDJOB.Controls[Counter] as TQRLabel).Color := clSilver;
      end
    end
    else
    begin
      QRGroupHDJOB.Color := clWhite;
      QRLabelLandscapeJ.Color := clWhite;
      for Counter := 0 to QRGroupHDJOB.ControlCount -1 do
      begin
        if QRGroupHDJOB.Controls[Counter] is TQRDBText then
          TQRDBText(QRGroupHDJOB.Controls[Counter]).Color := clWhite;
          if QRGroupHDJOB.Controls[Counter] is TQRLabel then
        (QRGroupHDJOB.Controls[Counter] as TQRLabel).Color := clWhite;
      end;
    end;
    FDept := '-1';
    FPlant := '-1';
    FBU := '-1';
    FWK := '-1';
  end; // if Result then

  // PIM-135 Correct position QRMemo-fields based on QRLabel-positions
  // The QRLabels used here are only used for positioning and not to
  // display anything.
  if Result then
  begin
    QRMWrkHrs.Left := QRLabelWrkHrs.Left - (QRMWrkHrs.Width - QRLabelWrkHrs.Width);
    QRMSalHrs.Left := QRLabelSalHrs.Left - (QRMSalHrs.Width - QRLabelSalHrs.Width);
    QRMDiffHrs.Left := QRLabelDiffHrs.Left - (QRMDiffHrs.Width - QRLabelDiffHrs.Width);
    QRMPieces.Left :=  QRLabelQuantity.Left - (QRMPieces.Width - QRLabelQuantity.Width);
    QRMPiecesHr.Left := QRLabelPcsPerHr.Left - (QRMPiecesHr.Width - QRLabelPcsPerHr.Width);
    QRMPerf.Left := QRLabelEff.Left - (QRMPerf.Width - QRLabelEff.Width);
    QRMOvertime.Left := QRLabelOvertime.Left - (QRMOvertime.Width - QRLabelOvertime.Width);
    QRMPayroll.Left := QRLabelPayroll.Left - (QRMPayroll.Width - QRLabelPayroll.Width);
    QRMCostPiece.Left := QRLabelCostPerPc.Left - (QRMCostPiece.Width - QRLabelCostPerPc.Width);
    QRMCostsPerHr.Left := QRLabelLaborRatePHr.Left - (QRMCostsPerHr.Width - QRLabelLaborRatePHr.Width);
    QRMBonus.Left := QRLabelIncentive.Left - (QRMBonus.Width - QRLabelIncentive.Width);
  end;

  // SO-20013769
  LastPageWidth := -1;
  LastPageLength := -1;

  if Result then
  begin
    ProgressIndicatorPos := 0;
    Report_RecordCount := ReportProductionDetailDM.QueryProduction.RecordCount;
  end;
  Screen.Cursor := crDefault;
// WLog('END - ExistsRecords');
end;

procedure TReportProductionDetailQR.SetHorShapeWidth(WidthShape: Integer );
 var
  TempComponent: TComponent;
  Counter: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRShape) then
    begin
      if  (TempComponent as TQRShape).Top < 1 then
        (TempComponent as TQRShape).Top := 1;
      (TempComponent as TQRShape).Width := WidthShape;
    end;
  end;
end;

procedure TReportProductionDetailQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := DisplayWorkspotFrom(QRParameters.FWKFrom); // 20015223
  QRLabelWKTo.Caption := DisplayWorkspotTo(QRParameters.FWKTo); // 20015223
  QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
  QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  if not QRParameters.FAllShifts then
  begin
    QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
    QRLabelShiftTo.Caption := QRParameters.FShiftTo;
  end
  else
  begin
    QRLabelShiftFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
  end;
  if UseDateTime then // 20015220
  begin
    QRLabelDateFrom.Caption := DateTimeToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateTimeToStr(QRParameters.FDateTo);
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  end;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
    QRLabelShiftFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
  end;
  if QRParameters.FShowShifts then
  begin
    QRGroupHDShift.Enabled := True;
    QRGroupHDShift.Expression := 'QueryProduction.SHIFT_NUMBER';
  end
  else
  begin
    QRGroupHDShift.Enabled := False;
    QRGroupHDShift.Expression := '';
  end;
  // 20015586
  case QRParameters.FIncludeDownTime of
  DOWNTIME_NO:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeNo;
  DOWNTIME_YES:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeYes;
  DOWNTIME_ONLY:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeOnly;
  end;
  // PIM-135
  if QRParameters.FShowPayroll then
  begin
    // Over time
    QRLabel97.Caption := QRLabel70.Caption;
    QRLabel98.Caption := QRLabel71.Caption;
    // Payroll %
    QRLabel99.Caption := QRLabel72.Caption;
    QRLabel123.Caption := '%';
    // Cost per piece
    QRLabel143.Caption := QRLabel74.Caption;
    QRLabel190.Caption := QRLabel75.Caption;
    QRLabel234.Caption := QRLabel76.Caption;
    // Labor rate per hour
    QRLabel235.Caption := QRLabel77.Caption;
    QRLabel236.Caption := QRLabel78.Caption;
    QRLabel237.Caption := QRLabel79.Caption;
  end
  else
  begin
    // Over time
    QRLabel97.Caption := '';
    QRLabel98.Caption := '';
    // Payroll %
    QRLabel99.Caption := '';
    QRLabel123.Caption := '';
    // Cost per piece
    QRLabel143.Caption := '';
    QRLabel190.Caption := '';
    QRLabel234.Caption := '';
    // Labor rate per hour
    QRLabel235.Caption := '';
    QRLabel236.Caption := '';
    QRLabel237.Caption := '';
  end;
  // PIM-135
  if QRParameters.FShowBonus then
  begin
    // Incentive
    QRLabel243.Caption := QRLabel167.Caption;
  end
  else
  begin
    // Incentive
    QRLabel243.Caption := '';
  end;
  // PIM-135
  if QRParameters.FShowSales then
  begin
    // Sales
    QRLabel244.Caption := QRLabel140.Caption;
    // % payroll to sales
    QRLabel245.Caption := '%';
    QRLabel246.Caption := QRLabel142.Caption;
    QRLabel247.Caption := QRLabel192.Caption;
    QRLabel248.Caption := QRLabel224.Caption;
    // Income per worked hour
    QRLabel249.Caption := QRLabel225.Caption;
    QRLabel250.Caption := QRLabel226.Caption;
    QRLabel251.Caption := QRLabel227.Caption;
    QRLabel252.Caption := QRLabel228.Caption;
    // Marginal rate per hour
    QRLabel253.Caption := QRLabel229.Caption;
    QRLabel254.Caption := QRLabel230.Caption;
    QRLabel255.Caption := QRLabel231.Caption;
    QRLabel256.Caption := QRLabel232.Caption;
  end
  else
  begin
    // Sales
    QRLabel244.Caption := '';
    // % payroll to sales
    QRLabel245.Caption := '';
    QRLabel246.Caption := '';
    QRLabel247.Caption := '';
    QRLabel248.Caption := '';
    // Income per worked hour
    QRLabel249.Caption := '';
    QRLabel250.Caption := '';
    QRLabel251.Caption := '';
    QRLabel252.Caption := '';
    // Marginal rate per hour
    QRLabel253.Caption := '';
    QRLabel254.Caption := '';
    QRLabel255.Caption := '';
    QRLabel256.Caption := '';
  end;
end;

procedure TReportProductionDetailQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportProductionDetailQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  FSaveLandscape := QRLabelLandscapeW.Caption;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
  QRLabel123.Caption := CurrencyString; // TD-21527
  QRLabel73.Caption := CurrencyString; // TD-21527
  QRLabel106.Caption := CurrencyString; // TD-21527
  QRLabel128.Caption := CurrencyString; // TD-21527
  ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := False; // 20015223
  Translate; // Translate some properties here!
  // PIM-135 Do not show Salary/Diff-columns
  QRLabel6.Caption := '';
  QRLabel7.Caption := '';
  QRLabel8.Caption := '';
end;

procedure TReportProductionDetailQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    // Init variables
    MyJobDescription := '';
    ExportTotalDeptLine := '';
    ExportTotalBULine := '';
    ExportTotalPlantLine := '';
    ExportTotalWKLine := '';
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel25.Caption + ' ' + QRLabel48.Caption + ' ' +
        QRLabelBusinessFrom.Caption + ' ' + QRLabel16.Caption + ' ' +
        QRLabelBusinessTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelWKFrom.Caption + ' ' +
        QRLabel90.Caption + ' ' + QRLabelWKTo.Caption);
      ExportClass.AddText(QRLabelFromShift.Caption + ' ' +
        QRLabelShift.Caption + ' ' + QRLabelShiftFrom.Caption + ' ' +
        QRLabelToShift.Caption + ' ' + QRLabelShiftTo.Caption);
      ExportClass.AddText(QRLabelFromDate.Caption + ' ' +
        QRLabelDate.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLabelDateTo.Caption);
      ExportClass.AddText(QRLabelIncludeDowntime.Caption); // 20015586
    end;
  end;
  TotalPlantPieces := 0;
  TotalBUPieces := 0;
  TotalDeptPieces := 0;

  TotalPlantWorkedHrs := 0;
  TotalBUWorkedHrs := 0;
  TotalDeptWorkedHrs := 0;

  TotalPlantOvertimeHrs := 0;
  TotalBUOvertimeHrs := 0;
  TotalDeptOvertimeHrs := 0;

  TotalPlantPayrollDollars := 0;
  TotalBUPayrollDollars := 0;
  TotalDeptPayrollDollars := 0;

  TotalPlantSalhrs := 0;
  TotalBUSalHrs := 0;
  TotalDeptSalHrs := 0;

  TotalPlantDiffHrs := 0;
  TotalBUDiffHrs := 0;
  TotalDeptDiffHrs := 0;
end;

procedure TReportProductionDetailQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  WorkedHrs: Integer;
  OvertimeHrs: Integer;
  Pieces: Double;
  PayrollDollars: Double;
  SalHrs: Integer; // PIM-135
begin
  inherited;
  PrintBand := False;
  if QRParameters.FShowOnlyJob then
    Exit;
  Pieces := TotalDetailPieces;
  WorkedHrs := TotalJobWorkedHrs;
  PayrollDollars := TotalJobPayrollDollars;
  OvertimeHrs := TotalJobOvertimeHrs;
  SalHrs := TotalJobSalHrs;

// ROP 05-MAR-2013 TD-21527  QRLabelPieces.Caption := Format('%.0f', [Pieces]);
  QRLabelWKPieces.Caption := Format('%.0n', [Pieces]);

  QRLabelWKWorkedHrs.Caption := DecodeHrsMin(WorkedHrs, True);

  QRLabelWKSalHrs.Caption := DecodeHrsSalMin(SalHrs);
  QRLabelWKDiffHrs.Caption := DecodeHrsDiffMin(WorkedHrs - SalHrs);

  // Must be calculated!
  QRLabelWKOvertimeHrs.Caption := DecodeHrsMin(OvertimeHrs, True);

// ROP 05-MAR-2013 TD-21527  QRLabelPayrollDollars.Caption := Format('%.2f', [PayrollDollars]);
  QRLabelWKPayrollDollars.Caption := Format('%.2n', [PayrollDollars]);

  // Costs per Piece
  if Pieces > 0 then
    QRLabelWKCostsPerPiece.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.3f', [PayrollDollars / Pieces])
      Format('%.3n', [PayrollDollars / Pieces])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelCostsPerPiece.Caption := Format('%.3f', [0.0]);
    QRLabelWKCostsPerPiece.Caption := Format('%.3n', [0.0]);
  // Costs per Hour
  if WorkedHrs > 0 then
    QRLabelWKCostsPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [PayrollDollars / (WorkedHrs / 60)])
      Format('%.2n', [PayrollDollars / (WorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelCostsPerHr.Caption := Format('%.2f', [0.0]);
    QRLabelWKCostsPerHr.Caption := Format('%.2n', [0.0]);
  // Pieces per Hour
  if WorkedHrs > 0 then
    QRLabelWKPiecesPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.0f', [Pieces / (WorkedHrs / 60)])
      Format('%.0n', [Pieces / (WorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelPiecesPerHrP.Caption := Format('%.0f', [0.0]);
    QRLabelWKPiecesPerHr.Caption := Format('%.0n', [0.0]);
end;

procedure TReportProductionDetailQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);

begin
  inherited;
  if QRParameters.FShowOnlyJob then
  begin
    PrintBand := False;
    Exit;
  end;
  FWK :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
  FWKDesc :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('WDESCRIPTION').AsString;

  TotalDetailPieces := 0;

  TotalJobWorkedHrs := 0;
  TotalJobPayrollDollars := 0;
  TotalJobOvertimeHrs := 0;
  TotalJobSalHrs := 0;
  TotalJobDiffHrs := 0;

  // 20012858
  TotalJobTheoTime := 0;

  if QRParameters.FShowOnlyJob then
    PrintBand := False;

end;

procedure TReportProductionDetailQR.QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;
  FBU :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString;
  FBUDesc :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('BDESCRIPTION').AsString;

  TotalBUPieces := 0;
  TotalBUWorkedHrs := 0;
  TotalBUOvertimeHrs := 0;
  TotalBUPayrollDollars := 0;
  TotalBUEfficiency := 0;
  TotalBUIncentive := 0;
  //550297
  TotalBUNormAmount := 0;
  // 20012858
  TotalBUTheoTime := 0;
  TotalBUSalHrs := 0;
  TotalBUDiffHrs := 0;
end;

procedure TReportProductionDetailQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;
  FPlant :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('PLANT_CODE').AsString;
  FPlantDesc :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('PDESCRIPTION').AsString;

  TotalPlantPieces := 0;
  TotalPlantWorkedHrs := 0;
  TotalPlantOvertimeHrs := 0;
  TotalPlantPayrollDollars := 0;
  TotalPlantEfficiency := 0;
  TotalPlantIncentive := 0;
  //550297
  TotalPlantNormAmount := 0;
  // 20012858
  TotalPlantTheoTime := 0;
  TotalPlantSalHrs := 0;
  TotalPlantDiffHrs := 0;
end;

function TReportProductionDetailQR.PrintFooterBU(PrintBand: Boolean): Boolean;
begin
  Result := PrintBand;
  if not PrintBand then
   Exit;
end;

function TReportProductionDetailQR.PrintFooterPlant(PrintBand: Boolean): Boolean;
begin
  Result := PrintBand;
  if not PrintBand then
   Exit;
end;

function TReportProductionDetailQR.PrintFooterDept(PrintBand: Boolean): Boolean;
begin
  Result := PrintBand;
  if not PrintBand then
   Exit;
end;

procedure TReportProductionDetailQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalBUSales: Double;
  TotalBUPayrollToSales: Double;
  TotalBUIncomePerWorkedHour: Double;
  TotalBUMarginalRatePerHour: Double;
//  TheoTime, TheoProd: Double;
begin
  inherited;
  PrintBand := True; // PIM-135
{
  if QRParameters.FShowPayroll and QRParameters.FShowBonus and PrintBand then
  begin
    PrintBand := PrintFooterBU(PrintBand);
  end
  else
    PrintBand := False;
}
  // 20012858
//  TheoTime := -1;
//  TheoProd := -1;
  TotalBUEfficiency := DetermineEfficiency(0, TotalBUWorkedHrs, TotalBUPieces,
    TotalBUTheoTime, TotalBUNormAmount);
// ROP 05-MAR-2013 TD-21527  QRLabelBUEfficiency.Caption := Format('%8.2f', [TotalBUEfficiency]) + '%';
  QRLabelBUEfficiency.Caption := Trim(Format('%8.2n', [TotalBUEfficiency]) + '%');
// ROP 05-MAR-2013 TD-21527  QRLabelBUIncentive.Caption := '$' + Format('%4.2f', [TotalBUIncentive]);
  // TD21527
  QRLabelBUIncentive.Caption := {'$'} CurrencyString + Trim(Format('%4.2n', [TotalBUIncentive]));
  QRLabelBUEfficiency1.Caption := QRLabelBUEfficiency.Caption;
  QRLabelBUEfficiency2.Caption := QRLabelBUEfficiency1.Caption;
  QRLabelBUEfficiency3.Caption := QRLabelBUEfficiency1.Caption;
  QRLabelBUIncentive1.Caption := QRLabelBUIncentive.Caption;

 //END
// ROP 05-MAR-2013 TD-21527  QRLabelTotalBUPieces.Caption := Format('%.0f', [TotalBUPieces]);
  QRLabelTotalBUPieces.Caption := Format('%.0n', [TotalBUPieces]);
  QRLabelTotalBUWorkedHrs.Caption := DecodeHrsMin(TotalBUWorkedHrs, True);
  QRLabelTotalBUSalHrs.Caption := DecodeHrsSalMin(TotalBUSalHrs);
  QRLabelTotalBUDiffHrs.Caption := DecodeHrsDiffMin(TotalBUDiffHrs);

  QRLabelTotalBUPieces1.Caption := QRLabelTotalBUPieces.Caption;
  QRLabelTotalBUPieces2.Caption := QRLabelTotalBUPieces.Caption;
  QRLabelTotalBUPieces3.Caption := QRLabelTotalBUPieces.Caption;
  QRLabelTotalBUWorkedHrs1.Caption := QRLabelTotalBUWorkedHrs.Caption;
  QRLabelTotalBUWorkedHrs2.Caption := QRLabelTotalBUWorkedHrs.Caption;
  QRLabelTotalBUWorkedHrs3.Caption := QRLabelTotalBUWorkedHrs.Caption;


  QRLabelTotalBUOverTimeHrs.Caption := DecodeHrsMin(TotalBUOvertimeHrs, True);
  QRLabelTotalBUPayrollDollars.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUPayrollDollars]);
    Format('%.2n', [TotalBUPayrollDollars]);
  if TotalBUPieces > 0 then
    QRLabelTotalBUCostsPerPiece.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.3f', [TotalBUPayrollDollars / TotalBUPieces])
      Format('%.3n', [TotalBUPayrollDollars / TotalBUPieces])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUCostsPerPiece.Caption := Format('%.3f', [0.0]);
    QRLabelTotalBUCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalBUWorkedHrs > 0 then
    QRLabelTotalBUCostsPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [TotalBUPayrollDollars / (TotalBUWorkedHrs / 60)])
      Format('%.2n', [TotalBUPayrollDollars / (TotalBUWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUCostsPerHr.Caption := Format('%.2f', [0.0]);
    QRLabelTotalBUCostsPerHr.Caption := Format('%.2n', [0.0]);

  QRLabelTotalBUOverTimeHrs1.Caption :=  QRLabelTotalBUOverTimeHrs.Caption;
  QRLabelTotalBUPayrollDollars1.Caption :=  QRLabelTotalBUPayrollDollars.Caption;
  QRLabelTotalBUCostsPerPiece1.Caption :=  QRLabelTotalBUCostsPerPiece.Caption;
  QRLabelTotalBUCostsPerHr1.Caption :=  QRLabelTotalBUCostsPerHr.Caption;

  if TotalBUWorkedHrs > 0 then
    QRLabelTotalBUPiecesPerHr.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.0f', [TotalBUPieces / (TotalBUWorkedHrs / 60)])
      Format('%.0n', [TotalBUPieces / (TotalBUWorkedHrs / 60)])
  else
// ROP 05-MAR-2013 TD-21527    QRLabelTotalBUPiecesPerHr.Caption := Format('%.0f', [0.0]);
    QRLabelTotalBUPiecesPerHr.Caption := Format('%.0n', [0.0]);
  QRLabelTotalBUPiecesPerHr1.Caption :=  QRLabelTotalBUPiecesPerHr.Caption;
  QRLabelTotalBUPiecesPerHr2.Caption :=  QRLabelTotalBUPiecesPerHr.Caption;
  QRLabelTotalBUPiecesPerHr3.Caption :=  QRLabelTotalBUPiecesPerHr.Caption;

  //
  // MR:22-7-2002
  // Now get information about revenues for this Business Unit
  //

  // Init the variables
  TotalBUSales := 0;
  TotalBUPayrollToSales := 0;
  TotalBUIncomePerWorkedHour := 0;
  TotalBUMarginalRatePerHour := 0;

  // Now get information about sales
  with ReportProductionDetailDM do
  begin
    qryRevenue.Close;
    qryRevenue.ParamByName('BUSINESSUNIT_CODE').AsString :=
      QueryProduction.FieldByName('BUSINESSUNIT_CODE').Value;
    // 20015220
    qryRevenue.ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
    qryRevenue.ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo + 1);
    qryRevenue.Open;
    if qryRevenue.RecordCount > 0 then
    begin
      qryRevenue.First;
      try
        // Revenue - calculations
        (* Column 8 *)
        TotalBUSales := qryRevenue.FieldByName('REVENUESUM').AsFloat;
      except
        TotalBUSales := 0;
      end;
    end;
    qryRevenue.Close;
  end;

  // Now calculate other values

  (* Column 9 *)
  if TotalBUSales > 0 then
    TotalBUPayrollToSales := TotalBUPayrollDollars / TotalBUSales * 100;
  (* Column 10 *)
  if (TotalBUSales > 0) and (TotalBUWorkedHrs > 0) then
    TotalBUIncomePerWorkedHour := TotalBUSales / (TotalBUWorkedHrs / 60);
  (* Column 11 *)
  if (TotalBUIncomePerWorkedHour > 0) and (TotalBUWorkedHrs > 0) then
    TotalBUMarginalRatePerHour := TotalBUIncomePerWorkedHour
      - (TotalBUPayrollDollars / (TotalBUWorkedHrs / 60));

      // Now put them to labels in the report
  QRLabelTotalBUSales.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUSales]);
    Format('%.2n', [TotalBUSales]);
  QRLabelTotalBUPercPayrollToSales.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.0f', [TotalBUPayrollToSales]);
    Format('%.0n', [TotalBUPayrollToSales]);
  QRLabelTotalBUIncomePerWorkedHour.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUIncomePerWorkedHour]);
    Format('%.2n', [TotalBUIncomePerWorkedHour]);
  QRLabelTotalBUMarginalRatePerHour.Caption :=
// ROP 05-MAR-2013 TD-21527    Format('%.2f', [TotalBUMarginalRatePerHour]);
    Format('%.2n', [TotalBUMarginalRatePerHour]);
 // car 2-4-2003
  QRLabelTotalBUSales1.Caption := QRLabelTotalBUSales.Caption;
  QRLabelTotalBUPercPayrollToSales1.Caption := QRLabelTotalBUPercPayrollToSales.Caption;
  QRLabelTotalBUIncomePerWorkedHour1.Caption := QRLabelTotalBUIncomePerWorkedHour.Caption;
  QRLabelTotalBUMarginalRatePerHour1.Caption := QRLabelTotalBUMarginalRatePerHour.Caption;
  QRLabelTotalBUSales2.Caption := QRLabelTotalBUSales.Caption;
  QRLabelTotalBUPercPayrollToSales2.Caption := QRLabelTotalBUPercPayrollToSales.Caption;
  QRLabelTotalBUIncomePerWorkedHour2.Caption := QRLabelTotalBUIncomePerWorkedHour.Caption;
  QRLabelTotalBUMarginalRatePerHour2.Caption := QRLabelTotalBUMarginalRatePerHour.Caption;
  QRLabelTotalBUSales3.Caption := QRLabelTotalBUSales.Caption;
  QRLabelTotalBUPercPayrollToSales3.Caption := QRLabelTotalBUPercPayrollToSales.Caption;
  QRLabelTotalBUIncomePerWorkedHour3.Caption := QRLabelTotalBUIncomePerWorkedHour.Caption;
  QRLabelTotalBUMarginalRatePerHour3.Caption := QRLabelTotalBUMarginalRatePerHour.Caption;
end;

procedure TReportProductionDetailQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;

  TotalPlantEfficiency := DetermineEfficiency(0, TotalPlantWorkedHrs,
    TotalPlantPieces, TotalPlantTheoTime, TotalPlantNormAmount);
  QRLabelPlantEfficiency.Caption := Trim(Format('%8.2n', [TotalPlantEfficiency]) + '%');
  QRLabelPlantIncentive.Caption := {'$'} CurrencyString + Trim(Format('%4.2n', [TotalPlantIncentive]));

  QRLabelTotalPlantPieces.Caption := Format('%.0n', [TotalPlantPieces]);
  QRLabelTotalPlantWorkedHrs.Caption := DecodeHrsMin(TotalPlantWorkedHrs, True);
  QRLabelTotalPlantSalHrs.Caption := DecodeHrsSalMin(TotalPlantSalHrs);
  QRLabelTotalPlantDiffHrs.Caption := DecodeHrsDiffMin(TotalPlantDiffHrs);
  QRLabelTotalPlantOvertimeHrs.Caption := DecodeHrsMin(TotalPlantOvertimeHrs, True);
  QRLabelTotalPlantPayrollDollars.Caption :=
    Format('%.2n', [TotalPlantPayrollDollars]);
  if TotalPlantPieces > 0 then
    QRLabelTotalPlantCostsPerPiece.Caption :=
      Format('%.3n', [TotalPlantPayrollDollars / TotalPlantPieces])
  else
    QRLabelTotalPlantCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalPlantWorkedHrs > 0 then
    QRLabelTotalPlantCostsPerHr.Caption :=
      Format('%.2n', [TotalPlantPayrollDollars / (TotalPlantWorkedHrs / 60)])
  else
    QRLabelTotalPlantCostsPerHr.Caption := Format('%.2n', [0.0]);
  if TotalPlantWorkedHrs > 0 then
    QRLabelTotalPlantPiecesPerHr.Caption :=
      Format('%.0n', [TotalPlantPieces / (TotalPlantWorkedHrs / 60)])
  else
    QRLabelTotalPlantPiecesPerHr.Caption := Format('%.0n', [0.0]);
end;

procedure TReportProductionDetailQR.QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;
  FDept :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('DEPARTMENT_CODE').AsString;
  FDeptDESC :=
    ReportProductionDetailDM.
      QueryProduction.FieldByName('DDESCRIPTION').AsString;

  TotalDeptPieces := 0;
  TotalDeptWorkedHrs := 0;
  TotalDeptOvertimeHrs := 0;
  TotalDeptPayrollDollars := 0;
  TotalDeptEfficiency := 0;
  TotalDeptIncentive := 0;
  //550297
  TotalDeptNormAmount := 0;
  // 20012858
  TotalDeptTheoTime := 0;
  TotalDeptSalHrs := 0;
  TotalDeptDiffHrs := 0;
end;

procedure TReportProductionDetailQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;

  TotalDeptEfficiency := DetermineEfficiency(0, TotalDeptWorkedHrs,
    TotalDeptPieces, TotalDeptTheoTime, TotalDeptNormAmount);
  QRLabelDeptEfficiency.Caption :=
    Trim(Format('%8.2n', [TotalDeptEfficiency]) + '%');
  QRLabelDeptIncentive.Caption :=
    {'$'} CurrencyString + Trim(Format('%4.2n', [TotalDeptIncentive]));

  QRLabelTotalDeptPieces.Caption := Format('%.0n', [TotalDeptPieces]);
  QRLabelTotalDeptWorkedHrs.Caption := DecodeHrsMin(TotalDeptWorkedHrs, True);
  QRLabelTotalDeptSalHrs.Caption := DecodeHrsSalMin(TotalDeptSalHrs);
  QRLabelTotalDeptDiffHrs.Caption := DecodeHrsDiffMin(TotalDeptDiffHrs);
  QRLabelTotalDeptOvertimeHrs.Caption := DecodeHrsMin(TOtalDeptOvertimeHrs, True);
  QRLabelTotalDeptPayrollDollars.Caption :=
    Format('%.2n', [TotalDeptPayrollDollars]);
  if TotalDeptPieces > 0 then
    QRLabelTotalDeptCostsPerPiece.Caption :=
      Format('%.3n', [TotalDeptPayrollDollars / TotalDeptPieces])
  else
    QRLabelTotalDeptCostsPerPiece.Caption := Format('%.3n', [0.0]);
  if TotalDeptWorkedHrs > 0 then
    QRLabelTotalDeptCostsPerHr.Caption :=
      Format('%.2n', [TotalDeptPayrollDollars / (TotalDeptWorkedHrs / 60)])
  else
    QRLabelTotalDeptCostsPerHr.Caption := Format('%.2n', [0.0]);
  if TotalDeptWorkedHrs > 0 then
    QRLabelTotalDeptPiecesPerHr.Caption :=
      Format('%.0n', [TotalDeptPieces / (TotalDeptWorkedHrs / 60)])
  else
    QRLabelTotalDeptPiecesPerHr.Caption := Format('%.0n', [0.0]);
end;

procedure TReportProductionDetailQR.QRDBTextDescDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRGroupHDJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
(*
var
  EmployeeNumber: Integer;
  HourlyWage, BonusPercentage: Double;
  BonusInMoney: Boolean;
  EmplPieces, EmplPayrollDollars,  EmplCostsPerPiece: Double;
  EmplWorkedHrs: Integer;
  EmplCostsPerHr, EmplPiecesPerHr: Double;
  PerformanceEmpl, BonusEmpl,  BonusFactor: Double;
  EmplOvertimeMinute, EmpCount: Integer;
  EmpList: TStringList;
  TheoTime, TheoProd: Double;
  TimeRecordingMinutes: Integer; // 20015220 + 20015221
  function MyShiftNumber: String;
  begin
    if QRParameters.FShowShifts then
      Result :=
        ReportProductionDetailDM.
          QueryProduction.FieldByName('SHIFT_NUMBER').AsString
    else
      Result := '0';
  end;
  // 20015586
  function IncludeJobQuantity: Boolean;
  begin
    with ReportProductionDetailDM do
    begin
      Result :=
        (cdsEmpPieces.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y');
      if (QueryProduction.FieldByName('JOB_CODE').AsString = DOWNJOB_1) or
        (QueryProduction.FieldByName('JOB_CODE').AsString = DOWNJOB_2) then
      begin
        Result := QRParameters.FIncludeDownTime <> DOWNTIME_NO;
      end;
    end;
  end; // IncludeJobQuantity
*)
begin
  inherited;

  FPrintFooterDept := True;
 // First := True;
  QRMENr.Lines.Clear;
  QRMEName.Lines.Clear;
  QRMPieces.Lines.Clear;
  QRMWrkHrs.Lines.Clear;
  QRMSalHrs.Lines.Clear;
  QRMDiffHrs.Lines.Clear;
  QRMOvertime.Lines.Clear;
  QRMPayroll.Lines.Clear;
  QRMCostPiece.Lines.Clear;
  QRMCostsPerHr.Lines.Clear;
  QRMPiecesHr.Lines.Clear;
  //CAR 2-4-2003
  QRMPerf.Lines.Clear;
  QRMBonus.Lines.Clear;

  TotalEmplPieces := 0;
  TotalJobDetailPieces := 0;

  PrintBand := True;

//  EmpList := TStringList.Create;

  //550297 - add variables for total line on jobcode
  FJobWorkedHrs := 0;
  TotalJobEfficiency := 0;
  TotalJobIncentive := 0;
  TotalEmplOvertime_PerJob := 0;
  TotalPayroll_PerJob := 0;

  TotalDetailPieces := 0;
  TotalJobWorkedHrs := 0;
  TotalJobPayrollDollars := 0;
  TotalJobOvertimeHrs := 0;
  TotalJobSalHrs := 0;
  TotalJobDiffHrs := 0;

  // 20012858
  TotalJobTheoTime := 0;

//  TimeRecordingMinutes := 0; // 20015220 + 20015221




  // 20014550.50 Determine values from a view
  // OLD ROUTINE:
(*
  // determine all values for this job.
  with ReportProductionDetailDM do
  begin
    try
      // MR:22-12-2004 Do this to show system is busy.
      Application.ProcessMessages;
      UpdateProgressIndicator;
      Screen.Cursor := crHourGlass;
      cdsEmpPieces.Filtered := False;
      cdsEmpPieces.Filter :=
        'PLANT_CODE = ' +
        '''' + QueryProduction.FieldByName('PLANT_CODE').AsString + '''' + ' AND ' +
        'WORKSPOT_CODE = ' +
        '''' + QueryProduction.FieldByName('WORKSPOT_CODE').AsString + '''' + ' AND ' +
        'JOB_CODE = ' +
         '''' + QueryProduction.FieldByName('JOB_CODE').AsString + '''' + ' AND ' +
        'DEPARTMENT_CODE = ' +
        '''' + QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + '''' + ' AND ' +
        'BUSINESSUNIT_CODE = ' +
        '''' + QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + '''' + ' AND ' +
        'EMPLOYEE_NUMBER <> -1 ' + ' AND ' +
        'SHIFT_NUMBER_FILTER = ' + MyShiftNumber;
      cdsEmpPieces.Filtered := True;
      while not cdsEmpPieces.Eof do
      begin
        if (EmpList.IndexOf(
          cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString) < 0) then
          EmpList.Add(cdsEmpPieces.FieldByName('EMPLOYEE_NUMBER').AsString);
        cdsEmpPieces.Next;
      end;
      cdsEmpPieces.Filtered := False;
      cdsEmpPieces.Filter := '';
      // For each Employee
      for EmpCount := 0 to EmpList.Count - 1 do
      begin
        EmployeeNumber := StrToInt(EmpList.Strings[EmpCount]);

        EmplPieces := 0;
        EmplWorkedHrs := 0;
        BonusPercentage := 0;
//        PerformanceEmpl := 0;
        BonusInMoney := False;
        HourlyWage := 0;

        EmplPayrollDollars := 0;
        EmplCostsPerPiece := 0;
        EmplCostsPerHr := 0;
        EmplPiecesPerHr := 0;
        EmplOvertimeMinute := 0;
        // MRA: RV032. Also filter on WORKSPOT.
        if QRParameters.FShowOnlyJob then
        begin
          cdsEmpPieces.Filtered := False;
          cdsEmpPieces.Filter :=
            'PLANT_CODE = ' +
             '''' + QueryProduction.FieldByName('PLANT_CODE').AsString + '''' + ' AND ' +
            'WORKSPOT_CODE = ' +
            '''' + QueryProduction.FieldByName('WORKSPOT_CODE').AsString + '''' + ' AND ' +
            'JOB_CODE = ' +
            '''' + QueryProduction.FieldByName('JOB_CODE').AsString + ''''  + ' AND ' +
            'DEPARTMENT_CODE = ' +
            '''' + QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ''''  + ' AND ' +
            'EMPLOYEE_NUMBER = ' + IntToStr(EmployeeNumber) + ' AND ' +
            'SHIFT_NUMBER_FILTER = ' + MyShiftNumber + ' AND  ' +
            'BUSINESSUNIT_CODE = ' +
            '''' + QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString  + '''';
          cdsEmpPieces.Filtered := True;
          if not cdsEmpPieces.IsEmpty then
          begin
            cdsEmpPieces.First;
            while not cdsEmpPieces.Eof do
            begin
              // 20013478.
              if IncludeJobQuantity // 20015586
                {cdsEmpPieces.
                FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y' } then
                EmplPieces := EmplPieces +
                  cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
              EmplWorkedHrs := EmplWorkedHrs +
                cdsEmpPieces.FieldByName('PRODMINS').AsInteger;
              EmplOvertimeMinute := EmplOvertimeMinute +
                cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger;
              BonusPercentage :=
                cdsEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat;
              HourlyWage :=
                cdsEmpPieces.FieldByName('HOURLY_WAGE').AsFloat;
              BonusInMoney :=
                (cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y');
              cdsEmpPieces.Next;
            end;
          end;
        end
        else
        begin
            if cdsEmpPieces.Locate(
              'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;SHIFT_NUMBER_FILTER',
              VarArrayOf([
                QueryProduction.FieldByName('PLANT_CODE').AsString,
                QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
                QueryProduction.FieldByName('JOB_CODE').AsString,
                QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
                EmployeeNumber,
                MyShiftNumber
              ]),
              []) then
          begin
            // MR:15-02-2005 Also check on businessunit!
            if cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString =
              QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString then
            begin
              // 20013478.
              if IncludeJobQuantity // 20015586
                {cdsEmpPieces.
                FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y'} then
                EmplPieces :=
                  cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
              EmplWorkedHrs := EmplWorkedHrs +
                cdsEmpPieces.FieldByName('PRODMINS').AsInteger;
              EmplOvertimeMinute := EmplOvertimeMinute +
                cdsEmpPieces.FieldByName('OVERTIMEMINS').AsInteger;
              BonusPercentage :=
                cdsEmpPieces.FieldByName('BONUS_PERCENTAGE').AsFloat;
              HourlyWage :=
                cdsEmpPieces.FieldByName('HOURLY_WAGE').AsFloat;
              BonusInMoney :=
                (cdsEmpPieces.FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y');
            end;
          end;
        end;
        // 20015220 + 20015221
        if UseDateTime or IncludeOpenScans then
        begin
          TimeRecordingMinutes := DetermineTimeRecProdMins(
            QRParameters.FDateFrom, QRParameters.FDateTo,
            QueryProduction.FieldByName('PLANT_CODE').AsString,
            QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
            QueryProduction.FieldByName('JOB_CODE').AsString,
            QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
            QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString,
            EmployeeNumber,
            StrToInt(MyShiftNumber),
            NullDate,
            EmplWorkedHrs);
          if (TimeRecordingMinutes > 0) and (TimeRecordingMinutes > EmplWorkedHrs) then
            EmplWorkedHrs := TimeRecordingMinutes;
        end;

        // 20015220 When nothing was found, then skip this part!
        //          This can happen with selection on date+time.
        if (EmplWorkedHrs = 0) and (EmplPieces = 0) then
          Continue; // skip!

        // Employee Number and Description
        FindEmployee(EmployeeNumber);
        QRMemoEmpNr.Lines.Add('  ' + IntToStr(EmployeeNumber));
        QRMemoEmpName.Lines.Add('  ' +
          Copy(cdsEmpl.FieldByName('DESCRIPTION').AsString,1,15));

        // Employee Worked Hours
        TotalJobWorkedHrs := TotalJobWorkedHrs +  EmplWorkedHrs;
        QRMemoWorkedHrs.Lines.Add(DecodeHrsMin(EmplWorkedHrs, True));
        FJobWorkedHrs := FJobWorkedHrs + EmplWorkedHrs;

        // Employee Total Pieces
        TotalJobDetailPieces := TotalJobDetailPieces + EmplPieces;
        TotalEmplPieces := TotalEmplPieces + EmplPieces;
// ROP 05-MAR-2013 TD-21527        QRMemoPieces.Lines.Add(Format('%8.0f', [EmplPieces]));
        QRMemoPieces.Lines.Add(Format('%8.0n', [EmplPieces]));

        TheoTime := -1;
        TheoProd := -1;
        PerformanceEmpl := DetermineEfficiency(
          QueryProduction.FieldByName('NORM_PROD_LEVEL').AsFloat,
          EmplWorkedHrs, EmplPieces, TheoTime, TheoProd);
        if QRParameters.FShowPayroll then
// ROP 05-MAR-2013 TD-21527          QRMemoPerformanceP.Lines.Add(Format('%8.2f', [PerformanceEmpl]) + '%')
          QRMemoPerformanceP.Lines.Add(Format('%8.2n', [PerformanceEmpl]) + '%')
        else
// ROP 05-MAR-2013 TD-21527          QRMemoPerformanceNP.Lines.Add(Format('%8.2f', [PerformanceEmpl])+'%');
          QRMemoPerformanceNP.Lines.Add(Format('%8.2n', [PerformanceEmpl])+'%');
        if EmplWorkedHrs > 0 then
          EmplPiecesPerHr := EmplPieces / (EmplWorkedHrs / 60);
        if QRParameters.FShowPayroll then
        begin
          // Get Payroll
          if EmplWorkedHrs > 0 then
            TotalJobPayrollDollars := TotalJobPayrollDollars +
             (HourlyWage * (EmplWorkedHrs / 60))
          else
            TotalJobPayrollDollars := 0;
          if EmplWorkedHrs > 0 then
            TotalPayroll_PerJob := TotalPayroll_PerJob +
             (HourlyWage * (EmplWorkedHrs / 60))
          else
            TotalPayroll_PerJob := 0;
          if TotalJobWorkedHrs > 0 then
            EmplPayrollDollars := HourlyWage *
              (EmplWorkedHrs / 60);

          // Get Overtime, Employee Overtime Hrs
          TotalJobOvertimeHrs := TotalJobOvertimeHrs + EmplOvertimeMinute;
          QRMemoOvertimeHrs.Lines.Add(DecodeHrsMin(EmplOvertimeMinute, True));
          TotalEmplOvertime_PerJob := TotalEmplOvertime_PerJob +
            EmplOvertimeMinute;

          if (EmplOvertimeMinute > 0) and (BonusPercentage > 0) and
            (BonusInMoney) then
          begin
            TotalJobPayrollDollars := TotalJobPayrollDollars +
              (HourlyWage * BonusPercentage / 100 *
              (EmplOvertimeMinute / 60));
            TotalPayroll_PerJob := TotalPayroll_PerJob +
              (HourlyWage * BonusPercentage / 100 *
              (EmplOvertimeMinute / 60));
            EmplPayrollDollars := EmplPayrollDollars +
              (HourlyWage * BonusPercentage / 100 *
              (EmplOvertimeMinute / 60));
          end;

// ROP 05-MAR-2013 TD-21527          QRMemoPayrollDollars.Lines.Add(Format('%8.2f', [EmplPayrollDollars]));
          QRMemoPayrollDollars.Lines.Add(Format('%8.2n', [EmplPayrollDollars]));
        end;  // if QRParameters.FShowPayroll

        if EmplPieces > 0 then
          EmplCostsPerPiece := EmplPayrollDollars / EmplPieces;
        if EmplWorkedHrs > 0 then
        begin
          EmplCostsPerHr := EmplPayrollDollars / (EmplWorkedHrs / 60);
          EmplPiecesPerHr := EmplPieces / (EmplWorkedHrs / 60);
        end;
        if QRParameters.FShowPayroll then
        begin
// ROP 05-MAR-2013 TD-21527          QRMCostPiece.Lines.Add(Format('%8.3f', [EmplCostsPerPiece]));
          QRMCostPiece.Lines.Add(Format('%8.3n', [EmplCostsPerPiece]));
// ROP 05-MAR-2013 TD-21527          QRMemoCostsPerHr.Lines.Add(Format('%8.2f', [EmplCostsPerHr]));
          QRMemoCostsPerHr.Lines.Add(Format('%8.2n', [EmplCostsPerHr]));
// ROP 05-MAR-2013 TD-21527          QRMPiecesHrP.Lines.Add(Format('%8.0f', [EmplPiecesPerHr]))
          QRMPiecesHrP.Lines.Add(Format('%8.0n', [EmplPiecesPerHr]))
        end
        else
// ROP 05-MAR-2013 TD-21527          QRMPiecesHrNP.Lines.Add(Format('%8.0f', [EmplPiecesPerHr]));
          QRMPiecesHrNP.Lines.Add(Format('%8.0n', [EmplPiecesPerHr]));
        if QRParameters.FShowBonus then
        begin
          BonusEmpl := 0;
          if PerformanceEmpl > 100 then
          begin
            BonusFactor := QueryProduction.FieldByName('BONUS_FACTOR').AsFloat;
            BonusEmpl := (PerformanceEmpl - 100) *
              EmplWorkedHrs * BonusFactor / 60;
          end;
          if QRParameters.FShowPayroll then
// ROP 05-MAR-2013 TD-21527            QRMBonusP.Lines.Add('$' + Format('%4.2f', [BonusEmpl]))
            QRMBonusP.Lines.Add({'$'} CurrencyString + Format('%4.2n', [BonusEmpl]))
          else
// ROP 05-MAR-2013 TD-21527            QRMBonusNP.Lines.Add('$' + Format('%4.2f', [BonusEmpl]));
            QRMBonusNP.Lines.Add({'$'} CurrencyString + Format('%4.2n', [BonusEmpl]));
          TotalDeptIncentive := TotalDeptIncentive +  BonusEmpl;
          TotalBUIncentive := TotalBUIncentive +  BonusEmpl;
          TotalPlantIncentive := TotalPlantIncentive + BonusEmpl;
          //550297 - calculate bonus incentive per jobcode level
          TotalJobIncentive :=  TotalJobIncentive + BonusEmpl;
        end; // if QRParameters.FShowBonus then
      end; // for EmpCount := 0 to EmpList.Count - 1 do

      // Finally determine NO-EMPLOYEE-PIECES
      Application.ProcessMessages;
      UpdateProgressIndicator;
      Screen.Cursor := crHourGlass;
      EmplPieces := 0;
      // MRA: RV032. Also filter on WORKSPOT.
      if QRParameters.FShowOnlyJob then
      begin
        cdsEmpPieces.Filtered := False;
        cdsEmpPieces.Filter :=
          'PLANT_CODE = ' +
           '''' + QueryProduction.FieldByName('PLANT_CODE').AsString + '''' + ' AND ' +
          'WORKSPOT_CODE = ' +
          '''' + QueryProduction.FieldByName('WORKSPOT_CODE').AsString + '''' + ' AND ' +
          'JOB_CODE = ' +
          '''' + QueryProduction.FieldByName('JOB_CODE').AsString + '''' + ' AND ' +
          'DEPARTMENT_CODE = ' +
          '''' + QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + '''' + ' AND ' +
          'EMPLOYEE_NUMBER = ' + IntToStr(NO_EMPLOYEE) + ' AND ' +
          'SHIFT_NUMBER_FILTER = ' +
          MyShiftNumber + ' AND ' +
          'BUSINESSUNIT_CODE = ' +
          '''' + QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + '''';
        cdsEmpPieces.Filtered := True;
        if not cdsEmpPieces.IsEmpty then
        begin
          cdsEmpPieces.First;
          while not cdsEmpPieces.Eof do
          begin
            // 20013478.
            if IncludeJobQuantity // 20015586
              {cdsEmpPieces.
              FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y'} then
              EmplPieces := EmplPieces +
                cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
            cdsEmpPieces.Next;
          end;
        end;
      end
      else
      begin
          if cdsEmpPieces.Locate(
            'PLANT_CODE;WORKSPOT_CODE;JOB_CODE;DEPARTMENT_CODE;EMPLOYEE_NUMBER;SHIFT_NUMBER_FILTER',
            VarArrayOf([
              QueryProduction.FieldByName('PLANT_CODE').AsString,
              QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
              QueryProduction.FieldByName('JOB_CODE').AsString,
              QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
              NO_EMPLOYEE,
              MyShiftNumber
              ]),
            []) then
        begin
          // MR:15-02-2005 Also check on businessunit!
          if cdsEmpPieces.FieldByName('BUSINESSUNIT_CODE').AsString =
            QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString then
            // 20013478.
            if IncludeJobQuantity // 20015586
              {cdsEmpPieces.
              FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y'} then
              EmplPieces :=
                cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
        end;
      end;
      TotalJobDetailPieces := TotalJobDetailPieces + EmplPieces;
    finally
      Screen.Cursor := crDefault;
      EmpList.Clear;
      EmpList.Free;
    end;
  end; // with ReportProductionDetailDM do
  // 20015220 When nothing was found here, do not print this part!
  if (QRMemoEmpNr.Lines.Count = 0) and (TotalJobDetailPieces = 0) then
    PrintBand := False;
*)
end; // QRGroupHDJOBBeforePrint

procedure TReportProductionDetailQR.QRBandFooterJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  JobCostsPerPiece,
  JobCostsPerHr: Double;
  PiecesPerHrs_Job,
  NormAmount_Job: Double;
  TheoTime, TheoProd: Double;
begin
  inherited;
  // 20015220 If nothing (no pieces) was found then exit this part
  // 20015220.100 (rework) This is WRONG! Only show nothing when also Hrs was 0!
  if (TotalJobDetailPieces = 0) and (TotalEmplPieces = 0)
    and (FJobWorkedHrs = 0) // 20015220.100
    and (TotalJobSalHrs = 0) // PIM-135
    then
  begin
    PrintBand := False;
    Exit;
  end;

  // CAR : Update progress indicator
  UpdateProgressIndicator;
  // No pieces found on employees?

  if TotalJobDetailPieces - TotalEmplPieces > 0 then
  begin
    QRMENr.Lines.Add('  -');
    //550297 - car  - fill total line per job
    QRMENr.Lines.Add('Total job');

    QRMEName.Lines.Add('  ' + SPimsNoEmployeeLine);
    //550297 - car  - fill job description
    QRMEName.Lines.Add(ReportProductionDetailDM.QueryProduction.
      FieldByName('JOB_CODE').Value + ' ' + Copy(MyJobDescription,1,12));
    QRMWrkHrs.Lines.Add('');
    QRMSalHrs.Lines.Add('');
    QRMDiffHrs.Lines.Add('');
    // fill total hours per job
    QRMWrkHrs.Lines.Add(DecodeHrsMin(FJobWorkedHrs, True));
    QRMSalHrs.Lines.Add(DecodeHrsSalMin(TotalJobSalHrs));
    QRMDiffHrs.Lines.Add(DecodeHrsDiffMin(TotalJobDiffHrs));

    QRMPieces.Lines.Add(Trim(Format('%8.0n',[TotalJobDetailPieces - TotalEmplPieces])));
     //550297 car  fill total pieces per job
    QRMPieces.Lines.Add(Trim(Format('%8.0n',  [TotalJobDetailPieces])));
    // fill bonus per no employee - line of memo field
    if QRParameters.FShowBonus then
    begin
      QRMBonus.Lines.Add('');
    end;
   // fill performance - efficiency per no employee line
   QRMPerf.Lines.Add('');
   // fill pieces per hour for no employee line
   QRMPiecesHr.Lines.Add('');
   //
   if QRParameters.FShowPayroll then
   begin
     QRMOvertime.Lines.Add('');
     QRMPayroll.Lines.Add('');
     QRMCostPiece.Lines.Add('');
     QRMCostsPerHr.Lines.Add('');
   end;
  end
  else
  begin
    TotalJobDetailPieces := TotalEmplPieces;
     //550297 car  -fill job code
    QRMENr.Lines.Add('Total job');
    // fill job description
    QRMEName.Lines.Add(ReportProductionDetailDM.QueryProduction.
      FieldByName('JOB_CODE').Value + ' ' + Copy(MyJobDescription,1,12));
       // fill worked hours per job
    QRMWrkHrs.Lines.Add(DecodeHrsMin(FJobWorkedHrs, True));
    QRMSalHrs.Lines.Add(DecodeHrsSalMin(TotalJobSalHrs));
    QRMDiffHrs.Lines.Add(DecodeHrsDiffMin(TotalJobDiffHrs));
    // fill pieces per job
    QRMPieces.Lines.Add(Trim(Format('%8.0n', [TotalEmplPieces])));
  end;
// 550297 fill bonus per jobcode
  if QRParameters.FShowBonus then
  begin
    QRMBonus.Lines.Add({'$'} CurrencyString + Trim(Format('%4.2n', [TotalJobIncentive])));
  end;

//550297 fill pieces per hour

  if FJobWorkedHrs > 0 then
  begin
    PiecesPerHrs_Job := TotalJobDetailPieces / (FJobWorkedHrs / 60);
    QRMPiecesHr.Lines.Add(Trim(Format('%8.0n', [PiecesPerHrs_Job])));
  end
  else
  begin
    QRMPiecesHr.Lines.Add(Trim(Format('%8.0n', [0.0])));
  end;

  // 20012858
{  TheoTime := 0;
  if ReportProductionDetailDM.QueryProduction.
    FieldByName('NORM_PROD_LEVEL').AsFloat <> 0 then
  begin
    TheoTime := TotalJobDetailPieces / ReportProductionDetailDM.QueryProduction.
      FieldByName('NORM_PROD_LEVEL').AsFloat * 60;
    TotalJobTheoTime := TotalJobTheoTime + TheoTime;
    TotalPlantTheoTime := TotalPlantTheoTime + TheoTime;
    TotalBUTheoTime := TotalBUTheoTime + TheoTime;
    TotalDeptTheoTime := TotalDeptTheoTime + TheoTime;
  end; }

  // 20012858
  TheoTime := -1;
  TheoProd := -1;
  TotalJobEfficiency := DetermineEfficiency(
    ReportProductionDetailDM.QueryProduction.
    FieldByName('NORM_PROD_LEVEL').AsFloat,
    FJobWorkedHrs, TotalJobDetailPieces, TheoTime, TheoProd);

  TotalJobTheoTime := TotalJobTheoTime + TheoTime;
  TotalPlantTheoTime := TotalPlantTheoTime + TheoTime;
  TotalBUTheoTime := TotalBUTheoTime + TheoTime;
  TotalDeptTheoTime := TotalDeptTheoTime + TheoTime;
{
  // 550297 fill efficiency per jobcode
  // 20012858
  if SystemDM.EffBasedOnQuantYN = CHECKEDVALUE then
  begin
    if (FJobWorkedHrs <> 0) and
      (ReportProductionDetailDM.QueryProduction.
         FieldByName('NORM_PROD_LEVEL').AsFloat <> 0)  then
      TotalJobEfficiency := ( TotalJobDetailPieces / (FJobWorkedHrs / 60)/
        ReportProductionDetailDM.QueryProduction.
        FieldByName('NORM_PROD_LEVEL').AsFloat)* 100
    else
      TotalJobEfficiency := 0;
  end
  else
  begin
    // 20012858
    // Eff. based on time
    if (FJobWorkedHrs <> 0) then
      TotalJobEfficiency := TheoTime / FJobWorkedHrs * 100
    else
      TotalJobEfficiency := 0;
  end;
}
// ROP 05-MAR-2013 TD-21527    QRMemoPerformanceP.Lines.Add(Format('%8.2f', [TotalJobEfficiency]) + '%')
  QRMPerf.Lines.Add(Trim(Format('%8.2n', [TotalJobEfficiency]) + '%'));
  //550297 fill payroll
  if QRParameters.FShowPayroll then
  begin
    QRMOvertime.Lines.Add(DecodeHrsMin(TotalEmplOvertime_PerJob, True));
// ROP 05-MAR-2013 TD-21527    QRMemoPayrollDollars.Lines.Add(Format('%8.2f', [TotalPayroll_PerJob]));
    QRMPayroll.Lines.Add(Trim(Format('%8.2n', [TotalPayroll_PerJob])));
  //550297 fill cost per pieces  and cost per hour
    JobCostsPerPiece := 0;
    if TotalJobDetailPieces > 0 then
      JobCostsPerPiece := TotalPayroll_PerJob / TotalJobDetailPieces;
    JobCostsPerHr := 0;
    if FJobWorkedHrs > 0 then
      JobCostsPerHr := TotalPayroll_PerJob / (FJobWorkedHrs / 60);

    QRMCostPiece.Lines.Add(Trim(Format('%8.3n', [JobCostsPerPiece])));
    QRMCostsPerHr.Lines.Add(Trim(Format('%8.2n', [JobCostsPerHr])));
  end;

  NormAmount_Job := ReportProductionDetailDM.QueryProduction.
    FieldByName('NORM_PROD_LEVEL').AsFloat * (FJobWorkedHrs / 60);

  TotalDeptNormAmount := TotalDeptNormAmount + NormAmount_Job;
  TotalBUNormAmount := TotalBUNormAmount + NormAmount_Job;
  TotalPlantNormAmount := TotalPlantNormAmount + NormAmount_Job;
  //
  TotalDetailPieces := TotalDetailPieces + TotalJobDetailPieces;
  // MRA: 13-NOV-2008 RV014. Totals are wrong! Always do this!
//  if QRParameters.FShowOnlyJob then
  begin

    TotalPlantPieces := TotalPlantPieces + TotalDetailPieces;
    TotalBUPieces := TotalBUPieces + TotalDetailPieces;
    TotalDeptPieces := TotalDeptPieces + TotalDetailPieces;

    TotalPlantWorkedHrs := TotalPlantWorkedHrs + TotalJobWorkedHrs;
    TotalBUWorkedHrs := TotalBUWorkedHrs + TotalJobWorkedHrs;
    TotalDeptWorkedHrs := TotalDeptWorkedHrs + TotalJobWorkedHrs;

    TotalPlantSalHrs := TotalPlantSalHrs + TotalJobSalHrs;
    TotalBUSalHrs := TotalBUSalHrs + TotalJobSalHrs;
    TotalDeptSalHrs := TotalDeptSalHrs + TotalJobSalHrs;

    TotalPlantDiffHrs := TotalPlantDiffHrs + TotalJobDiffHrs;
    TotalBUDiffHrs := TotalBUDiffHrs + TotalJobDiffHrs;
    TotalDeptDiffHrs := TotalDeptDiffHrs + TotalJobDiffHrs;

    TotalPlantOvertimeHrs := TotalPlantOvertimeHrs + TotalJobOvertimeHrs;
    TotalBUOvertimeHrs := TotalBUOvertimeHrs + TotalJobOvertimeHrs;
    TotalDeptOvertimeHrs := TotalDeptOvertimeHrs + TotalJobOvertimeHrs;

    TotalPlantPayrollDollars := TotalPlantPayrollDollars + TotalJobPayrollDollars;
    TotalBUPayrollDollars := TotalBUPayrollDollars + TotalJobPayrollDollars;
    TotalDeptPayrollDollars := TotalDeptPayrollDollars + TotalJobPayrollDollars;
  end;
  // RV080.1.
//  SetHeightMemoBand(PrintBand, QRBandFooterJob);

end;

procedure TReportProductionDetailQR.QRDBText11Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(FPlantDesc, 1, 10) + '...';
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRDBTextStandard2Print(sender: TObject;
  var Value: String);
begin
  inherited;
  // PIM-135
  Value := '';
//  if not QRParameters.FShowPayroll then
//   Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUSalesBPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowSales then
    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUSalesPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowSales  then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUIncomePerWorkedHourBPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowSales or not QRParameters.FShowBonus then
    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUMarginalRatePerHourPrint(
  sender: TObject; var Value: String);
begin
  inherited;
   if not QRParameters.FShowSales or not QRParameters.FShowBonus then
    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelWKBonusPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowBonus then
    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelWKPerformancePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if not QRParameters.FShowBonus then
    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUSalesNPPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll and QRParameters.FShowSales and
    not QRParameters.FShowBonus then
  else  
     Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPercPayrollToSalesNPPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if  QRParameters.FShowBonus or not QRParameters.FShowSales  then
     Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUIncomePerWorkedHourNPPrint(
  sender: TObject; var Value: String);
begin
  inherited;
 if  QRParameters.FShowBonus or not QRParameters.FShowSales  then
     Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalBUMarginalRatePerHourNPPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if  QRParameters.FShowBonus or not QRParameters.FShowSales  then
     Value := '';
end;

procedure TReportProductionDetailQR.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not QRParameters.FShowBonus and not QRParameters.FShowSales and
    QRParameters.FShowPayroll then
    PrintBand := True
  else
    PrintBand := False
end;

procedure TReportProductionDetailQR.QRLabelWKPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowOnlyJob then
    Value := SPimsJobs + ' (' + SPimsColumnWorkspot + ')'
  else
    Value := SPimsColumnWorkspot;
end;

procedure TReportProductionDetailQR.QRLabelJobPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowOnlyJob then
    Value := SStaffPlnEmployee
  else
    Value := SPimsJobs;
end;

procedure TReportProductionDetailQR.QRLabelEmplPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FShowOnlyJob then
   Value := ''
  else
    Value := SStaffPlnEmployee;
end;

procedure TReportProductionDetailQR.QRDBTextStandard1Print(sender: TObject;
  var Value: String);
begin
  inherited;
  // PIM-135
//  if QRParameters.FShowPayroll then
//    Value := '';
end;

procedure TReportProductionDetailQR.QRLabelWKOvertimeHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptPiecesPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelWKPiecesPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.ChildBandBU1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // PIM-135
  Exit;

  if not PrintBand then
    Exit;
  if FBU = '-1' then
  begin
    PrintBand := False;
    exit;
  end;
  if QRParameters.FShowPayroll AND  not QRParameters.FShowBonus then
  begin
    PrintBand := True;
    PrintBand := PrintFooterBU(PrintBand);
  end
  else
    PrintBand := False;
end;

procedure TReportProductionDetailQR.ChildBandBU2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // PIM-135
  Exit;
  if not PrintBand then
    Exit;
   if FBU = '-1' then
  begin
    PrintBand := False;
    exit;
  end;
  if not QRParameters.FShowPayroll AND  QRParameters.FShowBonus  then
  begin
    PrintBand := True;
    PrintBand := PrintFooterBU(PrintBand);
  end
  else
    PrintBand := False;
end;

procedure TReportProductionDetailQR.ChildBandBU3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False; // PIM-135
  Exit;
  if not PrintBand then
    Exit;
   if FBU = '-1' then
  begin
    PrintBand := False;
    exit;
  end;
  if not QRParameters.FShowPayroll AND not QRParameters.FShowBonus then
  begin
    PrintBand := True;
    PrintBand := PrintFooterBU(PrintBand);
  end
  else
    PrintBand := False;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantPiecesPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
  else
    Value := '';
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantPiecesPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPiecesPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPiecesPerHr1Print(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
end;

procedure TReportProductionDetailQR.QRDBText14Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FDept;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRDBText8Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FWK;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRDBText9Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FWKDesc;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRDBText6Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := FBU;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep +
      ExportClass.Sep; // 1 extra, because there's no description shown
end;

procedure TReportProductionDetailQR.QRDBText10Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value :=  FPlant;
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRDBText1Print(sender: TObject;
  var Value: String);
begin
  inherited;
  FDept := Value;
end;

procedure TReportProductionDetailQR.QRBandFooterDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FDept := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
//      ExportClass.AddText(ExportTotalDeptLine);
      ExportClass.AddText(QRLabelTotDept.Caption + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('DDESCRIPTION').AsString + ExportClass.Sep +
        ExportClass.Sep + ExportClass.Sep +
        QRLabelTotalDeptWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptPieces.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabelTotalDeptPiecesPerHr.Caption + ExportClass.Sep +
        QRLabelDeptEfficiency.Caption + ExportClass.Sep +
        QRLabelTotalDeptOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalDeptPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalDeptCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalDeptCostsPerHr.Caption + ExportClass.Sep +
        QRLabelDeptIncentive.Caption + ExportClass.Sep
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRBandFooterPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FPlant := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
//      ExportClass.AddText(ExportTotalPlantLine);
      ExportClass.AddText(
        QRLabelTotPlant.Caption +
        ReportProductionDetailDM.QueryProduction.FieldByName('PLANT_CODE').AsString + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('PDESCRIPTION').AsString + ExportClass.Sep +
        ExportClass.Sep + ExportClass.Sep +
        QRLabelTotalPlantWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantPieces.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabeltotalPlantPiecesPerHr.Caption + ExportClass.Sep +
        QRLabelPlantEfficiency.Caption + ExportClass.Sep +
        QRLabelTotalPlantOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalPlantPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalPlantCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalPlantCostsPerHr.Caption + ExportClass.Sep +
        QRLabelPlantIncentive.Caption + ExportClass.Sep
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRBandFooterBUAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FBU := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
//      ExportClass.AddText(ExportTotalBULine);
      ExportClass.AddText(QRLabelTotBU.Caption + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('BDESCRIPTION').AsString + ExportClass.Sep +
        ExportClass.Sep + ExportClass.Sep +
        QRLabelTotalBUWorkedHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUSalHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUDiffHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUPieces.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabelTotalBUPiecesPerHr.Caption + ExportClass.Sep +
        QRLabelBUEfficiency.Caption + ExportClass.Sep +
        QRLabelTotalBUOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelTotalBUPayrollDollars.Caption + ExportClass.Sep +
        QRLabelTotalBUCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelTotalBUCostsPerHr.Caption + ExportClass.Sep +
        QRLabelBUIncentive.Caption + ExportClass.Sep +
        QRLabelTotalBUSales.Caption + ExportClass.Sep +
        QRLabelTotalBUPercPayrollToSales.Caption + ExportClass.Sep +
        QRLabelTotalBUIncomePerWorkedHour.Caption + ExportClass.Sep +
        QRLabelTotalBUMarginalRatePerHour.Caption
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRLabelDeptEfficiencyPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelDeptIncentivePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FShowPayroll or not QRParameters.FShowBonus then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLblJobDescriptionPrint(
  Sender: TObject; var Value: String);
begin
  inherited;
// MR:05-06-2003
  // Because 'job-description' has been left out for
  // ShowOnlyJobs, we have to look the description up here.
  with ReportProductionDetailDM do
  begin
    if QRParameters.FShowOnlyJob then
    begin
      Value := '';
      qryJob.Close;
      qryJob.ParamByName('PLANT_CODE').AsString :=
        QueryProduction.FieldByName('PLANT_CODE').AsString;
      // MRA: 09-JUL-2009 Also filter on WORKSPOT.
      qryJob.ParamByName('WORKSPOT_CODE').AsString :=
        QueryProduction.FieldByName('WORKSPOT_CODE').AsString;
      qryJob.ParamByName('JOB_CODE').AsString :=
        QueryProduction.FieldByName('JOB_CODE').AsString;
      qryJob.Open;
      if not qryJob.IsEmpty then
        Value := qryJob.FieldByName('DESCRIPTION').AsString;
      // MRA: 09-JUL-2009 Also show workspot in report.
      Value := Value + '  (' +
        QueryProduction.FieldByName('WORKSPOT_CODE').AsString + ')';
    end
    else
      Value := QueryProduction.FieldByName('JDESCRIPTION').Value;
  end;
  MyJobDescription := Value;
end;

procedure TReportProductionDetailQR.ChildBand1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel82.Caption + ExportClass.Sep +
      QRLabel83.Caption + ExportClass.Sep +
      QRLabel100.Caption + ExportClass.Sep +
      QRLabel102.Caption + ' ' + QRLabel103.Caption + ExportClass.Sep +
      QRLabel101.Caption + ExportClass.Sep +
      QRLabel105.Caption + QRLabel104.Caption + ExportClass.Sep +
      QRLabel106.Caption + ' ' + QRLabel107.Caption + ExportClass.Sep +
      QRLabel108.Caption + ' ' + QRLabel109.Caption + ' ' + QRLabel110.Caption +
        ExportClass.Sep +
      QRLabel13.Caption + ' ' + QRLabel112.Caption + ' ' + QRLabel111.Caption +
        ExportClass.Sep +
//      QRLabel124.Caption + ExportClass.Sep +
      QRLabel114.Caption + QRLabel115.Caption + QRLabel116.Caption +
        ExportClass.Sep +
      QRLabel3.Caption + ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.QRGroupHdPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(
        QRLabel39.Caption + ExportClass.Sep +
        QueryProduction.FieldByName('PLANT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('PDESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRGroupHDBUAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(
        QRLabel40.Caption + ExportClass.Sep +
        QueryProduction.FieldByName('BUSINESSUNIT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('BDESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRGroupHDDEPTAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(
        QRLabel41.Caption + ExportClass.Sep +
        QueryProduction.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
        QueryProduction.FieldByName('DDESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRBandFooterJobAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  I: Integer;
  Line: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      for I := 0 to QRMENr.Lines.Count - 1 do
      begin
        Line := ExportClass.Sep + ExportClass.Sep;
        // A '-' in the Excel-cell gives a result of '#NAME?'.
        // So, here it is translated.
        //550297
        if QRMENr.Lines.Strings[I] <> '  -' then
          Line := Line + Trim(QRMENr.Lines.Strings[I]) + ' '
        else
          Line := Line + '';
        Line := Line + Trim(QRMEName.Lines.Strings[I]) + ExportClass.Sep;
        if I <= QRMWrkHrs.Lines.Count - 1 then
          Line := Line + QRMWrkHrs.Lines.Strings[I] + ExportClass.Sep
        else
        //550297
          if QRMENr.Lines.Strings[I] = '  -' then
            Line := Line + ExportClass.Sep; // skip a field
        if I <= QRMSalHrs.Lines.Count - 1 then
          Line := Line + QRMSalhrs.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMDiffHrs.Lines.Count - 1 then
          Line := Line + QRMDiffHrs.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMPieces.Lines.Count - 1 then
          Line := Line + QRMPieces.Lines.Strings[I] + ExportClass.Sep;
        Line := Line + ExportClass.Sep; // Standard (skip)
        if I <= QRMPiecesHr.Lines.Count - 1 then
          Line := Line + QRMPiecesHr.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMPerf.Lines.Count - 1 then
          Line := Line + QRMPerf.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMOvertime.Lines.Count - 1 then
          Line := Line + QRMOvertime.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMPayroll.Lines.Count - 1 then
          Line := Line + QRMPayroll.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMCostPiece.Lines.Count - 1 then
          Line := Line + QRMCostPiece.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMCostsPerHr.Lines.Count - 1 then
          Line := Line + QRMCostsPerHr.Lines.Strings[I] + ExportClass.Sep;
        if I <= QRMBonus.Lines.Count - 1 then
          Line := Line + QRMBonus.Lines.Strings[I] + ExportClass.Sep;
        ExportClass.AddText(Line);
      end;
    end;
  end;

end;

procedure TReportProductionDetailQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  ProgressIndicatorPos := 100;
  // SO-20013769
  LastPageWidth := qrptBase.QRPrinter.PaperWidthValue;
  LastPageLength := qrptBase.QRPrinter.PaperLengthValue;
  Screen.Cursor := crDefault;
  Update;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportProductionDetailQR.QRBandFooterWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
//      ExportClass.AddText(ExportTotalWKLine);
      ExportClass.AddText(
        ReportProductionDetailDM.QueryProduction.FieldByName('WORKSPOT_CODE').AsString + ' ' +
        ReportProductionDetailDM.QueryProduction.FieldByName('WDESCRIPTION').AsString +
        ExportClass.Sep + ExportClass.Sep +
        QRLabelWKWorkedHrs.Caption + ExportClass.Sep +
        QRLabelWKSalHrs.Caption + ExportClass.Sep +
        QRLabelWKDiffHrs.Caption + ExportClass.Sep +
        QRLabelWKPieces.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabelWKPiecesPerHr.Caption + ExportClass.Sep +
        ExportClass.Sep +
        QRLabelWKOvertimeHrs.Caption + ExportClass.Sep +
        QRLabelWKPayrollDollars.Caption + ExportClass.Sep +
        QRLabelWKCostsPerPiece.Caption + ExportClass.Sep +
        QRLabelWKCostsPerHr.Caption
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.ChildBandBU1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FBU := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(ExportTotalBULine);
    end;
  end;
end;

procedure TReportProductionDetailQR.ChildBandBU2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FBU := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(ExportTotalBULine);
    end;
  end;
end;

procedure TReportProductionDetailQR.ChildBandBU3AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if BandPrinted then
    FBU := '-1';
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(ExportTotalBULine);
    end;
  end;
end;

procedure TReportProductionDetailQR.QRGroupHDWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(
        QueryProduction.FieldByName('WORKSPOT_CODE').AsString +
          ExportClass.Sep +
        QueryProduction.FieldByName('WDESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRGroupHDJOBAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      ExportClass.AddText(
        ExportClass.Sep +
        QueryProduction.FieldByName('JOB_CODE').AsString + ' ' +
        MyJobDescription + ExportClass.Sep +
        QRLabel238.Caption + ': ' + ExportClass.Sep + // 'Standard: '
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep +
        ExportClass.Sep + 
        QueryProduction.FieldByName('NORM_PROD_LEVEL').AsString
        );
    end;
  end;
end;

procedure TReportProductionDetailQR.QRLabelTotDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptWorkedHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptPiecesPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotBUPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUWorkedHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPiecesPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUOvertimeHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPayrollDollarsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUCostsPerPiecePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUCostsPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelBUEfficiencyPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelBUIncentivePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll or not QRParameters.FShowBonus then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUPiecesPerHr2Print(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelBUEfficiency2Print(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelBUIncentive1Print(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FShowPayroll or not QRParameters.FShowBonus then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotPlantPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantWorkedHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantOvertimeHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantPayrollDollarsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantCostsPerPiecePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantCostsPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelPlantEfficiencyPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelPlantIncentivePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FShowPayroll or not QRParameters.FShowBonus then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelWKWorkedHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelWKPiecesPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptOvertimeHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptPayrollDollarsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptCostsPerHrPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptCostsPerPiecePrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if not QRParameters.FShowPayroll then
    Value := '';
  if QRParameters.FExportToFile then
    if Value <> '' then
      ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelMeasureHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(FJobWorkedHrs, True);
end;

procedure TReportProductionDetailQR.QRDBText15Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(FDeptDesc, 0, 10);
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.ChildBand12BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowPayroll and not QRParameters.FShowBonus and not QRParameters.fsHOWSales;
end;

procedure TReportProductionDetailQR.ChildBand11BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowPayroll and not QRParameters.FShowBonus and QRParameters.fShowSales;
end;

procedure TReportProductionDetailQR.ChildBand10BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowPayroll and QRParameters.FShowBonus and not QRParameters.fShowSales;
end;

procedure TReportProductionDetailQR.ChildBand9BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowPayroll and QRParameters.FShowBonus and QRParameters.fShowSales;
end;

procedure TReportProductionDetailQR.ChildBand8BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowBonus and not QRParameters.FShowSales and QRParameters.FShowPayroll;
end;

procedure TReportProductionDetailQR.ChildBand7BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowBonus and QRParameters.FShowSales and QRParameters.FShowPayroll;
end;

procedure TReportProductionDetailQR.ChildBand6BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := False;
//  PrintBand := not QRParameters.FShowSales and QRParameters.FShowBonus and QRParameters.FShowPayroll;
end;

procedure TReportProductionDetailQR.QRGroupColumnHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // PIM-135
  PrintBand := True;
//  PrintBand := QRParameters.FShowSales and QRParameters.FShowBonus and QRParameters.FShowPayroll;
end;

procedure TReportProductionDetailQR.ChildBand12AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel199.Caption + ExportClass.Sep +
      QRLabel200.Caption + ExportClass.Sep +
      QRLabel201.Caption + ExportClass.Sep +
      QRLabel202.Caption + ' ' + QRLabel203.Caption + ExportClass.Sep +
      QRLabel204.Caption + ExportClass.Sep +
      QRLabel206.Caption + ' ' + QRLabel207.Caption + ' ' + QRLabel208.Caption + ExportClass.Sep +
      QRLabel209.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;

end;

procedure TReportProductionDetailQR.ChildBand11AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel179.Caption + ExportClass.Sep +
      QRLabel180.Caption + ExportClass.Sep +
      QRLabel181.Caption + ExportClass.Sep +
      QRLabel182.Caption + ' ' + QRLabel183.Caption + ExportClass.Sep +
      QRLabel184.Caption + ExportClass.Sep +
      QRLabel186.Caption + ' ' + QRLabel187.Caption + ' ' + QRLabel188.Caption + ExportClass.Sep +
      QRLabel189.Caption + ExportClass.Sep +
      QRLabel210.Caption + ExportClass.Sep +
      QRLabel211.Caption + ' ' + QRLabel212.Caption + ' ' + QRLabel213.Caption + ' ' + QRLabel214.Caption + ' ' + ExportClass.Sep +
      QRLabel215.Caption + ' ' + QRLabel216.Caption + ' ' + QRLabel217.Caption + ExportClass.Sep +
      QRLabel218.Caption + ' ' + QRLabel219.Caption + ' ' + QRLabel220.Caption + ' ' + QRLabel221.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.ChildBand10AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel156.Caption + ExportClass.Sep +
      QRLabel157.Caption + ExportClass.Sep +
      QRLabel158.Caption + ExportClass.Sep +
      QRLabel159.Caption + ' ' + QRLabel160.Caption + ExportClass.Sep +
      QRLabel161.Caption + ExportClass.Sep +
      QRLabel163.Caption + ' ' + QRLabel164.Caption + ' ' +
      QRLabel165.Caption + ExportClass.Sep +
      QRLabel166.Caption + ' ' + QRLabel167.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.ChildBand9AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel144.Caption + ExportClass.Sep +
      QRLabel145.Caption + ExportClass.Sep +
      QRLabel146.Caption + ExportClass.Sep +
      QRLabel147.Caption + ' ' + QRLabel148.Caption + ExportClass.Sep +
      QRLabel149.Caption + ExportClass.Sep +
      QRLabel151.Caption + ' ' + QRLabel152.Caption + ' ' + QRLabel153.Caption + ExportClass.Sep +
      QRLabel154.Caption + ExportClass.Sep +
      QRLabel155.Caption + ExportClass.Sep +
      QRLabel168.Caption + ExportClass.Sep +
      QRLabel169.Caption + ' ' + QRLabel170.Caption + ' ' + QRLabel171.Caption + ' ' + QRLabel172.Caption + ExportClass.Sep +
      QRLabel173.Caption + ' ' + QRLabel174.Caption + ' ' + QRLabel175.Caption + ExportClass.Sep +
      QRLabel176.Caption + ' ' + QRLabel177.Caption + ' ' + QRLabel178.Caption + ' ' + QRLabel222.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.ChildBand8AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel35.Caption + ExportClass.Sep +
      QRLabel38.Caption + ExportClass.Sep +
      QRLabel119.Caption + ExportClass.Sep +
      QRLabel120.Caption + ' ' + QRLabel121.Caption + ExportClass.Sep +
      QRLabel122.Caption + ExportClass.Sep +
      QRLabel125.Caption + QRLabel126.Caption + ExportClass.Sep +
      QRLabel127.Caption + ' ' + QRLabel128.Caption + ExportClass.Sep +
      QRLabel129.Caption + ' ' + QRLabel130.Caption + ' ' + QRLabel131.Caption + ExportClass.Sep +
      QRLabel132.Caption + ' ' + QRLabel133.Caption + ' ' + QRLabel134.Caption + ExportClass.Sep +
      QRLabel136.Caption + QRLabel137.Caption + QRLabel138.Caption + ExportClass.Sep +
      QRLabel139.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.ChildBand7AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel3.Caption + ExportClass.Sep +
      QRLabel82.Caption + ExportClass.Sep +
      QRLabel83.Caption + ExportClass.Sep +
      QRLabel100.Caption + ' ' + QRLabel101.Caption + ExportClass.Sep +
      QRLabel102.Caption + ExportClass.Sep +
      QRLabel103.Caption + QRLabel104.Caption + ExportClass.Sep +
      QRLabel105.Caption + ' ' + QRLabel106.Caption + ExportClass.Sep +
      QRLabel107.Caption + ' ' + QRLabel108.Caption + ' ' + QRLabel109.Caption + ExportClass.Sep +
      QRLabel110.Caption + ' ' + QRLabel111.Caption + ' ' + QRLabel112.Caption + ExportClass.Sep +
      QRLabel114.Caption + QRLabel115.Caption + QRLabel116.Caption + ExportClass.Sep +
      QRLabel124.Caption + ExportClass.Sep +
      QRLabel140.Caption + QRLabel141.Caption + ' ' + QRLabel142.Caption + ' ' + QRLabel192.Caption + ' ' + QRLabel224.Caption + ExportClass.Sep +
      QRLabel225.Caption + ' ' + QRLabel226.Caption + ' ' + QRLabel227.Caption + ' ' + QRLabel228.Caption + ExportClass.Sep +
      QRLabel229.Caption + ' ' + QRLabel230.Caption + ' ' + QRLabel231.Caption + ' ' + QRLabel232.Caption + ExportClass.Sep + ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.ChildBand6AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel64.Caption + ExportClass.Sep +
      QRLabel65.Caption + ExportClass.Sep +
      QRLabel66.Caption + ExportClass.Sep +
      QRLabel67.Caption + ' ' + QRLabel68.Caption + ExportClass.Sep +
      QRLabel69.Caption + ExportClass.Sep +
      QRLabel70.Caption + QRLabel71.Caption + ExportClass.Sep +
      QRLabel72.Caption + ' ' + QRLabel73.Caption + ExportClass.Sep +
      QRLabel74.Caption + ' ' + QRLabel75.Caption + ' ' + QRLabel76.Caption + ExportClass.Sep +
      QRLabel77.Caption + ' ' + QRLabel78.Caption + ' ' + QRLabel79.Caption + ExportClass.Sep +
      QRLabel81.Caption + QRLabel84.Caption + QRLabel85.Caption + ExportClass.Sep +
      QRLabel86.Caption + ExportClass.Sep +
      QRLabel89.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep);
  end;
end;

procedure TReportProductionDetailQR.QRGroupColumnHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(
      QRLabel91.Caption + ExportClass.Sep + // Workspot
      QRLabel92.Caption + ExportClass.Sep + // Jobcode
      QRLabel93.Caption + ExportClass.Sep + // Employee
      QRLabel94.Caption + ' ' + QRLabel95.Caption + ExportClass.Sep + // Worked hours
      QRLabel6.Caption + ' ' + QRLabel7.Caption + ExportClass.Sep + // Salary hours
      QRLabel8.Caption + ExportClass.Sep + // Difference
      QRLabel96.Caption + ExportClass.Sep + // Quantity
      QRLabel238.Caption + ExportClass.Sep + // Standard
      QRLabel239.Caption + QRLabel240.Caption + QRLabel241.Caption + ExportClass.Sep + // Pieces per hour
      QRLabel242.Caption + ExportClass.Sep + // Efficiency
      QRLabel97.Caption + QRLabel98.Caption + ExportClass.Sep + // Over time
      QRLabel99.Caption + ' ' + QRLabel123.Caption + ExportClass.Sep + // Payroll $
      QRLabel143.Caption + ' ' + QRLabel190.Caption + ' ' + QRLabel234.Caption + ExportClass.Sep + // Cost per piece
      QRLabel235.Caption + ' ' + QRLabel236.Caption + ' ' + QRLabel237.Caption + ExportClass.Sep + // Labor rate per hour
      QRLabel243.Caption + ExportClass.Sep + // Incentive
      QRLabel244.Caption + ExportClass.Sep + // Sales
      QRLabel245.Caption + ' ' + QRLabel246.Caption + ' ' + QRLabel247.Caption + QRLabel248.Caption + ExportClass.Sep + // % Payroll to sales
      QRLabel249.Caption + ' ' + QRLabel250.Caption + ' ' + QRLabel251.Caption + ' ' + QRLabel252.Caption + ExportClass.Sep + // Income per worked hour
      QRLabel253.Caption + ' ' + QRLabel254.Caption + ' ' + QRLabel255.Caption + ' ' + QRLabel256.Caption + ExportClass.Sep // Marginal rate per hour
      );
  end;
end;

procedure TReportProductionDetailQR.QRGroup1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

// TD-24599 Also export shift!
procedure TReportProductionDetailQR.QRGroupHDShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  Shift: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportProductionDetailDM do
    begin
      if QueryProduction.FieldByName('SHIFT_NUMBER').AsString = '-999' then
        Shift := SPimsNoEmployeeLine
      else
        Shift := QueryProduction.FieldByName('SHIFT_NUMBER').AsString +
          ' ' + QueryProduction.FieldByName('SDESCRIPTION').AsString;
      ExportClass.AddText(
        QRLabel195.Caption + ExportClass.Sep +
        Shift
        );
    end;
  end;
end;

// TD-24599
procedure TReportProductionDetailQR.QRDBText5Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if Value = '-999' then
    Value := '-';
end;

// TD-24599
procedure TReportProductionDetailQR.QRDBTextSDESCRIPTIONPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if ReportProductionDetailDM.
    QueryProduction.FieldByName('SHIFT_NUMBER').AsString = '-999' then
    Value := SPimsNoEmployeeLine;
end;

// TD-26051
// Because of troubles with the translation-tool:
// We translate some properties here for the headers based on the top-header
procedure TReportProductionDetailQR.Translate;
begin
  // Workspot
  QRLabel64.Caption := QRLabel91.Caption;
  QRLabel3.Caption := QRLabel91.Caption;

  // Jobcode
  QRLabel65.Caption := QRLabel92.Caption;
  QRLabel82.Caption := QRlabel92.Caption;

  // Employee
  QRLabel66.Caption := QRLabel93.Caption;
  QRLabel83.Caption := QRLabel93.Caption;

  // Measured
  QRlabel100.Caption := QRLabel94.Caption;

  // hours
  QRLabel101.Caption := QRLabel95.Caption;

  // Quantity
  QRLabel69.Caption := QRLabel96.Caption;
  QRLabel102.Caption := QRLabel96.Caption;

  // Over
  QRlabel70.Caption := QRLabel97.Caption;
  QRLabel103.Caption := QRlabel97.Caption;
  QRLabel125.Caption := QRLabel97.Caption;

  // time
  QRLabel71.Caption := QRLabel98.Caption;
  QRLabel104.Caption := QRLabel98.Caption;
  QRLabel126.Caption := QRLabel98.Caption;

  // Incentive
  QRLabel167.Caption := QRLabel243.Caption;

  // Payroll
  QRLabel72.Caption := QRLabel99.Caption;
  QRLabel105.Caption := QRLabel99.Caption;
  QRlabel127.Caption := QRlabel99.Caption;

  // Cost
  QRLabel74.Caption := QRLabel143.Caption;
  QRlabel107.Caption := QRlabel143.Caption;
  QRlabel129.Caption := QRLabel143.Caption;

  // per
  QRLabel75.Caption := QRLabel190.Caption;
  QRLabel108.Caption := QRLabel190.Caption;
  QRLabel130.Caption := QRLabel190.Caption;

  // piece
  QRLabel76.Caption := QRLabel234.Caption;
  QRLabel109.Caption := QRLabel234.Caption;
  QRLabel131.Caption := QRLabel234.Caption;

  // Labor
  QRLabel77.Caption := QRLabel235.Caption;
  QRlabel110.Caption := QRLabel235.Caption;
  QRLabel132.Caption := QRlabel235.Caption;

  // rate per
  QRLabel78.Caption := QRlabel236.Caption;
  QRlabel111.Caption := QRLabel236.Caption;
  QRlabel133.Caption := QRlabel236.Caption;

  // hour
  QRLabel79.Caption := QRLabel237.Caption;
  QRlabel112.Caption := QRLabel237.Caption;
  QRlabel134.Caption := QRlabel237.Caption;

  // Standard
  QRlabel80.Caption := QRLabel238.Caption;
  QRlabel113.Caption := QRlabel238.Caption;
  QRlabel135.Caption := QRlabel238.Caption;

  // Pieces
  QRLabel81.Caption := QRlabel239.Caption;
  QRlabel114.Caption := QRLabel239.Caption;
  QRLabel136.Caption := QRLabel239.Caption;

  // per
  QRLabel84.Caption := QRLabel240.Caption;
  QRlabel115.Caption := QRLabel240.Caption;
  QRlabel137.Caption := QRlabel240.Caption;

  // hour
  QRLabel85.Caption := QRLabel241.Caption;
  QRlabel116.Caption := QRlabel241.Caption;
  QRLabel138.Caption := QRlabel241.Caption;

  // Efficiency
  QRLabel86.Caption := QRLabel242.Caption;
  QRlabel124.Caption := QRLabel242.Caption;
  QRLabel139.Caption := QRlabel242.Caption;

  // Incentive
  QRLabel89.Caption := QRLabel243.Caption;

  // Sales
  QRLabel140.Caption := QRLabel244.Caption;
  QRLabel168.Caption := QRlabel244.Caption;
  QRlabel210.Caption := QRlabel244.Caption;

  // %
  QRLabel141.Caption := QRLabel245.Caption;
  QRLabel169.Caption := QRLabel245.Caption;
  QRLabel211.Caption := QRLabel245.Caption;

  // payroll
  QRlabel142.Caption := QRLabel246.Caption;
  QRlabel170.Caption := QRlabel246.Caption;
  QRlabel212.Caption := QRlabel246.Caption;

  // to
  QRLabel192.Caption := QRLabel247.Caption;
  QRlabel171.Caption := QRlabel247.Caption;
  QRLabel213.Caption := QRlabel247.Caption;

  // sales
  QRLabel224.Caption := QRLabel248.Caption;
  QRlabel172.Caption := QRlabel248.Caption;
  QRlabel214.Caption := QRlabel248.Caption;

  // Income
  QRLabel225.Caption := QRLabel249.Caption;
  QRLabel173.Caption := QRlabel249.Caption;
  QRlabel215.Caption := QRlabel249.Caption;

  // per
  QRLabel226.Caption := QRLabel250.Caption;
  QRLabel4.Caption := QRLabel250.Caption;
  QRLabel5.Caption := QRLabel250.Caption;

  // worked
  QRLabel227.Caption := QRlabel251.Caption;
  QRlabel174.Caption := QRlabel251.Caption;
  QRlabel216.Caption := QRlabel251.Caption;

  // hour
  QRLabel228.Caption := QRLabel252.Caption;
  QRLabel175.Caption := QRlabel252.Caption;
  QRlabel217.Caption := QRLabel252.Caption;

  // Marginal
  QRLabel229.Caption := QRlabel253.Caption;
  QRlabel176.Caption := QRLabel253.Caption;
  QRLabel218.Caption := QRlabel253.Caption;

  // rate
  QRLabel230.Caption := QRlabel254.Caption;
  QRLabel177.Caption := QRlabel254.Caption;
  QRlabel219.Caption := QRlabel254.Caption;

  // per
  QRLabel231.Caption := QRLabel255.Caption;
  QRlabel178.Caption := QRlabel255.Caption;
  QRLabel220.Caption := QRlabel255.Caption;

  // hour
  QRLabel232.Caption := QRLabel256.Caption;
  QRlabel222.Caption := QRlabel256.Caption;
  QRLabel221.Caption := QRLabel256.Caption;

end; // Translate

// 20014550.50
procedure TReportProductionDetailQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  EmployeeNumber: Integer;
  HourlyWage, BonusPercentage: Double;
  BonusInMoney: Boolean;
  EmplPieces, EmplPayrollDollars,  EmplCostsPerPiece: Double;
  EmplWorkedHrs: Integer;
  EmplCostsPerHr, EmplPiecesPerHr: Double;
  PerformanceEmpl, BonusEmpl,  BonusFactor: Double;
  EmplOvertimeMinute{, EmpCount}: Integer;
//  EmpList: TStringList;
  TheoTime, TheoProd: Double;
  EmplSalHrs: Integer; // PIM-135
begin
  inherited;
  // 20014550.50 Determine values from a view
  PrintBand := False;
  with ReportProductionDetailDM do
  begin
    // PIM-12.4 When it is about a comparison-job then employee number is null
    if QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsString <> '' then
    begin
      EmployeeNumber := QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      QRMENr.Lines.Add('  ' + IntToStr(EmployeeNumber));
    end
    else
    begin
      // EmployeeNumber := -1;
      QRMENr.Lines.Add('  ');
    end;
    QRMEName.Lines.Add('  ' +
      Copy(QueryProduction.FieldByName('EDESCRIPTION').AsString,1,15));
    EmplWorkedHrs :=
      Round(QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60); // Convert to minutes!
    EmplSalHrs :=
      Round(QueryProduction.FieldByName('PRODHOURMIN').AsFloat); // PIM-135
    EmplPieces :=
      QueryProduction.FieldByName('EMPLOYEEQUANTITY').AsFloat;
    // Employee Worked Hours
    TotalJobWorkedHrs := TotalJobWorkedHrs +  EmplWorkedHrs;
    QRMWrkHrs.Lines.Add(DecodeHrsMin(EmplWorkedHrs, True));
    FJobWorkedHrs := FJobWorkedHrs + EmplWorkedHrs;
    // Employee Sal Hours
    TotalJobSalHrs := TotalJobSalHrs + EmplSalHrs;
    QRMSalHrs.Lines.Add(DecodeHrsSalMin(EmplSalHrs));
    // Employee Diff Hours
    TotalJobDiffHrs := TotalJobDiffHrs + (EmplWorkedHrs - EmplSalHrs);
    QRMDiffHrs.Lines.Add(DecodeHrsDiffMin(EmplWorkedHrs - EmplSalHrs));
    // Employee Total Pieces
    TotalJobDetailPieces := TotalJobDetailPieces + EmplPieces;
    TotalEmplPieces := TotalEmplPieces + EmplPieces;
    QRMPieces.Lines.Add(Trim(Format('%8.0n', [EmplPieces])));


    EmplPayrollDollars := 0;
    EmplCostsPerPiece := 0;
    EmplCostsPerHr := 0;
    EmplPiecesPerHr := 0;
//    EmplOvertimeMinute := 0;
    HourlyWage := 0;
    BonusPercentage := 0;
    BonusInMoney := False;

    TheoTime := -1;
    TheoProd := -1;
    PerformanceEmpl := DetermineEfficiency(
      QueryProduction.FieldByName('NORM_PROD_LEVEL').AsFloat,
      EmplWorkedHrs, EmplPieces, TheoTime, TheoProd);
    // PIM-12 Bugfix This must not be done! It is already calculate above!
{
    if QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat > 0 then
      PerformanceEmpl :=
        QueryProduction.FieldByName('EMPLOYEESECONDSNORM').AsFloat /
        QueryProduction.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat * 100;
}
    // PIM-135
    if QueryProduction.FieldByName('HOURLY_WAGE').AsString <> '' then
      HourlyWage := QueryProduction.FieldByName('HOURLY_WAGE').AsFloat;
    if QueryProduction.FieldByName('BONUS_FACTOR').AsString <> '' then
      BonusPercentage := QueryProduction.FieldByName('BONUS_FACTOR').AsFloat;

    QRMPerf.Lines.Add(Trim(Format('%8.2n', [PerformanceEmpl]) + '%'));
    if EmplWorkedHrs > 0 then
      EmplPiecesPerHr := EmplPieces / (EmplWorkedHrs / 60);
    if QRParameters.FShowPayroll then
    begin
      // PIM-135
      EmplOvertimeMinute := DetermineOvertime(
        QueryProduction.FieldByName('PLANT_CODE').AsString,
        QueryProduction.FieldByName('WORKSPOT_CODE').AsString,
        QueryProduction.FieldByName('JOB_CODE').AsString,
        QueryProduction.FieldByName('DEPARTMENT_CODE').AsString,
        QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        ContractGroupTFT,
        BonusInMoney);
(*
      with qryPHEPT do
      begin
        Filtered := False;
        Filter :=
          'PLANT_CODE=' +
          MyQuotes(QueryProduction.FieldByName('PLANT_CODE').AsString) +
          ' AND WORKSPOT_CODE=' +
          MyQuotes(QueryProduction.FieldByName('WORKSPOT_CODE').AsString) +
          ' AND JOB_CODE=' +
          MyQuotes(QueryProduction.FieldByName('JOB_CODE').AsString) +
          ' AND EMPLOYEE_NUMBER=' +
          QueryProduction.FieldByName('EMPLOYEE_NUMBER').AsString +
          ' AND DEPARTMENT_CODE=' +
          QueryProduction.FieldByName('DEPARTMENT_CODE').AsString +
          ' AND OVERTIME_YN=' + '''' + 'Y' + ''''; // PIM-137 Also look for this!
        Filtered := True;
        if not Eof then
        begin
        // PIMS-12 MRA:22-DEC-2015 Bugfix: Do not look for overtimemins!

          // PIM-135
          if FieldByName('OVERTIME_YN').AsString = 'Y' then
            EmplOvertimeMinute :=
              FieldByName('SUMPRODMIN').AsInteger; // OVERTIMEMINS
          // PIM-135 This is already assigned
{
          HourlyWage :=
            FieldByName('HOURLY_WAGE').AsFloat;
          BonusPercentage :=
            FieldByName('BONUS_PERCENTAGE').AsFloat;
}
          if ContractGroupTFT then
            BonusInMoney :=
              FieldByName('BONUS_IN_MONEY_YN').AsString = 'Y'
          else
            BonusInMoney :=
              FieldByName('HT_BONUS_IN_MONEY_YN').AsString = 'Y';
        end; // if
      end; // with
*)
      // Get Payroll
      if EmplWorkedHrs > 0 then
        TotalJobPayrollDollars := TotalJobPayrollDollars +
         (HourlyWage * (EmplWorkedHrs / 60))
      else
        TotalJobPayrollDollars := 0;
      if EmplWorkedHrs > 0 then
        TotalPayroll_PerJob := TotalPayroll_PerJob +
         (HourlyWage * (EmplWorkedHrs / 60))
      else
        TotalPayroll_PerJob := 0;
      if TotalJobWorkedHrs > 0 then
        EmplPayrollDollars := HourlyWage *
          (EmplWorkedHrs / 60);

      // Get Overtime, Employee Overtime Hrs
      TotalJobOvertimeHrs := TotalJobOvertimeHrs + EmplOvertimeMinute;
      QRMOvertime.Lines.Add(DecodeHrsMin(EmplOvertimeMinute, True));
      TotalEmplOvertime_PerJob := TotalEmplOvertime_PerJob +
        EmplOvertimeMinute;

      if (EmplOvertimeMinute > 0) and (BonusPercentage > 0) and
        (BonusInMoney) then
      begin
        TotalJobPayrollDollars := TotalJobPayrollDollars +
          (HourlyWage * BonusPercentage / 100 *
          (EmplOvertimeMinute / 60));
        TotalPayroll_PerJob := TotalPayroll_PerJob +
          (HourlyWage * BonusPercentage / 100 *
          (EmplOvertimeMinute / 60));
        EmplPayrollDollars := EmplPayrollDollars +
          (HourlyWage * BonusPercentage / 100 *
          (EmplOvertimeMinute / 60));
      end;

      QRMPayroll.Lines.Add(Trim(Format('%8.2n', [EmplPayrollDollars])));
    end;  // if QRParameters.FShowPayroll

    if EmplPieces > 0 then
      EmplCostsPerPiece := EmplPayrollDollars / EmplPieces;
    if EmplWorkedHrs > 0 then
    begin
      EmplCostsPerHr := EmplPayrollDollars / (EmplWorkedHrs / 60);
      EmplPiecesPerHr := EmplPieces / (EmplWorkedHrs / 60);
    end;
    if QRParameters.FShowPayroll then
    begin
      QRMCostPiece.Lines.Add(Trim(Format('%8.3n', [EmplCostsPerPiece])));
      QRMCostsPerHr.Lines.Add(Trim(Format('%8.2n', [EmplCostsPerHr])));
      QRMPiecesHr.Lines.Add(Trim(Format('%8.0n', [EmplPiecesPerHr])));
    end
    else
      QRMPiecesHr.Lines.Add(Trim(Format('%8.0n', [EmplPiecesPerHr])));
    if QRParameters.FShowBonus then
    begin
      BonusEmpl := 0;
      if PerformanceEmpl > 100 then
      begin
        BonusFactor := QueryProduction.FieldByName('BONUS_FACTOR').AsFloat;
        BonusEmpl := (PerformanceEmpl - 100) *
          EmplWorkedHrs * BonusFactor / 60;
      end;
      QRMBonus.Lines.Add({'$'} CurrencyString + Trim(Format('%4.2n', [BonusEmpl])));
      TotalDeptIncentive := TotalDeptIncentive +  BonusEmpl;
      TotalBUIncentive := TotalBUIncentive +  BonusEmpl;
      TotalPlantIncentive := TotalPlantIncentive + BonusEmpl;
      //550297 - calculate bonus incentive per jobcode level
      TotalJobIncentive :=  TotalJobIncentive + BonusEmpl;
    end; // if QRParameters.FShowBonus then
  end; // with ReportProductionDetailDM do
end;

procedure TReportProductionDetailQR.QRLabelBlankPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptSalHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalDeptDiffHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalDeptLine := ExportTotalDeptLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUSalHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalBUDiffHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalBULine := ExportTotalBULine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantSalHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelTotalPlantDiffHrsPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalPlantLine := ExportTotalPlantLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelWKSalHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

procedure TReportProductionDetailQR.QRLabelWKDiffHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FExportToFile then
    ExportTotalWKLine := ExportTotalWKLine + Value + ExportClass.Sep;
end;

// PIM-135
function TReportProductionDetailQR.DecodeHrsSalMin(
  AValue: Integer): String;
begin
  Result := '';
end;

// PIM-135
function TReportProductionDetailQR.DecodeHrsDiffMin(
  AValue: Integer): String;
begin
  Result := '';
end;

end.
