(*
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
*)
unit DialogReportQualityIncCalculationFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, Grids, DBGrids, dxLayout, dxExGrEd, dxExELib;

type
  TDialogReportQualityIncCalculationF = class(TDialogReportBaseF)
    Label3: TLabel;
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label6: TLabel;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    CheckBoxShowInfo: TCheckBox;
    CheckBoxCalculateWithOvertimeBonus: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private

  public
    { Public declarations }
    Constructor Create(Sender: TComponent; TeamCode: String = '';
      PlantCode: String = '' ; AYear: Integer = 0;
      AWeek: Integer = 0);reintroduce; overload;
  end;

var
  DialogReportQualityIncCalculationF: TDialogReportQualityIncCalculationF;

// RV089.1.
function DialogReportQualityIncCalculationForm: TDialogReportQualityIncCalculationF;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportQualityIncCalculationDMT, ListProcsFRM, UPimsMessageRes,
  ReportQualityIncCalculationQRPT;

// RV089.1.
var
  DialogReportQualityIncCalculationF_HND: TDialogReportQualityIncCalculationF;

// RV089.1.
function DialogReportQualityIncCalculationForm: TDialogReportQualityIncCalculationF;
begin
  if (DialogReportQualityIncCalculationF_HND = nil) then
  begin
    DialogReportQualityIncCalculationF_HND := TDialogReportQualityIncCalculationF.Create(Application);
    with DialogReportQualityIncCalculationF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportQualityIncCalculationF_HND;
end;

// RV089.1.
procedure TDialogReportQualityIncCalculationF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportQualityIncCalculationF_HND <> nil) then
  begin
    DialogReportQualityIncCalculationF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportQualityIncCalculationF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

constructor TDialogReportQualityIncCalculationF.Create(Sender: TComponent;
	TeamCode, PlantCode: String; AYear, AWeek: Integer);
var
  Year, Week: Word;
begin
  inherited Create(Sender);

  InitDialog(True, False, False, False, False, False, False);
  year := AYear;
  week := AWeek;
  if AYear <= 0 then
    ListProcsF.WeekUitDat(Now, Year, Week);
  dxSpinEditYear.IntValue := Year;
  dxSpinEditWeek.IntValue := Week;
  dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(Year);
// CREATE data module of report
  ReportQualityIncCalculationDM := CreateReportDM(TReportQualityIncCalculationDM);
  ReportQualityIncCalculationQR := CreateReportQR(TReportQualityIncCalculationQR);

  ListProcsF.FillComboBoxPlant(tblPlant, True, CmbPlusPlantFrom);
  ListProcsF.FillComboBoxPlant(tblPlant, False, CmbPlusPlantTo);
  if AYear > 0 then
  begin
    TblPlant.FindKey([PlantCode]);
    CmbPlusPlantFrom.DisplayValue := PlantCode;
    CmbPlusPlantTo.DisplayValue := PlantCode;
  end;
end;

procedure TDialogReportQualityIncCalculationF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportQualityIncCalculationQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    Round(dxSpinEditYear.Value),
    Round(dxSpinEditWeek.Value),
    CheckBoxShowSelection.Checked,
    CheckBoxShowInfo.Checked,
    CheckBoxCalculateWithOvertimeBonus.Checked) then
    ReportQualityIncCalculationQR.ProcessRecords;
end;

// MR:05-12-2003
procedure TDialogReportQualityIncCalculationF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportQualityIncCalculationF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportQualityIncCalculationF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
