inherited EmployeeContractsDM: TEmployeeContractsDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 286
  Top = 158
  Height = 446
  Width = 677
  inherited TableMaster: TTable
    Left = 100
    object TableMasterEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object TableMasterSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Required = True
      Size = 6
    end
    object TableMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 40
    end
    object TableMasterDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 40
    end
    object TableMasterZIPCODE: TStringField
      FieldName = 'ZIPCODE'
    end
    object TableMasterCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object TableMasterSTATE: TStringField
      FieldName = 'STATE'
      Size = 6
    end
    object TableMasterLANGUAGE_CODE: TStringField
      FieldName = 'LANGUAGE_CODE'
      Size = 3
    end
    object TableMasterPHONE_NUMBER: TStringField
      FieldName = 'PHONE_NUMBER'
      Size = 15
    end
    object TableMasterEMAIL_ADDRESS: TStringField
      FieldName = 'EMAIL_ADDRESS'
      Size = 40
    end
    object TableMasterDATE_OF_BIRTH: TDateTimeField
      FieldName = 'DATE_OF_BIRTH'
    end
    object TableMasterSEX: TStringField
      FieldName = 'SEX'
      Required = True
      Size = 1
    end
    object TableMasterSOCIAL_SECURITY_NUMBER: TStringField
      FieldName = 'SOCIAL_SECURITY_NUMBER'
    end
    object TableMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object TableMasterTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Size = 6
    end
    object TableMasterENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
    end
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableMasterIS_SCANNING_YN: TStringField
      FieldName = 'IS_SCANNING_YN'
      Size = 1
    end
    object TableMasterCUT_OF_TIME_YN: TStringField
      FieldName = 'CUT_OF_TIME_YN'
      Size = 1
    end
    object TableMasterREMARK: TStringField
      FieldName = 'REMARK'
      Size = 60
    end
    object TableMasterCALC_BONUS_YN: TStringField
      FieldName = 'CALC_BONUS_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_YN: TStringField
      FieldName = 'FIRSTAID_YN'
      Size = 1
    end
    object TableMasterFIRSTAID_EXP_DATE: TDateTimeField
      FieldName = 'FIRSTAID_EXP_DATE'
    end
    object TableMasterPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableMasterDEPARTMENTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DEPARTMENTLU'
      LookupKeyFields = 'DEPARTMENT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DEPARTMENT_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableMasterSTARTDATECAL: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'STARTDATECAL'
      Calculated = True
    end
    object TableMasterEMPCALC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
  end
  inherited TableDetail: TTable
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    MasterFields = 'EMPLOYEE_NUMBER'
    TableName = 'EMPLOYEECONTRACT'
    Left = 100
    Top = 108
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'EMPLOYEE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailENDDATE: TDateTimeField
      FieldName = 'ENDDATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailHOURLY_WAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'HOURLY_WAGE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailCONTRACT_TYPE: TIntegerField
      Alignment = taLeftJustify
      DefaultExpression = '1'
      FieldName = 'CONTRACT_TYPE'
      MaxValue = 2
    end
    object TableDetailCONTRACT_HOUR_PER_WEEK: TFloatField
      Alignment = taLeftJustify
      FieldName = 'CONTRACT_HOUR_PER_WEEK'
      MaxValue = 168
    end
    object TableDetailCONTRACT_DAY_PER_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'CONTRACT_DAY_PER_WEEK'
      MaxValue = 7
    end
    object TableDetailGUARANTEED_DAYS: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'GUARANTEED_DAYS'
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableDetailHOLIDAY_HOUR_PER_YEAR: TFloatField
      Alignment = taLeftJustify
      FieldName = 'HOLIDAY_HOUR_PER_YEAR'
    end
    object TableDetailSENIORITY_HOURS: TFloatField
      FieldName = 'SENIORITY_HOURS'
    end
    object TableDetailWORKER_YN: TStringField
      FieldName = 'WORKER_YN'
      Size = 1
    end
    object TableDetailWORKSCHEDULE_ID: TFloatField
      FieldName = 'WORKSCHEDULE_ID'
    end
    object TableDetailLUWORKSCHEDULE: TStringField
      FieldKind = fkLookup
      FieldName = 'LUWORKSCHEDULE'
      LookupDataSet = qryWorkSchedule
      LookupKeyFields = 'WORKSCHEDULE_ID'
      LookupResultField = 'CODEDESC'
      KeyFields = 'WORKSCHEDULE_ID'
      Size = 40
      Lookup = True
    end
  end
  inherited DataSourceMaster: TDataSource
    DataSet = QueryMaster
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 108
  end
  inherited TableExport: TTable
    Left = 316
    Top = 220
  end
  inherited DataSourceExport: TDataSource
    Left = 424
    Top = 220
  end
  object QueryMaster: TQuery
    OnCalcFields = QueryMasterCalcFields
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME, E.DESCRIPTION,'
      '  E.PLANT_CODE, P.DESCRIPTION AS PLANTLU, '
      '  E.STARTDATE, '
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTLU,'
      '  NVL(CNT.CODE, '#39'*'#39') COUNTRYLU'
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      '  INNER JOIN DEPARTMENT D ON'
      '    D.PLANT_CODE = E.PLANT_CODE AND'
      '    D.DEPARTMENT_CODE = E.DEPARTMENT_CODE'
      '  LEFT JOIN COUNTRY CNT ON'
      '    P.COUNTRY_ID = CNT.COUNTRY_ID'
      'WHERE'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      '')
    Left = 24
    Top = 48
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryMasterFLOAT_EMP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object QueryMasterEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object QueryMasterSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Size = 6
    end
    object QueryMasterPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryMasterPLANTLU: TStringField
      FieldName = 'PLANTLU'
      Size = 30
    end
    object QueryMasterDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryMasterDEPARTMENTLU: TStringField
      FieldName = 'DEPARTMENTLU'
      Size = 30
    end
    object QueryMasterSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
    end
    object QueryMasterCOUNTRYLU: TStringField
      FieldName = 'COUNTRYLU'
      Size = 3
    end
  end
  object ClientDataSetMaster: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderMaster'
    Left = 312
    Top = 48
  end
  object DataSetProviderMaster: TDataSetProvider
    DataSet = QueryMaster
    Constraints = True
    Left = 424
    Top = 48
  end
  object qryCheckBI: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  STARTDATE, ENDDATE '
      'FROM '
      '  EMPLOYEECONTRACT'
      'WHERE '
      '  EMPLOYEE_NUMBER = :CURRENT_EMPLOYEE_NUMBER')
    Left = 32
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CURRENT_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryCheckBU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  STARTDATE, ENDDATE '
      'FROM '
      '  EMPLOYEECONTRACT'
      'WHERE '
      '  (EMPLOYEE_NUMBER = :CURRENT_EMPLOYEE_NUMBER) AND '
      '  (STARTDATE <> :OLD_STARTDATE)')
    Left = 32
    Top = 232
    ParamData = <
      item
        DataType = ftInteger
        Name = 'CURRENT_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'OLD_STARTDATE'
        ParamType = ptUnknown
      end>
  end
  object qryLastEmployeeContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MAX(ENDDATE) AS ENDDATE'
      'FROM '
      '  EMPLOYEECONTRACT'
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER')
    Left = 312
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryWorkSchedule: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  WORKSCHEDULE_ID, CODE || '#39' | '#39' || DESCRIPTION CODEDESC'
      'FROM'
      '  WORKSCHEDULE'
      'ORDER BY'
      '  CODE  '
      ' ')
    Left = 32
    Top = 288
  end
end
