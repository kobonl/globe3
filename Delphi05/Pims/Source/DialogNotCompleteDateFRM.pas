unit DialogNotCompleteDateFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, DBPicker, dxCntner, dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TDialogNotCompleteDateF = class(TDialogBaseF)
    gBoxDateTime: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    dxDateEditDate: TdxDateEdit;
    dxTimeEditTime: TdxTimeEdit;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    FMyDateTimeOut: TDateTime;
    procedure SetMyDateTimeOut(const Value: TDateTime);
    { Private declarations }
  public
    { Public declarations }
    property MyDateTimeOut: TDateTime read FMyDateTimeOut
      write SetMyDateTimeOut;
  end;

var
  DialogNotCompleteDateF: TDialogNotCompleteDateF;

implementation

{$R *.DFM}

{ TDialogNotCompleteDateF }

procedure TDialogNotCompleteDateF.SetMyDateTimeOut(const Value: TDateTime);
begin
  FMyDateTimeOut := Value;
end;

procedure TDialogNotCompleteDateF.btnOkClick(Sender: TObject);
begin
  inherited;
  MyDateTimeOut := Trunc(dxDateEditDate.Date) + Frac(dxTimeEditTime.Time);
end;

procedure TDialogNotCompleteDateF.FormShow(Sender: TObject);
begin
  inherited;
  dxDateEditDate.Date := Trunc(MyDateTimeOut);
  dxTimeEditTime.Time := Frac(MyDateTimeOut);
  // First jump to time-field.
  dxTimeEditTime.SetFocus;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogNotCompleteDateF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
