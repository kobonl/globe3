inherited IllnessMessagesDM: TIllnessMessagesDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 192
  Top = 138
  Height = 487
  Width = 821
  inherited TableMaster: TTable
    BeforePost = nil
    BeforeDelete = nil
    OnDeleteError = nil
    OnEditError = nil
    OnNewRecord = nil
    OnPostError = nil
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    Left = 60
    Top = 28
  end
  inherited TableDetail: TTable
    BeforeEdit = TableDetailBeforeEdit
    BeforePost = TableDetailBeforePost
    AfterPost = TableDetailAfterPost
    AfterCancel = TableDetailAfterCancel
    BeforeDelete = TableDetailBeforeDelete
    AfterDelete = TableDetailAfterDelete
    OnNewRecord = TableDetailNewRecord
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    IndexFieldNames = 'ILLNESSMESSAGE_STARTDATE;EMPLOYEE_NUMBER'
    MasterSource = nil
    TableName = 'ILLNESSMESSAGE'
    Left = 60
    Top = 104
    object TableDetailILLNESSMESSAGE_STARTDATE: TDateTimeField
      Tag = 1
      FieldName = 'ILLNESSMESSAGE_STARTDATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailEMPLOYEE_NUMBER: TIntegerField
      Tag = 1
      FieldName = 'EMPLOYEE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailILLNESSMESSAGE_ENDDATE: TDateTimeField
      FieldName = 'ILLNESSMESSAGE_ENDDATE'
      OnChange = TableDetailILLNESSMESSAGE_ENDDATEChange
    end
    object TableDetailPROCESSED_YN: TStringField
      FieldName = 'PROCESSED_YN'
      Size = 1
    end
    object TableDetailABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 1
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailABSENCEREASONLU: TStringField
      FieldKind = fkLookup
      FieldName = 'ABSENCEREASONLU'
      LookupDataSet = TableAbsenceReason
      LookupKeyFields = 'ABSENCEREASON_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCEREASON_CODE'
      LookupCache = True
      Lookup = True
    end
    object TableDetailEMPLOYEELU: TStringField
      FieldKind = fkLookup
      FieldName = 'EMPLOYEELU'
      LookupDataSet = cdsFilterEmployee
      LookupKeyFields = 'EMPLOYEE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'EMPLOYEE_NUMBER'
      LookupCache = True
      Lookup = True
    end
    object TableDetailLINE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'LINE_NUMBER'
      Required = True
    end
  end
  inherited DataSourceMaster: TDataSource
    DataSet = cdsMaster
    Left = 188
    Top = 28
  end
  inherited DataSourceDetail: TDataSource
    Left = 188
    Top = 104
  end
  inherited TableExport: TTable
    Left = 480
    Top = 320
  end
  inherited DataSourceExport: TDataSource
    Left = 576
    Top = 320
  end
  object DataSourceAbsenceReason: TDataSource
    DataSet = qryAbsenceReason
    Left = 196
    Top = 172
  end
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 304
    Top = 104
  end
  object dspFilterEmployee: TDataSetProvider
    DataSet = QueryEmployee
    Constraints = True
    Left = 192
    Top = 312
  end
  object cdsFilterEmployee: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspFilterEmployee'
    StoreDefs = True
    Left = 56
    Top = 312
  end
  object QueryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      '')
    Left = 56
    Top = 256
  end
  object dspMaster: TDataSetProvider
    DataSet = QueryEmployee
    Constraints = True
    Left = 296
    Top = 24
  end
  object cdsMaster: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byNumber'
        Fields = 'EMPLOYEE_NUMBER'
      end>
    IndexName = 'byNumber'
    Params = <>
    ProviderName = 'dspMaster'
    StoreDefs = True
    Left = 384
    Top = 24
  end
  object qryMaxLine: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MAX(LINE_NUMBER) AS MAXLINE'
      'FROM '
      '  ILLNESSMESSAGE'
      'WHERE '
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '')
    Left = 632
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryUpdate: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 384
    Top = 104
  end
  object qryIMCheck1BI: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*) AS RECCOUNT'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NULL) AND'
      '  (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER)'
      ' ')
    Left = 520
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryIMCheck2BI: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT ILLNESSMESSAGE_STARTDATE,ILLNESSMESSAGE_ENDDATE'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NOT NULL) AND'
      '  (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER)'
      ' ')
    Left = 520
    Top = 144
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryIMCheck3BI: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT ILLNESSMESSAGE_ENDDATE'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NOT NULL) AND'
      ' (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER)'
      ' ')
    Left = 520
    Top = 192
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryIMCheck1BU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*) AS RECCOUNT'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NULL) AND'
      '  (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER) AND'
      '  (ILLNESSMESSAGE_STARTDATE <> :OLD_ILLNESSMESSAGE_STARTDATE)'
      ' '
      ' ')
    Left = 608
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'OLD_ILLNESSMESSAGE_STARTDATE'
        ParamType = ptUnknown
      end>
  end
  object qryIMCheck2BU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT ILLNESSMESSAGE_STARTDATE,ILLNESSMESSAGE_ENDDATE'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NOT NULL) AND'
      '  (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER) AND'
      '  (ILLNESSMESSAGE_STARTDATE <> :OLD_ILLNESSMESSAGE_STARTDATE)'
      ' ')
    Left = 608
    Top = 144
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'OLD_ILLNESSMESSAGE_STARTDATE'
        ParamType = ptUnknown
      end>
  end
  object qryIMCheck3BU: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT ILLNESSMESSAGE_ENDDATE'
      'FROM ILLNESSMESSAGE'
      'WHERE (ILLNESSMESSAGE_ENDDATE IS NOT NULL) AND'
      ' (EMPLOYEE_NUMBER = :NEW_EMPLOYEE_NUMBER) AND'
      ' (ILLNESSMESSAGE_STARTDATE <> :OLD_ILLNESSMESSAGE_STARTDATE)'
      '  '
      ' ')
    Left = 608
    Top = 192
    ParamData = <
      item
        DataType = ftInteger
        Name = 'NEW_EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'OLD_ILLNESSMESSAGE_STARTDATE'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select * from absencereason order by description')
    Left = 56
    Top = 208
  end
  object TableAbsenceReason: TTable
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    TableName = 'ABSENCEREASON'
    Left = 52
    Top = 168
    object TableAbsenceReasonABSENCEREASON_ID: TIntegerField
      FieldName = 'ABSENCEREASON_ID'
      Required = True
    end
    object TableAbsenceReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableAbsenceReasonCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableAbsenceReasonHOURTYPE_NUMBER: TIntegerField
      FieldName = 'HOURTYPE_NUMBER'
      Required = True
    end
    object TableAbsenceReasonMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableAbsenceReasonMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableAbsenceReasonPAYED_YN: TStringField
      FieldName = 'PAYED_YN'
      Required = True
      Size = 1
    end
    object TableAbsenceReasonOVERRULE_WITH_ILLNESS_YN: TStringField
      FieldName = 'OVERRULE_WITH_ILLNESS_YN'
      Required = True
      Size = 1
    end
  end
end
