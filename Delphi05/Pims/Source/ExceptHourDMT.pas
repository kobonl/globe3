(*
  Changes:
    MRA:1-APR-2009 RV025.
      Addition of extra hourtype to make it possible to book exceptional
      hours made during overtime.
    SO:04-AUG-2010 RV067.6. 550512
      Copy function exceptional hours
    MRA:12-FEB-2018 GLOB3-81
    - Related to this order: Display of description made larger of hourtype,
      it was 20, but should be 30 (TableDetailHOURTYPELU).
*)

unit ExceptHourDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TExceptHourDM = class(TGridBaseDM)
    TableDetailDAY_OF_WEEK: TIntegerField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    TableDetailSTARTTIME: TDateTimeField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailENDTIME: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterTIME_FOR_TIME_YN: TStringField;
    TableMasterBONUS_IN_MONEY_YN: TStringField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableMasterMUTATOR: TStringField;
    TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableMasterWEEKS_IN_PERIOD: TIntegerField;
    TableMasterWORK_TIME_REDUCTION_YN: TStringField;
    TableHourType: TTable;
    TableHourTypeHOURTYPE_NUMBER: TIntegerField;
    TableHourTypeDESCRIPTION: TStringField;
    TableHourTypeCREATIONDATE: TDateTimeField;
    TableHourTypeOVERTIME_YN: TStringField;
    TableHourTypeMUTATIONDATE: TDateTimeField;
    TableHourTypeMUTATOR: TStringField;
    TableHourTypeCOUNT_DAY_YN: TStringField;
    TableHourTypeEXPORT_CODE: TStringField;
    TableDetailHOURTYPELU: TStringField;
    TableDayTable: TTable;
    DataSourceHourType: TDataSource;
    DataSourceDayTable: TDataSource;
    TableDayTableDAY_NUMBER: TIntegerField;
    TableDayTableDAY_CODE: TStringField;
    TableDayTableDESCRIPTION: TStringField;
    TableDetailDAYLU: TStringField;
    TableHourTypeBONUS_PERCENTAGE: TFloatField;
    TableExceptionalHrs: TTable;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    DateTimeField4: TDateTimeField;
    StringField2: TStringField;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    TableDetailDAYDESC: TStringField;
    TableDetailOVERTIME_HOURTYPE_NUMBER: TIntegerField;
    TableHourTypeOvertime: TTable;
    IntegerField3: TIntegerField;
    StringField5: TStringField;
    DateTimeField5: TDateTimeField;
    StringField6: TStringField;
    DateTimeField6: TDateTimeField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    FloatField1: TFloatField;
    DataSourceHourtypeOvertime: TDataSource;
    TableDetailOVERTIME_HOURTYPELU: TStringField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    function ExceptionalHoursDefinedFor(AContractGroupCode: String): Boolean;
    procedure CopyExceptionalHours(ASrcContractGroupCode, ADestContractGroupCode: String; AHoursDefined: Boolean);
  end;

var
  ExceptHourDM: TExceptHourDM;

implementation

uses
  SystemDMT, UPimsMessageRes;

{$R *.DFM}

procedure TExceptHourDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not TableDayTable.IsEmpty then
  begin
    TableDayTable.First;
    TableDetail.FieldByName('DAY_OF_WEEK').Value :=
      TableDayTable.FieldByName('DAY_NUMBER').Value;
  end;
  if not TableHourType.IsEmpty then
  begin
    TableHourType.First;
    TableDetail.FieldByName('HOURTYPE_NUMBER').Value :=
      TableHourType.FieldByName('HOURTYPE_NUMBER').Value;
  end;

  TableExceptionalHrs.Close;
  TableExceptionalHrs.Open;
  TableExceptionalHrs.Last;
  TableDetail.FieldByName('STARTTIME').Value :=
    TableExceptionalHrs.FieldByName('STARTTIME').Value;
  TableDetail.FieldByName('ENDTIME').Value :=
    TableExceptionalHrs.FieldByName('ENDTIME').Value;
  if TableDetail.FieldByName('STARTTIME').Value = Null then
    TableDetail.FieldByName('STARTTIME').Value := 0;
  if TableDetail.FieldByName('ENDTIME').Value = Null then
    TableDetail.FieldByName('ENDTIME').Value := 0;
end;

procedure TExceptHourDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  if TableDetail.FieldByName('STARTTIME').Value = Null then
    TableDetail.FieldByName('STARTTIME').Value := 0;
  if TableDetail.FieldByName('ENDTIME').Value = Null then
    TableDetail.FieldByName('ENDTIME').Value := 0;
  // Pims -> Oracle - Check if starttime > endtime
  if (TableDetail.FieldByName('STARTTIME').Value >
    TableDetail.FieldByName('ENDTIME').Value) then
  begin
    DisplayMessage(SPimsStartEndTime, mtError, [mbOk]);
    Abort;
  end;
end;

procedure TExceptHourDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  TableDetailDAYDESC.Value :=
    SystemDM.GetDayWDescription(TableDetail.FieldByName('DAY_OF_WEEK').AsInteger);
end;

//RV067.6.
function TExceptHourDM.ExceptionalHoursDefinedFor(
  AContractGroupCode: String): Boolean;
var
  HoursFound: Integer;
begin
  HoursFound := SystemDM.GetDBValue(
    Format('SELECT COUNT(*) FROM EXCEPTIONALHOURDEF E ' +
           ' WHERE E.CONTRACTGROUP_CODE = ''%s''', [AContractGroupCode]),
           0);
  Result := HoursFound <> 0;
end;

procedure TExceptHourDM.CopyExceptionalHours(ASrcContractGroupCode,
  ADestContractGroupCode: String; AHoursDefined: Boolean);
begin
  if AHoursDefined then
    SystemDM.ExecSql(
      Format('DELETE FROM EXCEPTIONALHOURDEF E ' +
             ' WHERE E.CONTRACTGROUP_CODE = ''%s''', [ADestContractGroupCode])
             );

  SystemDM.ExecSql(
    Format(
      ' INSERT INTO ' +
      '   EXCEPTIONALHOURDEF(DAY_OF_WEEK, CONTRACTGROUP_CODE, ' +
      '   STARTTIME, ENDTIME, HOURTYPE_NUMBER, OVERTIME_HOURTYPE_NUMBER, ' +
      '   CREATIONDATE, MUTATIONDATE, MUTATOR) ' +
      ' SELECT ' +
      '   E.DAY_OF_WEEK, ''%s'', E.STARTTIME, E.ENDTIME, E.HOURTYPE_NUMBER, ' +
      '   E.OVERTIME_HOURTYPE_NUMBER, SYSDATE, SYSDATE, ''%s'' ' +
      ' FROM ' +
      '   EXCEPTIONALHOURDEF E ' +
      ' WHERE ' +
      '  E.CONTRACTGROUP_CODE = ''%s''',
    [ADestContractGroupCode, SystemDM.CurrentProgramUser, ASrcContractGroupCode]
    )
  );
  TableDetail.Refresh;
end;

end.
