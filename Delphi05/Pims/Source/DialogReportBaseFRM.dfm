inherited DialogReportBaseF: TDialogReportBaseF
  Left = 290
  Top = 114
  Caption = 'Report Dialog'
  ClientWidth = 635
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 635
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 518
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 635
    Caption = ''
    Font.Color = clBlack
    Font.Height = -11
    TabOrder = 4
    object LblFromPlant: TLabel
      Left = 8
      Top = 17
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblPlant: TLabel
      Left = 40
      Top = 17
      Width = 24
      Height = 13
      Caption = 'Plant'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromEmployee: TLabel
      Left = 8
      Top = 92
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblEmployee: TLabel
      Left = 40
      Top = 92
      Width = 46
      Height = 13
      Caption = 'Employee'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToPlant: TLabel
      Left = 315
      Top = 17
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToEmployee: TLabel
      Left = 315
      Top = 92
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarEmployeeFrom: TLabel
      Left = 128
      Top = 92
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarEmployeeTo: TLabel
      Left = 344
      Top = 92
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblFromDepartment: TLabel
      Left = 8
      Top = 42
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblDepartment: TLabel
      Left = 40
      Top = 42
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarDepartmentFrom: TLabel
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarDepartmentTo: TLabel
      Left = 344
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToDepartment: TLabel
      Left = 315
      Top = 43
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarTeamTo: TLabel
      Left = 344
      Top = 68
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToTeam: TLabel
      Left = 315
      Top = 68
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarTeamFrom: TLabel
      Left = 128
      Top = 68
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblTeam: TLabel
      Left = 40
      Top = 66
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromTeam: TLabel
      Left = 8
      Top = 66
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromShift: TLabel
      Left = 8
      Top = 116
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblShift: TLabel
      Left = 40
      Top = 116
      Width = 22
      Height = 13
      Caption = 'Shift'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStartShiftFrom: TLabel
      Left = 128
      Top = 118
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToShift: TLabel
      Left = 315
      Top = 118
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarShiftTo: TLabel
      Left = 344
      Top = 118
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarPlantFrom: TLabel
      Left = 128
      Top = 19
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblStarPlantTo: TLabel
      Left = 344
      Top = 19
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblFromPlant2: TLabel
      Left = 8
      Top = 143
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblPlant2: TLabel
      Left = 40
      Top = 143
      Width = 24
      Height = 13
      Caption = 'Plant'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarPlant2From: TLabel
      Left = 128
      Top = 145
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToPlant2: TLabel
      Left = 315
      Top = 143
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarPlant2To: TLabel
      Left = 344
      Top = 145
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblFromWorkspot: TLabel
      Left = 8
      Top = 168
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblWorkspot: TLabel
      Left = 40
      Top = 168
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarWorkspotFrom: TLabel
      Left = 128
      Top = 170
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToWorkspot: TLabel
      Left = 315
      Top = 168
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarWorkspotTo: TLabel
      Left = 344
      Top = 170
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblWorkspots: TLabel
      Left = 8
      Top = 168
      Width = 51
      Height = 13
      Caption = 'Workspots'
    end
    object BtnWorkspots: TSpeedButton
      Left = 528
      Top = 165
      Width = 20
      Height = 22
      Caption = '...'
      Flat = True
      OnClick = BtnWorkspotsClick
    end
    object LblStarJobTo: TLabel
      Left = 344
      Top = 195
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblToJob: TLabel
      Left = 315
      Top = 193
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblStarJobFrom: TLabel
      Left = 128
      Top = 195
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LblJob: TLabel
      Left = 40
      Top = 193
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromJob: TLabel
      Left = 8
      Top = 193
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblFromDateTime: TLabel
      Left = 8
      Top = 218
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblDateTime: TLabel
      Left = 40
      Top = 218
      Width = 49
      Height = 13
      Caption = 'Date-Time'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToDateTime: TLabel
      Left = 315
      Top = 218
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblFromDateBase: TLabel
      Left = 8
      Top = 218
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblDateBase: TLabel
      Left = 40
      Top = 218
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblToDateBase: TLabel
      Left = 315
      Top = 218
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CmbPlusPlantFrom: TComboBoxPlus
      Left = 120
      Top = 15
      Width = 180
      Height = 19
      ColCount = 158
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPlantFromCloseUp
    end
    object CmbPlusPlantTo: TComboBoxPlus
      Left = 342
      Top = 15
      Width = 180
      Height = 19
      ColCount = 159
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPlantToCloseUp
    end
    object CmbPlusTeamFrom: TComboBoxPlus
      Left = 120
      Top = 65
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusTeamFromCloseUp
    end
    object CmbPlusTeamTo: TComboBoxPlus
      Left = 342
      Top = 65
      Width = 180
      Height = 19
      ColCount = 161
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusTeamToCloseUp
    end
    object CheckBoxAllTeams: TCheckBox
      Left = 530
      Top = 68
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 22
      OnClick = CheckBoxAllTeamsClick
    end
    object CmbPlusDepartmentFrom: TComboBoxPlus
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 159
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusDepartmentFromCloseUp
    end
    object CmbPlusDepartmentTo: TComboBoxPlus
      Left = 342
      Top = 41
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusDepartmentToCloseUp
    end
    object CheckBoxAllDepartments: TCheckBox
      Left = 530
      Top = 44
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 21
      OnClick = CheckBoxAllDepartmentsClick
    end
    object dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 120
      Top = 90
      Width = 180
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Style.BorderStyle = xbsSingle
      Style.ButtonStyle = btsDefault
      TabOrder = 6
      AutoSize = False
      DataField = 'VALUEDISPLAY'
      DataSource = DataSourceEmplFrom
      PopupHeight = 150
      PopupWidth = 350
      OnCloseUp = dxDBExtLookupEditEmplFromCloseUp
      DBGridLayout = dxDBGridLayoutListEmployeeFrom
      Height = 19
    end
    object dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 342
      Top = 90
      Width = 180
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Style.BorderStyle = xbsSingle
      Style.ButtonStyle = btsDefault
      TabOrder = 7
      AutoSize = False
      DataField = 'VALUEDISPLAY'
      DataSource = DataSourceEmplTo
      PopupHeight = 150
      PopupWidth = 350
      OnCloseUp = dxDBExtLookupEditEmplToCloseUp
      DBGridLayout = dxDBGridLayoutListEmployeeTo
      Height = 19
    end
    object CmbPlusShiftFrom: TComboBoxPlus
      Left = 120
      Top = 115
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusShiftFromCloseUp
    end
    object CmbPlusShiftTo: TComboBoxPlus
      Left = 342
      Top = 115
      Width = 180
      Height = 19
      ColCount = 161
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusShiftToCloseUp
    end
    object CheckBoxAllShifts: TCheckBox
      Left = 530
      Top = 115
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 24
      OnClick = CheckBoxAllShiftsClick
    end
    object CheckBoxIncludeNotConnectedEmp: TCheckBox
      Left = 8
      Top = 240
      Width = 289
      Height = 17
      Caption = 'Include employees not connected to teams'
      Enabled = False
      TabOrder = 23
      Visible = False
      OnClick = CheckBoxIncludeNotConnectedEmpClick
    end
    object CheckBoxAllEmployees: TCheckBox
      Left = 530
      Top = 90
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 25
      OnClick = CheckBoxAllEmployeesClick
    end
    object CheckBoxAllPlants: TCheckBox
      Left = 530
      Top = 19
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 26
      OnClick = CheckBoxAllPlantsClick
    end
    object CmbPlusPlant2From: TComboBoxPlus
      Left = 120
      Top = 141
      Width = 180
      Height = 19
      ColCount = 159
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPlant2FromCloseUp
    end
    object CmbPlusPlant2To: TComboBoxPlus
      Left = 342
      Top = 141
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 11
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusPlant2ToCloseUp
    end
    object CmbPlusWorkspotFrom: TComboBoxPlus
      Left = 120
      Top = 166
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusWorkspotFromCloseUp
    end
    object CmbPlusWorkspotTo: TComboBoxPlus
      Left = 342
      Top = 166
      Width = 180
      Height = 19
      ColCount = 161
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusWorkspotToCloseUp
    end
    object EditWorkspots: TEdit
      Left = 120
      Top = 166
      Width = 402
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      TabOrder = 14
    end
    object CmbPlusJobTo: TComboBoxPlus
      Left = 342
      Top = 191
      Width = 180
      Height = 19
      ColCount = 161
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 16
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusJobToCloseUp
    end
    object CmbPlusJobFrom: TComboBoxPlus
      Left = 120
      Top = 191
      Width = 180
      Height = 19
      ColCount = 160
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 15
      TitleColor = clBtnFace
      OnCloseUp = CmbPlusJobFromCloseUp
    end
    object dxTimeEditFrom: TdxTimeEdit
      Left = 236
      Top = 216
      Width = 65
      TabOrder = 18
      TimeEditFormat = tfHourMin
      StoredValues = 4
    end
    object dxTimeEditTo: TdxTimeEdit
      Left = 458
      Top = 216
      Width = 65
      TabOrder = 20
      TimeEditFormat = tfHourMin
      StoredValues = 4
    end
    object DatePickerFrom: TDateTimePicker
      Left = 120
      Top = 216
      Width = 113
      Height = 21
      CalAlignment = dtaLeft
      Date = 41824.4050508681
      Time = 41824.4050508681
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 17
    end
    object DatePickerTo: TDateTimePicker
      Left = 342
      Top = 216
      Width = 113
      Height = 21
      CalAlignment = dtaLeft
      Date = 41824.4050508681
      Time = 41824.4050508681
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 19
    end
    object DatePickerFromBase: TDateTimePicker
      Left = 120
      Top = 216
      Width = 177
      Height = 21
      CalAlignment = dtaLeft
      Date = 41824.4050508681
      Time = 41824.4050508681
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 27
      OnCloseUp = DatePickerFromBaseCloseUp
    end
    object DatePickerToBase: TDateTimePicker
      Left = 342
      Top = 216
      Width = 179
      Height = 21
      CalAlignment = dtaLeft
      Date = 41824.4050508681
      Time = 41824.4050508681
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 28
      OnCloseUp = DatePickerToBaseCloseUp
    end
  end
  inherited stbarBase: TStatusBar
    Width = 635
  end
  inherited pnlBottom: TPanel
    Width = 635
    inherited btnOk: TBitBtn
      Left = 208
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 322
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 592
    Top = 152
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 592
  end
  inherited StandardMenuActionList: TActionList
    Left = 592
    Top = 72
  end
  object tblPlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = tblPlantFilterRecord
    TableName = 'PLANT'
    Left = 72
    Top = 24
    object tblPlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object tblPlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object tblPlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object tblPlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object tblPlantCITY: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object tblPlantSTATE: TStringField
      FieldName = 'STATE'
    end
    object tblPlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object tblPlantFAX: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object tblPlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object tblPlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object tblPlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object tblPlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object tblPlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object tblPlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object tblPlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object QueryEmplFrom: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.EMPLOYEE_NUMBER || '#39' | '#39' || E.DESCRIPTION AS VALUEDISPLAY,'
      '  E.DESCRIPTION,'
      '  E.SHORT_NAME,'
      '  E.DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.ADDRESS'
      'FROM  '
      '  EMPLOYEE E '
      'WHERE'
      '  (:USER_NAME = '#39'*'#39') OR'
      '  (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME)'
      '  )'
      'ORDER BY '
      '  E.EMPLOYEE_NUMBER'
      ' ')
    Left = 168
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryEmplFromPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'EMPLOYEE.PLANT_CODE'
      Size = 6
    end
    object QueryEmplFromEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
      Origin = 'EMPLOYEE.EMPLOYEE_NUMBER'
    end
    object QueryEmplFromVALUEDISPLAY: TStringField
      FieldName = 'VALUEDISPLAY'
      Origin = 'EMPLOYEE.EMPLOYEE_NUMBER'
      Size = 83
    end
    object QueryEmplFromDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'EMPLOYEE.DESCRIPTION'
      Size = 40
    end
    object QueryEmplFromSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Origin = 'EMPLOYEE.SHORT_NAME'
      Size = 6
    end
    object QueryEmplFromDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Origin = 'EMPLOYEE.DEPARTMENT_CODE'
      Size = 6
    end
    object QueryEmplFromTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Origin = 'EMPLOYEE.TEAM_CODE'
      Size = 6
    end
    object QueryEmplFromADDRESS: TStringField
      FieldName = 'ADDRESS'
      Origin = 'EMPLOYEE.ADDRESS'
      Size = 40
    end
  end
  object DataSourceEmplFrom: TDataSource
    DataSet = QueryEmplFrom
    Left = 280
    Top = 24
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 528
    Top = 24
    object dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        44040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507244469616C6F675265706F727442617365462E4461746153
        6F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A650B0E
        6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67106564
        676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E
        6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F43
        616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F4361
        6E496E73657274116564676F43616E4E617669676174696F6E116564676F436F
        6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264731065
        64676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D
        6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265720653
        6F7274656407046373557005576964746802410942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E
        554D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E5368
        6F72744E616D650743617074696F6E060A53686F7274206E616D650557696474
        6802540942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060A53484F52545F4E414D4500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557696474
        6803B4000942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741646472
        65737305576964746802450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506074144445245535300000F5464784442477269
        64436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F
        4465706172746D656E7420636F646505576964746802580942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F444550415254
        4D454E545F434F444500000F546478444247726964436F6C756D6E0A436F6C75
        6D6E5465616D0743617074696F6E06095465616D20636F64650942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
    object dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507224469616C6F675265706F72
        7442617365462E44617461536F75726365456D706C546F104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802310942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D65055769647468024E0942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07436170
        74696F6E06044E616D6505576964746803CC000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E06074164647265737305576964746802650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        5245535300000F546478444247726964436F6C756D6E0E436F6C756D6E446570
        74436F64650743617074696F6E060F4465706172746D656E7420636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
  end
  object DataSourceEmplTo: TDataSource
    DataSet = QueryEmplTo
    Left = 468
    Top = 24
  end
  object QueryEmplTo: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.EMPLOYEE_NUMBER || '#39' | '#39' || E.DESCRIPTION AS VALUEDISPLAY,'
      '  E.DESCRIPTION,'
      '  E.SHORT_NAME,'
      '  E.DEPARTMENT_CODE,'
      '  E.TEAM_CODE,'
      '  E.ADDRESS'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  (:USER_NAME = '#39'*'#39') OR'
      '  (E.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME)'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' ')
    Left = 384
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object QueryEmplToPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'EMPLOYEE.PLANT_CODE'
      Size = 6
    end
    object QueryEmplToEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
      Origin = 'EMPLOYEE.EMPLOYEE_NUMBER'
    end
    object QueryEmplToVALUEDISPLAY: TStringField
      FieldName = 'VALUEDISPLAY'
      Origin = 'EMPLOYEE.EMPLOYEE_NUMBER'
      Size = 83
    end
    object QueryEmplToDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'EMPLOYEE.DESCRIPTION'
      Size = 40
    end
    object QueryEmplToSHORT_NAME: TStringField
      FieldName = 'SHORT_NAME'
      Origin = 'EMPLOYEE.SHORT_NAME'
      Size = 6
    end
    object QueryEmplToDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Origin = 'EMPLOYEE.DEPARTMENT_CODE'
      Size = 6
    end
    object QueryEmplToTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Origin = 'EMPLOYEE.TEAM_CODE'
      Size = 6
    end
    object QueryEmplToADDRESS: TStringField
      FieldName = 'ADDRESS'
      Origin = 'EMPLOYEE.ADDRESS'
      Size = 40
    end
  end
  object qryDept: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = qryDeptFilterRecord
    SQL.Strings = (
      'SELECT'
      ' DISTINCT '
      ' D.PLANT_CODE,'
      ' D.DEPARTMENT_CODE,'
      ' D.DESCRIPTION'
      'FROM'
      ' DEPARTMENT D LEFT JOIN DEPARTMENTPERTEAM T ON'
      '   D.PLANT_CODE = T.PLANT_CODE AND'
      '   D.DEPARTMENT_CODE = T.DEPARTMENT_CODE'
      'WHERE'
      ' D.PLANT_CODE = :PLANT_CODE AND'
      ' ('
      '   (:USER_NAME = '#39'*'#39') OR'
      '   (T.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME)'
      '   )'
      ' )'
      'ORDER BY'
      ' D.DEPARTMENT_CODE'
      ' ')
    Left = 104
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object qryDeptPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'DEPARTMENT.PLANT_CODE'
      Size = 6
    end
    object qryDeptDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Origin = 'DEPARTMENT.DEPARTMENT_CODE'
      Size = 6
    end
    object qryDeptDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'DEPARTMENT.DESCRIPTION'
      Size = 30
    end
  end
  object qryTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = qryTeamFilterRecord
    SQL.Strings = (
      'SELECT PLANT_CODE, TEAM_CODE, DESCRIPTION'
      'FROM TEAM T'
      'WHERE'
      '  (T.PLANT_CODE = :PLANT_CODE) AND'
      '  ('
      '  (:USER_NAME = '#39'*'#39') OR'
      '  (T.TEAM_CODE IN'
      
        '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME =' +
        ' :USER_NAME)'
      '  )'
      '  )'
      'ORDER BY TEAM_CODE'
      ' ')
    Left = 136
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
    object qryTeamPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Origin = 'TEAM.PLANT_CODE'
      Size = 6
    end
    object qryTeamTEAM_CODE: TStringField
      FieldName = 'TEAM_CODE'
      Origin = 'TEAM.TEAM_CODE'
      Size = 6
    end
    object qryTeamDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Origin = 'TEAM.DESCRIPTION'
      Size = 30
    end
  end
  object qryShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_NUMBER,'
      '  S.PLANT_CODE,'
      '  S.DESCRIPTION'
      'FROM '
      '  SHIFT S'
      'WHERE'
      '  S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  S.SHIFT_NUMBER')
    Left = 200
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object tblPlant2: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = tblPlantFilterRecord
    TableName = 'PLANT'
    Left = 232
    Top = 24
    object StringField1: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object StringField2: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'ADDRESS'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'ZIPCODE'
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'CITY'
      Size = 30
    end
    object StringField6: TStringField
      FieldName = 'STATE'
    end
    object StringField7: TStringField
      FieldName = 'PHONE'
      Size = 15
    end
    object StringField8: TStringField
      FieldName = 'FAX'
      Size = 15
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object IntegerField1: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
    end
    object IntegerField2: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField9: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object IntegerField3: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
    end
    object IntegerField4: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
    end
  end
  object qryWorkspot: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      ' W.WORKSPOT_CODE, W.DESCRIPTION, W.PLANT_CODE'
      'FROM'
      ' WORKSPOT W'
      'LEFT JOIN DEPARTMENT D ON'
      ' W.PLANT_CODE = D.PLANT_CODE AND'
      ' W.DEPARTMENT_CODE = D.DEPARTMENT_CODE'
      'LEFT JOIN DEPARTMENTPERTEAM T ON'
      ' D.PLANT_CODE = T.PLANT_CODE AND'
      ' D.DEPARTMENT_CODE = T.DEPARTMENT_CODE'
      'WHERE'
      ' W.PLANT_CODE  = :PLANT_CODE AND'
      ' ('
      '   (:USER_NAME = '#39'*'#39') OR'
      '   (T.TEAM_CODE IN'
      
        '       (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME ' +
        '= :USER_NAME)'
      '   )'
      ' )'
      'ORDER BY'
      ' W.WORKSPOT_CODE')
    Left = 320
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object qryPivotWorkspot: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  CASE'
      '    WHEN A.CNT = 0 THEN'
      '      '#39'<ALL>'#39
      '    WHEN A.CNT = 1 THEN'
      '      (SELECT W.WORKSPOT_CODE || '#39' | '#39' || W.DESCRIPTION'
      '       FROM WORKSPOT W INNER JOIN PIVOTWORKSPOT PW ON'
      '         W.PLANT_CODE = PW.PLANT_CODE AND'
      '         W.WORKSPOT_CODE = PW.WORKSPOT_CODE'
      '       WHERE PW.PIVOTCOMPUTERNAME = :COMPUTERNAME AND'
      '       PW.PLANT_CODE = :PLANT_CODE)'
      
        '    WHEN A.CNT = (SELECT COUNT(*) FROM WORKSPOT WHERE PLANT_CODE' +
        ' = :PLANT_CODE) THEN'
      '      '#39'<ALL>'#39
      '    WHEN A.CNT > 1 THEN'
      '      '#39'<MULTIPLE>'#39
      '  END RESULT'
      'FROM'
      '('
      '  SELECT COUNT(*) CNT'
      '  FROM PIVOTWORKSPOT'
      '  WHERE PIVOTCOMPUTERNAME = :COMPUTERNAME AND'
      '    PLANT_CODE = :PLANT_CODE'
      ') A'
      ' ')
    Left = 440
    Top = 253
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryJob: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM'
      '  JOBCODE J'
      'WHERE'
      '  J.PLANT_CODE = :PLANT_CODE'
      '  AND J.WORKSPOT_CODE = :WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE'
      ''
      ' ')
    Left = 352
    Top = 21
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
end
