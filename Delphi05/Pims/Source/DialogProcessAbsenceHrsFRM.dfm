inherited DialogProcessAbsenceHrsF: TDialogProcessAbsenceHrsF
  Left = 400
  Top = 162
  Caption = 'Process Absence Hours'
  ClientHeight = 498
  ClientWidth = 629
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 629
    inherited imgOrbit: TImage
      Left = 512
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 629
    Height = 377
    inherited LblFromEmployee: TLabel
      Top = 68
    end
    inherited LblEmployee: TLabel
      Top = 68
    end
    inherited LblToEmployee: TLabel
      Top = 68
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 68
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 68
    end
    inherited LblFromDepartment: TLabel
      Top = 466
    end
    inherited LblDepartment: TLabel
      Top = 466
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 468
    end
    inherited LblStarDepartmentTo: TLabel
      Top = 468
    end
    inherited LblToDepartment: TLabel
      Top = 467
    end
    inherited LblStarTeamTo: TLabel
      Top = 44
    end
    inherited LblToTeam: TLabel
      Top = 44
    end
    inherited LblStarTeamFrom: TLabel
      Top = 44
    end
    inherited LblTeam: TLabel
      Top = 42
    end
    inherited LblFromTeam: TLabel
      Top = 42
    end
    object Label1: TLabel [18]
      Left = 8
      Top = 489
      Width = 51
      Height = 13
      Caption = 'From Plant'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [19]
      Left = 324
      Top = 489
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel [20]
      Left = 8
      Top = 516
      Width = 53
      Height = 13
      Caption = 'From Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label4: TLabel [21]
      Left = 324
      Top = 517
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label5: TLabel [22]
      Left = 8
      Top = 441
      Width = 50
      Height = 13
      Caption = 'From Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [23]
      Left = 324
      Top = 545
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object LblFromDate: TLabel [24]
      Left = 8
      Top = 97
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblDate: TLabel [25]
      Left = 40
      Top = 97
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LblToDate: TLabel [26]
      Left = 315
      Top = 97
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 215
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 216
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 41
      ColCount = 217
      TabOrder = 3
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 41
      ColCount = 218
      TabOrder = 4
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 44
      ParentFont = False
      TabOrder = 5
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 465
      ColCount = 216
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Top = 465
      ColCount = 217
      TabOrder = 21
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Top = 468
      TabOrder = 22
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 66
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 66
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 207
      TabOrder = 11
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 208
      TabOrder = 12
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 30
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 185
      TabOrder = 20
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 186
      TabOrder = 23
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 186
      TabOrder = 31
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 187
      TabOrder = 32
    end
    inherited EditWorkspots: TEdit
      TabOrder = 13
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 165
      TabOrder = 15
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 164
      TabOrder = 29
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 17
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 19
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 16
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 18
    end
    object pnlResults: TPanel
      Left = 0
      Top = 142
      Width = 629
      Height = 235
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'pnlResults'
      TabOrder = 14
      object grpBxResults: TGroupBox
        Left = 0
        Top = 0
        Width = 629
        Height = 190
        Align = alClient
        Caption = 'Results'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object mmLog: TMemo
          Left = 2
          Top = 15
          Width = 625
          Height = 173
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          Lines.Strings = (
            '')
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 190
        Width = 629
        Height = 45
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object grpBxProgress: TGroupBox
          Left = 0
          Top = 0
          Width = 629
          Height = 45
          Align = alClient
          Caption = 'Progress'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object pBar: TProgressBar
            Left = 2
            Top = 15
            Width = 625
            Height = 14
            Align = alTop
            Min = 0
            Max = 100
            TabOrder = 0
          end
          object pBar2: TProgressBar
            Left = 2
            Top = 29
            Width = 625
            Height = 14
            Align = alClient
            Min = 0
            Max = 100
            TabOrder = 1
          end
        end
      end
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 94
      Width = 100
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 8
      OnCloseUp = DateFromCloseUp
      OnChange = DateFromChange
    end
    object DateTo: TDateTimePicker
      Left = 344
      Top = 94
      Width = 100
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 9
      OnCloseUp = DateToCloseUp
      OnChange = DateToChange
    end
    object ckRecalculateHours: TCheckBox
      Left = 8
      Top = 122
      Width = 137
      Height = 17
      Caption = 'Recalculate Hours'
      Checked = True
      State = cbChecked
      TabOrder = 10
    end
  end
  inherited stbarBase: TStatusBar
    Top = 479
    Width = 629
  end
  inherited pnlBottom: TPanel
    Top = 438
    Width = 629
    inherited btnOk: TBitBtn
      Left = 184
    end
    inherited btnCancel: TBitBtn
      OnClick = btnCancelClick
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 600
    Top = 120
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Top = 92
  end
  inherited StandardMenuActionList: TActionList
    Left = 600
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4B040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072B4469616C6F6750726F63657373416273656E6365487273
        462E44617461536F75726365456D706C46726F6D104F7074696F6E7343757374
        6F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E645369
        7A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E
        53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E734442
        0B106564676F43616E63656C4F6E457869740D6564676F43616E44656C657465
        0D6564676F43616E496E73657274116564676F43616E4E617669676174696F6E
        116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265
        636F726473106564676F557365426F6F6B6D61726B7300000F54647844424772
        6964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E
        756D62657206536F7274656407046373557005576964746802410942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F454D50
        4C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F43
        6F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E61
        6D6505576964746802540942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060A53484F52545F4E414D4500000F546478444247
        726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E61
        6D6505576964746803B4000942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060B4445534352495054494F4E00000F54647844
        4247726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F
        6E06074164647265737305576964746802450942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D6506074144445245535300000F54
        6478444247726964436F6C756D6E0E436F6C756D6E44657074436F6465074361
        7074696F6E060F4465706172746D656E7420636F646505576964746802580942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        12040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507294469616C6F6750726F6365
        7373416273656E6365487273462E44617461536F75726365456D706C546F104F
        7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E
        6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E6710
        6564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700
        094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D656467
        6F43616E44656C6574650D6564676F43616E496E73657274116564676F43616E
        4E617669676174696F6E116564676F436F6E6669726D44656C65746512656467
        6F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B73
        00000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02310942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274206E616D65055769647468024E0942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E11436F6C756D6E4465736372697074
        696F6E0743617074696F6E06044E616D6505576964746803CC000942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060B444553
        4352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D
        6E416464726573730743617074696F6E06074164647265737305576964746802
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D6506074144445245535300000F546478444247726964436F6C756D6E0E436F
        6C756D6E44657074436F64650743617074696F6E060F4465706172746D656E74
        20636F64650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D65060F4445504152544D454E545F434F444500000F546478444247
        726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465
        616D20636F64650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D6506095445414D5F434F4445000000}
    end
  end
end
