inherited DialogReportHolidayCardF: TDialogReportHolidayCardF
  Left = 270
  Caption = 'Report Holiday Card'
  ClientHeight = 458
  ClientWidth = 599
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 599
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 482
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 599
    Height = 337
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Top = 115
    end
    inherited LblEmployee: TLabel
      Top = 115
    end
    inherited LblToPlant: TLabel
      Top = 116
    end
    inherited LblToEmployee: TLabel
      Top = 17
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 117
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 117
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [9]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [10]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [11]
      Left = 315
      Top = 41
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [12]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel [13]
      Left = 8
      Top = 430
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [14]
      Left = 40
      Top = 430
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [15]
      Left = 315
      Top = 430
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [16]
      Left = 315
      Top = 68
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label19: TLabel [17]
      Left = 128
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [18]
      Left = 336
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [19]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object LblFromDate: TLabel [20]
      Left = 8
      Top = 143
      Width = 24
      Height = 13
      Caption = 'From'
    end
    object LblDate: TLabel [21]
      Left = 40
      Top = 143
      Width = 23
      Height = 13
      Caption = 'Date'
    end
    object LblToDate: TLabel [22]
      Left = 315
      Top = 142
      Width = 10
      Height = 13
      Caption = 'to'
    end
    inherited LblFromDepartment: TLabel
      Top = 68
    end
    inherited LblDepartment: TLabel
      Top = 68
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 67
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 95
    end
    inherited LblToTeam: TLabel
      Top = 91
    end
    inherited LblStarTeamFrom: TLabel
      Top = 95
    end
    inherited LblTeam: TLabel
      Top = 91
    end
    inherited LblFromTeam: TLabel
      Top = 91
    end
    inherited LblFromShift: TLabel
      Left = -32
      Top = 332
    end
    inherited LblShift: TLabel
      Left = 0
      Top = 276
    end
    inherited LblStartShiftFrom: TLabel
      Left = 88
      Top = 278
    end
    inherited LblToShift: TLabel
      Left = 275
      Top = 278
    end
    inherited LblStarShiftTo: TLabel
      Left = 304
      Top = 278
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 134
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 135
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 90
      ColCount = 142
      TabOrder = 7
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 90
      ColCount = 143
      TabOrder = 8
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 521
      Top = 95
      TabOrder = 9
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 65
      ColCount = 145
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 65
      ColCount = 146
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 521
      Top = 67
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 115
      TabOrder = 10
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 334
      Top = 115
      TabOrder = 11
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Left = 96
      Top = 275
      ColCount = 145
      TabOrder = 23
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Left = 318
      Top = 275
      ColCount = 146
      TabOrder = 26
    end
    inherited CheckBoxAllShifts: TCheckBox
      Left = 490
      Top = 275
      TabOrder = 35
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 24
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 522
      Top = 350
      TabOrder = 32
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 33
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 150
      TabOrder = 25
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 151
      TabOrder = 34
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 151
      TabOrder = 29
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 152
      TabOrder = 22
    end
    inherited EditWorkspots: TEdit
      TabOrder = 30
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 155
      TabOrder = 17
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 154
      TabOrder = 31
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 19
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 21
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 18
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 20
    end
    object GroupBoxShow: TGroupBox
      Left = 150
      Top = 170
      Width = 409
      Height = 161
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      object CheckBoxHoliday: TCheckBox
        Left = 8
        Top = 19
        Width = 137
        Height = 17
        Caption = 'Holiday'
        TabOrder = 0
      end
      object CheckBoxWorkTimeReduction: TCheckBox
        Left = 8
        Top = 42
        Width = 145
        Height = 17
        Caption = 'Work time reduction'
        TabOrder = 1
      end
      object CheckBoxTimeForTime: TCheckBox
        Left = 8
        Top = 65
        Width = 97
        Height = 17
        Caption = 'Time For Time'
        TabOrder = 2
      end
      object CheckBoxSeniorityHoliday: TCheckBox
        Left = 184
        Top = 14
        Width = 209
        Height = 17
        Caption = 'Seniority Holiday'
        TabOrder = 6
      end
      object CheckBoxAddBankHoliday: TCheckBox
        Left = 8
        Top = 134
        Width = 217
        Height = 17
        Caption = 'Additional Bank Holiday'
        TabOrder = 5
      end
      object CheckBoxRsvBankHoliday: TCheckBox
        Left = 8
        Top = 111
        Width = 217
        Height = 17
        Caption = 'Bank Holiday to Reserve'
        TabOrder = 4
      end
      object CheckBoxShorterWWeek: TCheckBox
        Left = 184
        Top = 38
        Width = 217
        Height = 17
        Caption = 'Shorter Working Week'
        TabOrder = 7
      end
      object CheckBoxBankHoliday: TCheckBox
        Left = 8
        Top = 88
        Width = 129
        Height = 17
        Caption = 'Bank Holiday'
        TabOrder = 3
      end
    end
    object GroupBoxSelections: TGroupBox
      Left = 6
      Top = 170
      Width = 137
      Height = 161
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 19
        Width = 97
        Height = 17
        Caption = 'Show Selections'
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 43
        Width = 97
        Height = 17
        Caption = 'Export'
        TabOrder = 1
      end
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 40
      Width = 180
      Height = 19
      ColCount = 135
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 40
      Width = 180
      Height = 19
      ColCount = 136
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object CheckBoxAllTeam: TCheckBox
      Left = 520
      Top = 430
      Width = 49
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      Visible = False
    end
    object DateFrom: TDateTimePicker
      Left = 120
      Top = 141
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 12
      OnChange = DateFromChange
    end
    object DateTo: TDateTimePicker
      Left = 335
      Top = 141
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
      OnChange = DateToChange
    end
  end
  inherited stbarBase: TStatusBar
    Top = 398
    Width = 599
  end
  inherited pnlBottom: TPanel
    Top = 417
    Width = 599
    inherited btnOk: TBitBtn
      Left = 184
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 248
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        4B040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F75726365072B4469616C6F675265706F7274486F6C6964617943617264
        462E44617461536F75726365456D706C46726F6D104F7074696F6E7343757374
        6F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E645369
        7A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E
        53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E734442
        0B106564676F43616E63656C4F6E457869740D6564676F43616E44656C657465
        0D6564676F43616E496E73657274116564676F43616E4E617669676174696F6E
        116564676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265
        636F726473106564676F557365426F6F6B6D61726B7300000F54647844424772
        6964436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E
        756D62657206536F7274656407046373557005576964746802410942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060F454D50
        4C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F43
        6F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E61
        6D6505576964746802540942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060A53484F52545F4E414D4500000F546478444247
        726964436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E61
        6D6505576964746803B4000942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060B4445534352495054494F4E00000F54647844
        4247726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F
        6E06074164647265737305576964746802450942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D6506074144445245535300000F54
        6478444247726964436F6C756D6E0E436F6C756D6E44657074436F6465074361
        7074696F6E060F4465706172746D656E7420636F646505576964746802580942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        12040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507294469616C6F675265706F72
        74486F6C6964617943617264462E44617461536F75726365456D706C546F104F
        7074696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E
        6564676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E6710
        6564676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700
        094F7074696F6E7344420B106564676F43616E63656C4F6E457869740D656467
        6F43616E44656C6574650D6564676F43616E496E73657274116564676F43616E
        4E617669676174696F6E116564676F436F6E6669726D44656C65746512656467
        6F4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B73
        00000F546478444247726964436F6C756D6E0A436F6C756D6E456D706C074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02310942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274206E616D65055769647468024E0942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E11436F6C756D6E4465736372697074
        696F6E0743617074696F6E06044E616D6505576964746803CC000942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D65060B444553
        4352495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D
        6E416464726573730743617074696F6E06074164647265737305576964746802
        650942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D6506074144445245535300000F546478444247726964436F6C756D6E0E436F
        6C756D6E44657074436F64650743617074696F6E060F4465706172746D656E74
        20636F64650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D65060F4445504152544D454E545F434F444500000F546478444247
        726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465
        616D20636F64650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 420
  end
  object TableAbsReason: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCEREASON_CODE'
    TableName = 'ABSENCEREASON'
    Left = 496
    Top = 23
    object TableAbsReasonABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      Size = 1
    end
    object TableAbsReasonDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object TableAbsenceType: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCETYPE_CODE'
    TableName = 'ABSENCETYPE'
    Left = 464
    Top = 23
    object TableAbsenceTypeABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableAbsenceTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
  end
  object qryAbsenceReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  ABSENCEREASON_ID,'
      '  ABSENCEREASON_CODE,'
      '  DESCRIPTION,'
      '  ABSENCETYPE_CODE'
      'FROM'
      '  ABSENCEREASON'
      'ORDER BY'
      '  ABSENCEREASON_CODE'
      '')
    Left = 296
    Top = 24
  end
  object qryAbsenceTypePerCountryExist: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT T.COUNTRY_ID'
      'FROM ABSENCETYPEPERCOUNTRY T'
      'WHERE T.COUNTRY_ID = :COUNTRY_ID'
      '')
    Left = 496
    Top = 240
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
end
