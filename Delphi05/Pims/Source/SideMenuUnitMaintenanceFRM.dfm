inherited SideMenuUnitMaintenanceF: TSideMenuUnitMaintenanceF
  Left = 616
  Top = 237
  Caption = 'Definitions'
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 2
  end
  inherited pnlInsertBase: TPanel
    Left = 129
    Width = 655
    Caption = ''
  end
  object dxSideBar: TdxSideBar [4]
    Left = 0
    Top = 61
    Width = 129
    Height = 481
    BkGround.BeginColor = clGrayText
    BkGround.EndColor = clGrayText
    BkGround.FillStyle = bfsNone
    CanSelected = True
    GroupFont.Charset = DEFAULT_CHARSET
    GroupFont.Color = clWindowText
    GroupFont.Height = -11
    GroupFont.Name = 'MS Sans Serif'
    GroupFont.Style = []
    Groups = <
      item
        Caption = 'Plant Structure'
        Index = 0
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Plants '
            Index = 0
            IsDefault = True
            LargeImage = 21
            SmallImage = -1
            Tag = 1
          end
          item
            Caption = 'Business unit'
            Index = 1
            IsDefault = True
            LargeImage = 6
            SmallImage = -1
            Tag = 2
          end
          item
            Caption = 'Departments'
            Index = 2
            IsDefault = True
            LargeImage = 10
            SmallImage = -1
            Tag = 3
          end
          item
            Caption = 'Workspots'
            Index = 3
            IsDefault = True
            LargeImage = 30
            SmallImage = -1
            Tag = 4
          end
          item
            Caption = 'Teams'
            Index = 4
            IsDefault = True
            LargeImage = 25
            SmallImage = -1
            Tag = 5
          end
          item
            Caption = 'Departments per team'
            Index = 5
            IsDefault = True
            LargeImage = 25
            SmallImage = -1
            Tag = 6
          end
          item
            Caption = 'Workspots per workstation'
            Index = 6
            IsDefault = True
            LargeImage = 31
            SmallImage = -1
            Tag = 7
          end
          item
            Caption = 'Machines'
            Index = 7
            IsDefault = True
            LargeImage = 30
            SmallImage = -1
            Tag = 8
          end>
      end
      item
        Caption = 'Time Structure'
        Index = 1
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Shifts'
            Index = 0
            IsDefault = True
            LargeImage = 24
            SmallImage = -1
            Tag = 10
          end
          item
            Caption = 'Timeblocks per shift'
            Index = 1
            IsDefault = True
            LargeImage = 28
            SmallImage = -1
            Tag = 11
          end
          item
            Caption = 'Breaks per shift'
            Index = 2
            IsDefault = True
            LargeImage = 5
            SmallImage = -1
            Tag = 12
          end
          item
            Caption = 'Timeblocks per department'
            Index = 3
            IsDefault = True
            LargeImage = 26
            SmallImage = -1
            Tag = 13
          end
          item
            Caption = 'Breaks per department'
            Index = 4
            IsDefault = True
            LargeImage = 3
            SmallImage = -1
            Tag = 14
          end
          item
            Caption = 'Dummy'
            Index = 5
            IsDefault = True
            LargeImage = 3
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 6
            IsDefault = True
            LargeImage = 3
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 7
            IsDefault = True
            LargeImage = 3
            SmallImage = -1
            Tag = 999
          end>
      end
      item
        Caption = 'Contract Groups'
        Index = 2
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Contract groups'
            Index = 0
            IsDefault = True
            LargeImage = 8
            SmallImage = -1
            Tag = 20
          end
          item
            Caption = 'Exceptional hours'
            Index = 1
            IsDefault = True
            LargeImage = 13
            SmallImage = -1
            Tag = 21
          end
          item
            Caption = 'Overtime definitions'
            Index = 2
            IsDefault = True
            LargeImage = 20
            SmallImage = -1
            Tag = 22
          end
          item
            Caption = 'PTO definitions'
            Index = 3
            IsDefault = True
            LargeImage = 25
            SmallImage = -1
            Tag = 23
          end>
      end
      item
        Caption = ' Staff Data'
        Index = 3
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Employees'
            Index = 0
            IsDefault = True
            LargeImage = 12
            SmallImage = -1
            Tag = 30
          end
          item
            Caption = 'Employee contracts'
            Index = 1
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 31
          end
          item
            Caption = 'ID-cards'
            Index = 2
            IsDefault = True
            LargeImage = 15
            SmallImage = -1
            Tag = 32
          end
          item
            Caption = 'Timeblocks per employee'
            Index = 3
            IsDefault = True
            LargeImage = 27
            SmallImage = -1
            Tag = 33
          end
          item
            Caption = 'Breaks per employee'
            Index = 4
            IsDefault = True
            LargeImage = 4
            SmallImage = -1
            Tag = 34
          end
          item
            Caption = 'Workspots per employee'
            Index = 5
            IsDefault = True
            LargeImage = 31
            SmallImage = -1
            Tag = 35
          end>
      end
      item
        Caption = 'Configuration'
        Index = 4
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'System configuration'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 500
          end
          item
            Caption = 'Datacollection connection'
            Index = 1
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 501
          end
          item
            Caption = 'Comport connection'
            Index = 2
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 502
          end
          item
            Caption = 'Type of hours'
            Index = 3
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 503
          end
          item
            Caption = 'Absence types'
            Index = 4
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 504
          end
          item
            Caption = 'Absence reasons'
            Index = 5
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 505
          end
          item
            Caption = 'Quality incentive configuration'
            Index = 6
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 506
          end
          item
            Caption = 'Countries'
            Index = 7
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 507
          end
          item
            Caption = 'Absence types per country'
            Index = 8
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 509
          end
          item
            Caption = 'Absence reasons per country'
            Index = 9
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 510
          end
          item
            Caption = 'Type of hours per country'
            Index = 10
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 508
          end
          item
            Caption = 'Error Log'
            Index = 11
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 512
          end>
      end
      item
        Caption = 'Time Recording'
        Index = 5
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Time recording scannings'
            Index = 0
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 41
          end
          item
            Caption = 'Hours per employee'
            Index = 1
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 42
          end
          item
            Caption = 'Transfer free time to next year'
            Index = 2
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 43
          end
          item
            Caption = 'Calculate earned worktime reduction'
            Index = 3
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 44
          end
          item
            Caption = 'Bank holidays'
            Index = 4
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 45
          end
          item
            Caption = 'Process planned absence and non-scanning hours'
            Index = 5
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 46
          end
          item
            Caption = 'Illness messages'
            Index = 6
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 47
          end>
      end
      item
        Caption = 'Payroll System'
        Index = 6
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Payment export code'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 100
          end
          item
            Caption = 'Extra payments'
            Index = 1
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 101
          end
          item
            Caption = 'Export to payroll'
            Index = 2
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 102
          end
          item
            Caption = 'Monthly group efficiency'
            Index = 3
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 103
          end
          item
            Caption = 'Dummy'
            Index = 4
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 5
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 999
          end>
      end
      item
        Caption = 'Staff Planning'
        Index = 7
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Standard occupation'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 70
          end
          item
            Caption = 'Standard staff availability'
            Index = 1
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 71
          end
          item
            Caption = 'Shift schedule'
            Index = 2
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 72
          end
          item
            Caption = 'Staff availability'
            Index = 3
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 73
          end
          item
            Caption = 'Employee availability'
            Index = 4
            IsDefault = True
            LargeImage = 3
            SmallImage = -1
            Tag = 74
          end
          item
            Caption = 'Staff planning'
            Index = 5
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 75
          end
          item
            Caption = 'Work Schedule'
            Index = 6
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 76
          end
          item
            Caption = 'Work Schedule Details'
            Index = 7
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 77
          end
          item
            Caption = 'Dummy'
            Index = 8
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 9
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 10
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 999
          end>
      end
      item
        Caption = 'Unit Maintenance'
        Index = 8
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Report Plant Structure'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 700
          end
          item
            Caption = 'Condensed report employees'
            Index = 1
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 50
          end
          item
            Caption = 'Report job comments'
            Index = 2
            IsDefault = True
            LargeImage = 31
            SmallImage = -1
            Tag = 68
          end
          item
            Caption = 'Report Employee Info and Deviations'
            Index = 3
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 69
          end
          item
            Caption = 'Report In/Outscan, Absence, Overtime'
            Index = 4
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 92
          end
          item
            Caption = 'Dummy'
            Index = 5
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 6
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 7
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 8
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 9
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end
          item
            Caption = 'Dummy'
            Index = 10
            IsDefault = True
            LargeImage = 11
            SmallImage = -1
            Tag = 999
          end>
      end
      item
        Caption = 'Time Registration'
        Index = 9
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Checklist scans'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 51
          end
          item
            Caption = 'Comparison worked hours'
            Index = 1
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 52
          end
          item
            Caption = 'Report hours per employee'
            Index = 2
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 53
          end
          item
            Caption = 'Report work time reduction, holiday, illness '
            Index = 3
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 54
          end
          item
            Caption = 'Report absence hours'
            Index = 4
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 55
          end
          item
            Caption = 'Report hours per day'
            Index = 5
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 62
          end
          item
            Caption = 'Report absence card'
            Index = 6
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 56
          end
          item
            Caption = 'Period report direct/indirect hours per week'
            Index = 7
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 57
          end
          item
            Caption = 'Report hours per workspot'
            Index = 8
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 58
          end
          item
            Caption = 'Report hours per workspot cumulative'
            Index = 9
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 61
          end
          item
            Caption = 'Report ratio illness hours'
            Index = 10
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 59
          end
          item
            Caption = 'Report available, used and planned free time'
            Index = 11
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 60
          end
          item
            Caption = 'Report hours per contract group cumulative'
            Index = 12
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 63
          end
          item
            Caption = 'Report holiday card'
            Index = 13
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 64
          end
          item
            Caption = 'Report changes per scan'
            Index = 14
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 65
          end
          item
            Caption = 'Report employee illness'
            Index = 15
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 66
          end
          item
            Caption = 'Report incentive program'
            Index = 16
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 67
          end>
      end
      item
        Caption = 'Staff Planning'
        Index = 10
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Report staff availability'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 80
          end
          item
            Caption = 'Report employee availability'
            Index = 1
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 81
          end
          item
            Caption = 'Report staff planning'
            Index = 2
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 82
          end
          item
            Caption = 'Report Staff planning per day'
            Index = 3
            IsDefault = True
            LargeImage = 14
            SmallImage = -1
            Tag = 83
          end
          item
            Caption = 'Report Staff planning condensed'
            Index = 4
            IsDefault = True
            LargeImage = 14
            SmallImage = -1
            Tag = 84
          end>
      end
      item
        Caption = 'Productivity Control'
        Index = 11
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Data collection entry'
            Index = 0
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 90
          end
          item
            Caption = 'Real Time Efficiency Monitor'
            Index = 1
            IsDefault = True
            LargeImage = 6
            SmallImage = -1
            Tag = 91
          end>
      end
      item
        Caption = 'Sales'
        Index = 12
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Revenue'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 300
          end>
      end
      item
        Caption = 'Quality incentive'
        Index = 13
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Mistake per employee'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 400
          end
          item
            Caption = 'Quality incentive calculation'
            Index = 1
            IsDefault = True
            LargeImage = 1
            SmallImage = -1
            Tag = 401
          end>
      end
      item
        Caption = 'Registrations'
        Index = 14
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Employee Registrations'
            Index = 0
            IsDefault = True
            LargeImage = 25
            SmallImage = -1
            Tag = 600
          end>
      end
      item
        Caption = 'Productivity Control'
        Index = 15
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Production reports'
            Index = 0
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 200
          end
          item
            Caption = 'Production detail report'
            Index = 1
            IsDefault = True
            LargeImage = 14
            SmallImage = -1
            Tag = 202
          end
          item
            Caption = 'Team incentive report '
            Index = 2
            IsDefault = True
            LargeImage = 2
            SmallImage = -1
            Tag = 201
          end
          item
            Caption = 'Production comparison report'
            Index = 3
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 203
          end
          item
            Caption = 'Productivity compared to target report'
            Index = 4
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 204
          end
          item
            Caption = 'Report Ghost Counts'
            Index = 5
            IsDefault = True
            LargeImage = 0
            SmallImage = -1
            Tag = 205
          end>
      end
      item
        Caption = 'Payments'
        Index = 16
        IconType = dxsgLargeIcon
        Items = <
          item
            Caption = 'Report Employee Paylist'
            Index = 0
            IsDefault = True
            LargeImage = 25
            SmallImage = -1
            Tag = 110
          end>
      end>
    ActiveGroupIndex = 0
    GroupHeightOffSet = 0
    ItemFont.Charset = DEFAULT_CHARSET
    ItemFont.Color = clWhite
    ItemFont.Height = -11
    ItemFont.Name = 'MS Sans Serif'
    ItemFont.Style = []
    LargeImages = imgListBase
    ScrollDelay = 300
    SpaceHeight = 7
    TransparentImages = False
    ShowGroups = True
    StoreInRegistry = False
    OnItemClick = dxSideBarItemClick
    OnChangeActiveGroup = dxSideBarChangeActiveGroup
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemGo
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.Strings = (
      'Menus'
      'File'
      'Help'
      'Grid Navigator'
      'DB Navigator'
      'Export'
      'Grid Functions'
      'GoPlantStructure'
      'GoTimeStructure'
      'GoContractGroups'
      'GoFixedStaffData'
      'GoConfiguration'
      'GoTimeRecording'
      'GoPayrollSystem'
      'GoStaffPlanning'
      'GoRepUnitMaintenance'
      'GoReports'
      'GOReportStaffPlanning'
      'GoProductivity'
      'GoSales'
      'GoQualityIncentive'
      'GoRegistrations'
      'Production reports'
      'GoReportPayments')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    Images = imgListBase
    Left = 532
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemGo: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarSubItemPlantStrcture
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemTimeStructure
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemContractGroup
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemFixedStaffData
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemConfig
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemTimeRecording
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemExport
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemStaffPlanning
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemRepUnitMaintenance
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarReports
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItem1
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemProdControl
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemSales
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemQuality
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemGoRegistrations
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemProductionReport
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemReportPayments
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarButtonEditMode: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonRecordDetails: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonSort: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonSearch: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonCustCol: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonShowGroup: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonExpand: TdxBarButton
      Category = 6
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      Category = 4
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      Category = 4
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Category = 4
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      Category = 4
    end
    inherited dxBarButtonExportAllHTML: TdxBarButton
      Category = 5
    end
    inherited dxBarButtonExportSelectionHTML: TdxBarButton
      Category = 5
    end
    inherited dxBarButtonExportAllXLS: TdxBarButton
      Category = 5
    end
    inherited dxBarButtonExportSelectionXLS: TdxBarButton
      Category = 5
    end
    inherited dxBarButtonCollapse: TdxBarButton
      Category = 6
    end
    inherited dxBarButtonResetColumns: TdxBarButton
      Category = 6
    end
    object dxBarSubItemPlantStrcture: TdxBarSubItem
      Caption = '&Plant Structure'
      Category = 7
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonPlant
          Visible = True
        end
        item
          Item = dxBarButtonBusinessUnit
          Visible = True
        end
        item
          Item = dxBarButtonDepartment
          Visible = True
        end
        item
          Item = dxBarButtonWorkSpot
          Visible = True
        end
        item
          Item = dxBarButtonTeam
          Visible = True
        end
        item
          Item = dxBarButtonDeptTeam
          Visible = True
        end
        item
          Item = dxBarButtonWKPerWorkStation
          Visible = True
        end>
    end
    object dxBarButtonPlant: TdxBarButton
      Action = GoPSPlant
      Category = 7
    end
    object dxBarButtonBusinessUnit: TdxBarButton
      Action = GoBusinessUnit
      Caption = '&Business unit'
      Category = 7
      Hint = 'Business unit'
    end
    object dxBarButtonDepartment: TdxBarButton
      Action = GoDepartment
      Category = 7
    end
    object dxBarButtonWorkSpot: TdxBarButton
      Action = GoWorkspot
      Category = 7
    end
    object dxBarButtonTeam: TdxBarButton
      Action = GoTeam
      Category = 7
    end
    object dxBarSubItemTimeStructure: TdxBarSubItem
      Caption = '&Time Structure'
      Category = 8
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonShifts
          Visible = True
        end
        item
          Item = dxBarButtonTBPerShift
          Visible = True
        end
        item
          Item = dxBarButtonBreaksPerShift
          Visible = True
        end
        item
          Item = dxBarButtonTBPerDept
          Visible = True
        end
        item
          Item = dxBarButtonBreakPerDepartment
          Visible = True
        end>
    end
    object dxBarButtonShifts: TdxBarButton
      Action = GoShift
      Category = 8
    end
    object dxBarButtonTBPerShift: TdxBarButton
      Action = GoTimeBlockShift
      Caption = '&Timeblocks per shift'
      Category = 8
      Hint = 'Timeblocks per shift'
    end
    object dxBarButtonBreaksPerShift: TdxBarButton
      Action = GoBreakPerShift
      Caption = '&Breaks per shift'
      Category = 8
      Hint = 'Breaks per shift'
    end
    object dxBarButtonTBPerDept: TdxBarButton
      Action = GoTimeBlockPerDept
      Caption = '&Timeblocks per department'
      Category = 8
      Hint = 'Timeblocks per department'
    end
    object dxBarSubItemContractGroup: TdxBarSubItem
      Caption = '&Contract Groups'
      Category = 9
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonContractGroups
          Visible = True
        end
        item
          Item = dxBarButtonExceptionalHours
          Visible = True
        end
        item
          Item = dxBarButtonOverTime
          Visible = True
        end
        item
          Item = dxBarButtonPTODef
          Visible = True
        end>
    end
    object dxBarButtonContractGroups: TdxBarButton
      Action = GoContractGroups
      Caption = '&Contract groups'
      Category = 9
      Hint = 'Contract groups'
    end
    object dxBarButtonExceptionalHours: TdxBarButton
      Action = GoExceptionalhours
      Caption = '&Exceptional hours'
      Category = 9
      Hint = 'Exceptional hours'
    end
    object dxBarButtonBreakPerDepartment: TdxBarButton
      Action = GoBreakPerDept
      Caption = '&Breaks per department'
      Category = 8
      Hint = 'Breaks per department'
    end
    object dxBarButtonOverTime: TdxBarButton
      Action = GoOvertime
      Category = 9
      Hint = 'Overtime definitions'
    end
    object dxBarButtonDeptTeam: TdxBarButton
      Action = GoTeamDept
      Caption = '&Departments per team'
      Category = 7
      Hint = 'Departments per team'
    end
    object dxBarSubItemFixedStaffData: TdxBarSubItem
      Caption = '&Staff Data'
      Category = 10
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonEmployee
          Visible = True
        end
        item
          Item = dxBarButtonEmplContracts
          Visible = True
        end
        item
          Item = dxBarButtonIDCards
          Visible = True
        end
        item
          Item = dxBarButtonTBPerEmployee
          Visible = True
        end
        item
          Item = dxBarButtonBreaksPerEmployee
          Visible = True
        end
        item
          Item = dxBarButtonWKPerEmpl
          Visible = True
        end>
    end
    object dxBarButtonEmployee: TdxBarButton
      Action = GoEmployee
      Category = 10
      Hint = 'Employee'
    end
    object dxBarButtonEmplContracts: TdxBarButton
      Action = GoEmplContract
      Caption = 'Employee &contract'
      Category = 10
      Hint = 'Employee contract'
    end
    object dxBarButtonIDCards: TdxBarButton
      Action = GoIDCard
      Caption = '&ID card'
      Category = 10
      Hint = 'ID card'
    end
    object dxBarButtonTBPerEmployee: TdxBarButton
      Action = GoTimeBlockPerEmpl
      Caption = '&Timeblocks per employee'
      Category = 10
      Hint = 'Timeblocks per employee'
    end
    object dxBarButtonBreaksPerEmployee: TdxBarButton
      Action = GoBreakPerEmpl
      Caption = '&Breaks per employee'
      Category = 10
      Hint = 'Breaks per employee'
    end
    object dxBarButtonWKPerEmpl: TdxBarButton
      Action = GoWKPerEmpl
      Caption = '&Workspots per employee'
      Category = 10
      Hint = 'Workspots per employee'
    end
    object dxBarReports: TdxBarSubItem
      Caption = 'Reports timeregistration'
      Category = 16
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonCheckList
          Visible = True
        end
        item
          Item = dxBarButtonCompHours
          Visible = True
        end
        item
          Item = dxBarButtonHoursEmpl
          Visible = True
        end
        item
          Item = dxBarButtonAbsenceShedule
          Visible = True
        end
        item
          Item = dxBarButtonAbsenceHours
          Visible = True
        end
        item
          Item = dxBarButtonRepHrsPerDay
          Visible = True
        end
        item
          Item = dxBarButtonRepAbsenceCard
          Visible = True
        end
        item
          Item = dxBarButtonRepDirIndHrs
          Visible = True
        end
        item
          Item = dxBarButtonRepHrsPerWK
          Visible = True
        end
        item
          Item = dxBarButtonRepHrsPerWKCum
          Visible = True
        end
        item
          Item = dxBarButtonRepRatilIllHrs
          Visible = True
        end
        item
          Item = dxBarButtonRepAvailUsedTime
          Visible = True
        end
        item
          Item = dxBarButtonRepHrsPerContractGrpCUM
          Visible = True
        end
        item
          Item = dxBarButtonRepHolidayCard
          Visible = True
        end
        item
          Item = dxBarButtonRepChangesPerScan
          Visible = True
        end
        item
          Item = dxBarButtonRepEmployeeIllness
          Visible = True
        end
        item
          Item = dxBarButtonRepIncentiveProgram
          Visible = True
        end
        item
          Item = dxBarButtonRepJobComments
          Visible = True
        end>
    end
    object dxBarButtonCheckList: TdxBarButton
      Action = GoCheckListScans
      Category = 16
    end
    object dxBarButtonCompHours: TdxBarButton
      Action = GoCompHours
      Category = 16
    end
    object dxBarButtonHoursEmpl: TdxBarButton
      Action = GoRepHoursPerEmpl
      Category = 16
    end
    object dxBarButtonAbsenceShedule: TdxBarButton
      Action = GoWTRAbsenceReport
      Category = 16
    end
    object dxBarSubItemTimeRecording: TdxBarSubItem
      Caption = 'Time recording'
      Category = 12
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonTimeRecScan
          Visible = True
        end
        item
          Item = dxBarButtonHoursPerEmployee
          Visible = True
        end
        item
          Item = dxBarButtonTransferToNextYear
          Visible = True
        end
        item
          Item = dxBarButtonCalcEarnedWKT
          Visible = True
        end
        item
          Item = dxBarButtonBankHol
          Visible = True
        end
        item
          Item = dxBarButtonProcessPlannedAbs
          Visible = True
        end
        item
          Item = dxBarButtonIllnessMessages
          Visible = True
        end>
    end
    object dxBarButtonAbsenceHours: TdxBarButton
      Action = GoAbsenceReport
      Caption = 'Absence report'
      Category = 16
      Hint = 'Absence report'
    end
    object dxBarButtonRepHrsPerDay: TdxBarButton
      Action = GoRepHrsPerDay
      Caption = 'Report hours per day'
      Category = 16
      Hint = 'Report hours per day'
    end
    object dxBarButtonRepAbsenceCard: TdxBarButton
      Action = GoReportAbsenceCard
      Caption = 'Report absence card'
      Category = 16
      Hint = 'Repor absence card'
    end
    object dxBarButtonRepDirIndHrs: TdxBarButton
      Action = GoRepDirIndHrsWeek
      Category = 16
    end
    object dxBarButtonTimeRecScan: TdxBarButton
      Action = GoTimeRecScan
      Category = 12
    end
    object dxBarButtonRepHrsPerWK: TdxBarButton
      Action = GoRepHrsPerWorkspot
      Category = 16
    end
    object dxBarButtonHoursPerEmployee: TdxBarButton
      Action = GoHoursPerEmployee
      Category = 12
    end
    object dxBarSubItemStaffPlanning: TdxBarSubItem
      Caption = 'Staff planning'
      Category = 14
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonStdOccupation
          Visible = True
        end
        item
          Item = dxBarButtonStdStaffAvailability
          Visible = True
        end
        item
          Item = dxBarButtonShiftSchedule
          Visible = True
        end
        item
          Item = dxBarButtonStaffAvailability
          Visible = True
        end
        item
          Item = dxBarButtonEmplAvail
          Visible = True
        end
        item
          Item = dxBarButtonStaffPlanning
          Visible = True
        end
        item
          Item = dxBarButtonWorkSchedule
          Visible = True
        end
        item
          Item = dxBarButtonWorkScheduleDetails
          Visible = True
        end>
    end
    object dxBarButtonCalcEarnedWKT: TdxBarButton
      Action = GoCalculateEarnedWKR
      Category = 12
    end
    object dxBarButtonStdOccupation: TdxBarButton
      Action = GoStandardOccupation
      Category = 14
    end
    object dxBarButtonBankHol: TdxBarButton
      Action = GoActionBankHolidays
      Category = 12
    end
    object dxBarButtonStdStaffAvailability: TdxBarButton
      Action = GoStdStaffAvailability
      Category = 14
    end
    object dxBarButtonTransferToNextYear: TdxBarButton
      Action = GoTransferToNextYear
      Category = 12
    end
    object dxBarButtonShiftSchedule: TdxBarButton
      Action = GoShiftSchedule
      Caption = 'Shift schedule'
      Category = 14
      Hint = 'Shift schedule'
    end
    object dxBarButtonProcessPlannedAbs: TdxBarButton
      Action = GoActionProcessAbsence
      Category = 12
    end
    object dxBarButtonStaffAvailability: TdxBarButton
      Action = GoStaffAvailability
      Category = 14
    end
    object dxBarButtonEmplAvail: TdxBarButton
      Action = GoEmplAvailability
      Category = 14
    end
    object dxBarButtonIllnessMessages: TdxBarButton
      Action = GoIllnessMessages
      Category = 12
    end
    object dxBarButtonStaffPlanning: TdxBarButton
      Action = GoStaffPlanningAction
      Category = 14
    end
    object dxBarButtonWorkSchedule: TdxBarButton
      Action = GoWorkScheduleAction
      Category = 14
    end
    object dxBarButtonWorkScheduleDetails: TdxBarButton
      Action = GoWorkScheduleDetailsAction
      Category = 14
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = 'Reports staff planning'
      Category = 17
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonREPSTAFFAVAIL
          Visible = True
        end
        item
          Item = dxBarButtonREPEMPAVAIL
          Visible = True
        end
        item
          Item = dxBarButtonRepStaffPlanning
          Visible = True
        end
        item
          Item = dxBarButtonStaffPlanDay
          Visible = True
        end>
    end
    object dxBarButtonREPSTAFFAVAIL: TdxBarButton
      Action = GoReportStaffAvaillability
      Caption = 'Report staff availability'
      Category = 17
      Hint = 'Report staff availability'
    end
    object dxBarButtonREPEMPAVAIL: TdxBarButton
      Action = GoReportEMPAvailability
      Caption = 'Report employee availability'
      Category = 17
      Hint = 'Report employee availability'
    end
    object dxBarButtonRepStaffPlanning: TdxBarButton
      Action = GoReportStaffPlanning
      Caption = 'Report staff planning'
      Category = 17
      Hint = 'Report staff planning'
    end
    object dxBarButtonRepStaffPlanningCond: TdxBarButton
      Action = GoReportStaffPlanningCond
      Caption = 'Report staff planning condensed'
      Category = 17
      Hint = 'Report staff planning condensed'
    end
    object dxBarSubItemProdControl: TdxBarSubItem
      Caption = 'Productivity Control'
      Category = 18
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonManualDataCollection
          Visible = True
        end>
    end
    object dxBarButtonManualDataCollection: TdxBarButton
      Action = GoManualDataCollection
      Caption = 'Data collection entry'
      Category = 18
      Hint = 'Data collection entry'
    end
    object dxBarSubItemProductionReport: TdxBarSubItem
      Caption = 'Reports productivity'
      Category = 22
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonProdRep
          Visible = True
        end
        item
          Item = dxBarButtonProdDetailRep
          Visible = True
        end
        item
          Item = dxBarButtonTeamIncRep
          Visible = True
        end
        item
          Item = dxBarButtonRepProdComp
          Visible = True
        end
        item
          Item = dxBarButtonProdCompTarget
          Visible = True
        end>
    end
    object dxBarButtonProdRep: TdxBarButton
      Action = GoProductionReport
      Caption = 'Report production'
      Category = 22
      Hint = 'Production report'
    end
    object dxBarSubItemExport: TdxBarSubItem
      Caption = 'Payroll system'
      Category = 13
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonPaymentExpCode
          Visible = True
        end
        item
          Item = dxBarButtonExtraPayments
          Visible = True
        end
        item
          Item = dxBarButtonExport
          Visible = True
        end
        item
          Item = dxBarButtonMonthlyGroupEfficiency
          Visible = True
        end>
    end
    object dxBarButtonPaymentExpCode: TdxBarButton
      Action = GoPaymentExpCode
      Category = 13
    end
    object dxBarButtonProdDetailRep: TdxBarButton
      Action = GoProductionDetailReport
      Caption = 'Report production detail'
      Category = 22
      Hint = 'Report production detail'
    end
    object dxBarSubItemSales: TdxBarSubItem
      Caption = 'Sales'
      Category = 19
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonRevenue
          Visible = True
        end>
    end
    object dxBarButtonRevenue: TdxBarButton
      Action = GoRevenue
      Caption = 'Revenue'
      Category = 19
      Hint = 'Revenue'
    end
    object dxBarSubItemQuality: TdxBarSubItem
      Caption = 'Quality incentive'
      Category = 20
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonMistakePerEmpl
          Visible = True
        end
        item
          Item = dxBarButtonQualityCalc
          Visible = True
        end>
    end
    object dxBarButtonMistakePerEmpl: TdxBarButton
      Action = GoMistakePerEmpl
      Category = 20
    end
    object dxBarButtonQualityCalc: TdxBarButton
      Action = GoQualityIncentiveCalc
      Category = 20
    end
    object dxBarSubItemConfig: TdxBarSubItem
      Caption = 'Configuration'
      Category = 11
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonSystemConfg
          Visible = True
        end
        item
          Item = dxBarButtonDataCol
          Visible = True
        end
        item
          Item = dxBarButtonComport
          Visible = True
        end
        item
          Item = dxBarButtonTypesOfHrs
          Visible = True
        end
        item
          Item = dxBarAbsenceType
          Visible = True
        end
        item
          Item = dxBarButtonAbsRsn
          Visible = True
        end
        item
          Item = dxBarButtonQualityIncentiveConf
          Visible = True
        end
        item
          Item = dxBarButtonCountries
          Visible = True
        end
        item
          Item = dxBarButtonAbsenceTypesPerCountry
          Visible = True
        end
        item
          Item = dxBarButtonAbsenceReasonsPerCountry
          Visible = True
        end
        item
          Item = dxBarButtonHourTypesPerCountry
          Visible = True
        end>
    end
    object dxBarButtonSystemConfg: TdxBarButton
      Action = GoSettings
      Caption = 'System configuration'
      Category = 11
      Hint = 'System configuration'
    end
    object dxBarButtonDataCol: TdxBarButton
      Action = GoDataConnection
      Caption = 'Datacollection connection'
      Category = 11
      Hint = 'Datacollection connection'
    end
    object dxBarButtonComport: TdxBarButton
      Action = GoComportConnection
      Category = 11
    end
    object dxBarButtonTypesOfHrs: TdxBarButton
      Action = GoTypeOfHours
      Category = 11
    end
    object dxBarAbsenceType: TdxBarButton
      Action = GoAbsType
      Category = 11
    end
    object dxBarButtonAbsRsn: TdxBarButton
      Action = GoAbsenceRsn
      Category = 11
    end
    object dxBarButtonExtraPayments: TdxBarButton
      Action = GoExtraPayment
      Category = 13
    end
    object dxBarButtonExport: TdxBarButton
      Action = GoExportPayroll
      Category = 13
    end
    object dxBarButtonMonthlyGroupEfficiency: TdxBarButton
      Action = GoMonthlyGroupEfficiency
      Category = 13
    end
    object dxBarButtonQualityIncentiveConf: TdxBarButton
      Action = ActionQualityConf
      Category = 11
    end
    object dxBarButtonStaffPlanDay: TdxBarButton
      Action = GoReportStaffPlanDay
      Caption = 'Report staff planning per day'
      Category = 17
      Hint = 'Report staff planning per day'
    end
    object dxBarButtonTeamIncRep: TdxBarButton
      Action = GoTeamIncentiveRep
      Caption = 'Team incentive report'
      Category = 22
      Hint = 'Team incentive report'
    end
    object dxBarButtonRepProdComp: TdxBarButton
      Action = GoProductionCompReport
      Caption = 'Production comparison report'
      Category = 22
      Hint = 'Production comparison report'
    end
    object dxBarButtonRepHrsPerWKCum: TdxBarButton
      Action = GoRepHrsPerWKCum
      Category = 16
    end
    object dxBarButtonRepRatilIllHrs: TdxBarButton
      Action = GoReportRatioIllHrs
      Category = 16
    end
    object dxBarButtonRepAvailUsedTime: TdxBarButton
      Action = GoReportAvailableUsedTime
      Category = 16
    end
    object dxBarButtonRepHrsPerContractGrpCUM: TdxBarButton
      Action = GoRepHrsPerContractGrpCUM
      Category = 16
    end
    object dxBarButtonRepHolidayCard: TdxBarButton
      Action = GoReportHolidayCard
      Category = 16
    end
    object dxBarButtonWKPerWorkStation: TdxBarButton
      Action = GoWorkSpotWorkstation
      Caption = '&Workspots per workstation'
      Category = 7
      Hint = 'Workspots per workstation'
    end
    object dxBarButtonProdCompTarget: TdxBarButton
      Action = GoProductionCompTargetReport
      Caption = 'Productivity compared to target'
      Category = 22
      Hint = 'Productivity compared to target'
    end
    object dxBarSubItemGoRegistrations: TdxBarSubItem
      Caption = 'Registrations'
      Category = 21
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonEmployeeRegistrations
          Visible = True
        end>
    end
    object dxBarButtonEmployeeRegistrations: TdxBarButton
      Action = GoEmployeeRegistrations
      Category = 21
      Hint = 'Employee Registrations'
    end
    object dxBarSubItemReportPayments: TdxBarSubItem
      Caption = 'Reports payments'
      Category = 23
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonReportEmployeePaylist
          Visible = True
        end>
    end
    object dxBarButtonReportEmployeePaylist: TdxBarButton
      Action = GoReportEmployeePaylist
      Category = 23
    end
    object dxBarSubItemRepUnitMaintenance: TdxBarSubItem
      Caption = 'Reports unit maintenance'
      Category = 15
      Visible = ivAlways
      ItemLinks = <
        item
          Item = dxBarButtonRepPlant
          Visible = True
        end
        item
          Item = dxBarButtonRepEmpl
          Visible = True
        end
        item
          Item = dxBarButtonRepEmpInfoDevPeriod
          Visible = True
        end
        item
          Item = dxBarButtonRepInOutAbsenceOvertime
          Visible = True
        end>
    end
    object dxBarButtonRepPlant: TdxBarButton
      Action = GoRepPlantStructure
      Category = 15
    end
    object dxBarButtonRepEmpl: TdxBarButton
      Action = GoReportEmployee
      Category = 15
    end
    object dxBarButtonMachine: TdxBarButton
      Action = GoMachine
      Category = 7
    end
    object dxBarButtonCountries: TdxBarButton
      Action = GoCountries
      Category = 11
    end
    object dxBarButtonAbsenceTypesPerCountry: TdxBarButton
      Action = GoAbsenceTypesPerCountry
      Caption = 'Absence types per country'
      Category = 11
      Hint = 'Absence types per country'
    end
    object dxBarButtonAbsenceReasonsPerCountry: TdxBarButton
      Action = GoAbsenceReasonsPerCountry
      Caption = 'Absence reasons per country'
      Category = 11
      Hint = 'Absence reasons per country'
    end
    object dxBarButtonHourTypesPerCountry: TdxBarButton
      Action = GoHourTypesPerCountry
      Caption = 'Type of hours per country'
      Category = 11
      Hint = 'Type of hours per country'
    end
    object dxBarButtonErrorLog: TdxBarButton
      Action = GoErrorLog
      Category = 11
      Hint = 'Error Log'
    end
    object dxBarButtonRepChangesPerScan: TdxBarButton
      Action = GoReportChangesPerScan
      Category = 16
    end
    object dxBarButtonRepEmployeeIllness: TdxBarButton
      Action = GoReportEmployeeIllness
      Category = 16
    end
    object dxBarButtonRepIncentiveProgram: TdxBarButton
      Action = GoReportIncentiveProgram
      Category = 16
    end
    object dxBarButtonRepJobComments: TdxBarButton
      Action = GoReportJobComments
      Category = 15
    end
    object dxBarButtonRepEmpInfoDevPeriod: TdxBarButton
      Action = GoReportEmpInfoDevPeriod
      Category = 15
    end
    object dxBarButtonRealTimeEffMonitor: TdxBarButton
      Action = GoRealTimeEffMonitor
      Category = 18
    end
    object dxBarButtonReportGhostCounts: TdxBarButton
      Action = GoReportGhostCounts
      Category = 22
    end
    object dxBarButtonRepInOutAbsenceOvertime: TdxBarButton
      Action = GoReportInOutAbsenceOvertime
      Category = 16
    end
    object dxBarButtonPTODef: TdxBarButton
      Action = GoPTODef
      Category = 9
      Hint = 'PTO Definitions'
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 632
  end
  inherited StandardMenuActionList: TActionList
    Left = 632
    inherited HelpSolarHomePageAct: TAction
      Caption = '&Pims Home Page'
    end
  end
  inherited imgListBase: TImageList
    Left = 160
    Top = 112
    Bitmap = {
      494C010122002700040030003000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000C0000000E0010000010020000000000000A0
      0500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700EFEFEF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00B5B5B5009C9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF003939390000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00CECECE00BDBDBD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F700000000000000000000000000000000000000000000CEFF0000CE
      FF000000000000CEFF000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFEF00FFD69C00FFCE
      8400FFCE8C00FFCE8400FFEFD600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF000000000000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7CE00FFA54200D67B18007342
      000073420800A55A0800FF9C2900FFCE8400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFE7C600FF9C310073420800080000000000
      0000000000000000000029180000DE7B1800FFC67B00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB55A009C5A100000000000000000000000
      000000000000000000000000000031180800FF9C2100FFE7C600FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21002110000000000000000000000000
      000000000000000000000000000000000000A55A1000FFC67B00FFFFFF00BDBD
      BD00B58C8C00BD8C9400B58C8C00B58C9400A5848C00B5848C00BD848400AD8C
      9400B57B84008C7373008C73730094636300845A6300A55A63008C5252008C5A
      5A0000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFF7DE00EF8C21000000000000000000000000000000
      0000000000000000000000000000000000008C4A0800FFCE84007B7373004A5A
      5A00E7CECE008CADB500BDADAD007BA5B500B5BDCE00B5BDC600C6BDB500BDBD
      C600C6ADAD006B949C00638C9C00637B94006B7B7B009C8C8C00B5A5A500846B
      6B0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21001810000000000000000000000000
      000000000000000000000000000000000000844A1000635231004A4242009C9C
      940094949C00738CA500AD9CAD00B5A59C00D6D6CE00CEC6C600B5D6D600E7B5
      B500C6ADB5007B7B7B008C848400525A6B004A5A6B008CB5C6006394A50018AD
      CE0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB552009452100000000000000000000000
      0000000000000000000000000000211808006352520052424200635A5A00C6B5
      AD006B6B73006B84AD00C6ADB500847B8C00B5BDC600CEDEDE00EFC6C600D6B5
      AD009494AD007B7B8400847B7B005273940084849C0084847B0000637B0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFDEBD00FF9C290063390800000000000000
      00000000000000000000211810004A424200A59C9C00737B8C005A737300AD73
      73005A6B730021527300737373005A6B6B00B5A5A500BD949400CE9494009C84
      84008C6B6B00426B6B006B5A6300425A7B00525A6B00183939000063730000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7BD00FFA53100C67318006331
      0000633100008C4A0800524242005A6B6B0073948C00D6C6D6006B7B8C007B73
      7B0084737B00847B7B007B7B840094949C007B94A5009CA5A500A5A5A500AD9C
      A5009C94A5008CA5BD00BD9CB500A59CA500A59CB500C6C6C6002939390000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7E700FFCE9400FFC6
      7300FFC67B00FFC673003952520052737300BDA5AD00EFEFEF00F7CED600C6BD
      BD00947B84005A525A004A525200636B6B005A7B8400737B7B00638484006B73
      730039525A00423939006B525200A5A5AD00D6CEDE00CED6DE00395252000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00524A4A00C6A5A500A59CAD00634A6300212131002110
      100010425A00394A63004A5A5A0052635A00847B84007B8C8C00848C8C00847B
      7300295A7B00525263000018390021314A00424252007B6B7300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424242003939390039393900393939003939390039393900394242003942
      42003942420039424200736363009C9CBD000829310010101000000808002918
      2100292131001010100018080800080000000000000000000000000000000008
      080010081000101821000018310000181800424242007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C008C8C8C0084848C0042424A0052848400A5C6CE00FFDE
      CE00F7E7D600F7EFD600F7EFE700FFEFE700D6E7DE00C6DED600CEDED600CEDE
      D600DECEC600C6C6C6006BA5AD0073A59C006B73840010102100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700CECECE005A4A4A00A58484009CA5AD00CEF7EF00F7C6
      6300DE9C5A00D6945A00D6946300D6946B00D6946B00DE946B00DE9C6B00DE94
      6B00D69C6B00DE9C6B00E79C6B00EFCE8400F7EFD60029292900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B008C6B6B007BA5A50084ADB500DEE7D600FF9C
      3900FFBD3900FFC63100F7BD3100EFAD3900EFB53900FFB53100FFAD2100FFA5
      2900EF8C1000EF941000F7941000D67B3100FFFFE70039313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B004A7B7B005AA59C00BDBDBD00EFEFE700FFB5
      5200FFD66300FFCE9400FFCE7300FFD66B00FFD66B00FFD66300FFC63900FFB5
      2900FFAD3100EF9C1800E79C1000DE844200FFFFE70042313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B005A7B7B0063A5A500DEBDBD00E7EFE700FFB5
      6300FFE77B00FFEF8400FFEF7B00FFEF7B00FFE78400FFDE8400FFCE6300FFC6
      3100FFB53100FFAD3100E79C1800DE844200FFFFE70042313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B005A7B7B00ADADA500E7BDBD00D6EFE700E7B5
      6B00FFEF7B00FFFF5A00FFFF7300FFFF5A00FFEF7300FFEF8400FFDE6B00FFD6
      3100FFC63100FFB52900E79C1800DE843900FFFFD60042313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B008C847B00B5ADAD00DEC6C600D6E7DE00E7AD
      6B00FFFF6B00FFFF9400FFFF9C00FFFF8400FFFF5A00FFF77300FFD69400FFD6
      3900EFC64200F7B53100DE9C1000D6843100FFFFC60042313900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000008080800847B7B00ADB5B500E7CECE00D6E7E700E7A5
      5A00FFFF6B00FFFFA500FFFF9C00FFFFA500FFFF7300FFFF6300FFD69400FFD6
      3900F7C64200FFB53100FF9C3100DE843900FFFFC60031292900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B0073848400B5BDBD00E7CEC600D6DEE700E79C
      5A00FFFF6B00FFFF9400FFFF9C00FFFFA500FFFF7B00FFFF6300FFDE8C00FFD6
      3100FFC63100FFAD3100FF9C3100E7843100FFFFA50042313900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B0084848C00E7CEB500FFD6C600D6D6E700DE84
      5200FFF76300FFF77B00FFFF8C00FFFF9400FFFF9400FFFF7300FFD67300FFD6
      4200FFBD2900FFBD3100FF943100C6632900FFEF9C0042313900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C9C009C9C
      9C009C9C9C009C9C9C009C9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B6B6B00AD947B00FFDED600CEEFFF00EFE7E700DE9C
      9400BD636B00CE5263007B4A3900634A31006B4231006B4A29006B4229006B39
      29006B3918006B3921005A292100B5634A00FFEFDE0042313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000636B6300C69C9C00E7FFFF00DEE7EF00E7EFEF00E7F7
      F700DEEFF700D6EFF700D6EFF700D6EFF700D6E7E700D6E7EF00D6E7EF00D6E7
      EF00D6E7EF00D6EFEF00D6EFEF00E7EFEF00D6D6D6004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00636B7300CEB5B500DEADA500DEADA500DEA5
      A500DEADAD00D6ADAD00D6ADAD00D6ADAD00D6ADAD00D6ADAD00D6ADAD00D6AD
      AD00D6ADAD00D6ADAD00DEADAD00CEA5A5005273730084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF00009C0000009C0000FFF7
      F700009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000FFFFFF0000000000000000008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF0094948C00848C
      840000000000000000008C8C8C00FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000008C8C8C0000000000000000008C8C8C00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      00000000000000000000000000000000000094949400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      000000000000000000000000000000000000000000008C948C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848C8C00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000094949400FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00948C8C00948C8C00948C8C00948C8C00948C8C00948C8C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848C8400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700EFEFEF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F7000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00B5B5B5009C9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00B5B5B5009C9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF003939390000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000F7F7F700FFFFFF003939390000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF00000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00CECECE00BDBDBD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F700000000000000000000000000000000000000000000CEFF0000CE
      FF000000000000CEFF000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00CECECE00BDBDBD00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E7000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFEF00FFD69C00FFCE
      8400FFCE8C00FFCE8400FFEFD600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF000000000000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFEF00FFD69C00FFCE
      8400FFCE8C00FFCE8400FFEFD600FFFFFF000000000000000000000000000000
      0000000000004ABDFF004ABDFF004ABDFF0021ADFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF00000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7CE00FFA54200D67B18007342
      000073420800A55A0800FF9C2900FFCE8400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7CE00FFA54200D67B18007342
      000073420800A55A0800FF9C2900FFCE8400FFFFFF000000000000000000CE9C
      9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFE7C600FF9C310073420800080000000000
      0000000000000000000029180000DE7B1800FFC67B00FFFFFF00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFE7C600FF9C310073420800080000000000
      0000000000000000000029180000DE7B1800FFC67B00FFFFFF0000000000CE9C
      9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB55A009C5A100000000000000000000000
      000000000000000000000000000031180800FF9C2100FFE7C600FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB55A009C5A100000000000000000000000
      000000000000000000000000000031180800FF9C2100FFE7C60000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21002110000000000000000000000000
      000000000000000000000000000000000000A55A1000FFC67B00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21002110000000000000000000000000
      000000000000000000000000000000000000A55A1000FFC67B0000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFF7DE00EF8C21000000000000000000000000000000
      0000000000000000000000000000000000008C4A0800FFCE8400FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFF7DE00EF8C21000000000000000000000000000000
      0000000000000000000000000000000000008C4A0800FFCE840000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21001810000000000000000000000000
      0000000000000000000000000000000000009C5A1000FFC67B00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFEFDE00F78C21001810000000000000000000000000
      0000000000000000000000000000000000009C5A1000FFC67B0000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C0000009C0000009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB552009452100000000000000000000000
      000000000000000000000000000029180800FF942100FFDEBD00FFFFFF00FFFF
      FF00F7F7F70000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF000000000000CEFF000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFB552009452100000000000000000000000
      000000000000000000000000000029180800FF9421000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00000052000031520000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700009C0000009C0000009C0000009C00000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000E7E7E700E7E7E700E7E7E700E7E7
      E7000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFDEBD00FF9C290063390800000000000000
      0000000000000000000018100000D6731800FFBD7300FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFDEBD00FF9C290063390800000000000000
      0000000000000000000018100000D6731800FFBD730000FFFF0000FFFF0000FF
      FF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000E7E7E700E7E7
      E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7BD00FFA53100C67318006331
      0000633100008C4A0800EF8C2100FFC67300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF000000000000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7BD00FFA53100C67318006331
      0000633100008C4A0800EF8C2100FFC67300FFFFFF0000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C00000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000E7E7
      E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000FF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7E700FFCE9400FFC6
      7300FFC67B00FFC67300FFEFCE00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7E700FFCE9400FFC6
      7300FFC67B00FFC67300FFEFCE00FFFFF700FFFFFF0000FFFF0000FFFF000000
      520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF0000FFFF00F7FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C00000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000FF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF005252520000FFFF0000FF
      FF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000E7E7E700E7E7E7000000FF000000FF000000FF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424242003939390039393900393939003939390039393900394242003942
      4200394242003942420039393900393939003939390042424200424242004242
      4200424242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000424242003939390039393900393939003939390039393900394242003942
      4200394242003942420039393900393939003939390000000000000052000000
      5200000052000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C00000000FF000000FF000000FF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8484008C847B009484
      7B008C847B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C00000000000000520000CE
      FF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C
      0000009C0000009C0000009C0000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7D6AD00F7BD7B00F7C6
      7B00F7D6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFEFEF00F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F7000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00009C0000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000FF00000000000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000FF000000FF00000000000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000FF000000FF00000000000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000FF000000FF000000FF000000
      FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF00009C0000009C0000FFF7
      F700009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000FFFFFF0000000000000000008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF00009C0000009C0000FFF7
      F700009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000FFFFFF0000000000000000008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF0094948C00848C
      840000000000000000008C8C8C00FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF0094948C00848C
      840000000000000000008C8C8C00FFFFFF000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000FF00000000000000FF000000
      FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000FFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000008C8C8C0000000000000000008C8C8C00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000008C8C8C0000000000000000008C8C8C00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000FF000000FF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      00000000000000000000000000000000000094949400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      00000000000000000000000000000000000094949400FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF00000000000000FF0000000000000000000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      000000000000000000000000000000000000000000008C948C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF00000000000000
      0000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      000000000000000000000000000000000000000000008C948C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00009C0000009C0000FFFF
      FF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFF
      FF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848C8C00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848C8C00FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000094949400FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000094949400FFFFFF00009C0000009C0000FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00948C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00948C8C00948C8C00948C8C00948C8C00948C8C00948C8C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFF
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000FFFF
      FF00948C8C00948C8C00948C8C00948C8C00948C8C00948C8C008C8C8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848C8400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848C8400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00000000000000000000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000CEFF0000CEFF00000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E70000000000E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004ABDFF004ABDFF004ABDFF0021ADFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700009C0000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C00000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C0000009C000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004ABDFF004ABDFF004ABDFF0021ADFF0000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF0000000000ADADAD00ADADAD00848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF0000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000CE9C9C000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848484000000000000000000CE9C
      9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF000000000000000000ADADAD00848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00E7E7E700E7E7E700E7E7E700E7E7E70000000000CE9C9C00CE9C
      9C00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400ADADAD000000000000000000CE9C
      9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000ADADAD008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400ADADAD000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000004ABD
      FF004ABDFF004ABDFF0021ADFF00000000000000000000000000000000000000
      0000000000000000000000000000ADADAD008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000520000315200000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD00000000000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C004ABD
      FF004ABDFF00000052000000520000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000ADADAD0084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000E7E7E700E7E7E700CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD00000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C004ABD
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00000000000000000000000000ADADAD0084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD00000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000000000ADADAD0084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C
      0000009C0000009C000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000FFFF0000FFFF000000
      520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF0000FFFF00F7FFFF0000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C
      0000009C0000009C000000000000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000520000315200000000000000000000000000CE9C9C0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000000000000000000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C0000009C
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C00000000000000000000000000005252520000FFFF0000FF
      FF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD00000000000000000000FFFF0000FFFF0000FF
      FF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000CE9C9C00CE9C
      9C000094DE000094DE000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C00000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000052000000
      5200000052000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C0000009C0000009C00000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD00000000000000000000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000CE9C9C00CE9C
      9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700009C0000009C0000009C00000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C00000000000000000000000000000000520000CE
      FF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700009C0000009C00000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E70000FFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD00000000000000000000FFFF0000FFFF000000
      520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF0000FFFF00F7FFFF0000000000CE9C9C00CE9C
      9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      52000031520000000000000000000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C0000009C00000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C000000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700009C00000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E70000FFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD0000000000000000005252520000FFFF0000FF
      FF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF00000000000000000000FFFF00CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      310000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700009C000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C0000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E70000FFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD00000000000000000000000000000052000000
      5200000052000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000520000FFFF0000FF
      FF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      310000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000000000000000
      000000FFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E70000FFFF00E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD000000000000000000000000000000520000CE
      FF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000052000000520000FFFF0000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF00F7FFFF000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      31000000000000000000FF9C3100000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      310000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000000000000000
      00000000000000FFFF00E7E7E700E7E7E700E7E7E700E7E7E70000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00E7E7E700E7E7E700E7E7E700E7E7E70000FF
      FF00E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084848400ADADAD0000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000520000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF0000FFFF0000000000000000000000000000000000ADADAD00848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C310000000000FF9C3100000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      31000000000000000000FF9C3100000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E70000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD00000000000000000000CEFF0000CE
      FF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00000000000000000000000000ADADAD0084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      00000000000000000000FF9C310000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C310000000000FF9C3100000000000000000000000000000000000000
      00000000000000000000E7E7E70000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000520000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000
      00000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      00000000000000000000FF9C3100000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084848400ADADAD000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      31000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000
      00000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000
      00000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400ADADAD0000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000
      00000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000
      00000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400ADADAD000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      31000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000
      00000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400ADADAD00000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00E7E7E70000FFFF0000FFFF0000FFFF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400ADADAD00ADADAD000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00E7E7E700E7E7E7000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400ADADAD000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C310000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7
      E700E7E7E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400ADADAD00ADADAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7
      E700E7E7E700E7E7E7000000000000000000E7E7E70000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400ADADAD00ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000FF000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      00000000FF000000FF000000FF000000FF000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7E700E7E7
      E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400ADADAD00ADADAD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000FF000000FF000000FF00FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E7000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008484840084848400ADADAD00ADADAD00ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      0000ADADAD00ADADAD00ADADAD00848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000000000000000000000FF
      FF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADAD
      AD00848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000FF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF00FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF00FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000FF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF00FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      FF0000000000000000000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      FF00000000000000FF000000FF000000FF000000FF00009C00000000FF000000
      FF00009C00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E7000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00009C0000009C0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E7000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      FF000000FF000000FF000000FF000000FF00009C00000000FF00009C00000000
      FF00009C0000009C0000009C0000009C00000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      FF00000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00009C0000009C0000009C0000009C0000009C000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF00000000000000FF000000FF000000
      FF000000FF00009C00000000FF000000FF00009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF00009C0000009C00000000FF00009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E7000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF00009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF00009C00000000FF00009C00000000FF00009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00009C00000000FF00009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00000000000000FF00000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF00009C0000009C00000000FF00009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00009C00000000FF00009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      00000000000000000000CE9C9C00CE9C9C000000000000000000000000000000
      0000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      000000000000CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF00009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00009C00000000FF00009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      00000000000000000000CE9C9C00CE9C9C000000000000000000000000000000
      0000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      000000000000CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF00009C00000000FF00009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000000000000000000000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      00000000000000000000CE9C9C00CE9C9C000000000000000000000000000000
      0000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      000000000000CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000
      FF000000FF00009C00000000FF000000FF00009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      00004ABDFF004ABDFF004ABDFF0021ADFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00009C00000000
      FF00009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      00000000000000000000CE9C9C00CE9C9C000000000000000000000000000000
      0000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      000000000000CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000
      FF000000FF000000FF000000FF00009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000CE9C9C00CE9C
      9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00009C0000009C0000009C0000009C0000009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000FF000000
      FF000000FF00009C00000000FF00009C0000009C0000009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000CE9C9C00CE9C
      9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000FF000000FF000000FF000000FF000000FF000000FF00009C00000000
      FF00009C0000009C0000009C0000009C00000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF00009C0000009C00000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C0000000000000000000000000000CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000FF000000FF000000FF000000FF00009C00000000FF000000
      FF00009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF00009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000FF000000FF000000FF000000FF000000FF000000FF00009C
      0000009C00000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C0000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C0000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E7000000FF000000FF000000FF00009C00000000FF000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C000000
      000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      00000000000000000000009C0000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000FF000000FF0000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C000000000000009C
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000052000031520000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000FF000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C00000000000000FFFF0000FFFF0000FFFF00CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00000000000000
      0000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C00000000000000FFFF00000052000000520000FF
      FF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00D69CA500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000009C0000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C00000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9C9C00CE9C9C00CE9C9C000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000000000000000000000FFFF0000FFFF000000520000FF
      FF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000FFFF00F7FFFF000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000009C000000000000009C000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9C9C00CE9C9C00000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C000000000000000000000000000000
      0000000000000000000000000000CE9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000005252520000FFFF0000FFFF0000FF
      FF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FFFF000000
      000000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000009C0000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9C9C0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000005200000052000000
      52000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000000000000000000000000000000000000000520000CEFF0000CE
      FF0000CEFF0000CEFF000000520000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      00000000000000000000000000000000000000000000FFFFFF00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031FFFF0031FFFF0031CEFF0031CEFF0031CE
      FF003131310031313100009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000031FFFF0031CEFF0031CEFF0031CEFF0031CEFF0031CEFF0031CE
      FF0031CEFF0031CEFF0031313100009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C31000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000031FFFF0031CEFF0031CEFF00313131000031310000000000000000000000
      000031FFFF0031CEFF0031CEFF0031313100009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C000000000000009C
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000031FF
      FF0031CEFF0031CEFF0031313100000000000000000000000000000000000000
      00000000000031FFFF0031CEFF0031CEFF0031313100009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C000000000000009C
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000031CE
      FF0031CEFF003131310000000000000000000000000000000000000000000000
      0000000000000000000031FFFF0031CEFF0031313100009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C000000000000009C
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031FFFF0031CE
      FF00313131000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031CEFF0031CEFF0031313100009C0000009C
      0000009C0000009C0000009C000000000000009C0000009C000000000000009C
      0000009C000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C00000000000000000000000000000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031FFFF0031CE
      FF00313131000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031FFFF0031CEFF003131310000000000009C
      0000009C0000009C0000009C000000000000009C0000009C000000000000009C
      0000009C0000009C000000000000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031FFFF0031CE
      FF00313131000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031FFFF0031CEFF0031313100000000000000
      000000000000009C0000009C000000000000009C0000009C000000000000009C
      00000000000000000000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031FFFF0031CE
      FF00313131000000000000000000000000003131310000000000000000000000
      000000000000000000000000000031FFFF0031CEFF0031313100000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C00000000000000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000031CE
      FF0031CEFF0031313100000000000000000031FFFF0031313100000000000000
      0000000000000000000031FFFF0031CEFF0031CEFF0031313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000009C0000009C00000000000000000000009C0000009C000000000000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000031FF
      FF0031CEFF0031CEFF00313131000000000031FFFF0031CEFF00313131000000
      0000000000000031310031CEFF0031CEFF003131310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C000000000000009C00000000000000000000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000031FFFF0031CEFF0031CEFF00313131003131310031CEFF0031CEFF003131
      31003131310031CEFF0031CEFF0031CEFF003131310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFF
      FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000031FFFF0031CEFF0031CEFF0031FFFF0031CEFF0031CEFF0031CE
      FF003131310031CEFF0031CEFF00313131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000031FFFF0031FFFF0031FFFF0031CEFF0031CEFF0031CE
      FF0031FFFF003131310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000031CEFF0031CEFF0031FFFF0031FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031CEFF0031FFFF0031FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00B5B5B5009C9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF003939390000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFE7B500FFAD8C00FFAD8C00FFE7B500FFAD8C00FFAD
      8C00FF8C6B00FF8C6B00FFBD4A00FF734A00FF734A00DE940000BD7B0000BD7B
      0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B
      0000DE4A0000DE4A0000DE4A0000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00CECECE00BDBDBD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500F7CE
      A500F7CEA500F7CEA500FFAD8C00F7CEA500F7CEA500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FF734A00FF734A00BD7B0000BD7B0000BD7B0000BD7B0000DE4A
      0000DE4A0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A0000DE4A
      00006B6B6B0084008400BD390000DE4A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F7000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500FFC66B00FFC6
      6B00FFC66B00FFC66B00FFC66B00FFAD8C00FFE7B500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FFAD2100BD7B0000BD7B0000BD7B00009494940084848400BD7B
      0000949494000094DE00A5A5A500DE4A0000DE4A0000FFAD2100FFAD2100FF73
      4A0021ADFF000000FF000094DE00BD3900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFEF00FFD69C00FFCE8400FFCE8C00FFCE8400FFEFD600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F7000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500F7CE
      A500FFAD8C00FFAD8C00F7CEA500FFAD8C00F7CEA500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FF734A00FF734A00DE940000DE940000BD7B0000A5A5A5000094
      DE008CFFAD0000FFFF00A5A5A500DE4A0000FF734A00FFE7B500FFE7B500C6C6
      C600C6C6C6008C6BFF008C6BFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFFFFF00FFE7
      CE00FFA54200D67B18007342000073420800A55A0800FF9C2900FFCE8400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700000000000000000000CEFF0000CE
      FF000000000000CEFF000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFE7B500FFAD8C00FFC6
      6B00FFC66B00FFBD4A00FF734A00FFAD2100DE940000DE940000BD7B00008CFF
      AD008CFFAD008CFFAD0000000000BD7B0000DE940000DE940000FFAD0000BD7B
      000021ADFF000000FF000000FF00BD3900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFE7C600FF9C
      3100734208000800000000000000000000000000000029180000DE7B1800FFC6
      7B00FFFFFF00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000094310000FFE7B500FFAD8C00FFAD
      8C00F7CEA500F7CEA500F7CEA500F7CEA500FFE7B500FFE7B500FFAD8C00FFAD
      8C00FFC66B00FFC66B00FFC66B00FFBD4A00FFBD4A00FF734A00FFAD2100A5A5
      A5008CFFAD00B5B5B5008CFFAD00A5A5A500BD7B0000DE940000DE940000BD7B
      00006B6B6B007B7B7B0094630000DE9400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFFFFF00FFB55A009C5A
      100000000000000000000000000000000000000000000000000031180800FF9C
      2100FFE7C600FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF000000000000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000094310000B5B5B500B5B5B500B5B5
      B500B5B5B500B5B5B500B5B5B500B5B5B500A5A5A500A5A5A500C6C6C600C6C6
      C600FFAD8C00FFE7B500FFE7B500FFE7B500FFAD8C00FFC66B00FFC66B00C6C6
      C600C6C6C600C6C6C600ADADAD00C6C6C600FFC66B00FF734A00FF734A00FF73
      4A00FF734A00FF734A00FFBD4A00FFC66B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFEFDE00F78C21002110
      000000000000000000000000000000000000000000000000000000000000A55A
      1000FFC67B00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000094310000B5B5B50000000000A5A5
      A500B5B5B50000000000A5A5A50000000000A5A5A500A5A5A50094949400A5A5
      A500F7CEA500F7CEA500F7CEA500F7CEA500FFAD8C00FFC66B00FFC66B00FFAD
      8C00FFAD8C00F7CEA500F7CEA500F7CEA500FFAD8C00FFAD8C00FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F7F7F700FFFFFF00FFF7DE00EF8C21000000
      0000000000000000000000000000000000000000000000000000000000008C4A
      0800FFCE8400FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF000000000000000000000000007B7B7B000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000094310000A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5
      A500FFC66B00FFC66B00FFBD4A00FFC66B00FFC66B00FFC66B00FFC66B00FFC6
      6B00FFC66B00FFC66B00FFC66B00FFC66B00FFAD8C00FFAD8C00FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00F7CEA5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00000000000000000000000000F7F7F700FFFFFF00FFEFDE00F78C21001810
      0000000000000000000000000000000000000000000000000000000000009C5A
      1000FFC67B00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF000000000000000000F7F7F700FFFFFF00FFFF
      FF0000000000000000000000000000000000FFFFFF00000000000000000000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000094310000DE4A0000DE4A0000DE4A
      0000BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE940000BD7B0000DE940000DE940000FFBD4A00FFBD4A00FFBD
      4A00FFBD4A00FFBD4A00FFC66B00C6C6C600F7CEA500C6FF6B00D6FF8C00D6FF
      8C00FF734A00943100009463000094310000E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F700FFFF
      FF00FFFFFF000000000000000000F7F7F700FFFFFF00FFFFFF00FFB552009452
      100000000000000000000000000000000000000000000000000029180800FF94
      2100FFDEBD00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000000000FFFFFF007B7B7B007B7B7B007B7B
      7B001010100000000000000000007B7B7B007B7B7B000808080000CEFF0000CE
      FF0000CEFF0000CEFF0010101000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00D69CA500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000094310000DE4A0000DE4A00006B6B
      6B006B8CFF007B7B7B00000000006B8CFF00BD390000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000DE940000FFBD4A00DE940000DE94
      0000BD7B000094949400949494008C6BFF0094949400A5A5A500C6FF6B00D6FF
      8C00BD7B0000840000009463000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B001010100000000000EFEFEF00FFFFFF00FFFFFF00FFDEBD00FF9C
      2900633908000000000000000000000000000000000018100000D6731800FFBD
      7300FFFFFF00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0010101000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000094310000FFE7B500FFAD21000000
      00000094DE004ABDFF004ABDFF004ABDFF006B6B6B00DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FF8C6B00DE94
      0000A5A5A500A5A5A5008C6BFF00000000008C6BFF00A5A5A500A5A5A500D6FF
      8C00BD7B0000840000009463000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000008080800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFE7
      BD00FFA53100C673180063310000633100008C4A0800EF8C2100FFC67300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000094310000FFE7B500FFAD21000000
      000021ADFF004ABDFF004ABDFF004ABDFF00FF734A00DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000BD7B0000FFBD4A00A5A5
      A500A5A5A500C66BFF008C6BFF006B6B6B008C6BFF00C66BFF0094949400C6C6
      C600BD7B0000840000009463000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B00F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF7E700FFCE9400FFC67300FFC67B00FFC67300FFEFCE00FFFFF700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F70000000000000000000000000000CE
      FF0000CEFF0000CEFF000000000000CEFF000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000094310000DE940000DE9400000000
      00000094DE004A73FF006B8CFF004ABDFF00BD7B0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFBD4A009494
      9400B5B5B500D68CFF00949494009494940094949400C6C6C600C6C6C600B5B5
      B500BD7B0000840000005252520094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F7000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000094310000DE4A0000DE940000BD7B
      0000FF734A00BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A
      0000BD390000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFC66B00FF73
      4A00FFC66B00FFC66B00C6FF6B00C6FF6B00C6FF6B00D6FF8C00D6FF8C00D6FF
      8C00BD7B0000840000009463000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B00424242003939390039393900393939003939
      3900393939003942420039424200394242003942420039393900393939003939
      3900424242004242420042424200424242000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000CEFF00FFFFFF0000CE
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0010101000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF000000000000000000000000000000000094310000DE940000DE940000DE94
      0000FFAD0000DE940000DE940000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00FFBD4A00BD7B
      0000FFC66B00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF6B00D6FF
      8C00BD7B0000840000009463000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00949494008C8C8C008C8C8C008C8C8C008C8C
      8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C008C8C
      8C008C8484008C847B0094847B008C847B000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000094310000DE940000DE4A0000DE94
      0000DE940000DE940000DE940000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00DE940000FFBD
      4A00FFBD4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00FFC6
      6B0094630000840000006B6B6B0094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00D6D6FF00E7E7FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E7E7E700F7F7F700F7F7F700F7F7F700F7F7
      F700F7D6AD00F7BD7B00F7C67B00F7D6A5000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000094310000DE940000DE940000DE4A
      0000DE940000DE940000FFAD0000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000DE940000DE940000BD7B0000FFC6
      6B00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF6B00D6FF8C00BD7B
      000084000000732100009431000094310000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF000000FF000000FF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000094310000DE940000FFBD4A00FF73
      4A00FFBD4A00FFBD4A00FFBD4A00FFBD4A00FF734A00DE4A0000DE4A0000DE4A
      0000DE4A0000BD390000BD390000BD390000BD7B0000DE940000FFBD4A00FFBD
      4A00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00BD7B
      00008400000094310000BD39000073210000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF000000FF000000
      FF00FFFFFF00FFFFFF000000FF000000FF00FFFFFF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF000000FF000000
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000DE940000FFAD8C00FF8C
      6B00FFC66B00FFBD4A00FFBD4A00FFBD4A00FFBD4A00DE4A0000DE4A0000DE4A
      0000BD390000DE4A0000BD390000BD7B0000BD7B0000BD7B0000BD7B0000FFBD
      4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00C6FF6B00C6FF6B00D6FF8C009463
      000084000000732100009431000084000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000DE940000BD7B
      0000BD7B0000DE940000DE940000DE940000DE940000BD7B0000DE4A0000DE4A
      0000BD390000BD390000BD3900009463000094630000BD7B0000E7E7E700FFC6
      6B00D6FF8C00D6FF8C00D6FF8C00C6FF6B0094630000734A0000734A00009431
      0000840000007321000084000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000
      FF00FFFFFF00FFFFFF0010101000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF00FFFFFF00101010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      000094310000943100009431000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF000000000000000000000000000000000094310000FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00FFE7B500FFAD8C00FFAD8C00FFE7B500FFAD
      8C00FFAD8C00FF8C6B00FF8C6B00FFBD4A00FF734A00FF734A00DE940000BD7B
      0000BD7B0000BD7B0000BD7B000000000000FFFFFF00FFFFFF00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0008080800000000000000000094310000FFE7B500F7CE
      A500F7CEA500F7CEA500F7CEA500FFAD8C00F7CEA500F7CEA500FFE7B500FFAD
      8C00FFC66B00FFBD4A00FF734A00FF734A00BD7B0000BD7B0000BD7B0000BD7B
      0000DE4A0000DE4A0000BD7B000000000000FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000808
      0800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500FFC6
      6B00FFC66B00FFC66B00FFC66B00FFC66B00FFAD8C00FFE7B500FFE7B500FFAD
      8C00FFC66B00FFBD4A00FFAD2100BD7B0000BD7B0000BD7B0000949494008484
      8400BD7B0000949494000094DE000000000000000000FFFFFF00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000080808000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500F7CE
      A500F7CEA500FFAD8C00FFAD8C00F7CEA500FFAD8C00F7CEA500FFE7B500FFAD
      8C00FFC66B00FFBD4A00FF734A00FF734A00DE940000DE940000BD7B0000A5A5
      A5000094DE008CFFAD0000FFFF00A5A5A50000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000080000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500F7CE
      A500FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFE7B500FFAD
      8C00FFC66B00FFC66B00FFBD4A00FF734A00FFAD2100DE940000DE940000BD7B
      00008CFFAD008CFFAD008CFFAD000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF00000008000021080000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500FFAD
      8C00FFAD8C00F7CEA500F7CEA500F7CEA500F7CEA500FFE7B500FFE7B500FFAD
      8C00FFAD8C00FFC66B00FFC66B00FFC66B00FFBD4A00FFBD4A00FF734A00FFAD
      2100A5A5A5008CFFAD00B5B5B5008CFFAD0000000000FFFFFF00FFFFFF00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FF
      FF0000CEFF0000CEFF0000CEFF000000000000CEFF0000FFFF00000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000B5B5B500B5B5
      B500B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500A5A5A500A5A5A500C6C6
      C600C6C6C600FFAD8C00FFE7B500FFE7B500FFE7B500FFAD8C00FFC66B00FFC6
      6B00C6C6C600C6C6C600C6C6C600ADADAD0000000000FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000CEFF0000CEFF000000000000CEFF000000000000FFFF0000FF
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F700FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000B5B5B5000000
      0000A5A5A500B5B5B50000000000A5A5A50000000000A5A5A500A5A5A5009494
      9400A5A5A500F7CEA500F7CEA500F7CEA500F7CEA500FFAD8C00FFC66B00FFC6
      6B00FFAD8C00FFAD8C00F7CEA500F7CEA5000000000000000000FFFFFF00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000CEFF00000000000000000000FFFF0000FFFF0000FF
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B001010100000000000000000007B7B7B007B7B7B00080808000000
      0000000000000000000000000000101010000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000A5A5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5
      A500A5A5A500FFC66B00FFC66B00FFBD4A00FFC66B00FFC66B00FFC66B00FFC6
      6B00FFC66B00FFC66B00FFC66B00FFC66B00FFC66B0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF000000
      000000FFFF0000FFFF0000FFFF000000000000CEFF0000FFFF0000FFFF0000FF
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000008080800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000F7F7F700FFFFFF00FFFFFF00FFFFFF000808
      0800FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE4A0000DE4A
      0000DE4A0000BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE940000BD7B0000DE940000DE940000FFBD4A00FFBD
      4A00FFBD4A00FFBD4A00FFBD4A00FFC66B00C6C6C60000000000FFFFFF000000
      0000000000000000000000000000000000009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000CEFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE4A0000DE4A
      00006B6B6B006B8CFF007B7B7B00000000006B8CFF00BD390000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000DE940000FFBD4A00DE94
      0000DE940000BD7B000094949400949494008C6BFF0000000000000000000000
      0000D6FF8C00BD7B000084000000946300009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500FFAD
      2100000000000094DE004ABDFF004ABDFF004ABDFF006B6B6B00DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FF8C
      6B00DE940000A5A5A500A5A5A5008C6BFF00000000008C6BFF00A5A5A500A5A5
      A500D6FF8C00BD7B000084000000946300009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF000000000000CEFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFE7B500FFAD
      21000000000021ADFF004ABDFF004ABDFF004ABDFF00FF734A00DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000BD7B0000FFBD
      4A00A5A5A500A5A5A500C66BFF008C6BFF006B6B6B008C6BFF00C66BFF009494
      9400C6C6C600BD7B000084000000946300009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000CEFF000000000000FFFF0000FFFF0000FFFF0000CEFF000000000000CE
      FF000000000000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFEFEF00009C0000009C
      0000FFF7F700009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00D6D6
      D600FFFFFF00FFFFFF00FFFFFF00F7F7F700FFFFFF00FFFFFF00FFFFFF00EFEF
      EF00FFFFFF00FFFFFF00FFFFFF00101010000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE940000DE94
      0000000000000094DE004A73FF006B8CFF004ABDFF00BD7B0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFBD
      4A0094949400B5B5B500D68CFF00949494009494940094949400C6C6C600C6C6
      C600B5B5B500BD7B000084000000525252009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF00000000000000000000FFFF0000CEFF000000000000CE
      FF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00CEC6
      C600F7F7F700F7F7F700F7F7F7002121EF00FFFFFF00F7F7F700F7F7F700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE4A0000DE94
      0000BD7B0000FF734A00BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A
      0000DE4A0000BD390000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFC6
      6B00FF734A00FFC66B00FFC66B00C6FF6B00C6FF6B00C6FF6B00D6FF8C00D6FF
      8C00D6FF8C00BD7B000084000000946300009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000CEFF000000000000FFFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFF
      FF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE940000DE94
      0000DE940000FFAD0000DE940000DE940000DE940000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00FFBD
      4A00BD7B0000FFC66B00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF
      6B00D6FF8C00BD7B000084000000946300009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000CEFF0000CEFF00000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF000000
      000000000000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF004A4A
      CE00FFFFFF00FFFFFF001010FF000000FF00FFFFFF001010FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE940000DE4A
      0000DE940000DE940000DE940000DE940000DE940000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00DE94
      0000FFBD4A00FFBD4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF
      8C00FFC66B0094630000840000006B6B6B009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF0000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000000000000000000000000000FFFF
      FF00FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00524A
      CE000000FF00FFFFFF000000FF00FFFFFF00FFFFFF002121FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE940000DE94
      0000DE4A0000DE940000DE940000FFAD0000DE940000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000DE4A0000BD7B0000DE940000DE940000BD7B
      0000FFC66B00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF6B00D6FF
      8C00BD7B00008400000073210000943100009431000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF00FFFFFF000000000000000000FFFFFF00009C0000FFFFFF000000
      000000000000FFFFFF00009C0000FFFFFF000000000000000000FFFFFF00009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00CECE
      CE000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF001818FF001010
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000DE940000FFBD
      4A00FF734A00FFBD4A00FFBD4A00FFBD4A00FFBD4A00FF734A00DE4A0000DE4A
      0000DE4A0000DE4A0000BD390000BD390000BD390000BD7B0000DE940000FFBD
      4A00FFBD4A00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF
      8C00BD7B00008400000094310000BD3900007321000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00009C0000009C
      0000FFFFFF00009C0000FFFFFF00FFFFFF00009C0000009C0000009C0000FFFF
      FF00FFFFFF00009C0000009C0000009C0000FFFFFF00FFFFFF00009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00CECE
      CE00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF00FFFFFF00FFFFFF00101010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE940000FFAD
      8C00FF8C6B00FFC66B00FFBD4A00FFBD4A00FFBD4A00FFBD4A00DE4A0000DE4A
      0000DE4A0000BD390000DE4A0000BD390000BD7B0000BD7B0000BD7B0000BD7B
      0000FFBD4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00C6FF6B00C6FF6B00D6FF
      8C00946300008400000073210000943100008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000848C8C00FFFFFF00009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000FFFFFF00CECE
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DE94
      0000BD7B0000BD7B0000DE940000DE940000DE940000DE940000BD7B0000DE4A
      0000DE4A0000BD390000BD390000BD3900009463000094630000BD7B00000000
      0000FFC66B00D6FF8C00D6FF8C00D6FF8C00C6FF6B0094630000734A0000734A
      0000943100008400000073210000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094949400FFFFFF00009C0000009C
      0000FFFFFF00009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECE
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F7F700FFFFFF00FFFF
      FF00FFFFFF00009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000FFFFFF00948C8C00948C8C00948C8C0052525200CECECE00CECECE00E7E7
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000848C
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000080808000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF00E7E7
      E700E7E7E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00E7E7E70000000000E7E7E700E7E7E700E7E7E700EFEFEF00E7E7E700E7E7
      E7000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF00E7E7E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700EFEFEF00E7E7E700000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E7000000FF000000FF000000FF000000
      FF000000FF000000FF00E7E7E700E7E7E7000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E7000000
      FF000000FF000000FF00E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF00000000000000000000000000FFFF
      FF000000000000000000FFFFFF00000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000FF000000FF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000E7E7E700EFEFEF00E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000008484
      840000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF000000FF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000943100009431000094310000943100009431
      0000943100009431000094310000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004ABDFF004ABDFF004ABDFF0021ADFF0000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004ABDFF004ABDFF004ABDFF0021AD
      FF00000000000000000000000000000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094310000FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFE7B500FFAD8C00FFAD8C00FFE7B500FFAD8C00FFAD
      8C00FF8C6B00FF8C6B00FFBD4A00FF734A00FF734A00DE940000BD7B0000BD7B
      0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B0000BD7B
      0000DE4A0000DE4A0000DE4A0000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C004ABDFF004ABDFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500F7CE
      A500F7CEA500F7CEA500FFAD8C00F7CEA500F7CEA500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FF734A00FF734A00BD7B0000BD7B0000BD7B0000BD7B0000DE4A
      0000DE4A0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A0000DE4A
      00006B6B6B000000FF00BD390000DE4A00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700EFEFEF00000000000000000000000000E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500FFC66B00FFC6
      6B00FFC66B00FFC66B00FFC66B00FFAD8C00FFE7B500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FFAD2100BD7B0000BD7B0000BD7B00009494940084848400BD7B
      0000949494000094DE00A5A5A500DE4A0000DE4A0000FFAD2100FFAD2100FF73
      4A0021ADFF000000FF000094DE00BD3900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000004ABD
      FF004ABDFF004ABDFF0021ADFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000E7E7E700E7E7
      E700E7E7E700000000000000000000000000E7E7E700E7E7E70000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500F7CE
      A500FFAD8C00FFAD8C00F7CEA500FFAD8C00F7CEA500FFE7B500FFAD8C00FFC6
      6B00FFBD4A00FF734A00FF734A00DE940000DE940000BD7B0000A5A5A5000094
      DE008CFFAD0000FFFF00A5A5A500DE4A0000FF734A00FFE7B500FFE7B500C6C6
      C600C6C6C6000000FF008C6BFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C004ABD
      FF004ABDFF00000052000000520000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500F7CEA500FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C00FFE7B500FFAD8C00FFC6
      6B00FFC66B00FFBD4A00FF734A00FFAD2100DE940000DE940000BD7B00008CFF
      AD008CFFAD008CFFAD0000000000BD7B0000DE940000DE940000FFAD0000BD7B
      000021ADFF000000FF000000FF00BD3900000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C004ABD
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE000094DE000094
      DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500FFAD8C00FFAD
      8C00F7CEA500F7CEA500F7CEA500F7CEA500FFE7B500FFE7B500FFAD8C00FFAD
      8C00FFC66B00FFC66B00FFC66B00FFBD4A00FFBD4A00FF734A00FFAD2100A5A5
      A5008CFFAD00B5B5B5008CFFAD00A5A5A500BD7B0000DE940000DE940000BD7B
      00006B6B6B007B7B7B0094630000DE9400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000CE9C9C00CE9C9C0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE000094DE004ABD
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000B5B5B500B5B5B500B5B5
      B500B5B5B500B5B5B500B5B5B500B5B5B500A5A5A500A5A5A500C6C6C600C6C6
      C600FFAD8C00FFE7B500FFE7B500FFE7B500FFAD8C00FFC66B00FFC66B00C6C6
      C600C6C6C600C6C6C600ADADAD00C6C6C600FFC66B00FF734A00FF734A00FF73
      4A00FF734A00FF734A00FFBD4A00FFC66B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000520000315200000000000000000000000000CE9C9C0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000FF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000520000315200000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000B5B5B50000000000A5A5
      A500B5B5B50000000000A5A5A50000000000A5A5A500A5A5A50094949400A5A5
      A500F7CEA500F7CEA500F7CEA500F7CEA500FFAD8C00FFC66B00FFC66B00FFAD
      8C00FFAD8C00F7CEA500F7CEA500F7CEA500FFAD8C00FFAD8C00FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00FFAD8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000CE9C9C00CE9C
      9C000094DE000094DE000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      FF000000FF000000FF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5
      A500FFC66B00FFC66B00FFBD4A00FFC66B00FFC66B00FFC66B00FFC66B00FFC6
      6B00FFC66B00FFC66B00FFC66B00FFC66B00FFAD8C00FFAD8C00FFAD8C00FFAD
      8C00FFAD8C00FFAD8C00FFAD8C00F7CEA5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000CE9C9C00CE9C
      9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      FF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000FFFF00000052000000520000FFFF0000FFFF0000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE4A0000DE4A0000DE4A
      0000BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE940000BD7B0000DE940000DE940000FFBD4A00FFBD4A00FFBD
      4A00FFBD4A00FFBD4A00FFC66B00C6C6C600F7CEA500C6FF6B00D6FF8C00D6FF
      8C00FF734A009431000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF0000FFFF00F7FFFF0000000000CE9C9C00CE9C
      9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      5200003152000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000520000FFFF0000FFFF00000052000000520000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF00F7FF
      FF0000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE4A0000DE4A00006B6B
      6B004ABDFF007B7B7B00000000004ABDFF00BD390000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000DE940000FFBD4A00DE940000DE94
      0000BD7B000094949400949494000000FF0094949400A5A5A500C6FF6B00D6FF
      8C00BD7B00008400000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005252520000FFFF0000FF
      FF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF00000000000000000000FFFF00CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00005252520000FFFF0000FFFF0000FFFF0000005200000052000000520000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF00000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500FFAD21000000
      00004ABDFF004ABDFF004ABDFF004ABDFF006B6B6B00DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FF8C6B00DE94
      0000A5A5A500A5A5A5000000FF00000000000000FF00A5A5A500A5A5A500D6FF
      8C00BD7B00008400000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000052000000
      5200000052000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000520000FFFF0000FF
      FF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000520000005200000052000000520000CEFF000000840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000FFE7B500FFAD21000000
      00004ABDFF004ABDFF004ABDFF004ABDFF00FF734A00DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000BD7B0000FFBD4A00A5A5
      A500A5A5A5000000FF000000FF006B6B6B000000FF000000FF0094949400C6C6
      C600BD7B00008400000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000520000CE
      FF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000052000000520000FFFF0000FFFF0000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF00F7FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000520000CEFF0000CEFF0000CEFF0000CEFF000000520000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E7000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE940000DE9400000000
      00004ABDFF004ABDFF004ABDFF004ABDFF00BD7B0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFBD4A009494
      9400B5B5B500D68CFF00949494009494940094949400C6C6C600C6C6C600B5B5
      B500BD7B00008400000052525200943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000520000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000E7E7E700E7E7E700E7E7E7000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE4A0000DE940000BD7B
      0000FF734A00BD7B0000BD7B0000BD7B0000DE4A0000DE4A0000DE4A0000DE4A
      0000BD390000DE4A0000BD7B0000BD7B0000BD7B0000DE940000FFC66B00FF73
      4A00FFC66B00FFC66B00C6FF6B00C6FF6B00C6FF6B00D6FF8C00D6FF8C00D6FF
      8C00BD7B00008400000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF00000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C310000000000E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E70000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE940000DE940000DE94
      0000FFAD0000DE940000DE940000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00FFBD4A00BD7B
      0000FFC66B00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF6B00D6FF
      8C00BD7B00008400000094630000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000520000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C310000000000E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      00000000000000000000E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE940000DE4A0000DE94
      0000DE940000DE940000DE940000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000BD7B0000BD7B0000BD7B0000FFBD4A00DE940000FFBD
      4A00FFBD4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00FFC6
      6B0094630000840000006B6B6B00943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      31000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000FF9C3100FF9C3100FF9C310000000000E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE940000DE940000DE4A
      0000DE940000DE940000FFAD0000DE940000DE4A0000DE4A0000DE4A0000DE4A
      0000DE4A0000DE4A0000DE4A0000BD7B0000DE940000DE940000BD7B0000FFC6
      6B00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00C6FF6B00D6FF8C00BD7B
      0000840000007321000094310000943100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094310000DE940000FFBD4A00FF73
      4A00FFBD4A00FFBD4A00FFBD4A00FFBD4A00FF734A00DE4A0000DE4A0000DE4A
      0000DE4A0000BD390000BD390000BD390000BD7B0000DE940000FFBD4A00FFBD
      4A00FFC66B00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00D6FF8C00BD7B
      00008400000094310000BD390000732100000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      31000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DE940000FFAD8C00FF8C
      6B00FFC66B00FFBD4A00FFBD4A00FFBD4A00FFBD4A00DE4A0000DE4A0000DE4A
      0000BD390000DE4A0000BD390000BD7B0000BD7B0000BD7B0000BD7B0000FFBD
      4A00C6FF6B00D6FF8C00D6FF8C00D6FF8C00C6FF6B00C6FF6B00D6FF8C009463
      0000840000007321000094310000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE940000BD7B
      0000BD7B0000DE940000DE940000DE940000DE940000BD7B0000DE4A0000DE4A
      0000BD390000BD390000BD3900009463000094630000BD7B000000000000FFC6
      6B00D6FF8C00D6FF8C00D6FF8C00C6FF6B0094630000734A0000734A00009431
      0000840000007321000084000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C310000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C31000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000000000008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000FFFF
      FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      0000ADADAD00ADADAD00ADADAD00848484008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF000000
      0000FFFFFF0000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000ADADAD00ADADAD0084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000ADADAD00ADADAD008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD00ADAD
      AD00848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000004ABDFF004ABDFF004ABDFF0021AD
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ADADAD008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF0000000000CE9C9C00CE9C9C004ABDFF004ABDFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD0084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00D69CA500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000CE9C9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00ADADAD00848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004ABDFF004ABDFF004ABDFF0021ADFF000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000004ABDFF004ABDFF004ABDFF0021AD
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000CE9C9C00CE9C
      9C004ABDFF004ABDFF00000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C000000000000000000CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C004ABDFF004ABDFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000CE9C9C00CE9C
      9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000CE9C9C00CE9C9C000094DE000094DE000094
      DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00000000000000000000000000CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000CE9C9C00CE9C9C000094DE000094DE004ABD
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000CE9C9C00CE9C
      9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000000000CE9C9C00CE9C9C000094DE0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000520000315200000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C000000000000000000000000000000000000000000CE9C9C00CE9C9C0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00CE9C
      9C00CE9C9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF00000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000CE9C9C00CE9C9C0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C000000
      00000000000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C
      9C000000000000CEFF0000CEFF00000000000000000000000000CE9C9C00CE9C
      9C000000000000000000000000000000000000CEFF0000CEFF00000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE000094DE000094
      DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000FFFF0000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      00000000000000000000000000000000000000000000CE9C9C00CE9C9C000000
      00000000000000CEFF0000CEFF00000000000000000000000000CE9C9C000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE000094DE004ABD
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000520000315200000000000000000000000000000052000000520000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF00F7FF
      FF0000000000CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF000000000000000000CE9C9C000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C0000000000000000000000
      00000000000000000000000000000000000000000000CE9C9C00000000000000
      00000000000000CEFF0000CEFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CE9C9C00CE9C9C00CE9C9C000094DE0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000520000315200000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000FFFF0000FFFF0000FFFF00CE9C
      9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000052000000520000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00CE9C9C00FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000000000FFFF00000052000000520000FF
      FF0000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000CEFF000000840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      000000FFFF00000052000000520000FFFF0000FFFF0000FFFF00000052000000
      520000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000FFFF0000FFFF000000520000FF
      FF0000FFFF00000052000000520000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000FFFF00F7FFFF000000000000CEFF000000520000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF000000520000FFFF0000FFFF00000052000000520000FF
      FF0000FFFF0000FFFF00000000000000000000FFFF0000FFFF0000FFFF00F7FF
      FF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000005252520000FFFF0000FFFF0000FF
      FF0000005200000052000000520000FFFF0000FFFF0000FFFF0000FFFF000000
      000000FFFF0000FFFF00000000000000000000CEFF0000CEFF00000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005252520000FFFF0000FFFF0000FFFF0000005200000052000000520000FF
      FF0000FFFF0000FFFF0000FFFF000000000000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF0000000000000000000000000000005200000052000000
      52000000520000CEFF000000840000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C310000000000CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF000000000000000000CE9C9C00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000520000005200000052000000520000CEFF000000840000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000520000CEFF0000CE
      FF0000CEFF0000CEFF000000520000FFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C310000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E7000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00CE9C9C00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000520000CEFF0000CEFF0000CEFF0000CEFF000000520000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000FF9C3100FF9C3100FF9C310000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7
      E700E7E7E700EFEFEF00E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000CEFF0000CEFF000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000EFEFEF00E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF00000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000FF9C3100FF9C3100FF9C3100FF9C31000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C31000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700EFEFEF00E7E7E700000000000000
      0000000000000000000000000000FFFFFF00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000FF9C3100FF9C3100FF9C3100000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      000000000000000000000000000000000000FFFFFF00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FFFFFF00CE9C
      9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C31000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000E7E7E700EFEF
      EF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400ADADAD00ADADAD00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9C9C00CE9C9C00FFFFFF00FFFFFF000000000000000000E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400ADADAD00ADADAD0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008484840084848400ADADAD00ADADAD00ADAD
      AD0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00ADADAD0000000000FFFF
      FF00FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00CE9C9C00CE9C9C00FFFF
      FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700EFEFEF0000000000000000000000
      0000E7E7E700E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400848484008484840084848400848484008484840000000000FFFF
      FF00CE9C9C00CE9C9C00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E70000000000000000000000
      0000E7E7E700E7E7E70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00CE9C9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF000000000000000000FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000008484840084848400848484008484
      8400009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004ABDFF004ABDFF004ABDFF0021ADFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400000000008484
      840000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C000000
      0000000000000000000000000000CE9C9C00CE9C9C0000000000000000000000
      000000000000CE9C9C00CE9C9C0000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000008484
      8400009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000008484
      840000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C0000009C0000009C000000000000000000000000
      00000000000000000000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF0000CE
      FF0000CEFF00FFFFFF00FFFFFF0000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00CE9C9C000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C000000000000000000CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000000000000000CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00CE9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000080808000000
      0000000000000000000000CEFF0000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000005200003152000000000000000000000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000009C0000009C000000000000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000FFFFFF0000CE
      FF0000CEFF0000CEFF00FFFFFF0000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C00CE9C9C00000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00000000000000000000000000CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C00CE9C9C00CE9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000808
      0800000000000000000000CEFF0000CEFF0000CEFF0000000000000000000000
      00000000000000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C0000000000000000000000000000000000000000
      000000000000000000000000000000000000009C000000000000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000FFFFFF0000CE
      FF0000CEFF0000CEFF00FFFFFF0000000000009C00000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      000000000000000000000000000000000000000000000000000000000000CE9C
      9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000CE9C9C00CE9C
      9C00CE9C9C00CE9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF000000000000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0008080800000000000000
      000000CEFF0000CEFF0000CEFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      52000000520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C00000000000000000000000000000000000000CE
      FF0000CEFF00000000000000000000000000009C000000000000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000FFFF
      FF00FFFFFF00FFFFFF000000000000000000009C00000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C00CE9C
      9C00000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000CE9C9C00CE9C
      9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF000000000000CE
      FF0000CEFF0000CEFF0000FFFF0000FFFF0000FFFF000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF000000520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF00F7FFFF00000000008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C000000000000009C0000009C00000000000000CE
      FF0000CEFF0000CEFF000000000000000000009C000000000000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000009C00000000000000000000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00CE9C9C000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000CE9C9C00CE9C
      9C0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000FFFF000000
      000000CEFF0000CEFF0000FFFF0000FFFF0000FFFF000000000000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF00000000000000000008080800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005252520000FF
      FF0000FFFF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF000000000000FFFF0000FFFF000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C000000000000009C0000009C00000000000000CE
      FF0000CEFF0000CEFF000000000000000000009C00000000000000000000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C000000000000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C00CE9C9C00000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000CE9C9C000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000808080000CEFF0000CEFF000000000000FFFF0000FFFF0000FF
      FF000000000000CEFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000FFFF0000FFFF0000FFFF000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      520000005200000052000000520000CEFF000000840000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400009C000000000000009C00000000000000000000009C0000009C00000000
      000000000000000000000000000000000000009C00000000000000000000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE9C9C0000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF0000CEFF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      520000CEFF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400848484008484
      840000000000009C000000000000009C00000000000000000000000000000000
      0000000000000000000000000000009C0000009C00000000000000000000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF0000CEFF0000CEFF0063CECE0000FF
      FF0000FFFF0063CECE000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0063CECE000000000000CEFF0000CEFF0000CEFF00080808000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000084848400848484008484840084848400848484008484
      840000000000009C000000000000009C0000009C000000000000000000000000
      00000000000000000000009C0000009C0000009C000000000000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000063630000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000636300EFEFEF00D6E7E700CECECE009C9C9C00006363000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      310000000000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000084848400848484008484840084848400848484008484
      8400009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C0000000000000000000000000000000000000000
      00000000000000FFFF000000000000FFFF000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF00000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000063630000636300006363000063630000FFFF0000FFFF000063
      6300D6E7E70000636300F7FFFF00D6E7E700CECECE009C9C9C0000636300D6E7
      E7000063630000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C31000000000084848400848484008484840084848400848484008484
      8400009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF000063630000636300F7FFFF00D6E7E700CECECE009C9C9C00006363000063
      630000FFFF0000FFFF0000000000080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C
      3100FF9C31000000000084848400848484008484840084848400848484000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000063630063CECE0000FF
      FF0000FFFF0000636300F7FFFF00D6E7E700CECECE009C9C9C000063630000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000000000FFFF0000FFFF0000FFFF00000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000063630000FFFF0000FFFF0000FF
      FF0000FFFF0000636300F7FFFF00D6E7E700CECECE009C9C9C000063630000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00000000000000FF00000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000000000FFFF0000FFFF0000FFFF00000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000063630000FFFF0000FFFF000000
      00000000000000636300D6E7E700D6E7E700CECECE009C9C9C00006363000000
      00000808080000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000636300006363000000
      000000000000006363009C9C9C009C9C9C009C9C9C009C9C9C00006363000000
      0000000000000063630000636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000636300F7FFFF00D6E7E700D6E7E700CECECE00006363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C3100000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000000000FF
      FF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000636300006363000063630000636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000000000FF
      FF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF00000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF00000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF0000CEFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF00000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF000000FF00000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF000000FF000000FF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF00000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF000000000000FFFF0000000000000000000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF00000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF0000FFFF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000FF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF0000000000000000000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000808080000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000018181800000000001818180039393900525252005A5A5A005A5A5A006363
      63006B6B6B0073737300737373007373730000000000737373006B6B6B006363
      63005A5A5A005A5A5A0052525200393939001818180000000000181818000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000018181800424242005A5A5A005A5A5A006B6B6B00000000009C9C9C00ADAD
      AD00BDBDBD00C6C6C600C6C6C600C6C6C60000000000C6C6C600BDBDBD00ADAD
      AD009C9C9C00848484006B6B6B00636363005A5A5A0042424200181818000000
      0000292929000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF000000000000000000FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000101010004242
      42005A5A5A00636363007B7B7B009C9C9C00BDBDBD00CECECE00D6D6D600D6D6
      D600C6C6C6000000000000000000000000000000000000000000C6C6C600D6D6
      D600D6D6D600CECECE00BDBDBD009C9C9C007B7B7B00636363005A5A5A004242
      4200101010000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000080808000000
      0000000000000000000000000000000000000000000000000000000000006342
      1800000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000808080029292900525252006363
      63007B7B7B00ADADAD00C6C6C600D6D6D600C6C6C600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000C6C6C600D6D6D600C6C6C600ADADAD007B7B7B006363
      6300525252002121210008080800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000313131005A5A5A006B6B6B009C9C
      9C00C6C6C600D6D6D6000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6D6D600C6C6C6009C9C
      9C006B6B6B005A5A5A0031313100000000003131310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD000000000000000000FF9C
      310018181800000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00000000000000000000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000101010000000000063391000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000424242005A5A5A007B7B7B00ADADAD00CECE
      CE00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600CECE
      CE00ADADAD007B7B7B005A5A5A00424242000000000031313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD00FFFFFF00E7E7EF00FF9C31000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000000000FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848484000000000000000000000000000000
      000000000000000000004242290000000000089C9C00106B6B00000000005A5A
      5A000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF000000
      00000000000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000424242005A5A5A0000000000BDBDBD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6D6D600BDBDBD00000000005A5A5A004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7B7B0000000000F7F7F70084848C00848C9400000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00003131310018ADC600009CA5001831420008FFFF00105252004A4242007B7B
      7B000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000008080800313131005A5A5A0000000000BDBDBD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF000000
      000000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6D6D600BDBDBD00000000005A5A5A0031313100080808000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000847B840000000000000000004A6B9400085A9C0000FFFF0000FF
      FF00000000000000000010E7FF0000F7FF0000FFFF0000F7F700000000000000
      000000000000847B840000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00EFEFEF00000000000000
      000039424A0039DEDE0000BDBD00104A520021C6D600005A5A00525252000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000808
      0800212121005A5A5A007B7B7B00BDBDBD00FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      FF000000FF0000000000FFFFFF00000000000000000000000000080808000000
      0000000000000000000000000000000000000000000000000000000000006342
      1800000000000000000000000000000000007B7B7B005A5A5A00212121000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B000000000000FFFF0000FFFF0000FFFF000000
      000000000000633952009CADBD0000F7FF0000FFFF00000000006B6B6B007B7B
      7B00000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000086B7B00009CCE0000D6DE000073730029B5CE0000C6CE00214A4A000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF00000000000000
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000292929001010
      1000525252006B6B6B00ADADAD0000000000FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      310000000000000000000000000000000000ADADAD006B6B6B00525252001010
      1000292929000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00000000008CBDCE0000FFFF000000000000FF
      FF0000000000000839006B4A52004AD6FF00008CB50000000000000000002939
      3900847B84000000000000000000000000000000000000000000000000000000
      000000000000000000004ABDFF004ABDFF004ABDFF0021ADFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00000000000000FF000000FF000000FF00000000000000
      000000426B0000A5EF0000F7FF00006B7B00006B9C0008CED600186B6B007B7B
      7B000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004242
      42005A5A5A009C9C9C00CECECE0000000000FFFFFF00FFFFFF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF00000000000000FF000000
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD000000000000000000FF9C
      310018181800000000000000000000000000CECECE009C9C9C00636363004242
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000847B840000000000428C940094B5C60000FFFF0000FFFF0000FF
      FF0000FFFF000000000052394A0029F7FF0000E7EF0000F7FF0000C6C6000000
      00007B7B7B000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C004ABDFF004ABDFF00000052000000520000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      00003942420000637B0008637B0000526B00009CD600108CBD0021424A007B7B
      7B000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF00000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000018181800181818005A5A
      5A007B7B7B00C6C6C6000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000000000000000FF000000FF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000BDBDBD00FFFFFF00E7E7EF00FF9C31000000
      00000000000000000000847B84000000000000000000C6C6C6007B7B7B005A5A
      5A00181818001818180000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000007B7B7B000000000000CEFF0000ADC6000063940000F7F7000000
      000000FFFF0000000000633942004ACEE70000CEFF0000F7F700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C004ABDFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B00000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000005A635A009C9C9C009CB5B5005ACEF700008CC600424242007B84
      7B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000424242006363
      6300A5A5A500D6D6D6000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000007B7B7B0000000000F7F7F70084848C00848C9400000000000000
      000000000000FFFFFF00FFFFFF0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000847B84000000000000CEFF0000529400008CBD0000FFFF0000FF
      FF0000FFFF00000000005A3152006BB5CE0008B5EF0000FFFF0000FFFF0000FF
      FF0000000000847B840000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      00000000000000000000636363004A4A420042424200314A5A007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C00000000000000000000000000000000
      00000000000000000000000000000000000000000000181818005A5A5A007B7B
      7B00C6C6C600000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000847B840000000000000000004A6B9400085A9C0000FFFF0000FF
      FF0000000000FFFFFF000000000000FFFF0000FFFF0000FFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000CEFF000073CE0000528C0000CEFF0000CE
      FF0000CEFF000000000000528C00009CB500005A8C0000F7F7000000000018FF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000CEFF0000CEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C000000000000000000000000
      0000000000000000000000000000000000000000000031313100636363009C9C
      9C00D6D6D600000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000007B7B7B000000000000FFFF0000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000004AB5000084DE0000CEFF0000CEFF0000CE
      FF0000CEFF000000000000528C00002173000094BD0000FFFF0000E7DE0039B5
      B500000000000000000000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE000094DE000094DE0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000000000000000
      00000000000000000000000000000000000008080800525252006B6B6B00BDBD
      BD0000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000007B7B7B00000000008CBDCE0000FFFF000000000000FF
      FF00000000000000000010E7FF0000F7FF0000FFFF0000F7F700000000000000
      0000000000004A4A4A0008080800000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000CEF70000ADE7000073A500007BA5002994BD006373
      730000000000847B840000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE000094DE004ABDFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000426B6B005A9C9C0063A59C0063A5
      A500319494000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      000000000000000000000000000000000000181818005A5A5A0084848400CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000847B840000000000428C940094B5C60000FFFF0000FFFF0000FF
      FF0000FFFF000000000094A5AD0000F7FF0000FFFF00000000006B6B6B00C6C6
      C600848484005A5A5A0018181800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000847B84007B737B000000000000000000000000000000
      000000000000009CC60000DEFF0000CEFF00007BB500005AAD00085AAD00184A
      5200004A63007B7B7B0000000000000000000000000000000000000000000000
      0000CE9C9C00CE9C9C00CE9C9C000094DE0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000005200003152000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000BDE7000000000000000000000000000000
      00007B8C8C005AA59C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      000000000000000000000000000000000000000000005A5A5A009C9C9C00D6D6
      D60000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7B7B000000000000CEFF0000ADC6000063940000F7F7000000
      000000FFFF0000000000634A52004AD6FF00008CB50000000000000000004252
      52009C9C9C005A5A5A0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00000000004AA5B5004ADEFF0000B5F70000C6FF0000C6F7000000
      000000000000847B84000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF00CE9C9C00CE9C9C0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      000000000000000000000000000000000000000000007B7B7B00000000000000
      0000000000007B847B000000000000A5C600217384007B7B7B00000000000000
      00000000000000000000000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000063636300ADADAD00D6D6
      D60000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000847B84000000000000CEFF0000529400008CBD0000FFFF0000FF
      FF0000FFFF00000000005A424A0029F7FF0000E7EF0000F7FF0000C6C6000000
      0000A5A5A5006363630039393900100810000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0052BDF700009CFF00105A7B00847B
      840000000000000000000000000000000000000000000000000000FFFF000000
      52000000520000FFFF0000FFFF0000FFFF00000052000000520000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000739400297B940000000000008C00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000000000000000000000000000000000006B6B6B00BDBDBD000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000CEFF000073CE0000528C0000CEFF0000CE
      FF0000CEFF0000000000634242004ACEE70000CEFF0000F7F700000000000000
      0000BDBDBD006B6B6B004A4A4A00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000847B
      8400000000000000000000000000000000000000000000528400215273000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF000000520000FFFF0000FFFF00000052000000520000FFFF0000FFFF0000FF
      FF00000000000000000000FFFF0000FFFF0000FFFF00F7FFFF00000000000000
      000000000000000000000000000000000000000000007B7B7B00000000000000
      0000000000000000000084848400000000000894B50000000000006300000000
      00000000000000000000000000000000000000000000000000007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF0000CEFF0000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000000000000000000073737300C6C6C6000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000004AB5000084DE0000CEFF0000CEFF0000CE
      FF0000CEFF000000000052314A006BB5CE0008B5EF0000FFFF0000FFFF0000FF
      FF00000000006B6B6B0052525200080808000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7B7B007B7B7B007B7B7B007B7B7B0052525200314A52000000
      00000000000000000000000000000000000000000000000000005252520000FF
      FF0000FFFF0000FFFF0000005200000052000000520000FFFF0000FFFF0000FF
      FF0000FFFF000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006B000000000000000000000000000000000000000000000000
      0000006B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000073737300C6C6C6000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000CEFF0000CEFF0000CEFF0000CE
      FF00000000000000000000528C00009CB500005A8C0000F7F7000000000018FF
      FF00000000006B6B6B0052525200000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000FF000000FF000000FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      520000005200000052000000520000CEFF000000840000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B847B0000000000008C000000940000084A080000000000005200000073
      0000000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000073737300C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000847B84007B737B000000000000000000000000000000
      00000000000000DEFF00005A9C00002173000094BD0000FFFF0000E7DE0039B5
      B500000000007373730052525200080808000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      520000CEFF0000CEFF0000CEFF0000CEFF000000520000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B847B0000000000000000000000000000000000000000000000
      00007B847B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C000000000000000000000000000073737300C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF000000000000DEFF0000CEFF0000ADE7000073A500007BA5002994BD006373
      7300000000007373730052525200080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF0000CEFF0000CEFF0000CEFF000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000000000000000000073737300C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000009CC60000DEFF0000CEFF00007BB500005AAD00085AAD00184A
      5200004A63006B6B6B0052525200080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF00000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      310000000000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C00000000
      00000000000000000000009C000000000000000000006B6B6B00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00000000004AA5B5004ADEFF0000B5F70000C6FF0000C6F7000000
      0000000000006B6B6B004A4A4A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C0000000000000000000063636300ADADAD00D6D6
      D600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0052BDF700009CFF00105A7B00CECE
      CE00ADADAD006363630039393900080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000FF9C3100FF9C
      3100FF9C31000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C0000009C0000009C0000009C0000009C0000009C0000009C0000009C
      0000009C000000000000009C000000000000000000005A5A5A009C9C9C00D6D6
      D600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000847B
      840000000000000000000000000000000000000000000052840021527300D6D6
      D6009C9C9C005A5A5A0029292900080808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000000000000000000000FF0000000000000000005A5A5A0000000000CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7B7B007B7B7B007B7B7B007B7B7B0052525200314A5200CECE
      CE00848484005A5A5A0018181800100810000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF00000000000000FF000000000000000000525252006B6B6B00BDBD
      BD00C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600BDBD
      BD006B6B6B005252520008080800313131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100FF9C31000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      000000000000000000000000FF000000000000000000313131005A5A5A009C9C
      9C00D6D6D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D6009C9C
      9C005A5A5A003131310000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF9C3100FF9C3100FF9C3100FF9C3100FF9C
      3100FF9C3100FF9C3100FF9C3100000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700EFEFEF00E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF000000000000FFFF0000FFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000FFFF0000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000000000000000181818005A5A5A007B7B
      7B00C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6007B7B
      7B005A5A5A001818180029292900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C3100FF9C
      3100FF9C3100FF9C310000000000000000000000000000000000000000000000
      000000000000EFEFEF00E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000FFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000000000FF
      FF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000424242006363
      6300ADADAD00D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000D6D6D600A5A5A5006363
      6300424242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E70000000000E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF0000CE
      FF0000CEFF00FFFFFF00FFFFFF000000000000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000000000FF
      FF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000018181800181818005A5A
      5A007B7B7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B005A5A
      5A00181818001818180000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E70000000000E7E7E700E7E7E700EFEFEF00E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF000000000000FFFF0000000000009C0000009C0000FFFFFF0000CE
      FF0000CEFF0000CEFF00FFFFFF000000000000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF00000000000000000000FF
      FF0000FFFF0000FFFF0000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000009C9C9C00CECECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6D6D6009C9C9C00000000004242
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000CEFF0000CEFF0000CE
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7000000000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000FFFF0000000000009C0000009C0000FFFFFF0000CE
      FF0000CEFF0000CEFF00FFFFFF000000000000FFFF00000000000000000000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000212121001010
      1000525252006B6B6B00ADADAD00D6D6D6000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF000000000000000000000000000000000000000000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6D6D600ADADAD006B6B6B00525252001010
      1000212121000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF000000000000000000000000000000000000000000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000FFFF000000000000000000009C0000009C0000FFFF
      FF00FFFFFF00FFFFFF00000000000000000000FFFF00000000000000000000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000292929005A5A5A007B7B7B00BDBDBD00D6D6D60000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000000000000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D6D6D600BDBDBD007B7B7B005A5A5A00292929000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000000000000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000FFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00000000000000000000FFFF0000FFFF00000000000000000000FF
      FF000000FF000000FF000000FF000000FF000000FF000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000008080800313131005A5A5A0000000000BDBDBD00D6D6D600000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000000000000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6D6D600BDBDBD00000000005A5A5A0031313100080808000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000000000000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF000000000000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000424242005A5A5A0000000000BDBDBD00D6D6D6000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF00000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6D6D600BDBDBD00000000005A5A5A004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000CEFF0000CE
      FF0000CEFF00000000000000000000000000009C0000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700EFEFEF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000424242005A5A5A007B7B7B00ADADAD000000
      0000009C000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF00000000000000000000000000009C000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CECE
      CE00ADADAD007B7B7B005A5A5A00424242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C000000000000000000000000000000000000000000000000000000CE
      FF0000CEFF00000000000000000000000000009C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000313131005A5A5A006B6B6B000000
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000009C0000000000000000000000000000D6D6D600C6C6C6009C9C
      9C006B6B6B005A5A5A0031313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000009C0000009C000000000000000000000000000000000000000000000000
      000000CEFF0000CEFF0000000000000000000000000000000000000000000000
      000000000000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF00000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000808080029292900525252006363
      630000000000009C0000009C0000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      0000009C0000009C000000000000D6D6D600C6C6C600A5A5A5007B7B7B006363
      6300525252002929290008080800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009C0000009C0000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      0000009C0000009C000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF00000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000008080800101010004242
      42005A5A5A0000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000009C0000000000000000000000000000009C
      0000009C000000000000BDBDBD009C9C9C007B7B7B00636363005A5A5A004242
      4200101010000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000009C0000009C0000009C0000009C0000009C0000009C
      0000009C00000000000000000000009C0000000000000000000000000000009C
      0000009C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E70000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E700E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000FF00000000000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000212121000000
      0000181818004242420000000000009C0000009C0000009C0000009C0000009C
      000000000000C6C6C60000000000009C0000009C0000009C0000009C0000009C
      000000000000848484006B6B6B005A5A5A005A5A5A0042424200181818000000
      0000212121000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000009C0000009C0000009C0000009C0000009C
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7E700E7E7E700E7E7E700EFEFEF0000000000E7E7
      E70000000000E7E7E700E7E7E700E7E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF000000FF000000FF00000000000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000181818000000000018181800000000000000000000000000000000000000
      00006B6B6B0073737300737373000000000000000000009C0000000000000000
      00005A5A5A005A5A5A0052525200313131001818180000000000181818000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700E7E7E700E7E7E700000000000000
      000000000000E7E7E700E7E7E700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000000000FF
      FF0000000000000000000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029292900000000000000000018181800292929003939
      39004A4A4A0052525200525252005252520052525200000000004A4A4A003939
      3900292929001818180008080800000000002929290000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000031313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      28000000C0000000E00100000100010000000000002D00000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFF7FFFFFFFFFFFFFFFFF00000000
      0000000000000000FFFFFF7FFFFFFFFFFFFFFFFF000000000000000000000000
      FFFFFF7FFFFFFFFFFFFFFFFF000000000000000000000000FFFFFF7FFFFFFFFF
      FFFFFFFF000000000000000000000000F000077FFFFFFFFFFFFFFFFF00000000
      0000000000000000F00007000001FFFFFFFFFFFF000000000000000000000000
      F00007000001FFFFFFFFFFFF000000000000000000000000F00007407FFFFFFF
      FFFFFFFF000000000000000000000000F0000760FFF7FFFFFFFFFFFF00000000
      0000000000000000F0000761FFF7FFFFFFFFFFFF000000000000000000000000
      F0000761FFE3FFFFFFFFFFFF000000000000000000000000F0000761FFE3FFFF
      FFFFFFFF000000000000000000000000F0000761FFE3FFFFFFFFFFFF00000000
      0000000000000000F00000000FC3FFFFFFFFFFFF000000000000000000000000
      F00000000003FFFFFFFFFFFF000000000000000000000000F00000000003FFFF
      FFFFFFFF000000000000000000000000F00000000007FFFFFFFFFFFF00000000
      0000000000000000F00000000007FFFF7FFFFFFF000000000000000000000000
      F00000000FFFFFFF0003FFFF000000000000000000000000F00000001FFFFFFF
      0003FFFF000000000000000000000000F00000003FFFFFFF0003FFFF00000000
      0000000000000000F00000003FFFFFFF0003FFFF000000000000000000000000
      F00000003FFFFFFF0003FFFF000000000000000000000000F00000003FFFFFFF
      0003FFFF000000000000000000000000FFFC00003FFFFFFF0003FFFF00000000
      0000000000000000FFFC00003FFFFFFF0003FFFF000000000000000000000000
      FFFC00003FFFFFFF0003FFFF000000000000000000000000FFFC00003FFFFFFF
      0003FFFF000000000000000000000000FFFC00003FFFFFFF0003FFFF00000000
      0000000000000000FFFC00003FFFFFFF0003FFFF000000000000000000000000
      FFF800001FFFFFFF0003FFFF000000000000000000000000FFE4000027FFFFFF
      0001FFFF000000000000000000000000FF9C000039FFFFFFFFFFFFFF00000000
      0000000000000000FE7C00003EFFFFFFFFFFFFFF000000000000000000000000
      F9FC00003F3FFFFFFFFFFFFF000000000000000000000000E7FFFFFFFFCFFFFF
      FFFFFFFF0000000000000000000000009F0000001ED7FFFFFFFFFFFF00000000
      00000000000000007F0000000CD9FFFFFFFFFFFF000000000000000000000000
      FF000000000EFFFFFFFFFFFF000000000000000000000000FF0000001B1FFFFF
      FFFFFFFF000000000000000000000000FF0000001F1FFFFFFFFFFFFF00000000
      0000000000000000FF0000001F9FFFFFFFFFFFFF000000000000000000000000
      FF0000001FFFFFFFFFFFFFFF000000000000000000000000FE0000001FFFFFFF
      FFFFFFFF000000000000000000000000FE0000001FFFFFFFFFFFFFFF00000000
      0000000000000000FF0000001FFFFFFFFFFFFFFF000000000000000000000000
      FFC0000FFFFFFFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFF
      FFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F
      FFFFFF00000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFF8000000FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFFC000000FFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF7FFFFFFFE000001FFFFFFFF00FFFFFFFFFF00FFFFFF000077F
      FFFFF00000003FFFFFFF0000FFFFFFFF0000FFFFF00007000001F00000000001
      FFFC00003FFFFFFC00003FFFF00007000001F00000000001FFF800001FFFFFF8
      00001FFFF00007407FFFF00000007FFFFFE0000007FFFFE0000007FFF0000760
      FFF7F0000000FFF7FFC0000003FFFFC0000003FFF0000761FFF7F0000001FFF7
      FF80000001FFFF80000001FFF0000761FFE3F0000000FFE3FF00000000FFFF00
      000000FFF0000761FFE3F0000000FFE3FF00000000FFFF00000000FFF0000761
      FFE3F0000000FFE3FE000000007FFE000000007FF0000761FFC3F0000000FFC3
      FC000000003FFC000000003FF00007600003F00000000003FC000000003FFC00
      0000003FF00007600003F00000000003F8000000001FF8000000001FF0000760
      0007F00000000007F8000000001FF8000000001FF00007700007F00000000007
      F8000000001FF8000000001FF000077FAFFFF00000002FFFF8000000001FF800
      0000001FF000077FFFFFF00000003FFFF0000000000FF0000000000FF000077F
      FFFFF00000007FFFF0000000000FF0000000000FF000077FFFFFF00000007FFF
      F0000000000FF0000000000FF000077FFFFFF00000000FFFF0000000000FF000
      0000000FF000077FFFFFF000000007FFF0000000000FF0000000000FFFFFFF3F
      FFFFFFFF800007FFF0000000000FF0000000000FFFFFFC9FFFFFFFFF800007FF
      F0000000000FF0000000000FFFFFF3E7FFFFFFFF800007FFF0000000000FF000
      0000000FFFFFCFFBFFFFFFFF800007FFF8000000001FF8000000001FFFFF3FFC
      FFFFFFFF00003FFFF8000000001FF8000000001FFFFCFFFF3FFFFFFCC0003FFF
      F8000000001FF8000000001FFFFBFFFFDFFFFFFBE000DFFFF8000000001FF800
      0000001FFFE7FFFFE7FFFFE7F801E7FFFC000000003FFC000000003FFF9FFFFF
      F9FFFF9FFF03F9FFFC000000003FFC000000003FFE7FFFFFFEFFFE7FFFFFFEFF
      FE000000007FFE000000007FF9FFFFFFFF3FF9FFFFFFFF3FFF00000000FFFF00
      000000FFE7FFFFFFFFCFE7FFFFFFFFCFFF00000000FFFF00000000FF9F000000
      1ED79F0000001ED7FF80000001FFFF80000001FF7F0000000CD97F0000000CD9
      FFC0000003FFFFC0000003FFFF000000000EFF000000000EFFE0000007FFFFE0
      000007FFFF0000001B1FFF0000001B1FFFF800001FFFFFF800001FFFFF000000
      1F1FFF0000001F1FFFFC00003FFFFFFC00003FFFFF0000001F9FFF0000001F9F
      FFFF0000FFFFFFFF0000FFFFFF0000001FFFFF0000001FFFFFFFF00FFFFFFFFF
      F00FFFFFFE0000001FFFFE0000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFE000000
      1FFFFE0000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000001FFFFF0000001FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFC0000FFFFFFFC0000FFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFF80FF80000000000300E01807FFFF800007FFFFFF
      FFFFFFFE07FF80000000000300E01807FFFF80000FFFFFFFFFFFFFFC1FFF8000
      0000000300E01807FFFF80000FFFFFFFFFFFF0083FFF80000000000700E01807
      FFFF80001FFFFFFFFFFF00007FFF80000000000700E01807FFFF80003FFFFFFF
      FFFC00003FFF80000000000700E01807FFFF8000FFFFFFFFFFF801001FFFC000
      0000000FE7FCFF00FFFF8001FFFFFFFFFFE0000007FFE0000000001FE7FCF000
      0FFF8001FF00FFFFFFC0000003FFF0000000003FE7FCC00003FF8001F0000FFF
      FF80000001FFF8000080003FE000000001FF8001C00003FFFF00000000FFFF00
      0100007FE0000000007F8000800001FFFF00000000FFFF00000000FFFFFC0000
      003FC0000000007FFE000000007FFF00000000FFFFF80000001FC0000000003F
      FC000000003FFE000000007FFFF00000000FC0000000001FFC0000000037FC00
      0000043FF0000000000FC0000000000FF8000000000FFC400000063FF0000000
      0007C0000000000FF8000000001FF8400000061FF00000000003800000000007
      F8000000001FF8C00000031FF00000000003800000000003F8000000001FF8C0
      0000031FF00000000001800000000003F0000000000FF8C00000031FF0000000
      0001800000000001F0000000000FF1800000038FFF8000000001800100000001
      F0000000000FF1800000038FFF8000000001800000000001F0000000000FF180
      0000038FFF0000000000800000000001F0000000000FF1800000008FFF000000
      0000800000000000F1000000000FF1800100008FFF0000000000800000000000
      F0000000000FF1800000008FFF0000000000800000000000F0000000000FF180
      0000008FFF0000000000800000000000F8000000001FF1800000058FFF000000
      0000800000000000F8000000001FF8800000011FFF0000000000800000000000
      F8000000001FF8800000001FFF0000000000C00000000000F8000000001FF880
      0000001FFF8000000001E00000000000FC000000003FF8000000001FFF800000
      0001F80000000001F8000000003FFC000000001FFF8000000001FF0000000001
      F8000000007FFC000000001FFF8000000001FF80000000018800000000FFFE00
      0000001FFFC000000003FF8000000001F800000000FFFF18000000FFFFC00000
      0003FFC000000003F800000001FFFF0F030000FFFFE000000007FFC000000003
      FC00000003FFFF83FF8001FFFFF00000000FFFE000000007FC00000007FFFFC1
      FFE003FFFFF00000000FFFF00000000FFE0000001FFFFFE07FFC07FFFFF80000
      001FFFF00000000FFF0000003FFFFFF80FF01FFFFFFC0000003FFFF80000001F
      FBC10000FFFFFFFC00003FFFFFFE0000007FFFFC0000003FF7FFF00FFFFFFFFF
      0000FFFFFFFF800001FFFFFE0000007FFFF7FFFFFFFFFFFFF00FFFFFFFFFC000
      03FFFFFF800001FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFC00003FF
      FFF7FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFF0000FFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFF800003FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFC00003FF800003FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF800001FF800003FFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFE0000
      007F800007FFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFC0000003F80000FFFFFFF
      FFFC00003FFFFFFFFFFFFFFFFFF80000001F80001FFFFFFFFFF800001FFFFFFF
      FFFFFFFFFFF00000000F80001FFFFFFFFFE0000007FFFC00000001FFFFF00000
      000F80003FFFFFFFFFC0000003FFFC00000001FFFFE00000000780007FFFFFFF
      FF80000001FFFC00000001FFFFC0000000038001FFFFFFFFFF00000000FFFC00
      000001FFFFC0000000038003FFFFFFFFFF00000000FFFC00000001FFFF800000
      00018003FFFFFFFFFE000000007FFC00000001FFFF80000000018003FFFFFF8F
      FC000000003FFC00000001FFFF80000000018003FFFFF00FFC000000003FFC00
      000001FFFF80000000010001FFFE000FF8000000001FFC00000001FFFF000000
      00008001FFE00007F8000000001FFC00000001FFFF00000000008001FC000007
      F8000000001FFC00000001FFFF00000000008001F8000007F8000000001FFC00
      000001FFFF00000000008001FC000007F0000000000FFC00000003FFFF000000
      00008001FC000003F0000000000FFC10080407FFFF000000000000007C000003
      F0000000000FFC30180C0FFFFF000000000000007C000003F0000000000FFC70
      381C1FFFFF000000000000007C000003F0000000000FFCF0783C3FFFFF800000
      000100007E000003F0000000000FFCF0F87C7FFFFF80000000010002FE000001
      F0000000000FFCF1F8FCFFFFFF80000000010000FE000001F0000000000FFCF3
      F9FDFFFFFF800000000100001E000001F8000000001FFCF7FBFFFFFFFFC00000
      000300000E000001F8000000001FFCFFFFFFFFFFFFC00000000300000F000001
      F8000000001FFCFFFFFFFFFFFE000000000700000F000001F8000000001FFCFF
      FFFFFFFFF8000000000F00000F000000FC000000003FFCFFFFFFFFFFF0700000
      000F00000F000000FC000000003FFCFFFFFFFFFFE1F80000001F00007F000000
      FE000000007FFCFFFFFFFFFFE3FC0000003F8000FF800000FF00000000FFFCFF
      FFFFFFFFC7FE0000007FC001FF800000FF00000000FFFFE07FFFFFFFC7FE0000
      01FFF003FF800000FF80000001FFFFC0003FFFFFC7FE000003FFFE07FF800000
      FFC0000003FFFFE0001E001FC77E30000FFFFFFFFF800000FFE0000007FFFFFC
      001E0007E33C3E00FFFFFFFFFFC00000FFF800001FFFFFFF003E0007E1187EFF
      FFFFFFFFFFC00000FFFC00003FFFFFFFFFFF8007F0007FFFFFFFFFFFFFC00000
      FFFF0000FFFFFFFFFFFFFFFFF800FFFFFFFFFFFFFFC00000FFFFF00FFFFFFFFF
      FFFFFFFFFC03FFFFFFFFFFFFFFC0000FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF
      FFFFFFFFFFE001FFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFE03FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00007FFFFFFFFFFFFF
      FFFFC0000001FFFFFFFFFFFFFFFFFE00007FFFFFFFFFFFFFFE3F80000000FFFF
      FFFFFFFFFFFFFE00007FFFFFFFFFFFFFC03F00000000FFFFFFFF3FFFFFFFFE00
      00000001FFFFFFF8003F00000000FFFFFFFFCFFFFFFFFE0000000001FFFFFF80
      001F00000001FFFFFFFFB7FFFFFFFE0000407FFFFFFFF000001F00000200FFFF
      FFFFBFFFFFFFFE000060FFF7FFFFE000001F00000000FFFFFFFF7F8000FFFE00
      0061FFF7FFFFF000001F00000000FFFFFFF37F8000FFFE000061DFE3FFFFF000
      000F25000000FFFFFFF0FF8000FFEE0000618F63FFFFF000000F000000000FFF
      FFE03F8000FFC60000610623FFFFF000000F0000000003FFFFE01F8000FF8200
      00600401FFFFF000000F0200000001FFFFC01F8000FF020000600001FFFFF800
      000F10000100007FFFC03F8000FF000000600001FFFFF800000710000000003F
      FF803F8000FF000000600001FFFFF800000710000000001FFF807F8000FF0000
      00700001FFFFF800000700000000000FFF007F8000FF0000007E0001FFFFF800
      000700000000000FFF00FF8000FF0000007E0001FFFFFC000007000000000007
      FE00FF8000FF0000007E0001FFFFFC000007000000000003FF81FF8000FF0000
      FF7E0001FFFFFC000003000000000003FFC1FFFFFFFF0000FF7E0001FFFFFC00
      0003800000000001FFBBFFFFFFFF0000FF7E0001FFFFFC000003C00000000001
      FFBFFFFFFFFF0000FF7E0001FFFFFE000003FF8000000001FF7FFE1FFFFF0000
      FF7E0001E00000000003FF8000000001FF7FF807FFFF0000FF3E0001C0000000
      0001FF0000000000FEFFE001FFFF0000FC9E0001800000000001FF0000000000
      FEFF00001FFF0000F3E60001800000000001FF0000000000FDFE00001FFF0000
      CFFBFFFF800000000001FF0000000000FFFE000007FFFFFF3FFCFFFF80000100
      0001FF0000000000FFFE000003FFFFFCFFEF3FFF800000002000FF1000000000
      FFFE000001FFFFFBFFC79FFF800000000001FF0000000000FFFE000001FFFFE7
      FF8303FF92800000003FFF0000000000FFFF000001FFFF9FFF0210FF80000000
      07FFFF8000000001FFFF800001FFFE7FFF0000FF800000007FFFFF8000000001
      FFFFC00001FFF9FFFF00003F810000007FFFFF8000000001FFFFE00001FFE7FF
      FF0000CF880000807FFFFF8000000001FFFFE00003FF9FFFFF0000F788000000
      7FFFFFC000000003FFFFC00007FF0000000000F9880000007FFFFFC000000003
      FFFFC0000FFF8000000000FE800000007FFFFFE000000007FFFFC0003FFF8000
      000000FF800000007FFFFFF00000000FFFFFE000FFFF8000000000FF80000000
      7FFFFFF00000000FFFFFE001FFFF8000000000FF800000007FFFFFF80000001F
      FFFFF81FFFFF8000000000FF800000007FFFFFFC0000003FFFFFFFFFFFFF8000
      000000FFC00000007FFFFFFE0000007FFFFFFFFFFFFF0000000000FFE0001000
      FFFFFFFF800001FFFFFFFFFFFFFF0000000000FFFFFFFFFFFFFFFFFFC00003FF
      FFFFFFFFFFFF8000000000FFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFE000
      070000FFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF800000000003FFFFFFFFC01F8000003FFFFFFFFFFFFFFFFF
      800000000003FFFFFFFF800F8000003FFFFFFFFFFFFFFFFF800000000003FFFF
      FFFF00078000003FFFFFFFFFFFFFFFFF800000000007FFFFFFFE00038000007F
      FFFFFFFFFFFFFFFF000000000007FFFFFFFC0001800000FFFFFFFFFFFFFFFFFF
      800000000007800000780001800001FFFFFFFFFFFFFFFFFFC0000000000F8000
      00780001800001FFFFFFFFFFFFFFFFFFE0000000001F80000078000180000300
      FFFFFFFFFFFFFFFFF0000000003F800000780001800000000FFFFFFFFFFFFFFF
      F8000080003F8000007800018000000003FFFFFFFFFFFFFFFF000100007F8000
      00780001E000000001FFFFFFFFFFFFFFFF00000000FF800000780001E0000000
      007FFFFFFFFFFFFFFF00000003FF800000780001E0000000003FFFC0000001FF
      FF00000007FF8000007C0003E0000000001FFF80000000FFFF80000007FF8000
      007E0007F0000000000FFF00000000FFFFC0000007FF800000000001F8000000
      000FFF00000000FFFFC0000007FF800000000001F80000000007FF00000001FF
      FFC0000003FF800000000001F80000000003FF00000200FFFFC0000003FF8000
      00000001F80000000003FF00000000FFFFC0000003FF800000000001F8000000
      0001FF00000000FFFF80000003FF800000000001F00000000001FF25000000FF
      FF80000003FF800000000001F00000000001FF00000000FFFF80000003FF8000
      00000001F00000000001FF00000000FFFF80000000FF800000000001F0000000
      0000FF02000000FFFF80010000FF800000000001F00020000000FF10000100FF
      FF80000000FF800000000001F00000000000FF10000000FFFF80000000FF8000
      00000001F00000000000FF10000000FFFF80000005FF800000000001F0000000
      0000FF00000000FFFF80000001FF800000000001F00000000000FF00000000FF
      FF800000003F800000000001F00000000000FF00000000FFFF800000001F8000
      00000001F00000000000FF00000000FFFF800000001F800000000001F0000000
      0001FF00000000FFFF800000001F800000000001F00000000001FF80000000FF
      FFC00000001F800000000001F80000000001FFC0002001FFFFE00000001F8000
      00000001FC0000000001FFFFFFFFFFFFFFF8000000FF800000000001FF000000
      0003FFFFFFFFFFFFFFFF030001FF800000000001FFC000000003FFFFFFFFFFFF
      FFFFFF8003FF800000000001FFE000000007FFFFFFFFFFFFFFFFFFE007FF8000
      00000001FFF00000000FFFFFFFFFFFFFFFFFFFFC0FFF800000000001FFF00000
      000FFFFFFFFFFFFFFFFFFFFFFFFF800000000001FFF80000001FFFFFFFFFFFFF
      FFFFFFFFFFFF800000000001FFFC0000003FFFFFFFFFFFFFFFFFFFFFFFFF8000
      00000001FFFE0000007FFFFFFFFFFFFFFFFFFFFFFFFF800000000001FFFF8000
      01FFFFFFFFFFFFFFFFFFFFFFFFFF800000000001FFFFC00003FFFFFFFFFFFFFF
      FFFFFFFFFFFF800000000001FFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFF8000003FFFFF
      80000000FFFFFFFFFFFFF8FFFFFFFFFFFFFF8000003FFFFF80000001FFFFFFFF
      FFFF00FF800000003FFF8000003FFFFF80000001FFFFFFFFFFE000FF80000000
      3FFF8000007FFFFF80000000FFFFFFFFFE00007F800000003FFF800000FFFFFF
      800000003FFFFFFFC000007F800000003FFF800001FFFFFF800000101FFFFFFF
      8000007F800000003FFF800001FFFFFF8000003E07FFFFFFC000007F80000000
      3FFF800003FFFFFF8000003F83FFFFFFC000003F800000003FFF800007FFFFFF
      8000003FC1FFFFFFC000003F800000003FFF80001FFFFF8F8000003FF0FFFFFF
      C000003F800000003FFFE0003FFFF00F8000001FF8FFFFFFC000003F80000000
      3FFFE0003FFE000F8000001FF80FFFFFE000003F800000000601E0003FE00007
      8000001FF00FFFFFE000001F800000000601E0003C0000070000001E000FFFFF
      E000001F800000000601F00018000007800000000007FFFFE000001F82010000
      0601F8001C000007800000000007FFFFE000001F860300000601F8001C000003
      800000000007FFFFF000001F8E0700000601F8001C000003800000000007FFFF
      F000001F9E0F01873FCFF8001C000003800000000003FFFFF000000F9E1F098F
      3FCFF8001C000003000000000003FFFFF000000F9E3F199F3FCFF00006000003
      000000200003FFFFF000000F9E7F3800000FF00006000001000000000003FFFF
      F800000F9EFF7800000FF00006000001000000000003FFFFF800000F9FFFFFFF
      3FFFF00006000001000200000001FFFFF80000079FFFFFFF3FFFF0002E000001
      000000000001FC01F80000079FFFFFFF3FFFF0000F000001000000000001F800
      F80000079F00E01807FFF00001000001000000000001F0007C0000079F00E018
      07FFF00000000000000000000001E0003C0000079F00E01807FFF00000000000
      000000000001C0001C0000039F00E01807FFF000000000000000000000008000
      1C000007FC00E01807FFF0000080000000000000000080001C0000FFF8000018
      07FFF0000080000000000000000080001E001FFFFC0000C003FFF00007800000
      8000E000000080001E03FFFFFF8000C000FFF8000F800000C001FF8000008000
      1E3FFFFFFFE004C000FFFC001F800000F003FF80000080001FFFFFFFFFE00000
      00FFFF003FC00000FE03FF80000080001FFFFFFFFFE000003FFFFFE07FC00000
      FFC1FF80000080001FFFFFFFFFFFFCFFFFFFFFFFFFC00000FFE07FC00000C000
      3FFFFFFFFFFFFCFFFFFFFFFFFFC00000FFF80FC00000E0007FFFFFFFFFFFFCFF
      FFFFFFFFFFC0000FFFFC00000000F000FFFFFFFFFFF000007FFFFFFFFFE001FF
      FFFF00000000F801FFFFFFFFFFF000007FFFFFFFFFE03FFFFFFFF000000FFC03
      FFFFFFFFFFF000007FFFFFFFFFE3FFFFFFFFFFE001FFFFFFFFFFFFFFFFF00000
      7FFFFFFFFFFFFFFFFFFFFFE03FFFFFFFFFFFFFFFFFF000007FFFFFFFFFFFFFFF
      FFFFFFE3FFFFFFFFFFFFFFFFFFF000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF800003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      800007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800007FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF80000FFFFFFFFFFFF00FFFFF80000000
      3FFFFFFFFFFFFFFF80001FFFFFFFFFFF0F00FFFF800000003FFFFFFFFFFFFFFF
      80007FFFFFFFFFFC7C003FFF800000003FFFFFFFFFFFFFFF8000FFFFFFFFFFF9
      FD001FFF800000003FFFFFFFFFFFFFFF8000FF00FFFFFFE7FC0007FF80000000
      3FFFFFFFFFFFFFFF8000F0000FFFFFCFFD0003FF800000003FFFFFFFFFFFFFFF
      8000C00003FFFF9FFD0001FF800000003FFFFFFFFFFFFFFFC000000001FFFF3F
      FC0000FF800000003FFFFFFFFFFFFFFFE0000000007FFF7FFF0000FF80000000
      3FFFFFFFFFFFFFFFE0000000003FFEFB8300007F800000003FFFFFFFFFFFFFFF
      E0000000001FFCFB0100003F800000003FFFFFFFFFFFFFFFE0000000000FFDFA
      0000003F800000003FFFFFFFFC3FFFFFE0000000000FF9FA0000001F80000000
      7FFFFFFF1C39FFFFC00000003807FBFA0000001F82010080FFFFFFFF0C30FFFF
      C00000007C03FBFA0000001F86030181FFFFFFFF0000FFFFC0000000E603FBFA
      0000001F8E0700000C03FFFE00007FFFC00000002201F7F50100000F9E0F0000
      0C03FFFC00003FFFC00080002201F4758300000F9E1F00000C03FFF800001FFF
      C00000001C01F77FFF00000F9E3F00100C03FFF800001FFFC00000003801F77F
      FE00000F9E7F00300C03FFF800001FFFC00000000000F0000000000F9EFF0070
      0C03FFF800001FFFC00000000000F0000000000F9FFFF3FE7F9FFFF800003FFF
      C00000000000F0000000000F9FFFF3FE7F9FFFFF0000FFFFC00000000000F000
      0000000F9FFFF3FE7F9FFFFF0000FFFFC00000000000F8000000001F9FFFF000
      001FFFFF0000FFFFC00000000000F8000000001F9FFFF000001FFFFF0000FFFF
      E00000000000F8000000001F9FFFFFFE7FFFFFFF9819FFFFF00000000000F800
      0000001F9FFFFFFE7FFFFFFFF81FFFFFFC0000000001FC000000003FFC0FFFFE
      7FFFFFFFFC3FFFFFFF8000000001FC000000003FF80007FE7FFFFFFFFFFFFFFF
      FF8000000001FE000000007FFC0003C003FFFFFFFFFFFFFFFF8000000001FF00
      000000FFFF8003C000FFFFFFFFFFFFFFFFC000000003FF00000000FFFFE007C0
      00FFFFFFFFFFFFFFFFC000000003FF80000001FFFFFFFFF000FFFFFFFFFFFFFF
      FFE000000007FFC0000003FFFFFFF800003FFFFFFFFFFFFFFFF00000000FFFE0
      000007FFFFFFF800003FFFFFFFFFFFFFFFF00000000FFFF800001FFFFFFFF800
      003FFFFFFFFFFFFFFFF80000001FFFFC00003FFFFFFFF800003FFFFFFFFFFFFF
      FFFC0000003FFFFF0000FFFFFFFFF800003FFFFFFFFFFFFFFFFE0000007FFFFF
      F00FFFFFFFFFF800003FFFFFFFFFFFFFFFFF800001FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFC00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC00007FFFFFF9FF7FFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFF000001FFFFFFE7F7FFFFF800003FFFFFF00E01807FFFF
      FFC0400007FFFFFDBF7FFFFF800007FFFFFF00E01807FFFFFF80074003FFFFFD
      FF4000FF800007FFFFFF00E01807FFFFFF00017C01FFFFFBFF4000FF80000FFD
      DFFF00E01807FFFFFE00017F807FFF9BFF60000180001FFEC07F00E01807FFFF
      FC00017FC03FFF87FF70000100007FF8502F00E01807FFFFF880017FF23FFF01
      FF7800038000FFE0D00FE7FCFF3FFFFFF100017FF91FFF00FF7800038000FFC0
      101FE7FCFF00FFFFE0000140000FFE00FF7C000F8000FFC1D01FE7FCF0300FFF
      C10001400007FE01FF7C00078000FFC0100FE000000003FFC10001600107FC00
      01780007C0007FC1D00FE000001001FF830001700183FC000178000FE0007F9D
      D80FFFFC7FC0007F830001780001F80001780003E0007FBDDC1FFFFCFFD0003F
      870001780001F800017C0003E0007FBFDFFFFFF8FFD0001F0700017C0001F000
      017C0007E0007FFF07FFF0000040000F0F00017C0001FC00017C0003E0007FFC
      03FFF0000070000F0FE01F780001FE00017C0003C0001FF870FFF00000700007
      0FC01F780000FD00017FC003C0001FA01E7FF000007000030FC03F780000FD00
      017FE00FC0001F9C0F9FF000007000031F803F7C0000FB00017FE01FC0001F30
      03CFF000007000011F807F7C0000FB00017FF81FC000BFF003FFFFBFFFF00001
      1F007F7C0000F700017FFFFFC0003FF003FFFFBFFFF000011FC0FF7C0000F700
      017FFFFFC00007F847FFFFBFFFF000011FE0FF3FC000EFFFFF3FFFFFC00003FF
      FFFFFF7FFFF000001FDDFC9FC000FFFFFC9FFFFFC00003FFFFFFFF47FFF00000
      1FDFF3E7C000FFFFF3E7FFFFC00003FFFFFFFF77FFF000000FBFCFFBE000FFFF
      CFFBFFFFC00003FFFFFFFF77FFE000000FBF3FFCE000FFFF3FFCFFFFC00003FF
      FFFFFF00000000002F7CFFFF3800FFFCFFFF3FFFC0001E00FFFFFF0000000000
      077B0EF7DFC0FFFB0EF7DFFFE0003C007FFFFF000000000006E70C07E7C1FFE7
      0C07E7FFF00078003FFFFF0000000000079F0FF9F9C1FF9F0FF9F9FFFC00F000
      1FFFFF8000000001827F0FFEFE83FE7F0FFEFEFFFF81E0000FFFFF8000000001
      81FF0FFF7F03F9FF0FFF7F3FFFFFC0000FFFFF8000000001C1FF07BFFF07E7FF
      07BFFFCFFFFFC0000FFFFF8000000001C0FF871FFE079FFF871FFFF7FFFFC000
      0FFFFFC000000003E07F861FFC0F7FFF861FFFF9FFFFC0000FFFFFC000000003
      F13F861FF91FFFFF861FFFFEFFFFC0000FFFFFE000000007F897821FF23FFFF7
      821FFFFFFFFFC0000FFFFFF00000000FFC03C239E07FFFE3C239FFFFFFFFC000
      0FFFFFF00000000FFE01E16180FFFFE1E161FFFFFFFFC0000FFFFFF80000001F
      FF0000C001FFFFF000C1FFFFFFFFE0001FFFFFFC0000003FFF80000003FFFFF8
      0003FFFFFFFFF0003FFFFFFE0000007FFFC0000007FFFFFC0407FFFFFFFFF800
      7FFFFFFF800001FFFFF000001FFFFFFE0E0FFFFFFFFFFC00FFFFFFFFC00003FF
      FFFC00007FFFFFFFFFBFFFFFFFFFFE01FFFFFFFFF0000FFFFFFF8001FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF00000000000000000000000000000000
      000000000000}
  end
  object GoActionList: TActionList
    OnChange = GoActionListChange
    Left = 536
    Top = 72
    object GoPSPlant: TAction
      Category = 'PlantStructure'
      Caption = '&Plant'
      OnExecute = GoPSPlantExecute
    end
    object GoEmployee: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Employee'
      OnExecute = GoEmployeeExecute
    end
    object GoEmplContract: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Employee Contract'
      OnExecute = GoEmplContractExecute
    end
    object GoBusinessUnit: TAction
      Category = 'PlantStructure'
      Caption = '&Business Unit'
      OnExecute = GoBusinessUnitExecute
    end
    object GoDepartment: TAction
      Category = 'PlantStructure'
      Caption = '&Department'
      OnExecute = GoDepartmentExecute
    end
    object GoWorkspot: TAction
      Category = 'PlantStructure'
      Caption = '&Workspot'
      OnExecute = GoWorkspotExecute
    end
    object GoTeam: TAction
      Category = 'PlantStructure'
      Caption = '&Team'
      OnExecute = GoTeamExecute
    end
    object GoTeamDept: TAction
      Category = 'PlantStructure'
      Caption = '&Department per team'
      OnExecute = GoTeamDeptExecute
    end
    object GoShift: TAction
      Category = 'TimeStructure'
      Caption = '&Shift'
      OnExecute = GoShiftExecute
    end
    object GoTimeBlockShift: TAction
      Category = 'TimeStructure'
      Caption = '&Timeblock Per Shift'
      OnExecute = GoTimeBlockShiftExecute
    end
    object GoBreakPerShift: TAction
      Category = 'TimeStructure'
      Caption = '&Break Per Shift'
      OnExecute = GoBreakPerShiftExecute
    end
    object GoTimeBlockPerDept: TAction
      Category = 'TimeStructure'
      Caption = '&Time Breaks Per Department'
      OnExecute = GoTimeBlockPerDeptExecute
    end
    object GoBreakPerDept: TAction
      Category = 'TimeStructure'
      Caption = '&Breaks Per Department'
      OnExecute = GoBreakPerDeptExecute
    end
    object GoIDCard: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Fixed staff data'
      OnExecute = GoIDCardExecute
    end
    object GoTimeBlockPerEmpl: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Time Block Per Employee'
      OnExecute = GoTimeBlockPerEmplExecute
    end
    object GoBreakPerEmpl: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Break Per Employee'
      OnExecute = GoBreakPerEmplExecute
    end
    object GoContractGroups: TAction
      Category = 'Contract Groups'
      Caption = '&Contract Groups'
      OnExecute = GoContractGroupsExecute
    end
    object GoJobCodes: TAction
      Category = 'PlantStructure'
      Caption = '&Job Codes'
    end
    object GoBUPerWorkSpot: TAction
      Category = 'PlantStructure'
      Caption = 'Business Unit Per WorkSpot'
    end
    object GoExceptionalhours: TAction
      Category = 'Contract Groups'
      Caption = '&Exceptional Hours'
      OnExecute = GoExceptionalhoursExecute
    end
    object GoOvertime: TAction
      Category = 'Contract Groups'
      Caption = '&Overtime definitions'
      OnExecute = GoOvertimeExecute
    end
    object GoWorkSpotWorkstation: TAction
      Category = 'PlantStructure'
      Caption = '&Workspot Per Workstation'
      OnExecute = GoWorkSpotWorkstationExecute
    end
    object GoWKPerEmpl: TAction
      Category = 'Fixed Staff Data'
      Caption = '&Workspot Per Employee'
      OnExecute = GoWKPerEmplExecute
    end
    object GoHoursPerEmployee: TAction
      Category = 'Time recording'
      Caption = '&Hours per employee'
      OnExecute = GoHoursPerEmployeeExecute
    end
    object GoReportEmployee: TAction
      Category = 'Report'
      Caption = 'Report employee'
      OnExecute = GoReportEmployeeExecute
    end
    object GoCheckListScans: TAction
      Category = 'Report'
      Caption = 'Checklist scans'
      OnExecute = GoCheckListScansExecute
    end
    object GoCompHours: TAction
      Category = 'Report'
      Caption = 'Comparation hours with contract hours'
      OnExecute = GoCompHoursExecute
    end
    object GoRepHoursPerEmpl: TAction
      Category = 'Report'
      Caption = 'Hours per employee'
      OnExecute = GoRepHoursPerEmplExecute
    end
    object GoWTRAbsenceReport: TAction
      Category = 'Report'
      Caption = 'Absence report shedule'
      OnExecute = GoWTRAbsenceReportExecute
    end
    object GoTimeRecording: TAction
      Category = 'Time recording'
      Caption = '&Time recording'
    end
    object GoTimeRecScan: TAction
      Category = 'Time recording'
      Caption = 'Time recording scanning'
      OnExecute = GoTimeRecScanExecute
    end
    object GoAbsenceReport: TAction
      Category = 'Report'
      Caption = 'GoAbsenceReport'
      OnExecute = GoAbsenceReportExecute
    end
    object GoRepHrsPerDay: TAction
      Category = 'Report'
      Caption = 'GoRepHrsPerDay'
      OnExecute = GoRepHrsPerDayExecute
    end
    object GoReportAbsenceCard: TAction
      Category = 'Report'
      Caption = 'GoReportAbsenceCard'
      OnExecute = GoReportAbsenceCardExecute
    end
    object GoRepDirIndHrsWeek: TAction
      Category = 'Report'
      Caption = 'Period report direct/indirect hours per week'
      OnExecute = GoRepDirIndHrsWeekExecute
    end
    object GoTransferToNextYear: TAction
      Category = 'Time recording'
      Caption = 'Transfer free time to next year'
      OnExecute = GoTransferToNextYearExecute
    end
    object GoCalculateEarnedWKR: TAction
      Category = 'Time recording'
      Caption = 'Calculate earned worktime reduction'
      OnExecute = GoCalculateEarnedWKRExecute
    end
    object GoRepHrsPerWorkspot: TAction
      Category = 'Report'
      Caption = 'Report hours per workspot'
      OnExecute = GoRepHrsPerWorkspotExecute
    end
    object GoRepHrsPerWKCum: TAction
      Category = 'Report'
      Caption = 'Report hours per workspot cumulative'
      OnExecute = GoRepHrsPerWKCumExecute
    end
    object GoRepHrsPerContractGrpCUM: TAction
      Category = 'Report'
      Caption = 'Report hours per contract group cumulative'
      OnExecute = GoRepHrsPerContractGrpCUMExecute
    end
    object GoReportRatioIllHrs: TAction
      Category = 'Report'
      Caption = 'Report ratio illness hours'
      OnExecute = GoReportRatioIllHrsExecute
    end
    object GoReportAvailableUsedTime: TAction
      Category = 'Report'
      Caption = 'Report available, used and planned free time'
      OnExecute = GoReportAvailableUsedTimeExecute
    end
    object GoActionBankHolidays: TAction
      Category = 'Time recording'
      Caption = 'Bank holidays'
      OnExecute = GoActionBankHolidaysExecute
    end
    object GoActionProcessAbsence: TAction
      Category = 'Time recording'
      Caption = 'Process planned absnece and non-scanning hours'
      OnExecute = GoActionProcessAbsenceExecute
    end
    object GoIllnessMessages: TAction
      Category = 'Time recording'
      Caption = 'Illness messages'
      OnExecute = GoIllnessMessagesExecute
    end
    object GoStandardOccupation: TAction
      Category = 'Staff planning'
      Caption = 'Standard occupation'
      OnExecute = GoStandardOccupationExecute
    end
    object GoStdStaffAvailability: TAction
      Category = 'Staff planning'
      Caption = 'Standard staff availability'
      OnExecute = GoStdStaffAvailabilityExecute
    end
    object GoShiftSchedule: TAction
      Category = 'Staff planning'
      Caption = 'Sift schedule'
      OnExecute = GoShiftScheduleExecute
    end
    object GoStaffAvailability: TAction
      Category = 'Staff planning'
      Caption = 'Staff availability'
      OnExecute = GoStaffAvailabilityExecute
    end
    object GoEmplAvailability: TAction
      Category = 'Staff planning'
      Caption = 'Employee availability'
      OnExecute = GoEmplAvailabilityExecute
    end
    object GoStaffPlanningAction: TAction
      Category = 'Staff planning'
      Caption = 'Staff planning'
      OnExecute = GoStaffPlanningActionExecute
    end
    object ReportStaffPlanning: TAction
      Caption = 'GoReportStaffPlanning'
    end
    object GoReportStaffAvaillability: TAction
      Category = 'ReportStaffPlanning'
      Caption = 'Report Staff Availability'
      OnExecute = GoReportStaffAvaillabilityExecute
    end
    object GoReportEMPAvailability: TAction
      Category = 'ReportStaffPlanning'
      Caption = 'Report Employee Availability'
      OnExecute = GoReportEMPAvailabilityExecute
    end
    object GoReportStaffPlanning: TAction
      Category = 'ReportStaffPlanning'
      Caption = 'Report Staff Planning'
      OnExecute = GoReportStaffPlanningExecute
    end
    object GoReportStaffPlanningCond: TAction
      Category = 'ReportStaffPlanning'
      Caption = 'Report Staff Planning Condensed'
      OnExecute = GoReportStaffPlanningCondExecute
    end
    object ProductivityControl: TAction
      Caption = 'Productivity Control'
    end
    object GoManualDataCollection: TAction
      Category = 'ProductivityControl'
      Caption = 'Manual data collection'
      OnExecute = GoManualDataCollectionExecute
    end
    object GoExportPayroll: TAction
      Category = 'Payroll system'
      Caption = 'Export to payroll'
      OnExecute = GoExportPayrollExecute
    end
    object GoMonthlyGroupEfficiency: TAction
      Category = 'Payroll system'
      Caption = 'Monthly group efficiency'
      OnExecute = GoMonthlyGroupEffiencyExecute
    end
    object GoProductionReport: TAction
      Category = 'ReportProduction'
      Caption = 'ReportProduction'
      OnExecute = GoProductionReportExecute
    end
    object GoProductionDetailReport: TAction
      Category = 'ReportProduction'
      Caption = 'GoProductionDetailReport'
      OnExecute = GoProductionDetailReportExecute
    end
    object GoTeamIncentiveRep: TAction
      Category = 'ReportProduction'
      Caption = 'GoTeamIncentiveRep'
      OnExecute = GoTeamIncentiveRepExecute
    end
    object GoRevenue: TAction
      Category = 'Sales'
      Caption = 'GoRevenue'
      OnExecute = GoRevenueExecute
    end
    object GoMistakePerEmpl: TAction
      Category = 'Quality incentive'
      Caption = 'Mistake per employee'
      OnExecute = GoMistakePerEmplExecute
    end
    object GoQualityIncentiveCalc: TAction
      Category = 'Quality incentive'
      Caption = 'Quality incentive calculation'
      OnExecute = GoQualityIncentiveCalcExecute
    end
    object GoSettings: TAction
      Category = 'Configuration'
      Caption = 'GoSettings'
      OnExecute = GoConfigurationExecute
    end
    object GoDataConnection: TAction
      Category = 'Configuration'
      Caption = 'GoDataColConnection'
      OnExecute = GoColConnectionExecute
    end
    object GoComportConnection: TAction
      Category = 'Configuration'
      Caption = 'Comport connection'
      OnExecute = GoComportConnectionExecute
    end
    object GoTypeOfHours: TAction
      Category = 'Configuration'
      Caption = 'Type of hours'
      OnExecute = GoTypeHoursExecute
    end
    object GoErrorLog: TAction
      Category = 'Configuration'
      Caption = 'Error Log'
      OnExecute = GoErrorLogExecute
    end
    object GoAbsType: TAction
      Category = 'Configuration'
      Caption = 'Absence types'
      OnExecute = GoAbsTypeExecute
    end
    object GoAbsenceRsn: TAction
      Category = 'Configuration'
      Caption = 'Absence reasons'
      OnExecute = GoAbsenceReasonExecute
    end
    object ActionQualityConf: TAction
      Category = 'Configuration'
      Caption = 'Quality incentive configuration'
      OnExecute = GoQualityConfExecute
    end
    object GoPaymentExpCode: TAction
      Category = 'Payroll system'
      Caption = 'Payment export code'
      OnExecute = GoPaymentExpCodeExecute
    end
    object GoExtraPayment: TAction
      Category = 'Payroll system'
      Caption = 'Extra payments'
      OnExecute = GoExtraPaymentExecute
    end
    object GoReportStaffPlanDay: TAction
      Category = 'ReportStaffPlanning'
      Caption = 'GoReportStaffPlanDay'
      OnExecute = GoReportStaffPlanDayExecute
    end
    object GoProductionCompReport: TAction
      Category = 'ReportProduction'
      Caption = 'GoProductionCompReport'
      OnExecute = GoProductionCompReportExecute
    end
    object GoProductionCompTargetReport: TAction
      Category = 'ReportProduction'
      Caption = 'GoProductionCompTargetReport'
      OnExecute = GoProductionCompTargetReportExecute
    end
    object GoEmployeeRegistrations: TAction
      Category = 'Registrations'
      Caption = 'Employee Registrations'
      OnExecute = GoEmployeeRegistrationsExecute
    end
    object GoReportEmployeePaylist: TAction
      Category = 'ReportPayments'
      Caption = 'Report Employee Paylist'
      OnExecute = GoReportEmployeePaylistExecute
    end
    object UnitMaintenance: TAction
      Caption = 'Unit Maintenance'
    end
    object GoRepPlantStructure: TAction
      Category = 'UnitMaintenance'
      Caption = 'Report Plant Structure'
      OnExecute = GoRepPlantStructureExecute
    end
    object GoMachine: TAction
      Category = 'PlantStructure'
      Caption = '&Machine'
      OnExecute = GoMachineExecute
    end
    object GoCountries: TAction
      Category = 'Configuration'
      Caption = 'Countries'
      OnExecute = GoCountriesExecute
    end
    object GoAbsenceTypesPerCountry: TAction
      Category = 'Configuration'
      Caption = 'Absence Types Per Country'
      OnExecute = GoAbsenceTypesPerCountryExecute
    end
    object GoAbsenceReasonsPerCountry: TAction
      Category = 'Configuration'
      Caption = 'Absence Reasons Per Country'
      OnExecute = GoAbsenceReasonsPerCountryExecute
    end
    object GoHourTypesPerCountry: TAction
      Category = 'Configuration'
      Caption = 'Hour Types Per Country'
      OnExecute = GoHourTypesPerCountryExecute
    end
    object GoReportHolidayCard: TAction
      Category = 'Report'
      Caption = 'Report Holiday Card'
      OnExecute = GoReportHolidayCardExecute
    end
    object GoReportChangesPerScan: TAction
      Category = 'Report'
      Caption = 'Report Changes Per Scan'
      OnExecute = GoReportChangesPerScanExecute
    end
    object GoReportEmployeeIllness: TAction
      Category = 'Report'
      Caption = 'Report Employee Illness'
      OnExecute = GoReportEmployeeIllnessExecute
    end
    object GoReportIncentiveProgram: TAction
      Category = 'Report'
      Caption = 'Report Incentive Program'
      OnExecute = GoReportIncentiveProgramExecute
    end
    object GoReportJobComments: TAction
      Category = 'Report'
      Caption = 'Report Job Comments'
      OnExecute = GoReportJobCommentsExecute
    end
    object GoWorkScheduleAction: TAction
      Category = 'Staff planning'
      Caption = 'Work Schedule'
      OnExecute = GoWorkScheduleActionExecute
    end
    object GoWorkScheduleDetailsAction: TAction
      Category = 'Staff planning'
      Caption = 'Work Schedule Details'
      OnExecute = GoWorkScheduleDetailsActionExecute
    end
    object GoReportEmpInfoDevPeriod: TAction
      Category = 'Report'
      Caption = 'Report Employee Info and Deviations'
      OnExecute = GoReportEmpInfoDevPeriodExecute
    end
    object GoRealTimeEffMonitor: TAction
      Category = 'ProductivityControl'
      Caption = 'Real Time Efficiency Monitor'
      OnExecute = GoRealTimeEffMonitorExecute
    end
    object GoReportGhostCounts: TAction
      Category = 'ReportProduction'
      Caption = 'Report Ghost Counts'
      OnExecute = GoReportGhostCountsExecute
    end
    object GoReportInOutAbsenceOvertime: TAction
      Category = 'Report'
      Caption = 'Report In/Outscan, Absence, Overtime'
      OnExecute = GoReportInOutAbsenceOvertimeExecute
    end
    object GoPTODef: TAction
      Category = 'Contract Groups'
      Caption = '&PTO Definitions'
      OnExecute = GoPTODefExecute
    end
  end
end
