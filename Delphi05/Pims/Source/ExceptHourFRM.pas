(*
  Changes:
    MRA:1-APR-2009 RV025.
      Addition of extra hourtype to make it possible to book exceptional
      hours made during overtime.
    SO:04-AUG-2010 RV067.6. 550512
      Copy function exceptional hours
    MRA:25-AUG-2010. Addition of Copy and Paste-buttons for use in order 550512.
    MRA:21-SEP-2010. RV067.MRA.34.
    - Changed some hard-coded-strings to UPimsMessage-strings for
      translate purposes.
    MRA:1-DEC-2010. RV082.5.
    - Store ContractgroupCode for selected employee in memory,
      to use in Contractgroup-related dialogs, so it can
      be looked up when showing these dialogs.
*)

unit ExceptHourFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, DBCtrls,
  StdCtrls, dxDBTLCl, dxGrClms, dxDBEdtr, Menus;

type
  TExceptHourF = class(TGridBaseF)
    LabelWeekDay: TLabel;
    LabelStartTime: TLabel;
    dxDBTimeEditStartTime: TdxDBTimeEdit;
    LabelEndTime: TLabel;
    dxDBTimeEditEndTime: TdxDBTimeEdit;
    LabelHourTypeCode: TLabel;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnTime: TdxDBGridCheckColumn;
    dxMasterGridColumnMoney: TdxDBGridCheckColumn;
    dxMasterGridColumnReduction: TdxDBGridCheckColumn;
    dxDetailGridColumnHourType: TdxDBGridLookupColumn;
    dxDetailGridColumnStartTime: TdxDBGridTimeColumn;
    dxDetailGridColumnEndTime: TdxDBGridTimeColumn;
    dxDBLookupEditHour: TdxDBLookupEdit;
    dxDetailGridColumnDay: TdxDBGridColumn;
    dxDBImgEdtDayOfWeek: TdxDBImageEdit;
    Label1: TLabel;
    dxDBLookupEditOvertimeHourtype: TdxDBLookupEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure mniCopyClick(Sender: TObject);
    procedure mniPasteClick(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FCopyContractGroupCode: String;
    FPasteContractGroupCode: String;
  public
    { Public declarations }
  end;
  function ExceptHourF: TExceptHourF;

implementation

uses
  ExceptHourDMT, SystemDMT, ListProcsFRM, UPimsMessageRes;

{$R *.DFM}

var
  ExceptHourF_HND: TExceptHourF;

function ExceptHourF: TExceptHourF;
begin
  if (ExceptHourF_HND = nil) then
  begin
    ExceptHourF_HND := TExceptHourF.Create(Application);
  end;
  Result := ExceptHourF_HND;
end;
procedure TExceptHourF.FormDestroy(Sender: TObject);
begin
  inherited;
  ExceptHourF_HND := Nil;
end;

procedure TExceptHourF.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  ExceptHourDM := CreateFormDM(TExceptHourDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := ExceptHourDM.DataSourceDetail;
    dxMasterGrid.DataSource := ExceptHourDM.DataSourceMaster;
  end;
  inherited;
  // MR:11-05-2005 Use other component that is databound instead of TComboBox.
(*
  DaySelection.Clear;
  for i:= 1 to 7 do
    DaySelection.Items.Add(SystemDM.GetDayWDescription(i));
  i := ListProcsF.DayWStartOnFromDate(Now);
  DaySelection.ItemIndex := i - 1;
*)
  for i := 1 to 7 do
  begin
    dxDBImgEdtDayOfWeek.Values.Add(IntToStr(i));
    dxDBImgEdtDayOfWeek.Descriptions.Add(SystemDM.GetDayWDescription(i));
  end;
end;

procedure TExceptHourF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  dxDBImgEdtDayOfWeek.SetFocus;
end;
//RV067.6.
procedure TExceptHourF.mniCopyClick(Sender: TObject);
begin
  FCopyContractGroupCode := '';

  if (not ExceptHourDM.TableMaster.Active) or
    ExceptHourDM.TableMaster.Eof then Exit;

  FCopyContractGroupCode :=
    ExceptHourDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  dxBarButtonPaste.Enabled := False;
  dxBarButtonPaste.Hint := SPimsPaste; // RV067.MRA.34.
end;

procedure TExceptHourF.mniPasteClick(Sender: TObject);
var
  HoursDefined: Boolean;
begin
  FPasteContractGroupCode := '';

  if (not ExceptHourDM.TableMaster.Active) or
    ExceptHourDM.TableMaster.Eof then
    Exit;

  FPasteContractGroupCode :=
    ExceptHourDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;

  HoursDefined :=
    ExceptHourDM.ExceptionalHoursDefinedFor(FPasteContractGroupCode);
  if HoursDefined then
    if DisplayMessage(SPimsDefExceptHoursFound, // RV067.MRA.34
        mtConfirmation, [mbYes, mbNo]) = mrNo
    then
      Exit;

  ExceptHourDM.CopyExceptionalHours(FCopyContractGroupCode,
    FPasteContractGroupCode, HoursDefined);
end;

procedure TExceptHourF.dxGridEnter(Sender: TObject);
begin
  inherited;
  if ActiveGrid = dxMastergrid then
  begin
    dxBarButtonCopy.Visible := ivAlways;
    dxBarButtonPaste.Visible := ivAlways;
  end
  else
  begin
    dxBarButtonCopy.Visible := ivNever;
    dxBarButtonPaste.Visible := ivNever;
  end;
end;

procedure TExceptHourF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  FPasteContractGroupCode :=
    ExceptHourDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  dxBarButtonPaste.Enabled := (FCopyContractGroupCode <> '') and
    (FCopyContractGroupCode <> FPasteContractGroupCode);
  if dxBarButtonPaste.Enabled then
    dxBarButtonPaste.Hint :=
      Format(SPimsPasteExceptHours, // RV067.MRA.34
        [FCopyContractGroupCode])
  else
    dxBarButtonPaste.Hint := SPimsPaste; // RV067.MRA.34
end;

procedure TExceptHourF.FormShow(Sender: TObject);
begin
  inherited;
  // RV082.5.
  if SystemDM.ASaveTimeRecScanning.ContractGroupCode <> '' then
  begin
    try
      ExceptHourDM.TableMaster.Locate('CONTRACTGROUP_CODE',
        SystemDM.ASaveTimeRecScanning.ContractGroupCode, []);
    except
      // Ignore error
    end;
  end;
end;

procedure TExceptHourF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  // RV082.5.
  try
    if not ExceptHourDM.TableMaster.IsEmpty then
      SystemDM.ASaveTimeRecScanning.ContractGroupCode :=
        ExceptHourDM.TableMaster.FieldByName('CONTRACTGROUP_CODE').AsString;
  except
    // Ignore error
  end;
end;

end.
