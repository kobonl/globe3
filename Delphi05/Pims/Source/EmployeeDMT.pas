(*
  Changes:
    MR:25-02-2005 Order 550386 Shift_Number added to Employee-Table.
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:22-FEB-2010. RV054.9. 889956. UpdateScript: 48_1.
    - Make field SHIFT_NUMBER a mandatory field.
    SO: 07-JUN-2010 REV065.3 added the Free Text field
    - see REV065.3.sql
    SO: 08-JUN-2010 REV065.7
    - added two new queries to update the ilness messages and shift schedule
    tables when the employee end date is filled
    SO: 15-JUN-2010 RV066.1. 550491
    - tailor made non-scanners
    MRA:2-SEP-2010 RV067.MRA.24 Additional changes for 550482.
    - When the enddate for the employee is set, also
      delete:
      - Standard Availability
      - Employee Availability
      - Standard Employee Planning
    MRA:20-JAN-2011 RV085.9. Bugfix.
    - When filtering on teams-per-users is used,
      it shows multiple the same departments in
      department-selection.
   JVL:07-apr-2011 RV089.2. Order 550525.
    - Automatically add the enddate on employee contract when ending employee.
    MRA:11-JUL-211 RV094.7. Bugfix
    - Show message when delete/modify is not allowed based
      on system setting (update employee-checkbox).
    MRA:8-MAY-2012 20013133. Change employee number
    - Changing employee number does not work.
    MRA:4-FEB-2013 20013923 / TD-24853
    - Make separate settings for update/delete for plant/employee/workspot
    MRA:10-NOV-2014 TD-26057
    - Logging added when employee is changed.
    MRA:27-FEB-2015 TD-26114
    - Cannot change name in Pims
    - TableDetail: Changed 'UpdateMode' to 'upWhereKeyOnly'
    MRA:13-JUN-2016 VEI001-7
    - Addition of EMPLOYEE_ID that is only used for HYBRID=1.
    - It is filled automatically (sequence).
    MRA:7-SEP-2016 PIM-219
    - Delete planning when end-date is set.
    - When end-date is set delete any planning that exists after it.
    - This is needed, otherwise it is still shown in reports and it is
      difficult to delete it from Pims itself.
    MRA:3-OCT-2016 PIM-227
    - Make change of employee number optional
    - Related to this order:
      - A Refresh during AfterPost is needed to prevent an error about
        Record/Key deleted.
      - This can happen when you do the following:
        - Add a new employee-record.
        - Change the just added employee-record (change Short-name),
          without leaving the dialog.
        - It gives the error. You have to cancel the change to get rid of
          the error.
        - Also: It does not show the new record in the grid, only after
                re-opening the dialog.
    MRA:11-NOV-2016 PIM-174
    - New employee and add all related data
    MRA:7-DEC-2016 PIM-248
    - Employee dialog must take department by team as default.
    - Also take first team by plant as default.
    MRA:26-JAN-2017 PIM-266
    - Employee dialog gives error related to teams per user
    - Related to this: QueryDeptEmpl was not used, so has been
      removed.
    MRA:4-FEB-2019 GLOB3-209
    - Employee Information Page, add 3 extra fields.
    MRA:29-APR-2019 GLOB3-290
    - Handling information about gender
    MRA:25-JAN-2021 GLOB3-411 Add External Reference to Employee-dialog
*)

unit EmployeeDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, DBClient, Provider;

type
  TEmployeeDM = class(TGridBaseDM)
    TableContractGroup: TTable;
    DataSourceContractGroup: TDataSource;
    TableLanguage: TTable;
    DataSourceLanguage: TDataSource;
    TablePlant: TTable;
    DataSourcePlant: TDataSource;
    TableLanguageLANGUAGE_CODE: TStringField;
    TableLanguageDESCRIPTION: TStringField;
    TableLanguageCREATIONDATE: TDateTimeField;
    TableLanguageMUTATIONDATE: TDateTimeField;
    TableLanguageMUTATOR: TStringField;
    DataSourceTeam: TDataSource;
    TableContractGroupCONTRACTGROUP_CODE: TStringField;
    TableContractGroupDESCRIPTION: TStringField;
    TableContractGroupCREATIONDATE: TDateTimeField;
    TableContractGroupTIME_FOR_TIME_YN: TStringField;
    TableContractGroupBONUS_IN_MONEY_YN: TStringField;
    TableContractGroupMUTATIONDATE: TDateTimeField;
    TableContractGroupOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField;
    TableContractGroupMUTATOR: TStringField;
    TableContractGroupPERIOD_STARTS_IN_WEEK: TIntegerField;
    TableContractGroupWEEKS_IN_PERIOD: TIntegerField;
    TableContractGroupWORK_TIME_REDUCTION_YN: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    QueryWork: TQuery;
    StoredProcDelete: TStoredProc;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailSHORT_NAME: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailDEPARTMENT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailADDRESS: TStringField;
    TableDetailZIPCODE: TStringField;
    TableDetailCITY: TStringField;
    TableDetailSTATE: TStringField;
    TableDetailLANGUAGE_CODE: TStringField;
    TableDetailPHONE_NUMBER: TStringField;
    TableDetailEMAIL_ADDRESS: TStringField;
    TableDetailDATE_OF_BIRTH: TDateTimeField;
    TableDetailSEX: TStringField;
    TableDetailSOCIAL_SECURITY_NUMBER: TStringField;
    TableDetailTEAM_CODE: TStringField;
    TableDetailENDDATE: TDateTimeField;
    TableDetailCONTRACTGROUP_CODE: TStringField;
    TableDetailIS_SCANNING_YN: TStringField;
    TableDetailCUT_OF_TIME_YN: TStringField;
    TableDetailREMARK: TStringField;
    TableDetailCALC_BONUS_YN: TStringField;
    TableDetailFIRSTAID_YN: TStringField;
    TableDetailFIRSTAID_EXP_DATE: TDateTimeField;
    TableDetailCONTRACTLU: TStringField;
    TableDetailLANGUAGELU: TStringField;
    TableDetailTEAMLU: TStringField;
    TableDetailSTARTDATE: TDateTimeField;
    TableDetailFIRSTAIDEXP_DATE_CAL: TDateField;
    TableDetailEMPCALC: TFloatField;
    TableDetailPLANTLU: TStringField;
    QueryCheckDeptTeam: TQuery;
    TableTeamUser: TTable;
    dspFilterTeamUser: TDataSetProvider;
    cdsFilterTeamUser: TClientDataSet;
    QueryTeam: TQuery;
    TableDetailGRADE: TIntegerField;
    TableDetailSHIFT_NUMBER: TIntegerField;
    QueryShift: TQuery;
    TableShift: TTable;
    TableDetailSHIFTDESC: TStringField;
    TableDetailSHIFTLU: TStringField;
    TableDept: TTable;
    QueryDept: TQuery;
    TableDetailDEPTDESC: TStringField;
    TableDetailDEPTLU: TStringField;
    TableDeptDEPARTMENT_CODE: TStringField;
    TableDeptPLANT_CODE: TStringField;
    TableDeptDESCRIPTION: TStringField;
    TableShiftSHIFT_NUMBER: TIntegerField;
    TableShiftPLANT_CODE: TStringField;
    TableShiftDESCRIPTION: TStringField;
    TableDetailFREETEXT: TMemoField;
    QueryUpdateIlnessMessage: TQuery;
    QueryUpdateShiftSchedule: TQuery;
    TableDetailBOOK_PROD_HRS_YN: TStringField;
    qryDeleteStandAvail: TQuery;
    qryDeleteEmpAvail: TQuery;
    qryDeleteStandEmpPlan: TQuery;
    QueryEndDateEmplContract: TQuery;
    StoredProcUpdateEmpl: TStoredProc;
    TableDetailFAKE_EMPLOYEE_YN: TStringField;
    TableDetailEMPLOYEE_ID: TFloatField;
    qryDeleteEmpPlan: TQuery;
    qryCopyEmpContract: TQuery;
    qryCopyWSPerEmp: TQuery;
    qryCopyStandardAvail: TQuery;
    qryCopyShiftSchedule: TQuery;
    qryCopyEmpAvail: TQuery;
    qryCopyEmpPlan: TQuery;
    qryDeptPerTeam: TQuery;
    TableDetailETHNICITY: TStringField;
    TableDetailJOBTITLE: TStringField;
    TableDetailMARITALSTATE: TStringField;
    TableDetailEXT_REF: TStringField;
    procedure TableDetailAfterScroll(DataSet: TDataSet);
    procedure TableDetailBeforeDelete(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TablePlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDeptFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TableDetailAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
    FOldEmpl: Integer;
    FLastState: TDataSetState;
      //CAR 550274 - User rights for teams
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure UpdateIlnessMessage(AEmplNo: Integer; AEndDate: TDateTime);
    procedure UpdateShiftSchedule(AEmplNo: Integer; AEndDate: TDateTime);
    procedure DeleteOnEndDate(AEmplNo: Integer; AEndDate: TDateTime);
    procedure UpdateEndDateEmplContract(AEmplNo: Integer; AEndDate: TDateTime);
  public
    { Public declarations }
    procedure UpdateEmpl;
    function CheckEmptyDeptTeam: Boolean;
    property OldEmpl: Integer read FOldEmpl write FOldEmpl;
    property LastState: TDataSetState read FLastState write FLastState;
  end;

var
  EmployeeDM: TEmployeeDM;

implementation

{$R *.DFM}
uses
  UPimsConst, SystemDMT, UPimsMessageRes, EmployeeFRM, UGlobalFunctions,
  DialogCopyEmpDataFRM;

procedure TEmployeeDM.UpdateEmpl;
var
  NewEmp: Integer;
begin
  StoredProcUpdateEmpl.ParamByName('SHORT_NAME').AsString :=
    TableDetail.FieldByName('SHORT_NAME').AsString;
  StoredProcUpdateEmpl.ParamByName('PLANT_CODE').AsString :=
    TableDetail.FieldByName('PLANT_CODE').AsString;
  StoredProcUpdateEmpl.ParamByName('DESCRIPTION').AsString :=
    TableDetail.FieldByName('DESCRIPTION').AsString;
  StoredProcUpdateEmpl.ParamByName('DEPARTMENT_CODE').AsString :=
     TableDetail.FieldByName('DEPARTMENT_CODE').AsString;
  StoredProcUpdateEmpl.ParamByName('ADDRESS').AsString :=
    TableDetail.FieldByName('ADDRESS').AsString;
  StoredProcUpdateEmpl.ParamByName('ZIPCODE').AsString :=
    TableDetail.FieldByName('ZIPCODE').AsString;
  StoredProcUpdateEmpl.ParamByName('CITY').AsString :=
    TableDetail.FieldByName('CITY').AsString;
  StoredProcUpdateEmpl.ParamByName('STATE').AsString :=
    TableDetail.FieldByName('STATE').AsString;
  StoredProcUpdateEmpl.ParamByName('LANGUAGE_CODE').AsString :=
    TableDetail.FieldByName('LANGUAGE_CODE').AsString;
  StoredProcUpdateEmpl.ParamByName('PHONE_NUMBER').AsString :=
    TableDetail.FieldByName('PHONE_NUMBER').AsString;
  StoredProcUpdateEmpl.ParamByName('EMAIL_ADDRESS').AsString :=
    TableDetail.FieldByName('EMAIL_ADDRESS').AsString;
  StoredProcUpdateEmpl.ParamByName('DATE_OF_BIRTH').AsDateTime :=
    TableDetail.FieldByName('DATE_OF_BIRTH').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('CREATIONDATE').AsDateTime :=
    TableDetail.FieldByName('CREATIONDATE').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('MUTATIONDATE').AsDateTime :=
    TableDetail.FieldByName('MUTATIONDATE').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('MUTATOR').AsString :=
    TableDetail.FieldByName('MUTATOR').AsString;
  StoredProcUpdateEmpl.ParamByName('SEX').AsString :=
    TableDetail.FieldByName('SEX').AsString;
  StoredProcUpdateEmpl.ParamByName('SOCIAL_SECURITY_NUMBER').AsString :=
    TableDetail.FieldByName('SOCIAL_SECURITY_NUMBER').AsString;
  StoredProcUpdateEmpl.ParamByName('TEAM_CODE').AsString :=
    TableDetail.FieldByName('TEAM_CODE').AsString;
  StoredProcUpdateEmpl.ParamByName('CONTRACTGROUP_CODE').AsString :=
    TableDetail.FieldByName('CONTRACTGROUP_CODE').AsString;
  StoredProcUpdateEmpl.ParamByName('IS_SCANNING_YN').AsString :=
    TableDetail.FieldByName('IS_SCANNING_YN').AsString;
  StoredProcUpdateEmpl.ParamByName('CUT_OF_TIME_YN').AsString :=
    TableDetail.FieldByName('CUT_OF_TIME_YN').AsString;
  StoredProcUpdateEmpl.ParamByName('REMARK').AsString :=
    TableDetail.FieldByName('REMARK').AsString;
  StoredProcUpdateEmpl.ParamByName('CALC_BONUS_YN').AsString :=
    TableDetail.FieldByName('CALC_BONUS_YN').AsString;
  StoredProcUpdateEmpl.ParamByName('FIRSTAID_YN').AsString :=
    TableDetail.FieldByName('FIRSTAID_YN').AsString;
  StoredProcUpdateEmpl.ParamByName('STARTDATE').AsDateTime :=
    TableDetail.FieldByName('STARTDATE').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('ENDDATE').AsDateTime :=
    TableDetail.FieldByName('ENDDATE').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('FIRSTAID_EXP_DATE').AsDateTime :=
    TableDetail.FieldByName('FIRSTAID_EXP_DATE').AsDateTime;
  StoredProcUpdateEmpl.ParamByName('SPECIAL_CHR').AsSTRING := CHR_SEP;
  StoredProcUpdateEmpl.ParamByName('OLD_EMPLOYEE').AsInteger := OldEmpl;
  StoredProcUpdateEmpl.ParamByName('NEW_EMPLOYEE').AsInteger :=
     TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  StoredProcUpdateEmpl.ParamByName('DATETMP').AsDateTime := 0;
  // 20013133.
  StoredProcUpdateEmpl.ParamByName('GRADE').AsInteger :=
    TableDetail.FieldByName('GRADE').AsInteger;
  StoredProcUpdateEmpl.ParamByName('SHIFT_NUMBER').AsInteger :=
    TableDetail.FieldByName('SHIFT_NUMBER').AsInteger;
  StoredProcUpdateEmpl.ParamByName('FREETEXT').AsString :=
    TableDetail.FieldByName('FREETEXT').AsString;
  StoredProcUpdateEmpl.ParamByName('BOOK_PROD_HRS_YN').AsString :=
    TableDetail.FieldByName('BOOK_PROD_HRS_YN').AsString;
    
  NewEmp := TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;

  TableDetail.Cancel;
  StoredProcUpdateEmpl.Prepare;
  StoredProcUpdateEmpl.ExecProc;

  TableDetail.Refresh;
  TableDetail.FindKey([NewEmp]);
end;

procedure TEmployeeDM.TableDetailAfterScroll(DataSet: TDataSet);
begin
  inherited;
  LastState := dsBrowse;
  OldEmpl := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TEmployeeDM.TableDetailBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  LastState := dsBrowse;
  // 20013923
  if (SystemDM.DeleteEmployeeYN = CHECKEDVALUE) then
  begin
    DefaultBeforeDelete(DataSet);
    if DisplayMessage(SConfirmDeleteCascadeEmpl, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
      Abort
    else
      if DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger <> 0 then
      begin
        try
          SystemDM.Pims.StartTransaction;
          StoredProcDelete.ParamByName('EMPLOYEE').AsInteger :=
            DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          StoredProcDelete.Prepare;
          StoredProcDelete.ExecProc;
          SystemDM.Pims.Commit;
        except on E:EDBEngineError do
          begin
            WErrorLog('Before Delete: ' + E.Message); // TD-26057
            SystemDM.Pims.Rollback;
          end;
        end;
     end;
  end
  else
  begin // RV094.7.
    DisplayMessage(SPimsEmployeeDeleteNotAllowed, mtWarning, [mbOK]);
    Abort;
  end;
end;

function TEmployeeDM.CheckEmptyDeptTeam: Boolean;
var
  SelectStr: String;
begin
  QueryCheckDeptTeam.close;
  QueryCheckDeptTeam.SQL.Clear;
  SelectStr :=
    'SELECT ' +
    '  DEPARTMENT_CODE ' +
    'FROM ' +
    '  DEPARTMENTPERTEAM ' +
    'WHERE ' +
    '  PLANT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('PLANT_CODE').AsString) +
    ''' AND DEPARTMENT_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('DEPARTMENT_CODE').AsString) +
    ''' AND TEAM_CODE = ''' +
    DoubleQuote(TableDetail.FieldByName('TEAM_CODE').AsString) + '''';
  QueryCheckDeptTeam.SQL.Add(SelectStr);
  QueryCheckDeptTeam.Open;
  Result := (QueryCheckDeptTeam.RecordCount = 0);
end;
//REV065.7
procedure TEmployeeDM.UpdateIlnessMessage(AEmplNo : Integer; AEndDate: TDateTime);
begin
  with QueryUpdateIlnessMessage do
  begin
    Close;
    ParamByName('ENDDATE').AsDateTime := AEndDate;
    ParamByName('EMPLNO').AsInteger := AEmplNo;
    ExecSQL;
  end;
end;
//REV065.7
procedure TEmployeeDM.UpdateShiftSchedule(AEmplNo : Integer; AEndDate: TDateTime);
begin
  with QueryUpdateShiftSchedule do
  begin
    Close;
    ParamByName('ENDDATE').AsDateTime := AEndDate;
    ParamByName('EMPLNO').AsInteger := AEmplNo;
    ExecSQL;
  end;
end;

procedure TEmployeeDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  // RV094.7.
  if (SystemDM.UpdateEmployeeYN <> CHECKEDVALUE) then
  begin
    DisplayMessage(SPimsEmployeeUpdateNotAllowed, mtWarning, [mbOK]);
    Abort;
    Exit;
  end;
  SystemDM.DefaultBeforePost(DataSet);
  if (DataSet.FieldByName('ENDDATE').Value <> Null) and
    (Int(DataSet.FieldByName('ENDDATE').Value) <
    Int(DataSet.FieldByName('STARTDATE').Value)) then
  begin
    DisplayMessage(SPimsStartEndDate, mtWarning, [mbOK]);
    Abort;
  end;
   //Date begin
  if DataSet.FieldByName('FIRSTAID_EXP_DATE').Value <> Null then
    DataSet.FieldByName('FIRSTAID_EXP_DATE').Value :=
      Int(DataSet.FieldByName('FIRSTAID_EXP_DATE').Value);
  if DataSet.FieldByName('STARTDATE').Value <> Null then
    DataSet.FieldByName('STARTDATE').Value :=
      Int(DataSet.FieldByName('STARTDATE').Value);
  if DataSet.FieldByName('ENDDATE').Value <> Null then
    DataSet.FieldByName('ENDDATE').Value :=
      Int(DataSet.FieldByName('ENDDATE').Value);
  if DataSet.FieldByName('DATE_OF_BIRTH').Value <> Null then
    DataSet.FieldByName('DATE_OF_BIRTH').Value :=
      Int(DataSet.FieldByName('DATE_OF_BIRTH').Value);
  //RV066.1.
  if Dataset.FieldByName('BOOK_PROD_HRS_YN').AsString = 'Y' then
    Dataset.FieldByName('IS_SCANNING_YN').AsString := 'N';
  if Dataset.FieldByName('IS_SCANNING_YN').AsString = 'Y' then
    Dataset.FieldByName('BOOK_PROD_HRS_YN').AsString := 'N';
  if CheckEmptyDeptTeam then
  begin
    DisplayMessage(SPimsTeamPerEmployee, mtWarning, [mbOK]);
    Abort;
  end;

  if (OldEmpl <> DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger)
    and (SystemDM.UpdateEmployeeYN = CHECKEDVALUE) and
    (OldEmpl <> -1) and (OldEmpl <> 0) then
  begin
    QueryWork.Close;
    QueryWork.ParamByName('EMPLOYEE').AsInteger :=
      EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    QueryWork.Prepare;
    QueryWork.Open;
    if QueryWork.RecordCount = 1 then
    begin
      DisplayMessage(SEmpExist, mtWarning, [mbOK]);
      Abort;
    end;
    if DisplayMessage(SConfirmUpdateCascadeEmpl, mtConfirmation, [mbYes, mbNo])
    <> mrYes then
    begin
      TableDetail.Cancel;
      Abort;
    end
    else
    begin
      try
        SystemDM.Pims.StartTransaction;
        UpdateEmpl;
        SystemDM.Pims.Commit;
      except on E:EDBEngineError do
        begin
          WErrorLog('Before Post: ' + E.Message); // TD-26057
          SystemDM.Pims.Rollback;
        end;
      end;
      OldEmpl := EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      Abort;
    end;
  end;
  OldEmpl := EmployeeDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  LastState := DataSet.State;
end; // TableDetailBeforePost

procedure TEmployeeDM.TableDetailCalcFields(DataSet: TDataSet);
begin
  inherited;
  if (not DataSet.Active) or (not TableDept.Active) or
    (not TableShift.Active) then
    Exit;
  if DataSet.State in [dsInsert, dsEdit] then
    Exit;

(*
  if DataSet.FieldByName('DEPARTMENT_CODE').AsString <> '' then
  //car 550279 replace TableDept with cdsDepartment
    if cdsDepartment.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('DEPARTMENT_CODE').AsString]) then
      DataSet.FieldByName('DEPTDESC').Value :=
        cdsDepartment.FieldByName('DESCRIPTION').AsString;
*)
  DataSet.FieldByName('FLOAT_EMP').AsFLOAT :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  // MR:27-04-2005
  // Lookup dept and shift, because they are linked by 2 keys,
  // they otherwise are wrong displayed in grid.
  if DataSet.FieldByName('DEPARTMENT_CODE').AsString <> '' then
    if TableDept.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('DEPARTMENT_CODE').AsString]) then
      DataSet.FieldByName('DEPTDESC').AsString :=
        TableDept.FieldByName('DESCRIPTION').AsString;
  if DataSet.FieldByName('SHIFT_NUMBER').AsString <> '' then
    if TableShift.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('SHIFT_NUMBER').AsInteger]) then
      DataSet.FieldByName('SHIFTDESC').AsString :=
        TableShift.FieldByName('DESCRIPTION').AsString;
  TableDetailDEPTLU.Value := TableDetailDEPTDESC.Value;
  TableDetailSHIFTLU.Value := TableDetailSHIFTDESC.Value;
end;

procedure TEmployeeDM.TableDetailNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  DataSet.FieldByName('FIRSTAID_YN').Value := UNCHECKEDVALUE;
  DataSet.FieldByName('IS_SCANNING_YN').Value := CHECKEDVALUE;
  DataSet.FieldByName('CUT_OF_TIME_YN').Value := CHECKEDVALUE;
  DataSet.FieldByName('CALC_BONUS_YN').Value := UNCHECKEDVALUE;
  DataSet.FieldByName('STARTDATE').Value := Now;
  if not TableContractGroup.IsEmpty then
  begin
    TableContractGroup.First;
    DataSet.FieldByName('CONTRACTGROUP_CODE').Value :=
      TableContractGroup.FieldByName('CONTRACTGROUP_CODE').Value;
  end;
  if not TableLanguage.IsEmpty then
  begin
    TableLanguage.First;
    DataSet.FieldByName('LANGUAGE_CODE').Value :=
      TableLanguage.FieldByName('LANGUAGE_CODE').Value;
  end;
  if not TableLanguage.IsEmpty then
  begin
    TableLanguage.First;
    DataSet.FieldByName('LANGUAGE_CODE').Value :=
      TableLanguage.FieldByName('LANGUAGE_CODE').Value;
  end;
  if not TablePlant.IsEmpty then
  begin
    TablePlant.First;
    DataSet.FieldByName('PLANT_CODE').Value :=
      TablePlant.FieldByName('PLANT_CODE').Value;
  end;
  // PIM-248 Check for first team by plant!
  if not QueryTeam.IsEmpty then
  begin
    // PIM-248 Filter on Plant!
    QueryTeam.Filtered := False;
    QueryTeam.Filter := 'PLANT_CODE = ' +
      TablePlant.FieldByName('PLANT_CODE').AsString;
    QueryTeam.Filtered := True;
    QueryTeam.First;
    DataSet.FieldByName('TEAM_CODE').Value :=
      QueryTeam.FieldByName('TEAM_CODE').Value;
    QueryTeam.Filtered := False;
  end;
  // PIM-248 Check for first department by team!
  qryDeptPerTeam.Close;
  qryDeptPerTeam.ParamByName('PLANT_CODE').AsString :=
    TablePlant.FieldByName('PLANT_CODE').AsString;
  qryDeptPerTeam.ParamByName('TEAM_CODE').AsString :=
    QueryTeam.FieldByName('TEAM_CODE').AsString;
  qryDeptPerTeam.Open;
  if not qryDeptPerTeam.Eof then
  begin
    if TableDept.Locate('PLANT_CODE;DEPARTMENT_CODE',
      VarArrayOf([TablePlant.FieldByName('PLANT_CODE').AsString,
      qryDeptPerTeam.FieldByName('DEPARTMENT_CODE').AsString]), []) then
      DataSet.FieldByName('DEPARTMENT_CODE').Value :=
        TableDept.FieldByName('DEPARTMENT_CODE').Value
    else
      if not TableDept.IsEmpty then
      begin
        TableDept.First;
        DataSet.FieldByName('DEPARTMENT_CODE').Value :=
          TableDept.FieldByName('DEPARTMENT_CODE').Value;
      end;
  end
  else
    //car 550279
    if not TableDept.IsEmpty then
    begin
      TableDept.First;
      DataSet.FieldByName('DEPARTMENT_CODE').Value :=
        TableDept.FieldByName('DEPARTMENT_CODE').Value;
    end;
  // RV050.8.
  if not TableShift.IsEmpty then
  begin
    TableShift.First;
    DataSet.FieldByName('SHIFT_NUMBER').Value :=
      TableShift.FieldByName('SHIFT_NUMBER').Value;
  end;
  OldEmpl := -1;
  LastState := DataSet.State;
end;

procedure TEmployeeDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  LastState := dsBrowse;
  // PIM-266 QueryTeam has now a fixed SQL in component
  QueryTeam.Close;
  QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  QueryTeam.Prepare;

   //car 550279
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    TableTeamUser.Filtered := True;
    TableTeamUser.Filter := 'USER_NAME = ''' + SystemDM.UserTeamLoginUser + '''';
    cdsFilterTeamUser.Open;
    // PIM-266 See above
(*
    QueryTeam.SQL.Clear;
    QueryTeam.SQL.Add(
      'SELECT ' +
      '  T.PLANT_CODE, T.TEAM_CODE, T.DESCRIPTION ' +
      'FROM ' +
      '  TEAM T, TEAMPERUSER TU ' +
      'WHERE ' +
      '  TU.USER_NAME = :USER_NAME AND ' +
      '  T.TEAM_CODE = TU.TEAM_CODE ' +
      'ORDER BY ' +
      '  T.TEAM_CODE ');
    QueryTeam.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryTeam.Prepare;
*)
    // RV085.9. Distinct added: Otherwise you can have multiple the same depts.
    QueryDept.SQL.Clear;
    QueryDept.SQL.Add(
      'SELECT ' +
      '  DISTINCT D.PLANT_CODE, D.DEPARTMENT_CODE, D.DESCRIPTION ' +
      'FROM ' +
      '  DEPARTMENT D, TEAMPERUSER T, DEPARTMENTPERTEAM DT ' +
      'WHERE ' +
      '  T.USER_NAME = :USER_NAME AND ' +
      '  DT.PLANT_CODE = D.PLANT_CODE AND ' +
      '  DT.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND ' +
      '  T.TEAM_CODE = DT.TEAM_CODE ' +
      'ORDER BY ' +
      '  D.PLANT_CODE, D.DEPARTMENT_CODE ');
    QueryDept.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryDept.Prepare;
    // PIM-266 Related to this: This is not used!
(*
    QueryDeptEmpl.SQL.Clear;
    QueryDeptEmpl.SQL.Add(
      'SELECT ' +
      '  D.DEPARTMENT_CODE, D.DESCRIPTION ' +
      'FROM ' +
      '  DEPARTMENT D, TEAMPERUSER T, DEPARTMENTPERTEAM DT ' +
      'WHERE ' +
      '  D.PLANT_CODE = :PLANT_CODE AND ' +
      '  T.USER_NAME = :USER_NAME AND ' +
      '  DT.PLANT_CODE = D.PLANT_CODE AND ' +
      '  DT.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND ' +
      '  T.TEAM_CODE = DT.TEAM_CODE ' +
      'ORDER BY ' +
      '  D.DEPARTMENT_CODE ');
    QueryDeptEmpl.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryDeptEmpl.Prepare;
*)
  end;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    TableDetail.OnFilterRecord := FilterRecord;
    TableDetail.Filtered := True;
  end;
  QueryShift.Open;
  QueryTeam.Open;
//  QueryDeptEmpl.Open; // PIM-266
//  TableDept.Open;
  QueryDept.Open;
  TableShift.Open;
  QueryShift.Open;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(TablePlant);
  TablePlant.Open;
  SystemDM.PlantTeamFilterEnable(TableDept);
  TableDept.Open;
end;

//car 550279
procedure TEmployeeDM.FilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  Accept := cdsFilterTeamUser.FindKey([DataSet.
    FieldByName('TEAM_CODE').AsString]);
end;

procedure TEmployeeDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //car 550279
  QueryTeam.Close;
  QueryTeam.UnPrepare;
  QueryDept.Close;
  QueryDept.UnPrepare;
  TableDept.Close;
  QueryDept.Close;
  QueryDept.Unprepare;
  TableShift.Close;
  QueryShift.Close;
  QueryShift.UnPrepare;
  TablePlant.Close;
end;

// RV050.8.
procedure TEmployeeDM.TablePlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

// RV050.8.
procedure TEmployeeDM.TableDeptFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TEmployeeDM.TableDetailAfterPost(DataSet: TDataSet);
var
  EmpID: Integer;
begin
  inherited;
  //REV065.7
  if TableDetail.FieldByName('ENDDATE').Value <> Null then
  begin
    UpdateIlnessMessage(TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      TableDetail.FieldByName('ENDDATE').AsDateTime);
    UpdateShiftSchedule(TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      TableDetail.FieldByName('ENDDATE').AsDateTime);
    // RV067.MRA.24 Additional changes for 550482.
    DeleteOnEndDate(TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      TableDetail.FieldByName('ENDDATE').AsDateTime);
    UpdateEndDateEmplContract(TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      TableDetail.FieldByName('ENDDATE').AsDateTime);
  end;
  // PIM-227 This can be needed to prevent error about 'record/key deleted'.
  EmpID := 0;
  try
    EmpID := DataSet.FieldByName('EMPLOYEE_ID').AsInteger;
    TableDetail.Refresh;
    TableDetail.Locate('EMPLOYEE_ID', EmpID, []);
  except
    on E: EDBEngineError do
      WErrorLog('Employee-AfterPost: ' + E.Message);
  end;
  // PIM-172
  if LastState {DataSet.State} in [dsInsert] then
    if TableDetail.FieldByName('ENDDATE').Value = Null then
    begin
      try
        DialogCopyEmpDataF := TDialogCopyEmpDataF.Create(Self);
        DialogCopyEmpDataF.PlantCode := DataSet.FieldByName('PLANT_CODE').AsString;
        DialogCopyEmpDataF.EmployeeNumber := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        DialogCopyEmpDataF.StartDate := DataSet.FieldByName('STARTDATE').AsDateTime;
        if DialogCopyEmpDataF.ShowModal = mrOK then
        begin
          // Employee has been changed, refresh it here
          TableDetail.Refresh;
          TableDetail.Locate('EMPLOYEE_ID', EmpID, []);
        end;
      finally
        DialogCopyEmpDataF.Free;
      end;
    end;
  LastState := dsBrowse;
end; // TableDetailAfterPost

// RV089.2.JVL order 5505525.
procedure TEmployeeDM.UpdateEndDateEmplContract(AEmplNo: Integer; AEndDate: TDateTime);
begin
  with QueryEndDateEmplContract do
  begin
    Close;
    ParamByName('EMPLOYEENUMBER').AsInteger := AEmplNo;
    ParamByName('ENDDATE').AsDateTime := AEndDate;
    ParamByName('NODATE').AsDateTime := EncodeDate(2099,12,31);
    ExecSQL;
  end;
end;

// RV067.MRA.24 Additional changes for 550482.
procedure TEmployeeDM.DeleteOnEndDate(AEmplNo: Integer; AEndDate: TDateTime);
begin
  with qryDeleteStandAvail do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmplNo;
    ExecSQL;
  end;
  with qryDeleteEmpAvail do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmplNo;
    ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime := AEndDate;
    ExecSQL;
  end;
  with qryDeleteStandEmpPlan do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmplNo;
    ExecSQL;
  end;
  // PIM-219
  with qryDeleteEmpPlan do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmplNo;
    ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime := AEndDate;
    ExecSQL;
  end;
end;

end.
