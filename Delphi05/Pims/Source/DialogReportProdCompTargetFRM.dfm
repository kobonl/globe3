inherited DialogReportProdCompTargetF: TDialogReportProdCompTargetF
  Left = 262
  Top = 108
  Caption = 'Report productivity compared to target'
  ClientHeight = 468
  ClientWidth = 574
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 574
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 278
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 574
    Height = 347
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 104
      Top = 346
      Visible = False
    end
    inherited LblEmployee: TLabel
      Left = 88
      Top = 392
      Visible = False
    end
    inherited LblToPlant: TLabel
      Top = 18
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Left = 355
      Top = 417
      Visible = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 112
      Top = 348
      Visible = False
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 328
      Top = 380
      Visible = False
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 336
      Top = 46
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [15]
      Left = 8
      Top = 458
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [16]
      Left = 315
      Top = 71
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [17]
      Left = 128
      Top = 433
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [18]
      Left = 336
      Top = 433
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [19]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [20]
      Left = 128
      Top = 459
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [21]
      Left = 336
      Top = 456
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [22]
      Left = 40
      Top = 458
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [23]
      Left = 315
      Top = 459
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [24]
      Left = 8
      Top = 182
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [25]
      Left = 40
      Top = 182
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [26]
      Left = 315
      Top = 182
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel [27]
      Left = 8
      Top = 510
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [28]
      Left = 40
      Top = 510
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [29]
      Left = 307
      Top = 512
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [30]
      Left = 128
      Top = 512
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [31]
      Left = 344
      Top = 512
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [32]
      Left = 40
      Top = 434
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel [33]
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label27: TLabel [34]
      Left = 315
      Top = 432
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label28: TLabel [35]
      Left = 8
      Top = 434
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    inherited LblFromDepartment: TLabel
      Top = 69
    end
    inherited LblDepartment: TLabel
      Top = 69
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 72
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 71
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 156
    end
    inherited LblToTeam: TLabel
      Top = 156
    end
    inherited LblStarTeamFrom: TLabel
      Top = 156
    end
    inherited LblTeam: TLabel
      Top = 154
    end
    inherited LblFromTeam: TLabel
      Top = 154
    end
    inherited LblFromShift: TLabel
      Top = 588
    end
    inherited LblShift: TLabel
      Top = 588
    end
    inherited LblStartShiftFrom: TLabel
      Top = 590
    end
    inherited LblToShift: TLabel
      Top = 590
    end
    inherited LblStarShiftTo: TLabel
      Top = 590
    end
    inherited LblFromPlant2: TLabel
      Left = 24
      Top = 551
    end
    inherited LblPlant2: TLabel
      Left = 56
      Top = 551
    end
    inherited LblStarPlant2From: TLabel
      Left = 144
      Top = 553
    end
    inherited LblToPlant2: TLabel
      Left = 331
      Top = 551
    end
    inherited LblStarPlant2To: TLabel
      Left = 360
      Top = 553
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    inherited LblWorkspots: TLabel
      Top = 98
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 520
      Top = 94
    end
    inherited LblStarJobTo: TLabel
      Top = 126
    end
    inherited LblToJob: TLabel
      Top = 126
    end
    inherited LblStarJobFrom: TLabel
      Top = 126
    end
    inherited LblJob: TLabel
      Top = 126
    end
    inherited LblFromJob: TLabel
      Top = 126
    end
    inherited LblFromDateTime: TLabel
      Top = 182
    end
    inherited LblDateTime: TLabel
      Top = 182
    end
    inherited LblToDateTime: TLabel
      Top = 182
    end
    object DateTo: TDateTimePicker [73]
      Left = 334
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 16
    end
    object DateFrom: TDateTimePicker [74]
      Left = 120
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 15
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 136
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 137
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 153
      ColCount = 146
      TabOrder = 12
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 153
      ColCount = 147
      TabOrder = 13
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 156
      TabOrder = 14
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 68
      ColCount = 146
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 68
      ColCount = 147
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 71
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 136
      Top = 386
      TabOrder = 25
      Visible = False
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 350
      Top = 386
      TabOrder = 31
      Visible = False
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Top = 587
      ColCount = 152
      TabOrder = 30
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Top = 587
      ColCount = 153
      TabOrder = 32
    end
    inherited CheckBoxAllShifts: TCheckBox
      Top = 587
      TabOrder = 33
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 35
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 490
      Top = 514
      TabOrder = 26
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 514
      Top = 491
      TabOrder = 27
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Left = 136
      Top = 549
      ColCount = 167
      TabOrder = 28
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 358
      Top = 549
      ColCount = 168
      TabOrder = 29
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 97
      ColCount = 168
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 97
      ColCount = 169
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      Top = 97
      Width = 393
      TabOrder = 9
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 126
      ColCount = 169
      TabOrder = 11
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 126
      ColCount = 168
      TabOrder = 10
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 180
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Left = 450
      Top = 180
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 180
    end
    inherited DatePickerTo: TDateTimePicker
      Left = 334
      Top = 180
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 137
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 138
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 136
      Top = 457
      Width = 180
      Height = 19
      ColCount = 142
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 23
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 350
      Top = 457
      Width = 180
      Height = 19
      ColCount = 143
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 24
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object GroupBoxSelection: TGroupBox
      Left = 160
      Top = 208
      Width = 353
      Height = 121
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 22
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 20
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 40
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 8
        Top = 80
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 3
      end
      object CheckBoxPageWK: TCheckBox
        Left = 8
        Top = 100
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 4
      end
      object CheckBoxPageBU: TCheckBox
        Left = 8
        Top = 60
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 2
      end
      object RadioGroupIncludeDowntime: TRadioGroup
        Left = 184
        Top = 14
        Width = 161
        Height = 99
        Caption = 'Include down-time quantities'
        ItemIndex = 0
        Items.Strings = (
          'No'
          'Yes'
          'Only show down-time')
        TabOrder = 5
      end
    end
    object RadioGroupNorm: TRadioGroup
      Left = 8
      Top = 208
      Width = 145
      Height = 121
      Caption = 'Based on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Norm production level'
        'Norm machine output')
      ParentFont = False
      TabOrder = 21
    end
    object ProgressBar: TProgressBar
      Left = 8
      Top = 332
      Width = 505
      Height = 12
      Min = 0
      Max = 100
      TabOrder = 34
      Visible = False
    end
  end
  inherited stbarBase: TStatusBar
    Top = 408
    Width = 574
  end
  inherited pnlBottom: TPanel
    Top = 427
    Width = 574
    inherited btnOk: TBitBtn
      Left = 168
    end
    inherited btnCancel: TBitBtn
      Left = 298
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 200
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 232
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 576
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        44040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507244469616C6F675265706F727442617365462E4461746153
        6F75726365456D706C46726F6D104F7074696F6E73437573746F6D697A650B0E
        6564676F42616E644D6F76696E670E6564676F42616E6453697A696E67106564
        676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E
        6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F43
        616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F4361
        6E496E73657274116564676F43616E4E617669676174696F6E116564676F436F
        6E6669726D44656C657465126564676F4C6F6164416C6C5265636F7264731065
        64676F557365426F6F6B6D61726B7300000F546478444247726964436F6C756D
        6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D6265720653
        6F7274656407046373557005576964746802410942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E
        554D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E5368
        6F72744E616D650743617074696F6E060A53686F7274206E616D650557696474
        6802540942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060A53484F52545F4E414D4500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D650557696474
        6803B4000942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060B4445534352495054494F4E00000F546478444247726964436F
        6C756D6E0D436F6C756D6E416464726573730743617074696F6E060741646472
        65737305576964746802450942616E64496E646578020008526F77496E646578
        0200094669656C644E616D6506074144445245535300000F5464784442477269
        64436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F
        4465706172746D656E7420636F646505576964746802580942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060F444550415254
        4D454E545F434F444500000F546478444247726964436F6C756D6E0A436F6C75
        6D6E5465616D0743617074696F6E06095465616D20636F64650942616E64496E
        646578020008526F77496E6465780200094669656C644E616D6506095445414D
        5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        0B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507224469616C6F675265706F72
        7442617365462E44617461536F75726365456D706C546F104F7074696F6E7343
        7573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F42616E
        6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C
        756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074696F6E
        7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44656C
        6574650D6564676F43616E496E73657274116564676F43616E4E617669676174
        696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164416C
        6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54647844
        4247726964436F6C756D6E0A436F6C756D6E456D706C0743617074696F6E0606
        4E756D62657206536F7274656407046373557005576964746802310942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060F454D
        504C4F5945455F4E554D42455200000F546478444247726964436F6C756D6E0F
        436F6C756D6E53686F72744E616D650743617074696F6E060A53686F7274206E
        616D65055769647468024E0942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060A53484F52545F4E414D4500000F5464784442
        47726964436F6C756D6E11436F6C756D6E4465736372697074696F6E07436170
        74696F6E06044E616D6505576964746803CC000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E06074164647265737305576964746802650942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        5245535300000F546478444247726964436F6C756D6E0E436F6C756D6E446570
        74436F64650743617074696F6E060F4465706172746D656E7420636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        0F4445504152544D454E545F434F444500000F546478444247726964436F6C75
        6D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F6465
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 404
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 88
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    DataSet = qryWorkspot
    Left = 496
    Top = 23
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceWorkSpot
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE'
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 352
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
end
