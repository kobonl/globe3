inherited ReportProdCompTargetDM: TReportProdCompTargetDM
  OldCreateOrder = True
  Left = 212
  Top = 149
  Height = 581
  Width = 741
  object QueryProductionComp: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProductionCompFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION AS BUSINESSUNITDESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  PQ.JOB_CODE,'
      '  PQ.QUANTITY AS PIECES'
      'FROM '
      '  PRODUCTIONQUANTITY PQ,'
      '  PLANT P,'
      '  JOBCODE J,'
      '  WORKSPOT W,'
      '  BUSINESSUNIT B,'
      '  DEPARTMENT D,'
      '  PRODHOURPEREMPLOYEE PE'
      'WHERE'
      '  PQ.PLANT_CODE >= :PLANTFROM AND'
      '  PQ.PLANT_CODE <= :PLANTTO AND'
      '  B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND'
      '  B.BUSINESSUNIT_CODE <= :BUSINESSUNITTO AND'
      '  D.DEPARTMENT_CODE >= :DEPARTMENTFROM AND'
      '  D.DEPARTMENT_CODE <= :DEPARTMENTTO AND'
      '  PQ.WORKSPOT_CODE >= :WORKSPOTFROM AND'
      '  PQ.WORKSPOT_CODE <= :WORKSPOTTO AND'
      '  PQ.START_DATE >= :DATEFROM AND'
      '  PQ.START_DATE <= :DATETO AND'
      '  PQ.PLANT_CODE = J.PLANT_CODE AND'
      '  PQ.PLANT_CODE = P.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = J.JOB_CODE AND'
      '  J.PLANT_CODE = B.PLANT_CODE AND'
      '  J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE AND'
      '  PQ.PLANT_CODE = W.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = W.WORKSPOT_CODE AND'
      '  J.BUSINESSUNIT_CODE = D.BUSINESSUNIT_CODE AND'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  PQ.PLANT_CODE = PE.PLANT_CODE AND'
      '  PQ.SHIFT_NUMBER = PE.SHIFT_NUMBER AND'
      '  PQ.WORKSPOT_CODE = PE.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = PE.JOB_CODE'
      'ORDER BY'
      '  P.PLANT_CODE, P.DESCRIPTION,'
      '  B.BUSINESSUNIT_CODE, B.DESCRIPTION,'
      '  D.DEPARTMENT_CODE, D.DESCRIPTION,'
      '  W.WORKSPOT_CODE, W.DESCRIPTION')
    Left = 72
    Top = 24
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProduction: TDataSource
    DataSet = QueryProductionComp
    Left = 72
    Top = 296
  end
  object QueryPQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PQ.PLANT_CODE,'
      '  J.BUSINESSUNIT_CODE,'
      '  PQ.WORKSPOT_CODE,'
      '  PQ.START_DATE,'
      '  PQ.END_DATE,'
      '  PQ.JOB_CODE,'
      '  PQ.QUANTITY'
      'FROM'
      '  PRODUCTIONQUANTITY PQ, JOBCODE J'
      'WHERE'
      '  PQ.QUANTITY <> 0  AND'
      '  PQ.PLANT_CODE >= :PLANTFROM AND  PQ.PLANT_CODE <= :PLANTTO AND'
      ' (PQ.END_DATE > :FSTARTDATE AND PQ.START_DATE < :FENDDATE)  AND'
      ' (PQ.PLANT_CODE = J.PLANT_CODE AND'
      '  PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND'
      '  PQ.JOB_CODE = J.JOB_CODE)'
      'ORDER BY'
      '  PQ.PLANT_CODE, J.BUSINESSUNIT_CODE,'
      '  PQ.WORKSPOT_CODE, PQ.START_DATE, PQ.QUANTITY,'
      '  PQ.JOB_CODE'
      ' ')
    Left = 72
    Top = 96
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryProdHourPerEmployee: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceProduction
    SQL.Strings = (
      'SELECT'
      '  PRODHOUREMPLOYEE_DATE,'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  EMPLOYEE_NUMBER,'
      '  WORKSPOT_CODE,'
      '  JOB_CODE,'
      '  MANUAL_YN,'
      '  PRODUCTION_MINUTE,'
      '  PAYED_BREAK_MINUTE'
      'FROM'
      '  PRODHOURPEREMPLOYEE'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  WORKSPOT_CODE = :WORKSPOT_CODE AND'
      '  JOB_CODE = :JOB_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER AND'
      '  PRODHOUREMPLOYEE_DATE >= :DATEFROM AND'
      '  PRODHOUREMPLOYEE_DATE <= :DATETO AND'
      '  MANUAL_YN = '#39'N'#39)
    Left = 72
    Top = 168
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetMachineHrs: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetMachineHrsIDX'
        Fields = 'PLANT_CODE;BUSINESSUNIT_CODE;WORKSPOT_CODE;JOB_CODE'
      end>
    IndexName = 'ClientDataSetMachineHrsIDX'
    Params = <>
    StoreDefs = True
    Left = 72
    Top = 232
    object ClientDataSetMachineHrsPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object ClientDataSetMachineHrsWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object ClientDataSetMachineHrsJOB_CODE: TStringField
      FieldName = 'JOB_CODE'
      Size = 6
    end
    object ClientDataSetMachineHrsBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Size = 6
    end
    object ClientDataSetMachineHrsMACHINE_HOURS: TFloatField
      FieldName = 'MACHINE_HOURS'
    end
  end
  object qryMachineHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      ' ')
    Left = 72
    Top = 352
  end
end
