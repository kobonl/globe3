(*
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
*)
unit DialogProcessWorkScheduleDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseDMT, SystemDMT, Db, DBTables;

type
  TDialogProcessWorkScheduleDM = class(TDialogBaseDM)
    Query1: TQuery;
    qryWorkScheduleDetails: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogProcessWorkScheduleDM: TDialogProcessWorkScheduleDM;

implementation

{$R *.DFM}

{ TDialogProcessWorkScheduleDM }

end.
