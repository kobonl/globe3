inherited ReportRatioIllnessHrsDM: TReportRatioIllnessHrsDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryProdMin: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryProdMinFilterRecord
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE,  P.EMPLOYEE_NUMBER, '
      '  D.DIRECT_HOUR_YN,'
      '  SUM(P.PRODUCTION_MINUTE) AS PRODMIN'
      'FROM '
      '  PRODHOURPEREMPLOYEE P, WORKSPOT W , '
      '  DEPARTMENT D '
      'WHERE'
      '  P.PLANT_CODE = :PLANT_CODE AND'
      '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      ' P.PRODHOUREMPLOYEE_DATE <= :FENDDATE  '
      'GROUP BY '
      '  P.PLANT_CODE,'
      '  P.EMPLOYEE_NUMBER, D.DIRECT_HOUR_YN')
    Left = 64
    Top = 120
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryAbsence: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT   '
      '  A.PLANT_CODE, '
      '  A.EMPLOYEE_NUMBER, '
      '  D.DIRECT_HOUR_YN,'
      '  SUM(A.ABSENCE_MINUTE) AS ABSMIN '
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A , '
      '  DEPARTMENT D '
      'WHERE  '
      '  A.ABSENCEHOUR_DATE >= :FSTARTDATE AND'
      '  A.ABSENCEHOUR_DATE <= :FENDDATE '
      'GROUP BY   '
      '  A.PLANT_CODE, '
      '  A.EMPLOYEE_NUMBER, '
      '  D.DIRECT_HOUR_YN'
      '')
    Left = 152
    Top = 120
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object dspEmpl: TDataSetProvider
    DataSet = QueryEmpl
    Constraints = True
    Left = 152
    Top = 248
  end
  object cdsEmpl: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspEmpl'
    Left = 64
    Top = 248
  end
  object QueryEmpl: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.PLANT_CODE, P.DESCRIPTION, E.EMPLOYEE_NUMBER, '
      '  E.DESCRIPTION, D.DIRECT_HOUR_YN'
      'FROM '
      '  PLANT P, EMPLOYEE E, DEPARTMENT D '
      'WHERE'
      '  P.PLANT_CODE = E.PLANT_CODE AND'
      '  D.PLANT_CODE = E.PLANT_CODE AND '
      '  D.DEPARTMENT_CODE = E.DEPARTMENT_CODE'
      'ORDER BY '
      '  E.PLANT_CODE, E.EMPLOYEE_NUMBER'
      '')
    Left = 240
    Top = 248
  end
  object cdsEmplContract: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'byPrimary'
        Fields = 'EMPLOYEE_NUMBER;STARTDATE'
      end>
    IndexName = 'byPrimary'
    Params = <>
    ProviderName = 'dspEmplContract'
    StoreDefs = True
    Left = 64
    Top = 312
  end
  object dspEmplContract: TDataSetProvider
    DataSet = qryEmplContract
    Constraints = True
    Left = 152
    Top = 312
  end
  object qryEmplContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, STARTDATE, '
      '  ENDDATE, CONTRACT_HOUR_PER_WEEK'
      'FROM '
      '  EMPLOYEECONTRACT'
      'ORDER BY '
      '  EMPLOYEE_NUMBER, STARTDATE')
    Left = 240
    Top = 312
  end
end
