(*
  MRA:29-FEB-2016 PIM-142
  - Addition of Real Time Efficiency Monitor-dialog
  MRA:11-APR-2016 PIM-142 Rework
  - Sometimes it gave an error, cause: Some events tried to
    change some fields. Solved by removing the events for TableMaster
    and TableDetail because here we have only read-only data.
*)
unit RealTimeEffMonitorDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, GridBaseDMT, Db, DBTables;

type
  TDataFilter = record
    PlantCode: String;
    ShiftDate: TDateTime;
  end;

type
  TRealTimeEffMonitorDM = class(TGridBaseDM)
    dsWorkspotEffPerMin: TDataSource;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    qryWorkspotEffPerMin: TQuery;
    qryWorkspotEffPerMinWORKSPOTEFFPERMINUTE_ID: TFloatField;
    qryWorkspotEffPerMinSHIFT_DATE: TDateTimeField;
    qryWorkspotEffPerMinSHIFT_NUMBER: TFloatField;
    qryWorkspotEffPerMinPLANT_CODE: TStringField;
    qryWorkspotEffPerMinWORKSPOT_CODE: TStringField;
    qryWorkspotEffPerMinJOB_CODE: TStringField;
    qryWorkspotEffPerMinINTERVALSTARTTIME: TDateTimeField;
    qryWorkspotEffPerMinINTERVALENDTIME: TDateTimeField;
    qryWorkspotEffPerMinWORKSPOTSECONDSACTUAL: TFloatField;
    qryWorkspotEffPerMinWORKSPOTSECONDSNORM: TFloatField;
    qryWorkspotEffPerMinWORKSPOTQUANTITY: TFloatField;
    qryWorkspotEffPerMinNORMLEVEL: TFloatField;
    qryEmployeeEffPerMin: TQuery;
    dsEmployeeEffPerMin: TDataSource;
    qryEmployeeEffPerMinEMPLOYEEEFFPERMINUTE_ID: TFloatField;
    qryEmployeeEffPerMinSHIFT_DATE: TDateTimeField;
    qryEmployeeEffPerMinSHIFT_NUMBER: TFloatField;
    qryEmployeeEffPerMinPLANT_CODE: TStringField;
    qryEmployeeEffPerMinWORKSPOT_CODE: TStringField;
    qryEmployeeEffPerMinJOB_CODE: TStringField;
    qryEmployeeEffPerMinINTERVALSTARTTIME: TDateTimeField;
    qryEmployeeEffPerMinINTERVALENDTIME: TDateTimeField;
    qryEmployeeEffPerMinEMPLOYEESECONDSACTUAL: TFloatField;
    qryEmployeeEffPerMinEMPLOYEESECONDSNORM: TFloatField;
    qryEmployeeEffPerMinEMPLOYEEQUANTITY: TFloatField;
    qryEmployeeEffPerMinNORMLEVEL: TFloatField;
    qryEmployeeEffPerShift: TQuery;
    dsEmployeeEffPerShift: TDataSource;
    qryEmployeeEffPerMinEMPLOYEE_NUMBER: TFloatField;
    qryEmployeeEffPerShiftEMPLOYEEEFFPERSHIFT_ID: TFloatField;
    qryEmployeeEffPerShiftSHIFT_DATE: TDateTimeField;
    qryEmployeeEffPerShiftSHIFT_NUMBER: TFloatField;
    qryEmployeeEffPerShiftPLANT_CODE: TStringField;
    qryEmployeeEffPerShiftWORKSPOT_CODE: TStringField;
    qryEmployeeEffPerShiftJOB_CODE: TStringField;
    qryEmployeeEffPerShiftEMPLOYEE_NUMBER: TFloatField;
    qryEmployeeEffPerShiftEMPLOYEESECONDSACTUAL: TFloatField;
    qryEmployeeEffPerShiftEMPLOYEESECONDSNORM: TFloatField;
    qryEmployeeEffPerShiftEMPLOYEEQUANTITY: TFloatField;
    qryEmployeeEffPerShiftNORMLEVEL: TFloatField;
    qryWorkspotEffPerMinSTART_TIMESTAMP: TDateTimeField;
    qryWorkspotEffPerMinEND_TIMESTAMP: TDateTimeField;
  private
    { Private declarations }
    FDataFilter: TDataFilter;
    procedure SetDataFilter(const Value: TDataFilter);
  public
    { Public declarations }
    property DataFilter: TDataFilter read FDataFilter write SetDataFilter;
  end;

var
  RealTimeEffMonitorDM: TRealTimeEffMonitorDM;

implementation

{$R *.DFM}

{ TRealTimeEffMonitorDM }

procedure TRealTimeEffMonitorDM.SetDataFilter(const Value: TDataFilter);
begin
  FDataFilter := Value;
end;

end.
