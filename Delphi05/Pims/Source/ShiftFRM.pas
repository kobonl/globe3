unit ShiftFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, dxDBEdtr, CalculateTotalHoursDMT;

type
  TShiftF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    GroupBoxDetail: TGroupBox;
    LabelUnit: TLabel;
    LabelShift: TLabel;
    DBEditProcessUnit: TDBEdit;
    DBEditShift: TDBEdit;
    DBEditDescription: TDBEdit;
    DBEdit1: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    dxDBTimeEdit1: TdxDBTimeEdit;
    dxDBTimeEdit2: TdxDBTimeEdit;
    dxDBTimeEdit3: TdxDBTimeEdit;
    dxDBTimeEdit4: TdxDBTimeEdit;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    LabelHourType: TLabel;
    dxDBLookupEdit2: TdxDBLookupEdit;
    dxDetailGridColumn17: TdxDBGridLookupColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function ShiftF: TShiftF;

implementation

{$R *.DFM}

uses
  SystemDMT, ShiftDMT;

var
  ShiftF_HND: TShiftF;

function ShiftF: TShiftF;
begin
  if (ShiftF_HND = nil) then
    ShiftF_HND := TShiftF.Create(Application);
  Result := ShiftF_HND;
end;

procedure TShiftF.FormCreate(Sender: TObject);
begin
  ShiftDM := CreateFormDM(TShiftDM);
  if (dxDetailGrid.DataSource = nil) or (dxMasterGrid.DataSource = nil) then
  begin
    dxMasterGrid.DataSource := ShiftDM.DataSourceMaster;
    dxDetailGrid.DataSource := ShiftDM.DataSourceDetail;
  end;
  inherited;
end;

procedure TShiftF.FormDestroy(Sender: TObject);
begin
  inherited;
  ShiftF_HND := nil;
end;

procedure TShiftF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditShift.SetFocus;
end;

procedure TShiftF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  for Index := 1 to 7 do
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
end;

procedure TShiftF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditShift.SetFocus;
end;

procedure TShiftF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
end;

end.
