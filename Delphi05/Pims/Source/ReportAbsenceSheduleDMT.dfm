inherited ReportAbsenceSheduleDM: TReportAbsenceSheduleDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryAbsence: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryAbsenceFilterRecord
    SQL.Strings = (
      'SELECT '
      '  R.ABSENCETYPE_CODE, A.EMPLOYEE_NUMBER,  '
      '  A.ABSENCEHOUR_DATE,  SUM(A.ABSENCE_MINUTE) AS SUMMIN '
      'FROM '
      '  ABSENCEHOURPEREMPLOYEE A,  EMPLOYEE E, '
      '  ABSENCEREASON R  '
      'WHERE'
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  A.ABSENCEHOUR_DATE >= :FDATEFROM AND '
      '  A.ABSENCEHOUR_DATE <= :FDATETO AND '
      '  E.STARTDATE >= :FDATECURRENT  AND '
      '  E.ENDDATE <= :FDATECURRENT '
      'GROUP BY '
      '  R.ABSENCETYPE_CODE, A.EMPLOYEE_NUMBER,'
      '  A.ABSENCEHOUR_DATE')
    Left = 56
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end>
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryAbsence
    Left = 200
    Top = 16
  end
  object TableEarnedWTR: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EARNED_WTR_YEAR;EARNED_WTR_WEEK;EMPLOYEE_NUMBER'
    TableName = 'EARNEDWTRPERWEEK'
    Left = 56
    Top = 80
  end
  object TableAbsenceType: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCETYPE_CODE'
    MasterFields = 'ABSENCETYPE_CODE'
    MasterSource = DataSourceAbsence
    TableName = 'ABSENCETYPE'
    Left = 200
    Top = 88
  end
  object TableEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    MasterFields = 'EMPLOYEE_NUMBER'
    MasterSource = DataSourceAbsence
    TableName = 'EMPLOYEE'
    Left = 56
    Top = 136
  end
end
