(*
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  SO: 05-JUL-2010 RV067.3. 550494
    PIMS User rights related issues
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
  MRA:19-JUL-2011 RV095.1.
  - Workspot moved to DialogReportBase-form.
  MRA:23-JUL-2012 20012858.130.3. Change.
  - Report Production
    - Add option to show compare jobs or not using
      a checkbox.
      Note: A compare job is a job where field COMPARATION_JOB_YN
      is set to 'Y'.
  MRA:7-MAR-2013 20014002 Show jobs
  - Changes for option to show jobs:
    - Show jobs-checkbox
    - Show only jobs-checkbox (enabled when 'show jobs-checkbox' is checked)
    - Show job code/name (enabled when 'show jobs-checkbox' is checked)
    - Change of 'Show compare jobs' to 'Include compare jobs'.
  ROP: 26-MAR-2013 - 20013965 - Add a switch to show or hide payroll info
       from report Production.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
*)

unit DialogReportProductionFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportProductionF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    GroupBoxShow: TGroupBox;
    CheckBoxBU: TCheckBox;
    CheckBoxEmpl: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    CheckBoxWK: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    QueryBU: TQuery;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CheckBoxJobcode: TCheckBox;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    DataSourceWorkSpot: TDataSource;
    CheckBoxDept: TCheckBox;
    CheckBoxPageBU: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxExport: TCheckBox;
    Label14: TLabel;
    Label1: TLabel;
    CheckBoxShowCompareJobs: TCheckBox;
    CheckBoxShowJobs: TCheckBox;
    CheckBoxShowOnlyJobs: TCheckBox;
    RadioGroupSortOnJob: TRadioGroup;
    CheckBoxShowPayrollInformation: TCheckBox;
    ProgressBar: TProgressBar;
    RadioGroupIncludeDowntime: TRadioGroup; // [ROP] 20013965
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
{    procedure FillWorkSpot; }
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxWKClick(Sender: TObject);
    procedure SetNewPageWK;
    procedure SetNewPageJobCode;
    procedure CheckBoxBUClick(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CheckBoxShowJobsClick(Sender: TObject);
  private
    procedure ShowDateSelection(ShowYN: Boolean);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportProductionF: TDialogReportProductionF;

// RV089.1.
function DialogReportProductionForm: TDialogReportProductionF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportProductionDMT, ReportProductionQRPT, ListProcsFRM,
  UPimsMessageRes, ReportProductionDetailDMT;

// RV089.1.
var
  DialogReportProductionF_HND: TDialogReportProductionF;

// RV089.1.
function DialogReportProductionForm: TDialogReportProductionF;
begin
  if (DialogReportProductionF_HND = nil) then
  begin
    DialogReportProductionF_HND := TDialogReportProductionF.Create(Application);
    with DialogReportProductionF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportProductionF_HND;
end;

// RV089.1.
procedure TDialogReportProductionF.FormDestroy(Sender: TObject);
begin
  inherited;
//  ReportProductionDetailDM.Free; // 20015220
  if (DialogReportProductionF_HND <> nil) then
  begin
    DialogReportProductionF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportProductionF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportProductionF.btnOkClick(Sender: TObject);
begin
  inherited;
  if DateTimeFrom(DateFrom.Date) > DateTimeTo(DateTo.Date) then // 20015220
  begin
    DisplayMessage( SDateFromTo, mtInformation, [mbOk]);
    Exit;
  end;
  ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := False;
  ReportProductionDM.EnableWorkspotIncludeForThisReport := False;
  ReportProductionQR.EnableWorkspotIncludeForThisReport := False;
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
  begin
    ReportProductionDetailDM.EnableWorkspotIncludeForThisReport := True;
    ReportProductionDM.EnableWorkspotIncludeForThisReport := True;
    ReportProductionQR.EnableWorkspotIncludeForThisReport := True;
    ReportProductionQR.InclExclWorkspotsResult := EditWorkspots.Text;
    if not ReportProductionQR.InclExclWorkspotsAll then
      ReportProductionQR.InclExclWorkspotsResult :=
        GetStrValue(ReportProductionQR.InclExclWorkspotsResult);
    ReportProductionDM.InclExclWorkspotsResult :=
      ReportProductionQR.InclExclWorkspotsResult;
  end;
  // 20015220
  ReportProductionDetailDM.UseDateTime := SystemDM.DateTimeSel;
  ReportProductionDM.UseDateTime := SystemDM.DateTimeSel;
  ReportProductionQR.UseDateTime := SystemDM.DateTimeSel;
  if SystemDM.DateTimeSel then // 20015220
  begin
    ReportProductionQR.MyProgressBar := ProgressBar;
    ProgressBar.Position := 0;
  end;
  // 20015221
  ReportProductionDetailDM.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportProductionDM.IncludeOpenScans := SystemDM.InclOpenScans;
  ReportProductionQR.IncludeOpenScans := SystemDM.InclOpenScans;
  if ReportProductionQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      //550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      DateTimeFrom(DateFrom.Date), // 20015220
      DateTimeTo(DateTo.Date), // 20015220
      CheckBoxBU.Checked, CheckBoxDept.Checked, CheckBoxWK.Checked,
      CheckBoxJobcode.Checked, CheckBoxEmpl.Checked,
      CheckBoxShowSelection.Checked, CheckBoxPagePlant.Checked,
      CheckBoxPageBU.Checked, CheckBoxPageDept.Checked,
      CheckBoxPageWK.Checked,
      CheckBoxShowCompareJobs.Checked,
      CheckBoxShowJobs.Checked, // 20014002
      CheckBoxShowOnlyJobs.Checked, // 20014002
      RadioGroupSortOnJob.ItemIndex, // 20014002
      CheckBoxExport.Checked,
      CheckBoxShowPayrollInformation.Checked, // [ROP] 20013965 - switch for payroll info
      RadioGroupIncludeDowntime.ItemIndex) // 20015586
  then
    ReportProductionQR.ProcessRecords;
  if SystemDM.DateTimeSel then // 20015220
    ProgressBar.Position := 0;
end;

procedure TDialogReportProductionF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;
{
procedure TDialogReportProductionF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    //RV067.2.
    QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
procedure TDialogReportProductionF.FormShow(Sender: TObject);
begin
  if SystemDM.WorkspotInclSel then
    EnableWorkspotIncludeForThisReport := True; // 20015223
  if SystemDM.DateTimeSel then // 20015220
  begin
    UseDateTime := True;
    ShowDateSelection(False);
    ProgressBar.Visible := True;
  end;
  InitDialog(True, True, True, False, False, True, False);
  inherited;
  FillBusiness;
{  FillWorkSpot; }
{
  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(QueryTeam, 'TEAM_CODE',
    False, ComboBoxPlusTeamTo);
}
  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPageBU.Checked := False;
  CheckBoxPageDept.Checked := False;
  CheckBoxPageWK.Checked := False;

  CheckBoxPageBU.Enabled := False;
  CheckBoxPageDept.Enabled := False;
  CheckBoxPageWK.Enabled := False;

  CheckBoxBU.Checked := False;
  CheckBoxDept.Checked := False;
  CheckBoxWK.Checked := True;
  CheckBoxJobCode.Checked := False;
  CheckBoxEmpl.Checked := False;

end;

procedure TDialogReportProductionF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
//  ReportProductionDetailDM := TReportProductionDetailDM.Create(nil); // 20015220
  ReportProductionDetailDM.Init; // 20015220 + 20015221
  ReportProductionDM := CreateReportDM(TReportProductionDM);
  ReportProductionQR := CreateReportQR(TReportProductionQR);
  CheckBoxShowJobsClick(Sender); // 20014002
end;

procedure TDialogReportProductionF.CheckBoxDeptClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
 
end;

procedure TDialogReportProductionF.SetNewPageWK;
begin
  CheckBoxPageDept.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageDept.Checked := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxWK.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxWK.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxWK.Checked;
  CheckBoxPageBU.Checked := not CheckBoxWK.Checked;
end;

procedure TDialogReportProductionF.CheckBoxWKClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageWK.Enabled := CheckBoxWK.Checked;
  CheckBoxPageWK.Checked := CheckBoxWK.Checked;
  CheckBoxJobCode.Enabled := CheckBoxWK.Checked;
  CheckBoxJobCode.Checked := CheckBoxWK.Checked;

end;

procedure TDialogReportProductionF.SetNewPageJobCode;
begin
  CheckBoxPageDept.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageDept.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageWK.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPagePlant.Checked := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Enabled := not CheckBoxJobcode.Checked;
  CheckBoxPageBU.Checked := not CheckBoxJobcode.Checked;
end;

procedure TDialogReportProductionF.CheckBoxBUClick(Sender: TObject);
begin
  inherited;
  CheckBoxPageBU.Enabled := CheckBoxBU.Checked;
  CheckBoxPageBU.Checked := CheckBoxBU.Checked;
end;

procedure TDialogReportProductionF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
     GetStrValue(ComboBoxPlusBusinessTo.Value) then
       ComboBoxPlusBusinessTo.DisplayValue :=
         ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportProductionF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportProductionF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue; }
end;

procedure TDialogReportProductionF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue; }
end;

procedure TDialogReportProductionF.FillAll;
begin
  inherited;
  FillBusiness;
{  FillWorkSpot; }
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportProductionF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogReportProductionF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// 20014002
procedure TDialogReportProductionF.CheckBoxShowJobsClick(Sender: TObject);
begin
  inherited;
  CheckBoxShowOnlyJobs.Enabled := CheckBoxShowJobs.Checked;
  RadioGroupSortOnJob.Enabled := CheckBoxShowJobs.Checked;
end;

// 20015220
procedure TDialogReportProductionF.ShowDateSelection(ShowYN: Boolean);
begin
  Label2.Visible := ShowYN;
  Label4.Visible := ShowYN;
  Label26.Visible := ShowYN;
  DateFrom.Visible := ShowYN;
  DateTo.Visible := ShowYN;
end;

end.
