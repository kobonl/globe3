inherited ReportHoursPerEmplDM: TReportHoursPerEmplDM
  OldCreateOrder = True
  Left = 232
  Top = 148
  Height = 590
  Width = 760
  inherited qryPlantMinMax: TQuery
    Left = 184
    Top = 112
  end
  inherited qryWeekInsert: TQuery
    Left = 432
    Top = 64
  end
  inherited qryDepartmentMinMax: TQuery
    Top = 64
  end
  object QueryProductionMinute: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, '
      '  A.PRODHOUREMPLOYEE_DATE, S.SALARY_DATE,'
      '  SUM(A.PRODUCTION_MINUTE) AS SUMWK_HRS, '
      '  SUM(A.PAYED_BREAK_MINUTE) AS BREAK_MIN '
      'FROM  '
      '  EMPLOYEE E, PRODHOURPEREMPLOYEE A, '
      '  SALARYHOURPEREMPLOYEE  S '
      'WHERE  '
      '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER  AND '
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  A.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE,'
      '  S.SALARY_DATE')
    Left = 48
    Top = 112
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryAbsenceMinute: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  E.EMPLOYEE_NUMBER, '
      '  SUM(A.ABSENCE_MINUTE)  AS SUMABS_MIN '
      'FROM  '
      '  EMPLOYEE E, '
      '  ABSENCEHOURPEREMPLOYEE A '
      'WHERE  '
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A. ABSENCEHOUR_DATE >= :FSTARTDATE AND'
      '  A. ABSENCEHOUR_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 48
    Top = 160
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
    object QueryAbsenceMinuteEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAbsenceMinuteSUMABS_MIN: TFloatField
      FieldName = 'SUMABS_MIN'
    end
  end
  object QueryDirProduction: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE,'
      '  SUM(A.PRODUCTION_MINUTE) AS SUMWK_HRS'
      'FROM '
      '  EMPLOYEE E, PRODHOURPEREMPLOYEE A '
      'WHERE  '
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  A.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE')
    Left = 48
    Top = 208
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryProductive: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE,'
      '  SUM(A.PRODUCTION_MINUTE)  AS SUMWK_HRS'
      'FROM '
      '  EMPLOYEE E, PRODHOURPEREMPLOYEE A '
      'WHERE  '
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  A.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE')
    Left = 48
    Top = 256
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryNonProductive: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER,  A.PRODHOUREMPLOYEE_DATE,'
      '  SUM(A.PRODUCTION_MINUTE)  AS SUMWK_HRS'
      'FROM '
      '  EMPLOYEE E, PRODHOURPEREMPLOYEE A '
      'WHERE  '
      '  E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  A.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE')
    Left = 184
    Top = 256
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryIndProduction: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE,'
      '  SUM(A.PRODUCTION_MINUTE) AS SUMWK_HRS'
      'FROM '
      '  EMPLOYEE E, PRODHOURPEREMPLOYEE A '
      'WHERE  '
      '  E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  A.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.PRODHOUREMPLOYEE_DATE')
    Left = 184
    Top = 208
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceProdMinute: TDataSource
    DataSet = QueryProductionMinute
    Left = 352
    Top = 408
  end
  object DataSourceEmpl: TDataSource
    DataSet = QueryEmpl
    Left = 352
    Top = 352
  end
  object QueryDirSalary: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, A.SALARY_DATE,'
      '  SUM(A.SALARY_MINUTE)  AS SUMWK_HRS'
      'FROM '
      '  EMPLOYEE E, SALARYHOURPEREMPLOYEE A '
      'WHERE  '
      '  E.EMPLOYEE_NUMBER  =  A.EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.SALARY_DATE >= :FSTARTDATE AND'
      '  A.SALARY_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.SALARY_DATE'
      '')
    Left = 184
    Top = 304
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryIndSalary: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, A.SALARY_DATE,'
      '  SUM(A.SALARY_MINUTE)  AS SUMWK_HRS'
      'FROM  '
      '  EMPLOYEE E, SALARYHOURPEREMPLOYEE A '
      'WHERE  '
      '  A.EMPLOYEE_NUMBER  =  :EMPLOYEE_NUMBER AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.SALARY_DATE >= :FSTARTDATE AND'
      '  A.SALARY_DATE <= :FENDDATE '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, A.SALARY_DATE')
    Left = 48
    Top = 304
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT '
      '  TEAM_CODE, EMPLOYEE_NUMBER, DESCRIPTION, STARTDATE, '
      '  ENDDATE'
      'FROM  '
      '  EMPLOYEE '
      'WHERE  '
      '  EMPLOYEE_NUMBER  >= :EMPLOYEEFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  STARTDATE <= :FDATECURRENT AND'
      '  ENDDATE >= :FDATECURRENT ')
    Left = 48
    Top = 48
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end>
  end
  object TableTeam: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'TEAM'
    Left = 472
    Top = 352
  end
  object QueryCheckTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryCheckTeamFilterRecord
    SQL.Strings = (
      'SELECT '
      '  E.TEAM_CODE, P.PRODHOUREMPLOYEE_DATE, '
      '  SUM(P.PRODUCTION_MINUTE) AS SUMWK_HRS '
      'FROM '
      '  PRODHOURPEREMPLOYEE P, EMPLOYEE E '
      'WHERE '
      '  P.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER '
      '  AND E.TEAM_CODE = :TEAM_CODE '
      '  AND P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE '
      '  AND P.PRODHOUREMPLOYEE_DATE < :FENDDATE '
      '  AND E.STARTDATE <= :FDATECURRENT '
      '  AND E.ENDDATE >= :FDATECURRENT ')
    Left = 432
    Top = 112
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end>
  end
  object QueryAbsenceMinuteHourtype: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT  '
      '  E.EMPLOYEE_NUMBER, '
      '  AR.HOURTYPE_NUMBER,'
      '  A.ABSENCEHOUR_DATE,'
      '  SUM(A.ABSENCE_MINUTE) AS ABSENCE_MIN'
      'FROM  '
      '  EMPLOYEE E,'
      '  ABSENCEHOURPEREMPLOYEE A,'
      '  ABSENCEREASON AR'
      'WHERE  '
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER  AND '
      '  E.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND'
      '  A.ABSENCEREASON_ID = AR.ABSENCEREASON_ID AND'
      '  E.STARTDATE <= :FDATECURRENT AND'
      '  E.ENDDATE >= :FDATECURRENT AND'
      '  A.ABSENCEHOUR_DATE >= :FSTARTDATE AND'
      '  A.ABSENCEHOUR_DATE <= :FENDDATE'
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, '
      '  AR.HOURTYPE_NUMBER,'
      '  A.ABSENCEHOUR_DATE')
    Left = 336
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATECURRENT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryEmplContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    DataSource = DataSourceEmpl
    SQL.Strings = (
      'SELECT'
      
        '  EMPLOYEE_NUMBER, STARTDATE, ENDDATE,  CONTRACT_HOUR_PER_WEEK, ' +
        'NVL(HOURLY_WAGE,0) HOURLY_WAGE'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE'
      '  EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  STARTDATE <= :FMAXDATE AND  ENDDATE >= :FMINDATE '
      'ORDER BY'
      '  EMPLOYEE_NUMBER, STARTDATE DESC'
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 432
    Top = 184
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FMAXDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FMINDATE'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetHT: TClientDataSet
    Aggregates = <>
    IndexFieldNames = 'HOURTYPE_NUMBER'
    Params = <>
    ProviderName = 'DataSetProviderHT'
    Left = 288
    Top = 16
  end
  object DataSetProviderHT: TDataSetProvider
    DataSet = QueryHT
    Constraints = True
    Left = 456
    Top = 16
  end
  object QueryHT: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      
        '  HOURTYPE_NUMBER, NVL(BONUS_PERCENTAGE,0) BONUS_PERCENTAGE, DES' +
        'CRIPTION '
      'FROM'
      '  HOURTYPE '
      'ORDER BY '
      '  HOURTYPE_NUMBER'
      ' '
      ' ')
    Left = 368
    Top = 16
  end
  object ClientDataSetHrsPerWeek: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetHrsPerWeekIdx'
        Fields = 'WEEK_NR;DAY_NR'
      end>
    IndexName = 'ClientDataSetHrsPerWeekIdx'
    Params = <>
    StoreDefs = True
    Left = 184
    Top = 376
    object ClientDataSetHrsPerWeekWEEK_NR: TIntegerField
      FieldName = 'WEEK_NR'
    end
    object ClientDataSetHrsPerWeekDAY_NR: TIntegerField
      FieldName = 'DAY_NR'
    end
    object ClientDataSetHrsPerWeekDAY_MINUTE: TIntegerField
      FieldName = 'MINUTE_DAY'
    end
  end
  object ClientDataSetHrsPerHT: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetHrsPerWeekIdx'
        Fields = 'WEEK_NR;HOUR_TYPE'
      end>
    IndexName = 'ClientDataSetHrsPerWeekIdx'
    Params = <>
    StoreDefs = True
    Left = 48
    Top = 376
    object IntegerField1: TIntegerField
      FieldName = 'WEEK_NR'
    end
    object ClientDataSetHrsPerHTHOUR_TYPE: TIntegerField
      FieldName = 'HOUR_TYPE'
    end
    object ClientDataSetHrsPerHTHOUR_TYPE_DESCRIPTION: TStringField
      FieldName = 'HOUR_TYPE_DESCRIPTION'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_1: TIntegerField
      FieldName = 'MINUTE_DAY_1'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_2: TIntegerField
      FieldName = 'MINUTE_DAY_2'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_3: TIntegerField
      FieldName = 'MINUTE_DAY_3'
    end
    object ClientDataSetHrsPerHTMINUTEDAY_4: TIntegerField
      FieldName = 'MINUTE_DAY_4'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_5: TIntegerField
      FieldName = 'MINUTE_DAY_5'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_6: TIntegerField
      FieldName = 'MINUTE_DAY_6'
    end
    object ClientDataSetHrsPerHTMINUTE_DAY_7: TIntegerField
      FieldName = 'MINUTE_DAY_7'
    end
    object ClientDataSetHrsPerHTWORKED_HOURS: TBooleanField
      FieldName = 'WORKED_HOURS'
    end
    object ClientDataSetHrsPerHTISWORKSPOTHOURTYPE: TStringField
      FieldName = 'ISWORKSPOTHOURTYPE'
      Size = 5
    end
  end
  object ClientDataSetHrsPerPlantWeek: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetHrsPerPlantWeekIdx'
        Fields = 'WEEK_NR;PLANT_CODE'
      end>
    IndexName = 'ClientDataSetHrsPerPlantWeekIdx'
    Params = <>
    StoreDefs = True
    Left = 72
    Top = 432
    object ClientDataSetHrsPerPlantWeekWEEK_NR: TIntegerField
      FieldName = 'WEEK_NR'
    end
    object ClientDataSetHrsPerPlantWeekPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object ClientDataSetHrsPerPlantWeekDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_1: TIntegerField
      FieldName = 'MINUTE_DAY_1'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_2: TIntegerField
      FieldName = 'MINUTE_DAY_2'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_3: TIntegerField
      FieldName = 'MINUTE_DAY_3'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_4: TIntegerField
      FieldName = 'MINUTE_DAY_4'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_5: TIntegerField
      FieldName = 'MINUTE_DAY_5'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_6: TIntegerField
      FieldName = 'MINUTE_DAY_6'
    end
    object ClientDataSetHrsPerPlantWeekMINUTE_DAY_7: TIntegerField
      FieldName = 'MINUTE_DAY_7'
    end
  end
  object ClientDataSetHrsPerPlant: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ClientDataSetHrsPerPlantIdx'
        Fields = 'PLANT_CODE;DAY_NR'
      end>
    IndexName = 'ClientDataSetHrsPerPlantIdx'
    Params = <>
    StoreDefs = True
    Left = 224
    Top = 432
    object ClientDataSetHrsPerPlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object ClientDataSetHrsPerPlantDAY_NR: TIntegerField
      FieldName = 'DAY_NR'
    end
    object ClientDataSetHrsPerPlantMINUTE_DAY: TIntegerField
      FieldName = 'MINUTE_DAY'
    end
    object ClientDataSetHrsPerPlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object WorkQuery: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    Left = 472
    Top = 416
  end
  object qryExceptHrDef: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEmplFilterRecord
    SQL.Strings = (
      'SELECT'
      '  NVL(SUM('
      
        '   60*TRUNC(((86400*(EH.ENDTIME-EH.STARTTIME))/60)/60)-24*(TRUNC' +
        '((((86400*(EH.ENDTIME-EH.STARTTIME))/60)/60)/24)) +'
      
        '   TRUNC((86400*(EH.ENDTIME-EH.STARTTIME))/60)-60*(TRUNC(((86400' +
        '*(EH.ENDTIME-EH.STARTTIME))/60)/60))'
      '   ), 0)'
      '   EH_MINUTE'
      'FROM'
      '  EXCEPTIONALHOURDEF EH INNER JOIN EMPLOYEE E ON'
      '    EH.CONTRACTGROUP_CODE = E.CONTRACTGROUP_CODE'
      'WHERE'
      '  EH.HOURTYPE_NUMBER = 1 AND'
      '  E.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      ' ')
    Left = 464
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryExtraPaymentsWeek: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT EP.EMPLOYEE_NUMBER,'
      '  EP.PAYMENT_EXPORT_CODE, PC.DESCRIPTION,'
      '  SUM(EP.PAYMENT_AMOUNT) PAYMENT_AMOUNT'
      'FROM EXTRAPAYMENT EP INNER JOIN PAYMENTEXPORTCODE PC ON'
      '  EP.PAYMENT_EXPORT_CODE = PC.PAYMENT_EXPORT_CODE'
      '/*  INNER JOIN WEEK W ON'
      
        '  EP.PAYMENT_DATE >= W.DATEFROM AND EP.PAYMENT_DATE <= W.DATETO ' +
        '*/'
      
        'WHERE EP.PAYMENT_DATE >= :DATEFROM AND EP.PAYMENT_DATE <= :DATET' +
        'O'
      'GROUP BY'
      '  EP.EMPLOYEE_NUMBER,'
      '  EP.PAYMENT_EXPORT_CODE, PC.DESCRIPTION'
      'ORDER BY'
      '  EP.EMPLOYEE_NUMBER,'
      '  EP.PAYMENT_EXPORT_CODE, PC.DESCRIPTION'
      ''
      ' '
      ' '
      ' '
      ' ')
    Left = 544
    Top = 64
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryPaymentExportCode: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT PAYMENT_EXPORT_CODE, DESCRIPTION'
      'FROM PAYMENTEXPORTCODE'
      'ORDER BY PAYMENT_EXPORT_CODE'
      ' ')
    Left = 544
    Top = 120
  end
end
