(*
  Changes:
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Fields WEEKNUMBER, EMPLOYEE_NUMBER gave an error
      like:
        QueryProdHour:
          Type mismatch for field WEEKNUMBER, expecting float, actual integer.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'. (QueryProdHour).
      Also field-components for QueryProdHour for these fields must be 'float'.
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)

unit ReportHrsPerWKDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, Provider, DBClient;

type
  TReportHrsPerWKDM = class(TReportBaseDM)
    QueryProdHour: TQuery;
    DataSourceProd: TDataSource;
    QueryBUPerWK: TQuery;
    cdsQueryBUWK: TClientDataSet;
    dspQueryBUWK: TDataSetProvider;
    cdsEmpl: TClientDataSet;
    dspEmpl: TDataSetProvider;
    QueryEmpl: TQuery;
    QueryProdHourPLANT_CODE: TStringField;
    QueryProdHourPDESC: TStringField;
    QueryProdHourWORKSPOT_CODE: TStringField;
    QueryProdHourWDESC: TStringField;
    QueryProdHourPRODHOUREMPLOYEE_DATE: TDateTimeField;
    QueryProdHourSUMMIN: TFloatField;
    QueryProdHourBUSINESSUNIT_CODE: TStringField;
    QueryProdHourBDESC: TStringField;
    QueryProdHourDEPARTMENT_CODE: TStringField;
    QueryProdHourDDESC: TStringField;
    QueryProdHourJOB_CODE: TStringField;
    QueryProdHourJDESC: TStringField;
    QueryProdHourEMPLOYEE_NUMBER: TFloatField;
    QueryProdHourBUCODE: TStringField;
    QueryProdHourLISTCODE: TFloatField;
    QueryProdHourWEEKNUMBER: TFloatField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryProdHourFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportHrsPerWKDM: TReportHrsPerWKDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, SystemDMT;

procedure TReportHrsPerWKDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryProdHour);
end;

procedure TReportHrsPerWKDM.QueryProdHourFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
