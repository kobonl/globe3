(*
  Changes:
    MR:15-02-2005 QueryPQ-sql-statement changed: 'QUANTITY <> 0'
*)

unit ReportProductionCompDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db;

type
  TReportProductionCompDM = class(TReportBaseDM)
    QueryProductionComp: TQuery;
    DataSourceProduction: TDataSource;
    QueryPQ: TQuery;
    QueryCheck: TQuery;
    QueryCompJob: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryProductionCompFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InitQueryTotalPiecesPQ(QueryPQ : TQuery;
      DateFrom, DateTo: TDateTime;
      PlantFrom, PlantTo: String);
  end;

var
  ReportProductionCompDM: TReportProductionCompDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportProductionCompDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV067.2.
  SystemDM.PlantTeamFilterEnable(QueryProductionComp);

  QueryPQ.Close;
  QueryPQ.Prepare;
 
  QueryCompJob.Open;
end;

procedure TReportProductionCompDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  QueryCompJob.Close;
  QueryPQ.Close;
  QueryPQ.UnPrepare;
end;



procedure TReportProductionCompDM.InitQueryTotalPiecesPQ(
  QueryPQ : TQuery;
  DateFrom, DateTo: TDateTime;
  PlantFrom, PlantTo: String);
begin
  // MR:28-09-2005 550406
  // QueryPQ was not closed here! So it gave wrong results
  // when report-dialog was not closed and another selection was made.
  QueryPQ.Close;
  QueryPQ.ParamByName('FSTARTDATE').AsDateTime := DateFrom;
  QueryPQ.ParamByName('FENDDATE').AsDateTime :=  DateTo;
  QueryPQ.ParamByName('PLANTFROM').AsString :=  PlantFrom;
  QueryPQ.ParamByName('PLANTTO').AsString :=  PlantTo;
  QueryPQ.Open;
end;

procedure TReportProductionCompDM.QueryProductionCompFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV067.2.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
