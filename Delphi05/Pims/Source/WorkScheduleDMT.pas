(*
  MRA:2-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:6-APR-2018 GLOB3-114
  - Work Schedule adjust repeats to 26 (from 12 to 26)
  MRA:24-AUG-2018 GLOB3-142
  - Process workschedules does not use defined reference week.
  - Let the user set the reference week, but correct it (with a message)
    when it is not set to first day of the week.
*)
unit WorkScheduleDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, SystemDMT;

type
  TWorkScheduleDM = class(TGridBaseDM)
    TableDetailWORKSCHEDULE_ID: TFloatField;
    TableDetailCODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailREPEATS: TFloatField;
    TableDetailSTARTDATE: TDateTimeField;
    TableDetailCALCREFERENCEWEEK: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    qryWorkScheduleLine: TQuery;
    qryWorkScheduleLineInsert: TQuery;
    qryEmployeeContract: TQuery;
    qryWorkScheduleLineDelete: TQuery;
    procedure DefaultNewRecord(DataSet: TDataSet);
    procedure TableDetailCalcFields(DataSet: TDataSet);
    procedure DefaultBeforePost(DataSet: TDataSet);
    procedure TableDetailAfterPost(DataSet: TDataSet);
    procedure DefaultBeforeDelete(DataSet: TDataSet);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
    FNewRepeats: Integer;
    FOldRepeats: Integer;
    FMyState: TDataSetState;
    procedure DeleteWorkScheduleDetails(AWorkScheduleID: Integer);
    procedure InsertWorkScheduleDetails(AWorkScheduleID: Integer; ARepeats: Integer);
    function EmployeeContractCheck(AWorkScheduleID: Integer): Boolean;
  public
    { Public declarations }
    property OldRepeats: Integer read FOldRepeats write FOldRepeats;
    property NewRepeats: Integer read FNewRepeats write FNewRepeats;
    property MyState: TDataSetState read FMyState write FMyState; 
  end;

var
  WorkScheduleDM: TWorkScheduleDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM, UPimsMessageRes;

procedure TWorkScheduleDM.DefaultNewRecord(DataSet: TDataSet);
var
  Year, Week, Day: Word;
begin
  inherited;
  MyState := DataSet.State;
  DecodeDate(Now, Year, Week, Day);
  DataSet.FieldByName('REPEATS').AsInteger := 1;
  DataSet.FieldByName('STARTDATE').AsDateTime :=
    ListProcsF.DateFromWeek(Year, 1, 1);
end; // DefaultNewRecord

procedure TWorkScheduleDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  if DataSet.FieldByName('REPEATS').AsString <> '' then
    OldRepeats := DataSet.FieldByName('REPEATS').AsInteger
  else
    OldRepeats := 1;
end;

procedure TWorkScheduleDM.TableDetailCalcFields(DataSet: TDataSet);
var
  Year, Week: Word;
begin
  inherited;
  try
    ListProcsF.WeekUitDat(TableDetailSTARTDATE.Value, Year, Week);
    TableDetailCALCREFERENCEWEEK.Value := IntToStr(Week) + ';' + IntToStr(Year);
  except
    TableDetailCALCREFERENCEWEEK.Value := '';
  end;
end; // TableDetailCalcFields

procedure TWorkScheduleDM.DeleteWorkScheduleDetails(
  AWorkScheduleID: Integer);
begin
  try
    with qryWorkScheduleLineDelete do
    begin
      ParamByName('WORKSCHEDULE_ID').AsInteger := AWorkScheduleID;
      ExecSQL;
    end;
  except
  end;
end; // DeleteWorkScheduleDetails

procedure TWorkScheduleDM.InsertWorkScheduleDetails(AWorkScheduleID,
  ARepeats: Integer);
var
  I: Integer;
begin
  try
    with qryWorkScheduleLineInsert do
    begin
      for I := 1 to ARepeats do
      begin
        ParamByName('WORKSCHEDULE_ID').AsInteger := AWorkScheduleID;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
      end;
    end;
  except
  end;
end; // InsertWorkScheduleDetails

procedure TWorkScheduleDM.DefaultBeforePost(DataSet: TDataSet);
var
  GoOn: Boolean;
  function StartDateCheck: Boolean;
  var
    Year, Week, Day: Word;
  begin
    Result := False;
    DecodeDate(DataSet.FieldByName('STARTDATE').AsDateTime, Year, Week, Day);
    Day := ListProcsF.DayWStartOnFromDate(DataSet.FieldByName('STARTDATE').AsDateTime);
    if (Day <> 1) then
    begin
      DataSet.FieldByName('STARTDATE').AsDateTime :=
        ListProcsF.DateFromWeek(Year, Week, 1);
      DisplayMessage(SPimsWorkScheduleWrongStartDate, mtInformation, [mbOK]);
      Result := True;
    end;
  end; // StartDateCheck
begin
  inherited;
  MyState := DataSet.State;
  if not ((DataSet.FieldByName('REPEATS').AsInteger >= 1) and
    (DataSet.FieldByName('REPEATS').AsInteger <= 26)) then
  begin
    DataSet.FieldByName('REPEATS').AsInteger := 1;
    DisplayMessage(SPimsWorkScheduleWrongRepeats, mtInformation, [mbOK]);
    Abort;
    Exit;
  end;
  StartDateCheck;
  if not (DataSet.State in [dsInsert]) then
  begin
    NewRepeats := DataSet.FieldByName('REPEATS').AsInteger;
    GoOn := True;
    if NewRepeats < OldRepeats then
    begin
      if DisplayMessage(SPimsWorkScheduleDetailsExists, mtInformation,
        [mbYes, mbNo]) = mrYes then
          GoOn := True
      else
        GoOn := False;
    end;
    if not GoOn then
    begin
      DataSet.Cancel;
      Abort;
    end;
  end;
end; // DefaultBeforePost

procedure TWorkScheduleDM.TableDetailAfterPost(DataSet: TDataSet);
begin
  inherited;
  // Change existing record?
  if MyState in [dsInsert] then
  begin
    // Add new records
    InsertWorkScheduleDetails(
      DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger,
      DataSet.FieldByName('REPEATS').AsInteger);
  end
  else
  begin
    if MyState in [dsEdit] then
    begin
      NewRepeats := DataSet.FieldByName('REPEATS').AsInteger;
      if OldRepeats <> NewRepeats then
      begin
        if NewRepeats < OldRepeats then
        begin
          // Delete any old records
          DeleteWorkScheduleDetails(
            DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger);
          // Add new records
          InsertWorkScheduleDetails(
            DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger,
            DataSet.FieldByName('REPEATS').AsInteger);
        end
        else
        begin
          // NewRepeats is higher then OldRepeats, only add some records,
          // keep the old ones.
          // Add new records
          InsertWorkScheduleDetails(
            DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger,
            NewRepeats - OldRepeats);
        end;
      end;
    end;
  end;
end; // TableDetailAfterPost

function TWorkScheduleDM.EmployeeContractCheck(
  AWorkScheduleID: Integer): Boolean;
begin
  Result := False;
  with qryEmployeeContract do
  begin
    Close;
    ParamByName('WORKSCHEDULE_ID').AsInteger := AWorkScheduleID;
    Open;
    if FieldByName('MYCOUNT').AsInteger > 0 then
      Result := True;
    Close;
  end;
end; // EmployeeContractCheck

procedure TWorkScheduleDM.DefaultBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  MyState := DataSet.State;
  // Check if workschedule is in use: If so, then do not allow delete!
  // Delete any old records
  if EmployeeContractCheck(DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger) then
  begin
    DisplayMessage(SPimsWorkScheduleInUse, mtInformation, [mbOK]);
    DataSet.Cancel;
    Abort;
  end
  else
    DeleteWorkScheduleDetails(
      DataSet.FieldByName('WORKSCHEDULE_ID').AsInteger);
end;

end.
