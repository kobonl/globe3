(*
  MRA:18-MAR-2013 TD-22296 Related to this todo.
  - Report production details does not filter on department anymore.
  - Also: Filter on business units was missing.
*)
unit DialogMonthlyEmployeeEfficiencyDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, comctrls, SystemDMT, UGlobalFunctions, UScannedIDCard,
  DBClient, Provider;

type
  TDialogMonthlyEmployeeEfficiencyDM = class(TDataModule)
    qryEmployeeEfficiencyInsert: TQuery;
    qryEmployeeEfficiencyDelete: TQuery;
    qryJob: TQuery;
    dspJob: TDataSetProvider;
    cdsJob: TClientDataSet;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DetermineEmployeeEfficiency(
      const AYear, AMonth: Integer;
      const APlantFrom, APlantTo, ADeptFrom, ADeptTo,
        AWorkspotFrom, AWorkspotTo: String;
      AProgressBar: TProgressBar);
  end;

var
  DialogMonthlyEmployeeEfficiencyDM: TDialogMonthlyEmployeeEfficiencyDM;

implementation

{$R *.DFM}

uses
  ListProcsFRM,
  ReportProductionDetailDMT;

{ TDialogMonthlyEmployeeEfficiencyDM }

// MR:29-01-2007 Order 550438 Employee Efficiency

// Notes:
// 1. Arguments 'ADeptFrom' and 'ADeptTo' are not used here.
// 2. Datamodule 'ReportProductionDetailDMT' is also used here to
//    determine the Employee-efficiency.

procedure TDialogMonthlyEmployeeEfficiencyDM.DetermineEmployeeEfficiency(
  const AYear, AMonth: Integer; const APlantFrom, APlantTo, ADeptFrom,
  ADeptTo, AWorkspotFrom, AWorkspotTo: String;
  AProgressBar: TProgressBar);
var
  DateMin, DateMax: TDateTime;
  AllPlants, AllWorkspots: Boolean;
  WorkspotSelection: String;
  MyNormProdLevel: Double;
  procedure DeleteEmployeeEfficiency;
  begin
    // First delete records from table 'EMPLOYEEEFFICIENCY'
    with qryEmployeeEfficiencyDelete do
    begin
      WorkspotSelection := '';
      if APlantFrom = APlantTo then
        WorkspotSelection :=
          'AND WORKSPOT_CODE >= :WORKSPOTFROM ' +
          'AND WORKSPOT_CODE <= :WORKSPOTTO';
      Close;
      SQL.Clear;
      SQL.Add(
        'DELETE FROM EMPLOYEEEFFICIENCY ' +
        'WHERE YEAR_NUMBER = :YEAR_NUMBER ' +
        'AND MONTH_NUMBER = :MONTH_NUMBER ' +
        'AND PLANT_CODE >= :PLANTFROM ' +
        'AND PLANT_CODE <= :PLANTTO ' +
        WorkspotSelection
        );
      ParamByName('YEAR_NUMBER').AsInteger := AYear;
      ParamByName('MONTH_NUMBER').AsInteger := AMonth;
      ParamByName('PLANTFROM').AsString := APlantFrom;
      ParamByName('PLANTTO').AsString := APlantTo;
      if APlantFrom = APlantTo then
      begin
        ParamByName('WORKSPOTFROM').AsString := AWorkspotFrom;
        ParamByName('WORKSPOTTO').AsString := AWorkspotTo;
      end;
      ExecSQL;
    end;
  end;
  function DetermineNormProdLevel: Double;
  begin
    Result := 0;
    if not cdsJob.Active then
    begin
      cdsJob.Filtered := False;
      cdsJob.Open;
      cdsJob.LogChanges := False;
    end;
    cdsJob.Filtered := False;
    cdsJob.Filter :=
      'PLANT_CODE = ' + ReportProductionDetailDM.cdsEmpPieces.
      FieldByName('PLANT_CODE').AsString + ' AND ' +
      'WORKSPOT_CODE = ' + ReportProductionDetailDM.cdsEmpPieces.
      FieldByName('WORKSPOT_CODE').AsString + ' AND ' +
      'JOB_CODE = ' + ReportProductionDetailDM.cdsEmpPieces.
      FieldByName('JOB_CODE').AsString;
    cdsJob.Filtered := True;
    if not cdsJob.IsEmpty then
      Result := cdsJob.FieldByName('NORM_PROD_LEVEL').AsFloat;
  end;
begin
  DateMin := EncodeDate(AYear, AMonth, 1);
  // DateMax should be 1 day later.
  DateMax := EncodeDate(AYear, AMonth,
    ListProcsF.LastMounthDay(AYear, AMonth)) + 1;

  AllPlants := (APlantFrom <> APlantTo);
  AllWorkspots := False;

// MR:05-10-2007 Start
// MR:05-10-2007 Use one function to do all.
  ReportProductionDetailDM.InitAll(
    APlantFrom,
    APlantTo,
    '0', // TD-22296 (BusinessUnitFrom)
    '9999999', // TD-22296 (BusinessUnitTo)
    ADeptFrom, // TD-22296
    ADeptTo, // TD-22296
    AWorkspotFrom,
    AWorkspotTo,
    '', '',
    '0', '999',
    DateMin,
    DateMax,
    AllPlants,
    True, // TD-22296 (AllBusinessUnits)
    False, // TD-22296 (AllDepartments)
    AllWorkspots,
    True, True,
    False, // ShowShifts
    AProgressBar);
// MR:05-10-2007 End

  // First delete records from table EMPLOYEEEFFICIENCY
  DeleteEmployeeEfficiency;
  // Now fill records in table EMPLOYEEEFFICIENCY
  if ReportProductionDetailDM.cdsEmpPieces.RecordCount > 0 then
  begin
    ReportProductionDetailDM.cdsEmpPieces.First;
    while not ReportProductionDetailDM.cdsEmpPieces.Eof do
    begin
      // Insert record for table EMPLOYEEEFFICIENCY
      try
        if ReportProductionDetailDM.cdsEmpPieces.
            FieldByName('EMPLOYEE_NUMBER').AsInteger <> NO_EMPLOYEE then
        begin
          qryEmployeeEfficiencyInsert.Close;
          qryEmployeeEfficiencyInsert.ParamByName('YEAR_NUMBER').AsInteger :=
            AYear;
          qryEmployeeEfficiencyInsert.ParamByName('MONTH_NUMBER').AsInteger :=
            AMonth;
          qryEmployeeEfficiencyInsert.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
            ReportProductionDetailDM.cdsEmpPieces.
              FieldByName('EMPLOYEE_NUMBER').AsInteger;
          qryEmployeeEfficiencyInsert.ParamByName('PLANT_CODE').AsString :=
            ReportProductionDetailDM.cdsEmpPieces.
              FieldByName('PLANT_CODE').AsString;
          qryEmployeeEfficiencyInsert.ParamByName('WORKSPOT_CODE').AsString :=
            ReportProductionDetailDM.cdsEmpPieces.
              FieldByName('WORKSPOT_CODE').AsString;
          qryEmployeeEfficiencyInsert.ParamByName('JOB_CODE').AsString :=
            ReportProductionDetailDM.cdsEmpPieces.
              FieldByName('JOB_CODE').AsString;
          try
            MyNormProdLevel := DetermineNormProdLevel;
            if (MyNormProdLevel > 0) and
              (ReportProductionDetailDM.
               cdsEmpPieces.FieldByName('PRODMINS').AsFloat > 0) then
              qryEmployeeEfficiencyInsert.ParamByName('EFFICIENCY').AsFloat :=
                (
                  ReportProductionDetailDM.
                    cdsEmpPieces.FieldByName('EMPPIECES').AsFloat /
                  (ReportProductionDetailDM.
                    cdsEmpPieces.FieldByName('PRODMINS').AsFloat / 60) /
                  MyNormProdLevel
                ) * 100
            else
              qryEmployeeEfficiencyInsert.ParamByName('EFFICIENCY').AsFloat := 0;
          except
            qryEmployeeEfficiencyInsert.ParamByName('EFFICIENCY').AsFloat := 0;
          end;
          qryEmployeeEfficiencyInsert.ParamByName('CREATIONDATE').AsDateTime :=
            Now;
          qryEmployeeEfficiencyInsert.ParamByName('MUTATIONDATE').AsDateTime :=
            Now;
          qryEmployeeEfficiencyInsert.ParamByName('MUTATOR').AsString :=
            SystemDM.CurrentProgramUser;
          qryEmployeeEfficiencyInsert.ExecSQL;
        end;
      except
        on E: EDatabaseError do
        begin
          // Duplicate value?
        end;
      end;
      ReportProductionDetailDM.cdsEmpPieces.Next;
    end;
  end;
//  ReportProductionDetailDM.cdsEmpPieces.Close;
end;

procedure TDialogMonthlyEmployeeEfficiencyDM.DataModuleDestroy(
  Sender: TObject);
begin
  qryJob.Close;
  cdsJob.Close;
end;

procedure TDialogMonthlyEmployeeEfficiencyDM.DataModuleCreate(
  Sender: TObject);
begin
  //
end;

end.
