(*
  MRA:9-APR-2018 GLOB3-83 (NTS)
  - Report In/Outscan, Absence, Overtime (new report)
*)
unit DialogReportInOutAbsenceOvertimeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxExEdtr, dxEdLib, dxCntner, dxEditor,
  dxExGrEd, dxExELib, Dblup1a, jpeg, ExtCtrls, SystemDMT,
  ReportInOutAbsenceOvertimeDMT, ReportInOutAbsenceOvertimeQRPT;

type
  TDialogReportInOutAbsenceOvertimeF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    CheckBoxNewPagePerTeam: TCheckBox;
    CheckBoxNewPagePerDate: TCheckBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportInOutAbsenceOvertimeF: TDialogReportInOutAbsenceOvertimeF;

function DialogReportInOutAbsenceOvertimeForm: TDialogReportInOutAbsenceOvertimeF;

implementation

{$R *.DFM}

uses
  UPimsMessageRes, ListProcsFRM;

var
  DialogReportInOutAbsenceOvertimeF_HND: TDialogReportInOutAbsenceOvertimeF;

function DialogReportInOutAbsenceOvertimeForm: TDialogReportInOutAbsenceOvertimeF;
begin
  if (DialogReportInOutAbsenceOvertimeF_HND = nil) then
  begin
    DialogReportInOutAbsenceOvertimeF_HND := TDialogReportInOutAbsenceOvertimeF.Create(Application);
    with DialogReportInOutAbsenceOvertimeF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportInOutAbsenceOvertimeF_HND;
end;


procedure TDialogReportInOutAbsenceOvertimeF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportInOutAbsenceOvertimeF_HND <> nil) then
  begin
    DialogReportInOutAbsenceOvertimeF_HND := nil;
  end;
end;

procedure TDialogReportInOutAbsenceOvertimeF.FormShow(Sender: TObject);
begin
  InitDialog(True, False, True, True, False, False, False);
  UseDate := True;
  inherited;

end;

procedure TDialogReportInOutAbsenceOvertimeF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportInOutAbsenceOvertimeF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportInOutAbsenceOvertimeDM := CreateReportDM(TReportInOutAbsenceOvertimeDM);
  ReportInOutAbsenceOvertimeQR := CreateReportQR(TReportInOutAbsenceOvertimeQR);
end;

procedure TDialogReportInOutAbsenceOvertimeF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportInOutAbsenceOvertimeQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
    IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    Trunc(DatePickerFromBase.Date),
    Trunc(DatePickerToBase.Date),
    CheckBoxAllTeams.Checked,
    CBoxShowSelection.Checked,
    CheckBoxExport.Checked,
    CheckBoxNewPagePerTeam.Checked,
    CheckBoxNewPagePerDate.Checked) then
    ReportInOutAbsenceOvertimeQR.ProcessRecords;
end;

procedure TDialogReportInOutAbsenceOvertimeF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportInOutAbsenceOvertimeF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
