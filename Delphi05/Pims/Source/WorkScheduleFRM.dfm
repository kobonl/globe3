inherited WorkScheduleF: TWorkScheduleF
  Height = 528
  Caption = 'Work Schedule'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    TabOrder = 0
    Visible = False
    inherited dxMasterGrid: TdxDBGrid
      Bands = <
        item
          Caption = 'Work Schedule'
        end>
      Visible = False
      ShowBands = True
    end
  end
  inherited pnlDetail: TPanel
    Top = 324
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 640
      Height = 163
      Align = alClient
      Caption = 'Work Schedule'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 25
        Height = 13
        Caption = 'Code'
      end
      object Label2: TLabel
        Left = 8
        Top = 48
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object Label3: TLabel
        Left = 8
        Top = 74
        Width = 75
        Height = 13
        Caption = 'Weekly repeats'
      end
      object Label4: TLabel
        Left = 8
        Top = 99
        Width = 78
        Height = 13
        Caption = 'Reference week'
      end
      object DBText1: TDBText
        Left = 412
        Top = 102
        Width = 65
        Height = 17
        DataField = 'CALCREFERENCEWEEK'
        DataSource = WorkScheduleDM.DataSourceDetail
      end
      object Label5: TLabel
        Left = 232
        Top = 76
        Width = 36
        Height = 13
        Caption = '(1 - 26)'
      end
      object DBEdit1: TDBEdit
        Tag = 1
        Left = 144
        Top = 21
        Width = 121
        Height = 21
        DataField = 'CODE'
        DataSource = WorkScheduleDM.DataSourceDetail
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Tag = 1
        Left = 144
        Top = 48
        Width = 265
        Height = 21
        DataField = 'DESCRIPTION'
        DataSource = WorkScheduleDM.DataSourceDetail
        TabOrder = 1
      end
      object dxDBDateEdit1: TdxDBDateEdit
        Tag = 1
        Left = 144
        Top = 98
        Width = 257
        TabOrder = 3
        DataField = 'STARTDATE'
        DataSource = WorkScheduleDM.DataSourceDetail
      end
      object dxDBSpinEdit1: TdxDBSpinEdit
        Tag = 1
        Left = 144
        Top = 73
        Width = 81
        TabOrder = 2
        DataField = 'REPEATS'
        DataSource = WorkScheduleDM.DataSourceDetail
        StoredValues = 48
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Height = 169
    inherited spltDetail: TSplitter
      Top = 162
      Height = 6
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Height = 161
      KeyField = 'WORKSCHEDULE_ID'
      DataSource = WorkScheduleDM.DataSourceDetail
      object dxDetailGridColumnCODE: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CODE'
      end
      object dxDetailGridColumnDESCRIPTION: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnREPEATS: TdxDBGridColumn
        Caption = 'Weekly repeats'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REPEATS'
      end
      object dxDetailGridColumnSTARTDATE: TdxDBGridColumn
        Caption = 'Reference week'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CALCREFERENCEWEEK'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
end
