inherited frmDialogPassword: TfrmDialogPassword
  Left = 279
  Top = 153
  HorzScrollBar.Range = 0
  VertScrollBar.Range = 0
  ActiveControl = EditPassword
  BorderStyle = bsSingle
  Caption = 'Set New Password'
  ClientHeight = 236
  ClientWidth = 596
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbarBase: TStatusBar
    Top = 217
    Width = 596
  end
  inherited pnlInsertBase: TPanel
    Width = 596
    Height = 116
    TabOrder = 5
    object LabelPassword: TLabel
      Left = 56
      Top = 20
      Width = 114
      Height = 15
      Caption = 'Enter New Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelConfirmPassword: TLabel
      Left = 348
      Top = 20
      Width = 125
      Height = 15
      Caption = 'Confirm New Password'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object EditPassword: TEdit
      Left = 40
      Top = 40
      Width = 229
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 0
    end
    object EditConfirmPassword: TEdit
      Left = 332
      Top = 40
      Width = 229
      Height = 23
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      PasswordChar = '*'
      TabOrder = 1
      Visible = False
      OnChange = EditConfirmPasswordChange
    end
  end
  inherited pnlImageBase: TPanel
    Width = 596
    TabOrder = 1
    inherited imgBaseSolar: TImage
      Left = 298
    end
    inherited ImgOrbit: TImage
      Left = 1
    end
  end
  inherited pnlBottom: TPanel
    Top = 176
    Width = 596
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 23
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    Left = 512
    Top = 4
    DockControlHeights = (
      0
      0
      23
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemGo: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarSubItemPoeple
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemArticles
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemOther
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarSubItemMaintainUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemMaintainNotUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemIssueUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemIssueNotUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemGarments
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarSubItemGoStockUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarSubItemStockNotUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarSubItemScanning
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemMaintainUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonModify
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonPlaceFlag
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonRemoveFlag
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonReturnToStock
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScrap
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonPrintLabel
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScanToRepair
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonChangeCode
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemMaintainNotUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonPrintLabelNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonReturnToStockNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScrapNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonMakeUnique
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemIssueUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonIssueFromUStock
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonIssueFromNUStock
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonIssueDirect
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemIssueNotUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonIssueFromStockNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonIssueDirectNU
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemPoeple: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonCustomers
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonDepartments
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonWearers
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonSuppliers
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemArticles: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonArtType
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonArtSizeType
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonSizes
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonArtGroup
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonArticles
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemOther: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonLabels
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonLabelDesign
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonFlag
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonSystem
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemScanning: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonScanIn
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScanRepair
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScanRewash
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonScanOut
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemGoStockUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonGarmentsIntoStockU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonStockReportU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonTransactionsReport
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemStockNotUnique: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonGarmentsIntoStockNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonStockCorrectionNU
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonStockListNU
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 540
    Top = 4
  end
end
