(*
  MRA:24-JUL-2015 PIM-50
  - New report that can show comments for down-jobs.
*)
unit DialogReportJobCommentsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxExEdtr, dxEdLib, dxCntner, dxEditor,
  dxExGrEd, dxExELib, Dblup1a, ExtCtrls, SystemDMT, ReportJobCommentsDMT,
  ReportJobCommentsQRPT;

type
  TDialogReportJobCommentsF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure DateToCloseUp(Sender: TObject);
    procedure DateFromCloseUp(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogReportJobCommentsF: TDialogReportJobCommentsF;

function DialogReportJobCommentsForm: TDialogReportJobCommentsF;

implementation

{$R *.DFM}

var
  DialogReportJobCommentsF_HND: TDialogReportJobCommentsF;

function DialogReportJobCommentsForm: TDialogReportJobCommentsF;
begin
  if (DialogReportJobCommentsF_HND = nil) then
  begin
    DialogReportJobCommentsF_HND := TDialogReportJobCommentsF.Create(Application);
    with DialogReportJobCommentsF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportJobCommentsF_HND;
end;

procedure TDialogReportJobCommentsF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportJobCommentsDM := CreateReportDM(TReportJobCommentsDM);
  ReportJobCommentsQR := CreateReportQR(TReportJobCommentsQR);
end;

procedure TDialogReportJobCommentsF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportJobCommentsF_HND <> nil) then
  begin
    DialogReportJobCommentsF_HND := nil;
  end;
end;

procedure TDialogReportJobCommentsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportJobCommentsF.FormShow(Sender: TObject);
begin
  InitDialog(True, False, False, False, False, True, False);
  inherited;
  DateFrom.DateTime := Date;
  DateTo.DateTime := DateFrom.DateTime;
end;

procedure TDialogReportJobCommentsF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportJobCommentsF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportJobCommentsF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportJobCommentsQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      Trunc(DateFrom.DateTime),
      Trunc(DateTo.DateTime),
      CBoxShowSelection.Checked,
      CheckBoxExport.Checked)
  then
    ReportJobCommentsQR.ProcessRecords;
end;

procedure TDialogReportJobCommentsF.DateFromCloseUp(Sender: TObject);
begin
  inherited;
  if DateFrom.DateTime > DateTo.DateTime then
    DateTo.DateTime := DateFrom.DateTime;
end;

procedure TDialogReportJobCommentsF.DateToCloseUp(Sender: TObject);
begin
  inherited;
  if DateFrom.DateTime > DateTo.DateTime then
    DateTo.DateTime := DateFrom.DateTime;
end;

end.

