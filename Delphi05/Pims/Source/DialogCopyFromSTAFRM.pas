(*
  MRA:21-APR-2011. RV089.1.
  - Use other function for DisplayMessage with 4 buttons: Yes, No, All, NoToAll.
  MRA:18-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  - Set to other values (plant and shift) coming from calling dialog.
*)
unit DialogCopyFromSTAFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, ComCtrls, StdCtrls, Buttons, ExtCtrls, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, Dblup1a, Db, DBTables, dxLayout, dxExGrEd,
  dxExELib;

type
  TDialogCopyFromSTAF = class(TDialogSelectionF)
    GroupBoxCopy: TGroupBox;
    Label1: TLabel;
    cmbPlusPlant: TComboBoxPlus;
    Label3: TLabel;
    Label4: TLabel;
    ComboBoxPlusShift: TComboBoxPlus;
    QueryCopyFrom: TQuery;
    QuerySTAValues: TQuery;
    dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure cmbPlusPlantCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftCloseUp(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FShiftDisplayValue: String;
    FPlantDisplayValue: String;
    procedure FillEmpl;
  public
    { Public declarations }
    FEmployee, FShift: Integer;
    FPlant, FDepartment: String;
    FCopy: Boolean;
    property ShiftDisplayValue: String read FShiftDisplayValue
      write FShiftDisplayValue;
    property PlantDisplayValue: String read FPlantDisplayValue
      write FPlantDisplayValue;
  end;

var
  DialogCopyFromSTAF: TDialogCopyFromSTAF;

implementation

uses ListProcsFRM, ShiftScheduleDMT, SystemDMT,
  UPimsMessageRes, StandStaffAvailDMT;

{$R *.DFM}

procedure TDialogCopyFromSTAF.FormShow(Sender: TObject);
begin
  inherited;
  ListProcsF.FillComboBoxMaster(StandStaffAvailDM.QueryPlant, 'PLANT_CODE',
    True, cmbPlusPlant);
  ListProcsF.FillComboBox(StandStaffAvailDM.QueryShiftPlant, ComboBoxPlusShift,
    GetStrValue(cmbPlusPlant.Value),
    '', True, 'PLANT_CODE', '', 'SHIFT_NUMBER', 'DESCRIPTION');
  if (GetStrValue(cmbPlusPlant.Value) <> '') and (ComboBoxPlusShift.Value <> '') then
  begin
    FillEmpl;
    dxDBExtLookupEditEmplFrom.Visible := True;
  end;
  //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShift.IsSorted := True;
  // GLOB3-60 Set to other values coming from calling dialog.
  if Self.PlantDisplayValue <> '' then
    cmbPlusPlant.DisplayValue := Self.PlantDisplayValue;
  if Self.ShiftDisplayValue <> '' then
    ComboBoxPlusShift.DisplayValue := Self.ShiftDisplayValue;
end;

procedure TDialogCopyFromSTAF.btnOkClick(Sender: TObject);
var
  Result, Index, EmployeeFrom, ShiftFrom: Integer;
  PlantFrom, Department: String;
  UpdateTable: Boolean;
begin
  inherited;
  Result := 0;
  if (dxDBExtLookupEditEmplFrom.Text = '' ) or (cmbPlusPlant.Value = '') or
    (ComboBoxPlusShift.Value = '') then
  begin
    DisplayMessage(SEmptyValues , mtConfirmation, [mbOK]);
    Exit;
  end;
  EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
  PlantFrom := GetStrValue(cmbPlusPlant.Value);
  ShiftFrom := GetIntValue(ComboBoxPlusShift.Value);
  Department :=
    StandStaffAvailDM.QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString;

  if (EmployeeFrom = FEmployee) and (PlantFrom = FPlant) and
    (ShiftFrom = FShift) then
  begin
    DisplayMessage(SCopySameValues , mtConfirmation, [mbOK]);
    Exit;
  end;
  StandStaffAvailDM.GetValidTB(EmployeeFrom, ShiftFrom, PlantFrom, Department);
  if StandStaffAvailDM.FTBValid[1] = 0 then
  begin
    DisplayMessage(SCopySTANotValidFrom , mtConfirmation, [mbOK]);
    Exit;
  end;

  StandStaffAvailDM.GetValidTB(FEmployee, FShift, FPlant, FDepartment);
  // MR:22-03-2005 Order 550391
  // 'ORDER BY DAY_OF_WEEK' added to query 'QueryCopyFrom'.
  QueryCopyFrom.Active := False;
  QueryCopyFrom.ParamByName('PLANT_CODE').Value := PlantFrom;
  QueryCopyFrom.ParamByName('SHIFT_NUMBER').Value := ShiftFrom;
  QueryCopyFrom.ParamByName('EMPLOYEE_NUMBER').Value := EmployeeFrom;
  if not QueryCopyFrom.Prepared then
    QueryCopyFrom.Prepare;
  QueryCopyFrom.Active := True;

  if QueryCopyFrom.IsEmpty then
  begin
    DisplayMessage(SEmplSTARecords , mtConfirmation, [mbOK]);
    Exit;
  end;
//initialize array  use for faster refresh
  while not QueryCopyFrom.Eof do
  begin
    //car: 550244
    // MR:22-03-2005 Order 550391
    // The 'QuerySTAValues.Close' was not in the 'while', which
    // means this query is not closed and openened for
    // each record it finds in the while, so it finds only the first record.
    QuerySTAValues.Close;
    QuerySTAValues.ParamByName('PLANT').AsString :=  FPlant;
    QuerySTAValues.ParamByName('EMPLOYEE').AsInteger := FEmployee;
    QuerySTAValues.ParamByName('SHIFT').AsInteger := FShift;
    QuerySTAValues.ParamByName('DAY').AsInteger :=
      QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger;
    QuerySTAValues.Open;
    if not QuerySTAValues.IsEmpty then
    begin
      if (Result <> 8) and (Result <> 9) then
// RV089.1. Use other function!
{        Result := DisplayMessage(SCopySTA1 +
          SystemDM.GetDayWDescription(QuerySTAValues.FieldByName('DAY_OF_WEEK').AsInteger) +
          SCopySTA2 , mtConfirmation, [mbYes, mbNo, mbAll, mbNoToAll]); }
        Result := DisplayMessage4Buttons(SCopySTA1 +
          SystemDM.GetDayWDescription(QuerySTAValues.FieldByName('DAY_OF_WEEK').AsInteger) +
          SCopySTA2);
      if (Result = 8) or (Result = 6) then
      begin
         //copy
// copy the new value
          for Index := 1 to SystemDM.MaxTimeblocks do
            if (StandStaffAvailDM.FTBValid[Index] <> 0) and
              (QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
                IntToStr(Index)).Value <> '') and
              (not QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
                IntToStr(Index)).IsNull)
                then
// fill new values
              StandStaffAvailDM.FSTADAY[
                QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger, Index ] :=
                QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(Index)).Value;
            StandStaffAvailDM.UpdateStandardAvail(FPlant, FDepartment, FEmployee, FShift,
              QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger,
              QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger, UpdateTable);
// if not then save back the old value
            if not UpdateTable then
              for Index := 1 to SystemDM.MaxTimeblocks do
              if (StandStaffAvailDM.FTBValid[Index] <> 0) and
                (QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
                  IntToStr(Index)).Value <> '') then
                StandStaffAvailDM.FSTADAY[
                  QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger, Index ] :=
                  QuerySTAValues.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(Index)).Value;
        end;
    end
    else
    begin
      StandStaffAvailDM.QueryInsertSTA.ParamByName('DAY_OF_WEEK').AsInteger :=
        QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger;
       for Index := 1 to SystemDM.MaxTimeblocks do
         if (StandStaffAvailDM.FTBValid[Index] <> 0) and
           (QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
            IntToStr(Index)).Value <> '') and
           (not QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
             IntToStr(Index)).IsNull) then
           StandStaffAvailDM.QueryInsertSTA.ParamByName('AV' + IntToStr(Index)).Value :=
             QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
               IntToStr(Index)).Value;

      StandStaffAvailDM.QueryInsertSTA.ParamByName('PLANT_CODE').AsString :=
        FPlant;
      StandStaffAvailDM.QueryInsertSTA.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        FEmployee;
      StandStaffAvailDM.QueryInsertSTA.ParamByName('SHIFT_NUMBER').AsInteger :=
        FShift;
      StandStaffAvailDM.QueryInsertSTA.ParamByName('MUTATOR').AsString :=
        SystemDM.CurrentProgramUser;
      StandStaffAvailDM.QueryInsertSTA.ParamByName('MUTATIONDATE').AsDateTime :=
        StandStaffAvailDM.FCurrentDate;
      StandStaffAvailDM.QueryInsertSTA.ParamByName('CREATIONDATE').AsDateTime :=
        StandStaffAvailDM.FCurrentDate;
      StandStaffAvailDM.QueryInsertSTA.ExecSQL;

      for Index := 1 to SystemDM.MaxTimeblocks do
      begin
        if (StandStaffAvailDM.FTBValid[Index] <> 0) and
         (QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
          IntToStr(Index)).Value <> '') and
          (not QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' +
          IntToStr(Index)).IsNull) then
          StandStaffAvailDM.
            FSTADAY[QueryCopyFrom.FieldByName('DAY_OF_WEEK').AsInteger, Index ] :=
              QueryCopyFrom.FieldByName('AVAILABLE_TIMEBLOCK_' + IntToStr(Index)).Value;
      end;
    end;

    QueryCopyFrom.Next;
  end;  {while not QueryCopyFrom.Eof do}
  DisplayMessage( SCopyFinished, mtInformation, [mbOk]);
  DialogCopyFromSTAF.Close;
  FCopy := True;
end;

procedure TDialogCopyFromSTAF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  QueryCopyFrom.Close;

end;

procedure TDialogCopyFromSTAF.btnCancelClick(Sender: TObject);
begin
  inherited;
  FCopy := False;
end;

procedure TDialogCopyFromSTAF.FillEmpl;
begin
  if StandStaffAvailDM.QueryPlant.RecordCount > 1 then
  begin
    if not StandStaffAvailDM.QueryEmpl.Filtered then
      StandStaffAvailDM.QueryEmpl.Filtered := True;

    StandStaffAvailDM.QueryEmpl.Filter := 'PLANT_CODE = ''' +
      DoubleQuote(GetStrValue(cmbPlusPlant.Value)) + '''';
  end;
  StandStaffAvailDM.QueryEmpl.First;
end;

procedure TDialogCopyFromSTAF.cmbPlusPlantCloseUp(Sender: TObject);
begin
  inherited;
  ListProcsF.FillComboBox(StandStaffAvailDM.QueryShiftPlant, ComboBoxPlusShift,
    GetStrValue(cmbPlusPlant.Value), '', True, 'PLANT_CODE', '', 'SHIFT_NUMBER',
    'DESCRIPTION');
  if (GetStrValue(cmbPlusPlant.Value) <> '') and
    (ComboBoxPlusShift.Value <> '') then
    FillEmpl;
end;

procedure TDialogCopyFromSTAF.ComboBoxPlusShiftCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusPlant.Value <> '') and (ComboBoxPlusShift.Value <> '') then
    FillEmpl;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogCopyFromSTAF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
