(*
  MRA:13-JAN-2010. RV050.9. 889966.
  - Allow user to enter a user-name as parameter, options:
    1) -u:pims
    2) -u:               <empty)
    3) -u:%Username%
  MRA:24-FEB-2011 RV087.3. Dialog Login Change
  - Dialog Login
    - The Dialog Login is kept open all the time,
      because of 2 variables:
      - UserTeamSelectionYN
      - LoginUser
    - This must be changed by storing them in SystemDM.
  MRA:3-MAR-2014 TD-24370
  - Add param for Pims-main-app for setting the clientname
  MRA:10-NOV-2014 SO-20014826
  - Log optionally to DB. Title of application changed.
*)
program PimsORCL_DEBUG;

uses
  Forms,
  Windows,
  SysUtils,
  Dialogs,
  PimsFRM in '..\..\Pims Shared\PimsFRM.pas' {PimsF},
  TopPimsLogoFRM in '..\..\Pims Shared\TopPimsLogoFRM.pas' {TopPimsLogoF},
  TopMenuBaseFRM in '..\..\Pims Shared\TopMenuBaseFRM.pas' {TopMenuBaseF},
  SystemDMT in '..\..\Pims Shared\SystemDMT.pas' {SystemDM: TDataModule},
  AboutFRM in '..\..\Pims Shared\AboutFRM.pas' {AboutF},
  OrderInfoFRM in '..\..\Pims Shared\OrderInfoFRM.pas' {OrderInfoF},
  SideMenuBaseFRM in '..\..\Pims Shared\SideMenuBaseFRM.pas' {SideMenuBaseF},
  UPimsConst in '..\..\Pims Shared\UPimsConst.pas',
  UPimsMessageRes in '..\..\Pims Shared\UPimsMessageRes.pas',
  DialogBaseFRM in '..\..\Pims Shared\DialogBaseFRM.pas' {DialogBaseF},
  GridBaseDMT in '..\..\Pims Shared\GridBaseDMT.pas' {GridBaseDM: TDataModule},
  GridBaseFRM in '..\..\Pims Shared\GridBaseFRM.pas' {GridBaseF},
  ListProcsFRM in '..\..\Pims Shared\ListProcsFRM.pas' {ListProcsF},
  LogoPimsFRM in '..\..\Pims Shared\LogoPimsFRM.pas' {PimsLogoF},
  UGlobalFunctions in '..\..\Pims Shared\UGlobalFunctions.pas',
  UPIMSVersion in '..\..\Pims Shared\UPIMSVersion.pas',
  DialogSelectionFRM in '..\..\Pims Shared\DialogSelectionFRM.PAS' {DialogSelectionF},
  CalculateTotalHoursDMT in '..\..\Pims Shared\CalculateTotalHoursDMT.pas' {CalculateTotalHoursDM: TDataModule},
  DialogBaseDMT in '..\..\Pims Shared\DialogBaseDMT.pas' {DialogBaseDM: TDataModule},
  UScannedIDCard in '..\..\Pims Shared\UScannedIDCard.pas',
  DialogLoginDMT in '..\..\Pims Shared\DialogLoginDMT.pas' {DialogLoginDM: TDataModule},
  DialogLoginFRM in '..\..\Pims Shared\DialogLoginFRM.pas' {DialogLoginF},
  ULoginConst in '..\..\Pims Shared\ULoginConst.pas',
  EncryptIt in '..\..\Pims Shared\EncryptIt.pas',
  ReportBaseFRM in 'ReportBaseFRM.pas' {ReportBaseF},
  SideMenuUnitMaintenanceFRM in 'SideMenuUnitMaintenanceFRM.pas' {SideMenuUnitMaintenanceF},
  QualityIncentiveConfDMT in 'QualityIncentiveConfDMT.pas' {QualityIncentiveConfDM: TDataModule},
  DialogPasswordFRM in 'DialogPasswordFRM.pas' {DialogPasswordF},
  QualityIncentiveConfFRM in 'QualityIncentiveConfFRM.pas' {QualityIncentiveConfF},
  BusinessUnitFRM in 'BusinessUnitFRM.pas' {BusinessUnitF},
  BusinessUnitDMT in 'BusinessUnitDMT.pas' {BusinessUnitDM: TDataModule},
  ShiftFRM in 'ShiftFRM.pas' {ShiftF},
  ShiftDMT in 'ShiftDMT.pas' {ShiftDM: TDataModule},
  TimeBlockPerShiftFRM in 'TimeBlockPerShiftFRM.pas' {TimeBlockPerShiftF},
  TimeBlockPerShiftDMT in 'TimeBlockPerShiftDMT.pas' {TimeBlockPerShiftDM: TDataModule},
  BreakPerShiftFRM in 'BreakPerShiftFRM.pas' {BreakPerShiftF},
  BreakPerShiftDMT in 'BreakPerShiftDMT.pas' {BreakPerShiftDM: TDataModule},
  TimeBlockPerDeptFRM in 'TimeBlockPerDeptFRM.pas' {TimeBlockPerDeptF},
  TimeBlockPerDeptDMT in 'TimeBlockPerDeptDMT.pas' {TimeBlockPerDeptDM: TDataModule},
  DepartmentDMT in 'DepartmentDMT.pas' {DepartmentDM: TDataModule},
  DepartmentFRM in 'DepartmentFRM.pas' {DepartmentF},
  BreakPerDeptDMT in 'BreakPerDeptDMT.pas' {BreakPerDeptDM: TDataModule},
  BreakPerDeptFRM in 'BreakPerDeptFRM.pas' {BreakPerDeptF},
  TimeBlockPerEmplDMT in 'TimeBlockPerEmplDMT.pas' {TimeBlockPerEmplDM: TDataModule},
  TimeBlockPerEmplFRM in 'TimeBlockPerEmplFRM.pas' {TimeBlockPerEmplF},
  BreakPerEmplDMT in 'BreakPerEmplDMT.pas' {BreakPerEmplDM: TDataModule},
  BreakPerEmplFRM in 'BreakPerEmplFRM.pas' {BreakPerEmplF},
  IDCardFRM in 'IDCardFRM.pas' {IDCardF},
  IDCardDMT in 'IDCardDMT.pas' {IDCardDM: TDataModule},
  ContractGroupFRM in 'ContractGroupFRM.pas' {ContractGroupF},
  ContractGroupDMT in 'ContractGroupDMT.pas' {ContractGroupDM: TDataModule},
  EmployeeFRM in 'EmployeeFRM.pas' {EmployeeF},
  EmployeeDMT in 'EmployeeDMT.pas' {EmployeeDM: TDataModule},
  WorkSpotFRM in 'WorkSpotFRM.pas' {WorkSpotF},
  WorkSpotDMT in 'WorkSpotDMT.pas' {WorkSpotDM: TDataModule},
  BUPerWorkSpotFRM in 'BUPerWorkSpotFRM.pas' {BUPerWorkSpotF},
  ExceptHourFRM in 'ExceptHourFRM.pas' {ExceptHourF},
  ExceptHourDMT in 'ExceptHourDMT.pas' {ExceptHourDM: TDataModule},
  OvertimeDefFRM in 'OvertimeDefFRM.pas' {OvertimeDefF},
  OvertimeDefDMT in 'OvertimeDefDMT.pas' {OvertimeDefDM: TDataModule},
  TeamFRM in 'TeamFRM.pas' {TeamF},
  TeamDMT in 'TeamDMT.pas' {TeamDM: TDataModule},
  TeamDeptFRM in 'TeamDeptFRM.pas' {TeamDeptF},
  TeamDeptDMT in 'TeamDeptDMT.pas' {TeamDeptDM: TDataModule},
  WorkSpotPerStationFRM in 'WorkSpotPerStationFRM.pas' {WorkSpotPerStationF},
  WorkSpotPerStationDMT in 'WorkSpotPerStationDMT.pas' {WorkSpotPerStationDM: TDataModule},
  EmployeeContractsFRM in 'EmployeeContractsFRM.pas' {EmployeeContractsF},
  EmployeeContractsDMT in 'EmployeeContractsDMT.pas' {EmployeeContractsDM: TDataModule},
  SettingsFRM in 'SettingsFRM.pas' {SettingsF},
  SettingsDMT in 'SettingsDMT.pas' {SettingsDM: TDataModule},
  HomeFRM in 'HomeFRM.pas' {HomeF},
  SplashFRM in 'SplashFRM.pas' {SplashF},
  PimsSerialBaseFRM in 'PimsSerialBaseFRM.pas' {PimsSerialBaseF},
  DialogReportBaseFRM in 'DialogReportBaseFRM.pas' {DialogReportBaseF},
  DialogReportHoursPerEmplFRM in 'DialogReportHoursPerEmplFRM.pas' {DialogReportHoursPerEmplF},
  DialogReportAvailableUsedTimeFRM in 'DialogReportAvailableUsedTimeFRM.pas' {DialogReportAvailableUsedTimeF},
  ReportHoursPerEmplQRPT in 'ReportHoursPerEmplQRPT.pas' {ReportHoursPerEmplQR},
  ReportDirIndHrsWeekQRPT in 'ReportDirIndHrsWeekQRPT.pas' {ReportDirIndHrsWeekQR},
  DialogReportCheckListScanFRM in 'DialogReportCheckListScanFRM.pas' {DialogReportCheckListScanF},
  DialogReportRatioIllnessHrsFRM in 'DialogReportRatioIllnessHrsFRM.pas' {DialogReportRatioIllnessHrsF},
  ReportCheckListScanQRPT in 'ReportCheckListScanQRPT.pas' {ReportCheckListScanQR},
  TimeRecScanningFRM in 'TimeRecScanningFRM.pas' {TimeRecScanningF},
  TimeRecScanningDMT in 'TimeRecScanningDMT.pas' {TimeRecScanningDM: TDataModule},
  UMenuOptions in 'UMenuOptions.pas',
  DialogExportPayrollFRM in 'DialogExportPayrollFRM.pas' {DialogExportPayrollF},
  ReportBaseDMT in 'ReportBaseDMT.pas' {ReportBaseDM: TDataModule},
  ReportAvailableUsedTimeDMT in 'ReportAvailableUsedTimeDMT.pas' {ReportAvailableUsedTimeDM: TDataModule},
  ReportCheckListScanDMT in 'ReportCheckListScanDMT.pas' {ReportCheckListScanDM: TDataModule},
  ReportHoursPerEmplDMT in 'ReportHoursPerEmplDMT.pas' {ReportHoursPerEmplDM: TDataModule},
  DialogReportDirIndHrsWeekFRM in 'DialogReportDirIndHrsWeekFRM.pas' {DialogReportDirIndHrsWeekF},
  DialogReportHrsPerWKCUMFRM in 'DialogReportHrsPerWKCUMFRM.pas' {DialogReportHrsPerWKCUMF},
  ReportHrsPerWKCUMDMT in 'ReportHrsPerWKCUMDMT.pas' {ReportHrsPerWKCUMDM: TDataModule},
  ReportHrsPerWKCUMQRPT in 'ReportHrsPerWKCUMQRPT.pas' {ReportHrsPerWKCUMQR},
  ReportDirIndHrsWeekDMT in 'ReportDirIndHrsWeekDMT.pas' {ReportDirIndHrsWeekDM: TDataModule},
  ReportAvailableUsedTimeQRPT in 'ReportAvailableUsedTimeQRPT.pas' {ReportAvailableUsedTimeQR},
  DialogCalculatedEarnedWTRFRM in 'DialogCalculatedEarnedWTRFRM.pas' {DialogCalculateEarnedWTRF},
  DialogTransferFreeTimeFRM in 'DialogTransferFreeTimeFRM.pas' {DialogTransferFreeTimeF},
  DialogBankHolidayFRM in 'DialogBankHolidayFRM.pas' {DialogBankHolidayF},
  DialogReportStaffPlanDayFRM in 'DialogReportStaffPlanDayFRM.pas' {DialogReportStaffPlanDayF},
  ReportEmpAvailabilityDMT in 'ReportEmpAvailabilityDMT.pas' {ReportEmpAvailabilityDM: TDataModule},
  ReportEmpAvailabilityQRPT in 'ReportEmpAvailabilityQRPT.pas' {ReportEmpAvailabilityQR},
  DialogReportAbsenceSheduleFRM in 'DialogReportAbsenceSheduleFRM.pas' {DialogReportAbsenceSheduleF},
  ReportCompHoursQRPT in 'ReportCompHoursQRPT.pas' {ReportCompHoursQR},
  ReportCompHoursDMT in 'ReportCompHoursDMT.pas' {ReportCompHoursDM: TDataModule},
  DialogReportCompHoursFRM in 'DialogReportCompHoursFRM.pas' {DialogReportCompHoursF},
  ReportRatioIllnessHrsQRPT in 'ReportRatioIllnessHrsQRPT.pas' {ReportRatioIllnessHrsQR},
  ReportRatioIllnessHrsDMT in 'ReportRatioIllnessHrsDMT.pas' {ReportRatioIllnessHrsDM: TDataModule},
  ReportTeamIncentiveQRPT in 'ReportTeamIncentiveQRPT.pas' {ReportTeamIncentiveQR},
  DialogReportTeamIncentiveFRM in 'DialogReportTeamIncentiveFRM.pas' {DialogReportTeamIncentiveF},
  ReportTeamIncentiveDMT in 'ReportTeamIncentiveDMT.pas' {ReportTeamIncentiveDM: TDataModule},
  DialogProcessAbsenceHrsFRM in 'DialogProcessAbsenceHrsFRM.pas' {DialogProcessAbsenceHrsF},
  DialogProcessAbsenceHrsDMT in 'DialogProcessAbsenceHrsDMT.pas' {DialogProcessAbsenceHrsDM: TDataModule},
  IllnessMessagesDMT in 'IllnessMessagesDMT.pas' {IllnessMessagesDM: TDataModule},
  IllnessMessagesFRM in 'IllnessMessagesFRM.pas' {IllnessMessagesF},
  EmployeeAvailabilityFRM in 'EmployeeAvailabilityFRM.pas' {EmployeeAvailabilityF},
  EmployeeAvailabilityDMT in 'EmployeeAvailabilityDMT.pas' {EmployeeAvailabilityDM: TDataModule},
  StandardOccupationFRM in 'StandardOccupationFRM.pas' {StandardOccupationF},
  StandardOccupationDMT in 'StandardOccupationDMT.pas' {StandardOccupationDM: TDataModule},
  RevenueFRM in 'RevenueFRM.pas' {RevenueF},
  ShiftScheduleDMT in 'ShiftScheduleDMT.pas' {ShiftScheduleDM: TDataModule},
  StaffAvailabilityFRM in 'StaffAvailabilityFRM.pas' {StaffAvailabilityF},
  StaffAvailabilityDMT in 'StaffAvailabilityDMT.pas' {StaffAvailabilityDM: TDataModule},
  StandStaffAvailFRM in 'StandStaffAvailFRM.pas' {StandStaffAvailF},
  StandStaffAvailDMT in 'StandStaffAvailDMT.pas' {StandStaffAvailDM: TDataModule},
  DialogCopySelectionSHSFRM in 'DialogCopySelectionSHSFRM.pas' {DialogCopySelectionSHSF},
  DialogCopyFromSTAFRM in 'DialogCopyFromSTAFRM.pas' {DialogCopyFromSTAF},
  DialogCopyFromSHSFRM in 'DialogCopyFromSHSFRM.pas' {DialogCopyFromSHSF},
  HoursPerEmployeeFRM in 'HoursPerEmployeeFRM.pas' {HoursPerEmployeeF},
  HoursPerEmployeeDMT in 'HoursPerEmployeeDMT.pas' {HoursPerEmployeeDM: TDataModule},
  DialogWorkspotPerEmployeeFRM in 'DialogWorkspotPerEmployeeFRM.pas' {DialogWorkspotPerEmployeeF},
  StaffPlanningOCIDMT in 'StaffPlanningOCIDMT.pas' {StaffPlanningOCIDM: TDataModule},
  WorkspotPerEmployeeFRM in 'WorkspotPerEmployeeFRM.pas' {WorkspotPerEmployeeF},
  DialogWorkspotPerEmployeeDMT in 'DialogWorkspotPerEmployeeDMT.pas' {DialogWorkspotPerEmployeeDM: TDataModule},
  StaffPlanningUnit in 'StaffPlanningUnit.pas',
  StaffPlanningEMPDMT in 'StaffPlanningEMPDMT.pas' {StaffPlanningEMPDM: TDataModule},
  StaffPlanningLegendFRM in 'StaffPlanningLegendFRM.pas' {StaffPlanningLegendF},
  DialogReportEmpAvailabilityFRM in 'DialogReportEmpAvailabilityFRM.pas' {DialogReportEmpAvailabilityF},
  ReportAbsenceQRPT in 'ReportAbsenceQRPT.pas' {ReportAbsenceQR},
  ReportAbsenceDMT in 'ReportAbsenceDMT.pas' {ReportAbsenceDM: TDataModule},
  ReportStaffPlanDayQRPT in 'ReportStaffPlanDayQRPT.pas' {ReportStaffPlanDayQR},
  ReportStaffPlanDayDMT in 'ReportStaffPlanDayDMT.pas' {ReportStaffPlanDayDM: TDataModule},
  ReportStaffAvailabilityDMT in 'ReportStaffAvailabilityDMT.pas' {ReportStaffAvailabilityDM: TDataModule},
  ReportStaffAvailabilityQRPT in 'ReportStaffAvailabilityQRPT.pas' {ReportStaffAvailabilityQR},
  DialogReportStaffAvailabilityFRM in 'DialogReportStaffAvailabilityFRM.pas' {DialogReportStaffAvailabilityF},
  PaymentExportCodeDMT in 'PaymentExportCodeDMT.pas' {PaymentExportCodeDM: TDataModule},
  PaymentExportCodeFRM in 'PaymentExportCodeFRM.pas' {PaymentExportCodeF},
  RevenueDMT in 'RevenueDMT.pas' {RevenueDM: TDataModule},
  ShiftScheduleFRM in 'ShiftScheduleFRM.pas' {ShiftScheduleF},
  DialogReportQualityIncCalculationFRM in 'DialogReportQualityIncCalculationFRM.pas' {DialogReportQualityIncCalculationF},
  ReportQualityIncCalculationQRPT in 'ReportQualityIncCalculationQRPT.pas' {ReportQualityIncCalculationQR},
  ReportQualityIncCalculationDMT in 'ReportQualityIncCalculationDMT.pas' {ReportQualityIncCalculationDM: TDataModule},
  DataCollectionEntryFRM in 'DataCollectionEntryFRM.pas' {DataCollectionEntryF},
  DataCollectionEntryDMT in 'DataCollectionEntryDMT.pas' {DataCollectionEntryDM: TDataModule},
  DialogReportEmployeeFRM in 'DialogReportEmployeeFRM.pas' {DialogReportEmplF},
  DialogExportPayrollDMT in 'DialogExportPayrollDMT.pas' {DialogExportPayrollDM: TDataModule},
  DialogReportProductionCompFRM in 'DialogReportProductionCompFRM.pas' {DialogReportProductionCompF},
  ReportProdCompTargetQRPT in 'ReportProdCompTargetQRPT.pas' {ReportProdCompTargetQR},
  ReportProdCompTargetDMT in 'ReportProdCompTargetDMT.pas' {ReportProdCompTargetDM: TDataModule},
  DialogReportPlantStructure in 'DialogReportPlantStructure.pas' {DialogReportPlantStructureF},
  ReportPlantStructure in 'ReportPlantStructure.pas' {ReportPlantStructureQR},
  ReportPlantStructureDMT in 'ReportPlantStructureDMT.pas' {ReportPlantStructureDM: TDataModule},
  DataColConnectionFRM in 'DataColConnectionFRM.pas' {DataColConnectionF},
  DataColConnectionDMT in 'DataColConnectionDMT.pas' {DataColConnectionDM: TDataModule},
  ReportExportPayrollQRPT in 'ReportExportPayrollQRPT.pas' {ReportExportPayrollQR},
  ReportEmplDMT in 'ReportEmplDMT.pas' {ReportEmplDM: TDataModule},
  ReportEmplQRPT in 'ReportEmplQRPT.pas' {ReportEmplQR},
  ExtraPaymentFRM in 'ExtraPaymentFRM.pas' {ExtraPaymentF},
  ExtraPaymentDMT in 'ExtraPaymentDMT.pas' {ExtraPaymentDM: TDataModule},
  JobCodeFRM in 'JobCodeFRM.pas' {JobCodeF},
  CompareJobCodeFRM in 'CompareJobCodeFRM.pas' {CompareJobCodesF},
  DialogAddWKDATACOLLFRM in 'DialogAddWKDATACOLLFRM.pas' {DialogAddWKDATACOLLF},
  DialogAddWKWorkstationFRM in 'DialogAddWKWorkstationFRM.pas' {DialogAddWKWorkstationF},
  PlantFRM in 'PlantFRM.pas' {PlantF},
  PlantDMT in 'PlantDMT.pas' {PlantDM: TDataModule},
  MistakePerEmplDMT in 'MistakePerEmplDMT.pas' {MistakePerEmplDM: TDataModule},
  MistakePerEmplFRM in 'MistakePerEmplFRM.pas' {MistakePerEmplF},
  ComPortConnectionFRM in 'ComPortConnectionFRM.pas' {ComPortConnectionF},
  ComPortConnectionDMT in 'ComPortConnectionDMT.pas' {ComPortConnectionDM: TDataModule},
  DialogReportStaffPlanningFRM in 'DialogReportStaffPlanningFRM.pas' {DialogReportStaffPlanningF},
  ReportStaffPlanningDMT in 'ReportStaffPlanningDMT.pas' {ReportStaffPlanningDM: TDataModule},
  ReportStaffPlanningQRPT in 'ReportStaffPlanningQRPT.pas' {ReportStaffPlanningQR},
  IgnoreInterfaceCodeFRM in 'IgnoreInterfaceCodeFRM.pas' {IgnoreInterfaceCodeF},
  ReportAbsenceSheduleQRPT in 'ReportAbsenceSheduleQRPT.pas' {ReportAbsenceSheduleQR},
  ReportAbsenceSheduleDMT in 'ReportAbsenceSheduleDMT.pas' {ReportAbsenceSheduleDM: TDataModule},
  DialogReportProductionDetailFRM in 'DialogReportProductionDetailFRM.pas' {DialogReportProductionDetailF},
  ReportProductionDetailQRPT in 'ReportProductionDetailQRPT.pas' {ReportProductionDetailQR},
  ReportProductionDetailDMT in 'ReportProductionDetailDMT.pas' {ReportProductionDetailDM: TDataModule},
  ReportAbsenceCardQRPT in 'ReportAbsenceCardQRPT.pas' {ReportAbsenceCardQR},
  ReportAbsenceCardDMT in 'ReportAbsenceCardDMT.pas' {ReportAbsenceCardDM: TDataModule},
  DialogReportAbsenceCardFRM in 'DialogReportAbsenceCardFRM.pas' {DialogReportAbsenceCardF},
  DialogReportProdCompTargetFRM in 'DialogReportProdCompTargetFRM.pas' {DialogReportProdCompTargetF},
  ReportProductionQRPT in 'ReportProductionQRPT.pas' {ReportProductionQR},
  ReportProductionDMT in 'ReportProductionDMT.pas' {ReportProductionDM: TDataModule},
  ReportHrsPerDayQRPT in 'ReportHrsPerDayQRPT.pas' {ReportHrsPerDayQR},
  ReportHrsPerDayDMT in 'ReportHrsPerDayDMT.pas' {ReportHrsPerDayDM: TDataModule},
  DialogReportHrsPerDayFRM in 'DialogReportHrsPerDayFRM.pas' {DialogReportHrsPerDayF},
  DialogReportAbsenceFRM in 'DialogReportAbsenceFRM.pas' {DialogReportAbsenceF},
  DialogStaffPlanningFRM in 'DialogStaffPlanningFRM.pas' {DialogStaffPlanningF},
  DialogStaffPlanningDMT in 'DialogStaffPlanningDMT.pas' {DialogStaffPlanningDM: TDataModule},
  StaffPlanningFRM in 'StaffPlanningFRM.pas' {StaffPlanningF},
  DialogReportProductionFRM in 'DialogReportProductionFRM.pas' {DialogReportProductionF},
  ReportProductionCompQRPT in 'ReportProductionCompQRPT.pas' {ReportProductionCompQR},
  ReportProductionCompDMT in 'ReportProductionCompDMT.pas' {ReportProductionCompDM: TDataModule},
  DialogCalendarFRM in 'DialogCalendarFRM.pas',
  EmployeeRegsDMT in 'EmployeeRegsDMT.pas' {EmployeeRegsDM: TDataModule},
  EmployeeRegsFRM in 'EmployeeRegsFRM.pas' {EmployeeRegsF},
  DialogReportEmployeePaylistFRM in 'DialogReportEmployeePaylistFRM.pas' {DialogReportEmployeePaylistF},
  ReportEmployeePaylistDMT in 'ReportEmployeePaylistDMT.pas' {ReportEmployeePaylistDM: TDataModule},
  ReportEmployeePaylistQRPT in 'ReportEmployeePaylistQRPT.pas' {ReportEmployeePaylistQR},
  DialogReportHrsPerWKFRM in 'DialogReportHrsPerWKFRM.pas' {DialogReportHrsPerWKF},
  ReportHrsPerWKQRPT in 'ReportHrsPerWKQRPT.pas' {ReportHrsPerWKQR},
  ReportHrsPerWKDMT in 'ReportHrsPerWKDMT.pas' {ReportHrsPerWKDM: TDataModule},
  DialogNotCompleteDateFRM in 'DialogNotCompleteDateFRM.pas' {DialogNotCompleteDateF},
  DialogSettingsFRM in 'DialogSettingsFRM.pas' {DialogSettingsF},
  DialogReportStaffPlanningCondFRM in 'DialogReportStaffPlanningCondFRM.pas' {DialogReportStaffPlanningCondF},
  ReportStaffPlanningCondDMT in 'ReportStaffPlanningCondDMT.pas' {ReportStaffPlanningCondDM: TDataModule},
  ReportStaffPlanningCondQRPT in 'ReportStaffPlanningCondQRPT.pas' {ReportStaffPlanningCondQR},
  DialogMonthlyGroupEfficiencyFRM in 'DialogMonthlyGroupEfficiencyFRM.pas' {DialogMonthlyGroupEfficiencyF},
  ReportMonthlyGroupEfficiencyQRPT in 'ReportMonthlyGroupEfficiencyQRPT.pas' {ReportMonthlyGroupEfficiencyQR},
  ReportExportPayrollSRQRPT in 'ReportExportPayrollSRQRPT.pas' {ReportExportPayrollSRQR},
  DialogSelectionCopyFromOWFRM in 'DialogSelectionCopyFromOWFRM.pas' {DialogSelectionCopyFromOWF},
  GlobalDMT in '..\..\Pims Shared\GlobalDMT.pas' {GlobalDM: TDataModule},
  DialogMonthlyEmployeeEfficiencyFRM in 'DialogMonthlyEmployeeEfficiencyFRM.pas' {DialogMonthlyEmployeeEfficiencyF},
  DialogMonthlyEmployeeEfficiencyDMT in 'DialogMonthlyEmployeeEfficiencyDMT.pas' {DialogMonthlyEmployeeEfficiencyDM: TDataModule},
  DialogMonthlyGroupEfficiencyDMT in 'DialogMonthlyGroupEfficiencyDMT.pas' {DialogMonthlyGroupEfficiencyDM: TDataModule},
  ReportMonthlyEmployeeEfficiencyQRPT in 'ReportMonthlyEmployeeEfficiencyQRPT.pas' {ReportMonthlyEmployeeEfficiencyQR},
  TypeHourDMT in 'TypeHourDMT.pas' {TypeHourDM: TDataModule},
  TypeHourFRM in 'TypeHourFRM.pas' {TypeHourF},
  AbsTypeDMT in 'AbsTypeDMT.pas' {AbsTypeDM: TDataModule},
  AbsTypeFRM in 'AbsTypeFRM.pas' {AbsTypeF},
  AbsReasonDMT in 'AbsReasonDMT.pas' {AbsReasonDM: TDataModule},
  AbsReasonFRM in 'AbsReasonFRM.pas' {AbsReasonF},
  MachineDMT in 'MachineDMT.pas' {MachineDM: TDataModule},
  MachineFRM in 'MachineFRM.pas' {MachineF},
  MachineJobCodeFRM in 'MachineJobCodeFRM.pas' {MachineJobCodeF},
  MachineWorkspotFRM in 'MachineWorkspotFRM.pas' {MachineWorkspotF},
  CountriesDMT in 'CountriesDMT.pas' {CountriesDM: TDataModule},
  CountriesFRM in 'CountriesFRM.pas' {CountriesF},
  AbsTypePerCountryDMT in 'AbsTypePerCountryDMT.pas' {AbsTypePerCountryDM: TDataModule},
  AbsTypePerCountryFRM in 'AbsTypePerCountryFRM.pas' {AbsTypePerCountryF},
  AbsReasonPerCountryDMT in 'AbsReasonPerCountryDMT.pas' {AbsReasonPerCountryDM: TDataModule},
  AbsReasonPerCountryFRM in 'AbsReasonPerCountryFRM.pas' {AbsReasonPerCountryF},
  TypeHourPerCountryDMT in 'TypeHourPerCountryDMT.pas' {TypeHourPerCountryDM: TDataModule},
  TypeHourPerCountryFRM in 'TypeHourPerCountryFRM.pas' {TypeHourPerCountryF},
  MultipleSelectFRM in 'MultipleSelectFRM.pas' {DialogMultipleSelectF},
  EmployeeRegsBaseFRM in 'EmployeeRegsBaseFRM.pas' {EmployeeRegsBaseF},
  MessageDialogFRM in 'MessageDialogFRM.pas' {MessageDialogF},
  DialogReportHrsPerContractGrpCUMFRM in 'DialogReportHrsPerContractGrpCUMFRM.pas' {DialogReportHrsPerContractGrpCUMF},
  ReportHrsPerContractGrpCUMDMT in 'ReportHrsPerContractGrpCUMDMT.pas' {ReportHrsPerContractGrpCUMDM: TDataModule},
  ReportHrsPerContractGrpCUMQRPT in 'ReportHrsPerContractGrpCUMQRPT.pas' {ReportHrsPerContractGrpCUMQR},
  DialogReportHolidayCardFRM in 'DialogReportHolidayCardFRM.pas' {DialogReportHolidayCardF},
  ReportHolidayCardDMT in 'ReportHolidayCardDMT.pas' {ReportHolidayCardDM: TDataModule},
  ReportHolidayCardQRPT in 'ReportHolidayCardQRPT.pas' {ReportHolidayCardQR},
  DialogReportChangesPerScanFRM in 'DialogReportChangesPerScanFRM.pas' {DialogReportChangesPerScanF},
  ReportChangesPerScanDMT in 'ReportChangesPerScanDMT.pas' {ReportChangesPerScanDM: TDataModule},
  ReportChangesPerScanQRPT in 'ReportChangesPerScanQRPT.pas' {ReportChangesPerScanQR},
  DialogReportEmployeeIllnessFRM in 'DialogReportEmployeeIllnessFRM.pas' {DialogReportEmployeeIllnessF},
  ReportEmployeeIllnessDMT in 'ReportEmployeeIllnessDMT.pas' {ReportEmployeeIllnessDM: TDataModule},
  ReportEmployeeIllnessQRPT in 'ReportEmployeeIllnessQRPT.pas' {ReportEmployeeIllnessQR},
  DialogReportIncentiveProgramFRM in 'DialogReportIncentiveProgramFRM.pas' {DialogReportIncentiveProgramF},
  ReportIncentiveProgramDMT in 'ReportIncentiveProgramDMT.pas' {ReportIncentiveProgramDM: TDataModule},
  ReportIncentiveProgramQRPT in 'ReportIncentiveProgramQRPT.pas' {ReportIncentiveProgramQR},
  DialogLinkJobFRM in 'DialogLinkJobFRM.pas' {DialogLinkJobF},
  PlantFinalRunFRM in 'PlantFinalRunFRM.pas' {PlantFinalRunF},
  ErrorLogDMT in 'ErrorLogDMT.pas' {ErrorLogDM: TDataModule},
  ErrorLogFRM in 'ErrorLogFRM.pas' {ErrorLogF},
  UTimeZone in '..\..\Pims Shared\UTimeZone.pas',
  UEditableCalcDateTimeField in '..\..\Pims Shared\UEditableCalcDateTimeField.pas',
  DialogAddHolidayFRM in 'DialogAddHolidayFRM.pas' {DialogAddHolidayF},
  DialogAddHolidayDMT in 'DialogAddHolidayDMT.pas' {DialogAddHolidayDM: TDataModule},
  DialogReportJobCommentsFRM in 'DialogReportJobCommentsFRM.pas' {DialogReportJobCommentsF},
  ReportJobCommentsDMT in 'ReportJobCommentsDMT.pas' {ReportJobCommentsDM: TDataModule},
  ReportJobCommentsQRPT in 'ReportJobCommentsQRPT.pas' {ReportJobCommentsQR},
  WorkScheduleDMT in 'WorkScheduleDMT.pas' {WorkScheduleDM: TDataModule},
  WorkScheduleFRM in 'WorkScheduleFRM.pas' {WorkScheduleF},
  WorkScheduleDetailsDMT in 'WorkScheduleDetailsDMT.pas' {WorkScheduleDetailsDM: TDataModule},
  WorkScheduleDetailsFRM in 'WorkScheduleDetailsFRM.pas' {WorkScheduleDetailsF},
  DialogProcessWorkScheduleDMT in 'DialogProcessWorkScheduleDMT.pas' {DialogProcessWorkScheduleDM: TDataModule},
  DialogProcessWorkScheduleFRM in 'DialogProcessWorkScheduleFRM.pas' {DialogProcessWorkScheduleF},
  WorkScheduleProcessDMT in 'WorkScheduleProcessDMT.pas' {WorkScheduleProcessDM: TDataModule},
  DialogReportEmpInfoDevPeriodFRM in 'DialogReportEmpInfoDevPeriodFRM.pas' {DialogReportEmpInfoDevPeriodF},
  ReportEmpInfoDevPeriodDMT in 'ReportEmpInfoDevPeriodDMT.pas' {ReportEmpInfoDevPeriodDM: TDataModule},
  ReportEmpInfoDevPeriodQRPT in 'ReportEmpInfoDevPeriodQRPT.pas' {ReportEmpInfoDevPeriodQR},
  RealTimeEffMonitorDMT in 'RealTimeEffMonitorDMT.pas' {RealTimeEffMonitorDM: TDataModule},
  RealTimeEffMonitorFRM in 'RealTimeEffMonitorFRM.pas' {RealTimeEffMonitorF},
  DialogReportGhostCountsFRM in 'DialogReportGhostCountsFRM.pas' {DialogReportGhostCountsF},
  ReportGhostCountsDMT in 'ReportGhostCountsDMT.pas' {ReportGhostCountsDM: TDataModule},
  ReportGhostCountsQRPT in 'ReportGhostCountsQRPT.pas' {ReportGhostCountsQR},
  DialogSelectPrinterFRM in 'DialogSelectPrinterFRM.pas' {DialogSelectPrinterF},
  RealTimeEffDMT in 'RealTimeEffDMT.pas' {RealTimeEffDM: TDataModule},
  DialogCopyEmpDataDMT in 'DialogCopyEmpDataDMT.pas' {DialogCopyEmpDataDM: TDataModule},
  DialogCopyEmpDataFRM in 'DialogCopyEmpDataFRM.pas' {DialogCopyEmpDataF},
  DialogReportInOutAbsenceOvertimeFRM in 'DialogReportInOutAbsenceOvertimeFRM.pas' {DialogReportInOutAbsenceOvertimeF},
  ReportInOutAbsenceOvertimeDMT in 'ReportInOutAbsenceOvertimeDMT.pas' {ReportInOutAbsenceOvertimeDM: TDataModule},
  ReportInOutAbsenceOvertimeQRPT in 'ReportInOutAbsenceOvertimeQRPT.pas' {ReportInOutAbsenceOvertimeQR},
  PTODefDMT in 'PTODefDMT.pas' {PTODefDM: TDataModule},
  PTODefFRM in 'PTODefFRM.pas' {PTODefF};

var
  LoginUserFound: Boolean;
  PasswordOK: Boolean;

// How to get an alternative CLIENTNAME based on param?
function NewClientName: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
  function MySetEnvironmentVariable(AName, AValue: String): Boolean;
  begin
    Result := SetEnvironmentVariable(PChar(AName), PChar(AValue));
  end;
begin
  Result := '';
  Param1 := '-C:';
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      Break;
    end;
  end;
  if Result <> '' then
    MySetEnvironmentVariable('CLIENTNAME', Result);
end;

// RV050.9. Auto login option. get the user from parameter, for example:
//          -u:pims
//          -u:           (empty)
//          -u:%Username%
function AutoLoginUser: String;
var
  I, IPos: Integer;
  Param1, PStr: String;
begin
  Result := '';
  Param1 := '-U:';
  LoginUserFound := False;
  for I := 1 to ParamCount do
  begin
    PStr := UpperCase(ParamStr(I));
    IPos := Pos(Param1, PStr);
    if IPos > 0 then
    begin
      Result := Copy(PStr, IPos + Length(Param1), Length(Pstr));
      LoginUserFound := True;
      Break;
    end;
  end;
end;

{$R *.RES}
begin
  Application.Initialize;
  SplashF := TSplashF.Create(Application);
  Starting := True;
  SplashF.Show;
  SplashF.Update;
  Application.Title := 'ORCL - PIMS - UNIT MAINTENANCE';  // SO-20014826 Change title for logging?
  NewClientName;
  Application.CreateForm(TSystemDM, SystemDM);
  Application.CreateForm(TRealTimeEffDM, RealTimeEffDM);
  SystemDM.AppName := 'Pims';
//  Application.CreateForm(TDialogLoginDM, DialogLoginDM); // 20014450.50
  DialogLoginF := TDialogLoginF.Create(Application);
  // RV050.9. Auto Login.
  AutoLoginUser;
  if LoginUserFound then
  begin
//    DialogLoginF.AutoLogin := False;
    DialogLoginF.LoginUserParam := True;
    DialogLoginF.AutoLoginUser := AutoLoginUser;
  end
  else
  begin
//    DialogLoginF.AutoLogin := False;
    DialogLoginF.LoginUserParam := False;
    DialogLoginF.AutoLoginUser := '';
  end;
  //verifyPassword and login as SYSDBA
  PasswordOK := DialogLoginF.VerifyPassword;
  DialogLoginF.Hide;
  DialogLoginF.Close;
  DialogLoginF.Release;

  if PasswordOK then
  begin
    //update database as PIMS user
    if SystemDM.UpdatePimsBase('Pims') then
    begin
      Application.CreateForm(TGlobalDM, GlobalDM);
      Application.CreateForm(TCalculateTotalHoursDM, CalculateTotalHoursDM);

      // 20015220 + 20015221 Open this here and only once! It is used by multiple reports.
      Application.CreateForm(TReportProductionDetailDM, ReportProductionDetailDM);

      Application.CreateForm(THomeF, HomeF);
      Application.CreateForm(TSideMenuUnitMaintenanceF, SideMenuUnitMaintenanceF);
    end;
  end;

  SplashF.Hide;
  SplashF.Close;
  SplashF.Release;
  Starting := False;
  Application.Run;
end.
