inherited RevenueDM: TRevenueDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    TableName = 'BUSINESSUNIT'
    Left = 76
    Top = 24
  end
  inherited TableDetail: TTable
    AfterScroll = TableDetailAfterScroll
    OnNewRecord = TableDetailNewRecord
    Filtered = True
    OnFilterRecord = TableDetailFilterRecord
    MasterSource = nil
    TableName = 'REVENUE'
    Left = 76
    Top = 92
    object TableDetailBUSINESSUNIT_CODE: TStringField
      FieldName = 'BUSINESSUNIT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailREVENUE_DATE: TDateTimeField
      FieldName = 'REVENUE_DATE'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailREVENUE_AMOUNT: TFloatField
      FieldName = 'REVENUE_AMOUNT'
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 192
    Top = 24
  end
  inherited DataSourceDetail: TDataSource
    Top = 92
  end
  object TableBU: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BUSINESSUNIT'
    Left = 200
    Top = 160
  end
  object TableRevenue: TTable
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'REVENUE'
    Left = 80
    Top = 160
  end
  object QueryWork: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO REVENUE (BUSINESSUNIT_CODE, REVENUE_DATE,'
      'REVENUE_ AMOUNT,  CREATIONDATE,  MUTATIONDATE, MUTATOR) '
      'VALUES( :BUSINESSUNIT_CODE, :REVENUE_DATE,'
      ':REVENUE_AMOUNT,  :CREATIONDATE, :MUTATIONDATE, :MUTATOR)')
    Left = 64
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'BUSINESSUNIT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'REVENUE_DATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'REVENUE_AMOUNT'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'MUTATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
end
