(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
*)
unit ReportEmplDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportEmplDM = class(TReportBaseDM)
    TablePlant: TTable;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableEmployee: TTable;
    TableEmployeeEMPLOYEE_NUMBER: TIntegerField;
    TableEmployeeSHORT_NAME: TStringField;
    TableEmployeePLANT_CODE: TStringField;
    TableEmployeeDESCRIPTION: TStringField;
    TableEmployeeDEPARTMENT_CODE: TStringField;
    TableEmployeeCREATIONDATE: TDateTimeField;
    TableEmployeeMUTATIONDATE: TDateTimeField;
    TableEmployeeMUTATOR: TStringField;
    TableEmployeeADDRESS: TStringField;
    TableEmployeeZIPCODE: TStringField;
    TableEmployeeCITY: TStringField;
    TableEmployeeSTATE: TStringField;
    TableEmployeeLANGUAGE_CODE: TStringField;
    TableEmployeePHONE_NUMBER: TStringField;
    TableEmployeeEMAIL_ADDRESS: TStringField;
    TableEmployeeDATE_OF_BIRTH: TDateTimeField;
    TableEmployeeSEX: TStringField;
    TableEmployeeSOCIAL_SECURITY_NUMBER: TStringField;
    TableEmployeeSTARTDATE: TDateTimeField;
    TableEmployeeTEAM_CODE: TStringField;
    TableEmployeeENDDATE: TDateTimeField;
    TableEmployeeCONTRACTGROUP_CODE: TStringField;
    TableEmployeeIS_SCANNING_YN: TStringField;
    TableEmployeeCUT_OF_TIME_YN: TStringField;
    TableEmployeeREMARK: TStringField;
    TableEmployeeCALC_BONUS_YN: TStringField;
    TableEmployeeFIRSTAID_YN: TStringField;
    TableEmployeeFIRSTAID_EXP_DATE: TDateTimeField;
    QueryEmpl: TQuery;
    DataSourceQryEmpl: TDataSource;
    DataSourceEmpl: TDataSource;
    DataSourcePlant: TDataSource;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEmplFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportEmplDM: TReportEmplDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TReportEmplDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEmpl);
end;

procedure TReportEmplDM.QueryEmplFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
