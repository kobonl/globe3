inherited WorkSpotF: TWorkSpotF
  Left = 525
  Top = 187
  Width = 673
  Height = 486
  Caption = 'Workspots'
  Menu = MainMenu1
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 657
    Height = 111
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Top = 107
      Width = 655
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 655
      Height = 106
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Plants'
          Width = 797
        end>
      KeyField = 'PLANT_CODE'
      DataSource = WorkSpotDM.DataSourceMaster
      ShowBands = True
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANT_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 292
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnCity: TdxDBGridColumn
        Caption = 'City'
        Width = 190
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CITY'
      end
      object dxMasterGridColumnState: TdxDBGridColumn
        Caption = 'State'
        Width = 144
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STATE'
      end
      object dxMasterGridColumnPhone: TdxDBGridColumn
        Caption = 'Phone'
        Width = 82
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PHONE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 240
    Width = 657
    Height = 207
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 655
      Height = 205
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'General'
        object LabelCode: TLabel
          Left = 8
          Top = 4
          Width = 25
          Height = 13
          Caption = 'Code'
        end
        object Label3: TLabel
          Left = 152
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Short name'
        end
        object Label2: TLabel
          Left = 8
          Top = 28
          Width = 53
          Height = 13
          Caption = 'Description'
        end
        object Label1: TLabel
          Left = 8
          Top = 56
          Width = 63
          Height = 13
          Caption = 'Date inactive'
        end
        object LabelDepartment: TLabel
          Left = 8
          Top = 82
          Width = 57
          Height = 13
          Caption = 'Department'
        end
        object LabelHourType: TLabel
          Left = 8
          Top = 107
          Width = 48
          Height = 13
          Caption = 'Hour type'
        end
        object LabelMachine: TLabel
          Left = 8
          Top = 133
          Width = 39
          Height = 13
          Caption = 'Machine'
        end
        object LabelContractGroup: TLabel
          Left = 8
          Top = 157
          Width = 63
          Height = 13
          Caption = 'Contr. Group'
        end
        object LabelGrade: TLabel
          Left = 372
          Top = 133
          Width = 29
          Height = 13
          Caption = 'Grade'
        end
        object DBEditCode: TDBEdit
          Tag = 1
          Left = 80
          Top = 3
          Width = 61
          Height = 19
          Ctl3D = False
          DataField = 'WORKSPOT_CODE'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 0
          OnChange = DBEditCodeChange
        end
        object DBEditShortName: TDBEdit
          Tag = 1
          Left = 224
          Top = 3
          Width = 137
          Height = 19
          Ctl3D = False
          DataField = 'SHORT_NAME'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 1
          OnEnter = DBEditShortNameEnter
        end
        object DBEditDescription: TDBEdit
          Tag = 1
          Left = 80
          Top = 28
          Width = 281
          Height = 19
          Ctl3D = False
          DataField = 'DESCRIPTION'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 2
          OnEnter = DBEditDescriptionEnter
        end
        object dxDBDateEdit1: TdxDBDateEdit
          Left = 80
          Top = 53
          Width = 121
          TabOrder = 3
          DataField = 'DATE_INACTIVE'
          DataSource = WorkSpotDM.DataSourceDetail
        end
        object DBLookupComboBoxDep: TDBLookupComboBox
          Tag = 1
          Left = 81
          Top = 79
          Width = 280
          Height = 21
          DataField = 'DEPARTMENT_CODE'
          DataSource = WorkSpotDM.DataSourceDetail
          KeyField = 'DEPARTMENT_CODE'
          ListField = 'DESCRIPTION;DEPARTMENT_CODE'
          ListSource = WorkSpotDM.DataSourceDeptLU
          TabOrder = 4
        end
        object dxDBLookupEdit2: TdxDBLookupEdit
          Left = 80
          Top = 104
          Width = 281
          Style.BorderStyle = xbs3D
          TabOrder = 5
          DataField = 'HOURTYPELU'
          DataSource = WorkSpotDM.DataSourceDetail
          DropDownRows = 4
          ClearKey = 46
          ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
        end
        object dxDBLookupEdit1: TdxDBLookupEdit
          Left = 80
          Top = 128
          Width = 281
          TabOrder = 6
          DataField = 'MACHINELU'
          DataSource = WorkSpotDM.DataSourceDetail
          ClearKey = 46
          ListFieldName = 'DESCRIPTION;MACHINE_CODE'
        end
        object dxDBLookupEditContractGroup: TdxDBLookupEdit
          Left = 80
          Top = 153
          Width = 281
          Style.BorderStyle = xbs3D
          TabOrder = 7
          DataField = 'CONTRACTLU'
          DataSource = WorkSpotDM.DataSourceDetail
          DropDownRows = 6
          DropDownWidth = 240
          ClearKey = 46
          ListFieldName = 'DESCRIPTION;CONTRACTGROUP_CODE'
        end
        object DBCheckBoxGroupEfficiency: TDBCheckBox
          Left = 372
          Top = 154
          Width = 105
          Height = 17
          Caption = 'Group Efficiency'
          Ctl3D = False
          DataField = 'GROUP_EFFICIENCY_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 14
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object DBEditGrade: TDBEdit
          Left = 408
          Top = 131
          Width = 65
          Height = 19
          Ctl3D = False
          DataField = 'GRADE'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 13
        end
        object DBCheckBoxAutRescan: TDBCheckBox
          Tag = 1
          Left = 372
          Top = 110
          Width = 109
          Height = 17
          Caption = 'Automatic rescan'
          Ctl3D = False
          DataField = 'AUTOMATIC_RESCAN_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 12
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxAutRescanClick
          OnKeyDown = DBCheckBoxAutRescanKeyDown
          OnMouseDown = DBCheckBoxAutRescanMouseDown
        end
        object DBCheckBoxAutomaticDC: TDBCheckBox
          Tag = 1
          Left = 372
          Top = 84
          Width = 117
          Height = 17
          Caption = 'Automatic data col.'
          Ctl3D = False
          DataField = 'AUTOMATIC_DATACOL_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 11
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxAutomaticDCClick
          OnKeyDown = DBCheckBoxAutomaticDCKeyDown
          OnMouseDown = DBCheckBoxAutomaticDCMouseDown
        end
        object DBCheckBoxMeasureProd: TDBCheckBox
          Tag = 1
          Left = 372
          Top = 58
          Width = 125
          Height = 17
          Caption = 'Measure productivity'
          Ctl3D = False
          DataField = 'MEASURE_PRODUCTIVITY_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 10
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxMeasureProdClick
          OnKeyDown = DBCheckBoxMeasureProdKeyDown
          OnMouseDown = DBCheckBoxMeasureProdMouseDown
        end
        object DBCheckBoxJobCode_YN: TDBCheckBox
          Tag = 1
          Left = 372
          Top = 32
          Width = 93
          Height = 17
          Caption = 'Use job codes'
          Ctl3D = False
          DataField = 'USE_JOBCODE_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 9
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
          OnClick = DBCheckBoxJobCode_YNClick
          OnKeyDown = DBCheckBoxJobCode_YNKeyDown
          OnMouseDown = DBCheckBoxJobCode_YNMouseDown
        end
        object DBCheckBoxProductive: TDBCheckBox
          Left = 372
          Top = 6
          Width = 105
          Height = 17
          Caption = 'Productive hours'
          Ctl3D = False
          DataField = 'PRODUCTIVE_HOUR_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          ParentCtl3D = False
          TabOrder = 8
          ValueChecked = 'Y'
          ValueUnchecked = 'N'
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 498
          Top = -3
          Width = 143
          Height = 31
          Caption = 'Quantities'
          Columns = 2
          DataField = 'QUANT_PIECE_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          Items.Strings = (
            'Pieces'
            'Weight')
          TabOrder = 15
          Values.Strings = (
            'Y'
            'N')
        end
        object ButtonShowForm: TButton
          Left = 472
          Top = 28
          Width = 169
          Height = 24
          Caption = 'Job code'
          TabOrder = 16
          OnClick = ButtonShowFormClick
        end
        object btnShifts: TButton
          Left = 522
          Top = 106
          Width = 119
          Height = 23
          Caption = 'Shifts'
          TabOrder = 18
          OnClick = btnShiftsClick
        end
        object GroupBox1: TGroupBox
          Left = 505
          Top = 128
          Width = 136
          Height = 47
          TabOrder = 19
          object DBCheckBoxCounterValue: TDBCheckBox
            Tag = 1
            Left = 6
            Top = 28
            Width = 93
            Height = 17
            Caption = 'Counter value'
            Ctl3D = False
            DataField = 'COUNTER_VALUE_YN'
            DataSource = WorkSpotDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 1
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
            OnClick = DBCheckBoxCounterValueClick
            OnKeyDown = DBCheckBoxCounterValueKeyDown
            OnMouseDown = DBCheckBoxCounterValueMouseDown
          end
          object DBCheckBoxEnterCounter: TDBCheckBox
            Tag = 1
            Left = 6
            Top = 10
            Width = 127
            Height = 17
            Caption = 'Enter counter at scan'
            Ctl3D = False
            DataField = 'ENTER_COUNTER_AT_SCAN_YN'
            DataSource = WorkSpotDM.DataSourceDetail
            ParentCtl3D = False
            TabOrder = 0
            ValueChecked = 'Y'
            ValueUnchecked = 'N'
            OnClick = DBCheckBoxEnterCounterClick
            OnKeyDown = DBCheckBoxEnterCounterKeyDown
            OnMouseDown = DBCheckBoxEnterCounterMouseDown
          end
        end
        object DBRgrpEffBasedOn: TDBRadioGroup
          Left = 522
          Top = 52
          Width = 119
          Height = 53
          Caption = 'Efficiency based on'
          DataField = 'EFF_RUNNING_HRS_YN'
          DataSource = WorkSpotDM.DataSourceDetail
          Items.Strings = (
            'Manhours'
            'Running hours')
          TabOrder = 17
          Values.Strings = (
            'N'
            'Y')
          OnChange = DBRgrpEffBasedOnChange
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Additional'
        ImageIndex = 1
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 323
          Height = 177
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object gboxPersonalScreen: TGroupBox
            Left = 0
            Top = 0
            Width = 323
            Height = 107
            Align = alTop
            Caption = 'Personal Screen'
            TabOrder = 0
            object LblFakeEmpNr: TLabel
              Left = 8
              Top = 80
              Width = 112
              Height = 13
              Caption = 'Fake Employee Number'
              Visible = False
            end
            object DBRGrpTimeRecBasedOn: TDBRadioGroup
              Left = 2
              Top = 15
              Width = 319
              Height = 58
              Align = alTop
              Caption = 'Timerecording based on'
              DataField = 'TIMEREC_BY_MACHINE_YN'
              DataSource = WorkSpotDM.DataSourceDetail
              Items.Strings = (
                'Machine'
                'Employee')
              TabOrder = 0
              Values.Strings = (
                'Y'
                'N')
              OnChange = DBRGrpTimeRecBasedOnChange
            end
            object DBEditFakeEmpNr: TDBEdit
              Tag = 1
              Left = 176
              Top = 78
              Width = 137
              Height = 19
              Ctl3D = False
              DataField = 'FAKE_EMPLOYEE_NUMBER'
              DataSource = WorkSpotDM.DataSourceDetail
              MaxLength = 9
              ParentCtl3D = False
              TabOrder = 1
              Visible = False
              OnChange = DBEditCodeChange
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 107
            Width = 323
            Height = 70
            Align = alClient
            Caption = 'Personal Screen'
            TabOrder = 1
            object DBRadioGroup2: TDBRadioGroup
              Left = 2
              Top = 15
              Width = 319
              Height = 53
              Align = alClient
              Caption = 'Receive qty via'
              DataField = 'PS_QTYVIA_EXTERNAL_YN'
              DataSource = WorkSpotDM.DataSourceDetail
              Items.Strings = (
                'Blackbox'
                'External application')
              TabOrder = 0
              Values.Strings = (
                'N'
                'Y')
            end
          end
        end
        object Panel2: TPanel
          Left = 323
          Top = 0
          Width = 324
          Height = 177
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object gBoxCardReader: TGroupBox
            Left = 0
            Top = 0
            Width = 324
            Height = 81
            Align = alTop
            Caption = 'Card Reader'
            TabOrder = 0
            Visible = False
            object Label4: TLabel
              Left = 8
              Top = 19
              Width = 22
              Height = 13
              Caption = 'Host'
            end
            object Label5: TLabel
              Left = 184
              Top = 19
              Width = 20
              Height = 13
              Caption = 'Port'
            end
            object Label6: TLabel
              Left = 8
              Top = 46
              Width = 27
              Height = 13
              Caption = 'Batch'
            end
            object dxDBEdit1: TdxDBEdit
              Left = 56
              Top = 16
              Width = 121
              Style.BorderStyle = xbsSingle
              TabOrder = 0
              DataField = 'HOST'
              DataSource = WorkSpotDM.DataSourceDetail
            end
            object dxDBEdit2: TdxDBEdit
              Left = 251
              Top = 14
              Width = 65
              Style.BorderStyle = xbsSingle
              TabOrder = 1
              DataField = 'PORT'
              DataSource = WorkSpotDM.DataSourceDetail
            end
            object dxDBSpinEdit1: TdxDBSpinEdit
              Left = 58
              Top = 43
              Width = 63
              TabOrder = 2
              DataField = 'BATCH'
              DataSource = WorkSpotDM.DataSourceDetail
              MaxValue = 99
              StoredValues = 48
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = 81
            Width = 324
            Height = 96
            Align = alClient
            TabOrder = 1
            object DBCheckBoxRoaming: TDBCheckBox
              Left = 11
              Top = 6
              Width = 294
              Height = 17
              Caption = 'Roaming Workspot'
              Ctl3D = False
              DataField = 'ROAMING_YN'
              DataSource = WorkSpotDM.DataSourceDetail
              ParentCtl3D = False
              TabOrder = 0
              ValueChecked = 'Y'
              ValueUnchecked = 'N'
            end
          end
        end
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 137
    Width = 657
    Height = 103
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 1
      Width = 655
      Height = 101
      Cursor = crHSplit
      Align = alClient
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 655
      Height = 101
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Workspots'
          Width = 1395
        end>
      KeyField = 'WORKSPOT_CODE'
      DataSource = WorkSpotDM.DataSourceDetail
      ShowBands = True
      object dxDetailGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 75
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WORKSPOT_CODE'
      end
      object dxDetailGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 208
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumnDepartment: TdxDBGridCalcColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 140
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTLU'
      end
      object dxDetailGridColumnHourType: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 77
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
        ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
      end
      object dxDetailGridColumnInactiveDate: TdxDBGridDateColumn
        Caption = 'Date inactive'
        Width = 78
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DATE_INACTIVE'
      end
      object dxDetailGridColumnProductiveHour: TdxDBGridCheckColumn
        Caption = 'Productive hours'
        Width = 89
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PRODUCTIVE_HOUR_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnUseJobCodes: TdxDBGridCheckColumn
        Caption = 'Use job codes'
        DisableEditor = True
        Width = 81
        BandIndex = 0
        RowIndex = 0
        FieldName = 'USE_JOBCODE_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnMeasureProd: TdxDBGridCheckColumn
        Caption = 'Masure productivity'
        Width = 101
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MEASURE_PRODUCTIVITY_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnAutomatic: TdxDBGridCheckColumn
        Caption = 'Automatic date col.'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_DATACOL_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnCounter: TdxDBGridCheckColumn
        Caption = 'Counter value'
        Width = 77
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNTER_VALUE_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnEnter: TdxDBGridCheckColumn
        Caption = 'Enter counter at scan'
        Width = 113
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENTER_COUNTER_AT_SCAN_YN'
        ValueChecked = 'Y'
        ValueGrayed = 'N'
        ValueUnchecked = 'N'
        DisplayChecked = 'Y'
        DisplayUnChecked = 'N'
        DisplayNull = 'N'
      end
      object dxDetailGridColumnAutRescan: TdxDBGridCheckColumn
        Caption = 'Automatic rescan'
        MinWidth = 20
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'AUTOMATIC_RESCAN_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnShortName: TdxDBGridColumn
        Caption = 'Short name'
        MinWidth = 0
        Visible = False
        Width = 90
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SHORT_NAME'
      end
      object dxDetailGridColumnGroupEfficiency: TdxDBGridCheckColumn
        Caption = 'Group Efficiency'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'GROUP_EFFICIENCY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnMachine: TdxDBGridLookupColumn
        Caption = 'Machine'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MACHINELU'
        ListFieldName = 'Description'
      end
      object dxDetailGridColumnTRByMachine: TdxDBGridCalcColumn
        Caption = 'Timerec. by'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIMEREC_BY_MACHINE_YN'
        OnGetText = dxDetailGridColumnTRByMachineGetText
      end
      object dxDetailGridColumnFakeEmpNr: TdxDBGridColumn
        Caption = 'Fake Emp Nr'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FAKE_EMPLOYEE_NUMBER'
      end
      object dxDetailGridColumnHost: TdxDBGridColumn
        Caption = 'Host'
        DisableCustomizing = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOST'
      end
      object dxDetailGridColumnPort: TdxDBGridColumn
        Caption = 'Port'
        DisableCustomizing = True
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PORT'
      end
      object dxDetailGridColumnBatch: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Batch'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BATCH'
      end
      object dxDetailGridColumnRoaming: TdxDBGridCheckColumn
        Caption = 'Roaming'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ROAMING_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dsrcActive: TDataSource
    Left = 0
  end
  object MainMenu1: TMainMenu
    Left = 648
    Top = 81
  end
end
