(*
  MRA: 12-FEB-2009 RV022.
    Bugfix for main-select. Compare with DEPARTMENTPERTEAM was wrong, resulting
    in multiple records, because there can be multiple departments per team in
    this table.
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:25-NOV-2011 RV102.1. 20012378. Bugfix.
  - Report Productivity compared with target.
    - When printing to PDF or export it shows
      different result. Some values like machine hours
      are doubled.
  MRA:8-MAY-2012 20012858. Production Reports and NOJOB.
  - Filter out the NOJOB-job to prevent wrong quantities when
    this job has a negative quantity.
  MRA:2-AUG-2012 20013478.
  - Make it possible to exclude certain jobs in production reports.
  - When a job (JOBCODE-table) has field IGNOREQUANTITIESINREPORTS_YN set
    to 'Y' then the report should show no quantities for that job, only
    the time must be shown.
  MRA:22-OCT-2012 2013489 Overnight-Shift-System
  - This report gets data from production-quantity, but must use SHIFT_DATA-
    field instead of START_DATE for comparing with selection-dates.
  MRA:25-FEB-2013 TD-21554 Bugfix
  - Sometimes the machine hours are not shown.
    - Reasons it did not work:
      - The machine-hours were added during Workspot-group, but
        it must be done in Detail-part, or it misses records!
      - The machine-hours were added as Integer, but it must be
        added as Float/Double.
      - The machine-hrs per job were determined during Job-group, but
        it must be done during Job-footer, or it will miss values
        determined during Detail-part.
  - IMPORTANT: The query gets values for production quantities AND production
               hours. Because of this it will give 1 or 2 records (per job):
               - 1 record when either production quantities OR production hours
                 are found.
               - 2 records when both are found.
               This means care must be taken the report 'sees' all records to
               prevent some information is missing afterwards.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  ROP 13-MAR-2013 SR-20013769 - reorganised header
  MRA:21-MAR-2013 TD-21527
  - Also do this for time-values (DecodeHrsMin). For this purpose function
    DecodeHrsMin has extra argument that decides if it should new format or not.
    Default not.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:8-MAY-2014 TD-24850
  - Related to this todo: Fix added for the column header.
  - Some changes to layout had to be made to make it fit for landscape.
    It must be fixed to A4 to get it right.
  MRA:18-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015220
  - Productivity reports and selection on time
  MRA:24-JUN-2014 20015221
  - Include open scans
  MRA:19-AUG-2014 20015220.90 Rework
  - Make more room for from-to-columns in selection-part, because of
    from-to-date that can be much larger when time-part is shown.
  MRA:22-SEP-2014 20015586
  - Include down-time in production reports
  MRA:27-OCT-2014 20015220.110 Rework
  - When ProductionQuantity-records are entered over longer periods than
    5 min. then it cannot always find it, depending on what was entered
    for 'selection on time'.
  MRA:9-JUN-2015 SO-20014450.50
  - Real Time Efficiency
  - Get data from a view
  MRA:15-JAN-2016 PIM-137
  - Add EmployeeFrom, EmployeeTo to real-time-eff-build-query for reports where
    employees can be selected.
  MRA:5-FEB-2016 PIM-135
  - Production reports add salary-hours column
  - Related to this order:
  - Also show down-time-jobs + breaks.
  - Show down-time-jobs based on dialog-setting:
    - Show it not (default), yes, or only.
*)

unit ReportProdCompTargetQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, DBTables, DB,
  QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt, ComCtrls;


type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FWKFrom, FWKTo,
    FJobFrom, FJobTo, FTeamFrom, FTeamTo: String;
    FNorm: Integer;
    FShowSelection, FPagePlant, FPageBU, FPageDept, FPageWK: Boolean;
    FIncludeDownTime: Integer; // 20015586
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo: String;
      Norm: Integer;
      ShowSelection, PagePlant, PageBU, PageDept, PageWK: Boolean;
      IncludeDownTime: Integer // 20015586
      );
  end;

  TReportProdCompTargetQR = class(TReportBaseF)
    QRGroupHdPlant: TQRGroup;
    QRGroupHDBU: TQRGroup;
    QRGroupHDDEPT: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupJOB: TQRGroup;
    QRGroupColumnHeader: TQRGroup;
    QRBandFooterJob: TQRBand;
    QRBandGroupFooterBU: TQRBand;
    QRBandGroupFooterDept: TQRBand;
    QRBandGroupFooterPlant: TQRBand;
    QRBandGroupFooterWK: TQRBand;
    QRBandTitle: TQRBand;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBTextDescBU: TQRDBText;
    QRDBTextDescDept: TQRDBText;
    QRDBTextPlant: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel193: TQRLabel;
    QRLabel194: TQRLabel;
    QRLabel198: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabelDiffHrs: TQRLabel;
    QRLabelDiffHrsBU: TQRLabel;
    QRLabelDiffHrsDept: TQRLabel;
    QRLabelDiffHrsPlant: TQRLabel;
    QRLabelDiffHrsWK: TQRLabel;
    QRLabelDiffWageCost: TQRLabel;
    QRLabelDiffWageCostBU: TQRLabel;
    QRLabelDiffWageCostDept: TQRLabel;
    QRLabelDiffWageCostPlant: TQRLabel;
    QRLabelDiffWageCostWK: TQRLabel;
    QRLabelEfficiency: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelMachineHrs: TQRLabel;
    QRLabelManHrs: TQRLabel;
    QRLabelNrOfEmpl: TQRLabel;
    QRLabelOutputNorm: TQRLabel;
    QRLabelOutputReal: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelProdNorm: TQRLabel;
    QRLabelProdReal: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabelTotDept: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabelWageCost: TQRLabel;
    QRLabelWageCostBU: TQRLabel;
    QRLabelWageCostDept: TQRLabel;
    QRLabelWageCostPlant: TQRLabel;
    QRLabelWageCostWK: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRSubDetailPQ: TQRSubDetail;
    QRLabelIncludeDowntime: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupJOBBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDEPTBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRSubDetailPQBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelMachineHrsPrint(sender: TObject; var Value: String);
    procedure QRBandFooterJobBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterBUBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    FWageCostPlant,
    FWageCostBU,
    FWageCostDept,
    FWageCostWK,
    FDiffWageCostPlant,
    FDiffWageCostBU,
    FDiffWageCostDept,
    FDiffWageCostWK: Double;

    FDiffHrsPlant,
    FDiffHrsDept,
    FDiffHrsBU,
    FDiffHrsWK: Double;
//    function DeterminePieces: Double; // 20015220.110
    function DetermineHours: Double; // 20015220.110
//    function LengthDateInterval(StartDate, EndDate: TDateTime): Integer;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMachineHrsJob, FManHoursJob: Double;
    FPiecesJob,
    FJobNorm: Double;
    FDatePHE: TDateTime;
    MyProgressBar: TProgressBar;
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo: String;
      const DateFrom, DateTo: TDateTime;
      const Norm: Integer;
      const ShowSelection, PagePlant,
        PageBU, PageDept, PageWK: Boolean;
      const IncludeDownTime: Integer // 20015586
        ): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportProdCompTargetQR: TReportProdCompTargetQR;

implementation

{$R *.DFM}
uses
  SystemDMT, UGlobalFunctions, ListProcsFRM, UPimsConst,
  ReportProdCompTargetDMT, ReportProductionDetailDMT, UPimsMessageRes;


procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo, TeamFrom, TeamTo: String;
  Norm: Integer;
  ShowSelection, PagePlant, PageBU, PageDept, PageWK: Boolean;
  IncludeDownTime: Integer // 20015586
  );
begin
  FDateFrom := DateFrom;
  FDateTo:= DateTo;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  if SystemDM.WorkspotInclSel then // 20015223
  begin
    FWKFrom := ReportProdCompTargetQR.InclExclWorkspotsResult;
    FWKTo := ReportProdCompTargetQR.InclExclWorkspotsResult;
  end
  else
  begin
    FWKFrom := WKFrom;
    FWKTo := WKTo;
  end;
  FJobFrom := JobFrom;
  FJobTo := JobTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageBU := PageBU;
  FPageDept := PageDept;
  FPageWK := PageWK;
  FIncludeDownTime := IncludeDownTime; // 20015586
  FNorm := Norm;
end;

function TReportProdCompTargetQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
  JobFrom, JobTo, TeamFrom, TeamTo: String;
  const DateFrom, DateTo: TDateTime;
  const Norm: Integer;
  const ShowSelection, PagePlant,
    PageBU, PageDept, PageWK: Boolean;
  const IncludeDownTime: Integer // 20015586
    ): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo,'1', '1');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, WKFrom, WKTo,
      JobFrom, JobTo, TeamFrom, TeamTo, Norm,
      ShowSelection, PagePlant, PageBU, PageDept, PageWK,
      IncludeDownTime // 20015586
      );
  end;
  SetDataSetQueryReport(ReportProdCompTargetDM.QueryProductionComp);
end;

function TReportProdCompTargetQR.ExistsRecords: Boolean;
var
  SelectStr, SelectOthers: String;
  AllPlants: Boolean;
  AllTeams: Boolean;
  AllBusinessUnits: Boolean;
  AllDepartments: Boolean;
  AllWorkspots: Boolean;
begin
  Screen.Cursor := crHourGlass;

{  if UseDateTime or IncludeOpenScans then // 20015220 + 20015221
  begin
    // Init/Reset Client Datasets.
    ReportProductionDetailDM.cdsEmpl.Close;
    ReportProductionDetailDM.cdsEmpl.Filtered := False;
    ReportProductionDetailDM.cdsEmpl.Open;
    ReportProductionDetailDM.ShowCompareJobs := False;
  end;
}

  // 20014450.50
  AllPlants := ReportProductionDetailDM.
    DetermineAllPlants(QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
  AllTeams := ReportProductionDetailDM.DetermineAllTeams(
    QRParameters.FTeamFrom, QRParameters.FTeamTo);
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    AllBusinessUnits := ReportProductionDetailDM.
      DetermineAllBusinessUnits(
        QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
    AllDepartments := ReportProductionDetailDM.DetermineAllDepartments(
      QRBaseParameters.FPlantFrom, QRParameters.FDeptFrom, QRParameters.FDeptTo);
    AllWorkspots := ReportProductionDetailDM.DetermineAllWorkspots(
      QRBaseParameters.FPlantFrom, QRParameters.FWKFrom, QRParameters.FWKTo);
  end
  else
  begin
    AllBusinessUnits := True;
    AllDepartments := True;
    AllWorkspots := True;
  end;

  // 20014450.50
  // Query to get only machine hours
  SelectOthers := '';
  if QRBaseParameters.FPLantFrom = QRBaseParameters.FPLantTo then
  begin
    AllBusinessUnits := False;
    AllDepartments := False;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectOthers :=
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PQ.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
      SelectOthers :=
        ' AND PQ.WORKSPOT_CODE >= ' + '''' + QRParameters.FWKFrom + '''' + ' AND ' + NL +
        ' PQ.WORKSPOT_CODE <= ' + '''' + QRParameters.FWKTo + '''' + ' ' + NL;
    SelectOthers := SelectOthers +
      ' AND J.BUSINESSUNIT_CODE >= ' + '''' + QRParameters.FBusinessFrom + '''' + ' ' + NL +
      ' AND J.BUSINESSUNIT_CODE <= ' + '''' + QRParameters.FBusinessTo + '''' + ' ' + NL +
      ' AND W.DEPARTMENT_CODE >= ' + '''' + QRParameters.FDeptFrom + '''' + ' ' + NL +
      ' AND W.DEPARTMENT_CODE <= ' + '''' + QRParameters.FDeptTo + '''' + ' ' + NL;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      if not InclExclWorkspotsAll then
        SelectOthers := SelectOthers +
          ' AND  PQ.JOB_CODE >= ' + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PQ.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
    end
    else
      if QRParameters.FWKFrom = QRParameters.FWKTo then
        SelectOthers := SelectOthers +
          ' AND  PQ.JOB_CODE >= '  + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PQ.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
  end;
  SelectStr :=
    'SELECT /*+ USE_HASH(PQ,P,B,D,W,J) */ ' + NL +
    '  PQ.PLANT_CODE, ' + NL +
    '  J.BUSINESSUNIT_CODE, ' + NL +
    '  W.DEPARTMENT_CODE, ' + NL +
    '  PQ.WORKSPOT_CODE, ' + NL +
    '  PQ.JOB_CODE, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '  SUM(PQ.QUANTITY) AS PIECES, ' + NL;
  if UseDateTime then // 20015220.110
    SelectStr := SelectStr +
      ' SUM(DATETIME_RANGE_OVERLAP_MINS( ' +
      ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' +
      ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' +
      ' PQ.START_DATE, PQ.END_DATE)) AS MACHINEHOURS ' + NL;
  SelectStr := SelectStr +
    '  SUM(PQ.END_DATE - PQ.START_DATE) AS DIFFDATE  ' + NL +
   'FROM ' + NL +
    '  PRODUCTIONQUANTITY PQ ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    PQ.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    PQ.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    PQ.PLANT_CODE = J.PLANT_CODE AND  '+ NL +
    '    PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    PQ.JOB_CODE = J.JOB_CODE ' + NL +
    'WHERE ' + NL +
    // 20012858.
    '  (PQ.JOB_CODE <> ' + '''' + NOJOB + '''' + ') AND ' + NL;
  // 20015586
  if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
  begin
    SelectStr := SelectStr +
      '  ((PQ.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PQ.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  end;
  if SystemDM.UseShiftDateSystem then
  begin
    SelectStr := SelectStr +
      // 20013489 Compare with SHIFT_DATE and use < instead of <= and dateto + 1
      '  PQ.SHIFT_DATE >= ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateFrom)) + ' AND ' + NL + //:DATEFROM
      '  PQ.SHIFT_DATE < ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateTo + 1)) + NL; // :DATETO ' + NL;
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        '  AND DATETIME_RANGE_OVERLAP_MINS( ' +
        ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' + // :DATETIMEFROM,
        ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' + //:DATETIMETO,
        ' PQ.START_DATE, PQ.END_DATE) > 0 ' + NL; // 20015220.110
//      SelectStr := SelectStr +
//      '  AND PQ.START_DATE >= :DATETIMEFROM AND PQ.START_DATE < :DATETIMETO ' + NL;
  end
  else
  begin
    if UseDateTime then
      SelectStr := SelectStr +
        '  DATETIME_RANGE_OVERLAP_MINS( ' +
        ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' + // :DATETIMEFROM,
        ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' +  // :DATETIMETO,
        ' PQ.START_DATE, PQ.END_DATE) > 0 ' + NL // 20015220.110
//      SelectStr := SelectStr +
//      '  PQ.START_DATE >= :DATETIMEFROM AND PQ.START_DATE < :DATETIMETO ' + NL
    else
      SelectStr := SelectStr +
      '  PQ.START_DATE >= ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateFrom)) + ' AND ' + NL +
      '  PQ.START_DATE < ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateTo + 1)) + NL;
  end;
  SelectStr := SelectStr +
    '  AND PQ.QUANTITY <> 0 AND ' + NL +
    '  PQ.PLANT_CODE >= ' + '''' + QRBaseParameters.FPlantFrom + '''' + ' ' + NL +
    '  AND PQ.PLANT_CODE <= ' + '''' + QRBaseParameters.FPlantTo + '''' + ' ' + NL +
    '  AND W.DEPARTMENT_CODE IN ' + NL +
    '    (SELECT T.DEPARTMENT_CODE ' + NL +
    '     FROM DEPARTMENTPERTEAM T ' + NL +
    '     WHERE T.PLANT_CODE = PQ.PLANT_CODE AND ' + NL +
    '     T.TEAM_CODE >= ' + '''' + QRParameters.FTeamFrom + '''' + ' AND ' +
    '     T.TEAM_CODE <= ' + '''' + QRParameters.FTeamTo + '''' + ' ' + NL;
  //RV067.2.
  SelectStr := SelectStr +
    '  AND ( ' + NL +
    '    (' + QuotedStr(SystemDM.UserTeamLoginUser) + ' = ''*'') OR ' + NL +
    '    (T.TEAM_CODE IN ' + NL +
    '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = ' + QuotedStr(SystemDM.UserTeamLoginUser) + ')) ' + NL +
    '  ) ';
  SelectStr := SelectStr + ' ) ' + NL;
  SelectStr := SelectStr + SelectOthers + ' ' + NL +
    'GROUP BY ' + NL +
    '  PQ.PLANT_CODE, ' + NL +
    '  J.BUSINESSUNIT_CODE, ' + NL +
    '  W.DEPARTMENT_CODE, ' + NL +
    '  PQ.WORKSPOT_CODE, ' + NL +
    '  PQ.JOB_CODE, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN ' + NL +
    ' ORDER BY ' + NL +
    '   PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, ' + NL +
    '   WORKSPOT_CODE, JOB_CODE';

  with ReportProdCompTargetDM.qryMachineHours do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(SelectStr);
//    SQL.SaveToFile('c:\temp\RepProdCompTargetMachineHrs.sql');
    Open;
//    Result := not Eof;
  end;

  // 20014450.50
  SelectStr :=
    ReportProductionDetailDM.RealTimeEfficiencyBuildQuery(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo,
      QRParameters.FDeptFrom, QRParameters.FDeptTo,
      QRParameters.FWKFrom, QRParameters.FWKTo,
      QRParameters.FJobFrom, QRParameters.FJobTo,
      QRParameters.FTeamFrom, QRParameters.FTeamTo,
      '0', '999', // ShiftFrom, ShiftTo
      '', '', // EmployeeFrom, EmployeeTo PIM-137
      Trunc(QRParameters.FDateFrom),
      Trunc(QRParameters.FDateTo),
      AllPlants,
      AllBusinessUnits,
      AllDepartments,
      AllWorkspots,
      AllTeams,
      True, // AllShifts
      False, // ShowShifts
      False, // ShowOnlyJob
      QRParameters.FIncludeDownTime, // PIM-135
      False, // SelectShift
      False, // SelectDate
      0, // 0=GroupBy BU + Dept 1=GroupBy BU 2=GroupBy Dept
      0, // 0=SortByEmp Nr 1=SortByEmp Name
      True // Ttue=Filter on job False=not
      );

  with ReportProdCompTargetDM.QueryProductionComp do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(SelectStr);
//    SQL.SaveToFile('c:\temp\RepProdCompTargetProdComp.sql');
    Open;
    Result := not Eof;
  end;

{
  if QRBaseParameters.FPLantFrom = QRBaseParameters.FPLantTo then
  begin
    AllBusinessUnits := False;
    AllDepartments := False;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectOthers :=
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PQ.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
      SelectOthers :=
        ' AND PQ.WORKSPOT_CODE >= ' + '''' + QRParameters.FWKFrom + '''' + ' AND ' + NL +
        ' PQ.WORKSPOT_CODE <= ' + '''' + QRParameters.FWKTo + '''' + ' ' + NL;
    SelectOthers := SelectOthers +
      ' AND B.BUSINESSUNIT_CODE >= ' + '''' + QRParameters.FBusinessFrom + '''' + ' ' + NL +
      ' AND B.BUSINESSUNIT_CODE <= ' + '''' + QRParameters.FBusinessTo + '''' + ' ' + NL +
      ' AND D.DEPARTMENT_CODE >= ' + '''' + QRParameters.FDeptFrom + '''' + ' ' + NL +
      ' AND D.DEPARTMENT_CODE <= ' + '''' + QRParameters.FDeptTo + '''' + ' ' + NL;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      if not InclExclWorkspotsAll then
        SelectOthers := SelectOthers +
          ' AND  PQ.JOB_CODE >= ' + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PQ.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
    end
    else
      if QRPARAMETERS.FWKFROM = QRPARAMETERS.FWKTO then
        SelectOthers := SelectOthers +
          ' AND  PQ.JOB_CODE >= '  + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PQ.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
  end;

  // MR:01-02-2005 QUERY CHANGES:
  // GROUPING INCLUDED.
  // ALSO THE DIFFERENCE OF START_DATE, END_DATE IS CALCULATED IN
  // THE QUERY. SO ALL NECESSARY DATA IS AVAILABLE. NO NEED TO USE
  // ANOTHER QUERY FOR COMPARISON ANYMORE.
  SelectStr :=
    'SELECT /*+ USE_HASH(PQ,P,B,D,W,J) */ ' + NL +
    '  PQ.PLANT_CODE, ' + NL +
    '  P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '  P.AVERAGE_WAGE, ' + NL +
    '  B.BUSINESSUNIT_CODE, ' + NL +
    '  B.DESCRIPTION AS BUSINESSUNITDESCRIPTION, ' + NL +
    '  D.DEPARTMENT_CODE, ' + NL +
    '  D.DESCRIPTION AS DEPARTMENTDESCRIPTION, ' + NL +
    '  PQ.WORKSPOT_CODE, ' + NL +
    '  W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '  PQ.JOB_CODE, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '  J.DESCRIPTION AS JOBDESCRIPTION, ' + NL +
    '  J.NORM_PROD_LEVEL, ' + NL +
    '  J.NORM_OUTPUT_LEVEL, ' + NL +
    '  SUM(PQ.QUANTITY) AS PIECES, ' + NL;
  if UseDateTime then // 20015220.110
    SelectStr := SelectStr +
      ' SUM(DATETIME_RANGE_OVERLAP_MINS( ' +
      ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' +  //:DATETIMEFROM, :DATETIMETO, ' +
      ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' +
      ' PQ.START_DATE, PQ.END_DATE)) AS MACHINEHOURS, ' + NL;
  SelectStr := SelectStr +
    '  SUM(PQ.END_DATE - PQ.START_DATE) AS DIFFDATE,  ' + NL +
    '  ''P'' AS MYTYPE ' + NL +
   // '  PQ.SHIFT_NUMBER ' +
   'FROM ' + NL +
    '  PRODUCTIONQUANTITY PQ INNER JOIN PLANT P ON ' + NL +
    '    PQ.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    PQ.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    PQ.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  LEFT JOIN DEPARTMENT D ON ' + NL +
    '    PQ.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
    '  LEFT JOIN JOBCODE J ON ' + NL +
    '    PQ.PLANT_CODE = J.PLANT_CODE AND  '+ NL +
    '    PQ.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    PQ.JOB_CODE = J.JOB_CODE ' + NL +
    '  LEFT JOIN BUSINESSUNIT B ON ' + NL +
    '    PQ.PLANT_CODE = B.PLANT_CODE AND '+ NL +
    '    J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL +
    'WHERE ' + NL +
    // 20012858.
    '  (PQ.JOB_CODE <> ' + '''' + NOJOB + '''' + ') AND ' + NL;
  // 20015586
  if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
  begin
    SelectStr := SelectStr +
      '  ((PQ.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PQ.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  end;
  if SystemDM.UseShiftDateSystem then
  begin
    SelectStr := SelectStr +
      // 20013489 Compare with SHIFT_DATE and use < instead of <= and dateto + 1
      '  PQ.SHIFT_DATE >= ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateFrom)) + ' AND ' + NL + //:DATEFROM
      '  PQ.SHIFT_DATE < ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateTo + 1)) + NL; // :DATETO ' + NL;
    if UseDateTime then // 20015220
      SelectStr := SelectStr +
        '  AND DATETIME_RANGE_OVERLAP_MINS( ' +
        ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' + // :DATETIMEFROM,
        ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' + //:DATETIMETO,
        ' PQ.START_DATE, PQ.END_DATE) > 0 ' + NL; // 20015220.110
//      SelectStr := SelectStr +
//      '  AND PQ.START_DATE >= :DATETIMEFROM AND PQ.START_DATE < :DATETIMETO ' + NL;
  end
  else
  begin
    if UseDateTime then
      SelectStr := SelectStr +
        '  DATETIME_RANGE_OVERLAP_MINS( ' +
        ReportProductionDetailDM.ToDate(QRParameters.FDateFrom) + ',' + // :DATETIMEFROM,
        ReportProductionDetailDM.ToDate(QRParameters.FDateTo) + ',' +  // :DATETIMETO,
        ' PQ.START_DATE, PQ.END_DATE) > 0 ' + NL // 20015220.110
//      SelectStr := SelectStr +
//      '  PQ.START_DATE >= :DATETIMEFROM AND PQ.START_DATE < :DATETIMETO ' + NL
    else
      SelectStr := SelectStr +
      '  PQ.START_DATE >= ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateFrom)) + ' AND ' + NL + // :DATEFROM
      '  PQ.START_DATE < ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateTo + 1)) + NL; // :DATETO ' + NL;
  end;
  SelectStr := SelectStr +
    '  AND PQ.QUANTITY <> 0 AND ' + NL +
    '  PQ.PLANT_CODE >= ' + '''' + QRBaseParameters.FPlantFrom + '''' + ' ' + NL +
    '  AND PQ.PLANT_CODE <= ' + '''' + QRBaseParameters.FPlantTo + '''' + ' ' + NL +
    '  AND D.DEPARTMENT_CODE IN ' + NL +
    '    (SELECT T.DEPARTMENT_CODE ' + NL +
    '     FROM DEPARTMENTPERTEAM T ' + NL +
    '     WHERE T.PLANT_CODE = PQ.PLANT_CODE AND ' + NL +
    '     T.TEAM_CODE >= ' + '''' + QRParameters.FTeamFrom + '''' + ' AND ' +
    '     T.TEAM_CODE <= ' + '''' + QRParameters.FTeamTo + '''' + ' ' + NL;
    //RV067.2.
    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    (' + QuotedStr(SystemDM.UserTeamLoginUser) + ' = ''*'') OR ' + NL + // :USER_NAME
      '    (T.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = ' + QuotedStr(SystemDM.UserTeamLoginUser) + ')) ' + NL + // :USER_NAME
      '  ) ';
    SelectStr := SelectStr + ' ) ' + NL;
    SelectStr := SelectStr + SelectOthers + ' ' + NL +
    'GROUP BY ' + NL +
    '  PQ.PLANT_CODE, ' + NL +
    '  P.DESCRIPTION, ' + NL +
    '  P.AVERAGE_WAGE, ' + NL +
    '  B.BUSINESSUNIT_CODE, ' + NL +
    '  B.DESCRIPTION, ' + NL +
    '  D.DEPARTMENT_CODE, ' + NL +
    '  D.DESCRIPTION, ' + NL +
    '  PQ.WORKSPOT_CODE, ' + NL +
    '  W.DESCRIPTION, ' + NL +
    '  PQ.JOB_CODE, ' + NL +
    '  J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '  J.DESCRIPTION, ' + NL +
    '  J.NORM_PROD_LEVEL, ' + NL +
    '  J.NORM_OUTPUT_LEVEL ' + NL +
    // MR:22-12-2004 USE 'UNION ALL' OR YOU MISS PHE-RECORDS
    // THAT HAVE THE SAME FIELD-VALUES PER RECORD.
    ' UNION ALL ' + NL +
    ' SELECT /*+ USE_HASH(PHE,P,B,D,W,J) */ ' + NL +
    '   PHE.PLANT_CODE, ' + NL +
    '   P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '   P.AVERAGE_WAGE, ' + NL +
    '   B.BUSINESSUNIT_CODE, ' + NL +
    '   B.DESCRIPTION  AS BUSINESSUNITDESCRIPTION, ' + NL +
    '   D.DEPARTMENT_CODE, ' + NL +
    '   D.DESCRIPTION AS DEPARTMENTDESCRIPTION, ' + NL +
    '   PHE.WORKSPOT_CODE, ' + NL +
    '   W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '   PHE.JOB_CODE, ' + NL +
    '   J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '   J.DESCRIPTION AS JOBDESCRIPTION, ' + NL +
    '   J.NORM_PROD_LEVEL, ' + NL +
    '   J.NORM_OUTPUT_LEVEL, ' + NL +
    '   SUM(CAST(PHE.PRODUCTION_MINUTE AS DOUBLE PRECISION)) AS PIECES, ' + NL;
  if UseDateTime then
    SelectStr := SelectStr +
      '  99999.0 AS MACHINEHOURS, ' + NL;
  SelectStr := SelectStr +
    '   99999.0 AS DIFFDATE,  ' + NL +
    '  ''H'' AS MYTYPE ' + NL +
    ' FROM ' + NL +
    '   PRODHOURPEREMPLOYEE PHE LEFT JOIN PLANT P ON ' + NL +
    '     PHE.PLANT_CODE = P.PLANT_CODE ' + NL +
    // MR:02-05-2005 Use 'Inner' instead of 'Left' join.
    '   INNER JOIN JOBCODE J ON ' + NL +
    '     PHE.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '     PHE.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '     PHE.JOB_CODE = J.JOB_CODE ' + NL +
    // MR:02-05-2005 Use 'Inner' instead of 'Left' join.
    '   INNER JOIN WORKSPOT W ON ' + NL +
    '     PHE.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '     PHE.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '   LEFT JOIN BUSINESSUNIT B ON ' + NL +
    '     PHE.PLANT_CODE = B.PLANT_CODE AND ' + NL +
    '     J.BUSINESSUNIT_CODE = B.BUSINESSUNIT_CODE ' + NL +
    '   LEFT JOIN DEPARTMENT D ON ' + NL +
    '     PHE.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '     W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
    ' WHERE ' + NL;
  // 20015586
  if QRParameters.FIncludeDownTime = DOWNTIME_ONLY then // Only show downtime
  begin
    SelectStr := SelectStr +
      '  ((PHE.JOB_CODE = ' + '''' + DOWNJOB_1 + '''' + ') OR ' + NL +
      '   (PHE.JOB_CODE = ' + '''' + DOWNJOB_2 + '''' + ')) AND ' + NL;
  end;
  SelectStr := SelectStr +
// MR:01-06-2006 All shifts should be included!
//    '   PHE.SHIFT_NUMBER = 1 AND MANUAL_YN = ''N'' AND ' + NL +
    '   PHE.MANUAL_YN = ''N'' AND ' + NL +
    // 20013489 Compare with < instead of <= and dateto + 1
    '   PHE.PRODHOUREMPLOYEE_DATE < ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateTo + 1)) + 'AND ' + NL + // :DATETO AND ' + NL +
    '   PHE.PRODHOUREMPLOYEE_DATE  >= ' + ReportProductionDetailDM.ToDate(Trunc(QRParameters.FDateFrom)) + ' AND ' + NL + // :DATEFROM AND ' + NL +
    '   PHE.PRODUCTION_MINUTE <> 0 AND ' + NL +
    '   PHE.PLANT_CODE >= ' + '''' + QRBaseParameters.FPlantFrom + '''' + ' ' + NL +
    '   AND PHE.PLANT_CODE <= ' + '''' + QRBaseParameters.FPlantTo + '''' + ' ' + NL +
    '   AND ' + NL +
    '   W.DEPARTMENT_CODE IN ' + NL +
    '     (SELECT T.DEPARTMENT_CODE ' + NL +
    '      FROM DEPARTMENTPERTEAM T ' + NL +
    '      WHERE T.PLANT_CODE = PHE.PLANT_CODE AND ' + NL +
    '      T.TEAM_CODE >= ' + '''' + QRParameters.FTeamFrom + '''' + ' AND ' +
    '      T.TEAM_CODE <= ' + '''' + QRParameters.FTeamTo + '''' + ' ' + NL;
   //!!
    SelectStr := SelectStr +
      '  AND ( ' + NL +
      '    (' + QuotedStr(SystemDM.UserTeamLoginUser) + ' = ''*'') OR ' + NL + // :USER_NAME
      '    (T.TEAM_CODE IN ' + NL +
      '      (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = ' +
      QuotedStr(SystemDM.UserTeamLoginUser) + ' )) ' + NL + // :USER_NAME
      '  ) ';
    SelectStr := SelectStr + ' ) ' + NL;

  if QRBASEPARAMETERS.FPLANTFROM = QRBASEPARAMETERS.FPLANTTO then
  begin
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      SelectStr := SelectStr +
        ' AND ' + NL +
        ' (' + NL +
        '   (' + NL +
        '     PHE.WORKSPOT_CODE IN ' + NL +
        '     (SELECT PW.WORKSPOT_CODE FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') ' + NL +
        '   )' + NL +
        ' OR ' + NL +
        '   ( ' + NL +
        '     (SELECT COUNT(*) FROM PIVOTWORKSPOT PW ' + NL +
        '      WHERE PW.PIVOTCOMPUTERNAME = ' + '''' + SystemDM.CurrentComputerName + '''' +
        '      AND ' + NL +
        '      PW.PLANT_CODE = ' + '''' + QRBaseParameters.FPlantFrom + '''' + ') = 0 ' + NL +
        '   ) ' + NL +
        ' ) ' + NL;
    end
    else
      SelectStr := SelectStr +
        ' AND PHE.WORKSPOT_CODE >= ' + '''' + QRParameters.FWKFrom + '''' + ' AND ' + NL +
        ' PHE.WORKSPOT_CODE <= ' + '''' + QRParameters.FWKTo + '''' + ' ' + NL;
    SelectStr := SelectStr +
      ' AND B.BUSINESSUNIT_CODE >= ' + '''' + QRParameters.FBusinessFrom + '''' + ' ' + NL +
      ' AND B.BUSINESSUNIT_CODE <= ' + '''' + QRParameters.FBusinessTo + '''' + ' ' + NL +
      ' AND D.DEPARTMENT_CODE >= ' + '''' + QRParameters.FDeptFrom + '''' + ' ' + NL +
      ' AND D.DEPARTMENT_CODE <= ' + '''' + QRParameters.FDeptTo + '''' + ' ' + NL;
    if SystemDM.WorkspotInclSel then // 20015223
    begin
      if not InclExclWorkspotsAll then
        SelectStr := SelectStr +
          ' AND PHE.JOB_CODE >= ' + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PHE.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
    end
    else
      if QRPARAMETERS.FWKFROM = QRPARAMETERS.FWKTO then
        SelectStr := SelectStr +
          ' AND  PHE.JOB_CODE >= ' + '''' + QRParameters.FJobFrom + '''' + ' AND ' + NL +
          ' PHE.JOB_CODE <= ' + '''' + QRParameters.FJobTo + '''' + ' ' + NL;
  end;
  SelectStr := SelectStr +
    ' GROUP BY ' + NL +
    '   PHE.PLANT_CODE, ' + NL +
    '   P.DESCRIPTION, ' + NL +
    '   P.AVERAGE_WAGE, ' + NL +
    '   B.BUSINESSUNIT_CODE, ' + NL +
    '   B.DESCRIPTION, ' + NL +
    '   D.DEPARTMENT_CODE, ' + NL +
    '   D.DESCRIPTION, ' + NL +
    '   PHE.WORKSPOT_CODE, ' + NL +
    '   W.DESCRIPTION, ' + NL +
    '   PHE.JOB_CODE, ' + NL +
    '   J.IGNOREQUANTITIESINREPORTS_YN, ' + NL + // 20013478.
    '   J.DESCRIPTION, ' + NL +
    '   J.NORM_PROD_LEVEL, ' + NL +
    '   J.NORM_OUTPUT_LEVEL ' + NL;
  SelectStr := SelectStr +
//    ' ORDER BY 1, 4, 6, 8, 10, 15 ' ;
    ' ORDER BY ' + NL +
    '   PLANT_CODE, BUSINESSUNIT_CODE, DEPARTMENT_CODE, ' + NL +
    '   WORKSPOT_CODE, JOB_CODE';

  with ReportProdCompTargetDM.QueryProductionComp  do
  begin
    Active := False;
    UniDirectional := False;
    SQL.Clear;
    SQL.Add(UpperCase(SelectStr));
    //!!
//    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
//      ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
//    else
//      ParamByName('USER_NAME').AsString := '*';

// SQL.SaveToFile('c:\temp\ReportProdCompTarget.sql');

    // 20015220
//      QRParameters.FDateFrom := Trunc(QRParameters.FDateFrom);
//      QRParameters.FDateTo := Trunc(QRParameters.FDateTo);

//    ParamByName('PLANTFROM').Value := QRBaseParameters.FPlantFrom;
//    ParamByName('PLANTTO').Value := QRBaseParameters.FPlantTo;
    if SelectOthers <> '' then
    begin
      if not SystemDM.WorkspotInclSel then // 20015223
      begin
//        ParamByName('WORKSPOTFROM').Value := QRParameters.FWKFrom;
//        ParamByName('WORKSPOTTO').Value := QRParameters.FWKTo;
      end;
//      ParamByName('BUFROM').Value := QRParameters.FBusinessFrom;
//      ParamByName('BUTO').Value := QRParameters.FBusinessTo;
//      ParamByName('DEPTFROM').Value := QRParameters.FDeptFrom;
//      ParamByName('DEPTTO').Value := QRParameters.FDeptTo;
      if SystemDM.WorkspotInclSel then // 20015223
      begin
        if not InclExclWorkspotsAll then
        begin
//          ParamByName('JOBFROM').Value := QRParameters.FJobFrom;
//          ParamByName('JOBTO').Value := QRParameters.FJobTo;
        end;
      end
      else
        if QRParameters.FWKFrom = QRParameters.FWKTo then
        begin
//          ParamByName('JOBFROM').Value := QRParameters.FJobFrom;
//          ParamByName('JOBTO').Value := QRParameters.FJobTo;
        end;
    end;
    // 20015220.110 Arguments are put in query itself
//    ParamByName('DATEFROM').AsDateTime := Trunc(QRParameters.FDateFrom);
//    ParamByName('DATETO').AsDateTime := Trunc(QRParameters.FDateTo + 1);
//    if UseDateTime then // 20015220
//    begin
//      ParamByName('DATETIMEFROM').AsDateTime := QRParameters.FDateFrom;
//      ParamByName('DATETIMETO').AsDateTime := QRParameters.FDateTo;
//    end;
//    ParamByName('TEAMFROM').AsString := QRParameters.FTeamFrom;
//    ParamByName('TEAMTO').AsString := QRParameters.FTeamTo;
    Active := True;
    Result :=  not ReportProdCompTargetDM.QueryProductionComp.IsEmpty;
    if not Result then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;
   FDatePHE := 99999.0; //EncodeDate(1900, 1, 1);
(*
  ReportProdCompTargetDM.InitQueryTotalPiecesPQ(
    ReportProdCompTargetDM.QueryPQ,
    QRParameters.FDateFrom, QRParameters.FDateTo,
    QRBaseParameters.FPlantFrom,
    QRBaseParameters.FPlantTo);
*)
  // RV102.1. Do not empty the ClientDataSet here, to prevent it is done only once.
//  ReportProdCompTargetDM.ClientDataSetMachineHrs.EmptyDataSet;

  // 20015220 We need this to determine worked hours based on date+time
  // 20015221
  if (UseDateTime or IncludeOpenScans) and Result then
  begin
    ReportProductionDetailDM.InitAll(
      QRBaseParameters.FPlantFrom,
      QRBaseParameters.FPlantTo,
      QRParameters.FBusinessFrom,
      QRParameters.FBusinessTo,
      QRParameters.FDeptFrom,
      QRParameters.FDeptTo,
      QRParameters.FWKFrom,
      QRParameters.FWKTo,
      QRParameters.FTeamFrom,
      QRParameters.FTeamTo,
      '1', // QRParameters.FShiftFrom,
      '1', // QRParameters.FShiftTo,
      QRParameters.FDateFrom,
      QRParameters.FDateTo,
      False, // AllPlants,
      AllBusinessUnits,
      AllDepartments,
      AllWorkspots,
      False, // AllTeams,
      True, // QRParameters.FAllShifts,
      False, // QRParameters.FShowShifts,
      MyProgressBar,
      QRParameters.FIncludeDownTime = DOWNTIME_ONLY // 20015586
      );
  end;
}
  Screen.Cursor := crDefault;
end;

procedure TReportProdCompTargetQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  QRLabelWKFrom.Caption := DisplayWorkspotFrom(QRParameters.FWKFrom); // 20015223
  QRLabelWKTo.Caption := DisplayWorkspotTo(QRParameters.FWKTo); // 20015223
  if UseDateTime then // 20015220
  begin
    QRLabelDateFrom.Caption := DateTimeToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateTimeToStr(QRParameters.FDateTo);
  end
  else
  begin
    QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
    QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);
  end;
  QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
  QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelBusinessFrom.Caption := '*';
    QRLabelBusinessTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end;
  // 20015586
  case QRParameters.FIncludeDownTime of
  DOWNTIME_NO:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeNo;
  DOWNTIME_YES:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeYes;
  DOWNTIME_ONLY:
    QRLabelIncludeDowntime.Caption :=
      SPimsIncludeDowntime + ' ' + SPimsIncludeDowntimeOnly;
  end;
end;

procedure TReportProdCompTargetQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportProdCompTargetQR.FormCreate(Sender: TObject);
begin
  inherited;
  // TD-24850 This must be done to prevent the column-header is shown wrong.
  QRLabel4.Caption :=
    '                                                                ' +
    '                                                                ' +
    '     ';
  QRLabel37.Caption :=
    '                                                                ' +
    '                                                                ' +
    '     ';
  FQRParameters := TQRParameters.Create;
end;

procedure TReportProdCompTargetQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // RV102.1. Empty the ClientDataSet here!
  //          Reason: After the preview is done and when the report is
  //          printed or saved as PDF, it must do this again! Otherwise
  //          it will add the found values again, resulting in double values.
  ReportProdCompTargetDM.ClientDataSetMachineHrs.EmptyDataSet;
  try
    ReportProdCompTargetDM.ClientDataSetMachineHrs.LogChanges := False;
  except
  end;
  PrintBand := QRParameters.FShowSelection;
end;

procedure TReportProdCompTargetQR.QRGroupHdPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;

  FWageCostPlant := 0;
  FDiffHrsPlant := 0;
  FDiffWageCostPlant := 0;
end;

procedure TReportProdCompTargetQR.QRGroupHDBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHDBU.ForceNewPage := QRParameters.FPageBU;
  inherited;
  FWageCostBU := 0;
  FDiffHrsBU := 0;
  FDiffWageCostBU := 0;
end;

procedure TReportProdCompTargetQR.QRGroupHDDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHDDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
  FWageCostDept := 0;
  FDiffHrsDept := 0;
  FDiffWageCostDept := 0;
end;

procedure TReportProdCompTargetQR.QRGroupHDWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
(*
var
  Plant_Code, Businessunit_code, Workspot_Code, Job_Code: String;
  StartDate, EndDate: TDateTime;
  MachineHours_Job, MachineHours: Integer;
*)
  // TD-21554 This must be moved to Detail-part, or it will miss records!
{
  procedure AddMachineClientDataSet(Plant_code, Businessunit_code,
    Workspot_code, Job_Code: String; MachineHours: Integer);
  begin
    with ReportProdCompTargetDM do
    begin
      if not ClientDataSetMachineHrs.FindKey([Plant_code, Businessunit_code,
        Workspot_code, Job_Code]) then
      begin
        ClientDataSetMachineHrs.Insert;
        ClientDataSetMachineHrs.FieldByName('PLANT_CODE').AsString :=
          Plant_code;
        ClientDataSetMachineHrs.FieldByName('WORKSPOT_CODE').AsString :=
          Workspot_code;
        ClientDataSetMachineHrs.FieldByName('BUSINESSUNIT_CODE').AsString :=
          Businessunit_code;
        ClientDataSetMachineHrs.FieldByName('JOB_CODE').AsString :=
          Job_Code;
        ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsInteger :=
          MachineHours;
        ClientDataSetMachineHrs.Post;
      end
      else
      begin
        ClientDataSetMachineHrs.Edit;
        ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsInteger :=
          ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsInteger +
            MachineHours;
        ClientDataSetMachineHrs.Post;
      end;
    end;
  end;
}
(*
  function CheckMachineHrs: Boolean;
  begin
    Result :=
      not ReportProdCompTargetDM.QueryPQ.Eof and
       (Plant_Code = ReportProdCompTargetDM.QueryPQ.
        FieldByName('Plant_code').AsString) and
       (Workspot_Code = ReportProdCompTargetDM.QueryPQ.
        FieldByName('Workspot_code').AsString) and
       (Businessunit_code = ReportProdCompTargetDM.QueryPQ.
        FieldByName('Businessunit_code').AsString);

  end;
*)  
begin
  QRGroupHDWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
  // TD-21554 This must be moved to Detail-part, or it will miss records!
{
  with ReportProdCompTargetDM do
  begin
    if Round(QueryProductionComp.FieldByName('DIFFDATE').AsFloat) <>
      Round(FDatePHE) then
      AddMachineClientDataSet(
        QueryProductionComp.FieldByName('PLANT_CODE').AsString,
        QueryProductionComp.FieldByName('BUSINESSUNIT_CODE').AsString,
        QueryProductionComp.FieldByName('WORKSPOT_CODE').AsString,
        QueryProductionComp.FieldByName('JOB_CODE').AsString,
        Round(QueryProductionComp.FieldByName('DIFFDATE').AsFloat * 24 * 60)
        );
  end;
}
(*
  Plant_Code :=
    ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('PLANT_CODE').AsString;
  Workspot_Code :=
    ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('WORKSPOT_CODE').AsString;
  Businessunit_code :=
    ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('BUSINESSUNIT_CODE').AsString;
  ReportProdCompTargetDM.QueryPQ.First;
  if ReportProdCompTargetDM.QueryPQ.
    Locate('PLANT_CODE;BUSINESSUNIT_CODE;WORKSPOT_CODE',
     VarArrayOf([Plant_Code,Businessunit_code, Workspot_Code]),[]) then
  begin
    while CheckMachineHrs do
    begin
      StartDate :=
        ReportProdCompTargetDM.QueryPQ.FieldByName('START_DATE').AsDateTime;
      EndDate :=
        ReportProdCompTargetDM.QueryPQ.FieldByName('END_DATE').AsDateTime;
      Job_Code :=
        ReportProdCompTargetDM.QueryPQ.FieldByName('JOB_CODE').AsString;
      MachineHours := LengthDateInterval(StartDate, EndDate);

      ReportProdCompTargetDM.QueryPQ.Next;
      MachineHours_Job := 0;
      while CheckMachineHrs and (StartDate =
        ReportProdCompTargetDM.QueryPQ.FieldByName('START_DATE').AsDateTime) do
      begin
        AddMachineClientDataSet(Plant_code, Businessunit_code,
          Workspot_code, Job_Code, 1);
        MachineHours_Job := MachineHours_Job + 1;
        Job_Code :=
          ReportProdCompTargetDM.QueryPQ.FieldByName('JOB_CODE').AsString;
        ReportProdCompTargetDM.QueryPQ.Next;
      end;
      AddMachineClientDataSet(Plant_code, Businessunit_code, Workspot_code, Job_Code,
        MachineHours - MachineHours_Job);
    end;//while
   end;// if locate
*)
   FWageCostWK := 0;
   FDiffHrsWK := 0;
   FDiffWageCostWK := 0;
end;

procedure TReportProdCompTargetQR.QRGroupJOBBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  FPiecesJob := 0;
  FMachineHrsJob := 0;
  FManHoursJob := 0;
  if QRParameters.FNorm = 0 then
    FJobNorm := ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('NORM_PROD_LEVEL').AsFloat
  else
    FJobNorm := ReportProdCompTargetDM.
      QueryProductionComp.FieldByName('NORM_OUTPUT_LEVEL').AsFloat;
(*
  // TD-22171 Do this during QRJobFooter! Otherwise it misses values
  //          determined during 'details'-band.
  if ReportProdCompTargetDM.ClientDataSetMachineHrs.
    FindKey([ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('PLANT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('BUSINESSUNIT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('WORKSPOT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('JOB_CODE').AsString]) then
     FMachineHrsJob := ReportProdCompTargetDM.ClientDataSetMachineHrs.
       FieldByName('MACHINE_HOURS').AsFloat;
{$IFDEF DEBUG}
  WDebugLog(
    'P=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('PLANT_CODE').AsString +
    ' W=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('WORKSPOT_CODE').AsString +
    ' J=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('JOB_CODE').AsString +
    ' FMachineHrsJob=' + Format('%8.2f', [FMachineHrsJob])
    );
{$ENDIF}
*)
end;

procedure TReportProdCompTargetQR.QRBandFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProdCompTargetQR.QRBandFooterDEPTBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProdCompTargetQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProdCompTargetQR.QRBandFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportProdCompTargetQR.QRDBText7Print(sender: TObject;
  var Value: String);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  Value:= Format('%.0f', [FPiecesJob]);
  Value:= Format('%.0f', [FPiecesJob]);
end;
(*
function TReportProdCompTargetQR.LengthDateInterval(StartDate,
  EndDate: TDateTime): Integer;
var
  DaysDiff: Integer;
  HourStart, MinStart, SecStart, MSecStart,
  HourEnd, MinEnd, SecEnd, MSecEnd: Word;
begin
  DaysDiff := ListProcsF.DateDifferenceDays(StartDate, EndDate);
  if DaysDiff = 0 then
  begin
    DecodeTime(StartDate, HourStart,MinStart, SecStart, MSecStart);
    DecodeTime(EndDate, HourEnd, MinEnd, SecEnd, MSecEnd);
    Result := (HourEnd * 60 + MinEnd - HourStart * 60 - MinStart);
  end
  else
  begin
    if (HourStart <= HourEnd) then
      Result := DaysDiff * 60 * 24 +
        (HourEnd * 60 + MinEnd - HourStart * 60 - MinStart)
    else
      Result := (DaysDiff - 1)* 60 * 24 +
        (24 * 60 - HourStart * 60 - MinStart) + (HourEnd * 60 + MinEnd);
  end;
end;
*)
procedure TReportProdCompTargetQR.QRSubDetailPQBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  MachineHours: Double; // 20015220.110
  // TD-21554 This must be done in Detail-part or it will miss records!
  procedure AddMachineClientDataSet(Plant_code, Businessunit_code,
    Workspot_code, Job_Code: String; MachineHours: Double);
  begin
    with ReportProdCompTargetDM do
    begin
      // 20014450.50 Only add it, no edit! 
      if not ClientDataSetMachineHrs.FindKey([Plant_code, Businessunit_code,
        Workspot_code, Job_Code]) then
      begin
        ClientDataSetMachineHrs.Insert;
        ClientDataSetMachineHrs.FieldByName('PLANT_CODE').AsString :=
          Plant_code;
        ClientDataSetMachineHrs.FieldByName('WORKSPOT_CODE').AsString :=
          Workspot_code;
        ClientDataSetMachineHrs.FieldByName('BUSINESSUNIT_CODE').AsString :=
          Businessunit_code;
        ClientDataSetMachineHrs.FieldByName('JOB_CODE').AsString :=
          Job_Code;
        ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsFloat :=
          MachineHours;
        ClientDataSetMachineHrs.Post;
      end
      else
      begin
//        ClientDataSetMachineHrs.Edit;
//        ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsFloat :=
//          ClientDataSetMachineHrs.FieldByName('MACHINE_HOURS').AsFloat +
//            MachineHours;
//        ClientDataSetMachineHrs.Post;
      end;
    end;
  end;
  // 20015586
  function IncludeJobQuantity: Boolean;
  begin
    with ReportProdCompTargetDM do
    begin
      Result :=
        (QueryProductionComp.FieldByName('IGNOREQUANTITIESINREPORTS_YN').AsString <> 'Y');
      if (QueryProductionComp.FieldByName('JOB_CODE').AsString = DOWNJOB_1) or
        (QueryProductionComp.FieldByName('JOB_CODE').AsString = DOWNJOB_2) then
      begin
        Result := QRParameters.FIncludeDownTime <> DOWNTIME_NO;
      end;
    end;
  end; // IncludeJobQuantity
begin
  inherited;
  PrintBand := False;

  // 20014450.50
  with ReportProdCompTargetDM do
  begin
    if IncludeJobQuantity then // 20015586
      FPiecesJob := FPiecesJob +
        Round(QueryProductionComp.FieldByName('EMPLOYEEQUANTITY').AsFloat);
    FManHoursJob := FManHoursJob +
      Round(QueryProductionComp.FieldByName('EMPLOYEESECONDSACTUAL').AsFloat / 60);
  end;

  // 20014450.50
  // TD-21554 This must be done in Detail-part or it will miss records!
  with ReportProdCompTargetDM do
  begin
    qryMachineHours.Filtered := False;
    qryMachineHours.Filter :=
      'PLANT_CODE = ' + MyQuotes(QueryProductionComp.FieldByName('PLANT_CODE').AsString) +
      ' AND BUSINESSUNIT_CODE = ' + MyQuotes(QueryProductionComp.FieldByName('BUSINESSUNIT_CODE').AsString) +
      ' AND WORKSPOT_CODE = ' + MyQuotes(QueryProductionComp.FieldByName('WORKSPOT_CODE').AsString) +
      ' AND JOB_CODE = ' + MyQuotes(QueryProductionComp.FieldByName('JOB_CODE').AsString);
    qryMachineHours.Filtered := True;
    // TD-21554
    while not qryMachineHours.Eof do
    begin
      // 20015220.110
      if UseDateTime then
        MachineHours :=
          qryMachineHours.FieldByName('MACHINEHOURS').AsFloat
      else
        MachineHours :=
          qryMachineHours.FieldByName('DIFFDATE').AsFloat * 24 * 60;
      AddMachineClientDataSet(
       qryMachineHours.FieldByName('PLANT_CODE').AsString,
       qryMachineHours.FieldByName('BUSINESSUNIT_CODE').AsString,
       qryMachineHours.FieldByName('WORKSPOT_CODE').AsString,
       qryMachineHours.FieldByName('JOB_CODE').AsString,
       MachineHours
       );
       // 20014450.50
{      if IncludeJobQuantity then // 20015586
      begin
        if (UseDateTime or IncludeOpenScans) then
          FPiecesJob := FPiecesJob + DeterminePieces // 20015220.110
        else
          FPiecesJob := FPiecesJob +
            qryMachineHours.FieldByName('PIECES').AsInteger;
      end; }
      qryMachineHours.Next;
    end; // while
  end; // with
{
  with ReportProdCompTargetDM do
  begin
    // 20015221
    if (QueryProductionComp.FieldByName('MYTYPE').AsString = 'H') and
      (not (UseDateTime or IncludeOpenScans)) then // Hours
    begin
        // 20015220 + 20015221
        FManHoursJob := FManHoursJob +
          ReportProductionDetailDM.
            DetermineTimeRecProdMins(
              QRParameters.FDateFrom, QRParameters.FDateTo,
              ReportProdCompTargetDM.QueryProductionComp.FieldByName('PLANT_CODE').AsString,
              ReportProdCompTargetDM.QueryProductionComp.FieldByName('WORKSPOT_CODE').AsString,
              ReportProdCompTargetDM.QueryProductionComp.FieldByName('JOB_CODE').AsString,
              ReportProdCompTargetDM.QueryProductionComp.FieldByName('DEPARTMENT_CODE').AsString,
              '',
              -1,
              0,
              NullDate,
              QueryProductionComp.FieldByName('PIECES').Value
            ); // 20015220 + 20015221
    end;
  end;
}  
end;

procedure TReportProdCompTargetQR.QRLabelMachineHrsPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Round(FMachineHrsJob), True);
end;

procedure TReportProdCompTargetQR.QRBandFooterJobBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  DiffHrsJob: Double;
  JobRealProdMan,
  JobNormProdMan,
  EffJob,
  EmplNumberJob,
  WageCostJob,
  DiffWageCost: Double;
  function Minute(X: Double): Integer;
  var
    Hours, Min: Word;
  begin
    Hours := Trunc(X);
    Min := Round(Frac(X) * 60);
    Result := Hours * 60 + Min;
  end;
begin
  inherited;
  // TD-22171 Do this HERE (not during QRGroupJOB)! Otherwise it misses values
  //          determined during 'details'-band.
  if ReportProdCompTargetDM.ClientDataSetMachineHrs.
    FindKey([ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('PLANT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('BUSINESSUNIT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('WORKSPOT_CODE').AsString,
      ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('JOB_CODE').AsString]) then
     FMachineHrsJob := ReportProdCompTargetDM.ClientDataSetMachineHrs.
       FieldByName('MACHINE_HOURS').AsFloat;
{$IFDEF DEBUG}
  WDebugLog(
    'P=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('PLANT_CODE').AsString +
    ' W=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('WORKSPOT_CODE').AsString +
    ' J=' + ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('JOB_CODE').AsString +
    ' FMachineHrsJob=' + Format('%8.2f', [FMachineHrsJob])
    );
{$ENDIF}

  // 20015220.110
  if UseDateTime or IncludeOpenScans then
  begin
    FManHoursJob := DetermineHours;
  end;

  if FMachineHrsJob <> 0 then
  begin
    QRLabelOutputReal.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [(FPiecesJob/FMachineHrsJob) * 60]);
      Format('%.2n', [(FPiecesJob/FMachineHrsJob) * 60]);
    QRLabelOutputNorm.Caption :=
// ROP 05-MAR-2013 TD-21527      Format('%.2f', [FJobNorm * FManHoursJob/FMachineHrsJob]);
      Format('%.2n', [FJobNorm * FManHoursJob/FMachineHrsJob]);
  end
  else
  begin
    QRLabelOutputReal.Caption := '0.0';
    QRLabelOutputNorm.Caption := '0.0';
  end;

  //productivity per manhour
  JobRealProdMan := 0;
  JobNormProdMan := FJobNorm;
  if FManHoursJob <> 0 then
    JobRealProdMan := (FPiecesJob/FManHoursJob) * 60;

  QRLabelManHrs.Caption := DecodeHrsMin(Round(FManHoursJob), True);
// ROP 05-MAR-2013 TD-21527    QRLabelProdNorm.Caption := Format('%.2f', [JobNormProdMan]);
  QRLabelProdNorm.Caption := Format('%.2n', [JobNormProdMan]);
// ROP 05-MAR-2013 TD-21527  QRLabelProdReal.Caption := Format('%.2f', [JobRealProdMan]);
  QRLabelProdReal.Caption := Format('%.2n', [JobRealProdMan]);

  EmplNumberJob := 0;
  if FMachineHrsJob <> 0 then
    EmplNumberJob := FManHoursJob/FMachineHrsJob;
// ROP 05-MAR-2013 TD-21527  QRLabelNrOfEmpl.Caption := Format('%.2f', [EmplNumberJob]);
  QRLabelNrOfEmpl.Caption := Format('%.2n', [EmplNumberJob]);

  WageCostJob := (FManHoursJob /60)*
    ReportProdCompTargetDM.
      QueryProductionComp.FieldByName('AVERAGE_WAGE').AsFloat;
// ROP 05-MAR-2013 TD-21527  QRLabelWageCost.Caption := Format('%.2f', [WageCostJob]);
  QRLabelWageCost.Caption := Format('%.2n', [WageCostJob]);
// ROP bugfix after changing formats  WageCostJob := StrToFloat(QRLabelWageCost.Caption);
  WageCostJob := StrToFloat(Format('%.2f', [WageCostJob]));

  if JobNormProdMan <> 0 then
    DiffHrsJob := FManHoursJob - Minute(FPiecesJob/JobNormProdMan)
  else
    DiffHrsJob := FManHoursJob ;
  QRLabelDiffHrs.Caption := DecodeHrsMin(Round(DiffHrsJob), True);

  DiffWageCost := (DiffHrsJob/60)  *
    ReportProdCompTargetDM.QueryProductionComp.
      FieldByName('AVERAGE_WAGE').AsFloat;
// ROP 05-MAR-2013 TD-21527  QRLabelDiffWageCost.Caption := Format('%.2f', [DiffWageCost]);
  QRLabelDiffWageCost.Caption := Format('%.2n', [DiffWageCost]);
// ROP bugfix after changing formats  DiffWageCost := StrToFloat(QRLabelDiffWageCost.Caption);
  DiffWageCost := StrToFloat(Format('%.2f', [DiffWageCost]));

  EffJob := 0;
  if JobNormProdMan <> 0 then
    EffJob :=  100 * (JobRealProdMan / JobNormProdMan);
// ROP 05-MAR-2013 TD-21527  QRLabelEfficiency.Caption :=  Format('%.2f', [EffJob]);
  QRLabelEfficiency.Caption :=  Format('%.2n', [EffJob]);

  FWageCostWK :=  FWageCostWK + WageCostJob;
  FDiffHrsWK := FDiffHrsWK +  DiffHrsJob;
  FDiffWageCostWK := FDiffWageCostWK + DiffWageCost;

  // 20015220 When nothing was found, then do not print this footer
  //          This can happen when using a date+time selection. 
  if (FMachineHrsJob = 0) and (FPiecesJob = 0) and (FManHoursJob = 0) then
    PrintBand := False;
end; // QRBandFooterJobBeforePrint

procedure TReportProdCompTargetQR.QRBandGroupFooterWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // 20015220 + 20015221 Prevent it is showing this when nothing was found.
  if (FWageCostWK = 0) and (FDiffHrsWK = 0) and (FDiffWageCostWK = 0) then
    PrintBand := False;
// ROP 05-MAR-2013 TD-21527  QRLabelWageCostWK.Caption := Format('%.2f', [FWageCostWK]);
  QRLabelWageCostWK.Caption := Format('%.2n', [FWageCostWK]);
  QRLabelDiffHrsWK.Caption := DecodeHrsMin(Round(FDiffHrsWK), True);
// ROP 05-MAR-2013 TD-21527  QRLabelDiffWageCostWK.Caption := Format('%.2f', [FDiffWageCostWK]);
  QRLabelDiffWageCostWK.Caption := Format('%.2n', [FDiffWageCostWK]);
  FWageCostDept := FWageCostDept + FWageCostWK;
  FDiffHrsDept := FDiffHrsDept + FDiffHrsWK;
  FDiffWageCostDept := FDiffWageCostDept + FDiffWageCostWK;
end;

procedure TReportProdCompTargetQR.QRBandGroupFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  QRLabelWageCostDept.Caption := Format('%.2f', [FWageCostDept]);
  QRLabelWageCostDept.Caption := Format('%.2n', [FWageCostDept]);
  QRLabelDiffHrsDept.Caption := DecodeHrsMin(Round(FDiffHrsDept), True);
// ROP 05-MAR-2013 TD-21527  QRLabelDiffWageCostDept.Caption := Format('%.2f', [FDiffWageCostDept]);
  QRLabelDiffWageCostDept.Caption := Format('%.2n', [FDiffWageCostDept]);
  FWageCostBU := FWageCostDept + FWageCostBU;
  FDiffHrsBU := FDiffHrsDept + FDiffHrsBU;
  FDiffWageCostBU := FDiffWageCostDept + FDiffWageCostBU;
end;

procedure TReportProdCompTargetQR.QRBandGroupFooterBUBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  QRLabelWageCostBU.Caption := Format('%.2f', [FWageCostBU]);
  QRLabelWageCostBU.Caption := Format('%.2n', [FWageCostBU]);
  QRLabelDiffHrsBU.Caption := DecodeHrsMin(Round(FDiffHrsBU), True);
// ROP 05-MAR-2013 TD-21527  QRLabelDiffWageCostBU.Caption := Format('%.2f', [FDiffWageCostBU]);
  QRLabelDiffWageCostBU.Caption := Format('%.2n', [FDiffWageCostBU]);
  FWageCostPlant := FWageCostPlant + FWageCostBU;
  FDiffHrsPlant := FDiffHrsPlant + FDiffHrsBU;
  FDiffWageCostPlant := FDiffWageCostPlant + FDiffWageCostBU;
end;

procedure TReportProdCompTargetQR.QRBandGroupFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  QRLabelWageCostPlant.Caption := Format('%.2f', [FWageCostPlant]);
  QRLabelWageCostPlant.Caption := Format('%.2n', [FWageCostPlant]);
  QRLabelDiffHrsPlant.Caption := DecodeHrsMin(Round(FDiffHrsPlant), True);
// ROP 05-MAR-2013 TD-21527  QRLabelDiffWageCostPlant.Caption := Format('%.2f', [FDiffWageCostPlant]);
  QRLabelDiffWageCostPlant.Caption := Format('%.2n', [FDiffWageCostPlant]);
end;

(*
// 20015220.110
function TReportProdCompTargetQR.DeterminePieces: Double;
begin
  Result := 0;
  // There can be more than 1 record so use a filter here.
  with ReportProductionDetailDM do
  begin
    cdsEmpPieces.Filtered := False;
    cdsEmpPieces.Filter :=
      'PLANT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('PLANT_CODE').AsString) + ' AND ' +
      'WORKSPOT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('WORKSPOT_CODE').AsString) + ' AND ' +
      'JOB_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('JOB_CODE').AsString) + ' AND ' +
      'DEPARTMENT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('DEPARTMENT_CODE').AsString) + ' AND ' +
      'BUSINESSUNIT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('BUSINESSUNIT_CODE').AsString);
    cdsEmpPieces.Filtered := True;
    while not cdsEmpPieces.Eof do
    begin
      Result := Result + cdsEmpPieces.FieldByName('EMPPIECES').AsFloat;
      cdsEmpPieces.Next;
    end;
    cdsEmpPieces.Filtered := False;
  end; // with
end; // DeterminePieces
*)
// 20015220.110
function TReportProdCompTargetQR.DetermineHours: Double;
begin
  Result := 0;
  // There can be more than 1 record so use a filter here.
  with ReportProductionDetailDM do
  begin
    cdsEmpPieces.Filtered := False;
    cdsEmpPieces.Filter :=
      'PLANT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('PLANT_CODE').AsString) + ' AND ' +
      'WORKSPOT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('WORKSPOT_CODE').AsString) + ' AND ' +
      'JOB_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('JOB_CODE').AsString) + ' AND ' +
      'DEPARTMENT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('DEPARTMENT_CODE').AsString) + ' AND ' +
      'BUSINESSUNIT_CODE = ' +
        QuotedStr(ReportProdCompTargetDM.QueryProductionComp.
          FieldByName('BUSINESSUNIT_CODE').AsString);
    cdsEmpPieces.Filtered := True;
    while not cdsEmpPieces.Eof do
    begin
      Result := Result + cdsEmpPieces.FieldByName('PRODMINS').AsFloat;
      cdsEmpPieces.Next;
    end;
    cdsEmpPieces.Filtered := False;
  end; // with
end;  // DetermineHours

end.
