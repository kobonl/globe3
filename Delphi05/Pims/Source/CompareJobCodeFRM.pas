unit CompareJobCodeFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEdLib, dxDBELib, dxEditor, dxExEdtr, DBCtrls,
  StdCtrls, Mask, dxDBTLCl, dxGrClms, dxDBEdtr;

type
  TCompareJobCodesF = class(TGridBaseF)
    GroupBox2: TGroupBox;
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridLookupColumn;
    dxMasterGridColumn4: TdxDBGridLookupColumn;
    dxDetailGridColumnWK: TdxDBGridColumn;
    dxMasterGridColumn5: TdxDBGridCheckColumn;
    dxMasterGridColumn6: TdxDBGridCheckColumn;
    dxMasterGridColumn7: TdxDBGridCheckColumn;
    dxMasterGridColumn8: TdxDBGridDateColumn;
    dxMasterGridColumn9: TdxDBGridCheckColumn;
    dxMasterGridColumn10: TdxDBGridCheckColumn;
    dxMasterGridColumn11: TdxDBGridCheckColumn;
    dxMasterGridColumn12: TdxDBGridLookupColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    Label1: TLabel;
    Label2: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    DBEditPlant: TDBEdit;
    DBEditWK: TDBEdit;
    DBEditJobCodes: TDBEdit;
    DBEditDescription: TDBEdit;
    DBEditWKDesc: TDBEdit;
    DBEditPlantDesc: TDBEdit;
    dxDetailGridColumnWKDesc: TdxDBGridColumn;
    dxDetailGridColumnJobDESC: TdxDBGridColumn;
    DBLookupComboBoxWorkSpot: TDBLookupComboBox;
    DBLookupComboBoxJob: TDBLookupComboBox;
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    FInsertMode: Boolean;
  end;

function CompareJobCodesF: TCompareJobCodesF;

implementation

{$R *.DFM}

uses
   SystemDMT, WorkSpotDMT, UPimsMessageRes, UPimsConst;

var
  CompareJobCodesF_HDN: TCompareJobCodesF;

function CompareJobCodesF: TCompareJobCodesF;
begin
  if (CompareJobCodesF_HDN = nil) then
    CompareJobCodesF_HDN := TCompareJobCodesF.Create(Application);
  Result := CompareJobCodesF_HDN;
end;

procedure TCompareJobCodesF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBLookupComboBoxWorkSpot.SetFocus;
end;

procedure TCompareJobCodesF.FormDestroy(Sender: TObject);
begin
  inherited;
  CompareJobCodesF_HDN := Nil;
end;

procedure TCompareJobCodesF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
  inherited;
  CompareJobCodesF_HDN := Nil;
end;

procedure TCompareJobCodesF.FormShow(Sender: TObject);
begin
  inherited;
  WorkspotDM.OpenQueryForCompareJob;

end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TCompareJobCodesF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
