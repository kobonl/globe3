inherited ReportHolidayCardDM: TReportHolidayCardDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 475
  Width = 714
  inherited qryWorkspotMinMax: TQuery
    Top = 128
  end
  object QueryAbsence: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    Left = 56
    Top = 24
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryAbsence
    Left = 184
    Top = 24
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceAbsence
    TableName = 'PLANT'
    Left = 56
    Top = 192
  end
  object TableAbs: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'ABSENCEREASON'
    Left = 56
    Top = 88
  end
  object TableEmpl: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'EMPLOYEE_NUMBER'
    MasterFields = 'EMPLOYEE_NUMBER'
    MasterSource = DataSourceAbsence
    TableName = 'EMPLOYEE'
    Left = 56
    Top = 144
  end
  object QueryAbsencePlants: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  A.PLANT_CODE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A'
      'WHERE'
      '  A.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  A.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  A.ABSENCEHOUR_DATE >= :FDATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :FDATETO'
      'GROUP BY  '
      '  A.PLANT_CODE'
      'ORDER BY'
      '  1'
      ''
      ' ')
    Left = 56
    Top = 256
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReasons: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON'
      '    R.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      'WHERE'
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  A.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :DATETO'
      'GROUP BY'
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION'
      'ORDER BY'
      '  1')
    Left = 56
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotalCur: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  T.EMPLOYEE_NUMBER, '
      '  T.ABSENCE_YEAR,'
      '  T.LAST_YEAR_HOLIDAY_MINUTE,'
      '  T.NORM_HOLIDAY_MINUTE, '
      '  T.USED_HOLIDAY_MINUTE, '
      '  T.LAST_YEAR_TFT_MINUTE, '
      '  T.EARNED_TFT_MINUTE,'
      '  T.USED_TFT_MINUTE,'
      '  T.LAST_YEAR_WTR_MINUTE, '
      '  T.EARNED_WTR_MINUTE,'
      '  T.USED_WTR_MINUTE,'
      '  T.LAST_YEAR_HLD_PREV_YEAR_MINUTE,'
      '  T.USED_HLD_PREV_YEAR_MINUTE,'
      '  T.NORM_SENIORITY_HOLIDAY_MINUTE,'
      '  T.USED_SENIORITY_HOLIDAY_MINUTE,'
      '  T.LAST_YEAR_ADD_BANK_HLD_MINUTE,'
      '  T.USED_ADD_BANK_HLD_MINUTE,'
      '  T.LAST_YEAR_SAT_CREDIT_MINUTE,'
      '  T.USED_SAT_CREDIT_MINUTE,'
      '  T.NORM_BANK_HLD_RESERVE_MINUTE,'
      '  T.USED_BANK_HLD_RESERVE_MINUTE,'
      '  T.LAST_YEAR_SHORTWWEEK_MINUTE,'
      '  T.USED_SHORTWWEEK_MINUTE,'
      '  T.EARNED_SHORTWWEEK_MINUTE,'
      '  T.EARNED_SAT_CREDIT_MINUTE'
      'FROM '
      '  ABSENCETOTAL T '
      'WHERE '
      '  T.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  T.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  T.ABSENCE_YEAR = :ABSENCE_YEAR'
      'ORDER BY'
      '  1, 2'
      '')
    Left = 176
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceTotalPrev: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  T.EMPLOYEE_NUMBER, '
      '  T.ABSENCE_YEAR,'
      '  T.LAST_YEAR_HOLIDAY_MINUTE,'
      '  T.NORM_HOLIDAY_MINUTE, '
      '  T.USED_HOLIDAY_MINUTE, '
      '  T.LAST_YEAR_TFT_MINUTE, '
      '  T.EARNED_TFT_MINUTE,'
      '  T.USED_TFT_MINUTE,'
      '  T.LAST_YEAR_WTR_MINUTE, '
      '  T.EARNED_WTR_MINUTE,'
      '  T.USED_WTR_MINUTE,'
      '  T.LAST_YEAR_HLD_PREV_YEAR_MINUTE,'
      '  T.USED_HLD_PREV_YEAR_MINUTE,'
      '  T.NORM_SENIORITY_HOLIDAY_MINUTE,'
      '  T.USED_SENIORITY_HOLIDAY_MINUTE,'
      '  T.LAST_YEAR_ADD_BANK_HLD_MINUTE,'
      '  T.USED_ADD_BANK_HLD_MINUTE,'
      '  T.LAST_YEAR_SAT_CREDIT_MINUTE,'
      '  T.USED_SAT_CREDIT_MINUTE,'
      '  T.NORM_BANK_HLD_RESERVE_MINUTE,'
      '  T.USED_BANK_HLD_RESERVE_MINUTE,'
      '  T.LAST_YEAR_SHORTWWEEK_MINUTE,'
      '  T.USED_SHORTWWEEK_MINUTE,'
      '  T.EARNED_SHORTWWEEK_MINUTE,'
      '  T.EARNED_SAT_CREDIT_MINUTE'
      'FROM '
      '  ABSENCETOTAL T '
      'WHERE '
      '  T.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  T.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  T.ABSENCE_YEAR = :ABSENCE_YEAR'
      'ORDER BY'
      '  1, 2'
      ''
      ' ')
    Left = 296
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'ABSENCE_YEAR'
        ParamType = ptUnknown
      end>
  end
  object qryGetTFTHours: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  C.TIME_FOR_TIME_YN AS CGTFT_YN,'
      '  H.HOURTYPE_NUMBER,'
      '  H.BONUS_IN_MONEY_YN AS HT_BONUS_IN_MONEY_YN,'
      '  C.BONUS_IN_MONEY_YN,'
      '  H.BONUS_PERCENTAGE,'
      '  E.EMPLOYEE_NUMBER,'
      '  SHE.SALARY_DATE,'
      '  SUM(SHE.SALARY_MINUTE) AS SUMMINUTE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE SHE INNER JOIN HOURTYPE H ON'
      '    SHE.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '  INNER JOIN EMPLOYEE E ON'
      '    SHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      '  INNER JOIN CONTRACTGROUP C ON'
      '    E.CONTRACTGROUP_CODE = C.CONTRACTGROUP_CODE'
      'WHERE'
      '  E.PLANT_CODE >= :PLANTFROM AND'
      '  E.PLANT_CODE <= :PLANTTO AND'
      '  SHE.SALARY_DATE >= :FDATEFROM AND'
      '  SHE.SALARY_DATE <= :FDATETO AND'
      '  (H.OVERTIME_YN = '#39'Y'#39') AND'
      '  ('
      '    (C.TIME_FOR_TIME_YN = '#39'Y'#39' AND C.TIME_FOR_TIME_YN = '#39'Y'#39') OR'
      '    (H.TIME_FOR_TIME_YN = '#39'Y'#39' AND C.TIME_FOR_TIME_YN = '#39'N'#39')'
      '   )'
      'GROUP BY'
      '  C.TIME_FOR_TIME_YN,'
      '  H.HOURTYPE_NUMBER, H.BONUS_IN_MONEY_YN,'
      '  C.BONUS_IN_MONEY_YN, H.BONUS_PERCENTAGE,'
      '  E.EMPLOYEE_NUMBER,'
      '  SHE.SALARY_DATE'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER,'
      '  SHE.SALARY_DATE'
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 184
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATETO'
        ParamType = ptUnknown
      end>
  end
  object qryAbsenceReasonsEmp: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE A INNER JOIN ABSENCEREASON R ON'
      '    R.ABSENCEREASON_ID = A.ABSENCEREASON_ID'
      'WHERE'
      '  A.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  A.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  A.ABSENCEHOUR_DATE <= :DATETO'
      'GROUP BY'
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION'
      'ORDER BY'
      '  1')
    Left = 408
    Top = 296
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
