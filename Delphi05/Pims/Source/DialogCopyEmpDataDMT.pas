(*
  MRA:11-NOV-2016 PIM-174
  - New employee and add all related data
  MRA:18-FEB-2019 GLOB3-252
  - HLR: Copy function employees
  - Absence entered in Staff / Employee availability should NOT be copied
  MRA:22-FEB-2019 GLOB3-252
  - Bugfix: When it is about absence it should still copy standard available,
    not empty.
  MRA:12-APR-2019 GLOB3-282
  - Employee copy function also copy timeblocks-per-employee
*)
unit DialogCopyEmpDataDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, Db, DBTables, StdCtrls;

type
  TDialogCopyEmpDataDM = class(TDataModule)
    qryCopyEmpContract: TQuery;
    qryCopyWSPerEmp: TQuery;
    qryCopyStandardAvail: TQuery;
    qryCopyShiftSchedule: TQuery;
    qryCopyEmpAvail: TQuery;
    qryCopyEmpPlan: TQuery;
    qryCopyThis: TQuery;
    qryEmpContWorkSched: TQuery;
    qryCopyEmp: TQuery;
    qryEmp: TQuery;
    qryCopyEmpStdPlan: TQuery;
    qryCopyTBPEmp: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FMemo: TMemo;
    FWeekTo: Integer;
    FWeekFrom: Integer;
    FYear: Integer;
    procedure AddLog(AMsg: String);
    procedure WErrorLog(AMsg: String);
  public
    { Public declarations }
    function CopyEmpDataAction(AEmployeeNumber: Integer;
      AStartDate: TDateTime;
      AOtherEmployeeNumber: Integer;
      ADateFrom, ADateTo: TDateTime): Boolean;
    property Memo: TMemo read FMemo write FMemo;
    property Year: Integer read FYear write FYear;
    property WeekFrom: Integer read FWeekFrom write FWeekFrom;
    property WeekTo: Integer read FWeekTo write FWeekTo;
  end;

var
  DialogCopyEmpDataDM: TDialogCopyEmpDataDM;

implementation

{$R *.DFM}

uses UGlobalFunctions, UPimsMessageRes, WorkScheduleProcessDMT;

{ TDialogCopyEmpDataDM }

procedure TDialogCopyEmpDataDM.AddLog(AMsg: String);
begin
  if Assigned(Memo) then
    Memo.Lines.Add(AMsg);
end;

procedure TDialogCopyEmpDataDM.WErrorLog(AMsg: String);
begin
  AddLog(AMsg);
  UGlobalFunctions.WErrorLog(AMsg);
end;

function TDialogCopyEmpDataDM.CopyEmpDataAction(AEmployeeNumber: Integer;
  AStartDate: TDateTime; AOtherEmployeeNumber: Integer; ADateFrom,
  ADateTo: TDateTime): Boolean;
var
  WorkScheduleFound: Boolean;
  procedure CopyMsg(ATitle: String; ARows: Integer);
  begin
    AddLog(Format(ATitle + '.' + ' ' + SPimsCopied + ' %d ' + SPimsRows, [ARows]));
  end;
  procedure WorkScheduleCheck;
  begin
    try
      with qryEmpContWorkSched do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        Open;
        if not Eof then
          WorkScheduleFound := FieldByName('WORKSCHEDULE_ID').AsString <> '';
      end;
    except
    end;
  end; // WorkScheduleCheck;
  // Copy Employee
  procedure CopyEmp;
  begin
    try
      qryEmp.Close;
      qryEmp.ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
      qryEmp.Open;
      if not qryEmp.Eof then
      begin
        with qryCopyEmp do
        begin
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
          ParamByName('PLANT_CODE').AsString := qryEmp.FieldByName('PLANT_CODE').AsString;
          ParamByName('DEPARTMENT_CODE').AsString := qryEmp.FieldByName('DEPARTMENT_CODE').AsString;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ParamByName('LANGUAGE_CODE').AsString := qryEmp.FieldByName('LANGUAGE_CODE').AsString;
          ParamByName('TEAM_CODE').AsString := qryEmp.FieldByName('TEAM_CODE').AsString;
          ParamByName('CONTRACTGROUP_CODE').AsString := qryEmp.FieldByName('CONTRACTGROUP_CODE').AsString;
          ParamByName('IS_SCANNING_YN').AsString := qryEmp.FieldByName('IS_SCANNING_YN').AsString;
          ParamByName('CUT_OF_TIME_YN').AsString := qryEmp.FieldByName('CUT_OF_TIME_YN').AsString;
          ParamByName('CALC_BONUS_YN').AsString := qryEmp.FieldByName('CALC_BONUS_YN').AsString;
          ParamByName('GRADE').AsInteger := qryEmp.FieldByName('GRADE').AsInteger;
          ParamByName('SHIFT_NUMBER').AsInteger := qryEmp.FieldByName('SHIFT_NUMBER').AsInteger;
          ParamByName('BOOK_PROD_HRS_YN').AsString := qryEmp.FieldByName('BOOK_PROD_HRS_YN').AsString;
          ExecSQL;
          CopyMsg(SPimsEmp, RowsAffected);
        end;
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyEmp: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyEmp: ' + E.Message);
      end;
    end;
  end; // CopyEmp
  // Copy Employee Contract
  procedure CopyEmpContract;
  begin
    try
      with qryCopyEmpContract do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('STARTDATE').AsDateTime := AStartDate;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        CopyMsg(SPimsEmpContract, RowsAffected);
        WorkScheduleCheck;
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyEmpContract: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyEmpContract: ' + E.Message);
      end;
    end;
  end; // CopyEmpContract
  // Copy Workspots per employee
  procedure CopyWSPerEmp;
  begin
    try
      with qryCopyWSPerEmp do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('STARTDATE').AsDateTime := AStartDate;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        CopyMsg(SPimsWSPerEmp, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyWSPerEmp: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyWSPerEmp: ' + E.Message);
      end;
    end;
  end; // CopyWSPerEmp
  // Copy Standard Staff Availability
  procedure CopyStandardAvail;
  begin
    try
      with qryCopyStandardAvail do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        CopyMsg(SPimsStandStaffAvail, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyStandardAvail: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyStandardAvail: ' + E.Message);
      end;
    end;
  end; // CopyStandardAvail
  // Copy Shift Schedule (for the given period)
  procedure CopyShiftSchedule;
  begin
    try
      with qryCopyShiftSchedule do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ParamByName('DATEFROM').AsDateTime := ADateFrom;
        ParamByName('DATETO').AsDateTime := ADateTo;
        ExecSQL;
        CopyMsg(SPimsShiftSched, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyShiftSchedule: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyShiftSchedule: ' + E.Message);
      end;
    end;
  end; // CopyShiftSchedule
  // Copy Availability (for the given period)
  procedure CopyEmpAvail;
  begin
    try
      with qryCopyEmpAvail do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ParamByName('DATEFROM').AsDateTime := ADateFrom;
        ParamByName('DATETO').AsDateTime := ADateTo;
        ExecSQL;
        CopyMsg(SPimsEmpAvail, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyEmpAvail: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyEmpAvail: ' + E.Message);
      end;
    end;
  end; // CopyEmpAvail
  // Copy Employee standard planning
  procedure CopyEmpStdPlan;
  begin
    try
      with qryCopyEmpStdPlan do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        CopyMsg(SPimsStandEmpPlan, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyEmpStdPlan: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyEmpStdPlan: ' + E.Message);
      end;
    end;
  end; // CopyEmpStdPlan
  // Copy Employee Planning (for the given period)
  procedure CopyEmpPlan;
  begin
    try
      with qryCopyEmpPlan do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ParamByName('DATEFROM').AsDateTime := ADateFrom;
        ParamByName('DATETO').AsDateTime := ADateTo;
        ExecSQL;
        CopyMsg(SPimsEmpPlan, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('CopyEmpPlan: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('CopyEmpPlan: ' + E.Message);
      end;
    end;
  end; // CopyEmpPlan
  // Copy Timeblocks per Employee
  procedure CopyTBPEmp;
  begin
    try
      with qryCopyTBPEmp do
      begin
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('OTHEREMPLOYEE_NUMBER').AsInteger := AOtherEmployeeNumber;
        ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        ExecSQL;
        CopyMsg(SPimsTBPEmp, RowsAffected);
      end;
    except
      on E:EDBEngineError do
      begin
        Result := False;
        WErrorLog('SPimsTBPEmp: ' + E.Message);
      end;
      on E:Exception do
      begin
        Result := False;
        WErrorLog('SPimsTBPEmp: ' + E.Message);
      end;
    end;
  end; // CopyTBPEmp
begin
  Result := True;
  WorkScheduleFound := False;
  if AEmployeeNumber <> AOtherEmployeeNumber then
  begin
    // Assign to startdate of Employee
    ADateFrom := AStartDate;
    if ADateFrom <= ADateTo then
    begin
      CopyEmp;
      CopyEmpContract;
      CopyWSPerEmp;
      CopyStandardAvail;
      if WorkScheduleFound then
      begin
        WorkScheduleProcessDM.Year := Year;
        WorkScheduleProcessDM.FromWeek := WeekFrom;
        WorkScheduleProcessDM.ToWeek := WeekTo;
        WorkScheduleProcessDM.Memo1 := Memo;
        WorkScheduleProcessDM.ProcessWorkScheduleEmployee(AEmployeeNumber);
      end
      else
      begin
        CopyShiftSchedule;
        CopyEmpAvail;
      end;
      CopyEmpStdPlan;
      CopyEmpPlan;
      CopyTBPEmp;
    end
    else
    begin
      Result := False;
      AddLog(SPimsStartEndDate);
    end;
  end;
end; // CopyEmpDataAction

procedure TDialogCopyEmpDataDM.DataModuleCreate(Sender: TObject);
begin
  WorkScheduleProcessDM := TWorkScheduleProcessDM.Create(Self);
end;

procedure TDialogCopyEmpDataDM.DataModuleDestroy(Sender: TObject);
begin
  WorkScheduleProcessDM.Free;
end;

end.
