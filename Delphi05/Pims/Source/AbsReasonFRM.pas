(*
  Changes:
    MRA:8-JAN-2010. RV050.5. 889961.
    - Delete is not possible for absence reasons.
    SOLUTION: Make a referential key constraint on table ABSENCEHOURPEREMPLOYEE.
              This will prevent a delete when an absencereason is still in use.
    MRA:19-FEB-2018 Glob3-72.
    - Add Export_code to Absence Reason.
*)

unit AbsReasonFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, dxEditor, dxEdLib, dxDBELib,
  dxDBTLCl, dxGrClms, dxExEdtr, dxDBEdtr, SystemDMT;

type
  TAbsReasonF = class(TGridBaseF)
    Label1: TLabel;
    Label3: TLabel;
    DBEditCode: TdxDBEdit;
    DBCheckBoxPayed: TDBCheckBox;
    DBEditDescription: TdxDBEdit;
    Label2: TLabel;
    Label4: TLabel;
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnAbsenceType: TdxDBGridLookupColumn;
    dxDetailGridColumnHourType: TdxDBGridLookupColumn;
    dxDetailGridColumnPayed: TdxDBGridCheckColumn;
    dxDBLookupEditAbs: TdxDBLookupEdit;
    dxDBLookupEditHour: TdxDBLookupEdit;
    DBCheckBoxOverruleWithIllness: TDBCheckBox;
    dxDetailGridColumnOverruleWithIllness: TdxDBGridCheckColumn;
    dxDetailGridColumnCorrectWorkHrs: TdxDBGridCheckColumn;
    DBCheckBoxCorrectWorkHrs: TDBCheckBox;
    lblExportCode: TLabel;
    dxDBEditExportCode: TdxDBEdit;
    dxDetailGridColumnExportCode: TdxDBGridColumn;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function AbsReasonF: TAbsReasonF;

implementation

uses AbsReasonDMT;

{$R *.DFM}

var
  AbsReasonF_HND: TAbsReasonF;

// MR:09-02-2007 Order 550441.
// Delete has been disabled, to prevent problems with foreign keys.
  
function AbsReasonF: TAbsReasonF;
begin
  if (AbsReasonF_HND = nil) then
  begin
    AbsReasonF_HND := TAbsReasonF.Create(Application);
  end;
  Result := AbsReasonF_HND;
end;

procedure TAbsReasonF.FormDestroy(Sender: TObject);
begin
  inherited;
  AbsReasonF_HND := nil;
end;

procedure TAbsReasonF.FormCreate(Sender: TObject);
begin
  AbsReasonDM := CreateFormDM(TAbsReasonDM);
  if (dxDetailGrid.DataSource = nil) then
    dxDetailGrid.DataSource := AbsReasonDM.DataSourceDetail;
  inherited;
  // GLOB3-72
  lblExportCode.Visible := SystemDM.IsNTS;
  dxDBEditExportCode.Visible := SystemDM.IsNTS;
  dxDetailGridColumnExportCode.Visible := SystemDM.IsNTS;
  dxDetailGridColumnExportCode.DisableCustomizing := not SystemDM.IsNTS;
end;

procedure TAbsReasonF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

end.
