inherited DialogCopyFromSTAF: TDialogCopyFromSTAF
  Left = 372
  Top = 195
  Width = 386
  Height = 220
  Caption = 'Copy from'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlBottom: TPanel
    Top = 121
    Width = 370
    inherited btnOk: TBitBtn
      Left = 64
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 210
      OnClick = btnCancelClick
    end
  end
  inherited stbarBase: TStatusBar
    Top = 162
    Width = 370
  end
  object GroupBoxCopy: TGroupBox
    Left = 0
    Top = 0
    Width = 370
    Height = 121
    Align = alClient
    Caption = 'Copy standard staff availability of'
    TabOrder = 2
    object Label1: TLabel
      Left = 48
      Top = 32
      Width = 24
      Height = 13
      Caption = 'Plant'
    end
    object Label3: TLabel
      Left = 48
      Top = 92
      Width = 46
      Height = 13
      Caption = 'Employee'
    end
    object Label4: TLabel
      Left = 48
      Top = 64
      Width = 21
      Height = 13
      Caption = 'Shift'
    end
    object cmbPlusPlant: TComboBoxPlus
      Left = 120
      Top = 32
      Width = 177
      Height = 19
      ColCount = 62
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = DEFAULT_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      TabOrder = 0
      TitleColor = clBtnFace
      OnCloseUp = cmbPlusPlantCloseUp
    end
    object ComboBoxPlusShift: TComboBoxPlus
      Left = 120
      Top = 60
      Width = 177
      Height = 19
      ColCount = 63
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = DEFAULT_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      TabOrder = 1
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftCloseUp
    end
    object dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 120
      Top = 88
      Width = 177
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      AutoSize = False
      DataField = 'VALUEDISPLAY'
      DataSource = StandStaffAvailDM.DataSourceEmpl
      PopupHeight = 150
      PopupWidth = 350
      DBGridLayout = dxDBGridLayoutListEmployeeFrom
      Height = 19
    end
  end
  object QueryCopyFrom: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SA.DAY_OF_WEEK,'
      '  SA.AVAILABLE_TIMEBLOCK_1,'
      '  SA.AVAILABLE_TIMEBLOCK_2,'
      '  SA.AVAILABLE_TIMEBLOCK_3,'
      '  SA.AVAILABLE_TIMEBLOCK_4,'
      '  SA.AVAILABLE_TIMEBLOCK_5,'
      '  SA.AVAILABLE_TIMEBLOCK_6,'
      '  SA.AVAILABLE_TIMEBLOCK_7,'
      '  SA.AVAILABLE_TIMEBLOCK_8,'
      '  SA.AVAILABLE_TIMEBLOCK_9,'
      '  SA.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY SA'
      'WHERE '
      '  SA.PLANT_CODE = :PLANT_CODE AND '
      '  SA.SHIFT_NUMBER = :SHIFT_NUMBER AND '
      '  SA.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'ORDER BY'
      '  DAY_OF_WEEK'
      ''
      ' ')
    Left = 328
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QuerySTAValues: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  SA.DAY_OF_WEEK,'
      '  SA.AVAILABLE_TIMEBLOCK_1,'
      '  SA.AVAILABLE_TIMEBLOCK_2,'
      '  SA.AVAILABLE_TIMEBLOCK_3,'
      '  SA.AVAILABLE_TIMEBLOCK_4,'
      '  SA.AVAILABLE_TIMEBLOCK_5,'
      '  SA.AVAILABLE_TIMEBLOCK_6,'
      '  SA.AVAILABLE_TIMEBLOCK_7,'
      '  SA.AVAILABLE_TIMEBLOCK_8,'
      '  SA.AVAILABLE_TIMEBLOCK_9,'
      '  SA.AVAILABLE_TIMEBLOCK_10'
      'FROM'
      '  STANDARDAVAILABILITY SA'
      'WHERE'
      '  SA.PLANT_CODE = :PLANT AND SA.SHIFT_NUMBER = :SHIFT'
      '  AND SA.EMPLOYEE_NUMBER = :EMPLOYEE AND SA.DAY_OF_WEEK = :DAY'
      'ORDER BY'
      '  SA.DAY_OF_WEEK'
      ' '
      ' ')
    Left = 328
    Top = 56
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY'
        ParamType = ptUnknown
      end>
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 256
    object dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        40040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507205374616E645374616666417661696C444D2E4461746153
        6F75726365456D706C104F7074696F6E73437573746F6D697A650B0E6564676F
        42616E644D6F76696E670E6564676F42616E6453697A696E67106564676F436F
        6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E6564676F
        46756C6C53697A696E6700094F7074696F6E7344420B106564676F43616E6365
        6C4F6E457869740D6564676F43616E44656C6574650D6564676F43616E496E73
        657274116564676F43616E4E617669676174696F6E116564676F436F6E666972
        6D44656C657465126564676F4C6F6164416C6C5265636F726473106564676F55
        7365426F6F6B6D61726B7300000F546478444247726964436F6C756D6E0C436F
        6C756D6E4E756D6265720743617074696F6E06064E756D62657206536F727465
        6407046373557005576964746802410942616E64496E646578020008526F7749
        6E6465780200094669656C644E616D65060F454D504C4F5945455F4E554D4245
        5200000F546478444247726964436F6C756D6E0F436F6C756D6E53686F72744E
        616D650743617074696F6E060A53686F7274206E616D65055769647468025409
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060A53484F52545F4E414D4500000F546478444247726964436F6C756D6E0A43
        6F6C756D6E4E616D650743617074696F6E06044E616D6505576964746803B400
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060B4445534352495054494F4E00000F546478444247726964436F6C756D6E
        0D436F6C756D6E416464726573730743617074696F6E06074164647265737305
        576964746802450942616E64496E646578020008526F77496E64657802000946
        69656C644E616D6506074144445245535300000F546478444247726964436F6C
        756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F44657061
        72746D656E7420636F646505576964746802580942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060F4445504152544D454E54
        5F434F444500000F546478444247726964436F6C756D6E0A436F6C756D6E5465
        616D0743617074696F6E06095465616D20636F64650942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D6506095445414D5F434F44
        45000000}
    end
  end
end
