inherited StaffAvailabilityDM: TStaffAvailabilityDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 383
  Top = 189
  Height = 558
  Width = 777
  inherited TableMaster: TTable
    Filtered = True
    Left = 32
    Top = 24
  end
  inherited TableDetail: TTable
    MasterSource = nil
    TableName = ' '
    Left = 32
    Top = 84
  end
  inherited DataSourceMaster: TDataSource
    Left = 140
    Top = 24
  end
  inherited DataSourceDetail: TDataSource
    Left = 140
    Top = 76
  end
  inherited TableExport: TTable
    Top = 340
  end
  inherited DataSourceExport: TDataSource
    Top = 340
  end
  object QueryAvailability: TQuery
    AutoCalcFields = False
    AfterScroll = QueryAvailabilityAfterScroll
    OnCalcFields = QueryAvailabilityCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE EMPPLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE EMPSTARTDATE, E.ENDDATE EMPENDDATE,'
      '  CAST(MAX(ROWNUM) AS NUMBER) AS RECNO,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D11,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D12,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D13,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D14,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D15,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D16,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D17,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D18,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D19,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D110,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE = :STARTDATE AND EA2.EM' +
        'PLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D111,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D21,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D22,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D23,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D24,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D25,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D26,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D27,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D28,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D29,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D210,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 1 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D211,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D31,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D32,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D33,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D34,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D35,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D36,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D37,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D38,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D39,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D310,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 2 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D311,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D41,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D42,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D43,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D44,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D45,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D46,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D47,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D48,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D49,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D410,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 3 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D411,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D51,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D52,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D53,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D54,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D55,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D56,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D57,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D58,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D59,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D510,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 4 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D511,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D61,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D62,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D63,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D64,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D65,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D66,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D67,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D68,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D69,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D610,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 5 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D611,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D71,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D72,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D73,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D74,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_5 ELSE NULL END) D75,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_6 ELSE NULL END) D76,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_7 ELSE NULL END) D77,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_8 ELSE NULL END) D78,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_9 ELSE NULL END) D79,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_10 ELSE NULL END) D710,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 6 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D711,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE = :STARTDATE AND EA2.EM' +
        'PLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P1,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P2,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P3,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P4,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P5,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P6,'
      '  TO_CHAR('
      '    (SELECT NVL(EA2.PLANT_CODE, E.PLANT_CODE)'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) P7'
      'FROM'
      '  EMPLOYEE E'
      '  LEFT OUTER JOIN SHIFTSCHEDULE S ON'
      '    E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER'
      '    AND S.SHIFT_SCHEDULE_DATE >= :STARTDATE AND'
      '    (S.SHIFT_SCHEDULE_DATE - 6 <= :STARTDATE)'
      '  LEFT OUTER JOIN EMPLOYEEAVAILABILITY EA ON'
      '    (EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '    AND EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE AND'
      '    EA.EMPLOYEEAVAILABILITY_DATE - 6 <= :STARTDATE'
      '  LEFT OUTER JOIN TEAMPERUSER TPU ON'
      '    E.TEAM_CODE = TPU.TEAM_CODE'
      'WHERE'
      '  ((E.ENDDATE >= :STARTDATE) OR (E.ENDDATE IS NULL))'
      '  AND'
      '  (:EMP_PLANT_CODE = '#39'*'#39' OR E.PLANT_CODE = :EMP_PLANT_CODE) AND'
      '  ('
      '    ('
      '       (:USER_NAME <> '#39'*'#39') AND'
      '       E.TEAM_CODE IN'
      '         ('
      '           SELECT T.TEAM_CODE'
      '           FROM TEAMPERUSER T'
      '           WHERE'
      '             T.USER_NAME = :USER_NAME AND'
      '             ('
      '               (:TEAMFROM = '#39'*'#39') OR'
      '               (T.TEAM_CODE >= :TEAMFROM AND'
      '                T.TEAM_CODE <= :TEAMTO)'
      '             )'
      '         )'
      '     )'
      '     OR'
      '     ('
      '        (:USER_NAME = '#39'*'#39') AND'
      '        ('
      '          (:TEAMFROM = '#39'*'#39') OR'
      '          (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO)'
      '        )'
      '    )'
      '  )'
      '  AND :PLANT_CODE = :PLANT_CODE AND :INSERT_STAV = :INSERT_STAV'
      '  AND :CREATIONDATE = :CREATIONDATE AND :MUTATOR = :MUTATOR'
      'GROUP BY'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE, E.ENDDATE'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER, E.PLANT_CODE'
      ''
      ''
      ''
      ' '
      ' '
      ' ')
    UpdateMode = upWhereKeyOnly
    Left = 40
    Top = 140
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
    object QueryAvailabilityEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object QueryAvailabilityDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
    end
    object QueryAvailabilityDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
    end
    object QueryAvailabilityD11: TStringField
      FieldName = 'D11'
      Size = 1
    end
    object QueryAvailabilityD12: TStringField
      FieldName = 'D12'
      Size = 1
    end
    object QueryAvailabilityD13: TStringField
      FieldName = 'D13'
      Size = 1
    end
    object QueryAvailabilityD14: TStringField
      FieldName = 'D14'
      Size = 1
    end
    object QueryAvailabilityD15: TStringField
      DisplayWidth = 2
      FieldName = 'D15'
      Size = 1
    end
    object QueryAvailabilityD21: TStringField
      FieldName = 'D21'
      Size = 1
    end
    object QueryAvailabilityD22: TStringField
      FieldName = 'D22'
      Size = 1
    end
    object QueryAvailabilityD23: TStringField
      FieldName = 'D23'
      Size = 1
    end
    object QueryAvailabilityD24: TStringField
      FieldName = 'D24'
      Size = 1
    end
    object QueryAvailabilityD25: TStringField
      DisplayWidth = 2
      FieldName = 'D25'
      Size = 1
    end
    object QueryAvailabilityD31: TStringField
      FieldName = 'D31'
      Size = 1
    end
    object QueryAvailabilityD32: TStringField
      FieldName = 'D32'
      Size = 1
    end
    object QueryAvailabilityD33: TStringField
      FieldName = 'D33'
      Size = 1
    end
    object QueryAvailabilityD34: TStringField
      FieldName = 'D34'
      Size = 1
    end
    object QueryAvailabilityD35: TStringField
      DisplayWidth = 2
      FieldName = 'D35'
      Size = 1
    end
    object QueryAvailabilityD41: TStringField
      FieldName = 'D41'
      Size = 1
    end
    object QueryAvailabilityD42: TStringField
      FieldName = 'D42'
      Size = 1
    end
    object QueryAvailabilityD43: TStringField
      FieldName = 'D43'
      Size = 1
    end
    object QueryAvailabilityD44: TStringField
      FieldName = 'D44'
      Size = 1
    end
    object QueryAvailabilityD45: TStringField
      DisplayWidth = 2
      FieldName = 'D45'
      Size = 1
    end
    object QueryAvailabilityD51: TStringField
      FieldName = 'D51'
      Size = 1
    end
    object QueryAvailabilityD52: TStringField
      FieldName = 'D52'
      Size = 1
    end
    object QueryAvailabilityD53: TStringField
      FieldName = 'D53'
      Size = 1
    end
    object QueryAvailabilityD54: TStringField
      FieldName = 'D54'
      Size = 1
    end
    object QueryAvailabilityD55: TStringField
      DisplayWidth = 2
      FieldName = 'D55'
      Size = 1
    end
    object QueryAvailabilityD61: TStringField
      FieldName = 'D61'
      Size = 1
    end
    object QueryAvailabilityD62: TStringField
      FieldName = 'D62'
      Size = 1
    end
    object QueryAvailabilityD63: TStringField
      FieldName = 'D63'
      Size = 1
    end
    object QueryAvailabilityD64: TStringField
      FieldName = 'D64'
      Size = 1
    end
    object QueryAvailabilityD65: TStringField
      DisplayWidth = 2
      FieldName = 'D65'
      Size = 1
    end
    object QueryAvailabilityD71: TStringField
      FieldName = 'D71'
      Size = 1
    end
    object QueryAvailabilityD72: TStringField
      FieldName = 'D72'
      Size = 1
    end
    object QueryAvailabilityD73: TStringField
      FieldName = 'D73'
      Size = 1
    end
    object QueryAvailabilityD74: TStringField
      FieldName = 'D74'
      Size = 1
    end
    object QueryAvailabilityD75: TStringField
      DisplayWidth = 2
      FieldName = 'D75'
      Size = 1
    end
    object QueryAvailabilityRECNO: TFloatField
      FieldName = 'RECNO'
    end
    object QueryAvailabilityD11CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D11CALC'
      Calculated = True
    end
    object QueryAvailabilityD12Calc: TStringField
      FieldKind = fkCalculated
      FieldName = 'D12CALC'
      Calculated = True
    end
    object QueryAvailabilityD13Calc: TStringField
      FieldKind = fkCalculated
      FieldName = 'D13CALC'
      Calculated = True
    end
    object QueryAvailabilityD14Calc: TStringField
      FieldKind = fkCalculated
      FieldName = 'D14CALC'
      Calculated = True
    end
    object QueryAvailabilityD15CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D15CALC'
      Calculated = True
    end
    object QueryAvailabilityD21CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D21CALC'
      Calculated = True
    end
    object QueryAvailabilityD22CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D22CALC'
      Calculated = True
    end
    object QueryAvailabilityD23CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D23CALC'
      Calculated = True
    end
    object QueryAvailabilityD24CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D24CALC'
      Calculated = True
    end
    object QueryAvailabilityD25CALC: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D25CALC'
      Calculated = True
    end
    object QueryAvailabilityD31CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D31CALC'
      Calculated = True
    end
    object QueryAvailabilityD32CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D32CALC'
      Calculated = True
    end
    object QueryAvailabilityD33CALC: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D33CALC'
      Calculated = True
    end
    object QueryAvailabilityD34CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D34CALC'
      Calculated = True
    end
    object QueryAvailabilityD35CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D35CALC'
      Calculated = True
    end
    object QueryAvailabilityD41CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D41CALC'
      Calculated = True
    end
    object QueryAvailabilityD42CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D42CALC'
      Calculated = True
    end
    object QueryAvailabilityD43CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D43CALC'
      Calculated = True
    end
    object QueryAvailabilityD44CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D44CALC'
      Calculated = True
    end
    object QueryAvailabilityD45CALC: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D45CALC'
      Calculated = True
    end
    object QueryAvailabilityD51CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D51CALC'
      Calculated = True
    end
    object QueryAvailabilityD52CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D52CALC'
      Calculated = True
    end
    object QueryAvailabilityD53CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D53CALC'
      Calculated = True
    end
    object QueryAvailabilityD54CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D54CALC'
      Calculated = True
    end
    object QueryAvailabilityD55CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D55CALC'
      Calculated = True
    end
    object QueryAvailabilityD61CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D61CALC'
      Calculated = True
    end
    object QueryAvailabilityD62CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D62CALC'
      Calculated = True
    end
    object QueryAvailabilityD63CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D63CALC'
      Calculated = True
    end
    object QueryAvailabilityD64CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D64CALC'
      Calculated = True
    end
    object QueryAvailabilityD65CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D65CALC'
      Calculated = True
    end
    object QueryAvailabilityD71CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D71CALC'
      Calculated = True
    end
    object QueryAvailabilityD72CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D72CALC'
      Calculated = True
    end
    object QueryAvailabilityD73CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D73CALC'
      Calculated = True
    end
    object QueryAvailabilityD74CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D74CALC'
      Calculated = True
    end
    object QueryAvailabilityD75CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D75CALC'
      Calculated = True
    end
    object QueryAvailabilityFEMP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object QueryAvailabilityEMPSTARTDATE: TDateTimeField
      FieldName = 'EMPSTARTDATE'
    end
    object QueryAvailabilityEMPENDDATE: TDateTimeField
      FieldName = 'EMPENDDATE'
    end
    object QueryAvailabilityP1: TStringField
      FieldName = 'P1'
      Size = 6
    end
    object QueryAvailabilityP2: TStringField
      FieldName = 'P2'
      Size = 6
    end
    object QueryAvailabilityP3: TStringField
      FieldName = 'P3'
      Size = 6
    end
    object QueryAvailabilityP4: TStringField
      FieldName = 'P4'
      Size = 6
    end
    object QueryAvailabilityP5: TStringField
      FieldName = 'P5'
      Size = 6
    end
    object QueryAvailabilityP6: TStringField
      FieldName = 'P6'
      Size = 6
    end
    object QueryAvailabilityP7: TStringField
      FieldName = 'P7'
      Size = 6
    end
    object QueryAvailabilityP1CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P1CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP2CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P2CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP3CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P3CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP4CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P4CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP5CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P5CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP6CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P6CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityP7CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'P7CALC'
      Size = 6
      Calculated = True
    end
    object QueryAvailabilityEMPPLANT_CODE: TStringField
      FieldName = 'EMPPLANT_CODE'
      Size = 6
    end
    object QueryAvailabilityD16: TStringField
      FieldName = 'D16'
      Size = 4
    end
    object QueryAvailabilityD17: TStringField
      FieldName = 'D17'
      Size = 4
    end
    object QueryAvailabilityD18: TStringField
      FieldName = 'D18'
      Size = 4
    end
    object QueryAvailabilityD19: TStringField
      FieldName = 'D19'
      Size = 4
    end
    object QueryAvailabilityD110: TStringField
      FieldName = 'D110'
      Size = 4
    end
    object QueryAvailabilityD111: TStringField
      FieldName = 'D111'
      Size = 40
    end
    object QueryAvailabilityD26: TStringField
      FieldName = 'D26'
      Size = 4
    end
    object QueryAvailabilityD27: TStringField
      FieldName = 'D27'
      Size = 4
    end
    object QueryAvailabilityD28: TStringField
      FieldName = 'D28'
      Size = 4
    end
    object QueryAvailabilityD29: TStringField
      FieldName = 'D29'
      Size = 4
    end
    object QueryAvailabilityD210: TStringField
      FieldName = 'D210'
      Size = 4
    end
    object QueryAvailabilityD211: TStringField
      FieldName = 'D211'
      Size = 40
    end
    object QueryAvailabilityD36: TStringField
      FieldName = 'D36'
      Size = 4
    end
    object QueryAvailabilityD37: TStringField
      FieldName = 'D37'
      Size = 4
    end
    object QueryAvailabilityD38: TStringField
      FieldName = 'D38'
      Size = 4
    end
    object QueryAvailabilityD39: TStringField
      FieldName = 'D39'
      Size = 4
    end
    object QueryAvailabilityD310: TStringField
      FieldName = 'D310'
      Size = 4
    end
    object QueryAvailabilityD311: TStringField
      FieldName = 'D311'
      Size = 40
    end
    object QueryAvailabilityD46: TStringField
      FieldName = 'D46'
      Size = 4
    end
    object QueryAvailabilityD47: TStringField
      FieldName = 'D47'
      Size = 4
    end
    object QueryAvailabilityD48: TStringField
      FieldName = 'D48'
      Size = 4
    end
    object QueryAvailabilityD49: TStringField
      FieldName = 'D49'
      Size = 4
    end
    object QueryAvailabilityD410: TStringField
      FieldName = 'D410'
      Size = 4
    end
    object QueryAvailabilityD411: TStringField
      FieldName = 'D411'
      Size = 40
    end
    object QueryAvailabilityD56: TStringField
      FieldName = 'D56'
      Size = 4
    end
    object QueryAvailabilityD57: TStringField
      FieldName = 'D57'
      Size = 4
    end
    object QueryAvailabilityD58: TStringField
      FieldName = 'D58'
      Size = 4
    end
    object QueryAvailabilityD59: TStringField
      FieldName = 'D59'
      Size = 4
    end
    object QueryAvailabilityD510: TStringField
      FieldName = 'D510'
      Size = 4
    end
    object QueryAvailabilityD511: TStringField
      FieldName = 'D511'
      Size = 40
    end
    object QueryAvailabilityD66: TStringField
      FieldName = 'D66'
      Size = 4
    end
    object QueryAvailabilityD67: TStringField
      FieldName = 'D67'
      Size = 4
    end
    object QueryAvailabilityD68: TStringField
      FieldName = 'D68'
      Size = 4
    end
    object QueryAvailabilityD69: TStringField
      FieldName = 'D69'
      Size = 4
    end
    object QueryAvailabilityD610: TStringField
      FieldName = 'D610'
      Size = 4
    end
    object QueryAvailabilityD611: TStringField
      FieldName = 'D611'
      Size = 40
    end
    object QueryAvailabilityD76: TStringField
      FieldName = 'D76'
      Size = 4
    end
    object QueryAvailabilityD77: TStringField
      FieldName = 'D77'
      Size = 4
    end
    object QueryAvailabilityD78: TStringField
      FieldName = 'D78'
      Size = 4
    end
    object QueryAvailabilityD79: TStringField
      FieldName = 'D79'
      Size = 4
    end
    object QueryAvailabilityD710: TStringField
      FieldName = 'D710'
      Size = 4
    end
    object QueryAvailabilityD711: TStringField
      FieldName = 'D711'
      Size = 40
    end
    object QueryAvailabilityD16CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D16CALC'
      Calculated = True
    end
    object QueryAvailabilityD17CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D17CALC'
      Calculated = True
    end
    object QueryAvailabilityD18CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D18CALC'
      Calculated = True
    end
    object QueryAvailabilityD19CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D19CALC'
      Calculated = True
    end
    object QueryAvailabilityD110CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D110CALC'
      Calculated = True
    end
    object QueryAvailabilityD111CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D111CALC'
      Calculated = True
    end
    object QueryAvailabilityD26CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D26CALC'
      Calculated = True
    end
    object QueryAvailabilityD27CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D27CALC'
      Calculated = True
    end
    object QueryAvailabilityD28CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D28CALC'
      Calculated = True
    end
    object QueryAvailabilityD29CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D29CALC'
      Calculated = True
    end
    object QueryAvailabilityD210CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D210CALC'
      Calculated = True
    end
    object QueryAvailabilityD211CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D211CALC'
      Calculated = True
    end
    object QueryAvailabilityD36CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D36CALC'
      Calculated = True
    end
    object QueryAvailabilityD37CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D37CALC'
      Calculated = True
    end
    object QueryAvailabilityD38CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D38CALC'
      Calculated = True
    end
    object QueryAvailabilityD39CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D39CALC'
      Calculated = True
    end
    object QueryAvailabilityD310CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D310CALC'
      Calculated = True
    end
    object QueryAvailabilityD311CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D311CALC'
      Calculated = True
    end
    object QueryAvailabilityD46CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D46CALC'
      Calculated = True
    end
    object QueryAvailabilityD47CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D47CALC'
      Calculated = True
    end
    object QueryAvailabilityD48CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D48CALC'
      Calculated = True
    end
    object QueryAvailabilityD49CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D49CALC'
      Calculated = True
    end
    object QueryAvailabilityD410CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D410CALC'
      Calculated = True
    end
    object QueryAvailabilityD411CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D411CALC'
      Calculated = True
    end
    object QueryAvailabilityD56CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D56CALC'
      Calculated = True
    end
    object QueryAvailabilityD57CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D57CALC'
      Calculated = True
    end
    object QueryAvailabilityD58CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D58CALC'
      Calculated = True
    end
    object QueryAvailabilityD59CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D59CALC'
      Calculated = True
    end
    object QueryAvailabilityD510CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D510CALC'
      Calculated = True
    end
    object QueryAvailabilityD511CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D511CALC'
      Calculated = True
    end
    object QueryAvailabilityD66CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D66CALC'
      Calculated = True
    end
    object QueryAvailabilityD67CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D67CALC'
      Calculated = True
    end
    object QueryAvailabilityD68CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D68CALC'
      Calculated = True
    end
    object QueryAvailabilityD69CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D69CALC'
      Calculated = True
    end
    object QueryAvailabilityD610CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D610CALC'
      Calculated = True
    end
    object QueryAvailabilityD611CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D611CALC'
      Calculated = True
    end
    object QueryAvailabilityD76CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D76CALC'
      Calculated = True
    end
    object QueryAvailabilityD77CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D77CALC'
      Calculated = True
    end
    object QueryAvailabilityD78CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D78CALC'
      Calculated = True
    end
    object QueryAvailabilityD79CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D79CALC'
      Calculated = True
    end
    object QueryAvailabilityD710CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D710CALC'
      Calculated = True
    end
    object QueryAvailabilityD711CALC: TStringField
      FieldKind = fkCalculated
      FieldName = 'D711CALC'
      Calculated = True
    end
  end
  object DataSourceAvailability: TDataSource
    DataSet = QueryAvailability
    Left = 140
    Top = 140
  end
  object QueryWork: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 256
    Top = 20
  end
  object QueryWorkRestore: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 360
    Top = 20
  end
  object ClientDataSetAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsRsn'
    Left = 360
    Top = 136
  end
  object DataSetProviderAbsRsn: TDataSetProvider
    DataSet = QueryAbsReason
    Constraints = True
    Left = 480
    Top = 136
  end
  object QueryAbsReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT ABSENCEREASON_CODE, DESCRIPTION FROM ABSENCEREASON'
      ' ORDER BY DESCRIPTION'
      ' ')
    Left = 256
    Top = 136
  end
  object QueryTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT TEAM_CODE , DESCRIPTION'
      ' FROM TEAM T   ORDER BY TEAM_CODE')
    Left = 64
    Top = 264
  end
  object QueryPlant: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantFilterRecord
    SQL.Strings = (
      'SELECT PLANT_CODE, DESCRIPTION, COUNTRY_ID'
      'FROM PLANT'
      'ORDER BY PLANT_CODE'
      ' ')
    Left = 168
    Top = 264
  end
  object QuerySelect: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 512
    Top = 216
  end
  object QuerySelectPlanning: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT 1 AS RECNO,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB1, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' STD_TB2,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB3, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' STD_TB4,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB5, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' STD_TB6,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB7, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' STD_TB8,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS STD_TB9, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' STD_TB10,'
      
        '  EMA.AVAILABLE_TIMEBLOCK_1 AS EMA_TB1, EMA.AVAILABLE_TIMEBLOCK_' +
        '2 AS EMA_TB2,'
      
        '  EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB3, EMA.AVAILABLE_TIMEBLOCK_' +
        '4 AS EMA_TB4,'
      
        '  EMA.AVAILABLE_TIMEBLOCK_5 AS EMA_TB5, EMA.AVAILABLE_TIMEBLOCK_' +
        '6 AS EMA_TB6,'
      
        '  EMA.AVAILABLE_TIMEBLOCK_7 AS EMA_TB7, EMA.AVAILABLE_TIMEBLOCK_' +
        '8 AS EMA_TB8,'
      
        '  EMA.AVAILABLE_TIMEBLOCK_9 AS EMA_TB9, EMA.AVAILABLE_TIMEBLOCK_' +
        '10 AS EMA_TB10'
      'FROM'
      
        '  EMPLOYEEAVAILABILITY EMA WHERE EMA.EMPLOYEE_NUMBER = :EMPNO AN' +
        'D'
      '  EMA.EMPLOYEEAVAILABILITY_DATE = :EDATE AND'
      '  EMA.PLANT_CODE = :PCODE AND EMA.SHIFT_NUMBER = :SNO'
      'UNION SELECT 2 AS RECNO,'
      
        '  STD.AVAILABLE_TIMEBLOCK_1 AS STD_TB1, STD.AVAILABLE_TIMEBLOCK_' +
        '2 AS STD_TB2,'
      
        '  STD.AVAILABLE_TIMEBLOCK_3 AS STD_TB3, STD.AVAILABLE_TIMEBLOCK_' +
        '4 AS STD_TB4,'
      
        '  STD.AVAILABLE_TIMEBLOCK_5 AS STD_TB5, STD.AVAILABLE_TIMEBLOCK_' +
        '6 AS STD_TB6,'
      
        '  STD.AVAILABLE_TIMEBLOCK_7 AS STD_TB7, STD.AVAILABLE_TIMEBLOCK_' +
        '8 AS STD_TB8,'
      
        '  STD.AVAILABLE_TIMEBLOCK_9 AS STD_TB9, STD.AVAILABLE_TIMEBLOCK_' +
        '10 AS STD_TB10,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB1, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' EMA_TB2,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB3, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' EMA_TB4,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB5, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' EMA_TB6,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB7, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' EMA_TB8,'
      
        '  CAST('#39'*'#39' AS VARCHAR(1)) AS EMA_TB9, CAST('#39'*'#39' AS VARCHAR(1)) AS' +
        ' EMA_TB10'
      'FROM'
      '  STANDARDAVAILABILITY STD, SHIFTSCHEDULE SHS WHERE'
      '  STD.EMPLOYEE_NUMBER = :EMPNO AND STD.DAY_OF_WEEK = :DAY AND'
      '   STD.PLANT_CODE = :PCODE AND STD.SHIFT_NUMBER = :SNO AND'
      '   SHS.EMPLOYEE_NUMBER = :EMPNO AND'
      '   SHS.SHIFT_NUMBER = 0 AND SHS.SHIFT_SCHEDULE_DATE = :EDATE AND'
      
        '   (STD.EMPLOYEE_NUMBER  NOT IN (SELECT EMPLOYEE_NUMBER FROM EMP' +
        'LOYEEAVAILABILITY'
      
        '   WHERE  EMPLOYEEAVAILABILITY_DATE = :EDATE AND SHIFT_NUMBER = ' +
        '0 AND'
      '     EMPLOYEE_NUMBER = :EMPNO))'
      'UNION SELECT 3 AS RECNO,'
      
        '   STD.AVAILABLE_TIMEBLOCK_1 AS STD_TB1, STD.AVAILABLE_TIMEBLOCK' +
        '_2 AS STD_TB2,'
      
        '   STD.AVAILABLE_TIMEBLOCK_3 AS STD_TB3, STD.AVAILABLE_TIMEBLOCK' +
        '_4 AS STD_TB4,'
      
        '   STD.AVAILABLE_TIMEBLOCK_5 AS STD_TB5, STD.AVAILABLE_TIMEBLOCK' +
        '_6 AS STD_TB6,'
      
        '   STD.AVAILABLE_TIMEBLOCK_7 AS STD_TB7, STD.AVAILABLE_TIMEBLOCK' +
        '_8 AS STD_TB8,'
      
        '   STD.AVAILABLE_TIMEBLOCK_9 AS STD_TB9, STD.AVAILABLE_TIMEBLOCK' +
        '_10 AS STD_TB10,'
      
        '   EMA.AVAILABLE_TIMEBLOCK_1 AS EMA_TB1, EMA.AVAILABLE_TIMEBLOCK' +
        '_2 AS EMA_TB2,'
      
        '   EMA.AVAILABLE_TIMEBLOCK_3 AS EMA_TB3, EMA.AVAILABLE_TIMEBLOCK' +
        '_4 AS EMA_TB4,'
      
        '   EMA.AVAILABLE_TIMEBLOCK_5 AS EMA_TB5, EMA.AVAILABLE_TIMEBLOCK' +
        '_6 AS EMA_TB6,'
      
        '   EMA.AVAILABLE_TIMEBLOCK_7 AS EMA_TB7, EMA.AVAILABLE_TIMEBLOCK' +
        '_8 AS EMA_TB8,'
      
        '   EMA.AVAILABLE_TIMEBLOCK_9 AS EMA_TB9, EMA.AVAILABLE_TIMEBLOCK' +
        '_10 AS EMA_TB10'
      'FROM'
      
        '   STANDARDAVAILABILITY STD, SHIFTSCHEDULE SHS, EMPLOYEEAVAILABI' +
        'LITY EMA'
      '   WHERE STD.EMPLOYEE_NUMBER = :EMPNO AND'
      '   STD.DAY_OF_WEEK = :DAY AND  STD.PLANT_CODE = :PCODE AND'
      '   STD.SHIFT_NUMBER = :SNO AND SHS.EMPLOYEE_NUMBER = :EMPNO AND'
      '   SHS.SHIFT_NUMBER = 0 AND SHS.SHIFT_SCHEDULE_DATE = :EDATE AND'
      
        '   (EMA.EMPLOYEE_NUMBER = :EMPNO AND EMA.EMPLOYEEAVAILABILITY_DA' +
        'TE = :EDATE AND'
      '   EMA.SHIFT_NUMBER = 0)')
    Left = 376
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'DAY'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PCODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPNO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'EDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryPlantTeam: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT'
      '  DPT.PLANT_CODE,'
      '  P.DESCRIPTION'
      'FROM'
      '  DEPARTMENTPERTEAM DPT,'
      '  PLANT P'
      'WHERE'
      '  DPT.TEAM_CODE = :TEAM_CODE AND'
      '  DPT.PLANT_CODE = P.PLANT_CODE')
    Left = 256
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'TEAM_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT SHIFT_NUMBER, PLANT_CODE , DESCRIPTION, '
      
        'STARTTIME1,  ENDTIME1, STARTTIME2, ENDTIME2, STARTTIME3, ENDTIME' +
        '3,'
      
        'STARTTIME4, ENDTIME4, STARTTIME5, ENDTIME5, STARTTIME6, ENDTIME6' +
        ','
      
        'STARTTIME7, ENDTIME7 FROM SHIFT ORDER BY PLANT_CODE, SHIFT_NUMBE' +
        'R'
      ''
      '')
    Left = 256
    Top = 72
  end
  object ClientDataSetShift: TClientDataSet
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'DataSetProviderShift'
    Left = 360
    Top = 72
  end
  object DataSetProviderShift: TDataSetProvider
    DataSet = QueryShift
    Constraints = True
    Left = 480
    Top = 72
  end
  object StoredProcAVAILABILITY: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'STAFFAVAIL_QUERY'
    Left = 52
    Top = 208
    ParamData = <
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'EMP_PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'RECNO'
        ParamType = ptOutput
      end
      item
        DataType = ftFloat
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'DESCRIPTION'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'EMPPLANT_CODE'
        ParamType = ptOutput
      end
      item
        DataType = ftDateTime
        Name = 'EMPSTARTDATE'
        ParamType = ptOutput
      end
      item
        DataType = ftDateTime
        Name = 'EMPENDDATE'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D11'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D12'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D13'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D14'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D15'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D21'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D22'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D23'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D24'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D25'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D31'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D32'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D33'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D34'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D35'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D41'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D42'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D43'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D44'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D45'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D51'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D52'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D53'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D54'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D55'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D61'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D62'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D63'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D64'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D65'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D71'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D72'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D73'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D74'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'D75'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P2'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P3'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P5'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P6'
        ParamType = ptOutput
      end
      item
        DataType = ftString
        Name = 'P7'
        ParamType = ptOutput
      end
      item
        DataType = ftInteger
        Name = 'STATUS'
        ParamType = ptInputOutput
      end>
  end
  object QueryAvailabilityXXX: TQuery
    AutoCalcFields = False
    AfterScroll = QueryAvailabilityAfterScroll
    OnCalcFields = QueryAvailabilityCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, '
      '  E.PLANT_CODE EMPPLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE EMPSTARTDATE, E.ENDDATE EMPENDDATE,'
      '  CAST(MAX(ROWNUM) AS NUMBER) AS RECNO,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 0 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D11,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 0 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D12,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 0 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D13,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 0 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D14,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 0 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 0 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D15,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D21,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D22,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D23,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D24,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 1 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 1 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D25,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D31,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D32,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D33,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D34,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 2 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 2 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D35,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D41,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D42,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D43,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D44,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 3 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 3 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D45,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D51,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D52,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D53,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D54,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 4 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 4 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D55,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D61,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D62,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D63,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D64,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 5 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 5 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D65,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D71,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D72,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D73,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D74,'
      
        '  TO_CHAR(MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE- 6 = :START' +
        'DATE THEN EA.SHIFT_NUMBER'
      
        '    WHEN S.SHIFT_SCHEDULE_DATE - 6 = :STARTDATE THEN S.SHIFT_NUM' +
        'BER ELSE NULL END)) D75,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 0 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P1,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P2,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P3,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P4,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P5,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P6,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 7 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P7'
      'FROM'
      '  EMPLOYEE E'
      '  LEFT OUTER JOIN SHIFTSCHEDULE S ON'
      '    E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER'
      '    AND S.SHIFT_SCHEDULE_DATE >= :STARTDATE AND'
      '    (S.SHIFT_SCHEDULE_DATE - 6 <= :STARTDATE)'
      '  LEFT OUTER JOIN EMPLOYEEAVAILABILITY EA ON'
      '    (EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER) '
      '    AND EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE AND  '
      '    EA.EMPLOYEEAVAILABILITY_DATE - 6 <= :STARTDATE  '
      '  LEFT OUTER JOIN TEAMPERUSER TPU ON  '
      '    E.TEAM_CODE = TPU.TEAM_CODE  '
      'WHERE '
      
        '  (:EMP_PLANT_CODE = '#39'*'#39' OR E.PLANT_CODE = :EMP_PLANT_CODE) AND ' +
        ' '
      '  (  '
      '    (  '
      '       (:USER_NAME <> '#39'*'#39') AND   '
      '       E.TEAM_CODE IN   '
      '         (  '
      '           SELECT T.TEAM_CODE   '
      '           FROM TEAMPERUSER T  '
      '           WHERE   '
      '             T.USER_NAME = :USER_NAME AND  '
      '             (  '
      '               (:TEAMFROM = '#39'*'#39') OR   '
      '               (T.TEAM_CODE >= :TEAMFROM AND  '
      '                T.TEAM_CODE <= :TEAMTO)  '
      '             )  '
      '         )  '
      '     )  '
      '     OR  '
      '     (  '
      '        (:USER_NAME = '#39'*'#39') AND  '
      '        (   '
      '          (:TEAMFROM = '#39'*'#39') OR  '
      
        '          (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO) ' +
        '  '
      '        )  '
      '    )  '
      '  )  '
      
        '  AND :PLANT_CODE = :PLANT_CODE AND :INSERT_STAV = :INSERT_STAV ' +
        ' '
      '  AND :CREATIONDATE = :CREATIONDATE AND :MUTATOR = :MUTATOR '
      'GROUP BY '
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION, '
      '  E.PLANT_CODE, '
      '  E.DEPARTMENT_CODE, '
      '  E.STARTDATE, E.ENDDATE '
      'ORDER BY '
      '  E.EMPLOYEE_NUMBER, E.PLANT_CODE'
      ''
      ' '
      ' ')
    UpdateMode = upWhereKeyOnly
    Left = 56
    Top = 380
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object StringField1: TStringField
      FieldName = 'DESCRIPTION'
    end
    object StringField2: TStringField
      FieldName = 'DEPARTMENT_CODE'
    end
    object StringField3: TStringField
      FieldName = 'D11'
      Size = 1
    end
    object StringField4: TStringField
      FieldName = 'D12'
      Size = 1
    end
    object StringField5: TStringField
      FieldName = 'D13'
      Size = 1
    end
    object StringField6: TStringField
      FieldName = 'D14'
      Size = 1
    end
    object StringField7: TStringField
      DisplayWidth = 2
      FieldName = 'D15'
      Size = 1
    end
    object StringField8: TStringField
      FieldName = 'D21'
      Size = 1
    end
    object StringField9: TStringField
      FieldName = 'D22'
      Size = 1
    end
    object StringField10: TStringField
      FieldName = 'D23'
      Size = 1
    end
    object StringField11: TStringField
      FieldName = 'D24'
      Size = 1
    end
    object StringField12: TStringField
      DisplayWidth = 2
      FieldName = 'D25'
      Size = 1
    end
    object StringField13: TStringField
      FieldName = 'D31'
      Size = 1
    end
    object StringField14: TStringField
      FieldName = 'D32'
      Size = 1
    end
    object StringField15: TStringField
      FieldName = 'D33'
      Size = 1
    end
    object StringField16: TStringField
      FieldName = 'D34'
      Size = 1
    end
    object StringField17: TStringField
      DisplayWidth = 2
      FieldName = 'D35'
      Size = 1
    end
    object StringField18: TStringField
      FieldName = 'D41'
      Size = 1
    end
    object StringField19: TStringField
      FieldName = 'D42'
      Size = 1
    end
    object StringField20: TStringField
      FieldName = 'D43'
      Size = 1
    end
    object StringField21: TStringField
      FieldName = 'D44'
      Size = 1
    end
    object StringField22: TStringField
      DisplayWidth = 2
      FieldName = 'D45'
      Size = 1
    end
    object StringField23: TStringField
      FieldName = 'D51'
      Size = 1
    end
    object StringField24: TStringField
      FieldName = 'D52'
      Size = 1
    end
    object StringField25: TStringField
      FieldName = 'D53'
      Size = 1
    end
    object StringField26: TStringField
      FieldName = 'D54'
      Size = 1
    end
    object StringField27: TStringField
      DisplayWidth = 2
      FieldName = 'D55'
      Size = 1
    end
    object StringField28: TStringField
      FieldName = 'D61'
      Size = 1
    end
    object StringField29: TStringField
      FieldName = 'D62'
      Size = 1
    end
    object StringField30: TStringField
      FieldName = 'D63'
      Size = 1
    end
    object StringField31: TStringField
      FieldName = 'D64'
      Size = 1
    end
    object StringField32: TStringField
      DisplayWidth = 2
      FieldName = 'D65'
      Size = 1
    end
    object StringField33: TStringField
      FieldName = 'D71'
      Size = 1
    end
    object StringField34: TStringField
      FieldName = 'D72'
      Size = 1
    end
    object StringField35: TStringField
      FieldName = 'D73'
      Size = 1
    end
    object StringField36: TStringField
      FieldName = 'D74'
      Size = 1
    end
    object StringField37: TStringField
      DisplayWidth = 2
      FieldName = 'D75'
      Size = 1
    end
    object FloatField1: TFloatField
      FieldName = 'RECNO'
    end
    object StringField38: TStringField
      FieldKind = fkCalculated
      FieldName = 'D11CALC'
      Calculated = True
    end
    object StringField39: TStringField
      FieldKind = fkCalculated
      FieldName = 'D12CALC'
      Calculated = True
    end
    object StringField40: TStringField
      FieldKind = fkCalculated
      FieldName = 'D13CALC'
      Calculated = True
    end
    object StringField41: TStringField
      FieldKind = fkCalculated
      FieldName = 'D14CALC'
      Calculated = True
    end
    object StringField42: TStringField
      FieldKind = fkCalculated
      FieldName = 'D15CALC'
      Calculated = True
    end
    object StringField43: TStringField
      FieldKind = fkCalculated
      FieldName = 'D21CALC'
      Calculated = True
    end
    object StringField44: TStringField
      FieldKind = fkCalculated
      FieldName = 'D22CALC'
      Calculated = True
    end
    object StringField45: TStringField
      FieldKind = fkCalculated
      FieldName = 'D23CALC'
      Calculated = True
    end
    object StringField46: TStringField
      FieldKind = fkCalculated
      FieldName = 'D24CALC'
      Calculated = True
    end
    object StringField47: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D25CALC'
      Calculated = True
    end
    object StringField48: TStringField
      FieldKind = fkCalculated
      FieldName = 'D31CALC'
      Calculated = True
    end
    object StringField49: TStringField
      FieldKind = fkCalculated
      FieldName = 'D32CALC'
      Calculated = True
    end
    object StringField50: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D33CALC'
      Calculated = True
    end
    object StringField51: TStringField
      FieldKind = fkCalculated
      FieldName = 'D34CALC'
      Calculated = True
    end
    object StringField52: TStringField
      FieldKind = fkCalculated
      FieldName = 'D35CALC'
      Calculated = True
    end
    object StringField53: TStringField
      FieldKind = fkCalculated
      FieldName = 'D41CALC'
      Calculated = True
    end
    object StringField54: TStringField
      FieldKind = fkCalculated
      FieldName = 'D42CALC'
      Calculated = True
    end
    object StringField55: TStringField
      FieldKind = fkCalculated
      FieldName = 'D43CALC'
      Calculated = True
    end
    object StringField56: TStringField
      FieldKind = fkCalculated
      FieldName = 'D44CALC'
      Calculated = True
    end
    object StringField57: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D45CALC'
      Calculated = True
    end
    object StringField58: TStringField
      FieldKind = fkCalculated
      FieldName = 'D51CALC'
      Calculated = True
    end
    object StringField59: TStringField
      FieldKind = fkCalculated
      FieldName = 'D52CALC'
      Calculated = True
    end
    object StringField60: TStringField
      FieldKind = fkCalculated
      FieldName = 'D53CALC'
      Calculated = True
    end
    object StringField61: TStringField
      FieldKind = fkCalculated
      FieldName = 'D54CALC'
      Calculated = True
    end
    object StringField62: TStringField
      FieldKind = fkCalculated
      FieldName = 'D55CALC'
      Calculated = True
    end
    object StringField63: TStringField
      FieldKind = fkCalculated
      FieldName = 'D61CALC'
      Calculated = True
    end
    object StringField64: TStringField
      FieldKind = fkCalculated
      FieldName = 'D62CALC'
      Calculated = True
    end
    object StringField65: TStringField
      FieldKind = fkCalculated
      FieldName = 'D63CALC'
      Calculated = True
    end
    object StringField66: TStringField
      FieldKind = fkCalculated
      FieldName = 'D64CALC'
      Calculated = True
    end
    object StringField67: TStringField
      FieldKind = fkCalculated
      FieldName = 'D65CALC'
      Calculated = True
    end
    object StringField68: TStringField
      FieldKind = fkCalculated
      FieldName = 'D71CALC'
      Calculated = True
    end
    object StringField69: TStringField
      FieldKind = fkCalculated
      FieldName = 'D72CALC'
      Calculated = True
    end
    object StringField70: TStringField
      FieldKind = fkCalculated
      FieldName = 'D73CALC'
      Calculated = True
    end
    object StringField71: TStringField
      FieldKind = fkCalculated
      FieldName = 'D74CALC'
      Calculated = True
    end
    object StringField72: TStringField
      FieldKind = fkCalculated
      FieldName = 'D75CALC'
      Calculated = True
    end
    object FloatField2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'EMPSTARTDATE'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'EMPENDDATE'
    end
    object StringField73: TStringField
      FieldName = 'P1'
      Size = 6
    end
    object StringField74: TStringField
      FieldName = 'P2'
      Size = 6
    end
    object StringField75: TStringField
      FieldName = 'P3'
      Size = 6
    end
    object StringField76: TStringField
      FieldName = 'P4'
      Size = 6
    end
    object StringField77: TStringField
      FieldName = 'P5'
      Size = 6
    end
    object StringField78: TStringField
      FieldName = 'P6'
      Size = 6
    end
    object StringField79: TStringField
      FieldName = 'P7'
      Size = 6
    end
    object StringField80: TStringField
      FieldKind = fkCalculated
      FieldName = 'P1CALC'
      Size = 6
      Calculated = True
    end
    object StringField81: TStringField
      FieldKind = fkCalculated
      FieldName = 'P2CALC'
      Size = 6
      Calculated = True
    end
    object StringField82: TStringField
      FieldKind = fkCalculated
      FieldName = 'P3CALC'
      Size = 6
      Calculated = True
    end
    object StringField83: TStringField
      FieldKind = fkCalculated
      FieldName = 'P4CALC'
      Size = 6
      Calculated = True
    end
    object StringField84: TStringField
      FieldKind = fkCalculated
      FieldName = 'P5CALC'
      Size = 6
      Calculated = True
    end
    object StringField85: TStringField
      FieldKind = fkCalculated
      FieldName = 'P6CALC'
      Size = 6
      Calculated = True
    end
    object StringField86: TStringField
      FieldKind = fkCalculated
      FieldName = 'P7CALC'
      Size = 6
      Calculated = True
    end
    object StringField87: TStringField
      FieldName = 'EMPPLANT_CODE'
      Size = 6
    end
  end
  object QueryAvailabilityXXX2: TQuery
    AutoCalcFields = False
    AfterScroll = QueryAvailabilityAfterScroll
    OnCalcFields = QueryAvailabilityCalcFields
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE EMPPLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE EMPSTARTDATE, E.ENDDATE EMPENDDATE,'
      '  CAST(MAX(ROWNUM) AS NUMBER) AS RECNO,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D11,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D12,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D13,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D14,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE = :STARTDATE AND EA2.EM' +
        'PLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D15,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D21,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D22,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D23,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D24,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 1 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D25,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D31,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D32,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D33,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D34,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 2 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D35,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D41,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D42,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D43,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D44,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 3 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D45,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D51,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D52,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D53,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D54,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 4 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D55,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D61,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D62,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D63,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D64,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 5 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D65,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_1 ELSE NULL END) D71,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_2 ELSE NULL END) D72,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_3 ELSE NULL END) D73,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE TH' +
        'EN EA.AVAILABLE_TIMEBLOCK_4 ELSE NULL END) D74,'
      '  TO_CHAR('
      '    (SELECT'
      '      NVL(EA2.SHIFT_NUMBER,'
      '           (SELECT S2.SHIFT_NUMBER'
      '            FROM SHIFTSCHEDULE S2'
      '            WHERE S2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '            S2.SHIFT_SCHEDULE_DATE - 6 = :STARTDATE)'
      '           )'
      '     FROM EMPLOYEEAVAILABILITY EA2'
      
        '     WHERE EA2.EMPLOYEEAVAILABILITY_DATE - 6 = :STARTDATE AND EA' +
        '2.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER)'
      '   ) D75,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE = :STARTDATE THEN E' +
        'A.PLANT_CODE ELSE E.PLANT_CODE END) P1,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 1 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P2,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 2 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P3,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 3 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P4,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 4 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P5,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 5 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P6,'
      
        '  MAX(CASE WHEN EA.EMPLOYEEAVAILABILITY_DATE - 7 = :STARTDATE TH' +
        'EN EA.PLANT_CODE ELSE E.PLANT_CODE END) P7'
      'FROM'
      '  EMPLOYEE E'
      '  LEFT OUTER JOIN SHIFTSCHEDULE S ON'
      '    E.EMPLOYEE_NUMBER = S.EMPLOYEE_NUMBER'
      '    AND S.SHIFT_SCHEDULE_DATE >= :STARTDATE AND'
      '    (S.SHIFT_SCHEDULE_DATE - 6 <= :STARTDATE)'
      '  LEFT OUTER JOIN EMPLOYEEAVAILABILITY EA ON'
      '    (EA.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER) '
      '    AND EA.EMPLOYEEAVAILABILITY_DATE >= :STARTDATE AND  '
      '    EA.EMPLOYEEAVAILABILITY_DATE - 6 <= :STARTDATE  '
      '  LEFT OUTER JOIN TEAMPERUSER TPU ON  '
      '    E.TEAM_CODE = TPU.TEAM_CODE  '
      'WHERE '
      '  ((E.ENDDATE >= :STARTDATE) OR (E.ENDDATE IS NULL))'
      '  AND'
      '  (:EMP_PLANT_CODE = '#39'*'#39' OR E.PLANT_CODE = :EMP_PLANT_CODE) AND'
      '  (  '
      '    (  '
      '       (:USER_NAME <> '#39'*'#39') AND   '
      '       E.TEAM_CODE IN   '
      '         (  '
      '           SELECT T.TEAM_CODE   '
      '           FROM TEAMPERUSER T  '
      '           WHERE   '
      '             T.USER_NAME = :USER_NAME AND  '
      '             (  '
      '               (:TEAMFROM = '#39'*'#39') OR   '
      '               (T.TEAM_CODE >= :TEAMFROM AND  '
      '                T.TEAM_CODE <= :TEAMTO)  '
      '             )  '
      '         )  '
      '     )  '
      '     OR'
      '     (  '
      '        (:USER_NAME = '#39'*'#39') AND  '
      '        (   '
      '          (:TEAMFROM = '#39'*'#39') OR  '
      
        '          (E.TEAM_CODE >= :TEAMFROM AND E.TEAM_CODE <= :TEAMTO) ' +
        '  '
      '        )  '
      '    )  '
      '  )  '
      
        '  AND :PLANT_CODE = :PLANT_CODE AND :INSERT_STAV = :INSERT_STAV ' +
        ' '
      '  AND :CREATIONDATE = :CREATIONDATE AND :MUTATOR = :MUTATOR'
      'GROUP BY'
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION,'
      '  E.PLANT_CODE,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE, E.ENDDATE'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER, E.PLANT_CODE'
      ''
      ''
      ' ')
    UpdateMode = upWhereKeyOnly
    Left = 192
    Top = 380
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'STARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'EMP_PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'TEAMTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'INSERT_STAV'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'CREATIONDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object StringField88: TStringField
      FieldName = 'DESCRIPTION'
    end
    object StringField89: TStringField
      FieldName = 'DEPARTMENT_CODE'
    end
    object StringField90: TStringField
      FieldName = 'D11'
      Size = 1
    end
    object StringField91: TStringField
      FieldName = 'D12'
      Size = 1
    end
    object StringField92: TStringField
      FieldName = 'D13'
      Size = 1
    end
    object StringField93: TStringField
      FieldName = 'D14'
      Size = 1
    end
    object StringField94: TStringField
      DisplayWidth = 2
      FieldName = 'D15'
      Size = 1
    end
    object StringField95: TStringField
      FieldName = 'D21'
      Size = 1
    end
    object StringField96: TStringField
      FieldName = 'D22'
      Size = 1
    end
    object StringField97: TStringField
      FieldName = 'D23'
      Size = 1
    end
    object StringField98: TStringField
      FieldName = 'D24'
      Size = 1
    end
    object StringField99: TStringField
      DisplayWidth = 2
      FieldName = 'D25'
      Size = 1
    end
    object StringField100: TStringField
      FieldName = 'D31'
      Size = 1
    end
    object StringField101: TStringField
      FieldName = 'D32'
      Size = 1
    end
    object StringField102: TStringField
      FieldName = 'D33'
      Size = 1
    end
    object StringField103: TStringField
      FieldName = 'D34'
      Size = 1
    end
    object StringField104: TStringField
      DisplayWidth = 2
      FieldName = 'D35'
      Size = 1
    end
    object StringField105: TStringField
      FieldName = 'D41'
      Size = 1
    end
    object StringField106: TStringField
      FieldName = 'D42'
      Size = 1
    end
    object StringField107: TStringField
      FieldName = 'D43'
      Size = 1
    end
    object StringField108: TStringField
      FieldName = 'D44'
      Size = 1
    end
    object StringField109: TStringField
      DisplayWidth = 2
      FieldName = 'D45'
      Size = 1
    end
    object StringField110: TStringField
      FieldName = 'D51'
      Size = 1
    end
    object StringField111: TStringField
      FieldName = 'D52'
      Size = 1
    end
    object StringField112: TStringField
      FieldName = 'D53'
      Size = 1
    end
    object StringField113: TStringField
      FieldName = 'D54'
      Size = 1
    end
    object StringField114: TStringField
      DisplayWidth = 2
      FieldName = 'D55'
      Size = 1
    end
    object StringField115: TStringField
      FieldName = 'D61'
      Size = 1
    end
    object StringField116: TStringField
      FieldName = 'D62'
      Size = 1
    end
    object StringField117: TStringField
      FieldName = 'D63'
      Size = 1
    end
    object StringField118: TStringField
      FieldName = 'D64'
      Size = 1
    end
    object StringField119: TStringField
      DisplayWidth = 2
      FieldName = 'D65'
      Size = 1
    end
    object StringField120: TStringField
      FieldName = 'D71'
      Size = 1
    end
    object StringField121: TStringField
      FieldName = 'D72'
      Size = 1
    end
    object StringField122: TStringField
      FieldName = 'D73'
      Size = 1
    end
    object StringField123: TStringField
      FieldName = 'D74'
      Size = 1
    end
    object StringField124: TStringField
      DisplayWidth = 2
      FieldName = 'D75'
      Size = 1
    end
    object FloatField3: TFloatField
      FieldName = 'RECNO'
    end
    object StringField125: TStringField
      FieldKind = fkCalculated
      FieldName = 'D11CALC'
      Calculated = True
    end
    object StringField126: TStringField
      FieldKind = fkCalculated
      FieldName = 'D12CALC'
      Calculated = True
    end
    object StringField127: TStringField
      FieldKind = fkCalculated
      FieldName = 'D13CALC'
      Calculated = True
    end
    object StringField128: TStringField
      FieldKind = fkCalculated
      FieldName = 'D14CALC'
      Calculated = True
    end
    object StringField129: TStringField
      FieldKind = fkCalculated
      FieldName = 'D15CALC'
      Calculated = True
    end
    object StringField130: TStringField
      FieldKind = fkCalculated
      FieldName = 'D21CALC'
      Calculated = True
    end
    object StringField131: TStringField
      FieldKind = fkCalculated
      FieldName = 'D22CALC'
      Calculated = True
    end
    object StringField132: TStringField
      FieldKind = fkCalculated
      FieldName = 'D23CALC'
      Calculated = True
    end
    object StringField133: TStringField
      FieldKind = fkCalculated
      FieldName = 'D24CALC'
      Calculated = True
    end
    object StringField134: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D25CALC'
      Calculated = True
    end
    object StringField135: TStringField
      FieldKind = fkCalculated
      FieldName = 'D31CALC'
      Calculated = True
    end
    object StringField136: TStringField
      FieldKind = fkCalculated
      FieldName = 'D32CALC'
      Calculated = True
    end
    object StringField137: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D33CALC'
      Calculated = True
    end
    object StringField138: TStringField
      FieldKind = fkCalculated
      FieldName = 'D34CALC'
      Calculated = True
    end
    object StringField139: TStringField
      FieldKind = fkCalculated
      FieldName = 'D35CALC'
      Calculated = True
    end
    object StringField140: TStringField
      FieldKind = fkCalculated
      FieldName = 'D41CALC'
      Calculated = True
    end
    object StringField141: TStringField
      FieldKind = fkCalculated
      FieldName = 'D42CALC'
      Calculated = True
    end
    object StringField142: TStringField
      FieldKind = fkCalculated
      FieldName = 'D43CALC'
      Calculated = True
    end
    object StringField143: TStringField
      FieldKind = fkCalculated
      FieldName = 'D44CALC'
      Calculated = True
    end
    object StringField144: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'D45CALC'
      Calculated = True
    end
    object StringField145: TStringField
      FieldKind = fkCalculated
      FieldName = 'D51CALC'
      Calculated = True
    end
    object StringField146: TStringField
      FieldKind = fkCalculated
      FieldName = 'D52CALC'
      Calculated = True
    end
    object StringField147: TStringField
      FieldKind = fkCalculated
      FieldName = 'D53CALC'
      Calculated = True
    end
    object StringField148: TStringField
      FieldKind = fkCalculated
      FieldName = 'D54CALC'
      Calculated = True
    end
    object StringField149: TStringField
      FieldKind = fkCalculated
      FieldName = 'D55CALC'
      Calculated = True
    end
    object StringField150: TStringField
      FieldKind = fkCalculated
      FieldName = 'D61CALC'
      Calculated = True
    end
    object StringField151: TStringField
      FieldKind = fkCalculated
      FieldName = 'D62CALC'
      Calculated = True
    end
    object StringField152: TStringField
      FieldKind = fkCalculated
      FieldName = 'D63CALC'
      Calculated = True
    end
    object StringField153: TStringField
      FieldKind = fkCalculated
      FieldName = 'D64CALC'
      Calculated = True
    end
    object StringField154: TStringField
      FieldKind = fkCalculated
      FieldName = 'D65CALC'
      Calculated = True
    end
    object StringField155: TStringField
      FieldKind = fkCalculated
      FieldName = 'D71CALC'
      Calculated = True
    end
    object StringField156: TStringField
      FieldKind = fkCalculated
      FieldName = 'D72CALC'
      Calculated = True
    end
    object StringField157: TStringField
      FieldKind = fkCalculated
      FieldName = 'D73CALC'
      Calculated = True
    end
    object StringField158: TStringField
      FieldKind = fkCalculated
      FieldName = 'D74CALC'
      Calculated = True
    end
    object StringField159: TStringField
      FieldKind = fkCalculated
      FieldName = 'D75CALC'
      Calculated = True
    end
    object FloatField4: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FLOAT_EMP'
      Calculated = True
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'EMPSTARTDATE'
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'EMPENDDATE'
    end
    object StringField160: TStringField
      FieldName = 'P1'
      Size = 6
    end
    object StringField161: TStringField
      FieldName = 'P2'
      Size = 6
    end
    object StringField162: TStringField
      FieldName = 'P3'
      Size = 6
    end
    object StringField163: TStringField
      FieldName = 'P4'
      Size = 6
    end
    object StringField164: TStringField
      FieldName = 'P5'
      Size = 6
    end
    object StringField165: TStringField
      FieldName = 'P6'
      Size = 6
    end
    object StringField166: TStringField
      FieldName = 'P7'
      Size = 6
    end
    object StringField167: TStringField
      FieldKind = fkCalculated
      FieldName = 'P1CALC'
      Size = 6
      Calculated = True
    end
    object StringField168: TStringField
      FieldKind = fkCalculated
      FieldName = 'P2CALC'
      Size = 6
      Calculated = True
    end
    object StringField169: TStringField
      FieldKind = fkCalculated
      FieldName = 'P3CALC'
      Size = 6
      Calculated = True
    end
    object StringField170: TStringField
      FieldKind = fkCalculated
      FieldName = 'P4CALC'
      Size = 6
      Calculated = True
    end
    object StringField171: TStringField
      FieldKind = fkCalculated
      FieldName = 'P5CALC'
      Size = 6
      Calculated = True
    end
    object StringField172: TStringField
      FieldKind = fkCalculated
      FieldName = 'P6CALC'
      Size = 6
      Calculated = True
    end
    object StringField173: TStringField
      FieldKind = fkCalculated
      FieldName = 'P7CALC'
      Size = 6
      Calculated = True
    end
    object StringField174: TStringField
      FieldName = 'EMPPLANT_CODE'
      Size = 6
    end
  end
end
