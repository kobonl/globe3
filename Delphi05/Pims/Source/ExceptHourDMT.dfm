inherited ExceptHourDM: TExceptHourDM
  OldCreateOrder = True
  Left = 265
  Top = 135
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    TableName = 'CONTRACTGROUP'
    object TableMasterCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Size = 1
    end
    object TableMasterBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Size = 1
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterOVERTIME_PER_DAY_WEEK_PERIOD: TIntegerField
      FieldName = 'OVERTIME_PER_DAY_WEEK_PERIOD'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterPERIOD_STARTS_IN_WEEK: TIntegerField
      FieldName = 'PERIOD_STARTS_IN_WEEK'
    end
    object TableMasterWEEKS_IN_PERIOD: TIntegerField
      FieldName = 'WEEKS_IN_PERIOD'
    end
    object TableMasterWORK_TIME_REDUCTION_YN: TStringField
      FieldName = 'WORK_TIME_REDUCTION_YN'
      Size = 1
    end
  end
  inherited TableDetail: TTable
    BeforePost = TableDetailBeforePost
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 'CONTRACTGROUP_CODE'
    MasterFields = 'CONTRACTGROUP_CODE'
    TableName = 'EXCEPTIONALHOURDEF'
    Top = 120
    object TableDetailDAY_OF_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailCONTRACTGROUP_CODE: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object TableDetailSTARTTIME: TDateTimeField
      DefaultExpression = #39'00:00'#39
      FieldName = 'STARTTIME'
      DisplayFormat = 'hh:nn'
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailENDTIME: TDateTimeField
      DefaultExpression = #39'00:00'#39
      FieldName = 'ENDTIME'
      DisplayFormat = 'hh:nn'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailHOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailDAYLU: TStringField
      FieldKind = fkLookup
      FieldName = 'DAYLU'
      LookupDataSet = TableDayTable
      LookupKeyFields = 'DAY_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DAY_OF_WEEK'
      Lookup = True
    end
    object TableDetailDAYDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAYDESC'
      Size = 30
      Calculated = True
    end
    object TableDetailOVERTIME_HOURTYPE_NUMBER: TIntegerField
      FieldName = 'OVERTIME_HOURTYPE_NUMBER'
    end
    object TableDetailOVERTIME_HOURTYPELU: TStringField
      FieldKind = fkLookup
      FieldName = 'OVERTIME_HOURTYPELU'
      LookupDataSet = TableHourTypeOvertime
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'OVERTIME_HOURTYPE_NUMBER'
      Lookup = True
    end
  end
  inherited DataSourceDetail: TDataSource
    Left = 200
    Top = 124
  end
  inherited TableExport: TTable
    Left = 92
    Top = 332
  end
  inherited DataSourceExport: TDataSource
    Left = 208
    Top = 332
  end
  object TableHourType: TTable
    Active = True
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 92
    Top = 196
    object TableHourTypeHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object TableHourTypeDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableHourTypeCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableHourTypeOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableHourTypeMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableHourTypeMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableHourTypeCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableHourTypeEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableHourTypeBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
  object TableDayTable: TTable
    Active = True
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'DAYTABLE'
    Left = 92
    Top = 264
    object TableDayTableDAY_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_NUMBER'
    end
    object TableDayTableDAY_CODE: TStringField
      FieldName = 'DAY_CODE'
      Required = True
      Size = 2
    end
    object TableDayTableDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
    end
  end
  object DataSourceHourType: TDataSource
    DataSet = TableHourType
    Left = 204
    Top = 200
  end
  object DataSourceDayTable: TDataSource
    DataSet = TableDayTable
    Left = 204
    Top = 260
  end
  object TableExceptionalHrs: TTable
    BeforePost = TableDetailBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = TableDetailNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'CONTRACTGROUP_CODE;DAY_OF_WEEK;STARTTIME'
    MasterFields = 'CONTRACTGROUP_CODE'
    MasterSource = DataSourceMaster
    TableName = 'EXCEPTIONALHOURDEF'
    Left = 316
    Top = 120
    object IntegerField1: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
    end
    object StringField1: TStringField
      FieldName = 'CONTRACTGROUP_CODE'
      Required = True
      Size = 6
    end
    object DateTimeField1: TDateTimeField
      DefaultExpression = #39'00:00'#39
      FieldName = 'STARTTIME'
      DisplayFormat = 'hh:nn'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object DateTimeField3: TDateTimeField
      DefaultExpression = #39'00:00'#39
      FieldName = 'ENDTIME'
      DisplayFormat = 'hh:nn'
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField2: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object IntegerField2: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField3: TStringField
      FieldKind = fkLookup
      FieldName = 'HOURTYPELU'
      LookupDataSet = TableHourType
      LookupKeyFields = 'HOURTYPE_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'HOURTYPE_NUMBER'
      LookupCache = True
      Lookup = True
    end
    object StringField4: TStringField
      FieldKind = fkLookup
      FieldName = 'DAYLU'
      LookupDataSet = TableDayTable
      LookupKeyFields = 'DAY_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DAY_OF_WEEK'
      LookupCache = True
      Lookup = True
    end
  end
  object TableHourTypeOvertime: TTable
    Active = True
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 92
    Top = 396
    object IntegerField3: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
    end
    object StringField5: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object StringField6: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object StringField7: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object StringField8: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object StringField9: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object FloatField1: TFloatField
      Alignment = taLeftJustify
      FieldName = 'BONUS_PERCENTAGE'
    end
  end
  object DataSourceHourtypeOvertime: TDataSource
    DataSet = TableHourTypeOvertime
    Left = 236
    Top = 392
  end
end
