(*
  Changes:
      SO: O1-JUL-2010 RV067.1. 550490
      - completed the right click menu wth Absence codes
      - completed the *, - checks to check also by absence code
  SO:19-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
  MRA:23-APR-2018 GLOB3-94
  - Problem when 2 users work in planning dialogs
  - Always allow to use Refresh-button to refresh (not only when it is red).
  - Related to this issue, use bookmark to go to last line in grid after
    doing a copy + commit, instead of going to first line.
  - If in edit-mode, then do NOT refresh when refresh-button is clicked.
  MRA:18-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:24-JAN-2019 GLOB3-225
  - Changes for extension 4 to 10 blocks for planning
  - When MaxTimeblocks=4 do not show extra blocks in grid
  MRA:28-JAN-2019 GLOB3-225
  - The Tabs do not work OK when there are less then 10 timeblocks.
  - Do not show an error-msg during entering but only before save.
*)

unit StandStaffAvailFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Dblup1a, Mask, DBCtrls, Buttons, Menus,
  dxDBTLCl, dxGrClms;

type
  TStandStaffAvailF = class(TGridBaseF)
    pnlTeamPlantShift: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cmbPlusPlant: TComboBoxPlus;
    cmbPlusShift: TComboBoxPlus;
    CheckBoxPaidYN: TCheckBox;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxAllPlant: TCheckBox;
    CheckBoxAllShift: TCheckBox;
    dxDetailGridColumnDESCRIPTION: TdxDBGridColumn;
    dxDetailGridColumnPLANT: TdxDBGridColumn;
    dxDetailGridColumnSHIFT: TdxDBGridColumn;
    dxDetailGridColumnD11CALC: TdxDBGridColumn;
    dxDetailGridColumnD12CALC: TdxDBGridColumn;
    dxDetailGridColumnD13CALC: TdxDBGridColumn;
    dxDetailGridColumnD21CALC: TdxDBGridColumn;
    dxDetailGridColumnD22CALC: TdxDBGridColumn;
    dxDetailGridColumnD23CALC: TdxDBGridColumn;
    dxDetailGridColumnD31CALC: TdxDBGridColumn;
    dxDetailGridColumnD32CALC: TdxDBGridColumn;
    dxDetailGridColumnD33CALC: TdxDBGridColumn;
    dxDetailGridColumnD41CALC: TdxDBGridColumn;
    dxDetailGridColumnD42CALC: TdxDBGridColumn;
    dxDetailGridColumnD43CALC: TdxDBGridColumn;
    dxDetailGridColumnD51CALC: TdxDBGridColumn;
    dxDetailGridColumnD52CALC: TdxDBGridColumn;
    dxDetailGridColumnD53CALC: TdxDBGridColumn;
    dxDetailGridColumnD54CALC: TdxDBGridColumn;
    dxDetailGridColumnD61CALC: TdxDBGridColumn;
    dxDetailGridColumnD62CALC: TdxDBGridColumn;
    dxDetailGridColumnD63CALC: TdxDBGridColumn;
    dxDetailGridColumnD71CALC: TdxDBGridColumn;
    dxDetailGridColumnD72CALC: TdxDBGridColumn;
    dxDetailGridColumnD73CALC: TdxDBGridColumn;
    dxDetailGridColumnTOTALHRS: TdxDBGridColumn;
    dxDetailGridColumnD14CALC: TdxDBGridColumn;
    dxDetailGridColumnD24CALC: TdxDBGridColumn;
    dxDetailGridColumnD34CALC: TdxDBGridColumn;
    dxDetailGridColumnD44CALC: TdxDBGridColumn;
    dxDetailGridColumnD64CALC: TdxDBGridColumn;
    dxDetailGridColumnD74CALC: TdxDBGridColumn;
    BitBtnRefresh: TBitBtn;
    PopupMenuStdAvail: TPopupMenu;
    dxDetailGridColumnRECO: TdxDBGridColumn;
    dxDetailGridColumnEMPLOYEE_NUMBER: TdxDBGridColumn;
    CheckBoxOnlyActiveEmp: TCheckBox;
    dxDetailGridColumnCONTRACTHOURS: TdxDBGridColumn;
    Label12: TLabel;
    cmbPlusTeamFrom: TComboBoxPlus;
    Label14: TLabel;
    cmbPlusTeamTo: TComboBoxPlus;
    GroupBoxDetails: TGroupBox;
    Panel1: TPanel;
    Label4: TLabel;
    DBEdit32: TDBEdit;
    DBEdit29: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    Label7: TLabel;
    Panel2: TPanel;
    ScrollBox1: TScrollBox;
    Panel3: TPanel;
    ButtonCopy: TButton;
    GroupBoxDay1: TGroupBox;
    GroupBoxDay2: TGroupBox;
    GroupBoxDay3: TGroupBox;
    GroupBoxDay4: TGroupBox;
    GroupBoxDay5: TGroupBox;
    GroupBoxDay6: TGroupBox;
    GroupBoxDay7: TGroupBox;
    Panel11: TPanel;
    Edit11: TEdit;
    Edit12: TEdit;
    Edit13: TEdit;
    Edit14: TEdit;
    Edit15: TEdit;
    Edit16: TEdit;
    Edit17: TEdit;
    Edit18: TEdit;
    Edit19: TEdit;
    Edit110: TEdit;
    Panel22: TPanel;
    Edit21: TEdit;
    Edit22: TEdit;
    Edit23: TEdit;
    Edit24: TEdit;
    Panel33: TPanel;
    Edit31: TEdit;
    Edit32: TEdit;
    Edit33: TEdit;
    Edit34: TEdit;
    Panel44: TPanel;
    Edit41: TEdit;
    Edit42: TEdit;
    Edit43: TEdit;
    Edit44: TEdit;
    Panel55: TPanel;
    Edit51: TEdit;
    Edit52: TEdit;
    Edit53: TEdit;
    Edit54: TEdit;
    Panel66: TPanel;
    Edit61: TEdit;
    Edit62: TEdit;
    Edit63: TEdit;
    Edit64: TEdit;
    Panel77: TPanel;
    Edit71: TEdit;
    Edit72: TEdit;
    Edit73: TEdit;
    Edit74: TEdit;
    Edit25: TEdit;
    Edit26: TEdit;
    Edit27: TEdit;
    Edit28: TEdit;
    Edit29: TEdit;
    Edit210: TEdit;
    Edit35: TEdit;
    Edit36: TEdit;
    Edit37: TEdit;
    Edit38: TEdit;
    Edit39: TEdit;
    Edit310: TEdit;
    Edit45: TEdit;
    Edit46: TEdit;
    Edit47: TEdit;
    Edit48: TEdit;
    Edit49: TEdit;
    Edit410: TEdit;
    Edit55: TEdit;
    Edit56: TEdit;
    Edit57: TEdit;
    Edit58: TEdit;
    Edit59: TEdit;
    Edit510: TEdit;
    Edit65: TEdit;
    Edit66: TEdit;
    Edit67: TEdit;
    Edit68: TEdit;
    Edit69: TEdit;
    Edit610: TEdit;
    Edit75: TEdit;
    Edit76: TEdit;
    Edit77: TEdit;
    Edit78: TEdit;
    Edit79: TEdit;
    Edit710: TEdit;
    dxDetailGridColumnD15CALC: TdxDBGridColumn;
    dxDetailGridColumnD16CALC: TdxDBGridColumn;
    dxDetailGridColumnD17CALC: TdxDBGridColumn;
    dxDetailGridColumnD18CALC: TdxDBGridColumn;
    dxDetailGridColumnD19CALC: TdxDBGridColumn;
    dxDetailGridColumnD110CALC: TdxDBGridColumn;
    dxDetailGridColumnD25CALC: TdxDBGridColumn;
    dxDetailGridColumnD26CALC: TdxDBGridColumn;
    dxDetailGridColumnD27CALC: TdxDBGridColumn;
    dxDetailGridColumnD28CALC: TdxDBGridColumn;
    dxDetailGridColumnD29CALC: TdxDBGridColumn;
    dxDetailGridColumnD210CALC: TdxDBGridColumn;
    dxDetailGridColumnD35CALC: TdxDBGridColumn;
    dxDetailGridColumnD36CALC: TdxDBGridColumn;
    dxDetailGridColumnD37CALC: TdxDBGridColumn;
    dxDetailGridColumnD38CALC: TdxDBGridColumn;
    dxDetailGridColumnD39CALC: TdxDBGridColumn;
    dxDetailGridColumnD310CALC: TdxDBGridColumn;
    dxDetailGridColumnD45CALC: TdxDBGridColumn;
    dxDetailGridColumnD46CALC: TdxDBGridColumn;
    dxDetailGridColumnD47CALC: TdxDBGridColumn;
    dxDetailGridColumnD48CALC: TdxDBGridColumn;
    dxDetailGridColumnD49CALC: TdxDBGridColumn;
    dxDetailGridColumnD410CALC: TdxDBGridColumn;
    dxDetailGridColumnD55CALC: TdxDBGridColumn;
    dxDetailGridColumnD56CALC: TdxDBGridColumn;
    dxDetailGridColumnD57CALC: TdxDBGridColumn;
    dxDetailGridColumnD58CALC: TdxDBGridColumn;
    dxDetailGridColumnD59CALC: TdxDBGridColumn;
    dxDetailGridColumnD510CALC: TdxDBGridColumn;
    dxDetailGridColumnD65CALC: TdxDBGridColumn;
    dxDetailGridColumnD66CALC: TdxDBGridColumn;
    dxDetailGridColumnD67CALC: TdxDBGridColumn;
    dxDetailGridColumnD68CALC: TdxDBGridColumn;
    dxDetailGridColumnD69CALC: TdxDBGridColumn;
    dxDetailGridColumnD610CALC: TdxDBGridColumn;
    dxDetailGridColumnD75CALC: TdxDBGridColumn;
    dxDetailGridColumnD76CALC: TdxDBGridColumn;
    dxDetailGridColumnD77CALC: TdxDBGridColumn;
    dxDetailGridColumnD78CALC: TdxDBGridColumn;
    dxDetailGridColumnD79CALC: TdxDBGridColumn;
    dxDetailGridColumnD710CALC: TdxDBGridColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cmbPlusPlantChange(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure CheckBoxAllPlantClick(Sender: TObject);
    procedure CheckBoxAllShiftClick(Sender: TObject);
    procedure Edit11Change(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure Edit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxGridEnter(Sender: TObject);
    procedure Edit11Exit(Sender: TObject);
    procedure CheckBoxPaidYNClick(Sender: TObject);
    procedure ButtonCopyClick(Sender: TObject);
    procedure BitBtnRefreshClick(Sender: TObject);
    procedure cmbPlusShiftChange(Sender: TObject);
    procedure Edit11MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Edit11ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure PopupMenuStdAvailPopup(Sender: TObject);
    procedure dxDetailGridClick(Sender: TObject);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure CheckBoxOnlyActiveEmpClick(Sender: TObject);
    procedure cmbPlusTeamFromCloseUp(Sender: TObject);
    procedure cmbPlusTeamToCloseUp(Sender: TObject);
  private
    { Private declarations }
    FEmployee, FShift: Integer;
    FPlant, FDept: String;
    FEditField: TEdit;
    FCtrlState, FPopupOpen: Boolean;
    FRecNo: Double;
    procedure FillPlants;
    procedure FillShifts;
    procedure SetValueForAllEditFields(Value: String; DaySelected: Integer);
    procedure AddMenuItem(ACaption: String);
    procedure BuildPopupMenu;
    function ValidAbsenceReasonCode(ACode: String): Boolean;
    procedure GroupBoxResize(ADay, ATBCount: Integer);
    procedure HideColumns;
  public
    { Public declarations }
    procedure FillEditDetail(OK: Boolean);
    procedure FillArraySTA(var RecNo: Double);
    function CheckValidEditFields: Boolean;
    procedure EnabledNavButtons(Active: Boolean);
    procedure AskForChanges;
    procedure RefreshGrid(OK: Boolean);
    procedure SetRefreshStatus(OK: Boolean);
    procedure SetRedColor;
    procedure PopupItemClick(Sender: TObject);
    procedure SetPositionOnScreen(Recno: Double);
    function CheckArraySTA: Boolean;
  end;

function StandStaffAvailF: TStandStaffAvailF;

implementation

{$R *.DFM}

uses
  SystemDMT, ListProcsFRM, StandStaffAvailDMT, UPimsConst, UPimsMessageRes,
  DialogCopyFromSTAFRM;

var
  StandStaffAvailF_HDN: TStandStaffAvailF;

function StandStaffAvailF: TStandStaffAvailF;
begin
  if StandStaffAvailF_HDN = nil then
  begin
    StandStaffAvailF_HDN := TStandStaffAvailF.Create(Application);
  end;
  Result := StandStaffAvailF_HDN;
end;

procedure TStandStaffAvailF.FormCreate(Sender: TObject);
begin
  StandStaffAvailDM := CreateFormDM(TStandStaffAvailDM);
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := StandStaffAvailDM.DataSourceDetail;
  inherited;
  // GLOB3-60 Leave this empty
  GroupBoxDetails.Caption := '';
  HideColumns; // GLOB3-225
end;

procedure TStandStaffAvailF.FormDestroy(Sender: TObject);
begin
  inherited;
  StandStaffAvailF_HDN := nil;
end;

procedure TStandStaffAvailF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SUndoChanges, mtInformation, [mbOk]);
    dxBarBDBNavPost.Enabled := False;
  end;

  inherited;
  // RV050.8.
  // Turn filtering off to prevent a problem during OnRecordFilter!
  StandStaffAvailDM.QueryPlant.Filtered := False;
  StandStaffAvailDM.QueryPlant.Active := False;
  Action := caFree;
  //CAR 21-7-2003
//  TBLenghtClass.Free;
end;


procedure TStandStaffAvailF.FillPlants;
begin
  // MR:17-02-2005 Order 550378 Team-From-To.
  if CheckBoxAllTeam.Checked then
    ListProcsF.FillComboBoxPlant(
      StandStaffAvailDM.QueryPlant, True, CmbPlusPlant)
  else
  begin
    if cmbPlusTeamFrom.DisplayValue = cmbPlusTeamTo.DisplayValue then
    begin
      StandStaffAvailDM.QueryPlantTeam.Close;
      StandStaffAvailDM.QueryPlantTeam.ParamByName('TEAM_CODE').AsString :=
        GetStrValue(cmbPlusTeamFrom.Value);
      StandStaffAvailDM.QueryPlantTeam.Open;
      if StandStaffAvailDM.QueryPlantTeam.IsEmpty then
         ListProcsF.FillComboBoxPlant(StandStaffAvailDM.QueryPlant, True,
         CmbPlusPlant)
      else
        ListProcsF.FillComboBox(StandStaffAvailDM.QueryPlantTeam,
          CmbPlusPlant, GetStrValue(cmbPlusTeamFrom.Value), '',
            True, 'TEAM_CODE','', 'PLANT_CODE', 'DESCRIPTION');
    end
    else
      ListProcsF.FillComboBoxPlant(
        StandStaffAvailDM.QueryPlant, True, CmbPlusPlant);
  end;
end;

procedure TStandStaffAvailF.FillShifts;
begin
  if not CheckBoxAllShift.Checked then
  begin
    ListProcsF.FillComboBox(StandStaffAvailDM.QueryShiftPlant,
      CmbPlusShift, GetStrValue(CmbPlusPlant.Value), '', True, 'PLANT_CODE', '',
      'SHIFT_NUMBER', 'DESCRIPTION');
    cmbPlusShift.IsSorted := True; // GLOB3-60 Do this for numerical sort!
  end;

  SetRefreshStatus(False);
end;

//RV067.1.
function TStandStaffAvailF.ValidAbsenceReasonCode(ACode: String): Boolean;
begin
  with StandStaffAvailDM.QueryAbs do
  begin
    if not Active then Open;
    First;
    Result := Locate('ABSENCEREASON_CODE', ACode, []);
  end;
end;

procedure TStandStaffAvailF.AddMenuItem(ACaption: String);
var
  NewItem: TMenuItem;
begin
  NewItem := TMenuItem.Create(Self);
  NewItem.Caption := ACaption;
  PopupMenuStdAvail.Items.Add(NewItem);
  NewItem.OnClick := PopupItemClick;
end;

//RV067.4.
procedure TStandStaffAvailF.BuildPopupMenu();
var
  SaveSql: String;
begin
  PopupMenuStdAvail.Items.Clear;
  AddMenuItem(DEFAULTAVAILABILITYCODE);
  AddMenuItem('_');

  SaveSql := StandStaffAvailDM.QueryAbs.Sql.Text;
  if
    StandStaffAvailDM.QueryPlant.Active and
    (not StandStaffAvailDM.QueryPlant.Eof)
  then
  begin
    StandStaffAvailDM.QueryPlant.First;
    StandStaffAvailDM.QueryPlant.Locate('PLANT_CODE', GetStrValue(cmbPlusPlant.Value), []);
    SystemDM.UpdateAbsenceReasonPerCountry(
      StandStaffAvailDM.QueryPlant.FieldByName('Country_Id').AsInteger,
      StandStaffAvailDM.QueryAbs
    );
  end;

  with StandStaffAvailDM.QueryAbs do
  begin
    if not Active then Open;
    First;
    while not EOF do
    begin
      AddMenuItem(FieldByName('ABSENCEREASON_CODE').AsString + ' ' + FieldByName('DESCRIPTION').AsString);
      Next;
    end;
  end;
  StandStaffAvailDM.QueryAbs.Sql.Text := SaveSql;
end;

procedure TStandStaffAvailF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  CheckBoxOnlyActiveEmpClick(Sender);
//  inherited;
 // car 15-9-2003 - user changes
  SetNotEditable;

  cmbPlusTeamFrom.ShowSpeedButton := False;
  cmbPlusTeamFrom.ShowSpeedButton := True;
  cmbPlusTeamTo.ShowSpeedButton := False;
  cmbPlusTeamTo.ShowSpeedButton := True;
  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;
  cmbPlusShift.ShowSpeedButton := False;
  cmbPlusShift.ShowSpeedButton := True;
  // MR:17-02-2005 Order 550378 Team-From-To.
  ListProcsF.FillComboBoxMaster(StandStaffAvailDM.QueryTeam, 'TEAM_CODE',
    True, cmbPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(StandStaffAvailDM.QueryTeam, 'TEAM_CODE',
    False, cmbPlusTeamTo);
  FillPlants;
  FillShifts;
  for Index := 1 to 7 do
    dxDetailGrid.Bands[Index + 2].Caption := SystemDM.GetDayWDescription(Index);
  GroupBoxDay1.Caption := SystemDM.GetDayWDescription(1);
  GroupBoxDay2.Caption := SystemDM.GetDayWDescription(2);
  GroupBoxDay3.Caption := SystemDM.GetDayWDescription(3);
  GroupBoxDay4.Caption := SystemDM.GetDayWDescription(4);
  GroupBoxDay5.Caption := SystemDM.GetDayWDescription(5);
  GroupBoxDay6.Caption := SystemDM.GetDayWDescription(6);
  GroupBoxDay7.Caption := SystemDM.GetDayWDescription(7);
  dxDetailGrid.SetFocus;
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
  SetRedColor;
//RV067.1.
  BuildPopupMenu();

 //CAR 21-7-2003
//  TBLenghtClass := TTBLengthClass.Create;
end;

procedure TStandStaffAvailF.SetValueForAllEditFields(Value: String;
  DaySelected: Integer);
var
  C: TComponent;
  TBIndex: Integer;
begin
  if (not FCtrlState)  or (Value = '') then
    Exit;
  if not FPopupOpen then
    Exit;
  if DaySelected = 0 then
    Exit;
  DaySelected := DaySelected div 10;
  for TBIndex := 1 to SystemDM.MaxTimeblocks do
  begin
    C := FindComponent('Edit'+IntToStr(DaySelected)+IntToStr(TBIndex));
    if Assigned(C) then
      if (C is TEdit) then
        TEdit(C).Text := Value;
  end;
{
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TEdit) then
      if ((TempComponent as TEdit).Tag <> 0) and
       ((TempComponent as TEdit).Enabled) then
        begin
          IndexDay := (TempComponent as TEdit).Tag div 10;
          if DaySelected = IndexDay then
            (TempComponent as TEdit).Text   := Value;
        end;
  end;
}
end;

procedure TStandStaffAvailF.PopupItemClick(Sender: TObject);
var
  Absence: String;
begin
  inherited;
  Absence := (Sender as TMenuItem).Caption;
  if Absence = '_' then
    Absence := UNAVAILABLE;

  if (Absence <> DEFAULTAVAILABILITYCODE) and (Absence <> UNAVAILABLE) then
    Absence := Copy(Absence, 1, 1);
    
  FEditField.Text := Absence;
  EnabledNavButtons(True);

  FEmployee :=
    StandStaffAvailDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FPlant :=
    StandStaffAvailDM.QueryDetail.FieldByName('PLANT').AsString;
  FShift :=
    StandStaffAvailDM.QueryDetail.FieldByName('SHIFT').AsInteger;
  FDept :=
    StandStaffAvailDM.QueryDetail.FieldByName('DEPARTMENT').AsString;
  SetValueForAllEditFields(Absence, FEditField.Tag);
  FillArraySTA(FRecNo);
  FPopupOpen := False;
end;

procedure TStandStaffAvailF.SetRedColor;
begin
//  BitBtnRefresh.Font.Color := clRed;
end;

procedure TStandStaffAvailF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  BuildPopupMenu;
  FillShifts;
end;

procedure TStandStaffAvailF.SetRefreshStatus(OK: Boolean);
begin
  if OK then
  begin
    BitBtnRefresh.Font.Color := clWindowText;
    RefreshGrid(True);
  end
  else
  begin
//    BitBtnRefresh.Font.Color := clRed;
    RefreshGrid(False);
    FillEditDetail(False);
  end;
end;

procedure TStandStaffAvailF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
  // MR:17-02-2005 Order 550378 Team-From-To.
  cmbPlusTeamFrom.Enabled := not CheckBoxAllTeam.Checked;
  cmbPlusTeamTo.Enabled := not CheckBoxAllTeam.Checked;
  FillPlants;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.CheckBoxAllPlantClick(Sender: TObject);
begin
  inherited;
  cmbPlusPlant.Enabled := not CheckBoxAllPlant.Checked;
  CheckBoxAllShift.Enabled := not CheckBoxAllPlant.Checked;
  CheckBoxAllShift.Checked := CheckBoxAllPlant.Checked;
  cmbPlusShift.Enabled := not CheckBoxAllPlant.Checked;
  FillShifts;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.CheckBoxAllShiftClick(Sender: TObject);
begin
  inherited;
  cmbPlusShift.Enabled := not CheckBoxAllShift.Checked;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.FillEditDetail(OK: Boolean);
var
  Day, TBlock, TBCount: Integer;
  C: TComponent;
  procedure GridColumnDisplay(AVisible: Boolean);
  var
    C: TComponent;
  begin
    C := FindComponent('dxDetailGridColumnD'+IntToStr(Day)+IntToStr(TBlock)+'CALC');
    if Assigned(C) then
      TdxDBGridColumn(C).Visible := AVisible;
  end;
begin
  for Day := 1 to 7 do
  begin
    TBCount := 0;
    for TBlock := 1 to SystemDM.MaxTimeblocks do
    begin
      C := FindComponent('Edit'+IntToStr(Day)+IntToStr(TBlock));
      if Assigned(C) then
        if (C is TEdit) then
        begin
          if ((StandStaffAvailDM.FTBValid[TBlock] <> 0) and OK) then
          begin
            TEdit(C).Text := StandStaffAvailDM.QueryDetail.
              FieldByName('D'+IntToStr(Day)+IntToStr(TBlock)+'CALC').AsString;
            TEdit(C).Enabled := True;
            TEdit(C).Color := clRequired;
            TEdit(C).Visible := True;
            TEdit(C).TabStop := True; // GLOB3-225
            GridColumnDisplay(True);
            inc(TBCount);
          end
          else
          begin
            TEdit(C).Enabled := False;
            TEdit(C).Color := clDisabled;
            TEdit(C).Text := '';
            if TBlock > MIN_TBS then
            begin
              TEdit(C).Visible := False;
              TEdit(C).TabStop := False; // GLOB3-225
              GridColumnDisplay(False);
            end;
          end;
        end;
    end; // for TBlock
    // Resize the day-groupbox in detail-panel
    GroupBoxResize(Day, TBCount);
  end; // for Day
end; // FillEditDetail

//fill new values into FSTADAY array
procedure TStandStaffAvailF.FillArraySTA(var RecNo: Double);
var
//  TempComponent: TComponent;
//  Counter, Index, Day, TBlock: Integer;
  Day, TBlock: Integer;
  C: TComponent;
begin
  for Day := 1 to 7 do
    for TBlock := 1 to SystemDM.MaxTimeblocks do
    begin
      C := FindComponent('Edit'+IntToStr(Day)+IntToStr(TBlock));
      if Assigned(C) then
        if (C is TEdit) then
          StandStaffAvailDM.FSTADAY[Day, TBlock] := TEdit(C).Text;
    end;

{
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TEdit) then
    begin
      Index := (TempComponent as TEdit).Tag;
      Day := Index div 10;
      TBlock := Index - (Day * 10);
      StandStaffAvailDM.FSTADAY[Day, TBlock] := (TempComponent as TEdit).Text;
    end;
  end;
}
  Recno := StandStaffAvailDM.QueryDetail.FieldByName('RECNO').AsFloat;
end; // FillArraySTA

function TStandStaffAvailF.CheckValidEditFields: Boolean;
var
  C: TComponent;
  Day, TBlock: Integer;
begin
  Result := True;
  if not dxBarBDBNavPost.Enabled then
    Exit;
  for Day := 1 to 7 do
  begin
    for TBlock := 1 to SystemDM.MaxTimeblocks do
    begin
      C := FindComponent('Edit'+IntToStr(Day)+IntToStr(TBlock));
      if Assigned(C) then
        if (C is TEdit) then
          if (StandStaffAvailDM.FTBValid[TBlock] <> 0) and
            (TEdit(C).Text = '') then
          begin
            Result := False;
            DisplayMessage(SCheckAvailableTBNotEmpty, mtInformation, [mbOk]);
            Exit;
          end;
          if (TEdit(C).Text <> DEFAULTAVAILABILITYCODE) and
             (TEdit(C).Text <> UNAVAILABLE) and
             (TEdit(C).Text <> '') and
             (not ValidAbsenceReasonCode(TEdit(C).Text)) then
          begin
             Result := False;
             DisplayMessage(SCheckAvailableTB, mtInformation, [mbOk]);
             Exit;
           end;
    end;
  end;
{
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TEdit) then
    begin
      Index := (TempComponent as TEdit).Tag;
      Day := Index div 10;
      TBlock := Index - (Day * 10);
      if (StandStaffAvailDM.FTBValid[TBlock] <> 0) and
         ((TempComponent as TEdit).Text = '') then
      begin
        Result:=False;
        DisplayMessage(SCheckAvailableTBNotEmpty, mtInformation, [mbOk]);
        Exit;
      end;
//RV067.1.
      if ((TempComponent as TEdit).Text <> DEFAULTAVAILABILITYCODE) and
         ((TempComponent as TEdit).Text <> UNAVAILABLE) and
         ((TempComponent as TEdit).Text <> '') and
         (not ValidAbsenceReasonCode((TempComponent as TEdit).Text))
      then
       begin
         Result := False;
         DisplayMessage(SCheckAvailableTB, mtInformation, [mbOk]);
         Exit;
       end;
    end;
  end;
}
end; // CheckValidEditFields

function TStandStaffAvailF.CheckArraySTA: Boolean;
var
  Day, TBlock: Integer;
begin
  Result := True;
  if not dxBarBDBNavPost.Enabled then
    Exit;
  for Day := 1 to 7 do
    for TBlock := 1 to SystemDM.MaxTimeblocks do
    begin
      if (StandStaffAvailDM.FTBValid[TBlock] <> 0) and
        (StandStaffAvailDM.FSTADAY[Day, TBlock] = '') then
      begin
        Result:=False;
        DisplayMessage(SCheckAvailableTBNotEmpty, mtInformation, [mbOk]);
        Exit;
      end;
//RV067.1.
      if (StandStaffAvailDM.FSTADAY[Day, TBlock] <> DEFAULTAVAILABILITYCODE) and
         (StandStaffAvailDM.FSTADAY[Day, TBlock] <> UNAVAILABLE) and
         (StandStaffAvailDM.FSTADAY[Day, TBlock] <> '') and
         (not ValidAbsenceReasonCode(StandStaffAvailDM.FSTADAY[Day, TBlock]))
       then
       begin
         Result := False;
         DisplayMessage(SCheckAvailableTB, mtInformation, [mbOk]);
         Exit;
       end;
    end;
end;

procedure TStandStaffAvailF.Edit11Change(Sender: TObject);
begin
  inherited;
  (Sender as TEdit).SelectAll;
end;

//CAR 550244: changes for performance
procedure TStandStaffAvailF.SetPositionOnScreen(RecNo: Double);
var
  Day, TBlock: Integer;
// IndexColumn: Integer;
// RecNoMax: Double;
  StrTmp: String;
  function StrTmpInit: String;
  var
    TBIndex: Integer;
  begin
    Result := '';
    for TBIndex := 1 to SystemDM.MaxTimeblocks do
      Result := Result + '!';
  end; // StrTmpInit
begin
  EnabledNavButtons(False);
  FillEditDetail(True);

  if StandStaffAvailDM.ClientDataSetSave.FindKey([FRecno]) then
    StandStaffAvailDM.ClientDataSetSave.Edit
  else
  begin
    StandStaffAvailDM.ClientDataSetSave.Insert;
    StandStaffAvailDM.ClientDataSetSave.
      FieldByName('RECNO').AsFloat := FRecNo;
  end;
  for Day := 1 to 7 do
  begin
    StrTmp := StrTmpInit;
    for TBlock := 1 to SystemDM.MaxTimeblocks do
      if StandStaffAvailDM.FSTADAY[Day,TBlock] <> '' then
        StrTmp[TBlock] := StandStaffAvailDM.FSTADAY[Day,TBlock][1];
    StandStaffAvailDM.ClientDataSetSave.FieldByName('DAY' +
      IntToStr(Day)).AsString := StrTmp;
  end;
  StandStaffAvailDM.ClientDataSetSave.Post;
   // restore the screen position and recalculate fields
  if dxBarButtonSort.Down then
  begin
    StandStaffAvailDM.GotoCurrentRecord(Recno);
    dxDetailGrid.SetFocus;
    Exit;
  end;

  if not dxBarButtonSort.down then
  begin
  // MR:10-03-2005 Disabled, because it's function is not clear.
  // Pims -> Oracle
(*
// set position on last empl of the current screen , restore the screen
    IndexColumn := 0;
    for i := 0 to dxdetailgrid.columncount -1 do
      if dxDetailGrid.Columns[i].FieldName = 'RECNO' then
      begin
        IndexColumn := i;
        Break;
      end;
    RecNoMax := dxDetailGrid.Items[dxDetailGrid.Count-1].Values[IndexColumn];

    StandStaffAvailDM.GotoCurrentRecord(RecNoMax-1);
 // restore the position
    i := dxDetailGrid.Count-1 ;
    while (i >= 0) do
    begin
      if (dxDetailGrid.Items[i].Values[IndexColumn] = RecNo)then
        Exit;
      dxDetailGrid.GotoPrev(True);
      Dec(i);
    end;
// set edit panel
*)
    // MR:10-03-2005 The following should do the same...
    StandStaffAvailDM.QueryDetail.Locate('RECNO', RecNo, []);
  end;
end;

procedure TStandStaffAvailF.dxBarBDBNavPostClick(Sender: TObject);
var
  UpdateTable: Boolean;
begin
  inherited;
  StandStaffAvailDM.GetValidTB(FEmployee, FShift, FPlant, FDept);
  //550291
  FillArraySTA(FRecNo);
  if CheckArraySTA then
  begin
    StandStaffAvailDM.UpdateStandardAvail(FPlant, FDept, FEmployee ,
      FShift, 1, 7, UpdateTable);
    if UpdateTable then
    begin
      StandStaffAvailDM.QueryDetail.OnCalcFields :=
        StandStaffAvailDM.QueryDetailCalcFieldsAfterSave;
      SetPositionOnScreen(StandStaffAvailDM.
        QueryDetail.FieldByName('RECNO').AsFloat);
    end
    else
      dxBarBDBNavCancel.OnClick(Sender);
  end;
end;

procedure TStandStaffAvailF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
  ButtonCopy.Enabled := Not Active;
end;

procedure TStandStaffAvailF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  FillEditDetail(True);
  EnabledNavButtons(False);
  if dxDetailGrid.CanFocus then
    dxDetailGrid.SetFocus;
end;

procedure TStandStaffAvailF.AskForChanges;
var
  QuestionResult: Integer;
begin
  QuestionResult := 0;
  if dxBarBDBNavPost.Enabled then
    QuestionResult := DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo]);
  case QuestionResult of
    mrYes    : dxBarBDBNavPost.OnClick(dxBarDBNavigator);
    mrNo     : dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end; { case }
end;

procedure TStandStaffAvailF.Edit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  FPopupOpen := False;
  EnabledNavButtons(True);

  FEmployee :=
    StandStaffAvailDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  FPlant :=
    StandStaffAvailDM.QueryDetail.FieldByName('PLANT').AsString;
  FShift :=
    StandStaffAvailDM.QueryDetail.FieldByName('SHIFT').AsInteger;
  FDept :=
    StandStaffAvailDM.QueryDetail.FieldByName('DEPARTMENT').AsString;
end;

procedure TStandStaffAvailF.dxGridEnter(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
  begin
    with StandStaffAvailDM do
     GetValidTB(
       QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
       QueryDetail.FieldByName('SHIFT').AsInteger,
       QueryDetail.FieldByName('PLANT').AsString,
       QueryDetail.FieldByName('DEPARTMENT').AsString);
    FillEditDetail(True);
  end
  else
    AskForChanges;
end;

procedure TStandStaffAvailF.Edit11Exit(Sender: TObject);
begin
  inherited;
  Exit; // GLOB3-225 Do not show messages.

  if not dxBarBDBNavPost.Enabled then
     Exit;
  //CAR - 550291 - not necessary here
  // FillArraySTA(FRecNo);
  if ((Sender as TEdit).Text = '') then
  begin
    DisplayMessage(SPimsFieldRequired, mtInformation, [mbOk]);
    Exit;
  end;
//RV067.1.
  if ((Sender as TEdit).Text <> DEFAULTAVAILABILITYCODE) and
    ((Sender as TEdit).Text <> UNAVAILABLE) and 
    (not ValidAbsenceReasonCode((Sender as TEdit).Text))
  then
  begin
    DisplayMessage(SCheckAvailableTB, mtInformation, [mbOk]);
  end;
end;

procedure TStandStaffAvailF.CheckBoxPaidYNClick(Sender: TObject);
begin
  inherited;
  SetRefreshStatus(False);
end;

//CAR 6-10-2003: Improve performance: 550244
procedure TStandStaffAvailF.RefreshGrid(OK: Boolean);
var
  Bookmark: TBookmark;
begin
  BookMark := nil;
  if not OK then
  begin
    StandStaffAvailDM.QueryDetail.Active := False;
    StandStaffAvailDM.QueryDetail.AfterScroll := Nil;
    Exit;
  end;
  if StandStaffAvailDM <> Nil then
  begin
    if StandStaffAvailDM.QueryDetail.Active and
      (not StandStaffAvailDM.QueryDetail.Eof) then
      BookMark := StandStaffAvailDM.QueryDetail.GetBookmark;

    StandStaffAvailDM.QueryDetail.OnCalcFields :=
      StandStaffAvailDM.QueryDetailCalcFieldsOnRefresh;
    // MR:17-02-2005 Order 550378 Team-From-To.
    StandStaffAvailDM.FillQueryDetail(CheckBoxAllTeam.Checked,
       CheckBoxAllPlant.Checked, CheckBoxAllShift.Checked,
       not CheckBoxOnlyActiveEmp.Checked,
       GetStrValue(cmbPlusTeamFrom.Value),
       GetStrValue(cmbPlusTeamTo.Value),
       GetStrValue(cmbPlusPlant.Value),
       IntToStr(GetIntValue(cmbPlusShift.Value)));
    StandStaffAvailDM.QueryDetail.First;

    StandStaffAvailDM.QueryDetail.AfterScroll :=
        StandStaffAvailDM.DefaultAfterScroll;
    EnabledNavButtons(False);
    if Assigned(BookMark) then
    begin
      StandStaffAvailDM.QueryDetail.GotoBookmark(Bookmark);
      StandStaffAvailDM.QueryDetail.FreeBookmark(Bookmark);
    end;
    FillEditDetail(True);
    // GLOB3-60 Do this to ensure it shows the right number of
    //          timeblocks after refresh (instead of after clicking in grid).
    SetPositionOnScreen(StandStaffAvailDM.
      QueryDetail.FieldByName('RECNO').AsFloat);
  end;
end;

procedure TStandStaffAvailF.ButtonCopyClick(Sender: TObject);
var
  RecNo: Double;
  Bookmark: TBookmark;
begin
  inherited;
//  if (BitBtnRefresh.Font.Color = clRed) then
  if not StandStaffAvailDM.QueryDetail.Active then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbYes]);
    Exit;
  end;
  if (StandStaffAvailDM.QueryDetail.RecordCount = 0) then
  begin
    DisplayMessage(SEmptyRecord, mtConfirmation, [mbYes]);
    Exit;
  end;
  if StandStaffAvailDM.QueryDetail.Active and
    (not StandStaffAvailDM.QueryDetail.Eof) then
  begin
    Bookmark := StandStaffAvailDM.QueryDetail.GetBookmark;
    if not Assigned(Bookmark) then
    begin
      DisplayMessage(SEmptyRecord, mtConfirmation, [mbYes]);
      Exit;
    end
    else
      StandStaffAvailDM.QueryDetail.FreeBookmark(Bookmark);
  end;

  StandStaffAvailDM.GetValidTB(
    StandStaffAvailDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    StandStaffAvailDM.QueryDetail.FieldByName('SHIFT').AsInteger,
    StandStaffAvailDM.QueryDetail.FieldByName('PLANT').AsString,
    StandStaffAvailDM.QueryDetail.FieldByName('DEPARTMENT').AsString);
  if StandStaffAvailDM.FTBValid[1] = 0 then
  begin
    DisplayMessage(SCopySTANotValidTo , mtConfirmation, [mbYes]);
    Exit;
  end;
  FillEditDetail(True);
  DialogCopyFromSTAF := TDialogCopyFromSTAF.Create(Application);

  DialogCopyFromSTAF.FEmployee :=
    StandStaffAvailDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;

  // GLOB3-60 Use the selected plant here, not from query!
  DialogCopyFromSTAF.PlantDisplayValue := '';
  if not CheckBoxAllPlant.Checked then
  begin
    DialogCopyFromSTAF.FPlant := GetStrValue(CmbPlusPlant.Value);
    DialogCopyFromSTAF.PlantDisplayValue := CmbPlusPlant.Value;
  end
  else
    DialogCopyFromSTAF.FPlant :=
      StandStaffAvailDM.QueryDetail.FieldByName('PLANT').AsString;
  // GLOB3-60 Use the selected shift here, not from query!
  DialogCopyFromSTAF.ShiftDisplayValue := '';
  if not CheckBoxAllShift.Checked then
  begin
    DialogCopyFromSTAF.FSHIFT := GetIntValue(cmbPlusShift.Value);
    DialogCopyFromSTAF.ShiftDisplayValue := cmbPlusShift.Value;
  end
  else
    DialogCopyFromSTAF.FSHIFT :=
      StandStaffAvailDM.QueryDetail.FieldByName('SHIFT').AsInteger;
  DialogCopyFromSTAF.FDepartment :=
    StandStaffAvailDM.QueryDetail.FieldByName('DEPARTMENT').AsString;

  RecNo := StandStaffAvailDM.QueryDetail.FieldByName('RECNO').AsFloat;
  FRecNo := Recno;
  FillArraySTA(FRecNo);
  try
    DialogCopyFromSTAF.ShowModal;
  finally
    if DialogCopyFromSTAF.FCopy then
    begin
     StandStaffAvailDM.QueryDetail.OnCalcFields :=
        StandStaffAvailDM.QueryDetailCalcFieldsAfterSave;

  //    StandStaffAvailDM.ClientDataSetTotalHours.EmptyDataSet;
      StandStaffAvailDM.DeleteCalculateHours(DialogCopyFromSTAF.FPlant,
        DialogCopyFromSTAF.FDepartment,
        DialogCopyFromSTAF.FEmployee, DialogCopyFromSTAF.FSHIFT);
      SetPositionOnScreen(RecNo);
    end;
    DialogCopyFromSTAF.Free;
  end;
end;

procedure TStandStaffAvailF.BitBtnRefreshClick(Sender: TObject);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then // GLOB3-94
    Exit;

//  if BitBtnRefresh.Font.Color = clRed then
  begin
    if dxBarButtonSort.Down then
    begin
      dxBarButtonSort.Down := False;
      dxBarButtonSortClick(dxBarButtonSort);
    end;

    SetRefreshStatus(True);
  end;
end;

procedure TStandStaffAvailF.cmbPlusShiftChange(Sender: TObject);
begin
  inherited;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.Edit11MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

begin
  inherited;
  (Sender as TEdit).SelectAll;
  FEditField := (Sender as TEdit);
  FCtrlState := (ssCtrl in Shift);
end;

procedure TStandStaffAvailF.Edit11ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  inherited;
  FPopupOpen := False;
end;

procedure TStandStaffAvailF.PopupMenuStdAvailPopup(Sender: TObject);
begin
  inherited;
  FPopupOpen := True;
end;

procedure TStandStaffAvailF.dxDetailGridClick(Sender: TObject);
begin
  AskForChanges;
  inherited;
end;

procedure TStandStaffAvailF.dxBarButtonExportGridClick(Sender: TObject);
begin
  FloatEmpl := 'EMPLOYEE_NUMBER';
  inherited;
end;

procedure TStandStaffAvailF.CheckBoxOnlyActiveEmpClick(Sender: TObject);
begin
  inherited;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.cmbPlusTeamFromCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamTo.DisplayValue := cmbPlusTeamFrom.DisplayValue;
  FillPlants;
  FillShifts;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.cmbPlusTeamToCloseUp(Sender: TObject);
begin
  inherited;
  if (cmbPlusTeamFrom.DisplayValue <> '') and
     (cmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(cmbPlusTeamFrom.Value) >
      GetStrValue(cmbPlusTeamTo.Value) then
      cmbPlusTeamFrom.DisplayValue := cmbPlusTeamTo.DisplayValue;
  FillPlants;
  FillShifts;
  SetRefreshStatus(False);
end;

procedure TStandStaffAvailF.GroupBoxResize(ADay, ATBCount: Integer);
var
  C: TComponent;
begin
  if ATBCount < MIN_TBS then
    ATBCount := MIN_TBS;
  C := FindComponent('GroupBoxDay' + IntToStr(ADay));
  if Assigned(C) then
    if (C is TGroupBox) then
    begin
      TGroupBox(C).Width := (ATBCount * (Edit11.Width+1)) + 6;
    end;
end; // GroupBoxResize

// GLOB3-225
procedure TStandStaffAvailF.HideColumns;
var
  Day, I: Integer;
  C: TComponent;
begin
  for Day := 1 to 7 do
    for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
    begin
      C := FindComponent('dxDetailGridColumnD' + IntToStr(Day) +
        IntToStr(I) + 'CALC');
      if Assigned(C) then
        if (C is TdxDBGridColumn) then
        begin
          TdxDBGridColumn(C).Visible := False;
          TdxDBGridColumn(C).DisableGrouping := True;
        end;
    end;
  // Handle Tabs here
  for Day := 1 to 7 do
    for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
    begin
      C := FindComponent('Edit' + IntToStr(Day) + IntToStr(I));
      if Assigned(C) then
        if (C is TEdit) then
        begin
          TEdit(C).TabStop := False;
        end;
    end;
end; // HideColumns

end.
