(*
  MRA:17-DEC-2010 RV083.1.
  - This does not filter on absence reasons-per-country.
      Only show absence reasons at bottom of report
      that were shown in absence card.
*)

unit ReportHolidayCardDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables;

type
  TReportHolidayCardDM = class(TReportBaseDM)
    QueryAbsence: TQuery;
    DataSourceAbsence: TDataSource;
    TablePlant: TTable;
    TableAbs: TTable;
    TableEmpl: TTable;
    QueryAbsencePlants: TQuery;
    qryAbsenceReasons: TQuery;
    qryAbsenceTotalCur: TQuery;
    qryAbsenceTotalPrev: TQuery;
    qryGetTFTHours: TQuery;
    qryAbsenceReasonsEmp: TQuery; // RV083.1.
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportHolidayCardDM: TReportHolidayCardDM;

implementation

{$R *.DFM}

end.
