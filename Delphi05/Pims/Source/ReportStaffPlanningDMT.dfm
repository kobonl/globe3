inherited ReportStaffPlanningDM: TReportStaffPlanningDM
  OldCreateOrder = True
  Top = 155
  Height = 479
  Width = 741
  object QueryEMP: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEMPFilterRecord
    SQL.Strings = (
      'SELECT '
      '  EMA.EMPLOYEEAVAILABILITY_DATE '
      'FROM '
      '  EMPLOYEEAVAILABILITY EMA '
      'WHERE'
      '  EMA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART AND'
      '  EMA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND ')
    Left = 56
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'FDATESTART'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FDATEEND'
        ParamType = ptUnknown
      end>
  end
  object TableSTO: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'STANDARDOCCUPATION'
    Left = 232
    Top = 120
  end
  object QueryExec: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    Left = 464
    Top = 248
  end
  object QueryDept: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DEPARTMENT_CODE, '
      '  DESCRIPTION '
      'FROM '
      '  DEPARTMENT'
      'ORDER BY '
      '  PLANT_CODE, DEPARTMENT_CODE')
    Left = 296
    Top = 32
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION '
      'FROM '
      '  SHIFT'
      'ORDER BY '
      '  PLANT_CODE, SHIFT_NUMBER ')
    Left = 376
    Top = 32
  end
  object TablePlant: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'PLANT'
    Left = 456
    Top = 32
  end
  object QueryWK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, WORKSPOT_CODE, '
      '  DESCRIPTION '
      'FROM '
      '  WORKSPOT'
      'ORDER BY '
      '  PLANT_CODE, WORKSPOT_CODE')
    Left = 232
    Top = 32
  end
  object QueryOCP: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER, OCC_PLANNING_DATE,'
      '  OCC_A_TIMEBLOCK_1, OCC_A_TIMEBLOCK_2,'
      '  OCC_A_TIMEBLOCK_3, OCC_A_TIMEBLOCK_4,'
      '  OCC_A_TIMEBLOCK_5, OCC_A_TIMEBLOCK_6,'
      '  OCC_A_TIMEBLOCK_7, OCC_A_TIMEBLOCK_8,'
      '  OCC_A_TIMEBLOCK_9, OCC_A_TIMEBLOCK_10,'
      '  OCC_B_TIMEBLOCK_1, OCC_B_TIMEBLOCK_2,'
      '  OCC_B_TIMEBLOCK_3, OCC_B_TIMEBLOCK_4,'
      '  OCC_B_TIMEBLOCK_5, OCC_B_TIMEBLOCK_6,'
      '  OCC_B_TIMEBLOCK_7, OCC_B_TIMEBLOCK_8,'
      '  OCC_B_TIMEBLOCK_9, OCC_B_TIMEBLOCK_10,'
      '  OCC_C_TIMEBLOCK_1, OCC_C_TIMEBLOCK_2,'
      '  OCC_C_TIMEBLOCK_3, OCC_C_TIMEBLOCK_4,'
      '  OCC_C_TIMEBLOCK_5, OCC_C_TIMEBLOCK_6,'
      '  OCC_C_TIMEBLOCK_7, OCC_C_TIMEBLOCK_8,'
      '  OCC_C_TIMEBLOCK_9, OCC_C_TIMEBLOCK_10'
      'FROM'
      '  OCCUPATIONPLANNING '
      'WHERE'
      '  OCC_PLANNING_DATE >= :DATEMIN AND '
      '  OCC_PLANNING_DATE <= :DATEMAX '
      'ORDER BY '
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER, OCC_PLANNING_DATE')
    Left = 64
    Top = 120
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object QueryEMPPLN: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER, EMPLOYEEPLANNING_DATE, EMPLOYEE_NUMBER,'
      '  SCHEDULED_TIMEBLOCK_1, SCHEDULED_TIMEBLOCK_2,'
      '  SCHEDULED_TIMEBLOCK_3, SCHEDULED_TIMEBLOCK_4 '
      'FROM '
      '  EMPLOYEEPLANNING '
      'WHERE'
      '  EMPLOYEEPLANNING_DATE >= :DATEMIN AND '
      '  EMPLOYEEPLANNING_DATE <= :DATEMAX  AND'
      '( (SCHEDULED_TIMEBLOCK_1 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_2 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_3 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) OR '
      '  (SCHEDULED_TIMEBLOCK_4 IN ('#39'A'#39', '#39'B'#39','#39'C'#39') ) )'
      'ORDER BY '
      '  PLANT_CODE, DEPARTMENT_CODE, WORKSPOT_CODE,'
      '  SHIFT_NUMBER')
    Left = 64
    Top = 200
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEMIN'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEMAX'
        ParamType = ptUnknown
      end>
  end
  object cdsPlannedEmployee: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'ByPrimary'
        Fields = 'PLANT_CODE;SHIFT_NUMBER;EMPLOYEE_NUMBER;PLAN_DATE'
      end>
    IndexName = 'ByPrimary'
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 280
    object cdsPlannedEmployeePLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object cdsPlannedEmployeeSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object cdsPlannedEmployeeEMPLOYEE_NUMBER: TIntegerField
      FieldName = 'EMPLOYEE_NUMBER'
    end
    object cdsPlannedEmployeePLAN_DATE: TDateTimeField
      FieldName = 'PLAN_DATE'
    end
  end
  object QueryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, DESCRIPTION'
      'FROM '
      '  EMPLOYEE'
      'ORDER BY '
      '  EMPLOYEE_NUMBER')
    Left = 456
    Top = 88
  end
  object qryRequestEarlyLate: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT T.EMPLOYEE_NUMBER, T.REQUEST_DATE, T.REQUESTED_EARLY_TIME' +
        ', T.REQUESTED_LATE_TIME'
      'FROM REQUESTEARLYLATE T'
      'WHERE T.REQUEST_DATE >= :DATEFROM AND T.REQUEST_DATE <= :DATETO'
      'ORDER BY T.EMPLOYEE_NUMBER'
      ' ')
    Left = 200
    Top = 280
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
