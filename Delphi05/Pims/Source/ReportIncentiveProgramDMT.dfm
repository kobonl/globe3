inherited ReportIncentiveProgramDM: TReportIncentiveProgramDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object qryEmployee: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  E.PLANT_CODE,'
      '  E.EMPLOYEE_NUMBER,'
      '  E.DESCRIPTION,'
      '  E.DEPARTMENT_CODE,'
      '  E.STARTDATE,'
      '  E.ENDDATE,'
      '  CASE'
      '    WHEN TO_DATE(:LASTDATEPREVMONTH) - E.STARTDATE > 60 THEN'
      '      '#39'Y'#39
      '    ELSE'
      '      '#39'N'#39
      '  END EMPLOYED60'
      'FROM'
      '  EMPLOYEE E'
      'WHERE'
      '  E.PLANT_CODE >= :PLANTFROM AND E.PLANT_CODE <= :PLANTTO AND'
      
        '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND E.EMPLOYEE_NUMBER <= :E' +
        'MPLOYEETO AND'
      '  E.CALC_BONUS_YN = '#39'Y'#39' AND'
      '  (E.ENDDATE >= :ENDDATE OR E.ENDDATE IS NULL)'
      '  AND'
      '  ('
      '    (E.STARTDATE <= :DATETO)'
      '    AND'
      '    ('
      '      (E.ENDDATE IS NULL)'
      '      OR'
      '      (E.ENDDATE >= :DATEFROM)'
      '    )'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 40
    Top = 16
    ParamData = <
      item
        DataType = ftDate
        Name = 'LASTDATEPREVMONTH'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDate
        Name = 'ENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end>
  end
  object qryAbsence: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  AR.DESCRIPTION, AHE.EMPLOYEE_NUMBER, AHE.ABSENCEHOUR_DATE'
      'FROM'
      '  ABSENCEHOURPEREMPLOYEE AHE INNER JOIN ABSENCEREASON AR ON'
      '  AHE.ABSENCEREASON_ID = AR.ABSENCEREASON_ID'
      '  INNER JOIN EMPLOYEE E ON'
      '  AHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  E.CALC_BONUS_YN = '#39'Y'#39' AND'
      '  AHE.ABSENCEHOUR_DATE >= :DATEFROM AND'
      '  AHE.ABSENCEHOUR_DATE <= :DATETO AND'
      '  AR.ABSENCETYPE_CODE IN ('#39'I'#39', '#39'U'#39')'
      'ORDER BY'
      '  AHE.EMPLOYEE_NUMBER, AHE.ABSENCEHOUR_DATE'
      ''
      ' ')
    Left = 40
    Top = 72
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryEarlyLateXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT C.*'
      'FROM'
      '('
      'SELECT B.EMPLOYEE_NUMBER,'
      '       B.PLANT_CODE,'
      '       B.SHIFT_NUMBER,'
      '       B.SDATE,'
      '       B.DEPARTMENT_CODE,'
      '       B.SDATETIME_IN,'
      '       B.SDATETIME_OUT,'
      '       B.DATETIME_IN,'
      '       B.DATETIME_OUT,'
      '       CASE'
      '         WHEN B.STARTTIME = 0 AND B.ENDTIME = 0 THEN'
      '           (SELECT '
      '             CASE '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 1 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME1, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 2 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME2, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 3 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME3, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 4 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME4, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 5 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME5, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 6 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME6, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_IN) = 7 THEN'
      '                 TO_NUMBER(TO_CHAR(S.STARTTIME7, '#39'HH24MI'#39')) '
      '             END'
      
        '             FROM SHIFT S WHERE S.PLANT_CODE = B.PLANT_CODE AND ' +
        'S.SHIFT_NUMBER = B.SHIFT_NUMBER)'
      '         ELSE B.STARTTIME'
      '       END STARTTIME,'
      '       CASE '
      '         WHEN B.STARTTIME = 0 AND B.ENDTIME = 0 THEN'
      '           (SELECT '
      '             CASE '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 1 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME1, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 2 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME2, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 3 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME3, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 4 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME4, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 5 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME5, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 6 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME6, '#39'HH24MI'#39')) '
      '               WHEN PIMSDAYOFWEEK(B.SDATETIME_OUT) = 7 THEN'
      '                 TO_NUMBER(TO_CHAR(S.ENDTIME7, '#39'HH24MI'#39')) '
      '             END'
      
        '             FROM SHIFT S WHERE S.PLANT_CODE = B.PLANT_CODE AND ' +
        'S.SHIFT_NUMBER = B.SHIFT_NUMBER)'
      '         ELSE B.ENDTIME'
      '       END ENDTIME'
      '  FROM (SELECT A.EMPLOYEE_NUMBER,'
      '               A.PLANT_CODE,'
      '               A.SHIFT_NUMBER,'
      '               A.SDATE,'
      '               A.DEPARTMENT_CODE,'
      '               A.DATETIME_IN SDATETIME_IN,'
      '               A.DATETIME_OUT SDATETIME_OUT,'
      
        '               TO_NUMBER(TO_CHAR(A.DATETIME_IN, '#39'HH24MI'#39')) DATET' +
        'IME_IN,'
      
        '               TO_NUMBER(TO_CHAR(A.DATETIME_OUT, '#39'HH24MI'#39')) DATE' +
        'TIME_OUT,'
      '               TO_NUMBER(TO_CHAR('
      '                         (SELECT CASE'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 1 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME1)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME1)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME1)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 2 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME2)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME2)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME2)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 3 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME3)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME3)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME3)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 4 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME4)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME4)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME4)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 5 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME5)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME5)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME5)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 6 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME6)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME6)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME6)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      
        '                                     WHEN PIMSDAYOFWEEK(A.DATETI' +
        'ME_IN) = 7 THEN'
      
        '                                      NVL((SELECT MIN(TE.STARTTI' +
        'ME7)'
      
        '                                            FROM TIMEBLOCKPEREMP' +
        'LOYEE TE'
      
        '                                           WHERE TE.PLANT_CODE =' +
        ' A.PLANT_CODE'
      
        '                                             AND TE.EMPLOYEE_NUM' +
        'BER = A.EMPLOYEE_NUMBER'
      
        '                                             AND TE.TIMEBLOCK_NU' +
        'MBER ='
      
        '                                                 (SELECT MIN(TE.' +
        'TIMEBLOCK_NUMBER)'
      
        '                                                    FROM TIMEBLO' +
        'CKPEREMPLOYEE TE'
      
        '                                                   WHERE TE.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TE.EMPL' +
        'OYEE_NUMBER ='
      
        '                                                         A.EMPLO' +
        'YEE_NUMBER)),'
      
        '                                          NVL((SELECT MIN(TD.STA' +
        'RTTIME7)'
      
        '                                                FROM TIMEBLOCKPE' +
        'RDEPARTMENT TD'
      
        '                                               WHERE TD.PLANT_CO' +
        'DE = A.PLANT_CODE'
      
        '                                                 AND TD.DEPARTME' +
        'NT_CODE ='
      
        '                                                     A.DEPARTMEN' +
        'T_CODE'
      
        '                                                 AND TD.TIMEBLOC' +
        'K_NUMBER ='
      
        '                                                     (SELECT MIN' +
        '(TD.TIMEBLOCK_NUMBER)'
      
        '                                                        FROM TIM' +
        'EBLOCKPERDEPARTMENT TD'
      
        '                                                       WHERE TD.' +
        'PLANT_CODE ='
      
        '                                                             A.P' +
        'LANT_CODE'
      
        '                                                         AND TD.' +
        'DEPARTMENT_CODE ='
      
        '                                                             A.D' +
        'EPARTMENT_CODE)),'
      
        '                                              NVL((SELECT MIN(TS' +
        '.STARTTIME7)'
      
        '                                                    FROM TIMEBLO' +
        'CKPERSHIFT TS'
      
        '                                                   WHERE TS.PLAN' +
        'T_CODE ='
      
        '                                                         A.PLANT' +
        '_CODE'
      
        '                                                     AND TS.SHIF' +
        'T_NUMBER ='
      
        '                                                         A.SHIFT' +
        '_NUMBER'
      
        '                                                     AND TS.TIME' +
        'BLOCK_NUMBER ='
      
        '                                                         (SELECT' +
        ' MIN(TS.TIMEBLOCK_NUMBER)'
      
        '                                                            FROM' +
        ' TIMEBLOCKPERSHIFT TS'
      
        '                                                           WHERE' +
        ' TS.PLANT_CODE ='
      
        '                                                                ' +
        ' A.PLANT_CODE'
      
        '                                                             AND' +
        ' TS.SHIFT_NUMBER ='
      
        '                                                                ' +
        ' A.SHIFT_NUMBER)),'
      '                                                  NULL)))'
      '                                   END STARTTIME'
      '                             FROM DUAL)'
      ''
      '                            '
      '                 , '#39'HH24MI'#39')) STARTTIME,'
      '               TO_NUMBER(TO_CHAR('
      '('
      '  SELECT'
      '  CASE'
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 1 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME1) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME1) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = '
      '         A.DEPARTMENT_CODE'
      '         AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = '
      '          A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME1) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 2 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME2) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME2) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = '
      '         A.DEPARTMENT_CODE'
      '         AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME2) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 3 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME3) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME3) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME3) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 4 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME4) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME4) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME4) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 5 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME5) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME5) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME5) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 6 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME6) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME6) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME6) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  WHEN PIMSDAYOFWEEK(A.DATETIME_OUT) = 7 THEN'
      '  NVL('
      '    (SELECT MAX(TE.ENDTIME7) '
      '     FROM TIMEBLOCKPEREMPLOYEE TE '
      '     WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '     TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER AND '
      '     TE.TIMEBLOCK_NUMBER = '
      '     (SELECT MAX(TE.TIMEBLOCK_NUMBER) '
      '      FROM TIMEBLOCKPEREMPLOYEE TE '
      '      WHERE TE.PLANT_CODE = A.PLANT_CODE AND '
      '      TE.EMPLOYEE_NUMBER = A.EMPLOYEE_NUMBER)), '
      '      NVL('
      '        (SELECT MAX(TD.ENDTIME7) '
      '         FROM TIMEBLOCKPERDEPARTMENT TD '
      '         WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '         TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE AND '
      '         TD.TIMEBLOCK_NUMBER = '
      '         (SELECT MAX(TD.TIMEBLOCK_NUMBER) '
      '          FROM TIMEBLOCKPERDEPARTMENT TD '
      '          WHERE TD.PLANT_CODE = A.PLANT_CODE AND '
      '          TD.DEPARTMENT_CODE = A.DEPARTMENT_CODE)),'
      '        NVL('
      '          (SELECT MAX(TS.ENDTIME7) '
      '           FROM TIMEBLOCKPERSHIFT TS '
      '           WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '           TS.SHIFT_NUMBER = A.SHIFT_NUMBER AND '
      '           TS.TIMEBLOCK_NUMBER = '
      '           (SELECT MAX(TS.TIMEBLOCK_NUMBER) '
      '            FROM TIMEBLOCKPERSHIFT TS '
      '            WHERE TS.PLANT_CODE = A.PLANT_CODE AND '
      '            TS.SHIFT_NUMBER = A.SHIFT_NUMBER)),'
      '          NULL'
      '         )'
      '        )'
      '    ) '
      '  END'
      '  ENDTIME'
      'FROM'
      'DUAL'
      ')'
      '                 '
      '                 , '#39'HH24MI'#39')) ENDTIME'
      '          FROM (SELECT T.EMPLOYEE_NUMBER,'
      '                       T.PLANT_CODE,'
      '                       T.SHIFT_NUMBER,'
      '                       TRUNC(T.DATETIME_IN) SDATE,'
      
        '                       MIN((SELECT W.DEPARTMENT_CODE FROM WORKSP' +
        'OT W WHERE W.PLANT_CODE = T.PLANT_CODE AND W.WORKSPOT_CODE = T.W' +
        'ORKSPOT_CODE)) DEPARTMENT_CODE,'
      '                       MIN(T.DATETIME_IN) DATETIME_IN,'
      '                       MAX(T.DATETIME_OUT) DATETIME_OUT'
      '                  FROM TIMEREGSCANNING T'
      '                 WHERE T.DATETIME_IN >= :DATEFROM'
      '                   AND T.DATETIME_IN < :DATETO'
      '                   AND T.DATETIME_OUT IS NOT NULL'
      '                 GROUP BY T.EMPLOYEE_NUMBER,'
      '                          T.PLANT_CODE,'
      '                          T.SHIFT_NUMBER,'
      '                          TRUNC(T.DATETIME_IN)'
      '                 ORDER BY 1, 4) A) B) C'
      ' WHERE (C.DATETIME_IN > C.STARTTIME)'
      '    OR (C.DATETIME_OUT < C.ENDTIME)'
      ''
      ''
      '')
    Left = 184
    Top = 240
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryEarlyLateOLD: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  B.*'
      'FROM'
      '('
      'SELECT '
      '  A.EMPLOYEE_NUMBER, A.PLANT_CODE, A.SHIFT_NUMBER, SDATE,'
      '  TO_NUMBER(TO_CHAR(A.DATETIME_IN, '#39'HH24MI'#39')) DATETIME_IN,'
      '  TO_NUMBER(TO_CHAR(A.DATETIME_OUT, '#39'HH24MI'#39')) DATETIME_OUT,'
      '  TO_NUMBER(TO_CHAR(A.STARTTIME, '#39'HH24MI'#39')) STARTTIME,'
      '  TO_NUMBER(TO_CHAR(A.ENDTIME, '#39'HH24MI'#39')) ENDTIME'
      'FROM'
      '('
      'SELECT '
      
        '  T.EMPLOYEE_NUMBER, T.PLANT_CODE, T.SHIFT_NUMBER, TRUNC(T.DATET' +
        'IME_IN) SDATE, MIN(T.DATETIME_IN) DATETIME_IN, MAX(T.DATETIME_OU' +
        'T) DATETIME_OUT, '
      
        '  MIN((SELECT S.STARTTIME3 FROM SHIFT S WHERE S.PLANT_CODE = T.P' +
        'LANT_CODE AND S.SHIFT_NUMBER = T.SHIFT_NUMBER)) STARTTIME,'
      
        '  MIN((SELECT S.ENDTIME3 FROM SHIFT S WHERE S.PLANT_CODE = T.PLA' +
        'NT_CODE AND S.SHIFT_NUMBER = T.SHIFT_NUMBER)) ENDTIME'
      'FROM TIMEREGSCANNING T'
      'WHERE T.DATETIME_IN >= :DATEFROM AND T.DATETIME_IN < :DATETO'
      
        'GROUP BY T.EMPLOYEE_NUMBER, T.PLANT_CODE, T.SHIFT_NUMBER, TRUNC(' +
        'T.DATETIME_IN)'
      'ORDER BY 1'
      ') A'
      ') B'
      'WHERE '
      '  (B.DATETIME_IN > B.STARTTIME) OR'
      '  (B.DATETIME_OUT < B.ENDTIME)'
      '')
    Left = 184
    Top = 136
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryShiftXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT S.PLANT_CODE, S.SHIFT_NUMBER'
      'FROM SHIFT S'
      'ORDER BY 1, 2'
      '')
    Left = 184
    Top = 184
  end
  object qryShiftLoop: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  T.PLANT_CODE,'
      '  T.SHIFT_NUMBER,'
      '  W.DEPARTMENT_CODE,'
      '  T.SHIFT_DATE,'
      '  TRUNC(T.DATETIME_IN) DATE_IN'
      'FROM TIMEREGSCANNING T INNER JOIN WORKSPOT W ON'
      '  T.PLANT_CODE = W.PLANT_CODE AND'
      '  T.WORKSPOT_CODE = W.WORKSPOT_CODE'
      'WHERE'
      '  ('
      '    ('
      '      T.DATETIME_IN >= :DATEFROM AND'
      '      T.DATETIME_IN < :DATETO'
      '    )'
      '    OR'
      '    ('
      '      T.DATETIME_OUT IS NOT NULL AND'
      '      (T.DATETIME_OUT >= :DATEFROM AND'
      '       T.DATETIME_OUT < :DATETO)'
      '     )'
      '  )'
      '  AND'
      '  T.DATETIME_OUT IS NOT NULL'
      '  AND'
      '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      'GROUP BY'
      '  T.PLANT_CODE,'
      '  T.SHIFT_NUMBER,'
      '  W.DEPARTMENT_CODE,'
      '  T.SHIFT_DATE,'
      '  TRUNC(T.DATETIME_IN)'
      'ORDER BY'
      '  1,'
      '  2,'
      '  3,'
      '  4'
      ''
      ''
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 40
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryFirstLastScans: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      
        'SELECT MIN(T.DATETIME_IN) DATETIME_IN, MAX(T.DATETIME_OUT) DATET' +
        'IME_OUT'
      'FROM TIMEREGSCANNING T INNER JOIN WORKSPOT W ON'
      '  T.PLANT_CODE = W.PLANT_CODE AND'
      '  T.WORKSPOT_CODE = W.WORKSPOT_CODE'
      'WHERE'
      '  ('
      '    ('
      '      T.DATETIME_IN >= :DATEFROM AND'
      '      T.DATETIME_IN < :DATETO'
      '    )'
      '    OR'
      '    ('
      '      T.DATETIME_OUT IS NOT NULL AND'
      '      (T.DATETIME_OUT >= :DATEFROM AND'
      '       T.DATETIME_OUT < :DATETO)'
      '     )'
      '  )'
      '  AND T.DATETIME_OUT IS NOT NULL'
      '  AND T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER'
      '  AND T.SHIFT_NUMBER = :SHIFT_NUMBER'
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' '
      ' ')
    Left = 40
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
