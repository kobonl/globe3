unit ReportMonthlyGroupEfficiencyQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRCtrls, QuickRpt, ExtCtrls,
  SystemDMT, UGlobalFunctions, UPimsConst, UScannedIDCard, Db, DBTables,
  ListProcsFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FDepartmentFrom, FDepartmentTo,
    FWorkspotFrom, FWorkspotTo: String;
    FYear, FMonth: Integer;
  public
    procedure SetValues(
      DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo: String;
      Year, Month: Integer);
  end;

  TReportMonthlyGroupEfficiencyQR = class(TReportBaseF)
    qryWork: TQuery;
    qryWorkPLANT_CODE: TStringField;
    qryWorkPLANTDESCRIPTION: TStringField;
    qryWorkWORKSPOT_CODE: TStringField;
    qryWorkWORKSPOTDESCRIPTION: TStringField;
    qryWorkEFFICIENCY: TFloatField;
    QRBandTitle: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel55: TQRLabel;
    QRLblDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLblDeptTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLblWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLblWKTo: TQRLabel;
    QRLabel3: TQRLabel;
    QRLblYear: TQRLabel;
    QRLabel4: TQRLabel;
    QRLblMonth: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRGrpHdPlant: TQRGroup;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText2Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    procedure FreeMemory; override;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo: String;
      const Year, Month: Integer): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
  end;

var
  ReportMonthlyGroupEfficiencyQR: TReportMonthlyGroupEfficiencyQR;

implementation

{$R *.DFM}

procedure TQRParameters.SetValues(
  DepartmentFrom, DepartmentTo,
  WorkspotFrom, WorkspotTo: String;
  Year, Month: Integer);
begin
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FWorkspotFrom := WorkspotFrom;
  FWorkspotTo := WorkspotTo;
  FYear := Year;
  FMonth := Month;
end;

{ TReportMonthlyGroupEfficiencyQR }

function TReportMonthlyGroupEfficiencyQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const DepartmentFrom, DepartmentTo, WorkspotFrom, WorkspotTo: String;
  const Year, Month: Integer): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo,
      Year, Month);
  end;
end;

function TReportMonthlyGroupEfficiencyQR.ExistsRecords: Boolean;
var
  WorkspotSelection: String;
begin
  Screen.Cursor := crHourGlass;

  WorkspotSelection := '';
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    WorkspotSelection :=
      'AND G.WORKSPOT_CODE >= :WORKSPOTFROM ' +
      'AND G.WORKSPOT_CODE <= :WORKSPOTTO ';

  qryWork.Close;
  qryWork.SQL.Clear;
  qryWork.SQL.Add(
    'SELECT ' +
    '  G.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION, ' +
    '  G.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' +
    '  G.EFFICIENCY ' +
    'FROM ' +
    '  GROUPEFFICIENCY G, PLANT P, WORKSPOT W ' +
    'WHERE ' +
    '  G.PLANT_CODE = P.PLANT_CODE ' +
    '  AND G.PLANT_CODE = W.PLANT_CODE ' +
    '  AND G.WORKSPOT_CODE = W.WORKSPOT_CODE ' +
    '  AND G.YEAR_NUMBER = :YEAR_NUMBER ' +
    '  AND G.MONTH_NUMBER = :MONTH_NUMBER ' +
    '  AND G.PLANT_CODE >= :PLANTFROM ' +
    '  AND G.PLANT_CODE <= :PLANTTO ' +
    WorkspotSelection + ' ' +
    'ORDER BY ' +
    '  G.PLANT_CODE, G.WORKSPOT_CODE '
    );
  qryWork.ParamByName('YEAR_NUMBER').AsInteger :=
    QRParameters.FYear;
  qryWork.ParamByName('MONTH_NUMBER').AsInteger :=
    QRParameters.FMonth;
  qryWork.ParamByName('PLANTFROM').AsString :=
    QRBaseParameters.FPlantFrom;
  qryWork.ParamByName('PLANTTO').AsString :=
    QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    qryWork.ParamByName('WORKSPOTFROM').AsString :=
      QRParameters.FWorkspotFrom;
    qryWork.ParamByName('WORKSPOTTO').AsString :=
      QRParameters.FWorkspotTo;
  end;
  qryWork.Open;
  Result := not qryWork.IsEmpty;

  Screen.Cursor := crDefault;
end;

procedure TReportMonthlyGroupEfficiencyQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLblDeptFrom.Caption := QRParameters.FDepartmentFrom;
  QRLblDeptTo.Caption := QRParameters.FDepartmentTo;
  QRLblWKFrom.Caption := QRParameters.FWorkspotFrom;
  QRLblWKTo.Caption := QRParameters.FWorkspotTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLblDeptFrom.Caption := '*';
    QRLblDeptTo.Caption := '*';
    QRLblWKFrom.Caption := '*';
    QRLblWKTo.Caption := '*';
  end;
  QRLblYear.Caption := IntToStr(QRParameters.FYear);
  QRLblMonth.Caption := IntToStr(QRParameters.FMonth);
end;

procedure TReportMonthlyGroupEfficiencyQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportMonthlyGroupEfficiencyQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
end;

procedure TReportMonthlyGroupEfficiencyQR.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qryWork.Close;
end;

procedure TReportMonthlyGroupEfficiencyQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;
end;

procedure TReportMonthlyGroupEfficiencyQR.QRDBText2Print(Sender: TObject;
  var Value: String);
begin
  inherited;
  try
    if Value <> '' then
      Value := Format('%.3f', [StrToFloat(Value)]);
  except
    Value := '0';
    Value := Format('%.3f', [StrToFloat(Value)]);
  end;
end;

end.
