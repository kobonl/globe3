unit ReportMonthlyEmployeeEfficiencyQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRCtrls, QuickRpt, ExtCtrls, Db, DBTables,
  SystemDMT, UGlobalFunctions, UPimsConst, UScannedIDCard,
  ListProcsFRM, QRExport, QRXMLSFilt, QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FDepartmentFrom, FDepartmentTo,
    FWorkspotFrom, FWorkspotTo: String;
    FYear, FMonth: Integer;
  public
    procedure SetValues(
      DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo: String;
      Year, Month: Integer);
  end;

  TReportMonthlyEmployeeEfficiencyQR = class(TReportBaseF)
    qryWork: TQuery;
    qryWorkPLANT_CODE: TStringField;
    qryWorkPLANTDESCRIPTION: TStringField;
    qryWorkWORKSPOT_CODE: TStringField;
    qryWorkWORKSPOTDESCRIPTION: TStringField;
    qryWorkEFFICIENCY: TFloatField;
    QRBandTitle: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel2: TQRLabel;
    QRLblPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLblPlantTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLblWKFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLblWKTo: TQRLabel;
    QRLabel3: TQRLabel;
    QRLblYear: TQRLabel;
    QRLabel4: TQRLabel;
    QRLblMonth: TQRLabel;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText2: TQRDBText;
    QRGrpHdPlant: TQRGroup;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel8: TQRLabel;
    QRGrpHdWorkspot: TQRGroup;
    QRDBText1: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel9: TQRLabel;
    QRGrpHdJob: TQRGroup;
    QRDBText6: TQRDBText;
    qryWorkJOB_CODE: TStringField;
    qryWorkJOBDESCRIPTION: TStringField;
    qryWorkEMPLOYEEDESCRIPTION: TStringField;
    QRDBText7: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    qryWorkEMPLOYEE_NUMBER: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBText2Print(sender: TObject; var Value: String);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    procedure FreeMemory; override;
    function QRSendReportParameters(
      const PlantFrom, PlantTo: String;
      const DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo: String;
      const Year, Month: Integer): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
  end;

var
  ReportMonthlyEmployeeEfficiencyQR: TReportMonthlyEmployeeEfficiencyQR;

implementation

{$R *.DFM}

// MR:29-01-2007 Order 550438 New Report Monthly Employee Efficiency,
//               based on Report Monthly Group Efficiency.

procedure TQRParameters.SetValues(
  DepartmentFrom, DepartmentTo,
  WorkspotFrom, WorkspotTo: String;
  Year, Month: Integer);
begin
  FDepartmentFrom := DepartmentFrom;
  FDepartmentTo := DepartmentTo;
  FWorkspotFrom := WorkspotFrom;
  FWorkspotTo := WorkspotTo;
  FYear := Year;
  FMonth := Month;
end;

{ TReportMonthlyEmployeeEfficiencyQR }

function TReportMonthlyEmployeeEfficiencyQR.QRSendReportParameters(
  const PlantFrom, PlantTo: String;
  const DepartmentFrom, DepartmentTo, WorkspotFrom, WorkspotTo: String;
  const Year, Month: Integer): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DepartmentFrom, DepartmentTo,
      WorkspotFrom, WorkspotTo,
      Year, Month);
  end;
end;

function TReportMonthlyEmployeeEfficiencyQR.ExistsRecords: Boolean;
var
  WorkspotSelection: String;
begin
  Screen.Cursor := crHourGlass;

  WorkspotSelection := '';
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
    WorkspotSelection :=
      'AND G.WORKSPOT_CODE >= :WORKSPOTFROM ' +
      'AND G.WORKSPOT_CODE <= :WORKSPOTTO ';

  qryWork.Close;
  qryWork.SQL.Clear;
  qryWork.SQL.Add(
    'SELECT ' + NL +
    '  G.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '  G.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '  G.JOB_CODE, J.DESCRIPTION AS JOBDESCRIPTION, ' + NL +
    '  CAST(G.EMPLOYEE_NUMBER AS INTEGER) AS EMPLOYEE_NUMBER, ' + NL +
    '  E.DESCRIPTION AS EMPLOYEEDESCRIPTION, ' + NL +
    '  G.EFFICIENCY ' + NL +
    'FROM ' + NL +
    '  EMPLOYEEEFFICIENCY G INNER JOIN PLANT P ON ' + NL +
    '    G.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    G.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    G.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  LEFT JOIN JOBCODE J ON ' + NL +
    '    G.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '    G.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    G.JOB_CODE = J.JOB_CODE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    G.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    'WHERE ' + NL +
    '  G.YEAR_NUMBER = :YEAR_NUMBER ' + NL +
    '  AND G.MONTH_NUMBER = :MONTH_NUMBER ' + NL +
    '  AND G.PLANT_CODE >= :PLANTFROM ' + NL +
    '  AND G.PLANT_CODE <= :PLANTTO ' + NL +
    WorkspotSelection + ' ' + NL +
    'UNION ' + NL +
    'SELECT ' + NL +
    '  G.PLANT_CODE, P.DESCRIPTION AS PLANTDESCRIPTION, ' + NL +
    '  G.WORKSPOT_CODE, W.DESCRIPTION AS WORKSPOTDESCRIPTION, ' + NL +
    '  ''-'' AS JOB_CODE, ''-'' AS JOBDESCRIPTION, ' + NL +
    '  CAST(0 AS INTEGER) AS EMPLOYEE_NUMBER, ' + NL +
    '  ''-'' AS EMPLOYEEDESCRIPTION, ' + NL +
    '  G.EFFICIENCY ' + NL +
    'FROM ' + NL +
    '  GROUPEFFICIENCY G, PLANT P, WORKSPOT W ' + NL +
    'WHERE ' + NL +
    '  G.PLANT_CODE = P.PLANT_CODE ' + NL +
    '  AND G.PLANT_CODE = W.PLANT_CODE ' + NL +
    '  AND G.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    '  AND G.YEAR_NUMBER = :YEAR_NUMBER ' + NL +
    '  AND G.MONTH_NUMBER = :MONTH_NUMBER ' + NL +
    '  AND G.PLANT_CODE >= :PLANTFROM ' + NL +
    '  AND G.PLANT_CODE <= :PLANTTO ' + NL +
    WorkspotSelection + ' ' + NL +
    'ORDER BY ' + NL +
    ' 1, 3, 5, 7'
    );
  qryWork.ParamByName('YEAR_NUMBER').AsInteger :=
    QRParameters.FYear;
  qryWork.ParamByName('MONTH_NUMBER').AsInteger :=
    QRParameters.FMonth;
  qryWork.ParamByName('PLANTFROM').AsString :=
    QRBaseParameters.FPlantFrom;
  qryWork.ParamByName('PLANTTO').AsString :=
    QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    qryWork.ParamByName('WORKSPOTFROM').AsString :=
      QRParameters.FWorkspotFrom;
    qryWork.ParamByName('WORKSPOTTO').AsString :=
      QRParameters.FWorkspotTo;
  end;
//qryWork.SQL.SaveToFile('c:\temp\ttt.sql');
  qryWork.Open;
  Result := not qryWork.IsEmpty;

  Screen.Cursor := crDefault;
end;

procedure TReportMonthlyEmployeeEfficiencyQR.ConfigReport;
begin
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';

  QRLblPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLblPlantTo.Caption := QRBaseParameters.FPlantTo;
  // MR:29-01-2007 Do not use department-selection
//  QRLblDeptFrom.Caption := QRParameters.FDepartmentFrom;
//  QRLblDeptTo.Caption := QRParameters.FDepartmentTo;
  QRLblWKFrom.Caption := QRParameters.FWorkspotFrom;
  QRLblWKTo.Caption := QRParameters.FWorkspotTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
//    QRLblDeptFrom.Caption := '*';
//    QRLblDeptTo.Caption := '*';
    QRLblWKFrom.Caption := '*';
    QRLblWKTo.Caption := '*';
  end;
  QRLblYear.Caption := IntToStr(QRParameters.FYear);
  QRLblMonth.Caption := IntToStr(QRParameters.FMonth);
end;

procedure TReportMonthlyEmployeeEfficiencyQR.FreeMemory;
begin
  inherited;
  FreeAndNil(FQRParameters);
end;

procedure TReportMonthlyEmployeeEfficiencyQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
end;

procedure TReportMonthlyEmployeeEfficiencyQR.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  qryWork.Close;
end;

procedure TReportMonthlyEmployeeEfficiencyQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := True;
end;

procedure TReportMonthlyEmployeeEfficiencyQR.QRDBText2Print(Sender: TObject;
  var Value: String);
begin
  inherited;
  try
    if Value <> '' then
      Value := Format('%.3f', [StrToFloat(Value)]);
  except
    Value := '0';
    Value := Format('%.3f', [StrToFloat(Value)]);
  end;
end;

end.
