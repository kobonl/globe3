(*
  SO: 19-07-2010 Order 550497
  MRA:24-AUG-2010. RV067.MRA.8. 550497.
  - Changed hardcoded strings to UPimsMessageRes.
  MRA:27-AUG-2010 RV067.MRA.15. Small change.
  - Changed text 'ADV' to 'Shorter Working Week' (also in grid as 'SWW').
  MRA:31-AUG-2010 RV067.MRA.21. Changes linked to order 550497.
  - Made Description mandatory.
  MRA:8-NOV-2010. RV077.5.
  - Only show 'Saturday Credit' when there is an export type 'ATTENT'.
  MRA:27-JAN-2011 RV085.15. Small change. SC-50016904.
  - Checkbox 'ADV' must only be visible when in plants
    there is a country set as 'BEL'.
    This must also be based for plants defined in teams-per-users.
  MRA:2-NOV-2012 20013723
  - Addition of 2 fields: EXPORT_OVERTIME_YN and EXPORT_PAYROLL_YN.
*)

unit TypeHourPerCountryFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, DBCtrls, dxEditor, dxEdLib, dxDBELib, Mask,
  dxDBTLCl, dxGrClms;

type
  TTypeHourPerCountryF = class(TGridBaseF)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    dxDBEditBonusPerc: TdxDBEdit;
    GroupBox2: TGroupBox;
    DBCheckBoxOverTime: TDBCheckBox;
    DBCheckBoxCountDays: TDBCheckBox;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridColumn;
    dxDetailGridColumn4: TdxDBGridColumn;
    dxDetailGridColumn5: TdxDBGridCheckColumn;
    dxDetailGridColumn6: TdxDBGridCheckColumn;
    dxDetailGridColumn7: TdxDBGridColumn;
    Label6: TLabel;
    dxDBEdit1: TdxDBEdit;
    DBCheckBox1: TDBCheckBox;
    dxDetailGridColumn8: TdxDBGridCheckColumn;
    DBCheckBox2: TDBCheckBox;
    DBCheckBoxTimeForTime: TDBCheckBox;
    DBCheckBoxBonusInMoney: TDBCheckBox;
    dxDetailGridColumnTimeForTime: TdxDBGridCheckColumn;
    dxDetailGridColumnBonusInMoney: TdxDBGridCheckColumn;
    DBCheckBoxADV: TDBCheckBox;
    DBCheckBoxSatCredit: TDBCheckBox;
    dxDetailGridColumnADV: TdxDBGridCheckColumn;
    dxDetailGridColumn12: TdxDBGridCheckColumn;
    dxMasterGridColumnCode: TdxDBGridColumn;
    dxMasterGridColumnDescription: TdxDBGridColumn;
    dxMasterGridColumnExportType: TdxDBGridColumn;
    DBEditTypeHours: TdxDBEdit;
    DBEditExportCode: TdxDBEdit;
    DBEditDescription: TdxDBEdit;
    DBCheckBoxTravelTime: TDBCheckBox;
    DBCheckBoxExportOvertimeADP: TDBCheckBox;
    DBCheckBoxExportPayrollADP: TDBCheckBox;
    dxDetailGridColumnExportOvertime: TdxDBGridCheckColumn;
    dxDetailGridColumnExportPayroll: TdxDBGridCheckColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBCheckBoxOverTimeClick(Sender: TObject);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dxBarBDBNavDeleteClick(Sender: TObject);
    procedure dxDetailGridEnter(Sender: TObject);
  private
    { Private declarations }
    FContractGroupTFT: Boolean;
    procedure TimeForTimeEnableCheck;
    procedure SelectRecords;
    procedure CheckboxesADPCheck;
  public
    { Public declarations }
    property ContractGroupTFT: Boolean read FContractGroupTFT
      write FContractGroupTFT;
  end;

function TypeHourPerCountryF: TTypeHourPerCountryF;

implementation

{$R *.DFM}

uses
  TypeHourPerCountryDMT, GlobalDMT, MultipleSelectFRM,
  UPimsMessageRes, SystemDMT;

var
  TypeHourPerCountryF_HND: TTypeHourPerCountryF;

function TypeHourPerCountryF: TTypeHourPerCountryF;
begin
  if (TypeHourPerCountryF_HND = nil) then
    TypeHourPerCountryF_HND := TTypeHourPerCountryF.Create(Application);
  Result := TypeHourPerCountryF_HND;
end;

procedure TTypeHourPerCountryF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditDescription.SetFocus;
end;

procedure TTypeHourPerCountryF.FormCreate(Sender: TObject);
begin
  // RV045.1. Bug! Datamodule was not assigned!
//  CreateFormDM(TTypeHourDM);
  TypeHourPerCountryDM := CreateFormDM(TTypeHourPerCountryDM);
  if (dxMasterGrid.DataSource = Nil) then
    dxMasterGrid.DataSource := TypeHourPerCountryDM.DataSourceMaster;
  if (dxDetailGrid.DataSource = Nil) then
    dxDetailGrid.DataSource := TypeHourPerCountryDM.DataSourceDetail;
  // RV077.5. Only show for export type Attent.
  DBCheckBoxSatCredit.Visible := SystemDM.IsAttent;
  dxDetailGridColumn12.Visible := SystemDM.IsAttent;
  DBCheckBoxADV.Visible := GlobalDM.PlantIsBelgium; // RV085.15.
  // 20013723
  dxDetailGridColumnExportOvertime.Visible := SystemDM.IsCLF;
  dxDetailGridColumnExportPayroll.Visible := SystemDM.IsCLF;
  DBCheckBoxExportOvertimeADP.Visible := SystemDM.IsCLF;
  DBCheckBoxExportPayrollADP.Visible := SystemDM.IsCLF;
  inherited;
  SetEditable;
  dxBarBDBNavInsert.Enabled := True;
end;

procedure TTypeHourPerCountryF.FormDestroy(Sender: TObject);
begin
  inherited;
  TypeHourPerCountryF_HND := Nil;
end;

procedure TTypeHourPerCountryF.FormShow(Sender: TObject);
begin
  inherited;
  // RV045.1. Check on Contract group
  ContractGroupTFT := False;
  if GlobalDM.ContractGroupTFTExists then
  begin
    ContractGroupTFT := True;
    DBCheckBoxTimeForTime.Enabled := False;
    DBCheckBoxBonusInMoney.Enabled := False;
  end;
  dxMasterGrid.SetFocus;
end;

// RV045.1.
procedure TTypeHourPerCountryF.TimeForTimeEnableCheck;
begin
(*  if ContractGroupTFT then
  begin
    DBCheckBoxTimeForTime.Enabled := False;
    DBCheckBoxBonusInMoney.Enabled := False;
    //RV067.4.
    DBCheckBoxADV.Enabled := False;
    dxDetailGridColumnTimeForTime.DisableEditor := True;
    dxDetailGridColumnBonusInMoney.DisableEditor := True;
    //RV067.4.
    dxDetailGridColumnADV.DisableEditor := True;
    dxDetailGridColumnTimeForTime.ReadOnly := True;
    dxDetailGridColumnBonusInMoney.ReadOnly := True;
    //RV067.4.
    dxDetailGridColumnADV.ReadOnly := True;
//    dxDetailGridColumnTimeForTime.Visible := False;
//    dxDetailGridColumnBonusInMoney.Visible := False;
  end
  else
  begin
    DBCheckBoxTimeForTime.Enabled := DBCheckBoxOverTime.Checked;
    DBCheckBoxBonusInMoney.Enabled := DBCheckBoxOverTime.Checked;
    //RV067.4.
    DBCheckBoxADV.Enabled := DBCheckBoxOverTime.Checked;
    dxDetailGridColumnTimeForTime.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnBonusInMoney.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    //RV067.4.
    dxDetailGridColumnADV.DisableEditor :=
      not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnTimeForTime.ReadOnly := not DBCheckBoxOverTime.Checked;
    dxDetailGridColumnBonusInMoney.ReadOnly := not DBCheckBoxOverTime.Checked;
    //RV067.4.
    dxDetailGridColumnADV.ReadOnly := not DBCheckBoxOverTime.Checked;
//    dxDetailGridColumnTimeForTime.Visible := True;
//    dxDetailGridColumnBonusInMoney.Visible := True;
  end;
*)
end;

procedure TTypeHourPerCountryF.DBCheckBoxOverTimeClick(Sender: TObject);
begin
  inherited;
  TimeForTimeEnableCheck; // RV045.1.
end;

// RV045.1.
procedure TTypeHourPerCountryF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  inherited;
  if AFocused or ASelected then
  begin
    // 20013723
    if AColumn = dxDetailGridColumn1 then
    begin
      CheckboxesADPCheck;
    end;
    if AColumn = dxDetailGridColumn5 then
    begin
      TimeForTimeEnableCheck;
    end;
  end;
end;

procedure TTypeHourPerCountryF.SelectRecords;
var
  frm: TDialogMultipleSelectF;
  CountryId: String;
begin
  frm := TDialogMultipleSelectF.Create(Application);
  try
    CountryId := TypeHourPerCountryDM.TableMaster.FieldByName('COUNTRY_ID').AsString;
    frm.Init(SPimsHourTypes,
      'select t.code || '' | '' || description from country t where t.country_id = ' + CountryId,
      //Available,
      'select t.hourtype_number code, t.description from hourtype t ' +
      'where t.hourtype_number not in (select hc.hourtype_number from hourtypepercountry hc where hc.country_id =  ' + CountryId + ') ' +
      'order by t.hourtype_number',
      //Selected,
      'select t.hourtype_number code, t.description from hourtype t ' +
      'where t.hourtype_number in (select hc.hourtype_number from hourtypepercountry hc where hc.country_id =  ' + CountryId + ') ' +
      'order by t.hourtype_number',
      //Insert
      ' insert into hourtypepercountry(country_id, creationdate, mutationdate, mutator, hourtype_number, description, export_code) ' +
      ' select ' + CountryId + ', sysdate, sysdate, ''' + SystemDM.CurrentProgramUser + ''', %0:s, description, export_code ' +
      ' from hourtype where hourtype_number = %0:s',
      //Delete
      'delete from hourtypepercountry hc where hc.country_id = ' + CountryId +
      ' and hc.hourtype_number = %s',
      //Exists
      ' select NVL(count(*), 0) from hourtypepercountry hc where hc.country_id = ' + CountryId +
      ' and hc.hourtype_number = %s'
      );
    if frm.ShowModal = mrOk then
      TypeHourPerCountryDM.TableDetail.Refresh;
  finally
    frm.Free();
  end;
end;

procedure TTypeHourPerCountryF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TTypeHourPerCountryF.dxBarBDBNavDeleteClick(Sender: TObject);
begin
  SelectRecords;
end;

procedure TTypeHourPerCountryF.dxDetailGridEnter(Sender: TObject);
begin
  inherited dxGridEnter(Sender);
  dxBarButtonEditMode.Visible := ivNever;
end;

// 20013723
procedure TTypeHourPerCountryF.CheckboxesADPCheck;
begin
  if SystemDM.IsCLF then
  begin
    DBCheckBoxExportOvertimeADP.Visible :=
      TypeHourPerCountryDM.TableMasterEXPORT_TYPE.Value = 'ADP';
    DBCheckBoxExportPayrollADP.Visible :=
      TypeHourPerCountryDM.TableMasterEXPORT_TYPE.Value = 'ADP';
  end;
end;

end.




