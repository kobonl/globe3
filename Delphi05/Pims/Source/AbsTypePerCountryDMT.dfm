inherited AbsTypePerCountryDM: TAbsTypePerCountryDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    TableName = 'COUNTRY'
    Left = 20
    Top = 8
    object TableMasterCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableMasterEXPORT_TYPE: TStringField
      FieldName = 'EXPORT_TYPE'
      Size = 6
    end
    object TableMasterCODE: TStringField
      FieldName = 'CODE'
      Required = True
      Size = 3
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
  end
  inherited TableDetail: TTable
    IndexName = 'XPKABSENCETYPEPERCOUNTRY'
    MasterFields = 'COUNTRY_ID'
    TableName = 'ABSENCETYPEPERCOUNTRY'
    Left = 28
    Top = 108
    object TableDetailCOUNTRY_ID: TIntegerField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TableDetailABSENCETYPE_CODE: TStringField
      FieldName = 'ABSENCETYPE_CODE'
      Required = True
      Size = 1
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 30
    end
    object TableDetailACTIVE_YN: TStringField
      FieldName = 'ACTIVE_YN'
      Required = True
      Size = 1
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
    object TableDetailCOUNTER_ACTIVE_YN: TStringField
      FieldName = 'COUNTER_ACTIVE_YN'
      Required = True
      Size = 1
    end
    object TableDetailCOUNTER_DESCRIPTION: TStringField
      FieldName = 'COUNTER_DESCRIPTION'
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 120
    Top = 8
  end
  inherited DataSourceDetail: TDataSource
    Left = 120
    Top = 108
  end
end
