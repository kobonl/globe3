(*
  MRA:1-JUN-2011. RV093.2.
  - It should be possible to enable/disable the
    embedded dialogs using Pims Settings Dialog.
  MRA:1-JUN-2011. RV093.3.
  - It should be possible to enable/disable the logging
    of changes using Pims Settings Dialog.
  - Also delete log-data, for clean-up-functionality.
  MRA:6-JUN-2011. RV093.4.
  - Use stored procedures to delete records, where a certain amount of records
    is deleted with commits in-between, instead of deleting them all at once.
  MRA:10-APR-2012. 20012858.
  - Addition of setting for EFF_BO_QUANT_YN: Eff. based on quantity or time.
  MRA:28-JUN-2012. 20012858.
  - Addition of setting for AutoCloseSecs for Personal Screen. This value
    is used in Personal Screen to autom. close the dialogs after x secs.
    - DB-Field: PS_AUTO_CLOSE_SECS
  MRA:4-FEB-2013 20013923
  - Make separate settings for update/delete for plant/employee/workspot
  MRA:11-MAR-2013 20014035 Hide ID optionally in Timerecording/Personal Screen
  - Addition of column named Hide ID. This is a checkbox.
    It sets the field HIDE_ID_YN to 'Y' or 'N' in table
    WORKSTATION.
  MRA:2-OCT-2013 TD-23247
  - Addition of workstation-column 'Cardreader Delay'. This is a numeric field.
    It can be used to set a delay that is used by Timerecording.
  MRA:6-FEB-2014 20011800 Final Run System
  - Addition of checkbox to turn Final Run System on or off.
  - Only a user of type administrator is allowed to change this setting.
  MRA:30-MAY-2014 20015178
  - Measure Performance without employees
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  MRA:18-JUN-2014 20015520
  - Productivity reports and selection on time
  MRA:18-JUN-2014 20015521
  - Productivity reports and include open scans
  MRA:14-JUL-2014 20015302
  - Addition of workstation-setting named MANREGS_MULTJOBS_YN to make
    the manual registrations of multiple jobs optional.
  MRA:31-OCT-2014 20011800.80
  - Disable final run system: Make setting about this invisible.
  MRA:10-NOV-2014 20014826
  - Log error messages optionally to database.
  MRA:16-MAR-2015 20015346
  - Addition of 2 Pims-Settings
    - TimerecordingInSecs
    - IgnoreBreaks
  MRA:7-APR-2015 SO-20016449
  - Time Zone Implementation
  MRA:6-MAY-2015 SO-20014450.50
  - Real Time Efficiency
  - Make 'Calculate efficiency based on quantity or time' invisible.
  - Always use 'based on time' as default.
  - Addition of 2 Pims-Settings for Personal Screen:
    - PSShowEmpInfo
    - PSShowEmpEff
  MRA:21-JUL-2015 PIM-50
  - Addition of job comments (system setting).
  MRA:14-SEP-2015 PIM-87
  - Addition of pims-setting UsePackageHourCalc (system setting).
  MRA:28-SEP-2015 PIM-12
  - Addition of system setings to optionally show Break, Lunch
    and End-Of-Day-buttons.
  - Disabled checkboxes for 'Date Time Selections' and 'Include open scans'.
  MRA:9-OCT-2015 PIM-90
  - Adapt ABSSolute Script Quantities to PIMS for Personal Screen
  - Added pimssetting named ExternQtyFromPQ (PIMSSETTING.EXTERN_QTY_FROM_PQ_YN)
  MRA:29-OCT-2015 PIM-12.2
  - Bugfix for Show Employee Eff. After unchecking this it became checked
    again later on.
  MRA:27-JAN-2016 PIM-139
  - Lunch button optionally scan out.
  MRA:26-FEB-2016 PIM-12
  - Made 2 settings invisible to prevent they can be changed, they should
    always be set to True for RealTimeEff! It is about settings (checkbox):
    - Time recording in seconds
    - Ignore breaks
  MRA:11-MAR-2016 PIM-150
  - Add option to show only default job in timerecording-application.
  MRA:28-SEP-2016 PIM-223
  - Show/hide employee efficiency on scheme-level
  - These settings must be disabled here (made invisible),
    because they will be used on scheme-level in Personal
    Screen.
  - The settings can be left in database.
  - Related to this: Also disable 'add job comments'-checkbox.
  MRA:30-SEP-2016 PIM-227
  - Make change of employee number optional.
  MRA:27-MAY-2018 GLOB3-284
  - Week start at Wednesday
  MRA:8-JUL-2019 GLOB3-330
  - Make functionality with 'occupied' optional
*)
unit SettingsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Buttons, Mask, ComCtrls, dxEditor,
  dxExEdtr, dxEdLib, dxDBELib, dxDBTLCl, dxGrClms, DBTables;

type
  TSettingsF = class(TGridBaseF)
    dxDetailGridColumnComputer: TdxDBGridColumn;
    dxDetailGridColumnLicense: TdxDBGridColumn;
    dxDetailGridColumnTimeRecording: TdxDBGridCheckColumn;
    dxDetailGridColumnShowHrsYN: TdxDBGridCheckColumn;
    AQuery: TQuery;
    pgControl: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    GroupBoxAutomatic: TGroupBox;
    LabelOlder: TLabel;
    LabelDetailed: TLabel;
    LabelProcess: TLabel;
    LabelWeeksD: TLabel;
    LabelWeeksP: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dxSpinEditEmployeeHours: TdxSpinEdit;
    dxSpinEditProductionQuantities: TdxSpinEdit;
    dxSpinEditTimerecordingScans: TdxSpinEdit;
    btnCleanProductionQuantities: TButton;
    btnCleanEmployeeHours: TButton;
    btnCleanTimerecordingScans: TButton;
    GroupBoxWeek: TGroupBox;
    RadioButtonMonday: TRadioButton;
    RadioButtonSunday: TRadioButton;
    RadioButtonSaturday: TRadioButton;
    RadioGroupFirstWeekOfYear: TRadioGroup;
    Panel1: TPanel;
    GroupBoxUnits: TGroupBox;
    LabelWeight: TLabel;
    LabelPieces: TLabel;
    EditWeight: TEdit;
    EditPieces: TEdit;
    GroupBox2: TGroupBox;
    CheckEditUpdateEmpl: TCheckBox;
    CheckBoxPlant: TCheckBox;
    CheckEditDeleteWK: TCheckBox;
    GroupBox3: TGroupBox;
    LabelDataBase: TLabel;
    CheckEditReadPlanningOnce: TCheckBox;
    BitBtnSetpassword: TBitBtn;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    CBoxPlantDelete: TCheckBox;
    CBoxEmpDelete: TCheckBox;
    CBoxWKDelete: TCheckBox;
    dxDetailGridColumnHideIDYN: TdxDBGridCheckColumn;
    dxDetailGridColumnCardReaderDelay: TdxDBGridSpinColumn;
    Panel5: TPanel;
    Panel6: TPanel;
    GroupBox4: TGroupBox;
    rgrpEffBOQuant: TRadioGroup;
    cboxEmbedDialogs: TCheckBox;
    TabSheet3: TTabSheet;
    Panel4: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    GroupBoxProdReports: TGroupBox;
    cBoxWorkspotInclSel: TCheckBox;
    cBoxDateTimeSel: TCheckBox;
    cBoxInclOpenScans: TCheckBox;
    dxDetailGridColumnManRegsMultJobs: TdxDBGridCheckColumn;
    Panel9: TPanel;
    gbABSLog: TGroupBox;
    cbABSLogEmpHrs: TCheckBox;
    cbABSLogTRS: TCheckBox;
    Panel10: TPanel;
    gbABSLogCleanup: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    dxSpinEditTRSLog: TdxSpinEdit;
    dxSpinEditEmpHrsLog: TdxSpinEdit;
    btnCleanTRSLog: TButton;
    btnCleanEmpHrsLog: TButton;
    gbErrorLog: TGroupBox;
    cBoxLogErrorsToDB: TCheckBox;
    dxDetailGridColumnPlantCode: TdxDBGridLookupColumn;
    GroupBox5: TGroupBox;
    cBoxFinalRun: TCheckBox;
    TabSheet4: TTabSheet;
    Panel11: TPanel;
    Panel12: TPanel;
    GroupBoxPersonalScreen: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    dxSpinEditPSAutoCloseSecs: TdxSpinEdit;
    cBoxEnableMachTimeRec: TCheckBox;
    cBoxPSShowEmpInfo: TCheckBox;
    cBoxPSShowEmpEff: TCheckBox;
    cBoxTimeRecInSecs: TCheckBox;
    cBoxIgnoreBreaks: TCheckBox;
    cBoxAddJobComments: TCheckBox;
    cBoxUsePackageHourCalc: TCheckBox;
    GroupBox6: TGroupBox;
    cBoxBreakBtnVisible: TCheckBox;
    cBoxLunchBtnVisible: TCheckBox;
    GroupBox7: TGroupBox;
    cBoxTREndOfDayBtnVisible: TCheckBox;
    GroupBox8: TGroupBox;
    cBoxPSEndOfDayBtnVisible: TCheckBox;
    GroupBox9: TGroupBox;
    cBoxMANREGEndOfDayBtnVisible: TCheckBox;
    GroupBox10: TGroupBox;
    rgrpReadExtDataFrom: TRadioGroup;
    GroupBoxLunchButton: TGroupBox;
    cBoxRegisterTime: TCheckBox;
    dxDetailGridColumnShowOnlyDefJobYN: TdxDBGridCheckColumn;
    GroupBox11: TGroupBox;
    cBoxAllowChgEmpNr: TCheckBox;
    RadioButtonWednesday: TRadioButton;
    rgrpTimeRecOccupied: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure BitBtnSetpasswordClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxSpinEditProductionQuantitiesChange(Sender: TObject);
    procedure dxSpinEditEmployeeHoursChange(Sender: TObject);
    procedure dxSpinEditTimerecordingScansChange(Sender: TObject);
    procedure btnCleanProductionQuantitiesClick(Sender: TObject);
    procedure btnCleanEmployeeHoursClick(Sender: TObject);
    procedure btnCleanTimerecordingScansClick(Sender: TObject);
    procedure btnCleanEmpHrsLogClick(Sender: TObject);
    procedure btnCleanTRSLogClick(Sender: TObject);
    procedure dxSpinEditPSAutoCloseSecsChange(Sender: TObject);
//    procedure cBoxPSShowEmpInfoClick(Sender: TObject); // PIM-223
    procedure cBoxLunchBtnVisibleClick(Sender: TObject);
  private
    { Private declarations }
{    function Cleanup(SQLString: String; DateFrom: TDateTime): Boolean; }
  public
    { Public declarations }
    procedure GetValues;
    procedure SetValues;
  end;

  function SettingsF: TSettingsF;

implementation

uses
  SystemDMT, UPimsConst, DialogPasswordFRM, UPimsMessageRes, SettingsDMT;
{$R *.DFM}
var
  SettingsF_HND : TSettingsF;

function SettingsF: TSettingsF;
begin
  if (SettingsF_HND = nil) then
  begin
    SettingsF_HND := TSettingsF.Create(Application);
  end;
  Result := SettingsF_HND;
end;

procedure TSettingsF.GetValues;
begin
//  dxSpinEditDetailed.Text:=IntToStr(SystemDm.DetailedProd);
  dxSpinEditProductionQuantities.Text:=IntToStr(SystemDm.ProcessDayQantity);
  EditWeight.Text:=SystemDm.UnitsWeigh;
  EditPieces.Text:=SystemDm.UnitsPieces;
 //CAR - 4-12-2003
  Case SystemDM.FirstWeekOfYear of
    1 : RadioGroupFirstWeekOfYear.ItemIndex := 0;
    4 : RadioGroupFirstWeekOfYear.ItemIndex := 1;
    7 : RadioGroupFirstWeekOfYear.ItemIndex := 2;
  End;
  Case SystemDM.WeekStartsOn of
    2 : RadioButtonMonday.Checked := True;
    4 : RadioButtonWednesday.Checked := True;
    7 : RadioButtonSaturday.Checked := True;
    1 : RadioButtonSunday.Checked := True;
  End;
  CheckEditReadPlanningOnce.Checked := YNBool(SystemDM.ReadPlanningOnceYN);
  // 20013923
  // Update
  CheckEditUpdateEmpl.Checked := YNBool(SystemDM.UpdateEmployeeYN);
  CheckBoxPlant.Checked := YNBool(SystemDM.UpdatePlantYN);
  CheckEditDeleteWK.Checked := YNBool(SystemDM.UpdateWKYN);
  // 20013923
  // Delete
  CBoxPlantDelete.Checked := YNBool(SystemDM.DeletePlantYN);
  CBoxEmpDelete.Checked := YNBool(SystemDM.DeleteEmployeeYN);
  CBoxWKDelete.Checked := YNBool(SystemDM.DeleteWKYN);
  // RV093.2.
  cboxEmbedDialogs.Checked := YNBool(SystemDM.EmbedDialogsYN);
  // RV093.3.
  cbABSLogEmpHrs.Checked := YNBool(SystemDM.ABSLOG_EmpHrsYN);
  cbABSLogTRS.Checked := YNBool(SystemDM.ABSLOG_TRSYN);
  // 20012858: 0=Quantity 1=Time
  // 20014450.50
{  if YNBool(SystemDM.EffBasedOnQuantYN) then
    rgrpEffBOQuant.ItemIndex := 0
  else }
    rgrpEffBOQuant.ItemIndex := 1;
  // 20012858.
  try
    dxSpinEditPSAutoCloseSecs.Text := IntToStr(SystemDM.PSAutoCloseSecs);
  except
    dxSpinEditPSAutoCloseSecs.Text := '20';
  end;
  // 20011800
  cBoxFinalRun.Checked := SystemDM.UseFinalRun;
  cBoxEnableMachTimeRec.Checked := SystemDM.EnableMachineTimeRec; // 20015178
  cBoxWorkspotInclSel.Checked := SystemDM.WorkspotInclSel; // 20015223
  cBoxDateTimeSel.Checked := SystemDM.DateTimeSel; // 20015220
  cBoxInclOpenScans.Checked := SystemDM.InclOpenScans; // 20015221
  cBoxLogErrorsToDB.Checked := SystemDM.LogToDB; // 20014826
  cBoxTimeRecInSecs.Checked := SystemDM.TimerecordingInSecs; // 20015346
  cBoxIgnoreBreaks.Checked := SystemDM.IgnoreBreaks; // 20015346
//  cBoxPSShowEmpInfo.Checked := SystemDM.PSShowEmpInfo; // 20014450.50 // PIM-223
//  cBoxPSShowEmpEff.Checked := SystemDM.PSShowEmpEff; // 20014450.50 // PIM-223
//  cBoxPSShowEmpInfoClick(cBoxPSShowEmpInfo); // 20014450.50 // PIM-223
//  cBoxAddJobComments.Checked := SystemDM.AddJobComment; // PIM-50 // PIM-223
  cBoxUsePackageHourCalc.Checked := SystemDM.UsePackageHourCalc; // PIM-87
  cBoxBreakBtnVisible.Checked := SystemDM.BreakBtnVisible; // PIM-12
  cBoxLunchBtnVisible.Checked := SystemDM.LunchBtnVisible; // PIM-12
  cBoxTREndOfDayBtnVisible.Checked := SystemDM.TREndOfDayBtnVisible; // PIM-12
  cBoxPSEndOfDayBtnVisible.Checked := SystemDM.PSEndOfDayBtnVisible; // PIM-12
  cBoxMANREGEndOfDayBtnVisible.Checked := SystemDM.MANREGEndOfDayBtnVisible; // PIM-12
  if SystemDM.ExternQtyFromPQ then // PIM-90
    rgrpReadExtDataFrom.ItemIndex := 0
  else
    rgrpReadExtDataFrom.ItemIndex := 1;
  cBoxRegisterTime.Checked := not SystemDM.LunchBtnScanOut; // PIM-139
  cBoxAllowChgEmpNr.Checked := SystemDM.UpdateEmpNr; // PIM-227
  rgrpTimeRecOccupied.ItemIndex := SystemDM.Occup_Handling; // GLOB3-330
end;

procedure TSettingsF.SetValues;
begin
//  SystemDm.DetailedProd := StrToInt(dxSpinEditDetailed.text);
  SystemDm.ProcessDayQantity:= StrToInt(dxSpinEditProductionQuantities.text);
  //Car 20.2.2004 - remove leading and trailling spaces
  SystemDm.UnitsWeigh := Trim(EditWeight.text);
  SystemDm.UnitsPieces := Trim(EditPieces.text);
  if RadioButtonMonday.Checked then
    SystemDm.WeekStartsOn := 2;
  if RadioButtonWednesday.Checked then
    SystemDm.WeekStartsOn := 4;
  if RadioButtonSunday.Checked then
    SystemDm.WeekStartsOn := 1;
  if RadioButtonSaturday.Checked then
    SystemDm.WeekStartsOn := 7;
  // CAR 4-12-2003
  if RadioGroupFirstWeekOfYear.ItemIndex = 0 then
    SystemDm.FirstWeekOfYear := 1;
  if RadioGroupFirstWeekOfYear.ItemIndex = 1 then
    SystemDm.FirstWeekOfYear := 4;
  if RadioGroupFirstWeekOfYear.ItemIndex = 2 then
    SystemDm.FirstWeekOfYear := 7;

  SystemDM.ReadPlanningOnceYN := BoolYN(CheckEditReadPlanningOnce.Checked);
 //CAR 21-8-2003
  // 20013923
  // Update
  SystemDM.UpdateWKYN := BoolYN(CheckEditDeleteWK.Checked);
  SystemDM.UpdateEmployeeYN := BoolYN(CheckEditUpdateEmpl.Checked);
  SystemDM.UpdatePlantYN := BoolYN(CheckBoxPlant.Checked);
  // 20013923
  // Delete
  SystemDM.DeletePlantYN := BoolYN(CBoxPlantDelete.Checked);
  SystemDM.DeleteEmployeeYN := BoolYN(CBoxEmpDelete.Checked);
  SystemDM.DeleteWKYN := BoolYN(CBoxWKDelete.Checked);
  if not SystemDM.TableWorkStation.Active then
    SystemDM.TableWorkStation.Open;
  if SystemDM.TableWorkStation.FindKey([SystemDM.CurrentComputerName]) then
    SystemDM.TimeRecordingStation :=
      SystemDM.TableWorkStation.FieldByName('TIMERECORDING_YN').AsString;
  // RV093.2.
  SystemDM.EmbedDialogsYN := BoolYN(cboxEmbedDialogs.Checked);
  // RV093.3. Changed? Then give a message, because a restart of Pims is needed.
  if (SystemDM.ABSLOG_EmpHrsYN <> BoolYN(cbABSLogEmpHrs.Checked)) or
    (SystemDM.ABSLOG_TRSYN <> BoolYN(cbABSLogTRS.Checked)) then
     DisplayMessage(SPimsLoggingEnable, mtInformation, [mbOk]);
  SystemDM.ABSLOG_EmpHrsYN := BoolYN(cbABSLogEmpHrs.Checked);
  SystemDM.ABSLOG_TRSYN := BoolYN(cbABSLogTRS.Checked);
  // 20012858: Eff based on Y=Quantity or N=Time
  if rgrpEffBOQuant.ItemIndex = 0 then
    SystemDM.EffBasedOnQuantYN := 'Y'
  else
    SystemDM.EffBasedOnQuantYN := 'N';
  // 20012858.
  try
    SystemDM.PSAutoCloseSecs := StrToInt(dxSpinEditPSAutoCloseSecs.Text);
  except
    SystemDM.PSAutoCloseSecs := 20;
  end;
  SystemDM.UseFinalRun := cBoxFinalRun.Checked; // 20011800
  SystemDM.EnableMachineTimeRec := cBoxEnableMachTimeRec.Checked; // 20015178
  SystemDM.WorkspotInclSel := cBoxWorkspotInclSel.Checked; // 20015223
  SystemDM.DateTimeSel := cBoxDateTimeSel.Checked; // 20015220
  SystemDM.InclOpenScans := cBoxInclOpenScans.Checked; // 20015221
  SystemDM.LogToDB := cBoxLogErrorsToDB.Checked; // 20014826
  SystemDM.TimerecordingInSecs := cBoxTimeRecInSecs.Checked; // 20015346
  SystemDM.IgnoreBreaks := cBoxIgnoreBreaks.Checked; // 20015346
//  SystemDM.PSShowEmpInfo := cBoxPSShowEmpInfo.Checked; // 20014450.50 // PIM-223
//  SystemDM.PSShowEmpEff := cBoxPSShowEmpEff.Checked; // 20014450.50 // PIM-223
//  SystemDM.AddJobComment := cBoxAddJobComments.Checked; // PIM-50 // PIM-223
  SystemDM.UsePackageHourCalc := cBoxUsePackageHourCalc.Checked; // PIM-87
  SystemDM.BreakBtnVisible := cBoxBreakBtnVisible.Checked; // PIM-12
  SystemDM.LunchBtnVisible := cBoxLunchBtnVisible.Checked; // PIM-12
  SystemDM.TREndOfDayBtnVisible := cBoxTREndOfDayBtnVisible.Checked; // PIM-12
  SystemDM.PSEndOfDayBtnVisible := cBoxPSEndOfDayBtnVisible.Checked; // PIM-12
  SystemDM.MANREGEndOfDayBtnVisible := cBoxMANREGEndOfDayBtnVisible.Checked; // PIM-12
  SystemDM.ExternQtyFromPQ := rgrpReadExtDataFrom.ItemIndex = 0; // PIM-90
  SystemDM.LunchBtnScanOut := not cBoxRegisterTime.Checked; // PIM-139
  SystemDM.UpdateEmpNr := cBoxAllowChgEmpNr.Checked; // PIM-227
  SystemDM.Occup_Handling := rgrpTimeRecOccupied.ItemIndex; // GLOB3-330

  SystemDM.SetSettings;
end;

procedure TSettingsF.FormShow(Sender: TObject);
var
  Pims_DB, Pims_Func_DB: String;
  I: Integer;
begin
  inherited;
  GetValues;
  SystemDM.GetPimsDBVersion(SystemDM.QueryUpdatePims, Pims_DB, Pims_Func_DB);;
  LabelDataBase.Caption := LabelDataBase.Caption + ' ' + Pims_DB;
  SettingsF.Realign;
  // 20015178 Related to this order, this will only disable the contents of
  // tabsheets, but tabsheets can still be opened.
  if not pnlDetail.Enabled then
  begin
    pnlDetail.Enabled := True;
    // This will disable all tabsheets.
    for I := 0 to pgControl.PageCount - 1 do
      pgControl.Pages[I].Enabled := False;
  end;
  cBoxLunchBtnVisibleClick(Sender);
end;

procedure TSettingsF.BitBtnSetpasswordClick(Sender: TObject);
begin
  inherited;
  try
    DialogPasswordF:=TDialogPasswordF.Create(Application);
    if not DialogPasswordF.ChangePassword then
      DisplayMessage(SPimsPasswordNotChanged, mtInformation, [mbOk]);
  finally
    DialogPasswordF.free;
  end;
end;

procedure TSettingsF.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetValues;
  inherited;
end;

procedure TSettingsF.FormCreate(Sender: TObject);
begin
  SettingsDM := CreateFormDM(TSettingsDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := SettingsDM.DataSourceDetail;
  inherited;
  Caption := Caption + ' ' + SystemDm.CurrentComputerName;
  dxDetailGridColumnTimeRecording.ValueChecked := CHECKEDVALUE;
  dxDetailGridColumnTimeRecording.ValueUnchecked := UNCHECKEDVALUE;
  pgControl.ActivePageIndex := 0;
  // 20011800 Only an administrator can change this setting!
  cBoxFinalRun.Enabled := SystemDM.IsAdmin;
  // 20011800.80 Disable this functionality
  GroupBox5.Visible := False;
  // 20016449 Optionally disable this
  if SystemDM.DatabaseTimeZone = '' then
  begin
    dxDetailGridColumnPlantCode.Visible := False;
    dxDetailGridColumnPlantCode.DisableGrouping := True;
    dxDetailGridColumnPlantCode.DisableCustomizing := True;
  end;
  // 20014450.50
  rgrpEffBOQuant.Visible := False;
  // GLOB3-330
  GroupBoxProdReports.Visible := not (SystemDM.HybridMode = hmHybrid);
  rgrpTimeRecOccupied.Visible := (SystemDM.HybridMode = hmHybrid);
end;

procedure TSettingsF.FormDestroy(Sender: TObject);
begin
  inherited;
  SettingsF_HND := nil;
end;

procedure TSettingsF.dxSpinEditProductionQuantitiesChange(Sender: TObject);
begin
  inherited;
  try
  except
    dxSpinEditProductionQuantities.IntValue := 0;
  end;
end;

procedure TSettingsF.dxSpinEditEmployeeHoursChange(Sender: TObject);
begin
  inherited;
  try
  except
    dxSpinEditEmployeeHours.IntValue := 0;
  end;
end;

procedure TSettingsF.dxSpinEditTimerecordingScansChange(Sender: TObject);
begin
  inherited;
  try
  except
    dxSpinEditTimerecordingScans.IntValue := 0;
  end;
end;

{
// MR:03-05-2004
function TSettingsF.Cleanup(SQLString: String; DateFrom: TDateTime): Boolean;
begin
  Result := True;
  AQuery.Close;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(SQLString);
  AQuery.ParamByName('DATEFROM').AsDateTime := DateFrom;
  try
    AQuery.ExecSql;
  except
    on E: EDatabaseError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      Result := False;
    end;
  end;
end;
}

// MR:03-05-2004
procedure TSettingsF.btnCleanProductionQuantitiesClick(Sender: TObject);
var
  Weeks: Integer;
//  SQLString: String;
  DateFrom: TDateTime;
begin
  inherited;
  // Clean up ProductionQuantities
  Weeks := dxSpinEditProductionQuantities.IntValue;
  if (Weeks < 0) then
    Weeks := 0;
  if (DisplayMessage(
    Format(SPimsCleanUpProductionQuantities, [Weeks]),
      mtConfirmation, [mbYes, mbNo]) = mrYes) then
  begin
    DateFrom := Now - (Weeks * 7);
    SettingsDM.DateFrom := DateFrom;
    if SettingsDM.Cleanup_PQ then
      DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
{
    SQLString :=
      'DELETE FROM PRODUCTIONQUANTITY ' +
      'WHERE START_DATE < :DATEFROM';
    if Cleanup(SQLString, DateFrom) then
      DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
}
  end;
end;

// MR:03-05-2004
procedure TSettingsF.btnCleanEmployeeHoursClick(Sender: TObject);
var
  Weeks: Integer;
//  SQLString: String;
  DateFrom: TDateTime;
begin
  inherited;
  // RV093.3. First disable the logging!
  SystemDM.ABSLogEmpHrsEnable(False);
  try
    // Clean up EmployeeHours
    Weeks := dxSpinEditEmployeeHours.IntValue;
    if (Weeks < 0) then
      Weeks := 0;
    if (DisplayMessage(
      Format(SPimsCleanUpEmployeeHours, [Weeks]),
        mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      DateFrom := Now - (Weeks * 7);
      SettingsDM.DateFrom := DateFrom;
      if SettingsDM.Cleanup_PHE then
        if SettingsDM.Cleanup_SHE then
          if SettingsDM.Cleanup_PHEPT then
            DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
{
      SQLString :=
        'DELETE FROM PRODHOURPEREMPLOYEE ' +
        'WHERE PRODHOUREMPLOYEE_DATE < :DATEFROM';
      if Cleanup(SQLString, DateFrom) then
      begin
        SQLString :=
          'DELETE FROM SALARYHOURPEREMPLOYEE ' +
          'WHERE SALARY_DATE < :DATEFROM';
        if Cleanup(SQLString, DateFrom) then
        begin
          SQLString :=
            'DELETE FROM PRODHOURPEREMPLPERTYPE ' +
            'WHERE PRODHOUREMPLOYEE_DATE < :DATEFROM';
          if Cleanup(SQLString, DateFrom) then
            DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
        end;
      end;
}
    end;
  finally
    // RV093.3. Enable logging again, if needed.
    if SystemDM.ABSLOG_EmpHrsYN = CHECKEDVALUE then
      SystemDM.ABSLogEmpHrsEnable(True);
  end;
end;

// MR:03-05-2004
procedure TSettingsF.btnCleanTimerecordingScansClick(Sender: TObject);
var
  Weeks: Integer;
//  SQLString: String;
  DateFrom: TDateTime;
begin
  inherited;
  // RV093.3. First disable the logging!
  SystemDM.ABSLogTRSEnable(False);
  try
    // Clean up TimerecordingScans
    Weeks := dxSpinEditTimerecordingScans.IntValue;
    if (Weeks < 0) then
      Weeks := 0;
    if (DisplayMessage(
      Format(SPimsCleanUpTimerecordingScans, [Weeks]),
        mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      DateFrom := Now - (Weeks * 7);
      SettingsDM.DateFrom := DateFrom;
      if SettingsDM.Cleanup_TRS then
        DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
{
      SQLString :=
        'DELETE FROM TIMEREGSCANNING ' +
        'WHERE DATETIME_IN < :DATEFROM';
      if Cleanup(SQLString, DateFrom) then
        DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
}
    end;
  finally
    // RV093.3. Enable logging again, if needed.
    if SystemDM.ABSLOG_TRSYN = CHECKEDVALUE then
      SystemDM.ABSLogTRSEnable(True);
  end;
end;

// RV093.3. Clean up Emp Hrs Logging.
procedure TSettingsF.btnCleanEmpHrsLogClick(Sender: TObject);
var
  Weeks: Integer;
//  SQLString: String;
  DateFrom: TDateTime;
begin
  inherited;
  // RV093.3. First disable the logging!
  SystemDM.ABSLogTRSEnable(False);
  try
    // Clean up Employee hours logging
    Weeks := dxSpinEditEmpHrsLog.IntValue;
    if (Weeks < 0) then
      Weeks := 0;
    if (DisplayMessage(
      Format(SPimsCleanUpEmpHrsLog, [Weeks]),
        mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      DateFrom := Now - (Weeks * 7);
      SettingsDM.DateFrom := DateFrom;
      if SettingsDM.Cleanup_ABSLOGAHE then
        if SettingsDM.Cleanup_ABSLOGPHE then
          if SettingsDM.Cleanup_ABSLOGPHEPT then
            if SettingsDM.Cleanup_ABSLOGSHE then
              DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
{
      SQLString :=
        'DELETE FROM ABSLOGAHE ' +
        'WHERE ABSLOG_TIMESTAMP < :DATEFROM';
      if Cleanup(SQLString, DateFrom) then
      begin
        SQLString :=
          'DELETE FROM ABSLOGPHE ' +
          'WHERE ABSLOG_TIMESTAMP < :DATEFROM';
        if Cleanup(SQLString, DateFrom) then
        begin
          SQLString :=
            'DELETE FROM ABSLOGPHEPT ' +
            'WHERE ABSLOG_TIMESTAMP < :DATEFROM';
          if Cleanup(SQLString, DateFrom) then
          begin
            SQLString :=
              'DELETE FROM ABSLOGSHE ' +
              'WHERE ABSLOG_TIMESTAMP < :DATEFROM';
            if Cleanup(SQLString, DateFrom) then
              DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
          end;
        end;
      end;
}
    end;
  finally
    // RV093.3. Enable logging again, if needed.
    if SystemDM.ABSLOG_TRSYN = CHECKEDVALUE then
      SystemDM.ABSLogTRSEnable(True);
  end;
end;

// RV093.3. Clean up TRS logging.
procedure TSettingsF.btnCleanTRSLogClick(Sender: TObject);
var
  Weeks: Integer;
//  SQLString: String;
  DateFrom: TDateTime;
begin
  inherited;
  // RV093.3. First disable the logging!
  SystemDM.ABSLogTRSEnable(False);
  try
    // Clean up TimerecordingScans Logs
    Weeks := dxSpinEditTRSLog.IntValue;
    if (Weeks < 0) then
      Weeks := 0;
    if (DisplayMessage(
      Format(SPimsCleanUpTRSLog, [Weeks]),
        mtConfirmation, [mbYes, mbNo]) = mrYes) then
    begin
      DateFrom := Now - (Weeks * 7);
      SettingsDM.DateFrom := DateFrom;
      if SettingsDM.Cleanup_ABSLOGTRS then
        DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
{
      SQLString :=
        'DELETE FROM ABSLOGTRS ' +
        'WHERE ABSLOG_TIMESTAMP < :DATEFROM';
      if Cleanup(SQLString, DateFrom) then
        DisplayMessage(SSProcessFinished, mtInformation, [mbOK]);
}
    end;
  finally
    // RV093.3. Enable logging again, if needed.
    if SystemDM.ABSLOG_TRSYN = CHECKEDVALUE then
      SystemDM.ABSLogTRSEnable(True);
  end;
end;

procedure TSettingsF.dxSpinEditPSAutoCloseSecsChange(Sender: TObject);
begin
  inherited;
  try
  except
    dxSpinEditPSAutoCloseSecs.IntValue := 0;
  end;
end;

// PIM-223
(*
// 20014450.50
procedure TSettingsF.cBoxPSShowEmpInfoClick(Sender: TObject);
begin
  inherited;
  if cBoxPSShowEmpInfo.Checked then
  begin
//    cBoxPSShowEmpEff.Checked := True; // PIM-12.2
    cBoxPSShowEmpEff.Enabled := True;
  end
  else
  begin
    cBoxPSShowEmpEff.Checked := False;
    cBoxPSShowEmpEff.Enabled := False;
  end;
end;
*)

procedure TSettingsF.cBoxLunchBtnVisibleClick(Sender: TObject);
begin
  inherited;
  cBoxRegisterTime.Enabled := cBoxLunchBtnVisible.Checked; // PIM-139
end;

end.
