inherited DialogBankHolidayF: TDialogBankHolidayF
  Left = 261
  Top = 197
  Caption = 'Bank Holiday'
  ClientHeight = 528
  ClientWidth = 787
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Top = 26
    Width = 787
    TabOrder = 1
    inherited imgOrbit: TImage
      Left = 670
    end
  end
  inherited pnlInsertBase: TPanel
    Top = 63
    Width = 787
    Height = 386
    Caption = ''
    TabOrder = 4
    object Label5: TLabel
      Left = 2
      Top = 40
      Width = 46
      Height = 13
      Caption = 'January'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 133
      Top = 40
      Width = 51
      Height = 13
      Caption = 'February'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 264
      Top = 40
      Width = 35
      Height = 13
      Caption = 'March'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 395
      Top = 40
      Width = 26
      Height = 13
      Caption = 'April'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 526
      Top = 40
      Width = 24
      Height = 13
      Caption = 'May'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 657
      Top = 40
      Width = 27
      Height = 13
      Caption = 'June'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 2
      Top = 176
      Width = 23
      Height = 13
      Caption = 'July'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 133
      Top = 176
      Width = 40
      Height = 13
      Caption = 'August'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 264
      Top = 176
      Width = 63
      Height = 13
      Caption = 'September'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 395
      Top = 176
      Width = 46
      Height = 13
      Caption = 'Oktober'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 526
      Top = 176
      Width = 58
      Height = 13
      Caption = 'November'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 657
      Top = 176
      Width = 58
      Height = 13
      Caption = 'December'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 785
      Height = 41
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Year'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object LabelCountry: TLabel
        Left = 152
        Top = 16
        Width = 47
        Height = 13
        Caption = 'Count(r)y'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 72
        Top = 12
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnChange = dxSpinEditYearChange
        MaxValue = 2099
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
      object ComboBoxCountry: TComboBox
        Left = 211
        Top = 12
        Width = 150
        Height = 21
        DropDownCount = 6
        ItemHeight = 13
        TabOrder = 1
        OnChange = ComboBoxCountryChange
      end
    end
    object GroupBoxDetailBank: TGroupBox
      Left = 0
      Top = 312
      Width = 785
      Height = 73
      Caption = 'Bank holiday'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object Label2: TLabel
        Left = 8
        Top = 16
        Width = 23
        Height = 13
        Caption = 'Date'
      end
      object Label3: TLabel
        Left = 8
        Top = 42
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object Label4: TLabel
        Left = 400
        Top = 42
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'Absence reason'
      end
      object Label17: TLabel
        Left = 315
        Top = 16
        Width = 161
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hourtype worked on bank holiday'
      end
      object EditDescBK: TEdit
        Tag = 1
        Left = 72
        Top = 40
        Width = 313
        Height = 21
        TabOrder = 1
        Text = 'EditDescBK'
        OnChange = EditDescBKChange
      end
      object ComboBoxAbsRsn: TComboBox
        Tag = 1
        Left = 483
        Top = 40
        Width = 230
        Height = 21
        DropDownCount = 6
        ItemHeight = 13
        TabOrder = 0
        OnChange = ComboBoxAbsRsnChange
      end
      object ComboBoxHTWOBH: TComboBox
        Left = 483
        Top = 13
        Width = 230
        Height = 21
        DropDownCount = 6
        ItemHeight = 13
        TabOrder = 2
        OnChange = ComboBoxHTWOBHChange
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 509
    Width = 787
  end
  inherited pnlBottom: TPanel
    Top = 449
    Width = 787
    Height = 60
    inherited btnOk: TBitBtn
      Left = 16
      Top = 6
      Width = 257
      Caption = 'Process whole year to employee availability'
      ModalResult = 0
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 578
      Top = 6
      Width = 193
    end
    object btnSelectedDateOK: TBitBtn
      Left = 283
      Top = 6
      Width = 284
      Height = 25
      Caption = 'Process selected date to employee availability'
      Default = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btnSelectedDateOKClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000012170000121700000000000000000000C89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900FEFEFEC89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900FEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEFEFEFEC89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC0C0C0C0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEC89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0
        C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEC89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFE
        FEC89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFE
        FEFEFEFEFEFEFEC89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
    object btnUpdateCounterBankHolidays: TBitBtn
      Left = 16
      Top = 33
      Width = 257
      Height = 25
      Caption = 'Update Counter Bank Holidays'
      Default = True
      TabOrder = 3
      OnClick = btnUpdateCounterBankHolidaysClick
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        1800000000000006000012170000121700000000000000000000C89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900FEFEFEC89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900FEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0
        C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEFEFEFEC89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC0C0C0C0
        C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900FEFEFE
        FEFEFEFEFEFEC89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC89900C899
        00C89900C89900C89900FFFFFFC0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC0
        C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        FEFEFEC89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFEFEC899
        00C89900C89900C89900FFFFFFFFFFFFC0C0C0FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFEFEFEFE
        FEC89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFEFEFEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFEFEFEFE
        FEFEFEFEFEFEFEC89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0C0C0C0C0C0C0FFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900FEFE
        FEFEFEFEC89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFC0C0C0C0C0C0FFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC89900C89900
        C89900C89900C89900C89900C89900C89900C89900C89900C89900C89900C899
        00C89900C89900C89900FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      NumGlyphs = 2
    end
  end
  object DateBank: TDateTimePicker [5]
    Left = 72
    Top = 388
    Width = 113
    Height = 21
    CalAlignment = dtaLeft
    Date = 0.562410405102128
    Time = 0.562410405102128
    DateFormat = dfShort
    DateMode = dmComboBox
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Kind = dtkDate
    ParseInput = False
    ParentFont = False
    TabOrder = 0
    OnClick = DateBankClick
    OnChange = DateBankChange
  end
  object StringGrid1: TStringGrid [6]
    Tag = 1
    Left = 2
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedColor = clScrollBar
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 8
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid2: TStringGrid [7]
    Tag = 2
    Left = 133
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedColor = clActiveBorder
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 9
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    ColWidths = (
      18
      18
      18
      18
      18
      18
      18)
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid3: TStringGrid [8]
    Tag = 3
    Left = 264
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 10
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    ColWidths = (
      18
      18
      18
      18
      18
      18
      18)
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid4: TStringGrid [9]
    Tag = 4
    Left = 395
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 11
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid5: TStringGrid [10]
    Tag = 5
    Left = 526
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 12
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid6: TStringGrid [11]
    Tag = 6
    Left = 657
    Top = 119
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 13
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid7: TStringGrid [12]
    Tag = 7
    Left = 2
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 14
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid8: TStringGrid [13]
    Tag = 8
    Left = 133
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 15
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid9: TStringGrid [14]
    Tag = 9
    Left = 264
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 16
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid10: TStringGrid [15]
    Tag = 10
    Left = 395
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ParentShowHint = False
    ScrollBars = ssNone
    ShowHint = False
    TabOrder = 17
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid11: TStringGrid [16]
    Tag = 11
    Left = 526
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 18
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  object StringGrid12: TStringGrid [17]
    Tag = 12
    Left = 657
    Top = 255
    Width = 130
    Height = 114
    Color = clScrollBar
    ColCount = 7
    DefaultColWidth = 18
    DefaultRowHeight = 15
    FixedCols = 0
    RowCount = 7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [goFixedHorzLine, goRangeSelect, goDrawFocusSelected]
    ParentFont = False
    ScrollBars = ssNone
    TabOrder = 19
    OnClick = StringGrid1Click
    OnDrawCell = StringGridDrawCell
    RowHeights = (
      15
      15
      14
      15
      15
      15
      15)
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 696
    Top = 24
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavInsert: TdxBarDBNavButton
      OnClick = dxBarBDBNavInsertClick
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    DataSource = DataSourceBK
    Left = 656
    Top = 24
  end
  inherited StandardMenuActionList: TActionList
    Left = 608
    Top = 24
  end
  object TableBK: TTable
    BeforePost = TableBKBeforePost
    AfterPost = TableBKAfterPost
    BeforeDelete = TableBKBeforeDelete
    AfterDelete = TableBKAfterDelete
    OnDeleteError = TableBKDeleteError
    OnEditError = TableBKEditError
    OnPostError = TableBKPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'BANK_HOLIDAY_DATE'
    TableName = 'BANKHOLIDAY'
    Left = 432
    Top = 26
    object TableBKBANK_HOLIDAY_DATE: TDateTimeField
      FieldName = 'BANK_HOLIDAY_DATE'
    end
    object TableBKDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = TableBKDESCRIPTIONValidate
      Size = 30
    end
    object TableBKABSENCEREASON_CODE: TStringField
      FieldName = 'ABSENCEREASON_CODE'
      Required = True
      OnValidate = TableBKABSENCEREASON_CODEValidate
      Size = 1
    end
    object TableBKCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableBKMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableBKMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableBKABSENCELU: TStringField
      FieldKind = fkLookup
      FieldName = 'ABSENCELU'
      LookupDataSet = TableAbsReason_
      LookupKeyFields = 'ABSENCEREASON_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'ABSENCEREASON_CODE'
      Size = 30
      Lookup = True
    end
    object TableBKCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
    end
    object TableBKHOURTYPE_WRK_ON_BANKHOL: TIntegerField
      FieldName = 'HOURTYPE_WRK_ON_BANKHOL'
    end
  end
  object DataSourceBK: TDataSource
    DataSet = TableBK
    Left = 512
    Top = 26
  end
  object TableAbsReason_: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'ABSENCEREASON_CODE'
    MasterFields = 'ABSENCEREASON_CODE'
    TableName = 'ABSENCEREASON'
    Left = 392
    Top = 26
  end
  object DataSourceAbs: TDataSource
    DataSet = TableAbsReason_
    Left = 344
    Top = 26
  end
  object QueryProcess: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourceQryBK
    SQL.Strings = (
      'UPDATE EMPLOYEEAVAILABILITY '
      'SET '
      
        '  EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_1 = :ABSENCEREASON_CO' +
        'DE, '
      
        '  EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_2 = :ABSENCEREASON_CO' +
        'DE, '
      
        '  EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_3 =  :ABSENCEREASON_C' +
        'ODE, '
      
        '  EMPLOYEEAVAILABILITY.AVAILABLE_TIMEBLOCK_4 = :ABSENCEREASON_CO' +
        'DE '
      'WHERE '
      
        '  EMPLOYEEAVAILABILITY.EMPLOYEEAVAILABILITY_DATE = :BANK_HOLIDAY' +
        '_DATE')
    Left = 192
    Top = 26
    ParamData = <
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCEREASON_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'BANK_HOLIDAY_DATE'
        ParamType = ptUnknown
      end>
  end
  object QueryBK: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BANK_HOLIDAY_DATE, ABSENCEREASON_CODE,'
      '  COUNTRY_ID'
      'FROM'
      '  BANKHOLIDAY'
      'WHERE'
      '  BANK_HOLIDAY_DATE >= :FSTARTDATE AND'
      '  BANK_HOLIDAY_DATE <= :FENDDATE AND'
      '  COUNTRY_ID = :FCOUNTRY_ID'
      ' ')
    Left = 264
    Top = 26
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'FCOUNTRY_ID'
        ParamType = ptUnknown
      end>
  end
  object DataSourceQryBK: TDataSource
    DataSet = QueryBK
    Left = 136
    Top = 26
  end
  object StoredProcUpdateEMA: TStoredProc
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    StoredProcName = 'BANKHOL_UPDATEEMA'
    Left = 312
    Top = 26
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'DATE_BANK'
        ParamType = ptInput
      end
      item
        DataType = ftFloat
        Name = 'COUNTRYID'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'CURRENTUSER'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'USERNAME'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'NEW_ABSENCE'
        ParamType = ptInput
      end
      item
        DataType = ftString
        Name = 'OLD_ABSENCE'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'STATUS'
        ParamType = ptOutput
      end>
  end
  object qryCountry: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT DISTINCT C.COUNTRY_ID, C.CODE, C.DESCRIPTION'
      'FROM COUNTRY C LEFT JOIN PLANT P ON'
      '  C.COUNTRY_ID = P.COUNTRY_ID'
      'WHERE P.PLANT_CODE IN'
      '  (SELECT DISTINCT T.PLANT_CODE'
      '   FROM DEPARTMENTPERTEAM T'
      '   WHERE'
      '   ('
      '     (:USER_NAME = '#39'*'#39') OR'
      '     (T.TEAM_CODE IN'
      
        '       (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU WHERE TU.USER_NA' +
        'ME = :USER_NAME))'
      '   )'
      '  )'
      'ORDER BY C.CODE ')
    Left = 432
    Top = 64
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object QueryAbsReason: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'select * from absencereason'
      ' ')
    Left = 472
    Top = 26
  end
  object QueryEmployeesByCountry: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, P.COUNTRY_ID '
      'FROM'
      '  EMPLOYEE E INNER JOIN PLANT P ON'
      '    E.PLANT_CODE = P.PLANT_CODE'
      'WHERE'
      '  P.COUNTRY_ID = :COUNTRY_ID'
      '  AND'
      '  ('
      '    (:USER_NAME = '#39'*'#39') OR'
      '    (E.TEAM_CODE IN'
      
        '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :' +
        'USER_NAME))'
      '  )'
      'ORDER BY'
      '  E.EMPLOYEE_NUMBER')
    Left = 480
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'COUNTRY_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object qryStandAvail: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DAY_OF_WEEK'
      'FROM'
      '  STANDARDAVAILABILITY'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  ((AVAILABLE_TIMEBLOCK_1 = '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_2 = '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_3 = '#39'-'#39') OR'
      '   (AVAILABLE_TIMEBLOCK_4 = '#39'-'#39'))'
      'GROUP BY'
      '  DAY_OF_WEEK'
      'ORDER BY'
      '  DAY_OF_WEEK'
      ' '
      ' '
      ' ')
    Left = 536
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object qryStandAvailXXX: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DAY_OF_WEEK'
      'FROM'
      '  STANDARDAVAILABILITY'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      
        '  ((AVAILABLE_TIMEBLOCK_1 IN (SELECT A.ABSENCEREASON_CODE FROM A' +
        'BSENCEREASON A WHERE A.ABSENCETYPE_CODE = :ABSENCETYPE_CODE) ) O' +
        'R'
      
        '   (AVAILABLE_TIMEBLOCK_2 IN (SELECT A.ABSENCEREASON_CODE FROM A' +
        'BSENCEREASON A WHERE A.ABSENCETYPE_CODE = :ABSENCETYPE_CODE) ) O' +
        'R'
      
        '   (AVAILABLE_TIMEBLOCK_3 IN (SELECT A.ABSENCEREASON_CODE FROM A' +
        'BSENCEREASON A WHERE A.ABSENCETYPE_CODE = :ABSENCETYPE_CODE) ) O' +
        'R'
      
        '   (AVAILABLE_TIMEBLOCK_4 IN (SELECT A.ABSENCEREASON_CODE FROM A' +
        'BSENCEREASON A WHERE A.ABSENCETYPE_CODE = :ABSENCETYPE_CODE) ) )'
      'GROUP BY'
      '  DAY_OF_WEEK'
      'ORDER BY'
      '  DAY_OF_WEEK')
    Left = 584
    Top = 64
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'ABSENCETYPE_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryHourtypeWOBH: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  HOURTYPE_NUMBER, DESCRIPTION'
      'FROM'
      '  HOURTYPE'
      'ORDER BY'
      '  DESCRIPTION')
    Left = 632
    Top = 64
  end
end
