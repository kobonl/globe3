(*
  MRA:30-NOV-2009 RV045.1.
    - Time For Time and Bonus in Money can be set on Contractgroup-
      or Hourtype-level.
  MRA:20-JUN-2011. RV094.1. Bugfix.
  - When hourly rate in employee contract is 0,
    it still calculates something > 0 that is wrong.
  ROP 05-MAR-2013 TD-21527. Change all Format('%?.?f') to Format('%?.?n')
  MRA:1-FEB-2019 GLOB3-206
  - Disbursement details report
  - Also show employee extra payments
*)

unit ReportEmployeePaylistQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, QRCtrls, QuickRpt, ExtCtrls, QRExport, QRXMLSFilt,
  QRWebFilt, QRPDFFilt;

type
  TQRParameters = class
  private
    FBusinessFrom, FBusinessTo: String;
    {FYear, FWeek, }FStepSize: Integer;
    FDateFrom, FDateTo: TDateTime;
    FShowSteps, FShowHourtypes, FShowSelection, FExportToFile: Boolean;
  public
    procedure SetValues(BusinessFrom, BusinessTo: String;
      {Year, Week, }StepSize: Integer;
      DateFrom, DateTo: TDateTime;
      ShowSteps, ShowHourtypes, ShowSelection, ExportToFile: Boolean);
  end;
  TReportEmployeePaylistQR = class(TReportBaseF)
    QRBandTitle: TQRBand;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRBandSummary: TQRBand;
    QRGrpHeaderEmployee: TQRGroup;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRBandDetail: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLblWorkedHours: TQRLabel;
    QRBandColHdrHourtype: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRGrpFooterEmployee: TQRBand;
    QRLabel8: TQRLabel;
    QRLblTotalWorkedHours: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLblHourlyRate: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLblHoursPayment: TQRLabel;
    QRLblTotalHoursPayment: TQRLabel;
    QRChildBandColHdr: TQRChildBand;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel23: TQRLabel;
    QRChildBandColHdrHourtypeIncentive: TQRChildBand;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRChildBandColHdrIncentive: TQRChildBand;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLblQualInc: TQRLabel;
    QRLblQuanInc: TQRLabel;
    QRLblTotalPayment: TQRLabel;
    ChildBand1: TQRChildBand;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLblDateFrom: TQRLabel;
    QRLabel56: TQRLabel;
    QRLblDateTo: TQRLabel;
    ChildBand2: TQRChildBand;
    QRMemoPEC: TQRMemo;
    QRMemoPED: TQRMemo;
    QRMemoPEDate: TQRMemo;
    QRMemoPETot: TQRMemo;
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLblTotalWorkedHoursPrint(sender: TObject;
      var Value: String);
    procedure QRLblWorkedHoursPrint(sender: TObject; var Value: String);
    procedure QRGrpHeaderEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLblHourlyRatePrint(sender: TObject; var Value: String);
    procedure QRLblHoursPaymentPrint(sender: TObject; var Value: String);
    procedure QRLblTotalHoursPaymentPrint(sender: TObject;
      var Value: String);
    procedure QRBandColHdrHourtypeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandColHdrBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandColHdrHourtypeIncentiveBeforePrint(
      Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRChildBandColHdrIncentiveBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLblQualIncPrint(sender: TObject; var Value: String);
    procedure QRLblQuanIncPrint(sender: TObject; var Value: String);
    procedure QRLblTotalPaymentPrint(sender: TObject; var Value: String);
    procedure QRBandColHdrHourtypeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRChildBandColHdrAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRChildBandColHdrHourtypeIncentiveAfterPrint(
      Sender: TQRCustomBand; BandPrinted: Boolean);
    procedure QRChildBandColHdrIncentiveAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpHeaderEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandDetailAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGrpFooterEmployeeAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure ChildBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
    FQRParameters: TQRParameters;
    function DetermineExtraPayments(AEmployeeNumber: Integer): Boolean;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    { Public declarations }
    FMinDate,
    FMaxDate: TDateTime;
    WorkedMinutes, WorkedMinutesTotal: Double;
    WorkedHours: Integer;
    EmployeeNumber, EmployeeDescription: String;
    HourlyRate: Double;
    HourlyRateFromEmpContract: Double;
    HoursPayment, HoursPaymentTotal: Double;
    QualityIncentive, QuantityIncentive, TotalPayment: Double;
    ContractGroupCode: String;
    BonusPercentage: Double;
    UseIncentive: Boolean;
    function QRSendReportParameters(
      const PlantFrom, PlantTo : String;
      const BusinessFrom, BusinessTo: String;
      const EmployeeFrom, EmployeeTo: String;
      const {Year, Week, }StepSize: Integer;
      const DateFrom, DateTo: TDateTime;
      const ShowSteps, ShowHourtypes, ShowSelection,
        ExportToFile: Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
  end;

var
  ReportEmployeePaylistQR: TReportEmployeePaylistQR;

implementation

{$R *.DFM}

uses
  SystemDMT, ReportEmployeePaylistDMT, UGLobalFunctions, ListProcsFRM,
  UPimsConst;

procedure TQRParameters.SetValues(BusinessFrom, BusinessTo: String;
  {Year, Week, }StepSize: Integer;
  DateFrom, DateTo: TDateTime;
  ShowSteps, ShowHourtypes, ShowSelection, ExportToFile: Boolean);
begin
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
//  FYear := Year;
//  FWeek := Week;
  FStepSize := StepSize;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowSteps := ShowSteps;
  FShowHourtypes := ShowHourtypes;
  FShowSelection := ShowSelection;
  FExportToFile := ExportToFile;
end;

function TReportEmployeePaylistQR.QRSendReportParameters(
  const PlantFrom, PlantTo : String;
  const BusinessFrom, BusinessTo: String;
  const EmployeeFrom, EmployeeTo: String;
  const {Year, Week, }StepSize: Integer;
  const DateFrom, DateTo: TDateTime;
  const ShowSteps, ShowHourtypes, ShowSelection,
    ExportToFile: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(BusinessFrom, BusinessTo,
      {Year, Week, }StepSize, DateFrom, DateTo, ShowSteps, ShowHourtypes,
      ShowSelection,
      ExportToFile);
  end;
  SetDataSetQueryReport(ReportEmployeePaylistDM.qrySalHrPerEmp);
end;

function TReportEmployeePaylistQR.ExistsRecords: Boolean;
var
  SelectStr: String;
begin
  Screen.Cursor := crHourGlass;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FBusinessFrom := '1';
    QRParameters.FBusinessTo := 'zzzzzz';
    QRBaseParameters.FEmployeeFrom := '1';
    QRBaseParameters.FEmployeeTo := '999999999';
  end;
  FMinDate := QRParameters.FDateFrom;
//    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeek, 1);
  FMaxDate := QRParameters.FDateTo;
//    ListProcsF.DateFromWeek(QRParameters.FYear, QRParameters.FWeek, 7);

  with ReportEmployeePaylistDM do
  begin
    // Determine: Use Incentive or not?
    UseIncentive := False;
    if tblTaylorMade.RecordCount > 0 then
    begin
      tblTaylorMade.First;
      try
        if tblTaylorMade.FieldByName('QUALITY_INCENTIVE_YN').AsString <> '' then
          if tblTaylorMade.FieldByName('QUALITY_INCENTIVE_YN').Value = 'Y' then
            UseIncentive := True;
      except
        UseIncentive := False;
      end;
    end;

    // Determine SALARY_MINUTEs in given period
    qrySalHrPerEmp.Close;
    qrySalHrPerEmp.ParamByName('PLANTFROM').Value :=
      QRBaseParameters.FPlantFrom;
    qrySalHrPerEmp.ParamByName('PLANTTO').Value :=
      QRBaseParameters.FPlantTo;
    qrySalHrPerEmp.ParamByName('BUSINESSUNITFROM').Value :=
      QRParameters.FBusinessFrom;
    qrySalHrPerEmp.ParamByName('BUSINESSUNITTO').Value :=
      QRParameters.FBusinessTo;
    qrySalHrPerEmp.ParamByName('EMPLOYEEFROM').Value :=
      QRBaseParameters.FEmployeeFrom;
    qrySalHrPerEmp.ParamByName('EMPLOYEETO').Value :=
      QRBaseParameters.FEmployeeTo;
    qrySalHrPerEmp.ParamByName('DATEFROM').Value := FMinDate;
    qrySalHrPerEmp.ParamByName('DATETO').Value := FMaxDate;
    qrySalHrPerEmp.Open;

    Result := not qrySalHrPerEmp.IsEmpty;

    // Initialization for calculation of Quantity Incentive
    if Result and UseIncentive then
    begin
      qryMistakesPerEmp.Close;
      qryMistakesPerEmp.ParamByName('EMPLOYEEFROM').Value :=
        QRBaseParameters.FEmployeeFrom;
      qryMistakesPerEmp.ParamByName('EMPLOYEETO').Value :=
        QRBaseParameters.FEmployeeTo;
      qryMistakesPerEmp.ParamByName('DATEFROM').Value := FMinDate;
      qryMistakesPerEmp.ParamByName('DATETO').Value := FMaxDate;
      qryMistakesPerEmp.Open;

      // For 'QueryProdHour'-Query.
      SelectStr :=
        'SELECT ' + NL +
        '  PHE.PLANT_CODE, PHE.EMPLOYEE_NUMBER, ' + NL +
        '  PHE.PRODHOUREMPLOYEE_DATE, PHE.WORKSPOT_CODE, PHE.JOB_CODE, ' + NL +
        '  SUM(PHE.PRODUCTION_MINUTE) AS WORKEDHRS ' + NL +
        'FROM ' + NL +
        '  PRODHOURPEREMPLOYEE PHE, WORKSPOT W, JOBCODE J, EMPLOYEE E ' + NL +
        'WHERE ' + NL +
        '  PHE.PLANT_CODE >= ''' +
          DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
        '  AND PHE.PLANT_CODE <= ''' +
          DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
        '  AND PHE.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
        '  AND PHE.PLANT_CODE = W.PLANT_CODE ' + NL +
        '  AND PHE.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
        '  AND PHE.PLANT_CODE = J.PLANT_CODE AND ' + NL +
        '  PHE.WORKSPOT_CODE = J.WORKSPOT_CODE ' + NL +
        '  AND PHE.JOB_CODE = J.JOB_CODE ' + NL +
        '  AND W.USE_JOBCODE_YN = ''Y''' + NL +
        '  AND W.MEASURE_PRODUCTIVITY_YN = ''Y''' + NL;
      SelectStr := SelectStr +
        '  AND PHE.EMPLOYEE_NUMBER >= ' + QRBaseParameters.FEmployeeFrom + NL +
        '  AND PHE.EMPLOYEE_NUMBER <= ' + QRBaseParameters.FEmployeeTo + NL +
        '  AND J.BUSINESSUNIT_CODE >= ''' +
          DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
        '  AND J.BUSINESSUNIT_CODE <= ''' +
          DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
      SelectStr := SelectStr +
        '  AND PHE.PRODHOUREMPLOYEE_DATE >= :DATEFROM ' + NL +
        '  AND PHE.PRODHOUREMPLOYEE_DATE < :DATETO ' + NL;
      SelectStr := SelectStr +
        'GROUP BY ' + NL +
        '  PHE.PLANT_CODE, PHE.EMPLOYEE_NUMBER, ' + NL +
        '  PHE.PRODHOUREMPLOYEE_DATE, PHE.WORKSPOT_CODE, PHE.JOB_CODE';

      QueryProdHour.Active := False;
      QueryProdHour.UniDirectional := False;
      QueryProdHour.SQL.Clear;
      QueryProdHour.SQL.Add(SelectStr);
      QueryProdHour.ParamByName('DATEFROM').AsDateTime :=
        FMinDate;
      QueryProdHour.ParamByName('DATETO').AsDateTime :=
        FMaxDate + 1;
      if not QueryProdHour.Prepared then
        QueryProdHour.Prepare;
      QueryProdHour.Active := True;

      // Now open the Incentive-queries for 'Quantity Incentive'
      ListProcsF.InitQueryEmplTotalPieces(
        QueryPQ,
        QueryTRS,
        FMinDate, FMaxDate, False );
//      ListProcsF.InitQueryEmplTotalPiecesTRCount(
//        QueryTRSCNT,
//        FMinDate, FMaxDate,
//        False);
    end; // if Result and UseIncentive then
  end; // with ReportEmployeePaylistDM do

  Screen.Cursor := crDefault;
end;

procedure TReportEmployeePaylistQR.ConfigReport;
begin
  ExportClass.ExportDone := False;

  QRLblBaseLaundry.Caption := '';
  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;

  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  QRLblDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLblDateTo.Caption := DateToStr(QRParameters.FDateTo);
//  QRLabelYear.Caption := IntToStr(QRParameters.FYear);
//  QRLabelWeek.Caption := IntToStr(QRParameters.FWeek);
end;

procedure TReportEmployeePaylistQR.QRBandTitleBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;

  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(
          QRLabel11.Caption
          );
        // From Plant PlantFrom to PlantTo
        ExportClass.AddText(
          QRLabel13.Caption + ' ' +
          QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' +
          QRLabel49.Caption + ' ' +
          QRLabelPlantTo.Caption
          );
        // From BusinessUnit BUFrom to BUTo
        ExportClass.AddText(
          QRLabel25.Caption + ' ' +
          QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' +
          QRLabel16.Caption + ' ' +
          QRLabelBusinessTo.Caption
          );
        // From Employee EmployeeFrom to EmployeeTo
        ExportClass.AddText(
          QRLabel18.Caption + ' ' +
          QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' +
          QRLabel22.Caption + ' ' +
          QRLabelEmployeeTo.Caption
          );
        // From Date DateFrom to DateTo
        ExportClass.AddText(
          QRLabel2.Caption + ' ' + QRLabel3.Caption + ' ' +
            QRLblDateFrom.Caption + ' ' + QRLabel56.Caption + QRLblDateTo.Caption
          );
        // Week Week
//        ExportClass.AddText(
//          QRLabel3.Caption + ' ' + QRLabelWeek.Caption
//          );
      end;
    end;
end;

procedure TReportEmployeePaylistQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportEmployeePaylistQR.FreeMemory;
begin
  inherited;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportEmployeePaylistQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportEmployeePaylistQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  OvertimeYN, TimeForTimeYN, BonusInMoneyYN: String;
  EmployeeMistakes: Integer;
  FDept: String;
  FEmplBonus, FJobBonus: Double;
  FJob_NormProd, FBonusFactor, EmplTotal, FJobNorm: Double;
  FJobWorkHrs: Integer;
  FTotalJobPieces, Performance: Double;
{  FJobWorkHrsTotal: Integer; }
  function CorrectToStepSize(Value: Double; StepSize: Integer): Integer;
  var
    NewValue: Integer;
  begin
    NewValue := Trunc(Value);
    // To be sure comparison with double goes OK, use following compare.
    if not ((NewValue <= Value + 0.000001) and
      (NewValue >= Value - 0.000001)) then
//    if NewValue <> Value then
      NewValue := NewValue + 1;
    Result := NewValue DIV StepSize;
    if (NewValue MOD StepSize) <> 0 then
      Result := (Result + 1) * StepSize
    else
      Result := Result * StepSize;
  end;
  // RV045.1.
  procedure DetermineTimeForTimeSettings;
  begin
    with ReportEmployeePaylistDM do
    begin
      if ReportEmployeePayListDM.ContractGroupTFT then
      begin
        TimeForTimeYN :=
          tblContractgroup.FieldByName('TIME_FOR_TIME_YN').AsString;
        BonusInMoneyYN :=
          tblContractgroup.FieldByName('BONUS_IN_MONEY_YN').AsString;
      end
      else
      begin
        TimeForTimeYN :=
          tblHourtype.FieldByName('TIME_FOR_TIME_YN').AsString;
        BonusInMoneyYN :=
          tblHourtype.FieldByName('BONUS_IN_MONEY_YN').AsString;
      end;
    end;
  end;
begin
  inherited;
  PrintBand := QRParameters.FShowHourtypes;

  with ReportEmployeePaylistDM do
  begin
    // GLOB3-206
    // SALARY_MINUTE can be about salary minutes (TYPE=1) or
    // extra payments (TYPE=2)
    WorkedMinutes :=
      qrySalHrPerEmp.FieldByName('SALARY_MINUTE').AsFloat;
    if qrySalHrPerEmp.FieldByName('TYPE').AsString = '1' then // Salary
    begin
      WorkedMinutesTotal := WorkedMinutesTotal + WorkedMinutes;
      WorkedHours :=
        Round(qrySalHrPerEmp.FieldByName('SALARY_MINUTE').AsFloat / 60);
    end;

    ContractGroupCode :=
      qrySalHrPerEmp.FieldByName('CONTRACTGROUP_CODE').AsString;

    // Now get Hourtype-record
    OvertimeYN := 'N';
    if qrySalHrPerEmp.FieldByName('TYPE').AsString = '1' then // Salary
    begin
      tblHourtype.FindKey([
        qrySalHrPerEmp.FieldByName('HOURTYPE_NUMBER').Value]);
      OvertimeYN := tblHourtype.FieldByName('OVERTIME_YN').AsString;
    end;

    // Determine HourlyRate-value
    qryEmployeeContract.Close;
    qryEmployeeContract.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
      qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').Value;
    qryEmployeeContract.ParamByName('DATEFROM').AsDateTime := FMinDate;
    qryEmployeeContract.ParamByName('DATETO').AsDateTime := FMaxDate;
    qryEmployeeContract.Open;
    HourlyRate := 0;
    HourlyRateFromEmpContract := 0;
    if not qryEmployeeContract.IsEmpty then
    begin
      qryEmployeeContract.First;
      try
        if qryEmployeeContract.FieldByName('HOURLY_WAGE').AsString <> '' then
          HourlyRateFromEmpContract :=
            qryEmployeeContract.FieldByName('HOURLY_WAGE').Value;
      except
        HourlyRateFromEmpContract := 0;
      end;
      HourlyRate := HourlyRateFromEmpContract;
    end;

    // Determine Hourspayment
    HoursPayment := 0;
    // If Hourtype = 1 and overtime is yes
    // then take hourlyrate from employeecontract
    if (OvertimeYN = 'Y') then
    begin
      // If hourtype is not 1 then use contractgroup for determination
      // of hourlyrate.
      if (qrySalHrPerEmp.FieldByName('HOURTYPE_NUMBER').AsInteger <> 1) then
      begin
        // Get Contractgroup-record for fields like
        // TIME_FOR_TIME_YN and BONUS_IN_MONEY_YN
        if tblContractgroup.FindKey([ContractGroupCode]) then
        begin
          // RV045.1.
//          TimeForTimeYN :=
//            tblContractgroup.FieldByName('TIME_FOR_TIME_YN').AsString;
//          BonusInMoneyYN :=
//            tblContractgroup.FieldByName('BONUS_IN_MONEY_YN').AsString;
          DetermineTimeForTimeSettings;

          BonusPercentage := 0;
          HourlyRate := HourlyRateFromEmpContract;
          if (TimeForTimeYN = 'Y') and (BonusInMoneyYN = 'N') then
            HourlyRate := 0
          else
            if (TimeForTimeYN = 'Y') and (BonusInMoneyYN = 'Y') then
            begin
              BonusPercentage :=
               tblHourtype.FieldByName('BONUS_PERCENTAGE').Value;
              HourlyRate := BonusPercentage / 100 * HourlyRateFromEmpContract;
            end
            else
              if (TimeForTimeYN = 'N') then
              begin
                BonusPercentage :=
                  tblHourtype.FieldByName('BONUS_PERCENTAGE').Value;
                // RV094.1. Bugfix. Start.
                // Wrong version, this can give a value, even if
                // HourRateFromEmpContract is 0.
{
                HourlyRate := 100 + BonusPercentage / 100 *
                   HourlyRateFromEmpContract;
}
                // Replace by:
                HourlyRate := (100 + BonusPercentage) / 100 *
                  HourlyRateFromEmpContract;
                // RV094.1. Bugfix. End.
              end;
        end; // if tblContractgroup.FindKey([ContractGroupCode])
      end; // if (qrySalHrPerEmp.FieldByName('HOURTYPE_NUMBER').AsInteger = 1)
    end; // if (tblHourtype.FieldByName('OVERTIME_YN').AsString = 'Y')
    if qrySalHrPerEmp.FieldByName('TYPE').AsString = '1' then // Salary
      HoursPayment := WorkedMinutes / 60 * HourlyRate
    else
      HoursPayment := WorkedMinutes; // GLOB3-206 Extra Payment!
    HoursPaymentTotal := HoursPaymentTotal + HoursPayment;

    // Determine Incentive value
    if UseIncentive then
    begin
      // Quality Incentive
      qryMistakesPerEmp.Filtered := False;
      qryMistakesPerEmp.Filter := 'EMPLOYEE_NUMBER = ' +
        qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').AsString;
      qryMistakesPerEmp.Filtered := True;
      if not qryMistakesPerEmp.IsEmpty then
      begin
        EmployeeMistakes :=
          Round(qryMistakesPerEmp.FieldByName('SUM_MISTAKES').AsFloat);
        QualityIncentive :=
          BonusCalculation(EmployeeMistakes, Round(WorkedMinutes), 0);
      end;
      // Quantity Incentive
      // Do this only ONCE for each employee
      if QuantityIncentive = -1 then
      begin
        QuantityIncentive := 0;
{        FJobWorkHrsTotal := 0; }
        QueryProdHour.Filtered := False;
        QueryProdHour.Filter := 'EMPLOYEE_NUMBER = ' +
          qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').AsString;
        QueryProdHour.Filtered := True;
        FEmplBonus := 0;
        if not QueryProdHour.IsEmpty then
        begin
          QueryProdHour.First;
          while not QueryProdHour.Eof do
          begin
            TableWK.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
              QueryProdHour.FieldByName('WORKSPOT_CODE').AsString]);
            FDept := TableWK.FieldByName('DEPARTMENT_CODE').AsString;
            ListProcsF.DetermineEmplTotalPieces(
              AQuery,
              QueryPQ,
              QueryTRS,
              QueryProdHour.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime,
              QueryProdHour.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime,
              qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              0, 999999,
              False, True,
              QueryProdHour.FieldByName('PLANT_CODE').AsString,
              QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
              QueryProdHour.FieldByName('JOB_CODE').AsString,
              FDept, EmplTotal);
            FTotalJobPieces := EmplTotal;
            TableJob.FindKey([QueryProdHour.FieldByName('PLANT_CODE').AsString,
              QueryProdHour.FieldByName('WORKSPOT_CODE').AsString,
              QueryProdHour.FieldByName('JOB_CODE').AsString]);
            FJob_NormProd := TableJob.FieldByName('NORM_PROD_LEVEL').AsFloat;
            TableBU.FindKey([TableJob.FieldByName('BUSINESSUNIT_CODE').AsString]);
            FBonusFactor := TableBU.FieldByName('BONUS_FACTOR').AsFloat;
            FJobWorkHrs :=
              QueryProdHour.FieldByName('WORKEDHRS').AsInteger;
            FJobNorm := (FJob_NormProd * FJobWorkHrs) / 60;
{            Performance := 0; }
            if FJobNorm <> 0 then
            begin
              Performance :=  (FTotalJobPieces / FJobNorm * 100) - 100;
              if Performance > 0 then
              begin
                if QRParameters.FShowSteps then
                  Performance :=
                    CorrectToStepSize(Performance, QRParameters.FStepSize);
                FJobBonus := (Performance * FJobWorkHrs * FBonusFactor) / 60;
                FEmplBonus := FEmplBonus + FJobBonus;
              end;
            end;
{
            WDebugLog(
              'EMP=' +
              qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').AsString +
              ' WS=' +
              QueryProdHour.FieldByName('WORKSPOT_CODE').AsString +
              ' JOB=' +
              QueryProdHour.FieldByName('JOB_CODE').AsString +
              ' Date=' +
              DateToStr(QueryProdHour.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime) +
              ' JobNormProdLevel=' + Format('%f', [FJob_NormProd]) +
              ' FTotalJobPieces=' + Format('%f', [FTotalJobPieces]) +
              ' BonusFactor=' + Format('%f', [FBonusFactor]) +
              ' JobWorkHrs=' + IntToStr(FJobWorkHrs) +
              ' FJobNorm=' + Format('%f', [FJobNorm]) +
              ' Performance=' + Format('%f', [Performance]) +
              ' FEmplBonus=' + Format('%f', [FEmplBonus])
              );
            FJobWorkHrsTotal := FJobWorkHrsTotal + FJobWorkHrs;
}
            QueryProdHour.Next;
          end; // while not QueryProdHour.Eof do
        end; // if not QueryProdHour.IsEmpty then
        QuantityIncentive := FEmplBonus;
{
        WDebugLog(' JobWorkHrsTotal=' + IntToStr(FJobWorkHrsTotal) + ' (' +
          IntMin2StringTime(FJobWorkHrsTotal, True) + ')');
}
      end; // if QuantityIncentive = -1 then
    end; // if UseIncentive then
  end; // with ReportEmployeePaylistDM do
end;

procedure TReportEmployeePaylistQR.QRLblTotalWorkedHoursPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if ReportEmployeePaylistDM.qrySalHrPerEmp.FieldByName('TYPE').Value = '1' then
    Value :=
      IntMin2StringTime(Round(WorkedMinutesTotal), True)
  else
    Value := '';
  if (not QRParameters.FShowHourtypes) and (not UseIncentive) then
    Value := '';
end;

procedure TReportEmployeePaylistQR.QRLblWorkedHoursPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if ReportEmployeePaylistDM.qrySalHrPerEmp.FieldByName('TYPE').Value = '1' then
    Value := IntMin2StringTime(Round(WorkedMinutes), True)
  else
    Value := '';
  if (not QRParameters.FShowHourtypes) and (not UseIncentive) then
    Value := '';    
end;

procedure TReportEmployeePaylistQR.QRGrpHeaderEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // Init for this employee
  WorkedMinutesTotal := 0;
  HoursPaymentTotal := 0;
  QualityIncentive := 0;
  QuantityIncentive := -1;
  TotalPayment := 0;
end;

procedure TReportEmployeePaylistQR.QRLblHourlyRatePrint(sender: TObject;
  var Value: String);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  Value := Format('%10.2f', [HourlyRate]);
  if ReportEmployeePaylistDM.qrySalHrPerEmp.FieldByName('TYPE').Value = '1' then
    Value := Format('%10.3n', [HourlyRate])
  else
    Value := '';
end;

procedure TReportEmployeePaylistQR.QRLblHoursPaymentPrint(sender: TObject;
  var Value: String);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  Value := Format('%10.2f', [HoursPayment]);
  Value := Format('%10.2n', [HoursPayment]);
end;

procedure TReportEmployeePaylistQR.QRLblTotalHoursPaymentPrint(
  sender: TObject; var Value: String);
begin
  inherited;
// ROP 05-MAR-2013 TD-21527  Value := Format('%10.2f', [HoursPaymentTotal]);
  Value := Format('%10.2n', [HoursPaymentTotal]);
end;

procedure TReportEmployeePaylistQR.QRBandColHdrHourtypeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowHourtypes and (not UseIncentive);
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := (not QRParameters.FShowHourtypes) and (not UseIncentive);
  if PrintBand then
  begin
    QRLabel17.Caption := '';
    QRLabel19.Caption := '';
    QRLabel21.Caption := '';
    QRLabel23.Caption := '';
  end
  else
  begin
    QRLabel17.Caption := QRLabel6.Caption;
    QRLabel19.Caption := QRLabel7.Caption;
    QRLabel21.Caption := QRLabel12.Caption;
    QRLabel23.Caption := QRLabel14.Caption;
  end;
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrHourtypeIncentiveBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowHourtypes and UseIncentive;
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrIncentiveBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := (not QRParameters.FShowHourtypes) and UseIncentive;
end;

procedure TReportEmployeePaylistQR.QRLblQualIncPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  if UseIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%10.2f', [QualityIncentive]);
    Value := Format('%10.2n', [QualityIncentive]);
end;

procedure TReportEmployeePaylistQR.QRLblQuanIncPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  if UseIncentive then
// ROP 05-MAR-2013 TD-21527    Value := Format('%10.2f', [QuantityIncentive]);
    Value := Format('%10.2n', [QuantityIncentive]);

end;

procedure TReportEmployeePaylistQR.QRLblTotalPaymentPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := '';
  if UseIncentive then
  begin
    TotalPayment := HoursPaymentTotal + QualityIncentive + QuantityIncentive;
// ROP 05-MAR-2013 TD-21527    Value := Format('%10.2f', [TotalPayment]);
    Value := Format('%10.2n', [TotalPayment]);
  end;

end;

procedure TReportEmployeePaylistQR.QRBandColHdrHourtypeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel4.Caption + ExportClass.Sep + ExportClass.Sep +
      QRLabel5.Caption + ExportClass.Sep + ExportClass.Sep +
      QRLabel6.Caption + ' ' + QRLabel7.Caption + ExportClass.Sep +
      QRLabel9.Caption + ' ' + QRLabel10.Caption + ExportClass.Sep +
      QRLabel12.Caption + ' ' + QRLabel14.Caption
      );
  end;
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel15.Caption + ExportClass.Sep + ExportClass.Sep +
      ExportClass.Sep + ExportClass.Sep +
      QRLabel17.Caption + ' ' + QRLabel19.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel21.Caption + ' ' + QRLabel23.Caption
      );
  end;
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrHourtypeIncentiveAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel24.Caption + ExportClass.Sep + ExportClass.Sep +
      QRLabel26.Caption + ExportClass.Sep + ExportClass.Sep +
      QRLabel27.Caption + ' ' + QRLabel28.Caption + ExportClass.Sep +
      QRLabel29.Caption + ' ' + QRLabel30.Caption + ExportClass.Sep +
      QRLabel31.Caption + ' ' + QRLabel32.Caption + ExportClass.Sep +
      QRLabel33.Caption + ' ' + QRLabel34.Caption + ExportClass.Sep +
      QRLabel35.Caption + ' ' + QRLabel36.Caption + ExportClass.Sep +
      QRLabel37.Caption + ' ' + QRLabel38.Caption
      );
  end;
end;

procedure TReportEmployeePaylistQR.QRChildBandColHdrIncentiveAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    ExportClass.AddText(
      QRLabel39.Caption + ExportClass.Sep + ExportClass.Sep +
      ExportClass.Sep + ExportClass.Sep +
      QRLabel40.Caption + ' ' + QRLabel41.Caption + ExportClass.Sep +
      ExportClass.Sep +
      QRLabel42.Caption + ' ' + QRLabel43.Caption + ExportClass.Sep +
      QRLabel44.Caption + ' ' + QRLabel45.Caption + ExportClass.Sep +
      QRLabel46.Caption + ' ' + QRLabel47.Caption + ExportClass.Sep +
      QRLabel50.Caption + ' ' + QRLabel51.Caption
      );
  end;
end;

procedure TReportEmployeePaylistQR.QRGrpHeaderEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportEmployeePaylistDM do
    begin
      ExportClass.AddText(
        qrySalHrPerEmp.FieldByName('EMPLOYEE_NUMBER').AsString +
          ExportClass.Sep +
        qrySalHrPerEmp.FieldByName('DESCRIPTION').AsString
        );
    end;
  end;
end;

procedure TReportEmployeePaylistQR.QRBandDetailAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  HourlyRateText: String;
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportEmployeePaylistDM do
    begin
      if qrySalHrPerEmp.FieldByName('TYPE').AsString = '1' then
        HourlyRateText := Format('%10.2n', [HourlyRate])
      else
        HourlyRateText := '';
      ExportClass.AddText(
        ExportClass.Sep + ExportClass.Sep +
        qrySalHrPerEmp.FieldByName('HOURTYPE_NUMBER').AsString +
          ExportClass.Sep +
        qrySalHrPerEmp.FieldByName('HDESCRIPTION').AsString + ExportClass.Sep +
        '' {IntMin2StringTime(WorkedMinutes, True)} + ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527        Format('%10.2f', [HourlyRate]) + ExportClass.Sep +
        HourlyRateText + ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527        Format('%10.2f', [HoursPayment])
        Format('%10.2n', [HoursPayment])
        );
    end;
  end;
end;

procedure TReportEmployeePaylistQR.QRGrpFooterEmployeeAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    with ReportEmployeePaylistDM do
    begin
      if UseIncentive then
        ExportClass.AddText(
          QRLabel8.Caption + ExportClass.Sep + ExportClass.Sep +
          ExportClass.Sep + ExportClass.Sep +
          '' {IntMin2StringTime(WorkedMinutesTotal, True)} + ExportClass.Sep +
          ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527          Format('%10.2f', [HoursPaymentTotal]) + ExportClass.Sep +
          Format('%10.2n', [HoursPaymentTotal]) + ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527          Format('%10.2f', [QualityIncentive]) + ExportClass.Sep +
          Format('%10.2n', [QualityIncentive]) + ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527          Format('%10.2f', [QuantityIncentive]) + ExportClass.Sep +
          Format('%10.2n', [QuantityIncentive]) + ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527          Format('%10.2f', [TotalPayment])
          Format('%10.2n', [TotalPayment])
          )
      else
        ExportClass.AddText(
          QRLabel8.Caption + ExportClass.Sep + ExportClass.Sep +
          ExportClass.Sep + ExportClass.Sep +
          '' {IntMin2StringTime(WorkedMinutesTotal, True)} + ExportClass.Sep +
          ExportClass.Sep +
// ROP 05-MAR-2013 TD-21527          Format('%10.2f', [HoursPaymentTotal])
          Format('%10.2n', [HoursPaymentTotal])
          )
    end;
  end;
end;

// GLOB3-206
function TReportEmployeePaylistQR.DetermineExtraPayments(
  AEmployeeNumber: Integer): Boolean;
begin
  Result := False;
  QRMemoPEC.Lines.Clear;
  QRMemoPED.Lines.Clear;
  QRMemoPEDate.Lines.Clear;
  QRMemoPETot.Lines.Clear;

  with ReportEmployeePaylistDM.qryEmployeeExtraPayments do
  begin
    Close;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    ParamByName('DATEFROM').AsDateTime := QRParameters.FDateFrom;
    ParamByName('DATETO').AsDateTime := QRParameters.FDateTo;
    Open;
    if not Eof then
    begin
      Result := True;
      while not Eof do
      begin
        if QRMemoPEC.Lines.IndexOf(FieldByName('PAYMENT_EXPORT_CODE').AsString) < 0 then
          QRMemoPEC.Lines.Add(FieldByName('PAYMENT_EXPORT_CODE').AsString)
        else
          QRMemoPEC.Lines.Add('');
        if QRMemoPED.Lines.IndexOf(FieldByName('DESCRIPTION').AsString) < 0 then
          QRMemoPED.Lines.Add(FieldByName('DESCRIPTION').AsString)
        else
          QRMemoPED.Lines.Add('');
        QRMemoPEDate.Lines.Add(DateToStr(FieldByName('PAYMENT_DATE').AsDateTime));
        QRMemoPETot.Lines.Add(Format('%.2n', [FieldByName('PAYMENT_AMOUNT').AsFloat]));
        Next;
      end; // while
    end; // if
  end; // with
end; // DetermineExtraPayments

// GLOB3-206
procedure TReportEmployeePaylistQR.ChildBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  I: Integer;
begin
  inherited;
  PrintBand := DetermineExtraPayments(
    ReportEmployeePaylistDM.qrySalHrPerEmp.
      FieldByName('EMPLOYEE_NUMBER').Value);
  if QRParameters.FExportToFile and PrintBand then
  begin
    with ReportEmployeePaylistDM do
    begin
      // Column header
      ExportClass.AddText(
        QRLabel52.Caption + ExportClass.Sep +
        QRLabel53.Caption + ExportClass.Sep +
        QRLabel54.Caption + ExportClass.Sep +
        QRLabel55.Caption + ExportClass.Sep
        );
      // Details
      for I := 0 to QRMemoPEC.Lines.Count - 1 do
      begin
        ExportClass.AddText(
          ExportClass.Sep +
          QRMemoPEC.Lines.Strings[I] + ' ' +
          QRMemoPED.Lines.Strings[I] + ExportClass.Sep +
          QRMemoPEDate.Lines.Strings[I] + ExportClass.Sep +
          QRMemoPETot.Lines.Strings[I]
          );
      end;
    end;
  end;
end; // ChildBand1BeforePrint

// GLOB3-206
procedure TReportEmployeePaylistQR.ChildBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRMemoPEC.Lines.Count > 0;
end;

end.
