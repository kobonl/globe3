object Form1: TForm1
  Left = 202
  Top = 132
  Width = 714
  Height = 416
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dxTLPlanned: TdxTreeList
    Left = 24
    Top = 24
    Width = 233
    Height = 249
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    TabOrder = 0
    TreeLineColor = clGrayText
    ShowGrid = True
    ShowRoot = False
    object dxTLPlannedColumnTb: TdxTreeListColumn
      Caption = 'Tb'
      Width = 54
      BandIndex = 0
      RowIndex = 0
    end
    object dxTLPlannedColumnWorkspot: TdxTreeListColumn
      Caption = 'Workspot'
      Width = 147
      BandIndex = 0
      RowIndex = 0
    end
  end
  object dxTreeList1: TdxTreeList
    Left = 328
    Top = 24
    Width = 233
    Height = 249
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    TabOrder = 1
    TreeLineColor = clGrayText
    ShowGrid = True
    ShowRoot = False
    object dxTreeListColumn1: TdxTreeListColumn
      Caption = 'Tb'
      Width = 54
      BandIndex = 0
      RowIndex = 0
    end
    object dxTreeListColumn2: TdxTreeListColumn
      Caption = 'Workspot'
      Width = 147
      BandIndex = 0
      RowIndex = 0
    end
  end
end
