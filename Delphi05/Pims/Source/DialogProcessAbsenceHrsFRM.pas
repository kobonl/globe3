(*
  Last changes:
  MR:17-12-2003 Bug solved with 'IS_SCANNING_YN' for 'available'-selection,
                regardless of 'IS_SCANNING_YN' all available-records must
                be deleted, otherwise double hours will be stored in SalaryHrs.
  MR:18-12-2003 Optimizing part that gets timeblocks/breaks by using
                procedures from 'CalculateTotalHoursDMT'.
  MR:17-09-2004 Changes for order 550335, unpaid-illness-calculation
  MR:04-01-2007 Changes for order 550436. Error during process.
  RV004. Revision 004. Out-of-memory-fix.
  2.0.139.125. An out-of-memory can happen when 'process absence...' is
               done for a whole month (not per week). To solve this
               the Transaction-mechanism is turned off.
  MR:24-10-2008 RV012. Revision 012. 2.0.139.133.
    Problem with breaks/payed breaks and calculation of absencehours
    per employee.
    The breaks where subtracted from the absencehours.
    To solved this, the payedbreak-minutes are added to it.
  MRA:27-01-2009 RV021. Revision 021. 2.0.139.139.
    Wrong selection with TEAM + TEAMPERUSER-combination!
    This gave multiple absence-hours!
  MRA:09-04-2009 RV025.
    Fix problem when end-time is set as 00:00.
  MRA:14-MAY-2009 RV027.
    - Absence + breaks problem.
      Add the breaks! This is about Absence-hours (like Holiday/Vacation)
    - Memory-problem: Changes to prevent memory-problems.
    - With timeblock like: Start 23:00 and end 03:00, it gave 1 hours as result,
      but is should be 4 hours!
    - Only update AbsenceTotal-table when it is really needed, and also
      salaryhourperemployee-records.
  MRA:17-SEP-2009 RV033.2.
    - When calculating non-scanning employee availability hours, it does
      not remove the salary hours first. This results in multiple hours,
      when it is processed more than once.
  MRA:8-OCT-2009 RV038.
    - Adjustment for breaks (include yes/no). When export-type is 'PARIS' then
      include breaks, for all others only include payed breaks.
  MRA:26-OCT-2009 RV040.1.
    - Try to optimize the recalc-procedure by using the recalcsalary-procedure,
      because of memory-problems this is cancelled.
  MRA:28-OCT-2009 RV040.2.
    - Also recalculate the TFT time.
  MRA:30-OCT-2009 RV040.
    - Removed Prepare to avoid memory-problems.
  MRA:4-NOV-2009 RV040.
    - It appears 'RecalcProdSalary' does not calculate correct for
      employee with an overtime-period larger than 1 week.
      The Process Absence hours-procedure is doing this correct.
      Therefore this procedure is changed, so it will always do
      a recalculation for all employee that are scanners within the
      selection.
      EXAMPLE:
        When using 'Process Planned Absence' for the first week
        of a month, then this recalculates the complete overtime
        period correct, even if it was longer than this week.
        But when using 'RecalcProdSalary' for that same week,
        it only recalculates 1 week, which gives a wrong result
        when overtime is larger than 1 week!
      Conclusion:
        In future 'RecalcProdSalary' must not be used anymore!
  MRA:4-NOV-2009 RV040.
    - For date-selection, use 'OnCloseUp' instead of 'OnChange', because
      with 'OnChange' it changes too much.
  MRA:23-NOV-2009 RV045.1.
    - TimeForTime-settings are now possible on hourtype-level, instead
      of only contractgroup-level.
  MRA:10-DEC-2009 RV048.4. Bugfix.
    - When using 'Process Planned Absence' it can go wrong when
      processing scans for a week, but for Saturday + Sunday no timeblocks
      have been defined, and there are only scans for Monday till Friday.
      This results in removing of salary-hours for Friday and next Monday,
      because it does not find timeblocks for Saturday, so it takes
      the previous day as bookingday and removes first the salary.
      On Sunday there is no scan, but it tries to find the nearest shift-day,
      this is Monday and it removes the salary.
      As a result for 2 days the salary-hours are removed.
      Main-problem is: It does not look at the scans really, but just
      tries to calculate something for each day of the given period. And because
      it first deletes the salary for that day (without checking first if there
      are scans on that day), salary-hours are lost afterwards.
  MRA:11-JAN-2010 RV049.3. Bugfix.
  - Check scan during compute salary must NOT be done! (see RV048.4).
  MRA:12-JAN-2010. RV050.8. 889955.
  - Restrict plants using teamsperuser.
  - This dialog is now inherited from DialogReportBase.
  MRA:2-FEB-2010 RV051.1. Bugfix.
  - When selecting 'all teams', it used empty values for team-from-to.
    Cause: Because this dialog is inherited from DialogReportBase, it
    was missing an inherited in btnOKClick-procedure.
  MRA:4-FEB-2010. RV053.2.
  - When this dialog is started from EmployeeRegistrations, it must
    only show 1 employee.
  MRA:8-MAR-2010. RV056.1.
  - Also re-calculate Production-hours.
  SO: 17-JUN-2010 RV066.1. 550491
  - tailor made non-scanners
  MRA: 28-JUN-2010. RV066.1. 550491
  - tailer made non-scanners: Additions.
  SO:04-AUG-2010 RV067.7. 550499
  - recalculate hours check box
  SO:30-AUG-2010 RV067.12. 550498
  Wrong calculation of hours per employee
  MRA:1-SEP-2010 RV067.MRA.22 Change for order 550491.
  - Also check if employee is available!
    If not available, then it should not create production-records!
  MRA:3-SEP-2010 RV067.MRA.26 Changes for order 550499.
  - When checkbox 'recalculate hours' is unchecked, there still
    must be recalculated hours for 'changed employees'.
    For example, when an employee has absence-hours, the hours
    must be recalculated to determine the correct hourtypes.
  MRA:10-SEP-2010 RV067.MRA.31 Order 550515
  - A check must be made to ensure the balances are correct
    for absence hours. Compare ABSENCEHOURPEREMPLOYEE with
    ABSENCETOTALS.
  MRA:22-SEP-2010 RV067.MRA.35 Change for 550491
  - It did not create salary records when there was no
    employee availability, but it should do this,
    even if there is no employee availability.
  MRA:4-OCT-2010 RV071.4. 550497
  - Literal message put in UMessageRes.
  MRA:6-OCT-2010 RV071.6. 550497
  - Added call to: RecalcEarnedSWWHours
  MRA:18-OCT-2010 RV072.1. 550491. Bugfixing.
  - Tailor made give sometimes double values for
    Prod-hrs-per-emp-per-type (phpept).
    Reason: When it finds day-planning and standard-planning
            it can result in double values for phpept.
  MRA:22-OCT-2010 RV074.1. Bugfix.
  - If absence is calculated and absence is '',
    then it still did add absence-minutes.
    This must not be done!
  MRA:25-OCT-2010 RV074.2. Bugfix. Tailor Made non-scanners. CLF.
  - When there were standard planning on 4 different workspots on 1 day,
    it resulted in only salary hours for 1 workspot.
  MRA:26-OCT-2010 RV075.1. Bugfix.
  - When the action 'employee availability no-scan employees'
    is taking place, it determines manual absence hours.
    When found, these are subtracted from existing salary hours.
    But during this, the prod-hour-per-emp-per-type is NOT
    changed! As a result, it is different from salary-hours.
  - Cause: These hours must ONLY be calculated for the combination
    IS_SCANNING_YN = 'N' and BOOK_PROD_HRS_YN = 'N'
  MRA:27-OCT-2010 RV075.2. Bugfix.
  - It only recalculates balances like TFT when an employee
    is of type Is-Scanning. This is wrong, it must do this for every employee,
    when a change has been made in salary-hours.
  MRA:29-OCT-2010 RV075.6. Bugfix. CANCELLED.
  - A recalculation is needed for PHEPT-records. This is done
    by comparing PHE with PHEPT-records. If there is a difference,
    the PHEPT-records must be corrected.
  MRA:30-NOV-2010. RV082.4. Change for SO-550491.
  - Create automatic scans based on standard planning, instead
    of creating production, salary and prod-per-type records.
  - This is to ensure overtime-calculations will be performed.
  MRA:3-DEC-2010. RV082.6.
  - Tailer made non-scanners:
    - Get Department from Workspot, instead
      of from Employee. This is important for determining
      timeblocks per department.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed this dialog in Pims, instead of
    open it as a modal dialog.
  - To make the start of dialog faster, open/close tables in MainAction.
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Use always SystemDM -> DisplayMessage, this has 'on-top' setting.
  MRA:19-OCT-2011 RV099.1. 20012234 TFT/Holiday correction
  - Make correction for TFT/Holiday:
    - Based on settings made in COUNTRY-table, try to book
      vacation first on TFT when there are enough hours left in balance.
      When not, then book them on vacation.
    - During this employee-availability will be changed.
  MRA:1-NOV-2011 RV100.1.
  - Added Contractgroup to Query.
  MRA:11-JAN-2012. RV104.1. 20011799. Worked hours during bank holiday. Addition.
  - This did not work yet for non-scanners:
    - Process Planned Absence must also be changed:
      - When a non-scanner is made available on a bank-holiday,
        then the system must also check and change the hour type
        to the 'hourtype worked hours during bank holiday'.
  - An existing function from 'GlobalDMT' is changed/used for this purpose.
  MRA:31-JAN-2012 2.0.162.215. 20012085.1 Changes for Belgium Market
  - Correct Absence hours during planned absence
    - Process Planned Absence:
      - Correct absence hours based on
        Country- and Contract Group-settings.
  MRA:16-MAY-2012 20013183
  - The balance for travel time must be updated.
  MRA:25-MAY-2012 20012085.2. Changes Belgium Market.
  - Disabled for this order.
  MRA:1-JUN-2012 20013271 Bugfix.
  - Process Planned Absence does not calculate hours with
    overnight scans (PCO)
  MRA:18-OCT-2012 SC-50025602
  - Problem with Absence Balance, sometimes values are in balance that
    do not belong there. For example -8 hours for 'used holiday'.
    Cause: The balance is not initialized before it is reprocessed.
    Changed in: CorrectionBalanceAbsenceHours;
  MRA:26-OCT-2012 20013489 Overnight-Shift-System
  - Check/Correct the ShiftDate of the scans, before they are processed.
  MRA:28-MAR-2013 TD-22355 Performance issue.
  - Procedure 'ActionSalaryHourPerEmployee' takes a lot of time,
    this must be optimized.
  - Also procedure 'ActionAbsenceHourPerEmployee' must be optimized.
  - Also procedure 'ActionEmployeeAvailability' must be optimized.
  - All TTables replaced by TTQueries to improve speed.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Component TDateTimePicker is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:26-NOV-2013 TD-23672
  - Holiday counters show wrong data (CLF)
    - Be sure a commit is done after all has been processed.
    - Disabled all 'StartTransaction'-commands and commits done inbetween.
  MRA:17-FEB-2014 20011800
  - Final Run System
  MRA:21-MAR-2014 TD-24278 Problem with balance + holiday
  - Sometimes it books holiday hours from previous year in balance
    of current year.
  - To solve this:
    - For the balance of the year, start from 1-1 till 31-12,
      instead of using first day of week and last day of week.
  - Reason: A week can start in previous year and end in current year.
  MRA:28-APR-2014 20011800.70 Rework
  - Final Run System Rework
    - Because of this, it only shows 1 plant (no from-to), but other
      levels could not be selected. To solve this use:
      CmbPlusPlantFromCloseUp(Sender).
  MRA:23-OCT-2015 PIM-55
  - Worked hours and illness/absence registration
  - Only do the correction when CORRECT_WORK_HRS_YN equals 'Y' for
    absencereason.
  MRA:4-FEB-2016 PIM-55
  - Bugfix: It calculated less hours when there were no salary hours but
    the absence reason had CORRECT_WORK_HRS_YN equals 'Y'.
  - Bugfix: It calculated double hours when 2 absence days were involved and
    only 1 date should be recalculated. Reason: It called routine
    ActionEmployeeAvailability but this calculated always the whole period.
    Now it is changed to make it possible to calculate only 1 day.
  MRA:29-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)

unit DialogProcessAbsenceHrsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Dblup1a, dxCntner, dxEditor, dxExEdtr, dxEdLib, UScannedIDCard,
  dxExGrEd, dxExELib, dxLayout, Db, DBTables, GlobalDMT,
  DialogReportBaseFRM, SystemDMT, jpeg;

const
  NUMBER_OF_PROCEDURES = 6;

type
  TDialogProcessAbsenceHrsF = class(TDialogReportBaseF)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    pnlResults: TPanel;
    grpBxResults: TGroupBox;
    mmLog: TMemo;
    Panel1: TPanel;
    grpBxProgress: TGroupBox;
    pBar: TProgressBar;
    DateFrom: TDateTimePicker;
    DateTo: TDateTimePicker;
    pBar2: TProgressBar;
    LblFromDate: TLabel;
    LblDate: TLabel;
    LblToDate: TLabel;
    ckRecalculateHours: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure DateToChange(Sender: TObject);
    procedure DateFromCloseUp(Sender: TObject);
    procedure DateToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    FStartDate: TDateTime;
    FEndDate: TDateTime;
    ChangedEmployeeList: TStringList;
    FMyEmployee: String;
    FMyPlant: String;
    FMyError: Boolean;
    FMyErrorMessage: String;
    MyAddCount: Integer;
    FEmployeeFrom: Integer;
    FEmployeeTo: Integer;
    FTeamFrom: String;
    FTeamTo: String;
    FPlantFrom: String;
    FPlantTo: String;
    procedure Log(AMsg: String; AError: Boolean=False);
    function SalaryCheck(AEmployeeNumber: Integer;
      ASalaryDate: TDateTime): Integer;
    function DetermineTimeBlocks(
      TotalAvailMinutes: Integer;
      VAR TotalSalaryMinutes: Integer;
      VAR TotalMinutes: Integer;
      VAR TimeBlockLength: Integer;
      SelectedTimeBlock, WeekDay: Integer;
      AvailableTimeBlock: String;
      VAR ChangeCount, AddCount: Integer;
      IsAbsence: Boolean): Boolean;
    procedure ActionAbsenceHourPerEmployee;
    procedure ActionSalaryHourPerEmployee;
    procedure ActionEmployeeAvailability(AMyDate: TDateTime=0); // PIM-55
    procedure ActionEmployeeAvailabilityNoScanEmployees;
    procedure ActionComputeSalaryHours;
//    procedure ActionComputeSalaryHoursXXX;
//car 550261
    procedure ActionUnpaidIllness;
    procedure ProcessbarUpdate;
    procedure MainAction;
    function IsRFL: Boolean;
    procedure DeterminePlanningTimeBlocks(var ProdMin,
      BreaksMin: Integer; SelectedTimeBlock, WeekDay: Integer;
      PlanningDate: TDateTime;
      VAR AStart, AEnd: TDateTime);
    procedure ProductionHoursDayPlanning(Lst: TStringList);
    procedure ProductionHoursStandardPlanning(Lst: TStringList; ADate: TDateTime); overload;
    procedure ProductionHoursStandardPlanning(Lst: TStringList); overload;
    procedure ActionTailorMadeNonScanners;
    // RV082.4. Not needed.
{    procedure UpdateProdHourPerEmployee(ADate: TDateTime;
      APlantCode: String; AShiftNumber, AEmployeeNumber: Integer;
      AWorkspotCode: String; AProdMin, ABreaksMin: Integer); }
    procedure CheckDeleteProdHours;
    procedure DeleteAutoScanByEmpDate(ADate: TDateTime;
      AEmployeeNumber: Integer);
    procedure DeleteProdHourPerEmpRecordByEmpDate(
      ADate: TDateTime;
      AEmployeeNumber: Integer;
      AManualYN: String);
    procedure DeleteSalHourPerEmpRecordByEmpDate(
      ADate: TDateTime;
      AEmployeeNumber: Integer;
      AManualYN: String);
    procedure DeleteProdHourPerEmpPerTypeRecordByEmpDate(
      ADate: TDateTime;
      AEmployeeNumber: Integer;
      AManualYN: String);
    // RV082.4. Not needed.
{    procedure DeleteProdHourPerEmployeeRecord(ADate: TDateTime;
      APlantCode: String; AShiftNumber, AEmployeeNumber: Integer;
      AWorkspotCode: String; AProdMin, ABreaksMin: Integer; AJobCode,
      AManualYN: String); }
    procedure InsertAutoScanRecord(ADatetime_in: TDateTime;
      AEmployeeNumber: Integer; APlantCode, AWorkspotCode, AJobCode: String;
      AShiftNumber: Integer; ADatetime_out: TDateTime);
    // RV082.4. Not needed.
{    procedure InsertProdHourPerEmployeeRecord(ADate: TDateTime;
      APlantCode: String; AShiftNumber, AEmployeeNumber: Integer;
      AWorkspotCode: String; AProdMin, ABreaksMin: Integer; AJobCode,
      AManualYN: String); }
    procedure DeleteSalaryHoursForSelectedNoScanEmployees;
    procedure CorrectionBalanceAbsenceHours;
{    procedure CheckRecalcPHEPT(AEmployeeNumber: Integer;
      ADateFrom, ADateTo: TDateTime); }
    function ActionTFTHolidayCorrection: Boolean;
    function UpdateAHE(AAbsenceReasonCode, APlantCode: String;
      ADate: TDateTime; AEmployeeNumber, AShiftNumber,
      AAbsenceMinute: Integer): Boolean;
  public
    { Public declarations }
    property StartDate: TdateTime read FStartDate write FStartDate;
    property EndDate: TDateTime read FEndDate write FEndDate;
    property MyPlant: String read FMyPlant write FMyPlant;
    property MyEmployee: String read FMyEmployee write FMyEmployee;
    property PlantFrom: String read FPlantFrom write FPlantFrom;
    property PlantTo: String read FPlantTo write FPlantTo;
    property TeamFrom: String read FTeamFrom write FTeamFrom;
    property TeamTo: String read FTeamTo write FTeamTo;
    property EmployeeFrom: Integer read FEmployeeFrom write FEmployeeFrom;
    property EmployeeTo: Integer read FEmployeeTo write FEmployeeTo;
  end;

var
  DialogProcessAbsenceHrsF: TDialogProcessAbsenceHrsF;

// RV089.1.
function DialogProcessAbsenceHrsForm: TDialogProcessAbsenceHrsF;

implementation

{$R *.DFM}

uses
  DialogProcessAbsenceHrsDMT,
  UGlobalFunctions, UPimsConst, UPimsMessageRes, ListProcsFRM,
  CalculateTotalHoursDMT, SideMenuUnitMaintenanceFRM
  {, ReGlobalDMT}; // RV040.1.

// RV089.1.
var
  DialogProcessAbsenceHrsF_HND: TDialogProcessAbsenceHrsF;

// RV089.1.
function DialogProcessAbsenceHrsForm: TDialogProcessAbsenceHrsF;
begin
  if (DialogProcessAbsenceHrsF_HND = nil) then
  begin
    DialogProcessAbsenceHrsF_HND := TDialogProcessAbsenceHrsF.Create(Application);
    with DialogProcessAbsenceHrsF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogProcessAbsenceHrsF_HND;
end;

// RV089.1.
procedure TDialogProcessAbsenceHrsF.FormDestroy(Sender: TObject);
begin
  inherited;
  // RV089.1.
//  ActiveTables(DialogProcessAbsenceHrsDM, False);
  ChangedEmployeeList.Free;
  DialogProcessAbsenceHrsDM.Free;
  if (DialogProcessAbsenceHrsF_HND <> nil) then
  begin
    DialogProcessAbsenceHrsF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogProcessAbsenceHrsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogProcessAbsenceHrsF.Log(AMsg: String; AError: Boolean=False);
begin
  if AError then
    WErrorLog(AMsg)
  else
    WLog(AMsg);

  AMsg := DateTimeToStr(Now) + ' - ' + AMsg;
  mmLog.Lines.Add(AMsg);
end;

procedure TDialogProcessAbsenceHrsF.ProcessbarUpdate;
begin
  pBar.Position := pBar.Position + (100 DIV NUMBER_OF_PROCEDURES);
  Update;
end;

procedure TDialogProcessAbsenceHrsF.FormShow(Sender: TObject);
begin
  InitDialog(True, False, True, True, False, False, False);
  // RV053.2.
  if MyPlant <> '' then
    SearchPlant := MyPlant;
  inherited;
  DateFrom.Date := Date;
  DateTo.Date := Date;
  // RV089.1.
//  ActiveTables(DialogProcessAbsenceHrsDM, True);
  // MR:09-12-2003
  if (StartDate <> 0) and (EndDate <> 0) then
  begin
    DateFrom.Date := StartDate;
    DateTo.Date := EndDate;
  end;
  // RV053.2.
  if MyEmployee <> '' then
    SearchEmployee := MyEmployee;
  // 20011800
  DateFromCloseUp(Sender);
  // 20011800.70
  if SystemDM.UseFinalRun then
    CmbPlusPlantFromCloseUp(Sender);
end;

procedure TDialogProcessAbsenceHrsF.btnCancelClick(Sender: TObject);
begin
  inherited;
  Close;
end;

// TD-22355 SPECIAL NOTE: This procedure is changing AHE-records,
//                        based on 'IsAbsence' !
function TDialogProcessAbsenceHrsF.DetermineTimeBlocks(
  TotalAvailMinutes: Integer;
  VAR TotalSalaryMinutes: Integer;
  VAR TotalMinutes: Integer;
  VAR TimeBlockLength: Integer;
  SelectedTimeBlock, WeekDay: Integer;
  AvailableTimeBlock: String;
  VAR ChangeCount, AddCount: Integer;
  IsAbsence: Boolean): Boolean;
var
  AbsenceReason: String;
  AStart, AEnd: TDateTime;
  AEmployeeData: TScannedIDCard;
  ProdMin, BreaksMin, PayedBreaks: Integer;
  ErrorMessageLine: String;
  CorrectWorkHrs: Boolean; // PIM-55
begin
  Result := False;
  AbsenceReason := '';
  CorrectWorkHrs := False; // PIM-55
  with DialogProcessAbsenceHrsDM do
  begin
    ATBLengthClass.FillTBLength(
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      Query.FieldByName('SHIFT_NUMBER').AsInteger,
      Query.FieldByName('PLANT_CODE').AsString,
      Query.FieldByName('DEPARTMENT_CODE').AsString,
      True);
    ATBLengthClass.AEmployeeNumber :=
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    ATBLengthClass.APlantCode :=
      Query.FieldByName('PLANT_CODE').AsString;
    ATBLengthClass.AShiftNumber :=
      Query.FieldByName('SHIFT_NUMBER').AsInteger;
    ATBLengthClass.ADepartmentCode :=
      Query.FieldByName('DEPARTMENT_CODE').AsString;
    ATBLengthClass.ARemoveNotPaidBreaks := False;
    // MR:04-01-2007 Order 550436. Error during processing.
    // When timeblocks are not correctly defined in Pims, an error
    // arises. This will trap this error and gives a message about it.
    // All will be processed, except for these errors.
    try
      AStart := Trunc(Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime) +
        Frac(ATBLengthClass.GetStartTime(SelectedTimeBlock, WeekDay));
      AEnd := Trunc(Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime) +
        Frac(ATBLengthClass.GetEndTime(SelectedTimeBlock, WeekDay));
      // RV025. If start <> end (can happen when both are defined as 00:00.
      //        Then if end-time was set as 00:00, it will be smaller than
      //        start, then the end must be set a day further.
{$IFDEF DEBUG1}
(*  WDebugLog('AStart=' + DateTimeToStr(AStart) +
    ' AEnd=' + DateTimeToStr(AEnd)
    ); *)
{$ENDIF}
      if (AStart <> AEnd) then
        if (AStart > AEnd) then
          AEnd :=
            Trunc(Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime + 1) +
              Frac(AEnd); // RV027. Also add AEnd (timepart) !
    except
      // Error: Wrong timeblock for employee
      ErrorMessageLine := SErrorWrongTimeBlock + ' ' +
        IntToStr(ATBLengthClass.AEmployeeNumber);
      Log(ErrorMessageLine, True);
      FMyErrorMessage := FMyErrorMessage + ErrorMessageLine + #13;
      FMyError := True;
      Exit;
    end;

    EmptyIDCard(AEmployeeData);
    AEmployeeData.EmployeeCode :=
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    AEmployeeData.PlantCode :=
      Query.FieldByName('PLANT_CODE').AsString;
    AEmployeeData.ShiftNumber :=
      Query.FieldByName('SHIFT_NUMBER').AsInteger;
    AEmployeeData.DepartmentCode :=
      Query.FieldByName('DEPARTMENT_CODE').AsString;
    // MRA:23-OCT-2008 Start
    // The ProdMin is without breaks.
    // Be sure to add the PayedBreaks, if there are any (not 0).

    // Get the ProdMin (without breaks).
    AProdMinClass.ComputeBreaks(AEmployeeData, AStart, AEnd, 0,
      ProdMin, BreaksMin, PayedBreaks);
    // MRA:14-MAY-2009 RV027. Absence + breaks problem.
    // Add the breaks! This is about Absence-hours (like Holiday/Vacation)
    // MRA:8-OCT-2009 RV038.
    if SystemDM.ExportPayrollType = 'PARIS' then
      TimeBlockLength := ProdMin + BreaksMin
    else
      TimeBlockLength := ProdMin + PayedBreaks;

    // MRA:23-OCT-2008 End

    // Read AbsenceReason for AbsenceReasonType_Code
    // This is used during procedure UpdateABSENCETOTAL
    if TimeBlockLength <= 0 then
      Exit; //do not update/insert negative time.
    // TD-22355
    if IsAbsence then
    begin
      qryAbsenceReason.Close;
      qryAbsenceReason.ParamByName('ABSENCEREASON_CODE').AsString :=
        AvailableTimeBlock;
      qryAbsenceReason.Open;
      if not qryAbsenceReason.Eof then
      begin
        CorrectWorkHrs := qryAbsenceReason.FieldByName('CORRECT_WORK_HRS_YN').AsString = 'Y'; // PIM-55
        AbsenceReason := qryAbsenceReason.FieldByName('ABSENCEREASON_CODE').AsString;
      end
      else
      begin
        CorrectWorkHrs := False; // PIM-55
        AbsenceReason := NullStr;
      end;
      qryAbsenceReason.Close;
    end;

    // RV074.1. Bugfix.
    if IsAbsence and (AbsenceReason <> '') then
    begin
{$IFDEF DEBUG}
  WDebugLog(
    ' AvailableTimeBlock=[' + AvailableTimeBlock + ']' +
    ' AbsenceReason=[' + AbsenceReason + ']' +
    ' ProdMin=' + IntToStr(ProdMin) +
    ' BreaksMin=' + IntToStr(BreaksMin) +
    ' PayedBreaks=' + IntToStr(PayedBreaks) +
    ' CorrectWrkHrs=' + Bool2Str(CorrectWorkHrs) +
    ' TotalAvailMin=' + IntToStr(TotalAvailMinutes) +
    ' TotalSalMin=' + IntToStr(TotalSalaryMinutes) +
    ' TimeBlockLength=' + IntToStr(TimeBlockLength)
    );
{$ENDIF}

      // PIM-55
      if CorrectWorkHrs then
      begin
        TotalMinutes := TotalMinutes + TimeBlockLength;
        if TotalSalaryMinutes > 0 then
        begin
          if TotalSalaryMinutes + TotalMinutes > TotalAvailMinutes then
          begin
            if TimeBlockLength > (TotalAvailMinutes - TotalSalaryMinutes) then
            begin
              TimeBlockLength := TotalAvailMinutes - TotalSalaryMinutes;
              if TimeBlockLength < 0 then
                TimeBlockLength := 0;
            end
            else
              TimeBlockLength := 0;
          end;
          // PIM-55 Do this within the if, or it goes
          //        wrong when there was no salary, resulting in less absence!
          TotalSalaryMinutes := TotalSalaryMinutes + TimeBlockLength;
        end;
      end; // if

      if UpdateAHE(AbsenceReason,
        Query.FieldByName('PLANT_CODE').Value,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value,
        Query.FieldByName('EMPLOYEE_NUMBER').Value,
        Query.FieldByName('SHIFT_NUMBER').Value,
        TimeBlockLength) then
        inc(ChangeCount);
      Result := True;
    end; // if
  end; // with
end; // DetermineTimeBlocks

procedure TDialogProcessAbsenceHrsF.ActionAbsenceHourPerEmployee;
var
  ChangeCount: Integer;
  DeleteCount: Integer;
  Year, Month, Day: Word;
  SelectStr: String;
  MyAbsenceType: Char; // 20013183
begin
  // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
  Log('Start: Defining Absence Hours Per Employee');
  ChangeCount := 0;
  DeleteCount := 0;
  with DialogProcessAbsenceHrsDM do
  begin
    // Delete ABSENCEHOURPEREMPLOYEE-records if needed
    // TD-22355 Optimising. Get all needed fields from 1 query.
    SelectStr :=
      'SELECT ' + NL +
      '  AH.ABSENCEHOUR_DATE, AH.EMPLOYEE_NUMBER, ' + NL +
      '  R.ABSENCEREASON_ID, R.ABSENCETYPE_CODE, ' + NL + // TD-22355
      '  R.ABSENCEREASON_CODE, AH.MANUAL_YN, AH.ABSENCE_MINUTE ' + NL +
      'FROM ' + NL +
      '  ABSENCEHOURPEREMPLOYEE AH INNER JOIN EMPLOYEE E ON ' + NL +
      '    AH.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
      '  INNER JOIN ABSENCEREASON R ON ' + NL +
      '    AH.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL +
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' +
      '  (E.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
      ') AND ' +
      '  AH.MANUAL_YN = :MANUAL_YN AND ' + NL +
      '  AH.ABSENCEHOUR_DATE >= :DATEFROM AND ' + NL +
      '  AH.ABSENCEHOUR_DATE <= :DATETO AND ' + NL +
      '  E.PLANT_CODE >= :PLANTFROM AND ' + NL +
      '  E.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  E.TEAM_CODE >= :TEAMFROM AND ' + NL +
      '  E.TEAM_CODE <= :TEAMTO AND ' + NL +
      '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO ' + NL +
      'ORDER BY AH.EMPLOYEE_NUMBER, AH.ABSENCEHOUR_DATE '; // TD-22355
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
//Query.SQL.SaveToFile('c:\temp\absencehourperemployee.sql');
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      Query.ParamByName('USER_NAME').AsString := '*';
    Query.ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
    Query.ParamByName('DATEFROM').AsDate := StartDate;
    Query.ParamByName('DATETO').AsDate := EndDate;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;
    Log(SLogEmpNo + IntToStr(Query.RecordCount));
    Query.First;
    while not Query.Eof do
    begin
      // Before deleting check/change balance of ABSENCETOTAL
      // TD-22355 Get this from Query.
{      tblAbsenceReason.Locate('ABSENCEREASON_CODE', VarArrayOf([
        Query.FieldByName('ABSENCEREASON_CODE').AsString[1]]), []); }
      DecodeDate(Query.FieldByName('ABSENCEHOUR_DATE').AsDateTime,
        Year, Month, Day);
      // 20013183 Any absence hours booked on travel time must be
      //          booked also in balance in 'used travel time'.
      // TD-22355 Get this from Query.
//      MyAbsenceType := tblAbsenceReasonABSENCETYPE_CODE.AsString[1];
      MyAbsenceType := Query.FieldByName('ABSENCETYPE_CODE').AsString[1];
      if MyAbsenceType = TRAVELTIME then
        MyAbsenceType := USED_TRAVELTIME;
      UpdateAbsenceTotalMode(Query,
        Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        Year,
        Query.FieldByName('ABSENCE_MINUTE').AsInteger * -1, // Subtract minutes!
        MyAbsenceType);
      // TD-22355 Optimise.
      with qryDELAHE do
      begin
        ParamByName('ABSENCEHOUR_DATE').AsDateTime :=
          Query.FieldByName('ABSENCEHOUR_DATE').AsDateTime;
        ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        ParamByName('ABSENCEREASON_ID').AsInteger :=
          Query.FieldByName('ABSENCEREASON_ID').AsInteger;
        ParamByName('MANUAL_YN').AsString :=
          Query.FieldByName('MANUAL_YN').AsString;
        ExecSQL;
        if ChangedEmployeeList.IndexOf(
          Query.FieldByName('EMPLOYEE_NUMBER').AsString) = -1 then // Not found?
          ChangedEmployeeList.Add(Query.FieldByName('EMPLOYEE_NUMBER').AsString); //save changed employees for salaryhours recalculation.
        inc(DeleteCount);
      end;
{
      // Now delete ABSENCEHOURPEREMPLOYEE-record
      if tblAbsenceHourPerEmployee.FindKey(
        [Query.FieldByName('ABSENCEHOUR_DATE').Value,
         Query.FieldByName('EMPLOYEE_NUMBER').Value,
         //tblAbsenceReason.FieldByName('ABSENCEREASON_ID').Value, // TD-22355
         Query.FieldByName('ABSENCEREASON_ID').Value, // TD-22355
         Query.FieldByName('MANUAL_YN').Value]) then
      begin
        if ChangedEmployeeList.IndexOf(
          IntToStr(Query.FieldByName('EMPLOYEE_NUMBER').AsInteger)) = -1 then // Not found?
          ChangedEmployeeList.Add(IntToStr(Query.FieldByName('EMPLOYEE_NUMBER').
            AsInteger)); //save changed employees for salaryhours recalculation.
        tblAbsenceHourPerEmployee.Delete;
        inc(DeleteCount);
      end;
}
      Query.Next;
    end;
    Query.Close;
  end;
  if ChangeCount > 0 then
    Log(Format(SLogChangedAbsenceTotal, [ChangeCount]));
  if DeleteCount > 0 then
    Log(Format(SLogDeletedRec, [DeleteCount]));
  Log(SLogEndAbsence);
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.ActionSalaryHourPerEmployee;
var
  Year, Month, Day: Word;
  ChangeCount, DeleteCount: Integer;
  SelectStr: String;
  IsUpdated: Boolean;
  TimeForTimeYN: String; // RV045.1.
  BonusInMoneyYN: String; // RV045.1.
  SalaryMinute: Double;
begin
  // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
  Log('Start: Defining Earned TFT Hours Per Employee based on Salary Hour');
  ChangeCount := 0;
  DeleteCount := 0;
  with DialogProcessAbsenceHrsDM do
  begin
    // Delete SALARYHOURPEREMPLOYEE-records if needed
    // TD-22355 Performance issue.
    //          Get most info from query, instead of doing that afterwards!
    SelectStr :=
      'SELECT A.* ' + NL +
      'FROM ' + NL +
      '( ' + NL +
      'SELECT ' + NL +
      '  SH.SALARY_DATE, SH.EMPLOYEE_NUMBER, SH.HOURTYPE_NUMBER, ' + NL +
      '  SH.MANUAL_YN, EM.CONTRACTGROUP_CODE, ' + NL +
      '  CASE ' + NL +
      '    WHEN :CG_TFT = ''Y'' THEN ' + NL +
      '      CG.TIME_FOR_TIME_YN ' + NL +
      '    ELSE ' + NL +
      '      H.TIME_FOR_TIME_YN ' + NL +
      '  END TIME_FOR_TIME_YN, ' + NL +
      '  CASE ' + NL +
      '    WHEN :CG_TFT = ''Y'' THEN ' + NL +
      '      CG.BONUS_IN_MONEY_YN ' + NL +
      '    ELSE ' + NL +
      '      H.BONUS_IN_MONEY_YN ' + NL +
      '  END BONUS_IN_MONEY_YN, ' + NL +
      '  H.BONUS_PERCENTAGE, ' + NL +
      '  SH.SALARY_MINUTE ' + NL +
      'FROM ' + NL +
      '  SALARYHOURPEREMPLOYEE SH INNER JOIN EMPLOYEE EM ON ' + NL +
      '    SH.EMPLOYEE_NUMBER = EM.EMPLOYEE_NUMBER ' + NL +
      '  INNER JOIN CONTRACTGROUP CG ON ' + NL +
      '    CG.CONTRACTGROUP_CODE = EM.CONTRACTGROUP_CODE ' + NL +
      '  LEFT JOIN HOURTYPE H ON ' + NL +
      '    SH.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER ' + NL +
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' + NL +
      '  (EM.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' + NL +
      ') AND ' + NL +
      '  SH.MANUAL_YN = :MANUAL_YN AND ' + NL +
      '  SH.SALARY_DATE >= :DATEFROM AND ' + NL +
      '  SH.SALARY_DATE <= :DATETO AND ' + NL +
      '  EM.PLANT_CODE >= :PLANTFROM AND ' + NL +
      '  EM.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
      '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
      '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL +
      '  H.OVERTIME_YN = ''Y'' ' + NL +
      'ORDER BY ' + NL +
      '  SH.SALARY_DATE, SH.EMPLOYEE_NUMBER, SH.HOURTYPE_NUMBER ' + NL +
      ') A ' + NL +
      'WHERE (A.TIME_FOR_TIME_YN = ''Y'' AND A.BONUS_IN_MONEY_YN = ''Y'') ' + NL +
      'OR (A.TIME_FOR_TIME_YN = ''Y'' AND A.BONUS_IN_MONEY_YN = ''N'') ' + NL;
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      Query.ParamByName('USER_NAME').AsString := '*';
    Query.ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
    Query.ParamByName('DATEFROM').AsDate := StartDate;
    Query.ParamByName('DATETO').AsDate := EndDate;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    if DialogProcessAbsenceHrsDM.ContractGroupTFT then // TD-22355
      Query.ParamByName('CG_TFT').AsString := 'Y'
    else
      Query.ParamByName('CG_TFT').AsString := 'N';
    Query.Open;
    Query.First;
    while not Query.Eof do
    begin
      IsUpdated := False;
      DecodeDate(Query.FieldByName('SALARY_DATE').Value,
        Year, Month, Day);
      // Update earned TFT in Absence Total
      TimeForTimeYN := Query.FieldByName('TIME_FOR_TIME_YN').AsString;
      BonusInMoneyYN := Query.FieldByName('BONUS_IN_MONEY_YN').AsString;
      SalaryMinute := 0;
      if (TimeForTimeYN = CHECKEDVALUE) and
        (BonusInMoneyYN = CHECKEDVALUE) then
        SalaryMinute := Query.FieldByName('SALARY_MINUTE').Value
      else
        if (TimeForTimeYN = CHECKEDVALUE) and
          (BonusInMoneyYN = UNCHECKEDVALUE) then
          SalaryMinute :=
             Query.FieldByName('SALARY_MINUTE').Value *
                ((1 + Query.FieldByName('BONUS_PERCENTAGE').Value) / 100);
      if SalaryMinute <> 0 then
      begin
        try
          with qryAbsTotTFTUpdate do
          begin
            ParamByName('EMPLOYEE_NUMBER').AsInteger :=
              Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            ParamByName('ABSENCE_YEAR').AsInteger :=
              Year;
            ParamByName('EARNED_TFT_MINUTE').AsFloat :=
              SalaryMinute;
            ParamByName('MUTATOR').AsString :=
              SystemDM.CurrentProgramUser;
            ExecSQL;
            inc(ChangeCount);
            IsUpdated := True;
          end; // with
        except
          // Trap errors here.
        end; // try
      end; // if
      if IsUpdated then
      begin
        // Now delete SALARYHOURPEREMPLOYEE-record
        try
          with qryDELSHE do
          begin
            ParamByName('SALARY_DATE').AsDateTime :=
              Query.FieldByName('SALARY_DATE').AsDateTime;
            ParamByName('EMPLOYEE_NUMBER').AsInteger :=
              Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            ParamByName('HOURTYPE_NUMBER').AsInteger :=
              Query.FieldByName('HOURTYPE_NUMBER').AsInteger;
            ParamByName('MANUAL_YN').AsString :=
              Query.FieldByName('MANUAL_YN').AsString;
            ExecSQL;
            inc(DeleteCount);
            if ChangedEmployeeList.IndexOf(
              Query.FieldByName('EMPLOYEE_NUMBER').AsString) = -1 then // Not found?
              ChangedEmployeeList.Add(Query.FieldByName('EMPLOYEE_NUMBER').AsString); //save changed employees for salaryhours recalculation.
          end;
        except
          // Trap errors here.
        end;
      end;
      Query.Next;
    end; // while
    Query.Close;
  end; // with
  if ChangeCount > 0 then
    Log(Format(SLogChangedAbsenceTotal,[ChangeCount]));
  if DeleteCount > 0 then
    Log(Format(SLogDeletedRec,[DeleteCount]));
  Log('End: Defining Earned TFT Hours Per Employee based on Salary Hour');
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.ActionEmployeeAvailability(
  AMyDate: TDateTime=0); // PIM-55
var
  WeekDay: Word;
  TimeBlockLength: integer;
  ChangeCount, AddCount: integer;
  TimeBlockNumber: Integer;
  AvailableTimeBlock: array[1..MAX_TBS] of String;
  SelectStr: String;
  TotalMinutes: Integer; // PIM-55
  TotalSalaryMinutes: Integer; // PIM-55
  TotalAvailMinutes: Integer; // PIM-55
begin
  // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
  // MRA:27-01-2009 RV021. Wrong selection of TEAM + TEAMPERUSER-combination!
  //                       This gave multiple vacation-records!
  Log(SLogStartAvailability);
  ChangeCount := 0;
  AddCount := 0;
  with DialogProcessAbsenceHrsDM do
  begin
    // 20012085.1 Correct absence hours
    // Also get CORRECT_ABSENCE_HOURS_YN and ABSENCE_MINUTE_PER_DAY and
    // CONTRACTGROUP_CODE
    // 20012085.2
   SelectStr :=
      'SELECT ' + NL +
      '  EA.PLANT_CODE, EA.EMPLOYEEAVAILABILITY_DATE, EA.SHIFT_NUMBER, ' + NL +
      '  EA.EMPLOYEE_NUMBER, EA.AVAILABLE_TIMEBLOCK_1, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_2, EA.AVAILABLE_TIMEBLOCK_3, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_4, EA.AVAILABLE_TIMEBLOCK_5, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_6, EA.AVAILABLE_TIMEBLOCK_7, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_8, EA.AVAILABLE_TIMEBLOCK_9, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_10, EM.DEPARTMENT_CODE, ' + NL +
      '  CG.CONTRACTGROUP_CODE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE EM ON ' + NL +
      '    EA.EMPLOYEE_NUMBER = EM.EMPLOYEE_NUMBER ' + NL +
      '  INNER JOIN CONTRACTGROUP CG ON ' + NL +
      '    EM.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE ' + NL +
      '  INNER JOIN PLANT P ON ' + NL +
      '    EM.PLANT_CODE = P.PLANT_CODE ' + NL +
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' +
      '  (EM.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
      ') AND ' +
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND ' + NL +
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND ' + NL +
      '  EA.PLANT_CODE >= :PLANTFROM AND ' + NL +
      '  EA.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
      '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
      '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO ';
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
// Query.SQL.SaveToFile('c:\temp\actionempavail.sql');
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      Query.ParamByName('USER_NAME').AsString := '*';
    // PIM-55 Only look for 1 date when AMyDate is not 0.
    // This can be called to corret absence hours when worked hours were found.
    if AMyDate = 0 then // PIM-55
    begin
      Query.ParamByName('DATEFROM').AsDate := StartDate;
      Query.ParamByName('DATETO').AsDate := EndDate;
    end
    else
    begin
      Query.ParamByName('DATEFROM').AsDate := AMyDate; // PIM-55
      Query.ParamByName('DATETO').AsDate := AMyDate; // PIM-55
    end;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;
    Log(Format(SLogFoundEmployeeNo,[Query.RecordCount]));
    Query.First;
    while not Query.Eof do
    begin
      // MR:20-10-2004 Order 550335 First look if this record
      // is in a clientdataset, because of 'illnessmessage'.
      // AvailableTimeBlock-array is filled with 'Query'-values,
      // or values from 'cdsEmpAval'.
      SearchCdsEmpAvail(Query.FieldByName('PLANT_CODE').AsString,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime,
        Query.FieldByName('SHIFT_NUMBER').AsInteger,
        Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString,
        AvailableTimeBlock[1], AvailableTimeBlock[2],
        AvailableTimeBlock[3], AvailableTimeBlock[4],
        AvailableTimeBlock[5], AvailableTimeBlock[6],
        AvailableTimeBlock[7], AvailableTimeBlock[8],
        AvailableTimeBlock[9], AvailableTimeBlock[10]
        );

      if ChangedEmployeeList.IndexOf(
        Query.FieldByName('EMPLOYEE_NUMBER').AsString) = -1 then // Not found?
        ChangedEmployeeList.Add(
          Query.FieldByName('EMPLOYEE_NUMBER').AsString);

      // Get day of week - depends on 'Week Starts On'
      WeekDay := DayInWeek(SystemDM.WeekStartsOn,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value);

      // PIM-55
      TotalAvailMinutes := ComputeTotalPlannedDayTime(qrySHE,
        True, WeekDay, Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        Query.FieldByName('SHIFT_NUMBER').AsInteger,
        Query.FieldByName('PLANT_CODE').AsString,
        Query.FieldByName('DEPARTMENT_CODE').AsString);

{$IFDEF DEBUG}
  WDebugLog('TotalAvailMinutes=' + IntToStr(TotalAvailMinutes));
{$ENDIF}

      TimeBlockLength := 0;

      // PIM-55 The salary is NOT known here! It is calculated later!
      TotalMinutes := 0;
      TotalSalaryMinutes := SalaryCheck(
        Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime);
      for TimeBlockNumber := 1 to SystemDM.MaxTimeblocks do
      begin
        if (AvailableTimeBlock[TimeBlockNumber] <> '') and
          (AvailableTimeBlock[TimeBlockNumber] <> '-') and
          (AvailableTimeBlock[TimeBlockNumber] <> DEFAULTAVAILABILITYCODE) then
          // SPECIAL NOTE: This procedure is changing AHE-records!
          DetermineTimeBlocks(
            TotalAvailMinutes,
            TotalSalaryMinutes, TotalMinutes,
            TimeBlockLength,
            TimeBlockNumber, WeekDay,
            AvailableTimeBlock[TimeBlockNumber],
            ChangeCount, AddCount, True);
      end;
      Query.Next;
    end;
    Query.Close;
  end;
  if ChangeCount > 0 then
    Log(Format(SLogChangedAbsence,[ChangeCount]));
  if AddCount > 0 then
    Log(Format(SlogAddedAbsence,[AddCount]));
  Log(SLogEndAvailability);
  ProcessbarUpdate;
end; // ActionEmployeeAvailability

//begin RV067.12.

procedure TDialogProcessAbsenceHrsF.DeleteSalaryHoursForSelectedNoScanEmployees;
var
  SelectStr: String;
  procedure DeleteSalaryHoursForNoScan(AEmployeeNumber: Integer);
  begin
    with DialogProcessAbsenceHrsDM.qryDeleteSHE do
    begin
      ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
      ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
      ParamByName('STARTDATE').AsDate := StartDate;
      ParamByName('ENDDATE').AsDate := EndDate;
      ExecSQL;
    end;
  end;
{
  procedure DeleteSalaryHoursForNoScan(AEmployeeNumber: Integer);
  var
    qry: TQuery;
  begin
    qry := TQuery.Create(nil);
    try
      qry.SessionName := SystemDM.SessionPims.Name;
      qry.DataBaseName := SystemDM.Pims.Name;
      qry.SQL.Text :=
        'DELETE FROM SALARYHOURPEREMPLOYEE S ' + NL +
        'WHERE ' + NL +
        '  S.EMPLOYEE_NUMBER = :EMPNO ' + NL +
        '  AND S.MANUAL_YN = ''N'' ' + NL +
        '  AND (:STARTDATE <= S.SALARY_DATE) AND (S.SALARY_DATE <= :ENDDATE) ';
      qry.ParamByName('STARTDATE').AsDate := StartDate;
      qry.ParamByName('ENDDATE').AsDate := EndDate;
      qry.ParamByName('EMPNO').AsInteger := AEmployeeNumber;
      qry.ExecSQL;
    finally
      qry.Free;
    end;
  end;
}
begin
  with DialogProcessAbsenceHrsDM do
  begin
    // RV075.1. Bugfix.
    // Only do this only for combination:
    // EMPLOYEE.IS_SCANNING_YN = 'N' AND EMPLOYEE.BOOK_PROD_HRS_YN = 'N'
    SelectStr :=
      'SELECT DISTINCT ' + NL +
      '  EA.EMPLOYEE_NUMBER ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE EM ON ' + NL +
      '    EA.EMPLOYEE_NUMBER = EM.EMPLOYEE_NUMBER ' + NL +
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' +
      '  (EM.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
      ') AND ' +
      '  EM.IS_SCANNING_YN = :IS_SCANNING_YN AND ' + NL +
      '  EM.BOOK_PROD_HRS_YN = :BOOK_PROD_HRS_YN AND ' + NL +
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND ' + NL +
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND ' + NL +
      '  EA.PLANT_CODE >= :PLANTFROM AND ' + NL +
      '  EA.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
      '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
      '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO ';
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
    Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    Query.ParamByName('IS_SCANNING_YN').AsString := UNCHECKEDVALUE;
    // RV075.1.
    Query.ParamByName('BOOK_PROD_HRS_YN').AsString := UNCHECKEDVALUE;
    Query.ParamByName('DATEFROM').AsDate := StartDate;
    Query.ParamByName('DATETO').AsDate := EndDate;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;
    Query.First;
    while not Query.Eof do
    begin
      DeleteSalaryHoursForNoScan(Query.FieldByName('EMPLOYEE_NUMBER').AsInteger);
      Query.Next;
    end;
    // TD-22355 TTables are not used anymore.
//    tblSalaryHourPerEmployee.Refresh;
  end;
end;
//end RV067.12.
procedure TDialogProcessAbsenceHrsF.ActionEmployeeAvailabilityNoScanEmployees;
var
  WeekDay: Word;
  TimeBlockLength: Integer;
  TimeBlockLengthTotal: Integer;
  ChangeCount, AddCount: Integer;
  ManualAbsenceMinute: Integer;
  TimeBlockNumber: Integer;
  AvailableTimeBlock: array[1..MAX_TBS] of String;
  SelectStr: String;
  EmployeeData: TScannedIDCard; // RV104.1.
  TotalSalaryMinutesDummy, TotalMinutesDummy: Integer; // PIM-55
begin
  // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
  Log(SLogStartNonScanning);
  ChangeCount := 0;
  AddCount := 0;
  //RV067.12.
  DeleteSalaryHoursForSelectedNoScanEmployees;
  with DialogProcessAbsenceHrsDM do
  begin
    // RV075.1.
    // Only do this only for combination:
    // EMPLOYEE.IS_SCANNING_YN = 'N' AND EMPLOYEE.BOOK_PROD_HRS_YN = 'N'
    SelectStr :=
      'SELECT ' + NL +
      '  EA.PLANT_CODE, EA.EMPLOYEEAVAILABILITY_DATE, EA.SHIFT_NUMBER, ' + NL +
      '  EA.EMPLOYEE_NUMBER, EA.AVAILABLE_TIMEBLOCK_1, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_2, EA.AVAILABLE_TIMEBLOCK_3, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_4, EA.AVAILABLE_TIMEBLOCK_5, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_6, EA.AVAILABLE_TIMEBLOCK_7, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_8, EA.AVAILABLE_TIMEBLOCK_9, ' + NL +
      '  EA.AVAILABLE_TIMEBLOCK_10, EM.DEPARTMENT_CODE ' + NL +
      'FROM ' + NL +
      '  EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE EM ON ' + NL +
      '    EA.EMPLOYEE_NUMBER = EM.EMPLOYEE_NUMBER ' + NL +
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' +
      '  (EM.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
      ') AND ' +
      '  EM.IS_SCANNING_YN = :IS_SCANNING_YN AND ' + NL +
      '  EM.BOOK_PROD_HRS_YN = :BOOK_PROD_HRS_YN AND ' + NL +
      '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND ' + NL +
      '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND ' + NL +
      '  EA.PLANT_CODE >= :PLANTFROM AND ' + NL +
      '  EA.PLANT_CODE <= :PLANTTO AND ' + NL +
      '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
      '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
      '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO ';
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
    Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    Query.ParamByName('IS_SCANNING_YN').AsString := UNCHECKEDVALUE;
    // RV075.1.
    Query.ParamByName('BOOK_PROD_HRS_YN').AsString := UNCHECKEDVALUE;
    Query.ParamByName('DATEFROM').AsDate := StartDate;
    Query.ParamByName('DATETO').AsDate := EndDate;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;
    Log('Found Number of Records=' +
      IntToStr(Query.RecordCount));
    Query.First;
    while not Query.Eof do
    begin
      // MR:20-10-2004 Order 550335 First look if this record
      // is in a clientdataset, because of 'illnessmessage'.
      // AvailableTimeBlock-array is filled with 'Query'-values,
      // or values from 'cdsEmpAval'.
      SearchCdsEmpAvail(Query.FieldByName('PLANT_CODE').AsString,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime,
        Query.FieldByName('SHIFT_NUMBER').AsInteger,
        Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString,
        Query.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString,
        AvailableTimeBlock[1], AvailableTimeBlock[2],
        AvailableTimeBlock[3], AvailableTimeBlock[4],
        AvailableTimeBlock[5], AvailableTimeBlock[6],
        AvailableTimeBlock[7], AvailableTimeBlock[8],
        AvailableTimeBlock[9], AvailableTimeBlock[10]
        );
      // Get day of week - depends on 'Week Starts On'
      WeekDay := DayInWeek(SystemDM.WeekStartsOn,
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value);

      TimeBlockLength := 0;
      TimeBlockLengthTotal := 0;
      for TimeBlockNumber := 1 to SystemDM.MaxTimeblocks do
      begin
        if AvailableTimeBlock[TimeBlockNumber] = DEFAULTAVAILABILITYCODE then
        begin
          DetermineTimeBlocks(0, TotalSalaryMinutesDummy, TotalMinutesDummy,
            TimeBlockLength, TimeBlockNumber, WeekDay,
            AvailableTimeBlock[TimeBlockNumber],
            ChangeCount, AddCount, False);
          TimeBlockLengthTotal := TimeBlockLengthTotal + TimeBlockLength;
        end;
      end;
      TimeBlockLength := TimeBlockLengthTotal;

      if TimeBlockLength > 0 then
      begin
        // Determine ManualAbsenceHours
        ManualAbsenceMinute := 0;
        qryAbsenceHourPerEmployee.ParamByName('ABSENCEHOUR_DATE').AsDate :=
          Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value;
        qryAbsenceHourPerEmployee.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          Query.FieldByName('EMPLOYEE_NUMBER').Value;
        qryAbsenceHourPerEmployee.ParamByName('MANUAL_YN').AsString :=
          CHECKEDVALUE;
        qryAbsenceHourPerEmployee.Open;
        qryAbsenceHourPerEmployee.First;
        while not qryAbsenceHourPerEmployee.Eof do
        begin
          ManualAbsenceMinute := ManualAbsenceMinute +
            qryAbsenceHourPerEmployee.FieldByName('ABSENCE_MINUTE').Value;
          qryAbsenceHourPerEmployee.Next;
        end;
        qryAbsenceHourPerEmployee.Close;

        // RV033.2. First remove the already existing salary hours!
        // Delete and Insert SalaryHourPerEmployee-record.
        // First delete existing record.
(*RV067.12. this is now done in DeleteSalaryHoursForSelectedNoScanEmployees
        if tblSalaryHourPerEmployee.FindKey(
          [Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value,
           Query.FieldByName('EMPLOYEE_NUMBER').Value,
           1,
           UNCHECKEDVALUE]) then
          tblSalaryHourPerEmployee.Delete;
*)
        // RV075.1. START
        // The salary hours are changed here, but not the PHEPT-hours!

      // RV104.1. First check/book hours based on worked-on-bank-holiday
      //          When not found (<> 0) then book them just as regular hours.
      EmptyIDCard(EmployeeData);
      EmployeeData.EmployeeCode :=
        Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      if GlobalDM.BooksWorkedHoursOnBankHoliday(
        Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime, // BookingDay
        TimeBlockLength - ManualAbsenceMinute, // WorkedMin
        EmployeeData, // AEmployeeDate
        UNCHECKEDVALUE, // Manual
        False) // UpdateYes (PHPEPT -> No update needed)
          <> 0 then
      begin
        // Now insert the record.
        // TD-22355 Use TQuery instead of TTable
        qrySHEInsert.ParamByName('SALARY_DATE').AsDatetime :=
          Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value;
        qrySHEInsert.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          Query.FieldByName('EMPLOYEE_NUMBER').Value;
        qrySHEInsert.ParamByName('HOURTYPE_NUMBER').AsInteger := 1;
        qrySHEInsert.ParamByName('SALARY_MINUTE').AsInteger :=
          TimeBlockLength - ManualAbsenceMinute;
        qrySHEInsert.ParamByName('MANUAL_YN').AsString :=
          UNCHECKEDVALUE;
        qrySHEInsert.ParamByName('MUTATOR').AsString :=
          SystemDM.CurrentProgramUser;
        qrySHEInsert.ExecSQL;
{
        tblSalaryHourPerEmployee.Insert;
        tblSalaryHourPerEmployeeSALARY_DATE.Value :=
          Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value;
        tblSalaryHourPerEmployeeEMPLOYEE_NUMBER.Value :=
          Query.FieldByName('EMPLOYEE_NUMBER').Value;
        tblSalaryHourPerEmployeeHOURTYPE_NUMBER.Value :=
          1;
        tblSalaryHourPerEmployeeMANUAL_YN.Value :=
          UNCHECKEDVALUE;
        tblSalaryHourPerEmployeeSALARY_MINUTE.Value :=
          TimeBlockLength - ManualAbsenceMinute;
        tblSalaryHourPerEmployeeEXPORTED_YN.Value :=
          UNCHECKEDVALUE;
        tblSalaryHourPerEmployee.Post;
}
      end;
{
        // Fill EmployeeData first!
        EmptyIDCard(EmployeeData);
        EmployeeData.EmployeeCode :=
          Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
        EmployeeData.ShiftNumber := Query.FieldByName('SHIFT_NUMBER').AsInteger;
        EmployeeData.PlantCode := Query.FieldByName('PLANT_CODE').AsString;
        // More fields to fill here!

        // This will update salary + PHEPT.
        GlobalDM.UpdateSalaryHour(
          Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value,
          EmployeeData,
          1,
          TimeBlockLength - ManualAbsenceMinute,
          'N',
          True, // Replace
          True);
}
        // RV075.1. END


(*
        // Update SalaryHourPerEmployee
        if tblSalaryHourPerEmployee.FindKey(
          [Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value,
           Query.FieldByName('EMPLOYEE_NUMBER').Value,
           1,
           UNCHECKEDVALUE]) then
        begin
          tblSalaryHourPerEmployee.Edit;
          tblSalaryHourPerEmployee.FieldByName('SALARY_MINUTE').Value :=
            tblSalaryHourPerEmployee.FieldByName('SALARY_MINUTE').Value +
              TimeBlockLength - ManualAbsenceMinute;
          tblSalaryHourPerEmployee.Post;
        end
        else
        begin
          tblSalaryHourPerEmployee.Insert;
          tblSalaryHourPerEmployeeSALARY_DATE.Value :=
            Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value;
          tblSalaryHourPerEmployeeEMPLOYEE_NUMBER.Value :=
            Query.FieldByName('EMPLOYEE_NUMBER').Value;
          tblSalaryHourPerEmployeeHOURTYPE_NUMBER.Value :=
            1;
          tblSalaryHourPerEmployeeMANUAL_YN.Value :=
            UNCHECKEDVALUE;
          tblSalaryHourPerEmployeeSALARY_MINUTE.Value :=
            TimeBlockLength - ManualAbsenceMinute;
          tblSalaryHourPerEmployeeEXPORTED_YN.Value :=
            UNCHECKEDVALUE;
          tblSalaryHourPerEmployee.Post;
        end;
*)
      end;
      Query.Next;
    end;
    Query.Close;
  end;
//  if ChangeCount > 0 then
//    Log(Format(SLogChangedAbsence,[ChangeCount]));
  if AddCount > 0 then
    Log(Format(SLogChangedAbsence,[AddCount]));
  Log(SLogEndNonScanning);
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.FormCreate(Sender: TObject);
begin
  inherited;
  OnePlant := SystemDM.UseFinalRun; // 20011800
  OnlyShowPlantSelection := False; // 20011800
  DialogProcessAbsenceHrsDM := TDialogProcessAbsenceHrsDM.Create(Self);
//  RecalcSalaryDM := TRecalcSalaryDM.Create(Self); // RV040.1.
  ChangedEmployeeList := TStringList.Create;
  MyPlant := '';
  MyEmployee := '';
  StartDate := 0;
  EndDate := 0;
  FMyError := False;
  FMyErrorMessage := '';
end;

// RV040.1. This gives an access error before the end.
(*
procedure TDialogProcessAbsenceHrsF.ActionComputeSalaryHoursXXX;
begin
  Log('Start Reprocess Scans');
  RecalcSalaryDM.PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
  RecalcSalaryDM.PlantTo := GetStrValue(CmbPlusPlantTo.Value);
  RecalcSalaryDM.TeamFrom := GetStrValue(CmbPlusTeamFrom.Value);
  RecalcSalaryDM.TeamTo := GetStrValue(CmbPlusTeamTo.Value);
  RecalcSalaryDM.AllTeam := 0;
  RecalcSalaryDM.EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
  RecalcSalaryDM.EmployeeTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
  RecalcSalaryDM.AllEmp := 0;
  RecalcSalaryDM.DateFrom := DateFrom.Date;
  RecalcSalaryDM.DateTo := DateTo.Date;
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    RecalcSalaryDM.UserName := SystemDM.LoginUser
  else
    RecalcSalaryDM.UserName := '*';
  RecalcSalaryDM.Memo := mmLog;
  RecalcSalaryDM.ProgressBar := pBar2;
  RecalcSalaryDM.RecalcSalary;
end;
*)

procedure TDialogProcessAbsenceHrsF.ActionComputeSalaryHours;
var
  EmplIDcard: TScannedIDCard;
  StartOvertime,EndOvertime, CurrentDate: TDateTime;
  SelectStr: String;
  ScanCount: Integer;
  SkipCount: Integer;
  RecalcProdHours: Boolean;
  AbsenceCorrection: Boolean;
//  ProdDate1, ProdDate2: TDateTime;
//  TRSExists: Boolean;
//  TRSDate: TDateTime;
  // 20013489 Check and (if needed) correct shift date for scans.
  procedure CorrectShiftDateForScans(AEmployeeNumber: Integer;
    AStartDate, AEndDate: TDateTime);
  var
    CurrentStart, CurrentEnd: TDateTime;
    CurrentIDCard: TScannedIDCard;
  begin
    try
      with DialogProcessAbsenceHrsDM do
      begin
        with QueryWork do
        begin
          Close;
          SQL.Clear;
          SQL.Add(
            'SELECT ' +
            '  T.DATETIME_IN, T.EMPLOYEE_NUMBER, T.PLANT_CODE, ' +
            '  T.WORKSPOT_CODE, T.JOB_CODE, T.SHIFT_NUMBER, ' +
            '  T.DATETIME_OUT, T.PROCESSED_YN, T.IDCARD_IN, ' +
            '  T.IDCARD_OUT, W.DEPARTMENT_CODE, ' +
            '  T.SHIFT_DATE ' +
            'FROM ' +
            '  TIMEREGSCANNING T, WORKSPOT W ' +
            'WHERE ' +
            '  T.DATETIME_IN >= :DSTART AND ' +
            '  T.DATETIME_IN < :DEND AND ' +
            '  T.PLANT_CODE = W.PLANT_CODE AND ' +
            '  T.WORKSPOT_CODE = W.WORKSPOT_CODE AND ' +
            '  T.EMPLOYEE_NUMBER = :EMPNO AND ' +
            '  T.PROCESSED_YN = :PROCESSED ' +
            'ORDER BY ' +
            '  T.DATETIME_IN '
            );
          ParamByName('DSTART').AsDate := AStartDate - 1;
          ParamByName('DEND').AsDate := Trunc(AEndDate) + 1;
          ParamByName('EMPNO').AsInteger := AEmployeeNumber;
          ParamByName('PROCESSED').AsString := CHECKEDVALUE;
          Open;
          while not Eof do
          begin
            GlobalDM.PopulateIDCard(CurrentIDCard, QueryWork);
            AProdMinClass.GetShiftDay(CurrentIDCard, CurrentStart, CurrentEnd);
            if ((Trunc(CurrentStart) <> CurrentIDCard.ShiftDate) or
              (CurrentIDCard.ShiftDate = 0)) then
              GlobalDM.UpdateTimeRegScanning(CurrentIDCard, Trunc(CurrentStart));
            Next;
          end; // while
          Close;
        end; // with
      end; // with
    except
    end;
  end; // CorrectShiftDateForScans
  // PIM-55
  // Check if there were absence hours + salary hours booked on the same day
  // If so, then decrease the absence hours and return True to indicate
  // it has to recalculate the hours again!
  // IMPORTANT: We need to know the total hours based on availability here!
  // Only make a correction when Absence + Salary is greater than Availability!
  function CheckCorrectAbsenceHours(AIDCard: TScannedIDCard;
    AStartOverTime, AEndOverTime: TDateTime): Boolean;
  var
    ADate: TDateTime;
    AbsenceMinute, SalaryMinute: Integer;
    AFound: Boolean;
  begin
    Result := False;
    with DialogProcessAbsenceHrsDM do
    begin
      ADate := AStartOverTime;
      while (ADate <= AEndOverTime) do
      begin
        AbsenceMinute := 0;
        SalaryMinute := 0;
        with qryAHECorrectFind do
        begin
          Close;
          ParamByName('ABSENCEHOUR_DATE').AsDateTime := ADate;
          ParamByName('EMPLOYEE_NUMBER').AsInteger := AIDCard.EmployeeCode;
          Open;
          AFound := not Eof;
          if AFound then
            AbsenceMinute := FieldByName('ABSENCE_MINUTE').AsInteger;
          if AbsenceMinute = 0 then // PIM-55 No need to look further
            AFound := False;
        end;
        if AFound then
        begin
          with qrySHE do
          begin
            Close;
            ParamByName('SALARY_DATE').AsDateTime := ADate;
            ParamByName('EMPLOYEE_NUMBER').AsInteger := AIDCard.EmployeeCode;
            Open;
            AFound := not Eof;
            if AFound then
              SalaryMinute := FieldByName('SALARY_MINUTE').AsInteger;
          end;
        end; // if
        if AFound then
        begin
          // Correct Absence Hours
          if (AbsenceMinute > 0) and (SalaryMinute > 0) then
          begin
            // First delete the existing absence hours
            with qryDELAHECorrect do
            begin
              ParamByName('ABSENCEHOUR_DATE').AsDateTime := ADate;
              ParamByName('EMPLOYEE_NUMBER').AsInteger := AIDCard.EmployeeCode;
              ExecSQL;
            end;
            Self.EmployeeFrom := AIDCard.EmployeeCode;
            Self.EmployeeTo := AIDCard.EmployeeCode;
            ActionEmployeeAvailability(ADate); // PIM-55 Only look for this date!
            Result := True;
          end;
        end; // if
        ADate := ADate + 1;
      end; // while
    end; // with
  end; // CheckCorrectAbsenceHours
begin
  try
//    SystemDM.Pims.StartTransaction; // TD-23672
    // RV067.MRA.26 Only recalc prod hours when the recalc-checkbox is checked.
    RecalcProdHours := ckRecalculateHours.Checked;
    // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
    Log('Start Reprocess Scans');
    CurrentDate := 0;
    ScanCount := 0;
    SkipCount := 0;
    pBar2.Position := 0;
    Update;
    ReplaceDate(CurrentDate, Now);
    with DialogProcessAbsenceHrsDM do
    begin
      // RV075.2. Get all employee, also the non-scanning type.
      //          For all employees balances must be recalculated.
      // RV082.4. Also look for employees of type Book_Prod_Hrs_YN.
      // RV100.1. Added Contractgroup
      SelectStr :=
        'SELECT ' + NL +
        '  E.EMPLOYEE_NUMBER, E.PLANT_CODE, E.IS_SCANNING_YN, ' + NL +
        '  E.BOOK_PROD_HRS_YN, E.CONTRACTGROUP_CODE ' + NL +
        'FROM ' + NL +
        '  EMPLOYEE E ' + NL +
        'WHERE ' + NL +
        ' ( ' +
        '  (:USER_NAME = ''*'') OR ' +
        '  (E.TEAM_CODE IN ' +
        '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
        '  ) AND ' +
        '  E.PLANT_CODE >= :STARTPLANT AND ' + NL +
        '  E.PLANT_CODE <= :ENDPLANT AND ' + NL +
        '  E.TEAM_CODE >= :STARTTEAM AND ' + NL +
        '  E.TEAM_CODE <= :ENDTEAM AND ' + NL +
        '  E.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
        '  E.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL +
//        '  E.IS_SCANNING_YN = :IS_SCANNING_YN AND ' + NL +
        '  E.STARTDATE <= :ENDPERIOD AND ' + NL +
        '  ((E.ENDDATE >= :ENDPERIOD) OR (E.ENDDATE IS NULL)) ' + NL +
        'ORDER BY ' + NL +
        '  E.EMPLOYEE_NUMBER';
      qryCalcSalary.Close;
      qryCalcSalary.SQL.Clear;
      qryCalcSalary.SQL.Add(SelectStr);
//qryCalcSalary.SQL.SaveToFile('c:\temp\ProcessAbsence.sql');
//      qryCalcSalary.Prepare; // RV040.
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        qryCalcSalary.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
      else
        qryCalcSalary.ParamByName('USER_NAME').AsString := '*';
      qryCalcSalary.ParamByName('STARTPLANT').asString := PlantFrom;
      qryCalcSalary.ParamByName('ENDPLANT').asString := PlantTo;
      qryCalcSalary.ParamByName('STARTTEAM').asString := TeamFrom;
      qryCalcSalary.ParamByName('ENDTEAM').asString := TeamTo;
      qryCalcSalary.ParamByName('ENDPERIOD').asDateTime := EndDate;
//      qryCalcSalary.ParamByName('IS_SCANNING_YN').asString := CHECKEDVALUE;
      qryCalcSalary.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
      qryCalcSalary.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
      qryCalcSalary.Open;
      // RV049.3. Bugfix.
{
      qryTRS.Close;
      qryTRS.ParamByName('EMPFROM').AsInteger :=
        GetIntValue(dxDBExtLookupEditEmplFrom.Text);
      qryTRS.ParamByName('EMPTO').AsInteger :=
        GetIntValue(dxDBExtLookupEditEmplTo.Text);
      qryTRS.ParamByName('DATEFROM').AsDateTime := Trunc(DateFrom.Date);
      qryTRS.ParamByName('DATETO').AsDateTime :=
        Trunc(DateTo.Date) + Frac(EncodeTime(23, 59, 59, 00));
      qryTRS.Open;
}
      pBar2.Max := qryCalcSalary.RecordCount;
      while not qryCalcSalary.Eof do
      begin
        // RV075.6. Only for RFL.
(*
        if IsRFL then
          if (SystemDM.GetFillProdHourPerHourTypeYN = CHECKEDVALUE) then
            CheckRecalcPHEPT(qryCalcSalary.FieldByName('EMPLOYEE_NUMBER').asInteger,
                Trunc(DateFrom.Date), Trunc(DateTo.Date));
*)

        // RV075.2. When an employee is of type 'non-scanning', then still
        //          recalc some balances.
        // RV082.4. Also look for employees of type Book_Prod_Hrs_YN.
        if (qryCalcSalary.FieldByName('IS_SCANNING_YN').AsString = UNCHECKEDVALUE) and
          (qryCalcSalary.FieldByName('BOOK_PROD_HRS_YN').AsString = UNCHECKEDVALUE) then
        begin
          try
            GlobalDM.RecalculateBalances(
              qryCalcSalary.FieldByName('EMPLOYEE_NUMBER').asInteger,
              Trunc(DateFrom.Date), Trunc(DateTo.Date));
          except
            // Ignore error.
          end;
          qryCalcSalary.Next;
          Continue;
        end;

        // RV040. Always recalculate the hours for employee that are scanners!
        //        No skipping here!

        // RV067.MRA.26
        // If no recalculate is needed, then still do this for
        // the changed employee!
        if not ckRecalculateHours.Checked then
          if ChangedEmployeeList.IndexOf(
            qryCalcSalary.FieldByName('EMPLOYEE_NUMBER').AsString) < 0 then
          begin       // For this Employee is no need to recalculate salaryhours.
            inc(SkipCount);
            qryCalcSalary.Next;
            Continue;
          end;

        // Employee Selected
        EmptyIDCard(EmplIDcard);
        EmplIDcard.EmployeeCode :=
          qryCalcSalary.FieldByName('EMPLOYEE_NUMBER').asInteger;
        // RV100.1. Added Contractgroup
        EmplIDCard.ContractgroupCode :=
          qryCalcSalary.FieldByName('CONTRACTGROUP_CODE').AsString;
        EmplIDcard.PlantCode := qryCalcSalary.FieldByName('PLANT_CODE').asString;
        // MR:03-10-2003 Set ShiftNumber to default value
        EmplIDCard.ShiftNumber := -1;
        EmplIDcard.DateIn := StartDate;
        QueryWork.Active := False;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add(
          'SELECT ' +
          '  SHIFT_NUMBER, PLANT_CODE ' +
          'FROM ' +
          '  SHIFTSCHEDULE ' +
          'WHERE ' +
          '  (EMPLOYEE_NUMBER = :EMPNO) AND ' +
          '  (SHIFT_SCHEDULE_DATE = :SHDATE) AND ' +
          '  (SHIFT_NUMBER <> -1)');
//        QueryWork.Prepare; // RV040.
        QueryWork.ParamByName('EMPNO').asInteger := EmplIDcard.EmployeeCode;
        QueryWork.ParamByName('SHDATE').asDateTime := EmplIDcard.DateIn;
        QueryWork.Active := True;
        if QueryWork.Active and (not QueryWork.IsEmpty) then
        begin
          EmplIDcard.ShiftNumber :=
            QueryWork.FieldByName('SHIFT_NUMBER').asInteger;
          EmplIDcard.PlantCode :=
            QueryWork.FieldByname('PLANT_CODE').asString;
        end
        else
        begin
          QueryWork.Active := False;
          QueryWork.SQL.Clear;
          QueryWork.SQL.Add(
            'SELECT ' +
            '  SHIFT_NUMBER ' +
            'FROM ' +
            '  SHIFT ' +
            'WHERE ' +
            '  PLANT_CODE=:PCODE');
//          QueryWork.Prepare; // RV040.
          QueryWork.ParamByName('PCODE').asString := EmplIDcard.PlantCode;
          QueryWork.Open;
          if not QueryWork.IsEmpty then
            EmplIDcard.ShiftNumber :=
              QueryWork.FieldByName('SHIFT_NUMBER').asInteger;
          QueryWork.Close;
        end;
        StartOvertime := StartDate;
        EndOvertime := StartDate;

        EmplIDcard.DateIn := StartDate + 0.5;
        EmplIDcard.DateOut := StartDate + 0.5;
        // MR:19-12-2002
        // During compare of EndOvertime and DateTo.Date;
        // EndOvertime was a Date without time-part, but
        // DateTo.Date was a Date with time-part.
        // If both date-parts were the same, the compare was true,
        // which resulted in a endless loop.
        // To prevent this, both are truncated.
        AbsenceCorrection := False;
        while (Trunc(EmplIDcard.DateIn) <= Trunc(DateTo.Date)) do
        begin
          GlobalDM.ComputeOvertimePeriod(StartOverTime, EndOvertime,
            EmplIDcard);
          if SystemDM.UseShiftDateSystem then
          begin
            // 20013489
            CorrectShiftDateForScans(EmplIDCard.EmployeeCode, StartOvertime,
              EndOvertime);
          end; // if UseShiftDateSystem
          if (StartOverTime <= CurrentDate)  then
          // this overtime periode must be recalculated.
          begin
            // Delete all salary hours from period
            // MR:05-09-2003 Parameters have changed!
            // RV048.4. Check if there is really a scan, to prevent it is
            //          deleting salary on a wrong day when no timeblocks
            //          can be found.

            // RV049.3. Bugfix. Check scan during compute salary.
            //                  This is wrong! The while is not
            //                  a loop for all dates, but only for a period.
            //                  For example, when the period is 1 week,
            //                  it only comes here once!
            
            GlobalDM.UnprocessProcessScans(
              qryCalcSalary, EmplIDCard,
              EmplIDCard.DateIn, NullDate, NullDate, NullDate, NullDate,
              True, False,
              RecalcProdHours, // RV056.1. // RV067.MRA.26
              True // 20013271
              );
            inc(ScanCount);
            pBar2.Position := ScanCount + SkipCount;
            Update;
            Application.ProcessMessages;
          end;
          // PIM-55
          if not AbsenceCorrection then
          begin
            if CheckCorrectAbsenceHours(EmplIDCard, StartOverTime,
              EndOverTime) then
            begin
              AbsenceCorrection := True;
              Continue;
            end;
          end;
          AbsenceCorrection := False;
          // Next overtime period
          EmplIDcard.DateIn := Trunc(EndOvertime + 1) + 0.5;
          EmplIDcard.DateOut := Trunc(EndOvertime + 1) + 0.6;
          StartOverTime := Trunc(EndOverTime + 1);
        end; // while (Trunc(EmplIDcard.DateIn) <= Trunc(DateTo.Date)) do
        // RV040.2. Also do this!
        GlobalDM.RecalculateBalances(EmplIDCard.EmployeeCode,
          Trunc(DateFrom.Date), Trunc(DateTo.Date));
        // TD-23672
{
        if SystemDM.Pims.InTransaction then
        begin
          SystemDM.Pims.Commit;
        end;
}
        qryCalcSalary.Next;
      end; // while not qryCalcSalary.Eof do
    end;
    // TD-23672
{
    if SystemDM.Pims.InTransaction then
    begin
      SystemDM.Pims.Commit;
    end;
}
    if ScanCount > 0 then
      Log(Format('Changed %d salary hour records', [ScanCount]));
    if SkipCount > 0 then // RV067.MRA.26
      Log(Format('Skipped %d employees for compute salary', [SkipCount]));
    Log('End Reprocess Scans');
    ProcessbarUpdate;
  except
    on E: EDBEngineError do
    begin
      // TD-23672
{
      if SystemDM.Pims.InTransaction then
      begin
        Log('Rollback');
//        WLog('Rollback');
        SystemDM.Pims.Rollback;
      end;
}
      Log(GetErrorMessage(E, PIMS_POSTING), True);
    end;
  end;
end;

procedure TDialogProcessAbsenceHrsF.DateFromChange(Sender: TObject);
begin
  inherited;
//  if DateFrom.Date > DateTo.Date then
//    DateTo.Date := DateFrom.Date;
end;

procedure TDialogProcessAbsenceHrsF.DateToChange(Sender: TObject);
begin
  inherited;
//  if DateFrom.Date > DateTo.Date then
//    DateFrom.Date := DateTo.Date;
end;

// MR:20-09-2004 Order 550335
procedure TDialogProcessAbsenceHrsF.ActionUnpaidIllness;
var
  SelectStr: String;
begin
  // MR:14-02-2006 Changed the query for 'TEAMPERUSER'.
  Log('Start Unpaid Illness');
  with DialogProcessAbsenceHrsDM do
  begin
    cdsEmpAvail.EmptyDataSet;
    // First look in contract if necessary fields had been set
    SelectStr :=
      'SELECT ' + NL +
      '  EM.EMPLOYEE_NUMBER, CG.PAID_ILLNESS_PERIOD, ' + NL +
      '  CG.UNPAID_ILLN_ABSENCEREASON_ID, AR.ABSENCETYPE_CODE, ' + NL +
      '  AR.ABSENCEREASON_CODE, AR.HOURTYPE_NUMBER ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE EM INNER JOIN CONTRACTGROUP CG ON ' + NL +
      '    EM.CONTRACTGROUP_CODE = CG.CONTRACTGROUP_CODE ' + NL +
      '  INNER JOIN ABSENCEREASON AR ON ' + NL +
      '    CG.UNPAID_ILLN_ABSENCEREASON_ID = AR.ABSENCEREASON_ID ' + NL +
(* RV021.
      '  LEFT JOIN TEAMPERUSER T ON ' + NL +
      '    EM.TEAM_CODE = T.TEAM_CODE ' + NL +
*)
      'WHERE ' + NL +
      ' ( ' +
      '  (:USER_NAME = ''*'') OR ' +
      '  (EM.TEAM_CODE IN ' +
      '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
      ') AND ' +
(* RV021.
      '  ((:USER_NAME = ''*'') OR (T.USER_NAME = :USER_NAME)) AND ' + NL +
*)
      '  CG.PAID_ILLNESS_PERIOD > 0 AND ' + NL +
      '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
      '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO ';
    qryCntr.Close;
    qryCntr.SQL.Clear;
    qryCntr.SQL.Add(SelectStr);
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      qryCntr.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      qryCntr.ParamByName('USER_NAME').AsString := '*';
    qryCntr.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    qryCntr.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    qryCntr.Open;
    if not qryCntr.IsEmpty then
    begin
      qryCntr.First;
      while not qryCntr.Eof do
      begin
        DetermineUnpaidIllness(
          qryCntr.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          qryCntr.FieldByName('PAID_ILLNESS_PERIOD').AsInteger,
          qryCntr.FieldByName('ABSENCEREASON_CODE').AsString,
          Trunc(DateFrom.Date), Trunc(DateTo.Date));
        qryCntr.Next;
      end;
    end;
    qryCntr.Close;
  end;
  Log('End Unpaid Illness');
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.btnOkClick(Sender: TObject);
begin
  inherited; // RV051.1.
  MainAction;
end;

procedure TDialogProcessAbsenceHrsF.MainAction;
var
  ADate: TDateTime;
  // RV089.1.
  procedure OpenTables;
  begin
    try
      ActiveTables(DialogProcessAbsenceHrsDM, True);
    except
    end;
  end;
  // RV089.1.
  procedure CloseTables;
  begin
    try
      ActiveTables(DialogProcessAbsenceHrsDM, False);
    except
    end;
  end;
begin
  try
    DateFromCloseUp(nil); // 20011800.70
    // RV089.1. Assign values here. They can be changed in between, so
    //          assign them here!
    PlantFrom := GetStrValue(CmbPlusPlantFrom.Value);
    PlantTo := GetStrValue(CmbPlusPlantTo.Value);
    TeamFrom := GetStrValue(CmbPlusTeamFrom.Value);
    TeamTo := GetStrValue(CmbPlusTeamTo.Value);
    EmployeeFrom := GetIntValue(dxDBExtLookupEditEmplFrom.Text);
    EmployeeTo := GetIntValue(dxDBExtLookupEditEmplTo.Text);
    ADate := Trunc(DateFrom.DateTime);
    StartDate := ADate;
    ADate := Trunc(DateTo.DateTime);
    EndDate := ADate;
  except
    // Ignore error
  end;
  //RV067.7.
  if ckRecalculateHours.Checked then
    // RV089.1.
    if DisplayMessage(
        SPimsRecalcHoursConfirm, // RV071.4.
        mtConfirmation, [mbYes, mbNo]) = mrNo
    then
      Exit;

  inherited;
  // RV089.1. Optimalisations.
  // Open TTables here.

  try
    // MR:17-12-2003 Log selections
    WLog('----- Start Process Absence -----');
    WLog('From Plant ' + cmbPlusPlantFrom.Value + ' to ' + cmbPlusPlantTo.Value);
    WLog('From Team ' + cmbPlusTeamFrom.Value + ' to ' + cmbPlusTeamTo.Value);
    WLog('From Empl ' + dxDBExtLookupEditEmplFrom.Text + ' to ' +
      dxDBExtLookupEditEmplTo.Text);
    WLog('From Date ' + DateToStr(DateFrom.Date) + ' to ' +
      DateToStr(DateTo.Date));
    // RV089.1. Optimalisations.
    // Open TTables here.
    // TD-22355 Not tables are used anymore! No need to open them.
//    OpenTables;
    try
    // MRA:06-MAR-2006 RV004. Out-of-memory-fix. Do not use transaction.
(*
    SystemDM.Pims.StartTransaction;
*)
        // RV089.1. Do not do this! Or it will reset the from-to-values!
//      btnOk.Enabled := False;
//      btnCancel.Enabled := False;

      // Process Absence and non-scanning hours
      // RV089.1. This is done at start of this procedure.
//      ADate := 0;
//      ReplaceDate(ADate,DateFrom.DateTime);
//      StartDate := ADate;
//      ADate := 0;
//      Replacedate(ADate,DateTo.DateTime);
//      EndDate := ADate;

      mmLog.Lines.Clear;
      SystemDM.GetSettings;
      pBar.Position := 0;
      ChangedEmployeeList.Clear;
      if IsRFL then
      begin
        ActionTailorMadeNonScanners;
        Application.ProcessMessages;
      end;

      // RV099.1.
      // This changes Employee Availability records.
      ActionTFTHolidayCorrection;

      ActionUnpaidIllness;
      Application.ProcessMessages;
      ActionAbsenceHourPerEmployee;
      Application.ProcessMessages;

      //RV066.1.
      ActionSalaryHourPerEmployee;
      Application.ProcessMessages;
      ActionEmployeeAvailability;
      Application.ProcessMessages;

      //RV067.7.
      // RV067.MRA.26
{      if ckRecalculateHours.Checked then
      begin }
        ActionComputeSalaryHours;
        Application.ProcessMessages;
{      end; }
      ActionEmployeeAvailabilityNoScanEmployees;
      // RV067.MRA.31
      CorrectionBalanceAbsenceHours;
      Application.ProcessMessages;
      // MRA:06-MAR-2006 RV004. Out-of-memory-fix. Do not use transaction.
(*
    if SystemDM.Pims.InTransaction then
    begin
      Log('Commit');
      SystemDM.Pims.Commit;
    end;
*)
      // TD-23672 Be sure a commit is done afterwards!
      if SystemDM.Pims.InTransaction then
      begin
        Log('Commit');
        SystemDM.Pims.Commit;
      end;
    except
      on E: EDBEngineError do
      begin
        // MRA:06-MAR-2006 RV004. Out-of-memory-fix. Do not use transaction.
(*
      if SystemDM.Pims.InTransaction then
      begin
        Log('Rollback');
        SystemDM.Pims.Rollback;
      end;
*)
//      WLog(GetErrorMessage(E, PIMS_POSTING));
        Log(GetErrorMessage(E, PIMS_POSTING), True);
      end;
    end;
  finally
    if FMyError then
    begin
      // There were errors! Please see error-messages above.
      Log(SErrorDuringProcess, True);
    end
    else
      Log('Ready...');
    pBar.Position := 100;
    Update;
    // RV089.1. Optimalisations.
    // Close TTables here.
    // TD-22355 TTables are not used anymore, no need to close them!
//    CloseTables;
    Log('----- End Process Absence -----');
    if FMyError then
    begin
      FMyErrorMessage := SSProcessFinishedWithErrors + #13#13 + FMyErrorMessage;
      Log(FMyErrorMessage, True);
      DisplayMessage(FMyErrorMessage, mtInformation, [mbOK]); // RV089.1.
    end
    else
    begin
      DisplayMessage(SSProcessFinished, mtInformation, [mbOK]); // RV089.1.
      // RV089.1.
      if (DialogProcessAbsenceHrsF_HND <> nil) then // Embedded
        SideMenuUnitMaintenanceF.NilInsertForm
      else // Not embedded
        Close;
    end;
  end;
end;

// RV040. Use 'OnCloseUp' instead of 'OnChange'.
procedure TDialogProcessAbsenceHrsF.DateFromCloseUp(Sender: TObject);
var
  FinalRunExportDate: TDateTime;
begin
  inherited;
  // 20011800
  if SystemDM.UseFinalRun then
  begin
    FinalRunExportDate :=
      SystemDM.FinalRunExportDateByPlant(
        GetStrValue(CmbPlusPlantFrom.Value));
    if FinalRunExportDate <> NullDate then
      if Trunc(DateFrom.Date) <= FinalRunExportDate then
      begin
        DateFrom.Date := FinalRunExportDate + 1;
        DisplayMessage(SPimsFinalRunDateChange, mtInformation, [mbOK]);
      end;
  end;
  if DateFrom.Date > DateTo.Date then
    DateTo.Date := DateFrom.Date;
end;

// RV040. Use 'OnCloseUp' instead of 'OnChange'.
procedure TDialogProcessAbsenceHrsF.DateToCloseUp(Sender: TObject);
var
  FinalRunExportDate: TDateTime;
begin
  inherited;
  // 20011800
  if SystemDM.UseFinalRun then
  begin
    FinalRunExportDate :=
      SystemDM.FinalRunExportDateByPlant(
        GetStrValue(CmbPlusPlantFrom.Value));
    if FinalRunExportDate <> NullDate then
      if Trunc(DateTo.Date) <= FinalRunExportDate then
      begin
        DateTo.Date := FinalRunExportDate + 1;
        DisplayMessage(SPimsFinalRunDateChange, mtInformation, [mbOK]);
      end;
  end;
  if DateFrom.Date > DateTo.Date then
    DateFrom.Date := DateTo.Date;
end;
//RV066.1.
function TDialogProcessAbsenceHrsF.IsRFL: Boolean;
begin
  Result := SystemDM.IsRFL; //SystemDM.GetCode = 'RFL';
end;

procedure TDialogProcessAbsenceHrsF.DeterminePlanningTimeBlocks(
  VAR ProdMin, BreaksMin: Integer;
  SelectedTimeBlock, WeekDay: Integer;
  PlanningDate: TDateTime;
  VAR AStart, AEnd: TDateTime);
var
//  AStart, AEnd: TDateTime;
  AEmployeeData: TScannedIDCard;
  PayedBreaks: Integer;
  ErrorMessageLine: String;
begin
  AStart := 0;
  AEnd := 0;
  with DialogProcessAbsenceHrsDM do
  begin
    ATBLengthClass.FillTBLength(
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      Query.FieldByName('SHIFT_NUMBER').AsInteger,
      Query.FieldByName('PLANT_CODE').AsString,
      Query.FieldByName('DEPARTMENT_CODE').AsString,
      True);
    ATBLengthClass.AEmployeeNumber :=
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    ATBLengthClass.APlantCode :=
      Query.FieldByName('PLANT_CODE').AsString;
    ATBLengthClass.AShiftNumber :=
      Query.FieldByName('SHIFT_NUMBER').AsInteger;
    ATBLengthClass.ADepartmentCode :=
      Query.FieldByName('DEPARTMENT_CODE').AsString;
    ATBLengthClass.ARemoveNotPaidBreaks := False;
    try
      AStart := Trunc(PlanningDate) +
        Frac(ATBLengthClass.GetStartTime(SelectedTimeBlock, WeekDay));
      AEnd := Trunc(PlanningDate) +
        Frac(ATBLengthClass.GetEndTime(SelectedTimeBlock, WeekDay));
{$IFDEF DEBUG1}
(*  WDebugLog('AStart=' + DateTimeToStr(AStart) +
    ' AEnd=' + DateTimeToStr(AEnd)
    ); *)                             
{$ENDIF}
      if (AStart <> AEnd) then
        if (AStart > AEnd) then
          AEnd :=
            Trunc(PlanningDate + 1) +
              Frac(AEnd); // RV027. Also add AEnd (timepart) !
    except
      // Error: Wrong timeblock for employee
      ErrorMessageLine := SErrorWrongTimeBlock + ' ' +
        IntToStr(ATBLengthClass.AEmployeeNumber);
//      WLog(ErrorMessageLine);
      Log(ErrorMessageLine, True);
      FMyErrorMessage := FMyErrorMessage + ErrorMessageLine + #13;
      FMyError := True;
      Exit;
    end;

    EmptyIDCard(AEmployeeData);
    AEmployeeData.EmployeeCode :=
      Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    AEmployeeData.PlantCode :=
      Query.FieldByName('PLANT_CODE').AsString;
    AEmployeeData.ShiftNumber :=
      Query.FieldByName('SHIFT_NUMBER').AsInteger;
    AEmployeeData.DepartmentCode :=
      Query.FieldByName('DEPARTMENT_CODE').AsString;
    // MRA:23-OCT-2008 Start
    // The ProdMin is without breaks.
    // Be sure to add the PayedBreaks, if there are any (not 0).

    // Get the ProdMin (without breaks).
    AProdMinClass.ComputeBreaks(AEmployeeData, AStart, AEnd, 0,
      ProdMin, BreaksMin, PayedBreaks);
  end;
end;

//RV066.1.
procedure TDialogProcessAbsenceHrsF.ProductionHoursStandardPlanning(
  Lst: TStringList; ADate: TDateTime);
var
  SelectStr, Line: String;
  WeekDay: Word;
  // RV082.4.
{  TotalProdMin, TotalBreaksMin, } ProdMin, BreaksMin: integer;
  TimeBlockNumber: Integer;
  EmployeeFound: Boolean;
  EmployeeAvailabilityFound: Boolean;
  AvailableTimeblock_1, AvailableTimeblock_2,
  AvailableTimeblock_3, AvailableTimeblock_4,
  AvailableTimeblock_5, AvailableTimeblock_6,
  AvailableTimeblock_7, AvailableTimeblock_8,
  AvailableTimeblock_9, AvailableTimeblock_10: String;
  GoOn: Boolean;
  DateTimeIn, DateTimeOut: TDateTime;

  function ValidTimeBlock(ATimeBlock: String): Boolean;
  begin
    with DialogProcessAbsenceHrsDM do
      Result :=
        (Query.FieldByName(ATimeBlock).AsString = 'A') or
        (Query.FieldByName(ATimeBlock).AsString = 'B') or
        (Query.FieldByName(ATimeBlock).AsString = 'C');
  end;
begin
  WeekDay := DayInWeek(SystemDM.WeekStartsOn, ADate);
  // RV082.6. Get Department from Workspot for determining timeblocks-per-dept.
  SelectStr :=
    'SELECT ' + NL +
    '  EP.PLANT_CODE, EP.DAY_OF_WEEK, EP.SHIFT_NUMBER, EP.WORKSPOT_CODE, ' + NL +
    '  EP.EMPLOYEE_NUMBER, EP.SCHEDULED_TIMEBLOCK_1, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_2, EP.SCHEDULED_TIMEBLOCK_3, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_4, EP.SCHEDULED_TIMEBLOCK_5, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_6, EP.SCHEDULED_TIMEBLOCK_7, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_8, EP.SCHEDULED_TIMEBLOCK_9, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_10, W.DEPARTMENT_CODE ' + NL +
    'FROM ' + NL +
    '  EMPLOYEE EM INNER JOIN STANDARDEMPLOYEEPLANNING EP ON ' + NL +
    '    EM.EMPLOYEE_NUMBER = EP.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    EP.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    EP.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    'WHERE ' + NL +
    ' ( ' +
    '  (:USER_NAME = ''*'') OR ' +
    '  (EM.TEAM_CODE IN ' +
    '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
    '  ) AND ' +
    '  EP.DAY_OF_WEEK = :DAYOFWEEK AND ' + NL +
    '  EP.PLANT_CODE >= :PLANTFROM AND ' + NL +
    '  EP.PLANT_CODE <= :PLANTTO AND ' + NL +
    '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
    '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
    '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
    '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL +
    '  EM.BOOK_PROD_HRS_YN = ''Y'' ' + NL +
    'ORDER BY ' + NL +
    '  EP.EMPLOYEE_NUMBER, EP.DAY_OF_WEEK';

  with DialogProcessAbsenceHrsDM do
  begin
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      Query.ParamByName('USER_NAME').AsString := '*';
    Query.ParamByName('DAYOFWEEK').AsInteger := WeekDay;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;
    //look in table STANDARDEMPLOYEEPLANNING (EP), filter on EMPLOYEE_NUMBER  and EMPLOYEEPLANNING_DATE.
    //result PLANT_CODE, EMPLOYEE_PLANNING_DATE, EMPLOYEE_NUMBER, SHIFT_NUMBER, DEPARTMENT_CODE, WORKSPOT_CODE,
    //SCHEDULED_TIMEBLOCK_x (1 to 4).

    //An employee who is planned has the value 'A', 'B' or 'C' in one or more timeblocks.
    //To determine the production hours that were made, the found timeblocks (with contents 'A', 'B', or 'C')
    Query.First;
    while not Query.Eof do
    begin
      // Get day of week - depends on 'Week Starts On'
      WeekDay := DayInWeek(SystemDM.WeekStartsOn, ADate);

      // RV082.4.
{      TotalProdMin   := 0;
      TotalBreaksMin := 0; }
      EmployeeFound  := False;

      // RV072.1. Reduce key to Employee and Date, or it will result in
      //          double records.
      //Check if found in EmployeePlanning
      Line :=
        FormatDateTime('dd/mm/yyyy', ADate) + ' ' +
        Query.FieldByName('EMPLOYEE_NUMBER').AsString;
{
      Line :=
        Query.FieldByName('PLANT_CODE').AsString + ' ' +
        FormatDateTime('dd/mm/yyyy', ADate) + ' ' +
        Query.FieldByName('SHIFT_NUMBER').AsString + ' ' +
        Query.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
        Query.FieldByName('WORKSPOT_CODE').AsString + ' ' +
        Query.FieldByName('EMPLOYEE_NUMBER').AsString;
}
      if Lst.IndexOf(Line) = -1 then //not found in EmployeePlanning
      begin
        // RV067.MRA.22 Change for order 550491.
        EmployeeAvailabilityFound := CheckEmployeeAvailability(
          Query.FieldByName('EMPLOYEE_NUMBER').AsInteger, ADate,
          AvailableTimeblock_1, AvailableTimeblock_2,
          AvailableTimeblock_3, AvailableTimeblock_4,
          AvailableTimeblock_5, AvailableTimeblock_6,
          AvailableTimeblock_7, AvailableTimeblock_8,
          AvailableTimeblock_9, AvailableTimeblock_10);
        for TimeBlockNumber := 1 to SystemDM.MaxTimeblocks do
        begin
          if ValidTimeBlock('SCHEDULED_TIMEBLOCK_' + IntToStr(TimeBlockNumber)) then
          begin
            // RV067.MRA.22 Change for order 550491.
            // Also check if employee is available!
            // If not available, then it should not create production-records!
            GoOn := True;
            if EmployeeAvailabilityFound then
            begin
              case TimeBlockNumber of
              1: GoOn := AvailableTimeblock_1 = '*';
              2: GoOn := AvailableTimeblock_2 = '*';
              3: GoOn := AvailableTimeblock_3 = '*';
              4: GoOn := AvailableTimeblock_4 = '*';
              5: GoOn := AvailableTimeblock_5 = '*';
              6: GoOn := AvailableTimeblock_6 = '*';
              7: GoOn := AvailableTimeblock_7 = '*';
              8: GoOn := AvailableTimeblock_8 = '*';
              9: GoOn := AvailableTimeblock_9 = '*';
              10: GoOn := AvailableTimeblock_10 = '*';
              end;
            end;
            if GoOn then
            begin
              DeterminePlanningTimeBlocks(ProdMin, BreaksMin, TimeBlockNumber,
                WeekDay, ADate, DateTimeIn, DateTimeOut);
              EmployeeFound  := True;
              // RV082.4. Insert Auto Scan instead of ProductionHourPerEmployee
              if (DateTimeIn <> 0) and (DateTimeOut <> 0) then
                InsertAutoScanRecord(
                  DatetimeIn,
                  Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
                  Query.FieldByName('PLANT_CODE').AsString,
                  Query.FieldByName('WORKSPOT_CODE').AsString,
                  '0',
                  Query.FieldByName('SHIFT_NUMBER').AsInteger,
                  DateTimeOut);
{$IFDEF DEBUG}
  WDebugLog('- InsertAutoScan SP. ' +
    ' Date=' + DateToStr(ADate) +
    ' DateIn=' + DateTimeToStr(DateTimeIn) +
    ' DateOut=' + DateTimeToStr(DateTimeOut) +
    ' Plant=' + Query.FieldByName('PLANT_CODE').AsString +
    ' Shift=' + IntToStr(Query.FieldByName('SHIFT_NUMBER').AsInteger) +
    ' Emp=' +  IntToStr(Query.FieldByName('EMPLOYEE_NUMBER').AsInteger) +
    ' WS=' +  Query.FieldByName('WORKSPOT_CODE').AsString +
    ' ProdMin=' + IntToStr(ProdMin) +
    ' BreaksMin=' + IntToStr(BreaksMin)
    );
{$ENDIF}
              // RV082.4.
{              TotalProdMin := TotalProdMin + ProdMin;
              TotalBreaksMin := TotalBreaksMin + BreaksMin; }
            end;
          end;
        end; // for TimeBlockNumber 
      end; // if Lst.IndexOf(Line) = -1 then

      if EmployeeFound then
      begin
{
        UpdateProdHourPerEmployee(
          ADate,
          Query.FieldByName('PLANT_CODE').AsString,
          Query.FieldByName('SHIFT_NUMBER').AsInteger,
          Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          Query.FieldByName('WORKSPOT_CODE').AsString,
          TotalProdMin, //PRODUCTION_MINUTE
          TotalBreaksMin //PAYED BREAKS MINUTE
        );
}
        inc(MyAddCount);
      end;
      Query.Next;
    end;
    Query.Close;
  end;
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.ProductionHoursStandardPlanning(Lst: TStringList);
var
  i: Integer;
begin
  MyAddCount := 0;
  i := 0;
  //check standard planning for each day
  while StartDate + i <= EndDate do
  begin
    ProductionHoursStandardPlanning(Lst, StartDate + i);
    i := i + 1;
  end;
  Log('- Added ' + IntToStr(MyAddCount) +
    ' records based on Standard Planning.');
end;

// RV082.4. Change for SO-550491.
procedure TDialogProcessAbsenceHrsF.DeleteAutoScanByEmpDate(
  ADate: TDateTime; AEmployeeNumber: Integer);
var
  SelectStr: String;
begin
  // Delete scans for a certain date and employee.
  SelectStr :=
    'DELETE ' + NL +
    'FROM ' + NL +
    '  TIMEREGSCANNING T ' + NL +
    'WHERE ' + NL +
    // MRA:7-DEC-2010. Only for 'A.S' ???
//    '  T.IDCARD_IN = ''A.S.'' AND ' +
    '  TRUNC(T.DATETIME_IN) = :DATETIME_IN AND ' + NL +
    '  T.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('DATETIME_IN').AsDate := ADate;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ExecSql;
  end;
end;

// MRA:28-JUN-2010. RV066.1.
procedure TDialogProcessAbsenceHrsF.DeleteProdHourPerEmpRecordByEmpDate(
  ADate: TDateTime;
  AEmployeeNumber: Integer;
  AManualYN: String);
var
  SelectStr: String;
begin
//EMPLOYEE.BOOK_PROD_HRS_YN = 'Y'.
  SelectStr :=
    'DELETE ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P' + NL +
    'WHERE ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND ' + NL +
    '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' + NL +
    '  P.MANUAL_YN = :MANUAL_YN';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('PRODHOUREMPLOYEE_DATE').AsDate := ADate;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ParamByName('MANUAL_YN').AsString := AManualYN;
    QueryTmp.ExecSql;
  end;
end;

// RV067.MRA.35 Change for 550491
procedure TDialogProcessAbsenceHrsF.DeleteSalHourPerEmpRecordByEmpDate(
  ADate: TDateTime; AEmployeeNumber: Integer; AManualYN: String);
var
  SelectStr: String;
begin
//EMPLOYEE.BOOK_PROD_HRS_YN = 'Y'.
  SelectStr :=
    'DELETE ' + NL +
    'FROM ' + NL +
    '  SALARYHOURPEREMPLOYEE S' + NL +
    'WHERE ' + NL +
    '  S.SALARY_DATE = :SALARY_DATE AND ' + NL +
    '  S.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' + NL +
    '  S.MANUAL_YN = :MANUAL_YN';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('SALARY_DATE').AsDate := ADate;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ParamByName('MANUAL_YN').AsString := AManualYN;
    QueryTmp.ExecSql;
  end;
end;

procedure TDialogProcessAbsenceHrsF.DeleteProdHourPerEmpPerTypeRecordByEmpDate(
  ADate: TDateTime;
  AEmployeeNumber: Integer;
  AManualYN: String);
var
  SelectStr: String;
begin
//EMPLOYEE.BOOK_PROD_HRS_YN = 'Y'.
  SelectStr :=
    'DELETE ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLPERTYPE P' + NL +
    'WHERE ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND ' + NL +
    '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' + NL +
    '  P.MANUAL_YN = :MANUAL_YN';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('PRODHOUREMPLOYEE_DATE').AsDate := ADate;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ParamByName('MANUAL_YN').AsString := AManualYN;
    QueryTmp.ExecSql;
  end;
end;

// MRA:28-JUN-2010. RV066.1.
procedure TDialogProcessAbsenceHrsF.CheckDeleteProdHours;
var
  ThisDate: TDateTime;
begin
  with DialogProcessAbsenceHrsDM do
  begin
    // For all employees of type 'Book_Prod_Hrs_YN' equals 'Y'.
    qryEmpBookProdHrs.Close;
    qryEmpBookProdHrs.ParamByName('PLANTFROM').AsString := PlantFrom;
    qryEmpBookProdHrs.ParamByName('PLANTTO').AsString := PlantTo;
    qryEmpBookProdHrs.ParamByName('TEAMFROM').AsString := TeamFrom;
    qryEmpBookProdHrs.ParamByName('TEAMTO').AsString := TeamTo;
    qryEmpBookProdHrs.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    qryEmpBookProdHrs.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    // RV082.5. Check for all user that are NOT SCANNERS.
    qryEmpBookProdHrs.ParamByName('IS_SCANNING_YN').AsString := 'N';
    // RV082.5. Addition of team-selection!
    qryEmpBookProdHrs.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    // RV082.5. Do not look if this is Yes. In that way it is possible to
    //          have a way to initialise all.
//    qryEmpBookProdHrs.ParamByName('BOOK_PROD_HRS_YN').AsString := 'Y';
    qryEmpBookProdHrs.Open;
    while not qryEmpBookProdHrs.Eof do
    begin
      ThisDate := StartDate;
      while ThisDate <= EndDate do
      begin
        // RV082.4. Change for SO-550491.
        // Delete automatic scans.
        DeleteAutoScanByEmpDate(ThisDate,
          qryEmpBookProdHrs.FieldByName('EMPLOYEE_NUMBER').AsInteger);

        // Delete production hours per employee
        DeleteProdHourPerEmpRecordByEmpDate(
          ThisDate,
          qryEmpBookProdHrs.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          'N');

        // RV067.MRA.35 Change for 550491
        // Delete salary hours per employee
        DeleteSalHourPerEmpRecordByEmpDate(
          ThisDate,
          qryEmpBookProdHrs.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          'N');

        // Delete production hours per employee per type.
        if (SystemDM.GetFillProdHourPerHourTypeYN = CHECKEDVALUE) then
          DeleteProdHourPerEmpPerTypeRecordByEmpDate(
            ThisDate,
            qryEmpBookProdHrs.FieldByName('EMPLOYEE_NUMBER').AsInteger,
            'N');

        ThisDate := ThisDate + 1;
      end;
      qryEmpBookProdHrs.Next;
    end;
  end;
end;

// RV082.4. Not needed.
(*
procedure TDialogProcessAbsenceHrsF.DeleteProdHourPerEmployeeRecord(
          ADate: TDateTime;
          APlantCode: String;
          AShiftNumber: Integer;
          AEmployeeNumber: Integer;
          AWorkspotCode: String;
          AProdMin: Integer; //PRODUCTION_MINUTE
          ABreaksMin: Integer; //PAYED BREAKS MINUTE
          AJobCode: String;
          AManualYN: String
        );
var
  SelectStr: String;
begin
//EMPLOYEE.BOOK_PROD_HRS_YN = 'Y'.
  SelectStr :=
    'DELETE ' + NL +
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE P' + NL +
    'WHERE ' + NL +
    '  P.PRODHOUREMPLOYEE_DATE = :PRODHOUREMPLOYEE_DATE AND ' + NL +
    '  P.PLANT_CODE = :PLANT_CODE AND ' + NL +
    '  P.SHIFT_NUMBER = :SHIFT_NUMBER AND ' + NL +
    '  P.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND ' + NL +
    '  P.WORKSPOT_CODE = :WORKSPOT_CODE AND ' + NL +
    '  P.JOB_CODE = :JOB_CODE AND ' + NL +
    '  P.MANUAL_YN = :MANUAL_YN';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('PRODHOUREMPLOYEE_DATE').AsDate := ADate;
    QueryTmp.ParamByName('PLANT_CODE').AsString := APlantCode;
    QueryTmp.ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
    QueryTmp.ParamByName('JOB_CODE').AsString := AJobCode;
    QueryTmp.ParamByName('MANUAL_YN').AsString := AManualYN;
    QueryTmp.ExecSql;
  end;
end; // DeleteProdHourPerEmployeeRecord
*)

// RV082.4.
procedure TDialogProcessAbsenceHrsF.InsertAutoScanRecord(
  ADatetime_in: TDateTime; AEmployeeNumber: Integer; APlantCode,
  AWorkspotCode, AJobCode: String; AShiftNumber: Integer;
  ADatetime_out: TDateTime);
var
  SelectStr: String;
  DateTimeOutORI: TDateTime;
  procedure InsertScan;
  begin
    with DialogProcessAbsenceHrsDM do
    begin
      try
        QueryTmp.Close;
        QueryTmp.SQL.Clear;
        QueryTmp.SQL.Add(SelectStr);
        QueryTmp.ParamByName('DATETIME_IN').AsDateTime := ADatetime_in;
        QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        QueryTmp.ParamByName('PLANT_CODE').AsString := APlantCode;
        QueryTmp.ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
        QueryTmp.ParamByName('JOB_CODE').AsString := AJobCode;
        QueryTmp.ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
        QueryTmp.ParamByName('DATETIME_OUT').AsDateTime := ADatetime_out;
        QueryTmp.ParamByName('PROCESSED_YN').AsString := CHECKEDVALUE;
        QueryTmp.ParamByName('IDCARD_IN').AsString := 'A.S.';
        QueryTmp.ParamByName('IDCARD_OUT').AsString := 'A.S.';
        QueryTmp.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        QueryTmp.ExecSql;
      except
        // Ignore error, can be duplicate value.
      end;
    end;
  end; // InsertScan
begin
  SelectStr :=
    'INSERT INTO TIMEREGSCANNING( ' + NL +
    '  DATETIME_IN, EMPLOYEE_NUMBER, PLANT_CODE, WORKSPOT_CODE, ' + NL +
    '  JOB_CODE, SHIFT_NUMBER, DATETIME_OUT, PROCESSED_YN, ' + NL +
    '  IDCARD_IN, IDCARD_OUT, CREATIONDATE, MUTATIONDATE, MUTATOR ' + NL +
    ') VALUES ( ' + NL +
    '  :DATETIME_IN, :EMPLOYEE_NUMBER, :PLANT_CODE, :WORKSPOT_CODE, ' + NL +
    '  :JOB_CODE, :SHIFT_NUMBER, :DATETIME_OUT, :PROCESSED_YN, ' + NL +
    '  :IDCARD_IN, :IDCARD_OUT, SYSDATE, SYSDATE, :MUTATOR ' + NL +
    '  )';

    // Handle scan bigger then 12 hours.
    if ((ADatetime_out - ADatetime_in) > 0.5) then
    begin
      // Splits scan into two scans.
      DateTimeOutORI := ADatetime_out;
      ADatetime_out := ADatetime_in + 0.5;
      InsertScan;
      ADatetime_in := ADatetime_out;
      ADatetime_out := DateTimeOutORI;
      InsertScan;
    end
    else
      InsertScan;
end;

// RV082.4. Not needed.
(*
procedure TDialogProcessAbsenceHrsF.InsertProdHourPerEmployeeRecord(
          ADate: TDateTime;
          APlantCode: String;
          AShiftNumber: Integer;
          AEmployeeNumber: Integer;
          AWorkspotCode: String;
          AProdMin: Integer; //PRODUCTION_MINUTE
          ABreaksMin: Integer; //PAYED BREAKS MINUTE
          AJobCode: String;
          AManualYN: String
        );
var
  SelectStr: String;
begin
//EMPLOYEE.BOOK_PROD_HRS_YN = 'Y'.
  SelectStr :=
    'INSERT INTO PRODHOURPEREMPLOYEE( ' + NL +
    ' PRODHOUREMPLOYEE_DATE, PLANT_CODE, SHIFT_NUMBER, EMPLOYEE_NUMBER, ' + NL +
    ' WORKSPOT_CODE, JOB_CODE, MANUAL_YN, PRODUCTION_MINUTE, ' + NL +
    ' PAYED_BREAK_MINUTE, CREATIONDATE, MUTATIONDATE, MUTATOR ' + NL +
    ') VALUES ( ' + NL +
    ' :PRODHOUREMPLOYEE_DATE, :PLANT_CODE, :SHIFT_NUMBER, :EMPLOYEE_NUMBER, ' + NL +
    ' :WORKSPOT_CODE, :JOB_CODE, :MANUAL_YN, :PRODUCTION_MINUTE, ' + NL +
    ' :PAYED_BREAK_MINUTE, SYSDATE, SYSDATE, :MUTATOR ' + NL +
    '  )';

  with DialogProcessAbsenceHrsDM do
  begin
    QueryTmp.Close;
    QueryTmp.SQL.Clear;
    QueryTmp.SQL.Add(SelectStr);
    QueryTmp.ParamByName('PRODHOUREMPLOYEE_DATE').AsDate := ADate;
    QueryTmp.ParamByName('PLANT_CODE').AsString := APlantCode;
    QueryTmp.ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
    QueryTmp.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
    QueryTmp.ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
    QueryTmp.ParamByName('JOB_CODE').AsString := AJobCode;
    QueryTmp.ParamByName('MANUAL_YN').AsString := AManualYN;
    QueryTmp.ParamByName('PRODUCTION_MINUTE').AsInteger := AProdMin;
    QueryTmp.ParamByName('PAYED_BREAK_MINUTE').AsInteger := ABreaksMin;
    QueryTmp.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
    QueryTmp.ExecSql;
  end;
end; // InsertProdHourPerEmployeeRecord
*)

// RV082.4. Not needed.
(*
procedure TDialogProcessAbsenceHrsF.UpdateProdHourPerEmployee(
          ADate: TDateTime;
          APlantCode: String;
          AShiftNumber: Integer;
          AEmployeeNumber: Integer;
          AWorkspotCode: String;
          AProdMin: Integer; //PRODUCTION_MINUTE
          ABreaksMin:Integer //PAYED BREAKS MINUTE
        );
var
  EmployeeData: TScannedIDCard;
begin
//If there were existing PHE-records, then these must first be deleted.
//The primary key for PHE is:
//PRODHOUREMPLOYEE_DATE, PLANT_CODE, SHIFT_NUMBER, EMPLOYEE_NUMBER, WORKSPOT_CODE, JOB_CODE, MANUAL_YN.
//Fill these fields with the corresponding fields from the EP-record.

//Field PHE.JOB_CODE must be filled with '0'. Field PHE.MANUAL_YN must be filled with 'N'.
  DeleteProdHourPerEmployeeRecord(
    ADate,
    APlantCode,
    AShiftNumber,
    AEmployeeNumber,
    AWorkspotCode,
    AProdMin,
    ABreaksMin,
    '0',//JOB_CODE
    'N' //MANUAL_YN
  );
  InsertProdHourPerEmployeeRecord(
    ADate,
    APlantCode,
    AShiftNumber,
    AEmployeeNumber,
    AWorkspotCode,
    AProdMin,
    ABreaksMin,
    '0',//JOB_CODE
    'N' //MANUAL_YN
  );

  // MRA:28-JUN-2010. Addition: Also create ProdHourPerEmpPerType-records.
  EmptyIDCard(EmployeeData);
  EmployeeData.EmployeeCode := AEmployeeNumber;
  EmployeeData.ShiftNumber := AShiftNumber;
  EmployeeData.WorkSpotCode := AWorkspotCode;
  EmployeeData.JobCode := '0';
  EmployeeData.PlantCode := APlantCode;
  EmployeeData.RoundMinute := 1;

  // Also calculate salary hours!
  // This will also fill prod-hour-per-emp-per-type.
  // RV074.2. Bugfix. Argument for replace must be 'False' instead of 'True'!
  GlobalDM.UpdateSalaryHour(ADate, EmployeeData, 1, AProdMin, 'N', False, True);

{
  GlobalDM.FillProdHourPerHourType(EmployeeData, ADate,
    1 {HourTypeNumber}, AProdMin, 'N', True);
}

  // save changed employees for salary recalculation.
  ChangedEmployeeList.Add(IntToStr(AEmployeeNumber));
end; // UpdateProdHourPerEmployee
*)

// RV082.4. SO-550491.
procedure TDialogProcessAbsenceHrsF.ProductionHoursDayPlanning(Lst: TStringList);
var
  SelectStr, Line: String;
  WeekDay: Word;
  // RV082.4.
{  TotalProdMin, TotalBreaksMin, } ProdMin, BreaksMin: integer;
  TimeBlockNumber: Integer;
  EmployeeFound: Boolean;
  DateTimeIn, DateTimeOut: TDateTime;

  function ValidTimeBlock(ATimeBlock: String): Boolean;
  begin
    with DialogProcessAbsenceHrsDM do
      Result :=
        (Query.FieldByName(ATimeBlock).AsString = 'A') or
        (Query.FieldByName(ATimeBlock).AsString = 'B') or
        (Query.FieldByName(ATimeBlock).AsString = 'C');
  end;
begin
  MyAddCount := 0;
  // MRA: 28-JUN-2010.
  CheckDeleteProdHours;

  //Get the employee records following the dialog selection criteria which have
  //data in employeeplanning table and BOOK_PROD_HRS_YN = 'Y'
  // RV082.6. Get Department from Workspot for determining timeblocks-per-dept.
  SelectStr :=
    'SELECT ' + NL +
    '  EP.PLANT_CODE, EP.EMPLOYEEPLANNING_DATE, EP.SHIFT_NUMBER, ' + NL +
    '  EP.WORKSPOT_CODE,' + NL +
    '  EP.EMPLOYEE_NUMBER, EP.SCHEDULED_TIMEBLOCK_1, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_2, EP.SCHEDULED_TIMEBLOCK_3, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_4, EP.SCHEDULED_TIMEBLOCK_5, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_6, EP.SCHEDULED_TIMEBLOCK_7, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_8, EP.SCHEDULED_TIMEBLOCK_9, ' + NL +
    '  EP.SCHEDULED_TIMEBLOCK_10, W.DEPARTMENT_CODE ' + NL +
    'FROM ' + NL +
    '  EMPLOYEE EM JOIN EMPLOYEEPLANNING EP ON ' + NL +
    '    EM.EMPLOYEE_NUMBER = EP.EMPLOYEE_NUMBER ' + NL +
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    EP.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    EP.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL +
    'WHERE ' + NL +
    '  ( ' + NL +
    '   (:USER_NAME = ''*'') OR ' + NL +
    '   (EM.TEAM_CODE IN ' + NL +
    '     (SELECT U.TEAM_CODE FROM TEAMPERUSER U ' + NL +
    '      WHERE U.USER_NAME = :USER_NAME)) ' + NL +
    '  ) AND ' + NL +
    '  EP.EMPLOYEEPLANNING_DATE >= :DATEFROM AND ' + NL +
    '  EP.EMPLOYEEPLANNING_DATE <= :DATETO AND ' + NL +
    '  EP.PLANT_CODE >= :PLANTFROM AND ' + NL +
    '  EP.PLANT_CODE <= :PLANTTO AND ' + NL +
    '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
    '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
    '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
    '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL +
    '  EM.BOOK_PROD_HRS_YN = ''Y'' ' + NL +
    'ORDER BY ' + NL +
    '  EP.EMPLOYEE_NUMBER, EP.EMPLOYEEPLANNING_DATE ';

  with DialogProcessAbsenceHrsDM do
  begin
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add(SelectStr);
    if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
      Query.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
    else
      Query.ParamByName('USER_NAME').AsString := '*';
    Query.ParamByName('DATEFROM').AsDate := StartDate;
    Query.ParamByName('DATETO').AsDate := EndDate;
    Query.ParamByName('PLANTFROM').AsString := PlantFrom;
    Query.ParamByName('PLANTTO').AsString := PlantTo;
    Query.ParamByName('TEAMFROM').AsString := TeamFrom;
    Query.ParamByName('TEAMTO').AsString := TeamTo;
    Query.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
    Query.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
    Query.Open;

    //An employee who is planned has the value 'A', 'B' or 'C' in one or more timeblocks.
    //To determine the production hours that were made, the found timeblocks (with contents 'A', 'B', or 'C')
    Query.First;
    while not Query.Eof do
    begin
      // Get day of week - depends on 'Week Starts On'
      WeekDay := DayInWeek(SystemDM.WeekStartsOn,
        Query.FieldByName('EMPLOYEEPLANNING_DATE').Value);

      // RV082.4.
{      TotalProdMin   := 0;
      TotalBreaksMin := 0; }
      EmployeeFound  := False;
      for TimeBlockNumber := 1 to SystemDM.MaxTimeblocks do
      begin
        if ValidTimeBlock('SCHEDULED_TIMEBLOCK_' + IntToStr(TimeBlockNumber)) then
        begin
          DeterminePlanningTimeBlocks(ProdMin, BreaksMin, TimeBlockNumber, WeekDay,
            Query.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime,
            DateTimeIn, DateTimeOut);
          EmployeeFound  := True;
          // RV082.4. Insert Auto Scan instead of ProductionHourPerEmployee
          if (DateTimeIn <> 0) and (DateTimeOut <> 0) then
            InsertAutoScanRecord(
              DatetimeIn,
              Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
              Query.FieldByName('PLANT_CODE').AsString,
              Query.FieldByName('WORKSPOT_CODE').AsString,
              '0',
              Query.FieldByName('SHIFT_NUMBER').AsInteger,
              DateTimeOut);
{$IFDEF DEBUG}
if (DateTimeIn <> 0) and (DateTimeOut <> 0) then
  WDebugLog('- InsertAutoScan DP. ' +
    ' Date=' + DateToStr(Query.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime) +
    ' DateIn=' + DateTimeToStr(DateTimeIn) +
    ' DateOut=' + DateTimeToStr(DateTimeOut) +
    ' Plant=' + Query.FieldByName('PLANT_CODE').AsString +
    ' Shift=' + IntToStr(Query.FieldByName('SHIFT_NUMBER').AsInteger) +
    ' Emp=' +  IntToStr(Query.FieldByName('EMPLOYEE_NUMBER').AsInteger) +
    ' WS=' +  Query.FieldByName('WORKSPOT_CODE').AsString +
    ' ProdMin=' + IntToStr(ProdMin) +
    ' BreaksMin=' + IntToStr(BreaksMin)
    );
{$ENDIF}
          // RV082.4.
{          TotalProdMin := TotalProdMin + ProdMin;
          TotalBreaksMin := TotalBreaksMin + BreaksMin; }
        end;
      end;

      if EmployeeFound then
      begin
        //Save found records in lst to check it when standardemployee table will be used
        //PLANT_CODE EMPLOYEEPLANNING_DATE SHIFT_NUMBER DEPARTMENT_CODE WORKSPOT_CODE EMPLOYEE_NUMBER
        // RV072.1. Reduce key to Employee and Date, or it will result in
        //          double records.
        Line :=
          FormatDateTime('dd/mm/yyyy', Query.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime) + ' ' +
          Query.FieldByName('EMPLOYEE_NUMBER').AsString;
{
        Line :=
          Query.FieldByName('PLANT_CODE').AsString + ' ' +
          FormatDateTime('dd/mm/yyyy', Query.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime) + ' ' +
          Query.FieldByName('SHIFT_NUMBER').AsString + ' ' +
          Query.FieldByName('DEPARTMENT_CODE').AsString + ' ' +
          Query.FieldByName('WORKSPOT_CODE').AsString + ' ' +
          Query.FieldByName('EMPLOYEE_NUMBER').AsString;
}
        // RV072.1. Changed from '<> -1' to '= -1' !
        if Lst.IndexOf(Line) = -1 then // not found?
          Lst.Add(Line);

 //When the production hours are determined, then PRODHOURPEREMPLOYEE-records (PHE)
 //can be created based on the fields from the EP-record.
 //Field PHE.PRODUCTION_MINUTE must be filled with the calculated production minutes (without payed break minutes).
 //Field PHE.PAYED_BREAK_MINUTE must be filled with the calculated payed-break minutes.
{
        UpdateProdHourPerEmployee(
          Query.FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime,
          Query.FieldByName('PLANT_CODE').AsString,
          Query.FieldByName('SHIFT_NUMBER').AsInteger,
          Query.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          Query.FieldByName('WORKSPOT_CODE').AsString,
          TotalProdMin, //PRODUCTION_MINUTE
          TotalBreaksMin //PAYED BREAKS MINUTE
        );
}
        inc(MyAddCount);
      end;
      Query.Next;
    end;
    Query.Close;
  end;
  Log('- Added ' + IntToStr(MyAddCount) + ' records based on Day Planning.');
  ProcessbarUpdate;
end;

procedure TDialogProcessAbsenceHrsF.ActionTailorMadeNonScanners;
var
  Lst: TStringList;
begin
  Log('Start: Action Tailor Made Non Scanners');
  Lst := TStringList.Create;
  try
    //check first the employee planning table and save the found records to lst
//    SystemDM.Pims.StartTransaction; // TD-23672
    try
{$IFDEF DEBUG}
  WDebugLog('ProductionHoursDayPlanning');
{$ENDIF}
      ProductionHoursDayPlanning(Lst);
      // TD-23672
{      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit; }
    except
      // TD-23672
{      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback; }
    end;
    //check the standard employee planning table and complete if the corresponding
    //record is not found in lst
//    SystemDM.Pims.StartTransaction; // TD-23672
    try
{$IFDEF DEBUG}
  WDebugLog('ProductionHoursStandardPlanning');
{$ENDIF}
      ProductionHoursStandardPlanning(Lst);
      // TD-23672
{      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit; }
    except
      // TD-23672
{
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
}
    end;
  finally
    Lst.Free;
    Log('End: Action Tailor Made Non Scanners');
  end;
end;

// RV067.MRA.31 Compare absence hours with balance-table. If there are
//              differences, then correct this.
// 20013183. Added travel time-fields for balance in qryATCheck
procedure TDialogProcessAbsenceHrsF.CorrectionBalanceAbsenceHours;
var
  DateFrom, DateTo: TDateTime;
  YearFrom, YearTo, {WeekTo, } Month, Day: Word;
  Year, ATAbsenceMinutes, ChangeCount: Integer;
  UpdateField: String;
  MyAbsenceType: Char;
  // SC-50025602
  // First INIT all values!
  // If this is not done, then when nothing was found for absence, but
  // there was still a value in balance, it will keep the old value!
  procedure InitAbsenceTotal;
  begin
    with DialogProcessAbsenceHrsDM do
    begin
      try
        with qryAHEInit do
        begin
          Close;
          ParamByName('ABSENCE_YEAR').AsInteger := Year;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ParamByName('PLANTFROM').AsString := PlantFrom;
          ParamByName('PLANTTO').AsString := PlantTo;
          ParamByName('TEAMFROM').AsString := TeamFrom;
          ParamByName('TEAMTO').AsString := TeamTo;
          ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
          ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
          ExecSQL;
        end;
      except
      end;
    end;
  end; // InitAbsenceTotal
begin
  Log('Start: Correction Balance Absence Hours');
  ChangeCount := 0;
  try
    with DialogProcessAbsenceHrsDM do
    begin
      DecodeDate(StartDate, YearFrom, Month, Day);
      // Use EndDate for determine of YearTo.
      DecodeDate(EndDate, YearTo, Month, Day);
      for Year := YearFrom to YearTo do
      begin
        // SC-50025602
        // First INIT all values!!!
        InitAbsenceTotal;
        // TD-24278 This is WRONG; For the balance of the year, start
        // from 1-1 till 31-12, instead of using first day of week
        // and last day of week.
        // Reason: A week can start in previous year and end in current year.
{
        // Determine first day of first week of the year.
        DateFrom := ListProcsF.DateFromWeek(Year, 1, 1);
        // Determine last day of last week of the year.
        // Use EndDate when it is this year.
        if Year < YearTo then
          DateTo := ListProcsF.DateFromWeek(Year,
            ListProcsF.WeeksInYear(Year), 7)
        else
          DateTo := EndDate;
}
        // Determine first day of the year (1-Jan).
        DateFrom := EncodeDate(Year, 1, 1);
        // Determine last day of the year (31-Dec).
        if Year < YearTo then
          DateTo := EncodeDate(Year, 12, 31)
        else
          DateTo := EndDate;
        // Be sure the DateTo is not ending in following year.
        if DateTo > EncodeDate(Year, 12, 31) then
          DateTo := EncodeDate(Year, 12, 31);
        Log('- Date Period: ' +
          DateToStr(DateFrom) + ' - ' + DateToStr(DateTo));
        // ABSENCEHOURPEREMPLOYEE-table.
// qryAHECheck.SQL.SaveToFile('c:\temp\qryAHECheck');
        qryAHECheck.Close;
        if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
          qryAHECheck.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser
        else
          qryAHECheck.ParamByName('USER_NAME').AsString := '*';
        // Look at the whole year!
        qryAHECheck.ParamByName('DATEFROM').AsDate := DateFrom; // StartDate;
        qryAHECheck.ParamByName('DATETO').AsDate := DateTo;     // EndDate;
        qryAHECheck.ParamByName('PLANTFROM').AsString := PlantFrom;
        qryAHECheck.ParamByName('PLANTTO').AsString := PlantTo;
        qryAHECheck.ParamByName('TEAMFROM').AsString := TeamFrom;
        qryAHECheck.ParamByName('TEAMTO').AsString := TeamTo;
        qryAHECheck.ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        qryAHECheck.ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        qryAHECheck.Open;
        // ABSENCETOTAL-table.
        qryATCheck.Close;
        qryATCheck.ParamByName('ABSENCE_YEAR').AsInteger := Year;
        qryATCheck.Open;
        while not qryAHECheck.Eof do
        begin
          if qryATCheck.Locate('EMPLOYEE_NUMBER',
            qryAHECheck.FieldByName('EMPLOYEE_NUMBER').AsString, []) then
          begin
            UpdateField := AbsenceTotalField(
              qryAHECheck.FieldByName('ABSENCETYPE_CODE').AsString[1]);

            if UpdateField <> '' then
            begin
              if qryATCheck.FieldByName(UpdateField).AsString <> '' then
                ATAbsenceMinutes := qryATCheck.FieldByName(UpdateField).AsInteger
              else
                ATAbsenceMinutes := 0;
              if qryAHECheck.FieldByName('ABSENCE_MINUTE').AsInteger <>
                ATAbsenceMinutes then
              begin
                MyAbsenceType :=
                  qryAHECheck.FieldByName('ABSENCETYPE_CODE').AsString[1];
                if MyAbsenceType = TRAVELTIME then
                  MyAbsenceType := USED_TRAVELTIME;
                UpdateAbsenceTotalMode(Query,
                  qryAHECheck.FieldByName('EMPLOYEE_NUMBER').AsInteger,
                  Year,
                  qryAHECheck.FieldByName('ABSENCE_MINUTE').AsInteger,
                  MyAbsenceType,
                  False // Do not add to old minutes!
                  );
                inc(ChangeCount);
              end;
            end; // if UpdateField <> '' then
          end; // if qryATCheck.Locate('EMPLOYEE_NUMBER',
          qryAHECheck.Next;
        end; // while not qryAHECheck.Eof do
        qryAHECheck.Close;
        qryATCheck.Close;
      end; // for Year := YearFrom to YearTo do
    end; // with DialogProcessAbsenceHrsDM do
  finally
    Log('- Balance records changed: ' + IntToStr(ChangeCount));
    Log('End: Correction Balance Absence Hours');
  end;
end; // CorrectionBalanceAbsenceHours;

// RV075.6.
// Check and if needed recalculate the PHEPT-records, based
// on PHE-records.
(*
procedure TDialogProcessAbsenceHrsF.CheckRecalcPHEPT(AEmployeeNumber: Integer;
  ADateFrom, ADateTo: TDateTime);
var
  Lst: TStringList;
  Index: Integer;
  Line: String;
  Day, Month, Year: Word;
  PDate: TDateTime;
  PEmp: Integer;
  procedure RecalcPHEPT(AEmployeeNumber: Integer; AProdDate: TDateTime);
    procedure UpdatePHEPT(AProdHourEmployeeDate: TDateTime;
       APlantCode: String; AShiftNumber: Integer; AEmployeeNumber: Integer;
       AWorkspotCode, AJobCode: String; AProductionMinute: Integer;
       AManualYN: String);
    var
      EmployeeData: TScannedIDCard;
    begin
      EmployeeData.EmployeeCode := AEmployeeNumber;
      EmployeeData.PlantCode := APlantCode;
      EmployeeData.WorkspotCode := AWorkspotCode;
      EmployeeData.JobCode := AJobCode;
      EmployeeData.ShiftNumber := AShiftNumber;
      GlobalDM.UpdatePHEPT(EmployeeData,
        AProdHourEmployeeDate, 1, AProductionMinute, AManualYN, True);
    end;
  begin
    with DialogProcessAbsenceHrsDM do
    begin
      qryPHE.Close;
      qryPHE.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
      qryPHE.ParamByName('PRODHOUREMPLOYEE_DATE').AsDateTime := AProdDate;
      qryPHE.Open;
      while not qryPHE.Eof do
      begin
        // PRODHOUREMPLOYEE_DATE, PLANT_CODE, SHIFT_NUMBER, EMPLOYEE_NUMBER,
        // WORKSPOT_CODE, JOB_CODE, PRODUCTION_MINUTE, MANUAL_YN, PAYED_BREAK_MINUTE
        UpdatePHEPT(
          qryPHE.FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime,
          qryPHE.FieldByName('PLANT_CODE').AsString,
          qryPHE.FieldByName('SHIFT_NUMBER').AsInteger,
          qryPHE.FieldByName('EMPLOYEE_NUMBER').AsInteger,
          qryPHE.FieldByName('WORKSPOT_CODE').AsString,
          qryPHE.FieldByName('JOB_CODE').AsString,
          qryPHE.FieldByName('PRODUCTION_MINUTE').AsInteger,
          qryPHE.FieldByName('MANUAL_YN').AsString
          );
        qryPHE.Next;
      end;
      qryPHE.Close;
    end;
  end;
  // Extract a field from a given line.
  // The line will be truncated, the part that has been
  // found is taken from the line.
  function ExtractFieldSep(VAR ALine: String; ASeparator: String): String;
  var
    i: Integer;
    Part: String;
  begin
    i := Pos(ASeparator, ALine);
    if i > 0 then
    begin
      Part := Copy(ALine, 1, i-1);
      ALine := Copy(ALine, i+1, Length(ALine));
    end
    else
    begin
      Part := ALine;
      ALine := '';
    end;
    Result := Part;
  end;
begin
  Lst := TStringList.Create;
  try
    with DialogProcessAbsenceHrsDM do
    begin
      // Fase 1
      // Check PHEPT with PHE.
      with qryPHEPTCheck1 do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('DATEFROM').AsDateTime := ADateFrom;
        ParamByName('DATETO').AsDateTime := ADateTo;
        Open;
        while not Eof do
        begin
          DecodeDate(
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime, Year,
            Month, Day);
         Line :=
            IntToStr(Year) + ';' + IntToStr(Month) + ';' + IntToStr(Day) + ';' +
            IntToStr(AEmployeeNumber);
          if Lst.IndexOf(Line) = -1 then // not found?
            Lst.Add(Line);
          Next;
        end;
        Close;
      end;
      // Fase 2
      // Check PHE with PHEPT. (otherway round).
      with qryPHEPTCheck2 do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('DATEFROM').AsDateTime := ADateFrom;
        ParamByName('DATETO').AsDateTime := ADateTo;
        Open;
        while not Eof do
        begin
          DecodeDate(
            FieldByName('PRODHOUREMPLOYEE_DATE').AsDateTime, Year,
            Month, Day);
         Line :=
            IntToStr(Year) + ';' + IntToStr(Month) + ';' + IntToStr(Day) + ';' +
            IntToStr(AEmployeeNumber);
          if Lst.IndexOf(Line) = -1 then // not found?
            Lst.Add(Line);
          Next;
        end;
        Close;
      end;
      // For the differences, make changes to PHEPT-table.
      for Index := 0 to Lst.Count - 1 do
      begin
        try
          Line := Lst.Strings[Index];
          Year := StrToInt(ExtractFieldSep(Line, ';'));
          Month := StrToInt(ExtractFieldSep(Line, ';'));
          Day := StrToInt(ExtractFieldSep(Line, ';'));
          PEmp := StrToInt(ExtractFieldSep(Line, ';'));
          PDate := EncodeDate(Year, Month, Day);
          // First delete the records for Manual and non-manual.
          DeleteProdHourPerEmpPerTypeRecordByEmpDate(PDate, PEmp, 'Y');
          DeleteProdHourPerEmpPerTypeRecordByEmpDate(PDate, PEmp, 'N');
          // Now update records based on PHE.
          RecalcPHEPT(PEmp, PDate);
        except
          // Ignore error
        end;
      end;
    end;
  finally
    Lst.Free;
  end;
end; // CheckRecalcPHEPT
*)

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogProcessAbsenceHrsF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogProcessAbsenceHrsF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

// RV099.1.
// When Vacation/Holiday is set in Employee Availability
// then see if there are TFT hours (available hours) in balance.
// If this is the case, then replace Vacation by TFT in
// Employee Availability.
// Only do this based on Country-settings.
function TDialogProcessAbsenceHrsF.ActionTFTHolidayCorrection: Boolean;
var
  SelectStr: String;
  SelectPart, MainPart, OrderByPart: String;
  ChangeCount, AddCountDummy: Integer;
  TFTAvailableMinute: Integer;
  TotalSalaryMinutesDummy, TotalMinutesDummy: Integer;
  // Init Queries:
  //  Build select-statements and open queries here.
  function InitQueries: Boolean;
  begin
//    Result := False;
    with DialogProcessAbsenceHrsDM do
    begin
      // Step 1.
      // Find Employee Availability-records having Vacation/Holiday set
      // according to COUNTRY-settings:
      // - COUNTRY.TFT_HOL_AUTO_REPLACE_YN = 'Y'
      SelectPart :=
        'SELECT ' + NL +
        '  EA.PLANT_CODE, EA.EMPLOYEEAVAILABILITY_DATE, ' + NL +
        '  EA.SHIFT_NUMBER, EA.EMPLOYEE_NUMBER, ' + NL +
        '  EM.DEPARTMENT_CODE, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_1, EA.AVAILABLE_TIMEBLOCK_2, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_3, EA.AVAILABLE_TIMEBLOCK_4, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_5, EA.AVAILABLE_TIMEBLOCK_6, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_7, EA.AVAILABLE_TIMEBLOCK_8, ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_9, EA.AVAILABLE_TIMEBLOCK_10, ' + NL +
        '  AR.ABSENCEREASON_CODE AS TFTAR, ' + NL +
        '  AR2.ABSENCEREASON_CODE AS HOLAR ' + NL;
      MainPart :=
        'FROM EMPLOYEEAVAILABILITY EA INNER JOIN EMPLOYEE EM ON ' + NL +
        '  EA.EMPLOYEE_NUMBER = EM.EMPLOYEE_NUMBER ' + NL +
        '  INNER JOIN PLANT P ON ' + NL +
        '  EM.PLANT_CODE = P.PLANT_CODE ' + NL +
        '  INNER JOIN COUNTRY C ON ' + NL +
        '  P.COUNTRY_ID = C.COUNTRY_ID ' + NL +
        '  INNER JOIN ABSENCEREASON AR ON ' + NL +
        '  C.TFT_ABSENCEREASON_ID = AR.ABSENCEREASON_ID ' + NL +
        '  INNER JOIN ABSENCEREASON AR2 ON ' + NL +
        '  C.HOL_ABSENCEREASON_ID = AR2.ABSENCEREASON_ID ' + NL +
        'WHERE ' + NL +
        ' ( ' +
        '  (:USER_NAME = ''*'') OR ' +
        '  (EM.TEAM_CODE IN ' +
        '    (SELECT U.TEAM_CODE FROM TEAMPERUSER U WHERE U.USER_NAME = :USER_NAME)) ' +
        ') AND ' + NL +
        '  EA.EMPLOYEEAVAILABILITY_DATE >= :DATEFROM AND ' + NL +
        '  EA.EMPLOYEEAVAILABILITY_DATE <= :DATETO AND ' + NL +
        '  EA.PLANT_CODE >= :PLANTFROM AND ' + NL +
        '  EA.PLANT_CODE <= :PLANTTO AND ' + NL +
        '  EM.TEAM_CODE >= :TEAMFROM AND ' + NL +
        '  EM.TEAM_CODE <= :TEAMTO AND ' + NL +
        '  EM.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND ' + NL +
        '  EM.EMPLOYEE_NUMBER <= :EMPLOYEETO AND ' + NL +
        '  C.TFT_HOL_AUTO_REPLACE_YN = ''Y'' AND ' + NL +
        '  (EA.AVAILABLE_TIMEBLOCK_1 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_2 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_3 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_4 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_5 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_6 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_7 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_8 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_9 = AR2.ABSENCEREASON_CODE OR ' + NL +
        '  EA.AVAILABLE_TIMEBLOCK_10 = AR2.ABSENCEREASON_CODE) ' + NL;
      OrderByPart :=
        'ORDER BY ' + NL +
        '  EA.EMPLOYEE_NUMBER, EA.EMPLOYEEAVAILABILITY_DATE ';
      SelectStr :=
        SelectPart +
        MainPart +
        OrderByPart;

      // Query with only employees
      SelectStr :=
        'SELECT ' + NL +
        '  EA.EMPLOYEE_NUMBER ' + NL +
        MainPart +
        'GROUP BY ' + NL +
        '  EA.EMPLOYEE_NUMBER ' + NL +
        'ORDER BY ' + NL +
        '  EA.EMPLOYEE_NUMBER ' + NL;
      with QueryTmp do
      begin
        Close;
        SQL.Clear;
        SQL.Add(SelectStr);
        ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
        ParamByName('DATEFROM').AsDate := StartDate;
        ParamByName('DATETO').AsDate := EndDate;
        ParamByName('PLANTFROM').AsString := PlantFrom;
        ParamByName('PLANTTO').AsString := PlantTo;
        ParamByName('TEAMFROM').AsString := TeamFrom;
        ParamByName('TEAMTO').AsString := TeamTo;
        ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
        ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
        Open;
      end;
      Result := not QueryTmp.IsEmpty;
      if Result then
      begin
        // When something was found, first:
        // - Handle the current employee availability
        // - correct the balances
        // ATTENTION: SOME OTHER ACTIONS ALSO USES VARIABLE QUERY!!!
        ActionAbsenceHourPerEmployee;
        ActionEmployeeAvailability;
        ActionEmployeeAvailabilityNoScanEmployees;
        CorrectionBalanceAbsenceHours;
        // Main Query with all details
        SelectStr :=
          SelectPart +
          MainPart +
          OrderByPart;
        with Query do
        begin
          Close;
          SQL.Clear;
          SQL.Add(SelectStr);
          ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
          ParamByName('DATEFROM').AsDate := StartDate;
          ParamByName('DATETO').AsDate := EndDate;
          ParamByName('PLANTFROM').AsString := PlantFrom;
          ParamByName('PLANTTO').AsString := PlantTo;
          ParamByName('TEAMFROM').AsString := TeamFrom;
          ParamByName('TEAMTO').AsString := TeamTo;
          ParamByName('EMPLOYEEFROM').AsInteger := EmployeeFrom;
          ParamByName('EMPLOYEETO').AsInteger := EmployeeTo;
          Open;
        end;
        // Testing
        Log(Format(SLogFoundEmployeeNo, [Query.RecordCount]));
      end; // if not QueryTmp.IsEmpty then
    end; // with DialogProcessAbsenceHrsDM do
  end; // InitQueries
  // Determine Total TFT Available Minute from balance
  // for a certain employee and year based on enddate of selection.
  function DetermineTotalTFTAvailableMinute: Integer;
  var
    Year, Month, Day: Word;
    // TD-22355
    function TestOnNull(AValue: String): Integer;
    begin
      Result := 0;
      if AValue <> '' then
        try
          Result := StrToInt(AValue);
        except
          Result := 0;
        end;
    end;
  begin
    Result := 0;
    with DialogProcessAbsenceHrsDM do
    begin
      // Check if there are TFT-hours available in balance-table for the employee
      // and the same year of the selection.
      // Determine TFT-hours here!
      DecodeDate(EndDate, Year, Month, Day);
      // TD-22355 Use TQuery instead of TTable
      qryAbsenceTotal.Close;
      qryAbsenceTotal.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
        QueryTmp.FieldByName('EMPLOYEE_NUMBER').Value;
      qryAbsenceTotal.ParamByName('ABSENCE_YEAR').AsInteger := Year;
      qryAbsenceTotal.Open;
      if not qryAbsenceTotal.Eof then
      begin
        try
          Result :=
            TestOnNull(qryAbsenceTotal.FieldByName('LAST_YEAR_TFT_MINUTE').AsString) +
            TestOnNull(qryAbsenceTotal.FieldByName('EARNED_TFT_MINUTE').AsString) -
            TestOnNull(qryAbsenceTotal.FieldByName('USED_TFT_MINUTE').AsString);
        except
          Result := 0;
        end;
      end;
{
      if tblAbsenceTotal.FindKey(
         [QueryTmp.FieldByName('EMPLOYEE_NUMBER').Value, Year]) then
      begin
        // Check for any available TFT-hours.
        // Available TFT is:
        //   LAST_YEAR_TFT_MINUTE + EARNED_TFT_MINUTE - USED_TFT_MINUTE
        try
          Result :=
            tblAbsenceTotalLAST_YEAR_TFT_MINUTE.AsInteger +
            tblAbsenceTotalEARNED_TFT_MINUTE.AsInteger -
            tblAbsenceTotalUSED_TFT_MINUTE.AsInteger;
        except
          Result := 0;
        end;
      end;
}
    end;
    // Testing
//    Log(Format('TotalTFTAvailableMinute: %s', [DecodeHrsMin(Result)]));
  end; // DetermineTotalTFTAvailableMinute
  // Determine if Holiday should be replaced by TFT, for
  // a certain employee, by looking in employee availability,
  // and comparing with the TFT-balance-minutes that are left.
  procedure HandleTFTHolidayCorrection;
  var
    {Year, Month, Day,} WeekDay: Word;
    AvailableTimeBlock: array[1..MAX_TBS] of String;
    HolidayMinute, TimeBlockNumber, TimeBlockLengthTotal: Integer;
    TFTAbsenceReasonCode: String;
    HOLAbsenceReasonCode: String;
    function UpdateEmpAvailRecord: Boolean;
    var
      SelectStr: String;
      SetPart: String;
      TBNR: Integer;
    begin
      Result := False;
      with DialogProcessAbsenceHrsDM do
      begin
        try
          SetPart := '';
          for TBNR := 1 to SystemDM.MaxTimeblocks do
          begin
            if AvailableTimeBlock[TBNR] = HOLAbsenceReasonCode then
            begin
              if SetPart = '' then
                SetPart := SetPart +
                  'SET AVAILABLE_TIMEBLOCK_' + IntToStr(TBNR) + ' = ' +
                  '''' + TFTAbsenceReasonCode + '''' + NL
              else
                SetPart := SetPart +
                  ', AVAILABLE_TIMEBLOCK_' + IntToStr(TBNR) + ' = ' +
                  '''' + TFTAbsenceReasonCode + '''' + NL;
            end;
          end;
          if SetPart <> '' then
          begin
            SetPart := SetPart +
              ', MUTATIONDATE = :MUTATIONDATE ' + NL +
              ', MUTATOR = :MUTATOR ' + NL;
            SelectStr :=
              'UPDATE EMPLOYEEAVAILABILITY ' + NL +
              SetPart +
              'WHERE PLANT_CODE = :PLANT_CODE ' + NL +
              '  AND EMPLOYEEAVAILABILITY_DATE = :EMPLOYEEAVAILABILITY_DATE ' + NL +
              '  AND SHIFT_NUMBER = :SHIFT_NUMBER ' + NL +
              '  AND EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER ';
            qryEmpAvailUpdate.SQL.Clear;
            qryEmpAvailUpdate.SQL.Add(SelectStr);
            qryEmpAvailUpdate.ParamByName('PLANT_CODE').AsString :=
              Query.FieldByName('PLANT_CODE').AsString;
            qryEmpAvailUpdate.ParamByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime :=
              Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').AsDateTime;
            qryEmpAvailUpdate.ParamByName('SHIFT_NUMBER').AsInteger :=
              Query.FieldByName('SHIFT_NUMBER').AsInteger;
            qryEmpAvailUpdate.ParamByName('EMPLOYEE_NUMBER').AsInteger :=
              Query.FieldByName('EMPLOYEE_NUMBER').AsInteger;
            qryEmpAvailUpdate.ParamByName('MUTATIONDATE').AsDateTime := Now;
            qryEmpAvailUpdate.ParamByName('MUTATOR').AsString :=
              SystemDM.CurrentProgramUser;
            qryEmpAvailUpdate.ExecSQL;
            inc(ChangeCount);
            Result := True;
          end;
        except
          on E:EdbEngineError do
          begin
            Log(E.Message, True);
          end;
          on E:Exception do
          begin
            Log(E.Message, True);
          end;
        end;
      end; // with DialogProcessAbsenceHrsDM do
    end; // UpdateEmpAvailRecord
  begin
    with DialogProcessAbsenceHrsDM do
    begin
      // Query: Employee Availability-details per Employee
      while not Query.Eof do
      begin
        AvailableTimeBlock[1] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString;
        AvailableTimeBlock[2] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString;
        AvailableTimeBlock[3] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString;
        AvailableTimeBlock[4] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString;
        AvailableTimeBlock[5] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString;
        AvailableTimeBlock[6] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString;
        AvailableTimeBlock[7] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString;
        AvailableTimeBlock[8] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString;
        AvailableTimeBlock[9] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString;
        AvailableTimeBlock[10] :=
          Query.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString;

        if TFTAvailableMinute > 0 then
        begin
          // What is the absence reason code for TFT?
          TFTAbsenceReasonCode := Query.FieldByName('TFTAR').AsString;
          // What is the absence reason code for the Vacation/Holiday?
          HOLAbsenceReasonCode := Query.FieldByName('HOLAR').AsString;
          // Get day of week - depends on 'Week Starts On'
          WeekDay := DayInWeek(SystemDM.WeekStartsOn,
            Query.FieldByName('EMPLOYEEAVAILABILITY_DATE').Value);
          // Determine time based on the available time blocks for
          // Vacation/Holiday.
          HolidayMinute := 0;
          TimeBlockLengthTotal := 0;
          for TimeBlockNumber := 1 to SystemDM.MaxTimeblocks do
          begin
            if AvailableTimeBlock[TimeBlockNumber] = HOLAbsenceReasonCode then
            begin
              DetermineTimeBlocks(0, TotalSalaryMinutesDummy, TotalMinutesDummy,
                HolidayMinute,
                TimeBlockNumber, WeekDay,
                AvailableTimeBlock[TimeBlockNumber],
                ChangeCount, AddCountDummy, False);
              TimeBlockLengthTotal := TimeBlockLengthTotal + HolidayMinute;
            end;
          end;
          HolidayMinute := TimeBlockLengthTotal;
        end;
        // Any Holiday Minutes?
        if HolidayMinute > 0 then
        begin
          if (TFTAvailableMinute - HolidayMinute) > 0 then
          begin
            // Replace Holiday by TFT in Employee Availability
            // Subtract found minutes from TFT Available minute
            if UpdateEmpAvailRecord then
              TFTAvailableMinute := TFTAvailableMinute - HolidayMinute;
{$IFDEF DEBUG}
            // Testing
            Log(Format('HOLMinute: %s TFTAvailableMinute: %s (Updated)',
              [IntMin2StringTime(HolidayMinute, True),
               IntMin2StringTime(TFTAvailableMinute, True)]));
{$ENDIF}
          end
{$IFDEF DEBUG}
          else // Testing
            Log(Format('HOLMinute: %s TFTAvailableMinute: %s (NOT updated)',
              [IntMin2StringTime(HolidayMinute, True),
               IntMin2StringTime(TFTAvailableMinute, True)]));
{$ENDIF}
        end;
        Query.Next;
      end; // while Query
    end; // with DialogProcessAbsenceHrsDM do
  end; // HandleTFTHolidayCorrection
begin
  Log(SPimsLogStartTFTHolCorrection);
  ChangeCount := 0;
  AddCountDummy := 0;
  try
    with DialogProcessAbsenceHrsDM do
    begin
      // Init Queries
      if InitQueries then // Something found?
      begin
        // QueryTmp: Only employees
        while not QueryTmp.Eof do
        begin
          // This stores the TFT available minute from the balance in memory.
          TFTAvailableMinute := 0;
          // Filter on 1 employee.
          Query.Filtered := False;
          Query.Filter := 'EMPLOYEE_NUMBER=' +
            QueryTmp.FieldByName('EMPLOYEE_NUMBER').AsString;
          Query.Filtered := True;

          // Step 2.
          // Check if there are TFT-hours available in balance-table for the employee
          // and the same year of the selection.
          // Determine TFT-hours here!
          TFTAvailableMinute := DetermineTotalTFTAvailableMinute;
          Log(Format('TFTAvailableMinute: %s',
            [IntMin2StringTime(TFTAvailableMinute, True)]));

          // Step 3.
          // Handle the TFT/Holiday correction here.
          if TFTAvailableMinute > 0 then
            HandleTFTHolidayCorrection;

          QueryTmp.Next;
        end; // while QueryTmp
        // Reset filter
        Query.Filter := '';
        Query.Filtered := False;
      end;
    end; // with DialogProcessAbsenceHrsDM do
  finally
    Log(Format(SPimsLogChangesMade, [ChangeCount]));
    Log(SPimsLogEndTFTHolCorrection);
    Result := (ChangeCount > 0);
  end;
end; // ActionTFTHolidayCorrection;

// 20012085.1
// 20012085.2 When this is called without last arguments (Integer=0) then
//            it will not do any extra action.
// Update Absence Hours Per Employee
function TDialogProcessAbsenceHrsF.UpdateAHE(
  AAbsenceReasonCode, APlantCode: String;
  ADate: TDateTime;
  AEmployeeNumber, AShiftNumber, AAbsenceMinute: Integer): Boolean;
var
  AbsenceReasonCode, AbsenceTypeCode: String;
  AbsenceReasonID: Integer;
  Year, Month, Day: Word;
  MyAbsenceType: Char;
begin
  Result := False;
  AbsenceReasonCode := NullStr;
  AbsenceTypeCode := NullStr;
  AbsenceReasonID := 0;
  with DialogProcessAbsenceHrsDM do
  begin
    // TD-22355
    qryAbsenceReason.Close;
    qryAbsenceReason.ParamByName('ABSENCEREASON_CODE').AsString :=
      AAbsenceReasonCode;
    qryAbsenceReason.Open;
    if not qryAbsenceReason.Eof then
    begin
      AbsenceReasonCode := AAbsenceReasonCode;
      AbsenceReasonID := qryAbsenceReason.FieldByName('ABSENCEREASON_ID').AsInteger;
      AbsenceTypeCode := qryAbsenceReason.FieldByName('ABSENCETYPE_CODE').AsString;
    end;
    if AbsenceReasonCode <> NullStr then
    begin
      // Now read ABSENCEHOURPEREMPLOYEE
      // TD-22355 Optimize: Use TQueries instead of TTables.
      qryAHEFind.Close;
      qryAHEFind.ParamByName('ABSENCEHOUR_DATE').AsDateTime := ADate;
      qryAHEFind.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
      qryAHEFind.ParamByName('ABSENCEREASON_ID').AsInteger := AbsenceReasonID;
      qryAHEFind.ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
      qryAHEFind.Open;
      if not qryAHEFind.Eof then
      begin
        // Existing record
        // Add minutes to Absence-minutes
        qryAHEUpdate.ParamByName('ABSENCEHOUR_DATE').AsDateTime := ADate;
        qryAHEUpdate.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        qryAHEUpdate.ParamByName('ABSENCEREASON_ID').AsInteger := AbsenceReasonID;
        qryAHEUpdate.ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
        qryAHEUpdate.ParamByName('ABSENCE_MINUTE').AsInteger :=
          qryAHEFind.FieldByName('ABSENCE_MINUTE').AsInteger + AAbsenceMinute;
        qryAHEUpdate.ParamByName('MUTATOR').AsString :=
          SystemDM.CurrentProgramUser;
        qryAHEUpdate.ExecSQL;
        Result := True;
      end
      else
      begin
        // New record
        // Add ABSENCEHOURPEREMPLOYEE-Record
        // TD-22355 Use TQuery instead of TTable.
        qryAHEInsert.ParamByName('ABSENCEHOUR_DATE').AsDateTime := ADate;
        qryAHEInsert.ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        qryAHEInsert.ParamByName('PLANT_CODE').AsString := APlantCode;
        qryAHEInsert.ParamByName('MANUAL_YN').AsString := UNCHECKEDVALUE;
        qryAHEInsert.ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
        qryAHEInsert.ParamByName('ABSENCE_MINUTE').AsInteger := AAbsenceMinute;
        qryAHEInsert.ParamByName('REMARK').AsString := '';
        qryAHEInsert.ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
        qryAHEInsert.ParamByName('ABSENCEREASON_ID').AsInteger := AbsenceReasonID;
        qryAHEInsert.ExecSQL;
        Result := True;
      end;
      DecodeDate(ADate, Year, Month, Day);
      // 20013183 Any absence hours booked on travel time must be
      //          booked also in balance in 'used travel time'.
      MyAbsenceType := AbsenceTypeCode[1];
      if MyAbsenceType = TRAVELTIME then
        MyAbsenceType := USED_TRAVELTIME;
      UpdateAbsenceTotalMode(Query,
        AEmployeeNumber, Year,
        AAbsenceMinute,
        MyAbsenceType);
    end;
  end; // with
end; // UpdateAHE

procedure TDialogProcessAbsenceHrsF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  if SystemDM.UseFinalRun then
    CmbPlusPlantTo.Value := CmbPlusPlantFrom.Value;
  DateFromCloseUp(Sender);
  inherited;
end;

// PIM-55 Return Salary Minutes
function TDialogProcessAbsenceHrsF.SalaryCheck(AEmployeeNumber: Integer;
  ASalaryDate: TDateTime): Integer;
begin
  Result := 0;
  try
    with DialogProcessAbsenceHrsDM do
    begin
      with qrySHE do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger := AEmployeeNumber;
        ParamByName('SALARY_DATE').AsDate := ASalaryDate;
        Open;
        if not Eof then
          Result := FieldByName('SALARY_MINUTE').AsInteger;
      end;
    end;
  except
    Result := 0;
  end;
end; // SalaryCheck

end.
