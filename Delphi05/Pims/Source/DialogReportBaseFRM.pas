(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser
    - Addition of department selection
    - Addition of team selection
  MRA:2-FEB-2010 RV051.1. Bugfix.
  - Added default values for employee-selections.
  MRA:4-FEB-2010 RV053.2.
  - Addition of SearchEmployee. When this is not '', then it must
    show this employee as default-employee, but only as an initial value.
  - Addition of SearchDepartment. See above.
  - Addition of SearchPlant. See above.
  MRA:20-MAY-2010. RV063.2. 889997.
  - Addition of shift selection
  SO:10-JUN-2010. RV065.10. 550480
  - PIMS Hours per employee other plant
  SO: 04-JUL-2010 RV067.2. 550489
    PIMS User rights for production reports
  MRA:19-JAN-2011 RV085.3. Bugfix.
  - Report dialogs with department-selection
    - When filtering on teams-per-users is used and a report-dialog
      with departments is opened, it shows multiple the same
      departments in department-selection.
  - Solved by adding 'distinct' to 'qryDept'.
  MRA:18-JUL-2011 RV095.1.
  - Addition All Plants-checkbox.
  - Addition second From-To-Plant-selection.
    - This is visible when 'All Plants' was CHECKED.
  - Addition All Employees-checkbox.
  - Addition of Workspot-selection
  MRA:7-FEB-2014 SO-20011800
  - Final Run System.
  - For certain dialogs and for final run system, it is only allowed
    to select 1 plant at a time and without any further selections
  - Addition of property OnePlant: When this is True, there is only a selection
    for 1 plant, the rest is not visible.
  MRA:6-JUN-2014 20015223
  - Productivity reports and grouping on workspots
  - Related to this order: Addition of Jobs in this Base-form, so it can be
    handled here with grouping-on-workspots and with from-to-workspots.
  MRA:18-JUN-2014 SO-20015220
  - Addition of Date-Time-From-To-components that can be used instead of
    Date-From-To-components. Note: The Date-From-To-components are NOT present
    in this base-form. So when using the Date-Time-From-To-components, the
    existing Date-From-To-Components must be disabled in the dialog itself.
  MRA:18-NOV-2015 PIM-23 Related to this order
  - Addition of From/To Date-fields. This can be enabled via 'UseDate := True'
  MRA:12-DEC-2016 PIM-23 Related to this order
  - TDateTimePicker must be used via SystemDMT, where it links to another
    component that shows weeks and also takes care of first day of the week.
    Because of this 'SystemDMT' has been moved from lower uses-part to
    top-uses-part.
*)
unit DialogReportBaseFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogBaseFRM, ActnList, dxBarDBNav, dxBar, StdCtrls, Buttons, ComCtrls,
  ExtCtrls, Dblup1a, DBTables, Db, SystemDMT, ReportBaseDMT, ReportBaseFRM, Dblup1b,
  dxLayout, dxCntner, dxEditor, dxExEdtr, dxExGrEd, dxExELib,
  dxDBGrid, Provider, DBClient, dxEdLib, jpeg(*, DialogBaseDMT *);

type
  TDialogReportBaseF = class(TDialogBaseF)
    tblPlant: TTable;
    QueryEmplFrom: TQuery;
    LblFromPlant: TLabel;
    LblPlant: TLabel;
    LblFromEmployee: TLabel;
    LblEmployee: TLabel;
    CmbPlusPlantFrom: TComboBoxPlus;
    LblToPlant: TLabel;
    LblToEmployee: TLabel;
    CmbPlusPlantTo: TComboBoxPlus;
    LblStarEmployeeFrom: TLabel;
    LblStarEmployeeTo: TLabel;
    tblPlantPLANT_CODE: TStringField;
    tblPlantDESCRIPTION: TStringField;
    tblPlantADDRESS: TStringField;
    tblPlantZIPCODE: TStringField;
    tblPlantCITY: TStringField;
    tblPlantSTATE: TStringField;
    tblPlantPHONE: TStringField;
    tblPlantFAX: TStringField;
    tblPlantCREATIONDATE: TDateTimeField;
    tblPlantINSCAN_MARGIN_EARLY: TIntegerField;
    tblPlantINSCAN_MARGIN_LATE: TIntegerField;
    tblPlantMUTATIONDATE: TDateTimeField;
    tblPlantMUTATOR: TStringField;
    tblPlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    tblPlantOUTSCAN_MARGIN_LATE: TIntegerField;
    DataSourceEmplFrom: TDataSource;
    dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout;
    dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit;
    DataSourceEmplTo: TDataSource;
    dxDBGridLayoutListEmployeeTo: TdxDBGridLayout;
    QueryEmplTo: TQuery;
    LblFromDepartment: TLabel;
    LblDepartment: TLabel;
    LblStarDepartmentFrom: TLabel;
    LblStarDepartmentTo: TLabel;
    LblToDepartment: TLabel;
    CmbPlusDepartmentFrom: TComboBoxPlus;
    CmbPlusDepartmentTo: TComboBoxPlus;
    CheckBoxAllDepartments: TCheckBox;
    qryDept: TQuery;
    qryDeptPLANT_CODE: TStringField;
    qryDeptDEPARTMENT_CODE: TStringField;
    qryDeptDESCRIPTION: TStringField;
    qryTeam: TQuery;
    CheckBoxAllTeams: TCheckBox;
    CmbPlusTeamTo: TComboBoxPlus;
    LblStarTeamTo: TLabel;
    LblToTeam: TLabel;
    CmbPlusTeamFrom: TComboBoxPlus;
    LblStarTeamFrom: TLabel;
    LblTeam: TLabel;
    LblFromTeam: TLabel;
    qryTeamPLANT_CODE: TStringField;
    qryTeamTEAM_CODE: TStringField;
    qryTeamDESCRIPTION: TStringField;
    QueryEmplFromPLANT_CODE: TStringField;
    QueryEmplFromEMPLOYEE_NUMBER: TIntegerField;
    QueryEmplFromVALUEDISPLAY: TStringField;
    QueryEmplFromDESCRIPTION: TStringField;
    QueryEmplFromSHORT_NAME: TStringField;
    QueryEmplFromDEPARTMENT_CODE: TStringField;
    QueryEmplFromTEAM_CODE: TStringField;
    QueryEmplFromADDRESS: TStringField;
    QueryEmplToPLANT_CODE: TStringField;
    QueryEmplToEMPLOYEE_NUMBER: TIntegerField;
    QueryEmplToVALUEDISPLAY: TStringField;
    QueryEmplToDESCRIPTION: TStringField;
    QueryEmplToSHORT_NAME: TStringField;
    QueryEmplToDEPARTMENT_CODE: TStringField;
    QueryEmplToTEAM_CODE: TStringField;
    QueryEmplToADDRESS: TStringField;
    CheckBoxIncludeNotConnectedEmp: TCheckBox;
    LblFromShift: TLabel;
    LblShift: TLabel;
    LblStartShiftFrom: TLabel;
    CmbPlusShiftFrom: TComboBoxPlus;
    LblToShift: TLabel;
    LblStarShiftTo: TLabel;
    CmbPlusShiftTo: TComboBoxPlus;
    qryShift: TQuery;
    CheckBoxAllShifts: TCheckBox;
    CheckBoxAllEmployees: TCheckBox;
    CheckBoxAllPlants: TCheckBox;
    LblStarPlantFrom: TLabel;
    LblStarPlantTo: TLabel;
    LblFromPlant2: TLabel;
    LblPlant2: TLabel;
    LblStarPlant2From: TLabel;
    CmbPlusPlant2From: TComboBoxPlus;
    LblToPlant2: TLabel;
    LblStarPlant2To: TLabel;
    CmbPlusPlant2To: TComboBoxPlus;
    tblPlant2: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField2: TDateTimeField;
    StringField9: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    LblFromWorkspot: TLabel;
    LblWorkspot: TLabel;
    LblStarWorkspotFrom: TLabel;
    CmbPlusWorkspotFrom: TComboBoxPlus;
    LblToWorkspot: TLabel;
    LblStarWorkspotTo: TLabel;
    CmbPlusWorkspotTo: TComboBoxPlus;
    qryWorkspot: TQuery;
    LblWorkspots: TLabel;
    BtnWorkspots: TSpeedButton;
    EditWorkspots: TEdit;
    qryPivotWorkspot: TQuery;
    CmbPlusJobTo: TComboBoxPlus;
    LblStarJobTo: TLabel;
    LblToJob: TLabel;
    CmbPlusJobFrom: TComboBoxPlus;
    LblStarJobFrom: TLabel;
    LblJob: TLabel;
    LblFromJob: TLabel;
    qryJob: TQuery;
    LblFromDateTime: TLabel;
    LblDateTime: TLabel;
    LblToDateTime: TLabel;
    dxTimeEditFrom: TdxTimeEdit;
    dxTimeEditTo: TdxTimeEdit;
    DatePickerFrom: TDateTimePicker;
    DatePickerTo: TDateTimePicker;
    lblFromDateBase: TLabel;
    lblDateBase: TLabel;
    lblToDateBase: TLabel;
    DatePickerFromBase: TDateTimePicker;
    DatePickerToBase: TDateTimePicker;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxDBExtLookupEditEmplFromCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure dxDBExtLookupEditEmplToCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tblPlantFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CheckBoxAllDepartmentsClick(Sender: TObject);
    procedure CmbPlusDepartmentFromCloseUp(Sender: TObject);
    procedure CmbPlusDepartmentToCloseUp(Sender: TObject);
    procedure qryDeptFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure btnOkClick(Sender: TObject);
    procedure CheckBoxAllTeamsClick(Sender: TObject);
    procedure CmbPlusTeamFromCloseUp(Sender: TObject);
    procedure CmbPlusTeamToCloseUp(Sender: TObject);
    procedure qryTeamFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure CheckBoxIncludeNotConnectedEmpClick(Sender: TObject);
    procedure CmbPlusShiftFromCloseUp(Sender: TObject);
    procedure CmbPlusShiftToCloseUp(Sender: TObject);
    procedure CheckBoxAllShiftsClick(Sender: TObject);
    procedure CheckBoxAllEmployeesClick(Sender: TObject);
    procedure CheckBoxAllPlantsClick(Sender: TObject);
    procedure CmbPlusPlant2FromCloseUp(Sender: TObject);
    procedure CmbPlusPlant2ToCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotToCloseUp(Sender: TObject);
    procedure BtnWorkspotsClick(Sender: TObject);
    procedure CmbPlusJobFromCloseUp(Sender: TObject);
    procedure CmbPlusJobToCloseUp(Sender: TObject);
    procedure DatePickerFromBaseCloseUp(Sender: TObject);
    procedure DatePickerToBaseCloseUp(Sender: TObject);
  private
    { Private declarations }
    SaveTitle: String;
    UsePlant: Boolean;
    UseDepartment: Boolean;
    UseTeam: Boolean;
    UseEmployees: Boolean;
    UseShift: Boolean;
    UseWorkspot: Boolean;
    UseJob: Boolean;
    FReportDM: TReportBaseDM;
    FReportQR: TReportBaseF;
    FException: String;
    FSearchEmployee: String;
    FSearchDepartment: String;
    FSearchPlant: String;
    FSearchTeam: String;
    FCheckBoxAllEmployeesShow: Boolean;
    FCheckBoxAllPlantsShow: Boolean;
    FUsePlant2: Boolean;
    FOnePlant: Boolean;
    FOnlyShowPlantSelection: Boolean;
    FEnableWorkspotIncludeForThisReport: Boolean;
    FUseDateTime: Boolean;
    FUseDate: Boolean;
//    FMyDateTimeTo: TDateTime;
(*    FDialogFormDM: TDialogBaseDM; *)
    procedure SetException(const Value: String);
    function GetException: String;
    procedure DepartmentVisible(Vis: Boolean);
    procedure TeamVisible(Vis: Boolean);
    procedure ShiftVisible(Vis: Boolean);
    procedure EmployeeVisible(Vis: Boolean);
    procedure PlantVisible(Vis: Boolean);
    procedure ShowPlant2(ShowYN: Boolean);
  protected
    function GetLoginUser: String;
    procedure FillAll; virtual;
  public
    { Public declarations }
    procedure ComboSet(const Mode: Integer);
    procedure FillPlants;
    procedure FillPlants2;
    procedure FillDepartments;
    procedure FillWorkspots;
    procedure FillJobs;
    procedure FillTeams;
    procedure FillEmployees;
    procedure FillShifts;
    procedure InitDialog(
      const AUsePlant: Boolean;
      const AUseDepartment: Boolean;
      const AUseTeam: Boolean;
      const AUseEmployees: Boolean;
      const AUseShift: Boolean;
      const AUseWorkspot: Boolean;
      const AUseJob: Boolean);
(*    function CreateFormDM(ADialogFormDM: TDialogBaseDM): pointer; *)
    procedure WorkspotVisible(Vis: Boolean);
    procedure JobVisible(Vis: Boolean);
    function MyPlantFrom: String;
    function MyPlantTo: String;
    property ReportDM: TReportBaseDM read FReportDM write FReportDM;
    property ReportQR: TReportBaseF read FReportQR write FReportQR;
    property ReportException: String read GetException write SetException;
    function CreateReportDM(AReportDM: TReportBaseClassDM): Pointer; Virtual;
    function CreateReportQR(AReportQR: TReportBaseClassQR):  Pointer; Virtual;
    function DetectIncludeExcludeWorkspots: String;
    function DateTimeFrom(AValue: TDateTime): TDateTime; // 20015220
    function DateTimeTo(AValue: TDateTime): TDateTime; // 20015220
    property SearchPlant: String read FSearchPlant write FSearchPlant;
    property SearchEmployee: String read FSearchEmployee
      write FSearchEmployee;
    property SearchDepartment: String read FSearchDepartment
      write FSearchDepartment;
    property SearchTeam: String read FSearchTeam write FSearchTeam;
    property CheckBoxAllEmployeesShow: Boolean read FCheckBoxAllEmployeesShow
      write FCheckBoxAllEmployeesShow;
    property CheckBoxAllPlantsShow: Boolean read FCheckBoxAllPlantsShow
      write FCheckBoxAllPlantsShow;
    property UsePlant2: Boolean read FUsePlant2 write FUsePlant2;
(*    property DialogFormDM: TDialogBaseDM read FDialogFormDM write FDialogFormDM; *)
    property OnePlant: Boolean read FOnePlant write FOnePlant; // 20011800
    property OnlyShowPlantSelection: Boolean read FOnlyShowPlantSelection
      write FOnlyShowPlantSelection; // 20011800
    property EnableWorkspotIncludeForThisReport: Boolean
      read FEnableWorkspotIncludeForThisReport
      write FEnableWorkspotIncludeForThisReport; // 20015223
    property UseDateTime: Boolean read FUseDateTime write FUseDateTime; // 20015220
    property UseDate: Boolean read FUseDate write FUseDate; // PIM-23
  end;

var
  DialogReportBaseF: TDialogReportBaseF;

implementation

{$R *.DFM}
uses ListProcsFRM, UPimsMessageRes, SideMenuUnitMaintenanceFRM,
  UPimsConst, MultipleSelectFRM;

//set an error string to a private variable of report
//every time when the property ReportException is wrote give an error message}
procedure TDialogReportBaseF.SetException(const Value: String);
begin
  FException := Value;
  if (Value <> '') then
  begin
    DisplayMessage(Value, mtError, [mbOk]);
    Screen.Cursor := crDefault;
  end;
end;

{every time when the property ReportException is read give an error message}
function TDialogReportBaseF.GetException: String;
begin
  if (FException <> '') then
  begin
    DisplayMessage(FException, mtError, [mbOk]);
    Screen.Cursor := crDefault;
  end;
  Result := FException;
end;

procedure TDialogReportBaseF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CmbPlusPlantFrom.OnCloseUp := nil;
  CmbPlusPlantTo.OnCloseUp := nil;

  CmbPlusPlantFrom.ClearGridData;
  CmbPlusPlantTo.ClearGridData;

  tblPlant.Close;
  // RV095.1.
  if UsePlant2 then
    tblPlant2.Close;
  //550284
  QueryEmplFrom.Close;
  QueryEmplTo.Close;

  if ReportDM <> nil then
  begin
    if ReportDM.Name <> 'ReportProductionDetailDM' then // 20015220 + 20015221 This must never be freed here.
    begin
      ReportDM.Free;
      ReportDM := nil;
    end;
  end;
  if ReportQR <> nil then
  begin
    ReportQR.Free;
    ReportQR := nil;
  end;
 //Car 26-3-2004
{  ShowWindow(Application.Handle, SW_RESTORE);
  ShowWindow(Application.Handle, SW_HIDE);
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);

  Application.Title := SaveTitle;  }
end;

procedure TDialogReportBaseF.FillAll;
begin
  // RV095.1.
  if CheckBoxAllPlants.Checked then
  begin
    FillDepartments;
    FillWorkspots;
    FillJobs;
  end
  else
  begin
    FillDepartments;
    FillWorkspots;
    FillJobs;
    FillTeams;
    FillEmployees;
    FillShifts;
  end;
end;

procedure TDialogReportBaseF.FormCreate(Sender: TObject);
//Car 550284
var
  TempComponent: TComponent;
  Counter: Integer;
begin
  inherited;

  SaveTitle :=  Application.Title;
  //Car 26-3-2004
{  Application.Title := SPimsReport_IconTitle;
  ShowWindow(Application.Handle, SW_RESTORE);
  ShowWindow(Application.Handle, SW_Hide);
  ShowWindow(SideMenuUnitMaintenanceF.Handle, SW_RESTORE);}

  UsePlant := True;
  UseEmployees := True;
  UseDepartment := False;
  UseTeam := False;
  UseShift := False;
  UsePlant2 := False;
  UseWorkspot := False;
  UseJob := False;

  SearchPlant := '';
  SearchEmployee := '';
  SearchDepartment := '';
  SearchTeam := '';

  CheckBoxAllPlantsShow := False;
  CheckBoxAllEmployeesShow := False;

  // 550284 - change for all combo boxes used the style
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TComboBoxPlus) then
      (TempComponent as TComboBoxPlus).Style := csIncSearch;
  end;

  // RV050.8.
  SystemDM.PlantTeamFilterEnable(tblPlant);
  tblPlant.Open;

  // RV095.1.
  if UsePlant2 then
  begin
    SystemDM.PlantTeamFilterEnable(tblPlant2);
    tblPlant2.Open;
  end;

  qryDept.ParamByName('USER_NAME').AsString := GetLoginUser;
  qryTeam.ParamByName('USER_NAME').AsString := GetLoginUser;
  if SystemDM.PlantTeamFilterOn then
  begin
    //RV067.2.
    SystemDM.PlantTeamFilterEnable(qryDept);
    qryDept.Open;

    //RV067.2.
    SystemDM.PlantTeamFilterEnable(qryTeam);
    qryTeam.Open;
  end;
  EnableWorkspotIncludeForThisReport := False; // 20015223
  UseDateTime := False; // 20015220
end;

procedure TDialogReportBaseF.FormShow(Sender: TObject);
  procedure ShowPlant(ShowYN: Boolean);
  begin
    if OnePlant then
      ShowYN := False;
    LblFromPlant.Visible := ShowYN;
    LblPlant.Visible := ShowYN;
    LblToPlant.Visible := ShowYN;
    CmbPlusPlantFrom.Visible := ShowYN;
    CmbPlusPlantTo.Visible := ShowYN;
    LblStarPlantFrom.Visible := ShowYN;
    LblStarPlantTo.Visible := ShowYN;
    if ShowYN then
      CheckBoxAllPlants.Visible := CheckBoxAllPlantsShow;
    if OnePlant then
    begin
      LblPlant.Visible := True;
      CmbPlusPlantFrom.Visible := True;
      CmbPlusPlantTo.Visible := False;
      CheckBoxAllPlants.Visible := False;
    end;
  end;
  procedure ShowEmployee(ShowYN: Boolean);
  begin
    LblFromEmployee.Visible := ShowYN;
    LblStarEmployeeFrom.Visible := ShowYN;
    LblEmployee.Visible := ShowYN;
    LblStarEmployeeTo.Visible := ShowYN;
    LblToEmployee.Visible := ShowYN;
    dxDBExtLookupEditEmplFrom.Visible := ShowYN;
    dxDBExtLookupEditEmplTo.Visible := ShowYN;
    CheckBoxAllEmployees.Visible := ShowYN;
    if ShowYN then
      CheckBoxAllEmployees.Visible := CheckBoxAllEmployeesShow;
  end;
  procedure ShowDepartment(ShowYN: Boolean);
  begin
    LblFromDepartment.Visible := ShowYN;
    LblDepartment.Visible := ShowYN;
    LblStarDepartmentFrom.Visible := ShowYN;
    LblStarDepartmentTo.Visible := ShowYN;
    LblToDepartment.Visible := ShowYN;
    CmbPlusDepartmentFrom.Visible := ShowYN;
    CmbPlusDepartmentTo.Visible := ShowYN;
    CheckBoxAllDepartments.Visible := ShowYN;
  end;
  procedure ShowTeam(ShowYN: Boolean);
  begin
    CheckBoxAllTeams.Visible := ShowYN;
    CmbPlusTeamTo.Visible := ShowYN;
    LblStarTeamTo.Visible := ShowYN;
    LblToTeam.Visible := ShowYN;
    CmbPlusTeamFrom.Visible := ShowYN;
    LblStarTeamFrom.Visible := ShowYN;
    LblTeam.Visible := ShowYN;
    LblFromTeam.Visible := ShowYN;
  end;
  procedure ShowShift(ShowYN: Boolean);
  begin
    LblFromShift.Visible := ShowYN;
    LblShift.Visible := ShowYN;
    LblStartShiftFrom.Visible := ShowYN;
    CmbPlusShiftFrom.Visible := ShowYN;
    LblToShift.Visible := ShowYN;
    LblStarShiftTo.Visible := ShowYN;
    CmbPlusShiftTo.Visible := ShowYN;
    CheckBoxAllShifts.Visible := ShowYN;
  end;
  procedure ShowWorkspotFromTo(ShowYN: Boolean); // 20015223
  begin
    LblFromWorkspot.Visible := ShowYN;
    LblWorkspot.Visible := ShowYN;
    LblStarWorkspotFrom.Visible := ShowYN;
    CmbPlusWorkspotFrom.Visible := ShowYN;
    LblToWorkspot.Visible := ShowYN;
    LblStarWorkspotTo.Visible := ShowYN;
    CmbPlusWorkspotTo.Visible := ShowYN;
  end;
  procedure ShowWorkspotInclude(ShowYN: Boolean); // 20015223
  begin
    LblWorkspots.Visible := ShowYN;
    BtnWorkspots.Visible := ShowYN;
    EditWorkspots.Visible := ShowYN;
  end;
  procedure ShowWorkspot(ShowYN: Boolean); // 20015223
  begin
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then
    begin
      ShowWorkspotInclude(ShowYN);
      ShowWorkspotFromTo(False);
    end
    else
    begin
      ShowWorkspotFromTo(ShowYN);
      ShowWorkspotInclude(False);
    end;
  end;
  procedure ShowJob(ShowYN: Boolean);
  begin
    LblFromJob.Visible := ShowYN;
    LblJob.Visible := ShowYN;
    LblStarJobFrom.Visible := ShowYN;
    CmbPlusJobFrom.Visible := ShowYN;
    LblToJob.Visible := ShowYN;
    LblStarJobTo.Visible := ShowYN;
    CmbPlusJobTo.Visible := ShowYN;
  end;
  procedure ShowDateTime(ShowYN: Boolean);
  begin
    LblFromDateTime.Visible := ShowYN;
    LblDateTime.Visible := ShowYN;
    LblToDateTime.Visible := ShowYN;
    DatePickerFrom.Visible := ShowYN;
    dxTimeEditFrom.Visible := ShowYN;
    DatePickerTo.Visible := ShowYN;
    dxTimeEditTo.Visible := ShowYN;
    if ShowYN then
    begin
      DatePickerFrom.DateTime := Date;
      DatePickerTo.DateTime := DatePickerFrom.DateTime;
      dxTimeEditFrom.Time := EncodeTime(0, 0, 0, 0);
      dxTimeEditTo.Time := EncodeTime(23, 59, 0, 0);
    end;
  end;
  procedure ShowDate(ShowYN: Boolean); // PIM-23
  begin
    lblFromDateBase.Visible := ShowYN;
    lblDateBase.Visible := ShowYN;
    lblToDateBase.Visible := ShowYN;
    DatePickerFromBase.Visible := ShowYN;
    DatePickerToBase.Visible := ShowYN;
    if ShowYN then
    begin
      DatePickerFromBase.DateTime := Date;
      DatePickerToBase.DateTime := DatePickerFromBase.DateTime;
    end;
  end;
  // MR:13-07-2004: Get rid of ugly dropdown-arrow-part
  procedure InitComboBoxPlus;
  var
    TempComponent: TComponent;
    Counter: Integer;
  begin
    for Counter := 0 to (ComponentCount - 1) do
    begin
      TempComponent := Components[Counter];
      if (TempComponent is TComboBoxPlus) then
      begin
        (TempComponent as TComboBoxPlus).ShowSpeedButton := False;
        (TempComponent as TComboBoxPlus).ShowSpeedButton := True;
      end;
    end;
  end;
begin
  inherited;
  Update;
  Screen.Cursor := crHourGlass;
  if not UsePlant then
  begin
    QueryEmplFrom.Filtered := False;
    QueryEmplTo.Filtered := False;
  end;
  if UseEmployees then
  begin
    //!!
    QueryEmplFrom.ParamByName('USER_NAME').AsString := GetLoginUser;
    QueryEmplTo.ParamByName('USER_NAME').AsString := GetLoginUser;

    QueryEmplFrom.Open;
    QueryEmplTo.Open;
  end;
  if UsePlant then
  begin
    ShowPlant(True);
    ListProcsF.FillComboBoxPlant(tblPlant, True, CmbPlusPlantFrom);
    ListProcsF.FillComboBoxPlant(tblPlant, False, CmbPlusPlantTo);
    FillPlants;
    if SearchPlant <> '' then
    begin
      // RV053.2. Use this method to point to a specific value
      //          in the ComboBoxPlus.
      if tblPlant.Locate('PLANT_CODE', SearchPlant, []) then
      begin
        CmbPlusPlantFrom.DisplayValue :=
          tblPlant.FieldByName('PLANT_CODE').AsString +
          Str_Sep + tblPlant.FieldByName('DESCRIPTION').AsString;
        CmbPlusPlantTo.DisplayValue :=
          tblPlant.FieldByName('PLANT_CODE').AsString +
          Str_Sep + tblPlant.FieldByName('DESCRIPTION').AsString;
      end;
      SearchPlant := '';
    end;
  end
  else
    ShowPlant(False);


  ShowEmployee(UseEmployees);

  if UseEmployees then
  begin
    if not UsePlant then
    begin
      QueryEmplFrom.First;
      QueryEmplTo.Last;
    end;
  end;

  ShowDepartment(UseDepartment);
  ShowTeam(UseTeam);
  ShowShift(UseShift);
  ShowWorkspot(UseWorkspot);
  ShowJob(UseJob);
  ShowDateTime(UseDateTime);
  ShowDate(UseDate); // PIM-23

  // RV095.1. Initialise as non-visible. It is handled further by Plant-events.
  ShowPlant2(False);

  FillAll;

  // MR:13-07-2004
  InitComboBoxPlus;
  Update;
  Screen.Cursor := crDefault;
end;

procedure TDialogReportBaseF.InitDialog(
  const AUsePlant: Boolean;
  const AUseDepartment: Boolean;
  const AUseTeam: Boolean;
  const AUseEmployees: Boolean;
  const AUseShift: Boolean;
  const AUseWorkspot: Boolean;
  const AUseJob: Boolean);
begin
  // 20011800
  if OnePlant then
  begin
    UsePlant := True;
    // 20011800
    if OnlyShowPlantSelection then
    begin
      UseDepartment := False;
      UseTeam := False;
      UseEmployees := False;
      UseShift := False;
      UseWorkspot := False;
      UseJob := False;
      UsePlant2 := False;
    end
    else
    begin;
      UseDepartment := AUseDepartment;
      UseTeam := AUseTeam;
      UseEmployees := AUseEmployees;
      UseShift := AUseShift;
      UseWorkspot := AUseWorkspot;
      UseJob := AUseJob;
    end;
  end
  else
  begin
    UsePlant := AUsePlant;
    UseDepartment := AUseDepartment;
    UseTeam := AUseTeam;
    UseEmployees := AUseEmployees;
    UseShift := AUseShift;
    UseWorkspot := AUseWorkspot;
    UseJob := AUseJob;
  end;
end;

procedure TDialogReportBaseF.ComboSet(const Mode: Integer);
begin
  case Mode of
    0: { Plant }
    begin
      if UsePlant then
      begin
        EmployeeVisible(False);
      end;
    end;
    1:  { Employee }
    begin
      if UseEmployees then
      begin
        EmployeeVisible(True);
      end;
    end;
  end;
end;

function TDialogReportBaseF.CreateReportDM(AReportDM: TReportBaseClassDM): Pointer;
begin
  if ReportDM = nil then
  begin
    Screen.Cursor := crHourGlass;
    try
      try
        ReportDM := AReportDM.Create(Self);
        //initialize property ReportException when the report starts
        ReportException := '';
      except
        on exception do
          ReportException := SErrorOpenRep;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  Result := ReportDM;
end;

function TDialogReportBaseF.CreateReportQR(AReportQR: TReportBaseClassQR): Pointer;
begin
  if ReportQR = nil then
  begin
    Screen.Cursor := crHourGlass;
    try
      try
        ReportQR := AReportQR.Create(Self);

        //initialize property ReportException when the report starts
        ReportException := '';
      except
        on exception do
          ReportException := SErrorCreateRep;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  Result := ReportQR;
end;

procedure TDialogReportBaseF.FormActivate(Sender: TObject);
begin
  inherited;
  ShowWindow(Application.Handle, SW_RESTORE);
  ShowWindow(Application.Handle, SW_HIDE);
end;

//
// Plants - START
//
function TDialogReportBaseF.MyPlantFrom: String;
begin
  if not CheckBoxAllPlants.Checked then
    Result := GetStrValue(CmbPlusPlantFrom.Value)
  else
    Result := GetStrValue(CmbPlusPlant2From.Value);
end;

function TDialogReportBaseF.MyPlantTo: String;
begin
  if not CheckBoxAllPlants.Checked then
    Result := GetStrValue(CmbPlusPlantTo.Value)
  else
    Result := GetStrValue(CmbPlusPlant2To.Value);
  if OnePlant then
    Result := MyPlantFrom;
end;

procedure TDialogReportBaseF.PlantVisible(Vis: Boolean);
begin
  CmbPlusPlantFrom.Visible := Vis;
  if OnePlant then
    CmbPlusPlantTo.Visible := False
  else
    CmbPlusPlantTo.Visible := Vis;
  if UsePlant2 then
  begin
    ShowPlant2(not Vis);
    if UseEmployees then
      EmployeeVisible(Vis);
  end;
end;

procedure TDialogReportBaseF.FillPlants;
begin
  if not UsePlant then
    Exit;
  if CheckBoxAllPlants.Checked then
    PlantVisible(False)
  else
  begin
    ListProcsF.FillComboBoxPlant(tblPlant, True, CmbPlusPlantFrom);
    ListProcsF.FillComboBoxPlant(tblPlant, False, CmbPlusPlantTo);
    PlantVisible(True);
    CmbPlusPlantFromCloseUp(nil);
  end;
end;

procedure TDialogReportBaseF.CmbPlusPlantFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusPlantFrom.DisplayValue <> '') and
     (CmbPlusPlantTo.DisplayValue <> '') then
    if (CmbPlusPlantFrom.Value > CmbPlusPlantTo.Value) then
      CmbPlusPlantTo.DisplayValue := CmbPlusPlantFrom.DisplayValue;
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    if UseDepartment then
      CheckBoxAllDepartments.Checked := False;
    if UseTeam then
      CheckBoxAllTeams.Checked := False;
    if UseShift then
      CheckBoxAllShifts.Checked := False;
  end;
  FillAll;
end;

procedure TDialogReportBaseF.CmbPlusPlantToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusPlantFrom.DisplayValue <> '') and
     (CmbPlusPlantTo.DisplayValue <> '') then
    if (CmbPlusPlantFrom.Value >  CmbPlusPlantTo.Value) then
      CmbPlusPlantFrom.DisplayValue := CmbPlusPlantTo.DisplayValue;
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    if UseDepartment then
      CheckBoxAllDepartments.Checked := False;
    if UseTeam then
      CheckBoxAllTeams.Checked := False;
    if UseShift then
      CheckBoxAllShifts.Checked := False;
  end;
  FillAll;
  if OnePlant then
  begin
    CmbPlusPlantTo.DisplayValue := CmbPlusPlantFrom.DisplayValue;
    CmbPlusPlantTo.Value := CmbPlusPlantFrom.Value;
  end;
end;

// RV050.8.
procedure TDialogReportBaseF.tblPlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TDialogReportBaseF.CheckBoxAllPlantsClick(Sender: TObject);
begin
  inherited;
  FillPlants;
  FillPlants2;
end;
//
// Plants - END
//

//
// Plants2 - START
//
procedure TDialogReportBaseF.ShowPlant2(ShowYN: Boolean);
begin
  LblFromPlant2.Visible := ShowYN;
  LblPlant2.Visible := ShowYN;
  LblToPlant2.Visible := ShowYN;
  CmbPlusPlant2From.Visible := ShowYN;
  CmbPlusPlant2To.Visible := ShowYN;
  LblStarPlant2From.Visible := ShowYN;
  LblStarPlant2To.Visible := ShowYN;
end;

procedure TDialogReportBaseF.FillPlants2;
begin
  if not UsePlant2 then
    Exit;
  if CheckBoxAllPlants.Checked then
  begin
    ListProcsF.FillComboBoxPlant(tblPlant2, True, CmbPlusPlant2From);
    ListProcsF.FillComboBoxPlant(tblPlant2, False, CmbPlusPlant2To);
    ShowPlant2(True);
    CmbPlusPlant2FromCloseUp(nil);
  end
  else
  begin
    ShowPlant2(False);
  end;
end;

procedure TDialogReportBaseF.CmbPlusPlant2FromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusPlant2From.DisplayValue <> '') and
     (CmbPlusPlant2To.DisplayValue <> '') then
    if (CmbPlusPlant2From.Value > CmbPlusPlant2To.Value) then
      CmbPlusPlant2To.DisplayValue := CmbPlusPlant2From.DisplayValue;
  if (CmbPlusPlant2From.Value <> '') and
     (CmbPlusPlant2From.Value = CmbPlusPlant2To.Value) then
  begin
    if UseDepartment then
      CheckBoxAllDepartments.Checked := False;
{    if UseTeam then
      CheckBoxAllTeams.Checked := False;
    if UseShift then
      CheckBoxAllShifts.Checked := False; }
  end;
  FillAll;
end;

procedure TDialogReportBaseF.CmbPlusPlant2ToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusPlant2From.DisplayValue <> '') and
     (CmbPlusPlant2To.DisplayValue <> '') then
    if (CmbPlusPlant2From.Value >  CmbPlusPlant2To.Value) then
      CmbPlusPlant2From.DisplayValue := CmbPlusPlant2To.DisplayValue;
  if (CmbPlusPlant2From.Value <> '') and
     (CmbPlusPlant2From.Value = CmbPlusPlant2To.Value) then
  begin
    if UseDepartment then
      CheckBoxAllDepartments.Checked := False;
{    if UseTeam then
      CheckBoxAllTeams.Checked := False;
    if UseShift then
      CheckBoxAllShifts.Checked := False; }
  end;
  FillAll;
end;
//
// Plants2 - END
//

//
// Employees - START
//
procedure TDialogReportBaseF.EmployeeVisible(Vis: Boolean);
begin
  dxDBExtLookupEditEmplFrom.Visible := Vis;
  dxDBExtLookupEditEmplTo.Visible := Vis;
end;

procedure TDialogReportBaseF.FillEmployees;
var
  PlantRecord: Integer;
begin
  if not UsePlant then
    Exit;
  if not UseEmployees then
    Exit;
  if CheckBoxAllEmployees.Checked then
    EmployeeVisible(False)
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      PlantRecord := TblPlant.RecordCount;
      if PlantRecord > 1 then
      begin
        if not QueryEmplFrom.Filtered then
          QueryEmplFrom.Filtered := True;

        QueryEmplFrom.Filter := 'PLANT_CODE = ''' +
          DoubleQuote(GetStrValue(CmbPlusPlantFrom.Value)) + '''';
      end;
      QueryEmplFrom.First;
      if SearchEmployee <> '' then
        QueryEmplFrom.Locate('EMPLOYEE_NUMBER', SearchEmployee, []);

      if PlantRecord > 1 then
      begin
        if not QueryEmplTo.Filtered then
          QueryEmplTo.Filtered := True;
        QueryEmplTo.Filter := 'PLANT_CODE = ''' +
          DoubleQuote(GetStrValue(CmbPlusPlantFrom.Value)) + '''';
      end;
      QueryEmplTo.Last;
      if SearchEmployee <> '' then
        QueryEmplTo.Locate('EMPLOYEE_NUMBER', SearchEmployee, []);
      ComboSet(1); { Employees }
    end
    else
      ComboSet(0); { Plants }
  end;
end;

procedure TDialogReportBaseF.dxDBExtLookupEditEmplFromCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  //550284
  if (dxDBExtLookupEditEmplFrom.Text <> '') and
     (dxDBExtLookupEditEmplTo.Text <> '') then
  begin
    if GetIntValue(dxDBExtLookupEditEmplFrom.Text) >
       GetIntValue(dxDBExtLookupEditEmplTo.Text) then
      QueryEmplTo.Locate('EMPLOYEE_NUMBER',
        GetIntValue(dxDBExtLookupEditEmplFrom.Text), [] );
  end;
  SearchEmployee := '';
end;

procedure TDialogReportBaseF.dxDBExtLookupEditEmplToCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
   //550284
  if (dxDBExtLookupEditEmplFrom.Text <> '') and
     (dxDBExtLookupEditEmplTo.Text <> '') then
  begin
    if GetIntValue(dxDBExtLookupEditEmplFrom.Text) >
      GetIntValue(dxDBExtLookupEditEmplTo.Text) then
        QueryEmplFrom.Locate('EMPLOYEE_NUMBER',
          GetIntValue(dxDBExtLookupEditEmplTo.Text), [] );
  end;
  SearchEmployee := '';
end;

procedure TDialogReportBaseF.CheckBoxAllEmployeesClick(Sender: TObject);
begin
  inherited;
  FillEmployees;
end;
//
// Employees - END
//

//
// Departments - START
//
procedure TDialogReportBaseF.DepartmentVisible(Vis: Boolean);
begin
  CmbPlusDepartmentFrom.Visible := Vis;
  CmbPlusDepartmentTo.Visible := Vis;
end;

procedure TDialogReportBaseF.FillDepartments;
begin
  if not UseDepartment then
    Exit;
  // RV095.1. Use 'MyPlantFrom' and 'MyPlantTo', because it can be based on 2
  //          different plant-selections.
  if CheckBoxAllDepartments.Checked then
    DepartmentVisible(False)
  else
  begin
    if (MyPlantFrom <> '') and
       (MyPlantFrom = MyPlantTo) then
    begin
      DepartmentVisible(True);
      ListProcsF.FillComboBoxMasterPlant(
        qryDept, CmbPlusDepartmentFrom, MyPlantFrom,
        'DEPARTMENT_CODE', True);
      ListProcsF.FillComboBoxMasterPlant(
        qryDept, CmbPlusDepartmentTo, MyPlantFrom,
        'DEPARTMENT_CODE', False);
      // RV053.2. Use this method to point to a specific value
      //          in the ComboBoxPlus.
      if SearchDepartment <> '' then
      begin
        if qryDept.Locate('DEPARTMENT_CODE', SearchDepartment, []) then
        begin
          CmbPlusDepartmentFrom.DisplayValue :=
            qryDept.FieldByName('DEPARTMENT_CODE').AsString +
            Str_Sep + qryDept.FieldByName('DESCRIPTION').AsString;
          CmbPlusDepartmentTo.DisplayValue :=
            qryDept.FieldByName('DEPARTMENT_CODE').AsString +
            Str_Sep + qryDept.FieldByName('DESCRIPTION').AsString;
        end;
      end;
    end
    else
    begin
      DepartmentVisible(False);
      CheckBoxAllDepartments.Checked := True;
    end;
  end;
end;

procedure TDialogReportBaseF.CheckBoxAllDepartmentsClick(Sender: TObject);
begin
  inherited;
  FillDepartments;
end;

procedure TDialogReportBaseF.CmbPlusDepartmentFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusDepartmentFrom.DisplayValue <> '') and
     (CmbPlusDepartmentTo.DisplayValue <> '') then
    if GetStrValue(CmbPlusDepartmentFrom.Value) >
      GetStrValue(CmbPlusDepartmentTo.Value) then
        CmbPlusDepartmentTo.DisplayValue := CmbPlusDepartmentFrom.DisplayValue;
  SearchDepartment := '';
end;

procedure TDialogReportBaseF.CmbPlusDepartmentToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusDepartmentFrom.DisplayValue <> '') and
     (CmbPlusDepartmentTo.DisplayValue <> '') then
    if GetStrValue(CmbPlusDepartmentFrom.Value) >
      GetStrValue(CmbPlusDepartmentTo.Value) then
        CmbPlusDepartmentFrom.DisplayValue := CmbPlusDepartmentTo.DisplayValue;
  SearchDepartment := '';
end;

procedure TDialogReportBaseF.qryDeptFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.qryPlantDeptTeam.Locate('PLANT_CODE;DEPARTMENT_CODE',
    VarArrayOf([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('DEPARTMENT_CODE').AsString]), []);
end;
//
// Departments - END
//

//
// Teams - BEGIN
//
procedure TDialogReportBaseF.TeamVisible(Vis: Boolean);
begin
  CmbPlusTeamFrom.Visible := Vis;
  CmbPlusTeamTo.Visible := Vis;
end;

procedure TDialogReportBaseF.FillTeams;
begin
  if not UseTeam then
    Exit;
  if CheckBoxAllTeams.Checked then
    TeamVisible(False)
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      TeamVisible(True);
      ListProcsF.FillComboBoxMasterPlant(
        qryTeam, CmbPlusTeamFrom, GetStrValue(CmbPlusPlantFrom.Value),
        'TEAM_CODE', True);
      ListProcsF.FillComboBoxMasterPlant(
        qryTeam, cmbPlusTeamTo, GetStrValue(CmbPlusPlantFrom.Value),
        'TEAM_CODE', False);
      // RV053.2. Use this method to point to a specific value
      //          in the ComboBoxPlus.
      if SearchTeam <> '' then
      begin
        if qryTeam.Locate('TEAM_CODE', SearchTeam, []) then
        begin
          CmbPlusTeamFrom.DisplayValue :=
            qryTeam.FieldByName('TEAM_CODE').AsString +
            Str_Sep + qryTeam.FieldByName('DESCRIPTION').AsString;
          CmbPlusTeamTo.DisplayValue :=
            qryTeam.FieldByName('TEAM_CODE').AsString +
            Str_Sep + qryTeam.FieldByName('DESCRIPTION').AsString;
        end;
      end;
    end
    else
    begin
      TeamVisible(False);
      CheckBoxAllTeams.Checked := True;
    end;
  end;
end;

procedure TDialogReportBaseF.CheckBoxAllTeamsClick(Sender: TObject);
begin
  inherited;
  FillTeams;
end;

procedure TDialogReportBaseF.CmbPlusTeamFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusTeamFrom.DisplayValue <> '') and
     (CmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(CmbPlusTeamFrom.Value) >
      GetStrValue(CmbPlusTeamTo.Value) then
        CmbPlusTeamTo.DisplayValue := CmbPlusTeamFrom.DisplayValue;
  SearchTeam := '';
end;

procedure TDialogReportBaseF.CmbPlusTeamToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusTeamFrom.DisplayValue <> '') and
     (CmbPlusTeamTo.DisplayValue <> '') then
    if GetStrValue(CmbPlusTeamFrom.Value) >
      GetStrValue(CmbPlusTeamTo.Value) then
        CmbPlusTeamFrom.DisplayValue := CmbPlusTeamTo.DisplayValue;
  SearchTeam := '';
end;

procedure TDialogReportBaseF.qryTeamFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := SystemDM.qryPlantDeptTeam.Locate('PLANT_CODE;TEAM_CODE',
    VarArrayOf([DataSet.FieldByName('PLANT_CODE').AsString,
      DataSet.FieldByName('TEAM_CODE').AsString]), []);
end;
//
// Teams - END
//

//
// Shifts - BEGIN
//
procedure TDialogReportBaseF.ShiftVisible(Vis: Boolean);
begin
  CmbPlusShiftFrom.Visible := Vis;
  CmbPlusShiftTo.Visible := Vis;
end;

procedure TDialogReportBaseF.FillShifts;
begin
  if not UseShift then
    Exit;
  if CheckBoxAllShifts.Checked then
    ShiftVisible(False)
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      ShiftVisible(True);
      ListProcsF.FillComboBoxMasterPlant(qryShift, CmbPlusShiftFrom,
        GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
      ListProcsF.FillComboBoxMasterPlant(qryShift, CmbPlusShiftTo,
        GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
      CmbPlusShiftFrom.IsSorted := True;
      CmbPlusShiftTo.IsSorted := True;
    end
    else
    begin
      ShiftVisible(False);
      CheckBoxAllShifts.Checked := True;
    end;
  end;
end;

procedure TDialogReportBaseF.CheckBoxAllShiftsClick(Sender: TObject);
begin
  inherited;
  FillShifts;
end;

procedure TDialogReportBaseF.CmbPlusShiftFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusShiftFrom.DisplayValue <> '') and
     (CmbPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(CmbPlusShiftFrom.Value) >
      GetIntValue(CmbPlusShiftTo.Value) then
        CmbPlusShiftTo.DisplayValue := CmbPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportBaseF.CmbPlusShiftToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusShiftFrom.DisplayValue <> '') and
     (CmbPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(CmbPlusShiftFrom.Value) >
       GetIntValue(CmbPlusShiftTo.Value) then
         CmbPlusShiftFrom.DisplayValue := CmbPlusShiftTo.DisplayValue;
end;
//
// Shifts - END
//

//
// Workspots - START
//
procedure TDialogReportBaseF.WorkspotVisible(Vis: Boolean);
begin
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then // 20015223
  begin
    CmbPlusWorkspotFrom.Visible := False;
    CmbPlusWorkspotTo.Visible := False;
  end
  else
  begin
    CmbPlusWorkspotFrom.Visible := Vis;
    CmbPlusWorkspotTo.Visible := Vis;
  end;
end;

procedure TDialogReportBaseF.FillWorkspots;
begin
  if not UseWorkspot then
    Exit;
  // RV095.1. Use 'MyPlantFrom' and 'MyPlantTo', because it can be based on 2
  //          different plant-selections.
  if (MyPlantFrom <> '') and
     (MyPlantFrom = MyPlantTo) then
  begin
    qryWorkspot.ParamByName('USER_NAME').AsString := GetLoginUser;
    ListProcsF.FillComboBoxMasterPlant(
      qryWorkspot, CmbPlusWorkspotFrom, MyPlantFrom,
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      qryWorkspot, CmbPlusWorkspotTo, MyPlantTo,
      'WORKSPOT_CODE', False);
    WorkspotVisible(True);
    DetectIncludeExcludeWorkspots; // 20015223
  end
  else
  begin
    WorkspotVisible(False);
  end;
end;

procedure TDialogReportBaseF.CmbPlusWorkspotFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusWorkSpotFrom.DisplayValue <> '') and
     (CmbPlusWorkSpotTo.DisplayValue <> '') then
  begin
    if CmbPlusWorkSpotFrom.Value > CmbPlusWorkSpotTo.Value then
      CmbPlusWorkSpotTo.DisplayValue :=
        CmbPlusWorkSpotFrom.DisplayValue;
  end;
  FillJobs;
end;

procedure TDialogReportBaseF.CmbPlusWorkspotToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusWorkSpotFrom.DisplayValue <> '') and
     (CmbPlusWorkSpotTo.DisplayValue <> '') then
  begin
    if CmbPlusWorkSpotFrom.Value >  CmbPlusWorkSpotTo.Value then
      CmbPlusWorkSpotFrom.DisplayValue :=
        CmbPlusWorkSpotTo.DisplayValue;
  end;
  FillJobs;
end;
//
// Workspots - END
//

procedure TDialogReportBaseF.btnOkClick(Sender: TObject);
begin
  inherited;
  // RV051.1. Default value for employee-selection!
  if UseEmployees then
  begin
    if (
        UsePlant and
        (CmbPlusPlantFrom.Value <> '') and
        (CmbPlusPlantFrom.Value <> CmbPlusPlantTo.Value)
      ) then
    begin
      dxDBExtLookupEditEmplFrom.Text := '0 | 0';
      dxDBExtLookupEditEmplTo.Text := '999999999 | 999999999';
    end;
  end;
  // RV095.1. Use 'MyPlantFrom' and 'MyPlantTo', because it can be based on 2
  //          different plant-selections.
  if UseDepartment then
  begin
    if CheckBoxAllDepartments.Checked or
      (
        UsePlant and
        (MyPlantFrom <> '') and
        (MyPlantFrom <> MyPlantTo)
      ) then
    begin
      CmbPlusDepartmentFrom.DisplayValue := '0 | 0';
      CmbPlusDepartmentFrom.Value := '0 | 0';
      CmbPlusDepartmentTo.DisplayValue := 'zzzzzz | zzzzz';
      CmbPlusDepartmentTo.Value := 'zzzzzz | zzzzz';
    end;
  end;
  // RV095.1. Use 'MyPlantFrom' and 'MyPlantTo', because it can be based on 2
  //          different plant-selections.
  if UseWorkspot then
  begin
    if (
        UsePlant and
        (MyPlantFrom <> '') and
        (MyPlantFrom <> MyPlantTo)
      ) then
    begin
      if not (SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport) then // 20015223
      begin
        CmbPlusWorkspotFrom.DisplayValue := '0 | 0';
        CmbPlusWorkspotFrom.Value := '0 | 0';
        CmbPlusWorkspotTo.DisplayValue := 'zzzzzz | zzzzz';
        CmbPlusWorkspotTo.Value := 'zzzzzz | zzzzz';
      end;
    end;
  end;
  if UseJob then
  begin
    if (
        UsePlant and
        (MyPlantFrom <> '') and
        (MyPlantFrom <> MyPlantTo)
      ) then
    begin
      CmbPlusJobFrom.DisplayValue := '0 | 0';
      CmbPlusJobFrom.Value := '0 | 0';
      CmbPlusJobTo.DisplayValue := 'zzzzzz | zzzzz';
      CmbPlusJobTo.Value := 'zzzzzz | zzzzz';
    end;
  end;
  if UseTeam then
  begin
    if CheckBoxAllTeams.Checked or
      (
        UsePlant and
        (CmbPlusPlantFrom.Value <> '') and
        (CmbPlusPlantFrom.Value <> CmbPlusPlantTo.Value)
      ) then
    begin
      CmbPlusTeamFrom.DisplayValue := '0 | 0';
      CmbPlusTeamFrom.Value := '0 | 0';
      CmbPlusTeamTo.DisplayValue := 'zzzzzz | zzzzz';
      CmbPlusTeamTo.Value := 'zzzzzz | zzzzz';
    end;
  end;
  if UseShift then
  begin
    if CheckBoxAllShifts.Checked or
      (
        UsePlant and
        (CmbPlusPlantFrom.Value <> '') and
        (CmbPlusPlantFrom.Value <> CmbPlusPlantTo.Value)
      ) then
    begin
      CmbPlusShiftFrom.DisplayValue := '0 | 0';
      CmbPlusShiftFrom.Value := '0 | 0';
      CmbPlusShiftTo.DisplayValue := '99 | 99';
      CmbPlusShiftTo.Value := '99 | 99';
    end;
  end;
end;

// RV065.10. 550480
procedure TDialogReportBaseF.CheckBoxIncludeNotConnectedEmpClick(
  Sender: TObject);
var
  NewEmplFromFiltered, NewEmplToFiltered: Boolean;
begin
  // RV065.10. MRA:28-JUN-2010.
  // Only switch filtering when it is really needed!
  // Otherwise the First/Last is going wrong.
  NewEmplFromFiltered := not CheckBoxIncludeNotConnectedEmp.Checked;
  NewEmplToFiltered   := not CheckBoxIncludeNotConnectedEmp.Checked;
  if QueryEmplFrom.Filtered <> NewEmplFromFiltered then
  begin
    QueryEmplFrom.Filtered := NewEmplFromFiltered;
    QueryEmplFrom.First;
  end;
  if QueryEmplTo.Filtered <> NewEmplToFiltered then
  begin
    QueryEmplTo.Filtered := NewEmplToFiltered;
    QueryEmplTo.Last;
  end;
//  QueryEmplFrom.Filtered := not CheckBoxIncludeNotConnectedEmp.Checked;
//  QueryEmplTo.Filtered   := not CheckBoxIncludeNotConnectedEmp.Checked;
end;

function TDialogReportBaseF.GetLoginUser: String;
begin
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    Result := SystemDM.CurrentLoginUser
  else
    Result := '*';
end;

(*
function TDialogReportBaseF.CreateFormDM(ADialogFormDM: TDialogBaseDM): Pointer;
begin
  if Assigned(DialogFormDM) then
  begin
    DialogFormDM.Free;
    DialogFormDM := nil;
  end;
  DialogFormDM := ADialogFormDM.Create(Self);
  Result := DialogFormDM;
end;
*)

// 20015223
// Multiple selection of workspots
procedure TDialogReportBaseF.BtnWorkspotsClick(Sender: TObject);
var
  frm: TDialogMultipleSelectF;
  PlantCode: String;
begin
  inherited;
  frm := TDialogMultipleSelectF.Create(Application);
  frm.lblCountry.Caption := SPimsPlantTitle;
  try
    PlantCode := GetStrValue(CmbPlusPlantFrom.Value);
    frm.Init(SPimsWorkspots,
      'select t.plant_code || '' | '' || t.description ' +
      'from plant t where t.plant_code = ' + '''' + PlantCode + '''',
      //Available,
      'select t.workspot_code id, t.workspot_code code, t.description ' +
      'from workspot t ' +
      'where t.plant_code = ' + '''' + PlantCode + '''' + ' and ' +
      't.workspot_code not in (select w.workspot_code ' +
      'from pivotworkspot w where w.pivotcomputername =  ' + '''' + SystemDM.CurrentComputerName + '''' + ') ' +
      'order by t.workspot_code',
      //Selected,
      'select t.workspot_code id, t.workspot_code code, t.description ' +
      'from workspot t ' +
      'where t.workspot_code in (select w.workspot_code ' +
      'from pivotworkspot w where w.pivotcomputername =  ' + '''' + SystemDM.CurrentComputerName + '''' + ') ' +
      'order by t.workspot_code',
      //Insert
      'insert into pivotworkspot(pivotcomputername, plant_code, workspot_code) values (' +
      '''' + SystemDM.CurrentComputerName + '''' + ',' +
      '''' + PlantCode + '''' + ',' + '''' + '%s' + '''' + ')',
      //Delete
      'delete from pivotworkspot w ' +
      'where w.pivotcomputername = ' + '''' + SystemDM.CurrentComputerName + '''' + ' ' +
      'and w.plant_code = ' + '''' + PlantCode + '''' + ' ' +
      'and w.workspot_code = ' + ''''  + '%s' + '''' + ' ',
      //Exists
      'select nvl(count(*), 0) from pivotworkspot w ' +
      'where w.pivotcomputername = ' +
      '''' + SystemDM.CurrentComputerName + '''' + ' ' +
      'and w.plant_code = ' + '''' + PlantCode + '''' + ' ' +
      'and w.workspot_code = ' + ''''  + '%s' + '''' + ' '
    );
    frm.ShowModal;
  finally
    DetectIncludeExcludeWorkspots; // 20015223
    FillJobs;
    frm.Free();
  end;
end; // BtnWorkspotsClick

// 20015223
// Multiple selection of workspots
function TDialogReportBaseF.DetectIncludeExcludeWorkspots: String;
var
  PlantCode: String;
begin
  Result := '';
  if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then
  begin
    Result := SPimsAllWorkspots;
    if GetStrValue(CmbPlusPlantFrom.Value) = GetStrValue(CmbPlusPlantTo.Value) then
    begin
      PlantCode := GetStrValue(CmbPlusPlantFrom.Value);
      with qryPivotWorkspot do
      begin
        Close;
        ParamByName('COMPUTERNAME').AsString := SystemDM.CurrentComputerName;
        ParamByName('PLANT_CODE').AsString := PlantCode;
        Open;
        Result := FieldByName('RESULT').AsString;
        if Result = '<MULTIPLE>' then
          Result := SPimsMultipleWorkspots
        else
          if Result = '<ALL>' then
            Result := SPimsAllWorkspots;
      end; // with
    end;
    EditWorkspots.Text := Result;
  end; // if
end; // DetectIncludeExcludeWorkspots

// 20015223
procedure TDialogReportBaseF.CmbPlusJobFromCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusJobFrom.DisplayValue <> '') and
     (CmbPlusJobTo.DisplayValue <> '') then
  begin
    if CmbPlusJobFrom.Value > CmbPlusJobTo.Value then
      CmbPlusJobTo.DisplayValue :=
        CmbPlusJobFrom.DisplayValue;
  end;
end;

// 20015223
procedure TDialogReportBaseF.CmbPlusJobToCloseUp(Sender: TObject);
begin
  inherited;
  if (CmbPlusJobFrom.DisplayValue <> '') and
     (CmbPlusJobTo.DisplayValue <> '') then
  begin
    if CmbPlusJobFrom.Value >  CmbPlusJobTo.Value then
      CmbPlusJobFrom.DisplayValue :=
        CmbPlusJobTo.DisplayValue;
  end;
end;

// 20015223
procedure TDialogReportBaseF.JobVisible(Vis: Boolean);
begin
  CmbPlusJobFrom.Visible := Vis;
  CmbPlusJobTo.Visible := Vis;
end;

// 20015223
procedure TDialogReportBaseF.FillJobs;
var
  WorkspotCode: String;
  function WorkspotCheck: Boolean;
  begin
    Result := False;
    if SystemDM.WorkspotInclSel and EnableWorkspotIncludeForThisReport then
    begin
      WorkspotCode := EditWorkspots.Text;
      if (WorkspotCode = SPimsMultipleWorkspots) or
        (WorkspotCode = SPimsAllWorkspots) then
        Result := False
      else
        if WorkspotCode <> '' then
        begin
          WorkspotCode := GetStrValue(WorkspotCode);
          Result := True;
        end;
    end
    else
    begin
      if (CmbPlusWorkspotFrom.Value <> '') and
       (CmbPlusWorkSpotFrom.Value = CmbPlusWorkSpotTo.Value) then
      begin
        Result := True;
        WorkspotCode := GetStrValue(CmbPlusWorkSpotFrom.Value);
      end;
    end;
  end;
begin
  if not UseJob then
    Exit;
  // RV095.1. Use 'MyPlantFrom' and 'MyPlantTo', because it can be based on 2
  //          different plant-selections.
  if (MyPlantFrom <> '') and
     (MyPlantFrom = MyPlantTo) and
     WorkspotCheck then
  begin
    ListProcsF.FillComboBoxJobCode(
      qryJob, CmbPlusJobFrom, MyPlantFrom,
      WorkspotCode, True);
    ListProcsF.FillComboBoxJobCode(
      qryJob, CmbPlusJobTo, MyPlantTo,
      WorkspotCode, False);
    JobVisible(True);
  end
  else
  begin
    JobVisible(False);
  end;
end;

// 20015220
function TDialogReportBaseF.DateTimeFrom(AValue: TDateTime): TDateTime;
begin
  if SystemDM.DateTimeSel and UseDateTime then
    Result := Trunc(DatePickerFrom.DateTime) + Frac(dxTimeEditFrom.Time)
  else
    Result := AValue;
end;

// 20015220
function TDialogReportBaseF.DateTimeTo(AValue: TDateTime): TDateTime;
begin
  if SystemDM.DateTimeSel and UseDateTime then
    Result := Trunc(DatePickerTo.DateTime) + Frac(dxTimeEditTo.Time)
  else
    Result := AValue;
end;

procedure TDialogReportBaseF.DatePickerFromBaseCloseUp(Sender: TObject);
begin
  inherited;
  if DatePickerFromBase.DateTime > DatePickerToBase.DateTime then
    DatePickerToBase.DateTime := DatePickerFromBase.DateTime;
end;

procedure TDialogReportBaseF.DatePickerToBaseCloseUp(Sender: TObject);
begin
  inherited;
  if DatePickerFromBase.DateTime > DatePickerToBase.DateTime then
    DatePickerToBase.DateTime := DatePickerFromBase.DateTime;
end;

end.
