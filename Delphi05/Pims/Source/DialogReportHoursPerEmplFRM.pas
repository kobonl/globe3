(*
  Changes:
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    - Disabled (made non-visible) CheckBoxAllDate.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    SO: 15-JUN-2010 RV065.10. 550480
    - PIMS Hours per employee other plant
    MRA:6-SEP-2010 RV067.MRA.29 Bugfix.
    - Include employees not connected to teams: Must also be usable for
      salary-group
    MRA:17-SEP-2010 RV067.MRA.33 550480
    - Add option to show plant info for production hours. This
      can be necessary to see employee's production made
      in other plants.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    MRA:27-AUG-2018 PIM-387
    - Report salary hours not always right
    - When not all hourtypes were selected, do not show contract-hours+diff.
    MRA:4-FEB-2019 GLOB3-207
    - Employee Hours Summary
    - Add Department selection
    MRA:27-MAY-2019 GLOB3-311
    - Report hours per employee salary gives an error
    - Related to this: It showed some components that were not used when
      using 'Embed dialogs'. 
*)
unit DialogReportHoursPerEmplFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxLayout, dxCntner, dxEditor,
  dxExEdtr, dxExGrEd, dxExELib, SystemDMT, dxEdLib, jpeg;

type
  TDialogReportHoursPerEmplF = class(TDialogReportBaseF)
    Label2: TLabel;
    Label3: TLabel;
    DateFrom: TDateTimePicker;
    DateTo: TDateTimePicker;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    CheckBoxAllDate: TCheckBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroupProduction: TRadioGroup;
    RadioGroupStatusEmpl: TRadioGroup;
    CheckBoxShowHourType: TCheckBox;
    LabelFromHT: TLabel;
    LabelHT: TLabel;
    ComboBoxPlusHourTypeFrom: TComboBoxPlus;
    LabelToHT: TLabel;
    ComboBoxPlusHourTypeTo: TComboBoxPlus;
    CheckBoxAllHourType: TCheckBox;
    TableHourType: TTable;
    LabelHTFrom: TLabel;
    LabelHTTo: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    CheckBoxAllTeam: TCheckBox;
    Label1: TLabel;
    Label7: TLabel;
    CheckBoxPageTeam: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label8: TLabel;
    CheckBoxWeek: TCheckBox;
    CheckBoxSuppressZeroes: TCheckBox;
    CheckBoxShowPlantInfo: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxAllDateClick(Sender: TObject);
    procedure RadioGroupProductionClick(Sender: TObject);
    procedure CheckBoxAllHourTypeClick(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure ComboBoxPlusHourTypeFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusHourTypeToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CheckBoxAllEmployeesClick(Sender: TObject);
    procedure CheckBoxAllTeamsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
   // FPresetEmployee: String;
    procedure AllEmployeeAction;
    procedure EnableIncludeNotConnectedEmp;
    procedure EnableShowPlantInfoProdHrs; // RV067.MRA.33 550480
  public
    { Public declarations }
 //   property  PresetEmployee: String read FPresetEmployee write FPresetEmployee;
    procedure DateVisible;
    procedure VisibleHourType(Status: Boolean);
    procedure HourTypeComboVisible(Status: Boolean);
  end;

var
  DialogReportHoursPerEmplF: TDialogReportHoursPerEmplF;

// RV089.1.
function DialogReportHoursPerEmplForm: TDialogReportHoursPerEmplF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for all TComboBoxPlus

{$R *.DFM}
uses
  ReportHoursPerEmplQRPT, ReportHoursPerEmplDMT, ListProcsFRM,
  UPimsMessageRes;

// RV089.1.
var
  DialogReportHoursPerEmplF_HND: TDialogReportHoursPerEmplF;

// RV089.1.
function DialogReportHoursPerEmplForm: TDialogReportHoursPerEmplF;
begin
  if (DialogReportHoursPerEmplF_HND = nil) then
  begin
    DialogReportHoursPerEmplF_HND := TDialogReportHoursPerEmplF.Create(Application);
    with DialogReportHoursPerEmplF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportHoursPerEmplF_HND;
end;

// RV089.1.
procedure TDialogReportHoursPerEmplF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportHoursPerEmplF_HND <> nil) then
  begin
    DialogReportHoursPerEmplF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportHoursPerEmplF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportHoursPerEmplF.btnOkClick(Sender: TObject);
var
  TmpDateFrom, TmpDateTo: TDateTime;
begin
  inherited;
  if not CheckBoxAllDate.Checked then
    if DateFrom.DateTime > DateTo.DateTime then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbYes]);
      Exit;
    end;

//CAR 12.12.2002
    TmpDateFrom := GetDate(DateFrom.Date);
    TmpDateTo :=GetDate(DateTo.Date) + 1;
// END CAR
  if ReportHoursPerEmplQR.QRSendReportParameters(
  //550284
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      TmpDateFrom,
      TmpDateTo,
      GetIntValue(ComboBoxPlusHourTypeFrom.Value),
      GetIntValue(ComboBoxPlusHourTypeTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      RadioGroupProduction.ItemIndex,
      RadioGroupStatusEmpl.ItemIndex,
      CheckBoxAllDate.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxAllHourType.Checked,
      CheckBoxShowHourType.Checked,
      CheckBoxAllTeams.Checked,
      CheckBoxIncludeNotConnectedEmp.Checked,
      //CAR 550292
      CheckBoxWeek.Checked,
      CheckBoxPageTeam.Checked,
      CheckBoxExport.Checked,
      CheckBoxAllEmployees.Checked,
      CheckBoxSuppressZeroes.Checked,
      CheckBoxIncludeNotConnectedEmp.Checked,
      CheckBoxShowPlantInfo.Checked)
  then
    ReportHoursPerEmplQR.ProcessRecords;
end;

procedure TDialogReportHoursPerEmplF.FormShow(Sender: TObject);
//550284
var
  DateNow: TDateTime;
begin
  InitDialog(True, True, True, True, False, False, False);
  CheckBoxAllEmployeesShow := True;
  inherited;
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}
  ListProcsF.FillComboBoxMaster(TableHourType, 'HOURTYPE_NUMBER', True,
     ComboBoxPlusHourTypeFrom);
  ListProcsF.FillComboBoxMaster(TableHourType, 'HOURTYPE_NUMBER', False,
     ComboBoxPlusHourTypeTo);
  // PIM-387
  ReportHoursPerEmplQR.FirstHourType :=
    GetIntValue(ComboBoxPlusHourTypeFrom.Value);
  ReportHoursPerEmplQR.LastHourType :=
    GetIntValue(ComboBoxPlusHourTypeTo.Value);
  DateVisible;
  DateNow := Now();
  DateFrom.DateTime := DateNow;
  DateTo.DateTime := DateNow;
  CheckBoxAllDate.Checked := False;
  CheckBoxShowSelection.Checked := True;
  RadioGroupProduction.ItemIndex := 0;
  RadioGroupStatusEmpl.ItemIndex := 0;
  //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusHourTypeFrom.IsSorted := True;
  ComboBoxPlusHourTypeTo.IsSorted := True;
  CmbPlusPlantFromCloseUp(Sender);
  EnableShowPlantInfoProdHrs; // RV067.MRA.33 550480
end;

procedure TDialogReportHoursPerEmplF.FormCreate(Sender: TObject);
begin
  inherited;
  //PresetEmployee := '';
//  CheckBoxAllDate.Checked := True;
  ReportHoursPerEmplDM := CreateReportDM(TReportHoursPerEmplDM);
  ReportHoursPerEmplQR := CreateReportQR(TReportHoursPerEmplQR);
//  ComboBoxPlusHourTypeFrom.Style := csDropDownList;
{  if SystemDM.PlantTeamFilterOn then
  begin
    TableTeam.Filtered := True;
    TableTeam.Open;
  end; }
end;

procedure TDialogReportHoursPerEmplF.DateVisible;
begin
  if CheckBoxAllDate.Checked then
  begin
    DateFrom.Visible := False;
    DateTo.Visible := False;
  end
  else
  begin
    DateFrom.Visible := True;
    DateTo.Visible := True;
  end;
end;

procedure TDialogReportHoursPerEmplF.CheckBoxAllDateClick(Sender: TObject);
begin
  inherited;
  DateVisible;
end;

procedure  TDialogReportHoursPerEmplF.VisibleHourType(Status: Boolean);
begin
  CheckBoxShowHourType.Visible := Status;
  CheckBoxShowHourType.Checked := Status;
  LabelFromHT.Visible := Status;
  LabelHT.Visible := Status;
  ComboBoxPlusHourTypeFrom.Visible := Status;
  LabelToHT.Visible := Status;
  ComboBoxPlusHourTypeTo.Visible := Status;
  CheckBoxAllHourType.Visible := Status;
  LabelHTTo.Visible := Status;
  LabelHTFrom.Visible := Status;
end;

procedure TDialogReportHoursPerEmplF.RadioGroupProductionClick(
  Sender: TObject);
begin
  inherited;
  if RadioGroupProduction.ItemIndex = 1 then
    VisibleHourType(True)
  else
    VisibleHourType(False);
  //RV065.10.
  EnableIncludeNotConnectedEmp;
  EnableShowPlantInfoProdHrs; // RV067.MRA.33 550480
end;

procedure TDialogReportHoursPerEmplF.HourTypeComboVisible(Status: Boolean);
begin
  if Status then
  begin
    ComboBoxPlusHourTypeFrom.Visible := False;
    ComboBoxPlusHourTypeTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusHourTypeFrom.Visible := True;
    ComboBoxPlusHourTypeTo.Visible := True;
  end;
end;

procedure TDialogReportHoursPerEmplF.CheckBoxAllHourTypeClick(
  Sender: TObject);
begin
  inherited;
  HourTypeComboVisible(CheckBoxAllHourType.Checked);
end;

procedure TDialogReportHoursPerEmplF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
{
  ComboBoxPlusTeamFrom.Visible := not CheckBoxAllTeam.Checked;
  ComboBoxPlusTeamTo.Visible := not CheckBoxAllTeam.Checked;
  CheckBoxEmpTeam.Enabled := not CheckBoxAllTeam.Checked;
  CheckBoxEmpTeam.Checked := CheckBoxAllTeam.Checked;
}  
end;

procedure TDialogReportHoursPerEmplF.ComboBoxPlusHourTypeFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusHourTypeFrom.DisplayValue <> '') and
    (ComboBoxPlusHourTypeTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusHourTypeFrom.Value) >
      GetIntValue(ComboBoxPlusHourTypeTo.Value) then
    ComboBoxPlusHourTypeTo.DisplayValue := ComboBoxPlusHourTypeFrom.DisplayValue;
end;

procedure TDialogReportHoursPerEmplF.ComboBoxPlusHourTypeToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusHourTypeFrom.DisplayValue <> '') and
     (ComboBoxPlusHourTypeTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusHourTypeFrom.Value) >
      GetIntValue(ComboBoxPlusHourTypeTo.Value) then
      ComboBoxPlusHourTypeFrom.DisplayValue := ComboBoxPlusHourTypeTo.DisplayValue;
end;

procedure TDialogReportHoursPerEmplF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  AllEmployeeAction;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

procedure TDialogReportHoursPerEmplF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  AllEmployeeAction;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

// RV050.4.
procedure TDialogReportHoursPerEmplF.AllEmployeeAction;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    CheckBoxAllEmployees.Checked := False;
  end;
  CheckBoxAllEmployeesClick(nil);
end;

// RV050.4.
procedure TDialogReportHoursPerEmplF.CheckBoxAllEmployeesClick(
  Sender: TObject);
begin
  inherited;
  if CheckBoxAllEmployees.Checked then
  begin
    dxDBExtLookupEditEmplFrom.Visible := False;
    dxDBExtLookupEditEmplTo.Visible := False;
  end
  else
  begin
    if (CmbPlusPlantFrom.Value <> '') and
       (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
    begin
      dxDBExtLookupEditEmplFrom.Visible := True;
      dxDBExtLookupEditEmplTo.Visible := True;
    end
    else
    begin
      CheckBoxAllEmployees.Checked := True;
    end;
  end;

  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

//RV065.10.
procedure TDialogReportHoursPerEmplF.EnableIncludeNotConnectedEmp;
begin
  // RV067.MRA.29 Bugfix. Must also be usable for salary-group
  CheckBoxIncludeNotConnectedEmp.Enabled :=
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
    (not CheckBoxAllEmployees.Checked) and
    CheckBoxAllTeams.Checked {and
    (RadioGroupProduction.ItemIndex = 0) };
  if not CheckBoxIncludeNotConnectedEmp.Enabled then
  begin
    CheckBoxIncludeNotConnectedEmp.Checked := False;
    CheckBoxIncludeNotConnectedEmpClick(Self);
  end;
end;

procedure TDialogReportHoursPerEmplF.CheckBoxAllTeamsClick(
  Sender: TObject);
begin
  inherited;
  //RV065.10.
  EnableIncludeNotConnectedEmp;
end;

// RV067.MRA.33 550480
procedure TDialogReportHoursPerEmplF.EnableShowPlantInfoProdHrs;
begin
  CheckBoxShowPlantInfo.Visible := (RadioGroupProduction.ItemIndex = 0);
  if RadioGroupProduction.ItemIndex <> 0 then
    CheckBoxShowPlantInfo.Checked := False;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportHoursPerEmplF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportHoursPerEmplF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
