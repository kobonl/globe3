(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:13-JAN-2011 RV084.5. Bugfix.
  - The report did not filter on plants + departments when teams-per-
    user was used, but only on plants.
  MRA:4-FEB-2011 RV085.18. SC-50016412.
  - This must be changed, so it can show scans made in other
    plants. This will be done with an additional checkbox
    named 'Include scans made in other plants'.
    When this checkbox is checked, it will not filter on
    plant+workspots but only on plant+employees. It will
    also show the plantcode-info for each scan.
    When this checkbox is not checked, it works like before.
  MRA:10-MAR-2016 ABS-24733
  - Also filter on team!
  MRA:9-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:6-SEP-2019 GLOB3-342
  - Issue scanningen dekenhal
  - When using team-filtering it goes wrong for report checklist scans.
  - This filters on plant of TR-record, which related to the plant of the
    workspot where the employee worked on, but this can be different from
    employee's plant. To solve it, filter on plant of the employee, which
    was also done for filter on department.
*)
unit ReportCheckListScanDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, UPIMSConst, UGlobalFunctions,
  CalculateTotalHoursDMT, Provider, DBClient;

type
  TReportCheckListScanDM = class(TReportBaseDM)
    QueryTimeRegScan: TQuery;
    QueryRequest: TQuery;
    ClientDataSetEmpReq: TClientDataSet;
    DataSetProviderEmpRequest: TDataSetProvider;
    QueryEmployeeAvailability: TQuery;
    QueryEmployeeAvailabilityEMPLOYEE_NUMBER: TIntegerField;
    QueryEmployeeAvailabilitySHIFT_NUMBER: TIntegerField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_1: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_2: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_3: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_4: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_5: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_6: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_7: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_8: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_9: TStringField;
    QueryEmployeeAvailabilityAVAILABLE_TIMEBLOCK_10: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryTimeRegScanFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    FListAvail: TStringList;
    function CheckEmptyRequest: Boolean;
    function CheckEmptyEmplRequest(IsEmpty: Boolean;
      Employee: Integer): Boolean;
    procedure BuildListAvail(EmplMin, EmplMax: String;
      DateFrom: TDateTime);
    function GetAvail(Employee: String): String;
  end;

var
  ReportCheckListScanDM: TReportCheckListScanDM;

implementation

uses
  SystemDMT;

{$R *.DFM}

procedure TReportCheckListScanDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // create lists
  FListAvail := TStringList.Create;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryTimeRegScan);
end;

procedure TReportCheckListScanDM.BuildListAvail( EmplMin, EmplMax: String;
  DateFrom: TDateTime);
var
  Timeblock: Integer;
  TBString: String;
begin
  FListAvail.Clear;
  // MR:28-03-2006 Use fixed query instead of each time adding the
  //               (same) query-string.
  with QueryEmployeeAvailability do
  begin
    Close;
    if EmplMin = '' then
      EmplMin := '1';
    if EmplMax = '' then
      EmplMax := '999999999';
    ParamByName('EMPLOYEEFROM').AsInteger := StrToInt(EmplMin);
    ParamByName('EMPLOYEETO').AsInteger   := StrToInt(EmplMax);
    ParamByName('DATEFROM').AsDateTime    := DateFrom;
    Open;
    if not IsEmpty then
    begin
      First;
      while not Eof do
      begin
        TBString := '';
        for Timeblock := 1 to SystemDM.MaxTimeblocks do
          TBString := TBString +
            FieldByName('AVAILABLE_TIMEBLOCK_' +
              IntToStr(Timeblock)).AsString + ' ';
        FListAvail.Add(
          ' ' +
          FieldByName('EMPLOYEE_NUMBER').AsString + CHR_SEP +
          FieldByName('SHIFT_NUMBER').AsString + ' ' +
          TBString
          );
        Next;
      end;
    end;
  end;
end;

function TReportCheckListScanDM.GetAvail(Employee: String): String;
var
  i, p: Integer;
begin
  Result := '';
  for i := 0 to FListAvail.Count - 1 do
  begin
    p := Pos(' ' +  Employee  + CHR_SEP, FListAvail.Strings[i]);
    if p > 0 then
    begin
      p := Pos(CHR_SEP,  FListAvail.Strings[i]);
      Result := Copy(FListAvail.Strings[i], P + 1, 20);
      exit;
    end;
  end;
end;   

function TReportCheckListScanDM.CheckEmptyEmplRequest(IsEmpty: Boolean;
  Employee: Integer): Boolean;
begin
  Result := IsEmpty;
  If Result then
    Exit;
  Result := not ClientDataSetEmpReq.Locate('EMPLOYEE_NUMBER',
    VarArrayOf([Employee]), []);
end;

function TReportCheckListScanDM.CheckEmptyRequest: Boolean;
begin
  Result := ClientDataSetEmpReq.IsEmpty;
end;

procedure TReportCheckListScanDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  FListAvail.Clear;
  FListAvail.Free;
end;

procedure TReportCheckListScanDM.QueryTimeRegScanFilterRecord(
  DataSet: TDataSet; var Accept: Boolean);
begin
  inherited;
  // RV050.8.
//  Accept := SystemDM.PlantTeamFilter(DataSet);
  // RV084.5. Also filter on department (instead of only plants)
  // RV085.18. Use other (EMPLOYEE) fields during filtering!
//  Accept := SystemDM.PlantDeptTeamFilter(DataSet);
  Accept := SystemDM.PlantDeptTeamFilter(DataSet, 'EPLANT_CODE',
    'EDEPARTMENT_CODE');
  // ABS-24733 Also filter on team!
  if Accept then
     Accept := SystemDM.PlantTeamTeamFilter(DataSet, 'EPLANT_CODE', 'TEAM_CODE'); // GLOB3-342
end;

end.
