(*
  MRA:22-MAR-2016 PIM-152
  - Report Ghost Counts
*)
unit ReportGhostCountsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, Db, DBTables, SystemDMT;

type
  TReportGhostCountsDM = class(TReportBaseDM)
    qryGhostCounts: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ReportGhostCountsDM: TReportGhostCountsDM;

implementation

{$R *.DFM}

end.
