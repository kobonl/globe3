unit oldMultipleSelectFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxCntner, DialogBaseFRM, dxTL;

type
  TForm1 = class(TDialogBaseF)
    dxTLPlanned: TdxTreeList;
    dxTLPlannedColumnTb: TdxTreeListColumn;
    dxTLPlannedColumnWorkspot: TdxTreeListColumn;
    dxTreeList1: TdxTreeList;
    dxTreeListColumn1: TdxTreeListColumn;
    dxTreeListColumn2: TdxTreeListColumn;
  private
    { Private declarations }
    procedure AddNode(AId, ACode, AName: String);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

{ TForm1 }

procedure TForm1.AddNode(AId, ACode, AName: String);
var
  Node: TdxTreeListNode;
begin
  Node := dxTLPlanned.Add;
  Node.Values[0] := IntToStr(TB);
  Node.Values[1] := '';
end;

end.
