(*
  Changes: MR: 1-11-2004 Order 550349
    Plan employees across plants.
  MR:23-11-2004 Component 'PopupMenuCopyPaste' has been disabled, property
    'AutoPopup' has been set to 'False'. This does not function
    correctly. Also, possibly no-one knows about this functionality.
  MR:06-01-2006 Order 550421
    When using CTRL + Right-mouse-button and entering a shiftnumber,
    the whole week will be filled with the shift. But a check must be done
    if the each day can be filled according to the Timeblock-definitions.
  MRA:22-FEB-2010. RV054.10. 889957.
  - When adding new availability for a week, then:
    - when there is no shiftschedule, it does not fill in anything.
    - when there is no standard-availability, it does not fill in anything.
  - To solve it: First step is to show an error-message about this.
  MRA:25-MAR-2010. RV057.2.
  - Gave an error-message when trying to enter an absence-reason.
  MRA:29-MAR-2010. RV058.1.
  - Use Locate instead of FindKey!
  SO:19-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
  MRA:2-SEP-2010. RV067.MRA.23
  - Show week-period for year-week-component.
  MRA:12-NOV-2010 RV079.4. 550497. Addition.
  - Employee Availability - Balances
    - The information that is shown for 'balances' should be changed.
      It must show the same information that is shown in
      Hours Per Employee-dialog.
    - GlobalDM-routines are now used for Balance-information.
  MRA:11-JUL-2011 RV094.8. Bugfix.
  - When Employee Availability Dialog is called from Employee Regs,
    it does not detect correct when an employee is not-active.
    As a result, it shows availability for the last employee!
  MRA:28-JUN-2012 20013183. Extra counter Travel Time.
  - Employee Availability
    - This must show an extra counter named Travel Time in
      balance-tab.
      This should be visible when it is defined as
      COUNTER_ACTIVE_YN = 'Y' in Absence type per country-table,
      linked to the country of the plant of the current employee.
  ROP: 23-APR-2013 TD-22429 Employee Active in Future
  - Removed "(:FCURRENTDATE >= E.STARTDATE) AND" from the WHERE clause in QueryEmpl
    This causes not yet active employees to be visible anyway.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:14-FEB-2014 20011800
  - Final Run System
  MRA:15-JUL-2015 PIM-53
  - Add Holiday functionality
  NRA:30-SEP-2015 PIM-53
  - Add Holiday functionality Rework:
    - It only worked when there was no availability before
    - We now use the existing copy-function with some changes
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
  MRA:1-MAR-2016 PIM-53.2
  - Extra check: Only when STA is set to '-' assign it to empty,
    otherwise leave it like it is, because it can be a fixed absence!
  MRA:9-MAR-2016 PIM-53.3
  - When fixed absence reasons from STA were used, then give a message
    about this.
  MRA:20-APR-2016 PIM-52.1
  - Bugfix: When there is no availability yet it goes wrong when getting
    the current year + week.
  MRA:4-MAY-2018 GLOB3-94
  - Problem when 2 users work in planning dialogs
  - Check if in edit-mode when user wants to change year and ask if changes
    should be saved first.
  - Use try-except to trap and log errors, when a record was deleted via another
    instance and user tries to edit this record, that is still shown here.
  MRA:22-JUN-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
  MRA:26-JUL-2018 GLOB3-60 Bugfix.
  - Do not change the grid-column based on number of timeblocks, but keep it
    to a maximum to prevent problems (access violation).
    This means only the details-panel is showing the correct number of
    timeblocks, and the grid shows always the maximum.
  MRA:14-SEP-2018 GLOB3-60.1 Bugfix
  - Bugfix for access violation when choosing option staff availability and
    employee availability, when this is done one or more times, it
    gives access violation and program has to be ended via task-manager.
  MRA:24-JAN-2019 GLOB3-225
  - Changes for extension 4 to 10 blocks for planning
  - When MaxTimeblocks=4 do not show extra blocks in grid
  MRA:22-MAR-2019 GLOB3-275
  - Add Holiday/Absence and message about no working days selected
  - It should not check on working days (Monday to Friday), because it is
    possible employees are available on Saturday and/or Sunday.
*)

unit EmployeeAvailabilityFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, ComCtrls, dxEdLib, dxExEdtr, dxEditor, dxExGrEd,
  dxExELib, StdCtrls, dxDBTLCl, dxGrClms, dxLayout, Mask, DBCtrls, Grids,
  DBGrids, dxDBELib, Menus, DBTables;

type
  TEmployeeAvailabilityF = class(TGridBaseF)
    GroupBoxEmployee: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    dxSpinEditYear: TdxSpinEdit;
    Action1: TAction;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    EditWorkRemaining: TEdit;
    EditWorkEarned: TEdit;
    EditWorkUsed: TEdit;
    EditWorkPlanned: TEdit;
    EditWorkAvailable: TEdit;
    GroupBox4: TGroupBox;
    EditIllness: TEdit;
    PopupMenuCopyPaste: TPopupMenu;
    CopyRecord: TMenuItem;
    PasteRecord: TMenuItem;
    PopupMenuAbsRsn: TPopupMenu;
    Label3: TLabel;
    Label16: TLabel;
    EditIllnessUntil: TEdit;
    Employee: TGroupBox;
    Label4: TLabel;
    EditStartDate: TEdit;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    LabelEmplDesc: TLabel;
    PopupMenuShift: TPopupMenu;
    PanelGrid: TPanel;
    GroupBoxDetail: TGroupBox;
    dxDBGridAvailability: TdxDBGrid;
    dxDBGridAvailabilityColumnWeek: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD111: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD11: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD12: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD13: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD14: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD15: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD16: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD17: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD18: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD19: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD110: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD211: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD21: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD22: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD23: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD24: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD25: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD26: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD27: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD28: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD29: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD210: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD311: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD31: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD32: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD33: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD34: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD35: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD36: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD37: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD38: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD39: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD310: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD411: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD41: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD42: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD43: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD44: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD45: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD46: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD47: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD48: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD49: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD410: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD511: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD51: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD52: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD53: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD54: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD55: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD56: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD57: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD58: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD59: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD510: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD611: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD61: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD62: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD63: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD64: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD65: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD66: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD67: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD68: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD69: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD610: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD711: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD71: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD72: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD73: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD74: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD75: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD76: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD77: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD78: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD79: TdxDBGridColumn;
    dxDBGridAvailabilityColumnD710: TdxDBGridColumn;
    ButtonRestoreStd: TButton;
    ButtonStaffAvailability: TButton;
    CheckBoxPlanInOtherPlants: TCheckBox;
    dxDBGridAvailabilityColumnP1: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP2: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP3: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP4: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP5: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP6: TdxDBGridColumn;
    dxDBGridAvailabilityColumnP7: TdxDBGridColumn;
    dxDBEditP15: TdxDBEdit;
    dxDBEditP25: TdxDBEdit;
    dxDBEditP35: TdxDBEdit;
    dxDBEditP45: TdxDBEdit;
    dxDBEditP55: TdxDBEdit;
    dxDBEditP65: TdxDBEdit;
    dxDBEditP75: TdxDBEdit;
    ButtonCopyFromOtherWeek: TButton;
    TabSheet4: TTabSheet;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label5: TLabel;
    EditHolidayRemaining: TEdit;
    EditHolidayUsed: TEdit;
    EditHolidayPlanned: TEdit;
    EditHolidayAvailable: TEdit;
    EditHolidayNorm: TEdit;
    GroupBox5: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    EditTimeRemaining: TEdit;
    EditTimeEarned: TEdit;
    EditTimeUsed: TEdit;
    EditTimePlanned: TEdit;
    EditTimeAvailable: TEdit;
    GroupBoxHldPrevYear: TGroupBox;
    Label55: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    EditHldPrevYearRemaining: TEdit;
    EditHldPrevYearUsed: TEdit;
    EditHldPrevYearPlanned: TEdit;
    EditHldPrevYearAvailable: TEdit;
    GroupBoxSeniorityHld: TGroupBox;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label51: TLabel;
    EditSeniorityHldUsed: TEdit;
    EditSeniorityHldPlanned: TEdit;
    EditSeniorityHldAvailable: TEdit;
    GroupBoxAddBnkHoliday: TGroupBox;
    Label65: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    EditAddBnkHolidayRemaining: TEdit;
    EditAddBnkHolidayUsed: TEdit;
    EditAddBnkHolidayPlanned: TEdit;
    EditAddBnkHolidayAvailable: TEdit;
    GroupBoxSatCredit: TGroupBox;
    Label75: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label23: TLabel;
    EditSatCreditRemaining: TEdit;
    EditSatCreditUsed: TEdit;
    EditSatCreditPlanned: TEdit;
    EditSatCreditAvailable: TEdit;
    EditSatCreditEarned: TEdit;
    GroupBoxRsvBnkHoliday: TGroupBox;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label56: TLabel;
    EditRsvBnkHolidayUsed: TEdit;
    EditRsvBnkHolidayPlanned: TEdit;
    EditRsvBnkHolidayAvailable: TEdit;
    EditNormBankHldResMinManYN: TEdit;
    GroupBoxShorterWeek: TGroupBox;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label24: TLabel;
    EditShorterWeekRemaining: TEdit;
    EditShorterWeekUsed: TEdit;
    EditShorterWeekPlanned: TEdit;
    EditShorterWeekAvailable: TEdit;
    EditSeniorityHldNorm: TEdit;
    EditRsvBnkHolidayNorm: TEdit;
    EditShorterWeekEarned: TEdit;
    GroupBoxTravelTime: TGroupBox;
    Label49: TLabel;
    Label66: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label76: TLabel;
    EditTravelTimeRemaining: TEdit;
    EditTravelTimeEarned: TEdit;
    EditTravelTimeUsed: TEdit;
    EditTravelTimePlanned: TEdit;
    EditTravelTimeAvailable: TEdit;
    ButtonAddHoliday: TButton;
    ButtonWorkSchedule: TButton;
    Panel1: TPanel;
    GroupBox6: TGroupBox;
    lblPeriod: TLabel;
    ScrollBox1: TScrollBox;
    GroupBoxDay1: TGroupBox;
    dxDBEdit11: TdxDBEdit;
    dxDBEdit12: TdxDBEdit;
    dxDBEdit13: TdxDBEdit;
    dxDBEdit14: TdxDBEdit;
    dxDBEdit111: TdxDBEdit;
    GroupBoxDay2: TGroupBox;
    dxDBEdit21: TdxDBEdit;
    dxDBEdit22: TdxDBEdit;
    dxDBEdit23: TdxDBEdit;
    dxDBEdit24: TdxDBEdit;
    dxDBEdit211: TdxDBEdit;
    GroupBoxDay3: TGroupBox;
    dxDBEdit31: TdxDBEdit;
    dxDBEdit32: TdxDBEdit;
    dxDBEdit33: TdxDBEdit;
    dxDBEdit34: TdxDBEdit;
    dxDBEdit311: TdxDBEdit;
    GroupBoxDay4: TGroupBox;
    dxDBEdit41: TdxDBEdit;
    dxDBEdit42: TdxDBEdit;
    dxDBEdit43: TdxDBEdit;
    dxDBEdit44: TdxDBEdit;
    dxDBEdit411: TdxDBEdit;
    GroupBoxDay5: TGroupBox;
    dxDBEdit51: TdxDBEdit;
    dxDBEdit52: TdxDBEdit;
    dxDBEdit53: TdxDBEdit;
    dxDBEdit54: TdxDBEdit;
    dxDBEdit511: TdxDBEdit;
    GroupBoxDay6: TGroupBox;
    dxDBEdit61: TdxDBEdit;
    dxDBEdit62: TdxDBEdit;
    dxDBEdit63: TdxDBEdit;
    dxDBEdit64: TdxDBEdit;
    dxDBEdit611: TdxDBEdit;
    GroupBoxDay7: TGroupBox;
    dxDBEdit71: TdxDBEdit;
    dxDBEdit72: TdxDBEdit;
    dxDBEdit73: TdxDBEdit;
    dxDBEdit74: TdxDBEdit;
    dxDBEdit711: TdxDBEdit;
    dxDBSpinEditWeek: TdxDBSpinEdit;
    LabelWeek: TLabel;
    dxDBEdit15: TdxDBEdit;
    dxDBEdit16: TdxDBEdit;
    dxDBEdit17: TdxDBEdit;
    dxDBEdit18: TdxDBEdit;
    dxDBEdit19: TdxDBEdit;
    dxDBEdit110: TdxDBEdit;
    dxDBEdit25: TdxDBEdit;
    dxDBEdit26: TdxDBEdit;
    dxDBEdit27: TdxDBEdit;
    dxDBEdit28: TdxDBEdit;
    dxDBEdit29: TdxDBEdit;
    dxDBEdit210: TdxDBEdit;
    dxDBEdit35: TdxDBEdit;
    dxDBEdit36: TdxDBEdit;
    dxDBEdit37: TdxDBEdit;
    dxDBEdit38: TdxDBEdit;
    dxDBEdit39: TdxDBEdit;
    dxDBEdit310: TdxDBEdit;
    dxDBEdit45: TdxDBEdit;
    dxDBEdit46: TdxDBEdit;
    dxDBEdit47: TdxDBEdit;
    dxDBEdit48: TdxDBEdit;
    dxDBEdit49: TdxDBEdit;
    dxDBEdit410: TdxDBEdit;
    dxDBEdit55: TdxDBEdit;
    dxDBEdit56: TdxDBEdit;
    dxDBEdit57: TdxDBEdit;
    dxDBEdit58: TdxDBEdit;
    dxDBEdit59: TdxDBEdit;
    dxDBEdit510: TdxDBEdit;
    dxDBEdit65: TdxDBEdit;
    dxDBEdit66: TdxDBEdit;
    dxDBEdit67: TdxDBEdit;
    dxDBEdit68: TdxDBEdit;
    dxDBEdit69: TdxDBEdit;
    dxDBEdit610: TdxDBEdit;
    dxDBEdit75: TdxDBEdit;
    dxDBEdit76: TdxDBEdit;
    dxDBEdit77: TdxDBEdit;
    dxDBEdit78: TdxDBEdit;
    dxDBEdit79: TdxDBEdit;
    dxDBEdit710: TdxDBEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxSpinEditYearChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBExtLookupEditEmployeeCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure Edit1Exit(Sender: TObject);
    procedure dxDBGridAvailabilityEnter(Sender: TObject);
    procedure dxDBEdit111Change(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxBarBDBNavPostClick(Sender: TObject);
    procedure dxDBSpinEditWeekChange(Sender: TObject);
//    procedure dxDBEdit15Exit(Sender: TObject);
    procedure dxDBGridAvailabilityExit(Sender: TObject);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure ButtonRestoreStdClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure CopyRecordClick(Sender: TObject);
    procedure PasteRecordClick(Sender: TObject);
    procedure dxDBEdit14MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dxDBGridAvailabilityBackgroundDrawEvent(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect);
    procedure dxDBGridAvailabilityCustomDrawBand(Sender: TObject;
      ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxDBGridAvailabilityCustomDrawCell(Sender: TObject;
      ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
      AColumn: TdxTreeListColumn; ASelected, AFocused,
      ANewItemRow: Boolean; var AText: String; var AColor: TColor;
      AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
    procedure dxDBGridAvailabilityCustomDrawColumnHeader(Sender: TObject;
      AColumn: TdxTreeListColumn; ACanvas: TCanvas; ARect: TRect;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
      var ADone: Boolean);
    procedure FormHide(Sender: TObject);
    procedure ButtonStaffAvailabilityClick(Sender: TObject);
    procedure dxBarButtonCustColClick(Sender: TObject);
    procedure dxDBEdit11SelectionChange(Sender: TObject);
    procedure PopupMenuAbsRsnPopup(Sender: TObject);
    procedure dxDBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxDBEdit11ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dxDBEdit111ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure dxDBEdit111MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dxDBEdit111KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ScrollBarMasterScroll(Sender: TObject;
      ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure FormCreate(Sender: TObject);
    procedure CheckBoxPlanInOtherPlantsClick(Sender: TObject);
    procedure DefaultChangeShiftNo(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButtonCopyFromOtherWeekClick(Sender: TObject);
    procedure dxDBExtLookupEditEmployeeEnter(Sender: TObject);
    procedure ButtonAddHolidayClick(Sender: TObject);
    procedure ButtonWorkScheduleClick(Sender: TObject);
  private
    { Private declarations }
    FCtrlState, FPopupOpen: Boolean;
    FVisible_Staff_Avail: Boolean;
    FEditField: TdxDBEdit;
    FEditPlantField: TdxDBEdit;
    FPlanInOtherPlants: Boolean;
    WorkTimeReductionCaption, HolidayCaption, TimeForTimeCaption,
    HolidayPreviousYearCaption, SeniorityHolidayCaption, AddBankHolidayCaption,
    SatCreditCaption, BankHolToRsvCaption, ShorterWorkingWeekCaption,
    TravelTimeCaption: String;
    FMaxTBCount: Integer;
    procedure FillShiftPopup(Plant: String);
    procedure ShiftPopupItemClick(Sender: TObject);
    procedure SetPlanInOtherPlants(const Value: Boolean);
    function FormatPlantShift(PlantCode, ShiftNumber: String): String;
    function dxDBEditPlant(Day: Integer): String;
    procedure CreateAbsReasonPopupMenu;
    procedure ShowPeriod;
    procedure AddHoliday(ADateFrom, ADateTo: TDateTime; APlantCode: String;
      AShiftNumber: Integer; AbsenceReasonCode: String);
    procedure GroupBoxResize(ADay, ATBCount: Integer);
    procedure GridColumnVisible(ADay, ATBCount: Integer);
    procedure SetMaxTBCount(const Value: Integer);
    procedure HideColumns;
  public
    { Public declarations }
    FInvalidField: Boolean;
    FColumnName: String;
    FInitEmployee: Integer;
    FInitYear, FInitWeek: Word;
    property Visible_Staff_Avail: Boolean  read FVisible_Staff_Avail
      write FVisible_Staff_Avail;
    procedure FillEditDetail;
    procedure RefreshGrid;
    procedure FillEditDetailDefault;
    procedure EnableEditDetail(CheckShift: Boolean; PlantCode, Shift:
      String; IndexDay: Integer);
    procedure FillSTAArray;
    function CheckIfValidEditFields: Boolean;
    procedure AskForChanges;
    procedure EnabledNavButtons(Active: Boolean);
    procedure SetBalances;
    procedure PopupItemClick(Sender: TObject);
    procedure GetCurrentInformation(Day: Integer; var Empl: Integer;
      var Plant: String; var DateEMP: TDateTime);
    procedure SetFormParameters(AEmployee, AYear, AWeek: Integer;
      AMyDate: TDateTime);
    procedure SetValueForAllEditFields(PlantCode, Value: String;
      DaySelected, Tag: Integer);
    property PlanInOtherPlants: Boolean read FPlanInOtherPlants
      write SetPlanInOtherPlants;
    property MaxTBCount: Integer read FMaxTBCount write SetMaxTBCount;
  end;

function EmployeeAvailabilityF(AEmployee: Integer;
  AYear, AWeek: Word; AMyDate: TDateTime = 0): TEmployeeAvailabilityF;

var
  EmployeeAvailabilityF_HND: TEmployeeAvailabilityF;
  MyDate: TDateTime;

implementation

{$R *.DFM}

uses
  SystemDMT, EmployeeAvailabilityDMT, ListProcsFRM, UGlobalFunctions,
  UPimsConst, UPimsMessageRes, SideMenuUnitMaintenanceFRM,
  DialogSelectionCopyFromOWFRM, GlobalDMT, DialogAddHolidayFRM,
  DialogProcessWorkScheduleFRM;

function EmployeeAvailabilityF(AEmployee: Integer;
  AYear, AWeek: Word; AMyDate: TDateTime = 0): TEmployeeAvailabilityF;
begin
  // RV094.8.
  if AMyDate = 0 then
    MyDate := Date
  else
    MyDate := AMyDate;
  if (EmployeeAvailabilityF_HND = nil) then
  begin
    EmployeeAvailabilityF_HND := TEmployeeAvailabilityF.Create(Application);
    EmployeeAvailabilityF_HND.SetFormParameters(AEmployee, AYear, AWeek,
      AMyDate);
    // MR:08-12-2003 Moved to 'Create'
//    EmployeeAvailabilityDM :=
//    	EmployeeAvailabilityF_HND.CreateFormDM(TEmployeeAvailabilityDM);
//    EmployeeAvailabilityF_HND.dxDBSpinEditWeek.DataField := 'WEEK_NR';
  end;
  Result := EmployeeAvailabilityF_HND;
end;

procedure TEmployeeAvailabilityF.FormCreate(Sender: TObject);
begin
  // MR: 08-12-2003
  LabelWeek.Caption := GroupBox6.Caption; // Week
  FInitYear := 0;
  FInitWeek := 0;
  EmployeeAvailabilityDM := CreateFormDM(TEmployeeAvailabilityDM);
  EmployeeAvailabilityDM.MyDate := MyDate;
  EmployeeAvailabilityDM.OpenQueryEmpl;
  dxDBSpinEditWeek.DataField := 'WEEK_NR';
  if dxDBGridAvailability.DataSource = nil then
    dxDBGridAvailability.DataSource := EmployeeAvailabilityDM.DataSourceDetail;
  inherited;
  CheckBoxPlanInOtherPlants.Checked := False;
  CheckBoxPlanInOtherPlants.Visible :=
    SystemDM.TablePIMSettingsEMPLOYEES_ACROSS_PLANTS_YN.AsString = 'Y';
  PlanInOtherPlants := False;
  EmployeeAvailabilityDM.PlanInOtherPlants := PlanInOtherPlants;
  // RV079.4.
  WorkTimeReductionCaption := GroupBox3.Caption;
  HolidayCaption := GroupBox2.Caption;
  TimeForTimeCaption := GroupBox5.Caption;
  HolidayPreviousYearCaption := GroupBoxHldPrevYear.Caption;
  SeniorityHolidayCaption := GroupBoxSeniorityHld.Caption;
  AddBankHolidayCaption := GroupBoxAddBnkHoliday.Caption;
  SatCreditCaption := GroupBoxSatCredit.Caption;
  BankHolToRsvCaption := GroupBoxRsvBnkHoliday.Caption;
  ShorterWorkingWeekCaption := GroupBoxShorterWeek.Caption;
  // 20013183
  TravelTimeCaption := GroupBoxTravelTime.Caption;
  // 20014289
  dxDBGridAvailability.BandFont.Color := clWhite;
  dxDBGridAvailability.BandFont.Style := [fsBold];
  // GLOB3-60 Leave this empty
  GroupBoxDetail.Caption := '';
  MaxTBCount := SystemDM.MaxTimeblocks; // GLOB3-60.1 Do this here, to prevent access violation
  HideColumns; // GLOB3-225
end;

function TEmployeeAvailabilityF.FormatPlantShift(PlantCode,
  ShiftNumber: String): String;
begin
  Result := Format('%s - %s', [PlantCode, ShiftNumber]);
end;

function TEmployeeAvailabilityF.dxDBEditPlant(Day: Integer): String;
var
  TempComponent: TComponent;
begin
  Result := '';
  TempComponent := FindComponent('dxDBEditP' + IntToStr(Day) + '5');
  if TempComponent <> nil then
    Result := (TempComponent as TdxDBEdit).Text;
end;

procedure TEmployeeAvailabilityF.SetFormParameters(AEmployee, AYear,
  AWeek: Integer; AMyDate: TDateTime);
begin
//CAR 550262 :17-20-2003
  FInitEmployee := AEmployee;
  if AEmployee = 0 then
    if SystemDM.ASaveTimeRecScanning.Employee <> -1 then
       FInitEmployee := SystemDM.ASaveTimeRecScanning.Employee;
  // MR:12-07-2004: Order 550318
  // RV094.8. Date 'AMydate' will be filled when called from 'Employee Regs'.
  //          Set it here, so it will show emp. avail. for the correct year+week.
  if AMyDate = 0 then
  begin
    if SystemDM.ASaveTimeRecScanning.DateScanning = EncodeDate(1900,1,1) then
       SystemDM.ASaveTimeRecScanning.DateScanning := Now;
  end
  else
  begin
    SystemDM.ASaveTimeRecScanning.DateScanning := AMyDate;
  end;
//  FInitYear := Year;
//  FInitWeek := Week;
  ListProcsF.WeekUitDat(SystemDM.ASaveTimeRecScanning.DateScanning, FInitYear,
    FInitWeek);
end;

procedure TEmployeeAvailabilityF.FormDestroy(Sender: TObject);
begin
  inherited;
  EmployeeAvailabilityF_HND := Nil;
end;

procedure TEmployeeAvailabilityF.CreateAbsReasonPopupMenu;
var
  NewItem: TMenuItem;
  SaveSql: String;
  CountryId, EmplNo: Integer;
begin
  PopupMenuAbsRsn.Items.Clear;
  if dxDBExtLookupEditEmployee.DataSource.DataSet.IsEmpty then Exit;

  EmplNo := dxDBExtLookupEditEmployee.DataSource.DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  CountryId := SystemDM.GetDBValue(
  ' select NVL(p.country_id, 0) from ' +
  ' employee e, plant p ' +
  ' where ' +
  '   e.plant_code = p.plant_code and ' +
  '   e.employee_number = ' +
  IntToStr(EmplNo), 0
  );

  NewItem := TMenuItem.Create(Self);
  NewItem.Caption := '*';
  PopupMenuAbsRsn.Items.Add(NewItem);
  NewItem.OnClick := PopupItemClick;
  NewItem := TMenuItem.Create(Self);
  NewItem.Caption := '_';
  // '--';
  PopupMenuAbsRsn.Items.Add(NewItem);
  NewItem.OnClick := PopupItemClick;
//
  //RV067.4.
  SaveSql := EmployeeAvailabilityDM.qryAbsRsn.Sql.Text;
  SystemDM.UpdateAbsenceReasonPerCountry(
   CountryId,
   EmployeeAvailabilityDM.qryAbsRsn
  );
  EmployeeAvailabilityDM.cdsAbsRsn.Close;
  EmployeeAvailabilityDM.cdsAbsRsn.Open;
  EmployeeAvailabilityDM.cdsAbsRsn.First;

  while not EmployeeAvailabilityDM.cdsAbsRsn.Eof  do
  begin
    NewItem := TMenuItem.Create(Self);
    NewItem.Caption := EmployeeAvailabilityDM.cdsAbsRsn.
      FieldByName('ABSENCEREASON_CODE').AsString +  ' ' +
      EmployeeAvailabilityDM.cdsAbsRsn.
        FieldByName('DESCRIPTION').AsString;
    PopupMenuAbsRsn.Items.Add(NewItem);
    NewItem.OnClick := PopupItemClick;
    EmployeeAvailabilityDM.cdsAbsRsn.Next;
  end;
  EmployeeAvailabilityDM.qryAbsRsn.Close;
  EmployeeAvailabilityDM.qryAbsRsn.Sql.Text := SaveSql;
end;

procedure TEmployeeAvailabilityF.FormShow(Sender: TObject);
var
  Index: Integer;
  {Month, Day, }Week: Word;
begin
  // MR:14-07-2004 Set to default year+week
  if FInitWeek = 0 then
    ListProcsF.WeekUitDat(Now, FInitYear, FInitWeek);
  if dxDBGridAvailability.DataSource = Nil then
    dxDBGridAvailability.DataSource := EmployeeAvailabilityDM.DataSourceDetail;
  //CAR 21-7-2003
//  TBLenghtClass := TTBLengthClass.Create;
  inherited;
  pnlDetail.Enabled := True;
  // car 15-9-2003 user rights - changes
  if Form_Edit_YN = ITEM_VISIBIL then
  begin
    GroupBoxDetail.Enabled := False;
    dxDBGridAvailability.PopupMenu := nil;
    ButtonRestoreStd.Enabled := False;
    ButtonCopyFromOtherWeek.Enabled := False; // 20011800 Related to this order
  end;
  ButtonStaffAvailability.Enabled := True;
  if not Visible_Staff_Avail then
    ButtonStaffAvailability.Enabled := False;
  //
  PageControl1.ActivePage := TabSheet1;
  for Index := 1 to 7 do
    dxDBGridAvailability.Bands[Index].Caption :=
      SystemDM.GetDayWDescription(Index);
  GroupBoxDay1.Caption := SystemDM.GetDayWDescription(1);
  GroupBoxDay2.Caption := SystemDM.GetDayWDescription(2);
  GroupBoxDay3.Caption := SystemDM.GetDayWDescription(3);
  GroupBoxDay4.Caption := SystemDM.GetDayWDescription(4);
  GroupBoxDay5.Caption := SystemDM.GetDayWDescription(5);
  GroupBoxDay6.Caption := SystemDM.GetDayWDescription(6);
  GroupBoxDay7.Caption := SystemDM.GetDayWDescription(7);
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
  if FInitYear = 0 then
    // MR:06-01-2004 Show first year of week!
//    DecodeDate(Now, FInitYear, Month, Day);
    ListProcsF.WeekUitDat(Now, FInitYear, Week);

  if FInitEmployee = 0 then
    EmployeeAvailabilityDM.QueryEmpl.First
  else
  begin
  // CAR 18-8-2003
    EmployeeAvailabilityDM.QueryEmpl.First;
    while not EmployeeAvailabilityDM.QueryEmpl.Eof and
      (EmployeeAvailabilityDM.QueryEmpl.
        FieldByName('EMPLOYEE_NUMBER').AsInteger <> FInitEmployee) do
     EmployeeAvailabilityDM.QueryEmpl.Next;
    // RV094.8.
    if (EmployeeAvailabilityDM.QueryEmpl.
        FieldByName('EMPLOYEE_NUMBER').AsInteger <> FInitEmployee) then
    begin
      DisplayMessage(SPimsEmployeeNotActive, mtInformation, [mbOk]);
    end;

   //EmployeeAvailabilityDM.QueryEmpl.Locate('EMPLOYEE_NUMBER',
   //   VarArrayOf([FInitEmployee]), []);

  end;
  dxDBSpinEditWeek.Enabled := False;
  dxSpinEditYear.Value := FInitYear;
 // RefreshGrid;
  PasteRecord.Enabled := False;
  EmployeeAvailabilityDM.TablePIVOT.FindKey([SystemDM.CurrentComputerName,
    FInitEmployee, FInitYear, FInitWeek]);
  // CREATE a popup menu with all absence reason inside
// add * and - chars

  CreateAbsReasonPopupMenu();
  //Car 24-10-2003
  ActiveGrid := dxDBGridAvailability;
  dxDBGridAvailability.SetFocus;
  // MR:12-07-2004: Order 550318
  if FInitEmployee = 0 then
    if not dxDBExtLookupEditEmployee.DataSource.DataSet.IsEmpty then
      FInitEmployee := dxDBExtLookupEditEmployee.DataSource.DataSet.
        FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if not EmployeeAvailabilityDM.TablePIVOT.IsEmpty then
    EmployeeAvailabilityDM.TablePIVOT.FindNearest([
      SystemDM.CurrentComputerName, FInitEmployee, FInitYear, FInitWeek]);
  FillEditDetail; // MR:For correct showing of colors in edit-fields.
end;

procedure TEmployeeAvailabilityF.PopupItemClick(Sender: TObject);
var
  PosAbs: Integer;
  Absence: String;
begin
  inherited;
  Absence := (Sender as TMenuItem).Caption;
  if Absence = '_' then
    Absence := '-';
  PosAbs := Pos('&', Absence);
  if  PosAbs > 0 then
  begin
    Absence := Copy(Absence, 0, PosAbs - 1) + Copy(Absence, PosAbs + 1, 20);
    Absence := Copy(Absence, 0, 1);
  end;
  EmployeeAvailabilityDM.TablePivot.Edit;
  EmployeeAvailabilityDM.TablePivot.FieldByName(FColumnName).AsString :=
   Absence;
end;

procedure TEmployeeAvailabilityF.FillShiftPopup(Plant: String);
var
  i: Integer;
  NewItem: TMenuItem;
begin
  for i := 0 to (PopupMenuShift.Items.Count - 1) do
    PopupMenuShift.Items.Clear;

  with EmployeeAvailabilityDM.cdsShift do
  begin
    Filtered := False;
    if not PlanInOtherPlants then // For ShiftPopup-menu!
    begin
      Filter := 'PLANT_CODE = ' + Plant;
      Filtered := True;
    end;
    First;
    while not Eof do
    begin
      NewItem := TMenuItem.Create(Self);
      if PlanInOtherPlants then
        NewItem.Caption :=
          FormatPlantShift(FieldByName('PLANT_CODE').AsString,
            IntToStr(FieldByName('SHIFT_NUMBER').AsInteger))
      else
        NewItem.Caption := IntToStr(FieldByName('SHIFT_NUMBER').AsInteger);
      PopupMenuShift.Items.Add(NewItem);
      NewItem.OnClick := ShiftPopupItemClick;
      Next;
    end;
    Filtered := False;
    NewItem := TMenuItem.Create(Self);
    NewItem.Caption := '_';
    PopupMenuShift.Items.Add(NewItem);
    NewItem.OnClick := ShiftPopupItemClick;
  end;
end;

procedure TEmployeeAvailabilityF.ShiftPopupItemClick(Sender: TObject);
var
  Absence: String;
  PlantCode: String;
  function FilterPlantShift(AField: String; var APlantCode: String): String;
  var
    I: Integer;
  begin
    Result := AField;
    APlantCode :=
      EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
    if PlanInOtherPlants then // For ShiftPopupMenu!
      if AField <> '' then
      begin
        I := Pos('-', AField);
        try
          if I > 0 then
          begin
            APlantCode := Trim(Copy(AField, 1, I - 1));
            Result := Trim(Copy(AField, I + 1, Length(AField)));
          end;
        except
          Result := '';
          APlantCode := '';
        end;
      end;
  end;
begin
  inherited;
  Absence := FilterPlantShift((Sender as TMenuItem).Caption, PlantCode);
  if Absence = '_' then
    Absence := '-';
  EmployeeAvailabilityDM.TablePivot.Edit;
//  if PlanInOtherPlants then // Always do this!
    FEditPlantField.Text := PlantCode;
  FEditField.Text := Absence;
  // Force a call to 'dxDBEdit15Change'. If 'shift' is still
  // the same, it can be from another plant.
  dxDBEdit111Change(FEditField);
  EnabledNavButtons(True);
// second parameter does not mater in case of shift selected - tag ISHIFT
  SetValueForAllEditFields(PlantCode, Absence, 11, ISHIFT);
  FPopupOpen := False;
  //car 550276 - set day changed atribute
  EmployeeAvailabilityDM.FDayChanged[FEditField.Tag div 10] := 1;
end;

procedure TEmployeeAvailabilityF.RefreshGrid;
begin
  MaxTBCount := SystemDM.MaxTimeblocks;
  if (EmployeeAvailabilityDM <> Nil) and (dxSpinEditYear.Text <> '') then
  begin
    EmployeeAvailabilityDM.FYear := Round(dxSpinEditYear.Value);
    EmployeeAvailabilityDM.FillPivotTable;
    FillEditDetail;
    LabelEmplDesc.Caption :=
      EmployeeAvailabilityDM.QueryEmpl.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TEmployeeAvailabilityF.dxSpinEditYearChange(Sender: TObject);
begin
  inherited;
  AskForChanges;
  RefreshGrid;
  if (PageControl1.ActivePageIndex = 0) then
    Exit;
  SetBalances;
  ShowPeriod;
end;

procedure TEmployeeAvailabilityF.GetCurrentInformation(Day: Integer;
  var Empl: Integer; var Plant: String; var DateEMP: TDateTime);
var
  Week, Year: Integer;
begin
  Empl :=
    EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Plant :=
    EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
  Week := Round(dxDBSpinEditWeek.Value);
  Year := Round(dxSpinEditYear.Value);
  DateEMP := ListProcsF.DateFromWeek(Year, Week, Day);
end;

function TEmployeeAvailabilityF.CheckIfValidEditFields: Boolean;
var
 IndexDay, IndexTB, Empl, Shift: Integer;
 PlantCode, TBStr: String;
 DateEMP: TDateTime;
begin
  Result := True;
  if not dxBarBDBNavPost.Enabled then
    Exit;
  if FInvalidField then
  begin
    Result := False;
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
    FInvalidField := False;
  end;
  if not Result then
  begin
    DisplayMessage(SEmplAvail, mtInformation, [mbOk]);
    Exit;
  end;
  for IndexDay := 1 to 7 do
  begin
// shift not valid
    if not ((EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] = '-') or
      ((EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] >= '0') and
      (EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] <= '99')) or
    // CAR 550276
      (EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] = '')) then
    begin
      DisplayMessage(SShiftSchedule, mtInformation, [mbOk]);
      Result := False;
    end;
    if Result and ((EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] >= '0') and
       (EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] <= '99')) then
    begin
      Shift := StrToInt(EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT]);
      GetCurrentInformation(IndexDay, Empl, PlantCode, DateEMP);
//      if PlanInOtherPlants then
        PlantCode := EmployeeAvailabilityDM.FSTAPlantArray[IndexDay];
      if not EmployeeAvailabilityDM.IsShift(PlantCode, Shift) then
      begin
        DisplayMessage(SSHSShift, mtInformation, [mbOk]);
        Result := False;
      end;
    end;
    // if day was changed - check :
    // if all valid tb are filled properly
    if Result and (EmployeeAvailabilityDM.FDayChanged[IndexDay] = 1) and
      (EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] > '0') then
    begin
//      if PlanInOtherPlants then
        PlantCode := EmployeeAvailabilityDM.FSTAPlantArray[IndexDay];
//      else
//        PlantCode :=
//          EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
      EmployeeAvailabilityDM.ValidTB(EmployeeAvailabilityDM.FEmployee,
        StrToInt(EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT]),
        PlantCode,
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('DEPARTMENT_CODE').AsString,
        TBStr);
      for IndexTB := 1 to SystemDM.MaxTimeblocks do
        if ((EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] = '') or
            (EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB]  = ' ')) and
          (Pos(IntToStr(IndexTB), TBStr) > 0) then
        begin
          DisplayMessage(Format(SInvalidAvailability,[IndexDay, IndexTB]),
            mtInformation,[mbok]);
          Result := False;
        end;
      if Result then
        for IndexTB := 1 to SystemDM.MaxTimeblocks do
          if (Pos(IntToStr(IndexTB), TBStr) > 0) and
            (EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] <> '*') and
            (EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] <> '-') and
            (not EmployeeAvailabilityDM.cdsAbsRsn.Locate('ABSENCEREASON_CODE',
              EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB], [])) then
          begin
            DisplayMessage(Format(SInvalidAvailability,[IndexDay, IndexTB]),
              mtInformation,[mbok]);
            Result := False;
          end;
    end;
  end;
  if not Result then
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
end;

// color the edit fields
procedure TEmployeeAvailabilityF.FillEditDetail;
var
  TempComponent: TComponent;
  TBCount, IndexDay, Index, IndexTB, Employee: Integer;
  PlantCode, ShiftNr, TBStr: String;
begin
  if dxSpinEditYear.Text = '' then
    Exit;
  for IndexDay := 1 to 7 do
  begin
    TBCount := 0;
    for Index := 0 to SystemDM.MaxTimeblocks do // 5
    begin
      If Index = 0 then
        IndexTB := ISHIFT
      else
        IndexTB := Index;
      TempComponent := FindComponent('dxDBEdit' + IntToStr(IndexDay) +
        IntToStr(IndexTB));
      if Assigned(TempComponent) then
      begin
        if True {PlanInOtherPlants} then // Always show different color.
          PlantCode := EmployeeAvailabilityDM.TablePIVOT.
            FieldByName('PLANT' + IntToStr(IndexDay)).AsString
        else
          PlantCode :=
            EmployeeAvailabilityDM.QueryEmpl.
              FieldByName('PLANT_CODE').AsString;
        ShiftNr := EmployeeAvailabilityDM.TablePIVOT.
          FieldByName('DAY' + IntToStr(IndexDay) + IntToStr(ISHIFT)).AsString;
        Employee :=
          EmployeeAvailabilityDM.QueryEmpl.
            FieldByName('EMPLOYEE_NUMBER').AsInteger;
        if (Index > 0) then { timeblocks }
        begin
          if (ShiftNr <> '-') and (ShiftNr <> '') then
          begin
            EmployeeAvailabilityDM.ValidTB(Employee, StrToInt(ShiftNr),
              PlantCode,
              EmployeeAvailabilityDM.QueryEmpl.
                FieldByName('DEPARTMENT_CODE').AsString,
              TBStr);
            if Pos(IntToStr(IndexTB), TBStr) > 0 then
            begin
              (TempComponent as TdxDBEdit).Enabled := True;
              (TempComponent as TdxDBEdit).Color := clRequired;
              inc(TBCount);
            end
            else
            begin
              (TempComponent as TdxDBEdit).Enabled := False;
              (TempComponent as TdxDBEdit).Color := clDisabled;
              (TempComponent as TdxDBEdit).Text := '';
            end;
          end
          else
          begin
            (TempComponent as TdxDBEdit).Enabled := False;
            (TempComponent as TdxDBEdit).Color := clDisabled;
            (TempComponent as TdxDBEdit).Text := '';
          end;
        end
        else
        begin
          if (ShiftNr <> '-') and (ShiftNr <> '') then
          begin
            if (PlantCode <> '') and (PlantCode <>
              EmployeeAvailabilityDM.QueryEmpl.
                FieldByName('PLANT_CODE').AsString) then
               (TempComponent as TdxDBEdit).Color := clOtherPlant
            else
              (TempComponent as TdxDBEdit).Color := clRequired;
          end
          else
            (TempComponent as TdxDBEdit).Color := clRequired;
        end;
      end;
    end; // for Index
    GroupBoxResize(IndexDay, TBCount);
  end; // for IndexDay
end; // FillEditDetail

procedure TEmployeeAvailabilityF.FillEditDetailDefault;
var
  PlantCmp, TempComponent: TComponent;
  TBCount, IndexDay, Index, IndexTB: Integer;
begin
  if dxSpinEditYear.Text = '' then
    Exit;
  for IndexDay := 1 to 7 do
  begin
    PlantCmp := FindComponent('dxDBEditP' + IntToStr(IndexDay) + '5');
    if PlantCmp <> nil then
      (PlantCmp as TdxDBEdit).Text :=
        EmployeeAvailabilityDM.FSTAPlantArray[IndexDay];
    TBCount := 0;
    for Index := 0 to SystemDM.MaxTimeblocks do // 5
    begin
      if Index = 0 then
        IndexTB := ISHIFT
      else
        IndexTB := Index;
      TempComponent := FindComponent('dxDBEdit' + IntToStr(IndexDay) +
        IntToStr(IndexTB));
      if Assigned(TempComponent) then
      begin
        if EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] = '!' then
          (TempComponent as TdxDBEdit).Text := ''
        else
          (TempComponent as TdxDBEdit).Text :=
            EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB];
        if (Index > 0) then { timeblocks }
          if (EmployeeAvailabilityDM.FSTAArray[IndexDay, ISHIFT] <> '') then
          begin
            if (EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB]) <> '' then
            begin
              (TempComponent as TdxDBEdit).Enabled := True;
              (TempComponent as TdxDBEdit).Color := clRequired;
              inc(TBCount);
            end
            else
            begin
              (TempComponent as TdxDBEdit).Enabled := False;
              (TempComponent as TdxDBEdit).Color := clDisabled;
              (TempComponent as TdxDBEdit).Text := '';
            end;
          end
          else
          begin
            (TempComponent as TdxDBEdit).Enabled := False;
            (TempComponent as TdxDBEdit).Color := clDisabled;
            (TempComponent as TdxDBEdit).Text := '';
          end;
      end;
    end; // for Index
    GroupBoxResize(IndexDay, TBCount);
  end; // for IndexDay
end; // FillEditDetailDefault

procedure TEmployeeAvailabilityF.EnableEditDetail(CheckShift: Boolean;
  PlantCode, Shift: String;
  IndexDay: Integer);
var
  TempComponent: TComponent;
  TBCount, Index, IndexTB: Integer;
  EnabledEdit: Boolean;
  TBStr: String;
begin
  TBCount := 0;
  for Index := 0 to SystemDM.MaxTimeblocks do // 5
  begin
    if Index = 0 then
      IndexTB := ISHIFT
    else
      IndexTB := Index;
    TempComponent := FindComponent('dxDBEdit' + IntToStr(IndexDay) +
      IntToStr(IndexTB));
    if Assigned(TempComponent) then
    begin
      if (Index > 0) then { timeblocks }
      begin
        EnabledEdit := False;
        if (Shift = '-') then
          EnabledEdit := False;
        if (Shift >= '0') and (Shift <= '99') then
        begin
          EmployeeAvailabilityDM.ValidTB(EmployeeAvailabilityDM.FEmployee,
            StrToInt(Shift),
            PlantCode, {EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString,}
            EmployeeAvailabilityDM.QueryEmpl.
              FieldByName('DEPARTMENT_CODE').AsString, TBStr);
           if (Pos(IntToStr(IndexTB), TBStr) > 0) then
             EnabledEdit := True;
         end;
        (TempComponent as TdxDBEdit).Enabled := EnabledEdit;
        if EnabledEdit then
        begin
          (TempComponent as TdxDBEdit).Color := clRequired;
          inc(TBCount);
        end
        else
        begin
          (TempComponent as TdxDBEdit).Color := clDisabled;;
          (TempComponent as TdxDBEdit).Text := '';
        end;
      end{if}
      else
      begin { shift }
        // Change color if plant is different from employee-plant.
        if True {PlanInOtherPlants} then // Do this always
        begin
          if (PlantCode <> '') and (PlantCode <>
            EmployeeAvailabilityDM.QueryEmpl.
              FieldByName('PLANT_CODE').AsString) then
            (TempComponent as TdxDBEdit).Color := clOtherPlant
          else
            (TempComponent as TdxDBEdit).Color := clRequired;
        end;
      end;{if}
    end;{if}
  end;{for}
  GroupBoxResize(IndexDay, TBCount);
end; // EnableEditDetail

procedure TEmployeeAvailabilityF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
 //CAR 21-7-2003
//  TBLenghtClass.Free;
//CAR 17-10-2003
  SystemDM.ASaveTimeRecScanning.Employee :=
    EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TEmployeeAvailabilityF.dxDBExtLookupEditEmployeeCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  CreateAbsReasonPopupMenu;
  if (not (Sender is TCheckBox)) and
    (dxDBExtLookupEditEmployee.DataSource.
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger =
      EmployeeAvailabilityDM.FEmployee) then
    Exit;
  RefreshGrid;
  // MR:12-07-2004: Order 550318
  if not EmployeeAvailabilityDM.TablePIVOT.IsEmpty then
    EmployeeAvailabilityDM.TablePIVOT.Locate(
      'PIVOTCOMPUTERNAME;EMPLOYEE_NR;YEAR_NR;WEEK_NR',
      VarArrayOf([SystemDM.CurrentComputerName,
        dxDBExtLookupEditEmployee.DataSource.DataSet.
          FieldByName('EMPLOYEE_NUMBER').AsInteger, FInitYear, FInitWeek]),
            []);
  if (PageControl1.ActivePageIndex = 0) then
    Exit;
  SetBalances;
end;

procedure TEmployeeAvailabilityF.Edit1Exit(Sender: TObject);
begin
  inherited;
  FInvalidField := False;
  if not dxBarBDBNavPost.Enabled then
     Exit;

  if ((Sender as TdxDBEdit).Text = '') then
  begin
    DisplayMessage(SPimsFieldRequired, mtInformation, [mbOk]);
    FInvalidField := True;
    Exit;
  end;
  // RV058.1. Use Locate instead of FindKey!
//  if (not EmployeeAvailabilityDM.cdsAbsRsn.
//    FindKey([(Sender as TdxDBEdit).Text]))
    if (not EmployeeAvailabilityDM.cdsAbsRsn.
      Locate('ABSENCEREASON_CODE', (Sender as TdxDBEdit).Text, []))
    and ((Sender as TdxDBEdit).Text <> '*') and
    ((Sender as TdxDBEdit).Text <> '-') then
  begin
    DisplayMessage(SEmplAvail, mtInformation, [mbOk]);
    FInvalidField := True;
  end;
end;

procedure TEmployeeAvailabilityF.SetValueForAllEditFields(PlantCode,
  Value: String; DaySelected, Tag: Integer);
var
  TempComponent: TComponent;
  IndexDay, Index, IndexTB: Integer;
  PlantCmp: TComponent;
  Empl: Integer;
  PlantDummy: String;
  DateEMP: TDateTime;
begin
  if (not FCtrlState)  or (Value = '') then
    Exit;
  if not FPopupOpen then
    Exit;
  DaySelected := DaySelected div 10;
  for IndexDay := 1 to 7 do
    for Index := 0 to SystemDM.MaxTimeblocks do
    begin
      if Index = 0 then
        IndexTB := ISHIFT
      else
        IndexTB := Index;
      TempComponent := FindComponent('dxDBEdit' + IntToStr(IndexDay) +
        IntToStr(IndexTB));
      if TempComponent <> nil then
      begin
        if (Tag = 1) and (IndexDay = DaySelected) then
        begin
          if (IndexTB <> ISHIFT) then
            if (TempComponent as TdxDBEdit).Enabled then
              (TempComponent as TdxDBEdit).Text := Value;
        end
        else
        begin
          if Tag = ISHIFT then
            if (IndexTB = ISHIFT) then
            begin
              // MR:06-01-2006 Order 550421
              // Check if there is a valid shift.
              GetCurrentInformation(IndexDay, Empl, PlantDummy, DateEMP);
              if EmployeeAvailabilityDM.ValidTBForDay(
                PlantCode,
                EmployeeAvailabilityDM.QueryEmpl.
                  FieldByName('DEPARTMENT_CODE').AsString,
                StrToInt(Value), // Shift
                Empl,
                DateEMP) then
              begin
                if (TempComponent as TdxDBEdit).Enabled then
                  (TempComponent as TdxDBEdit).Text := Value;
                if {PlanInOtherPlants and }(PlantCode <> '') then
                begin
                  PlantCmp :=
                    FindComponent('dxDBEditP' + IntToStr(IndexDay) + '5');
                  if PlantCmp <> nil then
                    (PlantCmp as TdxDBEdit).Text := PlantCode;
                end;
              end
              else
                if (TempComponent as TdxDBEdit).Enabled then
                  (TempComponent as TdxDBEdit).Text := '';
            end;
        end;
      end;
    end;
end; // SetValueForAllEditFields

procedure TEmployeeAvailabilityF.dxDBGridAvailabilityEnter(
  Sender: TObject);
begin
  inherited;
// CAR 15-09-2003 user rights
  if Form_Edit_YN = ITEM_VISIBIL then
    Exit;
  ActiveGrid := dxDBGridAvailability;
  AskForChanges;
end;

procedure TEmployeeAvailabilityF.dxDBEdit111Change(Sender: TObject);
var
  PlantCode: String;
begin
  inherited;
  PlantCode := dxDBEditPlant((Sender as TdxDBEdit).Tag div 10);
  // MR:3-1-2005 To ensure plantcode is valid.
  if PlantCode = '' then
    PlantCode :=
      EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
  EnableEditDetail(True, PlantCode, (Sender as TdxDBEdit).Text,
    (Sender as TdxDBEdit).Tag div 10);
end;

procedure TEmployeeAvailabilityF.dxBarBDBNavInsertClick(Sender: TObject);
var
  Year: Integer;
  NextWeek: Integer;
  MyResult: Integer;
begin
  // MR:3-1-2005 If user presses '+' again, without pressing
  // save button, we should save the current record.
  if (ActiveGrid  as TdxDBGrid).DataSource.
    DataSet.State in [dsEdit, dsInsert] then
    dxBarBDBNavPostClick(Sender);

  dxDBSpinEditWeek.Enabled := True;
  dxDBSpinEditWeek.SetFocus;
  dxBarBDBNavPost.Enabled := True;
  // PIM-53
  if (Sender is TdxBarDBNavButton) then
  begin
    // MR:12-07-2004: Order 550318
    NextWeek := 1;
    if PageControl1.ActivePageIndex = 0 then
    begin
      if not (ActiveGrid as TdxDBGrid).DataSource.DataSet.IsEmpty then
      begin
        (ActiveGrid as TdxDBGrid).DataSource.DataSet.Last;
        try
          Year := (ActiveGrid as TdxDBGrid).DataSource.
            DataSet.FieldByName('YEAR_NR').AsInteger;
          NextWeek := (ActiveGrid as TdxDBGrid).DataSource.
            DataSet.FieldByName('WEEK_NR').AsInteger;
          inc(NextWeek);
          if NextWeek > ListProcsF.WeeksInYear(Year) then
            dec(NextWeek);
        except
          NextWeek := 1;
        end;
      end;
    end;
  end
  else
  begin
    // PIM-53
    NextWeek := dxDBSpinEditWeek.IntValue;
  end;
  EmployeeAvailabilityDM.FWeek := NextWeek; // PIM-53
  (ActiveGrid as TdxDBGrid).DataSource.DataSet.Append;
  dxDBSpinEditWeek.Text := IntToStr(NextWeek);

  // RV054.10.
  MyResult :=
    EmployeeAvailabilityDM.FillDefaultValues(Round(dxDBSpinEditWeek.Value));
  case MyResult of
  NO_SHIFT_SCHEDULE_FOUND:
    if (Sender is TdxBarDBNavButton) then // PIM-53
      DisplayMessage(SPimsNoShiftScheduleFound, mtInformation, [mbOk]);
  NO_STANDARD_AVAIL_FOUND:
    DisplayMessage(SEmplSTARecords, mtInformation, [mbOk]);
  end;

  FillEditDetailDefault;
end;

procedure TEmployeeAvailabilityF.dxBarBDBNavPostClick(Sender: TObject);
var
  ChangePlanning, ResultOkPlanning: Boolean;
  QuestionResult: Integer;
begin
  inherited;
  // 20011800 Insert is not allowed (this is always week-basde)
  if (ActiveGrid  as TdxDBGrid).DataSource. DataSet.State in [dsInsert] then
    if not EmployeeAvailabilityDM.FinalRunCheck(
      dxSpinEditYear.IntValue, dxDBSpinEditWeek.IntValue) then
    begin
      dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
      DisplayMessage(SPimsFinalRunAddAvaililityNotAllowed, mtInformation, [mbOK]);
      Exit;
    end;
  ChangePlanning:= False;
  ResultOkPlanning := True;
  FillSTAArray;
  EmployeeAvailabilityDM.GetOldEMAArray(
    EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger,
    Round(dxDBSpinEditWeek.Value),
    Round(dxSpinEditYear.Value));
// extra check
  if CheckIfValidEditFields then
  begin
    if EmployeeAvailabilityDM.CheckEmplAlreadyPlanned(False) then
    begin
      QuestionResult := DisplayMessage(SEmployeePlanned, mtConfirmation,
        [mbYes, mbNo]);
      if QuestionResult = mrYes then
      begin
        ResultOkPlanning := True;
        ChangePlanning := True;
      end
      else
        ResultOkPlanning := False;
    end;

    if (ResultOkPlanning) then
    begin
      EmployeeAvailabilityDM.TablePivot.Post;
      if not (Sender is TButton) then // PIM-53
        EmployeeAvailabilityDM.UpdateEMAPivot(Round(dxSpinEditYear.Value),
          Round(dxDBSpinEditWeek.Value), True, False);
      dxDBSpinEditWeek.Enabled := False;
      EmployeeAvailabilityDM.UpdateSHS_EMA;
      if ChangePlanning then
      //Car 4-6-2004
        EmployeeAvailabilityDM.UpdateEMP(False);
      EmployeeAvailabilityDM.FillEMAArray;
    end;
    FInvalidField := False;
  end;
  ButtonStaffAvailability.Enabled := True;
  ButtonRestoreStd.Enabled := True;
  //car 550276 - initialize array for saving days changed
  EmployeeAvailabilityDM.InitializeDayChangedArray;
end;

procedure TEmployeeAvailabilityF.FillSTAArray;
var
  TempComponent: TComponent;
  IndexDay, Index, IndexTB: Integer;
begin
  for IndexDay := 1 to 7 do
  begin
    for Index := 0 to SystemDM.MaxTimeblocks do // 5
    begin
      if Index = 0 then
        IndexTB := ISHIFT
      else
        IndexTB := Index;
      TempComponent := FindComponent('dxDBEdit' + IntToStr(IndexDay) +
        IntToStr(IndexTB));
      if Assigned(TempComponent) then
      begin
        if (TempComponent as TdxDBEdit).Enabled then
          EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] :=
            (TempComponent as TdxDBEdit).Text
        else
          EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] := '';
        if (IndexTB = ISHIFT) and ((TempComponent as TdxDBEdit).Text = '-') then
          EmployeeAvailabilityDM.FSTAArray[IndexDay, IndexTB] := '-';
      end;
    end;
      EmployeeAvailabilityDM.FSTAPlantArray[IndexDay] :=
        dxDBEditPlant(IndexDay)
  end;
end;

procedure TEmployeeAvailabilityF.dxDBSpinEditWeekChange(Sender: TObject);
begin
  inherited;
  if dxDBSpinEditWeek.Enabled then
  begin
   if (Round(dxDBSpinEditWeek.Value) >
     ListProcsF.WeeksInYear(Round(dxSpinEditYear.Value))) or
     (Round(dxDBSpinEditWeek.Value) = 0) then
    begin
      DisplayMessage(SPimsInvalidWeekNr, mtInformation, [mbOk]);
      Exit;
    end;
    // 20011800 Final Run Check
    if EmployeeAvailabilityDM.FinalRunCheck(dxSpinEditYear.IntValue,
      dxDBGridAvailabilityColumnWeek.Field.AsInteger) then
    begin
      if dxDBSpinEditWeek.Text <> '' then
      begin
       EmployeeAvailabilityDM.FillDefaultValues(Round(dxDBSpinEditWeek.Value));

       FillEditDetailDefault;
       if EmployeeAvailabilityDM.TablePivot.State in [dsEdit, dsInsert] then
         EmployeeAvailabilityDM.TablePivot.FieldByName('WEEK_NR').AsInteger :=
           Round(dxDBSpinEditWeek.Value);
      end;
    end;
  end;
  ShowPeriod;
end;
(*
procedure TEmployeeAvailabilityF.dxDBEdit15Exit(Sender: TObject);
var
  IndexDay, Shift, Empl: Integer;
  Plant: String;
  DateEMP: TDateTime;
  PlantCmp: TComponent;
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;
  if (((Sender as TdxDBEdit).Text < '0') or
    ((Sender as TdxDBEdit).Text > '99')) and
    ((Sender as TdxDBEdit).Text <> '-') and
    ((Sender as TdxDBEdit).Text <> '') then
  begin
    DisplayMessage(SShiftSchedule, mtInformation, [mbOk]);
  end;
  if ((Sender as TdxDBEdit).Text >= '1') and
    ((Sender as TdxDBEdit).Text <= '99') then
  begin
    IndexDay := (Sender as TdxDBEdit).Tag div 10;
    Shift := StrToInt((Sender as TdxDBEdit).Text);
    GetCurrentInformation(IndexDay, Empl, Plant, DateEMP);
//    if PlanInOtherPlants then
      Plant := dxDBEditPlant(IndexDay);
    if (Plant = '') {or (not PlanInOtherPlants)} then
    begin
      PlantCmp := FindComponent('dxDBEditP' + IntToStr(IndexDay) + '5');
      if PlantCmp <> nil then
      begin
        (PlantCmp as TdxDBEdit).Text :=
          EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
        Plant := (PlantCmp as TdxDBEdit).Text;
      end;
    end;
    if not EmployeeAvailabilityDM.IsShift(Plant, Shift) then
    begin
      DisplayMessage(SSHSShift, mtInformation, [mbOk]);
    end;
  end;
end;
*)
procedure TEmployeeAvailabilityF.dxDBGridAvailabilityExit(Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

procedure TEmployeeAvailabilityF.AskForChanges;
var
  QuestionResult: Integer;
begin
  QuestionResult := 0;
  if dxBarBDBNavPost.Enabled then
    QuestionResult := DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo]);
  case QuestionResult of
    mrYes    : dxBarBDBNavPost.OnClick(dxBarDBNavigator);
    mrNo     : dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end; { case }
  if dxBarBDBNavPost.Enabled then
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
end;

procedure TEmployeeAvailabilityF.dxBarBDBNavCancelClick(Sender: TObject);
var
  IndexDay: Integer;
  PlantCode: String;
begin
 // inherited;
  EmployeeAvailabilityDM.TablePivot.Cancel;
  //car 550276 - initialize array for saving days changed
  EmployeeAvailabilityDM.InitializeDayChangedArray;
  EnabledNavButtons(False);
  for IndexDay := 1 to 7 do
  begin
    if True {PlanInOtherPlants} then
      PlantCode := EmployeeAvailabilityDM.TablePivot.FieldByName('PLANT' +
        IntToStr(IndexDay)).AsString
    else
      PlantCode :=
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
    EnableEditDetail(False, PlantCode,
      EmployeeAvailabilityDM.TablePivot.FieldByName('DAY' +
        IntToStr(IndexDay) + '5').AsString, IndexDay);
  end;
  FInvalidField := False;
end;

procedure TEmployeeAvailabilityF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
  ButtonStaffAvailability.Enabled := Not Active;
  ButtonRestoreStd.Enabled := Not Active;
end;

procedure TEmployeeAvailabilityF.ButtonRestoreStdClick(Sender: TObject);
begin
  inherited;
  if not EmployeeavailabilityDM.TablePivot.IsEmpty  then
    EmployeeAvailabilityDM.CopySTA_EMA(Round(dxSpinEditYear.Value),
      Round(dxDBSpinEditWeek.Value));
end;

procedure TEmployeeAvailabilityF.SetBalances;
var
  Remaining, Normal, Earned, Used, Planned, Available, CounterDesc: String;
  GroupVisible: Boolean;
  DefaultVisible: Boolean;
begin
  TabSheet4.Visible := False;
  try
    if EmployeeAvailabilityDM.QueryEmpl.Active and
      (not EmployeeAvailabilityDM.QueryEmpl.IsEmpty) then
      EditStartDate.Text := FormatDateTime('dd/mm/yyyy',
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('STARTDATE').AsDateTime)
    else
      EditStartDate.Text := '';

    ABalanceCounters.EmployeeNumber := EmployeeAvailabilityDM.FEmployee;
    ABalanceCounters.Year := EmployeeAvailabilityDM.FYear;
    ABalanceCounters.DateFrom := Trunc(Now);

    // RV079.4. Do this AFTER initializing the parameters!
    DefaultVisible := not ABalanceCounters.AbsenceTypePerCountryExist;

    ABalanceCounters.GetCounters;

    // RV071.5.
    // Make all groupboxes invisible in reverse order.
    GroupBoxShorterWeek.Visible := False;
    GroupBoxRsvBnkHoliday.Visible := False;
    GroupBoxSatCredit.Visible := False;
    GroupBoxAddBnkHoliday.Visible := False;
    GroupBoxSeniorityHld.Visible := False;
    GroupBoxHldPrevYear.Visible := False;
    GroupBox5.Visible := False;
    GroupBox2.Visible := False;
    GroupBoxTravelTime.Visible := False; // 20013183

    // RV067.MRA.30 - Make some extra visible or not.
    // Holiday
    CounterDesc := HolidayCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible := 
        ABalanceCounters.GetCounterActivated(HOLIDAYAVAILABLETB, CounterDesc);
    GroupBox2.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox2.Caption := CounterDesc;
      // If absence type 'R' is activated as counter then 'remaining last year'
      // must not be shown for Holiday
      EditHolidayRemaining.Visible :=
        not
          ABalanceCounters.GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc);
      {LabelHolidayRemaining} Label6.Visible := EditHolidayRemaining.Visible;
      ABalanceCounters.ComputeHoliday(Remaining, Normal, Used, Planned,
        Available, {LabelHolidayRemaining} Label6.Visible);
      EditHolidayRemaining.Text := Remaining;
      {Mask}EditHolidayNorm.Text := Normal;

      EditHolidayUsed.Text := Used;
      EditHolidayPlanned.Text := Planned;
      EditHolidayAvailable.Text := Available;
    end;

    // RV067.MRA.30
    // Time for Time
    CounterDesc := TimeForTimeCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible := 
        ABalanceCounters.GetCounterActivated(TIMEFORTIMEAVAILABILITY, CounterDesc);
    GroupBox5.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox5.Caption := CounterDesc;
      ABalanceCounters.ComputeTimeForTime(Remaining, Normal, Used, Planned,
        Available);
      EditTimeRemaining.Text := Remaining;
      EditTimeEarned.Text := Normal;
      EditTimeUsed.Text := Used;
      EditTimePlanned.Text := Planned;
      EditTimeAvailable.Text := Available;
    end;

    // 20013183
    // Travel Time
    CounterDesc := TravelTimeCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(TRAVELTIMEAVAILABILITY, CounterDesc);
    GroupBoxTravelTime.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxTravelTime.Caption := CounterDesc;
      ABalanceCounters.ComputeTravelTime(Remaining, Earned, Used, Planned,
        Available);

      EditTravelTimeRemaining.Text := Remaining;
      EditTravelTimeEarned.Text := Earned;
      EditTravelTimeUsed.Text := Used;
      EditTravelTimePlanned.Text := Planned;
      EditTravelTimeAvailable.Text := Available;
    end;

    // RV067.MRA.30
    // Work time reduction
    CounterDesc := WorkTimeReductionCaption;
    if DefaultVisible then
      GroupVisible := True
    else
      GroupVisible :=
        ABalanceCounters.GetCounterActivated(WORKTIMEREDUCTIONAVAILABILITY, CounterDesc);
    GroupBox3.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBox3.Caption := CounterDesc;
      ABalanceCounters.ComputeWorkTime(Remaining, Normal, Used, Planned,
        Available);
      EditWorkRemaining.Text := Remaining;
      EditWorkEarned.Text := Normal;
      EditWorkUsed.Text := Used;
      EditWorkPlanned.Text := Planned;
      EditWorkAvailable.Text := Available;
    end;

    //Holiday previous year
    CounterDesc := HolidayPreviousYearCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(HOLIDAYPREVIOUSYEARAVAILABILITY, CounterDesc);
    GroupBoxHldPrevYear.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxHldPrevYear.Caption := CounterDesc;
      ABalanceCounters.ComputeHldPrevYear(Remaining, Used, Planned,
        Available);
      EditHldPrevYearRemaining.Text := Remaining;
      EditHldPrevYearUsed.Text := Used;
      EditHldPrevYearPlanned.Text := Planned;
      EditHldPrevYearAvailable.Text := Available;
    end;

    //Seniority Holiday
    CounterDesc := SeniorityHolidayCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(SENIORITYHOLIDAYAVAILABILITY, CounterDesc);
    GroupBoxSeniorityHld.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxSeniorityHld.Caption := CounterDesc;
      ABalanceCounters.ComputeSeniorityHld(Normal, Used, Planned,
        Available);
      {Mask}EditSeniorityHldNorm.Text := Normal;
      EditSeniorityHldUsed.Text := Used;
      EditSeniorityHldPlanned.Text := Planned;
      EditSeniorityHldAvailable.Text := Available;
    end;

    //Additional Bank Holiday
    CounterDesc := AddBankHolidayCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(ADDBANKHOLIDAYAVAILABILITY, CounterDesc);
    GroupBoxAddBnkHoliday.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxAddBnkHoliday.Caption := CounterDesc;
      ABalanceCounters.ComputeAddBnkHoliday(Remaining, Used, Planned,
        Available);
      EditAddBnkHolidayRemaining.Text := Remaining;
      EditAddBnkHolidayUsed.Text := Used;
      EditAddBnkHolidayPlanned.Text := Planned;
      EditAddBnkHolidayAvailable.Text := Available;
    end;

    //Saturday credit
    CounterDesc := SatCreditCaption;
    GroupVisible := 
      ABalanceCounters.GetCounterActivated(SATCREDITAVAILABILITY, CounterDesc);
    GroupBoxSatCredit.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxSatCredit.Caption := CounterDesc;
      ABalanceCounters.ComputeSatCredit(Remaining, Earned, Used, Planned,
        Available);
      EditSatCreditEarned.Text := Earned;
      EditSatCreditUsed.Text := Used;
      EditSatCreditPlanned.Text := Planned;
      EditSatCreditAvailable.Text := Available;
    end;

    //Bank holiday to reserve
    CounterDesc := BankHolToRsvCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(BANKHOLIDAYRESERVEAVAILABILITY, CounterDesc);
    GroupBoxRsvBnkHoliday.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxRsvBnkHoliday.Caption := CounterDesc;
      ABalanceCounters.ComputeRsvBnkHoliday(Normal, Used, Planned,
        Available);
      EditRsvBnkHolidayUsed.Text := Used;
      EditRsvBnkHolidayPlanned.Text := Planned;
      EditRsvBnkHolidayAvailable.Text := Available;
      {Mask}EditRsvBnkHolidayNorm.Text := Normal;
    end;

    //Shorter working week
    CounterDesc := ShorterWorkingWeekCaption;
    GroupVisible :=
      ABalanceCounters.GetCounterActivated(SHORTERWORKWEEKAVAILABILITY, CounterDesc);
    GroupBoxShorterWeek.Visible := GroupVisible;
    if GroupVisible then
    begin
      GroupBoxShorterWeek.Caption := CounterDesc;
      ABalanceCounters.ComputeShorterWeek(Remaining, Earned, Used, Planned,
        Available);
      EditShorterWeekRemaining.Text := Remaining;
      {Mask}EditShorterWeekEarned.Text := Earned;
      EditShorterWeekUsed.Text := Used;
      EditShorterWeekPlanned.Text := Planned;
      EditShorterWeekAvailable.Text := Available;
    end;

    // Illness
    ABalanceCounters.ComputeIllness(Remaining, Normal);
    EditIllness.Text := Remaining;   // currentyear
    EditIllnessUntil.Text := Normal; // previousyears
  finally
    TabSheet4.Visible := True;
  end;
end; // SetBalances


procedure TEmployeeAvailabilityF.PageControl1Change(Sender: TObject);
  procedure EnableButtonsTopBar(Status: Boolean);
  begin
    dxBarButtonSort.Enabled := Status;
    dxBarButtonCustCol.Enabled := Status;
    dxBarButtonExportAllHTML.Enabled := Status;
    dxBarButtonExportAllXLS.Enabled := Status;
    dxBarButtonResetColumns.Enabled := Status;
    dxBarButtonExpand.Enabled := Status;
    dxBarButtonCollapse.Enabled := Status;
    dxBarButtonShowGroup.Enabled := Status;
    PnlDetail.Visible := Status;
  end;
begin
  inherited;

  //CAR 27-10-2003
  EnableButtonsTopBar(True);

  if (PageControl1.ActivePageIndex = 0) then
    Exit;
  // RV079.4. There is an extra tab for 'holiday counters'.
  if (PageControl1.ActivePageIndex >= 1) then
    EnableButtonsTopBar(False);

  SetBalances;
end;

procedure TEmployeeAvailabilityF.CopyRecordClick(Sender: TObject);
begin
  inherited;
  // MR:24-11-2004 Disabled, because it didn't work correct
  // and was not used in practice.
(*
  if (dxDBSpinEditWeek.Text <> '') then
    EmployeeAvailabilityDM.SetCopyFromValues(EmployeeAvailabilityDM.FEmployee,
      Round(dxDBSpinEditWeek.Value));
  CopyRecord.Enabled := False;
  PasteRecord.Enabled := True;
*)
end;

procedure TEmployeeAvailabilityF.PasteRecordClick(Sender: TObject);
begin
  inherited;
  // MR:24-11-2004 Disabled, because it didn't work correct
  // and was not used in practice.
(*
  if not CopyRecord.Enabled then
  begin
    if (dxDBSpinEditWeek.Text = '') then
      DisplayMessage(SCopyFrom, mtInformation, [mbOk])
    else
    begin
      EmployeeAvailabilityDM.PasteExecute(EmployeeAvailabilityDM.FEmployee,
        Round(dxDBSpinEditWeek.Value));
      CopyRecord.Enabled := True;
      PasteRecord.Enabled := False;
    end;
  end
  else
    DisplayMessage(SCopyFrom, mtInformation, [mbOk]);
*)
end;

procedure TEmployeeAvailabilityF.dxDBEdit14MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  FColumnName := (Sender as TdxDBEdit).DataField;
  FCtrlState := (ssCtrl in Shift);
  (Sender as TdxDBEdit).SelectAll;
end;

procedure TEmployeeAvailabilityF.dxDBGridAvailabilityBackgroundDrawEvent(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect);
begin
  inherited;
 // Fill Group Panels Background with Color
  ACanvas.Brush.Color := (Sender as TdxDBGrid).GroupPanelColor;
  ACanvas.FillRect(ARect);
  // Draw the grouping text
  // This event is isolated for translation purposes
  if (Sender as TdxDBGrid).GroupColumnCount = 0 then
  begin
    ACanvas.Font.Color := clWhite;
    ACanvas.Brush.Style := bsClear;
    ACanvas.TextOut(10,(ARect.Bottom - ARect.Top + ACanvas.Font.Height) div 2,
      SPimsDragColumnHeader);
  end;
end;

procedure TEmployeeAvailabilityF.dxDBGridAvailabilityCustomDrawBand(
  Sender: TObject; ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  X, Y : Integer;
  IsClipRgnExists : Boolean;
  PrevClipRgn, Rgn : HRGN;
begin
  {save rgn}
  PrevClipRgn := CreateRectRgn(0, 0, 0, 0);
  IsClipRgnExists := GetClipRgn(ACanvas.Handle, PrevClipRgn) = 1;
  Rgn := CreateRectRgnIndirect(ARect);
  SelectClipRgn(ACanvas.Handle, Rgn);
  DeleteObject(Rgn);

  with Sender as TCustomdxTreeListControl do
  begin { Draw band texture }
    X := ARect.Left;
    while X < ARect.Right do
    begin
      Y := ARect.Top;
      while Y < ARect.Bottom do
      begin
        ACanvas.Draw(X, Y, imgBandTexture.Picture.Bitmap);
        inc(Y, imgBandTexture.Height);
      end;
      inc(X, imgBandTexture.Width);
    end;
// 20014289 Do not set colors here
    ACanvas.Font.Size := 9;
    X := ARect.Left + 4;
    Y := ARect.Top + (ARect.Bottom - ARect.Top - ACanvas.TextHeight(AText)) div 2;
    // Set background to neutral color
//    if (DownBandIndex = ABand.VisibleIndex) and DownBandPushed then
//      ACanvas.Brush.Color := clBtnFace;
    ACanvas.Brush.Style := bsClear;
    // Set font color to shadow effect or to normal color when pushed
//    if (DownBandIndex = ABand.VisibleIndex) and DownBandPushed then
//      ACanvas.Font.Color := clBtnText
//    else
//      ACanvas.Font.Color := clBtnShadow;
    // Draw shadow or caption when pushed
//    ACanvas.TextRect(ARect, X + 1, Y + 1, AText );
//    ACanvas.Font.Color := clBtnText;
    // Draw normal band caption
//    if not((DownBandIndex = ABand.VisibleIndex) and DownBandPushed) then
      ACanvas.TextRect(ARect, X, Y, AText );
    // Draw buttonize3D effect band
//    DrawEdge(ACanvas.Handle, ARect, BDR_RAISEDINNER, BF_TOPLEFT);
//    DrawEdge(ACanvas.Handle, ARect, BDR_RAISEDOUTER, BF_BOTTOMRIGHT);
    ADone := True;
  end;

  {restore rgn}
  if IsClipRgnExists then
    SelectClipRgn(ACanvas.Handle, PrevClipRgn)
  else
    SelectClipRgn(ACanvas.Handle, 0);
  DeleteObject(PrevClipRgn);
end;


procedure TEmployeeAvailabilityF.dxDBGridAvailabilityCustomDrawCell(
  Sender: TObject; ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  G: TCustomdxTreeListControl;//TdxDBGrid;
  StartDate, EndDate: TDateTime;
  Year, Week: Integer;
  procedure EditEnable(Year, Week: Integer);
  var
    Cmp: TComponent;
    {TBCount,} Index, IndexTB, Day: Integer;
    DayDate: TDateTime;
    MyEnable: Boolean;
  begin
    for Day := 1 to 7 do
    begin
      DayDate := ListProcsF.DateFromWeek(Year, Week, Day);
      MyEnable := (DayDate >= StartDate) and
        ((DayDate <= EndDate) or (EndDate = 0));
      // 20011800 Final Run System - set 'enabled' based on last-export-date
      if MyEnable then
        MyEnable := EmployeeAvailabilityDM.FinalRunCheck(DayDate);
      // 20011800 Also handle delete-button.
      if Day = 1 then
      begin
        if MyEnable then
          dxBarBDBNavDelete.Visible := ivAlways
        else
          dxBarBDBNavDelete.Visible := ivNever;
      end;
//      TBCount := 0;
      for Index := 0 to SystemDM.MaxTimeblocks do // 5
      begin
        if Index = 0 then
          IndexTB := ISHIFT
        else
          IndexTB := Index;
        Cmp := FindComponent('dxDBEdit' + IntToStr(Day) + IntToStr(IndexTB));
        if Assigned(Cmp) then
        begin
          // Because colors are also set in another part,
          // depending on other things, we make the component
          // (in)visible.
          (Cmp as TdxDBEdit).Visible := MyEnable;
//          if MyEnable then
//            inc(TBCount);
        end;
      end; // for Index
//      GroupBoxResize(Day, TBCount);
    end; // for Day
  end; // EditEnable
begin
  // Greenbar function
  inherited;
  G := Sender as TCustomdxTreeListControl;//TdxDBGrid;

  // grouping is on so keep the grid group node normal color
  if (G is TdxDBGrid) and ANode.HasChildren then
    Exit;

  if ASelected then
  begin
  // Different rowcolor while editing
    if (G is TdxDBGrid) and (edgoEditing in (G as TdxDBGrid).OptionsBehavior) then
    begin
      if AFocused then
      begin
        AColor := clHighlight;
        AFont.Color := clWhite;
      end
      else
      begin
        AColor := clGridNoFocus;
        AFont.Color := clBlack;
      end;
    end
    else
      if AFocused then
        AColor := clHighlight
      else
      begin
        AColor := clGridNoFocus;
        AFont.Color := clBlack;
      end;
  // Different rowcolor while editing
  end
  else { ASelected }
    if ANode.Parent = nil then // There is no groupping
      if (G is TdxDBGrid) then
        if ((edgoLoadAllRecords in (G as TdxDBGrid).OptionsDB) and (ANode.Index mod 2 <> 0))
          or (not (edgoLoadAllRecords in (G as TdxDBGrid).OptionsDB))
          and ((G as TdxDBGrid).DataSource.DataSet.RecNo mod 2 = 0) then
          AColor := clWindow
        else
          AColor := clSecondLine
      else { treelist }
        AColor := clTreeParent

    else // Tree Grouping
      if ANode.Index mod 2 <> 0 then
        AColor := clWindow
      else
        AColor := clSecondLine;
  // Greenbar function

  // MR:16-07-2004
  // Order 550232 For an employee how is not available
  // at a given date (startdate and enddate), disable
  // the days that he is not available.
  if AFocused or ASelected then
  begin
    if AColumn = dxDBGridAvailabilityColumnWeek then
    begin
      StartDate :=
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('STARTDATE').AsDateTime;
      EndDate :=
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('ENDDATE').AsDateTime;
      Year := dxSpinEditYear.IntValue;
      Week := dxDBGridAvailabilityColumnWeek.Field.AsInteger;
      EditEnable(Year, Week);
      ShowPeriod;
    end;
  end;
end; // dxDBGridAvailabilityCustomDrawCell


procedure TEmployeeAvailabilityF.dxDBGridAvailabilityCustomDrawColumnHeader(
  Sender: TObject; AColumn: TdxTreeListColumn; ACanvas: TCanvas;
  ARect: TRect; var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
  var ADone: Boolean);
begin
  AText    := LowerCase(AText);
  if AText <> '' then
    AText[1] := UpCase(AText[1]);
end;


procedure TEmployeeAvailabilityF.FormHide(Sender: TObject);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SUndoChanges, mtInformation, [mbOk]);
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end;
  inherited;
end;

procedure TEmployeeAvailabilityF.ButtonStaffAvailabilityClick(
  Sender: TObject);
var
  Plant, Team: String;
  Year, Week: Word;
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtInformation, [mbOk]);
    Exit;
  end;
  Plant := '';
  Team := '';
  with EmployeeAvailabilityDM do
  begin
    if (EmployeeAvailabilityDM <> nil) then
    begin
      Plant := QueryEmpl.FieldByName('PLANT_CODE').AsString;
      Team := QueryEmpl.FieldByName('TEAM_CODE').AsString;
      Year := dxSpinEditYear.IntValue;
      Week := TablePIVOT.FieldByName('WEEK_NR').AsInteger;
      SideMenuUnitMaintenanceF.SwitchStaffAvailForm(Team, Plant, Year, Week);
    end;
  end;
end;

procedure TEmployeeAvailabilityF.dxBarButtonCustColClick(Sender: TObject);
begin
  ActiveGrid := dxDBGridAvailability;
  inherited;

end;

procedure TEmployeeAvailabilityF.dxDBEdit11SelectionChange(Sender: TObject);
begin
  inherited;
  SetValueForAllEditFields('', (Sender as TdxDBEdit).Text,
    (Sender as TdxDBEdit).Tag, 1);
  FPopupOpen := False;
end;

procedure TEmployeeAvailabilityF.PopupMenuAbsRsnPopup(Sender: TObject);
begin
  inherited;
  FPopupOpen := True;
end;

procedure TEmployeeAvailabilityF.dxDBEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  (Sender as TdxDBEdit).SelectAll;
  FPopupOpen := False;
  //car 550276 - set day change atribute
  EmployeeAvailabilityDM.FDayChanged[TdxDBEdit(Sender).Tag div 10] := 1;
end;

procedure TEmployeeAvailabilityF.dxDBEdit11ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
  inherited;
  (Sender as TdxDBEdit).SelectAll;
  FPopupOpen := False;
  // GLOB3-94 use try-except to trap and log errors
  try
    EmployeeAvailabilityDM.TablePivot.Edit;
  except
    // Trap errors here
    on E:EdbEngineError do
    begin
      DisplayErrorMessage(E, PIMS_DELETING);
      // Log error!
      WErrorLog(GetErrorMessage(E, PIMS_DELETING));
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      Exit;
    end;
    on E:Exception do
    begin
      // Log error!
      WErrorLog(E.message);
      DisplayMessage(E.message, mtError, [mbOK]);
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Rollback;
      Exit;
    end;
  end;
// if lowercase then make it uppercase
  if (Sender is TdxDBEdit) then
  begin
    if EmployeeAvailabilityDM.TablePivot.
      FieldByName((Sender as TdxDBEdit).DataField).AsString <> '' then
    begin
      if (Ord(EmployeeAvailabilityDM.TablePivot.
        FieldByName((Sender as TdxDBEdit).DataField).AsString[1]) >= 97) then
          EmployeeAvailabilityDM.TablePivot.
            FieldByName((Sender as TdxDBEdit).DataField).AsString :=
              UpperCase((Sender as TdxDBEdit).Text);

// if uppercase then make it lowercase
      if (Ord(EmployeeAvailabilityDM.TablePivot.
        FieldByName((Sender as TdxDBEdit).DataField).AsString[1]) <= 90) then
        EmployeeAvailabilityDM.TablePivot.
        FieldByName((Sender as TdxDBEdit).DataField).AsString :=
          LowerCase((Sender as TdxDBEdit).Text);
    end;
  end;
  //car 550276
  EmployeeAvailabilityDM.FDayChanged[TdxDBEdit(Sender).Tag div 10] := 1;
end;

procedure TEmployeeAvailabilityF.dxDBEdit111ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
  MenuLine: String;
  MenuItem: TMenuItem;
begin
  inherited;
  if EmployeeAvailabilityDM.QueryEmpl.
    FieldByName('PLANT_CODE').AsString <> '' then
  begin
    FillShiftPopup(EmployeeAvailabilityDM.QueryEmpl.
      FieldByName('PLANT_CODE').AsString);
    if PlanInOtherPlants then // For ShiftPopupMenu!
    begin
      MenuLine := FormatPlantShift(FEditPlantField.Text, FEditField.Text);
      MenuItem := PopupMenuShift.Items.Find(MenuLine);
      if MenuItem <> nil then
      begin
        MenuItem.Checked := True;
      end;
    end;
  end;
  FPopupOpen := False;
end;

procedure TEmployeeAvailabilityF.dxDBEdit111MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  TempComponent: TComponent;
begin
  inherited;
  (Sender as TdxDBEdit).SelectAll;
  FEditField := (Sender as TdxDBEdit);
  TempComponent := FindComponent('dxDBEditP' +
    IntToStr((Sender as TdxDBEdit).Tag)); // ends with P15, P25, P35...P75
  if Assigned(TempComponent) then
    FEditPlantField := (TempComponent as TdxDBEdit);
  FCtrlState := (ssCtrl in Shift);
end;

procedure TEmployeeAvailabilityF.dxDBEdit111KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  FPopupOpen := False;
  EnabledNavButtons(True);
end;

procedure TEmployeeAvailabilityF.ScrollBarMasterScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  dxDBGridAvailability.SetFocus;
end;

procedure TEmployeeAvailabilityF.SetPlanInOtherPlants(
  const Value: Boolean);
begin
  FPlanInOtherPlants := Value;
end;

procedure TEmployeeAvailabilityF.CheckBoxPlanInOtherPlantsClick(
  Sender: TObject);
var
  Text: String;
  Accept: Boolean;
begin
  inherited;
  PlanInOtherPlants := CheckBoxPlanInOtherPlants.Checked;
  EmployeeAvailabilityDM.PlanInOtherPlants := PlanInOtherPlants;
  dxDBExtLookupEditEmployeeCloseUp(Sender, Text, Accept);
end;

procedure TEmployeeAvailabilityF.DefaultChangeShiftNo(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  IndexDay, ShiftNumber, Empl: Integer;
  Plant: String;
  DateEMP: TDateTime;
  PlantCmp: TComponent;
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;
  if (((Sender as TdxDBEdit).Text < '0') or
    ((Sender as TdxDBEdit).Text > '99')) and
    ((Sender as TdxDBEdit).Text <> '-') and
    ((Sender as TdxDBEdit).Text <> '') then
  begin
    DisplayMessage(SShiftSchedule, mtInformation, [mbOk]);
  end;
  if ((Sender as TdxDBEdit).Text >= '1') and
    ((Sender as TdxDBEdit).Text <= '99') then
  begin
    IndexDay := (Sender as TdxDBEdit).Tag div 10;
    ShiftNumber := StrToInt((Sender as TdxDBEdit).Text);
    GetCurrentInformation(IndexDay, Empl, Plant, DateEMP);
//    if PlanInOtherPlants then
      Plant := dxDBEditPlant(IndexDay);
    if (Plant = '') {or (not PlanInOtherPlants)} then
    begin
      PlantCmp := FindComponent('dxDBEditP' + IntToStr(IndexDay) + '5');
      if Assigned(PlantCmp) then
      begin
        (PlantCmp as TdxDBEdit).Text :=
          EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString;
        Plant := (PlantCmp as TdxDBEdit).Text;
      end;
    end;
    if not EmployeeAvailabilityDM.IsShift(Plant, ShiftNumber) then
    begin
      DisplayMessage(SSHSShift, mtInformation, [mbOk]);
    end;
  end;
end; // DefaultChangeShiftNo

// MR:12-01-2005 Order 550365
procedure TEmployeeAvailabilityF.ButtonCopyFromOtherWeekClick(
  Sender: TObject);
var
  EmployeeNumber, SourceWeekNumber, TargetWeekNumber: Integer;
begin
  inherited;
  EmployeeAvailabilityDM.PasteSkipCount := 0; // 20011800
  // Copy from other week...
  if EmployeeAvailabilityDM.TablePIVOT.IsEmpty or
    (EmployeeAvailabilityDM.TablePIVOT.RecordCount <= 1) then
    Exit;
  SourceWeekNumber := 0;
  DialogSelectionCopyFromOWF := TDialogSelectionCopyFromOWF.Create(nil);
//  if SystemDM.EmbedDialogs then
//    DialogSelectionCopyFromOWF.FormStyle := fsStayOnTop;
  try
    DialogSelectionCopyFromOWF.MaxWeekNumber :=
      ListProcsF.WeeksInYear(Round(dxSpinEditYear.Value));
    DialogSelectionCopyFromOWF.WeekNumber := SourceWeekNumber;
    if DialogSelectionCopyFromOWF.ShowModal = mrOK then
      SourceWeekNumber := DialogSelectionCopyFromOWF.WeekNumber;
  finally
    DialogSelectionCopyFromOWF.Free;
  end;
  with EmployeeAvailabilityDM do
  begin
    TargetWeekNumber := TablePIVOT.FieldByName('WEEK_NR').AsInteger;
    EmployeeNumber := dxDBExtLookupEditEmployee.DataSource.DataSet.
      FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if (SourceWeekNumber >= 1) and
      (SourceWeekNumber <=
        ListProcsF.WeeksInYear(Round(dxSpinEditYear.Value))) and
        (SourceWeekNumber <> TargetWeekNumber) then
    begin
      if EmployeeAvailabilityDM.CopyFromOtherWeek(EmployeeNumber,
        SourceWeekNumber, TargetWeekNumber) then
      begin
        TablePivot.Edit;
        dxBarBDBNavPostClick(Sender);
      end;
    end;
  end;
  if EmployeeAvailabilityDM.PasteSkipCount > 0 then // 20011800
    DisplayMessage(SPimsFinalRunAvailabilityChange, mtInformation, [mbOk]);
end;

// MR:16-11-2005 Order 550415, when switching to another employee,
// the record was not saved.
procedure TEmployeeAvailabilityF.dxDBExtLookupEditEmployeeEnter(
  Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

procedure TEmployeeAvailabilityF.ShowPeriod;
begin
  try
    // MRA:28-JAN-2011. Small bugfix: to prevent problems in compiler.
    if (dxSpinEditYear.Text <> '') and (dxDBSpinEditWeek.Text <> '') then
      lblPeriod.Caption :=
        '(' +
        DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
        Round(dxDBSpinEditWeek.Value), 1)) + ' - ' +
        DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
        Round(dxDBSpinEditWeek.Value), 7)) + ')'
    else
      lblPeriod.Caption := '';
  except
    lblPeriod.Caption := '';
  end;
end;

// PIM-53
procedure TEmployeeAvailabilityF.AddHoliday(ADateFrom, ADateTo: TDateTime;
  APlantCode: String; AShiftNumber: Integer; AbsenceReasonCode: String);
var
  EmployeeNumber: Integer;
  DateLoop: TDate;
  CurrentWeek, CurrentYear: Word;
  IndexDay, Index, IndexTB: Integer;
  Day{, DayOfTheWeek}: Integer;
  CurrentDate: TDateTime;
  EMAAdded: Boolean;
  DlgMsgResult, QuestionResult: Integer;
  StandardStaffAvailUsed: Boolean; // PIM-53.3
  function FillOneDay: Boolean;
  var
    IndexDay, IndexTB: Integer;
  begin
    Result := True;
    // Fill FSaveDayFrom
    with EmployeeAvailabilityDM do
    begin
      qrySTA.Close;
      qrySTA.ParamByName('EMPLOYEE_NUMBER').AsInteger := EmployeeNumber;
      qrySTA.ParamByName('PLANT_CODE').AsString := APlantCode;
      qrySTA.ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
      qrySTA.Open;
      if qrySTA.IsEmpty then
      begin
        Result := False;
        DisplayMessage(SPimsAddHolidayNoStandAvail, mtError, [mbOK]);
        Exit;
      end;
      for IndexDay := 1 to 7 do
      begin
        if not qrySTA.IsEmpty then
          if qrySTA.Locate('DAY_OF_WEEK', IndexDay, [])  then
          begin
            // REMARK: Check in standard-availability if a timeblock is set
            //         to available or not! Only when available then fill it!
            for IndexTB := 1 to SystemDM.MaxTimeblocks do // 4
            begin
              if qrySTA.FieldByName(
                Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString = '*' then
                FSaveDayFrom[IndexDay, IndexTB] := AbsenceReasonCode
              else
                // PIM-53.2 Extra check: Only when STA is set to '-' assign it to empty,
                // otherwise leave it like it is, because it can be a fixed absence!
                if (qrySTA.FieldByName(
                  Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString = '-') or
                  (qrySTA.FieldByName(
                  Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString = '') then
                  FSaveDayFrom[IndexDay, IndexTB] := ''
                else // PIM-53.2. Assign it to Standard Availability.
                begin
                  FSaveDayFrom[IndexDay, IndexTB] := qrySTA.FieldByName(
                    Format('AVAILABLE_TIMEBLOCK_%d', [IndexTB])).AsString;
                  if (Day = IndexDay) then
                    StandardStaffAvailUsed := True; // PIM-53.3
                end;
            end;
          end;
        FSaveDayFrom[IndexDay, ISHIFT] := IntToStr(AShiftNumber);
        FSaveDayPlantFrom[IndexDay] := APlantCode;
      end; // for IndexDay
    end; // with
  end; // FillOneDay
begin
  // PIM-53-Rework:
  // First get first week/year and last week/year based on DateFrom/DateTo
  // It should be witing the same year!
  EmployeeNumber :=
    EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  EmployeeAvailabilityDM.FEmployeeFrom := EmployeeNumber;
  EMAAdded := False;
  DlgMsgResult := 0;
  QuestionResult := 0;
  StandardStaffAvailUsed := False;
  try
    SystemDM.Pims.StartTransaction;
    try
      DateLoop := ADateFrom;
      while DateLoop <= ADateTo do
      begin
        ListProcsF.WeekUitDat(DateLoop, CurrentYear, CurrentWeek);
        Day := ListProcsF.PimsDayOfWeek(DateLoop);
//        DayOfTheWeek := DayOfWeek(DateLoop);
//        if not (DayOfTheWeek in [1,7]) then // Skip Saturday and Sunday // GLOB3-275 Look for all days!
        begin
          dxDBSpinEditWeek.Text := IntToStr(CurrentWeek);
          dxSpinEditYear.Text := IntToStr(CurrentYear);
          EmployeeAvailabilityDM.FYear := CurrentYear;
          if not FillOneDay then
            Break;
          // The record must already exist in PivotTable
          // (it is used in PasteOneDay-function)
          // when not then insert it first!
          // And do that for the whole week!
          if not EmployeeAvailabilityDM.TablePIVOT.FindKey([
            SystemDM.CurrentComputerName, EmployeeNumber,
            CurrentYear, CurrentWeek]) then
          begin
            EMAAdded := True;
            // Add record to TablePIVOT for the whole CurrentWeek!
            with EmployeeAvailabilityDM do
            begin
              TablePivot.Insert;
              TablePivot.FieldByName('PIVOTCOMPUTERNAME').AsString :=
                SystemDM.CurrentComputerName;
              TablePivot.FieldByName('WEEK_NR').AsInteger := CurrentWeek;
              TablePivot.FieldByName('YEAR_NR').AsInteger := CurrentYear;
              TablePivot.FieldByName('EMPLOYEE_NR').AsInteger := EmployeeNumber;
              for IndexDay := 1 to 7 do
              begin
                CurrentDate :=
                  ListProcsF.DateFromWeek(CurrentYear, CurrentWeek, IndexDay);
//                DayOfTheWeek := DayOfWeek(CurrentDate);
//                if not (DayOfTheWeek in [1,7]) then // Skip Saturday and Sunday //  GLOB3-275 Look for all days!
                  if (CurrentDate >= ADateFrom) and (CurrentDate <= ADateTo) then
                  begin
                    for Index := 0 to SystemDM.MaxTimeblocks do // 5
                    begin
                      if Index = 0 then
                        IndexTB := ISHIFT
                      else
                        IndexTB := Index;
                      TablePivot.FieldByName(
                        Format('DAY%d%d', [IndexDay, IndexTB])).AsString :=
                          FSaveDayFrom[IndexDay, IndexTB];
                    end;
                    TablePivot.FieldByName(
                      Format('PLANT%d', [IndexDay])).AsString :=
                         FSaveDayPlantFrom[IndexDay];
                  end;
              end; // for
              TablePivot.Post;
            end; // with EmployeeAvailabilityDM
          end; // if not TablePivot found
          if EmployeeAvailabilityDM.PasteOneDay(EmployeeNumber, Day,
            CurrentWeek, DlgMsgResult, QuestionResult, EMAAdded) then
            if DlgMsgResult <> mrAll then
              if StandardStaffAvailUsed then // PIM-53.3
              begin
                DisplayMessage(SPimsStandStaffAvailUsed, mtInformation, [mbOK]);
                StandardStaffAvailUsed := False;
              end;
        end;
        DateLoop := DateLoop + 1;
      end; // while
      if SystemDM.Pims.InTransaction then
        SystemDM.Pims.Commit;
      if DlgMsgResult = mrAll then
        if StandardStaffAvailUsed then // PIM-53.3
          DisplayMessage(SPimsStandStaffAvailUsed, mtInformation, [mbOK]);
    except
      // Trap errors here
      on E:EdbEngineError do
      begin
        // Log error!
        WErrorLog(E.message);
        DisplayMessage(E.message, mtError, [mbOK]);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
      on E:Exception do
      begin
        // Log error!
        WErrorLog(E.message);
        DisplayMessage(E.message, mtError, [mbOK]);
        if SystemDM.Pims.InTransaction then
          SystemDM.Pims.Rollback;
      end;
    end;
  finally
  end;
end; // AddHoliday

// PIM-53
procedure TEmployeeAvailabilityF.ButtonAddHolidayClick(Sender: TObject);
begin
  inherited;
  // NOTE: The Owner is in this case 'SideMenuUnitMaintananceF' to ensure
  //       it is centered correctly. Set sub-form to 'Position=OwnerFormCenter'.
  // Set this to nil, or it will insert pivot records when they are not needed!
  // This can happen when you enter holiday for the last week of the year
  // that causes a year-change because of the weeks!
  DialogAddHolidayF := TDialogAddHolidayF.Create(SideMenuUnitMaintenanceF);
  try
    DialogAddHolidayF.EmployeeNumber :=
      EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    if DialogAddHolidayF.ShowModal = mrOK then
    begin
      // Add Holiday to Availablity
      AddHoliday(
        DialogAddHolidayF.DateFrom,
        DialogAddHolidayF.DateTo,
        DialogAddHolidayF.PlantCode,
        DialogAddHolidayF.ShiftNumber,
        DialogAddHolidayF.AbsenceReasonCode
        );
    end;
  finally
    DialogAddHolidayF.Free;
  end;
end; // ButtonAddHolidayClick

// PIM-52
procedure TEmployeeAvailabilityF.ButtonWorkScheduleClick(Sender: TObject);
var
  BookMark: TBookMark;
  CurrentYear, CurrentWeek: Word;
begin
  inherited;
  // PIM-52.1
  ListProcsF.WeekUitDat(Date, CurrentYear, CurrentWeek);
  BookMark := EmployeeAvailabilityDM.TablePIVOT.GetBookmark;
  try
    DialogProcessWorkScheduleF := TDialogProcessWorkScheduleF.Create(SideMenuUnitMaintenanceF);
    try
      // PIM-52.1 Be sure the year/week are filled in!
      try
        DialogProcessWorkScheduleF.Year := dxSpinEditYear.IntValue;
      except
        dxSpinEditYear.IntValue := CurrentYear;
        DialogProcessWorkScheduleF.Year := dxSpinEditYear.IntValue;
      end;
      try
        if (dxDBSpinEditWeek.Text <> '') then
        begin
          DialogProcessWorkScheduleF.FromWeek := dxDBSpinEditWeek.IntValue;
          DialogProcessWorkScheduleF.ToWeek := dxDBSpinEditWeek.IntValue;
        end
        else
        begin
          dxDBSpinEditWeek.IntValue := CurrentWeek;
          DialogProcessWorkScheduleF.FromWeek := CurrentWeek;
          DialogProcessWorkScheduleF.ToWeek := CurrentWeek;
        end;
      except
        dxDBSpinEditWeek.IntValue := CurrentWeek;
        DialogProcessWorkScheduleF.FromWeek := CurrentWeek;
        DialogProcessWorkScheduleF.ToWeek := CurrentWeek;
      end;
      DialogProcessWorkScheduleF.qryEmployee := EmployeeAvailabilityDM.QueryEmpl;
      DialogProcessWorkScheduleF.MyEmployee :=
        EmployeeAvailabilityDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      DialogProcessWorkScheduleF.ShowModal;
    except
      on E:Exception do
        WErrorLog(E.Message);
    end;
  finally
    DialogProcessWorkScheduleF.Free;
    dxSpinEditYear.IntValue := dxSpinEditYear.IntValue + 1;
    RefreshGrid;
    dxSpinEditYear.IntValue := dxSpinEditYear.IntValue - 1;
    RefreshGrid;
    EmployeeAvailabilityDM.TablePIVOT.GotoBookmark(BookMark);
    EmployeeAvailabilityDM.TablePIVOT.FreeBookmark(BookMark);
  end;
end;

procedure TEmployeeAvailabilityF.GroupBoxResize(ADay, ATBCount: Integer);
var
  Cmp: TComponent;
begin
  if ATBCount < MIN_TBS then
    ATBCount := MIN_TBS;
  Cmp := FindComponent(Format('GroupBoxDay%d', [ADay]));
  if Assigned(Cmp) then
    if (Cmp is TGroupBox) then
      TGroupBox(Cmp).Width :=
        6 + (dxDBEdit111.Width+1) + ((dxDBEdit11.Width+1) * (ATBCount));
  GridColumnVisible(ADay, MaxTBCount);
end; // GroupBoxResize

procedure TEmployeeAvailabilityF.GridColumnVisible(ADay,
  ATBCount: Integer);
var
  Cmp: TComponent;
  TBIndex: Integer;
begin
  if ATBCount < MIN_TBS then
    ATBCount := MIN_TBS;
  for TBIndex := 1 to SystemDM.MaxTimeblocks do
  begin
    Cmp := FindComponent(Format('dxDBGridAvailabilityColumnD%d%d', [ADay, TBIndex]));
    if Assigned(Cmp) then
      if (Cmp is TdxDBGridColumn) then
      begin
        if TBIndex <= ATBCount then
          TdxDBGridColumn(Cmp).Visible := True
        else
          TdxDBGridColumn(Cmp).Visible := False;
      end;
  end;
end; // GridColumnVisible

// GLOB3-60
procedure TEmployeeAvailabilityF.SetMaxTBCount(const Value: Integer);
begin
  if Assigned(EmployeeAvailabilityDM) then
  begin
    if EmployeeAvailabilityDM.QueryEmpl.Active then
    begin
      if not EmployeeAvailabilityDM.QueryEmpl.Eof then
        FMaxTBCount :=
          PlantShiftMaxTBCount(
            EmployeeAvailabilityDM.QueryEmpl.FieldByName('PLANT_CODE').AsString)
      else
        FMaxTBCount := ShiftMaxTBCount;
    end
    else
      FMaxTBCount := ShiftMaxTBCount;
  end
  else
    FMaxTBCount := ShiftMaxTBCount;
end;

// GLOB3-225
procedure TEmployeeAvailabilityF.HideColumns;
var
  Day, I: Integer;
  C: TComponent;
begin
  for Day := 1 to 7 do
    for I := SystemDM.MaxTimeblocks + 1 to MAX_TBS do
    begin
      C := FindComponent('dxDBGridAvailabilityColumnD' + IntToStr(Day) +
        IntToStr(I));
      if Assigned(C) then
        if (C is TdxDBGridColumn) then
        begin
          TdxDBGridColumn(C).Visible := False;
          TdxDBGridColumn(C).DisableGrouping := True;
        end;
    end;
end; // HideColumns

end.
