inherited TypeHourPerCountryF: TTypeHourPerCountryF
  Left = 254
  Top = 47
  Width = 677
  Caption = 'Types of hour per country'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 669
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 667
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 667
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'code'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'description'
      end
      object dxMasterGridColumnExportType: TdxDBGridColumn
        Caption = 'Export Type'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'export_type'
      end
    end
  end
  inherited pnlDetail: TPanel
    Width = 669
    TabOrder = 3
    OnEnter = pnlDetailEnter
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 339
      Height = 163
      Align = alLeft
      Caption = 'Types of hour'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 20
        Width = 37
        Height = 13
        Caption = 'Number'
      end
      object Label3: TLabel
        Left = 8
        Top = 48
        Width = 53
        Height = 13
        Caption = 'Description'
      end
      object Label2: TLabel
        Left = 8
        Top = 76
        Width = 87
        Height = 13
        Caption = 'Bonus percentage'
      end
      object Label4: TLabel
        Left = 178
        Top = 77
        Width = 11
        Height = 13
        Caption = '%'
      end
      object Label5: TLabel
        Left = 8
        Top = 104
        Width = 58
        Height = 13
        Caption = 'Export code'
      end
      object Label6: TLabel
        Left = 8
        Top = 132
        Width = 69
        Height = 13
        Caption = 'Minimum wage'
      end
      object dxDBEditBonusPerc: TdxDBEdit
        Left = 115
        Top = 72
        Width = 62
        Enabled = False
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'BONUS_PERCENTAGE'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ReadOnly = True
        StoredValues = 64
      end
      object dxDBEdit1: TdxDBEdit
        Left = 115
        Top = 128
        Width = 62
        Enabled = False
        Style.BorderStyle = xbsSingle
        TabOrder = 5
        DataField = 'MINIMUM_WAGE'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ReadOnly = True
        StoredValues = 64
      end
      object DBCheckBox2: TDBCheckBox
        Left = 200
        Top = 72
        Width = 105
        Height = 17
        Caption = 'Wage bonus only'
        Ctl3D = False
        DataField = 'WAGE_BONUS_ONLY_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 4
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBEditTypeHours: TdxDBEdit
        Left = 115
        Top = 14
        Width = 62
        Enabled = False
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'HOURTYPE_NUMBER'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ReadOnly = True
        StoredValues = 64
      end
      object DBEditExportCode: TdxDBEdit
        Left = 115
        Top = 100
        Width = 62
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'EXPORT_CODE'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ReadOnly = False
        StoredValues = 64
      end
      object DBEditDescription: TdxDBEdit
        Tag = 1
        Left = 115
        Top = 44
        Width = 214
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'description'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ReadOnly = False
        StoredValues = 64
      end
      object DBCheckBoxExportOvertimeADP: TDBCheckBox
        Left = 200
        Top = 101
        Width = 136
        Height = 17
        Caption = 'Export Overtime ADP'
        Ctl3D = False
        DataField = 'EXPORT_OVERTIME_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 6
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxExportPayrollADP: TDBCheckBox
        Left = 200
        Top = 120
        Width = 136
        Height = 17
        Caption = 'Export Payroll ADP'
        Ctl3D = False
        DataField = 'EXPORT_PAYROLL_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        ParentCtl3D = False
        TabOrder = 7
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
    object GroupBox2: TGroupBox
      Left = 340
      Top = 1
      Width = 328
      Height = 163
      Align = alClient
      TabOrder = 1
      object DBCheckBoxOverTime: TDBCheckBox
        Left = 16
        Top = 15
        Width = 97
        Height = 17
        Caption = 'Overtime'
        Ctl3D = False
        DataField = 'OVERTIME_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
        OnClick = DBCheckBoxOverTimeClick
      end
      object DBCheckBoxCountDays: TDBCheckBox
        Left = 16
        Top = 86
        Width = 97
        Height = 17
        Caption = 'Count days'
        Ctl3D = False
        DataField = 'COUNT_DAY_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 4
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 105
        Width = 121
        Height = 17
        Caption = 'Ignore overtime'
        Ctl3D = False
        DataField = 'IGNORE_FOR_OVERTIME_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 5
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxTimeForTime: TDBCheckBox
        Left = 40
        Top = 34
        Width = 97
        Height = 17
        Caption = 'Time for time'
        DataField = 'TIME_FOR_TIME_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ReadOnly = True
        TabOrder = 1
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxBonusInMoney: TDBCheckBox
        Left = 40
        Top = 51
        Width = 97
        Height = 17
        Caption = 'Bonus in money'
        DataField = 'BONUS_IN_MONEY_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxADV: TDBCheckBox
        Left = 40
        Top = 68
        Width = 193
        Height = 17
        Caption = 'Shorter Working Week'
        DataField = 'ADV_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ReadOnly = True
        TabOrder = 3
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxSatCredit: TDBCheckBox
        Left = 16
        Top = 125
        Width = 121
        Height = 17
        Caption = 'Saturday Credit'
        Ctl3D = False
        DataField = 'SATURDAY_CREDIT_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 6
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object DBCheckBoxTravelTime: TDBCheckBox
        Left = 16
        Top = 144
        Width = 97
        Height = 17
        Caption = 'Travel Time'
        DataField = 'TRAVELTIME_YN'
        DataSource = TypeHourPerCountryDM.DataSourceDetail
        Enabled = False
        TabOrder = 7
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 669
    inherited spltDetail: TSplitter
      Width = 667
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 667
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Types of hour'
          Width = 649
        end>
      KeyField = 'HOURTYPE_NUMBER'
      OnEnter = dxDetailGridEnter
      ShowBands = True
      object dxDetailGridColumn1: TdxDBGridColumn
        Caption = 'Number'
        Width = 84
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPE_NUMBER'
      end
      object dxDetailGridColumn2: TdxDBGridColumn
        Caption = 'Description'
        Width = 191
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxDetailGridColumn4: TdxDBGridColumn
        Caption = 'Bonus '
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_PERCENTAGE'
      end
      object dxDetailGridColumn5: TdxDBGridCheckColumn
        Caption = 'Overtime'
        MinWidth = 20
        Width = 87
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OVERTIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnTimeForTime: TdxDBGridCheckColumn
        Caption = 'Time for time'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TIME_FOR_TIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnBonusInMoney: TdxDBGridCheckColumn
        Caption = 'Bonus in money'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'BONUS_IN_MONEY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnADV: TdxDBGridCheckColumn
        Caption = 'SWW'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADV_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn6: TdxDBGridCheckColumn
        Caption = 'Count'
        Width = 88
        BandIndex = 0
        RowIndex = 0
        FieldName = 'COUNT_DAY_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn8: TdxDBGridCheckColumn
        Caption = 'Ignore overtime'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IGNORE_FOR_OVERTIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn12: TdxDBGridCheckColumn
        Caption = 'Sat. Credit'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SATURDAY_CREDIT_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumn3: TdxDBGridColumn
        Caption = 'Export code'
        Width = 86
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_CODE'
      end
      object dxDetailGridColumn7: TdxDBGridColumn
        Caption = 'Minimum wage'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'MINIMUM_WAGE'
      end
      object dxDetailGridColumnExportOvertime: TdxDBGridCheckColumn
        Caption = 'Export Overtime ADP'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_OVERTIME_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
      object dxDetailGridColumnExportPayroll: TdxDBGridCheckColumn
        Caption = 'Export Payroll ADP'
        Width = 100
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EXPORT_PAYROLL_YN'
        ValueChecked = 'Y'
        ValueUnchecked = 'N'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
  end
end
