(*
  Changes:
    MRA:12-MAR-2008 Order 550463, RV005.
      Add plant-from-to selection to report.
    MRA:16-FEB-2010. RV054.4. 889938.
    - Suppress lines or fields with 0, when a Suppress zeroes-checkbox in
      report-dialog is checked.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:4-JUL-2016 PIM-197
    - Addition of Team-selection.
    MRA:14-SEP-2016 PIM-197 Rework
    - Addition of Team-selection
    - When using compare by planning hours or available hours the
      dialog gave a wrong layout because of the added team-selection.
*)

unit DialogReportCompHoursFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib;

type
  TDialogReportCompHoursF = class(TDialogReportBaseF)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeekFrom: TdxSpinEdit;
    dxSpinEditWeekTo: TdxSpinEdit;
    GroupBox1: TGroupBox;
    CheckBoxShowTotal: TCheckBox;
    CheckBoxShowSelection: TCheckBox;
    RadioGroupStatusEmpl: TRadioGroup;
    QueryEmpl: TQuery;
    CheckBoxShowOnlyDeviations: TCheckBox;
    RadioGroupCompareWith: TRadioGroup;
    lblPeriod: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DateFrom: TDateTimePicker;
    Label6: TLabel;
    DateTo: TDateTimePicker;
    CheckBoxExport: TCheckBox;
    CheckBoxSuppressZeroes: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure dxDBSpinEditYearChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxSpinEditWeekFromChange(Sender: TObject);
    procedure dxSpinEditWeekToChange(Sender: TObject);
    procedure dxSpinEditYearChange(Sender: TObject);
    procedure RadioGroupCompareWithClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
    procedure ShowPeriod;
    procedure SwitchDatePeriod;
  public
    { Public declarations }
  end;

var
  DialogReportCompHoursF: TDialogReportCompHoursF;

// RV089.1.
function DialogReportCompHoursForm: TDialogReportCompHoursF;

implementation

{$R *.DFM}
uses
  SystemDMT, ListProcsFRM, ReportCompHoursQRPT, ReportCompHoursDMT,
  ReportBaseDMT, UPimsMessageRes;

// RV089.1.
var
  DialogReportCompHoursF_HND: TDialogReportCompHoursF;

// RV089.1.
function DialogReportCompHoursForm: TDialogReportCompHoursF;
begin
  if (DialogReportCompHoursF_HND = nil) then
  begin
    DialogReportCompHoursF_HND := TDialogReportCompHoursF.Create(Application);
    with DialogReportCompHoursF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportCompHoursF_HND;
end;

// RV089.1.
procedure TDialogReportCompHoursF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportCompHoursF_HND <> nil) then
  begin
    DialogReportCompHoursF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportCompHoursF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportCompHoursF.btnOkClick(Sender: TObject);
begin
  inherited;
  if RadioGroupCompareWith.ItemIndex = 0 then
  begin
    if dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value then
    begin
      DisplayMessage(SPimsWeekStartEnd, mtInformation, [mbYes]);
      Exit;
    end;
  end
  else
  begin
    if DateFrom.DateTime > DateTo.DateTime then
    begin
      DisplayMessage(SPimsStartEndDate, mtInformation, [mbYes]);
      Exit;
    end;
  end;
  if ReportCompHoursQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value),
      Round(dxSpinEditWeekTo.Value),
      DateFrom.DateTime,
      DateTo.DateTime,
      RadioGroupStatusEmpl.ItemIndex,
      RadioGroupCompareWith.ItemIndex,
      CheckBoxShowTotal.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxShowOnlyDeviations.Checked,
      CheckBoxSuppressZeroes.Checked,
      CheckBoxAllTeams.Checked,
      CheckBoxExport.Checked)
  then
    ReportCompHoursQR.ProcessRecords;
end;

procedure TDialogReportCompHoursF.dxDBSpinEditYearChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportCompHoursF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  ListProcsF.WeekUitDat(Now, Year, Week);
  DateFrom.DateTime := Now();
  DateTo.DateTime := Now();
  dxSpinEditYear.Value :=  Year;
  dxSpinEditWeekFrom.Value := Week;
  dxSpinEditWeekTo.Value := Week;
  dxSpinEditWeekFrom.MaxValue := ListProcsF.WeeksInYear(Round(Year));
  dxSpinEditWeekTo.MaxValue := ListProcsF.WeeksInYear(Round(Year));
  CheckBoxShowSelection.Checked := True;
  CheckBoxShowTotal.Checked := True;
  CheckBoxShowOnlyDeviations.Checked := False;
  RadioGroupStatusEmpl.ItemIndex := 0;
  SwitchDatePeriod;
  ChangeDate(Sender);
  ShowPeriod;
  InitDialog(True, False, True, True, False, False, False);
  inherited;
end;

procedure TDialogReportCompHoursF.FormCreate(Sender: TObject);
begin
  inherited;
  CheckBoxShowSelection.Checked := True;
  CheckBoxShowTotal.Checked := True;
  ReportCompHoursDM := CreateReportDM(TReportCompHoursDM);
  ReportCompHoursQR := CreateReportQR(TReportCompHoursQR);
end;

procedure TDialogReportCompHoursF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekTo.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

procedure TDialogReportCompHoursF.dxSpinEditWeekFromChange(
  Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportCompHoursF.dxSpinEditWeekToChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportCompHoursF.dxSpinEditYearChange(Sender: TObject);
begin
  inherited;
  ChangeDate(Sender);
  ShowPeriod;
end;

procedure TDialogReportCompHoursF.SwitchDatePeriod;
begin
  if RadioGroupCompareWith.ItemIndex = 0 then
  begin
    Label4.Visible := False; // From
    Label5.Visible := False; // Date
    DateFrom.Visible := False;
    DateTo.Visible := False;
    Label6.Visible := False; // to

    Label1.Visible := True; // Year
    dxSpinEditYear.Visible := True;
    Label2.Visible := True; // From Week
    dxSpinEditWeekFrom.Visible := True;
    dxSpinEditWeekTo.Visible := True;
    Label3.Visible := True; // to
    lblPeriod.Visible := True;
  end
  else
  begin
    Label4.Visible := True; // From
    Label5.Visible := True; // Date
    DateFrom.Visible := True;
    DateTo.Visible := True;
    Label6.Visible := True; // to

    Label1.Visible := False; // Year
    dxSpinEditYear.Visible := False;
    Label2.Visible := False; // From Week
    dxSpinEditWeekFrom.Visible := False;
    dxSpinEditWeekTo.Visible := False;
    Label3.Visible := False; // to
    lblPeriod.Visible := False;
  end;
end;

procedure TDialogReportCompHoursF.RadioGroupCompareWithClick(
  Sender: TObject);
begin
  inherited;
  SwitchDatePeriod;
end;

// MR:05-12-2003
procedure TDialogReportCompHoursF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportCompHoursF.CreateParams(var params: TCreateParams);
begin
  inherited;
  if (DialogReportCompHoursF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
