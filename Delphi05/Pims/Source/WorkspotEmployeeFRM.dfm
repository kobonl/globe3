inherited WorkspotEmployeeF: TWorkspotEmployeeF
  Left = 260
  Top = 171
  Width = 646
  Height = 445
  Caption = 'Workspots per employee'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 638
    Align = alClient
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 636
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 636
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Employees'
        end>
      KeyField = 'EMPLOYEE_NUMBER'
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      ShowBands = True
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Number'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_NUMBER'
      end
      object dxMasterGridColumnName: TdxDBGridColumn
        Caption = 'Name'
        Width = 174
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnDepartment: TdxDBGridLookupColumn
        Caption = 'Department'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPARTMENTLU'
        ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
      end
      object dxMasterGridColumnPlant: TdxDBGridLookupColumn
        Caption = 'Plant'
        Width = 124
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
        ListFieldName = 'DESCRIPTION;PLANT_CODE'
      end
      object dxMasterGridColumnSecurityNumber: TdxDBGridColumn
        Caption = 'Social security number'
        Width = 124
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SOCIAL_SECURITY_NUMBER'
      end
      object dxMasterGridColumnAddress: TdxDBGridColumn
        Caption = 'Address'
        Width = 244
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ADDRESS'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 292
    Width = 638
    Height = 126
    TabOrder = 3
    object LabelWorkspot: TLabel
      Left = 316
      Top = 78
      Width = 46
      Height = 13
      Caption = 'Workspot'
    end
    object LabelStartDate: TLabel
      Left = 44
      Top = 56
      Width = 49
      Height = 13
      Caption = 'Start date'
    end
    object Label1: TLabel
      Left = 44
      Top = 16
      Width = 25
      Height = 13
      Caption = 'Level'
    end
    object Label2: TLabel
      Left = 316
      Top = 16
      Width = 24
      Height = 13
      Caption = 'Plant'
    end
    object Label3: TLabel
      Left = 316
      Top = 48
      Width = 57
      Height = 13
      Caption = 'Department'
    end
    object DBEditDescription: TDBEdit
      Tag = 1
      Left = 104
      Top = 12
      Width = 45
      Height = 19
      Ctl3D = False
      DataField = 'EMPLOYEE_LEVEL'
      DataSource = WorkspotEmployeeDM.DataSourceDetail
      ParentCtl3D = False
      TabOrder = 0
      OnKeyPress = DBEditDescriptionKeyPress
    end
    object DBPicker1: TDBPicker
      Tag = 1
      Left = 104
      Top = 54
      Width = 97
      Height = 21
      CalAlignment = dtaLeft
      Date = 37911.4813598032
      Time = 37911.4813598032
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 1
      DataField = 'STARTDATE_LEVEL'
      DataSource = WorkspotEmployeeDM.DataSourceDetail
    end
    object dxDBLookupEditWK: TdxDBLookupEdit
      Tag = 1
      Left = 384
      Top = 78
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 4
      DataField = 'WORKSPOTLU'
      DataSource = WorkspotEmployeeDM.DataSourceDetail
      OnCloseUp = dxDBLookupEditWKCloseUp
      DropDownRows = 3
      ListFieldName = 'DESCRIPTION;WORKSPOT_CODE'
    end
    object dxDBLookupEditDept: TdxDBLookupEdit
      Tag = 1
      Left = 384
      Top = 44
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 3
      DataField = 'DEPTLU'
      DataSource = WorkspotEmployeeDM.DataSourceDetail
      OnSelectionChange = dxDBLookupEditDeptSelectionChange
      OnCloseUp = dxDBLookupEditDeptCloseUp
      DropDownRows = 6
      ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
    end
    object dxDBLookupEditPlant: TdxDBLookupEdit
      Tag = 1
      Left = 384
      Top = 12
      Width = 233
      Style.BorderStyle = xbsSingle
      TabOrder = 2
      DataField = 'PLANTLU'
      DataSource = WorkspotEmployeeDM.DataSourceDetail
      OnSelectionChange = dxDBLookupEditDeptSelectionChange
      OnCloseUp = dxDBLookupEditPlantCloseUp
      ListFieldName = 'DESCRIPTION;PLANT_CODE'
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 638
    Height = 137
    Align = alBottom
    inherited spltDetail: TSplitter
      Top = 133
      Width = 636
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 636
      Height = 132
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Workspots per employee'
          Width = 716
        end>
      KeyField = 'WORKSPOT_CODE'
      OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoMouseScroll, edgoShowHourGlass, edgoTabThrough, edgoVertThrough]
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumnPlant: TdxDBGridLookupColumn
        Caption = 'Plant'
        DisableEditor = True
        Width = 56
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PLANTLU'
      end
      object dxDetailGridColumnWorkspot: TdxDBGridColumn
        Caption = 'Workspot'
        DisableEditor = True
        Width = 207
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WKDESC'
      end
      object dxDetailGridColumnDept: TdxDBGridLookupColumn
        Caption = 'Department'
        DisableEditor = True
        Width = 157
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DEPTDESC'
        ListFieldName = 'DESCRIPTION;DEPARTMENT_CODE'
      end
      object dxDetailGridColumnLevel: TdxDBGridColumn
        Caption = 'Level'
        DisableEditor = True
        Width = 48
        BandIndex = 0
        RowIndex = 0
        FieldName = 'EMPLOYEE_LEVEL'
      end
      object dxDetailGridColumnStartLevel: TdxDBGridDateColumn
        Caption = 'Start date level'
        DisableEditor = True
        Width = 156
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTDATE_LEVEL'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      Glyph.Data = {
        96070000424D9607000000000000D60000002800000018000000180000000100
        180000000000C006000000000000000000001400000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A600F0FBFF00A4A0A000808080000000FF0000FF000000FFFF00FF000000FF00
        FF00FFFF0000FFFFFF00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C600000099FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99FF33000000C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000000000FFFFFF99FFCC99CC33
        99FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6000000FFFFFF99FFCC99
        FF3366993366993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6669933FFFF
        FF99FFCC99FF3399CC33669933C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C666993399FFCC99FF33669933669933C6C3C6C6C3C666993399CC3399FF
        33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C666993399FF3399CC33669933C6C3C6C6C3C6C6C3C6
        C6C3C666993399CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33669933C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399FF33000000000000C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399
        FF3399FF33000000000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6669933C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C666993399CC3399FF3399FF3399FF33000000000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC3399CC3399FF3399FF330000
        00C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33
        99CC3399FF33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C666993399CC3399CC33000000C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C666993399CC33000000C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C66699336699
        33C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6
        C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6
        C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3C6C6C3
        C6C6C3C6C6C3C6C6C3C6}
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dsrcActive: TDataSource
    Left = 8
  end
end
