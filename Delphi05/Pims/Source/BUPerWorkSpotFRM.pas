unit BUPerWorkSpotFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEdLib, dxDBELib, dxEditor, dxExEdtr, DBCtrls,
  StdCtrls, Mask, dxDBTLCl, dxGrClms;

type
  TBUPerWorkSpotF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridLookupColumn;
    dxMasterGridColumn4: TdxDBGridLookupColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn5: TdxDBGridCheckColumn;
    dxMasterGridColumn6: TdxDBGridCheckColumn;
    dxMasterGridColumn7: TdxDBGridCheckColumn;
    dxMasterGridColumn8: TdxDBGridDateColumn;
    dxMasterGridColumn9: TdxDBGridCheckColumn;
    dxMasterGridColumn10: TdxDBGridCheckColumn;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    DBLookupComboBoxBusinessUnit: TDBLookupComboBox;
    dxDetailGridColumn1: TdxDBGridLookupColumn;
    dxMasterGridColumn11: TdxDBGridCheckColumn;
    DBEditPercentage: TDBEdit;
    dxMasterGridColumn12: TdxDBGridLookupColumn;
    Label8: TLabel;
    Label9: TLabel;
    DBEditPlant: TDBEdit;
    DBEditWK: TDBEdit;
    DBEditPlantDesc: TDBEdit;
    DBEditWKDesc: TDBEdit;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDetailGridClick(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
    FPlant,
    FWKCode: String;
  end;

function BUPerWorkSpotF: TBUPerWorkSpotF;

implementation

{$R *.DFM}

uses
  SystemDMT, WorkSpotDMT, UPimsMessageRes, WorkSpotFRM;

var
  BUPerWorkSpotF_HDN: TBUPerWorkSpotF;

function BUPerWorkSpotF: TBUPerWorkSpotF;
begin
  if (BUPerWorkSpotF_HDN = nil) then
  begin
    BUPerWorkSpotF_HDN := TBUPerWorkSpotF.Create(Application);
  end;
  Result := BUPerWorkSpotF_HDN;
end;

procedure TBUPerWorkSpotF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBLookupComboBoxBusinessUnit.SetFocus;
end;

procedure TBUPerWorkSpotF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBLookupComboBoxBusinessUnit.SetFocus;
end;

procedure TBUPerWorkSpotF.FormDestroy(Sender: TObject);
begin
  inherited;
  BUPerWorkSpotF_HDN := Nil;
end;

procedure TBUPerWorkSpotF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SPimsSaveBefore, mtError, [mbOk]);
    Abort;
  end;
  if (dxDetailGrid.Items[0].Values[0] = NULL ) then
  begin
    inherited;
    DisplayMessage(SPimsBUPerWorkspotPercentages, mtError, [mbOk]);
    Abort;
  end;
   FPlant := DBEditPlant.Text;
   FWKCode := DBEditWK.Text;
   if (WorkspotDM.GetSumPercentage(FPlant,FWKCode) <> 100) then
    begin
      DisplayMessage(SPimsBUPerWorkspotPercentages, mtError, [mbOk]);
      Abort
    end;
  inherited;
  BUPerWorkSpotF_HDN := Nil;
 
  WorkspotF.SetGridForm(WorkspotF);
end;

procedure TBUPerWorkSpotF.dxDetailGridClick(Sender: TObject);
begin
//  inherited;
 
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TBUPerWorkSpotF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
