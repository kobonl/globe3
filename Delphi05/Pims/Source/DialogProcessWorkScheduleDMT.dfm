inherited DialogProcessWorkScheduleDM: TDialogProcessWorkScheduleDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object Query1: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 56
    Top = 16
  end
  object qryWorkScheduleDetails: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  W.STARTDATE, WL.*'
      'FROM '
      '  WORKSCHEDULE W INNER JOIN WORKSCHEDULELINE WL ON'
      '    W.WORKSCHEDULE_ID = WL.WORKSCHEDULE_ID'
      '  INNER JOIN EMPLOYEECONTRACT EC ON'
      '    WL.WORKSCHEDULE_ID = EC.WORKSCHEDULE_ID'
      'WHERE '
      '  W.STARTDATE BETWEEN EC.STARTDATE AND EC.ENDDATE AND'
      '  EC.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER  '
      'ORDER BY'
      '  WL.WORKSCHEDULE_ID, WL.WORKSCHEDULELINE_ID  ')
    Left = 56
    Top = 88
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end>
  end
end
