inherited DialogReportInOutAbsenceOvertimeF: TDialogReportInOutAbsenceOvertimeF
  Caption = 'Report In/Outscan, Absence, Overtime'
  ClientHeight = 305
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 0
  end
  inherited pnlInsertBase: TPanel
    Height = 184
    TabOrder = 3
    inherited LblFromEmployee: TLabel
      Top = 66
    end
    inherited LblEmployee: TLabel
      Top = 66
    end
    inherited LblToEmployee: TLabel
      Top = 66
    end
    inherited LblStarEmployeeFrom: TLabel
      Top = 68
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 68
    end
    inherited LblStarTeamTo: TLabel
      Top = 44
    end
    inherited LblToTeam: TLabel
      Top = 41
    end
    inherited LblStarTeamFrom: TLabel
      Top = 44
    end
    inherited LblTeam: TLabel
      Top = 41
    end
    inherited LblFromTeam: TLabel
      Top = 41
    end
    inherited LblFromDateTime: TLabel
      Top = 90
    end
    inherited LblDateTime: TLabel
      Top = 90
    end
    inherited LblToDateTime: TLabel
      Top = 90
    end
    inherited lblFromDateBase: TLabel
      Top = 90
    end
    inherited lblDateBase: TLabel
      Top = 90
    end
    inherited lblToDateBase: TLabel
      Top = 90
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 165
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      ColCount = 166
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 41
      ColCount = 167
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Top = 41
      ColCount = 168
    end
    inherited CheckBoxAllTeams: TCheckBox
      Top = 44
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      ColCount = 166
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      ColCount = 167
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Top = 65
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Top = 65
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 167
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 168
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Top = 66
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 166
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 167
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 167
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 168
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 168
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 167
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 88
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Top = 88
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 88
    end
    inherited DatePickerTo: TDateTimePicker
      Top = 88
    end
    inherited DatePickerFromBase: TDateTimePicker
      Top = 88
    end
    inherited DatePickerToBase: TDateTimePicker
      Top = 88
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 114
      Width = 635
      Height = 70
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 29
      object CBoxShowSelection: TCheckBox
        Left = 10
        Top = 15
        Width = 183
        Height = 17
        Caption = 'Show selection'
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 10
        Top = 41
        Width = 207
        Height = 17
        Caption = 'Export'
        TabOrder = 1
      end
      object CheckBoxNewPagePerTeam: TCheckBox
        Left = 314
        Top = 15
        Width = 295
        Height = 17
        Caption = 'New Page Per Team'
        TabOrder = 2
      end
      object CheckBoxNewPagePerDate: TCheckBox
        Left = 314
        Top = 41
        Width = 295
        Height = 17
        Caption = 'New Page Per Date'
        TabOrder = 3
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 286
  end
  inherited pnlBottom: TPanel
    Top = 245
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        54040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507344469616C6F675265706F7274496E4F7574416273656E63
        654F76657274696D65462E44617461536F75726365456D706C46726F6D104F70
        74696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E65
        64676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E671065
        64676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E670009
        4F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F
        43616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E
        617669676174696F6E116564676F436F6E6669726D44656C657465126564676F
        4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300
        000F546478444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743
        617074696F6E06064E756D62657206536F727465640704637355700557696474
        6802410942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F454D504C4F5945455F4E554D42455200000F5464784442477269
        64436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E06
        0A53686F7274206E616D6505576964746802540942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060A53484F52545F4E414D45
        00000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D65074361
        7074696F6E06044E616D6505576964746803B4000942616E64496E6465780200
        08526F77496E6465780200094669656C644E616D65060B444553435249505449
        4F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E4164647265
        73730743617074696F6E06074164647265737305576964746802450942616E64
        496E646578020008526F77496E6465780200094669656C644E616D6506074144
        445245535300000F546478444247726964436F6C756D6E0E436F6C756D6E4465
        7074436F64650743617074696F6E060F4465706172746D656E7420636F646505
        576964746802580942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F4445504152544D454E545F434F444500000F54647844
        4247726964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609
        5465616D20636F64650942616E64496E646578020008526F77496E6465780200
        094669656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        1B040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507324469616C6F675265706F72
        74496E4F7574416273656E63654F76657274696D65462E44617461536F757263
        65456D706C546F104F7074696F6E73437573746F6D697A650B0E6564676F4261
        6E644D6F76696E670E6564676F42616E6453697A696E67106564676F436F6C75
        6D6E4D6F76696E67106564676F436F6C756D6E53697A696E670E6564676F4675
        6C6C53697A696E6700094F7074696F6E7344420B106564676F43616E63656C4F
        6E457869740D6564676F43616E44656C6574650D6564676F43616E496E736572
        74116564676F43616E4E617669676174696F6E116564676F436F6E6669726D44
        656C657465126564676F4C6F6164416C6C5265636F726473106564676F557365
        426F6F6B6D61726B7300000F546478444247726964436F6C756D6E0A436F6C75
        6D6E456D706C0743617074696F6E06064E756D62657206536F72746564070463
        73557005576964746802310942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060F454D504C4F5945455F4E554D42455200000F
        546478444247726964436F6C756D6E0F436F6C756D6E53686F72744E616D6507
        43617074696F6E060A53686F7274206E616D65055769647468024E0942616E64
        496E646578020008526F77496E6465780200094669656C644E616D65060A5348
        4F52545F4E414D4500000F546478444247726964436F6C756D6E11436F6C756D
        6E4465736372697074696F6E0743617074696F6E06044E616D65055769647468
        03CC000942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060B4445534352495054494F4E00000F546478444247726964436F6C
        756D6E0D436F6C756D6E416464726573730743617074696F6E06074164647265
        737305576964746802650942616E64496E646578020008526F77496E64657802
        00094669656C644E616D6506074144445245535300000F546478444247726964
        436F6C756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F44
        65706172746D656E7420636F64650942616E64496E646578020008526F77496E
        6465780200094669656C644E616D65060F4445504152544D454E545F434F4445
        00000F546478444247726964436F6C756D6E0A436F6C756D6E5465616D074361
        7074696F6E06095465616D20636F64650942616E64496E646578020008526F77
        496E6465780200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
end
