inherited ReportEmployeePaylistDM: TReportEmployeePaylistDM
  OldCreateOrder = True
  Left = 226
  Top = 175
  Height = 479
  Width = 742
  object tblTaylorMade: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'TAYLORMADE'
    Left = 40
    Top = 16
    object tblTaylorMadeQUALITY_INCENTIVE_YN: TStringField
      FieldName = 'QUALITY_INCENTIVE_YN'
      Size = 1
    end
  end
  object qryOvertimeDefinition: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  O.CONTRACTGROUP_CODE,'
      '  O.LINE_NUMBER,'
      '  O.HOURTYPE_NUMBER,'
      '  H.BONUS_PERCENTAGE,'
      '  H.OVERTIME_YN,'
      '  H.DESCRIPTION'
      'FROM'
      '  OVERTIMEDEFINITION O,'
      '  HOURTYPE H'
      'WHERE'
      '  O.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  O.CONTRACTGROUP_CODE = :CONTRACTGROUP_CODE AND'
      '  H.OVERTIME_YN = '#39'Y'#39
      '  ')
    Left = 632
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'CONTRACTGROUP_CODE'
        ParamType = ptUnknown
      end>
  end
  object tblHourtype: TTable
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    TableName = 'HOURTYPE'
    Left = 152
    Top = 192
  end
  object qryEmployeeContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  EMPLOYEE_NUMBER,'
      '  STARTDATE,'
      '  ENDDATE,'
      '  HOURLY_WAGE,'
      '  CONTRACT_TYPE'
      'FROM'
      '  EMPLOYEECONTRACT'
      'WHERE'
      '  EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  STARTDATE <= :DATEFROM AND'
      '  ENDDATE >= :DATETO'
      '  ')
    Left = 152
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object tblContractgroup: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'CONTRACTGROUP'
    Left = 152
    Top = 80
  end
  object qrySalHrPerEmp: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  S.EMPLOYEE_NUMBER,'
      '  E.CONTRACTGROUP_CODE,'
      '  E.DESCRIPTION,'
      '  TO_CHAR(S.HOURTYPE_NUMBER) HOURTYPE_NUMBER,'
      '  H.DESCRIPTION HDESCRIPTION,'
      '  SUM(S.SALARY_MINUTE) AS SALARY_MINUTE,'
      '  1 TYPE'
      'FROM'
      '  SALARYHOURPEREMPLOYEE S, EMPLOYEE E, HOURTYPE H'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  S.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER AND'
      '  E.PLANT_CODE >= :PLANTFROM AND E.PLANT_CODE <= :PLANTTO AND'
      '  E.PLANT_CODE IN'
      '    (SELECT B.PLANT_CODE FROM BUSINESSUNIT B WHERE'
      
        '     B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND  B.BUSINESSUNI' +
        'T_CODE <= :BUSINESSUNITTO)'
      '  AND'
      '  S.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  S.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  S.SALARY_DATE >= :DATEFROM AND'
      '  S.SALARY_DATE <= :DATETO AND'
      '  S.SALARY_MINUTE <> 0'
      'GROUP BY'
      '  S.EMPLOYEE_NUMBER,'
      '  E.CONTRACTGROUP_CODE,'
      '  E.DESCRIPTION,'
      '  S.HOURTYPE_NUMBER,'
      '  H.DESCRIPTION'
      'UNION ALL'
      'SELECT'
      '  EP.EMPLOYEE_NUMBER,'
      '  E.CONTRACTGROUP_CODE,'
      '  E.DESCRIPTION,'
      '  EP.PAYMENT_EXPORT_CODE,'
      '  PC.DESCRIPTION,'
      '  SUM(EP.PAYMENT_AMOUNT),'
      '  2'
      'FROM EXTRAPAYMENT EP INNER JOIN PAYMENTEXPORTCODE PC ON'
      '  EP.PAYMENT_EXPORT_CODE = PC.PAYMENT_EXPORT_CODE'
      '  INNER JOIN EMPLOYEE E ON'
      '  EP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER'
      'WHERE'
      '  E.PLANT_CODE >= :PLANTFROM AND E.PLANT_CODE <= :PLANTTO AND'
      '  E.PLANT_CODE IN'
      '    (SELECT B.PLANT_CODE FROM BUSINESSUNIT B WHERE'
      
        '     B.BUSINESSUNIT_CODE >= :BUSINESSUNITFROM AND  B.BUSINESSUNI' +
        'T_CODE <= :BUSINESSUNITTO) AND'
      '  EP.EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  EP.EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  EP.PAYMENT_DATE >= :DATEFROM AND'
      '  EP.PAYMENT_DATE <= :DATETO'
      'GROUP BY'
      '  EP.EMPLOYEE_NUMBER, '
      '  E.CONTRACTGROUP_CODE,'
      '  E.DESCRIPTION,'
      '  EP.PAYMENT_EXPORT_CODE,'
      '  PC.DESCRIPTION'
      'ORDER BY'
      '  1, 7, 4'
      ''
      '')
    Left = 40
    Top = 80
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'BUSINESSUNITTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryBonusPerHour: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  Q.MISTAKE, Q.BONUS_PER_HOUR '
      'FROM '
      '  QUALITYINCENTIVECONF Q'
      'WHERE '
      '  Q.MISTAKE >= :MISTAKE AND'
      '  ROWNUM = 1'
      'ORDER BY  '
      '  Q.MISTAKE')
    Left = 284
    Top = 80
    ParamData = <
      item
        DataType = ftInteger
        Name = 'MISTAKE'
        ParamType = ptUnknown
      end>
  end
  object qryMistakesPerEmp: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER,'
      '  SUM(NUMBER_OF_MISTAKE) AS SUM_MISTAKES'
      'FROM '
      '  MISTAKEPEREMPLOYEE'
      'WHERE '
      '  EMPLOYEE_NUMBER >= :EMPLOYEEFROM AND'
      '  EMPLOYEE_NUMBER <= :EMPLOYEETO AND'
      '  MISTAKE_DATE >= :DATEFROM AND '
      '  MISTAKE_DATE <= :DATETO AND'
      '  NUMBER_OF_MISTAKE IS NOT NULL'
      'GROUP BY'
      '  EMPLOYEE_NUMBER')
    Left = 288
    Top = 136
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'EMPLOYEETO'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object QueryProdHour: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE, '
      '  P.SHIFT_NUMBER, '
      '  W.DEPARTMENT_CODE, '
      '  P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER,'
      '  P.PRODHOUREMPLOYEE_DATE ,'
      '  SUM(P.PRODUCTION_MINUTE)  AS SUMMIN '
      'FROM '
      '  PRODHOURPEREMPLOYEE P , WORKSPOT W,'
      '  DEPARTMENT D'
      'WHERE'
      '  P.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND'
      '  P.PRODHOUREMPLOYEE_DATE <= :FENDDATE '
      'GROUP BY '
      '  P.PLANT_CODE,  D.BUSINESSUNIT_CODE, '
      '  P.SHIFT_NUMBER, '
      '  W.DEPARTMENT_CODE,  '
      '  P.WORKSPOT_CODE, P.JOB_CODE, P.EMPLOYEE_NUMBER, '
      '  P.PRODHOUREMPLOYEE_DATE ')
    Left = 384
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object QueryPQ: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE, WORKSPOT_CODE, JOB_CODE,'
      '  START_DATE, END_DATE,'
      '  QUANTITY'
      'FROM'
      '  PRODUCTIONQUANTITY'
      'WHERE'
      '  QUANTITY > 0'
      '  AND START_DATE >= :FSTARTDATE AND START_DATE < :FENDDATE'
      '  AND END_DATE >= :FSTARTDATE AND END_DATE < :FENDDATE AND'
      '  SHIFT_NUMBER >= :FSHIFTFROM AND SHIFT_NUMBER <= :FSHIFTTO '
      '  AND PLANT_CODE = :PLANT_CODE'
      '  AND WORKSPOT_CODE = :WORKSPOT_CODE'
      '  AND JOB_CODE = :JOB_CODE '
      'ORDER BY'
      '  START_DATE, END_DATE')
    Left = 384
    Top = 136
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FSHIFTFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'FSHIFTTO'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'JOB_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryTRSCNT: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 384
    Top = 192
  end
  object QueryTRS: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 384
    Top = 80
  end
  object AQuery: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    Left = 384
    Top = 256
  end
  object TableWK: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'WORKSPOT'
    Left = 464
    Top = 24
  end
  object TableBU: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'BUSINESSUNIT'
    Left = 464
    Top = 136
  end
  object TableJob: TTable
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    TableName = 'JOBCODE'
    Left = 464
    Top = 80
  end
  object qryEmployeeExtraPayments: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EP.EMPLOYEE_NUMBER, '
      '  EP.PAYMENT_EXPORT_CODE, '
      '  PC.DESCRIPTION,'
      '  EP.PAYMENT_DATE,'
      '  EP.PAYMENT_AMOUNT'
      'FROM EXTRAPAYMENT EP INNER JOIN PAYMENTEXPORTCODE PC ON'
      '  EP.PAYMENT_EXPORT_CODE = PC.PAYMENT_EXPORT_CODE'
      'WHERE '
      '  EP.EMPLOYEE_NUMBER = :EMPLOYEE_NUMBER AND'
      '  EP.PAYMENT_DATE >= :DATEFROM AND EP.PAYMENT_DATE <= :DATETO'
      'ORDER BY'
      '  EP.EMPLOYEE_NUMBER, '
      '  EP.PAYMENT_EXPORT_CODE,'
      '  EP.PAYMENT_DATE')
    Left = 152
    Top = 248
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE_NUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
end
