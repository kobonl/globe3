(*
  Changes:
    RV001: (Revision 001)
    2.0.139.122 - QueryMaster-Select was wrong.
                  Department was only linked by department_code,
                  not by plant_code, resulting in double lines for some
                  of the employees.
  MRA:22-FEB-2010. RV054.8. 889963.
  - When making first contract, take startdate of employees
    as employeecontract-startdate.
  - When making next contract, take enddate+1 of previous contract
    as employeecontract-startdate.
  SO:07-JUN-2010. RV065.6.
  - added properties to get the data from the two different hours and minutes values
  and store it in the detail table
  SO:19-JUL-2010 RV067.4. 550497
  - added new fields SENIORITY_HOURS, WORKER_YN for Belgium and a function to check if
  the country for the selected employee is Belgium
  MRA:1-OCT-2010 RV071.2.
  - Test for employee's plant belonging to country 'BEL' is not always OK.
  - Changed the query for QueryMaster.
  - Performance-issue. ClientDataSetMaster is not used anymore!
  - Default for WORKER_YN must be 'Y'.
  MRA:27-APR-2011. RV091.3.
  - Use new function 'SystemDM.FutureDate' to set a fixed futuredate.
  MRA:5-NOV-2015 PIM-52
  - Work Schedule Functionality
*)

unit EmployeeContractsDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables, Provider, DBClient;

type
  TEmployeeContractsDM = class(TGridBaseDM)
    TableMasterEMPLOYEE_NUMBER: TIntegerField;
    TableMasterSHORT_NAME: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterDEPARTMENT_CODE: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterADDRESS: TStringField;
    TableMasterZIPCODE: TStringField;
    TableMasterCITY: TStringField;
    TableMasterSTATE: TStringField;
    TableMasterLANGUAGE_CODE: TStringField;
    TableMasterPHONE_NUMBER: TStringField;
    TableMasterEMAIL_ADDRESS: TStringField;
    TableMasterDATE_OF_BIRTH: TDateTimeField;
    TableMasterSEX: TStringField;
    TableMasterSOCIAL_SECURITY_NUMBER: TStringField;
    TableMasterSTARTDATE: TDateTimeField;
    TableMasterTEAM_CODE: TStringField;
    TableMasterENDDATE: TDateTimeField;
    TableMasterCONTRACTGROUP_CODE: TStringField;
    TableMasterIS_SCANNING_YN: TStringField;
    TableMasterCUT_OF_TIME_YN: TStringField;
    TableMasterREMARK: TStringField;
    TableMasterCALC_BONUS_YN: TStringField;
    TableMasterFIRSTAID_YN: TStringField;
    TableMasterFIRSTAID_EXP_DATE: TDateTimeField;
    TableDetailEMPLOYEE_NUMBER: TIntegerField;
    TableDetailSTARTDATE: TDateTimeField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailENDDATE: TDateTimeField;
    TableDetailHOURLY_WAGE: TFloatField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailCONTRACT_TYPE: TIntegerField;
    TableDetailCONTRACT_HOUR_PER_WEEK: TFloatField;
    TableDetailCONTRACT_DAY_PER_WEEK: TIntegerField;
    TableMasterPLANTLU: TStringField;
    TableMasterDEPARTMENTLU: TStringField;
    TableMasterSTARTDATECAL: TDateTimeField;
    TableMasterEMPCALC: TFloatField;
    QueryMaster: TQuery;
    QueryMasterFLOAT_EMP: TFloatField;
    QueryMasterEMPLOYEE_NUMBER: TIntegerField;
    QueryMasterDESCRIPTION: TStringField;
    QueryMasterSHORT_NAME: TStringField;
    QueryMasterPLANT_CODE: TStringField;
    QueryMasterPLANTLU: TStringField;
    QueryMasterDEPARTMENT_CODE: TStringField;
    QueryMasterDEPARTMENTLU: TStringField;
    QueryMasterSTARTDATE: TDateTimeField;
    ClientDataSetMaster: TClientDataSet;
    DataSetProviderMaster: TDataSetProvider;
    TableDetailGUARANTEED_DAYS: TIntegerField;
    TableDetailEXPORT_CODE: TStringField;
    TableDetailHOLIDAY_HOUR_PER_YEAR: TFloatField;
    qryCheckBI: TQuery;
    qryCheckBU: TQuery;
    qryLastEmployeeContract: TQuery;
    TableDetailSENIORITY_HOURS: TFloatField;
    TableDetailWORKER_YN: TStringField;
    QueryMasterCOUNTRYLU: TStringField;
    TableDetailWORKSCHEDULE_ID: TFloatField;
    qryWorkSchedule: TQuery;
    TableDetailLUWORKSCHEDULE: TStringField;
    procedure TableDetailNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure QueryMasterCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure TableDetailBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
    OldStartDate: TDateTime;
    //RV065.6.
    FHoursPerWeek: Double;
    FHolidayHours: Double;
    FSeniorityHours: Double;
    function DatesCheckBI(DataSet: TDataSet): Boolean;
    function DatesCheckBU(DataSet: TDataSet): Boolean;
  public
    { Public declarations }
    procedure FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    //RV065.6.
    property HolidayHours: Double read FHolidayHours write FHolidayHours;
    property HoursPerWeek: Double read FHoursPerWeek write FHoursPerWeek;
    //!!
    property SeniorityHours: Double read FSeniorityHours write FSeniorityHours;
    function IsBelgium: Boolean;
    function ConvertTime(ADataSet: TDataSet; AFieldName: String): String;
  end;

var
  EmployeeContractsDM: TEmployeeContractsDM;

implementation

uses SystemDMT, UPimsConst, UPimsMessageRes;

{$R *.DFM}

procedure TEmployeeContractsDM.TableDetailNewRecord(DataSet: TDataSet);
  // RV054.8.
  function DetermineStartDate: TDateTime;
  begin
    if TableDetail.RecordCount = 0 then // First employee-contract ?
      Result := QueryMasterSTARTDATE.AsDateTime
    else // next employee-contract
    begin
      // next employee-contract, get startdate from previous emp-contract + 1
      with qryLastEmployeeContract do
      begin
        Close;
        ParamByName('EMPLOYEE_NUMBER').AsInteger :=
          QueryMasterEMPLOYEE_NUMBER.AsInteger;
        Open;
        if not Eof then
          Result := FieldByName('ENDDATE').AsDateTime + 1
        else
          Result := Trunc(Now);
        Close;
      end;
    end;
  end;
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  // RV054.8.
  TableDetailSTARTDATE.AsDateTime := DetermineStartDate;
  TableDetailENDDATE.AsDateTime := SystemDM.FutureDate;
  // RV071.2.
  TableDetailWORKER_YN.AsString := 'Y';
end;

// MR:27-07-2005 Instead of triggers, check dates here.
function TEmployeeContractsDM.DatesCheckBI(DataSet: TDataSet): Boolean;
var
  NewStartDate, NewEndDate: TDateTime;
  StartDate, EndDate: TDateTime;
begin
  Result := True;
  NewStartDate := DataSet.FieldByName('STARTDATE').AsDateTime;
  NewEndDate := DataSet.FieldByName('ENDDATE').AsDateTime;
  if (NewStartDate > NewEndDate) then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbOK]);
    Result := False;
  end;
  if Result then
  begin
    qryCheckBI.Close;
    qryCheckBI.ParamByName('CURRENT_EMPLOYEE_NUMBER').AsInteger :=
      DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryCheckBI.Open;
    if not qryCheckBI.IsEmpty then
    begin
      qryCheckBI.First;
      while not qryCheckBI.Eof do
      begin
        StartDate := qryCheckBI.FieldByName('STARTDATE').AsDateTime;
        EndDate := qryCheckBI.FieldByName('ENDDATE').AsDateTime;
        if (((NewStartDate <= StartDate) and
          (NewEndDate >= StartDate)) or
          ((NewStartDate >= StartDate) and
          (NewStartDate <= EndDate))) then
        begin
          DisplayMessage(SPimsEmplContractInDatePrevious,
            mtInformation, [mbOK]);
          Result := False;
          Break;
        end;
        qryCheckBI.Next;
      end;
    end;
    qryCheckBI.Close;
  end;
end;

// MR:27-07-2005 Instead of triggers, check dates here.
function TEmployeeContractsDM.DatesCheckBU(DataSet: TDataSet): Boolean;
var
  NewStartDate, NewEndDate: TDateTime;
  StartDate, EndDate: TDateTime;
begin
  Result := True;
  NewStartDate := DataSet.FieldByName('STARTDATE').AsDateTime;
  NewEndDate := DataSet.FieldByName('ENDDATE').AsDateTime;
  if (NewStartDate > NewEndDate) then
  begin
    DisplayMessage(SPimsStartEndDate, mtInformation, [mbOK]);
    Result := False;
  end;
  if Result then
  begin
    qryCheckBU.Close;
    qryCheckBU.ParamByName('CURRENT_EMPLOYEE_NUMBER').AsInteger :=
      DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    qryCheckBU.ParamByName('OLD_STARTDATE').AsDateTime :=
      OldStartDate;
    qryCheckBU.Open;
    if not qryCheckBU.IsEmpty then
    begin
      qryCheckBU.First;
      while not qryCheckBU.Eof do
      begin
        StartDate := qryCheckBU.FieldByName('STARTDATE').AsDateTime;
        EndDate := qryCheckBU.FieldByName('ENDDATE').AsDateTime;
        if (((NewStartDate <= StartDate) and
          (NewEndDate >= StartDate)) or
          ((NewStartDate >= StartDate) and
          (NewStartDate <= EndDate))) then
        begin
          DisplayMessage(SPimsEmplContractInDatePrevious,
            mtInformation, [mbOK]);
          Result := False;
          Break;
        end;
        qryCheckBU.Next;
      end;
    end;
    qryCheckBU.Close;
  end;
end;

procedure TEmployeeContractsDM.TableDetailBeforePost(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  //RV065.6.
  TableDetail.FieldByName('HOLIDAY_HOUR_PER_YEAR').Value := FHolidayHours;
  TableDetail.FieldByName('CONTRACT_HOUR_PER_WEEK').Value := FHoursPerWeek;
  //!!
  if IsBelgium then
    TableDetail.FieldByName('SENIORITY_HOURS').Value := FSeniorityHours;

  if TableDetail.FieldByName('ENDDATE').Value <> Null then
    TableDetail.FieldByName('ENDDATE').AsDateTime :=
      Trunc(TableDetail.FieldByName('ENDDATE').AsDateTime);
  if TableDetail.FieldByName('STARTDATE').Value <> Null then
    TableDetail.FieldByName('STARTDATE').AsDateTime :=
      Trunc(TableDetail.FieldByName('STARTDATE').AsDateTime);
  // Record is inserted.      
  if DataSet.State = dsInsert then
  begin
    if not DatesCheckBI(DataSet) then
    begin
      DataSet.Cancel;
      SysUtils.Abort;
    end;
  end
  else
    // Record is updated
    if not DatesCheckBU(DataSet) then
    begin
      DataSet.Cancel;
      SysUtils.Abort;
    end;
end;

procedure TEmployeeContractsDM.QueryMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  // fill for export long employee number values
  DataSet.FieldByName('FLOAT_EMP').AsFloat :=
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TEmployeeContractsDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //CAR 550279 - team selection -replace TableMASTER
  // RV071.1. SQL-statement is now put in QueryMaster-component,
  //          with addition of country.
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
(*
    QueryMaster.Sql.Clear;
    QueryMaster.Sql.Add(
      'SELECT ' + NL +
      '  E.EMPLOYEE_NUMBER, E.SHORT_NAME, E.DESCRIPTION, ' + NL +
      '  E.PLANT_CODE, P.DESCRIPTION AS PLANTLU, ' + NL +
      '  E.STARTDATE, ' + NL +
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DEPARTMENTLU ' + NL +
      'FROM ' + NL +
      '  EMPLOYEE E INNER JOIN PLANT P ON ' + NL +
      '    E.PLANT_CODE = P.PLANT_CODE ' + NL +
      '  INNER JOIN DEPARTMENT D ON ' + NL +
      '    D.PLANT_CODE = E.PLANT_CODE AND ' + NL +
      '    D.DEPARTMENT_CODE = E.DEPARTMENT_CODE ' + NL +
      '  INNER JOIN TEAMPERUSER T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL +
      'WHERE ' + NL +
      '  T.USER_NAME = :USER_NAME ' + NL +
      'ORDER BY ' + NL +
      '  E.EMPLOYEE_NUMBER'
      );
*)
    QueryMaster.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    //set Filtered property only on the export form event - for the speed reason
    TableExport.OnFilterRecord := FilterRecord;
    TableExport.Filtered := True;
  end
  else
  begin
    QueryMaster.ParamByName('USER_NAME').AsString := '*';
    TableExport.Filtered := False;
  end;
  QueryMaster.Prepare;
  // RV071.2. Performance-issue. Do not open this here, this is only
  //          needed for export-function.
//  ClientDataSetMaster.Open;
//  ClientDataSetMaster.LogChanges := False;
end;

procedure TEmployeeContractsDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //550279 Car 18-12-2003
  QueryMaster.Close;
  QueryMaster.Unprepare;
//  ClientDataSetMaster.Close;
end;

//car 550279 : team selection rights  - for export table
procedure TEmployeeContractsDM.FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := QueryMaster.Locate('EMPLOYEE_NUMBER',
    DataSet.FieldByName('EMPLOYEE_NUMBER').AsString, []);
(*
  Accept := ClientDataSetMaster.FindKey([DataSet.
    FieldByName('EMPLOYEE_NUMBER').AsInteger]);
*)
end;

procedure TEmployeeContractsDM.TableDetailBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  OldStartDate := DataSet.FieldByName('STARTDATE').AsDateTime;
end;

//RV067.4.
function TEmployeeContractsDM.IsBelgium: Boolean;
//var
//  CountryCode, Sql: String;
begin
  // RV071.2. Country added to QueryMaster.
  if QueryMaster.Active and (not QueryMaster.Eof) then
    Result := QueryMasterCOUNTRYLU.Value = 'BEL'
  else
    Result := False;

(*
  Sql :=
    'SELECT NVL(CNT.CODE, ''*'') ' + NL +
    'FROM ' + NL +
    '  EMPLOYEE E ' + NL +
    '    INNER JOIN PLANT P ON ' + NL +
    '      E.PLANT_CODE = P.PLANT_CODE ' + NL +
    '    LEFT JOIN COUNTRY CNT ON ' + NL +
    '      P.COUNTRY_ID = CNT.COUNTRY_ID ' + NL +
    'WHERE E.EMPLOYEE_NUMBER = %s ';

  if QueryMaster.Active and (not QueryMaster.Eof) then
  begin
    Sql := Format(sql, [QueryMaster.FieldByName('EMPLOYEE_NUMBER').AsString]);
    CountryCode := SystemDM.GetDBValue(Sql, '*');
  end;
*)
(*
    ' select NVL(cnt.code, ''*'') ' +
    ' from ' +
    '   employeecontract c ' +
    ' inner join employee e on c.employee_number = e.employee_number ' +
    '   and c.employee_number = %s ' +
    ' inner join plant p on e.plant_code = p.plant_code ' +
    ' left join country cnt on p.country_id = cnt.country_id';

  if TableDetail.Active and (not TableDetail.Eof) then
  begin
    Sql := Format(sql, [TableDetail.FieldByName('EMPLOYEE_NUMBER').AsString]);
    CountryCode := SystemDM.GetDBValue(Sql, '*');
  end;
  Result := CountryCode = 'BEL';
*)
end;

// RV067.MRA.13. Bugfix.
function TEmployeeContractsDM.ConvertTime(ADataSet: TDataSet;
  AFieldName: String): String;
  function FillZero(Value: String; Len: Integer): String;
  begin
    Result := Trim(Value);
    while Length(Result) < Len do
      Result := '0' + Result;
  end;
begin
  try
    if ADataSet.FieldByName(AFieldName).AsString = '' then
      Result := ''
    else
      if ADataSet.FieldByName(AFieldName).AsFloat > 0 then
        Result :=
          IntToStr(Trunc(ADataSet.FieldByName(AFieldName).AsFloat)) +
          ':' +
          FillZero(
            IntToStr(
              Round(
                (ADataSet.FieldByName(AFieldName).AsFloat * 60) -
                (Trunc(ADataSet.FieldByName(AFieldName).AsFloat) * 60)
                )
              )
            , 2)
      else
        Result := '';
  except
    Result := '';
  end;
end; // ConvertTime()

end.
