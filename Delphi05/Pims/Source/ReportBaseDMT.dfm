object ReportBaseDM: TReportBaseDM
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 224
  Top = 277
  Height = 480
  Width = 696
  object qryPlantMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MIN(PLANT_CODE) AS MINPLANT, '
      '  MAX(PLANT_CODE) AS MAXPLANT'
      'FROM '
      '  PLANT')
    Left = 320
    Top = 16
  end
  object qryTeamMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MIN(TEAM_CODE) AS MINTEAM, '
      '  MAX(TEAM_CODE) AS MAXTEAM'
      'FROM '
      '  TEAM')
    Left = 320
    Top = 72
  end
  object qryEmplMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MIN(EMPLOYEE_NUMBER) AS MINEMPLOYEE, '
      '  MAX(EMPLOYEE_NUMBER) AS MAXEMPLOYEE'
      'FROM '
      '  EMPLOYEE')
    Left = 320
    Top = 128
  end
  object qryBUMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  MIN(BUSINESSUNIT_CODE) AS MINBU,'
      '  MAX(BUSINESSUNIT_CODE) AS MAXBU'
      'FROM'
      '  BUSINESSUNIT')
    Left = 320
    Top = 184
  end
  object qryWeekDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM WEEK'
      'WHERE COMPUTERNAME = :COMPUTERNAME'
      '')
    Left = 416
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end>
  end
  object qryWeekInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WEEK'
      '(COMPUTERNAME, YEARNUMBER, WEEKNUMBER, DATEFROM, DATETO)'
      'VALUES'
      '(:COMPUTERNAME, :YEARNUMBER, :WEEKNUMBER, :DATEFROM, :DATETO)'
      ''
      ''
      ' '
      ' '
      ' ')
    Left = 416
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'COMPUTERNAME'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'YEARNUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'WEEKNUMBER'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object qryWorkspotMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MIN(WORKSPOT_CODE) AS MINWORKSPOT,'
      '  MAX(WORKSPOT_CODE) AS MAXWORKSPOT'
      'FROM'
      '  WORKSPOT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      '  '
      ''
      ' ')
    Left = 184
    Top = 16
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object qryDepartmentMinMax: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  MIN(DEPARTMENT_CODE) AS MINDEPARTMENT,'
      '  MAX(DEPARTMENT_CODE) AS MAXDEPARTMENT'
      'FROM'
      '  DEPARTMENT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE'
      ' ')
    Left = 184
    Top = 72
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
