(*
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase-form.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogReportStaffPlanningCondFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, SystemDMT;

type
  TDialogReportStaffPlanningCondF = class(TDialogReportBaseF)
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    CheckBoxPagePlant: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    ComboBoxPlusShiftFrom: TComboBoxPlus;
    ComboBoxPlusShiftTo: TComboBoxPlus;
    QueryShift: TQuery;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    Label2: TLabel;
    Label4: TLabel;
    DateFrom: TDateTimePicker;
    Label26: TLabel;
    DateTo: TDateTimePicker;
    Label1: TLabel;
    Label3: TLabel;
    CheckBoxPageShift: TCheckBox;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxPageWK: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    rgrpShowEmployeeMode: TRadioGroup;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillShift;
{    procedure FillWorkSpot; }
    procedure DateFromChange(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftToCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure CreateParams(var params: TCreateParams); override;
  private

  public
    { Public declarations }
   
  end;

var
  DialogReportStaffPlanningCondF: TDialogReportStaffPlanningCondF;

// RV089.1.
function DialogReportStaffPlanningCondForm: TDialogReportStaffPlanningCondF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  ReportStaffPlanningCondDMT, ReportStaffPlanningCondQRPT,
  ListProcsFRM, CalculateTotalHoursDMT, UPimsMessageRes;

// RV089.1.
var
  DialogReportStaffPlanningCondF_HND: TDialogReportStaffPlanningCondF;

// RV089.1.
function DialogReportStaffPlanningCondForm: TDialogReportStaffPlanningCondF;
begin
  if (DialogReportStaffPlanningCondF_HND = nil) then
  begin
    DialogReportStaffPlanningCondF_HND := TDialogReportStaffPlanningCondF.Create(Application);
    with DialogReportStaffPlanningCondF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportStaffPlanningCondF_HND;
end;

// RV089.1.
procedure TDialogReportStaffPlanningCondF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportStaffPlanningCondF_HND <> nil) then
  begin
    DialogReportStaffPlanningCondF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportStaffPlanningCondF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportStaffPlanningCondF.btnOkClick(Sender: TObject);
var
  DateTmpFrom, DateTmpTo: TDateTime;
begin
  inherited;
  if Int(DateTo.Date - DateFrom.Date) >= 7 then
  begin
    DisplayMessage(SWeek, mtInformation, [mbYes]);
    Exit;
  end;
  if Int(DateTo.Date - DateFrom.Date) < 0 then
  begin
    DisplayMessage(SDateFromTo, mtInformation, [mbYes]);
    Exit;
  end;
// CAR 12.12.2002
  DateTmpFrom := GetDate(DateFrom.Date);
  DateTmpTo := GetDate(DateTo.Date);
// END
  if ReportStaffPlanningCondQR.QRSendReportParameters(
    GetStrValue(CmbPlusPlantFrom.Value),
    GetStrValue(CmbPlusPlantTo.Value),
    GetStrValue(CmbPlusDepartmentFrom.Value),
    GetStrValue(CmbPlusDepartmentTo.Value),
    GetStrValue(CmbPlusTeamFrom.Value),
    GetStrValue(CmbPlusTeamTo.Value),
    GetStrValue(CmbPlusWorkspotFrom.Value),
    GetStrValue(CmbPlusWorkspotTo.Value),
    IntToStr(GetIntValue(ComboBoxPlusShiftFrom.Value)),
    IntToStr(GetIntValue(ComboBoxPlusShiftTo.Value)),
    DateTmpFrom, DateTmpTo,
    CheckBoxAllTeams.Checked,
    CheckBoxShowSelection.Checked,
    CheckBoxPagePlant.Checked,
    CheckBoxPageShift.Checked,
    CheckBoxPageWK.Checked,
    CheckBoxExport.Checked,
    rgrpShowEmployeeMode.ItemIndex)
  then
    ReportStaffPlanningCondQR.ProcessRecords;
end;

procedure TDialogReportStaffPlanningCondF.FillShift;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
    ComboBoxPlusShiftFrom.Visible := True;
    ComboBoxPlusShiftTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusShiftFrom.Visible := False;
    ComboBoxPlusShiftTo.Visible := False;
  end;
   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;
{
procedure TDialogReportStaffPlanningCondF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
    ComboBoxPlusWorkspotFrom.Value := '1';
    ComboBoxPlusWorkspotTo.Value := 'zzzzzz';
  end;
end;
}
procedure TDialogReportStaffPlanningCondF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, True, False, False, True, False);
  inherited;

//show team
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE', False, ComboBoxPlusTeamTo);
}  
{  FillWorkSpot; }
  FillShift;
//end team
  ListProcsF.WeekUitDat(Now, Year, Week);
  DateFrom.Date := Int(Now + 1);
  DateTo.Date := InT(Now + 7);

   //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportStaffPlanningCondF.FormCreate(Sender: TObject);
begin
  inherited;
// CREATE data module of report
  ReportStaffPlanningCondDM := CreateReportDM(TReportStaffPlanningCondDM);
  ReportStaffPlanningCondQR := CreateReportQR(TReportStaffPlanningCondQR);
end;

procedure TDialogReportStaffPlanningCondF.DateFromChange(Sender: TObject);
begin
  inherited;
  if DateTo.Date - DateFrom.Date >= 7 then
    DateTo.Date := DateFrom.Date + 6;
  if DateTo.Date - DateFrom.Date < 0 then
    DateTo.Date := DateFrom.Date ;
end;

procedure TDialogReportStaffPlanningCondF.CheckBoxAllTeamClick(
  Sender: TObject);
begin
  inherited;
{
  ComboBoxPlusTeamFrom.Visible := not CheckBoxAllTeam.Checked;
  ComboBoxPlusTeamTo.Visible := not CheckBoxAllTeam.Checked;
}  
end;

procedure TDialogReportStaffPlanningCondF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue; }
end;

procedure TDialogReportStaffPlanningCondF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue; }
end;

procedure TDialogReportStaffPlanningCondF.ComboBoxPlusShiftFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
      GetIntValue(ComboBoxPlusShiftTo.Value) then
        ComboBoxPlusShiftTo.DisplayValue := ComboBoxPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportStaffPlanningCondF.ComboBoxPlusShiftToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
       GetIntValue(ComboBoxPlusShiftTo.Value) then
         ComboBoxPlusShiftFrom.DisplayValue := ComboBoxPlusShiftTo.DisplayValue;
end;

procedure TDialogReportStaffPlanningCondF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
{  FillWorkSpot; }
  FillShift;
end;

procedure TDialogReportStaffPlanningCondF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  FillWorkSpot; }
  FillShift;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportStaffPlanningCondF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportStaffPlanningCondF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
