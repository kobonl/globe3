inherited DialogReportProductionCompF: TDialogReportProductionCompF
  Left = 270
  Top = 196
  Caption = 'Report production comparison'
  ClientHeight = 423
  ClientWidth = 591
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 591
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 295
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 591
    Height = 302
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 32
      Top = 344
      Visible = False
    end
    inherited LblEmployee: TLabel
      Left = 80
      Top = 344
      Visible = False
    end
    inherited LblToPlant: TLabel
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Left = 155
      Top = 353
      Visible = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 136
      Top = 348
      Visible = False
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 376
      Top = 340
      Visible = False
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [9]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [10]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [11]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [12]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [13]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [14]
      Left = 8
      Top = 394
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [15]
      Left = 315
      Top = 71
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [16]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [17]
      Left = 128
      Top = 395
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [18]
      Left = 336
      Top = 392
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [19]
      Left = 40
      Top = 394
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [20]
      Left = 315
      Top = 395
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [21]
      Left = 8
      Top = 176
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [22]
      Left = 40
      Top = 176
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [23]
      Left = 315
      Top = 174
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [24]
      Left = 8
      Top = 437
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel [25]
      Left = 40
      Top = 437
      Width = 40
      Height = 13
      Caption = 'Jobcode'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [26]
      Left = 307
      Top = 435
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [27]
      Left = 128
      Top = 445
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [28]
      Left = 336
      Top = 445
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [29]
      Left = 8
      Top = 325
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [30]
      Left = 40
      Top = 325
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label27: TLabel [31]
      Left = 315
      Top = 326
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [32]
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label19: TLabel [33]
      Left = 336
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 70
    end
    inherited LblDepartment: TLabel
      Top = 70
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 148
    end
    inherited LblToTeam: TLabel
      Top = 148
    end
    inherited LblStarTeamFrom: TLabel
      Top = 148
    end
    inherited LblTeam: TLabel
      Top = 149
    end
    inherited LblFromTeam: TLabel
      Top = 149
    end
    inherited LblFromShift: TLabel
      Left = 16
      Top = 516
    end
    inherited LblShift: TLabel
      Left = 48
      Top = 516
    end
    inherited LblStartShiftFrom: TLabel
      Left = 136
    end
    inherited LblToShift: TLabel
      Left = 323
      Top = 518
    end
    inherited LblFromPlant2: TLabel
      Left = 16
      Top = 487
    end
    inherited LblPlant2: TLabel
      Left = 48
      Top = 487
    end
    inherited LblToPlant2: TLabel
      Left = 323
      Top = 487
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    inherited LblWorkspots: TLabel
      Top = 97
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 520
      Top = 91
    end
    inherited LblStarJobTo: TLabel
      Top = 125
    end
    inherited LblToJob: TLabel
      Top = 125
    end
    inherited LblStarJobFrom: TLabel
      Top = 125
    end
    inherited LblJob: TLabel
      Top = 125
    end
    inherited LblFromJob: TLabel
      Top = 125
    end
    inherited LblFromDateTime: TLabel
      Top = 176
    end
    inherited LblDateTime: TLabel
      Top = 176
    end
    inherited LblToDateTime: TLabel
      Top = 176
    end
    object DateTo: TDateTimePicker [71]
      Left = 334
      Top = 172
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 16
    end
    object DateFrom: TDateTimePicker [72]
      Left = 120
      Top = 172
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 15
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 130
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 131
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 145
      ColCount = 142
      TabOrder = 12
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 145
      ColCount = 143
      TabOrder = 13
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 522
      Top = 148
      TabOrder = 14
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 66
      ColCount = 142
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 66
      ColCount = 143
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 522
      Top = 68
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 184
      Top = 362
      TabOrder = 29
      Visible = False
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 390
      Top = 362
      TabOrder = 30
      Visible = False
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      Left = 128
      Top = 515
      ColCount = 148
      TabOrder = 24
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      Top = 515
      ColCount = 149
      TabOrder = 33
    end
    inherited CheckBoxAllShifts: TCheckBox
      Left = 538
      Top = 515
      TabOrder = 31
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 32
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 514
      Top = 410
    end
    inherited CheckBoxAllPlants: TCheckBox
      Top = 443
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Left = 128
      Top = 485
      ColCount = 158
      TabOrder = 27
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Left = 350
      Top = 485
      ColCount = 159
      TabOrder = 28
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 94
      ColCount = 159
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 94
      ColCount = 160
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      Top = 94
      Width = 393
      TabOrder = 9
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Left = 334
      Top = 120
      ColCount = 163
      TabOrder = 11
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 120
      ColCount = 162
      TabOrder = 10
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 172
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Left = 450
      Top = 172
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 172
    end
    inherited DatePickerTo: TDateTimePicker
      Left = 334
      Top = 172
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus
      Left = 120
      Top = 41
      Width = 180
      Height = 19
      ColCount = 131
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus
      Left = 334
      Top = 41
      Width = 180
      Height = 19
      ColCount = 132
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus
      Left = 136
      Top = 390
      Width = 180
      Height = 19
      ColCount = 136
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 21
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus
      Left = 350
      Top = 390
      Width = 180
      Height = 19
      ColCount = 137
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 22
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object GroupBoxSelection: TGroupBox
      Left = 8
      Top = 196
      Width = 537
      Height = 97
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 23
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 60
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 1
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 160
        Top = 30
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 2
      end
      object CheckBoxPageDept: TCheckBox
        Left = 360
        Top = 30
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 4
      end
      object CheckBoxPageWK: TCheckBox
        Left = 360
        Top = 60
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 5
      end
      object CheckBoxPageBU: TCheckBox
        Left = 160
        Top = 60
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 3
      end
      object CheckBoxDetailRep: TCheckBox
        Left = 8
        Top = 30
        Width = 153
        Height = 17
        Caption = 'Detailed report'
        TabOrder = 0
      end
    end
  end
  inherited stbarBase: TStatusBar
    Top = 404
    Width = 591
  end
  inherited pnlBottom: TPanel
    Top = 363
    Width = 591
    inherited btnOk: TBitBtn
      Left = 176
    end
    inherited btnCancel: TBitBtn
      Left = 298
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 568
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited StandardMenuActionList: TActionList
    Left = 568
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 568
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        43030000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C20104F7074
        696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564
        676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564
        676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F
        7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43
        616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E61
        7669676174696F6E116564676F436F6E6669726D44656C657465126564676F4C
        6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B730000
        0F546478444247726964436F6C756D6E0C436F6C756D6E4E756D626572074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02410942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274204E616D6505576964746802480942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D6507436170
        74696F6E06044E616D6505576964746803F4000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E060741646472657373055769647468022C0942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        52455353000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        FA020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E7402010D53756D6D61727947726F7570730E001053756D6D6172
        79536570617261746F7206022C20104F7074696F6E73437573746F6D697A650B
        0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E671065
        64676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E67
        0E6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F
        43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43
        616E496E73657274116564676F43616E4E617669676174696F6E116564676F43
        6F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72647310
        6564676F557365426F6F6B6D61726B7300000F546478444247726964436F6C75
        6D6E0A436F6C756D6E456D706C0743617074696F6E06064E756D62657206536F
        7274656407046373557005576964746802310942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E55
        4D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E53686F
        72744E616D650743617074696F6E060A53686F7274206E616D65055769647468
        024E0942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060A53484F52545F4E414D4500000F546478444247726964436F6C756D
        6E11436F6C756D6E4465736372697074696F6E0743617074696F6E06044E616D
        6505576964746803CC000942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060B4445534352495054494F4E00000F5464784442
        47726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E
        06074164647265737305576964746802650942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060741444452455353000000}
    end
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 120
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
  object QueryJobCode: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  J.JOB_CODE, J.DESCRIPTION'
      'FROM '
      '  JOBCODE J'
      'WHERE '
      '  J.PLANT_CODE =:PLANT_CODE'
      '  AND J.WORKSPOT_CODE =:WORKSPOT_CODE AND'
      '  J.COMPARATION_JOB_YN = '#39'Y'#39' '
      'ORDER BY '
      '  J.JOB_CODE')
    Left = 368
    Top = 23
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'WORKSPOT_CODE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceWorkSpot: TDataSource
    DataSet = qryWorkspot
    Left = 480
    Top = 23
  end
end
