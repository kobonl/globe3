inherited OvertimeDefF: TOvertimeDefF
  Left = 263
  Top = 136
  Width = 614
  Height = 501
  Caption = 'Overtime definitions'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Width = 598
    TabOrder = 0
    inherited spltMasterGrid: TSplitter
      Width = 596
    end
    inherited dxMasterGrid: TdxDBGrid
      Width = 596
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Contract groups'
        end>
      KeyField = 'CONTRACTGROUP_CODE'
      ShowBands = True
      OnChangeNode = dxMasterGridChangeNode
      object dxMasterGridColumnCode: TdxDBGridColumn
        Caption = 'Code'
        Width = 130
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CONTRACTGROUP_CODE'
      end
      object dxMasterGridColumnDescription: TdxDBGridColumn
        Caption = 'Description'
        Width = 236
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DESCRIPTION'
      end
      object dxMasterGridColumnWeekNo: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Start in week no.'
        Width = 172
        BandIndex = 0
        RowIndex = 0
        FieldName = 'PERIOD_STARTS_IN_WEEK'
      end
      object dxMasterGridColumnWeeksPeriod: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Weeks in period'
        Width = 182
        BandIndex = 0
        RowIndex = 0
        FieldName = 'WEEKS_IN_PERIOD'
      end
      object dxMasterGridColumnCalcOvertimetype: TdxDBGridColumn
        Caption = 'Overtime type'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'CALC_OVERTIMETYPE'
      end
    end
  end
  inherited pnlDetail: TPanel
    Top = 336
    Width = 598
    Height = 126
    object Label3: TLabel
      Left = 64
      Top = 60
      Width = 3
      Height = 13
    end
    object GroupBoxOverTime: TGroupBox
      Left = 1
      Top = 1
      Width = 324
      Height = 124
      Align = alClient
      Caption = 'OverTime'
      TabOrder = 0
      object Label2: TLabel
        Left = 10
        Top = 41
        Width = 48
        Height = 13
        Caption = 'Hour type'
      end
      object Label1: TLabel
        Left = 10
        Top = 17
        Width = 38
        Height = 13
        Caption = 'Line no.'
      end
      object lblOnlyForHrsBetween: TLabel
        Left = 10
        Top = 64
        Width = 114
        Height = 13
        Caption = 'Only for hours between'
      end
      object LblWeekDay: TLabel
        Left = 10
        Top = 85
        Width = 60
        Height = 13
        Caption = 'Day of week'
      end
      object lblAnd: TLabel
        Left = 230
        Top = 64
        Width = 18
        Height = 13
        Caption = 'and'
      end
      object dxDBSpinEditLine: TdxDBSpinEdit
        Tag = 1
        Left = 67
        Top = 13
        Width = 54
        Style.BorderStyle = xbsSingle
        TabOrder = 0
        DataField = 'LINE_NUMBER'
        DataSource = OvertimeDefDM.DataSourceDetail
      end
      object dxDBLookupEdit1: TdxDBLookupEdit
        Tag = 1
        Left = 67
        Top = 37
        Width = 246
        Style.BorderStyle = xbsSingle
        TabOrder = 1
        DataField = 'HOURTYPELU'
        DataSource = OvertimeDefDM.DataSourceDetail
        DropDownRows = 4
        ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
      end
      object dxDBTimeEditFromTime: TdxDBTimeEdit
        Left = 165
        Top = 61
        Width = 57
        Style.BorderStyle = xbsSingle
        TabOrder = 2
        DataField = 'FROMTIME'
        DataSource = OvertimeDefDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBTimeEditToTime: TdxDBTimeEdit
        Left = 255
        Top = 61
        Width = 57
        Style.BorderStyle = xbsSingle
        TabOrder = 3
        DataField = 'TOTIME'
        DataSource = OvertimeDefDM.DataSourceDetail
        TimeEditFormat = tfHourMin
        StoredValues = 4
      end
      object dxDBImgEdtDayOfWeek: TdxDBImageEdit
        Tag = 1
        Left = 165
        Top = 84
        Width = 147
        Style.BorderStyle = xbsSingle
        TabOrder = 4
        DataField = 'DAY_OF_WEEK'
        DataSource = OvertimeDefDM.DataSourceDetail
      end
    end
    object GroupBoxMore: TGroupBox
      Left = 325
      Top = 1
      Width = 136
      Height = 124
      Align = alRight
      Caption = 'More than'
      TabOrder = 1
      object Label6: TLabel
        Left = 10
        Top = 28
        Width = 27
        Height = 13
        Caption = 'hours'
      end
      object Label8: TLabel
        Left = 10
        Top = 62
        Width = 37
        Height = 13
        Caption = 'minutes'
      end
      object dxSpinEditStartHour: TdxSpinEdit
        Tag = 1
        Left = 56
        Top = 24
        Width = 72
        TabOrder = 0
        OnExit = dxSpinEditStartHourExit
        OnChange = dxSpinEditStartHourChange
        MaxValue = 9999
        StoredValues = 16
      end
      object dxSpinEditStartMin: TdxSpinEdit
        Tag = 1
        Left = 56
        Top = 60
        Width = 41
        TabOrder = 1
        OnExit = dxSpinEditStartMinExit
        OnChange = dxSpinEditStartMinChange
        MaxValue = 59
        StoredValues = 16
      end
      object dxSpinEditStartHourOld: TdxSpinEdit
        Left = 7
        Top = 80
        Width = 41
        TabOrder = 2
        Visible = False
      end
      object dxSpinEditStartMinOld: TdxSpinEdit
        Left = 7
        Top = 104
        Width = 41
        TabOrder = 3
        Visible = False
      end
    end
    object GroupBoxuntil: TGroupBox
      Left = 461
      Top = 1
      Width = 136
      Height = 124
      Align = alRight
      Caption = 'Untill'
      TabOrder = 2
      object Label7: TLabel
        Left = 10
        Top = 28
        Width = 27
        Height = 13
        Caption = 'hours'
      end
      object Label9: TLabel
        Left = 10
        Top = 62
        Width = 37
        Height = 13
        Caption = 'minutes'
      end
      object dxSpinEditEndHour: TdxSpinEdit
        Tag = 1
        Left = 56
        Top = 24
        Width = 72
        TabOrder = 0
        OnExit = dxSpinEditEndHourExit
        OnChange = dxSpinEditEndHourChange
        MaxValue = 9999
        StoredValues = 16
      end
      object dxSpinEditEndMin: TdxSpinEdit
        Tag = 1
        Left = 56
        Top = 60
        Width = 45
        TabOrder = 1
        OnExit = dxSpinEditEndMinExit
        OnChange = dxSpinEditEndMinChange
        MaxValue = 59
        StoredValues = 16
      end
      object dxSpinEditEndHourOld: TdxSpinEdit
        Left = 7
        Top = 80
        Width = 41
        TabOrder = 2
        Visible = False
      end
      object dxSpinEditEndMinOld: TdxSpinEdit
        Left = 7
        Top = 104
        Width = 41
        TabOrder = 3
        Visible = False
      end
    end
  end
  inherited pnlDetailGrid: TPanel
    Width = 598
    Height = 181
    TabOrder = 3
    inherited spltDetail: TSplitter
      Top = 177
      Width = 596
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 596
      Height = 176
      Bands = <
        item
          Alignment = taLeftJustify
          Caption = 'Overtime definitions'
          Width = 617
        end>
      KeyField = 'LINE_NUMBER'
      ShowBands = True
      OnChangeNode = dxDetailGridChangeNode
      object dxDetailGridColumnLine: TdxDBGridColumn
        Caption = 'Line no.'
        Width = 62
        BandIndex = 0
        RowIndex = 0
        FieldName = 'LINE_NUMBER'
      end
      object dxDetailGridColumnHourType: TdxDBGridLookupColumn
        Caption = 'Hour type'
        Width = 264
        BandIndex = 0
        RowIndex = 0
        FieldName = 'HOURTYPELU'
        ListFieldName = 'DESCRIPTION;HOURTYPE_NUMBER'
      end
      object dxDetailGridColumnStartTime: TdxDBGridColumn
        Caption = 'More than (hrs:min)'
        Width = 184
        BandIndex = 0
        RowIndex = 0
        FieldName = 'STARTHOURMIN'
      end
      object dxDetailGridColumnEndTime: TdxDBGridColumn
        Caption = 'Until (hrs:min)'
        Width = 112
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ENDHOURMIN'
      end
      object dxDetailGridColumnFromTime: TdxDBGridColumn
        Caption = 'From'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'FROMTIMESTRING'
      end
      object dxDetailGridColumnToTime: TdxDBGridColumn
        Caption = 'To'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TOTIMESTRING'
      end
      object dxDetailGridColumnDayOfWeek: TdxDBGridColumn
        Caption = 'Day of week'
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DAYDESC'
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCopy
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPaste
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
    inherited dxBarButtonCopy: TdxBarButton
      Action = CopyAct
    end
    inherited dxBarButtonPaste: TdxBarButton
      Action = PasteAct
    end
  end
  inherited StandardMenuActionList: TActionList
    object CopyAct: TAction
      Caption = '&Copy'
      ShortCut = 16451
      OnExecute = CopyActExecute
    end
    object PasteAct: TAction
      Caption = '&Paste'
      ShortCut = 16470
      OnExecute = PasteActExecute
    end
  end
end
