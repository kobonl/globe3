inherited TypeHourDM: TTypeHourDM
  OldCreateOrder = True
  Left = 306
  Top = 118
  Height = 479
  Width = 741
  inherited TableMaster: TTable
    OnNewRecord = TableMasterNewRecord
    object TableMasterHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableMasterDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableMasterCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableMasterBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      DefaultExpression = '0'
      FieldName = 'BONUS_PERCENTAGE'
      OnValidate = DefaultNotEmptyValidate
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableMasterOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableMasterMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableMasterMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableMasterCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableMasterEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 6
    end
    object TableMasterMINIMUM_WAGE: TFloatField
      FieldName = 'MINIMUM_WAGE'
    end
    object TableMasterIGNORE_FOR_OVERTIME_YN: TStringField
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Size = 1
    end
    object TableMasterWAGE_BONUS_ONLY_YN: TStringField
      FieldName = 'WAGE_BONUS_ONLY_YN'
      Size = 1
    end
  end
  inherited TableDetail: TTable
    OnNewRecord = TableDetailNewRecord
    TableName = 'HOURTYPE'
    object TableDetailHOURTYPE_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'HOURTYPE_NUMBER'
      OnValidate = DefaultNotEmptyValidate
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      OnValidate = DefaultNotEmptyValidate
      Size = 30
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailBONUS_PERCENTAGE: TFloatField
      Alignment = taLeftJustify
      DefaultExpression = '0'
      FieldName = 'BONUS_PERCENTAGE'
      OnValidate = DefaultNotEmptyValidate
      DisplayFormat = '0.##'
      MaxValue = 999.99
      Precision = 5
    end
    object TableDetailOVERTIME_YN: TStringField
      FieldName = 'OVERTIME_YN'
      Size = 1
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailCOUNT_DAY_YN: TStringField
      FieldName = 'COUNT_DAY_YN'
      Size = 1
    end
    object TableDetailMINIMUM_WAGE: TFloatField
      FieldName = 'MINIMUM_WAGE'
    end
    object TableDetailIGNORE_FOR_OVERTIME_YN: TStringField
      FieldName = 'IGNORE_FOR_OVERTIME_YN'
      Size = 1
    end
    object TableDetailWAGE_BONUS_ONLY_YN: TStringField
      FieldName = 'WAGE_BONUS_ONLY_YN'
      Size = 1
    end
    object TableDetailTIME_FOR_TIME_YN: TStringField
      FieldName = 'TIME_FOR_TIME_YN'
      Required = True
      Size = 1
    end
    object TableDetailBONUS_IN_MONEY_YN: TStringField
      FieldName = 'BONUS_IN_MONEY_YN'
      Required = True
      Size = 1
    end
    object TableDetailEXPORTVL_NUMBER: TIntegerField
      FieldName = 'EXPORTVL_NUMBER'
    end
    object TableDetailADV_YN: TStringField
      FieldName = 'ADV_YN'
      Required = True
      Size = 1
    end
    object TableDetailSATURDAY_CREDIT_YN: TStringField
      FieldName = 'SATURDAY_CREDIT_YN'
      Required = True
      Size = 1
    end
    object TableDetailEXPORT_CODE: TStringField
      FieldName = 'EXPORT_CODE'
      Size = 7
    end
    object TableDetailTRAVELTIME_YN: TStringField
      FieldName = 'TRAVELTIME_YN'
      Required = True
      Size = 1
    end
    object TableDetailEXPORT_OVERTIME_YN: TStringField
      FieldName = 'EXPORT_OVERTIME_YN'
      Size = 1
    end
    object TableDetailEXPORT_PAYROLL_YN: TStringField
      FieldName = 'EXPORT_PAYROLL_YN'
      Size = 1
    end
  end
  object qryHTPerCountryADP: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  HC.HOURTYPE_NUMBER'
      'FROM'
      '  HOURTYPEPERCOUNTRY HC INNER JOIN PLANT P ON'
      '    P.COUNTRY_ID = HC.COUNTRY_ID'
      '    INNER JOIN HOURTYPE H ON'
      '    HC.HOURTYPE_NUMBER = H.HOURTYPE_NUMBER'
      '    INNER JOIN COUNTRY C ON'
      '    P.COUNTRY_ID = C.COUNTRY_ID'
      'WHERE'
      '  C.EXPORT_TYPE = '#39'ADP'#39
      ''
      ' ')
    Left = 96
    Top = 224
  end
end
