(*
  Changes:
    MRA:12-JAN-2010. RV050.8. 889955.
    - Restrict plants using teamsperuser.
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
*)
unit DialogReportStaffAvailabilityFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib, jpeg;

type
  TDialogReportStaffAvailabilityF = class(TDialogReportBaseF)
    Label3: TLabel;
    GroupBoxSelection: TGroupBox;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroupSort: TRadioGroup;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeek: TdxSpinEdit;
    GroupBoxShow: TGroupBox;
    CheckBoxPlant: TCheckBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    CheckBoxTeam: TCheckBox;
    CheckBoxPagePlant: TCheckBox;
    CheckBoxPageDept: TCheckBox;
    CheckBoxPageTeam: TCheckBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    CheckBoxDept: TCheckBox;
    CheckBoxPaidBreaks: TCheckBox;
    ComboBoxPlusShiftFrom: TComboBoxPlus;
    ComboBoxPlusShiftTo: TComboBoxPlus;
    QueryShift: TQuery;
    CheckBoxAllTeam: TCheckBox;
    CheckBoxShowTotals: TCheckBox;
    CheckBoxExport: TCheckBox;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    CheckBoxAllDepartment: TCheckBox;
    Label8: TLabel;
    Label9: TLabel;
    CheckBoxShowOnlyEmpAbsence: TCheckBox;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FillShift;
    procedure CheckBoxPlantClick(Sender: TObject);
    procedure CheckBoxTeamClick(Sender: TObject);
    procedure SetEnabledDept(Status: Boolean);
    procedure SetTeamEnabled(Status: Boolean);
    procedure CheckBoxDeptClick(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBoxPlusShiftFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusShiftToCloseUp(Sender: TObject);
    procedure CmbPlusPlantToCloseUp(Sender: TObject);
    procedure CmbPlusPlantFromCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declaration }
  public
    { Public declarations }
    // MR:17-02-2005 Order 550378 Instead of 'TeamCode', arguments
    // TeamFrom, TeamTo are used.
    // RV089.1. Added default-values: When called as 'embed', it does
    //          not set these params.
    constructor Create(Sender: TComponent; TeamFrom: String = '';
      TeamTo: String = ''; PlantCode: String = '';
      AYear: Integer = -1; AWeek: Integer = 0);reintroduce; overload;
  end;

var
  DialogReportStaffAvailabilityF: TDialogReportStaffAvailabilityF;

// RV089.1.
function DialogReportStaffAvailabilityForm: TDialogReportStaffAvailabilityF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  SystemDMT, ReportStaffAvailabilityDMT, ReportStaffAvailabilityQRPT,
  ListProcsFRM, CalculateTotalHoursDMT, UPimsMessageRes, UPimsConst;

// RV089.1.
var
  DialogReportStaffAvailabilityF_HND: TDialogReportStaffAvailabilityF;

// RV089.1.
function DialogReportStaffAvailabilityForm: TDialogReportStaffAvailabilityF;
begin
  if (DialogReportStaffAvailabilityF_HND = nil) then
  begin
    DialogReportStaffAvailabilityF_HND := TDialogReportStaffAvailabilityF.Create(Application);
    with DialogReportStaffAvailabilityF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportStaffAvailabilityF_HND;
end;

// RV089.1.
procedure TDialogReportStaffAvailabilityF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportStaffAvailabilityF_HND <> nil) then
  begin
    DialogReportStaffAvailabilityF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportStaffAvailabilityF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

constructor TDialogReportStaffAvailabilityF.Create(Sender: TComponent;
  TeamFrom, TeamTo, PlantCode: String; AYear, AWeek: Integer);
var
  Year, Week: Word;
begin
  inherited Create(Sender);
  year := AYear;
  week := AWeek;
  if AYear <= 0 then
  begin
    ListProcsF.WeekUitDat(Now, Year, Week);
  end;
  dxSpinEditYear.IntValue := Year;
  dxSpinEditWeek.IntValue := Week;
  dxSpinEditWeek.MaxValue := ListProcsF.WeeksInYear(Year);
// CREATE data module of report
  ReportStaffAvailabilityDM := CreateReportDM(TReportStaffAvailabilityDM);
  ReportStaffAvailabilityQR := CreateReportQR(TReportStaffAvailabilityQR);

//  ListProcsF.FillComboBoxPlant(tblPlant, True, CmbPlusPlantFrom);
//  ListProcsF.FillComboBoxPlant(tblPlant, False, CmbPlusPlantTo);
  tblPlant.Open;
  if AYear > 0 then
  begin
    if (PlantCode <> '') then // Search for one plant
    begin
      SearchPlant := PlantCode;
      tblPlant.FindKey([PlantCode]);
      CmbPlusPlantFrom.Value :=
        tblPlant.FieldByName('PLANT_CODE').AsString + Str_Sep +
          tblPlant.FieldByName('DESCRIPTION').AsString;
      CmbPlusPlantTo.Value :=
        tblPlant.FieldByName('PLANT_CODE').AsString + Str_Sep +
          tblPlant.FieldByName('DESCRIPTION').AsString;
    end
    else
    begin // search for all plants
      tblPlant.First;
      CmbPlusPlantFrom.Value :=
        tblPlant.FieldByName('PLANT_CODE').AsString + Str_Sep +
          tblPlant.FieldByName('DESCRIPTION').AsString;
      tblPlant.Last;
      CmbPlusPlantTo.Value :=
        tblPlant.FieldByName('PLANT_CODE').AsString + Str_Sep +
          tblPlant.FieldByName('DESCRIPTION').AsString;
    end;
  end;

//show team
{
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE',
    True, ComboBoxPlusTeamFrom);
  ListProcsF.FillComboBoxMaster(TableTeam, 'TEAM_CODE',
    False, ComboBoxPlusTeamTo);
}
  if (TeamFrom <> '') then
  begin
//    SearchTeam := TeamFrom;
{    TableTeam.Open;
    TableTeam.FindKey([TeamFrom]);
    CmbPlusTeamFrom.Value :=
      TableTeam.FieldByName('TEAM_CODE').AsString +
        Str_SEP + TableTeam.FieldByName('DESCRIPTION').AsString;
    TableTeam.FindKey([TeamTo]);
    CmbPlusTeamTo.Value :=
      TableTeam.FieldByName('TEAM_CODE').AsString +
        Str_SEP + TableTeam.FieldByName('DESCRIPTION').AsString;
    CheckBoxAllTeams.Checked := False; }
  end
  else
  begin
{    CheckBoxAllTeams.Checked := True; }
  end;
  //end team

  CheckBoxShowSelection.Checked := True;
  CheckBoxPagePlant.Checked := True;
  CheckBoxDept.Checked := False;
  CheckBoxTeam.Checked := False;
  CheckBoxShowTotals.Checked := False;
  FillShift;
  RadioGroupSort.ItemIndex := 0;
end;

procedure TDialogReportStaffAvailabilityF.btnOkClick(Sender: TObject);
begin
  inherited;
  if (not CheckBoxAllTeams.Checked and
  ((CmbPlusTeamFrom.Value = '') or (CmbPlusTeamTo.Value = '')) )then
  begin
    DisplayMessage(SEmptyValues,  mtInformation, [mbYes]);
    exit;
  end;
  if CmbPlusPlantFrom.Value <> CmbPlusPlantTo.Value then
  begin
    CmbPlusDepartmentFrom.Value := '1';
    CmbPlusDepartmentTo.Value := 'zzzzzz';
    ComboBoxPlusShiftFrom.Value := '1';
    ComboBoxPlusShiftTo.Value := '99';
  end;
  if ReportStaffAvailabilityQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusTeamFrom.Value),
      GetStrValue(CmbPlusTeamTo.Value),
      IntToStr(GetIntValue(ComboBoxPlusShiftFrom.Value)),
      IntToStr(GetIntValue(ComboBoxPlusShiftTO.Value)),
      Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeek.Value),
      RadioGroupSort.ItemIndex,
      CheckBoxPlant.Checked, CheckBoxDept.Checked, CheckBoxTeam.Checked,
      CheckBoxAllTeams.Checked,
      CheckBoxAllDepartment.Checked,
      CheckBoxShowSelection.Checked,
      CheckBoxPagePlant.Checked, CheckBoxPageDept.Checked,
      CheckBoxPageTeam.Checked,
      CheckBoxPaidBreaks.Checked,
      CheckBoxShowTotals.Checked,
      CheckBoxShowOnlyEmpAbsence.Checked,
      CheckBoxExport.Checked)
  then
    ReportStaffAvailabilityQR.ProcessRecords;
end;

procedure TDialogReportStaffAvailabilityF.FillShift;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', True);
    ListProcsF.FillComboBoxMasterPlant(queryShift, ComboBoxPlusShiftTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'SHIFT_NUMBER', False);
    ComboBoxPlusShiftFrom.Visible := True;
    ComboBoxPlusShiftTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusShiftFrom.Visible := False;
    ComboBoxPlusShiftTo.Visible := False;
  end;
  //Car 2-4-2004 - set property in order to be numerical sorted
  ComboBoxPlusShiftFrom.IsSorted := True;
  ComboBoxPlusShiftTo.IsSorted := True;
end;

procedure TDialogReportStaffAvailabilityF.FormShow(Sender: TObject);
begin
  InitDialog(True, True, True, False, False, False, False);
  inherited;

end;

procedure TDialogReportStaffAvailabilityF.CheckBoxPlantClick(Sender: TObject);
begin
  inherited;
  if CheckBoxPlant.Checked or CheckBoxDept.Checked then
    SetTeamEnabled(False)
  else
    SetTeamEnabled(True);
  CheckBoxPagePlant.Enabled := CheckBoxPlant.Checked;
  CheckBoxPagePlant.Checked := CheckBoxPlant.Checked;
  CheckBoxPagePlant.Enabled := CheckBoxPlant.Checked;
  CheckBoxPagePlant.Checked := CheckBoxPlant.Checked;
end;

procedure TDialogReportStaffAvailabilityF.SetEnabledDept(Status: Boolean);
begin
  CheckBoxDept.Checked := False;
  CheckBoxDept.Enabled := Status;
  CheckBoxPageDept.Checked := False;
  CheckBoxPageDept.Enabled := Status;
  CheckBoxPlant.Checked := False;
  CheckBoxPlant.Enabled := Status;
  CheckBoxPagePlant.Checked := False;
  CheckBoxPagePlant.Enabled := Status;
end;

procedure TDialogReportStaffAvailabilityF.CheckBoxTeamClick(Sender: TObject);
begin
  inherited;
  SetEnabledDept(not CheckBoxTeam.Checked);
  CheckBoxPageTeam.Enabled := CheckBoxTeam.Checked;
  CheckBoxPageTeam.Checked := CheckBoxTeam.Checked;
end;

procedure TDialogReportStaffAvailabilityF.SetTeamEnabled(Status: Boolean);
begin
  CheckBoxTeam.Enabled := Status;
  CheckBoxTeam.Checked := False;
  CheckBoxPageTeam.Enabled := Status;
  CheckBoxPageTeam.Checked := False;
end;

procedure TDialogReportStaffAvailabilityF.CheckBoxDeptClick(
  Sender: TObject);
begin
  inherited;
  if (CheckBoxPlant.Checked or CheckBoxDept.Checked) then
    SetTeamEnabled(False)
  else
    SetTeamEnabled(True);

  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
  CheckBoxPageDept.Enabled := CheckBoxDept.Checked;
  CheckBoxPageDept.Checked := CheckBoxDept.Checked;
end;

procedure TDialogReportStaffAvailabilityF.CheckBoxAllTeamClick(
  Sender: TObject);
begin
  inherited;
{
  if CheckBoxAllTeam.Checked then
  begin
    ComboBoxPlusTeamFrom.Visible := False;
    ComboBoxPlusTeamTo.Visible := False;
  end
  else
  begin
    ComboBoxPlusTeamFrom.Visible := True;
    ComboBoxPlusTeamTo.Visible := True;
  end;
}  
end;

// MR:05-12-2003
procedure TDialogReportStaffAvailabilityF.ChangeDate(Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeek.MaxValue := MaxWeeks;
    if dxSpinEditWeek.Value > MaxWeeks then
      dxSpinEditWeek.Value := MaxWeeks;
  except
    dxSpinEditWeek.Value := 1;
  end;
end;

procedure TDialogReportStaffAvailabilityF.FormCreate(Sender: TObject);
begin
  InitDialog(True, True, True, False, False, False, False);
  inherited;
end;

procedure TDialogReportStaffAvailabilityF.ComboBoxPlusShiftFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
      GetIntValue(ComboBoxPlusShiftTo.Value)then
        ComboBoxPlusShiftTo.DisplayValue := ComboBoxPlusShiftFrom.DisplayValue;
end;

procedure TDialogReportStaffAvailabilityF.ComboBoxPlusShiftToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusShiftFrom.DisplayValue <> '') and
     (ComboBoxPlusShiftTo.DisplayValue <> '') then
    if GetIntValue(ComboBoxPlusShiftFrom.Value) >
       GetIntValue(ComboBoxPlusShiftTo.Value) then
         ComboBoxPlusShiftFrom.DisplayValue := ComboBoxPlusShiftTo.DisplayValue;
end;

procedure TDialogReportStaffAvailabilityF.CmbPlusPlantToCloseUp(
  Sender: TObject);
begin
  inherited;
  FillShift;
end;

procedure TDialogReportStaffAvailabilityF.CmbPlusPlantFromCloseUp(
  Sender: TObject);
begin
  inherited;
  FillShift;
end;

procedure TDialogReportStaffAvailabilityF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportStaffAvailabilityF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportStaffAvailabilityF.FillAll;
begin
  inherited;
  FillShift;
end;

end.
