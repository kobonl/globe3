(*
    SO:08-JUL-2010 RV067.4. 550497
    - filter absence reasons per country
    MRA:30-AUG-2010 RV067.MRA.16.Bugfix.
    - Add/Delete/Modiay-buttons did not react anymore.
      Reason: DetailGrid was not linked to TableDetail.
    MRA:11-JAN-2011 RV084.3. SR-980041
    - The filtering on absence reasons per country goes wrong when
      the following is done:
      1. The dialog is opened (default tab-page is: Illness messages per employee).
      2. The Add-button is clicked.
      3. As default: In details, the same employee as above is filled in and
         the start date is filled in (today).
      4. When the absence reason is selected, it shows all absence reasons.
    MRA:13-JAN-2011 RV084.4. Bugfix.
    - When adding illness message from tab-page 'Illness messages per employee',
      then there are 2 places where you can enter the employee!
      - The second place (in detail-panel) must be non-editable.
    MRA:1-MAY-2013 20012944 Show weeknumbers
    - One component of type TdxDateEdit is replaced by TDateTimePicker:
      - dxDateEditStart
    - Component TDateTimePicker is overridden in SystemDMT,
      unit SystemDMT must be added as last unit in first uses-line.
    - Removed Clear-button from dxDBDateEditStartDate.
*)

unit IllnessMessagesFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, ComCtrls, DBPicker, DBCtrls, StdCtrls, Db, ActnList,
  dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl, dxDBGrid, ExtCtrls,
  dxEditor, dxExEdtr, dxEdLib, dxDBELib, Mask, dxDBTLCl, dxGrClms,
  dxExGrEd, dxExELib, dxLayout, SystemDMT;

const
  MinHeight = 30;
  NormalHeight = 82;
type
  TIllnessMessagesF = class(TGridBaseF)
    PanelTabs: TPanel;
    PageControlIllness: TPageControl;
    TabSheetOutstanding: TTabSheet;
    TabSheetIllnessMessages: TTabSheet;
    GroupBoxSelectEmployee: TGroupBox;
    LabelEmployee: TLabel;
    LabelIllnessMessages: TLabel;
    GroupBoxDetailIllness: TGroupBox;
    GroupBoxDetailEmployee: TGroupBox;
    LabelDetailEmployee: TLabel;
    GroupBoxDetailDate: TGroupBox;
    LabelStartDate: TLabel;
    LabelEndDate: TLabel;
    GroupBoxDetail: TGroupBox;
    LabelAbsenceReason: TLabel;
    LabelDescription: TLabel;
    DBEditDescription: TDBEdit;
    DBLookupComboBoxAbsenceReason: TDBLookupComboBox;
    dxDBDateEditStartdate: TdxDBDateEdit;
    dxDBDateEditEndDate: TdxDBDateEdit;
    DBLookupComboBoxEmployee: TDBLookupComboBox;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeItem: TdxDBGridLayout;
    dxDetailGridColumnStartdate: TdxDBGridDateColumn;
    dxDetailGridColumnEndDate: TdxDBGridDateColumn;
    dxDetailGridColumnEmployee: TdxDBGridLookupColumn;
    dxDetailGridColumnAbsencereason: TdxDBGridLookupColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    LabelEmplDesc: TLabel;
    Label1: TLabel;
    dxDetailGridColumnLineNumber: TdxDBGridColumn;
    dxDBSpinEditLINE_NUMBER: TdxDBSpinEdit;
    DateEditStart: TDateTimePicker;
    procedure PageControlIllnessChange(Sender: TObject);
//    procedure dxDateEditStartChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxDBExtLookupEditEmployeeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxDBExtLookupEditEmployeeCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
//    procedure dxDateEditStartValidate(Sender: TObject;
//      var ErrorText: String; var Accept: Boolean);
    procedure dxDetailGridClick(Sender: TObject);
    procedure dxGridEnter(Sender: TObject);
    procedure DBLookupComboBoxEmployeeCloseUp(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure DateEditStartChange(Sender: TObject);
    procedure DateEditStartCloseUp(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    StartYearDate: TDateTime;
    procedure SetDefaultValues;
    procedure SetDateToFilter;
    //550262
    procedure SetGridOnEmployee(Employee: Integer);
  end;
 function IllnessMessagesF: TIllnessMessagesF;

implementation

{$R *.DFM}
uses IllnessMessagesDMT, ListProcsFRM;

var
  IllnessMessagesF_HND: TIllnessMessagesF;

function IllnessMessagesF: TIllnessMessagesF;
begin
  if (IllnessMessagesF_HND = nil) then
    IllnessMessagesF_HND := TIllnessMessagesF.Create(Application);
  Result := IllnessMessagesF_HND;
end;

procedure TIllnessMessagesF.PageControlIllnessChange(Sender: TObject);
var
  Afilter: TillnessFilter;
  year, month, day: word;
  DateTmp: TDateTime;
begin
  inherited;
  Afilter.StartDate := IllnessMessagesDM.IllnessFilter.StartDate;

  case PageControlIllness.ActivePageIndex of
    0: // Outstanding Illness Messages
    begin
      PanelTabs.Height := MinHeight;
      AFilter.ActiveTab := OutStanding;
      DateTmp := Now; ReplaceTime(DateTmp, 0);
      AFilter.StartDate := DateTmp;
      //550286
      if not IllnessMessagesDM.cdsMaster.IsEmpty then
        Afilter.Employee_Number :=
          IllnessMessagesDM.cdsMaster.FieldByName('EMPLOYEE_NUMBER').AsInteger
      else
        Afilter.Employee_Number := 0;
      IllnessMessagesDM.IllnessFilter := AFilter;
      DBLookupComboBoxEmployee.Enabled := True; // RV084.4.
    end;
    1: // Illness Messages Per Employee
    begin
      if SystemDM.ASaveTimeRecScanning.Employee <> -1 then
      begin
        //550286
        Afilter.Employee_Number := SystemDM.ASaveTimeRecScanning.Employee;
        IllnessMessagesDM.cdsMaster.FindKey([Afilter.Employee_Number]);
      end
      else
      begin
        IllnessMessagesDM.cdsMaster.First;
        Afilter.Employee_Number :=
          IllnessMessagesDM.cdsMaster.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      end;
      PanelTabs.Height := NormalHeight;
      AFilter.ActiveTab := PerEmployee;
      Decodedate(AFilter.StartDate,year,month,day);
      AFilter.StartDate := EncodeDate(year,1,1);
      {dxDateEditStart.Date} DateEditStart.DateTime := AFilter.StartDate;
      LabelEmplDesc.Caption :=
        IllnessMessagesDM.cdsMaster.FieldByName('DESCRIPTION').AsString;
      IllnessMessagesDM.IllnessFilter := AFilter;
//CAR 21-10-2003
      SystemDM.ASaveTimeRecScanning.Employee :=
        IllnessMessagesDM.IllnessFilter.Employee_Number;
      DBLookupComboBoxEmployee.Enabled := False; // RV084.4.
    end;
  end;
  dxDetailGrid.Bands[0].Caption := PageControlIllness.ActivePage.Caption;
  //CAR 550262
  SetGridOnEmployee(SystemDM.ASaveTimeRecScanning.Employee);
end;

procedure TIllnessMessagesF.SetDefaultValues;
var
  AFilter: TIllnessFilter;
begin
  AFilter.StartDate := now;
  AFilter.ActiveTab := OutStanding;
  IllnessMessagesDM.IllnessFilter := AFilter;
  ActiveGrid := dxDetailGrid;
end;

procedure TIllnessMessagesF.SetDateToFilter;
var
  Afilter: TillnessFilter;
  DateTmp: TDateTime;
begin
  DateTmp := {dxDateEditStart.Date} DateEditStart.DateTime;
  ReplaceTime(DateTmp , 0);
  Afilter.StartDate := DateTmp;

  if (dxDBExtLookupEditEmployee.Text <> '') then
    Afilter.Employee_Number :=  StrToInt(dxDBExtLookupEditEmployee.Text)
  else
    Afilter.Employee_Number := 0;
  AFilter.ActiveTab := PerEmployee;
  IllnessMessagesDM.IllnessFilter := AFilter;
  dxDBExtLookupEditEmployee.Text :=
    IntToStr(IllnessMessagesDM.IllnessFilter.Employee_Number);
end;
{
procedure TIllnessMessagesF.dxDateEditStartChange(Sender: TObject);
var
  Afilter: TillnessFilter;
  DateTmp: TDateTime;
begin
  inherited;
  DateTmp := dxDateEditStart.Date;
  ReplaceTime(DateTmp, 0);
  Afilter.StartDate := DateTmp;
  if (dxDBExtLookupEditEmployee.Text <> '') then
    Afilter.Employee_Number :=  StrToInt(dxDBExtLookupEditEmployee.Text)
  else
    Afilter.Employee_Number := 0;
  AFilter.ActiveTab := PerEmployee;
  IllnessMessagesDM.IllnessFilter := AFilter;

  dxDBExtLookupEditEmployee.Text :=
    IntToStr(IllnessMessagesDM.IllnessFilter.Employee_Number);
end;
}
procedure TIllnessMessagesF.FormCreate(Sender: TObject);
begin
  IllnessMessagesDM := CreateFormDM(TIllnessMessagesDM);
  SetDefaultValues;
  inherited;
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := IllnessMessagesDM.DataSourceDetail;
end;

procedure TIllnessMessagesF.FormDestroy(Sender: TObject);
begin
  inherited;
  IllnessMessagesF_HND := Nil;
end;

procedure TIllnessMessagesF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TIllnessMessagesF.dxDBExtLookupEditEmployeeChange(
  Sender: TObject);
var
  Afilter: TillnessFilter;
  DateTmp: TDateTime;
begin
  inherited;
  DateTmp := {dxDateEditStart.Date} DateEditStart.DateTime;
  ReplaceTime(DateTmp, 0);
  Afilter.StartDate := DateTmp;
  if (dxDBExtLookupEditEmployee.Text <> '') then
    Afilter.Employee_Number := StrToInt(dxDBExtLookupEditEmployee.Text)
  else
  begin
    Afilter.Employee_Number := 0;
  end;
  AFilter.ActiveTab := PerEmployee;
  IllnessMessagesDM.IllnessFilter := AFilter;
  dxDBExtLookupEditEmployee.Text :=
    IntToStr(IllnessMessagesDM.IllnessFilter.Employee_Number);
end;

procedure TIllnessMessagesF.SetGridOnEmployee(Employee: Integer);
var
  AFilter: TIllnessFilter;
begin
  IllnessMessagesDM.TableDetail.AfterScroll := Nil;
  IllnessMessagesDM.TableDetail.First;
  while not IllnessMessagesDM.TableDetail.Eof and
    (IllnessMessagesDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <>
     Employee) do
     IllnessMessagesDM.TableDetail.Next;

  if IllnessMessagesDM.TableDetail.Eof and
    (IllnessMessagesDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger <>
       Employee) then
  begin
    IllnessMessagesDM.TableDetail.First;
    IllnessMessagesDM.cdsMaster.FindKey([Employee]);

    if DBLookupComboBoxEmployee.Visible and DBLookupComboBoxEmployee.Enabled and
      PnlDetail.Enabled then
      DBLookupComboBoxEmployee.SetFocus;
  end
  else
    dxDetailGrid.SetFocus;
  //550286
  AFilter.StartDate := IllnessMessagesDM.IllnessFilter.StartDate;
  AFilter.ActiveTab := IllnessMessagesDM.IllnessFilter.ActiveTab;
  AFilter.Employee_Number := Employee;
  //set filter
  IllnessMessagesDM.IllnessFilter := AFilter;

  IllnessMessagesDM.TableDetail.AfterScroll :=
    IllnessMessagesDM.DefaultAfterScroll;
end;

procedure TIllnessMessagesF.FormShow(Sender: TObject);
begin
  inherited;
  PageControlIllnessChange(Sender);
  //CAR 17-10-2003: 550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
  begin
    IllnessMessagesDM.TableDetail.First;
    if DBLookupComboBoxEmployee.Visible and DBLookupComboBoxEmployee.Enabled
      and PnlDetail.Enabled then // RV084.4.
      DBLookupComboBoxEmployee.SetFocus;
  end
  else
    SetGridOnEmployee(SystemDM.ASaveTimeRecScanning.Employee);
end;

procedure TIllnessMessagesF.dxDBExtLookupEditEmployeeCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
var
  Afilter: TillnessFilter;
  DateTmp: TDateTime;
begin
  inherited;
  if (dxDBExtLookupEditEmployee.Text <> '') then
  begin
    DateTmp := {dxDateEditStart.Date} DateEditStart.DateTime;
    ReplaceTime(DateTmp, 0);
    Afilter.StartDate := DateTmp;
    Afilter.Employee_Number := StrToInt(dxDBExtLookupEditEmployee.Text);
    AFilter.ActiveTab := PerEmployee;
    // set filter
    IllnessMessagesDM.IllnessFilter := AFilter;

    LabelEmplDesc.Caption :=
      IllnessMessagesDM.cdsMaster.FieldByName('DESCRIPTION').AsString;
    //CAR 21-10-2003
    SystemDM.ASaveTimeRecScanning.Employee :=
      IllnessMessagesDM.IllnessFilter.Employee_Number;
    DBLookupComboBoxEmployeeCloseUp(Sender);
  end;
end;
{
procedure TIllnessMessagesF.dxDateEditStartValidate(Sender: TObject;
  var ErrorText: String; var Accept: Boolean);
begin
  inherited;
  SetDateToFilter;
end;
}
procedure TIllnessMessagesF.dxDetailGridClick(Sender: TObject);
begin
  inherited;
  //CAR 21-10-2003
  SystemDM.ASaveTimeRecScanning.Employee :=
    IllnessMessagesDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  DBLookupComboBoxEmployeeCloseUp(Sender);
end;

procedure TIllnessMessagesF.dxGridEnter(Sender: TObject);
begin
  inherited;
 //CAR 21-10-2003
  SystemDM.ASaveTimeRecScanning.Employee :=
    IllnessMessagesDM.TableDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  DBLookupComboBoxEmployeeCloseUp(Sender);
end;

//RV067.4.
procedure TIllnessMessagesF.DBLookupComboBoxEmployeeCloseUp(
  Sender: TObject);
begin
  inherited;
  if DBLookupComboBoxEmployee.KeyValue <> NULL then
    IllnessMessagesDM.FillAbsenceReasons(DBLookupComboBoxEmployee.KeyValue);
end;

// RV084.3.
procedure TIllnessMessagesF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  if DBLookupComboBoxEmployee.Visible and DBLookupComboBoxEmployee.Enabled
    and PnlDetail.Enabled then // RV084.4.
  begin
    DBLookupComboBoxEmployee.SetFocus;
    DBLookupComboBoxEmployeeCloseUp(Sender);
  end;
  // RV084.4.
  if DBLookupComboBoxEmployee.Visible and (not DBLookupComboBoxEmployee.Enabled)
    and PnlDetail.Enabled then
  begin
    if dxDBDateEditStartdate.Visible and dxDBDateEditStartdate.Enabled then
      dxDBDateEditStartdate.SetFocus;
  end;

end;

procedure TIllnessMessagesF.DateEditStartChange(Sender: TObject);
var
  Afilter: TillnessFilter;
  DateTmp: TDateTime;
begin
  inherited;
  DateTmp := DateEditStart.DateTime;
  ReplaceTime(DateTmp, 0);
  Afilter.StartDate := DateTmp;
  if (dxDBExtLookupEditEmployee.Text <> '') then
    Afilter.Employee_Number :=  StrToInt(dxDBExtLookupEditEmployee.Text)
  else
    Afilter.Employee_Number := 0;
  AFilter.ActiveTab := PerEmployee;
  IllnessMessagesDM.IllnessFilter := AFilter;

  dxDBExtLookupEditEmployee.Text :=
    IntToStr(IllnessMessagesDM.IllnessFilter.Employee_Number);
end;

procedure TIllnessMessagesF.DateEditStartCloseUp(Sender: TObject);
begin
  inherited;
  SetDateToFilter;
end;

end.
