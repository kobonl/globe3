(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:2-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning.
*)
unit ReportEmpAvailabilityDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, Provider, DBClient, UPimsConst;

type

  TInt4Array = Array[1..MAX_TBS] of Integer;
  TStr4Array = Array[1..MAX_TBS] of String;

  TReportEmpAvailabilityDM = class(TReportBaseDM)
    QueryEMA: TQuery;
    ClientDataSetAbsRsn: TClientDataSet;
    DataSetProviderABSRSN: TDataSetProvider;
    QueryAbs: TQuery;
    ClientDataSetAbsTot: TClientDataSet;
    DataSetProviderAbsTot: TDataSetProvider;
    QueryAbsTot: TQuery;
    QuerySHS: TQuery;
    ClientDataSetSHS: TClientDataSet;
    DataSetProviderSHS: TDataSetProvider;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure QueryEMAFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
   
    function GetAvail(Empl: Integer; DateEMA: TdateTime): String;
    function GetShift(Empl: Integer; DateEMA: TdateTime): String;

    function GetAbsType(CH: String): String;
    procedure GetAbsenceTotal(Year, Empl: Integer;
      var LastYear_Hol, Norm_Hol, Used_Hol,
      LastYear_WTR, Earned_WTR, Used_WTR,
      LastYear_TFT, Earned_TFT, Used_TFT, Ill_Min: Integer);
    procedure GetPlannedMin(DateEma: TDateTime; Empl, Day, Shift, LastShift: Integer;
      var PlannedHol, PlannedWTR, PlannedTFT: Integer);
  end;

var
  ReportEmpAvailabilityDM: TReportEmpAvailabilityDM;

implementation

uses CalculateTotalHoursDMT, SystemDMT;

{$R *.DFM}

function TReportEmpAvailabilityDM.GetAvail(Empl: Integer; DateEMA: TdateTime): String;
begin
  Result := QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_1').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_2').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_3').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_4').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_5').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_6').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_7').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_8').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_9').AsString + ' ' +
    QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_10').AsString;
end;

function TReportEmpAvailabilityDM.GetShift(Empl: Integer;
  DateEMA: TdateTime): String;
var
  Shift: Integer;
  Found: Boolean;
begin
  Result := '';
  // MR:14-10-2004 Locate + datefield can give errors,
  // depending how the Windows-date-format been set.
  // Use 'ADateFmt'-routines to solve this.
(*
  if not ClientDataSetSHS.Locate('SHIFT_SCHEDULE_DATE',
    VarArrayOf([DateEMA]) , []) then
    Exit;
*)
  SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
  Found := ClientDataSetSHS.Locate('SHIFT_SCHEDULE_DATE',
    VarArrayOf([FormatDateTime(SystemDM.ADateFmt.DateTimeFmt, DateEMA)]),
      []);
  SystemDM.ADateFmt.SwitchDateTimeFmtWindows;

  if Found then
  begin
    Shift := ClientDataSetSHS.FieldByName('SHIFT_NUMBER').ASInteger;
    if Shift > 0 then
      Result := IntToStr(Shift);
//CAR 550276
     if Shift = -1 then
      Result := '-';
  end;
end;

procedure TReportEmpAvailabilityDM.GetAbsenceTotal(Year, Empl: Integer;
  var LastYear_Hol, Norm_Hol, Used_Hol,
  LastYear_WTR, Earned_WTR, Used_WTR,
  LastYear_TFT, Earned_TFT, Used_TFT, Ill_Min: Integer);
begin
  LastYear_Hol := 0; Norm_Hol:=0; Used_Hol := 0; LastYear_WTR := 0;
  Earned_WTR := 0; Used_WTR :=0; LastYear_TFT :=0;
  Earned_TFT :=0; Used_TFT := 0; Ill_Min := 0;
  if not ClientDataSetAbsTot.Locate('EMPLOYEE_NUMBER;ABSENCE_YEAR',
    VarArrayOf([Empl, Year]),[]) then
    Exit;
  LastYear_Hol :=
    Round(ClientDataSetAbsTot.FieldByName('LAST_YEAR_HOLIDAY_MINUTE').asFloat);
  Norm_Hol :=
    Round(ClientDataSetAbsTot.FieldByName('NORM_HOLIDAY_MINUTE').asFloat);
  Used_Hol :=
    Round(ClientDataSetAbsTot.FieldByName('USED_HOLIDAY_MINUTE').asFloat);
  LastYear_WTR :=
    Round(ClientDataSetAbsTot.FieldByName('LAST_YEAR_WTR_MINUTE').asFloat);
  Earned_WTR :=
    Round(ClientDataSetAbsTot.FieldByName('EARNED_WTR_MINUTE').asFloat);
  Used_WTR :=
    Round(ClientDataSetAbsTot.FieldByName('USED_WTR_MINUTE').asFloat);
  LastYear_TFT :=
    Round(ClientDataSetAbsTot.FieldByName('LAST_YEAR_TFT_MINUTE').asFloat);
  Earned_TFT :=
    Round(ClientDataSetAbsTot.FieldByName('EARNED_TFT_MINUTE').asFloat);
  Used_TFT :=
    Round(ClientDataSetAbsTot.FieldByName('USED_TFT_MINUTE').asFloat);
  Ill_Min :=
    Round(ClientDataSetAbsTot.FieldByName('ILLNESS_MINUTE').asFloat);
end;

function TReportEmpAvailabilityDM.GetAbsType(CH: String): String;
begin
  Result := '';
  if ClientDataSetAbsRsn.Locate('ABSENCEREASON_CODE', VarArrayOf([ch]), []) then
    Result := ClientDataSetAbsRsn.FieldByName('ABSENCETYPE_CODE').AsString;
end;

procedure TReportEmpAvailabilityDM.GetPlannedMin(DateEma: TDateTime;
  Empl, Day, Shift, LastShift: Integer;
  var PlannedHol, PlannedWTR, PlannedTFT: Integer);
var
  Plant, Dept, AbsType: String;
  i: Integer;
  TBStr: TSTR4Array;
begin
  Plant := QueryEMA.FieldByName('PLANT_CODE').AsString;
  Dept := QueryEMA.FieldByName('DEPARTMENT_CODE').AsString;
  for i := 1 to SystemDM.MaxTimeblocks do
    TBStr[i] := QueryEMA.FieldByName('AVAILABLE_TIMEBLOCK_'+ IntToStr(i)).AsString;

  if LastShift <> Shift then
  begin
  //CAR 22-7-2003
    ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept, True);
  end;

  for i := 1 to SystemDM.MaxTimeblocks do
    if (TBStr[i] <> '') and (TBStr[I] <> '*' ) and (TBStr[I]  <> '-') then
    begin
      AbsType := GetAbsType(TBStr[i]);
      if AbsType <> '' then
      begin
        if AbsType[1] = HOLIDAYAVAILABLETB then
          PlannedHol := PlannedHol + ATBLengthClass.GetLengthTB(I,Day);
        if AbsType[1] = TIMEFORTIMEAVAILABILITY then
          PlannedWTR := PlannedWTR + ATBLengthClass.GetLengthTB(I,Day);
        if AbsType[1] = WORKTIMEREDUCTIONAVAILABILITY then
          PlannedTFT := PlannedTFT + ATBLengthClass.GetLengthTB(I,Day);
      end;
    end;
end;

procedure TReportEmpAvailabilityDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  //CAR 17-7-2003
  QuerySHS.Prepare;
  // MR:28-04-2005 Open the query!
  QueryAbs.Open;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEMA);
end;

procedure TReportEmpAvailabilityDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //CAR 17-7-2003
  QuerySHS.Close;
  QuerySHS.UnPrepare;
  // MR:28-04-2005 Close the query
  QueryAbs.Close;
end;

procedure TReportEmpAvailabilityDM.QueryEMAFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  if SystemDM.qryPlantDeptTeam.Active and DataSet.Active then
    Accept := SystemDM.qryPlantDeptTeam.Locate('PLANT_CODE',
      DataSet.FieldByName('EMAPLANT_CODE').AsString, [])
  else
    Accept := True;
end;

end.
