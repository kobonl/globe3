(*
  MRA:28-JAN-2010. RV050.8. 889955.
  - Restrict shown information by user (team-per-user).
  MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
  - Make it possible to embed dialogs in Pims, instead of
    open it as a modal dialog.
  - Removed some TTable's that were not used and slowed down the start
    of the report. Also replaced TableEMP by qrySTB_EMP to make it faster.
  MRA:6-FEB-2013. 20013910 Plan on depts give problems.
  - Related to this order.
    - Performance-issue: Get the scheduled timeblocks from main-query.
*)
unit ReportStaffPlanDayDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseDMT, DBTables, Db, ReportStaffPlanDayQRPT;

type
  TReportStaffPlanDayDM = class(TReportBaseDM)
    QueryEMP: TQuery;
    DataSourceAbsence: TDataSource;
    TablePlant: TTable;
    TableShift: TTable;
    TableTeam: TTable;
    TableDept: TTable;
    TableWK: TTable;
    QueryExec: TQuery;
    qrySTB_EMPXXX: TQuery;
    qryRequestEarlyLate: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure QueryEMPFilterRecord(DataSet: TDataSet; var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    function ISNotDeptValid(Dept,Plant, TeamFrom, TeamTo: String) : Boolean;
{    procedure GetTBFromEMP(Empl, Shift: Integer;
      Plant, Workspot, Department: String;
      DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE); }
    procedure RequestEarlyLateOpen(ADateFrom, ADateTo: TDateTime);
    procedure RequestEarlyLateClose;
    function RequestEarlyLateFindEarlyTime(ADate: TDateTime; AEmployeeNumber: Integer; ADefault: TDateTime): TDateTime;
    function RequestEarlyLateFindLateTime(ADate: TDateTime; AEmployeeNumber: Integer; ADefault: TDateTime): TDateTime;
  end;

var
  ReportStaffPlanDayDM: TReportStaffPlanDayDM;

implementation

uses CalculateTotalHoursDMT, ListProcsFRM, SystemDMT;

{$R *.DFM}

// 20013910 Related to this order.
// Performance-issue: Get the scheduled timeblocks from main-query.
(*
procedure TReportStaffPlanDayDM.GetTBFromEMP(Empl, Shift: Integer;
  Plant, Workspot, Department: String;
  DateEMP: TDateTime; var  SCHEDULE_TB: TTBSCHEDULE);
var
  i: Integer;
begin
  for i:= 1 to 4 do
    Schedule_TB[i] := '';
  // RV089.1. Use TQuery instead of TTable to make it faster.
  with qrySTB_EMP do
  begin
    Close;
    ParamByName('PLANT_CODE').AsString := Plant;
    ParamByName('EMPLOYEEPLANNING_DATE').AsDateTime := DateEMP;
    ParamByName('SHIFT_NUMBER').AsInteger := Shift;
    ParamByName('DEPARTMENT_CODE').AsString := Department;
    ParamByName('WORKSPOT_CODE').AsString := Workspot;
    ParamByName('EMPLOYEE_NUMBER').AsInteger := Empl;
    Open;
    if not Eof then
    begin
      Schedule_TB[1] := FieldByName('SCHEDULED_TIMEBLOCK_1').AsString;
      Schedule_TB[2] := FieldByName('SCHEDULED_TIMEBLOCK_2').AsString;
      Schedule_TB[3] := FieldByName('SCHEDULED_TIMEBLOCK_3').AsString;
      Schedule_TB[4] := FieldByName('SCHEDULED_TIMEBLOCK_4').AsString;
    end;
    Close;
  end;
{
  if TableEmp.FindKey([Plant, DateEMP, Shift, Department, Workspot, Empl]) then
  begin
    for i := 1 to 4 do
      Schedule_TB[I] :=
        TableEmp.FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
  end;
}

end;
*)

function TReportStaffPlanDayDM.ISNotDeptValid(Dept,Plant, TeamFrom,
  TeamTo: String) : Boolean;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' +
    '  COUNT(TEAM_CODE) AS COUNTTEAM ' +
    'FROM ' +
    '  DEPARTMENTPERTEAM ' +
  //Car 20.2.2004 add doublequote function
    'WHERE ' +
    '  PLANT_CODE = ''' + DoubleQuote(Plant) + '''' +
    '  AND DEPARTMENT_CODE = ''' + DoubleQuote(DEPT) + '''' +
    '  AND TEAM_CODE >= ''' + DoubleQuote(TEAMFrom) + '''' +
    '  AND TEAM_CODE <= ''' + DoubleQuote(TeamTo) + '''';
  QueryExec.Active := False;
  QueryExec.SQL.Clear;
  QueryExec.SQL.Add(Selectstr);
  QueryExec.ExecSQL;
  QueryExec.Active := True;
  // Pims -> Oracle
  Result := QueryExec.FieldByName('COUNTTEAM').AsFloat > 0;
//  Result := QueryExec.RecordCount > 0 ;
end;

procedure TReportStaffPlanDayDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV050.8.
  SystemDM.PlantTeamFilterEnable(QueryEMP);
end;

procedure TReportStaffPlanDayDM.QueryEMPFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV050.8.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TReportStaffPlanDayDM.RequestEarlyLateOpen(ADateFrom,
  ADateTo: TDateTime);
begin
  with qryRequestEarlyLate do
  begin
    Close;
    ParamByName('DATEFROM').AsDateTime := ADateFrom;
    ParamByName('DATETO').AsDateTime := ADateTo;
    Open;
  end;
end;

procedure TReportStaffPlanDayDM.RequestEarlyLateClose;
begin
  qryRequestEarlyLate.Close;
end;

function TReportStaffPlanDayDM.RequestEarlyLateFindEarlyTime(
  ADate: TDateTime; AEmployeeNumber: Integer;
  ADefault: TDateTime): TDateTime;
begin
  Result := ADefault;
  with qryRequestEarlyLate do
  begin
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
    while not Eof do
    begin
      if FieldByName('REQUEST_DATE').AsDateTime = ADate then
        if FieldByName('REQUESTED_EARLY_TIME').AsString <> '' then
          Result := FieldByName('REQUESTED_EARLY_TIME').AsDateTime;
      Next;
    end;
  end;
end;

function TReportStaffPlanDayDM.RequestEarlyLateFindLateTime(
  ADate: TDateTime; AEmployeeNumber: Integer;
  ADefault: TDateTime): TDateTime;
begin
  Result := ADefault;
  with qryRequestEarlyLate do
  begin
    Filtered := False;
    Filter := 'EMPLOYEE_NUMBER = ' + IntToStr(AEmployeeNumber);
    Filtered := True;
    while not Eof do
    begin
      if FieldByName('REQUEST_DATE').AsDateTime = ADate then
        if FieldByName('REQUESTED_LATE_TIME').AsString <> '' then
          Result := FieldByName('REQUESTED_LATE_TIME').AsDateTime;
      Next;
    end;
  end;
end;

end.
