inherited RevenueF: TRevenueF
  Left = 221
  Top = 184
  Width = 770
  Height = 487
  Caption = 'Revenue on business unit'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlMasterGrid: TPanel
    Top = 126
    Width = 762
    Height = 11
    TabOrder = 0
    Visible = False
    inherited spltMasterGrid: TSplitter
      Top = 7
      Width = 760
    end
    inherited dxMasterGrid: TdxDBGrid
      Tag = 1
      Width = 760
      Height = 6
      Visible = False
    end
  end
  inherited pnlDetail: TPanel
    Top = 332
    Width = 762
    Height = 121
    TabOrder = 3
    object Label4: TLabel
      Left = 24
      Top = 24
      Width = 23
      Height = 13
      Caption = 'Date'
    end
    object Label5: TLabel
      Left = 24
      Top = 64
      Width = 43
      Height = 13
      Caption = 'Revenue'
    end
    object AmountRevenue: TdxDBEdit
      Left = 80
      Top = 64
      Width = 121
      TabOrder = 0
      DataField = 'REVENUE_AMOUNT'
      DataSource = RevenueDM.DataSourceDetail
    end
    object DateRevenue: TDBPicker
      Tag = 1
      Left = 80
      Top = 24
      Width = 121
      Height = 21
      CalAlignment = dtaLeft
      Date = 41395.6816250579
      Time = 41395.6816250579
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 1
      DataField = 'REVENUE_DATE'
      DataSource = RevenueDM.DataSourceDetail
    end
  end
  inherited pnlDetailGrid: TPanel
    Top = 137
    Width = 762
    Height = 195
    inherited spltDetail: TSplitter
      Top = 191
      Width = 760
    end
    inherited dxDetailGrid: TdxDBGrid
      Tag = 1
      Width = 760
      Height = 190
      Bands = <
        item
          Width = 458
        end>
      DefaultLayout = False
      KeyField = 'EMPLOYEE_NUMBER'
      DataSource = RevenueDM.DataSourceDetail
      OptionsDB = [edgoCanAppend, edgoCanInsert, edgoCanNavigation, edgoUseBookmarks]
      ShowBands = True
      object dxDetailGridColumnDate: TdxDBGridDateColumn
        Alignment = taLeftJustify
        Caption = 'Date'
        DisableEditor = True
        Width = 108
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REVENUE_DATE'
      end
      object dxDetailGridColumnRevenue: TdxDBGridColumn
        Alignment = taLeftJustify
        Caption = 'Revenue'
        DisableEditor = True
        Width = 350
        BandIndex = 0
        RowIndex = 0
        FieldName = 'REVENUE_AMOUNT'
      end
    end
  end
  object pnlTeamPlantShift: TPanel [4]
    Left = 0
    Top = 26
    Width = 762
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 7
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 762
      Height = 100
      Align = alClient
      Caption = 'Business unit'
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 24
        Width = 62
        Height = 13
        Caption = 'Business unit'
      end
      object Label3: TLabel
        Left = 384
        Top = 24
        Width = 22
        Height = 13
        Caption = 'Year'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 384
        Top = 56
        Width = 27
        Height = 13
        Caption = 'Week'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cmbPlusBU: TComboBoxPlus
        Left = 88
        Top = 22
        Width = 201
        Height = 19
        ColCount = 56
        Ctl3D = False
        DefaultRowHeight = 19
        DropDownAlign = Left
        ListColor = clWindow
        ListDefaultDrawing = True
        ListFont.Charset = DEFAULT_CHARSET
        ListFont.Color = clWindowText
        ListFont.Height = -11
        ListFont.Name = 'Tahoma'
        ListFont.Style = []
        ListCursor = crDefault
        ButtonCursor = crDefault
        Options = [loColLines, loThumbTracking]
        ParentCtl3D = False
        TabOrder = 0
        TitleColor = clBtnFace
        OnChange = cmbPlusBUChange
      end
      object dxSpinEditYear: TdxSpinEdit
        Left = 424
        Top = 22
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnChange = dxSpinEditYearChange
        MaxValue = 2099
        MinValue = 1950
        Value = 1950
        StoredValues = 48
      end
      object dxSpinEditWeek: TdxSpinEdit
        Left = 424
        Top = 54
        Width = 65
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnChange = dxSpinEditWeekChange
        MaxValue = 53
        MinValue = 1
        Value = 1
        StoredValues = 48
      end
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = False
        WholeRow = False
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <
          item
            Item = dxBarButtonFirst
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonPrior
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonNext
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonLast
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarBDBNavInsert
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavDelete
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavPost
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarBDBNavCancel
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonEditMode
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonRecordDetails
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSort
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonSearch
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCustCol
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonShowGroup
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExpand
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonCollapse
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonResetColumns
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            BeginGroup = True
            Item = dxBarButtonExportAllHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionHTML
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportAllXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarButtonExportSelectionXLS
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    Left = 648
    Top = 72
    DockControlHeights = (
      0
      0
      26
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarBDBNavDelete: TdxBarDBNavButton
      OnClick = dxBarBDBNavDeleteClick
    end
    inherited dxBarBDBNavPost: TdxBarDBNavButton
      OnClick = dxBarBDBNavPostClick
    end
    inherited dxBarBDBNavCancel: TdxBarDBNavButton
      OnClick = dxBarBDBNavCancelClick
    end
  end
  inherited dxBarDBNavigator: TdxBarDBNavigator
    Left = 688
    Top = 72
  end
  inherited StandardMenuActionList: TActionList
    Left = 648
    Top = 32
  end
  inherited dsrcActive: TDataSource
    Left = 688
    Top = 32
  end
end
