(*
  Changes:
  MR:05-02-2004 Closing of Not-Complete-Scans should be possible by filtering
                on a date, so more than 1 record can be closed at a time.
                Order 550300.
  MR:23-03-2004 Some changes for Order 550300.
  MR:24-03-2004 Some changes for Order 550300.
  MR:14-05-2004 Workspot-field changed to other component for Order 550314.
  SO:05-AUG-2010 RV067.8. 550500
    Optimise not completed time recording scans
  SO:27-08-2010 RV067.11. 550500
  - time components replacement
  MRA:23-SEP-2010 RV067.MRA.37 Change for 550500
  - When the detail grid is clicked, it is in edit-mode.
    It should not do this, only if clicked in datetime-out field.
  MRA:27-SEP-2010 RV068.2. Bugfix.
  - When changing time for an open scan, it does not fill the
    date-part, it stays 0!
  MRA:3-NOV-2010 RV076.5. Bugfix.
  - Time rec scanning
    - Not Complete-tab:
      - When inserting a new open scan it gives an error
        about wrong entered period.
        Reason: It is setting autom. the end-date during before-post.
      - It must not do this automatically!
  MRA:4-NOV-2010 RV076.7.
  - In Not-Complete-Tab it should be NOT allowed to add/delete records!
    Otherwise it can lead to multiple OPEN scan-records!
  MRA:8-NOV-2010 RV077.3. Bugfix. RFL-only change.
  - A check is made if an employee is a non-scanner,
    if so, then give a warning when trying to make a
    mutation. Otherwise it can lead to wrong hours,
    or overwrite hours made because of 'prod. hrs.
    based on standard planning.'.
  MRA:10-NOV-2010 RV078.1.
  - GridBase/TimeRecScanning
    - Removed delete-procedure from Gridbase (as it originally was).
    - Removed delete-call here, it is handled in TimeRecScanningDMT.
  MRA:18-NOV-2010 RV080.2.
  - When date-out-time is entered then the date-in-date is
    copied to date-out-date. But when you also change e.g.
    the workspot, then the date-out-date is changed back to
    31-12-1899!
  MRA:1-DEC-2010 RV082.4. Changes for SO-550491.
  - Do not allow changes at all for employees of type
    'Autom. scans based on standard planning'.
  MRA:2-MAY-2011 RV092.6. Bugfix.
  - Timerecording and open scans
    - Closing scans using 'Close Scans'-button
      does not work anymore.
    - First check for errors, without changing anything.
  MRA:25-JUL-2012 20013472. Change.
  - There is no option to sort in the grid in
    Not-completed-scans.
  - Change: Add sort-button and sort-options.
  MRA:12-OCT-2012 20013489 Overnight-Shift-System
  - For tab-page 'Scans per Employee':
    - Changed 'Date' to 'Shift Date'
    - Show scans for the ShiftDate based on selected (shift-) date.
    - Make it possible to enter a date for today and for the next date, not
      based on the selected (shift-) date.
  MRA:21-NOV-2012 20013288
  - Show MUTATIONDATE and MUTATOR in grid for tab-page Scans-Per-Employee.
    - Only for displaying. Call them 'Timestamp' and 'User'.
  MRA:25-MAR-2013 20013035 Do not show inactive employees
  - Added IsEmployeeActive-function + DoActiveAction-function that determines
    if an employee is active or not.
  MRA:1-MAY-2013 20012944 Show weeknumbers
  - Some components of type TdxDateEdit are replaced by TDateTimePicker:
    - dxDateEditScan
    - dxDateEditNotComplete
    - dxDateEditEarlyLate
  MRA:10-OCT-2013 20014327
  - Make use of Overnight-Shift-System optional.
  MRA:11-FEB-2014 20011800
  - Final Run System
  - Do not allow changes before last-export-date.
  MRA:28-APR-2014 20011800.70 Rework
  - Final Run System
    - It was still possible to enter a scan, even without using Add-button,
      by using the details-edit-boxes. These must be disabled!
  MRA:16-JAN-2015 20012155 (rework)
  - Be sure the second tab is selected in design-mode, or it will go wrong
    when called from Employee Reg. dialog.
  MRA:17-MAR-2015 20015346
  - Do not show scan-seconds in reports/dialogs
  MRA:23-MAR-2015 20015346
  - Because of the following problem we show DateIn/DateOut in seconds:
    - When previous scans ends at 12:51:22 (hrs:min:secs) then
      when we only show minutes we get an error when trying to
      enter 12:51 to 12:52 for next scan, because 12:51:00 is earlier then
      12:51:22.
  MRA:15-APR-2015 20013035 Part 2
  - Do not show inactive employees
  - Add 'Show only active' to show only active employees
  MRA:11-APR-2016 PIM-95
  - Translate issue with 'shift date' for overnight-shift-system.
  MRA:2-MAY-2016 ABS-27382
  - Show only active (employees)-checkbox must be default checked.
  MRA:17-MAY-2016 ABS-28216
  - Show only active problem:
    - When swithing between TimeRecScanning-dialog and HoursPerEmployee-dialog
      it changes the employee.
  MRA:1-NOV-2017 PIM-322
  - It should be possible to use the TimeRegScan-dialog in Pims but only
    for 2 tabs (Open scans and Early/Late) under condition:
    - TimeRegScan-dialog is made non-editable (defined via AdmTool and menu
      per Usergroup)
  - Purpose: In that way, in production, employees can indicate themselves if
    they are starting early or late, or close a scan themselves.
  - To solve a bug: Switch all pages to get it right with post/cancel-buttons,
    otherwise they stay disabled in NotComplete-tab, when this
    tab is first opened.
  MRA:3-NOV-2017 PIM-322 Rework
  - Only Early/Late-tab should be editable, the other 2 not.
  - In Early-Late-dialog: Drop-down-list must be sorted on Name. During editing,
    show always last employee at the bottom.
  MRA:3-NOV-2017 PIM-323
  - In Early/Late-Tab, do not check on an existing
    scan during entry/change of a record, so always allow a change.
  - In Early/Late-Tab: Show line in red when there was a first scan found for
    employee that is later than the Early-date.
*)
unit TimeRecScanningFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids, dxEditor,
  dxExEdtr, dxEdLib, DBCtrls, dxExGrEd, dxExELib, dxDBEdtr, dxDBELib,
  dxLayout, dxDBTLCl, dxGrClms, Mask, DBPicker, SystemDMT;

type
  TTimeRecScanningF = class(TGridBaseF)
    dxDetailGridColumnDateIN: TdxDBGridDateColumn;
    GroupBoxTimeRecordin: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    GroupBox4: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    GroupBox7: TGroupBox;
    Label10: TLabel;
    TabSheet1: TTabSheet;
    GroupBox5: TGroupBox;
    Label7: TLabel;
    Label6: TLabel;
    dxDBExtLookupEditEmployee: TdxDBExtLookupEdit;
    TabSheet4: TTabSheet;
    GroupBox6: TGroupBox;
    Label9: TLabel;
    dxMasterGridColumnRequestDate: TdxDBGridDateColumn;
    dxMasterGridColumnEarlyTime: TdxDBGridTimeColumn;
    dxMasterGridColumnLateTime: TdxDBGridTimeColumn;
    dxMasterGridColumnEmployee: TdxDBGridLookupColumn;
    dxDetailGridColumnDateOut: TdxDBGridDateColumn;
    dxDetailGridColumnEmployee: TdxDBGridColumn;
    dxDetailGridColumnPlant: TdxDBGridColumn;
    dxDBLookupEditEmployee: TDBLookupComboBox;
    DBLookupComboBoxPlant: TDBLookupComboBox;
    DBLookupComboBoxJobCode: TDBLookupComboBox;
    DBLookupComboBoxShift: TDBLookupComboBox;
    LabelEmpDesc: TLabel;
    dxDBGridLayoutListEmployee: TdxDBGridLayoutList;
    dxDBGridLayoutListEmployeeItemEmployee: TdxDBGridLayout;
    dxDBGridLayoutListShift: TdxDBGridLayoutList;
    dxDBGridLayoutListShiftItem1: TdxDBGridLayout;
    GroupBoxEarlyLate: TGroupBox;
    GroupBoxEarlyLateDate: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    dxDBDateEditEarlyLateDate: TdxDBDateEdit;
    GroupBoxEarlyLateRequest: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    dxDBTimeEditEarlyLateEary: TdxDBTimeEdit;
    dxDBTimeEditEarlyLateLate: TdxDBTimeEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    dxDetailGridColumnShift: TdxDBGridLookupColumn;
    lblDateNotComplete: TLabel;
    cBoxAllNotComplete: TCheckBox;
    btnCloseNotComplete: TButton;
    dxDBGridLayoutListWorkspot: TdxDBGridLayoutList;
    dxDBGridLayoutListWorkspotItem: TdxDBGridLayout;
    dxDBExtLookupEditWorkspot: TdxDBExtLookupEdit;
    lblWorkspotDescription: TLabel;
    dxDetailGridColumnWorkspot: TdxDBGridColumn;
    dxDetailGridColumnJob: TdxDBGridColumn;
    dxTimeEditIn: TdxDBTimeEdit;
    dxDateEditIn: TdxDBDateEdit;
    dxDateEditOut: TdxDBDateEdit;
    dxTimeEditOut: TdxDBTimeEdit;
    dxDetailGridColumnMutationDate: TdxDBGridColumn;
    dxDetailGridColumnMutator: TdxDBGridColumn;
    DateEditScan: TDateTimePicker;
    DateEditNotComplete: TDateTimePicker;
    DateEditEarlyLate: TDateTimePicker;
    cBoxShowOnlyActive: TCheckBox;
    procedure PageControl2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    procedure dxDateEditScanEnter(Sender: TObject);
//    procedure dxDateEditEarlyLateEnter(Sender: TObject);
    procedure dxDBDateEditEarlyLateDateEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ChangeFilter(Sender: TObject);
    procedure ChangeTablesFilter(Sender: TObject; var Text: String;
      var Accept: Boolean);
    procedure dxTimeEditOutExit(Sender: TObject);
    procedure dxDBDateEditOutEnter(Sender: TObject);
    procedure dxDBDateEditInDateChange(Sender: TObject);
    procedure dxDBDateEditOutDateChange(Sender: TObject);
    procedure dxDetailGridClick(Sender: TObject);
    procedure DBLookupComboBoxPlantCloseUp(Sender: TObject);
    procedure cBoxAllNotCompleteClick(Sender: TObject);
//    procedure dxDateEditNotCompleteEnter(Sender: TObject);
    procedure btnCloseNotCompleteClick(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure dxDBExtLookupEditWorkspotPopup(Sender: TObject;
      const EditText: String);
    procedure dxDBExtLookupEditWorkspotEnter(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotExit(Sender: TObject);
    procedure dxDBExtLookupEditWorkspotKeyPress(Sender: TObject;
      var Key: Char);
    procedure dxGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure DBLookupComboBoxJobCodeEnter(Sender: TObject);
    procedure dxDateEditOutEnter(Sender: TObject);
    procedure dxDateEditInDateChange(Sender: TObject);
    procedure dxDateEditOutDateChange(Sender: TObject);
    procedure dxTimeEditOutEnter(Sender: TObject);
    procedure dxTimeEditOutChange(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure DateEditScanEnter(Sender: TObject);
    procedure DateEditNotCompleteEnter(Sender: TObject);
    procedure DateEditEarlyLateEnter(Sender: TObject);
    procedure cBoxShowOnlyActiveClick(Sender: TObject);
    procedure dxMasterGridCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: String;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
  private
    { Private declarations }
    procedure CopyDateIn;
    procedure DoActiveAction;
    procedure EnableComponents(AEnable: Boolean);
    procedure MyChangeTablesFilter; // 20013035
//    procedure EnableEditable;
  public
    { Public declarations }
    ATime: TDateTime;
    FOldTime: TDateTime;
    procedure ShowMasterGrid;
    procedure ShowDetailNotCompleteGrid;
    procedure ShowDetailScanPerEmployeeGrid;
    procedure EnableDateTimeOut(Action: Boolean);
    procedure InitEmptySavedValue;
//    procedure AssignDxDateEditScan(ANewDate: TDateTime);
    procedure AssignDateEditScan(ANewDate: TDateTime);
    procedure AssignCBoxAllNotComplete(AValue: Boolean);
    function CBoxAllNotCompleteResult: Boolean;
  end;

function TimeRecScanningF: TTimeRecScanningF;

var
  TimeRecScanningF_HND: TTimeRecScanningF;
  pnlMasterGridHeight: Integer;

implementation

uses
  TimeRecScanningDMT, UPimsConst, UGlobalFUnctions,
  UPimsMessageRes, DialogNotCompleteDateFRM;

{$R *.DFM}

function TimeRecScanningF: TTimeRecScanningF;
begin
  if (TimeRecScanningF_HND = nil) then
    TimeRecScanningF_HND := TTimeRecScanningF.Create(Application);
  Result := TimeRecScanningF_HND;
end;

procedure TTimeRecScanningF.PageControl2Change(Sender: TObject);
var
  AScanFilter: TDataFilter;
begin
  inherited;
  if TimeRecScanningDM.TableDetail.State in [dsEdit, dsInsert] then
    if DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo]) = mrYes then
      TimeRecScanningDM.TableDetail.Post
    else
      TimeRecScanningDM.TableDetail.Cancel;

  AScanFilter := TimeRecScanningDM.ScanFilter;
  //CAR 18-8-2003 - reset the message AfterScroll
  TimeRecScanningDM.TableDetail.AfterScroll := Nil;
  //CAR 550274 -TEAM SELECTION
  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
    TimeRecScanningDM.TableDetail.OnFilterRecord := Nil;

  case TScanFilter(PageControl2.ActivePageIndex) of
  NotComplete : // Not Completed Scan
    begin
      dxDetailGrid.Tag := 2; // RV076.7. No Insert/Delete allowed!
      ActiveGridRefresh; // RV076.7. Refresh the toolbar.
      ShowDetailNotCompleteGrid;
      //RV067.8.
      EnableDateTimeOut(True);
      AScanFilter.ScanFilter := NotComplete;
      // MR:23-03-2004
      if not cBoxAllNotComplete.Checked then
        AScanFilter.StartDate := DateEditNotComplete.DateTime; // dxDateEditNotComplete.Date;
      dxDBLookupEditEmployee.Enabled := True;
      // MR:08-10-2003 Date-in is editable for this tabpage
      dxDateEditIn.Enabled := True;
      dxDateEditIn.Color := dxTimeEditIn.Color;
      dxBarButtonEditMode.Visible := ivNever;
      //CAR 550274 -TEAM SELECTION
      if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
        TimeRecScanningDM.TableDetail.OnFilterRecord :=
          TimeRecScanningDM.FilterRecord;
    end;
  ScanPerEmployee : // Scans Per Employee
    begin
      dxDetailGrid.Tag := 1; // RV076.7. Insert/Delete is allowed.
      ActiveGridRefresh; // RV076.7. Refresh the toolbar.
      ShowDetailScanPerEmployeeGrid;
      EnableDateTimeOut(True);
  //CAR 7-5-2003
  //CAR 17-10-2003 : 550262
      InitEmptySavedValue;
      AScanFilter.StartDate := SystemDM.ASaveTimeRecScanning.DateScanning;
      AScanFilter.EmployeeNumber :=
        SystemDM.ASaveTimeRecScanning.Employee;
      TimeRecScanningDM.cdsFilterEmployee.FindKey([AScanFilter.EmployeeNumber]);
      AScanFilter.PlantCode :=
        TimeRecScanningDM.cdsFilterEmployee.FieldByName('PLANT_CODE').AsString; // 20011800
      // MR:23-1-2004
      AScanFilter.IsScanning :=
        (TimeRecScanningDM.cdsFilterEmployee.
          FieldByName('IS_SCANNING_YN').AsString = 'Y');
      // RV077.3.
      AScanFilter.BookProdHrs :=
        (TimeRecScanningDM.cdsFilterEmployee.
          FieldByName('BOOK_PROD_HRS_YN').AsString = 'Y');
      LabelEmpDesc.Caption :=
        TimeRecScanningDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
      AScanFilter.ShiftNumber := SystemDM.ASaveTimeRecScanning.Shift;
      TimeRecScanningDM.cdsFilterShift.FindKey(
        [TimeRecScanningDM.DataSourceDetail.
        DataSet.FieldByName('PLANT_CODE').AsString,
        AScanFilter.ShiftNumber]);
      {dxDateEditScan.Date} DateEditScan.DateTime := AScanFilter.StartDate;
      AScanFilter.ScanFilter := ScanPerEmployee;
      dxDBLookupEditEmployee.Enabled := False;
      // MR:12-09-2003 Date-in should never be editable: it will
      // be filled by the program with the selected (filter-) date.
      // But if user wants to enter another data, this is not allowed, it
      // will otherwise result in the error 'Record/key deleted' (8708).

      // 20013489 Make it enabled, so user can enter also a next date for
      // an overnight-shift.
      if SystemDM.UseShiftDateSystem then // 20014327
      begin
        dxDateEditIn.Enabled := True;
        dxDateEditIn.Color := dxTimeEditIn.Color;
      end
      else
        dxDateEditIn.Enabled := False;
      dxBarButtonEditMode.Visible := ivNever;
      //18-8-2003 car - set the message - save old values
      TimeRecScanningDM.TableDetail.AfterScroll :=
        TimeRecScanningDM.DefaultAfterScroll;
      TimeRecScanningDM.ScanFilter := AScanFilter; // 20011800
      DoActiveAction; // 20013035
    end;
    EarlyLate :
    begin
      ShowMasterGrid;
      AScanFilter.StartDate := Trunc(DateEditEarlyLate.DateTime); // Now; // PIM-323
      AScanFilter.ScanFilter := EarlyLate;
      //car 15-9-2003 user rights changes
      if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
      begin
        dxBarButtonEditMode.Visible := ivAlways;
        dxBarButtonEditMode.Enabled := True;
      end;
    end;
  end;
  AScanFilter.Active := True;
  TimeRecScanningDM.ScanFilter := AScanFilter;
  // PIM-322 Set non-editable only for ScansPerEmployee-tab-page
  if (Form_Edit_YN = ITEM_VISIBIL) then
  begin
    if SystemDM.IsAdmin then
    begin
      case TScanFilter(PageControl2.ActivePageIndex) of
      NotComplete :
        begin
          SetNotEditable; 
          ShowDetailNotCompleteGrid;
        end;
      ScanPerEmployee : // Scans Per Employee
        begin
          SetNotEditable;
        end;
      EarlyLate :
        begin
          SetEditableNow;
          ShowMasterGrid;
        end;
      end; // case
    end;
  end; // if
end;

procedure TTimeRecScanningF.FormCreate(Sender: TObject);
  // ABS-28216 Set this property here based on checkbox.
  procedure InitFilter;
  var
    AScanFilter: TDataFilter;
  begin
    AScanFilter := TimeRecScanningDM.ScanFilter;
    AScanFilter.StartDate := Date;
    AScanFilter.ShowOnlyActive := cBoxShowOnlyActive.Checked;
    TimeRecScanningDM.ScanFilter := AScanFilter;
  end;
begin
  inherited;
  TimeRecScanningDM := CreateFormDM(TTimeRecScanningDM);
  InitFilter; // ABS-28216 Set this property here based on checkbox.
  // Open the TClientDataSets
  TimeRecScanningDM.cdsFilterEmployee.Open;
  TimeRecScanningDM.cdsFilterShift.Open;
  TimeRecScanningDM.QueryEmployeeLU.Open;
  TimeRecScanningDM.QueryPlantLU.Open;
  // MR:04-06-2004 Instead of this query, we use 'TableWorkspot'
//  TimeRecScanningDM.QueryWorkspotLU.Open;
  TimeRecScanningDM.QueryJobLU.Open;
  TimeRecScanningDM.QueryShiftLU.Open;
  {dxDateEditScan.Date} DateEditScan.DateTime := Now;
  // dxDateEditEarlyLate.Date := Now;
  DateEditEarlyLate.DateTime := Now;
  pnlMasterGridHeight := pnlMasterGrid.Height;
  // MR:05-02-2004
  {dxDateEditNotComplete.Date} DateEditNotComplete.DateTime := Now;
  cBoxAllNotCompleteClick(Sender);
  GroupBoxEarlyLate.Align := alClient;
  lblWorkspotDescription.Caption := '';
  // PIM-95
  if SystemDM.UseShiftDateSystem then
    Label6.Caption := SPimsShiftDate
  else
    Label6.Caption := SDate;
end;

procedure TTimeRecScanningF.ShowDetailNotCompleteGrid;
begin
//  TimeRecScanningDM.TableDetail :=
//    TimeRecScanningDM.TableDetail;
  ActiveGrid := dxDetailGrid;
  pnlMasterGrid.Align := alClient;
  pnlDetailGrid.Visible := False;
  pnlMasterGrid.Align := alTop;
  pnlMasterGrid.Height := pnlMasterGridHeight;
  pnlDetailGrid.Visible := True;
  GroupboxTimeRecordIn.Visible := True;
  GroupBoxEarlyLate.Visible := False;
end;

procedure TTimeRecScanningF.ShowDetailScanPerEmployeeGrid;
begin
//  TimeRecScanningDM.TableDetail :=
//    TimeRecScanningDM.TableDetail2;
  ActiveGrid := dxDetailGrid;
  pnlMasterGrid.Align := alClient;
  pnlDetailGrid.Visible := False;
  pnlMasterGrid.Align := alTop;
  pnlMasterGrid.Height := pnlMasterGridHeight;
  pnlDetailGrid.Visible := True;
  GroupboxTimeRecordIn.Visible := True;
  GroupBoxEarlyLate.Visible := False;
end;

procedure TTimeRecScanningF.ShowMasterGrid;
begin
  ActiveGrid := dxMasterGrid;
  pnlMasterGrid.Align := alClient;
  pnlDetailGrid.Visible := False;
  GroupboxTimeRecordIn.Visible := False;
  GroupBoxEarlyLate.Visible := True;
  // PIM-323 No sorting
  dxMasterGrid.OptionsBehavior := [edgoCaseInsensitive, edgoDragScroll,
    edgoEnterShowEditor, edgoImmediateEditor, edgoShowHourGlass,
    edgoTabThrough, edgoVertThrough];
  dxMasterGrid.OptionsView := dxMasterGrid.OptionsView  + [edgoRowSelect];
end;

procedure TTimeRecScanningF.InitEmptySavedValue;
begin
 //car 7-5-2003 initialize default saved value
 //CAR 17-10-2003 : 550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    if (not TimeRecScanningDM.cdsFilterEmployee.IsEmpty) then
    begin
      TimeRecScanningDM.cdsFilterEmployee.First;
      SystemDM.ASaveTimeRecScanning.Employee :=
        TimeRecScanningDM.cdsFilterEmployee.
          FieldByName('EMPLOYEE_NUMBER').AsInteger;
    end;

  if SystemDM.ASaveTimeRecScanning.Shift = -1 then
    if (not TimeRecScanningDM.cdsFilterShift.IsEmpty) then
    begin
      TimeRecScanningDM.cdsFilterShift.First;
      SystemDM.ASaveTimeRecScanning.Shift :=
        TimeRecScanningDM.cdsFilterShift.
          FieldByName('SHIFT_NUMBER').AsInteger;
    end;
  if SystemDM.ASaveTimeRecScanning.DateScanning = EncodeDate(1900,1,1) then
    SystemDM.ASaveTimeRecScanning.DateScanning := Now;
end;

procedure TTimeRecScanningF.FormShow(Sender: TObject);
begin
  if dxDetailGrid.DataSource = nil then
    dxDetailGrid.DataSource := TimeRecScanningDM.DataSourceDetail;
  if dxMasterGrid.DataSource = nil then
    dxMasterGrid.DataSource := TimeRecScanningDM.DataSourceMaster;
  inherited;
  // This sets the top-buttons to the correct state
  dxMasterGrid.SetFocus;
  // PIM-322 Switch all pages to get it right with post/cancel-buttons,
  //         otherwise they stay disabled in NotComplete-tab, when this
  //         tab is first opened.
  if (Form_Edit_YN = ITEM_VISIBIL) then
    if SystemDM.IsAdmin then
    begin
      PageControl2.ActivePageIndex := Integer(EarlyLate);
      PageControl2Change(Sender);
      PageControl2.ActivePageIndex := Integer(NotComplete);
      PageControl2Change(Sender);
    end;
  // Set default-tabpage
  PageControl2.ActivePageIndex := Integer(ScanPerEmployee);
  // Goto correct tabpage
  PageControl2Change(Sender);
  // ABS-28216 Do not do this here!
//  cBoxShowOnlyActiveClick(cBoxShowOnlyActive); // ABS-27382
end;
{
procedure TTimeRecScanningF.dxDateEditScanEnter(Sender: TObject);
begin
  inherited;
  if dxDateEditScan.Date < 0 then
    dxDateEditScan.Date := Now;
  DoActiveAction; // 20013035
end;
}
{
procedure TTimeRecScanningF.dxDateEditEarlyLateEnter(Sender: TObject);
begin
  inherited;
  if dxDateEditEarlyLate.Date < 0 then
    dxDateEditEarlyLate.Date := Now;
end;
}
procedure TTimeRecScanningF.dxDBDateEditEarlyLateDateEnter(
  Sender: TObject);
begin
  inherited;
  if dxDBDateEditEarlyLateDate.date < 0 then
    dxDBDateEditEarlyLateDate.date := Now;
end;

procedure TTimeRecScanningF.FormDestroy(Sender: TObject);
begin
  inherited;
  TimeRecScanningF_HND := nil;
end;

procedure TTimeRecScanningF.ChangeFilter(Sender: TObject);
var
  AScanFilter: TDataFilter;
begin
  AScanFilter := TimeRecScanningDM.ScanFilter;
  case AScanFilter.ScanFilter of
    // MR:05-02-2004 Filter on date if neeeded.
    NotComplete :
    begin
      if (Sender is TCheckBox) then
        if not cBoxAllNotComplete.Checked then
        begin
          if TimeRecScanningDM.TableDetail.RecordCount > 0 then
            {dxDateEditNotComplete.Date} DateEditNotComplete.DateTime :=
              Trunc(TimeRecScanningDM.TableDetailDATETIME_IN.AsDateTime);
        end;
      AScanFilter.NotCompleteFilterActive := not cBoxAllNotComplete.Checked;
      AScanFilter.StartDate := {dxDateEditNotComplete.Date} DateEditNotComplete.DateTime;
    end;
    ScanPerEmployee :
    begin
//CAR 7-5-2003
//CAR 17-10-2003 : 550262
      AScanFilter.ShowOnlyActive := cBoxShowOnlyActive.Checked;
      AScanFilter.StartDate := DateEditScan.DateTime; {dxDateEditScan.Date;}
      SystemDM.ASaveTimeRecScanning.DateScanning := DateEditScan.DateTime; {dxDateEditScan.Date;}
      TimeRecScanningDM.ScanFilter := AScanFilter; // 20011305
      DoActiveAction; // 20013035
    end;
    EarlyLate : AScanFilter.StartDate := {dxDateEditEarlyLate.Date} DateEditEarlyLate.DateTime;
  end;
  TimeRecScanningDM.ScanFilter := AScanFilter;
  // MR:05-02-2004 Set the button for 'not complete'.
  if AScanFilter.ScanFilter = NotComplete then
    if TimeRecScanningDM.TableDetail.Active then
      if not cBoxAllNotComplete.Checked then // MR:24-03-2004
        btnCloseNotComplete.Enabled :=
          (TimeRecScanningDM.TableDetail.RecordCount > 0);
end;

procedure TTimeRecScanningF.EnableDateTimeOut(Action: Boolean);
begin
  dxDateEditOut.Enabled := Action;
  dxTimeEditOut.Enabled := Action;
end;

// 20013035
procedure TTimeRecScanningF.MyChangeTablesFilter;
var
  AFilter: TDataFilter;
begin
  LabelEmpDesc.Caption :=
    TimeRecScanningDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString;
  AFilter := TimeRecScanningDM.ScanFilter;
  AFilter.EmployeeNumber :=
    TimeRecScanningDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  // MR:23-1-2004
  AFilter.IsScanning :=
    (TimeRecScanningDM.cdsFilterEmployee.
      FieldByName('IS_SCANNING_YN').AsString = 'Y');
  // RV077.3.
  AFilter.BookProdHrs :=
    (TimeRecScanningDM.cdsFilterEmployee.
      FieldByName('BOOK_PROD_HRS_YN').AsString = 'Y');

  // 20011800 Also assign PLANT here! This is needed for final-run-system
  AFilter.PlantCode :=
    TimeRecScanningDM.cdsFilterEmployee.FieldByName('PLANT_CODE').AsString;

  AFilter.Active := True;
  TimeRecScanningDM.ScanFilter := AFilter;

  //CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    TimeRecScanningDM.cdsFilterEmployee.FieldByName('EMPLOYEE_NUMBER').AsInteger;

//  EnableEditable; // RV082.4.
  DoActiveAction; // 20013035
end;

procedure TTimeRecScanningF.ChangeTablesFilter(Sender: TObject;
  var Text: String; var Accept: Boolean);
begin
  MyChangeTablesFilter; // 20013035
end;

procedure TTimeRecScanningF.dxTimeEditOutExit(Sender: TObject);
{var
  ATime: TDateTime; }
begin
  inherited;
{
  if not dxBarBDBNavPost.Enabled then
    Exit;
  ATime := TimeRecScanningDM.TableDetail.
    FieldByName('DATETIME_OUT').AsDateTime;
  ReplaceTime(ATime,TdxTimeEdit(Sender).Time);
  TimeRecScanningDM.TableDetail.Edit;
  TimeRecScanningDM.TableDetail.
    FieldByName('DATETIME_OUT').AsDateTime := ATime;
}
end;

procedure TTimeRecScanningF.dxDBDateEditOutEnter(Sender: TObject);
begin
  inherited;
  if TdxDBDateEdit(Sender).Date <= 0 then
    TdxDBDateEdit(Sender).Date := dxDateEditIn.Date;
end;

procedure TTimeRecScanningF.dxDBDateEditInDateChange(Sender: TObject);
var
  ADate: TDateTime;
begin
  inherited;
  ADate := TdxDBDateEdit(Sender).Date;
  ReplaceTime(Adate,
    TimeRecScanningDM.TableDetail.
      FieldByName('DATETIME_IN').AsDateTime);
  TdxDBDateEdit(Sender).Date := ADate;
end;

procedure TTimeRecScanningF.dxDBDateEditOutDateChange(Sender: TObject);
var
  ADate: TDateTime;
begin
  inherited;
  ADate := TdxDBDateEdit(Sender).Date;
  ReplaceTime(Adate,TimeRecScanningDM.TableDetail.
    FieldByName('DATETIME_OUT').AsDateTime);
  TdxDBDateEdit(Sender).Date := ADate;
  //MR:08-10-2003
  if Trunc(ADate) = -700000 then
  begin
    dxTimeEditOut.Time := 0;
    dxDateEditOut.OnDateChange := nil;
    ReplaceTime(ADate, 0);
    TdxDBDateEdit(Sender).Date := ADate;
    dxDateEditOut.OnDateChange := dxDBDateEditOutDateChange;
  end;
end;

procedure TTimeRecScanningF.dxDetailGridClick(Sender: TObject);
begin
  inherited;
  //CAR 17-10-2003 : 550262
  //CAR 11-11-2003 : BUG
  if (PageControl2.ActivePageIndex = Integer(NotComplete)) and
    (not TimeRecScanningDM.TableDetail.IsEmpty) then
  begin
    SystemDM.ASaveTimeRecScanning.Employee :=
      TimeRecScanningDM.TableDetail.
        FieldByName('EMPLOYEE_NUMBER').AsInteger;
    SystemDM.ASaveTimeRecScanning.Shift :=
      TimeRecScanningDM.TableDetail.
        FieldByName('SHIFT_NUMBER').AsInteger;
    SystemDM.ASaveTimeRecScanning.DateScanning :=
      TimeRecScanningDM.TableDetail.
        FieldByName('DATETIME_IN').AsDateTime;
  //!!
    // RV067.MRA.37 Do not do this here!
//    CopyDateIn;
  end;
end;

procedure TTimeRecScanningF.DBLookupComboBoxPlantCloseUp(Sender: TObject);
begin
  inherited;
  TimeRecScanningDM.IfUniqueSetShift;
end;

procedure TTimeRecScanningF.cBoxAllNotCompleteClick(Sender: TObject);
begin
  inherited;
  lblDateNotComplete.Enabled := not cBoxAllNotComplete.Checked;
  {dxDateEditNotComplete.Enabled} DateEditNotComplete.Enabled := not cBoxAllNotComplete.Checked;
  btnCloseNotComplete.Enabled := not cBoxAllNotComplete.Checked;
  ChangeFilter(Sender);
end;
{
procedure TTimeRecScanningF.dxDateEditNotCompleteEnter(Sender: TObject);
begin
  inherited;
  if dxDateEditNotComplete.Date < 0 then
    dxDateEditNotComplete.Date := Now;
end;
}
// MR:05-02-2004 Close not-completed scans (change one or more records).
procedure TTimeRecScanningF.btnCloseNotCompleteClick(Sender: TObject);
var
  MyDateTimeOut: TDateTime;
  Hours, Mins, Secs, MSecs: Word;
  MyEmployeeNumber: Integer;
  MyDateTimeIn: TDateTime;
  AScanFilter: TDataFilter;
  MyOK, RecordFound: Boolean;
  MyMessage: String;
  ErrorCount: Integer;
  // RV092.6.
  // First check for all scans that should be changed, if there is an error.
  // Only if there are no errors, it should make changes.
  function AcceptanceCheck: Boolean;
  begin
    Result := True;
    with TimeRecScanningDM do
    begin
      if (not QueryWork.Eof) then
      begin
        QueryWork.First;
        while (not QueryWork.Eof) and (Result) do
        begin
          MyDateTimeIn :=
            QueryWork.FieldByName('DATETIME_IN').AsDateTime;
          MyEmployeeNumber :=
            QueryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          // Is end-time smaller than start-time?
          if (Trunc(MyDateTimeOut) = Trunc(MyDateTimeIn)) and
            (Frac(MyDateTimeOut) < Frac(MyDateTimeIn)) then
          begin
            MyMessage := SPimsEndTimeError;
            Result := False;
          end;
          if Result then
            // Is end-date smaller than start-date?
            if (MyDateTimeOut < MyDateTimeIn) then
            begin
              MyMessage := SDateFromTo;
              Result := False;
            end;
          if Result then
            // Is difference larger than 12 hours?
            if(MyDateTimeOut - MyDateTimeIn > 0.5) then
            begin
              MyMessage := SPimsScanTooBig;
              Result := False;
            end;
          QueryWork.Next;
        end; // while (not QueryWork.Eof)...
        QueryWork.First;
      end; // if (not QueryWork.Eof)...
    end; // with TimeRecScanningDM do
    if not Result then
      ErrorCount := -1;
  end; // AcceptanceCheck;
  // RV092.6. Here the scans are changed based on entered end-date.
  //          Do this after acceptance-check was done.
  // 20011800 Also test on last-export-date to see if it is allowed
  //          to change the record.
  procedure ChangeScans;
  var
    Ready: Boolean;
    FinalRunExportDate: TDateTime;
    SkipCount: Integer;
  begin
    with TimeRecScanningDM do
    begin
      if not QueryWork.IsEmpty then
      begin
        TableDetail.DisableControls;
        Ready := False;
        QueryWork.First;
        SkipCount := 0;
        while (not QueryWork.Eof) and (not Ready) do
        begin
          MyDateTimeIn :=
            QueryWork.FieldByName('DATETIME_IN').AsDateTime;
          MyEmployeeNumber :=
            QueryWork.FieldByName('EMPLOYEE_NUMBER').AsInteger;
          FinalRunExportDate := NullDate;
          if SystemDM.UseFinalRun then
            FinalRunExportDate := SystemDM.FinalRunExportDateByPlant(
              QueryWork.FieldByName('PLANT_CODE').AsString);
          //RV067.11.
          // Now get the record again.
          // MR:14-10-2004 Locate + datefield can give errors,
          // depending how the Windows-date-format been set.
          // Use 'ADateFmt'-routines to solve this.
//        SystemDM.ADateFmt.SwitchDateTimeFmtBDE;
{         RecordFound := TableDetail.Locate(
            'EMPLOYEE_NUMBER;DATETIME_IN', VarArrayOf([
            MyEmployeeNumber, MyDateTimeIn]), []); }
          if (FinalRunExportDate <> NullDate) and
            (Trunc(MyDateTimeIn) <= FinalRunExportDate) then // 20011800
          begin
            RecordFound := False;
            inc(SkipCount);
          end
          else
            RecordFound := TableDetail.FindKey([MyDateTimeIn, MyEmployeeNumber]);
//        SystemDM.ADateFmt.SwitchDateTimeFmtWindows;
          if RecordFound then
          begin
            try
              // Now change it.
              TableDetail.Edit;
              // RV092.6. Watch the value of NewEnteredDateOut!
              //          This is set to 0 in TableDetailBeforeEdit!
              //          Be sure NewEnteredDateOut is set here!
              TimeRecScanningDM.NewEnteredDateOut := MyDateTimeOut;
              TableDetailDATETIME_OUT.AsDateTime := MyDateTimeOut;
              TableDetail.Post;
            except
              inc(ErrorCount);
              TableDetail.Cancel;
              Ready := True;
              SysUtils.Abort; // goto 'finally'
            end;
          end; // if RecordFound then
          QueryWork.Next;
        end; // while (not QueryWork.Eof) and (not Ready) do
        if SkipCount > 0 then
        begin
          WLog(SPimsCloseScansSkipped);
          DisplayMessage(SPimsCloseScansSkipped, mtInformation, [mbOk]);
        end;
      end; // if not QueryWork.IsEmpty then
    end; // with TimeRecScanningDM do
  end; // ChangeScans;
begin
  inherited;
  AScanFilter := TimeRecScanningDM.ScanFilter;
  MyOK := False;
  MyDateTimeOut := 0;
  MyMessage := '';
  ErrorCount := 0;
  with TimeRecScanningDM do
  begin
    if TableDetail.Active then
    begin
      if TableDetail.RecordCount > 0 then
      begin
        try
          DialogNotCompleteDateF := TDialogNotCompleteDateF.Create(nil);
          DecodeTime(Now, Hours, Mins, Secs, MSecs);
          //RV067.8.
          DialogNotCompleteDateF.MyDateTimeOut :=
            Trunc({dxDateEditNotComplete.Date} DateEditNotComplete.DateTime);
          MyOK := (DialogNotCompleteDateF.ShowModal = mrOK);
          if MyOK then
            MyDateTimeOut := DialogNotCompleteDateF.MyDateTimeOut;
        finally
          DialogNotCompleteDateF.Free;
        end;
      end;
    end;
    if MyOK then
    begin
      try
        //RV067.11.
        EnableValidation := False;
//        TableDetail.Filtered := False;  // RV092.6.
        // 20011800 Also get plant_code
        QueryWork.Close;
        QueryWork.SQL.Clear;
        QueryWork.SQL.Add(
          'SELECT ' + NL +
          '  DATETIME_IN, EMPLOYEE_NUMBER, PLANT_CODE ' + NL +
          'FROM ' + NL +
          '  TIMEREGSCANNING ' + NL +
          'WHERE ' + NL +
          '  (DATETIME_IN >= :START AND ' + NL +
          '  DATETIME_IN <= :END) AND ' + NL +
          '  PROCESSED_YN = :PROC_YN ' + NL +
          'ORDER BY ' + NL +
          '  DATETIME_IN, EMPLOYEE_NUMBER');
        QueryWork.ParamByName('START').AsDateTime := AScanFilter.StartDate;
        QueryWork.ParamByName('END').AsDateTime := AScanFilter.EndDate;
        QueryWork.ParamByName('PROC_YN').AsString := UNCHECKEDVALUE;
        QueryWork.Open;
        // RV092.6.
        if AcceptanceCheck then
          ChangeScans;
      finally
        TableDetail.EnableControls;
//        ChangeFilter(Sender);
        if ErrorCount >= 0 then
        begin
          // RV092.6. No need to reset the filter, only do a refresh.
//          TableDetail.Filtered := False;  // RV092.6.
//          TableDetail.Filtered := True;
          TableDetail.Refresh;
        end;
        EnableValidation := True;
        if ErrorCount <> 0 then
          DisplayMessage(MyMessage, mtInformation, [mbOk]);
      end; // try
    end; // if MyOK then
  end; // with TimeRecScanningDM do
end;

procedure TTimeRecScanningF.dxDBExtLookupEditWorkspotCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  TimeRecScanningDM.IfUniqueSetJob;
end;

procedure TTimeRecScanningF.dxDBExtLookupEditWorkspotPopup(Sender: TObject;
  const EditText: String);
begin
  inherited;
  with TimeRecScanningDM do
  begin
    if not TableWorkspot.FindKey([
      TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('WORKSPOT_CODE').AsString]) then
      TableWorkspot.Locate('PLANT_CODE',
        TableDetail.FieldByName('PLANT_CODE').AsString, []);
  end;
end;

procedure TTimeRecScanningF.dxDBExtLookupEditWorkspotEnter(
  Sender: TObject);
begin
  inherited;
  // MR:14-05-2004 Set temporary to nil, to prevent that this event
  // is triggered at this event. Why, is not clear.
  OnDeactivate := nil;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure TTimeRecScanningF.dxDBExtLookupEditWorkspotExit(Sender: TObject);
begin
  inherited;
  // MR:14-05-2004 Set back to Gridbase-Event.
  OnDeactivate := GridBaseF.FormDeactivate;
end;

procedure TTimeRecScanningF.dxDBExtLookupEditWorkspotKeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  dxDBExtLookupEditWorkspot.DroppedDown := True;
end;

procedure TTimeRecScanningF.dxGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  ExportDate: TDateTime;
begin
  inherited;
  // 20015346
  // We get problems when we do not show the seconds!
{
  if AText <> '' then
  begin
    if AColumn = dxDetailGridColumnDateIn then
      AText := DateTimeToStr(RoundTime(dxDetailGridColumnDateIn.Field.AsDateTime, 1))
    else
      if AColumn = dxDetailGridColumnDateOut then
        AText := DateTimeToStr(RoundTime(dxDetailGridColumnDateOut.Field.AsDateTime, 1));
  end;
}
  if ASelected then
  begin
    if AColumn = dxDetailGridColumnWorkspot then
    begin
      if (TimeRecScanningDM.TableDetail.State in [dsEdit, dsInsert]) then
        lblWorkspotDescription.Caption := ''
      else
        lblWorkspotDescription.Caption := AText;
    end;
    if AColumn = dxDetailGridColumnJob then
    begin
      // 20011800
      if (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
      begin
        if SystemDM.UseFinalRun then
        begin
          with TimeRecScanningDM do
          begin
            if not (TimeRecScanningDM.TableDetail.State in [dsEdit, dsInsert]) then
            begin
              // 20011800.70 This must be plant from scanfilter!
              // Reason: When no record is made yet, it will be empty!
              ExportDate := SystemDM.FinalRunExportDateByPlant(
                TimeRecScanningDM.ScanFilter.PlantCode);
                // TableDetail.FieldByName('PLANT_CODE').AsString);
              if ExportDate <> NullDate then
              begin
                // 20011800.70 Compare with ScanFilter.StartDate here, not
                // with DateTimeIn! Reason: When there is no record yet this
                // will be empty (during Insert).
                EnableComponents(
                  {TableDetail.FieldByName('DATETIME_IN').AsDateTime}
                  TimeRecScanningDM.ScanFilter.StartDate > ExportDate);
              end
              else
                EnableComponents(True);
            end;
          end;
        end; // if
      end; // if
      if not (TimeRecScanningDM.TableDetail.State in [dsEdit, dsInsert]) then
      with TimeRecScanningDM do
      begin
        // MR:07-09-2004 Check if table is open, otherwise
        // during changing of menu-options, the error-message
        // 'Cannot not perform this action on a closed dataset'
        // can be shown.
        if TableWorkspot.Active then
          if not TableWorkspot.FindKey([
            TableDetail.FieldByName('PLANT_CODE').AsString,
            TableDetail.FieldByName('WORKSPOT_CODE').AsString]) then
            TableWorkspot.Locate('PLANT_CODE',
              TableDetail.FieldByName('PLANT_CODE').AsString, []);
      end;
    end;
  end;
end;

procedure TTimeRecScanningF.DBLookupComboBoxJobCodeEnter(Sender: TObject);
begin
  inherited;
  with TimeRecScanningDM do
  begin
    if not TableWorkspot.FindKey([
      TableDetail.FieldByName('PLANT_CODE').AsString,
      TableDetail.FieldByName('WORKSPOT_CODE').AsString]) then
      TableWorkspot.Locate('PLANT_CODE',
        TableDetail.FieldByName('PLANT_CODE').AsString, []);
  end;
end;
{
procedure TTimeRecScanningF.AssignDxDateEditScan(ANewDate: TDateTime);
begin
  dxDateEditScan.Date := ANewDate;
end;
}
procedure TTimeRecScanningF.AssignCBoxAllNotComplete(AValue: Boolean);
begin
  cBoxAllNotComplete.Checked := AValue;
end;

function TTimeRecScanningF.CBoxAllNotCompleteResult: Boolean;
begin
  Result := cBoxAllNotComplete.Checked;
end;

procedure TTimeRecScanningF.CopyDateIn;
var
  ADate: TDateTime;
begin
  // RV076.5. Only do this when the date-out is really 0 AND when
  //          the components-date is empty.
  if (Trunc(TimeRecScanningDM.TableDetail.
    FieldByName('DATETIME_OUT').AsDateTime) = 0) and
    (dxDateEditOut.Date = -700000) then
  begin
    ReplaceDate(ADate, TimeRecScanningDM.TableDetail.FieldByName('DATETIME_IN').AsDateTime);
    TimeRecScanningDM.NewEnteredDateOut := ADate;
    dxDateEditOut.Date := ADate;
    // RV080.2.
    if TimeRecScanningDM.TableDetail.State in [dsEdit, dsInsert] then
      TimeRecScanningDM.TableDetail.
        FieldByName('DATETIME_OUT').AsDateTime :=
          TimeRecScanningDM.NewEnteredDateOut;
  end;
{
  if Trunc(TimeRecScanningDM.TableDetail.
    FieldByName('DATETIME_OUT').AsDateTime) = 0 then
  begin
    TimeRecScanningDM.TableDetail.Edit;
    ReplaceDate(ADate, TimeRecScanningDM.TableDetail.FieldByName('DATETIME_IN').AsDateTime);
    TimeRecScanningDM.TableDetail.FieldByName('DATETIME_OUT').AsDateTime := ADate;
  end;
}
end;

procedure TTimeRecScanningF.dxDateEditOutEnter(Sender: TObject);
begin
  inherited;
  if
   (not TimeRecScanningDM.TableDetail.Active) or
   (TimeRecScanningDM.TableDetail.RecordCount = 0) or
   (not TimeRecScanningDM.TableDetail.FieldByName('DATETIME_OUT').IsNull)
  then
    Exit;

  if PageControl2.ActivePage = Tabsheet1 then
    CopyDateIn;
end;

procedure TTimeRecScanningF.dxDateEditInDateChange(Sender: TObject);
var
  ADate: TDateTime;
begin
  inherited;
  if not SystemDM.UseShiftDateSystem then // 20014327
  begin
    ADate := TdxDBDateEdit(Sender).Date;
    if ADate = -700000 then
      dxTimeEditIn.Time := 0
    else
      ReplaceTime(Adate, dxTimeEditIn.Time);
    TdxDBDateEdit(Sender).Date := ADate;
  end
  else
  begin // 20013489
    if (Sender = dxDateEditIn) then
    begin
      ADate := TdxDBDateEdit(Sender).Date;
      if ADate = -700000 then
        dxTimeEditIn.Time := 0
      else
        ReplaceTime(Adate, dxTimeEditIn.Time);

      // 20013489 Check if entered date is equal to selection-date or the next day.
      //          This makes it possible to enter an overnight-shift.
      //          TODO: Problem: When the date is changed to next date, then also
      //          the selection-date is changed ???
      if not ((Trunc(ADate) = Trunc(DateEditScan.DateTime {dxDateEditScan.Date})) or
       (Trunc(ADate) = Trunc(DateEditScan.DateTime {dxDateEditScan.Date}) + 1)) then
      begin
        DisplayMessage(SPimsEnteredDateRemark, mtInformation, [mbOk]);
        ADate := Trunc(DateEditScan.DateTime {dxDateEditScan.Date});
      end;

      TdxDBDateEdit(Sender).Date := Trunc(ADate);

      // 20013489 Assign DateOut to same date of DateIn
      if dxDateEditOut.Date <> -700000 then
      begin
        if dxDateEditOut.Date < dxDateEditIn.Date then
          dxDateEditOut.Date := dxDateEditIn.Date;
      end;
    end;
  end;
end;

procedure TTimeRecScanningF.dxDateEditOutDateChange(Sender: TObject);
var
  ADate: TDateTime;
  ATime: TDateTime;
begin
  ADate := TdxDBDateEdit(Sender).Date;
  if ADate = -700000 then // Date cleared
  begin
    dxTimeEditOut.Time := 0;
    // RV076.5.
    TimeRecScanningDM.NewEnteredDateOut := 0;
  end
  else
  begin
    ReplaceTime(Adate, dxTimeEditOut.Time);
    // RV076.5.
    ATime := dxTimeEditOut.Time;
    TimeRecScanningDM.NewEnteredDateOut :=
      Trunc(ADate) + Frac(ATime);
  end;
  TdxDBDateEdit(Sender).Date := ADate;
  inherited;
end;


procedure TTimeRecScanningF.dxTimeEditOutEnter(Sender: TObject);
var
  ADate: TDateTime;
  ATime: TDateTime;
begin
  inherited;
  // RV067.MRA.37
  // RV076.5.
  // Copy the date to date-out when time-out is changed and
  // when date-out is null.
  if (PageControl2.ActivePageIndex = Integer(NotComplete)) or
    (PageControl2.ActivePageIndex = Integer(ScanPerEmployee)) then
  begin
    if (not TimeRecScanningDM.TableDetail.IsEmpty) then
    begin
      if (Trunc(TimeRecScanningDM.TableDetail.
        FieldByName('DATETIME_OUT').AsDateTime) = 0) then
        CopyDateIn
      else
      begin
        // RV076.5. Store this value for later use.
        ADate := TdxDBDateEdit(Sender).Date;
        ATime := dxTimeEditOut.Time;
        TimeRecScanningDM.NewEnteredDateOut := Trunc(ADate) + Frac(ATime);
      end;
    end;
  end;
end;

procedure TTimeRecScanningF.dxTimeEditOutChange(Sender: TObject);
begin
  inherited;
  dxTimeEditOutEnter(Sender);
end;

(*
procedure TTimeRecScanningF.dxBarBDBNavPostClick(Sender: TObject);
var
  ADate: TDateTime;
begin
  inherited;
  // RV076.5.
  // Upon saving the record, look if the dateout-field in dialog
  // was filled, if so, then fill in the dateout-field in table.
  if dxDateEditOut.Date <> -700000 then
  begin
    // Temporary disable this, because we are changing it here manually!
    try
      dxDateEditOut.OnDateChange := nil;
//    ReplaceDate(ADate, dxDateEditOut.Date);
      ADate := Trunc(dxDateEditOut.Date) + Frac(dxTimeEditOut.Time);
//      ADate := Trunc(dxDateEditOut.Date) +
//        Frac(TimeRecScanningDM.TableDetail.
//          FieldByName('DATETIME_OUT').AsDateTime);
      TimeRecScanningDM.TableDetail.FieldByName('DATETIME_OUT').AsDateTime :=
        ADate;
    finally
      dxDateEditOut.OnDateChange := dxDateEditOutDateChange;
    end;
  end;
  TimeRecScanningDM.TableDetail.Post;
end;
*)

procedure TTimeRecScanningF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  // RV077.3.
  if not TimeRecScanningDM.IsNoScannerWarning then
  begin
    SysUtils.Abort;
    Exit;
  end;
  inherited;
end;

// RV082.4. Enable editable or not.
// NOTE: Not used yet.
{
procedure TTimeRecScanningF.EnableEditable;
var
  AFilter: TDataFilter;
begin
  if SystemDM.IsRFL then
  begin
    AFilter := TimeRecScanningDM.ScanFilter;
    if (AFilter.BookProdHrs) then
      SetNotEditableNow
    else
    begin
      if (Form_Edit_YN = ITEM_VISIBIL) then
        SetNotEditable
      else
        SetEditableNow;
    end;
  end;
end;
}

// 20013035
procedure TTimeRecScanningF.DoActiveAction;
var
  FinalRunExportDate: TDateTime;
begin
  // Active employee handling
  if TimeRecScanningDM.IsEmployeeActive(DateEditScan.DateTime {dxDateEditScan.Date}) then
  begin
    dxDBExtLookupEditEmployee.Color := clWindow;
    LabelEmpDesc.Enabled := True;
  end
  else
  begin
    dxDBExtLookupEditEmployee.Color := clScrollBar;
    LabelEmpDesc.Enabled := False;
  end;
  LabelEmpDesc.Caption :=
    TimeRecScanningDM.cdsFilterEmployee.FieldByName('DESCRIPTION').AsString; // 20013035
  // 20011800 Final run system handling
  if (Form_Edit_YN = ITEM_VISIBIL) then
    Exit;
  if not (Form_Edit_YN = ITEM_VISIBIL_EDIT) then
    Exit;
  if TimeRecScanningDM.ScanFilter.ScanFilter = ScanPerEmployee then
    if SystemDM.UseFinalRun then
    begin
      FinalRunExportDate :=
        SystemDM.FinalRunExportDateByPlant(TimeRecScanningDM.ScanFilter.PlantCode);
      if FinalRunExportDate <> NullDate then
      begin
        if TimeRecScanningDM.ScanFilter.StartDate <= FinalRunExportDate then
        begin
          // It is not allowed to add/delete/change the record
          dxDetailGrid.Tag := 2;  // No Insert/Delete allowed!
          EnableComponents(False);
        end
        else
        begin
          // It is allowed to add/delete/change the record
          dxDetailGrid.Tag := 1;  // Insert/Delete allowed
          EnableComponents(True);
        end;
        ActiveGridRefresh; // Refresh the toolbar.
      end
      else
      begin
        // It is allowed to add/delete/change the record
        dxDetailGrid.Tag := 1;  // Insert/Delete allowed
        EnableComponents(True);
        ActiveGridRefresh; // Refresh the toolbar.
      end;
    end;
end; // DoActiveAction

procedure TTimeRecScanningF.DateEditScanEnter(Sender: TObject);
begin
  inherited;
  if DateEditScan.DateTime < 0 then
    DateEditScan.DateTime := Now;
  DoActiveAction;
end;

procedure TTimeRecScanningF.AssignDateEditScan(ANewDate: TDateTime);
begin
  DateEditScan.DateTime := ANewDate;
end;

procedure TTimeRecScanningF.DateEditNotCompleteEnter(Sender: TObject);
begin
  inherited;
  if DateEditNotComplete.DateTime < 0 then
    DateEditNotComplete.DateTime := Now;
end;

procedure TTimeRecScanningF.DateEditEarlyLateEnter(Sender: TObject);
begin
  inherited;
  if DateEditEarlyLate.DateTime < 0 then
    DateEditEarlyLate.DateTime := Now;
end;

// 20011800
procedure TTimeRecScanningF.EnableComponents(AEnable: Boolean);
begin
  case TScanFilter(PageControl2.ActivePageIndex) of
  NotComplete : // Not Completed Scan
    begin
      dxDateEditIn.Enabled := AEnable;
      dxDBLookupEditEmployee.Enabled := AEnable;
    end;
  ScanPerEmployee : // Scans Per Employee
    begin
      if SystemDM.UseShiftDateSystem then // 20014327
      begin
        dxDateEditIn.Enabled := AEnable;
        dxDateEditIn.Color := dxTimeEditIn.Color;
      end
      else
        dxDateEditIn.Enabled := False;
      dxDBLookupEditEmployee.Enabled := False;
    end;
  end; // case
  dxTimeEditIn.Enabled := AEnable;
  dxDateEditOut.Enabled := AEnable;
  dxTimeEditOut.Enabled := AEnable;
  DBLookupComboBoxPlant.Enabled := AEnable;
  dxDBExtLookupEditWorkspot.Enabled := AEnable;
  DBLookupComboBoxJobCode.Enabled := AEnable;
  DBLookupComboBoxShift.Enabled := AEnable;
  pnlDetail.Enabled := AEnable; // 20011800.70
end; // EnableComponents

// 20013035
procedure TTimeRecScanningF.cBoxShowOnlyActiveClick(Sender: TObject);
begin
  inherited;
  ChangeFilter(Sender);
  TimeRecScanningDM.ShowOnlyActiveSwitch;
  MyChangeTablesFilter;
end;

// PIM-323
procedure TTimeRecScanningF.dxMasterGridCustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: String; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var
  DataSet: TDataSet;
  Grid: TdxDBGrid;
  TestDate: TDateTime;
  EmployeeNumber: Integer;
begin
  GridBaseF.dxGridCustomDrawCell(Sender, ACanvas, ARect, ANode,
    AColumn, ASelected, AFocused, ANewItemRow, AText, AColor, AFont,
    AAlignment, ADone);
  inherited;
  Grid := TdxDBGrid(Sender);
  DataSet := Grid.DataSource.DataSet;
  if (not ASelected) then
  begin
    if DataSet.FieldByName('REQUESTED_EARLY_TIME').AsString <> '' then
    begin
      TestDate := Trunc(DataSet.FieldByName('REQUEST_DATE').AsDateTime) +
        Frac(DataSet.FieldByName('REQUESTED_EARLY_TIME').AsDateTime);
      EmployeeNumber := DataSet.FieldByName('EMPLOYEE_NUMBER').AsInteger;
      if TimeRecScanningDM.EarlyLateScanCheck(TestDate, EmployeeNumber) then
      begin
        AColor := clRed;
        AFont.Color := clWhite;
      end;
    end;
  end;
end; // dxMasterGridCustomDrawCell

end.

