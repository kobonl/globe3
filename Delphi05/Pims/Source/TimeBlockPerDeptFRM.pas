(*
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)  
unit TimeBlockPerDeptFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, DBTables, CalculateTotalHoursDMT;
 
type
  TTimeBlockPerDeptF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridTimeColumn;
    dxMasterGridColumn4: TdxDBGridTimeColumn;
    dxMasterGridColumn5: TdxDBGridTimeColumn;
    dxMasterGridColumn6: TdxDBGridTimeColumn;
    dxMasterGridColumn7: TdxDBGridTimeColumn;
    dxMasterGridColumn8: TdxDBGridTimeColumn;
    dxMasterGridColumn9: TdxDBGridTimeColumn;
    dxMasterGridColumn10: TdxDBGridTimeColumn;
    dxMasterGridColumn11: TdxDBGridTimeColumn;
    dxMasterGridColumn12: TdxDBGridTimeColumn;
    dxMasterGridColumn13: TdxDBGridTimeColumn;
    dxMasterGridColumn14: TdxDBGridTimeColumn;
    dxMasterGridColumn15: TdxDBGridTimeColumn;
    dxMasterGridColumn16: TdxDBGridTimeColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    Label1: TLabel;
    dxDBTimeEditST1: TdxDBTimeEdit;
    dxDBTimeEditET1: TdxDBTimeEdit;
    dxDBTimeEditST2: TdxDBTimeEdit;
    dxDBTimeEditET2: TdxDBTimeEdit;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    DBEditTimeBlock: TDBEdit;
    DBEditDesc: TDBEdit;
    dxDBGridDept: TdxDBGrid;
    dxDBGridDeptColumn4: TdxDBGridLookupColumn;
    dxDBGridDeptColumn5: TdxDBGridColumn;
    dxDBGridColumn1: TdxDBGridColumn;
    dxDBGridColumn2: TdxDBGridColumn;
    dxDBGridColumn3: TdxDBGridColumn;
    SplitterTopMaster: TSplitter;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBGridDeptClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxDBGridDeptChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridClick(Sender: TObject);
    procedure ScrollBarMasterScroll(Sender: TObject;
      ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  //  procedure TableMasterBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
     InitialHeight: Integer; 

  end;

function TimeBlockPerDeptF: TTimeBlockPerDeptF;

implementation

{$R *.DFM}
uses
  SystemDMT, TimeBlockPerDeptDMT, UPimsMessageRes;

var
  TimeBlockPerDeptF_HND: TTimeBlockPerDeptF;

function TimeBlockPerDeptF: TTimeBlockPerDeptF;
begin
  if (TimeBlockPerDeptF_HND = nil) then
  begin
    TimeBlockPerDeptF_HND := TTimeBlockPerDeptF.Create(Application);
    TimeBlockPerDeptDM.Handle_Grid := TimeBlockPerDeptF_HND.dxDetailGrid;
  end;
  Result := TimeBlockPerDeptF_HND;
end;

procedure TTimeBlockPerDeptF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TTimeBlockPerDeptF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TTimeBlockPerDeptF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
   
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  for Index := 1 to 7 do
  begin
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
    dxMasterGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
  end;
  TimeBlockPerDeptDM.SetDepartment;
  TimeBlockPerDeptF_HND.dxDetailGrid.DataSource.DataSet.Close;
  TimeBlockPerDeptF_HND.dxDetailGrid.DataSource.DataSet.Open;

  dxMasterGrid.SetFocus;
end;

procedure TTimeBlockPerDeptF.FormDestroy(Sender: TObject);
begin
  inherited;
  TimeBlockPerDeptF_HND := nil;
end;

procedure TTimeBlockPerDeptF.dxDBGridDeptClick(Sender: TObject);
begin
  inherited;
  TimeBlockPerDeptDM.SetDepartment;  
end;

procedure TTimeBlockPerDeptF.FormCreate(Sender: TObject);
begin
  TimeBlockPerDeptDM := CreateFormDM(TTimeBlockPerDeptDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) or
    (dxDBGridDept.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := TimeBlockPerDeptDM.DataSourceDetail;
    dxMasterGrid.DataSource := TimeBlockPerDeptDM.DataSourceMaster;
    dxDBGridDept.DataSource := TimeBlockPerDeptDM.DataSourceDept;
  end;
  inherited;
  InitialHeight := 0;
  // 20014289
  dxDBGridDept.BandFont.Color := clWhite;
  dxDBGridDept.BandFont.Style := [fsBold];
end;

procedure TTimeBlockPerDeptF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  TimeBlockPerDeptDM.SetDepartment;
end;

procedure TTimeBlockPerDeptF.dxDBGridDeptChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  TimeBlockPerDeptDM.SetDepartment;
end;

procedure TTimeBlockPerDeptF.dxMasterGridClick(Sender: TObject);
begin
  inherited;
  TimeBlockPerDeptDM.SetDepartment;
end;

procedure TTimeBlockPerDeptF.ScrollBarMasterScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  dxDBGridDept.SetFocus;
end;

procedure TTimeBlockPerDeptF.dxBarButtonExportGridClick(Sender: TObject);
begin
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'PLANT_CODE');
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'PLANTLU');
  CreateExportColumns(dxDBGridDept, SPimsColumnPlant + ' ', 'DEPARTMENT_CODE');
  CreateExportColumnsDesc(SExportDescDept , 'DEPTLU');
  inherited;
end;

procedure TTimeBlockPerDeptF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
end;

procedure TTimeBlockPerDeptF.FormResize(Sender: TObject);
begin
  inherited;
  //CAR 15-10-2003 - on maximize screen give the same height for both grids
  if TimeBlockPerDeptF.ClientHeight > 600 then
    dxMasterGrid.Height := 200
  else
    dxMasterGrid.Height := 120;
end;
 
end.
