(*
  Changes:
    MR:08-10-2004 Use datefrom-dateto instead of
                  year, weekfrom and weekto. Order 550344.
    MR:29-09-2005 Select-statements changed, to ensure the
                  businessunit is compared correctly.
                  Depending on 'use_jobcode_yn' the businessunit
                  from businessunitperworkspot OR from job
                  must be used.
                  Also 'percentage' used, based on businessunitperworkspot.

                  NOTE: The 'percentage' of the businessunitperworkspot
                        was not used in this report(?)
    MR:31-01-2006 An sql-error occured when selecting 'show employee',
                  'sort on week', 'show hours of salary',
                  'all absence reason' and 'all team'.
    MR:27-02-2006 Missing quotes error when not all teams were selected.
    MR:24-01-2008 Order 550461, Oracle 10, RV003.
      Fields WEEKNUMBER, EMPLOYEE_NUMBER, ABSENCEREASON_ID gave an error
      like:
        'Type mismatch for field WEEKNUMBER, expecting float, actual integer'.
      This is solved by casting them to float using syntax in queries:
      '(cast(fieldname) as number) as fieldname'.
    MRA:07-AUG-2008 RV008.
      - Add ORDER BY because of Oracle 10.
      - Link employee to plant instead of absence during select, to
        prevent absence is not shown if it has a different (employee-)plant.
    RV039. MRA:22-OCT-2009.
      - Report does not select all workspots. Cause: It must use workspot.
        department_code instead of employee.department_code, when linking
        to department-table and during comparison, when showing production.
        However, when showing salary, there are no workspots known in
        SALARYHOURPEREMPLOYEE-table, so it can only link to employee's
        departments, and this can give a wrong result when no employee's
        are available that on a workspot-department that has been worked on.
    MRA:7-JAN-2010 RV050.4. 889965.
    - Report cannot show employees that work in other plants.
      Solution: Add All-checkbox to Employee-selection. When this is checked,
                it should not filter on any employee.
      Problem: When showing salary it is not possible to show employees that
               have been worked in different plants, because the salary-table
               does not store the plant!
               When showing production it is possible.
      NOTE:    For salary this is not a problem.
    MRA:29-JAN-2010. RV050.4.2. Bugfix.
    - Report gives different result when using
      'show department' or not.
      Reason: Problem with businessunit per workspot and
      percentage. When 'show department' is off, it has
      the wrong workspot to compare with in 'DetermineSUMMIN'.
    MRA:11-JAN-2011 RV084.1. SR-890041
    - The report dialog always shows all absence
      reasons for selection. Instead of that it must
      show absence reasons per country. This should be
      based on plant-from and plant-to that is selected.
    - The report itself: When 1 plant (from-to the same) was choosen,
      it must look if there are absence-reasons-per-country (ARC).
      If this is the case, then it must use ARC-table to link.
      Otherwise it must use the absence-reasons to link.
    MRA:11-JAN-2011 RV084.2. Bugfix.
    - It used uppercase for the query. This
      must not be done, because it filters on
      absencereason-codes that can be upper- and lower-case.
    JVL:07-apr-2011 RV089.3. SO-550524
    - Add suppress zeroes
*)

unit ReportHrsPerDayQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, Db, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt;

const
  ProdID = 999999;
  SortOnAbsenceReason = 0;
  SortOnWeek = 1;

type
  TQRParameters = class
  private
    FDateFrom, FDateTo: TDateTime;
    FSort, FReportType: Integer;
    FBusinessFrom, FBusinessTo, FDeptFrom, FDeptTo, FAbsenceFrom, FAbsenceTo,
    FTeamFrom, FTeamTo: String;
    FShowDept, FShowTeam, FShowEmpl, FShowSelection, FPagePlant, FPageDept,
    FPageTeam, FPageEmpl, FAllAbsReason, FAllTeam, FTeamEMP: Boolean;
    FExportToFile, FAllEmployees, FIncludeNotConnectedEmp, FSuppressZeroes: Boolean;
  public
    procedure SetValues(DateFrom, DateTo: TDateTime;
      Sort, ReportType: Integer;
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo: String;
      AllAbsReason, AllTeam, ShowDept, ShowTeam, ShowEmpl,
      ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP: Boolean;
      ExportToFile, AllEmployees, IncludeNotConnectedEmp, SuppressZeroes: Boolean);
  end;

  TAmountsPerDay = Array[1..7] of Double;
  TAmountsPrintPerDay = Array[1..7] of Boolean;

  TReportHrsPerDayQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDDept: TQRGroup;
    QRGroupHDTeam: TQRGroup;
    QRGroupHDEmpl: TQRGroup;
    QRGroupHDAbsenceReason: TQRGroup;
    QRGroupHDWeekNumber: TQRGroup;
    QRBandTitle: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelEmployeeFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelEmployeeTo: TQRLabel;
    QRLabelFromDate: TQRLabel;
    QRLabelDate: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabelToDate: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabelBusinessTo: TQRLabel;
    QRBandDetail: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabelBusinessFrom: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabelAbsenceRsn: TQRLabel;
    QRLabelAbsReasonFrom: TQRLabel;
    QRLabelAbsReasonTo: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel54: TQRLabel;
    QRDBTextPlant: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBTextDeptDesc: TQRDBText;
    QRBandFooterPlant: TQRBand;
    QRBandFooterDept: TQRBand;
    ChildBandPlantWeekAbs: TQRChildBand;
    QRLabelWeekPlant: TQRLabel;
    QRLabelAbsReasonPlant: TQRLabel;
    QRLabelMO: TQRLabel;
    QRLabelTU: TQRLabel;
    QRLabelWE: TQRLabel;
    QRLabelTH: TQRLabel;
    QRLabelFR: TQRLabel;
    QRLabelSA: TQRLabel;
    QRLabelSU: TQRLabel;
    QRLabel5: TQRLabel;
    ChildBandDeptWeekAbs: TQRChildBand;
    QRLabelWeekDept: TQRLabel;
    QRLabelAbsReasonDept: TQRLabel;
    QRLabel19: TQRLabel;
    ChildBandTeamWeekAbs: TQRChildBand;
    QRLabelWeekTeam: TQRLabel;
    QRLabelAbsReasonTeam: TQRLabel;
    QRBandFooterEmpl: TQRBand;
    QRLabel35: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBTextTeamDesc: TQRDBText;
    QRLabel36: TQRLabel;
    QRLabel3: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBTextEmplDesc: TQRDBText;
    QRBandFooterTeam: TQRBand;
    ChildBandEmplWeek: TQRChildBand;
    QRLabelWeekEmpl: TQRLabel;
    QRLabelAbsReasonEmpl: TQRLabel;
    QRLabel77: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel76: TQRLabel;
    QRBandSummary: TQRBand;
    QRLabelShowReportType: TQRLabel;
    QRLabel4: TQRLabel;
    ChildBandProdEmployee: TQRChildBand;
    QRLabelFromWeek: TQRLabel;
    QRLabelWeekFrom: TQRLabel;
    QRLabelFromDay: TQRLabel;
    QRLabelDayFrom: TQRLabel;
    QRLabelToWeek: TQRLabel;
    QRLabelWeekTo: TQRLabel;
    QRLabelToDay: TQRLabel;
    QRLabelDayTo: TQRLabel;
    ChildBandProdEmlpoyee2: TQRChildBand;
    QRLabel96: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel97: TQRLabel;
    QRLabel98: TQRLabel;
    QRLabel99: TQRLabel;
    QRLabel100: TQRLabel;
    QRLabel101: TQRLabel;
    QRLabel102: TQRLabel;
    QRLabel103: TQRLabel;
    QRLabel104: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel105: TQRLabel;
    QRLabel114: TQRLabel;
    QRLabel115: TQRLabel;
    QRLabel116: TQRLabel;
    QRLabel117: TQRLabel;
    QRLabel118: TQRLabel;
    QRLabel119: TQRLabel;
    QRLabelTotalUnknownEmp2: TQRLabel;
    ChildBandFooterEmpl2: TQRChildBand;
    QRLabel34: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabelTotalEmpl: TQRLabel;
    QRLabel120: TQRLabel;
    QRLabel121: TQRLabel;
    QRLabel122: TQRLabel;
    QRLabel123: TQRLabel;
    QRLabel124: TQRLabel;
    QRLabel125: TQRLabel;
    QRLabel126: TQRLabel;
    QRLabel127: TQRLabel;
    QRLabelTotalUnknownEmp1: TQRLabel;
    ChildBandFooterTeam: TQRChildBand;
    QRLabel78: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel79: TQRLabel;
    QRLabel80: TQRLabel;
    QRLabel81: TQRLabel;
    QRLabel82: TQRLabel;
    QRLabel83: TQRLabel;
    QRLabel84: TQRLabel;
    QRLabel85: TQRLabel;
    QRLabel86: TQRLabel;
    QRLabel128: TQRLabel;
    QRLabel129: TQRLabel;
    QRLabel130: TQRLabel;
    QRLabel131: TQRLabel;
    QRLabel132: TQRLabel;
    QRLabel133: TQRLabel;
    QRLabel134: TQRLabel;
    QRLabel135: TQRLabel;
    QRLabelTotalUnknownTeam: TQRLabel;
    ChildBandFooterDept: TQRChildBand;
    QRLabel21: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel66: TQRLabel;
    QRLabelTotalDept: TQRLabel;
    QRLabel136: TQRLabel;
    QRLabel137: TQRLabel;
    QRLabel138: TQRLabel;
    QRLabel139: TQRLabel;
    QRLabel140: TQRLabel;
    QRLabel141: TQRLabel;
    QRLabel142: TQRLabel;
    QRLabel143: TQRLabel;
    QRLabelTotalUnknownDept: TQRLabel;
    ChildBandFooterPlant: TQRChildBand;
    QRLabel23: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRLabel69: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel71: TQRLabel;
    QRLabel72: TQRLabel;
    QRLabel73: TQRLabel;
    QRLabel74: TQRLabel;
    QRLabel75: TQRLabel;
    QRLabelTotalPlant: TQRLabel;
    QRLabel144: TQRLabel;
    QRLabel145: TQRLabel;
    QRLabel146: TQRLabel;
    QRLabel147: TQRLabel;
    QRLabel148: TQRLabel;
    QRLabel149: TQRLabel;
    QRLabel150: TQRLabel;
    QRLabel151: TQRLabel;
    QRLabelTotalUnknownPlant: TQRLabel;
    QRBandGroupFooterWeekNumber: TQRBand;
    QRLabelWeekReason: TQRLabel;
    QRLabelReasonWeek: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabel112: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabel152: TQRLabel;
    QRLabelTotalWeek: TQRLabel;
    QRBandGroupFooterAbsenceReason: TQRBand;
    QRLabel6: TQRLabel;
    QRLabelAbsenceReason: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabelTotalAbsenceReason: TQRLabel;
    QRLabel89: TQRLabel;
    QRLabel91: TQRLabel;
    QRLabel92: TQRLabel;
    ChildBandWeekNumberUnknown: TQRChildBand;
    QRLabelWeekReasonUN: TQRLabel;
    QRLabelReasonWeekUN: TQRLabel;
    QRLabel95: TQRLabel;
    QRLabel106: TQRLabel;
    QRLabel107: TQRLabel;
    QRLabel153: TQRLabel;
    QRLabel154: TQRLabel;
    QRLabel155: TQRLabel;
    QRLabel156: TQRLabel;
    QRLabelTotalWeekUN: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandPlantWeekAbsBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFooterPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelTotalEmplPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalDeptPrint(sender: TObject; var Value: String);
    procedure QRLabelTotalPlantPrint(sender: TObject; var Value: String);

    procedure QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);

    procedure QRBandFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel86Print(sender: TObject; var Value: String);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRDBText15Print(sender: TObject; var Value: String);
    procedure QRDBText9Print(sender: TObject; var Value: String);
    procedure QRDBText7Print(sender: TObject; var Value: String);
    procedure QRLabelTotalReportTypePrint(sender: TObject;
      var Value: String);
    procedure QRLabelTotalReportType_RWPrint(sender: TObject;
      var Value: String);
    procedure ChildBandProdEmployeeBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandProdEmlpoyee2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterEmpl2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterTeamBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure ChildBandFooterDeptBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterWeekNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDAbsenceReasonBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandGroupFooterAbsenceReasonBeforePrint(
      Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure QRLabelTotalAbsenceReasonPrint(sender: TObject;
      var Value: String);
    procedure ChildBandWeekNumberUnknownBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    FQRParameters: TQRParameters;
    FirstTimeDetail: Boolean;
    AllPlants, AllTeams, {AllEmployees, } AllBusinessUnits: Boolean;
    PreviousAbsenceReason, PreviousWeek: String;
    function Summarize(AmountsPerDaysOfWeek: TAmountsPerDay): Integer;
    function DetermineSUMMIN: Double;
(*    function DetermineUnknown(AQRLabel: TQRLabel): Boolean; *)
    procedure DetermineCaptions(
      AQRLabelWeekReason, AQRLabelReasonWeek: TQRLabel;
      AUnknownBusinessUnit: String);
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FMinDate,
    FMaxDate: TDateTime;

    PrintTotalDetailBand,
    PrintDetailBand,
    PrintTotalReportTypeBand: Boolean;

    AbsencePerEmpl,
    AbsencePerReason, TotalAbsencePerReason, TotalOnDetail,
    TotalOnDays, TotalEmpl, TotalTeam, TotalDept, TotalPlant,
    TotalReportType, TotalUnknownOnDays, TotalUnknown,
    TotalAbsenceReason: TAmountsPerDay;
    PrintTotalOnDays: TAmountsPrintPerDay;

    // MR:16-04-2004
    BookmarkSet: Boolean;
    MyBookmark: TBookmark;

    FReason, FReasonDesc, FPlant, FDept, FTeam: String;
    FEmpl: Integer;
    FYear, FWeek: Word;
    FEmplDesc, FDeptDesc, FTeamDesc, FPlantDesc: String; // MR:31-12-2002
    FTotalText: String; // MR:31-12-2002
    procedure InitializeAmountsPerDaysOfWeek(var AmountsPerDaysOfWeek: TAmountsPerDay);
    procedure FillAmountsPerDaysOfWeek(IndexMin: Integer;
      AmountsPerDaysOfWeek: TAmountsPerDay; PrintDecide: Boolean);
    procedure FillDescDaysPerWeek(IndexMin: Integer);
    function QRSendReportParameters(const PlantFrom, PlantTo,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
      const DateFrom, DateTo: TDateTime;
      const Sort, ReportType: Integer;
      const ShowAllAbsReason, ShowAllTeam, ShowDept, ShowTeam, ShowEmpl,
        ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl,
        TeamEMP: Boolean;
        ExportToFile, AllEmployees, IncludeNotConnectedEmp, SuppressZeroes : Boolean): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    function FillSalHrsPerType: String;
    function FillHrsPerType: String;
  end;

var
  ReportHrsPerDayQR: TReportHrsPerDayQR;

implementation

{$R *.DFM}
uses
  SystemDMT, ReportHrsPerDayDMT, UGlobalFunctions, ListProcsFRM,
  UPimsMessageRes, UPimsConst;

// MR:12-12-2002
procedure ExportDetail(
  TotalText, Code, Description, Week, D1, D2, D3, D4, D5, D6, D7, Total: String);
begin
  ExportClass.AddText(
    TotalText + ExportClass.Sep +
    Code + ExportClass.Sep +
    Description + ExportClass.Sep +
    Week + ExportClass.Sep +
    D1 + ExportClass.Sep +
    D2 + ExportClass.Sep +
    D3 + ExportClass.Sep +
    D4 + ExportClass.Sep +
    D5 + ExportClass.Sep +
    D6 + ExportClass.Sep +
    D7 + ExportClass.Sep +
    Total
    );
end;

procedure TQRParameters.SetValues(DateFrom, DateTo: TDateTime;
  Sort, ReportType: Integer;
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceFrom, AbsenceTo,
  TeamFrom, TeamTo: String;
  AllAbsReason, AllTeam, ShowDept, ShowTeam, ShowEmpl,
  ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP: Boolean;
  ExportToFile, AllEmployees, IncludeNotConnectedEmp, SuppressZeroes: Boolean);
begin
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FSort := Sort;
  FReportType := ReportType;
  FBusinessFrom := BusinessFrom;
  FBusinessTo := BusinessTo;
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FAbsenceFrom := AbsenceFrom;
  FAbsenceTo := AbsenceTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FAllAbsReason := AllAbsReason;
  FAllTeam := AllTeam;
  FShowDept := ShowDept;
  FShowTeam := ShowTeam;
  FShowEmpl := ShowEmpl;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageDept := PageDept;
  FPageTeam := PageTeam;
  FPageEmpl := PageEmpl;
  FTeamEMP := TeamEMP;
  FExportToFile := ExportToFile;
  FAllEmployees := AllEmployees;
  FIncludeNotConnectedEmp := IncludeNotConnectedEmp;
  FSuppressZeroes := SuppressZeroes;
end;

function TReportHrsPerDayQR.QRSendReportParameters(const PlantFrom, PlantTo,
  BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceFrom, AbsenceTo,
  TeamFrom, TeamTo, EmployeeFrom, EmployeeTo: String;
  const DateFrom, DateTo: TDateTime;
  const Sort, ReportType: Integer;
  const ShowAllAbsReason, ShowAllTeam, ShowDept, ShowTeam,
  ShowEmpl, ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl,
  TeamEMP: Boolean;
  ExportToFile, AllEmployees, IncludeNotConnectedEmp, SuppressZeroes: Boolean): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, EmployeeFrom, EmployeeTo);
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    FQRParameters.SetValues(DateFrom, DateTo, Sort, ReportType,
      BusinessFrom, BusinessTo, DeptFrom, DeptTo, AbsenceFrom, AbsenceTo,
      TeamFrom, TeamTo, ShowAllAbsReason, ShowAllTeam, ShowDept, ShowTeam, ShowEmpl,
      ShowSelection, PagePlant, PageDept, PageTeam, PageEmpl, TeamEMP,
      ExportToFile, AllEmployees, IncludeNotConnectedEmp, SuppressZeroes);
  end;
  SetDataSetQueryReport(ReportHrsPerDayDM.QueryAbsence);
end;

function TReportHrsPerDayQR.FillSalHrsPerType: String;
var
  SelectStr: String;
begin
  SelectStr :=
    'SELECT ' + NL +
    '  E.PLANT_CODE, P.DESCRIPTION AS PDESC, ' + NL;
  if QRParameters.FShowDept then
  begin
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, ' + NL;
  end;
  if QRParameters.FShowTeam then
  begin
    SelectStr := SelectStr +
      '  E.TEAM_CODE,  T.DESCRIPTION AS TDESC,  ' + NL;
  end;
  if QRParameters.FShowEmpl then
  begin
    SelectStr := SelectStr +
      '  CAST(A.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER, ' + NL +
      '  E.DESCRIPTION AS EDESC, ' + NL;
  end;

  if QRParameters.FSort = SortOnAbsenceReason then
  begin
    SelectStr := SelectStr +
      '  999999, CAST('''' AS VARCHAR(1)) AS ABSENCEREASON_CODE, ' + NL +
      '  CAST('''' AS VARCHAR(30)), ' + NL +
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL;
  end
  else
  begin
    SelectStr := SelectStr +
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL +
      '  999999, CAST('''' AS VARCHAR(1)) AS ABSENCEREASON_CODE, ' + NL +
      '  CAST('''' AS VARCHAR(30)), ' + NL;
  end;
  if not QRParameters.FShowDept then // RV050.4.2.
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, ' + NL;
  SelectStr := SelectStr +
    '  SALARY_DATE AS REPORT_DATE, ' + NL;

  SelectStr := SelectStr +
    '  SUM(A.SALARY_MINUTE) AS SUMMIN ' + NL +
    ', MAX(P.PLANT_CODE) AS BUCODE ' + NL + // Dummy
    ', MAX(P.PLANT_CODE) AS WORKSPOT_CODE ' + NL + // Dummy
    ', MAX(0) AS LISTCODE ' + NL + // Dummy
    'FROM ' + NL +
    '  SALARYHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
    '    (A.SALARY_DATE >= :FSTARTDATE AND ' + NL +
    '    A.SALARY_DATE <= :FENDDATE) ' + NL +
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    A.SALARY_DATE >= WK.DATEFROM AND ' + NL +
    '    A.SALARY_DATE <= WK.DATETO ' + NL;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
//    if not AllEmployees then
    if not QRParameters.FAllEmployees then
      SelectStr := SelectStr +
        '  AND A.EMPLOYEE_NUMBER >= ' +
        QRBaseParameters.FEmployeeFrom + NL +
        '  AND A.EMPLOYEE_NUMBER <= ' +
        QRBaseParameters.FEmployeeTo + NL;

  SelectStr := SelectStr +
    '  INNER JOIN PLANT P ON ' + NL +
    '    E.PLANT_CODE = P.PLANT_CODE ' + NL;
  if not AllPlants then
    SelectStr := SelectStr +
      '    AND E.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '    AND E.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;

  SelectStr := SelectStr +
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    E.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '    E.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
    SelectStr := SelectStr +
      '  AND D.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND D.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '  AND D.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '  AND D.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
  if not QRParameters.FTeamEMP then
    SelectStr := SelectStr +
      '  INNER JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL
  else
    SelectStr := SelectStr +
      '  LEFT JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL;
  if not QRParameters.FAllTeam then
  begin
    if not AllTeams then
      SelectStr := SelectStr +
// MR:27-2-2006 Removed quotes
        '  AND E.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        '  AND E.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
  end;
  SelectStr := SelectStr + ' ' +
    'GROUP BY ' +  NL +
    '  E.PLANT_CODE, P.DESCRIPTION ' + NL;
  if (QRParameters.FShowDept) then
    SelectStr := SelectStr +
      ', D.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
  if (QRParameters.FShowTeam) then
    SelectStr := SelectStr +
      ', E.TEAM_CODE, T.DESCRIPTION ' + NL;
  if (QRParameters.FShowEmpl) then
    SelectStr := SelectStr +
      ', A.EMPLOYEE_NUMBER, E.DESCRIPTION ' + NL;
  SelectStr := SelectStr +
    ', E.PLANT_CODE ' + NL;
  SelectStr := SelectStr +
    ', WK.WEEKNUMBER ' + NL;
  if not QRParameters.FShowDept then // RV050.4.2.
    SelectStr := SelectStr +
      ',  D.DEPARTMENT_CODE ' + NL;
  SelectStr := SelectStr +
    ', A.SALARY_DATE ' + NL;

  Result := SelectStr;
end;

function TReportHrsPerDayQR.FillHrsPerType: String;
var
  SelectStr1, SelectStr2, SelectStr3, TempStr: String;
begin
  TempStr :=
    'SELECT ' + NL +
    '  A.PLANT_CODE, P.DESCRIPTION AS PDESC, ' + NL;
  SelectStr1 := TempStr;
  SelectStr2 := TempStr;
  SelectStr3 := TempStr;
  if QRParameters.FShowDept then
  begin
    TempStr :=
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowTeam then
  begin
    TempStr :=
      '  E.TEAM_CODE,  T.DESCRIPTION AS TDESC,  ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FShowEmpl then
  begin
    TempStr :=
      '  CAST(A.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER,' +
      '  E.DESCRIPTION AS EDESC, ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if QRParameters.FSort = SortOnAbsenceReason then
  begin
    TempStr :=
      '  999999, CAST('''' AS VARCHAR(1)) AS ABSENCEREASON_CODE, ' + NL +
      '  CAST('''' AS VARCHAR(30)), ' + NL +
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL;
    if not QRParameters.FShowDept then  // RV050.4.2.
      TempStr := TempStr +
        '  D.DEPARTMENT_CODE, ' + NL;
    TempStr := TempStr +
      '  A.PRODHOUREMPLOYEE_DATE AS REPORT_DATE, ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end
  else
  begin
    TempStr :=
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL +
      '  999999, CAST('''' AS VARCHAR(1)) AS ABSENCEREASON_CODE, ' + NL +
      '  CAST('''' AS VARCHAR(30)), ' + NL;
    if not QRParameters.FShowDept then // RV050.4.2.
      TempStr := TempStr +
        '  D.DEPARTMENT_CODE, ' + NL;
    TempStr := TempStr +
      '  A.PRODHOUREMPLOYEE_DATE AS REPORT_DATE, ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
//
  TempStr :=
    '  SUM(A.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(A.WORKSPOT_CODE) AS WORKSPOT_CODE ' + NL +
    ', MAX(1) AS LISTCODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '  SUM(A.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(BW.BUSINESSUNIT_CODE) AS BUCODE ' + NL +
    ', MAX(A.WORKSPOT_CODE) AS WORKSPOT_CODE ' + NL +
    ', MAX(2) AS LISTCODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '  SUM(A.PRODUCTION_MINUTE) AS SUMMIN ' + NL +
    ', MAX(J.BUSINESSUNIT_CODE) AS BUCODE ' + NL + // DUMMY!
    ', MAX(A.WORKSPOT_CODE) AS WORKSPOT_CODE ' + NL +
    ', MAX(3) AS LISTCODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    'FROM ' + NL +
    '  PRODHOURPEREMPLOYEE A INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND ' + NL +
    '    (A.PRODHOUREMPLOYEE_DATE >= :FSTARTDATE AND ' + NL +
    '    A.PRODHOUREMPLOYEE_DATE <= :FENDDATE) ' + NL +
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    A.PRODHOUREMPLOYEE_DATE >= WK.DATEFROM AND ' + NL +
    '    A.PRODHOUREMPLOYEE_DATE <= WK.DATETO ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
//    if not AllEmployees then
    if not QRParameters.FAllEmployees then
    begin
      TempStr :=
        '  AND A.EMPLOYEE_NUMBER >= ' +
        QRBaseParameters.FEmployeeFrom + NL +
        '  AND A.EMPLOYEE_NUMBER <= ' +
        QRBaseParameters.FEmployeeTo + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
  end;

  TempStr :=
    '  INNER JOIN PLANT P ON ' + NL +
    '    A.PLANT_CODE = P.PLANT_CODE ' + NL;
  if not AllPlants then
    TempStr := TempStr +
      '    AND A.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '    AND A.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
//
  TempStr :=
    '  INNER JOIN WORKSPOT W ON ' + NL +
    '    A.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    A.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (A.JOB_CODE <> ''' + DUMMYSTR + ''') ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''N'' ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '    AND W.USE_JOBCODE_YN = ''Y'' ' + NL +
    '    AND (A.JOB_CODE = ''' + DUMMYSTR + ''')' + NL;
  SelectStr3 := SelectStr3 + TempStr;

// RV039. Make department join here, AFTER workspot-part and
//        make join to workspot instead of to employee!
  TempStr :=
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    A.PLANT_CODE = D.PLANT_CODE AND ' + NL +
    '    W.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
    TempStr :=
      '  AND W.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '  AND W.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
//
  TempStr :=
    '  INNER JOIN JOBCODE J ON ' + NL +
    '    A.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '    A.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    A.JOB_CODE = J.JOB_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  TempStr :=
    '  INNER JOIN BUSINESSUNITPERWORKSPOT BW ON ' + NL +
    '    BW.PLANT_CODE = W.PLANT_CODE AND ' + NL +
    '    BW.WORKSPOT_CODE = W.WORKSPOT_CODE ' + NL;
  SelectStr2 := SelectStr2 + TempStr;
  TempStr :=
    '  LEFT JOIN JOBCODE J ON ' + NL + // Left Join -> Jobs do not exist!
    '    A.PLANT_CODE = J.PLANT_CODE AND ' + NL +
    '    A.WORKSPOT_CODE = J.WORKSPOT_CODE AND ' + NL +
    '    A.JOB_CODE = J.JOB_CODE ' + NL;
  SelectStr3 := SelectStr3 + TempStr;
//
  if not QRParameters.FTeamEMP then
    TempStr :=
      '  INNER JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL
  else
    TempStr :=
      '  LEFT JOIN TEAM T ON ' + NL +
      '    E.TEAM_CODE = T.TEAM_CODE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if not QRParameters.FAllTeam then
  begin
    if not AllTeams then
    begin
      TempStr :=
// MR:27-2-2006 Removed quotes
        '  AND E.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        '  AND E.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL;
      SelectStr1 := SelectStr1 + TempStr;
      SelectStr2 := SelectStr2 + TempStr;
      SelectStr3 := SelectStr3 + TempStr;
    end;
  end;

  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
  begin
//
    TempStr :=
      'WHERE ' + NL +
      '  J.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '    AND J.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    TempStr :=
      'WHERE ' + NL +
      '  BW.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '  AND BW.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + ''; // No selection
//
  end;
  TempStr :=
    'GROUP BY ' +  NL +
    '  A.PLANT_CODE, P.DESCRIPTION ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;
  if (QRParameters.FShowDept) then
  begin
    TempStr :=
      ', D.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if (QRParameters.FShowTeam) then
  begin
    TempStr :=
      ', E.TEAM_CODE, T.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  if (QRParameters.FShowEmpl) then
  begin
    TempStr :=
      ', A.EMPLOYEE_NUMBER, E.DESCRIPTION ' + NL;
    SelectStr1 := SelectStr1 + TempStr;
    SelectStr2 := SelectStr2 + TempStr;
    SelectStr3 := SelectStr3 + TempStr;
  end;
  TempStr :=
    ', WK.WEEKNUMBER ' + NL;
  if not QRParameters.FShowDept then // RV050.4.2.
    TempStr := TempStr +
      ', D.DEPARTMENT_CODE ' + NL;
  TempStr := TempStr +
    ', A.PRODHOUREMPLOYEE_DATE ' + NL;
  SelectStr1 := SelectStr1 + TempStr;
  SelectStr2 := SelectStr2 + TempStr;
  SelectStr3 := SelectStr3 + TempStr;

  Result := SelectStr1 +
    ' UNION ' + NL + SelectStr2 +
    ' UNION ' + NL + SelectStr3;
end;

function TReportHrsPerDayQR.ExistsRecords: Boolean;
var
  SelectStr, OrderStr: String;
  CountryId, CountryCnt: Integer; // RV084.1.
  AbsenceReasonCountryJoin: String; // RV084.1.
begin
  Screen.Cursor := crHourGlass;
  with ReportHrsPerDayDM do
  begin
    AllPlants := DetermineAllPlants(
      QRBaseParameters.FPlantFrom, QRBaseParameters.FPlantTo);
    AllTeams := DetermineAllTeams(
      QRParameters.FTeamFrom, QRParameters.FTeamTo);
//    AllEmployees := DetermineAllEmployees(
//      QRBaseParameters.FEmployeeFrom, QRBaseParameters.FEmployeeTo);
    AllBusinessUnits := DetermineAllBusinessUnits(
      QRParameters.FBusinessFrom, QRParameters.FBusinessTo);
  end;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FDeptFrom := '0';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FBusinessFrom := '0';
    QRParameters.FBusinessTo := 'zzzzzz';
    QRBaseParameters.FEmployeeFrom := '0';
    QRBaseParameters.FEmployeeTo := '999999';
  end;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;

  ReportHrsPerDayDM.FillWeekTable(FMinDate, FMaxDate);
  ReportHrsPerDayDM.QueryAbsence.Close;
  // Set correct expression for grouping.
  if QRParameters.FSort = SortOnAbsenceReason then
  begin
    QRGroupHDAbsenceReason.Expression := 'QueryAbsence.ABSENCEREASON_CODE';
    QRGroupHDWeekNumber.Expression := 'QueryAbsence.ABSENCEREASON_CODE + ' +
      'QueryAbsence.WEEKNUMBER';
  end
  else
  begin
    QRGroupHDAbsenceReason.Expression := 'QueryAbsence.WEEKNUMBER';
    QRGroupHDWeekNumber.Expression := 'QueryAbsence.WEEKNUMBER + ' +
      'QueryAbsence.ABSENCEREASON_CODE';
  end;

  SelectStr :=
    'SELECT ' +
    '  A.PLANT_CODE, P.DESCRIPTION AS PDESC, ' + NL;

  if QRParameters.FShowDept then
  begin
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, D.DESCRIPTION AS DDESC, ' + NL;
    // MR: Set fields if they are needed.
    ReportHrsPerDayDM.QueryAbsenceDEPARTMENT_CODE.FieldKind := fkData;
    ReportHrsPerDayDM.QueryAbsenceDDESC.FieldKind := fkData;
  end
  else
  begin
    // MR: Disable fields if they are not needed.
    ReportHrsPerDayDM.QueryAbsenceDEPARTMENT_CODE.FieldKind := fkCalculated;
    ReportHrsPerDayDM.QueryAbsenceDDESC.FieldKind := fkCalculated;
  end;

  if QRParameters.FShowTeam then
  begin
    SelectStr := SelectStr +
      '  E.TEAM_CODE, T.DESCRIPTION AS TDESC, ' + NL;
    ReportHrsPerDayDM.QueryAbsenceTEAM_CODE.FieldKind := fkData;
    ReportHrsPerDayDM.QueryAbsenceTDESC.FieldKind := fkData;
  end
  else
  begin
    ReportHrsPerDayDM.QueryAbsenceTEAM_CODE.FieldKind := fkCalculated;
    ReportHrsPerDayDM.QueryAbsenceTDESC.FieldKind := fkCalculated;
  end;

  if QRParameters.FShowEmpl then
  begin
    SelectStr := SelectStr +
      '  CAST(A.EMPLOYEE_NUMBER AS NUMBER) AS EMPLOYEE_NUMBER, ' +
      '  E.DESCRIPTION AS EDESC,' + NL ;
    ReportHrsPerDayDM.QueryAbsenceEMPLOYEE_NUMBER.FieldKind := fkData;
    ReportHrsPerDayDM.QueryAbsenceEDESC.FieldKind := fkData;
  end
  else
  begin
    ReportHrsPerDayDM.QueryAbsenceEMPLOYEE_NUMBER.FieldKind := fkCalculated;
    ReportHrsPerDayDM.QueryAbsenceEDESC.FieldKind := fkCalculated;
  end;

  if QRParameters.FSort = SortOnAbsenceReason then
  begin
    SelectStr := SelectStr +
      '  A.ABSENCEREASON_ID, ' + NL +
      '  R.ABSENCEREASON_CODE, ' + NL +
      '  R.DESCRIPTION AS ABSDESC, ' + NL +
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL;
  end
  else
  begin
    SelectStr := SelectStr +
      '  CAST(WK.WEEKNUMBER AS NUMBER) AS WEEKNUMBER, ' + NL +
      '  A.ABSENCEREASON_ID, ' + NL +
      '  R.ABSENCEREASON_CODE, R.DESCRIPTION AS ABSDESC, ' + NL;
  end;
  if not QRParameters.FShowDept then // RV050.4.2.
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, ' + NL;
  SelectStr := SelectStr +
    '  A.ABSENCEHOUR_DATE AS REPORT_DATE, ' + NL;

  SelectStr := SelectStr +
    '  SUM(ABSENCE_MINUTE) AS SUMMIN ' + NL +
    ', MAX(A.PLANT_CODE) AS BUCODE ' + NL + // Dummy
    ', MAX(A.PLANT_CODE) AS WORKSPOT_CODE ' + NL + // Dummy
    ', MAX(0) AS LISTCODE ' + NL; // Dummy

  // RV084.1.
  AbsenceReasonCountryJoin := '';
  if QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo then
  begin
    CountryCnt := 0;
    CountryId := SystemDM.GetDBValue(
      ' SELECT NVL(P.COUNTRY_ID, 0) ' +
      ' FROM PLANT P ' +
      ' WHERE ' +
      '   P.PLANT_CODE =  ' + '''' +
      QRBaseParameters.FPlantFrom + '''', 0);
    if CountryId <> 0 then
      CountryCnt :=
        SystemDM.GetDBValue('SELECT COUNT(*) ' +
          ' FROM ABSENCEREASONPERCOUNTRY WHERE COUNTRY_ID = ' +
          IntToStr(CountryId), 0);
    if (CountryId <> 0) and (CountryCnt <> 0) then
      AbsenceReasonCountryJoin :=
        '  INNER JOIN ABSENCEREASONPERCOUNTRY R2 ON ' + NL +
        '     R.ABSENCEREASON_ID = R2.ABSENCEREASON_ID AND ' + NL +
        '     R2.COUNTRY_ID = ' + IntToStr(CountryId);
  end;

  SelectStr := SelectStr +
    'FROM ' + NL +
    '  ABSENCEHOURPEREMPLOYEE A INNER JOIN PLANT P ON ' + NL +
    '    A.PLANT_CODE = P.PLANT_CODE ' + NL +
    '    AND A.ABSENCEHOUR_DATE >= :FSTARTDATE ' + NL +
    '    AND A.ABSENCEHOUR_DATE <= :FENDDATE ' + NL +
    '  INNER JOIN EMPLOYEE E ON ' + NL +
    '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL +
    // RV050.4.2.
    '  INNER JOIN DEPARTMENT D ON ' + NL +
    '    A.PLANT_CODE = D.PLANT_CODE ' + NL +
    '    AND E.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL;
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) then
//    if not AllEmployees then
    if not QRParameters.FAllEmployees then
      SelectStr := SelectStr +
        '  AND E.EMPLOYEE_NUMBER >= ' +
        QRBaseParameters.FEmployeeFrom + NL +
        '  AND E.EMPLOYEE_NUMBER <= ' +
        QRBaseParameters.FEmployeeTo + NL;
  SelectStr := SelectStr +
    '  INNER JOIN WEEK WK ON ' + NL +
    '    WK.COMPUTERNAME = ' + '''' +
    SystemDM.CurrentComputerName + '''' + ' AND ' + NL +
    '    A.ABSENCEHOUR_DATE >= WK.DATEFROM AND ' + NL +
    '    A.ABSENCEHOUR_DATE <= WK.DATETO ' + NL;
  if not AllPlants then
    SelectStr := SelectStr +
      '  AND A.PLANT_CODE >= ''' +
      DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
      '  AND A.PLANT_CODE <= ''' +
      DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL;
  SelectStr := SelectStr +
    '  INNER JOIN ABSENCEREASON R ON ' + NL +
    '    A.ABSENCEREASON_ID = R.ABSENCEREASON_ID ' + NL;
  if not QRParameters.FAllAbsReason  then
    SelectStr := SelectStr +
      '  AND R.ABSENCEREASON_CODE >= ''' +
      DoubleQuote(QRParameters.FAbsenceFrom) + '''' + NL +
      '  AND R.ABSENCEREASON_CODE <= ''' +
      DoubleQuote(QRParameters.FAbsenceTo) + '''' + NL +
    AbsenceReasonCountryJoin + NL; // RV084.1.

  // RV050.4.2.
  if (QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) or
    (QRParameters.FShowDept) or (QRParameters.FShowTeam) or
    (not QRParameters.FAllTeam) then
  begin
    // MR:31-01-2006 Always join on employee!
{
    SelectStr := SelectStr +
      '  INNER JOIN EMPLOYEE E ON ' + NL +
      '    A.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' + NL;
    if not AllEmployees then
      SelectStr := SelectStr +
        '  AND A.EMPLOYEE_NUMBER >= ' +
        QRBaseParameters.FEmployeeFrom + NL +
        '  AND A.EMPLOYEE_NUMBER <= ' +
        QRBaseParameters.FEmployeeTo + NL;
}
    // RV050.4.2.
{
    SelectStr := SelectStr +
      '  INNER JOIN DEPARTMENT D ON ' + NL +
      '    A.PLANT_CODE = D.PLANT_CODE ' + NL +
      '    AND E.DEPARTMENT_CODE = D.DEPARTMENT_CODE ' + NL +
      '    AND D.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '    AND D.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '    AND D.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '    AND D.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
}
    if not QRParameters.FTeamEMP then
      SelectStr := SelectStr +
        '   INNER JOIN TEAM T ON ' + NL +
        '      E.TEAM_CODE = T.TEAM_CODE ' + NL
    else
      SelectStr := SelectStr +
        '   LEFT JOIN TEAM T ON ' + NL +
        '      E.TEAM_CODE = T.TEAM_CODE ' + NL;
    if not AllTeams then
      SelectStr := SelectStr +
        '  AND E.TEAM_CODE >= ''' +
        DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        '  AND E.TEAM_CODE <= ''' +
        DoubleQuote(QRParameters.FTeamTo) + '''' + NL;

    // RV050.4.2.
    SelectStr := SelectStr +
      ' WHERE ' + NL +
      '    D.DEPARTMENT_CODE >= ''' +
      DoubleQuote(QRParameters.FDeptFrom) + '''' + NL +
      '    AND D.DEPARTMENT_CODE <= ''' +
      DoubleQuote(QRParameters.FDeptTo) + '''' + NL +
      '    AND D.BUSINESSUNIT_CODE >= ''' +
      DoubleQuote(QRParameters.FBusinessFrom) + '''' + NL +
      '    AND D.BUSINESSUNIT_CODE <= ''' +
      DoubleQuote(QRParameters.FBusinessTo) + '''' + NL;
  end;

  SelectStr := SelectStr + ' ' +
    'GROUP BY ' + NL +
    '  A.PLANT_CODE, P.DESCRIPTION ' + NL;
  if (QRParameters.FShowDept) then
    SelectStr := SelectStr +
      ', D.DEPARTMENT_CODE, D.DESCRIPTION ' + NL;
  if (QRParameters.FShowTeam) then
    SelectStr := SelectStr +
      ', E.TEAM_CODE, T.DESCRIPTION ' + NL;
  if (QRParameters.FShowEmpl) then
    SelectStr := SelectStr +
      ', A.EMPLOYEE_NUMBER, E.DESCRIPTION ' + NL;

  if QRParameters.FSort = SortOnAbsenceReason then
  begin
    SelectStr := SelectStr +
      ', A.ABSENCEREASON_ID, R.ABSENCEREASON_CODE, ' + NL +
      ' R.DESCRIPTION, WK.WEEKNUMBER, ' + NL;
  end
  else
  begin
    SelectStr := SelectStr +
      ', WK.WEEKNUMBER, A.ABSENCEREASON_ID, ' + NL +
      ' R.ABSENCEREASON_CODE, R.DESCRIPTION, ' + NL;
  end;
  if not QRParameters.FShowDept then // RV050.4.2.
    SelectStr := SelectStr +
      '  D.DEPARTMENT_CODE, ' + NL;
  SelectStr := SelectStr +
    ' A.ABSENCEHOUR_DATE ' + NL;

  if QRParameters.FReportType = 0 then
    SelectStr := SelectStr +
      ' UNION ' + NL + FillHrsPerType
  else
    SelectStr := SelectStr +
      ' UNION ' + NL + FillSalHrsPerType;

  OrderStr := ' ' +
    'ORDER BY ' + NL +
    '  PLANT_CODE ' + NL;
  if (QRParameters.FShowDept) then
    OrderStr := OrderStr +
      '  ,DEPARTMENT_CODE ' + NL;
  if (QRParameters.FShowTeam) then
    OrderStr := OrderStr +
      '  ,TEAM_CODE ' + NL;
  if (QRParameters.FShowEmpl) then
    OrderStr := OrderStr +
      '  ,EMPLOYEE_NUMBER ' +  NL;
  if QRParameters.FSort = SortOnAbsenceReason then
    OrderStr := OrderStr +
      ' ,ABSENCEREASON_CODE, WEEKNUMBER, REPORT_DATE ' + NL
  else
    OrderStr := OrderStr +
      ' ,WEEKNUMBER, ABSENCEREASON_CODE, REPORT_DATE ' + NL;

  SelectStr := SelectStr + NL + OrderStr;
  with ReportHrsPerDayDM do
  begin
    QueryAbsence.Active := False;
//    QueryAbsence.Unidirectional := False;
    QueryAbsence.SQL.Clear;
// RV084.2. Do not use uppercase! Absencereasons can be lowercase!
//    QueryAbsence.SQL.Add(UpperCase(SelectStr));
    QueryAbsence.SQL.Add(SelectStr);
//
// QueryAbsence.SQL.SaveToFile('c:\temp\RepHrsPerDay.sql');
//
    QueryAbsence.ParamByName('FSTARTDATE').Value := FMinDate;
    QueryAbsence.ParamByName('FENDDATE').Value := FMaxDate;
    if not QueryAbsence.Prepared then
      QueryAbsence.Prepare;
    QueryAbsence.Active := True;
    Result := not QueryAbsence.IsEmpty;
  end;
  FirstTimeDetail := True;
 //CAR : Update progress indicator
  if Result then
  begin
    ReportHrsPerDayDM.cdsQueryBUWK.Close;
    ReportHrsPerDayDM.cdsQueryBUWK.Open;
    ProgressIndicatorPos := 0;
    Report_RecordCount := ReportHrsPerDayDM.QueryAbsence.RecordCount;
  end;
  {check if report is empty}
  Screen.Cursor := crDefault;
  FTeam := '';
  FPlant := '';
  FDept := '';
  FEmpl := -1;
end;

procedure TReportHrsPerDayQR.ConfigReport;
var
  Year, Week, Day: Word;
  procedure FillHeader(CaptionWeek, CaptionAbsence: String);
  begin
    QRLabelWeekPlant.Caption := CaptionWeek;
    QRLabelWeekDept.Caption :=  CaptionWeek;
    QRLabelWeekEmpl.Caption := CaptionWeek;
    QRLabelWeekTeam.Caption := CaptionWeek;
    QRLabelAbsReasonPlant.Caption := CaptionAbsence;
    QRLabelAbsReasonDept.Caption := CaptionAbsence;
    QRLabelAbsReasonEmpl.Caption := CaptionAbsence;
    QRLabelAbsReasonTeam.Caption := CaptionAbsence;
  end;
begin
  SuppressZeroes := QRParameters.FSuppressZeroes;
  // MR:12-12-2002
  ExportClass.ExportDone := False;
  FTotalText := SRepHrsPerEmplTotal;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  QRLabelBusinessFrom.Caption := QRParameters.FBusinessFrom;
  QRLabelBusinessTo.Caption := QRParameters.FBusinessTo;
  QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
  QRLabelDeptTo.Caption := QRParameters.FDeptTo;
  if QRParameters.FAllAbsReason then
  begin
    QRLabelAbsReasonFrom.Caption := '*';
    QRLabelAbsReasonTo.Caption := '*';
  end
  else
  begin
    QRLabelAbsReasonFrom.Caption := QRParameters.FAbsenceFrom;
    QRLabelAbsReasonTo.Caption := QRParameters.FAbsenceTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelEmployeeFrom.Caption := QRBaseParameters.FEmployeeFrom;
  QRLabelEmployeeTo.Caption := QRBaseParameters.FEmployeeTo;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);

  // RV050.4.
  if QRParameters.FAllEmployees then
  begin
    QRLabelEmployeeFrom.Caption := '*';
    QRLabelEmployeeTo.Caption := '*';
  end;

  ListProcsF.WeekUitDat(QRParameters.FDateFrom, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  QRLabelWeekFrom.Caption := IntToStr(Week);
  QRLabelDayFrom.Caption := IntToStr(Day);
  ListProcsF.WeekUitDat(QRParameters.FDateTo, Year, Week);
  Day := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  QRLabelWeekTo.Caption := IntToStr(Week);
  QRLabelDayTo.Caption := IntToStr(Day) + ')';

  ChildBandPlantWeekAbs.Enabled:= True;
  QRGroupHDDept.Enabled := False;
  ChildBandDeptWeekAbs.Enabled := False;
  QRGroupHDTeam.Enabled := False;
  ChildBandTeamWeekAbs.Enabled := False;
  QRBandFooterTeam.Enabled := False;
  QRGroupHDEmpl.Enabled := False;
  ChildBandEmplWeek.Enabled := False;
  QRBandFooterEmpl.Enabled := False;
  QRBandFooterDept.Enabled := False;
  if QRParameters.FShowDept then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDDept.Enabled := True;
    ChildBandDeptWeekAbs.Enabled := True;
    QRBandFooterDept.Enabled := True;
  end;
  if QRParameters.FShowTeam then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDTeam.Enabled := True;
    ChildBandTeamWeekAbs.Enabled := True;
    QRBandFooterTeam.Enabled := True;
  end;
  if QRParameters.FShowEmpl then
  begin
    ChildBandPlantWeekAbs.Enabled := False;
    QRGroupHDEmpl.Enabled := True;
    ChildBandEmplWeek.Enabled := True;
    QRBandFooterEmpl.Enabled := True;
  end;
  if (QRParameters.FShowEmpl) and (QRParameters.FShowDept) then
  begin
    ChildBandDeptWeekAbs.Enabled := False;
  end;
  if (QRParameters.FShowEmpl) and (QRParameters.FShowTeam) then
  begin
    ChildBandTeamWeekAbs.Enabled := False;
  end;
  FillHeader('', '');
  if (QRParameters.FSort = SortOnAbsenceReason) then
    FillHeader(SHeaderAbsRsn, SHeaderWeekAbsRsn)
  else
    FillHeader(SHeaderWeekAbsRsn, SHeaderAbsRsn);
  // MR:16-04-2004
  BookmarkSet := False;
end;

procedure TReportHrsPerDayQR.FreeMemory;
begin
  inherited;
  // MR:28-03-2006 ExportClass was not freed.
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportHrsPerDayQR.FormCreate(Sender: TObject);
begin
  inherited;
  FQRParameters := TQRParameters.Create;
  // MR:12-12-2002
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportHrsPerDayQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  MyAbsenceReason, MyWeek: String;
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  FEmpl := 0;
  FPlant := '';
  FDept := '';
  FTeam := '';
 //
 if QRParameters.FReportType = 0 then
   QRLabelShowReportType.Caption := SPimsHoursPerDayProduction
 else
   QRLabelShowReportType.Caption := SPimsHoursPerDaySalary;

  // MR:12-12-2002
  // Export Header (Column-names)
  if QRParameters.FExportToFile then
    if (not ExportClass.ExportDone) then
    begin
      ExportClass.AskFilename;
      ExportClass.ClearText;
      if QRParameters.FShowSelection then
      begin
        // Selections
        ExportClass.AddText(
          QRLabel11.Caption
          );
        // From Plant PlantFrom to PlantTo
        ExportClass.AddText(
          QRLabel13.Caption + ' ' +
          QRLabel1.Caption + ' ' +
          QRLabelPlantFrom.Caption + ' ' +
          QRLabel49.Caption + ' ' +
          QRLabelPlantTo.Caption
          );
        // From BusinessUnit BUFrom to BUTo
        ExportClass.AddText(
          QRLabel25.Caption + ' ' +
          QRLabel48.Caption + ' ' +
          QRLabelBusinessFrom.Caption + ' ' +
          QRLabel16.Caption + ' ' +
          QRLabelBusinessTo.Caption
          );
        // From Department DepartmentFrom to DepartmentTo
        ExportClass.AddText(
          QRLabel47.Caption + ' ' +
          QRLabelDepartment.Caption + ' ' +
          QRLabelDeptFrom.Caption + ' ' +
          QRLabel50.Caption + ' ' +
          QRLabelDeptTo.Caption
          );
        // From Team TeamFrom to TeamTo
        ExportClass.AddText(
          QRLabel87.Caption + ' ' +
          QRLabel88.Caption + ' ' +
          QRLabelTeamFrom.Caption + ' ' +
          QRLabel90.Caption + ' ' +
          QRLabelTeamTo.Caption
          );
        // From Employee EmployeeFrom to EmployeeTo
        ExportClass.AddText(
          QRLabel18.Caption + ' ' +
          QRLabel20.Caption + ' ' +
          QRLabelEmployeeFrom.Caption + ' ' +
          QRLabel22.Caption + ' ' +
          QRLabelEmployeeTo.Caption
          );
        // From AbsenceReason AbsReasonFrom to AbsReasonTo
        ExportClass.AddText(
          QRLabel51.Caption + ' ' +
          QRLabelAbsenceRsn.Caption + ' ' +
          QRLabelAbsReasonFrom.Caption + ' ' +
          QRLabel53.Caption + ' ' +
          QRLabelAbsReasonTo.Caption
          );
        // From Date to Date
        ExportClass.AddText(
          QRLabelFromDate.Caption + ' ' +
          QRLabelDate.Caption + ' ' +
          QRLabelDateFrom.Caption + ' ' +
          QRLabelToDate.Caption + ' ' +
          QRLabelDateTo.Caption
          );
      end;
      if (QRParameters.FSort = SortOnAbsenceReason) then
      begin
        MyAbsenceReason := SHeaderAbsRsn;
        MyWeek := SHeaderWeekAbsRsn;
      end
      else
      begin
        MyAbsenceReason := SHeaderWeekAbsRsn;
        MyWeek := SHeaderAbsRsn;
      end;
      // Show Column-names
      if (QRParameters.FSort = SortOnAbsenceReason) then
        ExportDetail(
          '',
          MyAbsenceReason,
          '',
          MyWeek,
          SystemDM.GetDayWCode(1),
          SystemDM.GetDayWCode(2),
          SystemDM.GetDayWCode(3),
          SystemDM.GetDayWCode(4),
          SystemDM.GetDayWCode(5),
          SystemDM.GetDayWCode(6),
          SystemDM.GetDayWCode(7),
          QRLabel5.Caption
          )
      else
        ExportDetail(
          '',
          MyAbsenceReason,
          MyWeek,
          '',
          SystemDM.GetDayWCode(1),
          SystemDM.GetDayWCode(2),
          SystemDM.GetDayWCode(3),
          SystemDM.GetDayWCode(4),
          SystemDM.GetDayWCode(5),
          SystemDM.GetDayWCode(6),
          SystemDM.GetDayWCode(7),
          QRLabel5.Caption
          );
    end;
end;

// Businessunitperworkspot and Percentage
function TReportHrsPerDayQR.DetermineSUMMIN: Double;
begin
  with ReportHrsPerDayDM do
  begin
    Result := QueryAbsence.FieldByName('SUMMIN').AsInteger;
    // Only do this for the second select of the UNIONS.
    // Only this select is of correct type (join to BUSINESSUNITPERWORKSPOT).
    if QueryAbsence.FieldByName('LISTCODE').AsInteger = 2 then
    begin
      if cdsQueryBUWK.Locate('PLANT_CODE;WORKSPOT_CODE;BUSINESSUNIT_CODE',
        VarArrayOf([QueryAbsence.FieldByName('PLANT_CODE').AsString,
          QueryAbsence.FieldByName('WORKSPOT_CODE').AsString,
          QueryAbsence.FieldByName('BUCODE').AsString]), []) then
        Result :=
          cdsQueryBUWK.FieldByName('SUMPERC').AsFloat / 100 *
            QueryAbsence.FieldByName('SUMMIN').AsInteger;
      if Result = 0 then
        Result := QueryAbsence.FieldByName('SUMMIN').AsInteger;
    end;
// WDebugLog('SUMMIN=' + Format('%f', [Result]));    
  end;
end;

procedure TReportHrsPerDayQR.InitializeAmountsPerDaysOfWeek(
  var AmountsPerDaysOfWeek: TAmountsPerDay);
var
  Counter: Integer;
begin
  for Counter := 1 to 7 do
    AmountsPerDaysOfWeek[Counter] := 0;
end;

function TReportHrsPerDayQR.Summarize(
  AmountsPerDaysOfWeek: TAmountsPerDay): Integer;
var
  Counter: Integer;
  Total: Double;
begin
  Total := 0;
  for Counter := 1 to 7 do
    Total := Total + AmountsPerDaysOfWeek[Counter];
  Result := Round(Total);
end;

procedure TReportHrsPerDayQR.FillAmountsPerDaysOfWeek(IndexMin: Integer;
  AmountsPerDaysOfWeek: TAmountsPerDay; PrintDecide: Boolean);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
  PrintItem: Boolean;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
      begin
        PrintItem := True;
        if PrintDecide then
          PrintItem := PrintTotalOnDays[Index - IndexMin + 1];
        if PrintItem then
          (TempComponent as TQRLabel).Caption :=
            DecodeHrsMin(Round(AmountsPerDaysOfWeek[Index - IndexMin + 1]))
        else
          (TempComponent as TQRLabel).Caption := ' '
      end;
    end;
  end;
end;

procedure TReportHrsPerDayQR.FillDescDaysPerWeek(IndexMin: Integer);
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= IndexMin) and (Index <= IndexMin + 6) then
      begin
        (TempComponent as TQRLabel).Caption :=
          SystemDM.GetDayWCode(Index - IndexMin + 1);
      end;
    end;
  end;
end;

procedure TReportHrsPerDayQR.ChildBandPlantWeekAbsBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  //tag 10-16
  FillDescDaysPerWeek(10);
end;

procedure TReportHrsPerDayQR.QRBandFooterEmplBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(50, TotalEmpl, False);
  // MR:14-11-2005 Don't show the TOTAL UNKNOWN BUSINESSUNIT
  PrintBand := False;
(*
  if QRParameters.FShowEmpl then
    PrintBand := DetermineUnknown(QRLabelTotalUnknownEmp1);
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    if (Summarize(TotalUnknown) <> 0) then
      ExportDetail(
        QRLabel120.Caption,
        '',
        '',
        '',
        IntToStr(Round(TotalUnknown[1])),
        IntToStr(Round(TotalUnknown[2])),
        IntToStr(Round(TotalUnknown[3])),
        IntToStr(Round(TotalUnknown[4])),
        IntToStr(Round(TotalUnknown[5])),
        IntToStr(Round(TotalUnknown[6])),
        IntToStr(Round(TotalUnknown[7])),
        IntToStr(Summarize(TotalUnknown))
        );
*)
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel34.Caption,
      IntToStr(FEmpl),
      FEmplDesc,
      '',
      IntToStr(Round(TotalEmpl[1])),
      IntToStr(Round(TotalEmpl[2])),
      IntToStr(Round(TotalEmpl[3])),
      IntToStr(Round(TotalEmpl[4])),
      IntToStr(Round(TotalEmpl[5])),
      IntToStr(Round(TotalEmpl[6])),
      IntToStr(Round(TotalEmpl[7])),
      IntToStr(Summarize(TotalEmpl))
      );
end;

procedure TReportHrsPerDayQR.ChildBandFooterEmpl2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  if QRParameters.FShowEmpl then
    PrintBand := True;
end;

procedure TReportHrsPerDayQR.QRGroupHDEmplBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  // MR:16-04-2004
  QRGroupHdEmpl.ForceNewPage := QRParameters.FPageEmpl;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalUnknown);
  InitializeAmountsPerDaysOfWeek(TotalEmpl);
  InitializeAmountsPerDaysOfWeek(TotalReportType);
  InitializeAmountsPerDaysOfWeek(TotalOnDetail);
  FirstTimeDetail := True;
  FEmpl := ReportHrsPerDayDM.QueryAbsence.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FEmplDesc := ReportHrsPerDayDM.QueryAbsence.FieldByName('EDESC').AsString;
    ExportClass.AddText(
      QRLabel3.Caption + ExportClass.Sep +
      IntToStr(FEmpl) + ExportClass.Sep + FEmplDesc
      );
  end;
end;

procedure TReportHrsPerDayQR.QRGroupHDDeptBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdDept.ForceNewPage := QRParameters.FPageDept;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalUnknown);
  InitializeAmountsPerDaysOfWeek(TotalDept);
  InitializeAmountsPerDaysOfWeek(TotalReportType);
  InitializeAmountsPerDaysOfWeek(TotalOnDetail);
  FirstTimeDetail := True;
  FDept := ReportHrsPerDayDM.QueryAbsence.FieldByName('DEPARTMENT_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FDeptDesc := ReportHrsPerDayDM.QueryAbsence.FieldByName('DDESC').AsString;
    ExportClass.AddText(
      QRLabel55.Caption + ExportClass.Sep +
      FDept + ExportClass.Sep + FDeptDesc
      );
  end;
end;

procedure TReportHrsPerDayQR.QRGroupHDPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalUnknown);
  InitializeAmountsPerDaysOfWeek(TotalPlant);
  InitializeAmountsPerDaysOfWeek(TotalReportType);
  InitializeAmountsPerDaysOfWeek(TotalOnDetail);
  InitializeAmountsPerDaysOfWeek(TotalAbsenceReason);
  FirstTimeDetail := True;
  FPlant := ReportHrsPerDayDM.QueryAbsence.FieldByName('PLANT_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FPlantDesc := ReportHrsPerDayDM.QueryAbsence.FieldByName('PDESC').AsString;
    ExportClass.AddText(
      QRLabel54.Caption + ExportClass.Sep +
      FPlant + ExportClass.Sep + FPlantDesc
      );
  end;
  // TESTING
{  with ReportHrsPerDayDM do
  begin
    WDebugLog(FPlant);
  end; }
end;

procedure TReportHrsPerDayQR.QRBandFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(60, TotalDept, False);
  // MR:14-11-2005 Don't show the TOTAL UNKNOWN BUSINESSUNIT
  PrintBand := False;
(*
  PrintBand := QRParameters.FShowDept and (not QRParameters.FShowEmpl);
  if PrintBand then
  begin
    PrintBand := DetermineUnknown(QRLabelTotalUnknownDept);
    if PrintBand then
      if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
        ExportDetail(
          QRLabel136.Caption,
          '',
          '',
          '',
          IntToStr(Round(TotalUnknown[1])),
          IntToStr(Round(TotalUnknown[2])),
          IntToStr(Round(TotalUnknown[3])),
          IntToStr(Round(TotalUnknown[4])),
          IntToStr(Round(TotalUnknown[5])),
          IntToStr(Round(TotalUnknown[6])),
          IntToStr(Round(TotalUnknown[7])),
          IntToStr(Summarize(TotalUnknown))
          );
  end;
*)
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel21.Caption,
      FDept,
      FDeptDesc,
      '',
      IntToStr(Round(TotalDept[1])),
      IntToStr(Round(TotalDept[2])),
      IntToStr(Round(TotalDept[3])),
      IntToStr(Round(TotalDept[4])),
      IntToStr(Round(TotalDept[5])),
      IntToStr(Round(TotalDept[6])),
      IntToStr(Round(TotalDept[7])),
      IntToStr(Summarize(TotalDept))
      );
end;

procedure TReportHrsPerDayQR.QRBandFooterPlantBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(70, TotalPlant, False);
  // MR:14-11-2005 Don't show the TOTAL UNKNOWN BUSINESSUNIT
  PrintBand := False;
(*
  PrintBand := not (QRParameters.FShowDept or QRParameters.FShowTeam or
    QRParameters.FShowEmpl);
  if PrintBand then
  begin
    PrintBand := DetermineUnknown(QRLabelTotalUnknownPlant);
    if PrintBand then
      if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
        ExportDetail(
          QRLabel144.Caption,
          '',
          '',
          '',
          IntToStr(Round(TotalUnknown[1])),
          IntToStr(Round(TotalUnknown[2])),
          IntToStr(Round(TotalUnknown[3])),
          IntToStr(Round(TotalUnknown[4])),
          IntToStr(Round(TotalUnknown[5])),
          IntToStr(Round(TotalUnknown[6])),
          IntToStr(Round(TotalUnknown[7])),
          IntToStr(Summarize(TotalUnknown))
          );
  end;
*)
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel23.Caption,
      FPlant,
      FPlantDesc,
      '',
      IntToStr(Round(TotalPlant[1])),
      IntToStr(Round(TotalPlant[2])),
      IntToStr(Round(TotalPlant[3])),
      IntToStr(Round(TotalPlant[4])),
      IntToStr(Round(TotalPlant[5])),
      IntToStr(Round(TotalPlant[6])),
      IntToStr(Round(TotalPlant[7])),
      IntToStr(Summarize(TotalPlant))
      );
end;

procedure TReportHrsPerDayQR.QRLabelTotalEmplPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalEmpl));
end;

procedure TReportHrsPerDayQR.QRLabelTotalDeptPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalDept));
end;

procedure TReportHrsPerDayQR.QRLabelTotalPlantPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalPlant));
end;

procedure TReportHrsPerDayQR.QRGroupHDTeamBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdTeam.ForceNewPage := QRParameters.FPageTeam;
  inherited;
  InitializeAmountsPerDaysOfWeek(TotalUnknown);
  InitializeAmountsPerDaysOfWeek(TotalTeam);
  InitializeAmountsPerDaysOfWeek(TotalReportType);
  InitializeAmountsPerDaysOfWeek(TotalOnDetail);
  FirstTimeDetail := True;
  FTeam := ReportHrsPerDayDM.QueryAbsence.FieldByName('TEAM_CODE').AsString;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    FTeamDesc := ReportHrsPerDayDM.QueryAbsence.FieldByName('TDESC').AsString;
    ExportClass.AddText(
      QRLabel35.Caption + ExportClass.Sep +
      FTeam + ExportClass.Sep + FTeamDesc
      );
  end;
end;

procedure TReportHrsPerDayQR.QRBandFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillAmountsPerDaysOfWeek(90, TotalTeam, False);
  // MR:14-11-2005 Don't show the TOTAL UNKNOWN BUSINESSUNIT
  PrintBand := False;
(*
  PrintBand := QRParameters.FShowTeam and (not QRParameters.FShowEmpl);
  if PrintBand then
  begin
    PrintBand := DetermineUnknown(QRLabelTotalUnknownTeam);
    if PrintBand then
      if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
        ExportDetail(
          QRLabel128.Caption,
          '',
          '',
          '',
          IntToStr(Round(TotalUnknown[1])),
          IntToStr(Round(TotalUnknown[2])),
          IntToStr(Round(TotalUnknown[3])),
          IntToStr(Round(TotalUnknown[4])),
          IntToStr(Round(TotalUnknown[5])),
          IntToStr(Round(TotalUnknown[6])),
          IntToStr(Round(TotalUnknown[7])),
          IntToStr(Summarize(TotalUnknown))
          );
  end;
*)
  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      QRLabel78.Caption,
      FTeam,
      FTeamDesc,
      '',
      IntToStr(Round(TotalTeam[1])),
      IntToStr(Round(TotalTeam[2])),
      IntToStr(Round(TotalTeam[3])),
      IntToStr(Round(TotalTeam[4])),
      IntToStr(Round(TotalTeam[5])),
      IntToStr(Round(TotalTeam[6])),
      IntToStr(Round(TotalTeam[7])),
      IntToStr(Summarize(TotalTeam))
      );
end;

procedure TReportHrsPerDayQR.QRLabel86Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalTeam));
end;

procedure TReportHrsPerDayQR.QRBandSummaryAfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin
  inherited;
  // MR:12-12-2002
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportHrsPerDayQR.QRDBText15Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportHrsPerDayDM.QueryAbsence.FieldByName('TDESC').AsString, 0 , 15);
end;

procedure TReportHrsPerDayQR.QRDBText9Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportHrsPerDayDM.QueryAbsence.FieldByName('EDESC').AsString, 0, 15);
end;

procedure TReportHrsPerDayQR.QRDBText7Print(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(ReportHrsPerDayDM.QueryAbsence.FieldByName('DDESC').AsString,0, 15);
end;

procedure TReportHrsPerDayQR.QRLabelTotalReportTypePrint(sender: TObject;
  var Value: String);
begin
  inherited;
  if QRParameters.FSort = SortOnAbsenceReason then
    Value := '';
end;

procedure TReportHrsPerDayQR.QRLabelTotalReportType_RWPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  if QRParameters.FSort = SortOnWeek then
  begin
(*
    if QRLabelDetail.Caption <> '' then
      Value := IntToStr(FWeek)
    else
      Value := '';
*)      
  end;
end;

procedure TReportHrsPerDayQR.ChildBandProdEmployeeBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // MR:14-11-2005 Don't show the TOTAL UNKNOWN BUSINESSUNIT
  PrintBand := False;
(*
  PrintBand := (QRParameters.FSort = 0) and (QRParameters.FShowEmpl);
  if PrintBand then
    PrintBand := DetermineUnknown(QRLabelTotalUnknownEmp2);
*)
end;

procedure TReportHrsPerDayQR.ChildBandProdEmlpoyee2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := (QRParameters.FSort = SortOnAbsenceReason) and (QRParameters.FShowEmpl);
end;
(*
function TReportHrsPerDayQR.DetermineUnknown(AQRLabel: TQRLabel): Boolean;
var
  Total_Unknown: Integer;
begin
  Total_Unknown := Summarize(TotalUnknown);
  Result := Total_Unknown <> 0;
  if Result then
  begin
    AQRLabel.Caption := DecodeHrsMin(Total_Unknown);
    FillAmountsPerDaysOfWeek(30, TotalUnknown, False);
  end;
end;
*)
procedure TReportHrsPerDayQR.ChildBandFooterTeamBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowTeam;
end;

procedure TReportHrsPerDayQR.ChildBandFooterDeptBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowDept;
end;

procedure TReportHrsPerDayQR.QRGroupHDWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  InitializeAmountsPerDaysOfWeek(TotalOnDays);
  InitializeAmountsPerDaysOfWeek(TotalUnknownOnDays);
end;

procedure TReportHrsPerDayQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  AbsDate: TDateTime;
  Year, Week: Word;
  Count: Integer;
begin
  inherited;
  PrintBand := False;

  with ReportHrsPerDayDM do
  begin
    AbsDate := QueryAbsence.FieldByName('REPORT_DATE').AsDateTime;
    ListProcsF.WeekUitDat(AbsDate, Year, Week);
    Count := ListProcsF.DayWStartOnFromDate(AbsDate);

    // MR:11-11-2005 If LISTCODE = 3 (UNKNOWN BUSINESS UNIT)
    // and if ONE PLANT and NOT ALL BUSINESSUNITS selected,
    // save the result in 'unknown'.
    if (QueryAbsence.FieldByName('LISTCODE').AsInteger = 3) and
      ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
       (not AllBusinessUnits)) then
      TotalUnknownOnDays[Count] := TotalUnknownOnDays[Count] + DetermineSUMMIN
    else
      TotalOnDays[Count] := TotalOnDays[Count] + DetermineSUMMIN;

(*
//  !!!Testing!!!
    QRLabel92.Caption :=
      Format('Week=%-2d Date=%-12s Count=%-2d ListCode=%d Summin=%d',
        [Week, DateToStr(AbsDate), Count,
        QueryAbsence.FieldByName('LISTCODE').AsInteger,
        Round(DetermineSUMMIN)]);
*)
  end;
end;

procedure TReportHrsPerDayQR.DetermineCaptions(
  AQRLabelWeekReason, AQRLabelReasonWeek: TQRLabel;
  AUnknownBusinessUnit: String);
var
  Text1, Text2, Text3: String;
begin
  with ReportHrsPerDayDM do
  begin
    if (QRParameters.FSort = SortOnAbsenceReason) then // Reason, Week
    begin
      if (AUnknownBusinessUnit = '') then
      begin
        Text1 := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
        Text2 := QueryAbsence.FieldByName('ABSDESC').AsString;
      end
      else
      begin
        Text1 := AUnknownBusinessUnit;
        Text2 := '';
      end;
      Text3 := IntToStr(QueryAbsence.FieldByName('WEEKNUMBER').AsInteger);
      if (Text1 = '') then
      begin
        Text2 := '';
        if (QRParameters.FReportType = 0) then
          Text1 := SPimsHoursPerDayProduction
        else
          Text1 := SPimsHoursPerDaySalary;
      end;
      AQRLabelWeekReason.Caption := Text1 + ' ' + Text2;
      AQRLabelReasonWeek.Caption := Text3;
      if (AUnknownBusinessUnit = '') then
      begin
        if (PreviousAbsenceReason = AQRLabelWeekReason.Caption) then
          AQRLabelWeekReason.Caption := '';
        if (PreviousWeek = AQRLabelReasonWeek.Caption) then
          AQRLabelReasonWeek.Caption := '';
      end;
      PreviousAbsenceReason := Text1 + ' ' + Text2;
      PreviousWeek := Text3;
    end
    else
    begin // Week, Reason
      Text1 := IntToStr(QueryAbsence.FieldByName('WEEKNUMBER').AsInteger);
      if (AUnknownBusinessUnit = '') then
      begin
        Text2 := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString;
        Text3 := QueryAbsence.FieldByName('ABSDESC').AsString;
      end
      else
      begin
        Text2 := AUnknownBusinessUnit;
        Text3 := '';
      end;
      if (Text2 = '') then
      begin
        Text3 := '';
        if (QRParameters.FReportType = 0) then
          Text2 := SPimsHoursPerDayProduction
        else
          Text2 := SPimsHoursPerDaySalary;
      end;
      AQRLabelWeekReason.Caption := Text1;
      AQRLabelReasonWeek.Caption := Text2 + ' ' + Text3;
      if (PreviousWeek = AQRLabelWeekReason.Caption) then
        AQRLabelWeekReason.Caption := '';
      if (PreviousAbsenceReason = AQRLabelReasonWeek.Caption) then
        AQRLabelReasonWeek.Caption := '';
      PreviousWeek := Text1;
      PreviousAbsenceReason := Text2 + ' ' + Text3;
    end;
  end;
end;

procedure TReportHrsPerDayQR.QRBandGroupFooterWeekNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Count: Integer;
  Year, Month, Day: Word;
  AbsDate: TDateTime;
begin
  inherited;
  PrintBand := True;
  // MR:14-11-2005 Always show the production on week-level.
(*
  with ReportHrsPerDayDM do
  begin
    // If 'show <none>' and 'sort on reason' and 'no-production' then
    // do not show the details.
    if not (QRParameters.FShowDept or QRParameters.FShowTeam or
      QRParameters.FShowEmpl) then
      if (QRParameters.FSort = 0) and
        (QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString = '') then
        PrintBand := False;
  end;
*)
  for Count := 1 to 7 do
  begin
    // Set totals
    TotalAbsenceReason[Count] := TotalAbsenceReason[Count] +
      TotalOnDays[Count] + TotalUnknownOnDays[Count];
    TotalUnknown[Count] := TotalUnknown[Count] +
      TotalUnknownOnDays[Count];
    TotalEmpl[Count] := TotalEmpl[Count] + TotalOnDays[Count] +
      TotalUnknownOnDays[Count];
    TotalDept[Count] := TotalDept[Count] + TotalOnDays[Count] +
      TotalUnknownOnDays[Count];
    TotalPlant[Count] := TotalPlant[Count] + TotalOnDays[Count] +
      TotalUnknownOnDays[Count];
    TotalTeam[Count] := TotalTeam[Count] + TotalOnDays[Count] +
      TotalUnknownOnDays[Count];
    // Determine if a day-of-the-week should be printed
    DecodeDate(
      ReportHrsPerDayDM.QueryAbsence.FieldByName('REPORT_DATE').AsDateTime,
      Year, Month, Day);
    AbsDate := ListProcsF.DateFromWeek(Year,
      ReportHrsPerDayDM.QueryAbsence.FieldByName('WEEKNUMBER').AsInteger,
      Count);
    PrintTotalOnDays[Count] := ((AbsDate >= QRParameters.FDateFrom) and
      (AbsDate <= QRParameters.FDateTo));
  end;
  if Summarize(TotalOnDays) = 0 then
    PrintBand := False;
  if PrintBand then
  begin
    FillAmountsPerDaysOfWeek(1, TotalOnDays, True);
    QRLabelTotalWeek.Caption := DecodeHrsMin(Summarize(TotalOnDays));
    DetermineCaptions(QRLabelWeekReason, QRLabelReasonWeek, '');

    // MR:12-12-2002
    if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
      ExportDetail(
        '',
        QRLabelWeekReason.Caption,
        QRLabelReasonWeek.Caption,
        '',
        IntToStr(Round(TotalOnDays[1])),
        IntToStr(Round(TotalOnDays[2])),
        IntToStr(Round(TotalOnDays[3])),
        IntToStr(Round(TotalOnDays[4])),
        IntToStr(Round(TotalOnDays[5])),
        IntToStr(Round(TotalOnDays[6])),
        IntToStr(Round(TotalOnDays[7])),
        IntToStr(Summarize(TotalOnDays))
        );
  end;
end;

procedure TReportHrsPerDayQR.ChildBandWeekNumberUnknownBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  TotalDetail: Integer;
begin
  inherited;
  // If show 'salary' then don't print this.
  if (QRParameters.FReportType = 1) then
    PrintBand := False;
  if PrintBand then
  begin
    TotalDetail := Summarize(TotalUnknownOnDays);
    PrintBand := (TotalDetail <> 0);
  end;
  with ReportHrsPerDayDM do
  begin
    if PrintBand then
    begin
      // MR:11-11-2005 (UNKNOWN BUSINESS UNIT)
      // If ONE PLANT and NOT ALL BUSINESSUNITS selected,
      // show the 'unknown'.
      PrintBand := False;
      if ((QRBaseParameters.FPlantFrom = QRBaseParameters.FPlantTo) and
         (not AllBusinessUnits)) then
         PrintBand := True;
      // If 'show <none>' and 'sort on reason' and 'no-production' then
      // do not show the details.
(*
      if not (QRParameters.FShowDept or QRParameters.FShowTeam or
        QRParameters.FShowEmpl) then
         if (QRParameters.FSort = 0) and
          (QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString = '') then
           PrintBand := False;
*)
    end;
    if PrintBand then
    begin
      FillAmountsPerDaysOfWeek(1, TotalUnknownOnDays, True);
      QRLabelTotalWeekUN.Caption := DecodeHrsMin(Summarize(TotalUnknownOnDays));
      DetermineCaptions(QRLabelWeekReasonUN, QRLabelReasonWeekUN,
        SPimsUnknownBusinessUnit);

      // MR:12-12-2002
      if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
        ExportDetail(
          '',
          QRLabelWeekReasonUN.Caption,
          QRLabelReasonWeekUN.Caption,
          '',
          IntToStr(Round(TotalUnknownOnDays[1])),
          IntToStr(Round(TotalUnknownOnDays[2])),
          IntToStr(Round(TotalUnknownOnDays[3])),
          IntToStr(Round(TotalUnknownOnDays[4])),
          IntToStr(Round(TotalUnknownOnDays[5])),
          IntToStr(Round(TotalUnknownOnDays[6])),
          IntToStr(Round(TotalUnknownOnDays[7])),
          IntToStr(Summarize(TotalUnknownOnDays))
          );
    end;
  end;
end;

procedure TReportHrsPerDayQR.QRGroupHDAbsenceReasonBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  InitializeAmountsPerDaysOfWeek(TotalAbsenceReason);
  PreviousAbsenceReason := '';
  PreviousWeek := '';
  // TESTING
{  with ReportHrsPerDayDM do
  begin
    WDebugLog(QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString + ' - ' +
      QueryAbsence.FieldByName('ABSDESC').AsString);
  end; }
end;

procedure TReportHrsPerDayQR.QRBandGroupFooterAbsenceReasonBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Text1, Text2, Text3: String;
begin
  inherited;
  with ReportHrsPerDayDM do
  begin
    if (QRParameters.FSort = SortOnAbsenceReason) then // Reason, Week
    begin
      Text1 := QueryAbsence.FieldByName('ABSENCEREASON_CODE').AsString + ' ';
      Text2 := QueryAbsence.FieldByName('ABSDESC').AsString;
      Text3 := '';
      if (Trim(Text1) = '') then
      begin
        if (QRParameters.FReportType = 0) then // Production
          Text1 := SPimsHoursPerDayProduction
        else // Salary
          Text1 := SPimsHoursPerDaySalary;
        Text2 := '';
      end;
    end
    else // Week, Reason
    begin
      Text1 := SHeaderWeekAbsRsn + ' ';
      Text2 := '';
      Text3 := IntToStr(QueryAbsence.FieldByName('WEEKNUMBER').AsInteger);
    end;
    QRLabelAbsenceReason.Caption := Text1 + Text2 + Text3;
  end;
  FillAmountsPerDaysOfWeek(80, TotalAbsenceReason, False);

  // MR:12-12-2002
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
    ExportDetail(
      '',
      QRLabel6.Caption + ' ' + Text1,
      Text2,
      Text3,
      IntToStr(Round(TotalAbsenceReason[1])),
      IntToStr(Round(TotalAbsenceReason[2])),
      IntToStr(Round(TotalAbsenceReason[3])),
      IntToStr(Round(TotalAbsenceReason[4])),
      IntToStr(Round(TotalAbsenceReason[5])),
      IntToStr(Round(TotalAbsenceReason[6])),
      IntToStr(Round(TotalAbsenceReason[7])),
      IntToStr(Summarize(TotalAbsenceReason))
      );
end;

procedure TReportHrsPerDayQR.QRLabelTotalAbsenceReasonPrint(
  sender: TObject; var Value: String);
begin
  inherited;
  Value := DecodeHrsMin(Summarize(TotalAbsenceReason));
end;

end.
