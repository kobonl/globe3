inherited DialogReportProductionDetailF: TDialogReportProductionDetailF
  Left = 255
  Top = 130
  Caption = 'Report Production Detail'
  ClientHeight = 461
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    TabOrder = 0
  end
  inherited pnlInsertBase: TPanel
    Height = 340
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 16
      Top = 376
      Visible = False
    end
    inherited LblEmployee: TLabel
      Left = 48
      Top = 376
      Visible = False
    end
    inherited LblToPlant: TLabel
      Visible = False
    end
    inherited LblToEmployee: TLabel
      Left = 323
      Top = 377
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 144
      Top = 380
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 352
      Top = 380
    end
    object Label5: TLabel [8]
      Left = 128
      Top = 71
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [9]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label7: TLabel [10]
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [11]
      Left = 40
      Top = 43
      Width = 65
      Height = 13
      Caption = 'Business unit '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel [12]
      Left = 315
      Top = 44
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [13]
      Left = 8
      Top = 70
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [14]
      Left = 40
      Top = 70
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [15]
      Left = 16
      Top = 421
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label15: TLabel [16]
      Left = 8
      Top = 450
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [17]
      Left = 315
      Top = 71
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [18]
      Left = 144
      Top = 377
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel [19]
      Left = 352
      Top = 377
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label21: TLabel [20]
      Left = 336
      Top = 70
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label22: TLabel [21]
      Left = 128
      Top = 451
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label23: TLabel [22]
      Left = 336
      Top = 448
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label24: TLabel [23]
      Left = 136
      Top = 422
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label25: TLabel [24]
      Left = 344
      Top = 422
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label16: TLabel [25]
      Left = 40
      Top = 450
      Width = 46
      Height = 13
      Caption = 'Workspot'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [26]
      Left = 48
      Top = 421
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label18: TLabel [27]
      Left = 315
      Top = 451
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [28]
      Left = 323
      Top = 422
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [29]
      Left = 8
      Top = 182
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [30]
      Left = 40
      Top = 182
      Width = 26
      Height = 13
      Caption = 'Date '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label26: TLabel [31]
      Left = 315
      Top = 182
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [32]
      Left = 128
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [33]
      Left = 336
      Top = 44
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited LblFromDepartment: TLabel
      Top = 69
    end
    inherited LblDepartment: TLabel
      Top = 69
    end
    inherited LblStarDepartmentFrom: TLabel
      Top = 68
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
      Top = 68
    end
    inherited LblToDepartment: TLabel
      Top = 70
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 126
    end
    inherited LblToTeam: TLabel
      Top = 127
    end
    inherited LblStarTeamFrom: TLabel
      Top = 130
    end
    inherited LblTeam: TLabel
      Top = 125
    end
    inherited LblFromTeam: TLabel
      Top = 125
    end
    inherited LblFromShift: TLabel
      Top = 561
    end
    inherited LblShift: TLabel
      Top = 561
    end
    inherited LblStartShiftFrom: TLabel
      Left = 136
      Top = 379
    end
    inherited LblToShift: TLabel
      Top = 563
    end
    inherited LblStarShiftTo: TLabel
      Top = 379
    end
    inherited LblFromPlant2: TLabel
      Top = 495
    end
    inherited LblPlant2: TLabel
      Top = 495
    end
    inherited LblStarPlant2From: TLabel
      Left = 136
      Top = 505
    end
    inherited LblToPlant2: TLabel
      Top = 495
    end
    inherited LblStarPlant2To: TLabel
      Left = 352
      Top = 505
    end
    inherited LblFromWorkspot: TLabel
      Top = 98
    end
    inherited LblWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotFrom: TLabel
      Top = 100
    end
    inherited LblToWorkspot: TLabel
      Top = 98
    end
    inherited LblStarWorkspotTo: TLabel
      Left = 336
      Top = 100
    end
    inherited LblWorkspots: TLabel
      Top = 98
    end
    inherited BtnWorkspots: TSpeedButton
      Left = 520
      Top = 95
    end
    inherited LblStarJobTo: TLabel
      Top = 531
    end
    inherited LblToJob: TLabel
      Top = 529
    end
    inherited LblStarJobFrom: TLabel
      Top = 531
    end
    inherited LblJob: TLabel
      Top = 529
    end
    inherited LblFromJob: TLabel
      Top = 529
    end
    inherited LblFromDateTime: TLabel
      Top = 182
    end
    inherited LblDateTime: TLabel
      Top = 182
    end
    inherited LblToDateTime: TLabel
      Top = 182
    end
    object GroupBoxShow: TGroupBox [71]
      Left = 8
      Top = 207
      Width = 137
      Height = 113
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 25
      Visible = False
      object CheckBoxBU: TCheckBox
        Left = 8
        Top = 12
        Width = 89
        Height = 17
        Caption = 'Business unit'
        TabOrder = 0
        OnClick = CheckBoxBUClick
      end
      object CheckBoxEmpl: TCheckBox
        Left = 8
        Top = 94
        Width = 73
        Height = 17
        Caption = 'Employee'
        TabOrder = 1
      end
      object CheckBoxWK: TCheckBox
        Left = 8
        Top = 53
        Width = 73
        Height = 17
        Caption = 'Workspot'
        TabOrder = 2
        Visible = False
        OnClick = CheckBoxWKClick
      end
      object CheckBoxJobcode: TCheckBox
        Left = 8
        Top = 67
        Width = 73
        Height = 17
        Caption = 'Jobcode'
        TabOrder = 3
        OnClick = CheckBoxJobcodeClick
      end
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 39
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 4
        OnClick = CheckBoxDeptClick
      end
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox [72]
      TabOrder = 27
    end
    object DateTo: TDateTimePicker [73]
      Left = 334
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 14
    end
    object DateFrom: TDateTimePicker [74]
      Left = 120
      Top = 180
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 37242.5624104051
      Time = 37242.5624104051
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
    end
    object ComboBoxPlusWorkspotFrom: TComboBoxPlus [75]
      Left = 136
      Top = 449
      Width = 180
      Height = 19
      ColCount = 163
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 19
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkspotFromCloseUp
    end
    object ProgressBar: TProgressBar [76]
      Left = 8
      Top = 325
      Width = 548
      Height = 8
      Min = 0
      Max = 100
      TabOrder = 26
    end
    object GroupBoxSelection: TGroupBox [77]
      Left = 8
      Top = 208
      Width = 548
      Height = 113
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 32
      object CheckBoxShowSelection: TCheckBox
        Left = 192
        Top = 16
        Width = 153
        Height = 17
        Caption = 'Show selections'
        TabOrder = 4
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 192
        Top = 62
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 6
      end
      object CheckBoxPageDept: TCheckBox
        Left = 368
        Top = 16
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 12
        Visible = False
      end
      object CheckBoxPageWK: TCheckBox
        Left = 368
        Top = 52
        Width = 169
        Height = 17
        Caption = 'New page per workspot'
        TabOrder = 9
        Visible = False
      end
      object CheckBoxPageJob: TCheckBox
        Left = 368
        Top = 87
        Width = 169
        Height = 17
        Caption = 'New page per jobcode'
        TabOrder = 10
        Visible = False
      end
      object CheckBoxPageBU: TCheckBox
        Left = 192
        Top = 87
        Width = 161
        Height = 17
        Caption = 'New page per business unit'
        TabOrder = 11
        Visible = False
      end
      object CheckBoxShowBonus: TCheckBox
        Left = 8
        Top = 16
        Width = 169
        Height = 17
        Caption = 'Show bonus'
        TabOrder = 0
      end
      object CheckBoxShowSales: TCheckBox
        Left = 8
        Top = 40
        Width = 169
        Height = 17
        Caption = 'Show sales'
        TabOrder = 1
      end
      object CheckBoxOnlyJob: TCheckBox
        Left = 8
        Top = 87
        Width = 169
        Height = 17
        Caption = 'Show only jobs'
        TabOrder = 3
      end
      object CheckBoxShowPayroll: TCheckBox
        Left = 8
        Top = 63
        Width = 169
        Height = 17
        Caption = 'Show  payroll information'
        TabOrder = 2
      end
      object CheckBoxExport: TCheckBox
        Left = 192
        Top = 38
        Width = 137
        Height = 17
        Caption = 'Export'
        TabOrder = 5
      end
      object CheckBoxShowShifts: TCheckBox
        Left = 368
        Top = 16
        Width = 161
        Height = 17
        Caption = 'Show Shifts'
        TabOrder = 8
      end
      object CheckBoxShowCompareJobs: TCheckBox
        Left = 192
        Top = 88
        Width = 169
        Height = 17
        Caption = 'Show compare jobs'
        TabOrder = 7
      end
      object RadioGroupIncludeDowntime: TRadioGroup
        Left = 368
        Top = 35
        Width = 175
        Height = 73
        Caption = 'Include down-time quantities'
        ItemIndex = 0
        Items.Strings = (
          'No'
          'Yes'
          'Only show down-time')
        TabOrder = 13
      end
    end
    object ComboBoxPlusWorkSpotTo: TComboBoxPlus [78]
      Left = 350
      Top = 449
      Width = 180
      Height = 19
      ColCount = 164
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 20
      TitleColor = clBtnFace
      Visible = False
      OnCloseUp = ComboBoxPlusWorkSpotToCloseUp
    end
    object ComboBoxPlusBusinessTo: TComboBoxPlus [79]
      Left = 334
      Top = 42
      Width = 180
      Height = 19
      ColCount = 159
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessToCloseUp
    end
    object ComboBoxPlusBusinessFrom: TComboBoxPlus [80]
      Left = 120
      Top = 42
      Width = 180
      Height = 19
      ColCount = 158
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csDropDownList
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusBusinessFromCloseUp
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus [81]
      ColCount = 157
    end
    inherited CmbPlusPlantTo: TComboBoxPlus [82]
      Left = 334
      ColCount = 158
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus [83]
      Top = 124
      ColCount = 152
      TabOrder = 10
    end
    inherited CmbPlusTeamTo: TComboBoxPlus [84]
      Left = 334
      Top = 124
      ColCount = 153
      TabOrder = 11
    end
    inherited CheckBoxAllTeams: TCheckBox [85]
      Left = 522
      Top = 127
      TabOrder = 12
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus [86]
      Top = 68
      ColCount = 152
      TabOrder = 4
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus [87]
      Left = 334
      Top = 68
      ColCount = 153
      TabOrder = 5
    end
    inherited CheckBoxAllDepartments: TCheckBox [88]
      Left = 522
      Top = 70
      TabOrder = 6
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit [89]
      Left = 128
      Top = 376
      TabOrder = 33
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit [90]
      Top = 376
      TabOrder = 34
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus [91]
      Top = 560
      ColCount = 158
      TabOrder = 35
    end
    inherited CmbPlusShiftTo: TComboBoxPlus [92]
      Left = 334
      Top = 560
      ColCount = 159
      TabOrder = 30
    end
    inherited CheckBoxAllShifts: TCheckBox [93]
      Left = 522
      Top = 563
      TabOrder = 31
    end
    inherited CheckBoxAllEmployees: TCheckBox
      Left = 514
      Top = 538
      TabOrder = 28
    end
    inherited CheckBoxAllPlants: TCheckBox
      Left = 514
      Top = 411
      TabOrder = 29
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      Top = 493
      ColCount = 165
      TabOrder = 21
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      Top = 493
      ColCount = 166
      TabOrder = 22
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      Top = 96
      ColCount = 166
      TabOrder = 7
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      Left = 334
      Top = 96
      ColCount = 167
      TabOrder = 8
    end
    inherited EditWorkspots: TEdit
      Top = 96
      Width = 393
      TabOrder = 9
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      Top = 527
      ColCount = 166
      TabOrder = 24
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      Top = 527
      ColCount = 165
      TabOrder = 23
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      Top = 180
      TabOrder = 16
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      Left = 450
      Top = 180
      TabOrder = 18
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      Top = 180
      TabOrder = 15
    end
    inherited DatePickerTo: TDateTimePicker
      Left = 334
      Top = 180
      TabOrder = 17
    end
  end
  inherited stbarBase: TStatusBar
    Top = 401
  end
  inherited pnlBottom: TPanel
    Top = 420
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 208
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 240
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 552
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        43030000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C20104F7074
        696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564
        676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564
        676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F
        7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43
        616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E61
        7669676174696F6E116564676F436F6E6669726D44656C657465126564676F4C
        6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B730000
        0F546478444247726964436F6C756D6E0C436F6C756D6E4E756D626572074361
        7074696F6E06064E756D62657206536F72746564070463735570055769647468
        02410942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060F454D504C4F5945455F4E554D42455200000F546478444247726964
        436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A
        53686F7274204E616D6505576964746802480942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060A53484F52545F4E414D4500
        000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D6507436170
        74696F6E06044E616D6505576964746803F4000942616E64496E646578020008
        526F77496E6465780200094669656C644E616D65060B4445534352495054494F
        4E00000F546478444247726964436F6C756D6E0D436F6C756D6E416464726573
        730743617074696F6E060741646472657373055769647468022C0942616E6449
        6E646578020008526F77496E6465780200094669656C644E616D650607414444
        52455353000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        FA020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E7402010D53756D6D61727947726F7570730E001053756D6D6172
        79536570617261746F7206022C20104F7074696F6E73437573746F6D697A650B
        0E6564676F42616E644D6F76696E670E6564676F42616E6453697A696E671065
        64676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E53697A696E67
        0E6564676F46756C6C53697A696E6700094F7074696F6E7344420B106564676F
        43616E63656C4F6E457869740D6564676F43616E44656C6574650D6564676F43
        616E496E73657274116564676F43616E4E617669676174696F6E116564676F43
        6F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F72647310
        6564676F557365426F6F6B6D61726B7300000F546478444247726964436F6C75
        6D6E0A436F6C756D6E456D706C0743617074696F6E06064E756D62657206536F
        7274656407046373557005576964746802310942616E64496E64657802000852
        6F77496E6465780200094669656C644E616D65060F454D504C4F5945455F4E55
        4D42455200000F546478444247726964436F6C756D6E0F436F6C756D6E53686F
        72744E616D650743617074696F6E060A53686F7274206E616D65055769647468
        024E0942616E64496E646578020008526F77496E6465780200094669656C644E
        616D65060A53484F52545F4E414D4500000F546478444247726964436F6C756D
        6E11436F6C756D6E4465736372697074696F6E0743617074696F6E06044E616D
        6505576964746803CC000942616E64496E646578020008526F77496E64657802
        00094669656C644E616D65060B4445534352495054494F4E00000F5464784442
        47726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E
        06074164647265737305576964746802650942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060741444452455353000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 428
  end
  inherited QueryEmplTo: TQuery
    Left = 392
  end
  object DataSourceWorkSpot: TDataSource
    DataSet = qryWorkspot
    Left = 480
    Top = 23
  end
  object QueryBU: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  BU.PLANT_CODE,'
      '  BU.BUSINESSUNIT_CODE,'
      '  BU.DESCRIPTION'
      'FROM'
      '  BUSINESSUNIT BU'
      'WHERE'
      '  BU.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  BU.BUSINESSUNIT_CODE')
    Left = 80
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
