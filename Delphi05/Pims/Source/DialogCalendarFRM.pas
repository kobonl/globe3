(*
  MRA:1-MAY-2013
  - Show optionally weeknumbers in calendar.
  - Component TMonthCalendar is overridden in SystemDMT,
    unit SystemDMT must be added as last unit in first uses-line.
*)
unit DialogCalendarFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, SystemDMT;

type
  TDialogCalendarF = class(TForm)
    MonthCalendarGrid: TMonthCalendar;
    procedure MonthCalendarGridClick(Sender: TObject);
    procedure MonthCalendarGridGetMonthInfo(Sender: TObject;
      Month: Cardinal; var MonthBoldInfo: Cardinal);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
     FChangeDate: Boolean;
  public
    { Public declarations }
  end;

var
  DialogCalendarF: TDialogCalendarF;

implementation

uses WorkspotPerEmployeeFRM;

{$R *.DFM}

procedure TDialogCalendarF.MonthCalendarGridClick(Sender: TObject);
begin
  inherited;
  if FChangeDate then
  begin
    WorkspotPerEmployeeF.EditWorkspotPerEmployee(False, True,
      WorkspotPerEmployeeF.FFocusedColumn, '', MonthCalendarGrid.Date);
    DialogCalendarF.Hide;
  end;
  FChangeDate := True;
end;

procedure TDialogCalendarF.MonthCalendarGridGetMonthInfo(Sender: TObject;
  Month: Cardinal; var MonthBoldInfo: Cardinal);
begin
  inherited;
  FChangeDate := False;
end;

procedure TDialogCalendarF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  WorkspotPerEmployeeF.EditWorkspotPerEmployee(False, True,
    WorkspotPerEmployeeF.FFocusedColumn, '', MonthCalendarGrid.Date);
end;

end.
