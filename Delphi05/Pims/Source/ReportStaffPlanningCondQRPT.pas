(*
  MRA:10-MAY-2011. RV092.14. Bugfix. 550518
  - The team-selection must refer to the workspot-department-teams, not to the
    team of the employee! Otherwise you can not show employees
    who worked in other plants.
  - Addition of teams-per-user.
  MRA:6-FEB-2013. 20013910. Rework.
  - Plan on departments gives problems:
    - Only first found department was used, all others were not shown.
    - CAUSE: Grouping went wrong because workspot is '0' for
             planning-on-departments.
  MRA:6-JUL-2018 GLOB3-60
  - Extension 4 to 10 blocks for planning
  - IMPORTANT: This report only appears to work when on timeblock-group-level
    (QRGroupHDTimeBlock) a number is returned that indicates the
    timeblock-level.
    Because of that it now uses an extra (fixed) table that has all the
    combinations for 10 timeblocks, this returns 1 number for the
    timeblock-group. Only then it works correct.
    Previously it used 1 query with all (15) combinations for 4 timeblocks,
    but with 10 timeblocks there are about 1024 combinations, which is too much
    for 1 query.
*)
unit ReportStaffPlanningCondQRPT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ReportBaseFRM, Qrctrls, QuickRpt, ExtCtrls, StdCtrls, QRExport,
  QRXMLSFilt, QRWebFilt, QRPDFFilt, UPimsConst;

const
  MyDebug: Boolean = False;

type
  TTBWeekArray = Array[1..7, 1..MAX_TBS] of Integer;
  TTBSCHEDULE = Array[1..MAX_TBS] of String;

  TQRParameters = class
  private
    FShiftFrom, FShiftTo: String;
    FDeptFrom, FDeptTo, FTeamFrom, FTeamTo, FWKFrom, FWKTo: String;
    FDateFrom, FDateTo: TDateTime;
    FAllTeam, FShowSelection, FPagePlant,  FPageShift, FPageWK: Boolean;
    FExportToFile: Boolean;
    FShowEmployeeMode: Integer;
  public
    procedure SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
      DateFrom, DateTo: TDateTime;
      AllTeam, ShowSelection, PagePlant,  PageShift, PageWK,
      ExportToFile: Boolean;
      ShowEmployeeMode: Integer);
  end;

  TReportStaffPlanningCondQR = class(TReportBaseF)
    QRGroupHDPlant: TQRGroup;
    QRGroupHDShift: TQRGroup;
    QRGroupHDWK: TQRGroup;
    QRGroupHDTimeBlock: TQRGroup;
    QRGroupHDEmp: TQRGroup;
    QRGroupHDDate: TQRGroup;
    QRBandFTEmp: TQRBand;
    QRBandColumnHeader: TQRBand;
    QRBandTitle: TQRBand;
    QRGroupFTTimeBlock: TQRBand;
    QRBandSummary: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabelWKFrom: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabelWKTo: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabelPlantFrom: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabelPlantTo: TQRLabel;
    QRLabelDepartment: TQRLabel;
    QRLabelDeptFrom: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabelDeptTo: TQRLabel;
    QRLabel87: TQRLabel;
    QRLabel88: TQRLabel;
    QRLabelTeamFrom: TQRLabel;
    QRLabel90: TQRLabel;
    QRLabelTeamTo: TQRLabel;
    QRLabel108: TQRLabel;
    QRLabel109: TQRLabel;
    QRLabelShiftFrom: TQRLabel;
    QRLabelShiftTo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel110: TQRLabel;
    QRLabel111: TQRLabel;
    QRLabelDateFrom: TQRLabel;
    QRLabel113: TQRLabel;
    QRLabelDateTo: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabelPlantDesc: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabelShiftDesc: TQRLabel;
    QRLabelWKCode: TQRLabel;
    QRLabel241: TQRLabel;
    QRLabel243: TQRLabel;
    QRLabel244: TQRLabel;
    QRLabel245: TQRLabel;
    QRLabel246: TQRLabel;
    QRLabel247: TQRLabel;
    QRLabel248: TQRLabel;
    QRLabel249: TQRLabel;
    QRLabel250: TQRLabel;
    QRLabel251: TQRLabel;
    QRLabel252: TQRLabel;
    QRLabel253: TQRLabel;
    QRLabel254: TQRLabel;
    QRLabel255: TQRLabel;
    QRLabel256: TQRLabel;
    QRLabel257: TQRLabel;
    QRDBText2: TQRDBText;
    QRLoopBand1: TQRLoopBand;
    QRLblDay1: TQRLabel;
    QRLblDay2: TQRLabel;
    QRLblDay3: TQRLabel;
    QRLblDay4: TQRLabel;
    QRLblDay5: TQRLabel;
    QRLblDay6: TQRLabel;
    QRLblDay7: TQRLabel;
    QRLblTB: TQRLabel;
    QRGroupFTWK: TQRBand;
    QRBandDetail: TQRBand;
    QRChildBandEmployeeNumber: TQRChildBand;
    QRLblDay1b: TQRLabel;
    QRLblDay2b: TQRLabel;
    QRLblDay3b: TQRLabel;
    QRLblDay4b: TQRLabel;
    QRLblDay5b: TQRLabel;
    QRLblDay6b: TQRLabel;
    QRLblDay7b: TQRLabel;
    QRLblPlantCode: TQRLabel;
    QRLblShiftNumber: TQRLabel;
    procedure FormCreate(Sender: TObject);
    procedure QRBandTitleBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRDBTextDeptDescPrint(sender: TObject; var Value: String);
    procedure QRDBTextShiftDescPrint(sender: TObject; var Value: String);
    procedure QRLabelPlantDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabelEmpDescPrint(sender: TObject; var Value: String);
    procedure QRGroupHDEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDDateBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandFTEmpBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRLabel160Print(sender: TObject; var Value: String);
    procedure QRGroupHDShiftBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandColumnHeaderAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDPlantAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDShiftAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRGroupHDWKAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandSummaryBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandSummaryAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRBandColumnHeaderBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupHDTimeBlockBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupFTTimeBlockAfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
    procedure QRLoopBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupFTTimeBlockBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBandDetailBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroupFTWKBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRChildBandEmployeeNumberBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
   private
    duration: TDateTime;
    FQRParameters: TQRParameters;
    FCurrentIndex: Integer;
    FMaxCount: Integer;
    FClientDataSetNumber: Integer;
    FList: array[1..7] of TStringList;
    FLoopBandPrintBand: Boolean;
    procedure FillMemo;
    function TBDescription(TB: String): String;
  protected
    function ExistsRecords: Boolean; override;
    procedure ConfigReport; override;
  public
    FStartDay, FEndDay: Integer;
    FMinDate, FMaxDate: TDateTime;
    FStartDate, FEndDate: TDateTime;
    FEmpPlanning: TTBWeekArray;
    FDayDesc, FDate: Array[1..7] of String;
    FWKPrint: Boolean;
    procedure InitializePlanningPerDaysOfWeek(var PlanningPerDaysOfWeek:
      TTBWeekArray);
    procedure FillDescDaysPerWeek;
    function QRSendReportParameters(
      const PlantFrom, PlantTo, DeptFrom, DeptTo,
      TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom, ShiftTo: String;
      const DateFrom, DateTo: TDateTime;
      const AllTeam, ShowSelection, PagePlant,  PageShift, PageWK,
        ExportToFile: Boolean;
      const ShowEmployeeMode: Integer): Boolean;
    property QRParameters: TQRParameters read FQRParameters;
    procedure FreeMemory; override;
    procedure FillAmountsPerDaysOfWeek(IndexMin, IndexMax: Integer;
      AmountsPerDaysOfWeek: TTBWeekArray);
    property MaxCount: Integer read FMaxCount write FMaxCount;
    property CurrentIndex: Integer read FCurrentIndex write FCurrentIndex;
    property ClientDataSetNumber: Integer read FClientDataSetNumber
      write FClientDataSetNumber;
  end;

var
  ReportStaffPlanningCondQR: TReportStaffPlanningCondQR;

implementation

{$R *.DFM}

uses
  SystemDMT, ReportStaffPlanningCondDMT, ListProcsFRM,
  CalculateTotalHoursDMT, UPimsMessageRes, UGlobalFunctions;

procedure TQRParameters.SetValues(ShiftFrom, ShiftTo: String;
  DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo: String;
  DateFrom, DateTo: TDateTime;
  AllTeam, ShowSelection, PagePlant,  PageShift, PageWK,
  ExportToFile: Boolean;
  ShowEmployeeMode: Integer);
begin
  FDeptFrom := DeptFrom;
  FDeptTo := DeptTo;
  FTeamFrom := TeamFrom;
  FTeamTo := TeamTo;
  FShiftFrom := ShiftFrom;
  FShiftTo := ShiftTo;
  FWKFrom := WKFrom;
  FWKTo := WKTo;
  FDateFrom := DateFrom;
  FDateTo := DateTo;
  FShowSelection := ShowSelection;
  FPagePlant := PagePlant;
  FPageWK := PageWK;
  FPageShift := PageShift;
  FAllTeam:= AllTeam;
  FExportToFile := ExportToFile;
  FShowEmployeeMode := ShowEmployeeMode;
end;

function TReportStaffPlanningCondQR.TBDescription(TB: String): String;
begin
  Result := TB;
(*
  case TB of
  15: Result := '1,2,3,4';
  14: Result := '1,2,3';
  13: Result := '1,2,4';
  12: Result := '1,3,4';
  11: Result := '2,3,4';
  10: Result := '1,2';
   9: Result := '1,3';
   8: Result := '1,4';
   7: Result := '2,3';
   6: Result := '2,4';
   5: Result := '3,4';
   4: Result := '1';
   3: Result := '2';
   2: Result := '3';
   1: Result := '4';
  end;
*)
end;

function TReportStaffPlanningCondQR.QRSendReportParameters(
  const PlantFrom, PlantTo,
    DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo, ShiftFrom,
    ShiftTo: String;
  const DateFrom, DateTo: TDateTime;
  const AllTeam, ShowSelection, PagePlant, PageShift,  PageWK,
    ExportToFile: Boolean;
  const ShowEmployeeMode: Integer): Boolean;
begin
  FQRBaseParameters.SetValues(PlantFrom, PlantTo, '0', '0');
 {check if parameters are valid}
  if not FQRBaseParameters.CheckSelection then
    Result := False
  else
  begin
    Result := True;
    if FQRParameters = Nil then
      FQRParameters := TQRParameters.Create;
    FQRParameters.SetValues(ShiftFrom, ShiftTo,
      DeptFrom, DeptTo, TeamFrom, TeamTo, WKFrom, WKTo,
      DateFrom, DateTo, AllTeam, ShowSelection, PagePlant,
      PageShift, PageWK, ExportToFile, ShowEmployeeMode);
  end;
  SetDataSetQueryReport(ReportStaffPlanningCondDM.QueryEMP);
end;

function TReportStaffPlanningCondQR.ExistsRecords: Boolean;
var
  SelectStr, Part1, Part2, Part3: String;
  i, K: Integer;
  Year, Month, Day: Word;
  FlagStr: String;
begin
  duration := now;
  Screen.Cursor := crHourGlass;
  FMinDate := QRParameters.FDateFrom;
  FMaxDate := QRParameters.FDateTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRParameters.FDeptFrom := '1';
    QRParameters.FDeptTo := 'zzzzzz';
    QRParameters.FWKFrom := '1';
    QRParameters.FWKTo := 'zzzzzz';
    QRParameters.FShiftFrom := '1';
    QRParameters.FShiftTo := '999999';
  end;
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);

  FlagStr :=
    ', CASE ' + NL +
    '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN ''W'' ' + NL +
    '    ELSE ''D'' ' + NL +
    '  END FLAG ';

  Part1 :=
    'SELECT ' + NL +
    '  DISTINCT EMP.PLANT_CODE, EMP.SHIFT_NUMBER, ' + NL +
    '  CASE ' + NL +
    '    WHEN EMP.WORKSPOT_CODE <> ''0'' THEN EMP.WORKSPOT_CODE ' + NL +
    '    ELSE EMP.DEPARTMENT_CODE ' + NL +
    '  END WORKSPOT_CODE, ' + NL +
    '  EMP.DEPARTMENT_CODE, EMP.EMPLOYEE_NUMBER, ' + NL +
    '  E.DESCRIPTION, EMP.EMPLOYEEPLANNING_DATE, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_5, EMP.SCHEDULED_TIMEBLOCK_6, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_7, EMP.SCHEDULED_TIMEBLOCK_8, ' + NL +
    '  EMP.SCHEDULED_TIMEBLOCK_9, EMP.SCHEDULED_TIMEBLOCK_10, ' + NL;
  Part2 :=
    'FROM ' + NL +
    '  EMPLOYEEPLANNING EMP INNER JOIN EMPLOYEE E ON ' + NL +
    '    EMP.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER ' +  NL;
  // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
  if not QRParameters.FAllTeam then
    Part2 := Part2 +
      '  INNER JOIN DEPARTMENTPERTEAM DT ON ' + NL +
      '    EMP.PLANT_CODE = DT.PLANT_CODE AND ' + NL +
      '    EMP.DEPARTMENT_CODE = DT.DEPARTMENT_CODE ' + NL;
  Part2 := Part2 +
    'WHERE ' + NL;
  Part3 :=
    '  AND EMP.EMPLOYEEPLANNING_DATE >= :FDATESTART '+ NL +
    '  AND EMP.EMPLOYEEPLANNING_DATE <= :FDATEEND  '+ NL +
    '  AND EMP.PLANT_CODE >= ''' + DoubleQuote(QRBaseParameters.FPlantFrom) + '''' + NL +
    '  AND EMP.PLANT_CODE <= ''' + DoubleQuote(QRBaseParameters.FPlantTo) + '''' + NL +
    '  AND ((EMP.WORKSPOT_CODE >= ''' + DoubleQuote(QRParameters.FWkFrom) +  '''' + NL +
    '  AND EMP.WORKSPOT_CODE <= ''' + DoubleQuote(QRParameters.FWkTo) +  ''')' + NL +
    '  OR (EMP.WORKSPOT_CODE = ''' + DummyStr + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE >= ''' + DoubleQuote(QRParameters.FDeptFrom)  + '''' + NL +
    '  AND EMP.DEPARTMENT_CODE <= ''' + DoubleQuote(QRParameters.FDeptTo) + '''))' + NL +
    '  AND EMP.SHIFT_NUMBER >= ' +  QRParameters.FShiftFrom + NL +
    '  AND EMP.SHIFT_NUMBER <= ' +  QRParameters.FShiftTo + NL;
    // RV092.14. Teams -> Test on teams in DEPARTMENTPERTEAM, not in EMPLOYEE.
    //           Plus: Addition of 'teams-per-user'-selection.
    if not QRParameters.FAllTeam then
      Part3 := Part3 +
        '  AND DT.TEAM_CODE >= ''' + DoubleQuote(QRParameters.FTeamFrom) + '''' + NL +
        '  AND DT.TEAM_CODE <= ''' + DoubleQuote(QRParameters.FTeamTo) + '''' + NL +
        '  AND ( ' + NL +
        '        (:USER_NAME = ''*'') OR ' + NL +
        '        (DT.TEAM_CODE IN ' + NL +
        '          (SELECT TU.TEAM_CODE FROM TEAMPERUSER TU ' + NL +
        '           WHERE TU.USER_NAME = :USER_NAME)) ' + NL +
        '      ) ' + NL;

  // Employees scheduled on all timeblocks
  SelectStr :=
    Part1 +
      '  TO_CHAR(15) AS TB, ' + NL +
      '  TO_CHAR(PlanTimeblockAsString( ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_5, EMP.SCHEDULED_TIMEBLOCK_6, ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_7, EMP.SCHEDULED_TIMEBLOCK_8, ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_9, EMP.SCHEDULED_TIMEBLOCK_10)) AS TBDESC ' + NL +
      FlagStr + NL;
    SelectStr := SelectStr +
      Part2 +
      '  ( ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_5 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_6 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_7 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_8 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_9 IN (''A'',''B'',''C'') OR ' + NL +
      '    EMP.SCHEDULED_TIMEBLOCK_10 IN (''A'',''B'',''C'') ' + NL +
      '  ) ' + NL;
  SelectStr := SelectStr +
    Part3;

  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  TO_CHAR(0), TO_CHAR(0) ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_5 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_6 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_7 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_8 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_9 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_10 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;

(*
  // Employees scheduled on 4 timeblocks
  SelectStr :=
    Part1 +
    '  15 AS TB ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'') ' + NL +
    '  ) ' + NL +
    Part3;
  // Employees scheduled on 3 timeblocks
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  14 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  13 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  12 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  11 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  // Employees scheduled on 2 timeblocks
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  10 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  9 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  8 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  7 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  6 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  5 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  // Employees scheduled on 1 timeblock
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  4 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  3 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  2 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 NOT IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  1 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 NOT IN (''A'',''B'',''C'') AND ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
  // Because the 'LoopBand' doesn't print the last records, we get
  // some additional data that will be used as 'eof'-indication.
  SelectStr := SelectStr + ' ' +
    'UNION '+ NL +
    Part1 +
    '  0 ' + FlagStr + NL +
    Part2 +
    '  ( ' + NL +
    '    (EMP.SCHEDULED_TIMEBLOCK_1 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_2 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_3 IN (''A'',''B'',''C'') OR ' + NL +
    '    EMP.SCHEDULED_TIMEBLOCK_4 IN (''A'',''B'',''C'')) ' + NL +
    '  ) ' + NL +
    Part3;
    SelectStr := SelectStr +  ' ORDER BY 1, 2, 3, 4, 12 DESC, 7, 5';
*)

// GLOB3-60 Put a wrapper around it, so we can use the names of the columns
//          for order by
// GLOB3-60 It does not recognize the expression used in
//          QRGroupHDTimeblock (WORKSPOT_CODE + TB), giving wrong results.
//          To solve it, combine the 2 here via an extra field.
    SelectStr :=
      'SELECT ' + NL +
      '  A.PLANT_CODE, ' + NL +
      '  A.SHIFT_NUMBER, ' + NL +
      '  A.WORKSPOT_CODE, ' + NL +
      '  A.DEPARTMENT_CODE, ' + NL +
      '  A.EMPLOYEE_NUMBER, ' + NL +
      '  A.DESCRIPTION, ' + NL +
      '  A.EMPLOYEEPLANNING_DATE, ' + NL +
      '  A.SCHEDULED_TIMEBLOCK_1, A.SCHEDULED_TIMEBLOCK_2, ' + NL +
      '  A.SCHEDULED_TIMEBLOCK_3, A.SCHEDULED_TIMEBLOCK_4, ' + NL +
      '  A.SCHEDULED_TIMEBLOCK_5, A.SCHEDULED_TIMEBLOCK_6, ' + NL +
      '  A.SCHEDULED_TIMEBLOCK_7, A.SCHEDULED_TIMEBLOCK_8, ' + NL +
      '  A.SCHEDULED_TIMEBLOCK_9, A.SCHEDULED_TIMEBLOCK_10, ' + NL +
      '  NVL((SELECT SL.ID FROM STAFFPLANLEVEL SL WHERE SL.DESCRIPTION = A.TBDESC), ''0'') AS TB, ' + NL +
      '  A.TBDESC, ' + NL +
      '  A.FLAG ' + NL +
      'FROM ' + NL +
      '( ' + NL +
      SelectStr + NL +
      ') A ' + NL +
      'ORDER BY '  + NL +
      '  PLANT_CODE, SHIFT_NUMBER, WORKSPOT_CODE, DEPARTMENT_CODE, ' + NL +
      '  TB DESC, EMPLOYEEPLANNING_DATE, EMPLOYEE_NUMBER';

(*
    1 = PLANT_CODE
    2 = SHIFT_NUMBER
    3 = WORKSPOT_CODE
    4 = DEPARTMENT_CODE
    12 = TIMEBLOCK-NUMBER
    7 = EMPLOYEEPLANNING_DATE
    5 = EMPLOYEE_NUMBER
*)
//    '  DISTINCT EMP.PLANT_CODE, EMP.SHIFT_NUMBER, ' +
//    '  EMP.WORKSPOT_CODE, EMP.DEPARTMENT_CODE, EMP.EMPLOYEE_NUMBER, ' +
//    '  E.DESCRIPTION, EMP.EMPLOYEEPLANNING_DATE, ' +
//    '  EMP.SCHEDULED_TIMEBLOCK_1, EMP.SCHEDULED_TIMEBLOCK_2, ' +
//    '  EMP.SCHEDULED_TIMEBLOCK_3, EMP.SCHEDULED_TIMEBLOCK_4, ';

  with ReportStaffPlanningCondDM do
  begin
    QueryEMP.Active := False;
    QueryEMP.UniDirectional := False;
    QueryEMP.SQL.Clear;
    QueryEMP.SQL.Add(UpperCase(SelectStr));
// QueryEMP.SQL.SaveToFile('c:\temp\reportstaffplanningcond-10-timeblocks.sql');
    QueryEMP.ParamByName('FDATESTART').Value := GetDate(QRParameters.FDateFrom);
    QueryEMP.ParamByName('FDATEEND').Value :=  GetDate(QRParameters.FDateTo);
    // RV092.14.
    if not QRParameters.FAllTeam then
      QueryEMP.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    if not QueryEMP.Prepared then
      QueryEMP.Prepare;
    QueryEMP.Active := True;
// TEST
if MyDebug then
begin
WLog('START');
while not QueryEMP.Eof do
begin
  WLog(
    DateToStr(QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDatetime) + ';' +
    QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
    QueryEMP.FieldByName('DESCRIPTION').AsString + ';' +
    QueryEMP.FieldByName('WORKSPOT_CODE').AsString + ';' +
    QueryEMP.FieldByName('TB').AsString + ';' +
    QueryEMP.FieldByName('TBDESC').AsString);
  QueryEMP.Next;
end;
QueryEMP.First;
end;
// TEST
    Result := not QueryEMP.IsEmpty;
  end;
  FEndDate :=  GetDate(QRParameters.FDateTo);
  FStartDate := GetDate(QRParameters.FDateFrom);
  FStartDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateFrom);
  FEndDay := ListProcsF.DayWStartOnFromDate(QRParameters.FDateTo);
  for i:= 1 to 7 do
  begin
    if (FStartDate + i - 1) <= FEndDate then
    begin
      K := FStartDay + i - 1;
      if K >= 8 then
        K := K - 8 + 1;
      FDayDesc[i] := SystemDM.GetDayWCode(K);
      DecodeDate(QRParameters.FDateFrom + i - 1, Year, Month, Day);
      FDate[i] := IntToStr(Day);
    end
    else
    begin
      FDayDesc[i] := '';
      FDate[i] := '';
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TReportStaffPlanningCondQR.ConfigReport;
begin
  // MR:29-09-2003
  ReportStaffPlanningCondDM.cdsEmpPln.EmptyDataSet;
  QRLoopBand1.PrintCount := 0;

  ExportClass.ExportDone := False;

  QRLblBaseTitle.Caption := Caption;
  qrptBase.ReportTitle := QRLblBaseTitle.Caption;
  QRLblBaseLaundry.Caption := '';
  QRLabelPlantFrom.Caption := QRBaseParameters.FPlantFrom;
  QRLabelPlantTo.Caption := QRBaseParameters.FPlantTo;
  if QRBaseParameters.FPlantFrom <> QRBaseParameters.FPlantTo then
  begin
    QRLabelShiftFrom.Caption := '*';
    QRLabelShiftTo.Caption := '*';
    QRLabelDeptFrom.Caption := '*';
    QRLabelDeptTo.Caption := '*';
    QRLabelWKFrom.Caption := '*';
    QRLabelWKTo.Caption := '*';
  end
  else
  begin
    QRLabelShiftFrom.Caption := QRParameters.FShiftFrom;
    QRLabelShiftTo.Caption := QRParameters.FShiftTo;
    QRLabelDeptFrom.Caption := QRParameters.FDeptFrom;
    QRLabelDeptTo.Caption := QRParameters.FDeptTo;
    QRLabelWKFrom.Caption := QRParameters.FWKFrom;
    QRLabelWKTo.Caption := QRParameters.FWKTo;
  end;
  if QRParameters.FAllTeam then
  begin
    QRLabelTeamFrom.Caption := '*';
    QRLabelTeamTo.Caption := '*';
  end
  else
  begin
    QRLabelTeamFrom.Caption := QRParameters.FTeamFrom;
    QRLabelTeamTo.Caption := QRParameters.FTeamTo;
  end;
  QRLabelDateFrom.Caption := DateToStr(QRParameters.FDateFrom);
  QRLabelDateTo.Caption := DateToStr(QRParameters.FDateTo);

  case QRParameters.FShowEmployeeMode of
    0, 1: // Name OR Number
      QRChildBandEmployeeNumber.Enabled := False;
    2: // Both (Name AND Number)
      QRChildBandEmployeeNumber.Enabled := True;
  end;
end;

procedure TReportStaffPlanningCondQR.FreeMemory;
var
  i: Integer;
begin
  inherited;
  for i := 1 to 7 do
    FList[i].Free;
  ExportClass.ClearText;
  ExportClass.Free;
  FreeAndNil(FQRParameters);
end;

procedure TReportStaffPlanningCondQR.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  for i := 1 to 7 do
    FList[i] := TStringList.Create;
  FQRParameters := TQRParameters.Create;
  ExportClass := TExportClass.Create;
  ExportClass.Filename := Caption; // + '.txt'; // RV097.1.
end;

procedure TReportStaffPlanningCondQR.InitializePlanningPerDaysOfWeek
 (var PlanningPerDaysOfWeek: TTBWeekArray);
var
  Counter, i: Integer;
begin
  for Counter := 1 to 7 do
    for i := 1 to SystemDM.MaxTimeblocks do
      PlanningPerDaysOfWeek[Counter][i] := 0;
end;

procedure TReportStaffPlanningCondQR.FillAmountsPerDaysOfWeek(
  IndexMin, IndexMax: Integer;
  AmountsPerDaysOfWeek: TTBWeekArray);
var
  TempComponent: TComponent;
  Counter, Index, IndexDay, IndexTB: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;

      if (Index >= IndexMin) and (Index <= IndexMax) then
      begin
        Index := Index - (IndexMin div 100) * 100;
        IndexDay := Index div 10;
        IndexTB := Index mod 10;
        if (FDayDesc[IndexDay - 1] = '') then
          (TempComponent as TQRLabel).Caption := ''
        else
          if ATBLengthClass.ExistTB(IndexTB) then
            (TempComponent as TQRLabel).Caption := ''
//                MyDecodeHrsMin(AmountsPerDaysOfWeek[IndexDay - 1][IndexTB])
          else
            (TempComponent as TQRLabel).Caption := '  -';
      end;
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.FillDescDaysPerWeek;
var
  TempComponent: TComponent;
  Counter, Index: Integer;
begin
  for Counter := 0 to (ComponentCount - 1) do
  begin
    TempComponent := Components[Counter];
    if (TempComponent is TQRLabel) then
    begin
      Index := (TempComponent as TQRLabel).Tag;
      if (Index >= 100) and (Index <= 106) then
      begin
        (TempComponent as TQRLabel).Caption := FDate[Index - 100 + 1];
      end;
      if (Index >= 10) and (Index <= 16) then
      begin
        (TempComponent as TQRLabel).Caption := FDayDesc[Index - 10 + 1];
      end;
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRBandTitleBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  inherited;
  PrintBand := QRParameters.FShowSelection;
  if QRParameters.FExportToFile and (not ExportClass.ExportDone) then
  begin
    ExportClass.AskFilename;
    ExportClass.ClearText;
    if QRParameters.FShowSelection then
    begin
      // Title
      ExportClass.AddText(QRLabel11.Caption);
      ExportClass.AddText(QRLabel13.Caption + ' ' + QRLabel1.Caption + ' ' +
        QRLabelPlantFrom.Caption + ' ' + QRLabel49.Caption + ' ' +
        QRLabelPlantTo.Caption);
      ExportClass.AddText(QRLabel47.Caption + ' ' +
        QRLabelDepartment.Caption + ' ' + QRLabelDeptFrom.Caption + ' ' +
        QRLabel50.Caption + ' ' + QRLabelDeptTo.Caption);
      ExportClass.AddText(QRLabel87.Caption + ' ' +
        QRLabel88.Caption + ' ' + QRLabelTeamFrom.Caption + ' ' +
        QRlabel90.Caption + ' ' + QRLabelTeamTo.Caption);
      ExportClass.AddText(QRLabel18.Caption + ' ' +
        QRLabel20.Caption + ' ' + QRLabelWKFrom.Caption + ' ' +
        QRLabel22.Caption + ' ' + QRLabelWKTo.Caption);
      ExportClass.AddText(QRLabel108.Caption + ' ' +
        QRLabel109.Caption + ' ' + QRLabelShiftFrom.Caption + ' ' +
        QRLabel2.Caption + ' ' + QRLabelShiftTo.Caption);
      ExportClass.AddText(QRLabel110.Caption + ' ' +
        QRLabel111.Caption + ' ' + QRLabelDateFrom.Caption + ' ' +
        QRLabel113.Caption + ' ' + QRLabelDateTo.Caption);
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupHdPlantBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  QRGroupHdPlant.ForceNewPage := QRParameters.FPagePlant;
  // MR:16-02-2005 Order 550380
  with ReportStaffPlanningCondDM do
  begin
    QRLblPlantCode.Caption := QueryEMP.FieldByName('PLANT_CODE').AsString;
  end;
  inherited;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDPlantAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Plant
    with ReportStaffPlanningCondDM do
    begin
      ExportClass.AddText(QRLabel16.Caption + ExportClass.Sep +
        QRLabel17.Caption + ExportClass.Sep +
        QueryEMP.FieldByName('PLANT_CODE').AsString + ExportClass.Sep +
        QRLabelPlantDesc.Caption
        );
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDShiftBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  QRGroupHdShift.ForceNewPage := QRParameters.FPageShift;
  // MR:16-02-2005 Order 550380
  with ReportStaffPlanningCondDM do
  begin
    QRLblShiftNumber.Caption := QueryEMP.FieldByName('SHIFT_NUMBER').AsString;
    if QueryShift.Locate('PLANT_CODE;SHIFT_NUMBER',
       VarArrayOf([QueryEMP.FieldByName('PLANT_CODE').AsString,
         QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]), []) then
      QRLabelShiftDesc.Caption :=
        QueryShift.FieldByName('DESCRIPTION').AsString;
  end;
  inherited;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDWKBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
var
  WKCode, Plant, Dept, TeamFrom, TeamTo, DeptDesc: String;
  Flag: String;
//  Shift: Integer;
begin
  ReportStaffPlanningCondDM.cdsEmpPln.EmptyDataSet;
  ClientDataSetNumber := 0;
  QRGroupHDWK.ForceNewPage := QRParameters.FPageWK;
  inherited;
  TeamFrom := QRParameters.FTeamFrom;
  TeamTo := QRParameters.FTeamTo;
  Plant := ReportStaffPlanningCondDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanningCondDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
//  Shift := ReportStaffPlanningCondDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
  WKCode := ReportStaffPlanningCondDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString;
  FWKPrint := True;
  if not QRParameters.FAllTeam then
    if not ReportStaffPlanningCondDM.ISNotDeptValid(Dept,Plant, TeamFrom,
      TeamTo) then
    begin
      PrintBand := False;
      FWKPrint := False;
      Exit;
    end;
  PrintBand := FWKPrint;
  QRLabelWKCode.Caption := '';
  DeptDesc := '';
  if ReportStaffPlanningCondDM.QueryDept.Locate('PLANT_CODE;DEPARTMENT_CODE',
       VarArrayOf([Plant, Dept]), []) then
    DeptDesc :=
      ReportStaffPlanningCondDM.QueryDept.FieldByName('DESCRIPTION').AsString;
  // 20013910
  Flag := ReportStaffPlanningCondDM.QueryEMP.FieldByName('FLAG').AsString;
  if (Flag = 'W') then
  begin
    if ReportStaffPlanningCondDM.QueryWK.Locate('PLANT_CODE;WORKSPOT_CODE',
      VarArrayOf([Plant, WKCode]) , []) then
    begin
      QRLabelWKCode.Caption := WKCode + ' ' +
        ReportStaffPlanningCondDM.QueryWK.FieldByName('DESCRIPTION').AsString ;
      if DeptDesc <> '' then
        QRLabelWKCode.Caption := QRLabelWKCode.Caption + ' (' +
          Dept + ' ' +  DeptDesc + ')';
    end;
  end
  else
  begin
    QRLabelWKCode.Caption :=
      ReportStaffPlanningCondDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
    if DeptDesc <> '' then
      QRLabelWKCode.Caption := QRLabelWKCode.Caption + ' ' + DeptDesc;
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDEmpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  Empl, Shift: Integer;
  Plant, Dept: String;
begin
  inherited;
  InitializePlanningPerDaysOfWeek(FEmpPlanning);
  PrintBand := False;
  if not FWKPrint then
  begin
    PrintBand := FWKPrint;
    Exit
  end;
  Empl := ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsInteger;
  Shift := ReportStaffPlanningCondDM.QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger;
  Plant := ReportStaffPlanningCondDM.QueryEMP.FieldByName('PLANT_CODE').AsString;
  Dept := ReportStaffPlanningCondDM.QueryEMP.FieldByName('DEPARTMENT_CODE').AsString;
  if Empl > 0 then
  begin
  //CAR 22-7-2003
    ATBLengthClass.FillTBLength(Empl, Shift, Plant, Dept,  False);
  end;
end;

procedure TReportStaffPlanningCondQR.FillMemo;
var
  IndexDay, j: Integer;
  EmpNumber, EmpName: String;
  DateEmp: TDateTime;
begin
  DateEMP := ReportStaffPlanningCondDM.QueryEMP.
    FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;
  // Fill Memo, fill the QRMemoSave-components here.
  IndexDay := Trunc(DateEMP) - Trunc(FStartDate) + 1;
  if (IndexDay >= 1) and (IndexDay <= 7) then
    for j:= 1 to SystemDM.MaxTimeblocks do
      if FEMPPlanning[IndexDay][j] > 0 then
      begin
        EmpNumber := ReportStaffPlanningCondDM.QueryEMP.
          FieldByName('EMPLOYEE_NUMBER').AsString;
        EmpName := ReportStaffPlanningCondDM.QueryEMP.
          FieldByName('DESCRIPTION').AsString;
        FList[IndexDay].Add(Copy(EmpName, 1, 12));
        FList[IndexDay].Add(EmpNumber);
        Break; // fill entry only once, when 1 of 10 timeblocks is filled.
      end;
end;

procedure TReportStaffPlanningCondQR.QRBandFTEmpBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  // MR:17-02-2005 Order 550380
  // Don't call 'FillMemo' here, but on 'detail'
//  FillMemo;
  PrintBand := False;
end;

// Fill EmpPlanning variable.
procedure TReportStaffPlanningCondQR.QRGroupHDDateBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  DateEMP: TDateTime;
  i, DayEMP : Integer;
  IndexDay: Integer;
  FTBPlanned: Array[1..MAX_TBS] of Integer;
  Schedule_TB: TTBSchedule;
begin
  inherited;
  if not FWKPrint then
  begin
    PrintBand := FWKPrint;
    Exit
  end;

  with ReportStaffPlanningCondDM.QueryEMP do
  begin
    if FieldByName('EMPLOYEE_NUMBER').AsInteger < 0 then
      Exit;

    DateEMP := FieldByName('EMPLOYEEPLANNING_DATE').AsDateTime;

    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      // 20013910 Related to this order.
      // There is no need to call a function, when we already have the
      // information from main-query.
      Schedule_TB[i] :=
        FieldByName('SCHEDULED_TIMEBLOCK_' + IntToStr(i)).AsString;
{
      ReportStaffPlanningCondDM.GetTBFromEMP(
        FieldByName('EMPLOYEE_NUMBER').AsInteger,
        FieldByName('SHIFT_NUMBER').AsInteger,
        FieldByName('PLANT_CODE').AsString,
        FieldByName('WORKSPOT_CODE').AsString,
        FieldByName('DEPARTMENT_CODE').AsString, DATEEMP, SCHEDULE_TB);
}
      if ((SCHEDULE_TB[i] = 'A') or (SCHEDULE_TB[i] = 'B') or
        (SCHEDULE_TB[i] = 'C')) then
        FTBPlanned[i]:= i
      else
        FTBPlanned[i] := 0;
    end;
  end;
  DayEMP := ListProcsF.DayWStartOnFromDate(DateEMP);
  IndexDay := Trunc(DateEMP) - Trunc(FStartDate) + 1;
  if (IndexDay >= 1) and (IndexDay <= 7) then
    for i:= 1 to SystemDM.MaxTimeblocks do
    begin
      if  ATBLengthClass.ExistTB(I) and (FTBPlanned[i] <> 0) then
        FEmpPlanning[IndexDay][i] := FEmpPlanning[IndexDay][i] +
          ATBLengthClass.GetLengthTB(I, DayEMP);
    end;
end;

procedure TReportStaffPlanningCondQR.QRDBTextDeptDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanningCondDM do
  begin
    Value := '';
    if QueryDept.Locate('PLANT_CODE;DEPARTMENT_CODE',
      VarArrayOf([QueryEMP.FieldByName('PLANT_CODE').AsString,
      QueryEMP.FieldByName('DEPARTMENT_CODE').AsString]) , [])  then
       Value := QueryDept.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanningCondQR.QRDBTextShiftDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanningCondDM do
  begin
    Value := '';
    // MR:16-02-2005 Wrong argument for plant_code was used.
    if QueryShift.Locate('PLANT_CODE;SHIFT_NUMBER',
       VarArrayOf([QueryEMP.FieldByName('PLANT_CODE').AsString,
         QueryEMP.FieldByName('SHIFT_NUMBER').AsInteger]), []) then
      Value := QueryShift.FieldByName('DESCRIPTION').AsString;
  end;
end;

procedure TReportStaffPlanningCondQR.QRLabelPlantDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  with ReportStaffPlanningCondDM do
  begin
    Value := '';
    if TablePlant.FindKey([QueryEMP.FieldByName('PLANT_CODE').AsString]) then
    // MR:22-02-2005 Order 550380 Don't truncate the description.
//      Value := Copy(TablePlant.FieldByName('DESCRIPTION').AsString,0,10);
      Value := TablePlant.FieldByName('DESCRIPTION').AsString;
  end;
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningCondQR.QRLabelEmpDescPrint(sender: TObject;
  var Value: String);
begin
  inherited;
  Value := Copy(
    ReportStaffPlanningCondDM.QueryEmp.FieldByName('DESCRIPTION').AsString, 0 , 15);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningCondQR.QRLabel160Print(sender: TObject;
  var Value: String);
begin
  inherited;
  if Pos('(', QRLabelWKCode.Caption) > 0 then
    Value := Copy(QRLabelWKCode.Caption,0, Pos('(', QRLabelWKCode.Caption) -1);
  (Sender as TQRLabel).Caption := Value;
end;

procedure TReportStaffPlanningCondQR.QRBandColumnHeaderAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Column header
    ExportClass.AddText(QRLabel241.Caption);
    ExportClass.AddText(
      QRLabel243.Caption + ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel244.Caption + ExportClass.Sep +
      QRLabel247.Caption + ExportClass.Sep +
      QRLabel248.Caption + ExportClass.Sep +
      QRLabel251.Caption + ExportClass.Sep +
      QRLabel252.Caption + ExportClass.Sep +
      QRLabel255.Caption + ExportClass.Sep +
      QRLabel256.Caption + ExportClass.Sep);
    ExportClass.AddText(
      ExportClass.Sep +
      ExportClass.Sep +
      ExportClass.Sep +
      QRLabel245.Caption + ExportClass.Sep +
      QRLabel246.Caption + ExportClass.Sep +
      QRLabel249.Caption + ExportClass.Sep +
      QRLabel250.Caption + ExportClass.Sep +
      QRLabel253.Caption + ExportClass.Sep +
      QRLabel254.Caption + ExportClass.Sep +
      QRLabel257.Caption + ExportClass.Sep);
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDShiftAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Shift
    with ReportStaffPlanningCondDM do
    begin
      ExportClass.AddText(QRLabel23.Caption + ExportClass.Sep +
        QRLabel24.Caption + ExportClass.Sep +
        QueryEMP.FieldByName('SHIFT_NUMBER').AsString + ExportClass.Sep +
        QRLabelShiftDesc.Caption
        );
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDWKAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile and BandPrinted then
  begin
    // Group HD Workspot
    ExportClass.AddText(QRLabelWKCode.Caption);
  end;
end;

procedure TReportStaffPlanningCondQR.QRBandSummaryBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
end;

procedure TReportStaffPlanningCondQR.QRBandSummaryAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  inherited;
  if QRParameters.FExportToFile then
  begin
    if not ExportClass.ExportDone then
      ExportClass.SaveText;
    ExportClass.ClearText;
    ExportClass.ExportDone := True;
  end;
end;

procedure TReportStaffPlanningCondQR.QRBandColumnHeaderBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  FillDescDaysPerWeek;
end;

procedure TReportStaffPlanningCondQR.QRGroupHDTimeBlockBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i: Integer;
begin
  inherited;
// TEST
if MyDebug then
  WLog('QRGroupHDTimeBlockBeforePrint: ' +
    DateToStr(ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDatetime) + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('DESCRIPTION').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TB').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TBDESC').AsString);

  PrintBand := False;
  for i := 1 to 7 do
    FList[i].Clear;
  MaxCount := 0;
  CurrentIndex := 0;
end;

procedure TReportStaffPlanningCondQR.QRGroupFTTimeBlockAfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
var
  i, j: Integer;
  OneLine: array[1..7] of String;
begin
  inherited;
  for i := 1 to 7 do
    if FList[i].Count > MaxCount then
      MaxCount := FList[i].Count;

  if QRParameters.FExportToFile then
  begin
    if ReportStaffPlanningCondDM.
      QueryEMP.FieldByName('TB').AsString <> '0' then
    begin
      for i := 0 to MaxCount - 1 do
      begin
        for j := 1 to 7 do
        begin
          OneLine[j] := '';
          if i < FList[j].Count then
            OneLine[j] := FList[j][i];
        end;
        ExportClass.AddText(
          ExportClass.Sep +
          ExportClass.Sep +
          ExportClass.Sep +
          TBDescription(ReportStaffPlanningCondDM.
            QueryEMP.FieldByName('TBDESC').AsString) + ExportClass.Sep +
          OneLine[1] + ExportClass.Sep +
          OneLine[2] + ExportClass.Sep +
          OneLine[3] + ExportClass.Sep +
          OneLine[4] + ExportClass.Sep +
          OneLine[5] + ExportClass.Sep +
          OneLine[6] + ExportClass.Sep +
          OneLine[7]
          );
      end;
    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRLoopBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
  function Short(Value: String): String;
  begin
    Result := Copy(Value, 1, 12);
  end;
begin
  inherited;
// Test
if MyDebug then
WLog('- QRLoopBand1BeforePrint');


  with ReportStaffPlanningCondDM.cdsEmpPln do
  begin
    if IsEmpty then
    begin
      PrintBand := False;
      FLoopBandPrintBand := PrintBand;
      Exit;
    end;
    if Eof then
    begin
      PrintBand := False;
      FLoopBandPrintBand := PrintBand;
    end
    else
    begin
      PrintBand := True;
      FLoopBandPrintBand := PrintBand;
      QRLblTB.Caption := TBDescription(FieldByName('TBDESC').AsString);
      case QRParameters.FShowEmployeeMode of
        0, 2: // Name
        begin
          QRLblDay1.Caption := Short(FieldByName('DAY1EMPNAME').AsString);
          QRLblDay2.Caption := Short(FieldByName('DAY2EMPNAME').AsString);
          QRLblDay3.Caption := Short(FieldByName('DAY3EMPNAME').AsString);
          QRLblDay4.Caption := Short(FieldByName('DAY4EMPNAME').AsString);
          QRLblDay5.Caption := Short(FieldByName('DAY5EMPNAME').AsString);
          QRLblDay6.Caption := Short(FieldByName('DAY6EMPNAME').AsString);
          QRLblDay7.Caption := Short(FieldByName('DAY7EMPNAME').AsString);
          if QRParameters.FShowEmployeeMode = 2 then
          begin
            QRLblDay1b.Caption := FieldByName('DAY1EMPNR').AsString;
            QRLblDay2b.Caption := FieldByName('DAY2EMPNR').AsString;
            QRLblDay3b.Caption := FieldByName('DAY3EMPNR').AsString;
            QRLblDay4b.Caption := FieldByName('DAY4EMPNR').AsString;
            QRLblDay5b.Caption := FieldByName('DAY5EMPNR').AsString;
            QRLblDay6b.Caption := FieldByName('DAY6EMPNR').AsString;
            QRLblDay7b.Caption := FieldByName('DAY7EMPNR').AsString;
          end;
        end;
        1: // Number
        begin
          QRLblDay1.Caption := FieldByName('DAY1EMPNR').AsString;
          QRLblDay2.Caption := FieldByName('DAY2EMPNR').AsString;
          QRLblDay3.Caption := FieldByName('DAY3EMPNR').AsString;
          QRLblDay4.Caption := FieldByName('DAY4EMPNR').AsString;
          QRLblDay5.Caption := FieldByName('DAY5EMPNR').AsString;
          QRLblDay6.Caption := FieldByName('DAY6EMPNR').AsString;
          QRLblDay7.Caption := FieldByName('DAY7EMPNR').AsString;
        end;
      end; // case
// TEST
if MyDebug then
WLog('- LOOPBAND: ' + QRLblTB.Caption + ';' + QRLblDay1.Caption + ';' + QRLblDay2.Caption +
  QRLblDay3.Caption + ';' + QRLblDay4.Caption + ';' + QRLblDay5.Caption + ';' +
  QRLblDay6.Caption + ';' + QRLblDay7.Caption);

    end; // if
    if not Eof then
      Next;
  end;
end;

procedure TReportStaffPlanningCondQR.QRGroupFTTimeBlockBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
var
  i, j, Number: Integer;
  OneLine: array[1..7] of String;
  OneLineEmpNr: array[1..7] of String;
begin
  inherited;
// TEST
if MyDebug then
  WLog('QRGroupFTTimeBlockBeforePrint: ' +
    DateToStr(ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDatetime) + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('DESCRIPTION').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('WORKSPOT_CODE').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TB').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TBDESC').AsString);

  PrintBand := False;
  for i := 1 to 7 do
    if FList[i].Count > MaxCount then
      MaxCount := FList[i].Count;

  // Store all in a clientdataset
  ReportStaffPlanningCondDM.cdsEmpPln.EmptyDataSet;
  Number := 1;
  for i := 0 to MaxCount - 1 do
  begin
    for j := 1 to 7 do
    begin
      if ((i+1) MOD 2) <> 0 then
        OneLine[j] := ''
      else
        OneLineEmpNr[j] := '';
        if i < FList[j].Count  then
        begin
          if ((i+1) MOD 2) <> 0 then
              OneLine[j] := FList[j][i]
          else
            OneLineEmpNr[j] := FList[j][i];
        end;
    end;
    if ((i+1) MOD 2) = 0 then
    begin
      with ReportStaffPlanningCondDM do
      begin
        cdsEmpPln.Insert;
        ClientDataSetNumber := ClientDataSetNumber + 1;
        cdsEmpPln.FieldByName('CDSNUMBER').AsInteger :=
          ClientDataSetNumber;
        cdsEmpPln.FieldByName('TB').AsString :=
          QueryEMP.FieldByName('TB').AsString;
        cdsEmpPln.FieldByName('TBDESC').AsString :=
          QueryEMP.FieldByName('TBDESC').AsString;
        cdsEmpPln.FieldByName('NUMBER').AsInteger := Number;
        cdsEmpPln.FieldByName('DAY1EMPNAME').AsString := OneLine[1];
        cdsEmpPln.FieldByName('DAY1EMPNR').AsString := OneLineEmpNr[1];
        cdsEmpPln.FieldByName('DAY2EMPNAME').AsString := OneLine[2];
        cdsEmpPln.FieldByName('DAY2EMPNR').AsString := OneLineEmpNr[2];
        cdsEmpPln.FieldByName('DAY3EMPNAME').AsString := OneLine[3];
        cdsEmpPln.FieldByName('DAY3EMPNR').AsString := OneLineEmpNr[3];
        cdsEmpPln.FieldByName('DAY4EMPNAME').AsString := OneLine[4];
        cdsEmpPln.FieldByName('DAY4EMPNR').AsString := OneLineEmpNr[4];
        cdsEmpPln.FieldByName('DAY5EMPNAME').AsString := OneLine[5];
        cdsEmpPln.FieldByName('DAY5EMPNR').AsString := OneLineEmpNr[5];
        cdsEmpPln.FieldByName('DAY6EMPNAME').AsString := OneLine[6];
        cdsEmpPln.FieldByName('DAY6EMPNR').AsString := OneLineEmpNr[6];
        cdsEmpPln.FieldByName('DAY7EMPNAME').AsString := OneLine[7];
        cdsEmpPln.FieldByName('DAY7EMPNR').AsString := OneLineEmpNr[7];
        cdsEmpPln.Post;
        inc(Number);
      end;
    end;
  end;
  if not ReportStaffPlanningCondDM.cdsEmpPln.IsEmpty then
  begin
    ReportStaffPlanningCondDM.cdsEmpPln.First;
    if ReportStaffPlanningCondDM.cdsEmpPln.FieldByName('TB').AsString <> '0' then
    begin
      QRLoopBand1.PrintCount := ReportStaffPlanningCondDM.cdsEmpPln.RecordCount;
// TEST
if MyDebug then
WLog('- QRLoopBand1.PrintCount=' + IntToStr(QRLoopBand1.PrintCount));

    end;
  end;
end;

procedure TReportStaffPlanningCondQR.QRBandDetailBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
// TEST
if MyDebug then
  WLog('QRBandDetailBeforePrint: ' +
    DateToStr(ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEEPLANNING_DATE').AsDatetime) + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('EMPLOYEE_NUMBER').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('DESCRIPTION').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TB').AsString + ';' +
    ReportStaffPlanningCondDM.QueryEMP.FieldByName('TBDESC').AsString);

  // MR:17-02-2005 Order 550380 Call 'FillMemo' here.
  FillMemo;
  PrintBand := False;
end;

procedure TReportStaffPlanningCondQR.QRGroupFTWKBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := False;
  Exit;
end;

procedure TReportStaffPlanningCondQR.QRChildBandEmployeeNumberBeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  PrintBand := FLoopBandPrintBand;
end;

end.
