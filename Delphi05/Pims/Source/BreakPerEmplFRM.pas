(*
  MRA:25-MAR-2013 TD-22311
  - In second grid: Show plant-shift-combination, to make
    it possible to also show breaks-per-employee for
    employee where the plant was changed.
  - To make it more user-friendly:
    - Upon opening this dialog, show as selected row in second grid the one that
      is based on current employee+plant-combination.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
  MRA:24-JUN-2019
  - Week start at Wednesday
*)
unit BreakPerEmplFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, CalculateTotalHoursDMT;

type
  TBreakPerEmplF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridTimeColumn;
    dxMasterGridColumn4: TdxDBGridTimeColumn;
    dxMasterGridColumn5: TdxDBGridTimeColumn;
    dxMasterGridColumn6: TdxDBGridTimeColumn;
    dxMasterGridColumn7: TdxDBGridTimeColumn;
    dxMasterGridColumn8: TdxDBGridTimeColumn;
    dxMasterGridColumn9: TdxDBGridTimeColumn;
    dxMasterGridColumn10: TdxDBGridTimeColumn;
    dxMasterGridColumn11: TdxDBGridTimeColumn;
    dxMasterGridColumn12: TdxDBGridTimeColumn;
    dxMasterGridColumn13: TdxDBGridTimeColumn;
    dxMasterGridColumn14: TdxDBGridTimeColumn;
    dxMasterGridColumn15: TdxDBGridTimeColumn;
    dxMasterGridColumn16: TdxDBGridTimeColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    Label1: TLabel;
    dxDBTimeEditST1: TdxDBTimeEdit;
    dxDBTimeEditET1: TdxDBTimeEdit;
    dxDBTimeEditST2: TdxDBTimeEdit;
    dxDBTimeEditET2: TdxDBTimeEdit;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    DBEditBreak: TDBEdit;
    DBEditDesc: TDBEdit;
    DBCheckBoxPayedYN: TDBCheckBox;
    PanelEmpl: TPanel;
    dxDBGridEmpl: TdxDBGrid;
    dxDBGridEmplColumn8: TdxDBGridColumn;
    dxDBGridEmplColumn4: TdxDBGridLookupColumn;
    dxDBGridEmplColumn5: TdxDBGridColumn;
    dxDBGridEmplColumn6: TdxDBGridColumn;
    dxDBGridEmplColumn7: TdxDBGridColumn;
    dxDBGridEmplColumn3: TdxDBGridColumn;
    dxDBGridEmplColumn9: TdxDBGridColumn;
    dxMasterGridColumnPlantCode: TdxDBGridColumn;
    dxMasterGridColumnPlantDescription: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBGridEmplClick(Sender: TObject);
    procedure dxMasterGridClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxDBGridEmplChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure ScrollBarMasterScroll(Sender: TObject;
      ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure LocateRowInMasterGrid;
  public
    { Public declarations }
   

  end;

function BreakPerEmplF: TBreakPerEmplF;

implementation

{$R *.DFM}
uses
  SystemDMT, BreakPerEmplDMT, UPimsMessageRes;

var
  BreakPerEmplF_HND: TBreakPerEmplF;

function BreakPerEmplF: TBreakPerEmplF;
begin
  if (BreakPerEmplF_HND = nil) then
  begin
    BreakPerEmplF_HND := TBreakPerEmplF.Create(Application);
    BreakPerEmplDM.Handle_Grid := BreakPerEmplF_HND.dxDetailGrid;
  end;
  Result := BreakPerEmplF_HND;
end;

procedure TBreakPerEmplF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  DBEditDesc.SetFocus;
  inherited;
end;

procedure TBreakPerEmplF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TBreakPerEmplF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  // TD-22311 Extra band was added for Plant.
  //          Assignment of days is changed!
  for Index := 1 to 7 do
  begin
    // GLOB3-284 Index for days must start at 1, for band at 2.
    dxMasterGrid.Bands[Index+1].Caption := SystemDM.GetDayWDescription(Index);
  end;
  for Index := 1 to 7 do
  begin
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
  end;
  //car 550279
  BreakPerEmplDM.QueryEmpl.First;
  //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      BreakPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
    BreakPerEmplDM.QueryEmpl.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee, []);
  //
  BreakPerEmplDM.SetEmployee;

  // TD-22311
  LocateRowInMasterGrid;

  dxMasterGrid.SetFocus;
end;

procedure TBreakPerEmplF.FormDestroy(Sender: TObject);
begin
  inherited;
  BreakPerEmplF_HND := nil;
end;

procedure TBreakPerEmplF.dxDBGridEmplClick(Sender: TObject);
begin
  inherited;
  BreakPerEmplDM.SetEmployee;
  LocateRowInMasterGrid; // TD-22311
end;

procedure TBreakPerEmplF.dxMasterGridClick(Sender: TObject);
begin
  inherited;
  BreakPerEmplDM.SetEmployee;
end;

procedure TBreakPerEmplF.FormCreate(Sender: TObject);
begin
  BreakPerEmplDM := CreateFormDM(TBreakPerEmplDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) or
    (dxDBGridEmpl.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := BreakPerEmplDM.DataSourceDetail;
    dxMasterGrid.DataSource := BreakPerEmplDM.DataSourceMaster;
    dxDBGridEmpl.DataSource := BreakPerEmplDM.DataSourceEmpl;
  end;
  inherited;
  // 20014289
  dxDBGridEmpl.BandFont.Color := clWhite;
  dxDBGridEmpl.BandFont.Style := [fsBold];
end;

procedure TBreakPerEmplF.dxDBGridEmplChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  BreakPerEmplDM.SetEmployee;
  LocateRowInMasterGrid; // TD-22311
end;

procedure TBreakPerEmplF.dxMasterGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  BreakPerEmplDM.SetEmployee;
end;

procedure TBreakPerEmplF.ScrollBarMasterScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  dxDBGridEmpl.SetFocus;
end;

procedure TBreakPerEmplF.dxBarButtonExportGridClick(Sender: TObject);
begin
//CAR 27.02.2003
  FloatEmpl := 'EMPLOYEE_NUMBER';
  CreateExportColumns(dxDBGridEmpl, '', 'PLANT_CODE');
  CreateExportColumns(dxDBGridEmpl, '', 'PLANTLU');
  CreateExportColumns(dxDBGridEmpl, '', 'EMPLOYEE_NUMBER');
  CreateExportColumnsDesc(SExportDescEMPL, 'EMPLLU');
  inherited;
end;

procedure TBreakPerEmplF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
  //CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    BreakPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TBreakPerEmplF.FormResize(Sender: TObject);
begin
  inherited;
//CAR 15-10-2003 - on maximize screen give the same height for both grids
  if BreakPerEmplF.ClientHeight > 600 then
    pnlMasterGrid.Height := 200
  else
    pnlMasterGrid.Height := 120;
end;

// TD-22311 In Second Grid: Locate first record based on current
//          Employee + Plant-combination.
procedure TBreakPerEmplF.LocateRowInMasterGrid;
begin
  try
    if BreakPerEmplDM.TableMaster.Active then
      BreakPerEmplDM.TableMaster.Locate('PLANT_CODE;SHIFT_NUMBER',
        VarArrayOf([BreakPerEmplDM.QueryEmpl.FieldByName('PLANT_CODE').AsString,
        BreakPerEmplDM.QueryEmpl.FieldByName('SHIFT_NUMBER').AsInteger]),
        []);
  except
  end;
end;

end.
