inherited StandardOccupationDM: TStandardOccupationDM
  OldCreateOrder = True
  OnDestroy = DataModuleDestroy
  Left = 321
  Top = 180
  Height = 563
  Width = 826
  inherited TableMaster: TTable
    Top = 64
  end
  inherited TableDetail: TTable
    BeforeInsert = TableDetailBeforeInsert
    OnCalcFields = TableDetailCalcFields
    OnNewRecord = TableDetailNewRecord
    IndexFieldNames = 
      'PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE;SHIFT_NUMBER;DAY_OF_WEE' +
      'K'
    MasterFields = 'PLANT_CODE;DEPARTMENT_CODE;WORKSPOT_CODE'
    MasterSource = DataSourceDepartmentMASTER
    TableName = 'STANDARDOCCUPATION'
    Top = 132
    object TableDetailPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Required = True
      Size = 6
    end
    object TableDetailSHIFT_NUMBER: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'SHIFT_NUMBER'
    end
    object TableDetailDAY_OF_WEEK: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'DAY_OF_WEEK'
    end
    object TableDetailOCC_A_TIMEBLOCK_1: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_1'
    end
    object TableDetailOCC_A_TIMEBLOCK_2: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_2'
    end
    object TableDetailOCC_A_TIMEBLOCK_3: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_3'
    end
    object TableDetailOCC_A_TIMEBLOCK_4: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_4'
    end
    object TableDetailOCC_B_TIMEBLOCK_1: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_1'
    end
    object TableDetailOCC_B_TIMEBLOCK_2: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_2'
    end
    object TableDetailOCC_B_TIMEBLOCK_3: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_3'
    end
    object TableDetailOCC_B_TIMEBLOCK_4: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_4'
    end
    object TableDetailOCC_C_TIMEBLOCK_1: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_1'
    end
    object TableDetailOCC_C_TIMEBLOCK_2: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_2'
    end
    object TableDetailOCC_C_TIMEBLOCK_3: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_3'
    end
    object TableDetailOCC_C_TIMEBLOCK_4: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_4'
    end
    object TableDetailFIXED_YN: TStringField
      FieldName = 'FIXED_YN'
      Size = 1
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
    end
    object TableDetailDAYDESCRIPTIONLU: TStringField
      DisplayLabel = 'DAY'
      FieldKind = fkLookup
      FieldName = 'DAYDESCRIPTIONLU'
      LookupDataSet = SystemDM.TableDay
      LookupKeyFields = 'DAY_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'DAY_OF_WEEK'
      LookupCache = True
      Lookup = True
    end
    object TableDetailDAYNR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DAYNR'
      Calculated = True
    end
    object TableDetailDAYDESC: TStringField
      FieldKind = fkCalculated
      FieldName = 'DAYDESC'
      Size = 30
      Calculated = True
    end
    object TableDetailPLANTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'PLANTLU'
      LookupDataSet = QueryPlantShift
      LookupKeyFields = 'PLANT_CODE'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PLANT_CODE'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailSHIFTLU: TStringField
      FieldKind = fkLookup
      FieldName = 'SHIFTLU'
      LookupDataSet = QueryShift
      LookupKeyFields = 'SHIFT_NUMBER'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'SHIFT_NUMBER'
      LookupCache = True
      Size = 30
      Lookup = True
    end
    object TableDetailOCC_A_TIMEBLOCK_5: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_5'
    end
    object TableDetailOCC_A_TIMEBLOCK_6: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_6'
    end
    object TableDetailOCC_A_TIMEBLOCK_7: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_7'
    end
    object TableDetailOCC_A_TIMEBLOCK_8: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_8'
    end
    object TableDetailOCC_A_TIMEBLOCK_9: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_9'
    end
    object TableDetailOCC_A_TIMEBLOCK_10: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_A_TIMEBLOCK_10'
    end
    object TableDetailOCC_B_TIMEBLOCK_5: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_5'
    end
    object TableDetailOCC_B_TIMEBLOCK_6: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_6'
    end
    object TableDetailOCC_B_TIMEBLOCK_7: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_7'
    end
    object TableDetailOCC_B_TIMEBLOCK_8: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_8'
    end
    object TableDetailOCC_B_TIMEBLOCK_9: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_9'
    end
    object TableDetailOCC_B_TIMEBLOCK_10: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_B_TIMEBLOCK_10'
    end
    object TableDetailOCC_C_TIMEBLOCK_5: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_5'
    end
    object TableDetailOCC_C_TIMEBLOCK_6: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_6'
    end
    object TableDetailOCC_C_TIMEBLOCK_7: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_7'
    end
    object TableDetailOCC_C_TIMEBLOCK_8: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_8'
    end
    object TableDetailOCC_C_TIMEBLOCK_9: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_9'
    end
    object TableDetailOCC_C_TIMEBLOCK_10: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'OCC_C_TIMEBLOCK_10'
    end
  end
  inherited DataSourceMaster: TDataSource
    Left = 248
    Top = 56
  end
  inherited DataSourceDetail: TDataSource
    Left = 248
  end
  inherited TableExport: TTable
    OnEditError = DefaultEditError
    Left = 388
    Top = 332
  end
  inherited DataSourceExport: TDataSource
    Left = 520
    Top = 332
  end
  object DataSourcePlantShift: TDataSource
    DataSet = QueryPlantShift
    Left = 248
    Top = 248
  end
  object DataSourceShift: TDataSource
    DataSet = QueryShift
    Left = 248
    Top = 184
  end
  object QueryPlantShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    OnFilterRecord = QueryPlantShiftFilterRecord
    SQL.Strings = (
      'SELECT'
      '  DISTINCT S.PLANT_CODE, P.DESCRIPTION'
      'FROM'
      '  SHIFT S, PLANT P'
      'WHERE'
      '  P.PLANT_CODE = S.PLANT_CODE'
      'ORDER BY'
      '  S.PLANT_CODE'
      ''
      ' ')
    Left = 96
    Top = 248
  end
  object QueryDepartmentMASTER: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  D.PLANT_CODE,'
      '  D.DEPARTMENT_CODE,'
      '  D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  '#39'0'#39' AS WORKSPOT_CODE,'
      '  D.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  D.PLAN_ON_WORKSPOT_YN'
      'FROM'
      '  DEPARTMENT D'
      'WHERE'
      '  D.PLANT_CODE = :PLANT_CODE AND'
      '  D.PLAN_ON_WORKSPOT_YN = '#39'N'#39
      'UNION'
      'SELECT'
      '  D.PLANT_CODE,'
      '  D.DEPARTMENT_CODE,'
      '  D.DESCRIPTION AS DEPARTMENTDESCRIPTION,'
      '  W.WORKSPOT_CODE,'
      '  W.DESCRIPTION AS WORKSPOTDESCRIPTION,'
      '  D.PLAN_ON_WORKSPOT_YN'
      'FROM '
      '  DEPARTMENT D, '
      '  WORKSPOT W'
      'WHERE '
      '  D.PLANT_CODE = :PLANT_CODE AND'
      '  W.PLANT_CODE = D.PLANT_CODE AND'
      '  W.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND'
      '  D.PLAN_ON_WORKSPOT_YN = '#39'Y'#39
      'ORDER BY 1, 2, 4'
      ' '
      ' ')
    Left = 88
    Top = 8
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
    object QueryDepartmentMASTERPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryDepartmentMASTERDEPARTMENT_CODE: TStringField
      FieldName = 'DEPARTMENT_CODE'
      Size = 6
    end
    object QueryDepartmentMASTERDEPARTMENTDESCRIPTION: TStringField
      FieldName = 'DEPARTMENTDESCRIPTION'
      Size = 30
    end
    object QueryDepartmentMASTERWORKSPOT_CODE: TStringField
      FieldName = 'WORKSPOT_CODE'
      Size = 6
    end
    object QueryDepartmentMASTERWORKSPOTDESCRIPTION: TStringField
      FieldName = 'WORKSPOTDESCRIPTION'
      Size = 30
    end
    object QueryDepartmentMASTERPLAN_ON_WORKSPOT_YN: TStringField
      FieldName = 'PLAN_ON_WORKSPOT_YN'
      Size = 1
    end
  end
  object DataSourceDepartmentMASTER: TDataSource
    DataSet = QueryDepartmentMASTER
    Left = 248
    Top = 8
  end
  object QueryTimeBlockPerDepartment: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  DEPARTMENT_CODE,'
      '  SHIFT_NUMBER,'
      '  TIMEBLOCK_NUMBER,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  TIMEBLOCKPERDEPARTMENT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  DEPARTMENT_CODE = :DEPARTMENT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER')
    Left = 392
    Top = 232
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'DEPARTMENT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryTimeBlockPerShift: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  PLANT_CODE,'
      '  SHIFT_NUMBER,'
      '  TIMEBLOCK_NUMBER,'
      '  STARTTIME1,'
      '  ENDTIME1,'
      '  STARTTIME2,'
      '  ENDTIME2,'
      '  STARTTIME3,'
      '  ENDTIME3,'
      '  STARTTIME4,'
      '  ENDTIME4,'
      '  STARTTIME5,'
      '  ENDTIME5,'
      '  STARTTIME6,'
      '  ENDTIME6,'
      '  STARTTIME7,'
      '  ENDTIME7'
      'FROM'
      '  TIMEBLOCKPERSHIFT'
      'WHERE'
      '  PLANT_CODE = :PLANT_CODE AND'
      '  SHIFT_NUMBER = :SHIFT_NUMBER')
    Left = 392
    Top = 280
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'SHIFT_NUMBER'
        ParamType = ptUnknown
      end>
  end
  object QueryStandardOccupationFROM: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT'
      '  DAY_OF_WEEK,'
      '  OCC_A_TIMEBLOCK_1,'
      '  OCC_A_TIMEBLOCK_2,'
      '  OCC_A_TIMEBLOCK_3,'
      '  OCC_A_TIMEBLOCK_4,'
      '  OCC_A_TIMEBLOCK_5,'
      '  OCC_A_TIMEBLOCK_6,'
      '  OCC_A_TIMEBLOCK_7,'
      '  OCC_A_TIMEBLOCK_8,'
      '  OCC_A_TIMEBLOCK_9,'
      '  OCC_A_TIMEBLOCK_10,'
      '  OCC_B_TIMEBLOCK_1,'
      '  OCC_B_TIMEBLOCK_2,'
      '  OCC_B_TIMEBLOCK_3,'
      '  OCC_B_TIMEBLOCK_4,'
      '  OCC_B_TIMEBLOCK_5,'
      '  OCC_B_TIMEBLOCK_6,'
      '  OCC_B_TIMEBLOCK_7,'
      '  OCC_B_TIMEBLOCK_8,'
      '  OCC_B_TIMEBLOCK_9,'
      '  OCC_B_TIMEBLOCK_10,'
      '  OCC_C_TIMEBLOCK_1,'
      '  OCC_C_TIMEBLOCK_2,'
      '  OCC_C_TIMEBLOCK_3,'
      '  OCC_C_TIMEBLOCK_4,'
      '  OCC_C_TIMEBLOCK_5,'
      '  OCC_C_TIMEBLOCK_6,'
      '  OCC_C_TIMEBLOCK_7,'
      '  OCC_C_TIMEBLOCK_8,'
      '  OCC_C_TIMEBLOCK_9,'
      '  OCC_C_TIMEBLOCK_10,'
      '  FIXED_YN'
      'FROM'
      '  STANDARDOCCUPATION'
      ' ')
    Left = 96
    Top = 320
    object QueryStandardOccupationFROMDAY_OF_WEEK: TIntegerField
      FieldName = 'DAY_OF_WEEK'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_1: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_1'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_2: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_2'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_3: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_3'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_4: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_4'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_1: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_1'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_2: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_2'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_3: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_3'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_4: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_4'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_1: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_1'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_2: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_2'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_3: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_3'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_4: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_4'
    end
    object QueryStandardOccupationFROMFIXED_YN: TStringField
      FieldName = 'FIXED_YN'
      Size = 1
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_5: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_5'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_5'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_6: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_6'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_6'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_7: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_7'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_7'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_8: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_8'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_8'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_9: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_9'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_9'
    end
    object QueryStandardOccupationFROMOCC_A_TIMEBLOCK_10: TIntegerField
      FieldName = 'OCC_A_TIMEBLOCK_10'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_A_TIMEBLOCK_10'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_5: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_5'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_5'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_6: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_6'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_6'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_7: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_7'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_7'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_8: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_8'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_8'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_9: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_9'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_9'
    end
    object QueryStandardOccupationFROMOCC_B_TIMEBLOCK_10: TIntegerField
      FieldName = 'OCC_B_TIMEBLOCK_10'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_B_TIMEBLOCK_10'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_5: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_5'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_5'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_6: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_6'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_6'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_7: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_7'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_7'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_8: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_8'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_8'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_9: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_9'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_9'
    end
    object QueryStandardOccupationFROMOCC_C_TIMEBLOCK_10: TIntegerField
      FieldName = 'OCC_C_TIMEBLOCK_10'
      Origin = 'PIMS.STANDARDOCCUPATION.OCC_C_TIMEBLOCK_10'
    end
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    DataSource = DataSourcePlantShift
    SQL.Strings = (
      'SELECT '
      '  PLANT_CODE, SHIFT_NUMBER, DESCRIPTION'
      'FROM '
      '  SHIFT'
      'WHERE '
      '  PLANT_CODE = :PLANT_CODE '
      '  AND SHIFT_NUMBER <> 0 '
      'ORDER BY '
      '  SHIFT_NUMBER')
    Left = 96
    Top = 184
    ParamData = <
      item
        DataType = ftString
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
    object QueryShiftPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Size = 6
    end
    object QueryShiftSHIFT_NUMBER: TIntegerField
      FieldName = 'SHIFT_NUMBER'
    end
    object QueryShiftDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Size = 30
    end
  end
  object QuerySelectDeptPerTeam: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  DISTINCT D.PLANT_CODE, D.DEPARTMENT_CODE '
      'FROM '
      '  DEPARTMENTPERTEAM D, TEAMPERUSER U '
      'WHERE'
      '  U.USER_NAME = :USER_NAME AND'
      '  D.TEAM_CODE = U.TEAM_CODE'
      'ORDER BY'
      '  D.PLANT_CODE, D.DEPARTMENT_CODE')
    Left = 248
    Top = 392
    ParamData = <
      item
        DataType = ftString
        Name = 'USER_NAME'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetExportDept: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderExportDept'
    Left = 392
    Top = 392
  end
  object DataSetProviderExportDept: TDataSetProvider
    DataSet = QuerySelectDeptPerTeam
    Constraints = True
    Left = 528
    Top = 392
  end
  object TablePlant: TTable
    BeforePost = DefaultBeforePost
    BeforeDelete = DefaultBeforeDelete
    OnDeleteError = DefaultDeleteError
    OnEditError = DefaultEditError
    OnNewRecord = DefaultNewRecord
    OnPostError = DefaultPostError
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    IndexFieldNames = 'PLANT_CODE'
    MasterFields = 'PLANT_CODE'
    MasterSource = DataSourceDepartmentMASTER
    TableName = 'PLANT'
    Left = 388
    Top = 64
    object TablePlantPLANT_CODE: TStringField
      FieldName = 'PLANT_CODE'
      Required = True
      Size = 24
    end
    object TablePlantSTDOCC_A_YN: TStringField
      FieldName = 'STDOCC_A_YN'
      Size = 4
    end
    object TablePlantSTDOCC_B_YN: TStringField
      FieldName = 'STDOCC_B_YN'
      Size = 4
    end
    object TablePlantSTDOCC_C_YN: TStringField
      FieldName = 'STDOCC_C_YN'
      Size = 4
    end
    object TablePlantDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
    object TablePlantADDRESS: TStringField
      FieldName = 'ADDRESS'
      Size = 120
    end
    object TablePlantZIPCODE: TStringField
      FieldName = 'ZIPCODE'
      Size = 60
    end
    object TablePlantCITY: TStringField
      FieldName = 'CITY'
      Size = 120
    end
    object TablePlantSTATE: TStringField
      FieldName = 'STATE'
      Size = 80
    end
    object TablePlantPHONE: TStringField
      FieldName = 'PHONE'
      Size = 60
    end
    object TablePlantFAX: TStringField
      FieldName = 'FAX'
      Size = 60
    end
    object TablePlantCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TablePlantINSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'INSCAN_MARGIN_EARLY'
      Required = True
    end
    object TablePlantINSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'INSCAN_MARGIN_LATE'
      Required = True
    end
    object TablePlantMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TablePlantMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      Size = 80
    end
    object TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_EARLY'
      Required = True
    end
    object TablePlantOUTSCAN_MARGIN_LATE: TIntegerField
      FieldName = 'OUTSCAN_MARGIN_LATE'
      Required = True
    end
    object TablePlantAVERAGE_WAGE: TFloatField
      FieldName = 'AVERAGE_WAGE'
    end
    object TablePlantCUSTOMER_NUMBER: TIntegerField
      FieldName = 'CUSTOMER_NUMBER'
    end
    object TablePlantCOUNTRY_ID: TFloatField
      FieldName = 'COUNTRY_ID'
      Required = True
    end
    object TablePlantTIMEZONE: TMemoField
      FieldName = 'TIMEZONE'
      BlobType = ftMemo
      Size = 400
    end
    object TablePlantTIMEZONEHRSDIFF: TSmallintField
      FieldName = 'TIMEZONEHRSDIFF'
    end
    object TablePlantCUTOFFTIMESHIFTDATE: TDateTimeField
      FieldName = 'CUTOFFTIMESHIFTDATE'
    end
    object TablePlantREALTIMEINTERVAL: TFloatField
      FieldName = 'REALTIMEINTERVAL'
    end
  end
  object DataSourcePlant: TDataSource
    DataSet = TablePlant
    Left = 480
    Top = 64
  end
end
