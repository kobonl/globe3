(*
  SO: 19-07-2010 Order 550497
  MRA:19-OCT-2011 RV099.1. Order 20012234
  - Addition of 3 fields/settings for TFT/Holiday correction.
  - These can be left empty (nullable).
    - TFT_HOL_AUTO_REPLACE_YN
    - TFT_ABSENCEREASON_ID
    - HOL_ABSENCEREASON_ID
*)
unit CountriesFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, DBCtrls, StdCtrls, dxEditor, dxEdLib, dxDBELib,
  dxDBTLCl, dxGrClms, dxExEdtr, dxDBEdtr;

type
  TCountriesF = class(TGridBaseF)
    dxDetailGridColumnCode: TdxDBGridColumn;
    dxDetailGridColumnDescription: TdxDBGridColumn;
    dxDetailGridColumnExportType: TdxDBGridColumn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEditCode: TdxDBEdit;
    DBEditDescription: TdxDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    lblAutoReplace: TLabel;
    dxDBCheckEditAutoReplace: TdxDBCheckEdit;
    lblAbsReasonTFT: TLabel;
    lblAbsReasonHOL: TLabel;
    DBLookupComboBoxAbsenceReasonTFT: TDBLookupComboBox;
    DBLookupComboBoxAbsenceReasonHOL: TDBLookupComboBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormShow(Sender: TObject);
    procedure dxDBCheckEditAutoReplaceChange(Sender: TObject);
  private
    { Private declarations }
    procedure HandleFields;
  public
    { Public declarations }
  end;

function CountriesF: TCountriesF;

implementation

uses CountriesDMT;

{$R *.DFM}

var
  CountriesF_HND: TCountriesF;
  
function CountriesF: TCountriesF;
begin
  if (CountriesF_HND = nil) then
  begin
    CountriesF_HND := TCountriesF.Create(Application);
  end;
  Result := CountriesF_HND;
end;

procedure TCountriesF.FormDestroy(Sender: TObject);
begin
  inherited;
  CountriesF_HND := nil;
end;

procedure TCountriesF.FormCreate(Sender: TObject);
begin
  CountriesDM := CreateFormDM(TCountriesDM);
  if (dxDetailGrid.DataSource = nil) then
    dxDetailGrid.DataSource := CountriesDM.DataSourceDetail;
  inherited;
end;

procedure TCountriesF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditCode.SetFocus;
end;

// RV099.1.
procedure TCountriesF.HandleFields;
  procedure MakeEnabled(AEnable: Boolean);
  begin
    lblAbsReasonTFT.Enabled := AEnable;
    lblAbsReasonHOL.Enabled := AEnable;
    DBLookupComboBoxAbsenceReasonTFT.Enabled := AEnable;
    DBLookupComboBoxAbsenceReasonHOL.Enabled := AEnable;
    DBLookupComboBoxAbsenceReasonTFT.Visible := AEnable;
    DBLookupComboBoxAbsenceReasonHOL.Visible := AEnable;
  end;
begin
  // Only look at checkbox-component if it is checked or not.
  // The database-field is not stored yet, so it can differ from the
  // checkbox.
  if dxDBCheckEditAutoReplace.Checked
    {
      or
      (CountriesDM.QueryDetail.
      FieldByName('TFT_HOL_AUTO_REPLACE_YN').AsString = CHECKEDVALUE)
    } then
    MakeEnabled(True)
  else
    MakeEnabled(False);
  CountriesDM.FillAbsenceReasons(
    CountriesDM.QueryDetail.FieldByName('COUNTRY_ID').AsInteger);
end;

// RV099.1.
procedure TCountriesF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  HandleFields;
end;

procedure TCountriesF.FormShow(Sender: TObject);
begin
  inherited;
  // RV099.1.
  HandleFields;
end;

// RV099.1.
procedure TCountriesF.dxDBCheckEditAutoReplaceChange(Sender: TObject);
begin
  inherited;
  HandleFields;
end;

end.
