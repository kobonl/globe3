unit DialogAddWKWorkstationFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogSelectionFRM, StdCtrls, Dblup1a, ComCtrls, Buttons, ExtCtrls,
  dxLayout, dxCntner, dxEditor, dxExEdtr, dxDBEdtr, dxDBELib, dxExGrEd,
  dxExELib, Db, DBTables;

type
  TDialogAddWKWorkstationF = class(TDialogSelectionF)
    LabelFrom: TLabel;
    dxDBExtLookupEditComputer: TdxDBExtLookupEdit;
    LabelTo: TLabel;
    LabelName: TLabel;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    TableWorkStation: TTable;
    TableWorkStationCOMPUTER_NAME: TStringField;
    TableWorkStationLICENSEVERSION: TIntegerField;
    TableWorkStationTIMERECORDING_YN: TStringField;
    DataSourceWorkStation: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure dxDBExtLookupEditComputerCloseUp(Sender: TObject;
      var Text: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
  protected
      procedure CreateParams(var params: TCreateParams); override;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogAddWKWorkstationF: TDialogAddWKWorkstationF;

implementation
uses WorkSpotPerStationDMT, ListProcsFRM, SystemDMT, UPimsMessageRes;
{$R *.DFM}

procedure TDialogAddWKWorkstationF.FormShow(Sender: TObject);
begin
  inherited;
  WorkSpotPerStationDM.TableWorkStation.First;
  dxDBExtLookupEditComputer.Text := WorkSpotPerStationDM.TableWorkStation.
  	FieldByName('COMPUTER_NAME').AsString;
end;

procedure TDialogAddWKWorkstationF.btnOkClick(Sender: TObject);
begin
  if WorkSpotPerStationDM.TableMaster.FieldByName('COMPUTER_NAME').
    AsString = dxDBExtLookupEditComputer.Text then
  begin
  	DisplayMessage(SPimsSameSourceAndDest, mtInformation, [mbOk]);
    ModalResult := mrNone;
    exit;
  end;
  inherited;
end;

procedure TDialogAddWKWorkstationF.dxDBExtLookupEditComputerCloseUp(
  Sender: TObject; var Text: String; var Accept: Boolean);
begin
  inherited;
  WorkSpotPerStationDM.TableWorkStation.FindKey([Text]);
end;

procedure TDialogAddWKWorkstationF.FormCreate(Sender: TObject);
begin
  inherited;
  TableWorkStation.Active := True;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogAddWKWorkstationF.CreateParams(var params: TCreateParams);
begin
  inherited;
  params.WndParent := Screen.ActiveForm.Handle;

  if (params.WndParent <> 0) and (IsIconic(params.WndParent)
    or not IsWindowVisible(params.WndParent)
    or not IsWindowEnabled(params.WndParent)) then
    params.WndParent := 0;

  if params.WndParent = 0 then
    params.WndParent := Application.Handle;
end;

end.
