inherited DialogReportCompHoursF: TDialogReportCompHoursF
  Left = 673
  Top = 142
  Caption = 'Comparison worked hours'
  ClientHeight = 391
  ClientWidth = 594
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 594
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 298
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 594
    Height = 270
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Left = 24
      Top = 9
    end
    inherited LblPlant: TLabel
      Left = 56
      Top = 9
    end
    inherited LblFromEmployee: TLabel
      Left = 24
      Top = 36
    end
    inherited LblEmployee: TLabel
      Left = 56
      Top = 36
    end
    inherited LblToPlant: TLabel
      Left = 320
      Top = 9
    end
    inherited LblToEmployee: TLabel
      Left = 320
      Top = 36
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 130
      Top = 38
    end
    inherited LblStarEmployeeTo: TLabel
      Top = 38
    end
    object Label1: TLabel [8]
      Left = 24
      Top = 88
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [9]
      Left = 214
      Top = 116
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [10]
      Left = 24
      Top = 90
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel [11]
      Left = 56
      Top = 90
      Width = 23
      Height = 13
      Caption = 'Date'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel [12]
      Left = 320
      Top = 90
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarTeamTo: TLabel
      Top = 66
    end
    inherited LblToTeam: TLabel
      Left = 320
      Top = 63
    end
    inherited LblStarTeamFrom: TLabel
      Left = 130
      Top = 66
    end
    inherited LblTeam: TLabel
      Left = 56
      Top = 63
    end
    inherited LblFromTeam: TLabel
      Left = 24
      Top = 63
    end
    object Label2: TLabel [53]
      Left = 24
      Top = 116
      Width = 54
      Height = 13
      Caption = 'From Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object lblPeriod: TLabel [54]
      Left = 328
      Top = 120
      Width = 40
      Height = 13
      Caption = 'lblPeriod'
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus [55]
      ColCount = 132
      TabOrder = 24
    end
    object DateTo: TDateTimePicker [56]
      Left = 342
      Top = 88
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 13
    end
    inherited CmbPlusShiftTo: TComboBoxPlus [57]
      ColCount = 138
      TabOrder = 22
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus [58]
      Left = 128
      Top = 7
      ColCount = 107
    end
    inherited CmbPlusPlantTo: TComboBoxPlus [59]
      Top = 7
      ColCount = 108
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus [60]
      Left = 128
      Top = 62
      ColCount = 138
      TabOrder = 29
    end
    inherited CmbPlusTeamTo: TComboBoxPlus [61]
      Top = 62
      ColCount = 139
      TabOrder = 26
    end
    inherited CheckBoxAllTeams: TCheckBox [62]
      Top = 65
      TabOrder = 25
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus [63]
      ColCount = 133
      TabOrder = 23
    end
    inherited CheckBoxAllDepartments: TCheckBox [64]
      TabOrder = 10
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit [65]
      Left = 127
      Top = 34
      TabOrder = 2
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit [66]
      Left = 341
      Top = 34
      TabOrder = 3
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus [67]
      ColCount = 137
      TabOrder = 30
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 34
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 21
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 35
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 36
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 149
      TabOrder = 31
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 150
      TabOrder = 32
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 150
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 151
      TabOrder = 33
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 157
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 156
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      StoredValues = 4
    end
    object dxSpinEditYear: TdxSpinEdit
      Left = 128
      Top = 88
      Width = 73
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Style.BorderStyle = xbs3D
      TabOrder = 4
      OnChange = dxSpinEditYearChange
      MaxValue = 2099
      MinValue = 1950
      Value = 1990
      StoredValues = 48
    end
    object GroupBox1: TGroupBox
      Left = 344
      Top = 143
      Width = 233
      Height = 122
      Caption = 'Selections'
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      object CheckBoxShowTotal: TCheckBox
        Left = 8
        Top = 58
        Width = 201
        Height = 17
        Caption = 'Show grand total '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 37
        Width = 185
        Height = 17
        Caption = 'Show selections'
        TabOrder = 1
      end
      object CheckBoxShowOnlyDeviations: TCheckBox
        Left = 8
        Top = 16
        Width = 209
        Height = 17
        Caption = 'Show only employee with deviations'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 100
        Width = 161
        Height = 17
        Caption = 'Export'
        TabOrder = 4
      end
      object CheckBoxSuppressZeroes: TCheckBox
        Left = 8
        Top = 79
        Width = 217
        Height = 17
        Caption = 'Suppress zeroes'
        TabOrder = 3
      end
    end
    object RadioGroupStatusEmpl: TRadioGroup
      Left = 192
      Top = 143
      Width = 145
      Height = 122
      Caption = 'Status Employee'
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Active'
        'Inactive'
        'All')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 8
    end
    object RadioGroupCompareWith: TRadioGroup
      Left = 8
      Top = 143
      Width = 177
      Height = 122
      Caption = 'Compare with'
      Ctl3D = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Contract hours'
        'Planning hours'
        'Available hours')
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
      OnClick = RadioGroupCompareWithClick
    end
    object DateFrom: TDateTimePicker
      Left = 128
      Top = 88
      Width = 180
      Height = 21
      CalAlignment = dtaLeft
      Date = 0.544151967595099
      Time = 0.544151967595099
      DateFormat = dfShort
      DateMode = dmComboBox
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnShadow
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Kind = dtkDate
      ParseInput = False
      ParentFont = False
      TabOrder = 11
    end
    object dxSpinEditWeekFrom: TdxSpinEdit
      Left = 128
      Top = 116
      Width = 73
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnChange = dxSpinEditWeekFromChange
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditWeekTo: TdxSpinEdit
      Left = 238
      Top = 116
      Width = 73
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnChange = dxSpinEditWeekToChange
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
  end
  inherited stbarBase: TStatusBar
    Top = 331
    Width = 594
  end
  inherited pnlBottom: TPanel
    Top = 350
    Width = 594
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    Left = 48
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 584
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        49040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507294469616C6F675265706F7274436F6D70486F757273462E
        44617461536F75726365456D706C46726F6D104F7074696F6E73437573746F6D
        697A650B0E6564676F42616E644D6F76696E670E6564676F42616E6453697A69
        6E67106564676F436F6C756D6E4D6F76696E67106564676F436F6C756D6E5369
        7A696E670E6564676F46756C6C53697A696E6700094F7074696F6E7344420B10
        6564676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D65
        64676F43616E496E73657274116564676F43616E4E617669676174696F6E1165
        64676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F
        726473106564676F557365426F6F6B6D61726B7300000F546478444247726964
        436F6C756D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D
        62657206536F7274656407046373557005576964746802410942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060F454D504C4F
        5945455F4E554D42455200000F546478444247726964436F6C756D6E0F436F6C
        756D6E53686F72744E616D650743617074696F6E060A53686F7274206E616D65
        05576964746802540942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060A53484F52545F4E414D4500000F5464784442477269
        64436F6C756D6E0A436F6C756D6E4E616D650743617074696F6E06044E616D65
        05576964746803B4000942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060B4445534352495054494F4E00000F546478444247
        726964436F6C756D6E0D436F6C756D6E416464726573730743617074696F6E06
        074164647265737305576964746802450942616E64496E646578020008526F77
        496E6465780200094669656C644E616D6506074144445245535300000F546478
        444247726964436F6C756D6E0E436F6C756D6E44657074436F64650743617074
        696F6E060F4465706172746D656E7420636F646505576964746802580942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060F44
        45504152544D454E545F434F444500000F546478444247726964436F6C756D6E
        0A436F6C756D6E5465616D0743617074696F6E06095465616D20636F64650942
        616E64496E646578020008526F77496E6465780200094669656C644E616D6506
        095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        10040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F7572636507274469616C6F675265706F72
        74436F6D70486F757273462E44617461536F75726365456D706C546F104F7074
        696F6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564
        676F42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564
        676F436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F
        7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43
        616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E61
        7669676174696F6E116564676F436F6E6669726D44656C657465126564676F4C
        6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B730000
        0F546478444247726964436F6C756D6E0A436F6C756D6E456D706C0743617074
        696F6E06064E756D62657206536F727465640704637355700557696474680231
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F454D504C4F5945455F4E554D42455200000F546478444247726964436F
        6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A5368
        6F7274206E616D65055769647468024E0942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F
        546478444247726964436F6C756D6E11436F6C756D6E4465736372697074696F
        6E0743617074696F6E06044E616D6505576964746803CC000942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060B4445534352
        495054494F4E00000F546478444247726964436F6C756D6E0D436F6C756D6E41
        6464726573730743617074696F6E060741646472657373055769647468026509
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        06074144445245535300000F546478444247726964436F6C756D6E0E436F6C75
        6D6E44657074436F64650743617074696F6E060F4465706172746D656E742063
        6F64650942616E64496E646578020008526F77496E6465780200094669656C64
        4E616D65060F4445504152544D454E545F434F444500000F5464784442477269
        64436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E06095465616D
        20636F64650942616E64496E646578020008526F77496E646578020009466965
        6C644E616D6506095445414D5F434F4445000000}
    end
  end
  object QueryEmpl: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  E.EMPLOYEE_NUMBER, E.DESCRIPTION'
      'FROM '
      '  EMPLOYEE E'
      'ORDER BY '
      '  E.EMPLOYEE_NUMBER')
    Left = 104
    Top = 23
  end
end
