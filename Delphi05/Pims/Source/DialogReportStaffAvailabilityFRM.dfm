inherited DialogReportStaffAvailabilityF: TDialogReportStaffAvailabilityF
  Left = 281
  Top = 123
  Caption = 'Report staff availability'
  ClientHeight = 490
  ClientWidth = 614
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlImageBase: TPanel
    Width = 614
    TabOrder = 0
    inherited imgOrbit: TImage
      Left = 497
    end
  end
  inherited pnlInsertBase: TPanel
    Width = 614
    Height = 369
    TabOrder = 3
    inherited LblFromPlant: TLabel
      Top = 16
    end
    inherited LblPlant: TLabel
      Top = 16
    end
    inherited LblFromEmployee: TLabel
      Left = 40
      Top = 385
      Visible = False
    end
    inherited LblEmployee: TLabel
      Left = 72
      Top = 384
      Visible = False
    end
    inherited LblToPlant: TLabel
      Top = 16
    end
    inherited LblToEmployee: TLabel
      Left = 323
      Top = 401
      Visible = False
    end
    object Label3: TLabel [6]
      Left = 296
      Top = 146
      Width = 27
      Height = 13
      Caption = 'Week'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited LblStarEmployeeFrom: TLabel
      Left = 120
      Top = 49
    end
    inherited LblStarEmployeeTo: TLabel
      Left = 336
      Top = 49
    end
    object Label5: TLabel [9]
      Left = 128
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label6: TLabel [10]
      Left = 352
      Top = 382
      Width = 6
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label1: TLabel [11]
      Left = 8
      Top = 146
      Width = 22
      Height = 13
      Caption = 'Year'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel [12]
      Left = 8
      Top = 46
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel [13]
      Left = 40
      Top = 46
      Width = 57
      Height = 13
      Caption = 'Department'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label12: TLabel [14]
      Left = 16
      Top = 429
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label13: TLabel [15]
      Left = 48
      Top = 429
      Width = 26
      Height = 13
      Caption = 'Team'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label14: TLabel [16]
      Left = 323
      Top = 429
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label17: TLabel [17]
      Left = 315
      Top = 380
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label19: TLabel [18]
      Left = 120
      Top = 113
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel [19]
      Left = 336
      Top = 113
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel [20]
      Left = 344
      Top = 430
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel [21]
      Left = 8
      Top = 112
      Width = 24
      Height = 13
      Caption = 'From'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [22]
      Left = 315
      Top = 112
      Width = 10
      Height = 13
      Caption = 'to'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel [23]
      Left = 40
      Top = 112
      Width = 22
      Height = 13
      Caption = 'Shift'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel [24]
      Left = 120
      Top = 48
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    object Label9: TLabel [25]
      Left = 336
      Top = 48
      Width = 7
      Height = 13
      Caption = '*'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited LblFromDepartment: TLabel
      Top = 45
    end
    inherited LblDepartment: TLabel
      Top = 45
    end
    inherited LblStarDepartmentFrom: TLabel
      Left = 120
    end
    inherited LblStarDepartmentTo: TLabel
      Left = 336
    end
    inherited LblStarTeamTo: TLabel
      Left = 336
      Top = 76
    end
    inherited LblToTeam: TLabel
      Top = 76
    end
    inherited LblStarTeamFrom: TLabel
      Left = 120
      Top = 76
    end
    inherited LblTeam: TLabel
      Top = 75
    end
    inherited LblFromTeam: TLabel
      Top = 75
    end
    object GroupBoxShow: TGroupBox [66]
      Left = 8
      Top = 208
      Width = 137
      Height = 157
      Caption = 'Show'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 14
      object CheckBoxPlant: TCheckBox
        Left = 8
        Top = 16
        Width = 89
        Height = 17
        Caption = 'Plant'
        TabOrder = 0
        OnClick = CheckBoxPlantClick
      end
      object CheckBoxTeam: TCheckBox
        Left = 8
        Top = 130
        Width = 73
        Height = 17
        Caption = 'Team'
        TabOrder = 2
        OnClick = CheckBoxTeamClick
      end
      object CheckBoxDept: TCheckBox
        Left = 8
        Top = 71
        Width = 89
        Height = 17
        Caption = 'Department'
        TabOrder = 1
        OnClick = CheckBoxDeptClick
      end
    end
    object dxSpinEditWeek: TdxSpinEdit [67]
      Left = 336
      Top = 144
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 12
      OnChange = ChangeDate
      MaxValue = 53
      MinValue = 1
      Value = 1
      StoredValues = 48
    end
    object dxSpinEditYear: TdxSpinEdit [68]
      Left = 120
      Top = 144
      Width = 65
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      OnChange = ChangeDate
      MaxValue = 2099
      MinValue = 1950
      Value = 1950
      StoredValues = 48
    end
    object RadioGroupSort: TRadioGroup [69]
      Left = 152
      Top = 208
      Width = 121
      Height = 157
      Caption = 'Sort on'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Items.Strings = (
        'Number'
        'Name')
      ParentFont = False
      TabOrder = 15
    end
    object ComboBoxPlusShiftFrom: TComboBoxPlus [70]
      Left = 120
      Top = 104
      Width = 180
      Height = 19
      ColCount = 143
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 9
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftFromCloseUp
    end
    object ComboBoxPlusShiftTo: TComboBoxPlus [71]
      Left = 334
      Top = 104
      Width = 180
      Height = 19
      ColCount = 143
      Ctl3D = False
      DefaultRowHeight = 19
      DropDownAlign = Left
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ListColor = clWindow
      ListDefaultDrawing = True
      ListFont.Charset = ANSI_CHARSET
      ListFont.Color = clWindowText
      ListFont.Height = -11
      ListFont.Name = 'Tahoma'
      ListFont.Style = []
      ListCursor = crDefault
      ButtonCursor = crDefault
      Options = [loColLines, loThumbTracking]
      Style = csIncSearch
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 10
      TitleColor = clBtnFace
      OnCloseUp = ComboBoxPlusShiftToCloseUp
    end
    object CheckBoxAllTeam: TCheckBox [72]
      Left = 528
      Top = 428
      Width = 97
      Height = 17
      Caption = 'All'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 35
      Visible = False
      OnClick = CheckBoxAllTeamClick
    end
    object CheckBoxPaidBreaks: TCheckBox [73]
      Left = 120
      Top = 176
      Width = 121
      Height = 17
      Caption = 'Include paid breaks'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 13
    end
    object GroupBoxSelection: TGroupBox [74]
      Left = 280
      Top = 208
      Width = 321
      Height = 157
      Caption = 'Selections'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 16
      object CheckBoxShowSelection: TCheckBox
        Left = 8
        Top = 16
        Width = 97
        Height = 17
        Caption = 'Show selections'
        TabOrder = 0
      end
      object CheckBoxPagePlant: TCheckBox
        Left = 8
        Top = 35
        Width = 153
        Height = 17
        Caption = 'New page per plant'
        TabOrder = 1
      end
      object CheckBoxPageDept: TCheckBox
        Left = 8
        Top = 55
        Width = 169
        Height = 17
        Caption = 'New page per department'
        TabOrder = 2
      end
      object CheckBoxPageTeam: TCheckBox
        Left = 8
        Top = 74
        Width = 169
        Height = 17
        Caption = 'New page per team'
        TabOrder = 3
      end
      object CheckBoxShowTotals: TCheckBox
        Left = 8
        Top = 93
        Width = 97
        Height = 17
        Caption = 'Show Totals'
        TabOrder = 4
      end
      object CheckBoxExport: TCheckBox
        Left = 8
        Top = 132
        Width = 153
        Height = 17
        Caption = 'Export'
        TabOrder = 6
      end
      object CheckBoxShowOnlyEmpAbsence: TCheckBox
        Left = 8
        Top = 113
        Width = 297
        Height = 17
        Caption = 'Show only empl. with planned absence'
        TabOrder = 5
      end
    end
    object CheckBoxAllDepartment: TCheckBox [75]
      Left = 520
      Top = 46
      Width = 97
      Height = 17
      Caption = 'All'
      TabOrder = 36
      Visible = False
    end
    inherited CmbPlusPlantFrom: TComboBoxPlus
      ColCount = 140
    end
    inherited CmbPlusPlantTo: TComboBoxPlus
      Left = 334
      ColCount = 141
    end
    inherited CmbPlusTeamFrom: TComboBoxPlus
      Top = 73
      ColCount = 136
      TabOrder = 5
    end
    inherited CmbPlusTeamTo: TComboBoxPlus
      Left = 334
      Top = 73
      ColCount = 137
      TabOrder = 6
    end
    inherited CheckBoxAllTeams: TCheckBox
      Left = 520
      Top = 76
      TabOrder = 7
    end
    inherited CmbPlusDepartmentFrom: TComboBoxPlus
      Top = 43
      ColCount = 137
    end
    inherited CmbPlusDepartmentTo: TComboBoxPlus
      Left = 334
      Top = 43
      ColCount = 138
    end
    inherited CheckBoxAllDepartments: TCheckBox
      Left = 520
      Top = 46
      TabOrder = 4
    end
    inherited dxDBExtLookupEditEmplFrom: TdxDBExtLookupEdit
      Left = 128
      Top = 394
      TabOrder = 34
      Visible = False
      Height = 19
    end
    inherited dxDBExtLookupEditEmplTo: TdxDBExtLookupEdit
      Left = 350
      Top = 394
      TabOrder = 32
      Visible = False
      Height = 19
    end
    inherited CmbPlusShiftFrom: TComboBoxPlus
      ColCount = 135
    end
    inherited CmbPlusShiftTo: TComboBoxPlus
      ColCount = 136
      TabOrder = 17
    end
    inherited CheckBoxAllShifts: TCheckBox
      TabOrder = 29
    end
    inherited CheckBoxIncludeNotConnectedEmp: TCheckBox
      TabOrder = 33
    end
    inherited CheckBoxAllEmployees: TCheckBox
      TabOrder = 30
    end
    inherited CheckBoxAllPlants: TCheckBox
      TabOrder = 31
    end
    inherited CmbPlusPlant2From: TComboBoxPlus
      ColCount = 146
      TabOrder = 18
    end
    inherited CmbPlusPlant2To: TComboBoxPlus
      ColCount = 147
      TabOrder = 19
    end
    inherited CmbPlusWorkspotFrom: TComboBoxPlus
      ColCount = 147
      TabOrder = 37
    end
    inherited CmbPlusWorkspotTo: TComboBoxPlus
      ColCount = 148
      TabOrder = 38
    end
    inherited EditWorkspots: TEdit
      TabOrder = 20
    end
    inherited CmbPlusJobTo: TComboBoxPlus
      ColCount = 154
      TabOrder = 22
    end
    inherited CmbPlusJobFrom: TComboBoxPlus
      ColCount = 153
      TabOrder = 21
    end
    inherited dxTimeEditFrom: TdxTimeEdit
      TabOrder = 24
      StoredValues = 4
    end
    inherited dxTimeEditTo: TdxTimeEdit
      TabOrder = 26
      StoredValues = 4
    end
    inherited DatePickerFrom: TDateTimePicker
      TabOrder = 23
    end
    inherited DatePickerTo: TDateTimePicker
      TabOrder = 25
    end
  end
  inherited stbarBase: TStatusBar
    Top = 430
    Width = 614
  end
  inherited pnlBottom: TPanel
    Top = 449
    Width = 614
    inherited btnOk: TBitBtn
      Left = 192
    end
    inherited btnCancel: TBitBtn
      Left = 306
    end
  end
  inherited dxBarManBase: TdxBarManager
    Bars = <
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'MainMenu'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 0
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        IsMainMenu = True
        ItemLinks = <
          item
            Item = dxBarSubItemFile
            UserGlyph.Data = {00000000}
            Visible = True
          end
          item
            Item = dxBarSubItemHelp
            UserGlyph.Data = {00000000}
            Visible = True
          end>
        Name = 'MainMenu'
        OneOnRow = True
        Row = 0
        UseOwnFont = False
        Visible = True
        WholeRow = True
      end
      item
        AllowCustomizing = False
        AllowQuickCustomizing = False
        Caption = 'Grid Functions'
        DockedDockingStyle = dsTop
        DockedLeft = 0
        DockedTop = 26
        DockingStyle = dsTop
        FloatLeft = 396
        FloatTop = 286
        FloatClientWidth = 23
        FloatClientHeight = 22
        ItemLinks = <>
        Name = 'Grid Functions'
        OneOnRow = True
        Row = 1
        UseOwnFont = False
        Visible = False
        WholeRow = True
      end>
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      24
      0)
    inherited dxBarSubItemFile: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonExit
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
    inherited dxBarSubItemHelp: TdxBarSubItem
      ItemLinks = <
        item
          Item = dxBarButtonHelpContents
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonHelpIndex
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpABSHome
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          BeginGroup = True
          Item = dxBarButtonHelpAbout
          UserGlyph.Data = {00000000}
          Visible = True
        end
        item
          Item = dxBarButtonOrderInfo
          UserGlyph.Data = {00000000}
          Visible = True
        end>
    end
  end
  inherited tblPlant: TTable
    IndexFieldNames = 'PLANT_CODE'
    Left = 24
  end
  inherited QueryEmplFrom: TQuery
    Left = 224
  end
  inherited DataSourceEmplFrom: TDataSource
    Left = 256
  end
  inherited dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 568
    inherited dxDBGridLayoutListEmployeeFrom: TdxDBGridLayout
      Data = {
        51040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656505576964746803F40100000D44656661756C74
        4C61796F7574081348656164657250616E656C526F77436F756E740201084B65
        794669656C64060F454D504C4F5945455F4E554D4245520D53756D6D61727947
        726F7570730E001053756D6D617279536570617261746F7206022C200A446174
        61536F7572636507314469616C6F675265706F72745374616666417661696C61
        62696C697479462E44617461536F75726365456D706C46726F6D104F7074696F
        6E73437573746F6D697A650B0E6564676F42616E644D6F76696E670E6564676F
        42616E6453697A696E67106564676F436F6C756D6E4D6F76696E67106564676F
        436F6C756D6E53697A696E670E6564676F46756C6C53697A696E6700094F7074
        696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E
        44656C6574650D6564676F43616E496E73657274116564676F43616E4E617669
        676174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F61
        64416C6C5265636F726473106564676F557365426F6F6B6D61726B7300000F54
        6478444247726964436F6C756D6E0C436F6C756D6E4E756D6265720743617074
        696F6E06064E756D62657206536F727465640704637355700557696474680241
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060F454D504C4F5945455F4E554D42455200000F546478444247726964436F
        6C756D6E0F436F6C756D6E53686F72744E616D650743617074696F6E060A5368
        6F7274206E616D6505576964746802540942616E64496E646578020008526F77
        496E6465780200094669656C644E616D65060A53484F52545F4E414D4500000F
        546478444247726964436F6C756D6E0A436F6C756D6E4E616D65074361707469
        6F6E06044E616D6505576964746803B4000942616E64496E646578020008526F
        77496E6465780200094669656C644E616D65060B4445534352495054494F4E00
        000F546478444247726964436F6C756D6E0D436F6C756D6E4164647265737307
        43617074696F6E06074164647265737305576964746802450942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D6506074144445245
        535300000F546478444247726964436F6C756D6E0E436F6C756D6E4465707443
        6F64650743617074696F6E060F4465706172746D656E7420636F646505576964
        746802580942616E64496E646578020008526F77496E6465780200094669656C
        644E616D65060F4445504152544D454E545F434F444500000F54647844424772
        6964436F6C756D6E0A436F6C756D6E5465616D0743617074696F6E0609546561
        6D20636F64650942616E64496E646578020008526F77496E6465780200094669
        656C644E616D6506095445414D5F434F4445000000}
    end
    inherited dxDBGridLayoutListEmployeeTo: TdxDBGridLayout
      Data = {
        18040000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060F454D504C4F5945455F4E554D
        4245520D53756D6D61727947726F7570730E001053756D6D6172795365706172
        61746F7206022C200A44617461536F75726365072F4469616C6F675265706F72
        745374616666417661696C6162696C697479462E44617461536F75726365456D
        706C546F104F7074696F6E73437573746F6D697A650B0E6564676F42616E644D
        6F76696E670E6564676F42616E6453697A696E67106564676F436F6C756D6E4D
        6F76696E67106564676F436F6C756D6E53697A696E670E6564676F46756C6C53
        697A696E6700094F7074696F6E7344420B106564676F43616E63656C4F6E4578
        69740D6564676F43616E44656C6574650D6564676F43616E496E736572741165
        64676F43616E4E617669676174696F6E116564676F436F6E6669726D44656C65
        7465126564676F4C6F6164416C6C5265636F726473106564676F557365426F6F
        6B6D61726B7300000F546478444247726964436F6C756D6E0A436F6C756D6E45
        6D706C0743617074696F6E06064E756D62657206536F72746564070463735570
        05576964746802310942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060F454D504C4F5945455F4E554D42455200000F546478
        444247726964436F6C756D6E0F436F6C756D6E53686F72744E616D6507436170
        74696F6E060A53686F7274206E616D65055769647468024E0942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060A53484F5254
        5F4E414D4500000F546478444247726964436F6C756D6E11436F6C756D6E4465
        736372697074696F6E0743617074696F6E06044E616D6505576964746803CC00
        0942616E64496E646578020008526F77496E6465780200094669656C644E616D
        65060B4445534352495054494F4E00000F546478444247726964436F6C756D6E
        0D436F6C756D6E416464726573730743617074696F6E06074164647265737305
        576964746802650942616E64496E646578020008526F77496E64657802000946
        69656C644E616D6506074144445245535300000F546478444247726964436F6C
        756D6E0E436F6C756D6E44657074436F64650743617074696F6E060F44657061
        72746D656E7420636F64650942616E64496E646578020008526F77496E646578
        0200094669656C644E616D65060F4445504152544D454E545F434F444500000F
        546478444247726964436F6C756D6E0A436F6C756D6E5465616D074361707469
        6F6E06095465616D20636F64650942616E64496E646578020008526F77496E64
        65780200094669656C644E616D6506095445414D5F434F4445000000}
    end
  end
  inherited DataSourceEmplTo: TDataSource
    Left = 388
  end
  inherited QueryEmplTo: TQuery
    Left = 352
  end
  inherited qryDept: TQuery
    Left = 80
  end
  object QueryShift: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_NUMBER,'
      '  S.PLANT_CODE,'
      '  S.DESCRIPTION'
      'FROM '
      '  SHIFT S'
      'WHERE'
      '  S.PLANT_CODE = :PLANT_CODE'
      'ORDER BY '
      '  S.SHIFT_NUMBER')
    Left = 112
    Top = 24
    ParamData = <
      item
        DataType = ftInteger
        Name = 'PLANT_CODE'
        ParamType = ptUnknown
      end>
  end
end
