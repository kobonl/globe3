inherited WorkScheduleDM: TWorkScheduleDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  inherited TableDetail: TTable
    BeforeEdit = TableDetailBeforeEdit
    AfterPost = TableDetailAfterPost
    OnCalcFields = TableDetailCalcFields
    TableName = 'WORKSCHEDULE'
    object TableDetailWORKSCHEDULE_ID: TFloatField
      AutoGenerateValue = arDefault
      FieldName = 'WORKSCHEDULE_ID'
    end
    object TableDetailCODE: TStringField
      FieldName = 'CODE'
      Required = True
      Size = 24
    end
    object TableDetailDESCRIPTION: TStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 120
    end
    object TableDetailREPEATS: TFloatField
      Alignment = taLeftJustify
      FieldName = 'REPEATS'
      Required = True
    end
    object TableDetailSTARTDATE: TDateTimeField
      FieldName = 'STARTDATE'
      Required = True
    end
    object TableDetailCALCREFERENCEWEEK: TStringField
      FieldKind = fkCalculated
      FieldName = 'CALCREFERENCEWEEK'
      Size = 30
      Calculated = True
    end
    object TableDetailCREATIONDATE: TDateTimeField
      FieldName = 'CREATIONDATE'
      Required = True
    end
    object TableDetailMUTATIONDATE: TDateTimeField
      FieldName = 'MUTATIONDATE'
      Required = True
    end
    object TableDetailMUTATOR: TStringField
      FieldName = 'MUTATOR'
      Required = True
      Size = 80
    end
  end
  object qryWorkScheduleLine: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*) MYCOUNT'
      'FROM WORKSCHEDULELINE T'
      'WHERE T.WORKSCHEDULE_ID = :WORKSCHEDULE_ID'
      ' '
      ' ')
    Left = 96
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'WORKSCHEDULE_ID'
        ParamType = ptUnknown
      end>
  end
  object qryWorkScheduleLineInsert: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'INSERT INTO WORKSCHEDULELINE'
      '('
      '  WORKSCHEDULE_ID,'
      '  WORKSCHEDULELINE_ID,'
      '  DAY1_ABSRSN_CODE,'
      '  DAY1_SHIFT_NUMBER,'
      '  DAY1_TYPE,'
      '  DAY1_REPEATS,'
      '  DAY2_ABSRSN_CODE,'
      '  DAY2_SHIFT_NUMBER,'
      '  DAY2_TYPE,'
      '  DAY2_REPEATS,'
      '  DAY3_ABSRSN_CODE,'
      '  DAY3_SHIFT_NUMBER,'
      '  DAY3_TYPE,'
      '  DAY3_REPEATS,'
      '  DAY4_ABSRSN_CODE,'
      '  DAY4_SHIFT_NUMBER,'
      '  DAY4_TYPE,'
      '  DAY4_REPEATS,'
      '  DAY5_ABSRSN_CODE,'
      '  DAY5_SHIFT_NUMBER,'
      '  DAY5_TYPE,'
      '  DAY5_REPEATS,'
      '  DAY6_ABSRSN_CODE,'
      '  DAY6_SHIFT_NUMBER,'
      '  DAY6_TYPE,'
      '  DAY6_REPEATS,'
      '  DAY7_ABSRSN_CODE,'
      '  DAY7_SHIFT_NUMBER,'
      '  DAY7_TYPE,'
      '  DAY7_REPEATS,'
      '  CREATIONDATE,'
      '  MUTATIONDATE,'
      '  MUTATOR'
      ')'
      'VALUES'
      '('
      '  :WORKSCHEDULE_ID,'
      '  SEQ_WORKSCHEDULELINE.NEXTVAL,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  '#39'*'#39','
      '  1,'
      '  1,'
      '  1,'
      '  SYSDATE,'
      '  SYSDATE,'
      '  :MUTATOR'
      ')  '
      ''
      ' '
      ' '
      ' ')
    Left = 96
    Top = 264
    ParamData = <
      item
        DataType = ftInteger
        Name = 'WORKSCHEDULE_ID'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'MUTATOR'
        ParamType = ptUnknown
      end>
  end
  object qryEmployeeContract: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT COUNT(*) MYCOUNT'
      'FROM EMPLOYEECONTRACT '
      'WHERE WORKSCHEDULE_ID = :WORKSCHEDULE_ID')
    Left = 96
    Top = 312
    ParamData = <
      item
        DataType = ftInteger
        Name = 'WORKSCHEDULE_ID'
        ParamType = ptUnknown
      end>
  end
  object qryWorkScheduleLineDelete: TQuery
    DatabaseName = 'Pims'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'DELETE FROM WORKSCHEDULELINE'
      'WHERE WORKSCHEDULE_ID = :WORKSCHEDULE_ID'
      ' '
      ' ')
    Left = 232
    Top = 216
    ParamData = <
      item
        DataType = ftInteger
        Name = 'WORKSCHEDULE_ID'
        ParamType = ptUnknown
      end>
  end
end
