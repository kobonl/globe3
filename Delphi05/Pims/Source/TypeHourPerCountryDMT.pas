(*
  SO: 19-07-2010 Order 550497
  MRA:31-AUG-2010 RV067.MRA.21. Changes linked to order 550497.
  - Made Description mandatory.
  MRA:2-NOV-2012 20013723
  - Addition of 2 fields: EXPORT_OVERTIME_YN and EXPORT_PAYROLL_YN.
*)
unit TypeHourPerCountryDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TTypeHourPerCountryDM = class(TGridBaseDM)
    QueryDetail_: TQuery;
    QueryDetail_HOURTYPE_NUMBER: TIntegerField;
    QueryDetail_DESCRIPTION: TStringField;
    QueryDetail_CREATIONDATE: TDateTimeField;
    QueryDetail_BONUS_PERCENTAGE: TFloatField;
    QueryDetail_OVERTIME_YN: TStringField;
    QueryDetail_MUTATIONDATE: TDateTimeField;
    QueryDetail_MUTATOR: TStringField;
    QueryDetail_COUNT_DAY_YN: TStringField;
    QueryDetail_EXPORT_CODE: TStringField;
    QueryDetail_IGNORE_FOR_OVERTIME_YN: TStringField;
    QueryDetail_MINIMUM_WAGE: TFloatField;
    QueryDetail_WAGE_BONUS_ONLY_YN: TStringField;
    QueryDetail_EXPORTVL_NUMBER: TIntegerField;
    QueryDetail_TIME_FOR_TIME_YN: TStringField;
    QueryDetail_BONUS_IN_MONEY_YN: TStringField;
    QueryDetail_ADV_YN: TStringField;
    QueryDetail_SATURDAY_CREDIT_YN: TStringField;
    QueryDetail_COUNTRY_ID: TIntegerField;
    QueryDetail_HOURTYPE_NUMBER_1: TIntegerField;
    TableMasterCOUNTRY_ID: TFloatField;
    TableMasterEXPORT_TYPE: TStringField;
    TableMasterCODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableDetailCOUNTRY_ID: TIntegerField;
    TableDetailHOURTYPE_NUMBER: TIntegerField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableHourType: TTable;
    TableHourTypeHOURTYPE_NUMBER: TIntegerField;
    TableHourTypeDESCRIPTION: TStringField;
    TableHourTypeBONUS_PERCENTAGE: TFloatField;
    TableHourTypeOVERTIME_YN: TStringField;
    TableHourTypeCOUNT_DAY_YN: TStringField;
    TableHourTypeEXPORT_CODE: TStringField;
    TableHourTypeIGNORE_FOR_OVERTIME_YN: TStringField;
    TableHourTypeMINIMUM_WAGE: TFloatField;
    TableHourTypeWAGE_BONUS_ONLY_YN: TStringField;
    TableHourTypeTIME_FOR_TIME_YN: TStringField;
    TableHourTypeBONUS_IN_MONEY_YN: TStringField;
    TableHourTypeADV_YN: TStringField;
    TableHourTypeSATURDAY_CREDIT_YN: TStringField;
    TableDetailBONUS_PERCENTAGE: TFloatField;
    TableDetailMINIMUM_WAGE: TWordField;
    TableDetailOVERTIME_YN: TStringField;
    TableDetailCOUNT_DAY_YN: TStringField;
    TableDetailIGNORE_FOR_OVERTIME_YN: TStringField;
    TableDetailWAGE_BONUS_ONLY_YN: TStringField;
    TableDetailTIME_FOR_TIME_YN: TStringField;
    TableDetailBONUS_IN_MONEY_YN: TStringField;
    TableDetailADV_YN: TStringField;
    TableDetailSATURDAY_CREDIT_YN: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailEXPORT_CODE: TStringField;
    TableHourTypeTRAVELTIME_YN: TStringField;
    TableDetailTRAVELTIME_YN: TStringField;
    TableDetailEXPORT_OVERTIME_YN: TStringField;
    TableDetailEXPORT_PAYROLL_YN: TStringField;
    TableHourTypeEXPORT_OVERTIME_YN: TStringField;
    TableHourTypeEXPORT_PAYROLL_YN: TStringField;
    TableDetailEXPORT_OVERTIME_YN_HT: TStringField;
    TableDetailEXPORT_PAYROLL_YN_HT: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TypeHourPerCountryDM: TTypeHourPerCountryDM;

implementation

uses SystemDMT;

{$R *.DFM}


procedure TTypeHourPerCountryDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  TableDetail.Open;
end;

end.
