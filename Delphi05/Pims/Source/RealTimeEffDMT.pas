unit RealTimeEffDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, Db, DBTables;

type
  TRealTimeEffDM = class(TDataModule)
    qryMergeWSEffPerMinuteXXX: TQuery;
    qryWSEffPerMinSelect: TQuery;
    qryWSEffPerMinInsert: TQuery;
    qryWSEffPerMinUpdate: TQuery;
    qryCopyThis: TQuery;
    qryCalcEff: TQuery;
    qryRecalcEff: TQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalcEfficiency(APlantCode: String);
    procedure RecalcEfficiency(APlantCode, AWorkspotCode, AJobCode: String;
      AShiftDate: TDateTime; AShiftNumber: Integer);
    procedure MergeWorkspotEffPerMinute(
      AShiftDate: TDateTime; AShiftNumber: Integer; APlantCode, AWorkspotCode,
      AJobCode: String; AIntervalStartTime, AIntervalEndTime: TDateTime;
      AWorkspotSecondsActual, AWorkspotSecondsNorm, AWorkspotQuantity,
      ANormLevel: Double; AStartTimestamp,
      AEndTimestamp: TDateTime);
  end;

var
  RealTimeEffDM: TRealTimeEffDM;

implementation

{$R *.DFM}

uses
  UGlobalFunctions;

{ TRealTimeEffDM }

// PIM-12 This is used when quantities are added via DataCollectionEntry
procedure TRealTimeEffDM.CalcEfficiency(APlantCode: String);
begin
  try
    with qryCalcEff do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ExecSQL;
    end;
  except
    on E:EDatabaseError do
      WErrorLog(E.Message);
    on E:EdbEngineError do
      WErrorLog(E.Message);
    on E:Exception do
      WErrorLog(E.Message);
  end;
end; // CalcEfficiency

procedure TRealTimeEffDM.RecalcEfficiency(APlantCode, AWorkspotCode,
  AJobCode: String; AShiftDate: TDateTime; AShiftNumber: Integer);
begin
  try
    with qryRecalcEff do
    begin
      Close;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
      ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      ExecSQL;
    end;
  except
    on E:EDatabaseError do
      WErrorLog(E.Message);
    on E:EdbEngineError do
      WErrorLog(E.Message);
    on E:Exception do
      WErrorLog(E.Message);
  end;
end; // RecalcEfficiency

procedure TRealTimeEffDM.MergeWorkspotEffPerMinute(AShiftDate: TDateTime;
  AShiftNumber: Integer; APlantCode, AWorkspotCode, AJobCode: String;
  AIntervalStartTime, AIntervalEndTime: TDateTime; AWorkspotSecondsActual,
  AWorkspotSecondsNorm, AWorkspotQuantity, ANormLevel: Double;
  AStartTimestamp, AEndTimestamp: TDateTime);
begin
  try
    with qryWSEffPerMinSelect do
    begin
      Close;
      ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
      ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      ParamByName('INTERVALSTARTTIME').AsDateTime := AIntervalStartTime;
      Open;
      if Eof then
      begin
        // Insert
        with qryWSEffPerMinInsert do
        begin
          Close;
          ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
          ParamByName('JOB_CODE').AsString := AJobCode;
          ParamByName('INTERVALSTARTTIME').AsDateTime := AIntervalStartTime;
          ParamByName('INTERVALENDTIME').AsDateTime := AIntervalEndTime;
          ParamByName('WORKSPOTSECONDSACTUAL').AsFloat := AWorkspotSecondsActual;
          ParamByName('WORKSPOTSECONDSNORM').AsFloat := AWorkspotSecondsNorm;
          ParamByName('WORKSPOTQUANTITY').AsFloat := AWorkspotQuantity;
          ParamByName('NORMLEVEL').AsFloat := ANormLevel;
          ParamByName('START_TIMESTAMP').AsDateTime := AStartTimestamp;
          ParamByName('END_TIMESTAMP').AsDateTime := AEndTimestamp;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end;
      end
      else
      begin
        // Update:
        // Important: Here the quantity is not added but replaced by the new value.
        with qryWSEffPerMinUpdate do
        begin
          Close;
          ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
          ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
          ParamByName('PLANT_CODE').AsString := APlantCode;
          ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
          ParamByName('JOB_CODE').AsString := AJobCode;
          ParamByName('INTERVALSTARTTIME').AsDateTime := AIntervalStartTime;
          ParamByName('WORKSPOTQUANTITY').AsFloat := AWorkspotQuantity;
          ParamByName('NORMLEVEL').AsFloat := ANormLevel;
          ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
          ExecSQL;
        end;
      end;
      Close;
    end;
    CalcEfficiency(APlantCode);
    RecalcEfficiency(APlantCode, AWorkspotCode, AJobCode, AShiftDate, AShiftNumber);
{
    with qryMergeWSEffPerMinute do
    begin
      Close;
      ParamByName('SHIFT_DATE').AsDateTime := AShiftDate;
      ParamByName('SHIFT_NUMBER').AsInteger := AShiftNumber;
      ParamByName('PLANT_CODE').AsString := APlantCode;
      ParamByName('WORKSPOT_CODE').AsString := AWorkspotCode;
      ParamByName('JOB_CODE').AsString := AJobCode;
      ParamByName('INTERVALSTARTTIME').AsFloat := AIntervalStartTime;
      ParamByName('INTERVALENDTIME').AsFloat := AIntervalEndTime;
      ParamByName('WORKSPOTSECONDSACTUAL').AsFloat := AWorkspotSecondsActual;
      ParamByName('WORKSPOTSECONDSNORM').AsFloat := AWorkspotSecondsNorm;
      ParamByName('WORKSPOTQUANTITY').AsFloat := AWorkspotQuantity;
      ParamByName('NORMLEVEL').AsFloat := ANormLevel;
      ParamByName('START_TIMESTAMP').AsFloat := AStartTimestamp;
      ParamByName('END_TIMESTAMP').AsFloat := AEndTimestamp;
      ParamByName('MUTATOR').AsString := SystemDM.CurrentProgramUser;
      ExecSQL;
    end;
}
  except
    on E:EDatabaseError do
      WErrorLog(E.Message);
    on E:EdbEngineError do
      WErrorLog(E.Message);
    on E:Exception do
      WErrorLog(E.Message);
  end;
end; // MergeWorkspotEffPerMinute

end.
