(*
  MRA:22-MAR-2016 PIM-152
  - Report Ghost Counts.
*)

unit DialogReportGhostCountsFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, dxLayout, Db, DBTables, ActnList, dxBarDBNav, dxBar,
  StdCtrls, Buttons, ComCtrls, dxExEdtr, dxEdLib, dxCntner, dxEditor,
  dxExGrEd, dxExELib, Dblup1a, ExtCtrls, SystemDMT;

type
  TDialogReportGhostCountsF = class(TDialogReportBaseF)
    GroupBox1: TGroupBox;
    CBoxShowSelection: TCheckBox;
    CheckBoxExport: TCheckBox;
    CBoxOnlyShowGhostCounts: TCheckBox;
    CBoxShowCompareJobs: TCheckBox;
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }
  end;

var
  DialogReportGhostCountsF: TDialogReportGhostCountsF;

function DialogReportGhostCountsForm: TDialogReportGhostCountsF;

implementation

{$R *.DFM}

uses
  ReportGhostCountsDMT, ReportGhostCountsQRPT;

var
  DialogReportGhostCountsF_HND: TDialogReportGhostCountsF;

function DialogReportGhostCountsForm: TDialogReportGhostCountsF;
begin
  if (DialogReportGhostCountsF_HND = nil) then
  begin
    DialogReportGhostCountsF_HND := TDialogReportGhostCountsF.Create(Application);
    with DialogReportGhostCountsF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportGhostCountsF_HND;
end;

procedure TDialogReportGhostCountsF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportGhostCountsF_HND <> nil) then
  begin
    DialogReportGhostCountsF_HND := nil;
  end;
end;

procedure TDialogReportGhostCountsF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportGhostCountsF.btnOkClick(Sender: TObject);
begin
  inherited;
  if ReportGhostCountsQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(CmbPlusShiftFrom.Value),
      GetStrValue(CmbPlusShiftTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkspotFrom.Value),
      GetStrValue(CmbPlusWorkspotTo.Value),
      Trunc(DatePickerFromBase.Date),
      Trunc(DatePickerToBase.Date),
      CheckBoxAllShifts.Checked,
      CheckBoxAllDepartments.Checked,
      CBoxShowSelection.Checked,
      CheckBoxExport.Checked,
      CBoxOnlyShowGhostCounts.Checked,
      CBoxShowCompareJobs.Checked
      )
  then
    ReportGhostCountsQR.ProcessRecords;
end;

procedure TDialogReportGhostCountsF.FormShow(Sender: TObject);
begin
  if SystemDM.WorkspotInclSel then
    EnableWorkspotIncludeForThisReport := True;
  InitDialog(True, True, False, False, True, True, False);
  UseDate := True;
  inherited;
end;

procedure TDialogReportGhostCountsF.FormCreate(Sender: TObject);
begin
  inherited;
  ReportGhostCountsDM := CreateReportDM(TReportGhostCountsDM);
  ReportGhostCountsQR := CreateReportQR(TReportGhostCountsQR);
end;

procedure TDialogReportGhostCountsF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportGhostCountsF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

end.
