(*
  MR: 17-02-2005 Order 550381
    Changes to the 2 main-queries for grids; sort on start-date in
    'desc'-order, so latest record will be shown at the top.
  MRA:30-OCT-2012 20013489 Overnight-Shift
  - Addition of field SHIFT_DATE to PQ-record.
  MRA:4-DEC-2012 20013489 Overnight-Shift - Rework
  - In Manual-tab it showed automatic-pq-records when 'by date' was selected.
    - Reason: Extra brackets were needed in queries.
  MRA:19-MAY-2014 20015349
  - Addition of field Shift in form + grid.
  MRA:19-MAY-2014 TD-24048
  - Details section does not match the selected item when Data Collection
    Entry dialog is sorted different than default
  - This is solved by using a calculated field that defines the primary key.
    This calculated field must then be used as KEYFIELD in grid.
  MRA:25-APR-2016 PIM-12
  - Store qty also in BI-table
*)

unit DataCollectionEntryDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SystemDMT, GridBaseDMT, Db, DBTables, Provider, DBClient;

type
  TDataFilter = record
    StartDate, EndDate: TDateTime;
  end;

type
  TDataCollectionEntryDM = class(TGridBaseDM)
    DataSourceWorkspotManual: TDataSource;
    TableJobcodeManual: TTable;
    DataSourceJobcodeManual: TDataSource;
    DataSourceWorkspotAuto: TDataSource;
    TableJobcodeAuto: TTable;
    DataSourceJobcodeAuto: TDataSource;
    TableJobcodeAutoPLANT_CODE: TStringField;
    TableJobcodeAutoWORKSPOT_CODE: TStringField;
    TableJobcodeAutoJOB_CODE: TStringField;
    TableJobcodeAutoBUSINESSUNIT_CODE: TStringField;
    TableJobcodeAutoDESCRIPTION: TStringField;
    TableJobcodeAutoNORM_PROD_LEVEL: TFloatField;
    TableJobcodeAutoBONUS_LEVEL: TFloatField;
    TableJobcodeAutoNORM_OUTPUT_LEVEL: TFloatField;
    TableJobcodeAutoINTERFACE_CODE: TStringField;
    TableJobcodeAutoDATE_INACTIVE: TDateTimeField;
    TableJobcodeAutoCREATIONDATE: TDateTimeField;
    TableJobcodeAutoMUTATIONDATE: TDateTimeField;
    TableJobcodeAutoMUTATOR: TStringField;
    TableJobcodeManualPLANT_CODE: TStringField;
    TableJobcodeManualWORKSPOT_CODE: TStringField;
    TableJobcodeManualJOB_CODE: TStringField;
    TableJobcodeManualBUSINESSUNIT_CODE: TStringField;
    TableJobcodeManualDESCRIPTION: TStringField;
    TableJobcodeManualNORM_PROD_LEVEL: TFloatField;
    TableJobcodeManualBONUS_LEVEL: TFloatField;
    TableJobcodeManualNORM_OUTPUT_LEVEL: TFloatField;
    TableJobcodeManualINTERFACE_CODE: TStringField;
    TableJobcodeManualDATE_INACTIVE: TDateTimeField;
    TableJobcodeManualCREATIONDATE: TDateTimeField;
    TableJobcodeManualMUTATIONDATE: TDateTimeField;
    TableJobcodeManualMUTATOR: TStringField;
    QueryDeptPlant: TQuery;
    QueryWKPlant: TQuery;
    qryPQManual: TQuery;
    dsQryPQManual: TDataSource;
    qryPQAuto: TQuery;
    dsQryPQAuto: TDataSource;
    qryPQManualPLANT_CODE: TStringField;
    qryPQManualWORKSPOT_CODE: TStringField;
    qryPQManualJOB_CODE: TStringField;
    qryPQManualSTART_DATE: TDateTimeField;
    qryPQManualEND_DATE: TDateTimeField;
    qryPQManualQUANTITY: TFloatField;
    qryPQManualCOUNTER_VALUE: TFloatField;
    qryPQManualAUTOMATIC_QUANTITY: TFloatField;
    qryPQManualWORKSPOT_CODELU: TStringField;
    qryPQAutoPLANT_CODE: TStringField;
    qryPQAutoWORKSPOT_CODE: TStringField;
    qryPQAutoJOB_CODE: TStringField;
    qryPQAutoSTART_DATE: TDateTimeField;
    qryPQAutoEND_DATE: TDateTimeField;
    qryPQAutoQUANTITY: TFloatField;
    qryPQAutoCOUNTER_VALUE: TFloatField;
    qryPQAutoAUTOMATIC_QUANTITY: TFloatField;
    qryPQAutoWORKSPOT_CODELU: TStringField;
    qryPQManualWORKSPOT_DESCRIPTIONLU: TStringField;
    qryPQAutoWORKSPOT_DESCRIPTIONLU: TStringField;
    qryPQManualAUTOMATIC_DATACOL_YNLU: TStringField;
    qryPQManualUSE_JOBCODE_YNLU: TStringField;
    qryPQManualMEASURE_PRODUCTIVITY_YNLU: TStringField;
    qryPQAutoAUTOMATIC_DATACOL_YNLU: TStringField;
    qryPQAutoUSE_JOBCODE_YNLU: TStringField;
    qryPQAutoMEASURE_PRODUCTIVITY_YNLU: TStringField;
    qryPQAutoSHIFT_NUMBER: TIntegerField;
    qryPQManualSHIFT_NUMBER: TIntegerField;
    qryPQManualMANUAL_YN: TStringField;
    qryPQAutoMANUAL_YN: TStringField;
    qryPQManualCREATIONDATE: TDateTimeField;
    qryPQManualMUTATIONDATE: TDateTimeField;
    qryPQManualMUTATOR: TStringField;
    qryPQAutoCREATIONDATE: TDateTimeField;
    qryPQAutoMUTATIONDATE: TDateTimeField;
    qryPQAutoMUTATOR: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    QueryWKManual: TQuery;
    QueryWKAuto: TQuery;
    ClientDataSetWKManual: TClientDataSet;
    DataSetProviderWKManual: TDataSetProvider;
    ClientDataSetWKAuto: TClientDataSet;
    DataSetProviderWKAuto: TDataSetProvider;
    QueryJOB: TQuery;
    ClientDataSetJob: TClientDataSet;
    DataSetProviderJob: TDataSetProvider;
    qryPQManualJOBCODELU: TStringField;
    qryPQAutoJOBCODELU: TStringField;
    qryPQManualJOBDESC_CAL: TStringField;
    qryPQAutoJOBDESC_CAL: TStringField;
    TableWorkspotManual: TTable;
    TableWorkspotManualPLANT_CODE: TStringField;
    TableWorkspotManualDESCRIPTION: TStringField;
    TableWorkspotManualWORKSPOT_CODE: TStringField;
    TableWorkspotManualCREATIONDATE: TDateTimeField;
    TableWorkspotManualDEPARTMENT_CODE: TStringField;
    TableWorkspotManualMUTATIONDATE: TDateTimeField;
    TableWorkspotManualMUTATOR: TStringField;
    TableWorkspotManualUSE_JOBCODE_YN: TStringField;
    TableWorkspotManualHOURTYPE_NUMBER: TIntegerField;
    TableWorkspotManualMEASURE_PRODUCTIVITY_YN: TStringField;
    TableWorkspotManualPRODUCTIVE_HOUR_YN: TStringField;
    TableWorkspotManualQUANT_PIECE_YN: TStringField;
    TableWorkspotManualAUTOMATIC_DATACOL_YN: TStringField;
    TableWorkspotManualCOUNTER_VALUE_YN: TStringField;
    TableWorkspotManualENTER_COUNTER_AT_SCAN_YN: TStringField;
    TableWorkspotManualDATE_INACTIVE: TDateTimeField;
    dsWorkspotManual: TDataSource;
    TableWorkspotAuto: TTable;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    DateTimeField1: TDateTimeField;
    StringField4: TStringField;
    DateTimeField2: TDateTimeField;
    StringField5: TStringField;
    StringField6: TStringField;
    IntegerField1: TIntegerField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    StringField11: TStringField;
    StringField12: TStringField;
    DateTimeField3: TDateTimeField;
    dsWorkspotAuto: TDataSource;
    qryPQManualSHIFT_DATE: TDateTimeField;
    qryPQAutoSHIFT_DATE: TDateTimeField;
    TableShiftManual: TTable;
    DataSourceShiftManual: TDataSource;
    TableShiftManualSHIFT_NUMBER: TIntegerField;
    TableShiftManualPLANT_CODE: TStringField;
    TableShiftManualDESCRIPTION: TStringField;
    qryPQManualSHIFT_DESC_LU: TStringField;
    TableShiftManualLU: TTable;
    TableShiftManualLUSHIFT_NUMBER: TIntegerField;
    TableShiftManualLUPLANT_CODE: TStringField;
    TableShiftManualLUDESCRIPTION: TStringField;
    TableShiftAuto: TTable;
    DataSourceShiftAuto: TDataSource;
    TableShiftAutoLU: TTable;
    TableShiftAutoSHIFT_NUMBER: TIntegerField;
    TableShiftAutoPLANT_CODE: TStringField;
    TableShiftAutoDESCRIPTION: TStringField;
    TableShiftAutoLUSHIFT_NUMBER: TIntegerField;
    TableShiftAutoLUPLANT_CODE: TStringField;
    TableShiftAutoLUDESCRIPTION: TStringField;
    qryPQAutoSHIFT_DESC_LU: TStringField;
    qryPQManualMYROWID_CAL: TStringField;
    qryPQAutoMYROWID_CAL: TStringField;
    procedure qryPQManualNewRecord(DataSet: TDataSet);
    procedure qryPQAutoNewRecord(DataSet: TDataSet);
    procedure qryPQManualAfterPost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure qryPQAutoAfterPost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure qryPQManualCalcFields(DataSet: TDataSet);
    procedure qryPQAutoCalcFields(DataSet: TDataSet);
    procedure qryPQManualWORKSPOT_CODEChange(Sender: TField);
    procedure qryPQAutoWORKSPOT_CODEChange(Sender: TField);
  private
    { Private declarations }
    FScanFilter: TDataFilter;
    InsertMode: Boolean;
    procedure SetFilter(Value: TDataFilter);
    procedure RefreshNow(DataSet: TDataSet);
    function SearchJob(DataSet: TDataSet): String;
    function FindJobNorm(DataSet: TDataSet): Double;
  public
    { Public declarations }

    procedure FilterAutoRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure FilterManualRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure RestoreError(ADataSet: TDataSet);
    property ScanFilter: TDataFilter read FScanFilter write SetFilter;
  end;

var
  DataCollectionEntryDM: TDataCollectionEntryDM;

implementation

uses UPimsConst, RealTimeEffDMT, UGlobalFunctions;

{$R *.DFM}

procedure TDataCollectionEntryDM.SetFilter(Value: TDataFilter);
begin
  FScanFilter := Value;
end;

procedure TDataCollectionEntryDM.RestoreError(ADataSet: TDataSet);
begin
  ADataSet.Cancel;
end;

procedure TDataCollectionEntryDM.qryPQManualNewRecord(DataSet: TDataSet);
begin
  inherited;
  InsertMode := True;
  SystemDM.DefaultNewRecord(DataSet);

  qryPQManualPLANT_CODE.Value := TableMasterPLANT_CODE.Value;
  qryPQManualSTART_DATE.Value := FScanFilter.StartDate;
  qryPQManualEND_DATE.Value := FScanFilter.EndDate;
  qryPQManualJOB_CODE.Value := '';
  qryPQManualSHIFT_NUMBER.Value := 1;
  qryPQManualMANUAL_YN.Value := 'Y';
  qryPQManualQUANTITY.Value := 0;
  qryPQManualAUTOMATIC_QUANTITY.Value := 0;
  qryPQManualCOUNTER_VALUE.Value := 0;
  qryPQManualSHIFT_DATE.Value := Trunc(FScanFilter.StartDate); // 20013489
end;

procedure TDataCollectionEntryDM.qryPQAutoNewRecord(DataSet: TDataSet);
begin
  inherited;
  InsertMode := True;
  SystemDM.DefaultNewRecord(DataSet);

  qryPQAutoPLANT_CODE.Value := TableMasterPLANT_CODE.Value;
  qryPQAutoSTART_DATE.Value := FScanFilter.StartDate;
  qryPQAutoEND_DATE.Value := FScanFilter.EndDate;
  qryPQAutoJOB_CODE.Value := '';
  qryPQAutoSHIFT_NUMBER.Value := 1;
  qryPQAutoMANUAL_YN.Value := 'N';
  qryPQAutoQUANTITY.Value := 0;
  qryPQAutoAUTOMATIC_QUANTITY.Value := 0;
  qryPQAutoCOUNTER_VALUE.Value := 0;
  qryPQAutoSHIFT_DATE.Value := Trunc(FScanFilter.StartDate); // 20013489
end;

procedure TDataCollectionEntryDM.RefreshNow(DataSet: TDataSet);
begin
  // Refresh dataset
  if InsertMode then
  begin
    DataSet.Close;
    DataSet.Open;
  end;
  InsertMode := False;
end;

procedure TDataCollectionEntryDM.qryPQManualAfterPost(DataSet: TDataSet);
var
  EndTime, StartTime: TDateTime;
  NormLevel, SecondsNorm: Double;
begin
  inherited;
  // PIM-12
  try
    try
      EndTime := RoundTime(qryPQManualEND_DATE.AsDateTime, 1);
      StartTime := EndTime - 1/24/60; // 1 minute earlier
      NormLevel := FindJobNorm(qryPQManual);
      SecondsNorm := 0;
      if NormLevel > 0 then
        SecondsNorm := qryPQManualQUANTITY.AsFloat * 3600 / NormLevel;
      RealTimeEffDM.MergeWorkspotEffPerMinute(
        qryPQManualSHIFT_DATE.AsDateTime,
        qryPQManualSHIFT_NUMBER.AsInteger,
        qryPQManualPLANT_CODE.AsString,
        qryPQManualWORKSPOT_CODE.AsString,
        qryPQManualJOB_CODE.AsString,
        StartTime,
        EndTime,
        0,
        SecondsNorm,
        qryPQManualQUANTITY.AsFloat,
        NormLevel,
        StartTime,
        EndTime
        );
    except
      on E:EdbEngineError do
        WErrorLog(E.Message);
      on E:Exception do
        WErrorLog(E.Message);
    end;
  finally
    RefreshNow(DataSet);
  end;
end;

procedure TDataCollectionEntryDM.qryPQAutoAfterPost(DataSet: TDataSet);
var
  EndTime, StartTime: TDateTime;
  NormLevel, SecondsNorm: Double;
begin
  inherited;
  // PIM-12
  try
    try
      EndTime := RoundTime(qryPQAutoEND_DATE.AsDateTime, 1);
      StartTime := EndTime - 1/24/60; // 1 minute earlier
      NormLevel := FindJobNorm(qryPQAuto);
      SecondsNorm := 0;
      if NormLevel > 0 then
        SecondsNorm := qryPQAutoQUANTITY.AsFloat * 3600 / NormLevel;
      RealTimeEffDM.MergeWorkspotEffPerMinute(
        qryPQAutoSHIFT_DATE.AsDateTime,
        qryPQAutoSHIFT_NUMBER.AsInteger,
        qryPQAutoPLANT_CODE.AsString,
        qryPQAutoWORKSPOT_CODE.AsString,
        qryPQAutoJOB_CODE.AsString,
        StartTime,
        EndTime,
        0,
        SecondsNorm,
        qryPQAutoQUANTITY.AsFloat,
        NormLevel,
        StartTime,
        EndTime
        );
    except
      on E:EdbEngineError do
        WErrorLog(E.Message);
      on E:Exception do
        WErrorLog(E.Message);
    end;
  finally
    RefreshNow(DataSet);
  end;
end;

procedure TDataCollectionEntryDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  InsertMode := False;
  // Car: 550279
  // Car - 6/1/2003 - delete tables TableProductionQuantityManual and
  // TableProductionQuantityAuto because they are not used
  // and they were replaced by qryPQManual and qryPQAuto

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    QueryWKManual.SQL.Clear;
    QueryWKManual.SQL.Add(
      'SELECT '+ NL +
      '  W.WORKSPOT_CODE, W.DESCRIPTION, ' + NL +
      '  W.AUTOMATIC_DATACOL_YN, W.USE_JOBCODE_YN, ' + NL +
      '  W.MEASURE_PRODUCTIVITY_YN  ' + NL +
      'FROM ' + NL +
      '  WORKSPOT W ' + NL +
      'WHERE ' + NL +
      '  W.PLANT_CODE = :PLANT_CODE AND ' + NL +
      '  W.AUTOMATIC_DATACOL_YN = ''N'' AND ' + NL +
      '  W.USE_JOBCODE_YN = ''Y'' AND ' + NL +
      '  W.MEASURE_PRODUCTIVITY_YN = ''Y'' AND ' + NL +
      '  W.DEPARTMENT_CODE IN ' + NL +
      '  (' + NL +
      '  SELECT ' +  NL +
      '    T.DEPARTMENT_CODE ' + NL +
      '  FROM ' + NL +
      '    DEPARTMENTPERTEAM T, ' + NL +
      '    TEAMPERUSER U ' + NL +
      '  WHERE ' + NL +
      '    U.USER_NAME = :USER_NAME AND '+ NL +
      '    T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ' + NL +
      '    U.TEAM_CODE = T.TEAM_CODE AND ' + NL +
      '    T.PLANT_CODE = W.PLANT_CODE ' + NL +
      '  ) ' +  NL +
      'ORDER BY ' +  NL +
      '  W.WORKSPOT_CODE ');
    QueryWKManual.Prepare;
    QueryWKManual.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryWKManual.ParamByName('PLANT_CODE').AsString :=
      TableMaster.FieldByName('PLANT_CODE').AsString;

    QueryWKAuto.SQL.Clear;
    QueryWKAuto.SQL.Add(
      'SELECT ' + NL +
      '  W.WORKSPOT_CODE, W.DESCRIPTION, ' + NL +
      '  W.AUTOMATIC_DATACOL_YN, W.USE_JOBCODE_YN, ' + NL +
      '  W.MEASURE_PRODUCTIVITY_YN ' + NL +
      'FROM ' + NL +
      '  WORKSPOT W ' + NL +
      'WHERE ' + NL +
      '  W.PLANT_CODE = :PLANT_CODE AND ' + NL +
      '  W.AUTOMATIC_DATACOL_YN = ''Y'' AND ' + NL +
      '  W.USE_JOBCODE_YN = ''Y'' AND ' + NL +
      '  W.MEASURE_PRODUCTIVITY_YN = ''Y'' AND ' + NL +
      '  W.DEPARTMENT_CODE IN ' + NL +
      '  (' + NL +
      '  SELECT ' + NL +
      '    T.DEPARTMENT_CODE ' + NL +
      '  FROM ' + NL +
      '    DEPARTMENTPERTEAM T, '+ NL +
      '    TEAMPERUSER U ' + NL +
      '  WHERE ' + NL +
      '    U.USER_NAME = :USER_NAME AND '+ NL +
      '    T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ' + NL +
      '    U.TEAM_CODE = T.TEAM_CODE AND ' + NL +
      '    T.PLANT_CODE = W.PLANT_CODE ' + NL +
      '    ) ' + NL +
      'ORDER BY ' + NL +
      '  W.WORKSPOT_CODE ');
    QueryWKAuto.Prepare;
    QueryWKAuto.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryWKAuto.ParamByName('PLANT_CODE').AsString :=
      TableMaster.FieldByName('PLANT_CODE').AsString;
    //queries used for dialog which add workspots
    QueryWKPlant.SQL.Clear;
    QueryWKPlant.SQL.Add(
      'SELECT ' + NL +
      '  DISTINCT W.PLANT_CODE, W.WORKSPOT_CODE, '+ NL +
      '  W.DESCRIPTION ' + NL +
      'FROM ' + NL +
      '  WORKSPOT W ' + NL +
      'WHERE ' + NL +
      '  W.PLANT_CODE = :PLANT_CODE AND '+ NL +
      '  W.DEPARTMENT_CODE IN ' + NL +
      '  (' + NL +
      '  SELECT ' + NL +
      '    T.DEPARTMENT_CODE ' + NL +
      '  FROM ' + NL +
      '    DEPARTMENTPERTEAM T, '+ NL +
      '    TEAMPERUSER U ' + NL +
      '  WHERE ' + NL +
      '    U.USER_NAME = :USER_NAME AND '+ NL +
      '    T.DEPARTMENT_CODE = W.DEPARTMENT_CODE AND ' + NL +
      '    U.TEAM_CODE = T.TEAM_CODE AND ' + NL +
      '    T.PLANT_CODE = W.PLANT_CODE' + NL +
      '  ) ' + NL +
      'ORDER BY ' + NL +
      '  W.WORKSPOT_CODE ');
    QueryWKPlant.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
    QueryDeptPlant.SQL.Clear;
    QueryDeptPlant.SQL.Add(
      'SELECT ' + NL +
      '  DISTINCT D.PLANT_CODE, D.DEPARTMENT_CODE, '+ NL +
      '  D.DESCRIPTION ' + NL +
      'FROM ' + NL +
      '  DEPARTMENT D ' + NL +
      'WHERE ' + NL +
      '  D.PLANT_CODE = :PLANT_CODE AND ' + NL +
      '  D.DEPARTMENT_CODE IN ' + NL +
      '  (' + NL +
      '  SELECT ' + NL +
      '    T.DEPARTMENT_CODE ' + NL +
      '  FROM ' + NL +
      '    DEPARTMENTPERTEAM T, '+ NL +
      '    TEAMPERUSER U ' + NL +
      '  WHERE ' + NL +
      '    U.USER_NAME = :USER_NAME AND '+ NL +
      '    T.DEPARTMENT_CODE = D.DEPARTMENT_CODE AND ' + NL +
      '    U.TEAM_CODE = T.TEAM_CODE AND ' + NL +
      '    T.PLANT_CODE = D.PLANT_CODE) ' + NL +
      'ORDER BY ' + NL +
      '  D.DEPARTMENT_CODE ');
    QueryDeptPlant.ParamByName('USER_NAME').AsString := SystemDM.UserTeamLoginUser;
  end;

  if SystemDM.UserTeamSelectionYN = CHECKEDVALUE then
  begin
    //add a filter on workspot code to belong only team selection
    // because filter was added for team selection then a speed up is done on
    // grid message "CustomDrawCell" use ClientDataSetJob instead of JobTable...
    qryPQManual.Filtered := True;
    qryPQManual.OnFilterRecord := FilterManualRecord;
    qryPQAuto.Filtered := True;
    qryPQAuto.OnFilterRecord := FilterAutoRecord;
  end;
  //replace table JobCode with a ClientDataSet - 550296 - speed reason
  ClientDataSetJob.Open;
  //550296
  qryPQManual.Prepare;
  qryPQAuto.Prepare;
end;

procedure TDataCollectionEntryDM.FilterManualRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := ClientDataSetWKManual.FindKey([DataSet.
    FieldByName('WORKSPOT_CODE').AsString]);
end;

procedure TDataCollectionEntryDM.FilterAutoRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  Accept := ClientDataSetWKAuto.FindKey([DataSet.
    FieldByName('WORKSPOT_CODE').AsString]);
end;

procedure TDataCollectionEntryDM.DataModuleDestroy(Sender: TObject);
begin
  inherited;
  //Car 550279
  ClientDataSetJob.Close;
  QueryWKAuto.Close;
  QueryWKAuto.Unprepare;
  QueryWKManual.Close;
  QueryWKManual.Unprepare;
  ClientDataSetWKManual.Close;
  ClientDataSetWKAuto.Close;
  //550296
  qryPQManual.Close;
  qryPQManual.UnPrepare;
  qryPQAuto.Close;
  qryPQAuto.UnPrepare;
end;

procedure TDataCollectionEntryDM.qryPQManualCalcFields(DataSet: TDataSet);
begin
  inherited;
  //550296  - add calculate description field instead of calculation on
  // Draw message grid  - for speed reason
  if ClientDataSetJob.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString,
    DataSet.FieldByName('JOB_CODE').AsString]) then
    DataSet.FieldByName('JOBDESC_CAL').AsString :=
      ClientDataSetJob.FieldByName('DESCRIPTION').AsString;
  // TD-24048
  DataSet.FieldByName('MYROWID_CAL').AsString :=
    DataSet.FieldByName('START_DATE').AsString +
    DataSet.FieldByName('PLANT_CODE').AsString +
    DataSet.FieldByName('WORKSPOT_CODE').AsString +
    DataSet.FieldByName('JOB_CODE').AsString +
    DataSet.FieldByName('MANUAL_YN').AsString;
end;

procedure TDataCollectionEntryDM.qryPQAutoCalcFields(DataSet: TDataSet);
begin
  inherited;
  //550296  - add calculate description field instead of calculation on
  // Draw message grid
  if ClientDataSetJob.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString,
    DataSet.FieldByName('JOB_CODE').AsString]) then
    DataSet.FieldByName('JOBDESC_CAL').AsString :=
      ClientDataSetJob.FieldByName('DESCRIPTION').AsString;
  // TD-24048
  DataSet.FieldByName('MYROWID_CAL').AsString :=
    DataSet.FieldByName('START_DATE').AsString +
    DataSet.FieldByName('PLANT_CODE').AsString +
    DataSet.FieldByName('WORKSPOT_CODE').AsString +
    DataSet.FieldByName('JOB_CODE').AsString +
    DataSet.FieldByName('MANUAL_YN').AsString;
end;

function TDataCollectionEntryDM.SearchJob(DataSet: TDataSet): String;
begin
  Result := NullStr;
  // MR: Because combobox for job is not updated during setting of a job
  //     here, following is disabled.
(*
  ClientDataSetJob.Filtered := False;
  ClientDataSetJob.Filter := 'PLANT_CODE = ' +
    '''' + DataSet.FieldByName('PLANT_CODE').AsString + '''' +
    ' AND WORKSPOT_CODE = ' +
    '''' + DataSet.FieldByName('WORKSPOT_CODE').AsString + '''';
  ClientDataSetJob.Filtered := True;
  if (not ClientDataSetJob.IsEmpty) then
    Result := ClientDataSetJob.FieldByName('JOB_CODE').AsString;
  ClientDataSetJob.Filtered := False;
*)
end;

procedure TDataCollectionEntryDM.qryPQManualWORKSPOT_CODEChange(
  Sender: TField);
begin
  inherited;
  if not ClientDataSetJob.IsEmpty then
    qryPQManual.FieldByName('JOB_CODE').AsString := SearchJob(qryPQManual)
  else
    qryPQManual.FieldBYName('JOB_CODE').AsString := DUMMYSTR;
end;

procedure TDataCollectionEntryDM.qryPQAutoWORKSPOT_CODEChange(
  Sender: TField);
begin
  inherited;
  if not ClientDataSetJob.IsEmpty then
    qryPQAuto.FieldByName('JOB_CODE').AsString := SearchJob(qryPQAuto)
  else
    qryPQAuto.FieldBYName('JOB_CODE').AsString := DUMMYSTR;
end;

// PIM-12
function TDataCollectionEntryDM.FindJobNorm(DataSet: TDataSet): Double;
begin
  Result := 0;
  if ClientDataSetJob.FindKey([DataSet.FieldByName('PLANT_CODE').AsString,
    DataSet.FieldByName('WORKSPOT_CODE').AsString,
    DataSet.FieldByName('JOB_CODE').AsString]) then
    Result :=
      ClientDataSetJob.FieldByName('NORM_PROD_LEVEL').AsFloat;
end;

end.
