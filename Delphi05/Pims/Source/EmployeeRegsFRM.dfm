inherited EmployeeRegsF: TEmployeeRegsF
  Left = 319
  Top = 115
  BorderStyle = bsSingle
  Caption = 'Employee Registrations'
  ClientHeight = 545
  ClientWidth = 965
  Constraints.MinHeight = 579
  Constraints.MinWidth = 973
  OldCreateOrder = True
  Position = poMainFormCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl0101: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 545
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnl0201: TPanel
      Left = 0
      Top = 0
      Width = 965
      Height = 218
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object pnl0302: TPanel
        Left = 761
        Top = 0
        Width = 204
        Height = 218
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object gBoxDate: TGroupBox
          Left = 0
          Top = 0
          Width = 204
          Height = 218
          Align = alClient
          Caption = 'Shift Date'
          TabOrder = 0
          object MonthCalendar: TMonthCalendar
            Left = 2
            Top = 15
            Width = 200
            Height = 162
            Align = alTop
            Date = 37960
            TabOrder = 0
            OnClick = MonthCalendarClick
          end
        end
      end
      object pnl0301: TPanel
        Left = 0
        Top = 0
        Width = 761
        Height = 218
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object gBoxEmployee: TGroupBox
          Left = 0
          Top = 0
          Width = 761
          Height = 42
          Align = alTop
          Caption = 'Employee'
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 17
            Width = 46
            Height = 13
            Caption = 'Employee'
          end
          object LabelEmpDesc: TLabel
            Left = 156
            Top = 17
            Width = 68
            Height = 13
            Caption = 'LabelEmpDesc'
          end
          object LabelStart: TLabel
            Left = 399
            Top = 17
            Width = 24
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            Caption = 'Start'
          end
          object btnEmployee: TButton
            Left = 660
            Top = 12
            Width = 94
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Employee'
            Default = True
            TabOrder = 0
            OnClick = btnEmployeeClick
          end
          object dxDBExtLookupEditEmployee: TdxDBExtLookupEdit
            Left = 82
            Top = 13
            Width = 69
            TabOrder = 1
            DataField = 'EMPLOYEE_NUMBER'
            DataSource = EmployeeRegsDM.DataSourceFilterEmployee
            ReadOnly = True
            PopupWidth = 500
            OnCloseUp = EmployeeFilter
            DBGridLayout = dxDBGridLayoutListEmployeeItemEmployee
            StoredValues = 64
          end
          object BitBtnFirst: TBitBtn
            Left = 428
            Top = 9
            Width = 27
            Height = 30
            Anchors = [akTop, akRight]
            TabOrder = 2
            OnClick = BitBtnFirstClick
            Glyph.Data = {
              76060000424D7606000000000000360400002800000018000000180000000100
              0800000000004002000000000000000000000001000000000000000000000034
              190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
              9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
              FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
              CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
              FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
              870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
              7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
              C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
              FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
              570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
              790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
              B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
              FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
              FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
              A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
              C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
              990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
              FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
              BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
              D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
              CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
              FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
              FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
              B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
              FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
              FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
              F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000ACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACAC2B2B2BAC
              ACACACACACACACACACACACACACACACACACACACAC2BCDCDACACACACACAC00ACAC
              ACACACACACACACACACACACAC2BCDCDACACACACAC0000ACACACACACACACACACAC
              ACACACAC2BCDCDACACACAC00C700ACACACACACACACACACACACACACAC2BCDCDAC
              ACAC00C7C700ACACACACACACACACACACACACACAC2BCDCDACAC00C7CDC7000000
              000000000000ACACACACACAC2BCDCDAC00C7CDCDC7C7C7C7C7C7C7C7C700ACAC
              ACACACAC2BCDCDC1D6D5CDCDCDCDCDCDCDCDCDCDC700ACACACACACAC2BCDCDAC
              C1D6D5CDD5D6D5D6D5D5D6D5C700ACACACACACAC2BCDCDACACC1D6D5D500C1C1
              C1C1C1C1C100ACACACACACAC2BCDCDACACACC1D6D500ACACACACACACACACACAC
              ACACACAC2BCDCDACACACACC1D600ACACACACACACACACACACACACACAC2BCDCDAC
              ACACACACC100ACACACACACACACACACACACACACAC2BCDCDACACACACACAC00ACAC
              ACACACACACACACACACACACAC2BCDCDACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
          end
          object BitBtnPrev: TBitBtn
            Left = 455
            Top = 9
            Width = 27
            Height = 30
            Anchors = [akTop, akRight]
            TabOrder = 3
            OnClick = BitBtnPrevClick
            Glyph.Data = {
              76060000424D7606000000000000360400002800000018000000180000000100
              0800000000004002000000000000000000000001000000000000000000000034
              190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
              9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
              FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
              CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
              FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
              870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
              7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
              C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
              FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
              570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
              790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
              B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
              FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
              FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
              A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
              C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
              990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
              FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
              BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
              D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
              CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
              FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
              FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
              B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
              FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
              FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
              F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000ACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC00ACAC
              ACACACACACACACACACACACACACACACACACACACAC0000ACACACACACACACACACAC
              ACACACACACACACACACACAC00C700ACACACACACACACACACACACACACACACACACAC
              ACAC00C7C700ACACACACACACACACACACACACACACACACACACAC00C7CDC7000000
              000000000000ACACACACACACACACACAC00C7CDCDC7C7C7C7C7C7C7C7C700ACAC
              ACACACACACACACC1D6D5CDCDCDCDCDCDCDCDCDCDC700ACACACACACACACACACAC
              C1D6D5CDD5D6D5D6D5D5D6D5C700ACACACACACACACACACACACC1D6D5D500C1C1
              C1C1C1C1C100ACACACACACACACACACACACACC1D6D500ACACACACACACACACACAC
              ACACACACACACACACACACACC1D600ACACACACACACACACACACACACACACACACACAC
              ACACACACC100ACACACACACACACACACACACACACACACACACACACACACACAC00ACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
          end
          object BitBtnNext: TBitBtn
            Left = 482
            Top = 9
            Width = 27
            Height = 30
            Anchors = [akTop, akRight]
            TabOrder = 4
            OnClick = BitBtnNextClick
            Glyph.Data = {
              76060000424D7606000000000000360400002800000018000000180000000100
              0800000000004002000000000000000000000001000000000000000000000034
              190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
              9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
              FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
              CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
              FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
              870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
              7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
              C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
              FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
              570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
              790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
              B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
              FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
              FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
              A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
              C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
              990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
              FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
              BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
              D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
              CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
              FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
              FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
              B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
              FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
              FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
              F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000ACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC00
              ACACACACACACACACACACACACACACACACACACACACACACAC0000ACACACACACACAC
              ACACACACACACACACACACACACACACAC00C100ACACACACACACACACACACACACACAC
              ACACACACACACAC00C7C100ACACACACACACACACACACACAC000000000000000000
              C7CDC100ACACACACACACACACACACACC1C7C7C7C7C7C7C7C7C7CDCDC100ACACAC
              ACACACACACACACC1D6CDCDCDCDCDCDCDCDCDCDCDC100ACACACACACACACACACC1
              D6D6D6D6D6D6D6D6D6CDD5CD00ACACACACACACACACACACC1C1C1C1C1C1C1C1C1
              D6D5CD00ACACACACACACACACACACACACACACACACACACACC1D6CD00ACACACACAC
              ACACACACACACACACACACACACACACACC1CD00ACACACACACACACACACACACACACAC
              ACACACACACACACC100ACACACACACACACACACACACACACACACACACACACACACACC1
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
          end
          object BitBtnLast: TBitBtn
            Left = 509
            Top = 9
            Width = 27
            Height = 30
            Anchors = [akTop, akRight]
            TabOrder = 5
            OnClick = BitBtnLastClick
            Glyph.Data = {
              76060000424D7606000000000000360400002800000018000000180000000100
              0800000000004002000000000000000000000001000000000000000000000034
              190000663100009A490000CC610001FF7A00001B3400000E3400000F6600000F
              9A00000FCC000114FF000035660000296600001C6600001B9A00001ECC000121
              FF0000519A0000419A0000369A00002A9A000029CC00012DFF00006BCC00005B
              CC000051CC000042CC000038CC00013AFF000186FF000179FF00016DFF000160
              FF000153FF000147FF00003434000034260000663D00009A590000CC710001FF
              870000283400333333001E7C4B0008C4620001FF7A0033FF9400004266001E4F
              7C001E387C000824C400011AFF003342FF00005C9A00086AC4000853C400083B
              C400012DFF003351FF00007ACC000186FF00016DFF00015AFF000147FF00335C
              FF000192FF00339EFF00338EFF003384FF003375FF00336BFF00006666000066
              570000664A00009A640000CC7B0001FF9300005B66001E7C7C001E7C620008C4
              790001FF930033FFA400004F66001E667C006666660051AF7E003BF7950067FF
              B000006B9A000881C4005182AF00516BAF003B57F7006776FF000084CC000199
              FF003B9DF7003B86F7003B6EF7006781FF00019FFF0033ADFF0067B6FF0067A7
              FF00679CFF006791FF00009A9A00009A8B00009A7F00009A700000CC8A0001FF
              A00000929A0008C4C40008C4A80008C4910001FFA60033FFAE0000829A0008B0
              C40051AFAF0051AF95003BF7AC0067FFBF0000779A000899C4005199AF009999
              990084E2B10099FFCA000093CC0001B2FF003BB5F70084B5E200849EE20099A8
              FF0001ACFF0033B7FF0067C2FF0099CEFF0099C2FF0099B5FF0000CCCC0000CC
              BD0000CCAE0000CCA30000CC940001FFAD0000C6CC0001FFFF0001FFE60001FF
              D30001FFB90033FFBD0000B7CC0001F1FF003BF7F7003BF7DB003BF7C40067FF
              CA0000ADCC0001D8FF003BE3F70084E2E20084E2C80099FFD600009DCC0001C5
              FF003BCCF70084CDE200CCCCCC00CDFFE50001B8FF0033C6FF0067D1FF0099DB
              FF00CDE7FF00CDDBFF0001FFFF0001FFEC0001FFDF0001FFD30001FFC60001FF
              B90001F8FF0033FFFF0033FFF00033FFE10033FFD60033FFC70001EBFF0033F9
              FF0067FFFF0067FFF00067FFE50067FFD50001DEFF0033EAFF0067F7FF0099FF
              FF0099FFF00099FFE30001D2FF0033E0FF0067E8FF0099F4FF00CDFFFF00CDFF
              F10001C5FF0033D0FF0067DCFF0099E8FF00CDF4FF00FFFFFF00000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000ACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACAC2B2B2BACACACACACACACACACACACACACAC00ACAC
              ACACACACCDCD2BACACACACACACACACACACACACACAC0000ACACACACACCDCD2BAC
              ACACACACACACACACACACACACAC00C100ACACACACCDCD2BACACACACACACACACAC
              ACACACACAC00C7C100ACACACCDCD2BACACACACACAC000000000000000000C7CD
              C100ACACCDCD2BACACACACACACC1C7C7C7C7C7C7C7C7C7CDCDC100ACCDCD2BAC
              ACACACACACC1D6CDCDCDCDCDCDCDCDCDCDCDC100CDCD2BACACACACACACC1D6D6
              D6D6D6D6D6D6D6CDD5CD00ACCDCD2BACACACACACACC1C1C1C1C1C1C1C1C1D6D5
              CD00ACACCDCD2BACACACACACACACACACACACACACACC1D6CD00ACACACCDCD2BAC
              ACACACACACACACACACACACACACC1CD00ACACACACCDCD2BACACACACACACACACAC
              ACACACACACC100ACACACACACCDCD2BACACACACACACACACACACACACACACC1ACAC
              ACACACACCDCD2BACACACACACACACACACACACACACACACACACACACACACCDCD2BAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACACAC
              ACACACACACACACACACACACACACACACACACACACACACACACACACAC}
          end
          object cBoxShowOnlyActive: TCheckBox
            Left = 542
            Top = 16
            Width = 115
            Height = 17
            Anchors = [akRight, akBottom]
            Caption = 'Show Only Active'
            Checked = True
            State = cbChecked
            TabOrder = 6
            OnClick = cBoxShowOnlyActiveClick
          end
        end
        object gBoxPlanningBase: TGroupBox
          Left = 0
          Top = 42
          Width = 761
          Height = 176
          Align = alClient
          Caption = 'Planning'
          TabOrder = 1
          object GroupBox1: TGroupBox
            Left = 2
            Top = 15
            Width = 757
            Height = 42
            Align = alTop
            Caption = 'Shift Schedule'
            TabOrder = 0
            object Label2: TLabel
              Left = 8
              Top = 16
              Width = 22
              Height = 13
              Caption = 'Shift'
            end
            object btnShiftSchedule: TButton
              Left = 656
              Top = 11
              Width = 94
              Height = 25
              Anchors = [akTop, akRight]
              Caption = 'Shift schedule'
              TabOrder = 0
              OnClick = btnShiftScheduleClick
            end
            object edtShift: TEdit
              Left = 79
              Top = 13
              Width = 170
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
              Text = 'edtShift'
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 57
            Width = 209
            Height = 111
            Caption = 'Planning'
            TabOrder = 1
            object dxTLPlanned: TdxTreeList
              Left = 2
              Top = 15
              Width = 205
              Height = 94
              Bands = <
                item
                end>
              DefaultLayout = True
              HeaderPanelRowCount = 1
              Align = alClient
              TabOrder = 0
              Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
              TreeLineColor = clGrayText
              ShowGrid = True
              ShowRoot = False
              object dxTLPlannedColumnTb: TdxTreeListColumn
                Caption = 'Tb'
                Width = 25
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLPlannedColumnPlant: TdxTreeListColumn
                Caption = 'Plant'
                Width = 38
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLPlannedColumnWorkspot: TdxTreeListColumn
                Caption = 'Workspot'
                Width = 138
                BandIndex = 0
                RowIndex = 0
              end
            end
          end
          object GroupBox3: TGroupBox
            Left = 215
            Top = 57
            Width = 204
            Height = 112
            Caption = 'Availability'
            TabOrder = 2
            object dxTLAvailable: TdxTreeList
              Left = 2
              Top = 15
              Width = 200
              Height = 95
              Bands = <
                item
                end>
              DefaultLayout = True
              HeaderPanelRowCount = 1
              Align = alClient
              TabOrder = 0
              Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
              TreeLineColor = clGrayText
              ShowGrid = True
              ShowRoot = False
              object dxTLAvailabilityColumnTb: TdxTreeListColumn
                Caption = 'Tb'
                Width = 26
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLAvailableColumnPlant: TdxTreeListColumn
                Caption = 'Plant'
                Width = 38
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLAvailabilityColumnAvailabilty: TdxTreeListColumn
                Caption = 'Availability'
                Width = 134
                BandIndex = 0
                RowIndex = 0
              end
            end
          end
          object btnPlanning: TButton
            Left = 659
            Top = 62
            Width = 94
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Planning'
            TabOrder = 3
            OnClick = btnPlanningClick
          end
          object btnAvailabilty: TButton
            Left = 659
            Top = 92
            Width = 94
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Availabilty'
            TabOrder = 4
            OnClick = btnAvailabiltyClick
          end
        end
      end
    end
    object pnl0202: TPanel
      Left = 0
      Top = 218
      Width = 965
      Height = 186
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object pnl0304: TPanel
        Left = 0
        Top = 0
        Width = 761
        Height = 186
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object gBoxHours: TGroupBox
          Left = 0
          Top = 0
          Width = 761
          Height = 186
          Align = alClient
          Caption = 'Hours'
          TabOrder = 0
          object Label7: TLabel
            Left = 629
            Top = 132
            Width = 54
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Total hours'
          end
          object Label8: TLabel
            Left = 629
            Top = 152
            Width = 68
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Planned hours'
          end
          object Label12: TLabel
            Left = 629
            Top = 105
            Width = 72
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Contract hours'
          end
          object Label15: TLabel
            Left = 629
            Top = 116
            Width = 44
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'per week'
          end
          object Label16: TLabel
            Left = 629
            Top = 81
            Width = 50
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'Calculated'
          end
          object Label14: TLabel
            Left = 629
            Top = 92
            Width = 32
            Height = 13
            Anchors = [akRight, akBottom]
            Caption = 'breaks'
          end
          object gBoxSalaryHours: TGroupBox
            Left = 6
            Top = 13
            Width = 203
            Height = 166
            Caption = 'Salary Hours'
            TabOrder = 0
            object Label3: TLabel
              Left = 8
              Top = 120
              Width = 86
              Height = 13
              Caption = 'Total salary hours'
            end
            object Label4: TLabel
              Left = 8
              Top = 138
              Width = 100
              Height = 13
              Caption = 'Planned salary hours'
            end
            object dxTLSalaryHours: TdxTreeList
              Left = 2
              Top = 15
              Width = 199
              Height = 97
              Bands = <
                item
                end>
              DefaultLayout = True
              HeaderPanelRowCount = 1
              Align = alTop
              TabOrder = 0
              Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
              TreeLineColor = clGrayText
              ShowGrid = True
              ShowRoot = False
              object dxTLSalaryHoursColumnHourtype: TdxTreeListColumn
                Caption = 'Hourtype'
                Width = 124
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLSalaryHoursColumnHours: TdxTreeListColumn
                Caption = 'Hours'
                Width = 53
                BandIndex = 0
                RowIndex = 0
              end
            end
            object edtTotalSalaryHours: TEdit
              Left = 128
              Top = 115
              Width = 51
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
              Text = 'edtTotalSalaryHours'
            end
            object edtPlannedSalaryHours: TEdit
              Left = 128
              Top = 137
              Width = 51
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
              Text = 'edtPlannedSalaryHours'
            end
          end
          object gBoxAbsenceHours: TGroupBox
            Left = 424
            Top = 14
            Width = 201
            Height = 165
            Caption = 'Absence Hours'
            TabOrder = 1
            object Label5: TLabel
              Left = 8
              Top = 119
              Width = 97
              Height = 13
              Caption = 'Total absence hours'
            end
            object Label6: TLabel
              Left = 8
              Top = 138
              Width = 111
              Height = 13
              Caption = 'Planned absence hours'
            end
            object dxTLAbsenceHours: TdxTreeList
              Left = 2
              Top = 15
              Width = 197
              Height = 97
              Bands = <
                item
                end>
              DefaultLayout = True
              HeaderPanelRowCount = 1
              Align = alTop
              TabOrder = 0
              Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
              TreeLineColor = clGrayText
              ShowGrid = True
              ShowRoot = False
              object dxTLAbsenceHoursColumnAbsenceHours: TdxTreeListColumn
                Caption = 'Absence reason'
                Width = 126
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLAbsenceHoursColumnHours: TdxTreeListColumn
                Caption = 'Hours'
                Width = 48
                BandIndex = 0
                RowIndex = 0
              end
            end
            object edtTotalAbsenceHours: TEdit
              Left = 128
              Top = 115
              Width = 51
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
              Text = 'edtTotalAbsenceHours'
            end
            object edtPlannedAbsenceHours: TEdit
              Left = 128
              Top = 137
              Width = 51
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 2
              Text = 'edtPlannedAbsenceHours'
            end
          end
          object btnHours: TButton
            Left = 658
            Top = 16
            Width = 94
            Height = 25
            Anchors = [akTop, akRight]
            Caption = 'Hours'
            TabOrder = 2
            OnClick = btnHoursClick
          end
          object edtTotalHours: TEdit
            Left = 702
            Top = 129
            Width = 51
            Height = 19
            Anchors = [akRight, akBottom]
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 4
            Text = 'edtTotalAbsenceHours'
          end
          object edtPlannedHours: TEdit
            Left = 702
            Top = 151
            Width = 51
            Height = 19
            Anchors = [akRight, akBottom]
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 5
            Text = 'edtPlannedAbsenceHours'
          end
          object edtContractHoursPerWeek: TEdit
            Left = 702
            Top = 107
            Width = 51
            Height = 19
            Anchors = [akRight, akBottom]
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 3
            Text = 'edtContractHoursPerWeek'
          end
          object gBoxProdHours: TGroupBox
            Left = 216
            Top = 13
            Width = 201
            Height = 165
            Caption = 'Production Hours'
            TabOrder = 6
            object Label13: TLabel
              Left = 8
              Top = 141
              Width = 83
              Height = 13
              Caption = 'Total prod. hours'
            end
            object dxTLProdHours: TdxTreeList
              Left = 2
              Top = 15
              Width = 197
              Height = 97
              Bands = <
                item
                end>
              DefaultLayout = True
              HeaderPanelRowCount = 1
              Align = alTop
              TabOrder = 0
              Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
              TreeLineColor = clGrayText
              ShowGrid = True
              ShowRoot = False
              object dxTLProdHoursColumnWkJob: TdxTreeListColumn
                Caption = 'Workspot / Job'
                Width = 126
                BandIndex = 0
                RowIndex = 0
              end
              object dxTLProdHoursColumnHours: TdxTreeListColumn
                Caption = 'Hours'
                Width = 48
                BandIndex = 0
                RowIndex = 0
              end
            end
            object edtTotalProdHours: TEdit
              Left = 128
              Top = 137
              Width = 51
              Height = 19
              Ctl3D = False
              ParentCtl3D = False
              ReadOnly = True
              TabOrder = 1
              Text = 'edtTotalAbsenceHours'
            end
          end
          object edtBreaks: TEdit
            Left = 702
            Top = 84
            Width = 51
            Height = 19
            Anchors = [akRight, akBottom]
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 7
            Text = 'edtBreaks'
          end
        end
      end
      object pnl0303: TPanel
        Left = 761
        Top = 0
        Width = 204
        Height = 186
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object gBoxProcessPlanned: TGroupBox
          Left = 0
          Top = 0
          Width = 204
          Height = 80
          Align = alTop
          TabOrder = 0
          object btnProcessPlanned: TButton
            Left = 8
            Top = 16
            Width = 188
            Height = 25
            Caption = 'Process planned absence...'
            TabOrder = 0
            OnClick = btnProcessPlannedClick
          end
          object btnCalculateWorktimeReduction: TButton
            Left = 8
            Top = 46
            Width = 188
            Height = 25
            Caption = 'Calculate worktime reduction'
            TabOrder = 1
            OnClick = btnCalculateWorktimeReductionClick
          end
        end
        object gBoxAvailableFreeTime: TGroupBox
          Left = 0
          Top = 80
          Width = 204
          Height = 106
          Align = alClient
          Caption = 'Available free time'
          TabOrder = 1
          object Label9: TLabel
            Left = 8
            Top = 30
            Width = 35
            Height = 13
            Caption = 'Holiday'
          end
          object Label10: TLabel
            Left = 8
            Top = 52
            Width = 93
            Height = 13
            Caption = 'Worktime reduction'
          end
          object Label11: TLabel
            Left = 8
            Top = 75
            Width = 62
            Height = 13
            Caption = 'Time for time'
          end
          object edtHoliday: TEdit
            Left = 131
            Top = 28
            Width = 65
            Height = 19
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 0
            Text = 'edtHoliday'
          end
          object edtWorktimeReduction: TEdit
            Left = 131
            Top = 50
            Width = 65
            Height = 19
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 1
            Text = 'edtWorktimeReduction'
          end
          object edtTimeForTime: TEdit
            Left = 131
            Top = 72
            Width = 65
            Height = 19
            Ctl3D = False
            ParentCtl3D = False
            ReadOnly = True
            TabOrder = 2
            Text = 'edtTimeForTime'
          end
        end
      end
    end
    object pnl0203: TPanel
      Left = 0
      Top = 404
      Width = 965
      Height = 120
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object gBoxScans: TGroupBox
        Left = 0
        Top = 0
        Width = 965
        Height = 120
        Align = alClient
        Caption = 'Scans'
        TabOrder = 0
        object pnlScansClient: TPanel
          Left = 2
          Top = 15
          Width = 856
          Height = 103
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object dxTLScans: TdxTreeList
            Left = 0
            Top = 0
            Width = 856
            Height = 103
            Bands = <
              item
              end>
            DefaultLayout = True
            HeaderPanelRowCount = 1
            Align = alClient
            TabOrder = 0
            Options = [aoColumnSizing, aoColumnMoving, aoEditing, aoTabThrough, aoRowSelect, aoAutoWidth]
            TreeLineColor = clGrayText
            ShowGrid = True
            ShowRoot = False
            object dxTLScansColumnDateTimeIn: TdxTreeListColumn
              Caption = 'Date/Time in'
              Width = 115
              BandIndex = 0
              RowIndex = 0
            end
            object dxTLScansColumnDateTimeOut: TdxTreeListColumn
              Caption = 'Date/Time out'
              Width = 114
              BandIndex = 0
              RowIndex = 0
            end
            object dxTLScansColumnPlant: TdxTreeListColumn
              Caption = 'Plant'
              Width = 121
              BandIndex = 0
              RowIndex = 0
            end
            object dxTLScansColumnWorkspot: TdxTreeListColumn
              Caption = 'Workspot'
              Width = 115
              BandIndex = 0
              RowIndex = 0
            end
            object dxTLScansColumnJob: TdxTreeListColumn
              Caption = 'Job'
              Width = 108
              BandIndex = 0
              RowIndex = 0
            end
            object dxTLScansColumnShift: TdxTreeListColumn
              Caption = 'Shift'
              Width = 92
              BandIndex = 0
              RowIndex = 0
            end
          end
        end
        object pnlScansRight: TPanel
          Left = 858
          Top = 15
          Width = 105
          Height = 103
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object pnlScans2Client: TPanel
            Left = 0
            Top = 0
            Width = 105
            Height = 81
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object btnScans: TButton
              Left = 7
              Top = 1
              Width = 92
              Height = 25
              Caption = 'Scans'
              TabOrder = 0
              OnClick = btnScansClick
            end
          end
          object pnlScans2Bottom: TPanel
            Left = 0
            Top = 81
            Width = 105
            Height = 22
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object cBoxCutOffTime: TCheckBox
              Left = 7
              Top = 0
              Width = 81
              Height = 17
              Caption = 'Cut off time'
              TabOrder = 0
              OnClick = cBoxCutOffTimeClick
            end
          end
        end
      end
    end
    object pnl0204: TPanel
      Left = 0
      Top = 524
      Width = 965
      Height = 21
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
    end
  end
  object dxDBGridLayoutListEmployee: TdxDBGridLayoutList
    Left = 501
    Top = 163
    object dxDBGridLayoutListEmployeeItemEmployee: TdxDBGridLayout
      Data = {
        B9040000545046301054647844424772696457726170706572000542616E6473
        0E0109416C69676E6D656E74070D74614C6566744A7573746966790743617074
        696F6E0608456D706C6F79656500000D44656661756C744C61796F7574081348
        656164657250616E656C526F77436F756E740201084B65794669656C64060F45
        4D504C4F5945455F4E554D4245520D53756D6D61727947726F7570730E001053
        756D6D617279536570617261746F7206022C200E506172656E7453686F774869
        6E74080A44617461536F757263650727456D706C6F79656552656773444D2E44
        617461536F7572636546696C746572456D706C6F7965650F4D6178526F774C69
        6E65436F756E7402010F4F7074696F6E734265686176696F720B0C6564676F41
        75746F536F72740B6564676F45646974696E67136564676F456E74657253686F
        77456469746F72136564676F496D6D656469617465456469746F720E6564676F
        5461625468726F7567680F6564676F566572745468726F75676800094F707469
        6F6E7344420B106564676F43616E63656C4F6E457869740D6564676F43616E44
        656C6574650D6564676F43616E496E73657274116564676F43616E4E61766967
        6174696F6E116564676F436F6E6669726D44656C657465126564676F4C6F6164
        416C6C5265636F726473106564676F557365426F6F6B6D61726B73000B4F7074
        696F6E73566965770B136564676F42616E644865616465725769647468106564
        676F496E7665727453656C6563740D6564676F5573654269746D6170000F5368
        6F775072657669657747726964080D53686F77526F77466F6F74657209125369
        6D706C65437573746F6D697A65426F7809000F546478444247726964436F6C75
        6D6E0C436F6C756D6E4E756D6265720743617074696F6E06064E756D62657205
        576964746802390942616E64496E646578020008526F77496E64657802000946
        69656C644E616D65060F454D504C4F5945455F4E554D42455200000F54647844
        4247726964436F6C756D6E0F436F6C756D6E53686F72744E616D650743617074
        696F6E060A53686F7274204E616D6505576964746802570942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060A53484F52545F
        4E414D4500000F546478444247726964436F6C756D6E0A436F6C756D6E4E616D
        650743617074696F6E06044E616D6505576964746803DE000942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D65060B4445534352
        495054494F4E00000F546478444247726964436F6C756D6E0B436F6C756D6E50
        6C616E740743617074696F6E0605506C616E7405576964746803B8000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060750
        4C414E544C5500000F546478444247726964436F6C756D6E0A436F6C756D6E54
        65616D0743617074696F6E06045465616D0942616E64496E646578020008526F
        77496E6465780200094669656C644E616D6506095445414D5F434F444500000F
        546478444247726964436F6C756D6E0A436F6C756D6E44657074074361707469
        6F6E0604446570740942616E64496E646578020008526F77496E646578020009
        4669656C644E616D65060F4445504152544D454E545F434F4445000000}
    end
  end
end
