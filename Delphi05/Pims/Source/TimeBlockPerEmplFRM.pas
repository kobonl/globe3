(*
  MRA:30-JAN-2013 TD-22045
  - In second grid: Show plant-shift-combination, to make
    it possible to also show timeblocks-per-employee for
    employee where the plant was changed.
  - To make it more user-friendly:
    - Upon opening this dialog, show as selected row in second grid the one that
      is based on current employee+plant-combination.
  MRA:28-MAY-2013 New look Pims
  - Some pictures/colors are changed.
*)
unit TimeBlockPerEmplFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, dxEditor, dxExEdtr, dxEdLib, dxDBELib, StdCtrls,
  Mask, DBCtrls, dxDBTLCl, dxGrClms, CalculateTotalHoursDMT;

type
  TTimeBlockPerEmplF = class(TGridBaseF)
    dxMasterGridColumn1: TdxDBGridColumn;
    dxMasterGridColumn2: TdxDBGridColumn;
    dxMasterGridColumn3: TdxDBGridTimeColumn;
    dxMasterGridColumn4: TdxDBGridTimeColumn;
    dxMasterGridColumn5: TdxDBGridTimeColumn;
    dxMasterGridColumn6: TdxDBGridTimeColumn;
    dxMasterGridColumn7: TdxDBGridTimeColumn;
    dxMasterGridColumn8: TdxDBGridTimeColumn;
    dxMasterGridColumn9: TdxDBGridTimeColumn;
    dxMasterGridColumn10: TdxDBGridTimeColumn;
    dxMasterGridColumn11: TdxDBGridTimeColumn;
    dxMasterGridColumn12: TdxDBGridTimeColumn;
    dxMasterGridColumn13: TdxDBGridTimeColumn;
    dxMasterGridColumn14: TdxDBGridTimeColumn;
    dxMasterGridColumn15: TdxDBGridTimeColumn;
    dxMasterGridColumn16: TdxDBGridTimeColumn;
    dxDetailGridColumn1: TdxDBGridColumn;
    dxDetailGridColumn2: TdxDBGridColumn;
    dxDetailGridColumn3: TdxDBGridTimeColumn;
    dxDetailGridColumn4: TdxDBGridTimeColumn;
    dxDetailGridColumn5: TdxDBGridTimeColumn;
    dxDetailGridColumn6: TdxDBGridTimeColumn;
    dxDetailGridColumn7: TdxDBGridTimeColumn;
    dxDetailGridColumn8: TdxDBGridTimeColumn;
    dxDetailGridColumn9: TdxDBGridTimeColumn;
    dxDetailGridColumn10: TdxDBGridTimeColumn;
    dxDetailGridColumn11: TdxDBGridTimeColumn;
    dxDetailGridColumn12: TdxDBGridTimeColumn;
    dxDetailGridColumn13: TdxDBGridTimeColumn;
    dxDetailGridColumn14: TdxDBGridTimeColumn;
    dxDetailGridColumn15: TdxDBGridTimeColumn;
    dxDetailGridColumn16: TdxDBGridTimeColumn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    LabelMO: TLabel;
    LabelTU: TLabel;
    LabelWE: TLabel;
    LabelTH: TLabel;
    LabelFR: TLabel;
    LabelSA: TLabel;
    LabelSU: TLabel;
    Label1: TLabel;
    dxDBTimeEditST1: TdxDBTimeEdit;
    dxDBTimeEditET1: TdxDBTimeEdit;
    dxDBTimeEditST2: TdxDBTimeEdit;
    dxDBTimeEditET2: TdxDBTimeEdit;
    dxDBTimeEditST3: TdxDBTimeEdit;
    dxDBTimeEditET3: TdxDBTimeEdit;
    dxDBTimeEditST4: TdxDBTimeEdit;
    dxDBTimeEditET4: TdxDBTimeEdit;
    dxDBTimeEditST5: TdxDBTimeEdit;
    dxDBTimeEditET5: TdxDBTimeEdit;
    dxDBTimeEditST6: TdxDBTimeEdit;
    dxDBTimeEditET6: TdxDBTimeEdit;
    dxDBTimeEditST7: TdxDBTimeEdit;
    dxDBTimeEditET7: TdxDBTimeEdit;
    DBEditTimeBlock: TDBEdit;
    DBEditDesc: TDBEdit;
    PanelEmpl: TPanel;
    dxDBGridEmpl: TdxDBGrid;
    dxDBGridEmplColumn4: TdxDBGridLookupColumn;
    dxDBGridEmplColumn5: TdxDBGridColumn;
    dxDBGridEmplColumn6: TdxDBGridColumn;
    dxDBGridEmplColumn7: TdxDBGridColumn;
    dxDBGridColumn3: TdxDBGridColumn;
    dxDBGridEmplColumn8: TdxDBGridColumn;
    Splitter1: TSplitter;
    dxMasterGridColumnPLANT_CODE: TdxDBGridColumn;
    dxMasterGridColumnPLANTLU: TdxDBGridColumn;
    procedure dxBarBDBNavInsertClick(Sender: TObject);
    procedure pnlDetailEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxDBGridEmplClick(Sender: TObject);
    procedure dxMasterGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure FormCreate(Sender: TObject);
    procedure dxDBGridEmplChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxMasterGridClick(Sender: TObject);
    procedure ScrollBarMasterScroll(Sender: TObject;
      ScrollCode: TScrollCode; var ScrollPos: Integer);
    procedure dxBarButtonExportGridClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure LocateRowInMasterGrid;
  public
    { Public declarations }
  end;

function TimeBlockPerEmplF: TTimeBlockPerEmplF;

implementation

{$R *.DFM}
uses
  SystemDMT, TimeBlockPerEmplDMT, UPimsMessageRes;

var
  TimeBlockPerEmplF_HND: TTimeBlockPerEmplF;

function TimeBlockPerEmplF: TTimeBlockPerEmplF;
begin
  if (TimeBlockPerEmplF_HND = nil) then
  begin
    TimeBlockPerEmplF_HND := TTimeBlockPerEmplF.Create(Application);
    TimeBlockPerEmplDM.Handle_Grid := TimeBlockPerEmplF_HND.dxDetailGrid;
  end;
  Result := TimeBlockPerEmplF_HND;
end;

procedure TTimeBlockPerEmplF.dxBarBDBNavInsertClick(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TTimeBlockPerEmplF.pnlDetailEnter(Sender: TObject);
begin
  inherited;
  DBEditDesc.SetFocus;
end;

procedure TTimeBlockPerEmplF.FormShow(Sender: TObject);
var
  Index: Integer;
begin
  inherited;
  LabelMO.Caption := SystemDM.GetDayWDescription(1);
  LabelTU.Caption := SystemDM.GetDayWDescription(2);
  LabelWE.Caption := SystemDM.GetDayWDescription(3);
  LabelTH.Caption := SystemDM.GetDayWDescription(4);
  LabelFR.Caption := SystemDM.GetDayWDescription(5);
  LabelSA.Caption := SystemDM.GetDayWDescription(6);
  LabelSU.Caption := SystemDM.GetDayWDescription(7);
  // TD-22045 Extra band was added for Plant.
  //          Assignment of days is changed!
  for Index := 2 to 8 do
  begin
    dxMasterGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index-1);
  end;
  for Index := 1 to 7 do
  begin
    dxDetailGrid.Bands[Index].Caption := SystemDM.GetDayWDescription(Index);
  end;
  //car 550279
  TimeBlockPerEmplDM.QueryEmpl.First;
  //CAR 17-10-2003 :550262
  if SystemDM.ASaveTimeRecScanning.Employee = -1 then
    SystemDM.ASaveTimeRecScanning.Employee :=
      TimeBlockPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger
  else
    TimeBlockPerEmplDM.QueryEmpl.Locate('EMPLOYEE_NUMBER',
      SystemDM.ASaveTimeRecScanning.Employee, []);
  //
  TimeBlockPerEmplDM.SetEmployee;

  // TD-22045
  LocateRowInMasterGrid;

  dxMasterGrid.SetFocus;
end;

procedure TTimeBlockPerEmplF.FormDestroy(Sender: TObject);
begin
  inherited;
  TimeBlockPerEmplF_HND := nil;
end;

procedure TTimeBlockPerEmplF.dxDBGridEmplClick(Sender: TObject);
begin
  inherited;
  TimeBlockPerEmplDM.SetEmployee;
  LocateRowInMasterGrid; // TD-22045
end;

procedure TTimeBlockPerEmplF.dxMasterGridChangeNode(Sender: TObject;
  OldNode, Node: TdxTreeListNode);
begin
  inherited;
  TimeBlockPerEmplDM.SetEmployee;
end;

procedure TTimeBlockPerEmplF.FormCreate(Sender: TObject);
begin
  TimeBlockPerEmplDM := CreateFormDM(TTimeBlockPerEmplDM);
  if (dxDetailGrid.DataSource = Nil) or (dxMasterGrid.DataSource = Nil) or
    (dxDBGridEmpl.DataSource = Nil) then
  begin
    dxDetailGrid.DataSource := TimeBlockPerEmplDM.DataSourceDetail;
    dxMasterGrid.DataSource := TimeBlockPerEmplDM.DataSourceMaster;
    dxDBGridEmpl.DataSource := TimeBlockPerEmplDM.DataSourceEmpl;
  end;
  inherited;
  // 20014289
  dxDBGridEmpl.BandFont.Color := clWhite;
  dxDBGridEmpl.BandFont.Style := [fsBold];
end;

procedure TTimeBlockPerEmplF.dxDBGridEmplChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  TimeBlockPerEmplDM.SetEmployee;
  LocateRowInMasterGrid; // TD-22045
end;

procedure TTimeBlockPerEmplF.dxMasterGridClick(Sender: TObject);
begin
  inherited;
  TimeBlockPerEmplDM.SetEmployee;
end;

procedure TTimeBlockPerEmplF.ScrollBarMasterScroll(Sender: TObject;
  ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
  inherited;
  dxDBGridEmpl.SetFocus;
end;

procedure TTimeBlockPerEmplF.dxBarButtonExportGridClick(Sender: TObject);
begin
//CAR 27.02.2003
  FloatEmpl := 'EMPLOYEE_NUMBER';
  CreateExportColumns(dxDBGridEmpl, '', 'PLANT_CODE');
  CreateExportColumns(dxDBGridEmpl, '', 'PLANTLU');
  CreateExportColumns(dxDBGridEmpl, '', 'EMPLOYEE_NUMBER');
  CreateExportColumnsDesc(SExportDescEMPL, 'EMPLLU');
  inherited;
end;

procedure TTimeBlockPerEmplF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  AProdMinClass.Refresh;
  //CAR 17-10-2003 : 550262
  SystemDM.ASaveTimeRecScanning.Employee :=
    TimeBlockPerEmplDM.QueryEmpl.FieldByName('EMPLOYEE_NUMBER').AsInteger;
end;

procedure TTimeBlockPerEmplF.FormResize(Sender: TObject);
begin
  inherited;
  //CAR 15-10-2003 - on maximize screen give the same height for both grids
  if TimeBlockPerEmplF.ClientHeight > 600 then
    pnlMasterGrid.Height := 200
  else
    pnlMasterGrid.Height := 120;
end;

// TD-22045 In Second Grid: Locate first record based on current
//          Employee + Plant-combination.
procedure TTimeBlockPerEmplF.LocateRowInMasterGrid;
begin
  try
    TimeBlockPerEmplDM.TableMaster.Locate('PLANT_CODE;SHIFT_NUMBER',
      VarArrayOf([TimeBlockPerEmplDM.QueryEmpl.FieldByName('PLANT_CODE').AsString,
      TimeBlockPerEmplDM.QueryEmpl.FieldByName('SHIFT_NUMBER').AsInteger]),
      []);
  except
  end;
end;

end.
