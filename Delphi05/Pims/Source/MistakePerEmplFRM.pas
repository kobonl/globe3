unit MistakePerEmplFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseFRM, Db, ActnList, dxBarDBNav, dxBar, dxCntner, dxTL, dxDBCtrl,
  dxDBGrid, ExtCtrls, StdCtrls, Dblup1a, Mask, DBCtrls, dxEditor, dxExEdtr,
  dxEdLib, ComCtrls;

type
  TMistakePerEmplF = class(TGridBaseF)
    dxDetailGridColumn1: TdxDBGridColumn;
    pnlTeamPlantShift: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cmbPlusTeam: TComboBoxPlus;
    cmbPlusPlant: TComboBoxPlus;
    CheckBoxAllTeam: TCheckBox;
    dxDetailGridColumn2: TdxDBGridColumn;
    DateFrom: TDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEditEmp: TDBEdit;
    dxDetailGridColumnDate: TdxDBGridColumn;
    dxDetailGridColumnWeek: TdxDBGridColumn;
    DBEditDesd: TDBEdit;
    EditMistake: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cmbPlusTeamChange(Sender: TObject);
    procedure CheckBoxAllTeamClick(Sender: TObject);
   
    procedure EditMistakeKeyPress(Sender: TObject; var Key: Char);
    procedure dxBarBDBNavCancelClick(Sender: TObject);
    procedure dxDetailGridExit(Sender: TObject);
    procedure EditMistakeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dxGridEnter(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);

    procedure FormHide(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);

    procedure cmbPlusPlantChange(Sender: TObject);
    procedure DateFromChange(Sender: TObject);
    procedure dxDetailGridChangeNode(Sender: TObject; OldNode,
      Node: TdxTreeListNode);
    procedure dxBarBDBNavPostClick(Sender: TObject);

  private
    { Private declarations }
    procedure FillPlants;
  public
    { Public declarations }
    procedure EnabledNavButtons(Active: Boolean);
    procedure AskForChanges;
    function ValidMistake(MistakeNr: String): Boolean;
    procedure RefreshGrid;
  end;

function MistakePerEmplF: TMistakePerEmplF;

implementation

{$R *.DFM}

uses
  SystemDMT, ListProcsFRM, UPimsConst, UPimsMessageRes,  MistakePerEmplDMT;

var
  MistakePerEmplF_HDN: TMistakePerEmplF;

function MistakePerEmplF: TMistakePerEmplF;
begin
  if MistakePerEmplF_HDN = nil then
    MistakePerEmplF_HDN := TMistakePerEmplF.Create(Application);
  Result := MistakePerEmplF_HDN;
end;

procedure TMistakePerEmplF.FormCreate(Sender: TObject);

begin
  MistakePerEmplDM := CreateFormDM(TMistakePerEmplDM);
  if dxDetailGrid.DataSource = Nil then
    dxDetailGrid.DataSource := MistakePerEmplDM.DataSourceDetail;
  inherited;
  
end;

procedure TMistakePerEmplF.FormDestroy(Sender: TObject);
begin
  inherited;
  MistakePerEmplF_HDN := nil;
end;

procedure TMistakePerEmplF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if dxBarBDBNavPost.Enabled then
  begin
    DisplayMessage(SUndoChanges, mtInformation, [mbOk]);
    dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end;
  Action := caFree;
end;

procedure TMistakePerEmplF.FillPlants;
begin
  if CheckBoxAllTeam.Checked then
    ListProcsF. FillComboBoxPlant(MistakePerEmplDM.TablePlant, True, CmbPlusPlant)
  else
    ListProcsF.FillComboBox(MistakePerEmplDM.QueryPlantTeam,
      CmbPlusPlant, GetStrValue(CmbPlusTeam.Value), '', True, 'TEAM_CODE', '',
      'PLANT_CODE', 'DESCRIPTION');
  RefreshGrid;
end;

procedure TMistakePerEmplF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;
  cmbPlusTeam.ShowSpeedButton := False;
  cmbPlusTeam.ShowSpeedButton := True;
  cmbPlusPlant.ShowSpeedButton := False;
  cmbPlusPlant.ShowSpeedButton := True;
  ListProcsF.WeekUitDat(Now, Year, Week);
  ListProcsF.FillComboBox(MistakePerEmplDM.TableTeam,
    CmbPlusTeam, '','', True, '','', 'TEAM_CODE', 'DESCRIPTION');
  FillPlants;
  DateFrom.Date := Now;
  ListProcsF.WeekUitDat(DateFrom.Date, Year, Week);
  dxDetailGridColumnDate.Caption := SDate + DateToStr(DateFrom.Date);
  dxDetailGridColumnWeek.Caption := STotalWeek + IntToStr(Week);
  RefreshGrid;
  EditMistake.Text :=
    MistakePerEmplDM.QueryDetail.FieldByName('NUMBER_OF_MISTAKE').AsString;
  dxDetailGrid.SetFocus;
  dxBarBDBNavPost.Enabled := False;
  dxBarBDBNavCancel.Enabled := False;
end;

procedure TMistakePerEmplF.cmbPlusTeamChange(Sender: TObject);
begin
  inherited;
  FillPlants;
end;

procedure TMistakePerEmplF.CheckBoxAllTeamClick(Sender: TObject);
begin
  inherited;
  cmbPlusTeam.Enabled := not CheckBoxAllTeam.Checked;
  FillPlants;
  RefreshGrid;
end;


procedure TMistakePerEmplF.EnabledNavButtons(Active: Boolean);
begin
  dxBarBDBNavPost.Enabled := Active;
  dxBarBDBNavCancel.Enabled := Active;
end;

procedure TMistakePerEmplF.EditMistakeKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  EnabledNavButtons(True);
end;

procedure TMistakePerEmplF.dxBarBDBNavCancelClick(Sender: TObject);
begin
  inherited;
  EnabledNavButtons(False);
  dxDetailGrid.SetFocus;
  EditMistake.Text :=
    MistakePerEmplDM.QueryDetail.FieldByName('NUMBER_OF_MISTAKE').AsString;
end;

procedure TMistakePerEmplF.AskForChanges;
var
  QuestionResult: Integer;
begin
  QuestionResult := 0;
  if dxBarBDBNavPost.Enabled then
     QuestionResult := DisplayMessage(SPimsSaveChanges, mtConfirmation, [mbYes, mbNo]);
  case QuestionResult of
    mrYes: dxBarBDBNavPost.OnClick(dxBarDBNavigator);
    mrNo: dxBarBDBNavCancel.OnClick(dxBarDBNavigator);
  end; { case }
end;

procedure TMistakePerEmplF.dxDetailGridExit(Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

procedure TMistakePerEmplF.EditMistakeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  EnabledNavButtons(True);
end;

procedure TMistakePerEmplF.dxGridEnter(Sender: TObject);
begin
  inherited;
  AskForChanges;
end;

procedure TMistakePerEmplF.Edit1Exit(Sender: TObject);
begin
  inherited;
  if not dxBarBDBNavPost.Enabled then
     Exit;
end;

procedure TMistakePerEmplF.FormHide(Sender: TObject);
begin
   EnabledNavButtons(False);
   inherited;
end;

procedure TMistakePerEmplF.FormDeactivate(Sender: TObject);
begin
  EnabledNavButtons(False);
  inherited;
end;

procedure TMistakePerEmplF.RefreshGrid;
begin
  if not MistakePerEmplDM.QueryDetail.Active then
    MistakePerEmplDM.QueryDetail.Active := True;
  if MistakePerEmplDM <> Nil then
  begin
    MistakePerEmplDM.FillQueryDetail(CheckBoxAllTeam.Checked,
      GetStrValue(cmbPlusTeam.Value), GetStrValue(cmbPlusPlant.Value),
      GetDate(DateFrom.Date));
    MistakePerEmplDM.QueryDetail.First;
    EnabledNavButtons(False);
  end;
end;

procedure TMistakePerEmplF.cmbPlusPlantChange(Sender: TObject);
begin
  inherited;
  RefreshGrid;
end;

procedure TMistakePerEmplF.DateFromChange(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;
  ListProcsF.WeekUitDat(GetDate(DateFrom.Date), Year, Week);
  dxDetailGridColumnDate.Caption := SDate + DateToStr(DateFrom.Date);
  dxDetailGridColumnWeek.Caption := STotalWeek + IntToStr(Week);
  RefreshGrid;
end;

procedure TMistakePerEmplF.dxDetailGridChangeNode(Sender: TObject; OldNode,
  Node: TdxTreeListNode);
begin
  inherited;
  EditMistake.Text :=
    MistakePerEmplDM.QueryDetail.FieldByName('NUMBER_OF_MISTAKE').AsString;
end;

function TMistakePerEmplF.ValidMistake(MistakeNr: String): Boolean;
var
  i: Integer;
begin
  Result := True;
  if MistakeNr = '' then
    Exit;
  for i := 1 to length(MistakeNr) do
    if not ((MistakeNr[i] >= '0') and (MistakeNr[i] <= '9')) then
    begin
      Result := False;
      Exit;
    end;
end;

procedure TMistakePerEmplF.dxBarBDBNavPostClick(Sender: TObject);
var
  SaveEmpl: Integer;
begin
  inherited;
  if ValidMistake(EditMistake.Text) then
  begin
    SaveEmpl :=
      MistakePerEmplDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger;
    MistakePerEmplDM.UpdateTableMPE(
      MistakePerEmplDM.QueryDetail.FieldByName('EMPLOYEE_NUMBER').AsInteger,
      GetDate(DateFrom.Date), EditMistake.Text);
    RefreshGrid;
    MistakePerEmplDM.GotoEmpl(SaveEmpl);
 end
 else
   DisplayMessage(SInvalidMistakeValue, mtInformation, [mbOk]);
end;

end.
