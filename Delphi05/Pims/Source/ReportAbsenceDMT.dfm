inherited ReportAbsenceDM: TReportAbsenceDM
  OldCreateOrder = True
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object QueryAbsence: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryAbsenceFilterRecord
    SQL.Strings = (
      'SELECT '
      '  A.PLANT_CODE, '
      '  R.ABSENCEREASON_CODE, '
      '  A.ABSENCEHOUR_DATE ,'
      '  SUM(A.ABSENCE_MINUTE) AS SUMWK_MIN '
      'FROM '
      '  ABSENCEHOURPEREMPLOYEE A ,'
      '  ABSENCEREASON R'
      'WHERE'
      '  A.ABSENCEREASON_ID = R.ABSENCEREASON_ID AND'
      '  A.ABSENCEHOUR_DATE >= :FSTARTDATE AND'
      '  A.ABSENCEHOUR_DATE <= :FENDDATE '
      'GROUP BY  '
      '  A.PLANT_CODE, '
      '  R.ABSENCEREASON_CODE,'
      '  A.ABSENCEHOUR_DATE ')
    Left = 48
    Top = 16
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FSTARTDATE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FENDDATE'
        ParamType = ptUnknown
      end>
  end
  object DataSourceAbsence: TDataSource
    DataSet = QueryAbsence
    Left = 168
    Top = 16
  end
end
