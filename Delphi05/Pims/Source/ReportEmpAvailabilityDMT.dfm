inherited ReportEmpAvailabilityDM: TReportEmpAvailabilityDM
  OldCreateOrder = True
  Left = 238
  Top = 155
  Height = 479
  Width = 741
  object QueryEMA: TQuery
    DatabaseName = 'PIMS'
    Filtered = True
    SessionName = 'SessionPims'
    OnFilterRecord = QueryEMAFilterRecord
    SQL.Strings = (
      'SELECT   EMA.EMPLOYEEAVAILABILITY_DATE '
      'FROM   EMPLOYEEAVAILABILITY EMA '
      'WHERE'
      '  EMA.EMPLOYEEAVAILABILITY_DATE >= :FDATESTART AND'
      '  EMA.EMPLOYEEAVAILABILITY_DATE <= :FDATEEND ')
    Left = 64
    Top = 24
    ParamData = <
      item
        DataType = ftDateTime
        Name = 'FDATESTART'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'FDATEEND'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetAbsRsn: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderABSRSN'
    Left = 184
    Top = 208
  end
  object DataSetProviderABSRSN: TDataSetProvider
    DataSet = QueryAbs
    Constraints = True
    Left = 312
    Top = 208
  end
  object QueryAbs: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  ABSENCEREASON_CODE, DESCRIPTION, ABSENCETYPE_CODE'
      'FROM '
      '  ABSENCEREASON '
      'ORDER BY '
      '  ABSENCEREASON_CODE')
    Left = 64
    Top = 208
  end
  object ClientDataSetAbsTot: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderAbsTot'
    Left = 184
    Top = 152
  end
  object DataSetProviderAbsTot: TDataSetProvider
    DataSet = QueryAbsTot
    Constraints = True
    Left = 312
    Top = 152
  end
  object QueryAbsTot: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  EMPLOYEE_NUMBER, ABSENCE_YEAR,  '
      '  LAST_YEAR_HOLIDAY_MINUTE, '
      '  NORM_HOLIDAY_MINUTE ,   USED_HOLIDAY_MINUTE,'
      '  LAST_YEAR_WTR_MINUTE,   EARNED_WTR_MINUTE,'
      '  USED_WTR_MINUTE, LAST_YEAR_TFT_MINUTE,'
      '  EARNED_TFT_MINUTE,   USED_TFT_MINUTE,'
      '  ILLNESS_MINUTE '
      'FROM '
      '  ABSENCETOTAL '
      'ORDER BY '
      '  EMPLOYEE_NUMBER, ABSENCE_YEAR')
    Left = 64
    Top = 152
  end
  object QuerySHS: TQuery
    DatabaseName = 'PIMS'
    SessionName = 'SessionPims'
    SQL.Strings = (
      'SELECT '
      '  S.SHIFT_SCHEDULE_DATE,'
      '  S.EMPLOYEE_NUMBER, S.SHIFT_NUMBER '
      'FROM'
      '  SHIFTSCHEDULE S, EMPLOYEEAVAILABILITY E'
      'WHERE'
      '  S.EMPLOYEE_NUMBER = E.EMPLOYEE_NUMBER AND'
      '  S.PLANT_CODE = E.PLANT_CODE AND'
      '  S.SHIFT_SCHEDULE_DATE = E.EMPLOYEEAVAILABILITY_DATE AND '
      '  S.EMPLOYEE_NUMBER  = :EMPLOYEE AND'
      '  S.SHIFT_SCHEDULE_DATE >= :DATEFROM AND '
      '  S.SHIFT_SCHEDULE_DATE <= :DATETO'
      'ORDER BY '
      '  S.SHIFT_SCHEDULE_DATE')
    Left = 64
    Top = 96
    ParamData = <
      item
        DataType = ftInteger
        Name = 'EMPLOYEE'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATEFROM'
        ParamType = ptUnknown
      end
      item
        DataType = ftDateTime
        Name = 'DATETO'
        ParamType = ptUnknown
      end>
  end
  object ClientDataSetSHS: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProviderSHS'
    Left = 184
    Top = 96
  end
  object DataSetProviderSHS: TDataSetProvider
    DataSet = QuerySHS
    Constraints = True
    Left = 304
    Top = 96
  end
end
