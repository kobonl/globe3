(*
  Changes:
    MRA:10-FEB-2011 RV086.1. SC-50014392.
    - Plan in other plants, related issues:
      - When teams-per-users-filtering is used, you still see
        plants/teams/department/workspots not belonging to the
        teams-per-user-selection.
*)
unit BusinessUnitDMT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  GridBaseDMT, Db, DBTables;

type
  TBusinessUnitDM = class(TGridBaseDM)
    TablePlant: TTable;
    DataSourcePlant: TDataSource;
    TableMasterPLANTLU: TStringField;
    TableMasterBUSINESSUNIT_CODE: TStringField;
    TableMasterDESCRIPTION: TStringField;
    TableMasterPLANT_CODE: TStringField;
    TableMasterCREATIONDATE: TDateTimeField;
    TableMasterBONUS_FACTOR: TFloatField;
    TableMasterNORM_ILL_VS_DIRECT_HOURS: TFloatField;
    TableMasterMUTATIONDATE: TDateTimeField;
    TableMasterMUTATOR: TStringField;
    TableMasterNORM_ILL_VS_INDIRECT_HOURS: TFloatField;
    TableMasterNORM_ILL_VS_TOTAL_HOURS: TFloatField;
    TablePlantPLANT_CODE: TStringField;
    TablePlantDESCRIPTION: TStringField;
    TablePlantADDRESS: TStringField;
    TablePlantZIPCODE: TStringField;
    TablePlantCITY: TStringField;
    TablePlantSTATE: TStringField;
    TablePlantPHONE: TStringField;
    TablePlantFAX: TStringField;
    TablePlantCREATIONDATE: TDateTimeField;
    TablePlantINSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantINSCAN_MARGIN_LATE: TIntegerField;
    TablePlantMUTATIONDATE: TDateTimeField;
    TablePlantMUTATOR: TStringField;
    TablePlantOUTSCAN_MARGIN_EARLY: TIntegerField;
    TablePlantOUTSCAN_MARGIN_LATE: TIntegerField;
    TableDetailPLANTLU: TStringField;
    TableDetailBUSINESSUNIT_CODE: TStringField;
    TableDetailDESCRIPTION: TStringField;
    TableDetailPLANT_CODE: TStringField;
    TableDetailCREATIONDATE: TDateTimeField;
    TableDetailBONUS_FACTOR: TFloatField;
    TableDetailNORM_ILL_VS_DIRECT_HOURS: TFloatField;
    TableDetailMUTATIONDATE: TDateTimeField;
    TableDetailMUTATOR: TStringField;
    TableDetailNORM_ILL_VS_INDIRECT_HOURS: TFloatField;
    TableDetailNORM_ILL_VS_TOTAL_HOURS: TFloatField;
    procedure TableMasterNewRecord(DataSet: TDataSet);
    procedure TableDetailBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure TableDetailFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TablePlantFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BusinessUnitDM: TBusinessUnitDM;

implementation

uses SystemDMT;

{$R *.DFM}

procedure TBusinessUnitDM.TableMasterNewRecord(DataSet: TDataSet);
begin
  inherited;
  SystemDM.DefaultNewRecord(DataSet);
  if not TablePlant.IsEmpty then
  begin
    TablePlant.First;
    DataSet.FieldByName('PLANT_CODE').Value :=
      TablePlant.FieldByName('PLANT_CODE').Value;
  end;
end;

procedure TBusinessUnitDM.TableDetailBeforePost(DataSet: TDataSet);
var
  Bonus: String;
begin
  inherited;
  SystemDM.DefaultBeforePost(DataSet);
  Bonus  :=
    Format('%*.*f', [3, 2, TableDetail.FieldByName('BONUS_FACTOR').AsFloat]);
  TableDetail.FieldByName('BONUS_FACTOR').AsFloat := StrToFloat(Bonus);
  Bonus  :=
    Format('%*.*f', [3, 2, TableDetail.FieldByName('NORM_ILL_VS_DIRECT_HOURS').AsFloat]);
  TableDetail.FieldByName('NORM_ILL_VS_DIRECT_HOURS').AsFloat := StrToFloat(Bonus);
  Bonus  :=
    Format('%*.*f', [3, 2, TableDetail.FieldByName('NORM_ILL_VS_INDIRECT_HOURS').AsFloat]);
  TableDetail.FieldByName('NORM_ILL_VS_INDIRECT_HOURS').AsFloat := StrToFloat(Bonus);
  Bonus  :=
    Format('%*.*f', [3, 2, TableDetail.FieldByName('NORM_ILL_VS_TOTAL_HOURS').AsFloat]);
  TableDetail.FieldByName('NORM_ILL_VS_TOTAL_HOURS').AsFloat := StrToFloat(Bonus);
end;

procedure TBusinessUnitDM.DataModuleCreate(Sender: TObject);
begin
  inherited;
  // RV086.1.
  SystemDM.PlantTeamFilterEnable(TableDetail);
  SystemDM.PlantTeamFilterEnable(TablePlant);
end;

procedure TBusinessUnitDM.TableDetailFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.1.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

procedure TBusinessUnitDM.TablePlantFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  inherited;
  // RV086.1.
  Accept := SystemDM.PlantTeamFilter(DataSet);
end;

end.
