(*
  Changes:
    MRA:08-AUG-2008 RV008.
      Show period for week-selection.
    SO: 05-JUL-2010 RV067.3. 550494
      PIMS User rights related issues
    MRA:8-APR-2011 RV089.1. SC-20011561.10 - Remote Apps problem
    - Make it possible to embed this dialog in Pims, instead of
      open it as a modal dialog.
    MRA:19-JUL-2011 RV095.1.
    - Workspot moved to DialogReportBase form.
*)

unit DialogReportDirIndHrsWeekFRM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DialogReportBaseFRM, DBTables, Db, ActnList, dxBarDBNav, dxBar, StdCtrls,
  Buttons, ComCtrls, Dblup1a, ExtCtrls, dxCntner, dxEditor, dxExEdtr,
  dxEdLib, dxDBELib, dxLayout, dxExGrEd, dxExELib;

type
  TDialogReportDirIndHrsWeekF = class(TDialogReportBaseF)
    Label2: TLabel;
    Label3: TLabel;
    GroupBoxSelection: TGroupBox;
    Label4: TLabel;
    CheckBoxShowSelection: TCheckBox;
    Label5: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    dxSpinEditYear: TdxSpinEdit;
    dxSpinEditWeekFrom: TdxSpinEdit;
    Label7: TLabel;
    Label8: TLabel;
    ComboBoxPlusBusinessFrom: TComboBoxPlus;
    Label9: TLabel;
    ComboBoxPlusBusinessTo: TComboBoxPlus;
    Label10: TLabel;
    Label11: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    ComboBoxPlusWorkspotFrom: TComboBoxPlus;
    Label17: TLabel;
    Label18: TLabel;
    ComboBoxPlusWorkSpotTo: TComboBoxPlus;
    QueryBU: TQuery;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    QueryJobCode: TQuery;
    DataSourceWorkSpot: TDataSource;
    dxSpinEditWeekTo: TdxSpinEdit;
    CheckBoxExport: TCheckBox;
    lblPeriod: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FillBusiness;
{    procedure FillWorkSpot; }
//    procedure FillJobCode;
    procedure CmbPlusEmployeeFromChange(Sender: TObject);
    procedure ChangeDate(Sender: TObject);
    procedure ComboBoxPlusBusinessFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusBusinessToCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkspotFromCloseUp(Sender: TObject);
    procedure ComboBoxPlusWorkSpotToCloseUp(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmbPlusWorkspotFromCloseUp(Sender: TObject);
    procedure CmbPlusWorkspotToCloseUp(Sender: TObject);
  private
    procedure ShowPeriod;
  protected
    procedure FillAll; override;
    procedure CreateParams(var params: TCreateParams); override;
  public
    { Public declarations }

  end;

var
  DialogReportDirIndHrsWeekF: TDialogReportDirIndHrsWeekF;

// RV089.1.
function DialogReportDirIndHrsWeekForm: TDialogReportDirIndHrsWeekF;

implementation
//CAR - 550284 - replace message OnChange with OnCloseUp for TComboBoxPlus
{$R *.DFM}
uses
  SystemDMT, ListProcsFRM,
  ReportDirIndHrsWeekQRPT, ReportDirIndHrsWeekDMT, UPimsMessageRes;

// RV089.1.
var
  DialogReportDirIndHrsWeekF_HND: TDialogReportDirIndHrsWeekF;

// RV089.1.
function DialogReportDirIndHrsWeekForm: TDialogReportDirIndHrsWeekF;
begin
  if (DialogReportDirIndHrsWeekF_HND = nil) then
  begin
    DialogReportDirIndHrsWeekF_HND := TDialogReportDirIndHrsWeekF.Create(Application);
    with DialogReportDirIndHrsWeekF_HND do
    begin
      BorderStyle := bsSingle;
      pnlImageBase.Visible := False;
      btnCancel.Enabled := False;
      dxBarSubItemFile.Visible := ivNever;
      dxBarSubItemHelp.Visible := ivNever;
      stBarBase.Visible := False;
      dxBarManBase.Free;
      dxBarDBNavigator.Free;
    end;
  end;
  Result := DialogReportDirIndHrsWeekF_HND;
end;

// RV089.1.
procedure TDialogReportDirIndHrsWeekF.FormDestroy(Sender: TObject);
begin
  inherited;
  if (DialogReportDirIndHrsWeekF_HND <> nil) then
  begin
    DialogReportDirIndHrsWeekF_HND := nil;
  end;
end;

// RV089.1.
procedure TDialogReportDirIndHrsWeekF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TDialogReportDirIndHrsWeekF.btnOkClick(Sender: TObject);
begin
  inherited;
  if dxSpinEditWeekFrom.Value > dxSpinEditWeekTo.Value then
    begin
      DisplayMessage(SPimsWeekStartEnd, mtInformation, [mbYes]);
      Exit;
    end;

  if ReportDirIndHrsWeekQR.QRSendReportParameters(
      GetStrValue(CmbPlusPlantFrom.Value),
      GetStrValue(CmbPlusPlantTo.Value),
      GetStrValue(ComboBoxPlusBusinessFrom.Value),
      GetStrValue(ComboBoxPlusBusinessTo.Value),
      GetStrValue(CmbPlusDepartmentFrom.Value),
      GetStrValue(CmbPlusDepartmentTo.Value),
      GetStrValue(CmbPlusWorkSpotFrom.Value),
      GetStrValue(CmbPlusWorkSpotTo.Value),
      GetStrValue(CmbPlusJobFrom.Value),
      GetStrValue(CmbPlusJobTo.Value),
      //Car 550284
      IntToStr(GetIntValue(dxDBExtLookupEditEmplFrom.Text)),
      IntToStr(GetIntValue(dxDBExtLookupEditEmplTo.Text)),
      Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value),
      Round(dxSpinEditWeekTo.Value),
      CheckBoxShowSelection.Checked,
      CheckBoxExport.Checked)
  then
    ReportDirIndHrsWeekQR.ProcessRecords;
end;

procedure TDialogReportDirIndHrsWeekF.FillBusiness;
begin
  if (CmbPlusPlantFrom.Value <> '') and
    (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value)
  then
  begin
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessFrom,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(QueryBU, ComboBoxPlusBusinessTo,
      GetStrValue(CmbPlusPlantFrom.Value), 'BUSINESSUNIT_CODE', False);
    ComboBoxPlusBusinessFrom.Visible := True;
    ComboBoxPlusBusinessTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusBusinessFrom.Visible := False;
    ComboBoxPlusBusinessTo.Visible := False;
  end;
end;
{
procedure TDialogReportDirIndHrsWeekF.FillWorkSpot;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) then
  begin
      QueryWorkSpot.ParamByName('USER_NAME').AsString := GetLoginUser;
      ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotFrom, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', True);
    ListProcsF.FillComboBoxMasterPlant(
      QueryWorkSpot, ComboBoxPlusWorkspotTo, GetStrValue(CmbPlusPlantFrom.Value),
      'WORKSPOT_CODE', False);
    ComboBoxPlusWorkspotFrom.Visible := True;
    ComboBoxPlusWorkspotTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusWorkspotFrom.Visible := False;
    ComboBoxPlusWorkspotTo.Visible := False;
  end;
end;
}
{
procedure TDialogReportDirIndHrsWeekF.FillJobCode;
begin
  if (CmbPlusPlantFrom.Value <> '') and
     (CmbPlusPlantFrom.Value = CmbPlusPlantTo.Value) and
     (CmbPlusWorkspotFrom.Value <> '') and
     (CmbPlusWorkspotFrom.Value = CmbPlusWorkspotTo.Value) then
  begin
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeFrom,
        GetStrValue(CmbPlusPlantFrom.Value),
        GetStrValue(CmbPlusWorkspotFrom.Value), True);
    ListProcsF.FillComboBoxJobCode(QueryJobCode, ComboBoxPlusJobCodeTo,
       GetStrValue(CmbPlusPlantFrom.Value),
       GetStrValue(CmbPlusWorkspotFrom.Value), False);
    ComboBoxPlusJobCodeFrom.Visible := True;
    ComboBoxPlusJobCodeTo.Visible := True;
  end
  else
  begin
    ComboBoxPlusJobCodeFrom.Visible := False;
    ComboBoxPlusJobCodeTo.Visible := False;
  end;
end;
}
procedure TDialogReportDirIndHrsWeekF.FormShow(Sender: TObject);
var
  Year, Week: Word;
begin
  InitDialog(True, True, False, True, False, True, True);
  inherited;
  ListProcsF.WeekUitDat(Now, Year, Week);
  dxSpinEditYear.Value := Year;
  dxSpinEditWeekFrom.Value := Week;
  dxSpinEditWeekTo.Value := ListProcsF.WeeksInYear(Year);
  dxSpinEditWeekFrom.MaxValue := ListProcsF.WeeksInYear(Year);
  dxSpinEditWeekTo.MaxValue := ListProcsF.WeeksInYear(Year);
  CheckBoxShowSelection.Checked := True;
  ChangeDate(Sender);
end;

procedure TDialogReportDirIndHrsWeekF.FormCreate(Sender: TObject);
var
  Year, Week: Word;
begin
  inherited;
  ListProcsF.WeekUitDat(Now, Year, Week);
  dxSpinEditYear.Value := Year;

// CREATE data module of report
  ReportDirIndHrsWeekDM := CreateReportDM(TReportDirIndHrsWeekDM);
  ReportDirIndHrsWeekQR := CreateReportQR(TReportDirIndHrsWeekQR);
end;

procedure TDialogReportDirIndHrsWeekF.CmbPlusEmployeeFromChange(
  Sender: TObject);
begin
  inherited;
 
end;

// MR:05-12-2003
procedure TDialogReportDirIndHrsWeekF.ChangeDate(
  Sender: TObject);
var
  MaxWeeks: Integer;
begin
  inherited;
  try
    // Set max of weeks
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekFrom.MaxValue := MaxWeeks;
    if dxSpinEditWeekFrom.Value > MaxWeeks then
      dxSpinEditWeekFrom.Value := MaxWeeks;
  except
    dxSpinEditWeekFrom.Value := 1;
  end;

  try
    MaxWeeks := ListProcsF.WeeksInYear(dxSpinEditYear.IntValue);
    dxSpinEditWeekTo.MaxValue := MaxWeeks;
    if dxSpinEditWeekTo.Value > MaxWeeks then
      dxSpinEditWeekTo.Value := MaxWeeks;
  except
    dxSpinEditWeekTo.Value := 1;
  end;
  ShowPeriod;
end;

procedure TDialogReportDirIndHrsWeekF.ComboBoxPlusBusinessFromCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
    (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if  GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessTo.DisplayValue :=
          ComboBoxPlusBusinessFrom.DisplayValue;
end;

procedure TDialogReportDirIndHrsWeekF.ComboBoxPlusBusinessToCloseUp(
  Sender: TObject);
begin
  inherited;
  if (ComboBoxPlusBusinessFrom.DisplayValue <> '') and
     (ComboBoxPlusBusinessTo.DisplayValue <> '') then
    if  GetStrValue(ComboBoxPlusBusinessFrom.Value) >
      GetStrValue(ComboBoxPlusBusinessTo.Value) then
        ComboBoxPlusBusinessFrom.DisplayValue :=
          ComboBoxPlusBusinessTo.DisplayValue;
end;

procedure TDialogReportDirIndHrsWeekF.ComboBoxPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotTo.DisplayValue :=
          ComboBoxPlusWorkSpotFrom.DisplayValue;
  FillJobCode; }
end;

procedure TDialogReportDirIndHrsWeekF.ComboBoxPlusWorkSpotToCloseUp(
  Sender: TObject);
begin
  inherited;
{  if (ComboBoxPlusWorkSpotFrom.DisplayValue <> '') and
     (ComboBoxPlusWorkSpotTo.DisplayValue <> '') then
    if GetStrValue(ComboBoxPlusWorkSpotFrom.Value) >
      GetStrValue(ComboBoxPlusWorkSpotTo.Value) then
        ComboBoxPlusWorkSpotFrom.DisplayValue :=
          ComboBoxPlusWorkSpotTo.DisplayValue;
  FillJobCode; }
end;

procedure TDialogReportDirIndHrsWeekF.ShowPeriod;
begin
  try
    lblPeriod.Caption :=
      '(' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekFrom.Value), 1)) + ' - ' +
      DateTimeToStr(ListProcsF.DateFromWeek(Round(dxSpinEditYear.Value),
      Round(dxSpinEditWeekTo.Value), 7)) + ')';
  except
    lblPeriod.Caption := '';
  end;
end;

procedure TDialogReportDirIndHrsWeekF.FillAll;
begin
  inherited;
  FillBusiness;
//  FillWorkSpot;
//  FillJobCode;
end;

// RV089.1. Ensure the form stays on top of the calling form.
procedure TDialogReportDirIndHrsWeekF.CreateParams(
  var params: TCreateParams);
begin
  inherited;
  if (DialogReportDirIndHrsWeekF_HND = nil) then
  begin
    params.WndParent := Screen.ActiveForm.Handle;

    if (params.WndParent <> 0) and (IsIconic(params.WndParent)
      or not IsWindowVisible(params.WndParent)
      or not IsWindowEnabled(params.WndParent)) then
      params.WndParent := 0;

    if params.WndParent = 0 then
      params.WndParent := Application.Handle;
  end;
end;

procedure TDialogReportDirIndHrsWeekF.CmbPlusWorkspotFromCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

procedure TDialogReportDirIndHrsWeekF.CmbPlusWorkspotToCloseUp(
  Sender: TObject);
begin
  inherited;
//  FillJobCode;
end;

end.
